package com.portfolio.platform.gson;

import com.fossil.fu3;
import com.fossil.ju3;
import com.fossil.ku3;
import com.fossil.mu3;
import com.fossil.ou3;
import com.fossil.pu3;
import com.fossil.rc6;
import com.fossil.vi4;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetAppSettingSerializer implements pu3<List<? extends HybridPresetAppSetting>> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(List<HybridPresetAppSetting> list, Type type, ou3 ou3) {
        wg6.b(list, "src");
        fu3 fu3 = new fu3();
        mu3 mu3 = new mu3();
        for (HybridPresetAppSetting next : list) {
            String component1 = next.component1();
            String component2 = next.component2();
            String component3 = next.component3();
            String component4 = next.component4();
            ku3 ku3 = new ku3();
            ku3.a("appId", component2);
            ku3.a("buttonPosition", component1);
            ku3.a("localUpdatedAt", component3);
            if (!vi4.a(component4)) {
                try {
                    ku3 a = mu3.a(component4);
                    if (a != null) {
                        ku3.a(Constants.USER_SETTING, a);
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception unused) {
                    ku3.a(Constants.USER_SETTING, new ju3());
                }
            } else {
                ku3.a(Constants.USER_SETTING, new ju3());
            }
            fu3.a(ku3);
        }
        return fu3;
    }
}
