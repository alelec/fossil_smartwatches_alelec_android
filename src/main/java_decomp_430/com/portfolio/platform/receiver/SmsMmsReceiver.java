package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rl4;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sl4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl4;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SmsMmsReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public DianaNotificationComponent a;
    @DexIgnore
    public an4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.receiver.SmsMmsReceiver$onReceive$1", f = "SmsMmsReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $notificationInfo;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(jh6 jh6, xe6 xe6) {
            super(2, xe6);
            this.$notificationInfo = jh6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$notificationInfo, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                String defaultSmsPackage = Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance());
                if (defaultSmsPackage == null || NotificationAppHelper.b.a(defaultSmsPackage)) {
                    FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive() - SMS app filter is assigned - ignore");
                } else {
                    LightAndHapticsManager.i.a().a(this.$notificationInfo.element);
                    FLogger.INSTANCE.getLocal().d(SmsMmsReceiver.c, "onReceive) - SMS app filter is not assigned - add to Queue");
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = SmsMmsReceiver.class.getSimpleName();
        wg6.a((Object) simpleName, "SmsMmsReceiver::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        sl4 a2;
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        jh6 jh6 = new jh6();
        jh6.element = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        StringBuilder sb = new StringBuilder();
        sb.append("SmsMmsReceiver : ");
        String action = intent.getAction();
        if (action != null) {
            sb.append(action);
            local.d(str, sb.toString());
            an4 an4 = this.b;
            if (an4 == null) {
                wg6.d("mSharePref");
                throw null;
            } else if (an4.N()) {
                FLogger.INSTANCE.getLocal().d(c, "SmsMmsReceiver user new solution");
            } else {
                String str2 = "";
                if (TextUtils.isEmpty(intent.getAction()) || !wg6.a((Object) intent.getAction(), (Object) "android.provider.Telephony.WAP_PUSH_RECEIVED")) {
                    try {
                        SmsMessage[] messagesFromIntent = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                        if (messagesFromIntent != null) {
                            if (!(messagesFromIntent.length == 0)) {
                                for (SmsMessage smsMessage : messagesFromIntent) {
                                    wg6.a((Object) smsMessage, "message");
                                    String messageBody = smsMessage.getMessageBody();
                                    String originatingAddress = smsMessage.getOriginatingAddress();
                                    jh6.element = new NotificationInfo(NotificationSource.TEXT, originatingAddress, messageBody, str2);
                                    if (!TextUtils.isEmpty(messageBody) && !TextUtils.isEmpty(originatingAddress)) {
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - SMS - error " + e);
                    }
                } else {
                    try {
                        if (intent.hasExtra("data") && (a2 = new zl4(intent.getByteArrayExtra("data")).a()) != null && a2.a() != null) {
                            rl4 a3 = a2.a();
                            wg6.a((Object) a3, "pdu.from");
                            String a4 = a3.a();
                            FLogger.INSTANCE.getLocal().d(c, "onReceive() - MMS - originatingAddress = " + a4);
                            jh6.element = new NotificationInfo(NotificationSource.TEXT, a4, str2, str2);
                        }
                    } catch (Exception e2) {
                        FLogger.INSTANCE.getLocal().e(c, "onReceive() - MMS - error " + e2);
                    }
                }
                if (((NotificationInfo) jh6.element) != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("onReceive() - SMSMMS - sender info ");
                    String senderInfo = ((NotificationInfo) jh6.element).getSenderInfo();
                    if (senderInfo != null) {
                        sb2.append(senderInfo);
                        local2.d(str3, sb2.toString());
                        if (!FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.get.instance().e())) {
                            an4 an42 = this.b;
                            if (an42 == null) {
                                wg6.d("mSharePref");
                                throw null;
                            } else if (an42.E()) {
                                FLogger.INSTANCE.getLocal().d(c, "onReceive() - Blocked by DND mode");
                            } else {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String str4 = c;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("onReceive() - SMS-MMS - sender info ");
                                String senderInfo2 = ((NotificationInfo) jh6.element).getSenderInfo();
                                if (senderInfo2 != null) {
                                    sb3.append(senderInfo2);
                                    local3.d(str4, sb3.toString());
                                    rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new b(jh6, (xe6) null), 3, (Object) null);
                                    return;
                                }
                                wg6.a();
                                throw null;
                            }
                        } else {
                            if (((NotificationInfo) jh6.element).getSenderInfo() != null) {
                                str2 = ((NotificationInfo) jh6.element).getSenderInfo();
                            }
                            DianaNotificationComponent dianaNotificationComponent = this.a;
                            if (dianaNotificationComponent == null) {
                                wg6.d("mDianaNotificationComponent");
                                throw null;
                            } else if (str2 != null) {
                                String body = ((NotificationInfo) jh6.element).getBody();
                                wg6.a((Object) body, "notificationInfo.body");
                                Calendar instance = Calendar.getInstance();
                                wg6.a((Object) instance, "Calendar.getInstance()");
                                dianaNotificationComponent.a(str2, body, instance.getTimeInMillis());
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
