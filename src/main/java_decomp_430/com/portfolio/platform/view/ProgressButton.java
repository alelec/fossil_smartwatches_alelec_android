package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.appcompat.app.AppCompatActivity;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.ty5;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.x9;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProgressButton extends FlexibleButton {
    @DexIgnore
    public ty5 A;
    @DexIgnore
    public ViewGroup B;
    @DexIgnore
    public FrameLayout C;
    @DexIgnore
    public String D; // = "";
    @DexIgnore
    public String E; // = "";
    @DexIgnore
    public String F; // = "";
    @DexIgnore
    public String G; // = "";
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public int x;
    @DexIgnore
    public long y;
    @DexIgnore
    public Drawable[] z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context) {
        super(context);
        wg6.b(context, "context");
        a(context, (AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    public final void a() {
        CharSequence charSequence;
        ty5 ty5 = this.A;
        if (ty5 != null) {
            if (!this.v) {
                if (ty5 != null) {
                    charSequence = ty5.a(isSelected());
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (ty5 != null) {
                charSequence = ty5.a();
            } else {
                wg6.a();
                throw null;
            }
            setText(charSequence);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v0, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    public final void b() {
        ty5 ty5 = this.A;
        if (ty5 != null) {
            setText(ty5.a(isSelected()));
            Drawable[] drawableArr = this.z;
            if (drawableArr != null) {
                if (drawableArr != null) {
                    Drawable drawable = drawableArr[0];
                    if (drawableArr != null) {
                        Drawable drawable2 = drawableArr[1];
                        if (drawableArr != null) {
                            Drawable drawable3 = drawableArr[2];
                            if (drawableArr != null) {
                                setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawableArr[3]);
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            if (this.u) {
                setClickable(true);
            }
            this.v = false;
            this.x = 0;
            ViewGroup viewGroup = this.B;
            if (viewGroup == null) {
                return;
            }
            if (viewGroup != null) {
                viewGroup.removeView(this.C);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    public final void c() {
        ty5 ty5 = this.A;
        if (ty5 != null) {
            setText(ty5.a());
            this.z = (Drawable[]) Arrays.copyOf(getCompoundDrawables(), 4);
            setCompoundDrawables((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
            if (this.u) {
                setClickable(false);
            }
            this.v = true;
            this.x = 0;
            if (this.B != null) {
                FrameLayout frameLayout = this.C;
                if (frameLayout != null) {
                    ViewParent parent = frameLayout.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(this.C);
                    }
                    ViewGroup viewGroup = this.B;
                    if (viewGroup != null) {
                        viewGroup.addView(this.C);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r10v0, types: [android.widget.Button, android.view.View, com.portfolio.platform.view.ProgressButton] */
    public void onDraw(Canvas canvas) {
        wg6.b(canvas, "canvas");
        ProgressButton.super.onDraw(canvas);
        if (this.w != null && this.v) {
            canvas.save();
            int width = getWidth();
            Drawable drawable = this.w;
            if (drawable != null) {
                int minimumWidth = (width - drawable.getMinimumWidth()) / 2;
                int height = getHeight();
                Drawable drawable2 = this.w;
                if (drawable2 != null) {
                    canvas.translate((float) minimumWidth, (float) ((height - drawable2.getMinimumHeight()) / 2));
                    long drawingTime = getDrawingTime();
                    if (drawingTime - this.y > 100) {
                        this.y = drawingTime;
                        this.x++;
                        if (((long) this.x) >= 12) {
                            this.x = 0;
                        }
                    }
                    int i = (int) (((float) (this.x * 10000)) / ((float) 12));
                    Drawable drawable3 = this.w;
                    if (drawable3 != null) {
                        drawable3.setLevel(i);
                        Drawable drawable4 = this.w;
                        if (drawable4 != null) {
                            drawable4.draw(canvas);
                            canvas.restore();
                            x9.I(this);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton] */
    public void setSelected(boolean z2) {
        ProgressButton.super.setSelected(z2);
        a();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(context, attributeSet);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [android.widget.Button, com.portfolio.platform.view.ProgressButton, com.portfolio.platform.view.FlexibleButton] */
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.ProgressButton);
        if (obtainStyledAttributes.hasValue(4)) {
            this.w = obtainStyledAttributes.getDrawable(4);
        }
        ty5 ty5 = new ty5();
        String str = "";
        ty5.a((CharSequence) str);
        ty5.b(getText());
        ty5.c(getText());
        if (obtainStyledAttributes.hasValue(6)) {
            ty5.b(obtainStyledAttributes.getString(6));
        }
        if (obtainStyledAttributes.hasValue(9)) {
            ty5.c(obtainStyledAttributes.getString(9));
        }
        if (obtainStyledAttributes.hasValue(5)) {
            ty5.a((CharSequence) obtainStyledAttributes.getString(5));
        }
        if (obtainStyledAttributes.hasValue(3)) {
            this.v = obtainStyledAttributes.getBoolean(3, false);
        }
        if (obtainStyledAttributes.hasValue(0)) {
            this.u = obtainStyledAttributes.getBoolean(0, true);
        }
        if (obtainStyledAttributes.hasValue(7)) {
            String string = obtainStyledAttributes.getString(7);
            if (string == null) {
                string = str;
            }
            this.D = string;
        }
        if (obtainStyledAttributes.hasValue(8)) {
            String string2 = obtainStyledAttributes.getString(8);
            if (string2 == null) {
                string2 = str;
            }
            this.E = string2;
        }
        if (obtainStyledAttributes.hasValue(1)) {
            String string3 = obtainStyledAttributes.getString(1);
            if (string3 == null) {
                string3 = str;
            }
            this.F = string3;
        }
        if (obtainStyledAttributes.hasValue(2)) {
            String string4 = obtainStyledAttributes.getString(2);
            if (string4 != null) {
                str = string4;
            }
            this.G = str;
        }
        this.A = ty5;
        obtainStyledAttributes.recycle();
        if (context != null) {
            try {
                Window window = ((AppCompatActivity) context).getWindow();
                wg6.a((Object) window, "(context as AppCompatActivity).window");
                this.B = (ViewGroup) window.getDecorView().findViewById(16908290);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            this.C = new FrameLayout(getContext());
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            FrameLayout frameLayout = this.C;
            if (frameLayout != null) {
                frameLayout.setBackgroundColor(0);
                FrameLayout frameLayout2 = this.C;
                if (frameLayout2 != null) {
                    frameLayout2.setClickable(true);
                    FrameLayout frameLayout3 = this.C;
                    if (frameLayout3 != null) {
                        frameLayout3.setLayoutParams(layoutParams);
                        Drawable drawable = this.w;
                        if (drawable != null) {
                            if (drawable == null) {
                                wg6.a();
                                throw null;
                            } else if (drawable != null) {
                                int intrinsicWidth = drawable.getIntrinsicWidth();
                                Drawable drawable2 = this.w;
                                if (drawable2 != null) {
                                    drawable.setBounds(0, 0, intrinsicWidth, drawable2.getIntrinsicHeight());
                                    if (this.v) {
                                        c();
                                    } else {
                                        b();
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        if (!TextUtils.isEmpty(this.D) || !TextUtils.isEmpty(this.E) || TextUtils.isEmpty(this.F) || TextUtils.isEmpty(this.G)) {
                            a(this.D, this.E, this.F, this.G);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        throw new rc6("null cannot be cast to non-null type androidx.appcompat.app.AppCompatActivity");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(context, attributeSet);
    }
}
