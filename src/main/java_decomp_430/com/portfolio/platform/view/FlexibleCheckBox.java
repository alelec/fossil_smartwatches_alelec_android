package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.fossil.wg6;
import com.fossil.x24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleCheckBox extends AppCompatCheckBox {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleCheckBox);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = "";
            }
            this.d = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "";
            }
            this.e = string2;
            obtainStyledAttributes.recycle();
        }
        b();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v0, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    public final void b() {
        if (!TextUtils.isEmpty(this.d) && !TextUtils.isEmpty(this.e)) {
            String b = ThemeManager.l.a().b(this.e);
            String b2 = ThemeManager.l.a().b(this.d);
            if (!TextUtils.isEmpty(b) && !TextUtils.isEmpty(b2)) {
                setButtonTintList(new ColorStateList(new int[][]{new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(b2), Color.parseColor(b)}));
            }
        }
    }

    @DexIgnore
    public final void setEnableColor(String str) {
        wg6.b(str, "color");
        this.e = str;
        b();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }
}
