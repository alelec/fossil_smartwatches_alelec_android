package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseFitnessChart extends View implements ViewTreeObserver.OnGlobalLayoutListener {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public TypedArray a;
    @DexIgnore
    public Bitmap b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = BaseFitnessChart.class.getSimpleName();
        wg6.a((Object) simpleName, "BaseFitnessChart::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseFitnessChart(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a() {
        Bitmap bitmap = this.b;
        if (bitmap != null) {
            if (bitmap == null) {
                wg6.d("mStarBitmap");
                throw null;
            } else if (!bitmap.isRecycled()) {
                return;
            }
        }
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), getStarIconResId());
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeResource, getStarSizeInPx(), getStarSizeInPx(), false);
        wg6.a((Object) createScaledBitmap, "Bitmap.createScaledBitma\u2026nPx, starSizeInPx, false)");
        this.b = createScaledBitmap;
        Bitmap bitmap2 = this.b;
        if (bitmap2 == null) {
            wg6.d("mStarBitmap");
            throw null;
        } else if (!wg6.a((Object) bitmap2, (Object) decodeResource)) {
            decodeResource.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        Bitmap bitmap = this.b;
        if (bitmap == null) {
            return;
        }
        if (bitmap != null) {
            bitmap.recycle();
        } else {
            wg6.d("mStarBitmap");
            throw null;
        }
    }

    @DexIgnore
    public final TypedArray getMTypedArray() {
        return this.a;
    }

    @DexIgnore
    public abstract int getStarIconResId();

    @DexIgnore
    public abstract int getStarSizeInPx();

    @DexIgnore
    public final Bitmap getStartBitmap() {
        if (this.b == null) {
            a();
        }
        Bitmap bitmap = this.b;
        if (bitmap != null) {
            return bitmap;
        }
        wg6.d("mStarBitmap");
        throw null;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        FLogger.INSTANCE.getLocal().d(c, "onAttachedToWindow, initStartBitmap");
        super.onAttachedToWindow();
        a();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        FLogger.INSTANCE.getLocal().d(c, "onDetachedFromWindow, recycleStarBitmap");
        super.onDetachedFromWindow();
        b();
    }

    @DexIgnore
    public final void setMTypedArray(TypedArray typedArray) {
        this.a = typedArray;
    }
}
