package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.d;
import com.fossil.d7;
import com.fossil.gy5;
import com.fossil.iz5;
import com.fossil.jm4;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSleepSessionChart extends BaseChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public Integer B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public int N;
    @DexIgnore
    public short O;
    @DexIgnore
    public short P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public ArrayList<iz5> U;
    @DexIgnore
    public float V;
    @DexIgnore
    public boolean W; // = true;
    @DexIgnore
    public String u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public String y;
    @DexIgnore
    public String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Path a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(Path path, int i) {
            wg6.b(path, "path");
            this.a = path;
            this.b = i;
        }

        @DexIgnore
        public final Path a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return wg6.a((Object) this.a, (Object) bVar.a) && this.b == bVar.b;
        }

        @DexIgnore
        public int hashCode() {
            Path path = this.a;
            return ((path != null ? path.hashCode() : 0) * 31) + d.a(this.b);
        }

        @DexIgnore
        public String toString() {
            return "PathDist(path=" + this.a + ", sleepState=" + this.b + ")";
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v26, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSleepSessionChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface c;
        String b2;
        String b3;
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        String str = "";
        this.u = str;
        this.y = str;
        this.z = str;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.HeartRateSleepSessionChart);
        String string = obtainStyledAttributes.getString(1);
        this.u = string == null ? str : string;
        this.v = obtainStyledAttributes.getColor(0, w6.a(context, R.color.activeTime));
        this.w = obtainStyledAttributes.getColor(5, w6.a(context, 2131099846));
        this.x = obtainStyledAttributes.getColor(2, w6.a(context, 2131099846));
        String string2 = obtainStyledAttributes.getString(6);
        this.y = string2 == null ? str : string2;
        String string3 = obtainStyledAttributes.getString(4);
        this.z = string3 != null ? string3 : str;
        this.A = obtainStyledAttributes.getDimension(3, gy5.c(13.0f));
        this.B = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
        this.C = obtainStyledAttributes.getDimension(7, gy5.c(13.0f));
        obtainStyledAttributes.recycle();
        this.Q = new Paint();
        if (!TextUtils.isEmpty(this.u) && (b3 = ThemeManager.l.a().b(this.u)) != null) {
            this.Q.setColor(Color.parseColor(b3));
        }
        this.Q.setStrokeWidth(2.0f);
        this.Q.setStyle(Paint.Style.STROKE);
        this.R = new Paint(1);
        if (!TextUtils.isEmpty(this.y) && (b2 = ThemeManager.l.a().b(this.y)) != null) {
            this.R.setColor(Color.parseColor(b2));
        }
        this.R.setStyle(Paint.Style.FILL);
        this.R.setTextSize(this.A);
        if (!TextUtils.isEmpty(this.z) && (c = ThemeManager.l.a().c(this.z)) != null) {
            this.R.setTypeface(c);
        }
        this.S = new Paint(1);
        this.S.setColor(this.R.getColor());
        this.S.setStyle(Paint.Style.FILL);
        this.S.setTextSize(this.C);
        Integer num = this.B;
        if (num == null || num.intValue() != -1) {
            Paint paint = this.S;
            Integer num2 = this.B;
            if (num2 != null) {
                paint.setTypeface(d7.a(context, num2.intValue()));
            } else {
                wg6.a();
                throw null;
            }
        }
        this.T = new Paint(1);
        this.T.setStrokeWidth(gy5.a(1.0f));
        this.T.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.R.getTextBounds("2222", 0, 4, rect);
        this.K = (float) rect.width();
        this.L = (float) rect.height();
        Rect rect2 = new Rect();
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886974);
        this.S.getTextBounds(a2, 0, a2.length(), rect2);
        this.M = (float) rect2.height();
        this.U = new ArrayList<>();
        a();
    }

    @DexIgnore
    public final void a(int i, int i2, int i3) {
        this.v = i3;
        this.w = i2;
        this.x = i;
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.b(canvas);
        float f = (float) 2;
        this.D = ((float) canvas.getHeight()) - (this.M * f);
        this.E = 25.0f;
        this.F = ((float) canvas.getWidth()) + 30.0f;
        this.G = this.C;
        this.H = this.E + gy5.a(5.0f);
        this.J = this.D - (this.C * f);
        this.I = (this.F - this.K) + gy5.a(10.0f);
        this.V = (this.I - this.H) / ((float) this.N);
        f(canvas);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.E, this.J, (float) canvas.getWidth(), this.J, this.Q);
        canvas.drawLine(this.E, this.G, (float) canvas.getWidth(), this.G, this.Q);
        if (this.W) {
            canvas.drawText(String.valueOf(this.O), this.I + ((((float) canvas.getWidth()) - this.I) - this.R.measureText(String.valueOf(this.O))), this.J + (this.L * 1.5f), this.R);
            canvas.drawText(String.valueOf(this.P), this.I + ((((float) canvas.getWidth()) - this.I) - this.R.measureText(String.valueOf(this.P))), this.G - (this.L * 0.5f), this.R);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        e(canvas);
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        int i;
        int i2;
        ArrayList<b> arrayList = new ArrayList<>();
        iz5 iz5 = (iz5) yd6.e(this.U);
        int d = iz5 != null ? iz5.d() : -1;
        Iterator<T> it = this.U.iterator();
        int i3 = d;
        Path path = null;
        boolean z2 = false;
        int i4 = 0;
        while (true) {
            boolean z3 = true;
            if (!it.hasNext()) {
                break;
            }
            iz5 iz52 = (iz5) it.next();
            int a2 = iz52.a();
            int b2 = iz52.b();
            int c = iz52.c();
            float f = this.H + (((float) b2) * this.V);
            float f2 = this.G;
            float f3 = this.J;
            short s = this.P;
            float f4 = f2 + (((f3 - f2) / ((float) (s - this.O))) * ((float) (s - a2)));
            if (!z2 || f4 > f3 || i3 != c) {
                if (!(f4 > this.J || i3 == c || path == null)) {
                    path.lineTo(f, f4);
                }
                if (path != null) {
                    if (i4 > 1) {
                        arrayList.add(new b(path, i3));
                    }
                    path = null;
                }
                if (f4 <= this.J) {
                    path = new Path();
                    path.moveTo(f, f4);
                    i2 = 1;
                } else {
                    i2 = 0;
                    z3 = false;
                }
                i4 = i2;
                i3 = c;
                z2 = z3;
            } else {
                if (path != null) {
                    path.lineTo(f, f4);
                }
                i4++;
            }
        }
        if (path != null && i4 > 1) {
            arrayList.add(new b(path, i3));
        }
        for (b bVar : arrayList) {
            Path a3 = bVar.a();
            int b3 = bVar.b();
            Paint paint = this.T;
            if (b3 == 0) {
                i = this.v;
            } else if (b3 != 1) {
                i = this.x;
            } else {
                i = this.w;
            }
            paint.setColor(i);
            canvas.drawPath(a3, this.T);
        }
    }

    @DexIgnore
    public final int getMDuration() {
        return this.N;
    }

    @DexIgnore
    public final short getMMaxHRValue() {
        return this.P;
    }

    @DexIgnore
    public final short getMMinHRValue() {
        return this.O;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void setMDuration(int i) {
        this.N = i;
    }

    @DexIgnore
    public final void setMMaxHRValue(short s) {
        if (s == ((short) 0)) {
            s = (short) 100;
        }
        this.P = s;
    }

    @DexIgnore
    public final void setMMinHRValue(short s) {
        this.O = s;
    }

    @DexIgnore
    public final void a(ArrayList<iz5> arrayList) {
        wg6.b(arrayList, "heartRatePointList");
        this.U.clear();
        this.U.addAll(arrayList);
        this.W = !this.U.isEmpty();
        getMGraph().invalidate();
    }
}
