package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.fossil.d7;
import com.fossil.gy5;
import com.fossil.gz5;
import com.fossil.hz5;
import com.fossil.jm4;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.yd6;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WLHeartRateChart extends View {
    @DexIgnore
    public short A;
    @DexIgnore
    public short B;
    @DexIgnore
    public Paint C;
    @DexIgnore
    public Paint D;
    @DexIgnore
    public Paint E;
    @DexIgnore
    public Paint F;
    @DexIgnore
    public Paint G;
    @DexIgnore
    public Paint H;
    @DexIgnore
    public Paint I;
    @DexIgnore
    public List<RectF> J;
    @DexIgnore
    public b K; // = b.AVERAGE;
    @DexIgnore
    public List<hz5> L;
    @DexIgnore
    public int M; // = c.NONE.ordinal();
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Integer e;
    @DexIgnore
    public float f;
    @DexIgnore
    public Integer g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public short y;
    @DexIgnore
    public short z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        AVERAGE,
        RESTING,
        PEAK
    }

    @DexIgnore
    public enum c {
        GENERAL,
        DAY,
        WEEK,
        MONTH,
        NONE
    }

    /*
    static {
        new a((qg6) null);
        wg6.a((Object) WLHeartRateChart.class.getSimpleName(), "WLHeartRateChart::class.java.simpleName");
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WLHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.WLHeartRateChart);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.getColor(2, -3355444);
            this.a = obtainStyledAttributes.getColor(0, -3355444);
            this.b = obtainStyledAttributes.getColor(1, -3355444);
            this.c = obtainStyledAttributes.getColor(10, -3355444);
            this.d = obtainStyledAttributes.getColor(6, -3355444);
            this.e = Integer.valueOf(obtainStyledAttributes.getResourceId(4, -1));
            this.f = obtainStyledAttributes.getDimension(3, gy5.c(13.0f));
            this.g = Integer.valueOf(obtainStyledAttributes.getResourceId(8, -1));
            this.h = obtainStyledAttributes.getDimension(7, gy5.c(13.0f));
            this.i = obtainStyledAttributes.getDimension(5, gy5.a(4.0f));
            this.M = obtainStyledAttributes.getInt(9, c.NONE.ordinal());
            this.C = new Paint();
            this.C.setColor(this.a);
            this.C.setStrokeWidth(2.0f);
            this.C.setStyle(Paint.Style.STROKE);
            this.D = new Paint();
            this.D.setColor(this.a);
            this.D.setStyle(Paint.Style.STROKE);
            this.D.setPathEffect(new DashPathEffect(new float[]{6.0f, 6.0f}, 0.0f));
            this.E = new Paint(1);
            this.E.setColor(this.d);
            this.E.setStyle(Paint.Style.FILL);
            this.E.setTextSize(this.f);
            Integer num = this.e;
            if (num == null || num.intValue() != -1) {
                Paint paint = this.E;
                Integer num2 = this.e;
                if (num2 != null) {
                    paint.setTypeface(d7.a(context, num2.intValue()));
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.F = new Paint(1);
            this.F.setColor(this.d);
            this.F.setStyle(Paint.Style.FILL);
            this.F.setTextSize(this.h);
            Integer num3 = this.g;
            if (num3 == null || num3.intValue() != -1) {
                Paint paint2 = this.F;
                Integer num4 = this.g;
                if (num4 != null) {
                    paint2.setTypeface(d7.a(context, num4.intValue()));
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.G = new Paint(1);
            this.G.setColor(this.b);
            this.G.setStyle(Paint.Style.FILL);
            this.H = new Paint(1);
            this.H.setColor(-1);
            this.H.setStyle(Paint.Style.FILL);
            this.I = new Paint(1);
            this.I.setColor(this.c);
            this.I.setStyle(Paint.Style.STROKE);
            Rect rect = new Rect();
            this.E.getTextBounds("222", 0, 3, rect);
            this.v = (float) rect.width();
            this.w = (float) rect.height();
            Rect rect2 = new Rect();
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886974);
            this.F.getTextBounds(a2, 0, a2.length(), rect2);
            this.x = (float) rect2.height();
            this.L = new ArrayList();
            this.J = new ArrayList();
            obtainStyledAttributes.recycle();
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(Canvas canvas) {
        e(canvas);
        float size = (this.t - this.s) / ((float) ((this.L.size() + this.L.size()) - 1));
        a(canvas, size, ((this.t - this.s) - (((float) this.L.size()) * size)) / ((float) (this.L.size() - 1)));
        a(canvas, qd6.d(jm4.a((Context) PortfolioApp.get.instance(), 2131886974), jm4.a((Context) PortfolioApp.get.instance(), 2131886975), jm4.a((Context) PortfolioApp.get.instance(), 2131886974)), qd6.d(Float.valueOf(this.J.get(0).left), Float.valueOf(this.J.get(11).left), Float.valueOf(this.J.get(23).left)), this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2)));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(Canvas canvas) {
        this.u = this.o;
        f(canvas);
        float size = (this.t - this.s) / ((float) ((this.L.size() + this.L.size()) - 1));
        a(canvas, size, ((this.t - this.s) - (((float) this.L.size()) * size)) / ((float) (this.L.size() - 1)));
        a(canvas, qd6.d(jm4.a((Context) PortfolioApp.get.instance(), 2131886974), jm4.a((Context) PortfolioApp.get.instance(), 2131886975), jm4.a((Context) PortfolioApp.get.instance(), 2131886974)), qd6.d(Float.valueOf(this.J.get(0).left), Float.valueOf(this.J.get(11).left), Float.valueOf(this.J.get(23).left)), this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2)));
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        e(canvas);
        float size = (this.t - this.s) / ((float) ((this.L.size() + this.L.size()) - 1));
        a(canvas, size, ((this.t - this.s) - (((float) this.L.size()) * size)) / ((float) (this.L.size() - 1)));
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        float measuredHeight = this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2));
        int size2 = this.L.size() / 4;
        int i2 = 0;
        int i3 = 0;
        while (i2 < this.L.size()) {
            arrayList.add(String.valueOf(i2 + 1));
            arrayList2.add(Float.valueOf(this.J.get(i2).left));
            int i4 = i3 + 1;
            int i5 = i3 + (size2 * i4);
            i3 = i4;
            i2 = i5;
        }
        arrayList.add(String.valueOf(this.J.size()));
        List<RectF> list = this.J;
        arrayList2.add(Float.valueOf(list.get(qd6.a(list)).left));
        a(canvas, arrayList, arrayList2, measuredHeight);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v21, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void d(Canvas canvas) {
        e(canvas);
        float f2 = this.t;
        float f3 = this.s;
        float f4 = (f2 - f3) / ((float) 48);
        a(canvas, f4, ((f2 - f3) - (((float) 7) * f4)) / ((float) 6));
        List d2 = qd6.d(jm4.a((Context) PortfolioApp.get.instance(), 2131886537), jm4.a((Context) PortfolioApp.get.instance(), 2131886536), jm4.a((Context) PortfolioApp.get.instance(), 2131886539), jm4.a((Context) PortfolioApp.get.instance(), 2131886541), jm4.a((Context) PortfolioApp.get.instance(), 2131886540), jm4.a((Context) PortfolioApp.get.instance(), 2131886535), jm4.a((Context) PortfolioApp.get.instance(), 2131886538));
        List<RectF> list = this.J;
        ArrayList arrayList = new ArrayList(rd6.a(list, 10));
        for (RectF rectF : list) {
            arrayList.add(Float.valueOf(rectF.left));
        }
        a(canvas, d2, yd6.d(arrayList), this.o + (((((float) getMeasuredHeight()) - this.o) + this.x) / ((float) 2)));
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        canvas.drawLine(this.p, this.o, (float) getMeasuredWidth(), this.o, this.C);
        canvas.drawLine(this.p, this.j, (float) getMeasuredWidth(), this.j, this.C);
        float f2 = (float) 2;
        float measuredWidth = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.A))) / f2);
        float f3 = this.u;
        float f4 = (float) 1;
        float f5 = (float) 4;
        float f6 = f3 - (((f3 - this.j) * f4) / f5);
        canvas.drawText(String.valueOf(this.A), measuredWidth, (this.w / f2) + f6, this.E);
        float measuredWidth2 = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.B))) / f2);
        float f7 = this.r;
        float f8 = f7 + (((this.u - f7) * f4) / f5);
        canvas.drawText(String.valueOf(this.B), measuredWidth2, (this.w / f2) + f8, this.E);
        Path path = new Path();
        path.moveTo(this.p, f6);
        path.lineTo(this.q, f6);
        canvas.drawPath(path, this.D);
        path.moveTo(this.p, f8);
        path.lineTo(this.q, f8);
        canvas.drawPath(path, this.D);
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        float f2 = (float) 2;
        float measuredWidth = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.z))) / f2);
        float f3 = this.r;
        canvas.drawText(String.valueOf(this.z), measuredWidth, (this.w / f2) + f3, this.E);
        float measuredWidth2 = this.q + (((((float) getMeasuredWidth()) - this.q) - this.E.measureText(String.valueOf(this.y))) / f2);
        float f4 = this.u;
        canvas.drawText(String.valueOf(this.y), measuredWidth2, (this.w / f2) + f4, this.E);
        Path path = new Path();
        path.moveTo(this.p, f3);
        path.lineTo(this.q, f3);
        canvas.drawPath(path, this.D);
        path.moveTo(this.p, f4);
        path.lineTo(this.q, f4);
        canvas.drawPath(path, this.D);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        wg6.b(canvas, "canvas");
        this.j = 0.0f;
        this.o = ((float) getMeasuredHeight()) - (this.x * ((float) 2));
        this.p = 0.0f;
        this.q = ((float) getMeasuredWidth()) - (this.v * 1.5f);
        this.r = this.h;
        this.s = this.p + gy5.a(5.0f);
        this.u = this.o - this.h;
        this.t = this.q - gy5.a(5.0f);
        int i2 = this.M;
        if (i2 == c.GENERAL.ordinal()) {
            b(canvas);
        } else if (i2 == c.DAY.ordinal()) {
            if (!this.L.isEmpty()) {
                a(canvas);
            }
        } else if (i2 == c.WEEK.ordinal()) {
            if (!this.L.isEmpty()) {
                d(canvas);
            }
        } else if (i2 == c.MONTH.ordinal() && (!this.L.isEmpty())) {
            c(canvas);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, float f2, float f3) {
        Canvas canvas2 = canvas;
        this.J.clear();
        int i2 = 0;
        int i3 = 0;
        for (T next : this.L) {
            int i4 = i3 + 1;
            if (i3 >= 0) {
                hz5 hz5 = (hz5) next;
                short a2 = hz5.a();
                short b2 = hz5.b();
                float f4 = this.r;
                float f5 = this.u;
                short s2 = this.z;
                short s3 = this.y;
                float f6 = (((f5 - f4) / ((float) (s2 - s3))) * ((float) (s2 - b2))) + f4;
                float f7 = this.s + ((f2 + f3) * ((float) i3));
                RectF rectF = new RectF(f7, f6, f7 + f2, (((float) (b2 - a2)) * ((f5 - f4) / ((float) (s2 - s3)))) + f6);
                float f8 = this.i;
                canvas2.drawRoundRect(rectF, f8, f8, this.G);
                if (a2 == -1 && b2 == -1) {
                    rectF.top = 1.0f;
                    rectF.bottom = -1.0f;
                }
                this.J.add(rectF);
                i3 = i4;
            } else {
                qd6.c();
                throw null;
            }
        }
        int i5 = gz5.a[this.K.ordinal()];
        if (i5 != 1 && i5 == 2 && (!this.J.isEmpty())) {
            float f9 = f2 / ((float) 2);
            this.I.setStrokeWidth(f9 / 1.5f);
            for (T next2 : this.J) {
                int i6 = i2 + 1;
                if (i2 >= 0) {
                    RectF rectF2 = (RectF) next2;
                    if (i2 < qd6.a(this.J)) {
                        RectF rectF3 = this.J.get(i6);
                        float f10 = rectF2.top;
                        if (f10 <= rectF2.bottom) {
                            float f11 = rectF3.top;
                            if (f11 <= rectF3.bottom) {
                                canvas.drawLine(rectF2.left + f9, f10 + f9, rectF3.left + f9, f11 + f9, this.I);
                            }
                        }
                    }
                    float f12 = rectF2.top;
                    if (f12 <= rectF2.bottom) {
                        canvas2.drawCircle(rectF2.left + f9, f12 + f9, f9, this.H);
                        canvas2.drawCircle(rectF2.left + f9, rectF2.top + f9, f9, this.I);
                    }
                    i2 = i6;
                } else {
                    qd6.c();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, List<String> list, List<Float> list2, float f2) {
        int i2 = 0;
        for (T next : list) {
            int i3 = i2 + 1;
            if (i2 >= 0) {
                canvas.drawText((String) next, list2.get(i2).floatValue(), f2, this.F);
                i2 = i3;
            } else {
                qd6.c();
                throw null;
            }
        }
    }
}
