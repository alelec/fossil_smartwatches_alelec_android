package com.portfolio.platform.view.fastscrollrecyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.lz5;
import com.fossil.x24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AlphabetFastScrollRecyclerView extends RecyclerView {
    @DexIgnore
    public lz5 a; // = null;
    @DexIgnore
    public GestureDetector b; // = null;
    @DexIgnore
    public boolean c; // = true;
    @DexIgnore
    public int d; // = 12;
    @DexIgnore
    public float e; // = 20.0f;
    @DexIgnore
    public float f; // = 5.0f;
    @DexIgnore
    public int g; // = 5;
    @DexIgnore
    public int h; // = 5;
    @DexIgnore
    public float i; // = 0.6f;
    @DexIgnore
    public int j; // = -16777216;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public int p; // = -16777216;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public a(AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        }

        @DexIgnore
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return super.onFling(motionEvent, motionEvent2, f, f2);
        }
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes;
        if (!(attributeSet == null || (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.AlphabetFastScrollRecyclerView, 0, 0)) == null)) {
            try {
                this.d = obtainStyledAttributes.getInt(8, this.d);
                this.e = obtainStyledAttributes.getFloat(10, this.e);
                this.f = obtainStyledAttributes.getFloat(9, this.f);
                this.g = obtainStyledAttributes.getInt(11, this.g);
                this.h = obtainStyledAttributes.getInt(2, this.h);
                this.i = obtainStyledAttributes.getFloat(7, this.i);
                if (obtainStyledAttributes.hasValue(0)) {
                    this.j = Color.parseColor(obtainStyledAttributes.getString(0));
                }
                if (obtainStyledAttributes.hasValue(5)) {
                    this.o = Color.parseColor(obtainStyledAttributes.getString(5));
                }
                if (obtainStyledAttributes.hasValue(3)) {
                    this.p = Color.parseColor(obtainStyledAttributes.getString(3));
                }
                if (obtainStyledAttributes.hasValue(1)) {
                    this.j = obtainStyledAttributes.getColor(1, this.j);
                }
                if (obtainStyledAttributes.hasValue(6)) {
                    this.o = obtainStyledAttributes.getColor(6, this.o);
                }
                if (obtainStyledAttributes.hasValue(4)) {
                    this.p = obtainStyledAttributes.getColor(3, this.p);
                }
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.a = new lz5(context, this);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        AlphabetFastScrollRecyclerView.super.draw(canvas);
        lz5 lz5 = this.a;
        if (lz5 != null) {
            lz5.a(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r3.a;
     */
    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        lz5 lz5;
        return (this.c && lz5 != null && lz5.a(motionEvent.getX(), motionEvent.getY())) || AlphabetFastScrollRecyclerView.super.onInterceptTouchEvent(motionEvent);
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        AlphabetFastScrollRecyclerView.super.onSizeChanged(i2, i3, i4, i5);
        lz5 lz5 = this.a;
        if (lz5 != null) {
            lz5.a(i2, i3, i4, i5);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView, androidx.recyclerview.widget.RecyclerView, android.view.ViewGroup] */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.c) {
            lz5 lz5 = this.a;
            if (lz5 != null && lz5.a(motionEvent)) {
                return true;
            }
            if (this.b == null) {
                this.b = new GestureDetector(getContext(), new a(this));
            }
            this.b.onTouchEvent(motionEvent);
        }
        return AlphabetFastScrollRecyclerView.super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setAdapter(RecyclerView.g gVar) {
        AlphabetFastScrollRecyclerView.super.setAdapter(gVar);
        lz5 lz5 = this.a;
        if (lz5 != null) {
            lz5.a(gVar);
        }
    }

    @DexIgnore
    public void setIndexBarColor(String str) {
        this.a.a(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarCornerRadius(int i2) {
        this.a.b(i2);
    }

    @DexIgnore
    public void setIndexBarHighLateTextVisibility(boolean z) {
        this.a.a(z);
    }

    @DexIgnore
    public void setIndexBarTextColor(String str) {
        this.a.d(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexBarTransparentValue(float f2) {
        this.a.c(f2);
    }

    @DexIgnore
    public void setIndexBarVisibility(boolean z) {
        this.a.b(z);
        this.c = z;
    }

    @DexIgnore
    public void setIndexTextSize(int i2) {
        this.a.e(i2);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(String str) {
        this.a.c(Color.parseColor(str));
    }

    @DexIgnore
    public void setIndexbarMargin(float f2) {
        this.a.d(f2);
    }

    @DexIgnore
    public void setIndexbarWidth(float f2) {
        this.a.e(f2);
    }

    @DexIgnore
    public void setPreviewPadding(int i2) {
        this.a.f(i2);
    }

    @DexIgnore
    public void setPreviewVisibility(boolean z) {
        this.a.c(z);
    }

    @DexIgnore
    public void setTypeface(Typeface typeface) {
        this.a.a(typeface);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView, android.view.ViewGroup] */
    public void setIndexBarColor(int i2) {
        this.a.a(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setIndexBarTextColor(int i2) {
        this.a.d(i2);
    }

    @DexIgnore
    public void setIndexbarHighLateTextColor(int i2) {
        this.a.c(i2);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    @DexIgnore
    public AlphabetFastScrollRecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }
}
