package com.portfolio.platform.view.cardstackview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import com.fossil.bz5;
import com.fossil.cz5;
import com.fossil.ez5;
import com.fossil.fz5;
import com.fossil.gy5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.cardstackview.CardContainerView;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CardStackView extends FrameLayout {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ bz5 a;
    @DexIgnore
    public /* final */ cz5 b;
    @DexIgnore
    public BaseAdapter c;
    @DexIgnore
    public /* final */ LinkedList<CardContainerView> d;
    @DexIgnore
    public a e;
    @DexIgnore
    public /* final */ d f;
    @DexIgnore
    public /* final */ c g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(float f, float f2);

        @DexIgnore
        void a(int i);

        @DexIgnore
        void a(fz5 fz5);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CardContainerView.c {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        public c(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        public void a(float f, float f2) {
            this.a.a(f, f2);
        }

        @DexIgnore
        public void b() {
            this.a.c();
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(Point point, fz5 fz5) {
            wg6.b(point, "point");
            wg6.b(fz5, "direction");
            this.a.b(point, fz5);
        }

        @DexIgnore
        public void a() {
            if (this.a.getCardEventListener$app_fossilRelease() != null) {
                a cardEventListener$app_fossilRelease = this.a.getCardEventListener$app_fossilRelease();
                if (cardEventListener$app_fossilRelease != null) {
                    cardEventListener$app_fossilRelease.a(this.a.getState$app_fossilRelease().a);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends DataSetObserver {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;

        @DexIgnore
        public d(CardStackView cardStackView) {
            this.a = cardStackView;
        }

        @DexIgnore
        public void onChanged() {
            if (this.a.getAdapter$app_fossilRelease() != null) {
                boolean z = false;
                if (this.a.getState$app_fossilRelease().d) {
                    this.a.getState$app_fossilRelease().d = false;
                } else {
                    int i = this.a.getState$app_fossilRelease().c;
                    BaseAdapter adapter$app_fossilRelease = this.a.getAdapter$app_fossilRelease();
                    if (adapter$app_fossilRelease != null) {
                        if (i == adapter$app_fossilRelease.getCount()) {
                            z = true;
                        }
                        z = !z;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                this.a.a(z);
                cz5 state$app_fossilRelease = this.a.getState$app_fossilRelease();
                BaseAdapter adapter$app_fossilRelease2 = this.a.getAdapter$app_fossilRelease();
                if (adapter$app_fossilRelease2 != null) {
                    state$app_fossilRelease.c = adapter$app_fossilRelease2.getCount();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ CardStackView a;
        @DexIgnore
        public /* final */ /* synthetic */ Point b;
        @DexIgnore
        public /* final */ /* synthetic */ fz5 c;

        @DexIgnore
        public e(CardStackView cardStackView, Point point, fz5 fz5) {
            this.a = cardStackView;
            this.b = point;
            this.c = fz5;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            wg6.b(animator, "animator");
            this.a.a(this.b, this.c);
        }
    }

    /*
    static {
        new b((qg6) null);
        String simpleName = CardStackView.class.getSimpleName();
        wg6.a((Object) simpleName, "CardStackView::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        wg6.b(context, "context");
        this.a = new bz5();
        this.b = new cz5();
        this.d = new LinkedList<>();
        this.f = new d(this);
        this.g = new c(this);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.CardStackView);
        setVisibleCount(obtainStyledAttributes.getInt(11, this.a.a));
        setSwipeThreshold(obtainStyledAttributes.getFloat(8, this.a.b));
        setTranslationDiff(obtainStyledAttributes.getFloat(10, this.a.c));
        setScaleDiff(obtainStyledAttributes.getFloat(4, this.a.d));
        setStackFrom(ez5.values()[obtainStyledAttributes.getInt(5, this.a.e.ordinal())]);
        setElevationEnabled(obtainStyledAttributes.getBoolean(1, this.a.f));
        setSwipeEnabled(obtainStyledAttributes.getBoolean(7, this.a.g));
        List<fz5> from = fz5.from(obtainStyledAttributes.getInt(6, 0));
        wg6.a((Object) from, "SwipeDirection.from(arra\u2026kView_swipeDirection, 0))");
        setSwipeDirection(from);
        setLeftOverlay(obtainStyledAttributes.getResourceId(2, 0));
        setRightOverlay(obtainStyledAttributes.getResourceId(3, 0));
        setBottomOverlay(obtainStyledAttributes.getResourceId(0, 0));
        setTopOverlay(obtainStyledAttributes.getResourceId(9, 0));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final CardContainerView getBottomView() {
        CardContainerView last = this.d.getLast();
        wg6.a((Object) last, "containers.last");
        return last;
    }

    @DexIgnore
    private final CardContainerView getTopView() {
        CardContainerView first = this.d.getFirst();
        wg6.a((Object) first, "containers.first");
        return first;
    }

    @DexIgnore
    private final void setBottomOverlay(int i) {
        this.a.j = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setElevationEnabled(boolean z) {
        this.a.f = z;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setLeftOverlay(int i) {
        this.a.h = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setRightOverlay(int i) {
        this.a.i = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setScaleDiff(float f2) {
        this.a.d = f2;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setStackFrom(ez5 ez5) {
        this.a.e = ez5;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeDirection(List<? extends fz5> list) {
        this.a.l = list;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeEnabled(boolean z) {
        this.a.g = z;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setSwipeThreshold(float f2) {
        this.a.b = f2;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setTopOverlay(int i) {
        this.a.k = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setTranslationDiff(float f2) {
        this.a.c = f2;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    private final void setVisibleCount(int i) {
        this.a.a = i;
        if (this.c != null) {
            a(false);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        b(z);
        e();
        c();
        d();
    }

    @DexIgnore
    public final void b(boolean z) {
        if (z) {
            this.b.a();
        }
    }

    @DexIgnore
    public final void c() {
        a();
        a(0.0f, 0.0f);
    }

    @DexIgnore
    public final void d() {
        if (this.c != null) {
            int i = this.a.a;
            int i2 = 0;
            while (i2 < i) {
                CardContainerView cardContainerView = this.d.get(i2);
                wg6.a((Object) cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                int i3 = this.b.a + i2;
                BaseAdapter baseAdapter = this.c;
                if (baseAdapter != null) {
                    if (i3 < baseAdapter.getCount()) {
                        ViewGroup contentContainer = cardContainerView2.getContentContainer();
                        BaseAdapter baseAdapter2 = this.c;
                        if (baseAdapter2 != null) {
                            View view = baseAdapter2.getView(i3, contentContainer.getChildAt(0), contentContainer);
                            wg6.a((Object) contentContainer, "parent");
                            if (contentContainer.getChildCount() == 0) {
                                contentContainer.addView(view);
                            }
                            cardContainerView2.setVisibility(0);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        cardContainerView2.setVisibility(8);
                    }
                    i2++;
                } else {
                    wg6.a();
                    throw null;
                }
            }
            BaseAdapter baseAdapter3 = this.c;
            if (baseAdapter3 == null) {
                wg6.a();
                throw null;
            } else if (!baseAdapter3.isEmpty()) {
                getTopView().setDraggable(true);
            }
        }
    }

    @DexIgnore
    public final void e() {
        removeAllViews();
        this.d.clear();
        int i = this.a.a;
        int i2 = 0;
        while (i2 < i) {
            View inflate = LayoutInflater.from(getContext()).inflate(2131558445, this, false);
            if (inflate != null) {
                CardContainerView cardContainerView = (CardContainerView) inflate;
                cardContainerView.setDraggable(false);
                cardContainerView.setCardStackOption(this.a);
                bz5 bz5 = this.a;
                cardContainerView.a(bz5.h, bz5.i, bz5.j, bz5.k);
                this.d.add(0, cardContainerView);
                addView(cardContainerView);
                i2++;
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.cardstackview.CardContainerView");
            }
        }
        this.d.getFirst().setContainerEventListener(this.g);
        this.b.e = true;
    }

    @DexIgnore
    public final void f() {
        int i = (this.b.a + this.a.a) - 1;
        BaseAdapter baseAdapter = this.c;
        if (baseAdapter == null) {
            return;
        }
        if (baseAdapter != null) {
            boolean z = false;
            if (i < baseAdapter.getCount()) {
                CardContainerView bottomView = getBottomView();
                bottomView.setDraggable(false);
                ViewGroup contentContainer = bottomView.getContentContainer();
                BaseAdapter baseAdapter2 = this.c;
                if (baseAdapter2 != null) {
                    View view = baseAdapter2.getView(i, contentContainer.getChildAt(0), contentContainer);
                    wg6.a((Object) contentContainer, "parent");
                    if (contentContainer.getChildCount() == 0) {
                        contentContainer.addView(view);
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                CardContainerView bottomView2 = getBottomView();
                bottomView2.setDraggable(false);
                bottomView2.setVisibility(8);
            }
            int i2 = this.b.a;
            BaseAdapter baseAdapter3 = this.c;
            if (baseAdapter3 != null) {
                if (i2 < baseAdapter3.getCount()) {
                    z = true;
                }
                if (z) {
                    getTopView().setDraggable(true);
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void g() {
        a(getTopView());
        LinkedList<CardContainerView> linkedList = this.d;
        linkedList.addLast(linkedList.removeFirst());
    }

    @DexIgnore
    public final BaseAdapter getAdapter$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final a getCardEventListener$app_fossilRelease() {
        return this.e;
    }

    @DexIgnore
    public final cz5 getState$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (this.b.e && i == 0) {
            c();
        }
    }

    @DexIgnore
    public final void setAdapter(BaseAdapter baseAdapter) {
        wg6.b(baseAdapter, "adapter");
        BaseAdapter baseAdapter2 = this.c;
        if (baseAdapter2 != null) {
            try {
                baseAdapter2.unregisterDataSetObserver(this.f);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = h;
                local.d(str, "Exception when unregisterDataSetObserver e=" + e2);
            }
        }
        this.c = baseAdapter;
        BaseAdapter baseAdapter3 = this.c;
        if (baseAdapter3 != null) {
            baseAdapter3.registerDataSetObserver(this.f);
            this.b.c = baseAdapter.getCount();
            a(true);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void setAdapter$app_fossilRelease(BaseAdapter baseAdapter) {
        this.c = baseAdapter;
    }

    @DexIgnore
    public final void setCardEventListener$app_fossilRelease(a aVar) {
        this.e = aVar;
    }

    @DexIgnore
    public final void b() {
        this.d.getFirst().setContainerEventListener((CardContainerView.c) null);
        this.d.getFirst().setDraggable(false);
        if (this.d.size() > 1) {
            this.d.get(1).setContainerEventListener(this.g);
            this.d.get(1).setDraggable(true);
        }
    }

    @DexIgnore
    public final void a() {
        int i = this.a.a;
        for (int i2 = 0; i2 < i; i2++) {
            CardContainerView cardContainerView = this.d.get(i2);
            wg6.a((Object) cardContainerView, "containers[i]");
            CardContainerView cardContainerView2 = cardContainerView;
            cardContainerView2.b();
            cardContainerView2.setTranslationX(0.0f);
            cardContainerView2.setTranslationY(0.0f);
            cardContainerView2.setScaleX(1.0f);
            cardContainerView2.setScaleY(1.0f);
            cardContainerView2.setRotation(0.0f);
        }
    }

    @DexIgnore
    public final void b(Point point, fz5 fz5) {
        wg6.b(point, "point");
        wg6.b(fz5, "direction");
        b();
        a(point, (Animator.AnimatorListener) new e(this, point, fz5));
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        a aVar = this.e;
        if (aVar != null) {
            if (aVar != null) {
                aVar.a(f2, f3);
            } else {
                wg6.a();
                throw null;
            }
        }
        bz5 bz5 = this.a;
        if (bz5.f) {
            int i = bz5.a;
            for (int i2 = 1; i2 < i; i2++) {
                CardContainerView cardContainerView = this.d.get(i2);
                wg6.a((Object) cardContainerView, "containers[i]");
                CardContainerView cardContainerView2 = cardContainerView;
                float f4 = (float) i2;
                float f5 = this.a.d;
                float f6 = 1.0f - (f4 * f5);
                float f7 = (float) (i2 - 1);
                float abs = f6 + (((1.0f - (f5 * f7)) - f6) * Math.abs(f2));
                cardContainerView2.setScaleX(abs);
                cardContainerView2.setScaleY(abs);
                float a2 = f4 * gy5.a(this.a.c);
                if (this.a.e == ez5.Top) {
                    a2 *= -1.0f;
                }
                float a3 = f7 * gy5.a(this.a.c);
                if (this.a.e == ez5.Top) {
                    a3 *= -1.0f;
                }
                cardContainerView2.setTranslationY(a2 - (Math.abs(f2) * (a2 - a3)));
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CardStackView(Context context) {
        this(context, (AttributeSet) null);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Point point, Animator.AnimatorListener animatorListener) {
        getTopView().animate().translationX((float) point.x).translationY(-((float) point.y)).setDuration(400).setListener(animatorListener).start();
    }

    @DexIgnore
    public final void a(CardContainerView cardContainerView) {
        CardStackView cardStackView = (CardStackView) cardContainerView.getParent();
        if (cardStackView != null) {
            cardStackView.removeView(cardContainerView);
            cardStackView.addView(cardContainerView, 0);
        }
    }

    @DexIgnore
    public final void a(Point point, fz5 fz5) {
        wg6.b(point, "point");
        wg6.b(fz5, "direction");
        g();
        this.b.b = point;
        c();
        this.b.a++;
        a aVar = this.e;
        if (aVar != null) {
            if (aVar != null) {
                aVar.a(fz5);
            } else {
                wg6.a();
                throw null;
            }
        }
        f();
        this.d.getLast().setContainerEventListener((CardContainerView.c) null);
        this.d.getFirst().setContainerEventListener(this.g);
    }
}
