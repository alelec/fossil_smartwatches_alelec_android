package com.portfolio.platform.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;
import com.fossil.x24;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilPopupWindow {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public Drawable C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public WeakReference<View> H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public int L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public Context c;
    @DexIgnore
    public WindowManager d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public c g;
    @DexIgnore
    public View h;
    @DexIgnore
    public View i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnAttachStateChangeListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            FossilPopupWindow.this.I = false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ViewTreeObserver.OnScrollChangedListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onScrollChanged() {
            c cVar;
            WeakReference<View> weakReference = FossilPopupWindow.this.H;
            View view = weakReference != null ? (View) weakReference.get() : null;
            if (view != null && (cVar = FossilPopupWindow.this.g) != null) {
                WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) cVar.getLayoutParams();
                FossilPopupWindow fossilPopupWindow = FossilPopupWindow.this;
                fossilPopupWindow.b(fossilPopupWindow.a(view, layoutParams, fossilPopupWindow.J, fossilPopupWindow.K, layoutParams.width, layoutParams.height, FossilPopupWindow.this.L));
                FossilPopupWindow.this.a(layoutParams.x, layoutParams.y, -1, -1, true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends FrameLayout {
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public void a(View view) {
        View view2;
        if (!this.e) {
            this.i = view;
            if (this.c == null && (view2 = this.i) != null) {
                this.c = view2.getContext();
            }
            if (this.d == null && this.i != null) {
                this.d = (WindowManager) this.c.getSystemService("window");
            }
            Context context = this.c;
            if (context != null && !this.v) {
                a(context.getApplicationInfo().targetSdkVersion >= 22);
            }
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        View view;
        if (z2 != this.D) {
            this.D = z2;
            if (this.A != null && (view = this.h) != null) {
                Drawable drawable = this.B;
                if (drawable == null) {
                    view.refreshDrawableState();
                } else if (this.D) {
                    view.setBackground(drawable);
                } else {
                    view.setBackground(this.C);
                }
            }
        }
    }

    @DexIgnore
    public boolean c() {
        Context context;
        if (this.o >= 0 || (context = this.c) == null) {
            if (this.o == 1) {
                return true;
            }
            return false;
        } else if (context.getApplicationInfo().targetSdkVersion >= 11) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final void d() {
        View view;
        c cVar;
        WeakReference<View> weakReference = this.H;
        if (weakReference != null && (view = (View) weakReference.get()) != null && this.N && (cVar = this.g) != null) {
            cVar.setLayoutDirection(view.getLayoutDirection());
        }
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public FossilPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.a = new int[2];
        this.b = new int[2];
        new Rect();
        this.k = 0;
        this.l = true;
        this.n = true;
        this.o = -1;
        this.r = true;
        this.u = true;
        this.E = false;
        this.F = -1;
        this.G = 0;
        new a();
        new b();
        this.c = context;
        this.d = (WindowManager) context.getSystemService("window");
        context.obtainStyledAttributes(attributeSet, x24.PopupWindow, i2, i3).recycle();
    }

    @DexIgnore
    public void a(boolean z2) {
        this.u = z2;
        this.v = true;
    }

    @DexIgnore
    public final int b() {
        int i2 = this.G;
        if (i2 == 0) {
            i2 = 8388659;
        }
        if (this.f) {
            return (this.q || this.n) ? i2 | 268435456 : i2;
        }
        return i2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r3.k == 1) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r3.k == 2) goto L_0x001f;
     */
    @DexIgnore
    @TargetApi(22)
    public final int a(int i2) {
        int i3 = i2 & -8815129;
        if (this.E) {
            i3 |= 32768;
        }
        if (!this.j) {
            i3 |= 8;
        }
        i3 |= 131072;
        if (!this.l) {
            i3 |= 16;
        }
        if (this.m) {
            i3 |= 262144;
        }
        if (!this.n || this.q) {
            i3 |= 512;
        }
        if (c()) {
            i3 |= 8388608;
        }
        if (this.p) {
            i3 |= 256;
        }
        if (this.s) {
            i3 |= 65536;
        }
        if (this.t) {
            i3 |= 32;
        }
        return this.u ? i3 | 1073741824 : i3;
    }

    @DexIgnore
    public final boolean b(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z2) {
        WindowManager.LayoutParams layoutParams2 = layoutParams;
        int i9 = i3;
        int i10 = layoutParams2.y + (i6 - i5);
        int i11 = i8 - i10;
        if (i10 >= 0 && i9 <= i11) {
            return true;
        }
        if (i9 <= (i10 - i4) - i7) {
            layoutParams2.y = (i5 - i9) + (this.M ? i2 + i4 : i2);
            return true;
        }
        return b(layoutParams, i3, i5, i6, i7, i8, z2);
    }

    @DexIgnore
    public final boolean b(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, boolean z2) {
        boolean z3;
        int i7 = i4 - i3;
        layoutParams.y += i7;
        layoutParams.height = i2;
        int i8 = layoutParams.y;
        int i9 = i8 + i2;
        if (i9 > i6) {
            layoutParams.y = i8 - (i9 - i6);
        }
        if (layoutParams.y < i5) {
            layoutParams.y = i5;
            int i10 = i6 - i5;
            if (!z2 || i2 <= i10) {
                z3 = false;
                layoutParams.y -= i7;
                return z3;
            }
            layoutParams.height = i10;
        }
        z3 = true;
        layoutParams.y -= i7;
        return z3;
    }

    @DexIgnore
    public FossilPopupWindow() {
        this((View) null, 0, 0);
    }

    @DexIgnore
    public FossilPopupWindow(View view, int i2, int i3) {
        this(view, i2, i3, false);
    }

    @DexIgnore
    public final int a() {
        return this.F;
    }

    @DexIgnore
    public FossilPopupWindow(View view, int i2, int i3, boolean z2) {
        this.a = new int[2];
        this.b = new int[2];
        new Rect();
        this.k = 0;
        this.l = true;
        this.n = true;
        this.o = -1;
        this.r = true;
        this.u = true;
        this.E = false;
        this.F = -1;
        this.G = 0;
        new a();
        new b();
        if (view != null) {
            this.c = view.getContext();
            this.d = (WindowManager) this.c.getSystemService("window");
        }
        a(view);
        this.j = z2;
    }

    @DexIgnore
    public boolean a(View view, WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6) {
        char c2;
        WindowManager.LayoutParams layoutParams2;
        View view2 = view;
        WindowManager.LayoutParams layoutParams3 = layoutParams;
        int height = view.getHeight();
        int width = view.getWidth();
        int i7 = this.M ? i3 - height : i3;
        int[] iArr = this.a;
        view2.getLocationInWindow(iArr);
        layoutParams3.x = iArr[0] + i2;
        layoutParams3.y = iArr[1] + height + i7;
        Rect rect = new Rect();
        view2.getWindowVisibleDisplayFrame(rect);
        int i8 = i4;
        if (i8 == -1) {
            i8 = rect.right - rect.left;
        }
        int i9 = i8;
        int i10 = i5;
        int i11 = i10 == -1 ? rect.bottom - rect.top : i10;
        layoutParams3.gravity = b();
        layoutParams3.width = i9;
        layoutParams3.height = i11;
        int absoluteGravity = Gravity.getAbsoluteGravity(i6, view.getLayoutDirection()) & 7;
        if (absoluteGravity == 8388613) {
            layoutParams3.x -= i9 - width;
        }
        int[] iArr2 = this.b;
        view2.getLocationOnScreen(iArr2);
        WindowManager.LayoutParams layoutParams4 = layoutParams;
        int[] iArr3 = iArr2;
        int i12 = absoluteGravity;
        int i13 = i11;
        int i14 = iArr[1];
        int i15 = i9;
        int[] iArr4 = iArr;
        boolean b2 = b(layoutParams4, i7, i11, height, i14, iArr2[1], rect.top, rect.bottom, false);
        Rect rect2 = rect;
        Rect rect3 = rect2;
        boolean a2 = a(layoutParams4, i2, i15, width, iArr4[0], iArr3[0], rect2.left, rect2.right, false);
        if (!b2 || !a2) {
            int scrollX = view.getScrollX();
            int scrollY = view.getScrollY();
            Rect rect4 = new Rect(scrollX, scrollY, scrollX + i15 + i2, scrollY + i13 + height + i7);
            if (!this.r || !view2.requestRectangleOnScreen(rect4, true)) {
                layoutParams2 = layoutParams;
                c2 = 1;
            } else {
                view2.getLocationInWindow(iArr4);
                layoutParams2 = layoutParams;
                c2 = 1;
                layoutParams2.x = iArr4[0] + i2;
                layoutParams2.y = iArr4[1] + height + i7;
                if (i12 == 8388613) {
                    layoutParams2.x -= i15 - width;
                }
            }
            Rect rect5 = rect3;
            WindowManager.LayoutParams layoutParams5 = layoutParams;
            int i16 = height;
            Rect rect6 = rect5;
            b(layoutParams5, i7, i13, i16, iArr4[c2], iArr3[c2], rect5.top, rect5.bottom, this.q);
            a(layoutParams5, i2, i15, width, iArr4[0], iArr3[0], rect6.left, rect6.right, this.q);
        } else {
            layoutParams2 = layoutParams;
            c2 = 1;
        }
        if (layoutParams2.y < iArr4[c2]) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x000b, code lost:
        if (r11 > r0) goto L_0x000f;
     */
    @DexIgnore
    public final boolean a(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, int i7, int i8, boolean z2) {
        int i9 = layoutParams.x + (i6 - i5);
        int i10 = i8 - i9;
        if (i9 < 0) {
            int i11 = i3;
        }
        return a(layoutParams, i3, i5, i6, i7, i8, z2);
    }

    @DexIgnore
    public final boolean a(WindowManager.LayoutParams layoutParams, int i2, int i3, int i4, int i5, int i6, boolean z2) {
        boolean z3;
        int i7 = i4 - i3;
        layoutParams.x += i7;
        int i8 = layoutParams.x;
        int i9 = i8 + i2;
        if (i9 > i6) {
            layoutParams.x = i8 - (i9 - i6);
        }
        if (layoutParams.x < i5) {
            layoutParams.x = i5;
            int i10 = i6 - i5;
            if (!z2 || i2 <= i10) {
                z3 = false;
                layoutParams.x -= i7;
                return z3;
            }
            layoutParams.width = i10;
        }
        z3 = true;
        layoutParams.x -= i7;
        return z3;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5, boolean z2) {
        c cVar;
        if (i4 >= 0) {
            this.x = i4;
        }
        if (i5 >= 0) {
            this.z = i5;
        }
        if (this.e && this.i != null && (cVar = this.g) != null) {
            WindowManager.LayoutParams layoutParams = (WindowManager.LayoutParams) cVar.getLayoutParams();
            int i6 = this.w;
            if (i6 >= 0) {
                i6 = this.x;
            }
            if (!(i4 == -1 || layoutParams.width == i6)) {
                this.x = i6;
                layoutParams.width = i6;
                z2 = true;
            }
            int i7 = this.y;
            if (i7 >= 0) {
                i7 = this.z;
            }
            if (!(i5 == -1 || layoutParams.height == i7)) {
                this.z = i7;
                layoutParams.height = i7;
                z2 = true;
            }
            if (layoutParams.x != i2) {
                layoutParams.x = i2;
                z2 = true;
            }
            if (layoutParams.y != i3) {
                layoutParams.y = i3;
                z2 = true;
            }
            int a2 = a();
            if (a2 != layoutParams.windowAnimations) {
                layoutParams.windowAnimations = a2;
                z2 = true;
            }
            int a3 = a(layoutParams.flags);
            if (a3 != layoutParams.flags) {
                layoutParams.flags = a3;
                z2 = true;
            }
            int b2 = b();
            if (b2 != layoutParams.gravity) {
                layoutParams.gravity = b2;
                z2 = true;
            }
            if (z2) {
                d();
                this.d.updateViewLayout(this.g, layoutParams);
            }
        }
    }
}
