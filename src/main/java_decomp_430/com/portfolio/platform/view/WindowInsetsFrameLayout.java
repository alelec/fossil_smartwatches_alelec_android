package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.x9;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WindowInsetsFrameLayout extends FrameLayout {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ViewGroup.OnHierarchyChangeListener {
        @DexIgnore
        public void onChildViewAdded(View view, View view2) {
            wg6.b(view, "parent");
            wg6.b(view2, "child");
            x9.J(view);
        }

        @DexIgnore
        public void onChildViewRemoved(View view, View view2) {
            wg6.b(view, "parent");
            wg6.b(view2, "child");
        }
    }

    @DexIgnore
    public WindowInsetsFrameLayout(Context context) {
        this(context, (AttributeSet) null, 0, 6, (qg6) null);
    }

    @DexIgnore
    public WindowInsetsFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (qg6) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WindowInsetsFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        setOnHierarchyChangeListener(new a());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WindowInsetsFrameLayout(Context context, AttributeSet attributeSet, int i, int i2, qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }
}
