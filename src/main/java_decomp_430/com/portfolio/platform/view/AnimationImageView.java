package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.lk4;
import com.fossil.nh6;
import com.fossil.wg6;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AnimationImageView extends AppCompatImageView {
    @DexIgnore
    public /* final */ ArrayList<String> c; // = new ArrayList<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public Bitmap j;
    @DexIgnore
    public Bitmap o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public Runnable r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ AnimationImageView a;

        @DexIgnore
        public a(AnimationImageView animationImageView) {
            this.a = animationImageView;
        }

        @DexIgnore
        public final void run() {
            AnimationImageView animationImageView = this.a;
            animationImageView.setImageBitmap(animationImageView.o);
            AnimationImageView animationImageView2 = this.a;
            animationImageView2.a(animationImageView2.j);
            AnimationImageView animationImageView3 = this.a;
            animationImageView3.j = animationImageView3.o;
            if (!(!this.a.c.isEmpty())) {
                return;
            }
            if (this.a.i == this.a.f) {
                AnimationImageView animationImageView4 = this.a;
                Object obj = animationImageView4.c.get(0);
                wg6.a(obj, "mListImagesPathFromAssets[0]");
                animationImageView4.o = animationImageView4.a((String) obj);
                this.a.a(0);
                return;
            }
            AnimationImageView animationImageView5 = this.a;
            Object obj2 = animationImageView5.c.get(this.a.i + 1);
            wg6.a(obj2, "mListImagesPathFromAssets[mFrameNo + 1]");
            animationImageView5.o = animationImageView5.a((String) obj2);
            AnimationImageView animationImageView6 = this.a;
            animationImageView6.a(animationImageView6.i + 1);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.AnimationImageView, android.widget.ImageView] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context) {
        super(context);
        wg6.b(context, "context");
        Context context2 = getContext();
        wg6.a((Object) context2, "context");
        Resources resources = context2.getResources();
        wg6.a((Object) resources, "context.resources");
        this.p = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        wg6.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        wg6.a((Object) resources2, "context.resources");
        this.q = resources2.getDisplayMetrics().widthPixels;
        this.r = new a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.AnimationImageView, android.widget.ImageView] */
    public final void d() {
        removeCallbacks(this.r);
        this.e = false;
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, int i4, int i5, int i6) {
        wg6.b(str, "imagesPathFromAssets");
        this.c.clear();
        if (i2 <= i3) {
            while (true) {
                ArrayList<String> arrayList = this.c;
                nh6 nh6 = nh6.a;
                Object[] objArr = {Integer.valueOf(i2)};
                String format = String.format(str, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                arrayList.add(format);
                if (i2 == i3) {
                    break;
                }
                i2++;
            }
        }
        if (this.c.size() > 1) {
            this.d = i4;
            this.g = i5;
            this.h = i6;
            this.f = this.c.size() - 1;
            String str2 = this.c.get(0);
            wg6.a((Object) str2, "mListImagesPathFromAssets[0]");
            this.j = a(str2);
            String str3 = this.c.get(1);
            wg6.a((Object) str3, "mListImagesPathFromAssets[1]");
            this.o = a(str3);
            setImageBitmap(this.j);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.AnimationImageView, android.widget.ImageView] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        Context context2 = getContext();
        wg6.a((Object) context2, "context");
        Resources resources = context2.getResources();
        wg6.a((Object) resources, "context.resources");
        this.p = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        wg6.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        wg6.a((Object) resources2, "context.resources");
        this.q = resources2.getDisplayMetrics().widthPixels;
        this.r = new a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.AnimationImageView, android.widget.ImageView] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        Context context2 = getContext();
        wg6.a((Object) context2, "context");
        Resources resources = context2.getResources();
        wg6.a((Object) resources, "context.resources");
        this.p = resources.getDisplayMetrics().heightPixels;
        Context context3 = getContext();
        wg6.a((Object) context3, "context");
        Resources resources2 = context3.getResources();
        wg6.a((Object) resources2, "context.resources");
        this.q = resources2.getDisplayMetrics().widthPixels;
        this.r = new a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.AnimationImageView, android.widget.ImageView] */
    public final void a() {
        if (this.e) {
            this.i = 0;
            return;
        }
        this.e = true;
        removeCallbacks(this.r);
        a(1);
    }

    @DexIgnore
    public final void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                bitmap.recycle();
            } catch (Exception unused) {
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.AnimationImageView, android.widget.ImageView] */
    public final void a(int i2) {
        int i3;
        if (this.e) {
            int i4 = this.d;
            if ((i2 != this.f || (i3 = this.h) <= 0) && (i2 != 1 || (i3 = this.g) <= 0)) {
                i3 = i4;
            }
            this.i = i2;
            try {
                postDelayed(this.r, (long) i3);
            } catch (Exception unused) {
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.graphics.Bitmap} */
    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        if (r5 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001c, code lost:
        if (r5 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        r5.close();
        r0 = r0;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
    public final Bitmap a(String str) {
        Throwable th;
        InputStream inputStream;
        Object r0 = 0;
        try {
            Context context = getContext();
            wg6.a((Object) context, "context");
            inputStream = context.getAssets().open(str);
            try {
                Bitmap a2 = lk4.a(inputStream, this.q, this.p / 2);
                r0 = a2;
                r0 = a2;
            } catch (IOException e2) {
                e = e2;
                try {
                    e.printStackTrace();
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    r0 = inputStream;
                    th = th3;
                    if (r0 != 0) {
                    }
                    throw th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            inputStream = null;
            e.printStackTrace();
        } catch (Throwable th4) {
            th = th4;
            if (r0 != 0) {
                r0.close();
            }
            throw th;
        }
    }
}
