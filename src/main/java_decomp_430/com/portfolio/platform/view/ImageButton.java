package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.fossil.gy5;
import com.fossil.jm4;
import com.fossil.w6;
import com.fossil.x24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ImageButton extends RelativeLayout {
    @DexIgnore
    public /* final */ ImageView a; // = ((ImageView) findViewById(2131362264));
    @DexIgnore
    public /* final */ TextView b; // = ((TextView) findViewById(2131362265));
    @DexIgnore
    public /* final */ ViewGroup c; // = ((ViewGroup) findViewById(2131362263));

    @DexIgnore
    public ImageButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        RelativeLayout.inflate(context, 2131558803, this);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.ImageButton);
        setIcon(obtainStyledAttributes.getResourceId(1, -1));
        setTitle(obtainStyledAttributes.getResourceId(2, -1));
        setTitleColor(obtainStyledAttributes.getResourceId(3, -1));
        setBackground(obtainStyledAttributes.getResourceId(0, -1));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private void setBackground(int i) {
        Drawable c2 = w6.c(getContext(), 2131230873);
        if (i != -1) {
            c2 = w6.c(getContext(), i);
        }
        this.c.setBackground(c2);
    }

    @DexIgnore
    private void setIcon(int i) {
        if (i != -1) {
            this.a.setImageDrawable(w6.c(getContext(), i));
            this.a.setVisibility(0);
            return;
        }
        this.a.setVisibility(8);
    }

    @DexIgnore
    private void setTitle(int i) {
        if (i != -1) {
            jm4.a(this.b, i);
            this.b.setVisibility(0);
            return;
        }
        this.b.setVisibility(8);
    }

    @DexIgnore
    private void setTitleColor(int i) {
        if (i != -1) {
            this.b.setTextColor(getResources().getColorStateList(i));
        }
    }

    @DexIgnore
    public void a(int i, int i2) {
        ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.a.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.a.getLayoutParams();
        layoutParams2.setMarginStart((int) (gy5.a(70.0f) - (((float) i) - gy5.a(30.0f))));
        this.a.setLayoutParams(layoutParams2);
    }

    @DexIgnore
    public TextView getTextView() {
        return this.b;
    }

    @DexIgnore
    public String getTitle() {
        return this.b.getText().toString();
    }

    @DexIgnore
    public void setTitle(String str) {
        this.b.setText(str);
        this.b.setVisibility(0);
    }
}
