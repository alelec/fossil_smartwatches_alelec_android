package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bk4;
import com.fossil.jm4;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rz5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewCalendar extends ConstraintLayout implements View.OnClickListener {
    @DexIgnore
    public static /* final */ String K; // = K;
    @DexIgnore
    public static /* final */ int L; // = 7;
    @DexIgnore
    public TextView A;
    @DexIgnore
    public ConstraintLayout B;
    @DexIgnore
    public String C;
    @DexIgnore
    public String D;
    @DexIgnore
    public String E;
    @DexIgnore
    public String F;
    @DexIgnore
    public String G;
    @DexIgnore
    public String H;
    @DexIgnore
    public int I;
    @DexIgnore
    public Calendar J;
    @DexIgnore
    public GridLayoutManager u;
    @DexIgnore
    public rz5 v;
    @DexIgnore
    public c w;
    @DexIgnore
    public d x;
    @DexIgnore
    public View y;
    @DexIgnore
    public View z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, Calendar calendar);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(Calendar calendar);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void next();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends GridLayoutManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewCalendar e;

        @DexIgnore
        public e(RecyclerViewCalendar recyclerViewCalendar) {
            this.e = recyclerViewCalendar;
        }

        @DexIgnore
        public int a(int i) {
            int itemViewType = this.e.getMAdapter$app_fossilRelease().getItemViewType(i);
            return (itemViewType == 0 || itemViewType == 1) ? 1 : -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerViewCalendar a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView b;

        @DexIgnore
        public f(RecyclerViewCalendar recyclerViewCalendar, RecyclerView recyclerView) {
            this.a = recyclerViewCalendar;
            this.b = recyclerView;
        }

        @DexIgnore
        public void onGlobalLayout() {
            rz5 mAdapter$app_fossilRelease = this.a.getMAdapter$app_fossilRelease();
            RecyclerView recyclerView = this.b;
            wg6.a((Object) recyclerView, "recyclerView");
            mAdapter$app_fossilRelease.b(recyclerView.getMeasuredWidth() / 7);
            RecyclerView recyclerView2 = this.b;
            wg6.a((Object) recyclerView2, "recyclerView");
            recyclerView2.setAdapter(this.a.getMAdapter$app_fossilRelease());
            RecyclerView recyclerView3 = this.b;
            wg6.a((Object) recyclerView3, "recyclerView");
            recyclerView3.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public RecyclerViewCalendar(Context context) {
        this(context, (AttributeSet) null, 0, 6, (qg6) null);
    }

    @DexIgnore
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, (qg6) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        String str = "";
        this.C = str;
        this.D = str;
        this.E = str;
        this.F = str;
        this.G = str;
        this.H = str;
        this.J = Calendar.getInstance();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.RecyclerViewCalendar);
            try {
                String string = obtainStyledAttributes.getString(1);
                if (string == null) {
                    string = "onDianaStepsTab";
                }
                this.C = string;
                String string2 = obtainStyledAttributes.getString(2);
                if (string2 == null) {
                    string2 = "secondaryText";
                }
                this.D = string2;
                String string3 = obtainStyledAttributes.getString(3);
                if (string3 == null) {
                    string3 = "nonBrandDisableCalendarDay";
                }
                this.E = string3;
                String string4 = obtainStyledAttributes.getString(5);
                if (string4 == null) {
                    string4 = "primaryText";
                }
                this.F = string4;
                String string5 = obtainStyledAttributes.getString(4);
                if (string5 == null) {
                    string5 = "dianaStepsTab";
                }
                this.G = string5;
                String string6 = obtainStyledAttributes.getString(0);
                this.H = string6 != null ? string6 : str;
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = K;
                local.e(str2, "RecyclerViewCalendar - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a(context);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r10v0, types: [android.view.View$OnClickListener, com.portfolio.platform.view.recyclerview.RecyclerViewCalendar, android.view.ViewGroup] */
    public final void a(Context context) {
        View inflate = View.inflate(context, 2131558800, this);
        this.B = inflate.findViewById(2131362088);
        this.A = (TextView) inflate.findViewById(2131362706);
        this.y = inflate.findViewById(2131362737);
        this.z = inflate.findViewById(2131362817);
        RecyclerView findViewById = inflate.findViewById(2131362157);
        this.v = new rz5(context);
        String b2 = ThemeManager.l.a().b(this.H);
        int parseColor = Color.parseColor(ThemeManager.l.a().b(this.C));
        int parseColor2 = Color.parseColor(ThemeManager.l.a().b(this.D));
        int parseColor3 = Color.parseColor(ThemeManager.l.a().b(this.E));
        int parseColor4 = Color.parseColor(ThemeManager.l.a().b(this.F));
        int parseColor5 = Color.parseColor(ThemeManager.l.a().b(this.G));
        rz5 rz5 = this.v;
        if (rz5 != null) {
            rz5.a(parseColor, parseColor2, parseColor3, parseColor4, parseColor5);
            if (!TextUtils.isEmpty(b2)) {
                ConstraintLayout constraintLayout = this.B;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                ConstraintLayout constraintLayout2 = this.B;
                if (constraintLayout2 != null) {
                    constraintLayout2.setBackgroundColor(w6.a(context, 2131099970));
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.u = new RecyclerViewCalendar$init$Anon1(context, context, L, 0, true);
            GridLayoutManager gridLayoutManager = this.u;
            if (gridLayoutManager != null) {
                gridLayoutManager.a(new e(this));
                wg6.a((Object) findViewById, "recyclerView");
                findViewById.setLayoutManager(this.u);
                findViewById.setItemAnimator((RecyclerView.j) null);
                findViewById.setNestedScrollingEnabled(false);
                findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new f(this, findViewById));
                View view = this.y;
                if (view != null) {
                    view.setOnClickListener(this);
                    View view2 = this.z;
                    if (view2 != null) {
                        view2.setOnClickListener(this);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "reachGoal");
        int parseColor = Color.parseColor(ThemeManager.l.a().b(this.C));
        int parseColor2 = Color.parseColor(ThemeManager.l.a().b(this.D));
        int parseColor3 = Color.parseColor(ThemeManager.l.a().b(this.E));
        int parseColor4 = Color.parseColor(ThemeManager.l.a().b(this.F));
        String b2 = ThemeManager.l.a().b(str);
        if (b2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = K;
            local.d(str2, "invalidateStyle reachGoal=" + str + " color=" + b2);
            int parseColor5 = Color.parseColor(b2);
            rz5 rz5 = this.v;
            if (rz5 != null) {
                rz5.a(parseColor, parseColor2, parseColor3, parseColor4, parseColor5);
                rz5 rz52 = this.v;
                if (rz52 != null) {
                    rz52.notifyDataSetChanged();
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            } else {
                wg6.d("mAdapter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void c(int i) {
        d dVar = this.x;
        if (dVar == null) {
            return;
        }
        if (i != 2131362737) {
            if (i == 2131362817) {
                if (dVar != null) {
                    dVar.a();
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else if (dVar != null) {
            dVar.next();
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void d() {
        rz5 rz5 = this.v;
        if (rz5 != null) {
            Calendar c2 = rz5.c();
            rz5 rz52 = this.v;
            if (rz52 != null) {
                Calendar d2 = rz52.d();
                c cVar = this.w;
                if (cVar != null) {
                    if (cVar != null) {
                        Calendar calendar = this.J;
                        wg6.a((Object) calendar, "chosenCalendar");
                        cVar.a(calendar);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                a(d2, c2);
                return;
            }
            wg6.d("mAdapter");
            throw null;
        }
        wg6.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public final rz5 getMAdapter$app_fossilRelease() {
        rz5 rz5 = this.v;
        if (rz5 != null) {
            return rz5;
        }
        wg6.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        wg6.b(view, "view");
        setEnableButtonNextAndPrevMonth(false);
        int id = view.getId();
        if (id == 2131362737) {
            this.I++;
        } else if (id == 2131362817) {
            this.I--;
        }
        int i = this.I;
        rz5 rz5 = this.v;
        if (rz5 != null) {
            this.J = bk4.a(i, rz5.c());
            d();
            c(view.getId());
            return;
        }
        wg6.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public final void setData(Map<Long, Float> map) {
        wg6.b(map, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = K;
        local.d(str, "setData dataSize=" + map.size());
        rz5 rz5 = this.v;
        if (rz5 != null) {
            rz5.a(map, this.J);
            rz5 rz52 = this.v;
            if (rz52 != null) {
                rz52.notifyDataSetChanged();
            } else {
                wg6.d("mAdapter");
                throw null;
            }
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setEnableButtonNextAndPrevMonth(Boolean bool) {
        View view = this.y;
        if (view == null) {
            wg6.a();
            throw null;
        } else if (bool != null) {
            view.setEnabled(bool.booleanValue());
            View view2 = this.z;
            if (view2 != null) {
                view2.setEnabled(bool.booleanValue());
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void setEndDate(Calendar calendar) {
        wg6.b(calendar, "endDate");
        this.J = bk4.a(this.I, calendar);
        rz5 rz5 = this.v;
        if (rz5 != null) {
            rz5.a(calendar);
            rz5 rz52 = this.v;
            if (rz52 != null) {
                rz52.notifyDataSetChanged();
                rz5 rz53 = this.v;
                if (rz53 != null) {
                    a(rz53.d(), calendar);
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            } else {
                wg6.d("mAdapter");
                throw null;
            }
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setMAdapter$app_fossilRelease(rz5 rz5) {
        wg6.b(rz5, "<set-?>");
        this.v = rz5;
    }

    @DexIgnore
    public final void setNavigatingListener(d dVar) {
        wg6.b(dVar, "listener");
        this.x = dVar;
    }

    @DexIgnore
    public final void setOnCalendarItemClickListener(b bVar) {
        wg6.b(bVar, "listener");
        rz5 rz5 = this.v;
        if (rz5 != null) {
            rz5.a(bVar);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setOnCalendarMonthChanged(c cVar) {
        this.w = cVar;
    }

    @DexIgnore
    public final void setStartDate(Calendar calendar) {
        wg6.b(calendar, "startCalendar");
        rz5 rz5 = this.v;
        if (rz5 != null) {
            rz5.c(calendar);
            rz5 rz52 = this.v;
            if (rz52 != null) {
                rz52.notifyDataSetChanged();
                rz5 rz53 = this.v;
                if (rz53 != null) {
                    a(calendar, rz53.c());
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            } else {
                wg6.d("mAdapter");
                throw null;
            }
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void setTintColor(int i) {
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ RecyclerViewCalendar(Context context, AttributeSet attributeSet, int i, int i2, qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2, Calendar calendar3) {
        wg6.b(calendar, "currentCalendar");
        wg6.b(calendar2, "startCalendar");
        wg6.b(calendar3, "endCalendar");
        rz5 rz5 = this.v;
        if (rz5 != null) {
            rz5.c(calendar2);
            rz5 rz52 = this.v;
            if (rz52 != null) {
                rz52.a(calendar3);
                rz5 rz53 = this.v;
                if (rz53 != null) {
                    rz53.b(calendar);
                    a(calendar2, calendar3);
                    rz5 rz54 = this.v;
                    if (rz54 != null) {
                        rz54.notifyDataSetChanged();
                    } else {
                        wg6.d("mAdapter");
                        throw null;
                    }
                } else {
                    wg6.d("mAdapter");
                    throw null;
                }
            } else {
                wg6.d("mAdapter");
                throw null;
            }
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Calendar calendar, Calendar calendar2) {
        if (calendar != null && calendar2 != null) {
            int i = this.J.get(2);
            int i2 = this.J.get(1);
            View view = this.z;
            if (view != null) {
                int i3 = 8;
                view.setVisibility((i == calendar.get(2) && i2 == calendar.get(1)) ? 8 : 0);
                View view2 = this.y;
                if (view2 != null) {
                    if (!(i == calendar2.get(2) && i2 == calendar2.get(1))) {
                        i3 = 0;
                    }
                    view2.setVisibility(i3);
                    TextView textView = this.A;
                    if (textView != null) {
                        nh6 nh6 = nh6.a;
                        Calendar calendar3 = this.J;
                        wg6.a((Object) calendar3, "chosenCalendar");
                        Object[] objArr = {a(calendar3), Integer.valueOf(i2)};
                        String format = String.format("%s %s", Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                        int length = format.length() - 1;
                        int i4 = 0;
                        boolean z2 = false;
                        while (i4 <= length) {
                            boolean z3 = format.charAt(!z2 ? i4 : length) <= ' ';
                            if (!z2) {
                                if (!z3) {
                                    z2 = true;
                                } else {
                                    i4++;
                                }
                            } else if (!z3) {
                                break;
                            } else {
                                length--;
                            }
                        }
                        textView.setText(format.subSequence(i4, length + 1).toString());
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v21, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v33, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v36, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v39, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(Calendar calendar) {
        switch (calendar.get(2)) {
            case 0:
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886621);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a2;
            case 1:
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886620);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026hs_Month_Title__February)");
                return a3;
            case 2:
                String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886624);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026onths_Month_Title__March)");
                return a4;
            case 3:
                String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886617);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026onths_Month_Title__April)");
                return a5;
            case 4:
                String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886625);
                wg6.a((Object) a6, "LanguageHelper.getString\u2026_Months_Month_Title__May)");
                return a6;
            case 5:
                String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886623);
                wg6.a((Object) a7, "LanguageHelper.getString\u2026Months_Month_Title__June)");
                return a7;
            case 6:
                String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886622);
                wg6.a((Object) a8, "LanguageHelper.getString\u2026Months_Month_Title__July)");
                return a8;
            case 7:
                String a9 = jm4.a((Context) PortfolioApp.get.instance(), 2131886618);
                wg6.a((Object) a9, "LanguageHelper.getString\u2026nths_Month_Title__August)");
                return a9;
            case 8:
                String a10 = jm4.a((Context) PortfolioApp.get.instance(), 2131886628);
                wg6.a((Object) a10, "LanguageHelper.getString\u2026s_Month_Title__September)");
                return a10;
            case 9:
                String a11 = jm4.a((Context) PortfolioApp.get.instance(), 2131886627);
                wg6.a((Object) a11, "LanguageHelper.getString\u2026ths_Month_Title__October)");
                return a11;
            case 10:
                String a12 = jm4.a((Context) PortfolioApp.get.instance(), 2131886626);
                wg6.a((Object) a12, "LanguageHelper.getString\u2026hs_Month_Title__November)");
                return a12;
            case 11:
                String a13 = jm4.a((Context) PortfolioApp.get.instance(), 2131886619);
                wg6.a((Object) a13, "LanguageHelper.getString\u2026hs_Month_Title__December)");
                return a13;
            default:
                String a14 = jm4.a((Context) PortfolioApp.get.instance(), 2131886621);
                wg6.a((Object) a14, "LanguageHelper.getString\u2026ths_Month_Title__January)");
                return a14;
        }
    }
}
