package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WaveView extends View {
    @DexIgnore
    public Paint a;
    @DexIgnore
    public int b;
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String o;
    @DexIgnore
    public long p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public WaveView(Context context) {
        this(context, (AttributeSet) null, 2, (qg6) null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.a = new Paint(1);
        this.b = 3;
        this.c = 374.0f;
        this.d = 1670;
        this.e = 330;
        this.f = 244.0f;
        this.g = 618.0f;
        this.h = 318.0f;
        this.i = 6.0f;
        this.j = 50;
        this.o = "nonBrandSurface";
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        Context context;
        TypedArray obtainStyledAttributes;
        this.a.setStyle(Paint.Style.STROKE);
        if (attributeSet != null && (context = getContext()) != null && (obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.WaveView)) != null) {
            try {
                String string = obtainStyledAttributes.getString(0);
                if (string == null) {
                    string = "disabled";
                }
                this.o = string;
                a();
                this.b = obtainStyledAttributes.getInteger(1, 3);
                this.d = obtainStyledAttributes.getInteger(2, 1670);
                this.e = obtainStyledAttributes.getInteger(3, 330);
                obtainStyledAttributes.getInteger(4, 417);
                this.f = (float) obtainStyledAttributes.getDimensionPixelSize(6, 244);
                this.g = (float) obtainStyledAttributes.getDimensionPixelSize(5, 618);
                this.c = this.g - this.f;
                this.h = this.f + ((this.c * ((float) this.e)) / ((float) this.d));
                this.i = (float) obtainStyledAttributes.getDimensionPixelSize(8, 6);
                this.j = obtainStyledAttributes.getDimensionPixelSize(7, 50);
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("WaveView", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public void dispatchDraw(Canvas canvas) {
        long j2;
        float f2;
        float f3;
        int i2;
        wg6.b(canvas, "canvas");
        super.dispatchDraw(canvas);
        long currentTimeMillis = System.currentTimeMillis();
        int width = getWidth() / 2;
        int height = getHeight() / 2;
        long j3 = this.p;
        if (j3 == 0) {
            this.p = currentTimeMillis;
            j2 = 0;
        } else {
            j2 = currentTimeMillis - j3;
        }
        if (j2 == 0) {
            f2 = this.f;
        } else {
            f2 = ((((float) j2) * this.c) / ((float) this.d)) + this.f;
        }
        if (f2 - ((float) ((this.b - 1) * this.j)) > this.g) {
            f2 = this.f;
            this.p = currentTimeMillis;
        }
        int i3 = 0;
        while (i3 < this.b) {
            float f4 = this.f;
            if (f2 < f4) {
                break;
            }
            float f5 = this.h;
            if (f2 < f5) {
                float f6 = (f2 - f4) / (f5 - f4);
                f3 = f6 * this.i;
                i2 = (int) (((float) 255) * f6);
            } else {
                float f7 = (f2 - f5) / (this.g - f5);
                i2 = 255 - ((int) (((float) 255) * f7));
                if (i2 < 0) {
                    i2 = 0;
                }
                float f8 = this.i;
                f3 = f8 - (f7 * f8);
                if (f3 < ((float) 0)) {
                    f3 = 0.0f;
                }
            }
            this.a.setAlpha(i2);
            this.a.setStrokeWidth(f3);
            canvas.drawCircle((float) width, (float) height, f2, this.a);
            i3++;
            f2 -= (float) this.j;
        }
        postInvalidateDelayed(0);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WaveView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WaveView(Context context, AttributeSet attributeSet, int i2, qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet);
    }

    @DexIgnore
    public final void a() {
        String b2 = ThemeManager.l.a().b(this.o);
        if (b2 != null) {
            this.a.setColor(Color.parseColor(b2));
        }
    }
}
