package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import com.fossil.xk4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class RTLEditText extends FlexibleEditText {
    @DexIgnore
    public RTLEditText(Context context) {
        super(context);
        a(context);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.view.View, com.portfolio.platform.view.RTLEditText] */
    public final void a(Context context) {
        xk4.a(this, context);
    }

    @DexIgnore
    public RTLEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    @DexIgnore
    public RTLEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }
}
