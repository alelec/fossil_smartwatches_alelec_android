package com.portfolio.platform;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.fossil.kb;
import com.fossil.l54;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.u6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"Registered"})
public class BaseWebViewActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ a E; // = new a((qg6) null);
    @DexIgnore
    public l54 B;
    @DexIgnore
    public String C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return BaseWebViewActivity.D;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            wg6.b(context, "context");
            wg6.b(str, Explore.COLUMN_TITLE);
            wg6.b(str2, "url");
            Intent intent = new Intent(context, BaseWebViewActivity.class);
            intent.putExtra("urlToLoad", str2);
            intent.putExtra(Explore.COLUMN_TITLE, str);
            context.startActivity(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends WebViewClient {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        public b(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.BaseWebViewActivity, android.app.Activity] */
        @TargetApi(21)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BaseWebViewActivity.E.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Should override ");
            Uri uri = null;
            sb.append(webResourceRequest != null ? webResourceRequest.getUrl() : null);
            local.d(a2, sb.toString());
            if (!xj6.c(String.valueOf(webResourceRequest != null ? webResourceRequest.getUrl() : null), "mailto", false, 2, (Object) null)) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            FLogger.INSTANCE.getLocal().d(BaseWebViewActivity.E.a(), "We are overriding the mailto urlToLoad");
            if (webResourceRequest != null) {
                uri = webResourceRequest.getUrl();
            }
            String valueOf = String.valueOf(uri);
            if (valueOf != null) {
                String substring = valueOf.substring(7);
                wg6.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                u6 a3 = u6.a(this.a);
                a3.b("message/rfc822");
                a3.a(substring);
                a3.c();
                return true;
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends WebChromeClient {
        @DexIgnore
        public /* final */ /* synthetic */ BaseWebViewActivity a;

        @DexIgnore
        public c(BaseWebViewActivity baseWebViewActivity) {
            this.a = baseWebViewActivity;
        }

        @DexIgnore
        public void onProgressChanged(WebView webView, int i) {
            wg6.b(webView, "view");
            if (i == 100) {
                this.a.h();
            }
        }
    }

    /*
    static {
        String simpleName = BaseWebViewActivity.class.getSimpleName();
        wg6.a((Object) simpleName, "BaseWebViewActivity::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    public void a(l54 l54) {
        wg6.b(l54, "<set-?>");
        this.B = l54;
    }

    @DexIgnore
    public void c(String str) {
        wg6.b(str, "<set-?>");
        this.C = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        if (getIntent().getStringExtra("urlToLoad") == null) {
            str = "";
        } else {
            str = getIntent().getStringExtra("urlToLoad");
            wg6.a((Object) str, "intent.getStringExtra(KEY_URL)");
        }
        c(str);
        l54 a2 = kb.a(this, 2131558438);
        wg6.a((Object) a2, "DataBindingUtil.setConte\u2026.layout.activity_webview)");
        a(a2);
        Object r3 = u().q;
        wg6.a((Object) r3, "binding.ftvTitle");
        r3.setText(getIntent().getStringExtra(Explore.COLUMN_TITLE));
        BaseActivity.a(this, true, (String) null, 2, (Object) null);
        t();
        w();
    }

    @DexIgnore
    public WebViewClient s() {
        FLogger.INSTANCE.getLocal().d(D, "Building default web client");
        return new b(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.BaseWebViewActivity, android.app.Activity] */
    public final void t() {
        if (Build.VERSION.SDK_INT >= 22) {
            CookieManager.getInstance().removeAllCookies((ValueCallback) null);
            CookieManager.getInstance().flush();
            return;
        }
        CookieSyncManager createInstance = CookieSyncManager.createInstance(getBaseContext());
        createInstance.startSync();
        CookieManager instance = CookieManager.getInstance();
        instance.removeAllCookie();
        instance.removeSessionCookie();
        createInstance.stopSync();
        createInstance.sync();
    }

    @DexIgnore
    public l54 u() {
        l54 l54 = this.B;
        if (l54 != null) {
            return l54;
        }
        wg6.d("binding");
        throw null;
    }

    @DexIgnore
    public String v() {
        String str = this.C;
        if (str != null) {
            return str;
        }
        wg6.d("urlToLoad");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"SetJavaScriptEnabled"})
    public void w() {
        WebView webView = u().r;
        wg6.a((Object) webView, "binding.webView");
        WebSettings settings = webView.getSettings();
        wg6.a((Object) settings, "ws");
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        u().r.clearCache(true);
        WebView webView2 = u().r;
        wg6.a((Object) webView2, "binding.webView");
        webView2.setWebViewClient(s());
        WebView webView3 = u().r;
        wg6.a((Object) webView3, "binding.webView");
        WebSettings settings2 = webView3.getSettings();
        wg6.a((Object) settings2, Constants.USER_SETTING);
        settings2.setJavaScriptEnabled(true);
        settings2.setDomStorageEnabled(true);
        settings2.setAllowFileAccess(true);
        settings2.setAllowFileAccessFromFileURLs(true);
        settings2.setAllowUniversalAccessFromFileURLs(true);
        settings2.setUserAgentString("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");
        WebView webView4 = u().r;
        wg6.a((Object) webView4, "binding.webView");
        webView4.setWebChromeClient(new c(this));
        if (PortfolioApp.get.e() && TextUtils.isEmpty(v())) {
            FLogger.INSTANCE.getLocal().e(D, "You must create a urlToLoad to load before the webview is created");
        }
        u().r.loadUrl(v());
    }
}
