package com.portfolio.platform.data;

import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.helper.DeviceHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SpecialSkuSetting {

    @DexIgnore
    public enum AngleSubeye {
        NONE(0, 0, 0),
        MOVEMBER(113, 158, Action.Selfie.TAKE_BURST);
        
        @DexIgnore
        public int angleForApp;
        @DexIgnore
        public int angleForCall;
        @DexIgnore
        public int angleForSms;

        @DexIgnore
        public AngleSubeye(int i, int i2, int i3) {
            this.angleForApp = i;
            this.angleForCall = i2;
            this.angleForSms = i3;
        }

        @DexIgnore
        public int getAngleForApp() {
            return this.angleForApp;
        }

        @DexIgnore
        public int getAngleForCall() {
            return this.angleForCall;
        }

        @DexIgnore
        public int getAngleForSms() {
            return this.angleForSms;
        }
    }

    @DexIgnore
    public enum SpecialSku {
        NONE("NONE", AngleSubeye.NONE, ""),
        MOVEMBER("FTW1175", AngleSubeye.MOVEMBER, "W0FA01");
        
        @DexIgnore
        public AngleSubeye angleSubeye;
        @DexIgnore
        public String prefixSerialNumber;
        @DexIgnore
        public String sku;

        @DexIgnore
        public SpecialSku(String str, AngleSubeye angleSubeye2, String str2) {
            this.sku = str;
            this.angleSubeye = angleSubeye2;
            this.prefixSerialNumber = str2;
        }

        @DexIgnore
        public static SpecialSku fromSerialNumber(String str) {
            for (SpecialSku specialSku : values()) {
                if (DeviceHelper.o.b(str).equalsIgnoreCase(specialSku.getPrefixSerialNumber())) {
                    return specialSku;
                }
            }
            return NONE;
        }

        @DexIgnore
        public static SpecialSku fromType(String str) {
            for (SpecialSku specialSku : values()) {
                if (specialSku.getSku().equalsIgnoreCase(str)) {
                    return specialSku;
                }
            }
            return NONE;
        }

        @DexIgnore
        public AngleSubeye getAngleSubeye() {
            return this.angleSubeye;
        }

        @DexIgnore
        public String getPrefixSerialNumber() {
            return this.prefixSerialNumber;
        }

        @DexIgnore
        public String getSku() {
            return this.sku;
        }
    }
}
