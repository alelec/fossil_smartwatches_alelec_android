package com.portfolio.platform.data;

import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRepository_Factory implements Factory<RingStyleRepository> {
    @DexIgnore
    public /* final */ Provider<FileRepository> mFileRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<RingStyleDao> mRingStyleDaoProvider;
    @DexIgnore
    public /* final */ Provider<RingStyleRemoteDataSource> mRingStyleRemoveSourceProvider;

    @DexIgnore
    public RingStyleRepository_Factory(Provider<RingStyleDao> provider, Provider<RingStyleRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        this.mRingStyleDaoProvider = provider;
        this.mRingStyleRemoveSourceProvider = provider2;
        this.mFileRepositoryProvider = provider3;
    }

    @DexIgnore
    public static RingStyleRepository_Factory create(Provider<RingStyleDao> provider, Provider<RingStyleRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        return new RingStyleRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static RingStyleRepository newRingStyleRepository(RingStyleDao ringStyleDao, RingStyleRemoteDataSource ringStyleRemoteDataSource, FileRepository fileRepository) {
        return new RingStyleRepository(ringStyleDao, ringStyleRemoteDataSource, fileRepository);
    }

    @DexIgnore
    public static RingStyleRepository provideInstance(Provider<RingStyleDao> provider, Provider<RingStyleRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        return new RingStyleRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public RingStyleRepository get() {
        return provideInstance(this.mRingStyleDaoProvider, this.mRingStyleRemoveSourceProvider, this.mFileRepositoryProvider);
    }
}
