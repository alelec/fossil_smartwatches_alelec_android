package com.portfolio.platform.data;

import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummary {
    @DexIgnore
    public MFSleepDay sleepDay;
    @DexIgnore
    public List<MFSleepSession> sleepSessions;

    @DexIgnore
    public SleepSummary() {
        this((MFSleepDay) null, (List) null, 3, (qg6) null);
    }

    @DexIgnore
    public SleepSummary(MFSleepDay mFSleepDay, List<MFSleepSession> list) {
        this.sleepDay = mFSleepDay;
        this.sleepSessions = list;
    }

    @DexIgnore
    public static /* synthetic */ SleepSummary copy$default(SleepSummary sleepSummary, MFSleepDay mFSleepDay, List<MFSleepSession> list, int i, Object obj) {
        if ((i & 1) != 0) {
            mFSleepDay = sleepSummary.sleepDay;
        }
        if ((i & 2) != 0) {
            list = sleepSummary.sleepSessions;
        }
        return sleepSummary.copy(mFSleepDay, list);
    }

    @DexIgnore
    public final MFSleepDay component1() {
        return this.sleepDay;
    }

    @DexIgnore
    public final List<MFSleepSession> component2() {
        return this.sleepSessions;
    }

    @DexIgnore
    public final SleepSummary copy(MFSleepDay mFSleepDay, List<MFSleepSession> list) {
        return new SleepSummary(mFSleepDay, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepSummary)) {
            return false;
        }
        SleepSummary sleepSummary = (SleepSummary) obj;
        return wg6.a((Object) this.sleepDay, (Object) sleepSummary.sleepDay) && wg6.a((Object) this.sleepSessions, (Object) sleepSummary.sleepSessions);
    }

    @DexIgnore
    public final Date getDate() {
        MFSleepDay mFSleepDay = this.sleepDay;
        if (mFSleepDay == null) {
            List<MFSleepSession> list = this.sleepSessions;
            if (list == null) {
                return null;
            }
            if (list == null) {
                wg6.a();
                throw null;
            } else if (!(!list.isEmpty())) {
                return null;
            } else {
                List<MFSleepSession> list2 = this.sleepSessions;
                if (list2 != null) {
                    return list2.get(0).getDay();
                }
                wg6.a();
                throw null;
            }
        } else if (mFSleepDay != null) {
            return mFSleepDay.getDate();
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final MFSleepDay getSleepDay() {
        return this.sleepDay;
    }

    @DexIgnore
    public final List<MFSleepSession> getSleepSessions() {
        return this.sleepSessions;
    }

    @DexIgnore
    public int hashCode() {
        MFSleepDay mFSleepDay = this.sleepDay;
        int i = 0;
        int hashCode = (mFSleepDay != null ? mFSleepDay.hashCode() : 0) * 31;
        List<MFSleepSession> list = this.sleepSessions;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setSleepDay(MFSleepDay mFSleepDay) {
        this.sleepDay = mFSleepDay;
    }

    @DexIgnore
    public final void setSleepSessions(List<MFSleepSession> list) {
        this.sleepSessions = list;
    }

    @DexIgnore
    public String toString() {
        return "SleepSummary(sleepDay=" + this.sleepDay + ", sleepSessions=" + this.sleepSessions + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SleepSummary(MFSleepDay mFSleepDay, List list, int i, qg6 qg6) {
        this((i & 1) != 0 ? null : mFSleepDay, (i & 2) != 0 ? null : list);
    }
}
