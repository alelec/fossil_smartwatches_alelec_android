package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.gg6;
import com.fossil.xg6;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository$getSummariesPaging$Anon2 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$getSummariesPaging$Anon2(HeartRateSummaryDataSourceFactory heartRateSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = heartRateSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = (HeartRateSummaryLocalDataSource) this.$sourceFactory.getSourceLiveData().a();
        if (heartRateSummaryLocalDataSource != null) {
            heartRateSummaryLocalDataSource.invalidate();
        }
    }
}
