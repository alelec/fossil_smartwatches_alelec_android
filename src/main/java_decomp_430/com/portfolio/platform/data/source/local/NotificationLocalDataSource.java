package com.portfolio.platform.data.source.local;

import android.util.SparseArray;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationLocalDataSource implements NotificationsDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "NotificationLocalDataSource";

    @DexIgnore
    public void clearAllNotificationSetting() {
        zm4.p.a().b().removeAllContactGroups();
        zm4.p.a().a().removeAllAppFilters();
    }

    @DexIgnore
    public void clearAllPhoneFavoritesContacts() {
        zm4.p.a().i().e();
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilterByHour(int i, int i2) {
        return zm4.p.a().a().getAllAppFiltersWithHour(i, i2);
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilterByVibration(int i) {
        return zm4.p.a().a().getAllAppFilterVibration(i);
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilters(int i) {
        return zm4.p.a().a().getAllAppFilters(i);
    }

    @DexIgnore
    public List<ContactGroup> getAllContactGroups(int i) {
        return zm4.p.a().b().getAllContactGroups(i);
    }

    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i) {
        SparseArray<List<BaseFeatureModel>> sparseArray = new SparseArray<>();
        ArrayList<BaseFeatureModel> arrayList = new ArrayList<>();
        List allContactGroups = zm4.p.a().b().getAllContactGroups(i);
        List allAppFilters = zm4.p.a().a().getAllAppFilters(i);
        if (allContactGroups != null && !allContactGroups.isEmpty()) {
            arrayList.addAll(allContactGroups);
        }
        if (allAppFilters != null && !allAppFilters.isEmpty()) {
            arrayList.addAll(allAppFilters);
        }
        for (BaseFeatureModel baseFeatureModel : arrayList) {
            List list = sparseArray.get(baseFeatureModel.getHour());
            if (list == null || list.isEmpty()) {
                list = new ArrayList();
            }
            list.add(baseFeatureModel);
            sparseArray.put(baseFeatureModel.getHour(), list);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Inside .getAllNotificationsByHour inLocal, result=" + sparseArray);
        return sparseArray;
    }

    @DexIgnore
    public AppFilter getAppFilterByType(String str, int i) {
        return zm4.p.a().a().getAppFilterMatchingType(str, i);
    }

    @DexIgnore
    public Contact getContactById(int i) {
        return zm4.p.a().b().a(i);
    }

    @DexIgnore
    public List<Integer> getContactGroupId(List<Integer> list) {
        return zm4.p.a().b().b(list);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        return zm4.p.a().b().getContactGroupsMatchingEmail(str, i);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        return zm4.p.a().b().getContactGroupsMatchingIncomingCall(str, i);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        return zm4.p.a().b().getContactGroupsMatchingSms(str, i);
    }

    @DexIgnore
    public List<Integer> getLocalContactId() {
        return zm4.p.a().b().h();
    }

    @DexIgnore
    public void removeAllAppFilters() {
        zm4.p.a().a().removeAllAppFilters();
    }

    @DexIgnore
    public void removeAppFilter(AppFilter appFilter) {
        zm4.p.a().a().removeAppFilter(appFilter);
    }

    @DexIgnore
    public void removeContact(Contact contact) {
        zm4.p.a().b().removeContact(contact);
    }

    @DexIgnore
    public void removeContactGroup(ContactGroup contactGroup) {
        zm4.p.a().b().removeContactGroup(contactGroup);
    }

    @DexIgnore
    public void removeContactGroupList(List<ContactGroup> list) {
        ContactProvider b = zm4.p.a().b();
        for (ContactGroup removeContactGroup : list) {
            b.removeContactGroup(removeContactGroup);
        }
    }

    @DexIgnore
    public void removeListAppFilter(List<AppFilter> list) {
        AppFilterProvider a = zm4.p.a().a();
        for (AppFilter removeAppFilter : list) {
            a.removeAppFilter(removeAppFilter);
        }
    }

    @DexIgnore
    public void removeListContact(List<Contact> list) {
        ContactProvider b = zm4.p.a().b();
        for (Contact removeContact : list) {
            b.removeContact(removeContact);
        }
    }

    @DexIgnore
    public void removeLocalRedundantContact(List<Integer> list) {
        zm4.p.a().b().c(list);
    }

    @DexIgnore
    public void removeLocalRedundantContactGroup(List<Integer> list) {
        zm4.p.a().b().e(list);
    }

    @DexIgnore
    public void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        zm4.p.a().i().removePhoneFavoritesContact(phoneFavoritesContact.getPhoneNumber());
    }

    @DexIgnore
    public void removePhoneNumber(List<Integer> list) {
        zm4.p.a().b().d(list);
    }

    @DexIgnore
    public void removePhoneNumberByContactGroupId(int i) {
        zm4.p.a().b().b(i);
    }

    @DexIgnore
    public void removeRedundantContact() {
    }

    @DexIgnore
    public void saveAppFilter(AppFilter appFilter) {
        zm4.p.a().a().saveAppFilter(appFilter);
    }

    @DexIgnore
    public void saveContact(Contact contact) {
        zm4.p.a().b().saveContact(contact);
    }

    @DexIgnore
    public void saveContactGroup(ContactGroup contactGroup) {
        zm4.p.a().b().saveContactGroup(contactGroup);
    }

    @DexIgnore
    public void saveContactGroupList(List<ContactGroup> list) {
        ContactProvider b = zm4.p.a().b();
        for (ContactGroup saveContactGroup : list) {
            b.saveContactGroup(saveContactGroup);
        }
    }

    @DexIgnore
    public void saveEmailAddress(EmailAddress emailAddress) {
        zm4.p.a().b().saveEmailAddress(emailAddress);
    }

    @DexIgnore
    public void saveListAppFilters(List<AppFilter> list) {
        AppFilterProvider a = zm4.p.a().a();
        for (AppFilter saveAppFilter : list) {
            a.saveAppFilter(saveAppFilter);
        }
    }

    @DexIgnore
    public void saveListContact(List<Contact> list) {
        ContactProvider b = zm4.p.a().b();
        for (Contact saveContact : list) {
            b.saveContact(saveContact);
        }
    }

    @DexIgnore
    public void saveListPhoneNumber(List<PhoneNumber> list) {
        ContactProvider b = zm4.p.a().b();
        for (PhoneNumber savePhoneNumber : list) {
            b.savePhoneNumber(savePhoneNumber);
        }
    }

    @DexIgnore
    public void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        zm4.p.a().i().a(phoneFavoritesContact);
    }

    @DexIgnore
    public void savePhoneNumber(PhoneNumber phoneNumber) {
        zm4.p.a().b().savePhoneNumber(phoneNumber);
    }

    @DexIgnore
    public void removePhoneFavoritesContact(String str) {
        zm4.p.a().i().removePhoneFavoritesContact(str);
    }
}
