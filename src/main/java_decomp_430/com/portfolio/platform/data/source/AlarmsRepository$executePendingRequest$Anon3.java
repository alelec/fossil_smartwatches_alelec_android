package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$3", f = "AlarmsRepository.kt", l = {}, m = "invokeSuspend")
public final class AlarmsRepository$executePendingRequest$Anon3 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ap4 $upsertResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$executePendingRequest$Anon3(AlarmsRepository alarmsRepository, ap4 ap4, xe6 xe6) {
        super(2, xe6);
        this.this$0 = alarmsRepository;
        this.$upsertResponse = ap4;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AlarmsRepository$executePendingRequest$Anon3 alarmsRepository$executePendingRequest$Anon3 = new AlarmsRepository$executePendingRequest$Anon3(this.this$0, this.$upsertResponse, xe6);
        alarmsRepository$executePendingRequest$Anon3.p$ = (il6) obj;
        return alarmsRepository$executePendingRequest$Anon3;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$executePendingRequest$Anon3) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            List list = (List) ((cp4) this.$upsertResponse).a();
            if (list == null) {
                return null;
            }
            this.this$0.mAlarmsLocalDataSource.insertAlarms(list);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
