package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$downloadAlarms$2", f = "AlarmsRepository.kt", l = {}, m = "invokeSuspend")
public final class AlarmsRepository$downloadAlarms$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ap4 $alarmsResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$downloadAlarms$Anon2(AlarmsRepository alarmsRepository, ap4 ap4, xe6 xe6) {
        super(2, xe6);
        this.this$0 = alarmsRepository;
        this.$alarmsResponse = ap4;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AlarmsRepository$downloadAlarms$Anon2 alarmsRepository$downloadAlarms$Anon2 = new AlarmsRepository$downloadAlarms$Anon2(this.this$0, this.$alarmsResponse, xe6);
        alarmsRepository$downloadAlarms$Anon2.p$ = (il6) obj;
        return alarmsRepository$downloadAlarms$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$downloadAlarms$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        List<Alarm> list;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            if (!((cp4) this.$alarmsResponse).b() && (list = (List) ((cp4) this.$alarmsResponse).a()) != null) {
                for (Alarm alarm : list) {
                    if (AlarmHelper.d.a(alarm)) {
                        alarm.setActive(false);
                        alarm.setPinType(2);
                    }
                }
                this.this$0.mAlarmsLocalDataSource.insertAlarms(list);
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
