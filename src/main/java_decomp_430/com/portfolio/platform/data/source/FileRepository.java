package com.portfolio.platform.data.source;

import android.text.TextUtils;
import android.util.Base64;
import com.fossil.FileHelper;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.nc6;
import com.fossil.nm4;
import com.fossil.qg6;
import com.fossil.vu6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yf6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.manager.FileDownloadManager;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ PortfolioApp mApp;
    @DexIgnore
    public /* final */ FileDao mFileDao;
    @DexIgnore
    public /* final */ FileDownloadManager mFileDownloadManager;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return FileRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = FileRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "FileRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FileRepository(FileDao fileDao, FileDownloadManager fileDownloadManager, PortfolioApp portfolioApp) {
        wg6.b(fileDao, "mFileDao");
        wg6.b(fileDownloadManager, "mFileDownloadManager");
        wg6.b(portfolioApp, "mApp");
        this.mFileDao = fileDao;
        this.mFileDownloadManager = fileDownloadManager;
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public static /* synthetic */ Object downloadFromURL$default(FileRepository fileRepository, String str, String str2, xe6 xe6, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = null;
        }
        return fileRepository.downloadFromURL(str, str2, xe6);
    }

    @DexIgnore
    private final void upsertLocalFile(LocalFile localFile) {
        this.mFileDao.upsertLocalFile(localFile);
    }

    @DexIgnore
    public final Object cleanUp(xe6<? super cd6> xe6) {
        this.mFileDao.clearLocalFileTable();
        return cd6.a;
    }

    @DexIgnore
    public final Object downloadFromURL(String str, String str2, xe6<? super cd6> xe6) {
        if (TextUtils.isEmpty(str)) {
            return cd6.a;
        }
        LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
        if (localFileByRemoteUrl == null) {
            String a = nm4.a(str);
            wg6.a((Object) a, "fileName");
            if (str != null) {
                LocalFile localFile = new LocalFile(a, "", str, str2);
                this.mFileDao.upsertLocalFile(localFile);
                localFileByRemoteUrl = localFile;
            } else {
                wg6.a();
                throw null;
            }
        } else if (localFileByRemoteUrl.getPinType() == 0) {
            return cd6.a;
        }
        this.mFileDownloadManager.b(new FileDownloadManager.b(localFileByRemoteUrl, new FileRepository$downloadFromURL$Anon2(this, str)));
        return cd6.a;
    }

    @DexIgnore
    public final void downloadPendingFile() {
        List<LocalFile> listPendingFile = this.mFileDao.getListPendingFile();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "downloadPendingFile with size " + listPendingFile.size());
        for (LocalFile localFile : listPendingFile) {
            this.mFileDownloadManager.b(new FileDownloadManager.b(localFile, new FileRepository$downloadPendingFile$$inlined$forEach$lambda$Anon1(localFile, this)));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006e, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006f, code lost:
        com.fossil.yf6.a(r0, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
        throw r6;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getBinaryString(String str, xe6<? super String> xe6) {
        FileRepository$getBinaryString$Anon1 fileRepository$getBinaryString$Anon1;
        int i;
        if (xe6 instanceof FileRepository$getBinaryString$Anon1) {
            fileRepository$getBinaryString$Anon1 = (FileRepository$getBinaryString$Anon1) xe6;
            int i2 = fileRepository$getBinaryString$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fileRepository$getBinaryString$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fileRepository$getBinaryString$Anon1.result;
                Object a = ff6.a();
                i = fileRepository$getBinaryString$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    fileRepository$getBinaryString$Anon1.L$0 = this;
                    fileRepository$getBinaryString$Anon1.L$1 = str;
                    fileRepository$getBinaryString$Anon1.label = 1;
                    obj = getFileByRemoteUrl(str, fileRepository$getBinaryString$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) fileRepository$getBinaryString$Anon1.L$1;
                    FileRepository fileRepository = (FileRepository) fileRepository$getBinaryString$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                File file = (File) obj;
                FileInputStream fileInputStream = file == null ? new FileInputStream(file) : null;
                String encodeToString = Base64.encodeToString(vu6.b(fileInputStream), 0);
                wg6.a((Object) encodeToString, "Base64.encodeToString(IO\u2026rray(it), Base64.DEFAULT)");
                yf6.a(fileInputStream, (Throwable) null);
                wg6.a((Object) encodeToString, "file?.inputStream().use \u2026Base64.DEFAULT)\n        }");
                return encodeToString;
            }
        }
        fileRepository$getBinaryString$Anon1 = new FileRepository$getBinaryString$Anon1(this, xe6);
        Object obj2 = fileRepository$getBinaryString$Anon1.result;
        Object a2 = ff6.a();
        i = fileRepository$getBinaryString$Anon1.label;
        if (i != 0) {
        }
        File file2 = (File) obj2;
        if (file2 == null) {
        }
        String encodeToString2 = Base64.encodeToString(vu6.b(fileInputStream), 0);
        wg6.a((Object) encodeToString2, "Base64.encodeToString(IO\u2026rray(it), Base64.DEFAULT)");
        yf6.a(fileInputStream, (Throwable) null);
        wg6.a((Object) encodeToString2, "file?.inputStream().use \u2026Base64.DEFAULT)\n        }");
        return encodeToString2;
    }

    @DexIgnore
    public final Object getFileByRemoteUrl(String str, xe6<? super File> xe6) {
        LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
        String fileName = localFileByRemoteUrl != null ? localFileByRemoteUrl.getFileName() : null;
        boolean a = FileHelper.a.a(fileName);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getFileLocalUrl with remoteUrl " + str + " isFileExist " + a);
        if (TextUtils.isEmpty(fileName) || !a) {
            return null;
        }
        if (fileName != null) {
            return new File(fileName);
        }
        wg6.a();
        throw null;
    }
}
