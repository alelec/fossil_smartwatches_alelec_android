package com.portfolio.platform.data.source.local.dnd;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDSettingsDatabase_Impl extends DNDSettingsDatabase {
    @DexIgnore
    public volatile DNDScheduledTimeDao _dNDScheduledTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `dndScheduledTimeModel` (`scheduledTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `scheduledTimeType` INTEGER NOT NULL, PRIMARY KEY(`scheduledTimeName`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'edbea9a651a6427903efe74c61fd6b87')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `dndScheduledTimeModel`");
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = DNDSettingsDatabase_Impl.this.mDatabase = iiVar;
            DNDSettingsDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (DNDSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = DNDSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DNDSettingsDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("scheduledTimeName", new fi.a("scheduledTimeName", "TEXT", true, 1, (String) null, 1));
            hashMap.put("minutes", new fi.a("minutes", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("scheduledTimeType", new fi.a("scheduledTimeType", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("dndScheduledTimeModel", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "dndScheduledTimeModel");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "dndScheduledTimeModel(com.portfolio.platform.data.model.DNDScheduledTimeModel).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        DNDSettingsDatabase_Impl.super.assertNotMainThread();
        ii a = DNDSettingsDatabase_Impl.super.getOpenHelper().a();
        try {
            DNDSettingsDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `dndScheduledTimeModel`");
            DNDSettingsDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            DNDSettingsDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"dndScheduledTimeModel"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "edbea9a651a6427903efe74c61fd6b87", "1a7527e5c35fa2c8a6aef70719b78d76");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public DNDScheduledTimeDao getDNDScheduledTimeDao() {
        DNDScheduledTimeDao dNDScheduledTimeDao;
        if (this._dNDScheduledTimeDao != null) {
            return this._dNDScheduledTimeDao;
        }
        synchronized (this) {
            if (this._dNDScheduledTimeDao == null) {
                this._dNDScheduledTimeDao = new DNDScheduledTimeDao_Impl(this);
            }
            dNDScheduledTimeDao = this._dNDScheduledTimeDao;
        }
        return dNDScheduledTimeDao;
    }
}
