package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.SleepStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$1$saveCallResult$1", f = "SleepSummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepStatistic $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository$getSleepStatistic$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2(SleepSummariesRepository$getSleepStatistic$Anon1 sleepSummariesRepository$getSleepStatistic$Anon1, SleepStatistic sleepStatistic, xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummariesRepository$getSleepStatistic$Anon1;
        this.$item = sleepStatistic;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2 sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2 = new SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2(this.this$0, this.$item, xe6);
        sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2.p$ = (il6) obj;
        return sleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummariesRepository$getSleepStatistic$Anon1$saveCallResult$Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.mSleepDao.upsertSleepStatistic(this.$item);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
