package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideComplicationRemoteDataSourceFactory implements Factory<ComplicationRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceV2Provider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideComplicationRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.apiServiceV2Provider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideComplicationRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideComplicationRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static ComplicationRemoteDataSource provideInstance(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return proxyProvideComplicationRemoteDataSource(repositoriesModule, provider.get());
    }

    @DexIgnore
    public static ComplicationRemoteDataSource proxyProvideComplicationRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        ComplicationRemoteDataSource provideComplicationRemoteDataSource = repositoriesModule.provideComplicationRemoteDataSource(apiServiceV2);
        z76.a(provideComplicationRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideComplicationRemoteDataSource;
    }

    @DexIgnore
    public ComplicationRemoteDataSource get() {
        return provideInstance(this.module, this.apiServiceV2Provider);
    }
}
