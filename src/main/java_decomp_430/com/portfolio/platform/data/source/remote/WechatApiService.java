package com.portfolio.platform.data.source.remote;

import com.fossil.ny6;
import com.fossil.zy6;
import com.portfolio.platform.data.WechatToken;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WechatApiService {
    @DexIgnore
    @ny6("oauth2/access_token")
    Call<WechatToken> getWechatToken(@zy6("appid") String str, @zy6("secret") String str2, @zy6("code") String str3, @zy6("grant_type") String str4);
}
