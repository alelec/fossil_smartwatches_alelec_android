package com.portfolio.platform.data.source;

import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRepository implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ServerSettingDataSource mServerSettingLocalDataSource;
    @DexIgnore
    public /* final */ ServerSettingDataSource mServerSettingRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerSettingRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "ServerSettingRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRepository(@Local ServerSettingDataSource serverSettingDataSource, @Remote ServerSettingDataSource serverSettingDataSource2) {
        wg6.b(serverSettingDataSource, "mServerSettingLocalDataSource");
        wg6.b(serverSettingDataSource2, "mServerSettingRemoteDataSource");
        this.mServerSettingLocalDataSource = serverSettingDataSource;
        this.mServerSettingRemoteDataSource = serverSettingDataSource2;
    }

    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
        this.mServerSettingLocalDataSource.addOrUpdateServerSetting(serverSetting);
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
        this.mServerSettingLocalDataSource.addOrUpdateServerSettingList(list);
    }

    @DexIgnore
    public void clearData() {
        this.mServerSettingLocalDataSource.clearData();
    }

    @DexIgnore
    public final ServerSetting generateSetting(String str, String str2) {
        wg6.b(str, "key");
        wg6.b(str2, ServerSetting.VALUE);
        ServerSetting serverSetting = new ServerSetting();
        serverSetting.setKey(str);
        serverSetting.setValue(str2);
        return serverSetting;
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        wg6.b(str, "key");
        return this.mServerSettingLocalDataSource.getServerSettingByKey(str);
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        wg6.b(onGetServerSettingList, Constants.CALLBACK);
        this.mServerSettingRemoteDataSource.getServerSettingList(new ServerSettingRepository$getServerSettingList$Anon1(this, onGetServerSettingList));
    }
}
