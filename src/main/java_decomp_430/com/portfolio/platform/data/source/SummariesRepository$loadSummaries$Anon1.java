package com.portfolio.platform.data.source;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SummariesRepository", f = "SummariesRepository.kt", l = {326}, m = "loadSummaries")
public final class SummariesRepository$loadSummaries$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$loadSummaries$Anon1(SummariesRepository summariesRepository, xe6 xe6) {
        super(xe6);
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.loadSummaries((Date) null, (Date) null, this);
    }
}
