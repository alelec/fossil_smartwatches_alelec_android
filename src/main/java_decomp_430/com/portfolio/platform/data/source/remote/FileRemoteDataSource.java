package com.portfolio.platform.data.source.remote;

import com.fossil.FileHelper;
import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.nc6;
import com.fossil.nm4;
import com.fossil.qg6;
import com.fossil.rx6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.fossil.zq6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.ServerError;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "FileRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public FileRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mService");
        this.mService = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object downloadFileToStorage(String str, String str2, xe6<? super ap4<Object>> xe6) {
        FileRemoteDataSource$downloadFileToStorage$Anon1 fileRemoteDataSource$downloadFileToStorage$Anon1;
        int i;
        String str3;
        rx6 rx6;
        if (xe6 instanceof FileRemoteDataSource$downloadFileToStorage$Anon1) {
            fileRemoteDataSource$downloadFileToStorage$Anon1 = (FileRemoteDataSource$downloadFileToStorage$Anon1) xe6;
            int i2 = fileRemoteDataSource$downloadFileToStorage$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                fileRemoteDataSource$downloadFileToStorage$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = fileRemoteDataSource$downloadFileToStorage$Anon1.result;
                Object a = ff6.a();
                i = fileRemoteDataSource$downloadFileToStorage$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    String a2 = nm4.a(str);
                    String str4 = str2 + File.separator + a2;
                    ApiServiceV2 apiServiceV2 = this.mService;
                    fileRemoteDataSource$downloadFileToStorage$Anon1.L$0 = this;
                    fileRemoteDataSource$downloadFileToStorage$Anon1.L$1 = str;
                    fileRemoteDataSource$downloadFileToStorage$Anon1.L$2 = str2;
                    fileRemoteDataSource$downloadFileToStorage$Anon1.L$3 = a2;
                    fileRemoteDataSource$downloadFileToStorage$Anon1.L$4 = str4;
                    fileRemoteDataSource$downloadFileToStorage$Anon1.label = 1;
                    obj = apiServiceV2.downloadFile(str, fileRemoteDataSource$downloadFileToStorage$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    str3 = str4;
                } else if (i == 1) {
                    str3 = (String) fileRemoteDataSource$downloadFileToStorage$Anon1.L$4;
                    String str5 = (String) fileRemoteDataSource$downloadFileToStorage$Anon1.L$3;
                    String str6 = (String) fileRemoteDataSource$downloadFileToStorage$Anon1.L$2;
                    String str7 = (String) fileRemoteDataSource$downloadFileToStorage$Anon1.L$1;
                    FileRemoteDataSource fileRemoteDataSource = (FileRemoteDataSource) fileRemoteDataSource$downloadFileToStorage$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                rx6 = (rx6) obj;
                if (rx6.d()) {
                    FLogger.INSTANCE.getLocal().e(TAG, "saveRemoteFile() fail");
                    return new zo4(500, (ServerError) null, (Throwable) null, (String) null, 12, (qg6) null);
                } else if (rx6.a() != null) {
                    FileHelper fileHelper = FileHelper.a;
                    Object a3 = rx6.a();
                    if (a3 != null) {
                        wg6.a(a3, "response.body()!!");
                        if (fileHelper.a((zq6) a3, str3)) {
                            FLogger.INSTANCE.getLocal().e(TAG, "saveRemoteFile() success");
                            return new cp4(new Object(), false, 2, (qg6) null);
                        }
                        FLogger.INSTANCE.getLocal().e(TAG, "saveRemoteFile() fail do to write file to local storage");
                        return new zo4(500, (ServerError) null, (Throwable) null, (String) null, 12, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else {
                    FLogger.INSTANCE.getLocal().e(TAG, "saveRemoteFile() fail due to empty content");
                    return new zo4(204, (ServerError) null, (Throwable) null, (String) null, 12, (qg6) null);
                }
            }
        }
        fileRemoteDataSource$downloadFileToStorage$Anon1 = new FileRemoteDataSource$downloadFileToStorage$Anon1(this, xe6);
        Object obj2 = fileRemoteDataSource$downloadFileToStorage$Anon1.result;
        Object a4 = ff6.a();
        i = fileRemoteDataSource$downloadFileToStorage$Anon1.label;
        if (i != 0) {
        }
        rx6 = (rx6) obj2;
        if (rx6.d()) {
        }
    }
}
