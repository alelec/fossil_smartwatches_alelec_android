package com.portfolio.platform.data.source;

import com.fossil.FileHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rc6;
import com.fossil.rl6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yj6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalizationRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;
    @DexIgnore
    public /* final */ an4 sharedPreferencesManager;

    @DexIgnore
    public WatchLocalizationRepository(ApiServiceV2 apiServiceV2, an4 an4) {
        wg6.b(apiServiceV2, "api");
        wg6.b(an4, "sharedPreferencesManager");
        this.api = apiServiceV2;
        this.sharedPreferencesManager = an4;
        String simpleName = WatchLocalizationRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchLocalizationRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final rl6<String> processDownloadAndStore(String str, String str2) {
        return ik6.a(jl6.a(zl6.b()), (af6) null, (ll6) null, new WatchLocalizationRepository$processDownloadAndStore$Anon1(this, str, str2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r14v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public final Object getWatchLocalizationFromServer(boolean z, xe6<? super String> xe6) {
        WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1 watchLocalizationRepository$getWatchLocalizationFromServer$Anon1;
        int i;
        WatchLocalizationRepository watchLocalizationRepository;
        boolean z2;
        String str;
        ap4 ap4;
        WatchLocalization watchLocalization;
        Object obj;
        xe6<? super String> xe62 = xe6;
        if (xe62 instanceof WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1) {
            watchLocalizationRepository$getWatchLocalizationFromServer$Anon1 = (WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1) xe62;
            int i2 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj2 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.result;
                Object a = ff6.a();
                i = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label;
                if (i != 0) {
                    nc6.a(obj2);
                    str = PortfolioApp.get.instance().p();
                    WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 watchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 = new WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(this, str, (xe6) null);
                    watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$0 = this;
                    boolean z3 = z;
                    watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$0 = z3;
                    watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$1 = str;
                    watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label = 1;
                    obj2 = ResponseKt.a(watchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1, watchLocalizationRepository$getWatchLocalizationFromServer$Anon1);
                    if (obj2 == a) {
                        return a;
                    }
                    z2 = z3;
                    watchLocalizationRepository = this;
                } else if (i == 1) {
                    str = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$1;
                    z2 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$0;
                    watchLocalizationRepository = (WatchLocalizationRepository) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$0;
                    nc6.a(obj2);
                } else if (i == 2) {
                    String str2 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$10;
                    rl6 rl6 = (rl6) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$9;
                    File file = (File) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$8;
                    String str3 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$7;
                    String str4 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$6;
                    WatchLocalization watchLocalization2 = (WatchLocalization) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$5;
                    List list = (List) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$4;
                    List list2 = (List) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$3;
                    ap4 ap42 = (ap4) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$2;
                    String str5 = (String) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$1;
                    boolean z4 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$0;
                    WatchLocalizationRepository watchLocalizationRepository2 = (WatchLocalizationRepository) watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$0;
                    nc6.a(obj2);
                    return (String) obj2;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj2;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    ApiResponse apiResponse = (ApiResponse) cp4.a();
                    List list3 = apiResponse != null ? apiResponse.get_items() : null;
                    if (list3 == null) {
                        return null;
                    }
                    WatchLocalization watchLocalization3 = (WatchLocalization) list3.get(0);
                    watchLocalizationRepository.sharedPreferencesManager.d(watchLocalization3.getName(), str);
                    watchLocalizationRepository.sharedPreferencesManager.t(watchLocalization3.getMetaData().getVersion().toString());
                    String url = watchLocalization3.getData().getUrl();
                    StringBuilder sb = new StringBuilder();
                    sb.append("localization_");
                    sb.append(str);
                    StringBuilder sb2 = sb;
                    int b = yj6.b((CharSequence) url, ".", 0, false, 6, (Object) null);
                    if (url != null) {
                        String substring = url.substring(b);
                        wg6.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                        sb2.append(substring);
                        String sb3 = sb2.toString();
                        String str6 = PortfolioApp.get.instance().getFilesDir() + "/localization";
                        File file2 = new File(str6);
                        if (!file2.exists()) {
                            boolean mkdirs = file2.mkdirs();
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str7 = watchLocalizationRepository.TAG;
                            obj = a;
                            StringBuilder sb4 = new StringBuilder();
                            watchLocalization = watchLocalization3;
                            sb4.append("create ");
                            sb4.append(file2);
                            sb4.append(" -  ");
                            sb4.append(mkdirs);
                            local.d(str7, sb4.toString());
                        } else {
                            obj = a;
                            watchLocalization = watchLocalization3;
                        }
                        if (cp4.b()) {
                            if (FileHelper.a.a(str6 + File.separator + sb3)) {
                                return null;
                            }
                        }
                        rl6<String> processDownloadAndStore = watchLocalizationRepository.processDownloadAndStore(url, str6 + File.separator + sb3);
                        if (!z2) {
                            return null;
                        }
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$0 = watchLocalizationRepository;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.Z$0 = z2;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$1 = str;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$2 = ap4;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$3 = list3;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$4 = list3;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$5 = watchLocalization;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$6 = url;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$7 = str6;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$8 = file2;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$9 = processDownloadAndStore;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.L$10 = sb3;
                        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label = 2;
                        obj2 = processDownloadAndStore.a(watchLocalizationRepository$getWatchLocalizationFromServer$Anon1);
                        Object obj3 = obj;
                        if (obj2 == obj3) {
                            return obj3;
                        }
                        return (String) obj2;
                    }
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
                FLogger.INSTANCE.getLocal().d(watchLocalizationRepository.TAG, "getWatchLocalizationFromServer - FAILED");
                return null;
            }
        }
        watchLocalizationRepository$getWatchLocalizationFromServer$Anon1 = new WatchLocalizationRepository$getWatchLocalizationFromServer$Anon1(this, xe62);
        Object obj22 = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.result;
        Object a2 = ff6.a();
        i = watchLocalizationRepository$getWatchLocalizationFromServer$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj22;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
