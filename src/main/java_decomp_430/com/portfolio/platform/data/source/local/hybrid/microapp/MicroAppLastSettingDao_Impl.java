package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingDao_Impl implements MicroAppLastSettingDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<MicroAppLastSetting> __insertionAdapterOfMicroAppLastSetting;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteMicroAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<MicroAppLastSetting> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppLastSetting` (`appId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, MicroAppLastSetting microAppLastSetting) {
            if (microAppLastSetting.getAppId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, microAppLastSetting.getAppId());
            }
            if (microAppLastSetting.getUpdatedAt() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, microAppLastSetting.getUpdatedAt());
            }
            if (microAppLastSetting.getSetting() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, microAppLastSetting.getSetting());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppLastSetting WHERE appId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM microAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<MicroAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<MicroAppLastSetting> call() throws Exception {
            Cursor a = bi.a(MicroAppLastSettingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "appId");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new MicroAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public MicroAppLastSettingDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfMicroAppLastSetting = new Anon1(ohVar);
        this.__preparedStmtOfDeleteMicroAppLastSettingById = new Anon2(ohVar);
        this.__preparedStmtOfCleanUp = new Anon3(ohVar);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteMicroAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteMicroAppLastSettingById.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    public List<MicroAppLastSetting> getAllMicroAppLastSetting() {
        rh b = rh.b("SELECT * FROM microAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "appId");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new MicroAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<MicroAppLastSetting>> getAllMicroAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"microAppLastSetting"}, false, new Anon4(rh.b("SELECT * FROM microAppLastSetting", 0)));
    }

    @DexIgnore
    public MicroAppLastSetting getMicroAppLastSetting(String str) {
        rh b = rh.b("SELECT * FROM microAppLastSetting WHERE appId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroAppLastSetting microAppLastSetting = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "appId");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, MicroAppSetting.SETTING);
            if (a.moveToFirst()) {
                microAppLastSetting = new MicroAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4));
            }
            return microAppLastSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(microAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
