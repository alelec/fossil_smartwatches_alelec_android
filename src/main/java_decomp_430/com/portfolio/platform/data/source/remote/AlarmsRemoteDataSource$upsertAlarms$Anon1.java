package com.portfolio.platform.data.source.remote;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource", f = "AlarmsRemoteDataSource.kt", l = {51}, m = "upsertAlarms")
public final class AlarmsRemoteDataSource$upsertAlarms$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRemoteDataSource$upsertAlarms$Anon1(AlarmsRemoteDataSource alarmsRemoteDataSource, xe6 xe6) {
        super(xe6);
        this.this$0 = alarmsRemoteDataSource;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.upsertAlarms((List<Alarm>) null, this);
    }
}
