package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideWorkoutDaoFactory implements Factory<WorkoutDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideWorkoutDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideWorkoutDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideWorkoutDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WorkoutDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideWorkoutDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static WorkoutDao proxyProvideWorkoutDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        WorkoutDao provideWorkoutDao = portfolioDatabaseModule.provideWorkoutDao(fitnessDatabase);
        z76.a(provideWorkoutDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideWorkoutDao;
    }

    @DexIgnore
    public WorkoutDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
