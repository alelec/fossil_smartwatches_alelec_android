package com.portfolio.platform.data.source.local.sleep;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.SleepStatistic;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDatabase_Impl extends SleepDatabase {
    @DexIgnore
    public volatile SleepDao _sleepDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `sleep_session` (`pinType` INTEGER NOT NULL, `date` INTEGER NOT NULL, `day` TEXT NOT NULL, `deviceSerialNumber` TEXT, `syncTime` INTEGER, `bookmarkTime` INTEGER, `normalizedSleepQuality` REAL NOT NULL, `source` INTEGER NOT NULL, `realStartTime` INTEGER NOT NULL, `realEndTime` INTEGER NOT NULL, `realSleepMinutes` INTEGER NOT NULL, `realSleepStateDistInMinute` TEXT NOT NULL, `editedStartTime` INTEGER, `editedEndTime` INTEGER, `editedSleepMinutes` INTEGER, `editedSleepStateDistInMinute` TEXT, `sleepStates` TEXT NOT NULL, `heartRate` TEXT, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, PRIMARY KEY(`realEndTime`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `sleep_date` (`pinType` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, `date` TEXT NOT NULL, `goalMinutes` INTEGER NOT NULL, `sleepMinutes` INTEGER NOT NULL, `sleepStateDistInMinute` TEXT, `createdAt` INTEGER, `updatedAt` INTEGER, PRIMARY KEY(`date`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `sleep_settings` (`id` INTEGER NOT NULL, `sleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `sleepRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedSleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `sleep_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `sleepTimeBestDay` TEXT, `sleepTimeBestStreak` TEXT, `totalDays` INTEGER NOT NULL, `totalSleeps` INTEGER NOT NULL, `totalSleepMinutes` INTEGER NOT NULL, `totalSleepStateDistInMinute` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6563ab2be16ee1f30194e9dfab2b7513')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `sleep_session`");
            iiVar.b("DROP TABLE IF EXISTS `sleep_date`");
            iiVar.b("DROP TABLE IF EXISTS `sleep_settings`");
            iiVar.b("DROP TABLE IF EXISTS `sleepRecommendedGoals`");
            iiVar.b("DROP TABLE IF EXISTS `sleep_statistic`");
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) SleepDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) SleepDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = SleepDatabase_Impl.this.mDatabase = iiVar;
            SleepDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) SleepDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(21);
            hashMap.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "INTEGER", true, 0, (String) null, 1));
            hashMap.put("day", new fi.a("day", "TEXT", true, 0, (String) null, 1));
            hashMap.put("deviceSerialNumber", new fi.a("deviceSerialNumber", "TEXT", false, 0, (String) null, 1));
            hashMap.put(DataFile.COLUMN_SYNC_TIME, new fi.a(DataFile.COLUMN_SYNC_TIME, "INTEGER", false, 0, (String) null, 1));
            hashMap.put("bookmarkTime", new fi.a("bookmarkTime", "INTEGER", false, 0, (String) null, 1));
            hashMap.put("normalizedSleepQuality", new fi.a("normalizedSleepQuality", "REAL", true, 0, (String) null, 1));
            hashMap.put("source", new fi.a("source", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("realStartTime", new fi.a("realStartTime", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("realEndTime", new fi.a("realEndTime", "INTEGER", true, 1, (String) null, 1));
            hashMap.put("realSleepMinutes", new fi.a("realSleepMinutes", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("realSleepStateDistInMinute", new fi.a("realSleepStateDistInMinute", "TEXT", true, 0, (String) null, 1));
            hashMap.put("editedStartTime", new fi.a("editedStartTime", "INTEGER", false, 0, (String) null, 1));
            hashMap.put("editedEndTime", new fi.a("editedEndTime", "INTEGER", false, 0, (String) null, 1));
            hashMap.put("editedSleepMinutes", new fi.a("editedSleepMinutes", "INTEGER", false, 0, (String) null, 1));
            hashMap.put("editedSleepStateDistInMinute", new fi.a("editedSleepStateDistInMinute", "TEXT", false, 0, (String) null, 1));
            hashMap.put("sleepStates", new fi.a("sleepStates", "TEXT", true, 0, (String) null, 1));
            hashMap.put("heartRate", new fi.a("heartRate", "TEXT", false, 0, (String) null, 1));
            hashMap.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("timezoneOffset", new fi.a("timezoneOffset", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("sleep_session", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "sleep_session");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "sleep_session(com.portfolio.platform.data.model.room.sleep.MFSleepSession).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(8);
            hashMap2.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("timezoneOffset", new fi.a("timezoneOffset", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 1, (String) null, 1));
            hashMap2.put("goalMinutes", new fi.a("goalMinutes", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("sleepMinutes", new fi.a("sleepMinutes", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("sleepStateDistInMinute", new fi.a("sleepStateDistInMinute", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("createdAt", new fi.a("createdAt", "INTEGER", false, 0, (String) null, 1));
            hashMap2.put("updatedAt", new fi.a("updatedAt", "INTEGER", false, 0, (String) null, 1));
            fi fiVar2 = new fi("sleep_date", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, "sleep_date");
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "sleep_date(com.portfolio.platform.data.model.room.sleep.MFSleepDay).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap3.put("sleepGoal", new fi.a("sleepGoal", "INTEGER", true, 0, (String) null, 1));
            fi fiVar3 = new fi("sleep_settings", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "sleep_settings");
            if (!fiVar3.equals(a3)) {
                return new qh.b(false, "sleep_settings(com.portfolio.platform.data.model.room.sleep.MFSleepSettings).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(2);
            hashMap4.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap4.put("recommendedSleepGoal", new fi.a("recommendedSleepGoal", "INTEGER", true, 0, (String) null, 1));
            fi fiVar4 = new fi("sleepRecommendedGoals", hashMap4, new HashSet(0), new HashSet(0));
            fi a4 = fi.a(iiVar2, "sleepRecommendedGoals");
            if (!fiVar4.equals(a4)) {
                return new qh.b(false, "sleepRecommendedGoals(com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal).\n Expected:\n" + fiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(10);
            hashMap5.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap5.put("uid", new fi.a("uid", "TEXT", true, 0, (String) null, 1));
            hashMap5.put("sleepTimeBestDay", new fi.a("sleepTimeBestDay", "TEXT", false, 0, (String) null, 1));
            hashMap5.put("sleepTimeBestStreak", new fi.a("sleepTimeBestStreak", "TEXT", false, 0, (String) null, 1));
            hashMap5.put("totalDays", new fi.a("totalDays", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("totalSleeps", new fi.a("totalSleeps", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("totalSleepMinutes", new fi.a("totalSleepMinutes", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("totalSleepStateDistInMinute", new fi.a("totalSleepStateDistInMinute", "TEXT", true, 0, (String) null, 1));
            hashMap5.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            fi fiVar5 = new fi(SleepStatistic.TABLE_NAME, hashMap5, new HashSet(0), new HashSet(0));
            fi a5 = fi.a(iiVar2, SleepStatistic.TABLE_NAME);
            if (fiVar5.equals(a5)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "sleep_statistic(com.portfolio.platform.data.SleepStatistic).\n Expected:\n" + fiVar5 + "\n Found:\n" + a5);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        SleepDatabase_Impl.super.assertNotMainThread();
        ii a = SleepDatabase_Impl.super.getOpenHelper().a();
        try {
            SleepDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `sleep_session`");
            a.b("DELETE FROM `sleep_date`");
            a.b("DELETE FROM `sleep_settings`");
            a.b("DELETE FROM `sleepRecommendedGoals`");
            a.b("DELETE FROM `sleep_statistic`");
            SleepDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            SleepDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"sleep_session", "sleep_date", "sleep_settings", "sleepRecommendedGoals", SleepStatistic.TABLE_NAME});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(9), "6563ab2be16ee1f30194e9dfab2b7513", "318ff9dd57f69c3e478563bc453dc851");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public SleepDao sleepDao() {
        SleepDao sleepDao;
        if (this._sleepDao != null) {
            return this._sleepDao;
        }
        synchronized (this) {
            if (this._sleepDao == null) {
                this._sleepDao = new SleepDao_Impl(this);
            }
            sleepDao = this._sleepDao;
        }
        return sleepDao;
    }
}
