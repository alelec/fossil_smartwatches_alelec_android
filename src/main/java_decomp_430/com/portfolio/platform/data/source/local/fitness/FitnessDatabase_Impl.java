package com.portfolio.platform.data.source.local.fitness;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.FitnessDataDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao_Impl;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao_Impl;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao_Impl;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDatabase_Impl extends FitnessDatabase {
    @DexIgnore
    public volatile ActivitySampleDao _activitySampleDao;
    @DexIgnore
    public volatile ActivitySummaryDao _activitySummaryDao;
    @DexIgnore
    public volatile FitnessDataDao _fitnessDataDao;
    @DexIgnore
    public volatile HeartRateDailySummaryDao _heartRateDailySummaryDao;
    @DexIgnore
    public volatile HeartRateSampleDao _heartRateSampleDao;
    @DexIgnore
    public volatile SampleRawDao _sampleRawDao;
    @DexIgnore
    public volatile WorkoutDao _workoutDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `sampleraw` (`id` TEXT NOT NULL, `pinType` INTEGER NOT NULL, `uaPinType` INTEGER NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `sourceId` TEXT NOT NULL, `sourceTypeValue` TEXT NOT NULL, `movementTypeValue` TEXT, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneID` TEXT, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `activity_sample` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `activeTime` INTEGER NOT NULL, `intensityDistInSteps` TEXT NOT NULL, `timeZoneOffsetInSecond` INTEGER NOT NULL, `sourceId` TEXT NOT NULL, `syncTime` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `sampleday` (`createdAt` INTEGER, `updatedAt` INTEGER, `pinType` INTEGER NOT NULL, `year` INTEGER NOT NULL, `month` INTEGER NOT NULL, `day` INTEGER NOT NULL, `timezoneName` TEXT, `dstOffset` INTEGER, `steps` REAL NOT NULL, `calories` REAL NOT NULL, `distance` REAL NOT NULL, `intensities` TEXT NOT NULL, `activeTime` INTEGER NOT NULL, `stepGoal` INTEGER NOT NULL, `caloriesGoal` INTEGER NOT NULL, `activeTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`year`, `month`, `day`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `activitySettings` (`id` INTEGER NOT NULL, `currentStepGoal` INTEGER NOT NULL, `currentCaloriesGoal` INTEGER NOT NULL, `currentActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `activityRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedStepsGoal` INTEGER NOT NULL, `recommendedCaloriesGoal` INTEGER NOT NULL, `recommendedActiveTimeGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `workout_session` (`speed` TEXT, `location` TEXT, `states` TEXT NOT NULL, `id` TEXT NOT NULL, `date` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `deviceSerialNumber` TEXT, `step` TEXT, `calorie` TEXT, `distance` TEXT, `heartRate` TEXT, `sourceType` TEXT, `workoutType` TEXT, `timezoneOffset` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `fitness_data` (`step` TEXT NOT NULL, `activeMinute` TEXT NOT NULL, `calorie` TEXT NOT NULL, `distance` TEXT NOT NULL, `stress` TEXT, `resting` TEXT NOT NULL, `heartRate` TEXT, `sleeps` TEXT NOT NULL, `workouts` TEXT NOT NULL, `startTime` TEXT NOT NULL, `endTime` TEXT NOT NULL, `syncTime` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `serialNumber` TEXT NOT NULL, PRIMARY KEY(`startTime`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `heart_rate_sample` (`id` TEXT NOT NULL, `average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `endTime` TEXT NOT NULL, `startTime` TEXT NOT NULL, `timezoneOffset` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `daily_heart_rate_summary` (`average` REAL NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `min` INTEGER NOT NULL, `max` INTEGER NOT NULL, `minuteCount` INTEGER NOT NULL, `resting` TEXT, PRIMARY KEY(`date`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `activity_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `activeTimeBestDay` TEXT, `activeTimeBestStreak` TEXT, `caloriesBestDay` TEXT, `caloriesBestStreak` TEXT, `stepsBestDay` TEXT, `stepsBestStreak` TEXT, `totalActiveTime` INTEGER NOT NULL, `totalCalories` REAL NOT NULL, `totalDays` INTEGER NOT NULL, `totalDistance` REAL NOT NULL, `totalSteps` INTEGER NOT NULL, `totalIntensityDistInStep` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '9c43c28ab51a9a7a22d2899f05515d5a')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `sampleraw`");
            iiVar.b("DROP TABLE IF EXISTS `activity_sample`");
            iiVar.b("DROP TABLE IF EXISTS `sampleday`");
            iiVar.b("DROP TABLE IF EXISTS `activitySettings`");
            iiVar.b("DROP TABLE IF EXISTS `activityRecommendedGoals`");
            iiVar.b("DROP TABLE IF EXISTS `workout_session`");
            iiVar.b("DROP TABLE IF EXISTS `fitness_data`");
            iiVar.b("DROP TABLE IF EXISTS `heart_rate_sample`");
            iiVar.b("DROP TABLE IF EXISTS `daily_heart_rate_summary`");
            iiVar.b("DROP TABLE IF EXISTS `activity_statistic`");
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = FitnessDatabase_Impl.this.mDatabase = iiVar;
            FitnessDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (FitnessDatabase_Impl.this.mCallbacks != null) {
                int size = FitnessDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) FitnessDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(14);
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("uaPinType", new fi.a("uaPinType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("startTime", new fi.a("startTime", "TEXT", true, 0, (String) null, 1));
            hashMap.put("endTime", new fi.a("endTime", "TEXT", true, 0, (String) null, 1));
            hashMap.put("sourceId", new fi.a("sourceId", "TEXT", true, 0, (String) null, 1));
            hashMap.put("sourceTypeValue", new fi.a("sourceTypeValue", "TEXT", true, 0, (String) null, 1));
            hashMap.put("movementTypeValue", new fi.a("movementTypeValue", "TEXT", false, 0, (String) null, 1));
            hashMap.put("steps", new fi.a("steps", "REAL", true, 0, (String) null, 1));
            hashMap.put(Constants.CALORIES, new fi.a(Constants.CALORIES, "REAL", true, 0, (String) null, 1));
            hashMap.put("distance", new fi.a("distance", "REAL", true, 0, (String) null, 1));
            hashMap.put("activeTime", new fi.a("activeTime", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("intensityDistInSteps", new fi.a("intensityDistInSteps", "TEXT", true, 0, (String) null, 1));
            hashMap.put("timeZoneID", new fi.a("timeZoneID", "TEXT", false, 0, (String) null, 1));
            fi fiVar = new fi("sampleraw", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "sampleraw");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "sampleraw(com.portfolio.platform.data.model.room.fitness.SampleRaw).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(15);
            hashMap2.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap2.put("uid", new fi.a("uid", "TEXT", true, 0, (String) null, 1));
            hashMap2.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 0, (String) null, 1));
            hashMap2.put("startTime", new fi.a("startTime", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("endTime", new fi.a("endTime", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("steps", new fi.a("steps", "REAL", true, 0, (String) null, 1));
            hashMap2.put(Constants.CALORIES, new fi.a(Constants.CALORIES, "REAL", true, 0, (String) null, 1));
            hashMap2.put("distance", new fi.a("distance", "REAL", true, 0, (String) null, 1));
            hashMap2.put("activeTime", new fi.a("activeTime", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("intensityDistInSteps", new fi.a("intensityDistInSteps", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("timeZoneOffsetInSecond", new fi.a("timeZoneOffsetInSecond", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("sourceId", new fi.a("sourceId", "TEXT", true, 0, (String) null, 1));
            hashMap2.put(DataFile.COLUMN_SYNC_TIME, new fi.a(DataFile.COLUMN_SYNC_TIME, "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap2.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            Object obj = "endTime";
            Object obj2 = "startTime";
            fi fiVar2 = new fi(ActivitySample.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, ActivitySample.TABLE_NAME);
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "activity_sample(com.portfolio.platform.data.model.room.fitness.ActivitySample).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(16);
            hashMap3.put("createdAt", new fi.a("createdAt", "INTEGER", false, 0, (String) null, 1));
            hashMap3.put("updatedAt", new fi.a("updatedAt", "INTEGER", false, 0, (String) null, 1));
            hashMap3.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("year", new fi.a("year", "INTEGER", true, 1, (String) null, 1));
            hashMap3.put("month", new fi.a("month", "INTEGER", true, 2, (String) null, 1));
            hashMap3.put("day", new fi.a("day", "INTEGER", true, 3, (String) null, 1));
            hashMap3.put("timezoneName", new fi.a("timezoneName", "TEXT", false, 0, (String) null, 1));
            hashMap3.put("dstOffset", new fi.a("dstOffset", "INTEGER", false, 0, (String) null, 1));
            hashMap3.put("steps", new fi.a("steps", "REAL", true, 0, (String) null, 1));
            hashMap3.put(Constants.CALORIES, new fi.a(Constants.CALORIES, "REAL", true, 0, (String) null, 1));
            hashMap3.put("distance", new fi.a("distance", "REAL", true, 0, (String) null, 1));
            hashMap3.put("intensities", new fi.a("intensities", "TEXT", true, 0, (String) null, 1));
            hashMap3.put("activeTime", new fi.a("activeTime", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("stepGoal", new fi.a("stepGoal", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("caloriesGoal", new fi.a("caloriesGoal", "INTEGER", true, 0, (String) null, 1));
            hashMap3.put("activeTimeGoal", new fi.a("activeTimeGoal", "INTEGER", true, 0, (String) null, 1));
            fi fiVar3 = new fi("sampleday", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "sampleday");
            if (!fiVar3.equals(a3)) {
                return new qh.b(false, "sampleday(com.portfolio.platform.data.model.room.fitness.ActivitySummary).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(4);
            hashMap4.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap4.put("currentStepGoal", new fi.a("currentStepGoal", "INTEGER", true, 0, (String) null, 1));
            hashMap4.put("currentCaloriesGoal", new fi.a("currentCaloriesGoal", "INTEGER", true, 0, (String) null, 1));
            hashMap4.put("currentActiveTimeGoal", new fi.a("currentActiveTimeGoal", "INTEGER", true, 0, (String) null, 1));
            fi fiVar4 = new fi("activitySettings", hashMap4, new HashSet(0), new HashSet(0));
            fi a4 = fi.a(iiVar2, "activitySettings");
            if (!fiVar4.equals(a4)) {
                return new qh.b(false, "activitySettings(com.portfolio.platform.data.model.room.fitness.ActivitySettings).\n Expected:\n" + fiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(4);
            hashMap5.put("id", new fi.a("id", "INTEGER", true, 1, (String) null, 1));
            hashMap5.put("recommendedStepsGoal", new fi.a("recommendedStepsGoal", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("recommendedCaloriesGoal", new fi.a("recommendedCaloriesGoal", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("recommendedActiveTimeGoal", new fi.a("recommendedActiveTimeGoal", "INTEGER", true, 0, (String) null, 1));
            fi fiVar5 = new fi("activityRecommendedGoals", hashMap5, new HashSet(0), new HashSet(0));
            fi a5 = fi.a(iiVar2, "activityRecommendedGoals");
            if (!fiVar5.equals(a5)) {
                return new qh.b(false, "activityRecommendedGoals(com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals).\n Expected:\n" + fiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(18);
            hashMap6.put("speed", new fi.a("speed", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("location", new fi.a("location", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("states", new fi.a("states", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap6.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 0, (String) null, 1));
            Object obj3 = obj2;
            hashMap6.put(obj3, new fi.a("startTime", "TEXT", true, 0, (String) null, 1));
            Object obj4 = obj;
            hashMap6.put(obj4, new fi.a("endTime", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("deviceSerialNumber", new fi.a("deviceSerialNumber", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("step", new fi.a("step", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("calorie", new fi.a("calorie", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("distance", new fi.a("distance", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("heartRate", new fi.a("heartRate", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("sourceType", new fi.a("sourceType", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("workoutType", new fi.a("workoutType", "TEXT", false, 0, (String) null, 1));
            hashMap6.put("timezoneOffset", new fi.a("timezoneOffset", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("duration", new fi.a("duration", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            fi fiVar6 = new fi("workout_session", hashMap6, new HashSet(0), new HashSet(0));
            fi a6 = fi.a(iiVar2, "workout_session");
            if (!fiVar6.equals(a6)) {
                return new qh.b(false, "workout_session(com.portfolio.platform.data.model.diana.workout.WorkoutSession).\n Expected:\n" + fiVar6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(14);
            hashMap7.put("step", new fi.a("step", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("activeMinute", new fi.a("activeMinute", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("calorie", new fi.a("calorie", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("distance", new fi.a("distance", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("stress", new fi.a("stress", "TEXT", false, 0, (String) null, 1));
            hashMap7.put("resting", new fi.a("resting", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("heartRate", new fi.a("heartRate", "TEXT", false, 0, (String) null, 1));
            hashMap7.put("sleeps", new fi.a("sleeps", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("workouts", new fi.a("workouts", "TEXT", true, 0, (String) null, 1));
            hashMap7.put(obj3, new fi.a("startTime", "TEXT", true, 1, (String) null, 1));
            hashMap7.put(obj4, new fi.a("endTime", "TEXT", true, 0, (String) null, 1));
            hashMap7.put(DataFile.COLUMN_SYNC_TIME, new fi.a(DataFile.COLUMN_SYNC_TIME, "TEXT", true, 0, (String) null, 1));
            hashMap7.put("timezoneOffsetInSecond", new fi.a("timezoneOffsetInSecond", "INTEGER", true, 0, (String) null, 1));
            hashMap7.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 0, (String) null, 1));
            fi fiVar7 = new fi("fitness_data", hashMap7, new HashSet(0), new HashSet(0));
            fi a7 = fi.a(iiVar2, "fitness_data");
            if (!fiVar7.equals(a7)) {
                return new qh.b(false, "fitness_data(com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper).\n Expected:\n" + fiVar7 + "\n Found:\n" + a7);
            }
            HashMap hashMap8 = new HashMap(12);
            hashMap8.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap8.put("average", new fi.a("average", "REAL", true, 0, (String) null, 1));
            hashMap8.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 0, (String) null, 1));
            hashMap8.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap8.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            hashMap8.put(obj4, new fi.a("endTime", "TEXT", true, 0, (String) null, 1));
            hashMap8.put(obj3, new fi.a("startTime", "TEXT", true, 0, (String) null, 1));
            hashMap8.put("timezoneOffset", new fi.a("timezoneOffset", "INTEGER", true, 0, (String) null, 1));
            hashMap8.put("min", new fi.a("min", "INTEGER", true, 0, (String) null, 1));
            hashMap8.put("max", new fi.a("max", "INTEGER", true, 0, (String) null, 1));
            hashMap8.put("minuteCount", new fi.a("minuteCount", "INTEGER", true, 0, (String) null, 1));
            hashMap8.put("resting", new fi.a("resting", "TEXT", false, 0, (String) null, 1));
            fi fiVar8 = new fi("heart_rate_sample", hashMap8, new HashSet(0), new HashSet(0));
            fi a8 = fi.a(iiVar2, "heart_rate_sample");
            if (!fiVar8.equals(a8)) {
                return new qh.b(false, "heart_rate_sample(com.portfolio.platform.data.model.diana.heartrate.HeartRateSample).\n Expected:\n" + fiVar8 + "\n Found:\n" + a8);
            }
            HashMap hashMap9 = new HashMap(8);
            hashMap9.put("average", new fi.a("average", "REAL", true, 0, (String) null, 1));
            hashMap9.put(HardwareLog.COLUMN_DATE, new fi.a(HardwareLog.COLUMN_DATE, "TEXT", true, 1, (String) null, 1));
            hashMap9.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap9.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            hashMap9.put("min", new fi.a("min", "INTEGER", true, 0, (String) null, 1));
            hashMap9.put("max", new fi.a("max", "INTEGER", true, 0, (String) null, 1));
            hashMap9.put("minuteCount", new fi.a("minuteCount", "INTEGER", true, 0, (String) null, 1));
            hashMap9.put("resting", new fi.a("resting", "TEXT", false, 0, (String) null, 1));
            fi fiVar9 = new fi("daily_heart_rate_summary", hashMap9, new HashSet(0), new HashSet(0));
            fi a9 = fi.a(iiVar2, "daily_heart_rate_summary");
            if (!fiVar9.equals(a9)) {
                return new qh.b(false, "daily_heart_rate_summary(com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary).\n Expected:\n" + fiVar9 + "\n Found:\n" + a9);
            }
            HashMap hashMap10 = new HashMap(16);
            hashMap10.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap10.put("uid", new fi.a("uid", "TEXT", true, 0, (String) null, 1));
            hashMap10.put("activeTimeBestDay", new fi.a("activeTimeBestDay", "TEXT", false, 0, (String) null, 1));
            hashMap10.put("activeTimeBestStreak", new fi.a("activeTimeBestStreak", "TEXT", false, 0, (String) null, 1));
            hashMap10.put("caloriesBestDay", new fi.a("caloriesBestDay", "TEXT", false, 0, (String) null, 1));
            hashMap10.put("caloriesBestStreak", new fi.a("caloriesBestStreak", "TEXT", false, 0, (String) null, 1));
            hashMap10.put("stepsBestDay", new fi.a("stepsBestDay", "TEXT", false, 0, (String) null, 1));
            hashMap10.put("stepsBestStreak", new fi.a("stepsBestStreak", "TEXT", false, 0, (String) null, 1));
            hashMap10.put("totalActiveTime", new fi.a("totalActiveTime", "INTEGER", true, 0, (String) null, 1));
            hashMap10.put("totalCalories", new fi.a("totalCalories", "REAL", true, 0, (String) null, 1));
            hashMap10.put("totalDays", new fi.a("totalDays", "INTEGER", true, 0, (String) null, 1));
            hashMap10.put("totalDistance", new fi.a("totalDistance", "REAL", true, 0, (String) null, 1));
            hashMap10.put("totalSteps", new fi.a("totalSteps", "INTEGER", true, 0, (String) null, 1));
            hashMap10.put("totalIntensityDistInStep", new fi.a("totalIntensityDistInStep", "TEXT", true, 0, (String) null, 1));
            hashMap10.put("createdAt", new fi.a("createdAt", "INTEGER", true, 0, (String) null, 1));
            hashMap10.put("updatedAt", new fi.a("updatedAt", "INTEGER", true, 0, (String) null, 1));
            fi fiVar10 = new fi(ActivityStatistic.TABLE_NAME, hashMap10, new HashSet(0), new HashSet(0));
            fi a10 = fi.a(iiVar2, ActivityStatistic.TABLE_NAME);
            if (fiVar10.equals(a10)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "activity_statistic(com.portfolio.platform.data.ActivityStatistic).\n Expected:\n" + fiVar10 + "\n Found:\n" + a10);
        }
    }

    @DexIgnore
    public ActivitySampleDao activitySampleDao() {
        ActivitySampleDao activitySampleDao;
        if (this._activitySampleDao != null) {
            return this._activitySampleDao;
        }
        synchronized (this) {
            if (this._activitySampleDao == null) {
                this._activitySampleDao = new ActivitySampleDao_Impl(this);
            }
            activitySampleDao = this._activitySampleDao;
        }
        return activitySampleDao;
    }

    @DexIgnore
    public ActivitySummaryDao activitySummaryDao() {
        ActivitySummaryDao activitySummaryDao;
        if (this._activitySummaryDao != null) {
            return this._activitySummaryDao;
        }
        synchronized (this) {
            if (this._activitySummaryDao == null) {
                this._activitySummaryDao = new ActivitySummaryDao_Impl(this);
            }
            activitySummaryDao = this._activitySummaryDao;
        }
        return activitySummaryDao;
    }

    @DexIgnore
    public void clearAllTables() {
        FitnessDatabase_Impl.super.assertNotMainThread();
        ii a = FitnessDatabase_Impl.super.getOpenHelper().a();
        try {
            FitnessDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `sampleraw`");
            a.b("DELETE FROM `activity_sample`");
            a.b("DELETE FROM `sampleday`");
            a.b("DELETE FROM `activitySettings`");
            a.b("DELETE FROM `activityRecommendedGoals`");
            a.b("DELETE FROM `workout_session`");
            a.b("DELETE FROM `fitness_data`");
            a.b("DELETE FROM `heart_rate_sample`");
            a.b("DELETE FROM `daily_heart_rate_summary`");
            a.b("DELETE FROM `activity_statistic`");
            FitnessDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            FitnessDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"sampleraw", ActivitySample.TABLE_NAME, "sampleday", "activitySettings", "activityRecommendedGoals", "workout_session", "fitness_data", "heart_rate_sample", "daily_heart_rate_summary", ActivityStatistic.TABLE_NAME});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(21), "9c43c28ab51a9a7a22d2899f05515d5a", "62fbf7a454b61c101100bb808cb34c36");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public FitnessDataDao getFitnessDataDao() {
        FitnessDataDao fitnessDataDao;
        if (this._fitnessDataDao != null) {
            return this._fitnessDataDao;
        }
        synchronized (this) {
            if (this._fitnessDataDao == null) {
                this._fitnessDataDao = new FitnessDataDao_Impl(this);
            }
            fitnessDataDao = this._fitnessDataDao;
        }
        return fitnessDataDao;
    }

    @DexIgnore
    public HeartRateDailySummaryDao getHeartRateDailySummaryDao() {
        HeartRateDailySummaryDao heartRateDailySummaryDao;
        if (this._heartRateDailySummaryDao != null) {
            return this._heartRateDailySummaryDao;
        }
        synchronized (this) {
            if (this._heartRateDailySummaryDao == null) {
                this._heartRateDailySummaryDao = new HeartRateDailySummaryDao_Impl(this);
            }
            heartRateDailySummaryDao = this._heartRateDailySummaryDao;
        }
        return heartRateDailySummaryDao;
    }

    @DexIgnore
    public HeartRateSampleDao getHeartRateDao() {
        HeartRateSampleDao heartRateSampleDao;
        if (this._heartRateSampleDao != null) {
            return this._heartRateSampleDao;
        }
        synchronized (this) {
            if (this._heartRateSampleDao == null) {
                this._heartRateSampleDao = new HeartRateSampleDao_Impl(this);
            }
            heartRateSampleDao = this._heartRateSampleDao;
        }
        return heartRateSampleDao;
    }

    @DexIgnore
    public WorkoutDao getWorkoutDao() {
        WorkoutDao workoutDao;
        if (this._workoutDao != null) {
            return this._workoutDao;
        }
        synchronized (this) {
            if (this._workoutDao == null) {
                this._workoutDao = new WorkoutDao_Impl(this);
            }
            workoutDao = this._workoutDao;
        }
        return workoutDao;
    }

    @DexIgnore
    public SampleRawDao sampleRawDao() {
        SampleRawDao sampleRawDao;
        if (this._sampleRawDao != null) {
            return this._sampleRawDao;
        }
        synchronized (this) {
            if (this._sampleRawDao == null) {
                this._sampleRawDao = new SampleRawDao_Impl(this);
            }
            sampleRawDao = this._sampleRawDao;
        }
        return sampleRawDao;
    }
}
