package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingListLoader_Factory implements Factory<MicroAppSettingListLoader> {
    @DexIgnore
    public /* final */ Provider<Context> contextProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppSettingRepository> microAppSettingRepositoryProvider;

    @DexIgnore
    public MicroAppSettingListLoader_Factory(Provider<Context> provider, Provider<MicroAppSettingRepository> provider2) {
        this.contextProvider = provider;
        this.microAppSettingRepositoryProvider = provider2;
    }

    @DexIgnore
    public static MicroAppSettingListLoader_Factory create(Provider<Context> provider, Provider<MicroAppSettingRepository> provider2) {
        return new MicroAppSettingListLoader_Factory(provider, provider2);
    }

    @DexIgnore
    public static MicroAppSettingListLoader newMicroAppSettingListLoader(Context context, MicroAppSettingRepository microAppSettingRepository) {
        return new MicroAppSettingListLoader(context, microAppSettingRepository);
    }

    @DexIgnore
    public static MicroAppSettingListLoader provideInstance(Provider<Context> provider, Provider<MicroAppSettingRepository> provider2) {
        return new MicroAppSettingListLoader(provider.get(), provider2.get());
    }

    @DexIgnore
    public MicroAppSettingListLoader get() {
        return provideInstance(this.contextProvider, this.microAppSettingRepositoryProvider);
    }
}
