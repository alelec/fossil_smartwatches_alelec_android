package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideSleepDatabaseFactory implements Factory<SleepDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideSleepDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideSleepDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideSleepDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static SleepDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideSleepDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static SleepDatabase proxyProvideSleepDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        SleepDatabase provideSleepDatabase = portfolioDatabaseModule.provideSleepDatabase(portfolioApp);
        z76.a(provideSleepDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideSleepDatabase;
    }

    @DexIgnore
    public SleepDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
