package com.portfolio.platform.data.source.local.thirdparty;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GFitSampleDao {
    @DexIgnore
    void clearAll();

    @DexIgnore
    void deleteListGFitSample(List<GFitSample> list);

    @DexIgnore
    List<GFitSample> getAllGFitSample();

    @DexIgnore
    void insertGFitSample(GFitSample gFitSample);

    @DexIgnore
    void insertListGFitSample(List<GFitSample> list);
}
