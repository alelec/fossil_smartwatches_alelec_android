package com.portfolio.platform.data.source;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.lifecycle.LiveData;
import com.fossil.ai4;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.lw4;
import com.fossil.nc6;
import com.fossil.tu6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ FileRepository fileRepository;
    @DexIgnore
    public /* final */ WatchFaceDao watchFaceDao;
    @DexIgnore
    public /* final */ WatchFaceRemoteDataSource watchFaceRemoteDataSource;

    @DexIgnore
    public WatchFaceRepository(Context context2, WatchFaceDao watchFaceDao2, WatchFaceRemoteDataSource watchFaceRemoteDataSource2, FileRepository fileRepository2) {
        wg6.b(context2, "context");
        wg6.b(watchFaceDao2, "watchFaceDao");
        wg6.b(watchFaceRemoteDataSource2, "watchFaceRemoteDataSource");
        wg6.b(fileRepository2, "fileRepository");
        this.context = context2;
        this.watchFaceDao = watchFaceDao2;
        this.watchFaceRemoteDataSource = watchFaceRemoteDataSource2;
        this.fileRepository = fileRepository2;
        String simpleName = WatchFaceRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchFaceRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final String buildPhotoName() {
        FLogger.INSTANCE.getLocal().d(this.TAG, "buildPhotoName()");
        String latestWatchFaceName = this.watchFaceDao.getLatestWatchFaceName(ai4.PHOTO.getValue());
        int i = 1;
        if (!(latestWatchFaceName == null || latestWatchFaceName.length() == 0)) {
            try {
                i = 1 + Integer.parseInt(latestWatchFaceName);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.TAG;
                local.d(str, "buildPhotoName() name=" + i);
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(this.TAG, e.getMessage());
            }
        }
        return String.valueOf(i);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(this.TAG, "cleanUp()");
        for (WatchFace id : this.watchFaceDao.getAllWatchFaces()) {
            String id2 = id.getId();
            StringBuilder sb = new StringBuilder();
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            sb.append(applicationContext.getFilesDir());
            sb.append(File.separator);
            deleteBackgroundPhoto(sb.toString(), id2 + Constants.PHOTO_IMAGE_NAME_SUFFIX, id2 + Constants.PHOTO_BINARY_NAME_SUFFIX);
        }
        this.watchFaceDao.deleteAll();
    }

    @DexIgnore
    public final void deleteBackgroundPhoto(String str, String str2, String str3) {
        wg6.b(str, "directory");
        wg6.b(str2, "imageName");
        wg6.b(str3, "binaryName");
        FLogger.INSTANCE.getLocal().d(this.TAG, "deleteBackgroundPhoto()");
        if (new File(str).exists()) {
            new File(str + str2).deleteOnExit();
            new File(str + str3).deleteOnExit();
            FLogger.INSTANCE.getLocal().d(this.TAG, "deleteBackgroundPhoto - success");
        }
    }

    @DexIgnore
    public final void deleteWatchFace(String str, ai4 ai4) {
        wg6.b(str, "id");
        wg6.b(ai4, "type");
        this.watchFaceDao.deleteWatchFace(str, ai4.getValue());
    }

    @DexIgnore
    public final void deleteWatchFacesWithSerial(String str) {
        wg6.b(str, "serial");
        this.watchFaceDao.deleteWatchFacesWithSerial(str);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    public final WatchFace getWatchFaceWithId(String str) {
        wg6.b(str, "id");
        return this.watchFaceDao.getWatchFaceWithId(str);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.RingStyleItem>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.RingStyleItem>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: java.util.ArrayList<com.portfolio.platform.data.model.diana.preset.RingStyleItem>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v14, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r18v7, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v18, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v20, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x01ec, code lost:
        r7 = (com.fossil.ap4) r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x01f1, code lost:
        if ((r7 instanceof com.fossil.cp4) == false) goto L_0x04ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x01f3, code lost:
        r1 = (com.fossil.cp4) r7;
        r5 = r1.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x01fd, code lost:
        if (r5 == null) goto L_0x04fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x01ff, code lost:
        r2 = r9.getWatchFacesWithType(r8, com.fossil.ai4.BACKGROUND);
        r6 = new java.util.ArrayList(com.fossil.rd6.a(r5, 10));
        r10 = r5.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0218, code lost:
        if (r10.hasNext() == false) goto L_0x0229;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x021a, code lost:
        ((com.portfolio.platform.data.model.diana.preset.WatchFace) r10.next()).setSerial(r8);
        r6.add(com.fossil.cd6.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0229, code lost:
        r6 = r5.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0231, code lost:
        if (r6.hasNext() == false) goto L_0x0243;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0233, code lost:
        ((com.portfolio.platform.data.model.diana.preset.WatchFace) r6.next()).setWatchFaceType(com.fossil.ai4.BACKGROUND.getValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0243, code lost:
        r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r10 = r9.TAG;
        r6.d(r10, "getWatchFacesFromServer isFromCache " + r1.b() + " localSize " + r2.size() + " serverSize " + r5.size());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x027f, code lost:
        if (r1.b() == false) goto L_0x028e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0289, code lost:
        if (r2.size() == r5.size()) goto L_0x028c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x028c, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x028e, code lost:
        r9.watchFaceDao.deleteAllBackgroundWatchface();
        r9.watchFaceDao.insertAllWatchFaces(r5);
        r1 = com.portfolio.platform.PortfolioApp.get.instance().l();
        r3.L$0 = r9;
        r3.L$1 = r8;
        r3.L$2 = r7;
        r3.L$3 = r5;
        r3.L$4 = r5;
        r3.L$5 = r2;
        r3.label = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x02b5, code lost:
        if (r1.a(r8, r3) != r4) goto L_0x028c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x02b7, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x02b8, code lost:
        r6 = r9.context.getFilesDir().toString();
        com.fossil.wg6.a((java.lang.Object) r6, "context.filesDir.toString()");
        r10 = new java.util.ArrayList();
        r12 = r5;
        r13 = r7;
        r14 = r8;
        r15 = r9;
        r7 = r5.iterator();
        r11 = r1;
        r8 = r3;
        r5 = r4;
        r9 = r6;
        r6 = r10;
        r10 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x02df, code lost:
        if (r7.hasNext() == false) goto L_0x04fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x02e1, code lost:
        r1 = (com.portfolio.platform.data.model.diana.preset.WatchFace) r7.next();
        r4 = r1.component3();
        r3 = r1.component4();
        r2 = r1.component5();
        r1 = r15.fileRepository;
        r16 = r3.getData().getUrl();
        r8.L$0 = r15;
        r8.L$1 = r14;
        r8.L$2 = r13;
        r8.L$3 = r12;
        r8.L$4 = r11;
        r8.L$5 = r10;
        r8.L$6 = r9;
        r8.L$7 = r6;
        r8.L$8 = r7;
        r8.L$9 = r4;
        r8.L$10 = r3;
        r8.L$11 = r2;
        r8.label = 3;
        r0 = r2;
        r2 = r16;
        r16 = r3;
        r17 = r4;
        r18 = r6;
        r4 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0336, code lost:
        if (com.portfolio.platform.data.source.FileRepository.downloadFromURL$default(r1, r2, (java.lang.String) null, r8, 2, (java.lang.Object) null) != r4) goto L_0x0339;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0338, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0339, code lost:
        r3 = r8;
        r2 = r9;
        r1 = r10;
        r5 = r16;
        r10 = r17;
        r9 = r0;
        r8 = r4;
        r0 = r11;
        r4 = r18;
        r11 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0346, code lost:
        r6 = r15.fileRepository;
        r7 = r5.getData().getPreviewUrl();
        r3.L$0 = r15;
        r3.L$1 = r14;
        r3.L$2 = r13;
        r3.L$3 = r12;
        r3.L$4 = r0;
        r3.L$5 = r1;
        r3.L$6 = r2;
        r3.L$7 = r4;
        r3.L$8 = r11;
        r3.L$9 = r10;
        r3.L$10 = r5;
        r3.L$11 = r9;
        r19 = r0;
        r3.label = 4;
        r0 = r8;
        r16 = r9;
        r20 = r10;
        r17 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0385, code lost:
        if (com.portfolio.platform.data.source.FileRepository.downloadFromURL$default(r6, r7, (java.lang.String) null, r3, 2, (java.lang.Object) null) != r0) goto L_0x0388;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0387, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0388, code lost:
        r10 = r1;
        r9 = r2;
        r7 = r3;
        r8 = r4;
        r3 = r5;
        r6 = r16;
        r5 = r17;
        r11 = r19;
        r4 = r20;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0396, code lost:
        r0 = r15.fileRepository;
        r7.L$0 = r15;
        r7.L$1 = r14;
        r7.L$2 = r13;
        r7.L$3 = r12;
        r7.L$4 = r11;
        r7.L$5 = r10;
        r7.L$6 = r9;
        r7.L$7 = r8;
        r7.L$8 = r5;
        r7.L$9 = r4;
        r7.L$10 = r3;
        r7.L$11 = r6;
        r7.label = 5;
        r16 = r3;
        r20 = r4;
        r17 = r5;
        r4 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x03cf, code lost:
        if (com.portfolio.platform.data.source.FileRepository.downloadFromURL$default(r0, r6, (java.lang.String) null, r7, 2, (java.lang.Object) null) != r4) goto L_0x03d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x03d1, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x03d2, code lost:
        r1 = r6;
        r3 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x03d4, code lost:
        if (r20 == null) goto L_0x04e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x03d6, code lost:
        r0 = r20.iterator();
        r2 = r4;
        r7 = r8;
        r5 = r16;
        r6 = r20;
        r4 = r1;
        r8 = r3;
        r1 = r17;
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x03e9, code lost:
        if (r0.hasNext() == false) goto L_0x04d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x03eb, code lost:
        r28 = r2;
        r2 = r0.next();
        r29 = r0;
        r0 = (com.portfolio.platform.data.model.diana.preset.RingStyleItem) r2;
        r16 = r2;
        r2 = r15.fileRepository;
        r17 = r0.getRingStyle().getData().getUrl();
        r8.L$0 = r15;
        r8.L$1 = r14;
        r8.L$2 = r13;
        r8.L$3 = r12;
        r8.L$4 = r11;
        r8.L$5 = r10;
        r8.L$6 = r9;
        r8.L$7 = r7;
        r8.L$8 = r1;
        r8.L$9 = r6;
        r8.L$10 = r5;
        r8.L$11 = r4;
        r8.L$12 = r3;
        r21 = r7;
        r7 = r29;
        r8.L$13 = r7;
        r29 = r1;
        r1 = r16;
        r8.L$14 = r1;
        r8.L$15 = r0;
        r16 = r0;
        r8.label = 6;
        r0 = r1;
        r1 = r28;
        r22 = r3;
        r3 = r17;
        r17 = r4;
        r18 = r5;
        r23 = r6;
        r19 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0453, code lost:
        if (com.portfolio.platform.data.source.FileRepository.downloadFromURL$default(r2, r3, (java.lang.String) null, r8, 2, (java.lang.Object) null) != r1) goto L_0x0456;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0455, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0456, code lost:
        r7 = r29;
        r24 = r0;
        r25 = r1;
        r1 = r15;
        r0 = r16;
        r4 = r17;
        r5 = r18;
        r16 = r19;
        r3 = r22;
        r6 = r23;
        r15 = r13;
        r13 = r11;
        r11 = r9;
        r9 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x046e, code lost:
        r2 = r1.fileRepository;
        r17 = r0.getRingStyle().getData().getPreviewUrl();
        r8.L$0 = r1;
        r8.L$1 = r14;
        r8.L$2 = r15;
        r8.L$3 = r12;
        r8.L$4 = r13;
        r8.L$5 = r10;
        r8.L$6 = r11;
        r8.L$7 = r9;
        r8.L$8 = r7;
        r8.L$9 = r6;
        r8.L$10 = r5;
        r8.L$11 = r4;
        r8.L$12 = r3;
        r21 = r1;
        r8.L$13 = r16;
        r8.L$14 = r24;
        r8.L$15 = r0;
        r8.label = 7;
        r0 = r16;
        r22 = r3;
        r3 = r17;
        r17 = r4;
        r18 = r5;
        r23 = r6;
        r1 = r7;
        r4 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x04c5, code lost:
        if (com.portfolio.platform.data.source.FileRepository.downloadFromURL$default(r2, r3, (java.lang.String) null, r8, 2, (java.lang.Object) null) != r4) goto L_0x04c8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x04c7, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x04c8, code lost:
        r2 = r4;
        r7 = r9;
        r9 = r11;
        r11 = r13;
        r13 = r15;
        r4 = r17;
        r5 = r18;
        r15 = r21;
        r3 = r22;
        r6 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x04d9, code lost:
        r21 = r7;
        r7 = r1;
        r5 = r2;
        r6 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x04e4, code lost:
        r5 = r4;
        r6 = r8;
        r7 = r17;
        r8 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x04e9, code lost:
        r0 = r27;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x04ed, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(r9.TAG, "Failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x04fc, code lost:
        return com.fossil.cd6.a;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x012b  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public final Object getWatchFacesFromServer(String str, xe6<? super cd6> xe6) {
        WatchFaceRepository$getWatchFacesFromServer$Anon1 watchFaceRepository$getWatchFacesFromServer$Anon1;
        WatchFaceRepository watchFaceRepository;
        String str2;
        ap4 ap4;
        ArrayList arrayList;
        ArrayList arrayList2;
        List<WatchFace> list;
        String str3;
        WatchFaceRepository$getWatchFacesFromServer$Anon1 watchFaceRepository$getWatchFacesFromServer$Anon12;
        List list2;
        WatchFaceRepository watchFaceRepository2;
        String str4;
        String str5 = str;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof WatchFaceRepository$getWatchFacesFromServer$Anon1) {
            watchFaceRepository$getWatchFacesFromServer$Anon1 = (WatchFaceRepository$getWatchFacesFromServer$Anon1) xe62;
            int i = watchFaceRepository$getWatchFacesFromServer$Anon1.label;
            if ((i & Integer.MIN_VALUE) != 0) {
                watchFaceRepository$getWatchFacesFromServer$Anon1.label = i - Integer.MIN_VALUE;
                Object obj = watchFaceRepository$getWatchFacesFromServer$Anon1.result;
                Object a = ff6.a();
                switch (watchFaceRepository$getWatchFacesFromServer$Anon1.label) {
                    case 0:
                        nc6.a(obj);
                        WatchFaceRemoteDataSource watchFaceRemoteDataSource2 = this.watchFaceRemoteDataSource;
                        watchFaceRepository$getWatchFacesFromServer$Anon1.L$0 = this;
                        watchFaceRepository$getWatchFacesFromServer$Anon1.L$1 = str5;
                        watchFaceRepository$getWatchFacesFromServer$Anon1.label = 1;
                        obj = watchFaceRemoteDataSource2.getWatchFacesFromServer(str5, watchFaceRepository$getWatchFacesFromServer$Anon1);
                        if (obj != a) {
                            watchFaceRepository2 = this;
                            str4 = str5;
                            break;
                        } else {
                            return a;
                        }
                    case 1:
                        nc6.a(obj);
                        str4 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        watchFaceRepository2 = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        break;
                    case 2:
                        ap4 ap42 = (ap4) watchFaceRepository$getWatchFacesFromServer$Anon1.L$2;
                        str4 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        watchFaceRepository2 = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        nc6.a(obj);
                        List<WatchFace> list3 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$5;
                        ArrayList arrayList3 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$4;
                        ArrayList arrayList4 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$3;
                        break;
                    case 3:
                        Background background = (Background) watchFaceRepository$getWatchFacesFromServer$Anon1.L$10;
                        arrayList = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$3;
                        ap4 = (ap4) watchFaceRepository$getWatchFacesFromServer$Anon1.L$2;
                        str2 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        watchFaceRepository = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        nc6.a(obj);
                        String str6 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$6;
                        ArrayList arrayList5 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$4;
                        String str7 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$11;
                        Iterator it = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$8;
                        List<WatchFace> list4 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$5;
                        Iterable iterable = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$9;
                        Object obj2 = a;
                        List list5 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$7;
                        break;
                    case 4:
                        list2 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$7;
                        str3 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$6;
                        list = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$5;
                        arrayList2 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$4;
                        arrayList = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$3;
                        ap4 = (ap4) watchFaceRepository$getWatchFacesFromServer$Anon1.L$2;
                        str2 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        watchFaceRepository = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        nc6.a(obj);
                        Object obj3 = a;
                        Iterable iterable2 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$9;
                        String str8 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$11;
                        Iterator it2 = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$8;
                        WatchFaceRepository$getWatchFacesFromServer$Anon1 watchFaceRepository$getWatchFacesFromServer$Anon13 = watchFaceRepository$getWatchFacesFromServer$Anon1;
                        Background background2 = (Background) watchFaceRepository$getWatchFacesFromServer$Anon1.L$10;
                        Iterator it3 = it2;
                        break;
                    case 5:
                        String str9 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$11;
                        list2 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$7;
                        str3 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$6;
                        list = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$5;
                        arrayList2 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$4;
                        arrayList = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$3;
                        ap4 = (ap4) watchFaceRepository$getWatchFacesFromServer$Anon1.L$2;
                        str2 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        watchFaceRepository = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        nc6.a(obj);
                        Background background3 = (Background) watchFaceRepository$getWatchFacesFromServer$Anon1.L$10;
                        Iterable iterable3 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$9;
                        Iterator it4 = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$8;
                        break;
                    case 6:
                        T t = watchFaceRepository$getWatchFacesFromServer$Anon1.L$14;
                        RingStyleItem ringStyleItem = (RingStyleItem) watchFaceRepository$getWatchFacesFromServer$Anon1.L$15;
                        ArrayList arrayList6 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$3;
                        ap4 ap43 = (ap4) watchFaceRepository$getWatchFacesFromServer$Anon1.L$2;
                        String str10 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        WatchFaceRepository watchFaceRepository3 = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        nc6.a(obj);
                        RingStyleItem ringStyleItem2 = ringStyleItem;
                        Object obj4 = a;
                        T t2 = t;
                        String str11 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$11;
                        Background background4 = (Background) watchFaceRepository$getWatchFacesFromServer$Anon1.L$10;
                        List list6 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$7;
                        arrayList = arrayList6;
                        watchFaceRepository$getWatchFacesFromServer$Anon12 = watchFaceRepository$getWatchFacesFromServer$Anon1;
                        Iterable iterable4 = (Iterable) watchFaceRepository$getWatchFacesFromServer$Anon1.L$12;
                        Iterator it5 = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$8;
                        String str12 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$6;
                        ArrayList arrayList7 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$4;
                        ap4 ap44 = ap43;
                        Iterator<T> it6 = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$13;
                        Iterable iterable5 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$9;
                        list = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$5;
                        str2 = str10;
                        break;
                    case 7:
                        RingStyleItem ringStyleItem3 = (RingStyleItem) watchFaceRepository$getWatchFacesFromServer$Anon1.L$15;
                        Object obj5 = watchFaceRepository$getWatchFacesFromServer$Anon1.L$14;
                        Iterator<T> it7 = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$13;
                        String str13 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$1;
                        nc6.a(obj);
                        Iterator<T> it8 = it7;
                        Object obj6 = a;
                        String str14 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$11;
                        Iterable iterable6 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$9;
                        watchFaceRepository$getWatchFacesFromServer$Anon12 = watchFaceRepository$getWatchFacesFromServer$Anon1;
                        Iterable iterable7 = (Iterable) watchFaceRepository$getWatchFacesFromServer$Anon1.L$12;
                        Background background5 = (Background) watchFaceRepository$getWatchFacesFromServer$Anon1.L$10;
                        List list7 = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$7;
                        list = (List) watchFaceRepository$getWatchFacesFromServer$Anon1.L$5;
                        arrayList = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$3;
                        str2 = str13;
                        ap4 ap45 = (ap4) watchFaceRepository$getWatchFacesFromServer$Anon1.L$2;
                        watchFaceRepository = (WatchFaceRepository) watchFaceRepository$getWatchFacesFromServer$Anon1.L$0;
                        Iterator it9 = (Iterator) watchFaceRepository$getWatchFacesFromServer$Anon1.L$8;
                        str3 = (String) watchFaceRepository$getWatchFacesFromServer$Anon1.L$6;
                        arrayList2 = (ArrayList) watchFaceRepository$getWatchFacesFromServer$Anon1.L$4;
                        ap4 = ap45;
                        break;
                    default:
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }
        watchFaceRepository$getWatchFacesFromServer$Anon1 = new WatchFaceRepository$getWatchFacesFromServer$Anon1(this, xe62);
        Object obj7 = watchFaceRepository$getWatchFacesFromServer$Anon1.result;
        Object a2 = ff6.a();
        switch (watchFaceRepository$getWatchFacesFromServer$Anon1.label) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                break;
            case 7:
                break;
        }
    }

    @DexIgnore
    public final LiveData<List<WatchFace>> getWatchFacesLiveDataWithSerial(String str) {
        wg6.b(str, "serial");
        return this.watchFaceDao.getWatchFacesLiveData(str);
    }

    @DexIgnore
    public final LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, ai4 ai4) {
        wg6.b(str, "serial");
        wg6.b(ai4, "type");
        return this.watchFaceDao.getWatchFacesLiveDataWithType(str, ai4.getValue());
    }

    @DexIgnore
    public final List<WatchFace> getWatchFacesWithSerial(String str) {
        wg6.b(str, "serial");
        return this.watchFaceDao.getWatchFacesWithSerial(str);
    }

    @DexIgnore
    public final List<WatchFace> getWatchFacesWithType(String str, ai4 ai4) {
        wg6.b(str, "serial");
        wg6.b(ai4, "type");
        return this.watchFaceDao.getWatchFacesWithType(str, ai4.getValue());
    }

    @DexIgnore
    public final void insertWatchFace(String str, String str2, String str3, ArrayList<RingStyleItem> arrayList) {
        wg6.b(str, "directory");
        wg6.b(str2, "fileName");
        wg6.b(str3, "binaryName");
        FLogger.INSTANCE.getLocal().d(this.TAG, "insertWatchFace()");
        String str4 = str + File.separator + str2;
        this.watchFaceDao.insertWatchFace(new WatchFace(str2, "", arrayList, new Background(str2, new Data(str4, str + File.separator + str3)), str4, PortfolioApp.get.instance().e(), ai4.PHOTO.getValue()));
    }

    @DexIgnore
    public final void saveBackgroundPhoto(Context context2, String str, String str2, Bitmap bitmap, String str3, byte[] bArr) {
        wg6.b(context2, "context");
        wg6.b(str, "directory");
        wg6.b(str2, "fileName");
        wg6.b(bitmap, "bitmap");
        wg6.b(str3, "dataName");
        wg6.b(bArr, "data");
        FLogger.INSTANCE.getLocal().d(this.TAG, "saveBackgroundPhoto()");
        if (new File(str).exists()) {
            tu6.a(new File(str + str3), bArr);
            lw4.a(context2, str, str2, bitmap);
        }
    }
}
