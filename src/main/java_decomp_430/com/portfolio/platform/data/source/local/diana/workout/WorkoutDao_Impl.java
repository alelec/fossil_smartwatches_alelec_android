package com.portfolio.platform.data.source.local.diana.workout;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.ci4;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.v44;
import com.fossil.vh;
import com.fossil.x34;
import com.fossil.z34;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDao_Impl implements WorkoutDao {
    @DexIgnore
    public /* final */ x34 __dateShortStringConverter; // = new x34();
    @DexIgnore
    public /* final */ z34 __dateTimeISOStringConverter; // = new z34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<WorkoutSession> __insertionAdapterOfWorkoutSession;
    @DexIgnore
    public /* final */ hh<WorkoutSession> __insertionAdapterOfWorkoutSession_1;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllWorkoutSession;
    @DexIgnore
    public /* final */ v44 __workoutTypeConverter; // = new v44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<WorkoutSession> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `workout_session` (`speed`,`location`,`states`,`id`,`date`,`startTime`,`endTime`,`deviceSerialNumber`,`step`,`calorie`,`distance`,`heartRate`,`sourceType`,`workoutType`,`timezoneOffset`,`duration`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, WorkoutSession workoutSession) {
            String a = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSpeed());
            if (a == null) {
                miVar.a(1);
            } else {
                miVar.a(1, a);
            }
            String a2 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getLocation());
            if (a2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, a2);
            }
            String a3 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getStates());
            if (a3 == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a3);
            }
            if (workoutSession.getId() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, workoutSession.getId());
            }
            String a4 = WorkoutDao_Impl.this.__dateShortStringConverter.a(workoutSession.getDate());
            if (a4 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a4);
            }
            String a5 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getStartTime());
            if (a5 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a5);
            }
            String a6 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEndTime());
            if (a6 == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a6);
            }
            if (workoutSession.getDeviceSerialNumber() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, workoutSession.getDeviceSerialNumber());
            }
            String a7 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getStep());
            if (a7 == null) {
                miVar.a(9);
            } else {
                miVar.a(9, a7);
            }
            String a8 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getCalorie());
            if (a8 == null) {
                miVar.a(10);
            } else {
                miVar.a(10, a8);
            }
            String a9 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getDistance());
            if (a9 == null) {
                miVar.a(11);
            } else {
                miVar.a(11, a9);
            }
            String a10 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getHeartRate());
            if (a10 == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a10);
            }
            String a11 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSourceType());
            if (a11 == null) {
                miVar.a(13);
            } else {
                miVar.a(13, a11);
            }
            String a12 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutType());
            if (a12 == null) {
                miVar.a(14);
            } else {
                miVar.a(14, a12);
            }
            miVar.a(15, (long) workoutSession.getTimezoneOffsetInSecond());
            miVar.a(16, (long) workoutSession.getDuration());
            miVar.a(17, workoutSession.getCreatedAt());
            miVar.a(18, workoutSession.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<WorkoutSession> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `workout_session` (`speed`,`location`,`states`,`id`,`date`,`startTime`,`endTime`,`deviceSerialNumber`,`step`,`calorie`,`distance`,`heartRate`,`sourceType`,`workoutType`,`timezoneOffset`,`duration`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, WorkoutSession workoutSession) {
            String a = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSpeed());
            if (a == null) {
                miVar.a(1);
            } else {
                miVar.a(1, a);
            }
            String a2 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getLocation());
            if (a2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, a2);
            }
            String a3 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getStates());
            if (a3 == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a3);
            }
            if (workoutSession.getId() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, workoutSession.getId());
            }
            String a4 = WorkoutDao_Impl.this.__dateShortStringConverter.a(workoutSession.getDate());
            if (a4 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a4);
            }
            String a5 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getStartTime());
            if (a5 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a5);
            }
            String a6 = WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(workoutSession.getEndTime());
            if (a6 == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a6);
            }
            if (workoutSession.getDeviceSerialNumber() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, workoutSession.getDeviceSerialNumber());
            }
            String a7 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getStep());
            if (a7 == null) {
                miVar.a(9);
            } else {
                miVar.a(9, a7);
            }
            String a8 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getCalorie());
            if (a8 == null) {
                miVar.a(10);
            } else {
                miVar.a(10, a8);
            }
            String a9 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getDistance());
            if (a9 == null) {
                miVar.a(11);
            } else {
                miVar.a(11, a9);
            }
            String a10 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getHeartRate());
            if (a10 == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a10);
            }
            String a11 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getSourceType());
            if (a11 == null) {
                miVar.a(13);
            } else {
                miVar.a(13, a11);
            }
            String a12 = WorkoutDao_Impl.this.__workoutTypeConverter.a(workoutSession.getWorkoutType());
            if (a12 == null) {
                miVar.a(14);
            } else {
                miVar.a(14, a12);
            }
            miVar.a(15, (long) workoutSession.getTimezoneOffsetInSecond());
            miVar.a(16, (long) workoutSession.getDuration());
            miVar.a(17, workoutSession.getCreatedAt());
            miVar.a(18, workoutSession.getUpdatedAt());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workout_session";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WorkoutSession> call() throws Exception {
            Cursor a = bi.a(WorkoutDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "speed");
                int b2 = ai.b(a, "location");
                int b3 = ai.b(a, "states");
                int b4 = ai.b(a, "id");
                int b5 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b6 = ai.b(a, "startTime");
                int b7 = ai.b(a, "endTime");
                int b8 = ai.b(a, "deviceSerialNumber");
                int b9 = ai.b(a, "step");
                int b10 = ai.b(a, "calorie");
                int b11 = ai.b(a, "distance");
                int b12 = ai.b(a, "heartRate");
                int b13 = ai.b(a, "sourceType");
                int b14 = ai.b(a, "workoutType");
                int i = b3;
                int b15 = ai.b(a, "timezoneOffset");
                int i2 = b2;
                int b16 = ai.b(a, "duration");
                int i3 = b;
                int b17 = ai.b(a, "createdAt");
                int b18 = ai.b(a, "updatedAt");
                int i4 = b16;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i5 = b4;
                    int i6 = i4;
                    int i7 = b17;
                    i4 = i6;
                    int i8 = b18;
                    b18 = i8;
                    WorkoutSession workoutSession = new WorkoutSession(a.getString(b4), WorkoutDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7)), a.getString(b8), WorkoutDao_Impl.this.__workoutTypeConverter.h(a.getString(b9)), WorkoutDao_Impl.this.__workoutTypeConverter.a(a.getString(b10)), WorkoutDao_Impl.this.__workoutTypeConverter.b(a.getString(b11)), WorkoutDao_Impl.this.__workoutTypeConverter.c(a.getString(b12)), WorkoutDao_Impl.this.__workoutTypeConverter.e(a.getString(b13)), WorkoutDao_Impl.this.__workoutTypeConverter.i(a.getString(b14)), a.getInt(b15), a.getInt(i6), a.getLong(i7), a.getLong(i8));
                    int i9 = b15;
                    int i10 = i3;
                    int i11 = i7;
                    int i12 = i10;
                    workoutSession.setSpeed(WorkoutDao_Impl.this.__workoutTypeConverter.f(a.getString(i10)));
                    int i13 = i2;
                    i2 = i13;
                    workoutSession.setLocation(WorkoutDao_Impl.this.__workoutTypeConverter.d(a.getString(i13)));
                    int i14 = i;
                    i = i14;
                    workoutSession.setStates(WorkoutDao_Impl.this.__workoutTypeConverter.g(a.getString(i14)));
                    arrayList.add(workoutSession);
                    b15 = i9;
                    b4 = i5;
                    b17 = i11;
                    i3 = i12;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<List<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon5(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WorkoutSession> call() throws Exception {
            Cursor a = bi.a(WorkoutDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "speed");
                int b2 = ai.b(a, "location");
                int b3 = ai.b(a, "states");
                int b4 = ai.b(a, "id");
                int b5 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b6 = ai.b(a, "startTime");
                int b7 = ai.b(a, "endTime");
                int b8 = ai.b(a, "deviceSerialNumber");
                int b9 = ai.b(a, "step");
                int b10 = ai.b(a, "calorie");
                int b11 = ai.b(a, "distance");
                int b12 = ai.b(a, "heartRate");
                int b13 = ai.b(a, "sourceType");
                int b14 = ai.b(a, "workoutType");
                int i = b3;
                int b15 = ai.b(a, "timezoneOffset");
                int i2 = b2;
                int b16 = ai.b(a, "duration");
                int i3 = b;
                int b17 = ai.b(a, "createdAt");
                int b18 = ai.b(a, "updatedAt");
                int i4 = b16;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i5 = b4;
                    int i6 = i4;
                    int i7 = b17;
                    i4 = i6;
                    int i8 = b18;
                    b18 = i8;
                    WorkoutSession workoutSession = new WorkoutSession(a.getString(b4), WorkoutDao_Impl.this.__dateShortStringConverter.a(a.getString(b5)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6)), WorkoutDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7)), a.getString(b8), WorkoutDao_Impl.this.__workoutTypeConverter.h(a.getString(b9)), WorkoutDao_Impl.this.__workoutTypeConverter.a(a.getString(b10)), WorkoutDao_Impl.this.__workoutTypeConverter.b(a.getString(b11)), WorkoutDao_Impl.this.__workoutTypeConverter.c(a.getString(b12)), WorkoutDao_Impl.this.__workoutTypeConverter.e(a.getString(b13)), WorkoutDao_Impl.this.__workoutTypeConverter.i(a.getString(b14)), a.getInt(b15), a.getInt(i6), a.getLong(i7), a.getLong(i8));
                    int i9 = b15;
                    int i10 = i3;
                    int i11 = i7;
                    int i12 = i10;
                    workoutSession.setSpeed(WorkoutDao_Impl.this.__workoutTypeConverter.f(a.getString(i10)));
                    int i13 = i2;
                    i2 = i13;
                    workoutSession.setLocation(WorkoutDao_Impl.this.__workoutTypeConverter.d(a.getString(i13)));
                    int i14 = i;
                    i = i14;
                    workoutSession.setStates(WorkoutDao_Impl.this.__workoutTypeConverter.g(a.getString(i14)));
                    arrayList.add(workoutSession);
                    b15 = i9;
                    b4 = i5;
                    b17 = i11;
                    i3 = i12;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WorkoutDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfWorkoutSession = new Anon1(ohVar);
        this.__insertionAdapterOfWorkoutSession_1 = new Anon2(ohVar);
        this.__preparedStmtOfDeleteAllWorkoutSession = new Anon3(ohVar);
    }

    @DexIgnore
    public void deleteAllWorkoutSession() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllWorkoutSession.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllWorkoutSession.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<List<WorkoutSession>> getWorkoutSessions(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"workout_session"}, false, new Anon4(b));
    }

    @DexIgnore
    public LiveData<List<WorkoutSession>> getWorkoutSessionsDesc(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM workout_session WHERE date >= ? AND date <= ? ORDER BY startTime DESC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"workout_session"}, false, new Anon5(b));
    }

    @DexIgnore
    public List<WorkoutSession> getWorkoutSessionsInDate(Date date) {
        rh rhVar;
        rh b = rh.b("SELECT * FROM workout_session WHERE date = ? ORDER BY startTime ASC", 1);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "speed");
            int b3 = ai.b(a2, "location");
            int b4 = ai.b(a2, "states");
            int b5 = ai.b(a2, "id");
            int b6 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a2, "startTime");
            int b8 = ai.b(a2, "endTime");
            int b9 = ai.b(a2, "deviceSerialNumber");
            int b10 = ai.b(a2, "step");
            int b11 = ai.b(a2, "calorie");
            int b12 = ai.b(a2, "distance");
            int b13 = ai.b(a2, "heartRate");
            int b14 = ai.b(a2, "sourceType");
            rhVar = b;
            try {
                int b15 = ai.b(a2, "workoutType");
                int i = b4;
                int b16 = ai.b(a2, "timezoneOffset");
                int i2 = b3;
                int b17 = ai.b(a2, "duration");
                int i3 = b2;
                int b18 = ai.b(a2, "createdAt");
                int b19 = ai.b(a2, "updatedAt");
                int i4 = b17;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b5);
                    int i5 = b5;
                    Date a3 = this.__dateShortStringConverter.a(a2.getString(b6));
                    DateTime a4 = this.__dateTimeISOStringConverter.a(a2.getString(b7));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a2.getString(b8));
                    String string2 = a2.getString(b9);
                    WorkoutStep h = this.__workoutTypeConverter.h(a2.getString(b10));
                    WorkoutCalorie a6 = this.__workoutTypeConverter.a(a2.getString(b11));
                    WorkoutDistance b20 = this.__workoutTypeConverter.b(a2.getString(b12));
                    WorkoutHeartRate c = this.__workoutTypeConverter.c(a2.getString(b13));
                    ci4 e = this.__workoutTypeConverter.e(a2.getString(b14));
                    String string3 = a2.getString(b15);
                    int i6 = i4;
                    int i7 = b18;
                    int i8 = b15;
                    int i9 = b19;
                    b19 = i9;
                    WorkoutSession workoutSession = new WorkoutSession(string, a3, a4, a5, string2, h, a6, b20, c, e, this.__workoutTypeConverter.i(string3), a2.getInt(b16), a2.getInt(i6), a2.getLong(i7), a2.getLong(i9));
                    i4 = i6;
                    int i10 = i3;
                    int i11 = b16;
                    int i12 = i10;
                    workoutSession.setSpeed(this.__workoutTypeConverter.f(a2.getString(i10)));
                    int i13 = i2;
                    i2 = i13;
                    workoutSession.setLocation(this.__workoutTypeConverter.d(a2.getString(i13)));
                    int i14 = i;
                    i = i14;
                    workoutSession.setStates(this.__workoutTypeConverter.g(a2.getString(i14)));
                    arrayList.add(workoutSession);
                    b16 = i11;
                    b15 = i8;
                    i3 = i12;
                    b18 = i7;
                    b5 = i5;
                }
                a2.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<WorkoutSession> getWorkoutSessionsInDateAfterDesc(Date date, long j, int i) {
        rh rhVar;
        rh b = rh.b("SELECT * FROM workout_session WHERE date = ? AND createdAt < ? ORDER BY startTime DESC limit ?", 3);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        b.a(2, j);
        b.a(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "speed");
            int b3 = ai.b(a2, "location");
            int b4 = ai.b(a2, "states");
            int b5 = ai.b(a2, "id");
            int b6 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a2, "startTime");
            int b8 = ai.b(a2, "endTime");
            int b9 = ai.b(a2, "deviceSerialNumber");
            int b10 = ai.b(a2, "step");
            int b11 = ai.b(a2, "calorie");
            int b12 = ai.b(a2, "distance");
            int b13 = ai.b(a2, "heartRate");
            int b14 = ai.b(a2, "sourceType");
            rhVar = b;
            try {
                int b15 = ai.b(a2, "workoutType");
                int i2 = b4;
                int b16 = ai.b(a2, "timezoneOffset");
                int i3 = b3;
                int b17 = ai.b(a2, "duration");
                int i4 = b2;
                int b18 = ai.b(a2, "createdAt");
                int b19 = ai.b(a2, "updatedAt");
                int i5 = b17;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b5);
                    int i6 = b5;
                    Date a3 = this.__dateShortStringConverter.a(a2.getString(b6));
                    DateTime a4 = this.__dateTimeISOStringConverter.a(a2.getString(b7));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a2.getString(b8));
                    String string2 = a2.getString(b9);
                    WorkoutStep h = this.__workoutTypeConverter.h(a2.getString(b10));
                    WorkoutCalorie a6 = this.__workoutTypeConverter.a(a2.getString(b11));
                    WorkoutDistance b20 = this.__workoutTypeConverter.b(a2.getString(b12));
                    WorkoutHeartRate c = this.__workoutTypeConverter.c(a2.getString(b13));
                    ci4 e = this.__workoutTypeConverter.e(a2.getString(b14));
                    String string3 = a2.getString(b15);
                    int i7 = i5;
                    int i8 = b18;
                    int i9 = b15;
                    int i10 = b19;
                    b19 = i10;
                    WorkoutSession workoutSession = new WorkoutSession(string, a3, a4, a5, string2, h, a6, b20, c, e, this.__workoutTypeConverter.i(string3), a2.getInt(b16), a2.getInt(i7), a2.getLong(i8), a2.getLong(i10));
                    i5 = i7;
                    int i11 = i4;
                    int i12 = b16;
                    int i13 = i11;
                    workoutSession.setSpeed(this.__workoutTypeConverter.f(a2.getString(i11)));
                    int i14 = i3;
                    i3 = i14;
                    workoutSession.setLocation(this.__workoutTypeConverter.d(a2.getString(i14)));
                    int i15 = i2;
                    i2 = i15;
                    workoutSession.setStates(this.__workoutTypeConverter.g(a2.getString(i15)));
                    arrayList.add(workoutSession);
                    b16 = i12;
                    b15 = i9;
                    b18 = i8;
                    i4 = i13;
                    b5 = i6;
                }
                a2.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<WorkoutSession> getWorkoutSessionsInDateDesc(Date date, int i) {
        rh rhVar;
        rh b = rh.b("SELECT * FROM workout_session WHERE date = ? ORDER BY startTime DESC limit ?", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        b.a(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a2, "speed");
            int b3 = ai.b(a2, "location");
            int b4 = ai.b(a2, "states");
            int b5 = ai.b(a2, "id");
            int b6 = ai.b(a2, HardwareLog.COLUMN_DATE);
            int b7 = ai.b(a2, "startTime");
            int b8 = ai.b(a2, "endTime");
            int b9 = ai.b(a2, "deviceSerialNumber");
            int b10 = ai.b(a2, "step");
            int b11 = ai.b(a2, "calorie");
            int b12 = ai.b(a2, "distance");
            int b13 = ai.b(a2, "heartRate");
            int b14 = ai.b(a2, "sourceType");
            rhVar = b;
            try {
                int b15 = ai.b(a2, "workoutType");
                int i2 = b4;
                int b16 = ai.b(a2, "timezoneOffset");
                int i3 = b3;
                int b17 = ai.b(a2, "duration");
                int i4 = b2;
                int b18 = ai.b(a2, "createdAt");
                int b19 = ai.b(a2, "updatedAt");
                int i5 = b17;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b5);
                    int i6 = b5;
                    Date a3 = this.__dateShortStringConverter.a(a2.getString(b6));
                    DateTime a4 = this.__dateTimeISOStringConverter.a(a2.getString(b7));
                    DateTime a5 = this.__dateTimeISOStringConverter.a(a2.getString(b8));
                    String string2 = a2.getString(b9);
                    WorkoutStep h = this.__workoutTypeConverter.h(a2.getString(b10));
                    WorkoutCalorie a6 = this.__workoutTypeConverter.a(a2.getString(b11));
                    WorkoutDistance b20 = this.__workoutTypeConverter.b(a2.getString(b12));
                    WorkoutHeartRate c = this.__workoutTypeConverter.c(a2.getString(b13));
                    ci4 e = this.__workoutTypeConverter.e(a2.getString(b14));
                    String string3 = a2.getString(b15);
                    int i7 = i5;
                    int i8 = b18;
                    int i9 = b15;
                    int i10 = b19;
                    b19 = i10;
                    WorkoutSession workoutSession = new WorkoutSession(string, a3, a4, a5, string2, h, a6, b20, c, e, this.__workoutTypeConverter.i(string3), a2.getInt(b16), a2.getInt(i7), a2.getLong(i8), a2.getLong(i10));
                    i5 = i7;
                    int i11 = i4;
                    int i12 = b16;
                    int i13 = i11;
                    workoutSession.setSpeed(this.__workoutTypeConverter.f(a2.getString(i11)));
                    int i14 = i3;
                    i3 = i14;
                    workoutSession.setLocation(this.__workoutTypeConverter.d(a2.getString(i14)));
                    int i15 = i2;
                    i2 = i15;
                    workoutSession.setStates(this.__workoutTypeConverter.g(a2.getString(i15)));
                    arrayList.add(workoutSession);
                    b16 = i12;
                    b15 = i9;
                    i4 = i13;
                    b18 = i8;
                    b5 = i6;
                }
                a2.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public void insertWorkoutSession(WorkoutSession workoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession.insert(workoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertListWorkoutSession(List<WorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
