package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.v3;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$getGoalTrackingDataPaging$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public static /* final */ GoalTrackingRepository$getGoalTrackingDataPaging$Anon1 INSTANCE; // = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon1();

    @DexIgnore
    public final LiveData<NetworkState> apply(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        return goalTrackingDataLocalDataSource.getMNetworkState();
    }
}
