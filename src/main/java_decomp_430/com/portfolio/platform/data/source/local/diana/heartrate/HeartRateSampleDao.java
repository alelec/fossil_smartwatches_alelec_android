package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.LiveData;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class HeartRateSampleDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSampleDao.class.getSimpleName();
        wg6.a((Object) simpleName, "HeartRateSampleDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateDailyHeartRateSample(HeartRateSample heartRateSample, HeartRateSample heartRateSample2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateDailyHeartRateSample - currentSample=" + heartRateSample + ", newSample=" + heartRateSample2);
        if (heartRateSample.getAverage() > 0.0f) {
            int minuteCount = heartRateSample.getMinuteCount() + heartRateSample2.getMinuteCount();
            heartRateSample2.setAverage(((heartRateSample2.getAverage() * ((float) heartRateSample2.getMinuteCount())) + (heartRateSample.getAverage() * ((float) heartRateSample.getMinuteCount()))) / ((float) minuteCount));
            heartRateSample2.setMinuteCount(minuteCount);
        }
        int min = Math.min(heartRateSample2.getMin(), heartRateSample.getMin());
        int max = Math.max(heartRateSample2.getMax(), heartRateSample.getMax());
        heartRateSample2.setDate(heartRateSample.getDate());
        heartRateSample2.setCreatedAt(heartRateSample.getCreatedAt());
        heartRateSample2.setUpdatedAt(System.currentTimeMillis());
        heartRateSample2.setStartTimeId(heartRateSample.getStartTimeId());
        heartRateSample2.setTimezoneOffsetInSecond(heartRateSample.getTimezoneOffsetInSecond());
        heartRateSample2.setMin(min);
        heartRateSample2.setMax(max);
        Resting resting = heartRateSample2.getResting();
        if (resting == null) {
            resting = heartRateSample.getResting();
        }
        heartRateSample2.setResting(resting);
    }

    @DexIgnore
    public abstract void deleteAllHeartRateSamples();

    @DexIgnore
    public abstract HeartRateSample getHeartRateSample(String str);

    @DexIgnore
    public abstract LiveData<List<HeartRateSample>> getHeartRateSamples(Date date, Date date2);

    @DexIgnore
    public abstract void insertHeartRateSample(HeartRateSample heartRateSample);

    @DexIgnore
    public final void insertHeartRateSamples(List<HeartRateSample> list) {
        wg6.b(list, "heartRateSamples");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, "insertHeartRateSamples - sampleSize=" + list.size());
        for (HeartRateSample heartRateSample : list) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "addHeartRateSample - sample=" + heartRateSample);
            HeartRateSample heartRateSample2 = getHeartRateSample(heartRateSample.getId());
            if (heartRateSample2 != null) {
                calculateDailyHeartRateSample(heartRateSample2, heartRateSample);
            } else {
                heartRateSample.setUpdatedAt(System.currentTimeMillis());
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local3.d(str3, "addHeartRateSample - after calculate sample=" + heartRateSample);
        }
        upsertHeartRateSampleList(list);
    }

    @DexIgnore
    public abstract void upsertHeartRateSampleList(List<HeartRateSample> list);
}
