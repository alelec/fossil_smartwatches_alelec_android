package com.portfolio.platform.data.source;

import com.fossil.z76;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDeviceDaoFactory implements Factory<DeviceDao> {
    @DexIgnore
    public /* final */ Provider<DeviceDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDeviceDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDeviceDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideDeviceDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DeviceDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DeviceDatabase> provider) {
        return proxyProvideDeviceDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static DeviceDao proxyProvideDeviceDao(PortfolioDatabaseModule portfolioDatabaseModule, DeviceDatabase deviceDatabase) {
        DeviceDao provideDeviceDao = portfolioDatabaseModule.provideDeviceDao(deviceDatabase);
        z76.a(provideDeviceDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideDeviceDao;
    }

    @DexIgnore
    public DeviceDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
