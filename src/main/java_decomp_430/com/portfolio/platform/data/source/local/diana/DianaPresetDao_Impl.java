package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.l44;
import com.fossil.m44;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDao_Impl implements DianaPresetDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<DianaPreset> __insertionAdapterOfDianaPreset;
    @DexIgnore
    public /* final */ hh<DianaRecommendPreset> __insertionAdapterOfDianaRecommendPreset;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearDianaPresetTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearDianaRecommendPresetTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveAllDeletePinTypePreset;
    @DexIgnore
    public /* final */ l44 __presetComplicationSettingTypeConverter; // = new l44();
    @DexIgnore
    public /* final */ m44 __presetWatchAppSettingTypeConverter; // = new m44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<DianaRecommendPreset> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaRecommendPreset` (`serialNumber`,`id`,`name`,`isDefault`,`complications`,`watchapps`,`watchFaceId`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, DianaRecommendPreset dianaRecommendPreset) {
            if (dianaRecommendPreset.getSerialNumber() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, dianaRecommendPreset.getSerialNumber());
            }
            if (dianaRecommendPreset.getId() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, dianaRecommendPreset.getId());
            }
            if (dianaRecommendPreset.getName() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, dianaRecommendPreset.getName());
            }
            miVar.a(4, dianaRecommendPreset.isDefault() ? 1 : 0);
            String a = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaRecommendPreset.getComplications());
            if (a == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a);
            }
            String a2 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaRecommendPreset.getWatchapps());
            if (a2 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a2);
            }
            if (dianaRecommendPreset.getWatchFaceId() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, dianaRecommendPreset.getWatchFaceId());
            }
            if (dianaRecommendPreset.getCreatedAt() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, dianaRecommendPreset.getCreatedAt());
            }
            if (dianaRecommendPreset.getUpdatedAt() == null) {
                miVar.a(9);
            } else {
                miVar.a(9, dianaRecommendPreset.getUpdatedAt());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<DianaPreset> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaPreset` (`createdAt`,`updatedAt`,`pinType`,`id`,`serialNumber`,`name`,`isActive`,`complications`,`watchapps`,`watchFaceId`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, DianaPreset dianaPreset) {
            if (dianaPreset.getCreatedAt() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, dianaPreset.getCreatedAt());
            }
            if (dianaPreset.getUpdatedAt() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, dianaPreset.getUpdatedAt());
            }
            miVar.a(3, (long) dianaPreset.getPinType());
            if (dianaPreset.getId() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, dianaPreset.getId());
            }
            if (dianaPreset.getSerialNumber() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, dianaPreset.getSerialNumber());
            }
            if (dianaPreset.getName() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, dianaPreset.getName());
            }
            miVar.a(7, dianaPreset.isActive() ? 1 : 0);
            String a = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaPreset.getComplications());
            if (a == null) {
                miVar.a(8);
            } else {
                miVar.a(8, a);
            }
            String a2 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaPreset.getWatchapps());
            if (a2 == null) {
                miVar.a(9);
            } else {
                miVar.a(9, a2);
            }
            if (dianaPreset.getWatchFaceId() == null) {
                miVar.a(10);
            } else {
                miVar.a(10, dianaPreset.getWatchFaceId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends vh {
        @DexIgnore
        public Anon6(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends vh {
        @DexIgnore
        public Anon7(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon8(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset = null;
            Cursor a = bi.a(DianaPresetDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "createdAt");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, "pinType");
                int b4 = ai.b(a, "id");
                int b5 = ai.b(a, "serialNumber");
                int b6 = ai.b(a, "name");
                int b7 = ai.b(a, "isActive");
                int b8 = ai.b(a, "complications");
                int b9 = ai.b(a, "watchapps");
                int b10 = ai.b(a, "watchFaceId");
                if (a.moveToFirst()) {
                    dianaPreset = new DianaPreset(a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(a.getString(b8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(a.getString(b9)), a.getString(b10));
                    dianaPreset.setCreatedAt(a.getString(b));
                    dianaPreset.setUpdatedAt(a.getString(b2));
                    dianaPreset.setPinType(a.getInt(b3));
                }
                return dianaPreset;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<List<DianaPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon9(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<DianaPreset> call() throws Exception {
            Cursor a = bi.a(DianaPresetDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "createdAt");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, "pinType");
                int b4 = ai.b(a, "id");
                int b5 = ai.b(a, "serialNumber");
                int b6 = ai.b(a, "name");
                int b7 = ai.b(a, "isActive");
                int b8 = ai.b(a, "complications");
                int b9 = ai.b(a, "watchapps");
                int b10 = ai.b(a, "watchFaceId");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    DianaPreset dianaPreset = new DianaPreset(a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(a.getString(b8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(a.getString(b9)), a.getString(b10));
                    dianaPreset.setCreatedAt(a.getString(b));
                    dianaPreset.setUpdatedAt(a.getString(b2));
                    dianaPreset.setPinType(a.getInt(b3));
                    arrayList.add(dianaPreset);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DianaPresetDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfDianaRecommendPreset = new Anon1(ohVar);
        this.__insertionAdapterOfDianaPreset = new Anon2(ohVar);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon3(ohVar);
        this.__preparedStmtOfClearDianaPresetTable = new Anon4(ohVar);
        this.__preparedStmtOfClearDianaRecommendPresetTable = new Anon5(ohVar);
        this.__preparedStmtOfDeletePreset = new Anon6(ohVar);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(ohVar);
    }

    @DexIgnore
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    public void clearDianaPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearDianaPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearDianaRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearDianaRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    public DianaPreset getActivePresetBySerial(String str) {
        DianaPreset dianaPreset;
        String str2 = str;
        rh b = rh.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "serialNumber");
            int b7 = ai.b(a, "name");
            int b8 = ai.b(a, "isActive");
            int b9 = ai.b(a, "complications");
            int b10 = ai.b(a, "watchapps");
            int b11 = ai.b(a, "watchFaceId");
            if (a.moveToFirst()) {
                dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
            } else {
                dianaPreset = null;
            }
            return dianaPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<DianaPreset> getActivePresetBySerialLiveData(String str) {
        rh b = rh.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"dianaPreset"}, false, new Anon8(b));
    }

    @DexIgnore
    public List<DianaPreset> getAllPendingPreset(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "serialNumber");
            int b7 = ai.b(a, "name");
            int b8 = ai.b(a, "isActive");
            int b9 = ai.b(a, "complications");
            int b10 = ai.b(a, "watchapps");
            int b11 = ai.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<DianaPreset> getAllPreset(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "serialNumber");
            int b7 = ai.b(a, "name");
            int b8 = ai.b(a, "isActive");
            int b9 = ai.b(a, "complications");
            int b10 = ai.b(a, "watchapps");
            int b11 = ai.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<DianaPreset>> getAllPresetAsLiveData(String str) {
        rh b = rh.b("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"dianaPreset"}, false, new Anon9(b));
    }

    @DexIgnore
    public List<DianaRecommendPreset> getDianaRecommendPresetList(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM dianaRecommendPreset WHERE serialNumber=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "serialNumber");
            int b3 = ai.b(a, "id");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "isDefault");
            int b6 = ai.b(a, "complications");
            int b7 = ai.b(a, "watchapps");
            int b8 = ai.b(a, "watchFaceId");
            int b9 = ai.b(a, "createdAt");
            int b10 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DianaRecommendPreset(a.getString(b2), a.getString(b3), a.getString(b4), a.getInt(b5) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b6)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b7)), a.getString(b8), a.getString(b9), a.getString(b10)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public DianaPreset getPresetById(String str) {
        DianaPreset dianaPreset;
        String str2 = str;
        rh b = rh.b("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "serialNumber");
            int b7 = ai.b(a, "name");
            int b8 = ai.b(a, "isActive");
            int b9 = ai.b(a, "complications");
            int b10 = ai.b(a, "watchapps");
            int b11 = ai.b(a, "watchFaceId");
            if (a.moveToFirst()) {
                dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
            } else {
                dianaPreset = null;
            }
            return dianaPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<DianaPreset> getPresetListByWatchFaceId(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM dianaPreset WHERE watchFaceId=? AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "pinType");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "serialNumber");
            int b7 = ai.b(a, "name");
            int b8 = ai.b(a, "isActive");
            int b9 = ai.b(a, "complications");
            int b10 = ai.b(a, "watchapps");
            int b11 = ai.b(a, "watchFaceId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(a.getString(b5), a.getString(b6), a.getString(b7), a.getInt(b8) != 0, this.__presetComplicationSettingTypeConverter.a(a.getString(b9)), this.__presetWatchAppSettingTypeConverter.a(a.getString(b10)), a.getString(b11));
                dianaPreset.setCreatedAt(a.getString(b2));
                dianaPreset.setUpdatedAt(a.getString(b3));
                dianaPreset.setPinType(a.getInt(b4));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    public void upsertDianaRecommendPresetList(List<DianaRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertPreset(DianaPreset dianaPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(dianaPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertPresetList(List<DianaPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
