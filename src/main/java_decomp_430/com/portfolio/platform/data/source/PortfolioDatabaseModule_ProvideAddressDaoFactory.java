package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideAddressDaoFactory implements Factory<AddressDao> {
    @DexIgnore
    public /* final */ Provider<AddressDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideAddressDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AddressDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideAddressDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AddressDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideAddressDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AddressDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AddressDatabase> provider) {
        return proxyProvideAddressDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static AddressDao proxyProvideAddressDao(PortfolioDatabaseModule portfolioDatabaseModule, AddressDatabase addressDatabase) {
        AddressDao provideAddressDao = portfolioDatabaseModule.provideAddressDao(addressDatabase);
        z76.a(provideAddressDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideAddressDao;
    }

    @DexIgnore
    public AddressDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
