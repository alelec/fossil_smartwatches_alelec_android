package com.portfolio.platform.data.source;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveData$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitHeartRate;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listMFSleepSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listUASample;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveData$Anon1(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, List list5) {
        this.this$0 = thirdPartyRepository;
        this.$listGFitSample = list;
        this.$gFitActiveTime = gFitActiveTime;
        this.$listGFitHeartRate = list2;
        this.$listGFitWorkoutSession = list3;
        this.$listUASample = list4;
        this.$listMFSleepSession = list5;
    }

    @DexIgnore
    public final void run() {
        if (!this.$listGFitSample.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitSampleDao().insertListGFitSample(this.$listGFitSample);
        }
        if (this.$gFitActiveTime != null) {
            this.this$0.getMThirdPartyDatabase().getGFitActiveTimeDao().insertGFitActiveTime(this.$gFitActiveTime);
        }
        if (!this.$listGFitHeartRate.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitHeartRateDao().insertListGFitHeartRate(this.$listGFitHeartRate);
        }
        if (!this.$listGFitWorkoutSession.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitWorkoutSessionDao().insertListGFitWorkoutSession(this.$listGFitWorkoutSession);
        }
        if (!this.$listUASample.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getUASampleDao().insertListUASample(this.$listUASample);
        }
        if (!this.$listMFSleepSession.isEmpty()) {
            this.this$0.getMThirdPartyDatabase().getGFitSleepDao().insertListGFitSleep(this.this$0.convertListMFSleepSessionToListGFitSleep(this.$listMFSleepSession));
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "listMFSleepSession.isNotEmpty");
        }
    }
}
