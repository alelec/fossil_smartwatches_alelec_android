package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataSourceFactory extends xe.b<Long, GoalTrackingData> {
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public GoalTrackingDataLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ u04 mAppExecutors;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ vk4.a mListener;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingDataLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, u04 u04, vk4.a aVar) {
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(date, "currentDate");
        wg6.b(u04, "mAppExecutors");
        wg6.b(aVar, "mListener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.currentDate = date;
        this.mAppExecutors = u04;
        this.mListener = aVar;
    }

    @DexIgnore
    public xe<Long, GoalTrackingData> create() {
        this.localDataSource = new GoalTrackingDataLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDao, this.mGoalTrackingDatabase, this.currentDate, this.mAppExecutors, this.mListener);
        this.sourceLiveData.a(this.localDataSource);
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.localDataSource;
        if (goalTrackingDataLocalDataSource != null) {
            return goalTrackingDataLocalDataSource;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingDataLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingDataLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.localDataSource = goalTrackingDataLocalDataSource;
    }
}
