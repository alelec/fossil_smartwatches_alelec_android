package com.portfolio.platform.data.source;

import com.fossil.z76;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory implements Factory<UAppSystemVersionDataSource> {
    @DexIgnore
    public /* final */ UAppSystemVersionRepositoryModule module;

    @DexIgnore
    public UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory(UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        this.module = uAppSystemVersionRepositoryModule;
    }

    @DexIgnore
    public static UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory create(UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        return new UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory(uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static UAppSystemVersionDataSource provideInstance(UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        return proxyProvideUserLocalDataSource(uAppSystemVersionRepositoryModule);
    }

    @DexIgnore
    public static UAppSystemVersionDataSource proxyProvideUserLocalDataSource(UAppSystemVersionRepositoryModule uAppSystemVersionRepositoryModule) {
        UAppSystemVersionDataSource provideUserLocalDataSource = uAppSystemVersionRepositoryModule.provideUserLocalDataSource();
        z76.a(provideUserLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideUserLocalDataSource;
    }

    @DexIgnore
    public UAppSystemVersionDataSource get() {
        return provideInstance(this.module);
    }
}
