package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$1", f = "GoalTrackingDataLocalDataSource.kt", l = {94, 97, 102}, m = "invokeSuspend")
public final class GoalTrackingDataLocalDataSource$loadData$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ vk4.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$1$1", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDataLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.$helperCallback.a();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$1$2", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ap4 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, ap4 ap4, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDataLocalDataSource$loadData$Anon1;
            this.$data = ap4;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$data, xe6);
            anon2_Level2.p$ = (il6) obj;
            return anon2_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon2_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (((zo4) this.$data).d() != null) {
                    this.this$0.$helperCallback.a(((zo4) this.$data).d());
                } else if (((zo4) this.$data).c() != null) {
                    ServerError c = ((zo4) this.$data).c();
                    vk4.b.a aVar = this.this$0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aVar.a(new Throwable(userMessage));
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDataLocalDataSource$loadData$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, int i, vk4.b.a aVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingDataLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1 = new GoalTrackingDataLocalDataSource$loadData$Anon1(this.this$0, this.$offset, this.$helperCallback, xe6);
        goalTrackingDataLocalDataSource$loadData$Anon1.p$ = (il6) obj;
        return goalTrackingDataLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((GoalTrackingDataLocalDataSource$loadData$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingDataLocalDataSource.Companion.getTAG();
            local.d(tag, "loadData currentDate=" + this.this$0.mCurrentDate + ", offset=" + this.$offset);
            GoalTrackingRepository access$getMGoalTrackingRepository$p = this.this$0.mGoalTrackingRepository;
            Date access$getMCurrentDate$p = this.this$0.mCurrentDate;
            Date access$getMCurrentDate$p2 = this.this$0.mCurrentDate;
            int i2 = this.$offset;
            this.L$0 = il6;
            this.label = 1;
            obj = GoalTrackingRepository.loadGoalTrackingDataList$default(access$getMGoalTrackingRepository$p, access$getMCurrentDate$p, access$getMCurrentDate$p2, i2, 0, this, 8, (Object) null);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2 || i == 3) {
            ap4 ap4 = (ap4) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ap4 ap42 = (ap4) obj;
        if (ap42 instanceof cp4) {
            GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$0;
            goalTrackingDataLocalDataSource.mOffset = goalTrackingDataLocalDataSource.mOffset + 100;
            cn6 c = zl6.c();
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = ap42;
            this.label = 2;
            if (gk6.a(c, anon1_Level2, this) == a) {
                return a;
            }
        } else if (ap42 instanceof zo4) {
            cn6 c2 = zl6.c();
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, ap42, (xe6) null);
            this.L$0 = il6;
            this.L$1 = ap42;
            this.label = 3;
            if (gk6.a(c2, anon2_Level2, this) == a) {
                return a;
            }
        }
        return cd6.a;
    }
}
