package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.o44;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleDao_Impl implements RingStyleDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<DianaComplicationRingStyle> __insertionAdapterOfDianaComplicationRingStyle;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllData;
    @DexIgnore
    public /* final */ o44 __ringStyleConverter; // = new o44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<DianaComplicationRingStyle> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DianaComplicationRingStyle` (`id`,`category`,`name`,`data`,`metaData`,`serial`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, DianaComplicationRingStyle dianaComplicationRingStyle) {
            if (dianaComplicationRingStyle.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, dianaComplicationRingStyle.getId());
            }
            if (dianaComplicationRingStyle.getCategory() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, dianaComplicationRingStyle.getCategory());
            }
            if (dianaComplicationRingStyle.getName() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, dianaComplicationRingStyle.getName());
            }
            String a = RingStyleDao_Impl.this.__ringStyleConverter.a(dianaComplicationRingStyle.getData());
            if (a == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a);
            }
            String a2 = RingStyleDao_Impl.this.__ringStyleConverter.a(dianaComplicationRingStyle.getMetaData());
            if (a2 == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a2);
            }
            if (dianaComplicationRingStyle.getSerial() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, dianaComplicationRingStyle.getSerial());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM DianaComplicationRingStyle";
        }
    }

    @DexIgnore
    public RingStyleDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfDianaComplicationRingStyle = new Anon1(ohVar);
        this.__preparedStmtOfClearAllData = new Anon2(ohVar);
    }

    @DexIgnore
    public void clearAllData() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllData.release(acquire);
        }
    }

    @DexIgnore
    public List<DianaComplicationRingStyle> getRingStyles() {
        rh b = rh.b("SELECT * FROM DianaComplicationRingStyle", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "category");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "data");
            int b6 = ai.b(a, "metaData");
            int b7 = ai.b(a, "serial");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new DianaComplicationRingStyle(a.getString(b2), a.getString(b3), a.getString(b4), this.__ringStyleConverter.a(a.getString(b5)), this.__ringStyleConverter.b(a.getString(b6)), a.getString(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public DianaComplicationRingStyle getRingStylesBySerial(String str) {
        DianaComplicationRingStyle dianaComplicationRingStyle;
        String str2 = str;
        rh b = rh.b("SELECT * FROM DianaComplicationRingStyle WHERE serial = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "category");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "data");
            int b6 = ai.b(a, "metaData");
            int b7 = ai.b(a, "serial");
            if (a.moveToFirst()) {
                dianaComplicationRingStyle = new DianaComplicationRingStyle(a.getString(b2), a.getString(b3), a.getString(b4), this.__ringStyleConverter.a(a.getString(b5)), this.__ringStyleConverter.b(a.getString(b6)), a.getString(b7));
            } else {
                dianaComplicationRingStyle = null;
            }
            return dianaComplicationRingStyle;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert(dianaComplicationRingStyle);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertRingStyles(List<DianaComplicationRingStyle> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
