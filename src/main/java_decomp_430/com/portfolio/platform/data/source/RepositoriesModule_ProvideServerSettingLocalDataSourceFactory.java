package com.portfolio.platform.data.source;

import com.fossil.z76;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideServerSettingLocalDataSourceFactory implements Factory<ServerSettingDataSource> {
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideServerSettingLocalDataSourceFactory(RepositoriesModule repositoriesModule) {
        this.module = repositoriesModule;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideServerSettingLocalDataSourceFactory create(RepositoriesModule repositoriesModule) {
        return new RepositoriesModule_ProvideServerSettingLocalDataSourceFactory(repositoriesModule);
    }

    @DexIgnore
    public static ServerSettingDataSource provideInstance(RepositoriesModule repositoriesModule) {
        return proxyProvideServerSettingLocalDataSource(repositoriesModule);
    }

    @DexIgnore
    public static ServerSettingDataSource proxyProvideServerSettingLocalDataSource(RepositoriesModule repositoriesModule) {
        ServerSettingDataSource provideServerSettingLocalDataSource = repositoriesModule.provideServerSettingLocalDataSource();
        z76.a(provideServerSettingLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideServerSettingLocalDataSource;
    }

    @DexIgnore
    public ServerSettingDataSource get() {
        return provideInstance(this.module);
    }
}
