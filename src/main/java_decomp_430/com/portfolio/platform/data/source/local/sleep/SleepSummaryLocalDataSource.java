package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.LiveData;
import com.fossil.af;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cp4;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.lh;
import com.fossil.ll6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wk4;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource extends af<Date, SleepSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public vk4 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = wk4.a(this.mHelper);
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public List<lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public /* final */ SleepDao mSleepDao;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends lh.c {
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = sleepSummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wg6.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            wg6.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = bk4.c(instance);
            if (bk4.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            wg6.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public SleepSummaryLocalDataSource(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wg6.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(sleepDao, "mSleepDao");
        wg6.b(sleepDatabase, "mSleepDatabase");
        wg6.b(date, "mCreatedDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        wg6.b(calendar, "key");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDao = sleepDao;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new vk4(u04.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "sleep_date", new String[]{"sleep_session"});
        this.mSleepDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = bk4.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        wg6.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (bk4.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<SleepSummary> calculateSummaries(List<SleepSummary> list) {
        int i;
        List<SleepSummary> list2 = list;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "endCalendar");
            instance.setTime(((SleepSummary) yd6.f(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar q = bk4.q(instance.getTime());
                wg6.a((Object) q, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                SleepDao sleepDao = this.mSleepDao;
                Date time = q.getTime();
                wg6.a((Object) time, "startCalendar.time");
                Date time2 = instance.getTime();
                wg6.a((Object) time2, "endCalendar.time");
                i = sleepDao.getTotalSleep(time, time2);
            } else {
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "calendar");
            instance2.setTime(((SleepSummary) yd6.d(list)).getDate());
            Calendar q2 = bk4.q(instance2.getTime());
            wg6.a((Object) q2, "DateHelper.getStartOfWeek(calendar.time)");
            q2.add(5, -1);
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            double d = 0.0d;
            for (T next : list) {
                int i5 = i3 + 1;
                if (i3 >= 0) {
                    SleepSummary sleepSummary = (SleepSummary) next;
                    if (bk4.d(sleepSummary.getDate(), q2.getTime())) {
                        MFSleepDay sleepDay = list2.get(i2).getSleepDay();
                        if (sleepDay != null) {
                            if (i4 > 1) {
                                d /= (double) i4;
                            }
                            sleepDay.setAverageSleepOfWeek(Double.valueOf(d));
                        }
                        q2.add(5, -7);
                        i2 = i3;
                        i4 = 0;
                        d = 0.0d;
                    }
                    MFSleepDay sleepDay2 = sleepSummary.getSleepDay();
                    d += (double) (sleepDay2 != null ? sleepDay2.getSleepMinutes() : 0);
                    MFSleepDay sleepDay3 = sleepSummary.getSleepDay();
                    if ((sleepDay3 != null ? sleepDay3.getSleepMinutes() : 0) > 0) {
                        i4++;
                    }
                    if (i3 == list.size() - 1) {
                        d += (double) i;
                    }
                    i3 = i5;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            MFSleepDay sleepDay4 = list2.get(i2).getSleepDay();
            if (sleepDay4 != null) {
                if (i4 > 1) {
                    d /= (double) i4;
                }
                sleepDay4.setAverageSleepOfWeek(Double.valueOf(d));
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateSummaries summaries.size=" + list.size());
        return list2;
    }

    @DexIgnore
    private final void combineData(vk4.d dVar, ap4<ku3> ap4, ap4<ku3> ap42, vk4.b.a aVar) {
        if ((ap4 instanceof cp4) && (ap42 instanceof cp4)) {
            if (dVar == vk4.d.AFTER && (!this.mRequestAfterQueue.isEmpty())) {
                this.mRequestAfterQueue.remove(0);
            }
            aVar.a();
        } else if (ap4 instanceof zo4) {
            zo4 zo4 = (zo4) ap4;
            if (zo4.d() != null) {
                aVar.a(zo4.d());
            } else if (zo4.c() != null) {
                ServerError c = zo4.c();
                String userMessage = c.getUserMessage();
                String message = userMessage != null ? userMessage : c.getMessage();
                if (message == null) {
                    message = "";
                }
                aVar.a(new Throwable(message));
            }
        } else if (ap42 instanceof zo4) {
            zo4 zo42 = (zo4) ap42;
            if (zo42.d() != null) {
                aVar.a(zo42.d());
            } else if (zo42.c() != null) {
                ServerError c2 = zo42.c();
                String userMessage2 = c2.getUserMessage();
                String message2 = userMessage2 != null ? userMessage2 : c2.getMessage();
                if (message2 == null) {
                    message2 = "";
                }
                aVar.a(new Throwable(message2));
            }
        }
    }

    @DexIgnore
    private final SleepSummary dummySummary(SleepSummary sleepSummary, Date date) {
        MFSleepDay sleepDay = sleepSummary.getSleepDay();
        return new SleepSummary(new MFSleepDay(date, sleepDay != null ? sleepDay.getGoalMinutes() : 0, 0, new SleepDistribution(0, 0, 0, 7, (qg6) null), DateTime.now(), DateTime.now()), (List) null, 2, (qg6) null);
    }

    @DexIgnore
    private final List<SleepSummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        Object obj2;
        Date date3;
        Date date4 = date;
        Date date5 = date2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getDataInDatabase - startDate=" + date4 + ", endDate=" + date5);
        List<SleepSummary> calculateSummaries = calculateSummaries(this.mSleepDao.getSleepSummariesDesc(bk4.b(date, date2) ? date5 : date4, date5));
        ArrayList arrayList = new ArrayList();
        MFSleepDay lastSleepDay = this.mSleepDao.getLastSleepDay();
        Date date6 = (lastSleepDay == null || (date3 = lastSleepDay.getDate()) == null) ? date4 : date3;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        SleepSummary sleepSummary = this.mSleepDao.getSleepSummary(date5);
        if (sleepSummary == null) {
            MFSleepDay mFSleepDay = r2;
            MFSleepDay mFSleepDay2 = new MFSleepDay(date2, this.mSleepDao.getNearestSleepGoalFromDate(date5), 0, new SleepDistribution(0, 0, 0, 7, (qg6) null), DateTime.now(), DateTime.now());
            obj = null;
            sleepSummary = new SleepSummary(mFSleepDay, (List) null, 2, (qg6) null);
        } else {
            obj = null;
        }
        if (!bk4.b(date4, date6)) {
            date4 = date6;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", summaryParent=" + sleepSummary + ", " + "lastDate=" + date6 + ", startDateToFill=" + date4);
        Date date7 = date2;
        while (bk4.c(date7, date4)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = obj;
                    break;
                }
                obj2 = it.next();
                if (bk4.d(((SleepSummary) obj2).getDate(), date7)) {
                    break;
                }
            }
            SleepSummary sleepSummary2 = (SleepSummary) obj2;
            if (sleepSummary2 == null) {
                arrayList.add(dummySummary(sleepSummary, date7));
            } else {
                arrayList.add(sleepSummary2);
                arrayList2.remove(sleepSummary2);
            }
            date7 = bk4.n(date7);
            wg6.a((Object) date7, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            SleepSummary sleepSummary3 = (SleepSummary) yd6.d(arrayList);
            Boolean t = bk4.t(sleepSummary3.getDate());
            wg6.a((Object) t, "DateHelper.isToday(todaySummary.getDate())");
            if (t.booleanValue()) {
                arrayList.add(0, sleepSummary3.copy(sleepSummary3.getSleepDay(), sleepSummary3.getSleepSessions()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final rm6 loadData(vk4.d dVar, Date date, Date date2, vk4.b.a aVar) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SleepSummaryLocalDataSource$loadData$Anon1(this, date, date2, dVar, aVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final vk4 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mSleepDatabase.getInvalidationTracker().b();
        return SleepSummaryLocalDataSource.super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(af.f<Date> fVar, af.a<Date, SleepSummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (bk4.b((Date) fVar.a, this.mCreatedDate)) {
            Object obj = fVar.a;
            wg6.a(obj, "params.key");
            Date date = (Date) obj;
            Companion companion = Companion;
            Object obj2 = fVar.a;
            wg6.a(obj2, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) obj2, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date m = bk4.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : bk4.m(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + m + ", endQueryDate=" + date);
            wg6.a((Object) m, "startQueryDate");
            aVar.a(getDataInDatabase(m, date), this.key.getTime());
            if (bk4.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new lc6(this.mStartDate, this.mEndDate));
                this.mHelper.a(vk4.d.AFTER, (vk4.b) new SleepSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(af.f<Date> fVar, af.a<Date, SleepSummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(af.e<Date> eVar, af.c<Date, SleepSummary> cVar) {
        wg6.b(eVar, "params");
        wg6.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date m = bk4.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : bk4.m(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + m + ", endQueryDate=" + date);
        wg6.a((Object) m, "startQueryDate");
        cVar.a(getDataInDatabase(m, date), (Object) null, this.key.getTime());
        this.mHelper.a(vk4.d.INITIAL, (vk4.b) new SleepSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mSleepDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(vk4 vk4) {
        wg6.b(vk4, "<set-?>");
        this.mHelper = vk4;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wg6.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
