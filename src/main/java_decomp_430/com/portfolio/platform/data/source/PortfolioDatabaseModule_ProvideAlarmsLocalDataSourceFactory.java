package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory implements Factory<AlarmsLocalDataSource> {
    @DexIgnore
    public /* final */ Provider<AlarmDao> alarmDaoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AlarmDao> provider) {
        this.module = portfolioDatabaseModule;
        this.alarmDaoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AlarmDao> provider) {
        return new PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AlarmsLocalDataSource provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AlarmDao> provider) {
        return proxyProvideAlarmsLocalDataSource(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static AlarmsLocalDataSource proxyProvideAlarmsLocalDataSource(PortfolioDatabaseModule portfolioDatabaseModule, AlarmDao alarmDao) {
        AlarmsLocalDataSource provideAlarmsLocalDataSource = portfolioDatabaseModule.provideAlarmsLocalDataSource(alarmDao);
        z76.a(provideAlarmsLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideAlarmsLocalDataSource;
    }

    @DexIgnore
    public AlarmsLocalDataSource get() {
        return provideInstance(this.module, this.alarmDaoProvider);
    }
}
