package com.portfolio.platform.data.source;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.hf6;
import com.fossil.qg6;
import com.fossil.xe6;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UserDataSource {
    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationEmailExisting$suspendImpl(UserDataSource userDataSource, String str, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationSocialExisting$suspendImpl(UserDataSource userDataSource, String str, String str2, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loadUserInfo$suspendImpl(UserDataSource userDataSource, MFUser mFUser, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginEmail$suspendImpl(UserDataSource userDataSource, String str, String str2, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginWithSocial$suspendImpl(UserDataSource userDataSource, String str, String str2, String str3, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object requestEmailOtp$suspendImpl(UserDataSource userDataSource, String str, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object resetPassword$suspendImpl(UserDataSource userDataSource, String str, xe6 xe6) {
        return new cp4(hf6.a((int) MFNetworkReturnCode.RESPONSE_OK), false, 2, (qg6) null);
    }

    @DexIgnore
    public static /* synthetic */ Object signUpEmail$suspendImpl(UserDataSource userDataSource, SignUpEmailAuth signUpEmailAuth, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpSocial$suspendImpl(UserDataSource userDataSource, SignUpSocialAuth signUpSocialAuth, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object verifyEmailOtp$suspendImpl(UserDataSource userDataSource, String str, String str2, xe6 xe6) {
        return null;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, xe6<? super ap4<Boolean>> xe6) {
        return checkAuthenticationEmailExisting$suspendImpl(this, str, xe6);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, xe6<? super ap4<Boolean>> xe6) {
        return checkAuthenticationSocialExisting$suspendImpl(this, str, str2, xe6);
    }

    @DexIgnore
    public void clearAllUser() {
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, xe6<? super Integer> xe6) {
        return hf6.a(0);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return null;
    }

    @DexIgnore
    public abstract void insertUser(MFUser mFUser);

    @DexIgnore
    public Object loadUserInfo(MFUser mFUser, xe6<? super ap4<MFUser>> xe6) {
        return loadUserInfo$suspendImpl(this, mFUser, xe6);
    }

    @DexIgnore
    public Object loginEmail(String str, String str2, xe6<? super ap4<Auth>> xe6) {
        return loginEmail$suspendImpl(this, str, str2, xe6);
    }

    @DexIgnore
    public Object loginWithSocial(String str, String str2, String str3, xe6<? super ap4<Auth>> xe6) {
        return loginWithSocial$suspendImpl(this, str, str2, str3, xe6);
    }

    @DexIgnore
    public Object logoutUser(xe6<? super Integer> xe6) {
        return hf6.a(0);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, xe6<? super ap4<Void>> xe6) {
        return requestEmailOtp$suspendImpl(this, str, xe6);
    }

    @DexIgnore
    public Object resetPassword(String str, xe6<? super ap4<Integer>> xe6) {
        return resetPassword$suspendImpl(this, str, xe6);
    }

    @DexIgnore
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, xe6<? super ap4<Auth>> xe6) {
        return signUpEmail$suspendImpl(this, signUpEmailAuth, xe6);
    }

    @DexIgnore
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, xe6<? super ap4<Auth>> xe6) {
        return signUpSocial$suspendImpl(this, signUpSocialAuth, xe6);
    }

    @DexIgnore
    public abstract Object updateUser(MFUser mFUser, boolean z, xe6<? super ap4<MFUser>> xe6);

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, xe6<? super ap4<Void>> xe6) {
        return verifyEmailOtp$suspendImpl(this, str, str2, xe6);
    }
}
