package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideComplicationDaoFactory implements Factory<ComplicationDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideComplicationDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideComplicationDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideComplicationDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static ComplicationDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return proxyProvideComplicationDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static ComplicationDao proxyProvideComplicationDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        ComplicationDao provideComplicationDao = portfolioDatabaseModule.provideComplicationDao(dianaCustomizeDatabase);
        z76.a(provideComplicationDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideComplicationDao;
    }

    @DexIgnore
    public ComplicationDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
