package com.portfolio.platform.data.source;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Device;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDao_Impl implements DeviceDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<Device> __insertionAdapterOfDevice;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveDeviceByDeviceId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<Device> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `device` (`major`,`minor`,`createdAt`,`updatedAt`,`owner`,`productDisplayName`,`manufacturer`,`softwareRevision`,`hardwareRevision`,`deviceId`,`macAddress`,`sku`,`firmwareRevision`,`batteryLevel`,`vibrationStrength`,`isActive`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, Device device) {
            miVar.a(1, (long) device.getMajor());
            miVar.a(2, (long) device.getMinor());
            if (device.getCreatedAt() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, device.getCreatedAt());
            }
            if (device.getUpdatedAt() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, device.getUpdatedAt());
            }
            if (device.getOwner() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, device.getOwner());
            }
            if (device.getProductDisplayName() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, device.getProductDisplayName());
            }
            if (device.getManufacturer() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, device.getManufacturer());
            }
            if (device.getSoftwareRevision() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, device.getSoftwareRevision());
            }
            if (device.getHardwareRevision() == null) {
                miVar.a(9);
            } else {
                miVar.a(9, device.getHardwareRevision());
            }
            if (device.getDeviceId() == null) {
                miVar.a(10);
            } else {
                miVar.a(10, device.getDeviceId());
            }
            if (device.getMacAddress() == null) {
                miVar.a(11);
            } else {
                miVar.a(11, device.getMacAddress());
            }
            if (device.getSku() == null) {
                miVar.a(12);
            } else {
                miVar.a(12, device.getSku());
            }
            if (device.getFirmwareRevision() == null) {
                miVar.a(13);
            } else {
                miVar.a(13, device.getFirmwareRevision());
            }
            miVar.a(14, (long) device.getBatteryLevel());
            if (device.getVibrationStrength() == null) {
                miVar.a(15);
            } else {
                miVar.a(15, (long) device.getVibrationStrength().intValue());
            }
            miVar.a(16, device.isActive() ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM device WHERE deviceId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM device";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<Device>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<Device> call() throws Exception {
            Integer num;
            Cursor a = bi.a(DeviceDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "major");
                int b2 = ai.b(a, "minor");
                int b3 = ai.b(a, "createdAt");
                int b4 = ai.b(a, "updatedAt");
                int b5 = ai.b(a, "owner");
                int b6 = ai.b(a, "productDisplayName");
                int b7 = ai.b(a, "manufacturer");
                int b8 = ai.b(a, "softwareRevision");
                int b9 = ai.b(a, "hardwareRevision");
                int b10 = ai.b(a, "deviceId");
                int b11 = ai.b(a, "macAddress");
                int b12 = ai.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int b13 = ai.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int b14 = ai.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int b15 = ai.b(a, "vibrationStrength");
                int i = b9;
                int b16 = ai.b(a, "isActive");
                int i2 = b8;
                int i3 = b7;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b10);
                    String string2 = a.getString(b11);
                    String string3 = a.getString(b12);
                    String string4 = a.getString(b13);
                    int i4 = a.getInt(b14);
                    if (a.isNull(b15)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b15));
                    }
                    Device device = new Device(string, string2, string3, string4, i4, num, a.getInt(b16) != 0);
                    int i5 = b15;
                    device.setMajor(a.getInt(b));
                    device.setMinor(a.getInt(b2));
                    device.setCreatedAt(a.getString(b3));
                    device.setUpdatedAt(a.getString(b4));
                    device.setOwner(a.getString(b5));
                    device.setProductDisplayName(a.getString(b6));
                    int i6 = i3;
                    int i7 = b;
                    device.setManufacturer(a.getString(i6));
                    int i8 = i2;
                    int i9 = i6;
                    device.setSoftwareRevision(a.getString(i8));
                    int i10 = i;
                    int i11 = i8;
                    device.setHardwareRevision(a.getString(i10));
                    arrayList.add(device);
                    b = i7;
                    i3 = i9;
                    i2 = i11;
                    i = i10;
                    b15 = i5;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon5(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public Device call() throws Exception {
            Device device;
            Integer num;
            Cursor a = bi.a(DeviceDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "major");
                int b2 = ai.b(a, "minor");
                int b3 = ai.b(a, "createdAt");
                int b4 = ai.b(a, "updatedAt");
                int b5 = ai.b(a, "owner");
                int b6 = ai.b(a, "productDisplayName");
                int b7 = ai.b(a, "manufacturer");
                int b8 = ai.b(a, "softwareRevision");
                int b9 = ai.b(a, "hardwareRevision");
                int b10 = ai.b(a, "deviceId");
                int b11 = ai.b(a, "macAddress");
                int b12 = ai.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int b13 = ai.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int b14 = ai.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int b15 = ai.b(a, "vibrationStrength");
                int i = b9;
                int b16 = ai.b(a, "isActive");
                if (a.moveToFirst()) {
                    String string = a.getString(b10);
                    String string2 = a.getString(b11);
                    String string3 = a.getString(b12);
                    String string4 = a.getString(b13);
                    int i2 = a.getInt(b14);
                    if (a.isNull(b15)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b15));
                    }
                    Device device2 = new Device(string, string2, string3, string4, i2, num, a.getInt(b16) != 0);
                    device2.setMajor(a.getInt(b));
                    device2.setMinor(a.getInt(b2));
                    device2.setCreatedAt(a.getString(b3));
                    device2.setUpdatedAt(a.getString(b4));
                    device2.setOwner(a.getString(b5));
                    device2.setProductDisplayName(a.getString(b6));
                    device2.setManufacturer(a.getString(b7));
                    device2.setSoftwareRevision(a.getString(b8));
                    device2.setHardwareRevision(a.getString(i));
                    device = device2;
                } else {
                    device = null;
                }
                return device;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public DeviceDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfDevice = new Anon1(ohVar);
        this.__preparedStmtOfRemoveDeviceByDeviceId = new Anon2(ohVar);
        this.__preparedStmtOfCleanUp = new Anon3(ohVar);
    }

    @DexIgnore
    public void addAllDevice(List<Device> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void addOrUpdateDevice(Device device) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(device);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public List<Device> getAllDevice() {
        rh rhVar;
        Integer num;
        rh b = rh.b("SELECT * FROM device", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "major");
            int b3 = ai.b(a, "minor");
            int b4 = ai.b(a, "createdAt");
            int b5 = ai.b(a, "updatedAt");
            int b6 = ai.b(a, "owner");
            int b7 = ai.b(a, "productDisplayName");
            int b8 = ai.b(a, "manufacturer");
            int b9 = ai.b(a, "softwareRevision");
            int b10 = ai.b(a, "hardwareRevision");
            int b11 = ai.b(a, "deviceId");
            int b12 = ai.b(a, "macAddress");
            int b13 = ai.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b14 = ai.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            int b15 = ai.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
            rhVar = b;
            try {
                int b16 = ai.b(a, "vibrationStrength");
                int i = b10;
                int b17 = ai.b(a, "isActive");
                int i2 = b9;
                int i3 = b8;
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    String string = a.getString(b11);
                    String string2 = a.getString(b12);
                    String string3 = a.getString(b13);
                    String string4 = a.getString(b14);
                    int i4 = a.getInt(b15);
                    if (a.isNull(b16)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b16));
                    }
                    Device device = new Device(string, string2, string3, string4, i4, num, a.getInt(b17) != 0);
                    int i5 = b13;
                    device.setMajor(a.getInt(b2));
                    device.setMinor(a.getInt(b3));
                    device.setCreatedAt(a.getString(b4));
                    device.setUpdatedAt(a.getString(b5));
                    device.setOwner(a.getString(b6));
                    device.setProductDisplayName(a.getString(b7));
                    int i6 = i3;
                    int i7 = b15;
                    device.setManufacturer(a.getString(i6));
                    int i8 = i2;
                    int i9 = i6;
                    device.setSoftwareRevision(a.getString(i8));
                    int i10 = i;
                    int i11 = i8;
                    device.setHardwareRevision(a.getString(i10));
                    arrayList.add(device);
                    b15 = i7;
                    i3 = i9;
                    i2 = i11;
                    i = i10;
                    b13 = i5;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<List<Device>> getAllDeviceAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"device"}, false, new Anon4(rh.b("SELECT * FROM device", 0)));
    }

    @DexIgnore
    public Device getDeviceByDeviceId(String str) {
        rh rhVar;
        Device device;
        Integer num;
        String str2 = str;
        rh b = rh.b("SELECT * FROM device WHERE deviceId=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "major");
            int b3 = ai.b(a, "minor");
            int b4 = ai.b(a, "createdAt");
            int b5 = ai.b(a, "updatedAt");
            int b6 = ai.b(a, "owner");
            int b7 = ai.b(a, "productDisplayName");
            int b8 = ai.b(a, "manufacturer");
            int b9 = ai.b(a, "softwareRevision");
            int b10 = ai.b(a, "hardwareRevision");
            int b11 = ai.b(a, "deviceId");
            int b12 = ai.b(a, "macAddress");
            int b13 = ai.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b14 = ai.b(a, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            int b15 = ai.b(a, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
            rhVar = b;
            try {
                int b16 = ai.b(a, "vibrationStrength");
                int i = b10;
                int b17 = ai.b(a, "isActive");
                if (a.moveToFirst()) {
                    String string = a.getString(b11);
                    String string2 = a.getString(b12);
                    String string3 = a.getString(b13);
                    String string4 = a.getString(b14);
                    int i2 = a.getInt(b15);
                    if (a.isNull(b16)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a.getInt(b16));
                    }
                    Device device2 = new Device(string, string2, string3, string4, i2, num, a.getInt(b17) != 0);
                    device2.setMajor(a.getInt(b2));
                    device2.setMinor(a.getInt(b3));
                    device2.setCreatedAt(a.getString(b4));
                    device2.setUpdatedAt(a.getString(b5));
                    device2.setOwner(a.getString(b6));
                    device2.setProductDisplayName(a.getString(b7));
                    device2.setManufacturer(a.getString(b8));
                    device2.setSoftwareRevision(a.getString(b9));
                    device2.setHardwareRevision(a.getString(i));
                    device = device2;
                } else {
                    device = null;
                }
                a.close();
                rhVar.c();
                return device;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        rh b = rh.b("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"device"}, false, new Anon5(b));
    }

    @DexIgnore
    public void removeDeviceByDeviceId(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveDeviceByDeviceId.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeviceByDeviceId.release(acquire);
        }
    }
}
