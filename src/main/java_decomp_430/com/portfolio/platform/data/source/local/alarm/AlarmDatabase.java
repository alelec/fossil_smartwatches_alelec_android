package com.portfolio.platform.data.source.local.alarm;

import com.fossil.oh;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xh;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class AlarmDatabase extends oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ xh MIGRATION_FROM_5_TO_6; // = new AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1(5, 6);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final xh getMIGRATION_FROM_5_TO_6() {
            return AlarmDatabase.MIGRATION_FROM_5_TO_6;
        }

        @DexIgnore
        public final xh migrating3Or4To5(String str, int i) {
            wg6.b(str, ButtonService.USER_ID);
            return new AlarmDatabase$Companion$migrating3Or4To5$Anon1(i, str, i, 5);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmDatabase.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmDatabase::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public abstract AlarmDao alarmDao();
}
