package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchLastSleepGoal$response$1", f = "SleepSummariesRepository.kt", l = {145}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchLastSleepGoal$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<MFSleepSettings>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchLastSleepGoal$response$Anon1(SleepSummariesRepository sleepSummariesRepository, xe6 xe6) {
        super(1, xe6);
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new SleepSummariesRepository$fetchLastSleepGoal$response$Anon1(this.this$0, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((SleepSummariesRepository$fetchLastSleepGoal$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            this.label = 1;
            obj = access$getMApiService$p.getSleepSetting(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
