package com.portfolio.platform.data.source;

import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1 extends xg6 implements hg6<GoalTrackingData, Boolean> {
    @DexIgnore
    public static /* final */ GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1 INSTANCE; // = new GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1();

    @DexIgnore
    public GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((GoalTrackingData) obj));
    }

    @DexIgnore
    public final boolean invoke(GoalTrackingData goalTrackingData) {
        wg6.b(goalTrackingData, "it");
        return goalTrackingData.getPinType() == 1;
    }
}
