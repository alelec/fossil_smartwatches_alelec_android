package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$deleteAlarm$2", f = "AlarmsRepository.kt", l = {}, m = "invokeSuspend")
public final class AlarmsRepository$deleteAlarm$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $alarm;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$deleteAlarm$Anon2(AlarmsRepository alarmsRepository, Alarm alarm, xe6 xe6) {
        super(2, xe6);
        this.this$0 = alarmsRepository;
        this.$alarm = alarm;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2 = new AlarmsRepository$deleteAlarm$Anon2(this.this$0, this.$alarm, xe6);
        alarmsRepository$deleteAlarm$Anon2.p$ = (il6) obj;
        return alarmsRepository$deleteAlarm$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AlarmsRepository$deleteAlarm$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.$alarm.setPinType(3);
            this.this$0.mAlarmsLocalDataSource.upsertAlarm(this.$alarm);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
