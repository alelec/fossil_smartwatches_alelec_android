package com.portfolio.platform.data.source;

import com.fossil.cd6;
import com.fossil.gg6;
import com.fossil.xg6;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSummariesPaging$Anon2 extends xg6 implements gg6<cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSummariesPaging$Anon2(SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = sleepSummaryDataSourceFactory;
    }

    @DexIgnore
    public final void invoke() {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.$sourceFactory.getSourceLiveData().a();
        if (sleepSummaryLocalDataSource != null) {
            sleepSummaryLocalDataSource.invalidate();
        }
    }
}
