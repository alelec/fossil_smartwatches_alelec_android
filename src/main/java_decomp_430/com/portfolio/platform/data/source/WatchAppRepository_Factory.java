package com.portfolio.platform.data.source;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRepository_Factory implements Factory<WatchAppRepository> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> mPortfolioAppProvider;
    @DexIgnore
    public /* final */ Provider<WatchAppDao> mWatchAppDaoProvider;
    @DexIgnore
    public /* final */ Provider<WatchAppRemoteDataSource> mWatchAppRemoteDataSourceProvider;

    @DexIgnore
    public WatchAppRepository_Factory(Provider<WatchAppDao> provider, Provider<WatchAppRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        this.mWatchAppDaoProvider = provider;
        this.mWatchAppRemoteDataSourceProvider = provider2;
        this.mPortfolioAppProvider = provider3;
    }

    @DexIgnore
    public static WatchAppRepository_Factory create(Provider<WatchAppDao> provider, Provider<WatchAppRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new WatchAppRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static WatchAppRepository newWatchAppRepository(WatchAppDao watchAppDao, WatchAppRemoteDataSource watchAppRemoteDataSource, PortfolioApp portfolioApp) {
        return new WatchAppRepository(watchAppDao, watchAppRemoteDataSource, portfolioApp);
    }

    @DexIgnore
    public static WatchAppRepository provideInstance(Provider<WatchAppDao> provider, Provider<WatchAppRemoteDataSource> provider2, Provider<PortfolioApp> provider3) {
        return new WatchAppRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public WatchAppRepository get() {
        return provideInstance(this.mWatchAppDaoProvider, this.mWatchAppRemoteDataSourceProvider, this.mPortfolioAppProvider);
    }
}
