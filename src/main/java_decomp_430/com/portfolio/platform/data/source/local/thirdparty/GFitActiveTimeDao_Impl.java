package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.c44;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitActiveTimeDao_Impl implements GFitActiveTimeDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<GFitActiveTime> __deletionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ c44 __gFitActiveTimeConverter; // = new c44();
    @DexIgnore
    public /* final */ hh<GFitActiveTime> __insertionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<GFitActiveTime> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitActiveTime` (`id`,`activeTimes`) VALUES (nullif(?, 0),?)";
        }

        @DexIgnore
        public void bind(mi miVar, GFitActiveTime gFitActiveTime) {
            miVar.a(1, (long) gFitActiveTime.getId());
            String a = GFitActiveTimeDao_Impl.this.__gFitActiveTimeConverter.a(gFitActiveTime.getActiveTimes());
            if (a == null) {
                miVar.a(2);
            } else {
                miVar.a(2, a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<GFitActiveTime> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitActiveTime` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, GFitActiveTime gFitActiveTime) {
            miVar.a(1, (long) gFitActiveTime.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM gFitActiveTime";
        }
    }

    @DexIgnore
    public GFitActiveTimeDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfGFitActiveTime = new Anon1(ohVar);
        this.__deletionAdapterOfGFitActiveTime = new Anon2(ohVar);
        this.__preparedStmtOfClearAll = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitActiveTime.handle(gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitActiveTime> getAllGFitActiveTime() {
        rh b = rh.b("SELECT * FROM gFitActiveTime", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "activeTimes");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitActiveTime gFitActiveTime = new GFitActiveTime(this.__gFitActiveTimeConverter.a(a.getString(b3)));
                gFitActiveTime.setId(a.getInt(b2));
                arrayList.add(gFitActiveTime);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert(gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitActiveTime(List<GFitActiveTime> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
