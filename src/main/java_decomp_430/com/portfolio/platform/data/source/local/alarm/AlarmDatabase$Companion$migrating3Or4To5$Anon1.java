package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import com.fossil.ii;
import com.fossil.wg6;
import com.fossil.xh;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.data.model.Explore;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase$Companion$migrating3Or4To5$Anon1 extends xh {
    @DexIgnore
    public /* final */ /* synthetic */ int $previous;
    @DexIgnore
    public /* final */ /* synthetic */ String $userId;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmDatabase$Companion$migrating3Or4To5$Anon1(int i, String str, int i2, int i3) {
        super(i2, i3);
        this.$previous = i;
        this.$userId = str;
    }

    @DexIgnore
    public void migrate(ii iiVar) {
        AlarmDatabase$Companion$migrating3Or4To5$Anon1 alarmDatabase$Companion$migrating3Or4To5$Anon1 = this;
        ii iiVar2 = iiVar;
        wg6.b(iiVar2, "database");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$cp = AlarmDatabase.TAG;
        local.d(access$getTAG$cp, "migrating3Or4To5 - previous: " + alarmDatabase$Companion$migrating3Or4To5$Anon1.$previous);
        iiVar.u();
        try {
            iiVar2.b("CREATE TABLE alarm_temp (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
            if (alarmDatabase$Companion$migrating3Or4To5$Anon1.$previous == 4) {
                iiVar2.b("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt, pinType FROM alarm");
            } else {
                iiVar2.b("INSERT INTO alarm_temp (id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) SELECT objectId, uri, alarmTitle, 0, alarmMinute, days, isActiveAlarm, isRepeat, createdAt, updatedAt FROM alarm");
            }
            iiVar2.b("UPDATE alarm_temp SET hour = minute/60, minute = minute%60");
            iiVar2.b("DROP TABLE alarm");
            iiVar2.b("ALTER TABLE alarm_temp RENAME TO alarm");
            Cursor d = iiVar2.d("SELECT*FROM alarm WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
            d.moveToFirst();
            while (true) {
                wg6.a((Object) d, "cursor");
                if (d.isAfterLast()) {
                    break;
                }
                StringBuilder sb = new StringBuilder();
                sb.append(alarmDatabase$Companion$migrating3Or4To5$Anon1.$userId);
                sb.append(':');
                Calendar instance = Calendar.getInstance();
                wg6.a((Object) instance, "Calendar.getInstance()");
                sb.append(instance.getTimeInMillis());
                String sb2 = sb.toString();
                String string = d.getString(d.getColumnIndex(Explore.COLUMN_TITLE));
                int i = d.getInt(d.getColumnIndex("hour"));
                int i2 = d.getInt(d.getColumnIndex("minute"));
                String string2 = d.getString(d.getColumnIndex(Alarm.COLUMN_DAYS));
                int i3 = d.getInt(d.getColumnIndex("isActive"));
                int i4 = d.getInt(d.getColumnIndex("isRepeated"));
                String string3 = d.getString(d.getColumnIndex("createdAt"));
                String string4 = d.getString(d.getColumnIndex("updatedAt"));
                StringBuilder sb3 = new StringBuilder();
                Cursor cursor = d;
                sb3.append("INSERT INTO alarm(id, uri, title, hour, minute, days, isActive, isRepeated, createdAt, updatedAt, pinType) VALUES (null, '");
                sb3.append(sb2);
                sb3.append("', '");
                sb3.append(string);
                sb3.append("', ");
                sb3.append(i);
                sb3.append(", ");
                sb3.append(i2);
                sb3.append(", '");
                sb3.append(string2);
                sb3.append("', ");
                sb3.append(i3);
                sb3.append(", ");
                sb3.append(i4);
                sb3.append(", '");
                sb3.append(string3);
                sb3.append("', '");
                sb3.append(string4);
                sb3.append("', 1)");
                iiVar2.b(sb3.toString());
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp2 = AlarmDatabase.TAG;
                local2.d(access$getTAG$cp2, "migrating3Or4To5 uri: " + sb2 + " - title: " + string);
                cursor.moveToNext();
                alarmDatabase$Companion$migrating3Or4To5$Anon1 = this;
                d = cursor;
            }
            d.close();
            iiVar2.b("UPDATE alarm SET pinType = 3 WHERE instr(uri, 'uri:') > 0 or instr(uri, ':') = 0");
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().e(AlarmDatabase.TAG, "migration is failed!");
            iiVar2.b("DROP TABLE IF EXISTS alarm_temp");
            iiVar2.b("DROP TABLE IF EXISTS alarm");
            iiVar2.b("CREATE TABLE IF NOT EXISTS  alarm (id TEXT, uri TEXT PRIMARY KEY NOT NULL, title TEXT NOT NULL, hour INTEGER NOT NULL, minute INTEGER NOT NULL, days TEXT, isActive INTEGER NOT NULL, isRepeated INTEGER NOT NULL, createdAt TEXT, updatedAt TEXT NOT NULL, pinType INTEGER NOT NULL DEFAULT 1)");
        }
        iiVar.x();
        iiVar.y();
    }
}
