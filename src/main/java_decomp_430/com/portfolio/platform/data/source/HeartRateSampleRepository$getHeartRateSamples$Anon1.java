package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.hh6;
import com.fossil.lc6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleRepository$getHeartRateSamples$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<HeartRateSample>, ApiResponse<HeartRate>> {
        @DexIgnore
        public /* final */ /* synthetic */ lc6 $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ hh6 $offset;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository$getHeartRateSamples$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(HeartRateSampleRepository$getHeartRateSamples$Anon1 heartRateSampleRepository$getHeartRateSamples$Anon1, hh6 hh6, int i, lc6 lc6) {
            this.this$0 = heartRateSampleRepository$getHeartRateSamples$Anon1;
            this.$offset = hh6;
            this.$limit = i;
            this.$downloadingDate = lc6;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ApiResponse<HeartRate>>> xe6) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            lc6 lc6 = this.$downloadingDate;
            if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                date = this.this$0.$startDate;
            }
            String e = bk4.e(date);
            wg6.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            lc6 lc62 = this.$downloadingDate;
            if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                date2 = this.this$0.$endDate;
            }
            String e2 = bk4.e(date2);
            wg6.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getHeartRateSamples(e, e2, this.$offset.element, this.$limit, xe6);
        }

        @DexIgnore
        public LiveData<List<HeartRateSample>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSampleRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getHeartRateSamples loadFromDb isNotToday day = " + this.this$0.$end);
            HeartRateSampleDao access$getMHeartRateSampleDao$p = this.this$0.this$0.mHeartRateSampleDao;
            Date date = this.this$0.$startDate;
            wg6.a((Object) date, "startDate");
            Date date2 = this.this$0.$endDate;
            wg6.a((Object) date2, "endDate");
            return access$getMHeartRateSampleDao$p.getHeartRateSamples(date, date2);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSamples onFetchFailed");
        }

        @DexIgnore
        public boolean processContinueFetching(ApiResponse<HeartRate> apiResponse) {
            wg6.b(apiResponse, "item");
            Range range = apiResponse.get_range();
            if (range == null || !range.isHasNext()) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSamples processContinueFetching hasNext");
            this.$offset.element += this.$limit;
            return true;
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<HeartRate> apiResponse) {
            wg6.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSampleRepository.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getHeartRateSamples saveCallResult onResponse: response = ");
            sb.append(apiResponse.get_items().size());
            sb.append(" hasNext=");
            Range range = apiResponse.get_range();
            sb.append(range != null ? Boolean.valueOf(range.isHasNext()) : null);
            local.d(tAG$app_fossilRelease, sb.toString());
            ArrayList arrayList = new ArrayList();
            for (HeartRate heartRateSample : apiResponse.get_items()) {
                HeartRateSample heartRateSample2 = heartRateSample.toHeartRateSample();
                if (heartRateSample2 != null) {
                    arrayList.add(heartRateSample2);
                }
            }
            this.this$0.this$0.mHeartRateSampleDao.upsertHeartRateSampleList(arrayList);
        }

        @DexIgnore
        public boolean shouldFetch(List<HeartRateSample> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public HeartRateSampleRepository$getHeartRateSamples$Anon1(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, boolean z, Date date3) {
        this.this$0 = heartRateSampleRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
        this.$end = date3;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon1$Anon1_Level2] */
    public final LiveData<yx5<List<HeartRateSample>>> apply(List<FitnessDataWrapper> list) {
        hh6 hh6 = new hh6();
        hh6.element = 0;
        wg6.a((Object) list, "fitnessDataList");
        Date date = this.$startDate;
        wg6.a((Object) date, "startDate");
        Date date2 = this.$endDate;
        wg6.a((Object) date2, "endDate");
        return new Anon1_Level2(this, hh6, 300, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
    }
}
