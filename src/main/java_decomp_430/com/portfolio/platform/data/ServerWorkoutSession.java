package com.portfolio.platform.data;

import com.fossil.cd6;
import com.fossil.ci4;
import com.fossil.fi4;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutLocation;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerWorkoutSession extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    @vu3("location")
    public WorkoutLocation location;
    @DexIgnore
    @vu3("calorie")
    public /* final */ WorkoutCalorie mCalorie;
    @DexIgnore
    @vu3("createdAt")
    public /* final */ DateTime mCreatedAt;
    @DexIgnore
    @vu3("date")
    public /* final */ Date mDate;
    @DexIgnore
    @vu3("deviceSerialNumber")
    public /* final */ String mDeviceSerialNumber;
    @DexIgnore
    @vu3("distance")
    public /* final */ WorkoutDistance mDistance;
    @DexIgnore
    @vu3("duration")
    public /* final */ int mDuration;
    @DexIgnore
    @vu3("endTime")
    public /* final */ String mEndTime;
    @DexIgnore
    @vu3("heartRate")
    public /* final */ WorkoutHeartRate mHeartRate;
    @DexIgnore
    @vu3("id")
    public /* final */ String mId;
    @DexIgnore
    @vu3("source")
    public /* final */ ci4 mSourceType;
    @DexIgnore
    @vu3("startTime")
    public /* final */ String mStartTime;
    @DexIgnore
    @vu3("step")
    public /* final */ WorkoutStep mStep;
    @DexIgnore
    @vu3("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @vu3("updatedAt")
    public /* final */ DateTime mUpdatedAt;
    @DexIgnore
    @vu3("type")
    public /* final */ fi4 mWorkoutType;
    @DexIgnore
    @vu3("speed")
    public WorkoutSpeed speed;
    @DexIgnore
    @vu3("states")
    public List<WorkoutStateChange> states;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerWorkoutSession.class.getSimpleName();
        wg6.a((Object) simpleName, "ServerWorkoutSession::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, ci4 ci4, WorkoutStep workoutStep, int i, fi4 fi4, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, WorkoutLocation workoutLocation, List<WorkoutStateChange> list) {
        String str5 = str4;
        DateTime dateTime3 = dateTime;
        DateTime dateTime4 = dateTime2;
        List<WorkoutStateChange> list2 = list;
        wg6.b(str, "mId");
        wg6.b(date, "mDate");
        wg6.b(str3, "mStartTime");
        wg6.b(str5, "mEndTime");
        wg6.b(dateTime3, "mCreatedAt");
        wg6.b(dateTime4, "mUpdatedAt");
        wg6.b(list2, "states");
        this.mId = str;
        this.mCalorie = workoutCalorie;
        this.mDate = date;
        this.mDeviceSerialNumber = str2;
        this.mDistance = workoutDistance;
        this.mStartTime = str3;
        this.mEndTime = str5;
        this.mHeartRate = workoutHeartRate;
        this.mSourceType = ci4;
        this.mStep = workoutStep;
        this.mTimeZoneOffsetInSecond = i;
        this.mWorkoutType = fi4;
        this.mCreatedAt = dateTime3;
        this.mUpdatedAt = dateTime4;
        this.mDuration = i2;
        this.speed = workoutSpeed;
        this.location = workoutLocation;
        this.states = list2;
    }

    @DexIgnore
    public final WorkoutLocation getLocation() {
        return this.location;
    }

    @DexIgnore
    public final WorkoutCalorie getMCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMDeviceSerialNumber() {
        return this.mDeviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getMDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public final int getMDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public final String getMEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public final WorkoutHeartRate getMHeartRate() {
        return this.mHeartRate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final ci4 getMSourceType() {
        return this.mSourceType;
    }

    @DexIgnore
    public final String getMStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public final WorkoutStep getMStep() {
        return this.mStep;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final fi4 getMWorkoutType() {
        return this.mWorkoutType;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final void setLocation(WorkoutLocation workoutLocation) {
        this.location = workoutLocation;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        wg6.b(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public final WorkoutSession toWorkoutSession() {
        WorkoutSession workoutSession;
        try {
            DateTimeFormatter withZone = ISODateTimeFormat.dateTime().withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            DateTime parseDateTime = withZone.parseDateTime(this.mStartTime);
            DateTime parseDateTime2 = withZone.parseDateTime(this.mEndTime);
            String str = this.mId;
            Date date = this.mDate;
            wg6.a((Object) parseDateTime, "startTime");
            wg6.a((Object) parseDateTime2, "endTime");
            workoutSession = new WorkoutSession(str, date, parseDateTime, parseDateTime2, this.mDeviceSerialNumber, this.mStep, this.mCalorie, this.mDistance, this.mHeartRate, this.mSourceType, this.mWorkoutType, this.mTimeZoneOffsetInSecond, this.mDuration, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
            try {
                workoutSession.setSpeed(this.speed);
                workoutSession.setLocation(this.location);
                workoutSession.getStates().addAll(this.states);
            } catch (ParseException e) {
                e = e;
            }
        } catch (ParseException e2) {
            e = e2;
            workoutSession = null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("toWorkoutSession exception=");
            e.printStackTrace();
            sb.append(cd6.a);
            local.d(str2, sb.toString());
            e.printStackTrace();
            return workoutSession;
        }
        return workoutSession;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, ci4 ci4, WorkoutStep workoutStep, int i, fi4 fi4, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, WorkoutLocation workoutLocation, List list, int i3, qg6 qg6) {
        this(str, workoutCalorie, date, str2, workoutDistance, str3, str4, workoutHeartRate, ci4, workoutStep, i, fi4, dateTime, dateTime2, i2, workoutSpeed, workoutLocation, (i3 & 131072) != 0 ? new ArrayList() : list);
    }
}
