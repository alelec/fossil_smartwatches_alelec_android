package com.portfolio.platform.data.legacy.threedotzero;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum PresetType {
    RECOMMENDED("recommended"),
    DEFAULT("default"),
    USER_SAVED("saved"),
    USER_NOT_SAVED("not saved");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public PresetType(String str) {
        this.value = str;
    }

    @DexIgnore
    public static PresetType fromString(String str) {
        for (PresetType presetType : values()) {
            if (presetType.value.equals(str)) {
                return presetType;
            }
        }
        return USER_SAVED;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
