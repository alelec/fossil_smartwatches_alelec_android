package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.qg6;
import com.fossil.u04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRemoteDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ u04 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            wg6.b(str, "<set-?>");
            MicroAppGalleryRemoteDataSource.TAG = str;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "MicroAppGalleryRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRemoteDataSource(ShortcutApiService shortcutApiService, u04 u04) {
        wg6.b(shortcutApiService, "mShortcutApiService");
        wg6.b(u04, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        wg6.b(str, "deviceSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getMicroAppGallery deviceSerial=" + str);
        this.mShortcutApiService.getMicroAppGallery(0, 100, str).a(new MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }
}
