package com.portfolio.platform.data.legacy.threedotzero;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PusherConfiguration {
    @DexIgnore
    public static /* final */ String TAG; // = "PusherConfiguration";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$Gesture; // = new int[Gesture.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher; // = new int[Pusher.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(52:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(53:0|(2:1|2)|3|5|6|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(54:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(55:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0093 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x009f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00ab */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00b7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00c3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00cf */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00db */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00e7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00f3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00ff */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x010b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0117 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x0123 */
        /*
        static {
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.TOP_PUSHER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.MID_PUSHER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.BOTTOM_PUSHER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.TRIPLE_TAP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_PRESSED.ordinal()] = 1;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS.ordinal()] = 2;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD.ordinal()] = 3;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS.ordinal()] = 4;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD.ordinal()] = 5;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS.ordinal()] = 6;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD.ordinal()] = 7;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_PRESSED.ordinal()] = 8;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS.ordinal()] = 9;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD.ordinal()] = 10;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS.ordinal()] = 11;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD.ordinal()] = 12;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS.ordinal()] = 13;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD.ordinal()] = 14;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_PRESSED.ordinal()] = 15;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS.ordinal()] = 16;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD.ordinal()] = 17;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS.ordinal()] = 18;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD.ordinal()] = 19;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS.ordinal()] = 20;
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD.ordinal()] = 21;
        }
        */
    }

    @DexIgnore
    public enum Pusher {
        TOP_PUSHER("top"),
        MID_PUSHER("middle"),
        BOTTOM_PUSHER("bottom"),
        DOUBLE_TAP("double tap"),
        TRIPLE_TAP("triple tap");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public Pusher(String str) {
            this.value = str;
        }

        @DexIgnore
        public static Pusher getPusherFromValue(String str) {
            for (Pusher pusher : values()) {
                if (pusher.value.equals(str)) {
                    return pusher;
                }
            }
            return TOP_PUSHER;
        }

        @DexIgnore
        public String getValue() {
            return this.value;
        }

        @DexIgnore
        public String toString() {
            return this.value;
        }
    }

    @DexIgnore
    public static Gesture getGestureWithLinkAction(Pusher pusher, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Get action of pusher=" + pusher);
        int i2 = Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[pusher.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 == 4) {
                        return Gesture.TRIPLE_PRESS;
                    }
                    FLogger.INSTANCE.getLocal().d(TAG, "Error!!! Gesture is undefined");
                    return Gesture.NONE;
                } else if (i == 102) {
                    return Gesture.SAM_BT3_DOUBLE_PRESS;
                } else {
                    if (i == 103) {
                        return Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD;
                    }
                    if (i == 202) {
                        return Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD;
                    }
                    if (Action.DisplayMode.isActionBelongToThisType(i)) {
                        return Gesture.SAM_BT3_PRESSED;
                    }
                    return Gesture.SAM_BT3_SINGLE_PRESS;
                }
            } else if (i == 102) {
                return Gesture.SAM_BT2_DOUBLE_PRESS;
            } else {
                if (i == 103) {
                    return Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD;
                }
                if (i == 202) {
                    return Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD;
                }
                if (Action.DisplayMode.isActionBelongToThisType(i)) {
                    return Gesture.SAM_BT2_PRESSED;
                }
                return Gesture.SAM_BT2_SINGLE_PRESS;
            }
        } else if (i == 102) {
            return Gesture.SAM_BT1_DOUBLE_PRESS;
        } else {
            if (i == 103) {
                return Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD;
            }
            if (i == 202) {
                return Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD;
            }
            if (Action.DisplayMode.isActionBelongToThisType(i)) {
                return Gesture.SAM_BT1_PRESSED;
            }
            return Gesture.SAM_BT1_SINGLE_PRESS;
        }
    }

    @DexIgnore
    public static Pusher getPusherByGesture(Gesture gesture) {
        if (gesture == null) {
            return null;
        }
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture[gesture.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                return Pusher.TOP_PUSHER;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return Pusher.MID_PUSHER;
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                return Pusher.BOTTOM_PUSHER;
            default:
                return null;
        }
    }
}
