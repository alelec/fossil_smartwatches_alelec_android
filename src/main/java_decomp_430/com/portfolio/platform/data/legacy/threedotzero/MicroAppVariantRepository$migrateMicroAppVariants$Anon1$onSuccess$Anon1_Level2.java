package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.hp4;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository$migrateMicroAppVariants$Anon1$onSuccess$Anon1_Level2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $variantParserList;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository$migrateMicroAppVariants$Anon1 this$0;

    @DexIgnore
    public MicroAppVariantRepository$migrateMicroAppVariants$Anon1$onSuccess$Anon1_Level2(MicroAppVariantRepository$migrateMicroAppVariants$Anon1 microAppVariantRepository$migrateMicroAppVariants$Anon1, ArrayList arrayList) {
        this.this$0 = microAppVariantRepository$migrateMicroAppVariants$Anon1;
        this.$variantParserList = arrayList;
    }

    @DexIgnore
    public final void run() {
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadAllVariants");
        MicroAppVariantDataSource access$getMMicroAppVariantLocalDataSource$p = this.this$0.this$0.mMicroAppVariantLocalDataSource;
        MicroAppVariantRepository$migrateMicroAppVariants$Anon1 microAppVariantRepository$migrateMicroAppVariants$Anon1 = this.this$0;
        access$getMMicroAppVariantLocalDataSource$p.removeMicroAppVariants(microAppVariantRepository$migrateMicroAppVariants$Anon1.$serialNumber, microAppVariantRepository$migrateMicroAppVariants$Anon1.$major, microAppVariantRepository$migrateMicroAppVariants$Anon1.$minor);
        ArrayList<hp4> filterVariantList$app_fossilRelease = this.this$0.this$0.filterVariantList$app_fossilRelease(this.$variantParserList);
        MicroAppVariantRepository$migrateMicroAppVariants$Anon1 microAppVariantRepository$migrateMicroAppVariants$Anon12 = this.this$0;
        microAppVariantRepository$migrateMicroAppVariants$Anon12.this$0.saveMicroAppVariant$app_fossilRelease(microAppVariantRepository$migrateMicroAppVariants$Anon12.$serialNumber, filterVariantList$app_fossilRelease);
        UAppSystemVersionModel uAppSystemVersionModel = this.this$0.this$0.mUAppSystemVersionRepository.getUAppSystemVersionModel(this.this$0.$serialNumber);
        if (uAppSystemVersionModel != null && uAppSystemVersionModel.getMajorVersion() == this.this$0.$major && uAppSystemVersionModel.getMinorVersion() == this.this$0.$minor) {
            uAppSystemVersionModel.setPinType(0);
            this.this$0.this$0.mUAppSystemVersionRepository.addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
        }
        MicroAppVariantRepository$migrateMicroAppVariants$Anon1 microAppVariantRepository$migrateMicroAppVariants$Anon13 = this.this$0;
        microAppVariantRepository$migrateMicroAppVariants$Anon13.this$0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", microAppVariantRepository$migrateMicroAppVariants$Anon13.$serialNumber);
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadAllVariants");
        MicroAppVariantDataSource.MigrateVariantCallback migrateVariantCallback = this.this$0.$callback;
        if (migrateVariantCallback != null) {
            migrateVariantCallback.onDone();
        }
    }
}
