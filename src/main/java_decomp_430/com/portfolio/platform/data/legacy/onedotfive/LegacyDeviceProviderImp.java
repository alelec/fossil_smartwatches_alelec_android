package com.portfolio.platform.data.legacy.onedotfive;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LegacyDeviceProviderImp extends BaseDbProvider implements LegacyDeviceProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "device.db";
    @DexIgnore
    public static /* final */ String TAG; // = DeviceProviderImp.class.getSimpleName();
    @DexIgnore
    public String mCurrentDBPath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public LegacyDeviceProviderImp(Context context, String str) {
        super(context, str);
        this.mCurrentDBPath = str;
    }

    @DexIgnore
    private Dao<LegacyDeviceModel, Integer> getDeviceSessionDao() throws SQLException {
        return this.databaseHelper.getDao(LegacyDeviceModel.class);
    }

    @DexIgnore
    private Dao<FavoriteMappingSet, Integer> getMappingSetSessionDao() throws SQLException {
        return this.databaseHelper.getDao(FavoriteMappingSet.class);
    }

    @DexIgnore
    public List<LegacyDeviceModel> getAllDevice() {
        ArrayList arrayList = new ArrayList();
        try {
            return getDeviceSessionDao().query(getDeviceSessionDao().queryBuilder().prepare());
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error when get all device " + e);
            return arrayList;
        }
    }

    @DexIgnore
    public List<FavoriteMappingSet> getAllFavoriteMappingSet() {
        try {
            return getMappingSetSessionDao().queryForAll();
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, ".getListMappingSetByDeviceFamily - ex=" + e.toString());
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{LegacyDeviceModel.class, FavoriteMappingSet.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.mCurrentDBPath;
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 10;
    }

    @DexIgnore
    public LegacyDeviceModel getDeviceById(String str) {
        if (TextUtils.isEmpty(str)) {
            String str2 = TAG;
            MFLogger.e(str2, "Error when get device, deviceId " + str);
        }
        try {
            String str3 = TAG;
            MFLogger.d(str3, "---Inside .getDeviceById deviceId: " + str);
            QueryBuilder<LegacyDeviceModel, Integer> queryBuilder = getDeviceSessionDao().queryBuilder();
            queryBuilder.where().eq("deviceId", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            String str4 = TAG;
            MFLogger.e(str4, "Error inside " + TAG + ".getDeviceById - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public LegacyDeviceProviderImp(Context context, String str, String str2) {
        super(context, str, str2);
    }
}
