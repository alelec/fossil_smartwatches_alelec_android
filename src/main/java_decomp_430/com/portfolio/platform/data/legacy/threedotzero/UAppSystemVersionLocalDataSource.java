package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.zm4;
import com.portfolio.platform.data.source.UAppSystemVersionDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class UAppSystemVersionLocalDataSource implements UAppSystemVersionDataSource {
    @DexIgnore
    public void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel) {
        zm4.p.a().d().addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
    }

    @DexIgnore
    public UAppSystemVersionModel getUAppSystemVersionModel(String str) {
        return zm4.p.a().d().getUAppSystemVersionModel(str);
    }
}
