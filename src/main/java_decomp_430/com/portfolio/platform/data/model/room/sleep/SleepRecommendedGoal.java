package com.portfolio.platform.data.model.room.sleep;

import com.fossil.d;
import com.fossil.pj4;
import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepRecommendedGoal {
    @DexIgnore
    @pj4
    public int id;
    @DexIgnore
    @vu3("recommendedGoalMinutes")
    public int recommendedSleepGoal;

    @DexIgnore
    public SleepRecommendedGoal(int i) {
        this.recommendedSleepGoal = i;
    }

    @DexIgnore
    public static /* synthetic */ SleepRecommendedGoal copy$default(SleepRecommendedGoal sleepRecommendedGoal, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = sleepRecommendedGoal.recommendedSleepGoal;
        }
        return sleepRecommendedGoal.copy(i);
    }

    @DexIgnore
    public final int component1() {
        return this.recommendedSleepGoal;
    }

    @DexIgnore
    public final SleepRecommendedGoal copy(int i) {
        return new SleepRecommendedGoal(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof SleepRecommendedGoal) && this.recommendedSleepGoal == ((SleepRecommendedGoal) obj).recommendedSleepGoal;
        }
        return true;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final int getRecommendedSleepGoal() {
        return this.recommendedSleepGoal;
    }

    @DexIgnore
    public int hashCode() {
        return d.a(this.recommendedSleepGoal);
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setRecommendedSleepGoal(int i) {
        this.recommendedSleepGoal = i;
    }

    @DexIgnore
    public String toString() {
        return "SleepRecommendedGoal(recommendedSleepGoal=" + this.recommendedSleepGoal + ")";
    }
}
