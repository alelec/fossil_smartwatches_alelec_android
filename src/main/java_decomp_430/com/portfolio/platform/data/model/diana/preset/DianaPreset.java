package com.portfolio.platform.data.model.diana.preset;

import android.content.Context;
import com.fossil.bk4;
import com.fossil.jm4;
import com.fossil.pj4;
import com.fossil.qg6;
import com.fossil.tu3;
import com.fossil.uu3;
import com.fossil.vu3;
import com.fossil.wg6;
import com.fossil.zi4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    @uu3(DianaPresetComplicationSettingSerializer.class)
    @vu3("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @vu3("createdAt")
    public String createdAt; // = "";
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @vu3("isActive")
    public boolean isActive;
    @DexIgnore
    @tu3
    @vu3("name")
    public String name;
    @DexIgnore
    @pj4
    public int pinType; // = 1;
    @DexIgnore
    @vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @vu3("updatedAt")
    public String updatedAt; // = "";
    @DexIgnore
    @vu3("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @tu3
    @uu3(DianaPresetWatchAppSettingSerializer.class)
    @vu3("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final DianaPreset cloneFrom(DianaPreset dianaPreset) {
            wg6.b(dianaPreset, "preset");
            String u = bk4.u(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wg6.a((Object) uuid, "UUID.randomUUID().toString()");
            String serialNumber = dianaPreset.getSerialNumber();
            String a = jm4.a((Context) PortfolioApp.get.instance(), 2131886370);
            wg6.a((Object) a, "LanguageHelper.getString\u2026wPreset_Title__NewPreset)");
            DianaPreset dianaPreset2 = new DianaPreset(uuid, serialNumber, a, false, zi4.b(dianaPreset.getComplications()), zi4.c(dianaPreset.getWatchapps()), dianaPreset.getWatchFaceId());
            dianaPreset2.setCreatedAt(u);
            dianaPreset2.setUpdatedAt(u);
            return dianaPreset2;
        }

        @DexIgnore
        public final DianaPreset cloneFromDefaultPreset(DianaRecommendPreset dianaRecommendPreset) {
            wg6.b(dianaRecommendPreset, "recommendPreset");
            String u = bk4.u(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            wg6.a((Object) uuid, "UUID.randomUUID().toString()");
            DianaPreset dianaPreset = new DianaPreset(uuid, dianaRecommendPreset.getSerialNumber(), dianaRecommendPreset.getName(), dianaRecommendPreset.isDefault(), zi4.b(dianaRecommendPreset.getComplications()), zi4.c(dianaRecommendPreset.getWatchapps()), dianaRecommendPreset.getWatchFaceId());
            dianaPreset.setCreatedAt(u);
            dianaPreset.setUpdatedAt(u);
            for (DianaPresetComplicationSetting localUpdateAt : dianaPreset.getComplications()) {
                wg6.a((Object) u, "timestamp");
                localUpdateAt.setLocalUpdateAt(u);
            }
            for (DianaPresetWatchAppSetting localUpdateAt2 : dianaPreset.getWatchapps()) {
                wg6.a((Object) u, "timestamp");
                localUpdateAt2.setLocalUpdateAt(u);
            }
            return dianaPreset;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DianaPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        wg6.b(str, "id");
        wg6.b(str2, "serialNumber");
        wg6.b(str3, "name");
        wg6.b(arrayList, "complications");
        wg6.b(arrayList2, "watchapps");
        wg6.b(str4, "watchFaceId");
        this.id = str;
        this.serialNumber = str2;
        this.name = str3;
        this.isActive = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaPreset copy$default(DianaPreset dianaPreset, String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaPreset.id;
        }
        if ((i & 2) != 0) {
            str2 = dianaPreset.serialNumber;
        }
        String str5 = str2;
        if ((i & 4) != 0) {
            str3 = dianaPreset.name;
        }
        String str6 = str3;
        if ((i & 8) != 0) {
            z = dianaPreset.isActive;
        }
        boolean z2 = z;
        if ((i & 16) != 0) {
            arrayList = dianaPreset.complications;
        }
        ArrayList<DianaPresetComplicationSetting> arrayList3 = arrayList;
        if ((i & 32) != 0) {
            arrayList2 = dianaPreset.watchapps;
        }
        ArrayList<DianaPresetWatchAppSetting> arrayList4 = arrayList2;
        if ((i & 64) != 0) {
            str4 = dianaPreset.watchFaceId;
        }
        return dianaPreset.copy(str, str5, str6, z2, arrayList3, arrayList4, str4);
    }

    @DexIgnore
    public final DianaPreset clone() {
        DianaPreset dianaPreset = new DianaPreset(this.id, this.serialNumber, this.name, this.isActive, zi4.b(this.complications), zi4.c(this.watchapps), this.watchFaceId);
        dianaPreset.updatedAt = this.updatedAt;
        dianaPreset.createdAt = this.createdAt;
        return dianaPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isActive;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final DianaPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        wg6.b(str, "id");
        wg6.b(str2, "serialNumber");
        wg6.b(str3, "name");
        wg6.b(arrayList, "complications");
        wg6.b(arrayList2, "watchapps");
        String str5 = str4;
        wg6.b(str5, "watchFaceId");
        return new DianaPreset(str, str2, str3, z, arrayList, arrayList2, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DianaPreset)) {
            return false;
        }
        DianaPreset dianaPreset = (DianaPreset) obj;
        return wg6.a((Object) this.id, (Object) dianaPreset.id) && wg6.a((Object) this.serialNumber, (Object) dianaPreset.serialNumber) && wg6.a((Object) this.name, (Object) dianaPreset.name) && this.isActive == dianaPreset.isActive && wg6.a((Object) this.complications, (Object) dianaPreset.complications) && wg6.a((Object) this.watchapps, (Object) dianaPreset.watchapps) && wg6.a((Object) this.watchFaceId, (Object) dianaPreset.watchFaceId);
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.serialNumber;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        int i2 = (hashCode3 + (z ? 1 : 0)) * 31;
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = (i2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = (hashCode4 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        String str4 = this.watchFaceId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode5 + i;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        wg6.b(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.id + ", serialNumber=" + this.serialNumber + ", name=" + this.name + ", isActive=" + this.isActive + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ")";
    }
}
