package com.portfolio.platform.data.model.room.sleep;

import com.fossil.al4;
import com.fossil.b;
import com.fossil.d;
import com.fossil.e;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.gson.Gson;
import com.j256.ormlite.logger.Logger;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFSleepSession implements Serializable {
    @DexIgnore
    public Integer bookmarkTime;
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public long date;
    @DexIgnore
    public Date day;
    @DexIgnore
    public String deviceSerialNumber;
    @DexIgnore
    public Integer editedEndTime;
    @DexIgnore
    public Integer editedSleepMinutes;
    @DexIgnore
    public SleepDistribution editedSleepStateDistInMinute;
    @DexIgnore
    public Integer editedStartTime;
    @DexIgnore
    public SleepSessionHeartRate heartRate;
    @DexIgnore
    public double normalizedSleepQuality;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int realEndTime;
    @DexIgnore
    public int realSleepMinutes;
    @DexIgnore
    public SleepDistribution realSleepStateDistInMinute;
    @DexIgnore
    public int realStartTime;
    @DexIgnore
    public String sleepStates;
    @DexIgnore
    public int source;
    @DexIgnore
    public Integer syncTime;
    @DexIgnore
    public int timezoneOffset;
    @DexIgnore
    public DateTime updatedAt;

    @DexIgnore
    public MFSleepSession(long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5) {
        SleepDistribution sleepDistribution3 = sleepDistribution;
        String str3 = str2;
        DateTime dateTime3 = dateTime;
        DateTime dateTime4 = dateTime2;
        wg6.b(date2, "day");
        wg6.b(sleepDistribution3, "realSleepStateDistInMinute");
        wg6.b(str3, "sleepStates");
        wg6.b(dateTime3, "createdAt");
        wg6.b(dateTime4, "updatedAt");
        this.date = j;
        this.day = date2;
        this.deviceSerialNumber = str;
        this.syncTime = num;
        this.bookmarkTime = num2;
        this.normalizedSleepQuality = d;
        this.source = i;
        this.realStartTime = i2;
        this.realEndTime = i3;
        this.realSleepMinutes = i4;
        this.realSleepStateDistInMinute = sleepDistribution3;
        this.editedStartTime = num3;
        this.editedEndTime = num4;
        this.editedSleepMinutes = num5;
        this.editedSleepStateDistInMinute = sleepDistribution2;
        this.sleepStates = str3;
        this.heartRate = sleepSessionHeartRate;
        this.createdAt = dateTime3;
        this.updatedAt = dateTime4;
        this.timezoneOffset = i5;
    }

    @DexIgnore
    public static /* synthetic */ MFSleepSession copy$default(MFSleepSession mFSleepSession, long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5, int i6, Object obj) {
        MFSleepSession mFSleepSession2 = mFSleepSession;
        int i7 = i6;
        return mFSleepSession.copy((i7 & 1) != 0 ? mFSleepSession2.date : j, (i7 & 2) != 0 ? mFSleepSession2.day : date2, (i7 & 4) != 0 ? mFSleepSession2.deviceSerialNumber : str, (i7 & 8) != 0 ? mFSleepSession2.syncTime : num, (i7 & 16) != 0 ? mFSleepSession2.bookmarkTime : num2, (i7 & 32) != 0 ? mFSleepSession2.normalizedSleepQuality : d, (i7 & 64) != 0 ? mFSleepSession2.source : i, (i7 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? mFSleepSession2.realStartTime : i2, (i7 & 256) != 0 ? mFSleepSession2.realEndTime : i3, (i7 & 512) != 0 ? mFSleepSession2.realSleepMinutes : i4, (i7 & 1024) != 0 ? mFSleepSession2.realSleepStateDistInMinute : sleepDistribution, (i7 & 2048) != 0 ? mFSleepSession2.editedStartTime : num3, (i7 & 4096) != 0 ? mFSleepSession2.editedEndTime : num4, (i7 & 8192) != 0 ? mFSleepSession2.editedSleepMinutes : num5, (i7 & 16384) != 0 ? mFSleepSession2.editedSleepStateDistInMinute : sleepDistribution2, (i7 & 32768) != 0 ? mFSleepSession2.sleepStates : str2, (i7 & 65536) != 0 ? mFSleepSession2.heartRate : sleepSessionHeartRate, (i7 & 131072) != 0 ? mFSleepSession2.createdAt : dateTime, (i7 & 262144) != 0 ? mFSleepSession2.updatedAt : dateTime2, (i7 & 524288) != 0 ? mFSleepSession2.timezoneOffset : i5);
    }

    @DexIgnore
    public final long component1() {
        return this.date;
    }

    @DexIgnore
    public final int component10() {
        return this.realSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution component11() {
        return this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public final Integer component12() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final Integer component13() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Integer component14() {
        return this.editedSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution component15() {
        return this.editedSleepStateDistInMinute;
    }

    @DexIgnore
    public final String component16() {
        return this.sleepStates;
    }

    @DexIgnore
    public final SleepSessionHeartRate component17() {
        return this.heartRate;
    }

    @DexIgnore
    public final DateTime component18() {
        return this.createdAt;
    }

    @DexIgnore
    public final DateTime component19() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Date component2() {
        return this.day;
    }

    @DexIgnore
    public final int component20() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final String component3() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final Integer component4() {
        return this.syncTime;
    }

    @DexIgnore
    public final Integer component5() {
        return this.bookmarkTime;
    }

    @DexIgnore
    public final double component6() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int component7() {
        return this.source;
    }

    @DexIgnore
    public final int component8() {
        return this.realStartTime;
    }

    @DexIgnore
    public final int component9() {
        return this.realEndTime;
    }

    @DexIgnore
    public final MFSleepSession copy(long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5) {
        long j2 = j;
        wg6.b(date2, "day");
        wg6.b(sleepDistribution, "realSleepStateDistInMinute");
        wg6.b(str2, "sleepStates");
        wg6.b(dateTime, "createdAt");
        wg6.b(dateTime2, "updatedAt");
        return new MFSleepSession(j, date2, str, num, num2, d, i, i2, i3, i4, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MFSleepSession)) {
            return false;
        }
        MFSleepSession mFSleepSession = (MFSleepSession) obj;
        return this.date == mFSleepSession.date && wg6.a((Object) this.day, (Object) mFSleepSession.day) && wg6.a((Object) this.deviceSerialNumber, (Object) mFSleepSession.deviceSerialNumber) && wg6.a((Object) this.syncTime, (Object) mFSleepSession.syncTime) && wg6.a((Object) this.bookmarkTime, (Object) mFSleepSession.bookmarkTime) && Double.compare(this.normalizedSleepQuality, mFSleepSession.normalizedSleepQuality) == 0 && this.source == mFSleepSession.source && this.realStartTime == mFSleepSession.realStartTime && this.realEndTime == mFSleepSession.realEndTime && this.realSleepMinutes == mFSleepSession.realSleepMinutes && wg6.a((Object) this.realSleepStateDistInMinute, (Object) mFSleepSession.realSleepStateDistInMinute) && wg6.a((Object) this.editedStartTime, (Object) mFSleepSession.editedStartTime) && wg6.a((Object) this.editedEndTime, (Object) mFSleepSession.editedEndTime) && wg6.a((Object) this.editedSleepMinutes, (Object) mFSleepSession.editedSleepMinutes) && wg6.a((Object) this.editedSleepStateDistInMinute, (Object) mFSleepSession.editedSleepStateDistInMinute) && wg6.a((Object) this.sleepStates, (Object) mFSleepSession.sleepStates) && wg6.a((Object) this.heartRate, (Object) mFSleepSession.heartRate) && wg6.a((Object) this.createdAt, (Object) mFSleepSession.createdAt) && wg6.a((Object) this.updatedAt, (Object) mFSleepSession.updatedAt) && this.timezoneOffset == mFSleepSession.timezoneOffset;
    }

    @DexIgnore
    public final Integer getBookmarkTime() {
        return this.bookmarkTime;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final long getDate() {
        return this.date;
    }

    @DexIgnore
    public final Date getDay() {
        return this.day;
    }

    @DexIgnore
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final Integer getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Integer getEditedSleepMinutes() {
        return this.editedSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getEditedSleepStateDistInMinute() {
        return this.editedSleepStateDistInMinute;
    }

    @DexIgnore
    public final Integer getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final int getEndTime() {
        Integer num = this.editedEndTime;
        return num != null ? num.intValue() : this.realEndTime;
    }

    @DexIgnore
    public final SleepSessionHeartRate getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final double getNormalizedSleepQuality() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getRealEndTime() {
        return this.realEndTime;
    }

    @DexIgnore
    public final int getRealSleepMinutes() {
        return this.realSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getRealSleepStateDistInMinute() {
        return this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public final int getRealStartTime() {
        return this.realStartTime;
    }

    @DexIgnore
    public final int getSleepMinutes() {
        Integer num = this.editedSleepMinutes;
        return num != null ? num.intValue() : this.realSleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getSleepState() {
        SleepDistribution sleepDistribution = this.editedSleepStateDistInMinute;
        return sleepDistribution != null ? sleepDistribution : this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public final List<WrapperSleepStateChange> getSleepStateChange() {
        try {
            return (List) new Gson().a(this.sleepStates, new MFSleepSession$getSleepStateChange$Anon1().getType());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    public final String getSleepStates() {
        return this.sleepStates;
    }

    @DexIgnore
    public final int getSource() {
        return this.source;
    }

    @DexIgnore
    public final int getStartTime() {
        Integer num = this.editedStartTime;
        return num != null ? num.intValue() : this.realStartTime;
    }

    @DexIgnore
    public final Integer getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int a = e.a(this.date) * 31;
        Date date2 = this.day;
        int i = 0;
        int hashCode = (a + (date2 != null ? date2.hashCode() : 0)) * 31;
        String str = this.deviceSerialNumber;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        Integer num = this.syncTime;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.bookmarkTime;
        int hashCode4 = (((((((((((hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31) + b.a(this.normalizedSleepQuality)) * 31) + d.a(this.source)) * 31) + d.a(this.realStartTime)) * 31) + d.a(this.realEndTime)) * 31) + d.a(this.realSleepMinutes)) * 31;
        SleepDistribution sleepDistribution = this.realSleepStateDistInMinute;
        int hashCode5 = (hashCode4 + (sleepDistribution != null ? sleepDistribution.hashCode() : 0)) * 31;
        Integer num3 = this.editedStartTime;
        int hashCode6 = (hashCode5 + (num3 != null ? num3.hashCode() : 0)) * 31;
        Integer num4 = this.editedEndTime;
        int hashCode7 = (hashCode6 + (num4 != null ? num4.hashCode() : 0)) * 31;
        Integer num5 = this.editedSleepMinutes;
        int hashCode8 = (hashCode7 + (num5 != null ? num5.hashCode() : 0)) * 31;
        SleepDistribution sleepDistribution2 = this.editedSleepStateDistInMinute;
        int hashCode9 = (hashCode8 + (sleepDistribution2 != null ? sleepDistribution2.hashCode() : 0)) * 31;
        String str2 = this.sleepStates;
        int hashCode10 = (hashCode9 + (str2 != null ? str2.hashCode() : 0)) * 31;
        SleepSessionHeartRate sleepSessionHeartRate = this.heartRate;
        int hashCode11 = (hashCode10 + (sleepSessionHeartRate != null ? sleepSessionHeartRate.hashCode() : 0)) * 31;
        DateTime dateTime = this.createdAt;
        int hashCode12 = (hashCode11 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return ((hashCode12 + i) * 31) + d.a(this.timezoneOffset);
    }

    @DexIgnore
    public final void setBookmarkTime(Integer num) {
        this.bookmarkTime = num;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDate(long j) {
        this.date = j;
    }

    @DexIgnore
    public final void setDay(Date date2) {
        wg6.b(date2, "<set-?>");
        this.day = date2;
    }

    @DexIgnore
    public final void setDeviceSerialNumber(String str) {
        this.deviceSerialNumber = str;
    }

    @DexIgnore
    public final void setEditedEndTime(Integer num) {
        this.editedEndTime = num;
    }

    @DexIgnore
    public final void setEditedSleepMinutes(Integer num) {
        this.editedSleepMinutes = num;
    }

    @DexIgnore
    public final void setEditedSleepStateDistInMinute(SleepDistribution sleepDistribution) {
        this.editedSleepStateDistInMinute = sleepDistribution;
    }

    @DexIgnore
    public final void setEditedStartTime(Integer num) {
        this.editedStartTime = num;
    }

    @DexIgnore
    public final void setHeartRate(SleepSessionHeartRate sleepSessionHeartRate) {
        this.heartRate = sleepSessionHeartRate;
    }

    @DexIgnore
    public final void setNormalizedSleepQuality(double d) {
        this.normalizedSleepQuality = d;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setRealEndTime(int i) {
        this.realEndTime = i;
    }

    @DexIgnore
    public final void setRealSleepMinutes(int i) {
        this.realSleepMinutes = i;
    }

    @DexIgnore
    public final void setRealSleepStateDistInMinute(SleepDistribution sleepDistribution) {
        wg6.b(sleepDistribution, "<set-?>");
        this.realSleepStateDistInMinute = sleepDistribution;
    }

    @DexIgnore
    public final void setRealStartTime(int i) {
        this.realStartTime = i;
    }

    @DexIgnore
    public final void setSleepStates(String str) {
        wg6.b(str, "<set-?>");
        this.sleepStates = str;
    }

    @DexIgnore
    public final void setSource(int i) {
        this.source = i;
    }

    @DexIgnore
    public final void setSyncTime(Integer num) {
        this.syncTime = num;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepSession(date=" + this.date + ", day=" + this.day + ", deviceSerialNumber=" + this.deviceSerialNumber + ", syncTime=" + this.syncTime + ", bookmarkTime=" + this.bookmarkTime + ", normalizedSleepQuality=" + this.normalizedSleepQuality + ", source=" + this.source + ", realStartTime=" + this.realStartTime + ", realEndTime=" + this.realEndTime + ", realSleepMinutes=" + this.realSleepMinutes + ", realSleepStateDistInMinute=" + this.realSleepStateDistInMinute + ", editedStartTime=" + this.editedStartTime + ", editedEndTime=" + this.editedEndTime + ", editedSleepMinutes=" + this.editedSleepMinutes + ", editedSleepStateDistInMinute=" + this.editedSleepStateDistInMinute + ", sleepStates=" + this.sleepStates + ", heartRate=" + this.heartRate + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", timezoneOffset=" + this.timezoneOffset + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MFSleepSession(long j, Date date2, String str, Integer num, Integer num2, double d, int i, int i2, int i3, int i4, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2, int i5, int i6, qg6 qg6) {
        this(j, date2, str, num, num2, d, i, i2, i3, i4, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, (i6 & 524288) != 0 ? al4.a() : i5);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MFSleepSession(long j, Date date2, int i, String str, Integer num, Integer num2, double d, int i2, int i3, int i4, int i5, SleepDistribution sleepDistribution, Integer num3, Integer num4, Integer num5, SleepDistribution sleepDistribution2, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime, DateTime dateTime2) {
        this(r1, date2, str, num, num2, d, i2, i3, i4, i5, sleepDistribution, num3, num4, num5, sleepDistribution2, str2, sleepSessionHeartRate, dateTime, dateTime2, i);
        long j2 = j;
        wg6.b(date2, "day");
        wg6.b(sleepDistribution, "realSleepStateDistInMinute");
        wg6.b(str2, "sleepStates");
        wg6.b(dateTime, "createdAt");
        wg6.b(dateTime2, "updatedAt");
        long j3 = j;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public MFSleepSession(long j, Date date2, int i, String str, int i2, int i3, double d, int i4, int i5, int i6, int i7, SleepDistribution sleepDistribution, String str2, SleepSessionHeartRate sleepSessionHeartRate, DateTime dateTime) {
        this(j, date2, str, r5, r6, d, i4, i5, i6, i7, r13, r14, r15, r16, sleepDistribution, str2, sleepSessionHeartRate, r20, dateTime, i);
        SleepDistribution sleepDistribution2 = sleepDistribution;
        wg6.b(date2, "day");
        wg6.b(str, "deviceSerialNumber");
        wg6.b(sleepDistribution, "realSleepStateDistInMinute");
        wg6.b(str2, "sleepStates");
        wg6.b(dateTime, "updatedAt");
        Integer valueOf = Integer.valueOf(i2);
        Integer valueOf2 = Integer.valueOf(i3);
        Integer valueOf3 = Integer.valueOf(i5);
        Integer valueOf4 = Integer.valueOf(i6);
        Integer valueOf5 = Integer.valueOf(i7);
        DateTime dateTime2 = r23;
        DateTime dateTime3 = new DateTime();
    }
}
