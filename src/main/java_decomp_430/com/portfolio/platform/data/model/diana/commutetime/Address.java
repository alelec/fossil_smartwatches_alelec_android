package com.portfolio.platform.data.model.diana.commutetime;

import com.fossil.b;
import com.fossil.qg6;
import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Address {
    @DexIgnore
    @vu3("lat")
    public double lat;
    @DexIgnore
    @vu3("lng")
    public double lng;

    @DexIgnore
    public Address() {
        this(0.0d, 0.0d, 3, (qg6) null);
    }

    @DexIgnore
    public Address(double d, double d2) {
        this.lat = d;
        this.lng = d2;
    }

    @DexIgnore
    public static /* synthetic */ Address copy$default(Address address, double d, double d2, int i, Object obj) {
        if ((i & 1) != 0) {
            d = address.lat;
        }
        if ((i & 2) != 0) {
            d2 = address.lng;
        }
        return address.copy(d, d2);
    }

    @DexIgnore
    public final double component1() {
        return this.lat;
    }

    @DexIgnore
    public final double component2() {
        return this.lng;
    }

    @DexIgnore
    public final Address copy(double d, double d2) {
        return new Address(d, d2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Address)) {
            return false;
        }
        Address address = (Address) obj;
        return Double.compare(this.lat, address.lat) == 0 && Double.compare(this.lng, address.lng) == 0;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public int hashCode() {
        return (b.a(this.lat) * 31) + b.a(this.lng);
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setLng(double d) {
        this.lng = d;
    }

    @DexIgnore
    public String toString() {
        return "Address(lat=" + this.lat + ", lng=" + this.lng + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Address(double d, double d2, int i, qg6 qg6) {
        this((i & 1) != 0 ? 0.0d : d, (i & 2) != 0 ? 0.0d : d2);
    }
}
