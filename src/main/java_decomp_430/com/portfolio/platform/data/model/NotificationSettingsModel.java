package com.portfolio.platform.data.model;

import com.fossil.d;
import com.fossil.tu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsModel {
    @DexIgnore
    @tu3
    public boolean isCall;
    @DexIgnore
    @tu3
    public String settingsName;
    @DexIgnore
    @tu3
    public int settingsType;

    @DexIgnore
    public NotificationSettingsModel(String str, int i, boolean z) {
        wg6.b(str, "settingsName");
        this.settingsName = str;
        this.settingsType = i;
        this.isCall = z;
    }

    @DexIgnore
    public static /* synthetic */ NotificationSettingsModel copy$default(NotificationSettingsModel notificationSettingsModel, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = notificationSettingsModel.settingsName;
        }
        if ((i2 & 2) != 0) {
            i = notificationSettingsModel.settingsType;
        }
        if ((i2 & 4) != 0) {
            z = notificationSettingsModel.isCall;
        }
        return notificationSettingsModel.copy(str, i, z);
    }

    @DexIgnore
    public final String component1() {
        return this.settingsName;
    }

    @DexIgnore
    public final int component2() {
        return this.settingsType;
    }

    @DexIgnore
    public final boolean component3() {
        return this.isCall;
    }

    @DexIgnore
    public final NotificationSettingsModel copy(String str, int i, boolean z) {
        wg6.b(str, "settingsName");
        return new NotificationSettingsModel(str, i, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationSettingsModel)) {
            return false;
        }
        NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) obj;
        return wg6.a((Object) this.settingsName, (Object) notificationSettingsModel.settingsName) && this.settingsType == notificationSettingsModel.settingsType && this.isCall == notificationSettingsModel.isCall;
    }

    @DexIgnore
    public final String getSettingsName() {
        return this.settingsName;
    }

    @DexIgnore
    public final int getSettingsType() {
        return this.settingsType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.settingsName;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + d.a(this.settingsType)) * 31;
        boolean z = this.isCall;
        if (z) {
            z = true;
        }
        return hashCode + (z ? 1 : 0);
    }

    @DexIgnore
    public final boolean isCall() {
        return this.isCall;
    }

    @DexIgnore
    public final void setCall(boolean z) {
        this.isCall = z;
    }

    @DexIgnore
    public final void setSettingsName(String str) {
        wg6.b(str, "<set-?>");
        this.settingsName = str;
    }

    @DexIgnore
    public final void setSettingsType(int i) {
        this.settingsType = i;
    }

    @DexIgnore
    public String toString() {
        return "NotificationSettingsModel(settingsName=" + this.settingsName + ", settingsType=" + this.settingsType + ", isCall=" + this.isCall + ")";
    }
}
