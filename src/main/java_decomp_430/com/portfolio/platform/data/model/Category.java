package com.portfolio.platform.data.model;

import com.fossil.d;
import com.fossil.tu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Category {
    @DexIgnore
    @tu3
    public String createdAt;
    @DexIgnore
    @tu3
    public String englishName;
    @DexIgnore
    @tu3
    public String id;
    @DexIgnore
    @tu3
    public String name;
    @DexIgnore
    @tu3
    public int priority;
    @DexIgnore
    @tu3
    public String updatedAt;

    @DexIgnore
    public Category(String str, String str2, String str3, String str4, String str5, int i) {
        wg6.b(str, "id");
        wg6.b(str2, "englishName");
        wg6.b(str3, "name");
        wg6.b(str4, "updatedAt");
        wg6.b(str5, "createdAt");
        this.id = str;
        this.englishName = str2;
        this.name = str3;
        this.updatedAt = str4;
        this.createdAt = str5;
        this.priority = i;
    }

    @DexIgnore
    public static /* synthetic */ Category copy$default(Category category, String str, String str2, String str3, String str4, String str5, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = category.id;
        }
        if ((i2 & 2) != 0) {
            str2 = category.englishName;
        }
        String str6 = str2;
        if ((i2 & 4) != 0) {
            str3 = category.name;
        }
        String str7 = str3;
        if ((i2 & 8) != 0) {
            str4 = category.updatedAt;
        }
        String str8 = str4;
        if ((i2 & 16) != 0) {
            str5 = category.createdAt;
        }
        String str9 = str5;
        if ((i2 & 32) != 0) {
            i = category.priority;
        }
        return category.copy(str, str6, str7, str8, str9, i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.englishName;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final String component4() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final int component6() {
        return this.priority;
    }

    @DexIgnore
    public final Category copy(String str, String str2, String str3, String str4, String str5, int i) {
        wg6.b(str, "id");
        wg6.b(str2, "englishName");
        wg6.b(str3, "name");
        wg6.b(str4, "updatedAt");
        wg6.b(str5, "createdAt");
        return new Category(str, str2, str3, str4, str5, i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Category)) {
            return false;
        }
        Category category = (Category) obj;
        return wg6.a((Object) this.id, (Object) category.id) && wg6.a((Object) this.englishName, (Object) category.englishName) && wg6.a((Object) this.name, (Object) category.name) && wg6.a((Object) this.updatedAt, (Object) category.updatedAt) && wg6.a((Object) this.createdAt, (Object) category.createdAt) && this.priority == category.priority;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getEnglishName() {
        return this.englishName;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.englishName;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.name;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.updatedAt;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.createdAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return ((hashCode4 + i) * 31) + d.a(this.priority);
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setEnglishName(String str) {
        wg6.b(str, "<set-?>");
        this.englishName = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPriority(int i) {
        this.priority = i;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "Category(id=" + this.id + ", englishName=" + this.englishName + ", name=" + this.name + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ", priority=" + this.priority + ")";
    }
}
