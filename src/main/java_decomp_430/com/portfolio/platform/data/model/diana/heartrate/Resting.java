package com.portfolio.platform.data.model.diana.heartrate;

import com.fossil.d;
import com.fossil.wg6;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.Serializable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Resting implements Serializable {
    @DexIgnore
    public /* final */ DateTime time;
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public Resting(DateTime dateTime, int i) {
        wg6.b(dateTime, LogBuilder.KEY_TIME);
        this.time = dateTime;
        this.value = i;
    }

    @DexIgnore
    public static /* synthetic */ Resting copy$default(Resting resting, DateTime dateTime, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            dateTime = resting.time;
        }
        if ((i2 & 2) != 0) {
            i = resting.value;
        }
        return resting.copy(dateTime, i);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.time;
    }

    @DexIgnore
    public final int component2() {
        return this.value;
    }

    @DexIgnore
    public final Resting copy(DateTime dateTime, int i) {
        wg6.b(dateTime, LogBuilder.KEY_TIME);
        return new Resting(dateTime, i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Resting)) {
            return false;
        }
        Resting resting = (Resting) obj;
        return wg6.a((Object) this.time, (Object) resting.time) && this.value == resting.value;
    }

    @DexIgnore
    public final DateTime getTime() {
        return this.time;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.time;
        return ((dateTime != null ? dateTime.hashCode() : 0) * 31) + d.a(this.value);
    }

    @DexIgnore
    public String toString() {
        return "Resting(time=" + this.time + ", value=" + this.value + ")";
    }
}
