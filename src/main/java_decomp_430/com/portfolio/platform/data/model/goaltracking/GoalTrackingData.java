package com.portfolio.platform.data.model.goaltracking;

import com.fossil.d;
import com.fossil.e;
import com.fossil.vu3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingData implements Serializable {
    @DexIgnore
    @vu3("createdAt")
    public long createdAt;
    @DexIgnore
    @vu3("date")
    public Date date;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @vu3("timezoneOffset")
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    @vu3("trackedAt")
    public /* final */ DateTime trackedAt;
    @DexIgnore
    @vu3("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingData(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        wg6.b(str, "id");
        wg6.b(dateTime, "trackedAt");
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        this.id = str;
        this.trackedAt = dateTime;
        this.timezoneOffsetInSecond = i;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingData copy$default(GoalTrackingData goalTrackingData, String str, DateTime dateTime, int i, Date date2, long j, long j2, int i2, Object obj) {
        GoalTrackingData goalTrackingData2 = goalTrackingData;
        return goalTrackingData.copy((i2 & 1) != 0 ? goalTrackingData2.id : str, (i2 & 2) != 0 ? goalTrackingData2.trackedAt : dateTime, (i2 & 4) != 0 ? goalTrackingData2.timezoneOffsetInSecond : i, (i2 & 8) != 0 ? goalTrackingData2.date : date2, (i2 & 16) != 0 ? goalTrackingData2.createdAt : j, (i2 & 32) != 0 ? goalTrackingData2.updatedAt : j2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.trackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.date;
    }

    @DexIgnore
    public final long component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingData copy(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        wg6.b(str, "id");
        wg6.b(dateTime, "trackedAt");
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        return new GoalTrackingData(str, dateTime, i, date2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GoalTrackingData)) {
            return false;
        }
        GoalTrackingData goalTrackingData = (GoalTrackingData) obj;
        return wg6.a((Object) this.id, (Object) goalTrackingData.id) && wg6.a((Object) this.trackedAt, (Object) goalTrackingData.trackedAt) && this.timezoneOffsetInSecond == goalTrackingData.timezoneOffsetInSecond && wg6.a((Object) this.date, (Object) goalTrackingData.date) && this.createdAt == goalTrackingData.createdAt && this.updatedAt == goalTrackingData.updatedAt;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getTrackedAt() {
        return this.trackedAt;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        DateTime dateTime = this.trackedAt;
        int hashCode2 = (((hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31) + d.a(this.timezoneOffsetInSecond)) * 31;
        Date date2 = this.date;
        if (date2 != null) {
            i = date2.hashCode();
        }
        return ((((hashCode2 + i) * 31) + e.a(this.createdAt)) * 31) + e.a(this.updatedAt);
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        wg6.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingData(id=" + this.id + ", trackedAt=" + this.trackedAt + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
