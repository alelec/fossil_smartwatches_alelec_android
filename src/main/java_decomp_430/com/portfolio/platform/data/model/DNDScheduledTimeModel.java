package com.portfolio.platform.data.model;

import com.fossil.d;
import com.fossil.tu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDScheduledTimeModel {
    @DexIgnore
    @tu3
    public int minutes;
    @DexIgnore
    @tu3
    public String scheduledTimeName;
    @DexIgnore
    @tu3
    public int scheduledTimeType;

    @DexIgnore
    public DNDScheduledTimeModel(String str, int i, int i2) {
        wg6.b(str, "scheduledTimeName");
        this.scheduledTimeName = str;
        this.minutes = i;
        this.scheduledTimeType = i2;
    }

    @DexIgnore
    public static /* synthetic */ DNDScheduledTimeModel copy$default(DNDScheduledTimeModel dNDScheduledTimeModel, String str, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = dNDScheduledTimeModel.scheduledTimeName;
        }
        if ((i3 & 2) != 0) {
            i = dNDScheduledTimeModel.minutes;
        }
        if ((i3 & 4) != 0) {
            i2 = dNDScheduledTimeModel.scheduledTimeType;
        }
        return dNDScheduledTimeModel.copy(str, i, i2);
    }

    @DexIgnore
    public final String component1() {
        return this.scheduledTimeName;
    }

    @DexIgnore
    public final int component2() {
        return this.minutes;
    }

    @DexIgnore
    public final int component3() {
        return this.scheduledTimeType;
    }

    @DexIgnore
    public final DNDScheduledTimeModel copy(String str, int i, int i2) {
        wg6.b(str, "scheduledTimeName");
        return new DNDScheduledTimeModel(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DNDScheduledTimeModel)) {
            return false;
        }
        DNDScheduledTimeModel dNDScheduledTimeModel = (DNDScheduledTimeModel) obj;
        return wg6.a((Object) this.scheduledTimeName, (Object) dNDScheduledTimeModel.scheduledTimeName) && this.minutes == dNDScheduledTimeModel.minutes && this.scheduledTimeType == dNDScheduledTimeModel.scheduledTimeType;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getScheduledTimeName() {
        return this.scheduledTimeName;
    }

    @DexIgnore
    public final int getScheduledTimeType() {
        return this.scheduledTimeType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.scheduledTimeName;
        return ((((str != null ? str.hashCode() : 0) * 31) + d.a(this.minutes)) * 31) + d.a(this.scheduledTimeType);
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setScheduledTimeName(String str) {
        wg6.b(str, "<set-?>");
        this.scheduledTimeName = str;
    }

    @DexIgnore
    public final void setScheduledTimeType(int i) {
        this.scheduledTimeType = i;
    }

    @DexIgnore
    public String toString() {
        return "DNDScheduledTimeModel(scheduledTimeName=" + this.scheduledTimeName + ", minutes=" + this.minutes + ", scheduledTimeType=" + this.scheduledTimeType + ")";
    }
}
