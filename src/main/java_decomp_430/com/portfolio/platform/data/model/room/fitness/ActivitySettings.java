package com.portfolio.platform.data.model.room.fitness;

import com.fossil.d;
import com.fossil.pj4;
import com.fossil.vu3;
import java.util.Date;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySettings {
    @DexIgnore
    @vu3("currentGoalActiveTime")
    public int currentActiveTimeGoal;
    @DexIgnore
    @vu3("currentGoalCalories")
    public int currentCaloriesGoal;
    @DexIgnore
    @vu3("currentGoalSteps")
    public int currentStepGoal;
    @DexIgnore
    @pj4
    public int id;
    @DexIgnore
    @vu3("timezoneOffset")
    public Integer timezone; // = Integer.valueOf(TimeZone.getDefault().getOffset(new Date().getTime()) / 1000);

    @DexIgnore
    public ActivitySettings(int i, int i2, int i3) {
        this.currentStepGoal = i;
        this.currentCaloriesGoal = i2;
        this.currentActiveTimeGoal = i3;
    }

    @DexIgnore
    public static /* synthetic */ ActivitySettings copy$default(ActivitySettings activitySettings, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = activitySettings.currentStepGoal;
        }
        if ((i4 & 2) != 0) {
            i2 = activitySettings.currentCaloriesGoal;
        }
        if ((i4 & 4) != 0) {
            i3 = activitySettings.currentActiveTimeGoal;
        }
        return activitySettings.copy(i, i2, i3);
    }

    @DexIgnore
    public final int component1() {
        return this.currentStepGoal;
    }

    @DexIgnore
    public final int component2() {
        return this.currentCaloriesGoal;
    }

    @DexIgnore
    public final int component3() {
        return this.currentActiveTimeGoal;
    }

    @DexIgnore
    public final ActivitySettings copy(int i, int i2, int i3) {
        return new ActivitySettings(i, i2, i3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivitySettings)) {
            return false;
        }
        ActivitySettings activitySettings = (ActivitySettings) obj;
        return this.currentStepGoal == activitySettings.currentStepGoal && this.currentCaloriesGoal == activitySettings.currentCaloriesGoal && this.currentActiveTimeGoal == activitySettings.currentActiveTimeGoal;
    }

    @DexIgnore
    public final int getCurrentActiveTimeGoal() {
        return this.currentActiveTimeGoal;
    }

    @DexIgnore
    public final int getCurrentCaloriesGoal() {
        return this.currentCaloriesGoal;
    }

    @DexIgnore
    public final int getCurrentStepGoal() {
        return this.currentStepGoal;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final Integer getTimezone() {
        return this.timezone;
    }

    @DexIgnore
    public int hashCode() {
        return (((d.a(this.currentStepGoal) * 31) + d.a(this.currentCaloriesGoal)) * 31) + d.a(this.currentActiveTimeGoal);
    }

    @DexIgnore
    public final void setCurrentActiveTimeGoal(int i) {
        this.currentActiveTimeGoal = i;
    }

    @DexIgnore
    public final void setCurrentCaloriesGoal(int i) {
        this.currentCaloriesGoal = i;
    }

    @DexIgnore
    public final void setCurrentStepGoal(int i) {
        this.currentStepGoal = i;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setTimezone(Integer num) {
        this.timezone = num;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySettings(currentStepGoal=" + this.currentStepGoal + ", currentCaloriesGoal=" + this.currentCaloriesGoal + ", currentActiveTimeGoal=" + this.currentActiveTimeGoal + ")";
    }
}
