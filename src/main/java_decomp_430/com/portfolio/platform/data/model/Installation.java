package com.portfolio.platform.data.model;

import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Installation {
    @DexIgnore
    @vu3("appBuildNumber")
    public int appBuildNumber;
    @DexIgnore
    @vu3("appMarketingVersion")
    public String appMarketingVersion;
    @DexIgnore
    @vu3("appName")
    public String appName;
    @DexIgnore
    @vu3("appPermissions")
    public String[] appPermissions;
    @DexIgnore
    @vu3("appVersion")
    public String appVersion;
    @DexIgnore
    @vu3("badge")
    public int badge;
    @DexIgnore
    @vu3("channels")
    public String[] channels;
    @DexIgnore
    @vu3("deviceToken")
    public String deviceToken;
    @DexIgnore
    @vu3("deviceType")
    public String deviceType;
    @DexIgnore
    @vu3("gcmSenderId")
    public String gcmId;
    @DexIgnore
    @vu3("id")
    public String installationId;
    @DexIgnore
    @vu3("localeIdentifier")
    public String localeIdentifier;
    @DexIgnore
    @vu3("model")
    public String model;
    @DexIgnore
    @vu3("osVersion")
    public String osVersion;
    @DexIgnore
    @vu3("pushType")
    public String pushType;
    @DexIgnore
    @vu3("timeZone")
    public String timeZone;
    @DexIgnore
    public transient MFUser user;

    @DexIgnore
    public final int getAppBuildNumber() {
        return this.appBuildNumber;
    }

    @DexIgnore
    public final String getAppMarketingVersion() {
        return this.appMarketingVersion;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String[] getAppPermissions() {
        return this.appPermissions;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final int getBadge() {
        return this.badge;
    }

    @DexIgnore
    public final String[] getChannels() {
        return this.channels;
    }

    @DexIgnore
    public final String getDeviceToken() {
        return this.deviceToken;
    }

    @DexIgnore
    public final String getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public final String getGcmId() {
        return this.gcmId;
    }

    @DexIgnore
    public final String getInstallationId() {
        return this.installationId;
    }

    @DexIgnore
    public final String getLocaleIdentifier() {
        return this.localeIdentifier;
    }

    @DexIgnore
    public final String getModel() {
        return this.model;
    }

    @DexIgnore
    public final String getOsVersion() {
        return this.osVersion;
    }

    @DexIgnore
    public final String getPushType() {
        return this.pushType;
    }

    @DexIgnore
    public final String getTimeZone() {
        return this.timeZone;
    }

    @DexIgnore
    public final MFUser getUser() {
        return this.user;
    }

    @DexIgnore
    public final void setAppBuildNumber(int i) {
        this.appBuildNumber = i;
    }

    @DexIgnore
    public final void setAppMarketingVersion(String str) {
        this.appMarketingVersion = str;
    }

    @DexIgnore
    public final void setAppName(String str) {
        this.appName = str;
    }

    @DexIgnore
    public final void setAppPermissions(String[] strArr) {
        this.appPermissions = strArr;
    }

    @DexIgnore
    public final void setAppVersion(String str) {
        this.appVersion = str;
    }

    @DexIgnore
    public final void setBadge(int i) {
        this.badge = i;
    }

    @DexIgnore
    public final void setChannels(String[] strArr) {
        this.channels = strArr;
    }

    @DexIgnore
    public final void setDeviceToken(String str) {
        this.deviceToken = str;
    }

    @DexIgnore
    public final void setDeviceType(String str) {
        this.deviceType = str;
    }

    @DexIgnore
    public final void setGcmId(String str) {
        this.gcmId = str;
    }

    @DexIgnore
    public final void setInstallationId(String str) {
        this.installationId = str;
    }

    @DexIgnore
    public final void setLocaleIdentifier(String str) {
        this.localeIdentifier = str;
    }

    @DexIgnore
    public final void setModel(String str) {
        this.model = str;
    }

    @DexIgnore
    public final void setOsVersion(String str) {
        this.osVersion = str;
    }

    @DexIgnore
    public final void setPushType(String str) {
        this.pushType = str;
    }

    @DexIgnore
    public final void setTimeZone(String str) {
        this.timeZone = str;
    }

    @DexIgnore
    public final void setUser(MFUser mFUser) {
        this.user = mFUser;
    }
}
