package com.portfolio.platform.data.model.microapp.weather;

import com.fossil.b;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressOfWeather {
    @DexIgnore
    public /* final */ String address;
    @DexIgnore
    public int id;
    @DexIgnore
    public /* final */ double lat;
    @DexIgnore
    public /* final */ double lng;

    @DexIgnore
    public AddressOfWeather() {
        this(0.0d, 0.0d, (String) null, 7, (qg6) null);
    }

    @DexIgnore
    public AddressOfWeather(double d, double d2, String str) {
        wg6.b(str, "address");
        this.lat = d;
        this.lng = d2;
        this.address = str;
    }

    @DexIgnore
    public static /* synthetic */ AddressOfWeather copy$default(AddressOfWeather addressOfWeather, double d, double d2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            d = addressOfWeather.lat;
        }
        double d3 = d;
        if ((i & 2) != 0) {
            d2 = addressOfWeather.lng;
        }
        double d4 = d2;
        if ((i & 4) != 0) {
            str = addressOfWeather.address;
        }
        return addressOfWeather.copy(d3, d4, str);
    }

    @DexIgnore
    public final double component1() {
        return this.lat;
    }

    @DexIgnore
    public final double component2() {
        return this.lng;
    }

    @DexIgnore
    public final String component3() {
        return this.address;
    }

    @DexIgnore
    public final AddressOfWeather copy(double d, double d2, String str) {
        wg6.b(str, "address");
        return new AddressOfWeather(d, d2, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AddressOfWeather)) {
            return false;
        }
        AddressOfWeather addressOfWeather = (AddressOfWeather) obj;
        return Double.compare(this.lat, addressOfWeather.lat) == 0 && Double.compare(this.lng, addressOfWeather.lng) == 0 && wg6.a((Object) this.address, (Object) addressOfWeather.address);
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public int hashCode() {
        int a = ((b.a(this.lat) * 31) + b.a(this.lng)) * 31;
        String str = this.address;
        return a + (str != null ? str.hashCode() : 0);
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ AddressOfWeather(double d, double d2, String str, int i, qg6 qg6) {
        this(r2, (i & 2) == 0 ? d2 : r0, (i & 4) != 0 ? "" : str);
        double d3 = 0.0d;
        double d4 = (i & 1) != 0 ? 0.0d : d;
    }
}
