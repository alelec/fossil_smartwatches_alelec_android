package com.portfolio.platform.data.model;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum NotificationPriority {
    ENTOURAGE_CALL,
    APP_FILTER,
    ENTOURAGE_SMS
}
