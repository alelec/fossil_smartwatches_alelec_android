package com.portfolio.platform.data.model.diana.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.b;
import com.fossil.pj4;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherLocationWrapper implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("fullName")
    public String fullName;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    @pj4
    public boolean isEnableLocation;
    @DexIgnore
    @pj4
    public boolean isUseCurrentLocation;
    @DexIgnore
    @vu3("lat")
    public double lat;
    @DexIgnore
    @vu3("lng")
    public double lng;
    @DexIgnore
    @vu3("name")
    public String name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherLocationWrapper> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public WeatherLocationWrapper createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new WeatherLocationWrapper(parcel);
        }

        @DexIgnore
        public WeatherLocationWrapper[] newArray(int i) {
            return new WeatherLocationWrapper[i];
        }
    }

    @DexIgnore
    public WeatherLocationWrapper() {
        this((String) null, 0.0d, 0.0d, (String) null, (String) null, false, false, 127, (qg6) null);
    }

    @DexIgnore
    public WeatherLocationWrapper(String str, double d, double d2, String str2, String str3, boolean z, boolean z2) {
        wg6.b(str, "id");
        wg6.b(str2, "name");
        wg6.b(str3, "fullName");
        this.id = str;
        this.lat = d;
        this.lng = d2;
        this.name = str2;
        this.fullName = str3;
        this.isUseCurrentLocation = z;
        this.isEnableLocation = z2;
    }

    @DexIgnore
    public static /* synthetic */ WeatherLocationWrapper copy$default(WeatherLocationWrapper weatherLocationWrapper, String str, double d, double d2, String str2, String str3, boolean z, boolean z2, int i, Object obj) {
        WeatherLocationWrapper weatherLocationWrapper2 = weatherLocationWrapper;
        return weatherLocationWrapper.copy((i & 1) != 0 ? weatherLocationWrapper2.id : str, (i & 2) != 0 ? weatherLocationWrapper2.lat : d, (i & 4) != 0 ? weatherLocationWrapper2.lng : d2, (i & 8) != 0 ? weatherLocationWrapper2.name : str2, (i & 16) != 0 ? weatherLocationWrapper2.fullName : str3, (i & 32) != 0 ? weatherLocationWrapper2.isUseCurrentLocation : z, (i & 64) != 0 ? weatherLocationWrapper2.isEnableLocation : z2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final double component2() {
        return this.lat;
    }

    @DexIgnore
    public final double component3() {
        return this.lng;
    }

    @DexIgnore
    public final String component4() {
        return this.name;
    }

    @DexIgnore
    public final String component5() {
        return this.fullName;
    }

    @DexIgnore
    public final boolean component6() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final boolean component7() {
        return this.isEnableLocation;
    }

    @DexIgnore
    public final WeatherLocationWrapper copy(String str, double d, double d2, String str2, String str3, boolean z, boolean z2) {
        wg6.b(str, "id");
        String str4 = str2;
        wg6.b(str4, "name");
        String str5 = str3;
        wg6.b(str5, "fullName");
        return new WeatherLocationWrapper(str, d, d2, str4, str5, z, z2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WeatherLocationWrapper)) {
            return false;
        }
        WeatherLocationWrapper weatherLocationWrapper = (WeatherLocationWrapper) obj;
        return wg6.a((Object) this.id, (Object) weatherLocationWrapper.id) && Double.compare(this.lat, weatherLocationWrapper.lat) == 0 && Double.compare(this.lng, weatherLocationWrapper.lng) == 0 && wg6.a((Object) this.name, (Object) weatherLocationWrapper.name) && wg6.a((Object) this.fullName, (Object) weatherLocationWrapper.fullName) && this.isUseCurrentLocation == weatherLocationWrapper.isUseCurrentLocation && this.isEnableLocation == weatherLocationWrapper.isEnableLocation;
    }

    @DexIgnore
    public final String getFullName() {
        return this.fullName;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (((((str != null ? str.hashCode() : 0) * 31) + b.a(this.lat)) * 31) + b.a(this.lng)) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.fullName;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z = this.isUseCurrentLocation;
        if (z) {
            z = true;
        }
        int i3 = (i2 + (z ? 1 : 0)) * 31;
        boolean z2 = this.isEnableLocation;
        if (z2) {
            z2 = true;
        }
        return i3 + (z2 ? 1 : 0);
    }

    @DexIgnore
    public final boolean isEnableLocation() {
        return this.isEnableLocation;
    }

    @DexIgnore
    public final boolean isUseCurrentLocation() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final void setEnableLocation(boolean z) {
        this.isEnableLocation = z;
    }

    @DexIgnore
    public final void setFullName(String str) {
        wg6.b(str, "<set-?>");
        this.fullName = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLat(double d) {
        this.lat = d;
    }

    @DexIgnore
    public final void setLng(double d) {
        this.lng = d;
    }

    @DexIgnore
    public final void setName(String str) {
        wg6.b(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setUseCurrentLocation(boolean z) {
        this.isUseCurrentLocation = z;
    }

    @DexIgnore
    public String toString() {
        return "WeatherLocationWrapper(id=" + this.id + ", lat=" + this.lat + ", lng=" + this.lng + ", name=" + this.name + ", fullName=" + this.fullName + ", isUseCurrentLocation=" + this.isUseCurrentLocation + ", isEnableLocation=" + this.isEnableLocation + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeDouble(this.lat);
        parcel.writeDouble(this.lng);
        parcel.writeString(this.name);
        parcel.writeString(this.fullName);
        parcel.writeString(String.valueOf(this.isUseCurrentLocation));
        parcel.writeString(String.valueOf(this.isEnableLocation));
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ WeatherLocationWrapper(String str, double d, double d2, String str2, String str3, boolean z, boolean z2, int i, qg6 qg6) {
        this(r0, (i & 2) != 0 ? 0.0d : d, (i & 4) == 0 ? d2 : 0.0d, (i & 8) != 0 ? r1 : str2, (i & 16) == 0 ? str3 : r1, (i & 32) != 0 ? false : z, (i & 64) != 0 ? true : z2);
        String str4 = "";
        String str5 = (i & 1) != 0 ? str4 : str;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WeatherLocationWrapper(Parcel parcel) {
        this(r3, r4, r6, r8, r9, r10, r13.booleanValue());
        String str;
        String str2;
        wg6.b(parcel, "in");
        String readString = parcel.readString();
        String str3 = readString != null ? readString : "";
        double readDouble = parcel.readDouble();
        double readDouble2 = parcel.readDouble();
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
        Boolean valueOf = Boolean.valueOf(parcel.readString());
        wg6.a((Object) valueOf, "java.lang.Boolean.valueOf(`in`.readString())");
        boolean booleanValue = valueOf.booleanValue();
        Boolean valueOf2 = Boolean.valueOf(parcel.readString());
        wg6.a((Object) valueOf2, "java.lang.Boolean.valueOf(`in`.readString())");
    }
}
