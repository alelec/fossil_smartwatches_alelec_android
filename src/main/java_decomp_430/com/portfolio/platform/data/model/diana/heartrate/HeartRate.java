package com.portfolio.platform.data.model.diana.heartrate;

import com.fossil.bk4;
import com.fossil.vu3;
import com.fossil.wg6;
import com.portfolio.platform.data.model.ServerError;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRate extends ServerError {
    @DexIgnore
    @vu3("average")
    public /* final */ float mAverage;
    @DexIgnore
    @vu3("createdAt")
    public /* final */ Date mCreatedAt;
    @DexIgnore
    @vu3("date")
    public /* final */ Date mDate;
    @DexIgnore
    @vu3("endTime")
    public /* final */ String mEndTime;
    @DexIgnore
    @vu3("id")
    public /* final */ String mId;
    @DexIgnore
    @vu3("max")
    public /* final */ int mMax;
    @DexIgnore
    @vu3("min")
    public /* final */ int mMin;
    @DexIgnore
    @vu3("minuteCount")
    public /* final */ int mMinuteCount;
    @DexIgnore
    @vu3("resting")
    public /* final */ Resting mResting;
    @DexIgnore
    @vu3("startTime")
    public /* final */ String mStartTime;
    @DexIgnore
    @vu3("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @vu3("updatedAt")
    public /* final */ Date mUpdatedAt;

    @DexIgnore
    public HeartRate(String str, float f, Date date, Date date2, Date date3, String str2, String str3, int i, int i2, int i3, int i4, Resting resting) {
        wg6.b(str, "mId");
        wg6.b(date, "mDate");
        wg6.b(date2, "mCreatedAt");
        wg6.b(date3, "mUpdatedAt");
        wg6.b(str2, "mEndTime");
        wg6.b(str3, "mStartTime");
        this.mId = str;
        this.mAverage = f;
        this.mDate = date;
        this.mCreatedAt = date2;
        this.mUpdatedAt = date3;
        this.mEndTime = str2;
        this.mStartTime = str3;
        this.mTimeZoneOffsetInSecond = i;
        this.mMin = i2;
        this.mMax = i3;
        this.mMinuteCount = i4;
        this.mResting = resting;
    }

    @DexIgnore
    public final HeartRateSample toHeartRateSample() {
        Date date;
        Date date2;
        Date date3;
        try {
            Calendar instance = Calendar.getInstance();
            date = bk4.a(this.mTimeZoneOffsetInSecond, this.mStartTime);
            try {
                wg6.a((Object) instance, "calendar");
                instance.setTime(date);
                instance.set(13, 0);
                instance.set(14, 0);
                date = instance.getTime();
                date3 = bk4.a(this.mTimeZoneOffsetInSecond, this.mEndTime);
            } catch (ParseException e) {
                e = e;
                date3 = null;
                e.printStackTrace();
                date2 = date3;
                String str = this.mId;
                float f = this.mAverage;
                Date date4 = this.mDate;
                long time = this.mCreatedAt.getTime();
                long time2 = this.mUpdatedAt.getTime();
                DateTime a = bk4.a(date2, this.mTimeZoneOffsetInSecond);
                wg6.a((Object) a, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime a2 = bk4.a(date, this.mTimeZoneOffsetInSecond);
                wg6.a((Object) a2, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str, f, date4, time, time2, a, a2, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
            try {
                instance.setTime(date3);
                instance.set(13, 0);
                instance.set(14, 0);
                date2 = instance.getTime();
            } catch (ParseException e2) {
                e = e2;
                e.printStackTrace();
                date2 = date3;
                String str2 = this.mId;
                float f2 = this.mAverage;
                Date date42 = this.mDate;
                long time3 = this.mCreatedAt.getTime();
                long time22 = this.mUpdatedAt.getTime();
                DateTime a3 = bk4.a(date2, this.mTimeZoneOffsetInSecond);
                wg6.a((Object) a3, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime a22 = bk4.a(date, this.mTimeZoneOffsetInSecond);
                wg6.a((Object) a22, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str2, f2, date42, time3, time22, a3, a22, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
        } catch (ParseException e3) {
            e = e3;
            date = null;
            date3 = null;
            e.printStackTrace();
            date2 = date3;
            String str22 = this.mId;
            float f22 = this.mAverage;
            Date date422 = this.mDate;
            long time32 = this.mCreatedAt.getTime();
            long time222 = this.mUpdatedAt.getTime();
            DateTime a32 = bk4.a(date2, this.mTimeZoneOffsetInSecond);
            wg6.a((Object) a32, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime a222 = bk4.a(date, this.mTimeZoneOffsetInSecond);
            wg6.a((Object) a222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str22, f22, date422, time32, time222, a32, a222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        }
        try {
            String str222 = this.mId;
            float f222 = this.mAverage;
            Date date4222 = this.mDate;
            long time322 = this.mCreatedAt.getTime();
            long time2222 = this.mUpdatedAt.getTime();
            DateTime a322 = bk4.a(date2, this.mTimeZoneOffsetInSecond);
            wg6.a((Object) a322, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime a2222 = bk4.a(date, this.mTimeZoneOffsetInSecond);
            wg6.a((Object) a2222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str222, f222, date4222, time322, time2222, a322, a2222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }
}
