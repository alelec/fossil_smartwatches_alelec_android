package com.portfolio.platform.data.model.sleep;

import com.fossil.d;
import com.fossil.wg6;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionData {
    @DexIgnore
    public int durationInMinutes;
    @DexIgnore
    public String endTimeString;
    @DexIgnore
    public List<WrapperSleepStateChange> sleepStates;
    @DexIgnore
    public String startTimeString;

    @DexIgnore
    public SleepSessionData(int i, String str, String str2, List<WrapperSleepStateChange> list) {
        wg6.b(str, "startTimeString");
        wg6.b(str2, "endTimeString");
        wg6.b(list, "sleepStates");
        this.durationInMinutes = i;
        this.startTimeString = str;
        this.endTimeString = str2;
        this.sleepStates = list;
    }

    @DexIgnore
    public static /* synthetic */ SleepSessionData copy$default(SleepSessionData sleepSessionData, int i, String str, String str2, List<WrapperSleepStateChange> list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = sleepSessionData.durationInMinutes;
        }
        if ((i2 & 2) != 0) {
            str = sleepSessionData.startTimeString;
        }
        if ((i2 & 4) != 0) {
            str2 = sleepSessionData.endTimeString;
        }
        if ((i2 & 8) != 0) {
            list = sleepSessionData.sleepStates;
        }
        return sleepSessionData.copy(i, str, str2, list);
    }

    @DexIgnore
    public final int component1() {
        return this.durationInMinutes;
    }

    @DexIgnore
    public final String component2() {
        return this.startTimeString;
    }

    @DexIgnore
    public final String component3() {
        return this.endTimeString;
    }

    @DexIgnore
    public final List<WrapperSleepStateChange> component4() {
        return this.sleepStates;
    }

    @DexIgnore
    public final SleepSessionData copy(int i, String str, String str2, List<WrapperSleepStateChange> list) {
        wg6.b(str, "startTimeString");
        wg6.b(str2, "endTimeString");
        wg6.b(list, "sleepStates");
        return new SleepSessionData(i, str, str2, list);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepSessionData)) {
            return false;
        }
        SleepSessionData sleepSessionData = (SleepSessionData) obj;
        return this.durationInMinutes == sleepSessionData.durationInMinutes && wg6.a((Object) this.startTimeString, (Object) sleepSessionData.startTimeString) && wg6.a((Object) this.endTimeString, (Object) sleepSessionData.endTimeString) && wg6.a((Object) this.sleepStates, (Object) sleepSessionData.sleepStates);
    }

    @DexIgnore
    public final int getDurationInMinutes() {
        return this.durationInMinutes;
    }

    @DexIgnore
    public final String getEndTimeString() {
        return this.endTimeString;
    }

    @DexIgnore
    public final List<WrapperSleepStateChange> getSleepStates() {
        return this.sleepStates;
    }

    @DexIgnore
    public final String getStartTimeString() {
        return this.startTimeString;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.durationInMinutes) * 31;
        String str = this.startTimeString;
        int i = 0;
        int hashCode = (a + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.endTimeString;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        List<WrapperSleepStateChange> list = this.sleepStates;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setDurationInMinutes(int i) {
        this.durationInMinutes = i;
    }

    @DexIgnore
    public final void setEndTimeString(String str) {
        wg6.b(str, "<set-?>");
        this.endTimeString = str;
    }

    @DexIgnore
    public final void setSleepStates(List<WrapperSleepStateChange> list) {
        wg6.b(list, "<set-?>");
        this.sleepStates = list;
    }

    @DexIgnore
    public final void setStartTimeString(String str) {
        wg6.b(str, "<set-?>");
        this.startTimeString = str;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionData(durationInMinutes=" + this.durationInMinutes + ", startTimeString=" + this.startTimeString + ", endTimeString=" + this.endTimeString + ", sleepStates=" + this.sleepStates + ")";
    }
}
