package com.portfolio.platform.data.model.room.microapp;

import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeclarationFile {
    @DexIgnore
    public String appId;
    @DexIgnore
    @vu3("content")
    public /* final */ String content;
    @DexIgnore
    @vu3("description")
    public /* final */ String description;
    @DexIgnore
    @vu3("id")
    public /* final */ String id;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String variantName;

    @DexIgnore
    public DeclarationFile(String str, String str2, String str3) {
        wg6.b(str, "id");
        this.id = str;
        this.description = str2;
        this.content = str3;
    }

    @DexIgnore
    public static /* synthetic */ DeclarationFile copy$default(DeclarationFile declarationFile, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = declarationFile.id;
        }
        if ((i & 2) != 0) {
            str2 = declarationFile.description;
        }
        if ((i & 4) != 0) {
            str3 = declarationFile.content;
        }
        return declarationFile.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.description;
    }

    @DexIgnore
    public final String component3() {
        return this.content;
    }

    @DexIgnore
    public final DeclarationFile copy(String str, String str2, String str3) {
        wg6.b(str, "id");
        return new DeclarationFile(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DeclarationFile)) {
            return false;
        }
        DeclarationFile declarationFile = (DeclarationFile) obj;
        return wg6.a((Object) this.id, (Object) declarationFile.id) && wg6.a((Object) this.description, (Object) declarationFile.description) && wg6.a((Object) this.content, (Object) declarationFile.content);
    }

    @DexIgnore
    public final String getAppId() {
        String str = this.appId;
        if (str != null) {
            return str;
        }
        wg6.d("appId");
        throw null;
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getSerialNumber() {
        String str = this.serialNumber;
        if (str != null) {
            return str;
        }
        wg6.d("serialNumber");
        throw null;
    }

    @DexIgnore
    public final String getVariantName() {
        String str = this.variantName;
        if (str != null) {
            return str;
        }
        wg6.d("variantName");
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.description;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.content;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        wg6.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        wg6.b(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setVariantName(String str) {
        wg6.b(str, "<set-?>");
        this.variantName = str;
    }

    @DexIgnore
    public String toString() {
        return "DeclarationFile(id=" + this.id + ", description=" + this.description + ", content=" + this.content + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DeclarationFile(String str, String str2, String str3, int i, qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? "" : str3);
    }
}
