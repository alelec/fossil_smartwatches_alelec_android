package com.portfolio.platform.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpSocialAuth implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("acceptedLocationDataSharing")
    public ArrayList<String> acceptedLocationDataSharing;
    @DexIgnore
    @vu3("acceptedPrivacies")
    public ArrayList<String> acceptedPrivacies;
    @DexIgnore
    @vu3("acceptedTermsOfService")
    public ArrayList<String> acceptedTermsOfService;
    @DexIgnore
    @vu3("birthday")
    public String birthday;
    @DexIgnore
    @vu3("clientId")
    public String clientId;
    @DexIgnore
    @vu3("diagnosticEnabled")
    public boolean diagnosticEnabled;
    @DexIgnore
    @vu3("email")
    public String email;
    @DexIgnore
    @vu3("firstName")
    public String firstName;
    @DexIgnore
    @vu3("gender")
    public String gender;
    @DexIgnore
    @vu3("lastName")
    public String lastName;
    @DexIgnore
    @vu3("service")
    public String service;
    @DexIgnore
    @vu3("token")
    public String token;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<SignUpSocialAuth> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public SignUpSocialAuth createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new SignUpSocialAuth(parcel);
        }

        @DexIgnore
        public SignUpSocialAuth[] newArray(int i) {
            return new SignUpSocialAuth[i];
        }
    }

    @DexIgnore
    public SignUpSocialAuth(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        wg6.b(str, "email");
        wg6.b(str2, Constants.SERVICE);
        wg6.b(str3, "token");
        wg6.b(str4, "clientId");
        wg6.b(str5, "firstName");
        wg6.b(str6, "lastName");
        wg6.b(str7, "birthday");
        wg6.b(str8, "gender");
        wg6.b(arrayList, "acceptedLocationDataSharing");
        wg6.b(arrayList2, "acceptedPrivacies");
        wg6.b(arrayList3, "acceptedTermsOfService");
        this.email = str;
        this.service = str2;
        this.token = str3;
        this.clientId = str4;
        this.firstName = str5;
        this.lastName = str6;
        this.birthday = str7;
        this.gender = str8;
        this.diagnosticEnabled = z;
        this.acceptedLocationDataSharing = arrayList;
        this.acceptedPrivacies = arrayList2;
        this.acceptedTermsOfService = arrayList3;
    }

    @DexIgnore
    public static /* synthetic */ SignUpSocialAuth copy$default(SignUpSocialAuth signUpSocialAuth, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, int i, Object obj) {
        SignUpSocialAuth signUpSocialAuth2 = signUpSocialAuth;
        int i2 = i;
        return signUpSocialAuth.copy((i2 & 1) != 0 ? signUpSocialAuth2.email : str, (i2 & 2) != 0 ? signUpSocialAuth2.service : str2, (i2 & 4) != 0 ? signUpSocialAuth2.token : str3, (i2 & 8) != 0 ? signUpSocialAuth2.clientId : str4, (i2 & 16) != 0 ? signUpSocialAuth2.firstName : str5, (i2 & 32) != 0 ? signUpSocialAuth2.lastName : str6, (i2 & 64) != 0 ? signUpSocialAuth2.birthday : str7, (i2 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? signUpSocialAuth2.gender : str8, (i2 & 256) != 0 ? signUpSocialAuth2.diagnosticEnabled : z, (i2 & 512) != 0 ? signUpSocialAuth2.acceptedLocationDataSharing : arrayList, (i2 & 1024) != 0 ? signUpSocialAuth2.acceptedPrivacies : arrayList2, (i2 & 2048) != 0 ? signUpSocialAuth2.acceptedTermsOfService : arrayList3);
    }

    @DexIgnore
    public final String component1() {
        return this.email;
    }

    @DexIgnore
    public final ArrayList<String> component10() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> component11() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> component12() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String component2() {
        return this.service;
    }

    @DexIgnore
    public final String component3() {
        return this.token;
    }

    @DexIgnore
    public final String component4() {
        return this.clientId;
    }

    @DexIgnore
    public final String component5() {
        return this.firstName;
    }

    @DexIgnore
    public final String component6() {
        return this.lastName;
    }

    @DexIgnore
    public final String component7() {
        return this.birthday;
    }

    @DexIgnore
    public final String component8() {
        return this.gender;
    }

    @DexIgnore
    public final boolean component9() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final SignUpSocialAuth copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, boolean z, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        wg6.b(str, "email");
        String str9 = str2;
        wg6.b(str9, Constants.SERVICE);
        String str10 = str3;
        wg6.b(str10, "token");
        String str11 = str4;
        wg6.b(str11, "clientId");
        String str12 = str5;
        wg6.b(str12, "firstName");
        String str13 = str6;
        wg6.b(str13, "lastName");
        String str14 = str7;
        wg6.b(str14, "birthday");
        String str15 = str8;
        wg6.b(str15, "gender");
        ArrayList<String> arrayList4 = arrayList;
        wg6.b(arrayList4, "acceptedLocationDataSharing");
        ArrayList<String> arrayList5 = arrayList2;
        wg6.b(arrayList5, "acceptedPrivacies");
        ArrayList<String> arrayList6 = arrayList3;
        wg6.b(arrayList6, "acceptedTermsOfService");
        return new SignUpSocialAuth(str, str9, str10, str11, str12, str13, str14, str15, z, arrayList4, arrayList5, arrayList6);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SignUpSocialAuth)) {
            return false;
        }
        SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) obj;
        return wg6.a((Object) this.email, (Object) signUpSocialAuth.email) && wg6.a((Object) this.service, (Object) signUpSocialAuth.service) && wg6.a((Object) this.token, (Object) signUpSocialAuth.token) && wg6.a((Object) this.clientId, (Object) signUpSocialAuth.clientId) && wg6.a((Object) this.firstName, (Object) signUpSocialAuth.firstName) && wg6.a((Object) this.lastName, (Object) signUpSocialAuth.lastName) && wg6.a((Object) this.birthday, (Object) signUpSocialAuth.birthday) && wg6.a((Object) this.gender, (Object) signUpSocialAuth.gender) && this.diagnosticEnabled == signUpSocialAuth.diagnosticEnabled && wg6.a((Object) this.acceptedLocationDataSharing, (Object) signUpSocialAuth.acceptedLocationDataSharing) && wg6.a((Object) this.acceptedPrivacies, (Object) signUpSocialAuth.acceptedPrivacies) && wg6.a((Object) this.acceptedTermsOfService, (Object) signUpSocialAuth.acceptedTermsOfService);
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedLocationDataSharing() {
        return this.acceptedLocationDataSharing;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedPrivacies() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final ArrayList<String> getAcceptedTermsOfService() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String getBirthday() {
        return this.birthday;
    }

    @DexIgnore
    public final String getClientId() {
        return this.clientId;
    }

    @DexIgnore
    public final boolean getDiagnosticEnabled() {
        return this.diagnosticEnabled;
    }

    @DexIgnore
    public final String getEmail() {
        return this.email;
    }

    @DexIgnore
    public final String getFirstName() {
        return this.firstName;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getLastName() {
        return this.lastName;
    }

    @DexIgnore
    public final String getService() {
        return this.service;
    }

    @DexIgnore
    public final String getToken() {
        return this.token;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.email;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.service;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.token;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.clientId;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.firstName;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.lastName;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.birthday;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.gender;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        boolean z = this.diagnosticEnabled;
        if (z) {
            z = true;
        }
        int i2 = (hashCode8 + (z ? 1 : 0)) * 31;
        ArrayList<String> arrayList = this.acceptedLocationDataSharing;
        int hashCode9 = (i2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<String> arrayList2 = this.acceptedPrivacies;
        int hashCode10 = (hashCode9 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        ArrayList<String> arrayList3 = this.acceptedTermsOfService;
        if (arrayList3 != null) {
            i = arrayList3.hashCode();
        }
        return hashCode10 + i;
    }

    @DexIgnore
    public final void setAcceptedLocationDataSharing(ArrayList<String> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.acceptedLocationDataSharing = arrayList;
    }

    @DexIgnore
    public final void setAcceptedPrivacies(ArrayList<String> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.acceptedPrivacies = arrayList;
    }

    @DexIgnore
    public final void setAcceptedTermsOfService(ArrayList<String> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.acceptedTermsOfService = arrayList;
    }

    @DexIgnore
    public final void setBirthday(String str) {
        wg6.b(str, "<set-?>");
        this.birthday = str;
    }

    @DexIgnore
    public final void setClientId(String str) {
        wg6.b(str, "<set-?>");
        this.clientId = str;
    }

    @DexIgnore
    public final void setDiagnosticEnabled(boolean z) {
        this.diagnosticEnabled = z;
    }

    @DexIgnore
    public final void setEmail(String str) {
        wg6.b(str, "<set-?>");
        this.email = str;
    }

    @DexIgnore
    public final void setFirstName(String str) {
        wg6.b(str, "<set-?>");
        this.firstName = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        wg6.b(str, "<set-?>");
        this.gender = str;
    }

    @DexIgnore
    public final void setLastName(String str) {
        wg6.b(str, "<set-?>");
        this.lastName = str;
    }

    @DexIgnore
    public final void setService(String str) {
        wg6.b(str, "<set-?>");
        this.service = str;
    }

    @DexIgnore
    public final void setToken(String str) {
        wg6.b(str, "<set-?>");
        this.token = str;
    }

    @DexIgnore
    public String toString() {
        return "SignUpSocialAuth(email=" + this.email + ", service=" + this.service + ", token=" + this.token + ", clientId=" + this.clientId + ", firstName=" + this.firstName + ", lastName=" + this.lastName + ", birthday=" + this.birthday + ", gender=" + this.gender + ", diagnosticEnabled=" + this.diagnosticEnabled + ", acceptedLocationDataSharing=" + this.acceptedLocationDataSharing + ", acceptedPrivacies=" + this.acceptedPrivacies + ", acceptedTermsOfService=" + this.acceptedTermsOfService + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.email);
        parcel.writeString(this.service);
        parcel.writeString(this.token);
        parcel.writeString(this.clientId);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.birthday);
        parcel.writeString(this.gender);
        parcel.writeByte(this.diagnosticEnabled ? (byte) 1 : 0);
        parcel.writeStringList(this.acceptedLocationDataSharing);
        parcel.writeStringList(this.acceptedPrivacies);
        parcel.writeStringList(this.acceptedTermsOfService);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SignUpSocialAuth(Parcel parcel) {
        this(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15);
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        String str8 = readString != null ? readString : "";
        String readString2 = parcel.readString();
        if (readString2 != null) {
            str = readString2;
        } else {
            str = "";
        }
        String readString3 = parcel.readString();
        if (readString3 != null) {
            str2 = readString3;
        } else {
            str2 = "";
        }
        String readString4 = parcel.readString();
        if (readString4 != null) {
            str3 = readString4;
        } else {
            str3 = "";
        }
        String readString5 = parcel.readString();
        if (readString5 != null) {
            str4 = readString5;
        } else {
            str4 = "";
        }
        String readString6 = parcel.readString();
        if (readString6 != null) {
            str5 = readString6;
        } else {
            str5 = "";
        }
        String readString7 = parcel.readString();
        if (readString7 != null) {
            str6 = readString7;
        } else {
            str6 = "";
        }
        String readString8 = parcel.readString();
        if (readString8 != null) {
            str7 = readString8;
        } else {
            str7 = "";
        }
        boolean z = parcel.readByte() != ((byte) 0);
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        if (createStringArrayList != null) {
            ArrayList<String> createStringArrayList2 = parcel.createStringArrayList();
            if (createStringArrayList2 != null) {
                ArrayList<String> createStringArrayList3 = parcel.createStringArrayList();
                if (createStringArrayList3 != null) {
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */");
            }
            throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */");
        }
        throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<kotlin.String> /* = java.util.ArrayList<kotlin.String> */");
    }

    @DexIgnore
    public SignUpSocialAuth() {
        this("", "", "", "", "", "", "", "", false, new ArrayList(), new ArrayList(), new ArrayList());
    }
}
