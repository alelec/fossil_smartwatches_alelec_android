package com.portfolio.platform.service.fcm;

import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilFirebaseInstanceIDService extends FirebaseMessagingService {
    @DexIgnore
    public an4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService$sendRegistrationToServer$1", f = "FossilFirebaseInstanceIDService.kt", l = {69}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        public b(xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                PortfolioApp instance = PortfolioApp.get.instance();
                this.L$0 = il6;
                this.label = 1;
                if (instance.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "refreshedToken");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FossilFirebaseInstanceIDService", "Refreshed token: " + str);
        an4 an4 = this.g;
        if (an4 != null) {
            an4.r(str);
            c(str);
            return;
        }
        wg6.d("mSharedPrefs");
        throw null;
    }

    @DexIgnore
    public final rm6 c(String str) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b((xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.app.Service, com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService] */
    public void onCreate() {
        FossilFirebaseInstanceIDService.super.onCreate();
        FLogger.INSTANCE.getLocal().d("FossilFirebaseInstanceIDService", "onCreate()");
        PortfolioApp.get.instance().g().a((FossilFirebaseInstanceIDService) this);
    }
}
