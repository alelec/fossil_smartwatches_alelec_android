package com.portfolio.platform.service.notification;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.lq4;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wj6;
import com.fossil.xe6;
import com.fossil.yf6;
import com.fossil.yj6;
import com.fossil.zl6;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import com.portfolio.platform.manager.LightAndHapticsManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridMessageNotificationComponent {
    @DexIgnore
    public /* final */ SparseArray<lq4.c> a; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public b c; // = new b(0, (String) null, 0, 7, (qg6) null);
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<lq4.c> f; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ String[] g;
    @DexIgnore
    public String h; // = "date DESC LIMIT 1";
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Runnable j; // = new e(this);
    @DexIgnore
    public /* final */ Runnable k; // = new f(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public b() {
            this(0, (String) null, 0, 7, (qg6) null);
        }

        @DexIgnore
        public b(long j, String str, long j2) {
            wg6.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.a = j;
            this.b = str;
            this.c = j2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final long b() {
            return this.c;
        }

        @DexIgnore
        public final long c() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && wg6.a((Object) this.b, (Object) bVar.b) && this.c == bVar.c;
        }

        @DexIgnore
        public int hashCode() {
            int a2 = com.fossil.e.a(this.a) * 31;
            String str = this.b;
            return ((a2 + (str != null ? str.hashCode() : 0)) * 31) + com.fossil.e.a(this.c);
        }

        @DexIgnore
        public String toString() {
            return "InboxMessage(id=" + this.a + ", sender=" + this.b + ", dateSent=" + this.c + ")";
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(long j, String str, long j2, int i, qg6 qg6) {
            this(r2, (i & 2) != 0 ? "" : str, (i & 4) != 0 ? 0 : j2);
            long j3 = (i & 1) != 0 ? 0 : j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public c(String str, String str2, boolean z) {
            wg6.b(str, "packageName");
            wg6.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.a = str;
            this.b = str2;
            this.c = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return wg6.a((Object) this.a, (Object) cVar.a) && wg6.a((Object) this.b, (Object) cVar.b) && this.c == cVar.c;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            return i2 + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "MessageNotification(packageName=" + this.a + ", sender=" + this.b + ", shouldCheckAppAndAllText=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$1", f = "HybridMessageNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridMessageNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HybridMessageNotificationComponent hybridMessageNotificationComponent, StatusBarNotification statusBarNotification, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridMessageNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$sbn, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r10v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x012b A[SYNTHETIC, Splitter:B:45:0x012b] */
        public final Object invokeSuspend(Object obj) {
            boolean z;
            String tag;
            CharSequence charSequence;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (this.this$0.b(this.$sbn)) {
                    HybridMessageNotificationComponent hybridMessageNotificationComponent = this.this$0;
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    b a = hybridMessageNotificationComponent.b(applicationContext, this.this$0.d);
                    if (a != null) {
                        String a2 = a.a();
                        long b = a.b();
                        if (TextUtils.isEmpty(a2) || this.this$0.c.c() >= b) {
                            NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                            String packageName = this.$sbn.getPackageName();
                            wg6.a((Object) packageName, "sbn.packageName");
                            if (notificationAppHelper.a(packageName)) {
                                HybridMessageNotificationComponent hybridMessageNotificationComponent2 = this.this$0;
                                String packageName2 = this.$sbn.getPackageName();
                                wg6.a((Object) packageName2, "sbn.packageName");
                                hybridMessageNotificationComponent2.a("", packageName2, true);
                            }
                        } else {
                            z = false;
                            for (String str : yj6.a((CharSequence) a2, new String[]{" "}, false, 0, 6, (Object) null)) {
                                if (str != null) {
                                    Long c = wj6.c(yj6.d(str).toString());
                                    if (c != null) {
                                        long longValue = c.longValue();
                                        HybridMessageNotificationComponent hybridMessageNotificationComponent3 = this.this$0;
                                        Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                                        wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                                        List a3 = hybridMessageNotificationComponent3.a(applicationContext2, longValue);
                                        int i = 0;
                                        for (Object next : a3) {
                                            int i2 = i + 1;
                                            if (i >= 0) {
                                                String str2 = (String) next;
                                                int intValue = hf6.a(i).intValue();
                                                if (!TextUtils.isEmpty(str2)) {
                                                    this.this$0.c = a;
                                                    HybridMessageNotificationComponent hybridMessageNotificationComponent4 = this.this$0;
                                                    String packageName3 = this.$sbn.getPackageName();
                                                    wg6.a((Object) packageName3, "sbn.packageName");
                                                    hybridMessageNotificationComponent4.a(str2, packageName3, intValue == a3.size() - 1);
                                                    z = true;
                                                }
                                                i = i2;
                                            } else {
                                                qd6.c();
                                                throw null;
                                            }
                                        }
                                        continue;
                                    }
                                } else {
                                    throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                                }
                            }
                            if (!z && (tag = this.$sbn.getTag()) != null && yj6.a((CharSequence) tag, (CharSequence) "one_to_one", true)) {
                                String packageName4 = this.$sbn.getPackageName();
                                charSequence = this.$sbn.getNotification().tickerText;
                                if (charSequence != null) {
                                    try {
                                        int length = charSequence.length();
                                        int i3 = 0;
                                        while (true) {
                                            if (i3 >= length) {
                                                i3 = -1;
                                                break;
                                            } else if (hf6.a(wg6.a((Object) String.valueOf(hf6.a(charSequence.charAt(i3)).charValue()), (Object) ":")).booleanValue()) {
                                                break;
                                            } else {
                                                i3++;
                                            }
                                        }
                                        if (i3 != -1) {
                                            String obj2 = charSequence.subSequence(0, i3).toString();
                                            if (obj2 != null) {
                                                packageName4 = yj6.d(obj2).toString();
                                            } else {
                                                throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                                            }
                                        }
                                    } catch (Exception e) {
                                        FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", e.getMessage());
                                    }
                                }
                                HybridMessageNotificationComponent hybridMessageNotificationComponent5 = this.this$0;
                                wg6.a((Object) packageName4, "contact");
                                String packageName5 = this.$sbn.getPackageName();
                                wg6.a((Object) packageName5, "sbn.packageName");
                                hybridMessageNotificationComponent5.a(packageName4, packageName5, true);
                            }
                        }
                    }
                    z = false;
                    String packageName42 = this.$sbn.getPackageName();
                    charSequence = this.$sbn.getNotification().tickerText;
                    if (charSequence != null) {
                    }
                    HybridMessageNotificationComponent hybridMessageNotificationComponent52 = this.this$0;
                    wg6.a((Object) packageName42, "contact");
                    String packageName52 = this.$sbn.getPackageName();
                    wg6.a((Object) packageName52, "sbn.packageName");
                    hybridMessageNotificationComponent52.a(packageName42, packageName52, true);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HybridMessageNotificationComponent a;

        @DexIgnore
        public e(HybridMessageNotificationComponent hybridMessageNotificationComponent) {
            this.a = hybridMessageNotificationComponent;
        }

        @DexIgnore
        public final void run() {
            if (this.a.a.size() == 0) {
                this.a.d();
                return;
            }
            this.a.a.clear();
            this.a.b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ HybridMessageNotificationComponent a;

        @DexIgnore
        public f(HybridMessageNotificationComponent hybridMessageNotificationComponent) {
            this.a = hybridMessageNotificationComponent;
        }

        @DexIgnore
        public final void run() {
            if (this.a.f.isEmpty()) {
                this.a.e();
                return;
            }
            this.a.a();
            this.a.c();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HybridMessageNotificationComponent() {
        this.d = TextUtils.equals(Build.MANUFACTURER, "samsung") ? "display_recipient_ids" : "recipient_ids";
        this.g = new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, HardwareLog.COLUMN_DATE, this.d};
    }

    @DexIgnore
    public final void b() {
        d();
        this.b.postDelayed(this.j, ButtonService.CONNECT_TIMEOUT);
    }

    @DexIgnore
    public final void c() {
        this.i = true;
        this.e.postDelayed(this.k, 500);
    }

    @DexIgnore
    public final void d() {
        this.b.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    public final void e() {
        this.i = false;
        this.e.removeCallbacksAndMessages((Object) null);
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification) {
        String tag = statusBarNotification.getTag();
        if (tag == null || (!yj6.a((CharSequence) tag, (CharSequence) "sms", true) && !yj6.a((CharSequence) tag, (CharSequence) "mms", true) && !yj6.a((CharSequence) tag, (CharSequence) "rcs", true))) {
            return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
        }
        return true;
    }

    @DexIgnore
    public final rm6 a(StatusBarNotification statusBarNotification) {
        wg6.b(statusBarNotification, "sbn");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new d(this, statusBarNotification, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", "processMessage()");
        c poll = this.f.poll();
        if (poll != null) {
            LightAndHapticsManager.i.a().a(new NotificationInfo(NotificationSource.TEXT, poll.b(), "", poll.a()), poll.c());
            this.a.put(poll.hashCode(), poll);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0056, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.fossil.yf6.a(r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005a, code lost:
        throw r1;
     */
    @DexIgnore
    public final b b(Context context, String str) {
        try {
            Cursor query = context.getContentResolver().query(Uri.parse("content://mms-sms/conversations?simple=true"), this.g, (String) null, (String[]) null, this.h);
            if (query != null) {
                if (query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex(str));
                    long j2 = query.getLong(query.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
                    long j3 = query.getLong(query.getColumnIndex(HardwareLog.COLUMN_DATE));
                    query.close();
                    wg6.a((Object) string, "recipientIds");
                    b bVar = new b(j2, string, j3);
                    yf6.a(query, (Throwable) null);
                    return bVar;
                }
                query.close();
                cd6 cd6 = cd6.a;
                yf6.a(query, (Throwable) null);
            }
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().e("HybridMessageNotificationComponent", e2.getMessage());
        }
        return null;
    }

    @DexIgnore
    public final void a(String str, String str2, boolean z) {
        if (this.a.size() == 0) {
            b();
        }
        c cVar = new c(str2, str, z);
        if (this.a.indexOfKey(cVar.hashCode()) < 0) {
            this.f.offer(cVar);
            if (!this.i) {
                c();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.fossil.yf6.a(r11, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004d, code lost:
        throw r12;
     */
    @DexIgnore
    public final List<String> a(Context context, long j2) {
        ArrayList arrayList = new ArrayList();
        try {
            Cursor query = context.getContentResolver().query(ContentUris.withAppendedId(Uri.parse("content://mms-sms/canonical-address"), j2), (String[]) null, (String) null, (String[]) null, (String) null);
            if (query != null) {
                if (query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex("address"));
                    if (!TextUtils.isEmpty(string)) {
                        wg6.a((Object) string, "address");
                        arrayList.addAll(a(context, string));
                    }
                    query.close();
                }
                cd6 cd6 = cd6.a;
                yf6.a(query, (Throwable) null);
            }
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().e("HybridMessageNotificationComponent", e2.getMessage());
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        com.fossil.yf6.a(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        throw r1;
     */
    @DexIgnore
    public final List<String> a(Context context, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            Cursor query = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name", "normalized_number"}, (String) null, (String[]) null, (String) null);
            if (query != null) {
                if (query.moveToFirst()) {
                    do {
                        String string = query.getString(query.getColumnIndex("normalized_number"));
                        String string2 = query.getString(query.getColumnIndex("display_name"));
                        if (string2 != null) {
                            string = string2;
                        }
                        wg6.a((Object) string, "name");
                        arrayList.add(string);
                    } while (query.moveToNext());
                } else {
                    arrayList.add(str);
                }
                query.close();
                cd6 cd6 = cd6.a;
                yf6.a(query, (Throwable) null);
            }
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().e("HybridMessageNotificationComponent", e2.getMessage());
        }
        return arrayList;
    }
}
