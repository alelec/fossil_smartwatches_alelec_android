package com.portfolio.platform.service.microapp;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import com.fossil.af6;
import com.fossil.aw2;
import com.fossil.bw2;
import com.fossil.cd6;
import com.fossil.cq4;
import com.fossil.cw2;
import com.fossil.eq4;
import com.fossil.ff6;
import com.fossil.gw2;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.kc3;
import com.fossil.lc3;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mc3;
import com.fossil.nc6;
import com.fossil.qc3;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sv1;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.wv2;
import com.fossil.xe6;
import com.fossil.xs4;
import com.fossil.yv2;
import com.fossil.zl6;
import com.fossil.zv2;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.gson.Gson;
import com.google.maps.model.TravelMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeETAMicroAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.CommuteTimeTravelMicroAppResponse;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeService extends cq4 implements zv2, LocationListener {
    @DexIgnore
    public static /* final */ a A; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ int x; // = 120000;
    @DexIgnore
    public static /* final */ int y; // = y;
    @DexIgnore
    public static /* final */ float z; // = 50.0f;
    @DexIgnore
    public Location d;
    @DexIgnore
    public Location e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public String g;
    @DexIgnore
    public long h;
    @DexIgnore
    public CommuteTimeSetting i;
    @DexIgnore
    public LocationManager j;
    @DexIgnore
    public String o;
    @DexIgnore
    public Handler p;
    @DexIgnore
    public wv2 q;
    @DexIgnore
    public gw2 r;
    @DexIgnore
    public LocationRequest s;
    @DexIgnore
    public bw2 t;
    @DexIgnore
    public yv2 u;
    @DexIgnore
    public xs4 v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CommuteTimeService.w;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, Bundle bundle, eq4 eq4) {
            wg6.b(context, "context");
            wg6.b(bundle, Mapping.COLUMN_EXTRA_INFO);
            wg6.b(eq4, "listener");
            Intent intent = new Intent(context, CommuteTimeService.class);
            intent.putExtras(bundle);
            context.startService(intent);
            cq4.a(eq4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yv2 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public b(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public void onLocationResult(LocationResult locationResult) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "onLocationResult");
            CommuteTimeService.super.onLocationResult(locationResult);
            CommuteTimeService commuteTimeService = this.a;
            if (locationResult != null) {
                commuteTimeService.b(locationResult.p());
                if (this.a.i() != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.A.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onLocationResult lastLocation=");
                    Location i = this.a.i();
                    if (i != null) {
                        sb.append(i);
                        local.d(a2, sb.toString());
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "onLocationResult lastLocation is null");
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.microapp.CommuteTimeService$getDurationTime$1", f = "CommuteTimeService.kt", l = {280}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Location $location;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeService commuteTimeService, Location location, xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeService;
            this.$location = location;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$location, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.i != null) {
                    CommuteTimeSetting a2 = this.this$0.i;
                    if (a2 != null) {
                        if (!(a2.getAddress().length() == 0)) {
                            xs4 h = this.this$0.h();
                            if (h != null) {
                                CommuteTimeSetting a3 = this.this$0.i;
                                if (a3 != null) {
                                    String address = a3.getAddress();
                                    TravelMode travelMode = TravelMode.DRIVING;
                                    CommuteTimeSetting a4 = this.this$0.i;
                                    if (a4 != null) {
                                        boolean avoidTolls = a4.getAvoidTolls();
                                        double latitude = this.$location.getLatitude();
                                        double longitude = this.$location.getLongitude();
                                        this.L$0 = il6;
                                        this.label = 1;
                                        obj = h.a(address, travelMode, avoidTolls, latitude, longitude, this);
                                        if (obj == a) {
                                            return a;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            long longValue = ((Number) obj).longValue();
            if (longValue != -1) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a5 = CommuteTimeService.A.a();
                local.d(a5, "getDurationTime duration " + longValue);
                this.this$0.h = longValue;
                if (this.this$0.h < ((long) 60)) {
                    this.this$0.h = 60;
                }
                this.this$0.k();
            } else {
                this.this$0.a();
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<TResult> implements kc3<Location> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onComplete(qc3<Location> qc3) {
                wg6.b(qc3, "task");
                if (!qc3.e() || qc3.b() == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = CommuteTimeService.A.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getLastLocation:exception");
                    Exception a3 = qc3.a();
                    if (a3 != null) {
                        sb.append(a3);
                        local.d(a2, sb.toString());
                        this.a.a.a();
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                CommuteTimeService commuteTimeService = this.a.a;
                Location i = commuteTimeService.i();
                if (i != null) {
                    commuteTimeService.b(commuteTimeService.a(i, (Location) qc3.b()));
                    long currentTimeMillis = System.currentTimeMillis();
                    Location i2 = this.a.a.i();
                    if (i2 == null) {
                        wg6.a();
                        throw null;
                    } else if (currentTimeMillis - i2.getTime() > ((long) CommuteTimeService.y)) {
                        FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Runnable after 5s over 5 mins not trust");
                        this.a.a.a();
                    } else {
                        CommuteTimeService commuteTimeService2 = this.a.a;
                        Location i3 = commuteTimeService2.i();
                        if (i3 != null) {
                            commuteTimeService2.a(i3);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public d(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void run() {
            qc3 i;
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Runnable after 5s");
            if (this.a.d == null && this.a.j != null) {
                LocationManager f = this.a.j;
                if (f != null) {
                    f.removeUpdates(this.a);
                    if (w6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_FINE_LOCATION") == 0 && w6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.get.instance())) {
                        CommuteTimeService commuteTimeService = this.a;
                        LocationManager f2 = commuteTimeService.j;
                        if (f2 != null) {
                            commuteTimeService.b(f2.getLastKnownLocation(this.a.o));
                            if (this.a.i() != null) {
                                wv2 c = this.a.q;
                                if (c != null && (i = c.i()) != null) {
                                    i.a(new a(this));
                                    return;
                                }
                                return;
                            }
                            this.a.a();
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Runnable after 5s permission not granted");
                    this.a.a();
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<TResult> implements mc3<cw2> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public e(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(cw2 cw2) {
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "All location settings are satisfied");
            if (w6.a(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0 || w6.a(this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
                wv2 c = this.a.q;
                if (c != null) {
                    c.a(this.a.s, this.a.u, Looper.myLooper());
                    return;
                }
                return;
            }
            this.a.a();
            this.a.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public f(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            wg6.b(exc, "exception");
            int statusCode = ((sv1) exc).getStatusCode();
            if (statusCode == 6) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Location settings are not satisfied. Attempting to upgrade location settings ");
                this.a.a();
            } else if (statusCode == 8502) {
                FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                this.a.a(false);
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<TResult> implements kc3<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeService a;

        @DexIgnore
        public g(CommuteTimeService commuteTimeService) {
            this.a = commuteTimeService;
        }

        @DexIgnore
        public final void onComplete(qc3<Void> qc3) {
            wg6.b(qc3, "it");
            FLogger.INSTANCE.getLocal().d(CommuteTimeService.A.a(), "stopLocationUpdates success");
            this.a.a(false);
        }
    }

    /*
    static {
        String simpleName = CommuteTimeService.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeService::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void j() {
        FLogger.INSTANCE.getLocal().d(w, "initLocationManager");
        if (w6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_FINE_LOCATION") == 0 || w6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            Criteria criteria = new Criteria();
            criteria.setPowerRequirement(1);
            Object systemService = getSystemService("location");
            if (systemService != null) {
                this.j = (LocationManager) systemService;
                boolean z2 = false;
                LocationManager locationManager = this.j;
                if (locationManager != null) {
                    if (locationManager != null) {
                        z2 = locationManager.isProviderEnabled("network");
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (z2) {
                    criteria.setAccuracy(2);
                } else {
                    criteria.setAccuracy(1);
                }
                LocationManager locationManager2 = this.j;
                if (locationManager2 != null) {
                    this.o = locationManager2.getBestProvider(criteria, true);
                    if (this.j != null && this.o != null) {
                        FLogger.INSTANCE.getLocal().d(w, "mLocationManager");
                        LocationManager locationManager3 = this.j;
                        if (locationManager3 != null) {
                            locationManager3.requestLocationUpdates(this.o, 0, 0.0f, this);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type android.location.LocationManager");
            }
        } else {
            a();
        }
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(w, "playHands");
        CommuteTimeSetting commuteTimeSetting = this.i;
        if (commuteTimeSetting == null) {
            wg6.a();
            throw null;
        } else if (wg6.a((Object) commuteTimeSetting.getFormat(), (Object) "travel")) {
            m();
        } else {
            l();
        }
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(w, "playHandsETA");
        long currentTimeMillis = System.currentTimeMillis() + (this.h * ((long) 1000));
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTimeInMillis(currentTimeMillis);
        int i2 = instance.get(11) % 12;
        int i3 = instance.get(12);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "playHandsETA - duration=" + this.h + ", hour=" + i2 + ", minute=" + i3);
        try {
            PortfolioApp instance2 = PortfolioApp.get.instance();
            String str2 = this.g;
            if (str2 != null) {
                instance2.a(str2, (DeviceAppResponse) new CommuteTimeETAMicroAppResponse(i2, i3));
                a();
                return;
            }
            wg6.a();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = w;
            local2.d(str3, "playHandsETA exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(w, "playHandsMinute");
        int round = Math.round(((float) this.h) / 60.0f);
        try {
            PortfolioApp instance = PortfolioApp.get.instance();
            String str = this.g;
            if (str != null) {
                instance.a(str, (DeviceAppResponse) new CommuteTimeTravelMicroAppResponse(round));
                a();
                return;
            }
            wg6.a();
            throw null;
        } catch (IllegalArgumentException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = w;
            local.d(str2, "playHandsMinute exception exception=" + e2.getMessage());
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void n() {
        FLogger.INSTANCE.getLocal().d(w, "startLocationUpdates");
        if (w6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_FINE_LOCATION") == 0 && w6.a(PortfolioApp.get.instance(), "android.permission.ACCESS_COARSE_LOCATION") == 0 && LocationUtils.isLocationEnable(PortfolioApp.get.instance())) {
            this.f = true;
            gw2 gw2 = this.r;
            if (gw2 != null) {
                qc3 a2 = gw2.a(this.t);
                a2.a(new e(this));
                a2.a(new f(this));
                return;
            }
            wg6.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "startLocationUpdates permission not granted");
        a();
        this.f = false;
    }

    @DexIgnore
    public final void o() {
        qc3 a2;
        if (!this.f) {
            FLogger.INSTANCE.getLocal().d(w, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        wv2 wv2 = this.q;
        if (wv2 != null && (a2 = wv2.a(this.u)) != null) {
            a2.a(new g(this));
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        wg6.b(intent, "intent");
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        FLogger.INSTANCE.getLocal().d(w, "onCreate");
        PortfolioApp.get.instance().g().a(this);
        this.p = new Handler();
        this.g = PortfolioApp.get.instance().e();
        this.q = aw2.a(this);
        this.r = aw2.b(this);
        f();
        g();
        e();
        j();
        n();
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(w, "onDestroy");
        super.onDestroy();
        o();
        Handler handler = this.p;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
            LocationManager locationManager = this.j;
            if (locationManager == null) {
                return;
            }
            if (locationManager != null) {
                locationManager.removeUpdates(this);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void onLocationChanged(Location location) {
        FLogger.INSTANCE.getLocal().d(w, "onLocationChanged");
        if (!(this.i == null || location == null || this.d != null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onLocationChanged location=lat: " + location.getLatitude() + " long: " + location.getLongitude());
            this.d = location;
            Location location2 = this.d;
            if (location2 != null) {
                a(location2);
            } else {
                wg6.a();
                throw null;
            }
        }
        LocationManager locationManager = this.j;
        if (locationManager == null) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void onProviderDisabled(String str) {
        wg6.b(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "onProviderDisabled provider=" + str);
    }

    @DexIgnore
    public void onProviderEnabled(String str) {
        wg6.b(str, "provider");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "onProviderEnabled provider=" + str);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        Bundle extras;
        wg6.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(w, "onStartCommand");
        this.a = Action.MicroAppAction.SHOW_COMMUTE;
        super.d();
        if (this.i != null || (extras = intent.getExtras()) == null) {
            return 2;
        }
        String string = extras.getString(Constants.EXTRA_INFO);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartCommand json=");
        if (string != null) {
            sb.append(string);
            local.d(str, sb.toString());
            this.i = (CommuteTimeSetting) new Gson().a(string, CommuteTimeSetting.class);
            Handler handler = this.p;
            if (handler != null) {
                handler.postDelayed(new d(this), LocationSource.LOCATION_WAITING_THRESHOLD_TIME);
                return 2;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void onStatusChanged(String str, int i2, Bundle bundle) {
        wg6.b(str, "provider");
        wg6.b(bundle, "extras");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local.d(str2, "onStatusChanged status=" + i2);
    }

    @DexIgnore
    public final void b(Location location) {
        this.e = location;
    }

    @DexIgnore
    public final void e() {
        bw2.a aVar = new bw2.a();
        LocationRequest locationRequest = this.s;
        if (locationRequest != null) {
            if (locationRequest != null) {
                aVar.a(locationRequest);
            } else {
                wg6.a();
                throw null;
            }
        }
        this.t = aVar.a();
    }

    @DexIgnore
    public final void f() {
        this.u = new b(this);
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d(w, "createLocationRequest");
        this.s = new LocationRequest();
        LocationRequest locationRequest = this.s;
        if (locationRequest != null) {
            locationRequest.k(1000);
            LocationRequest locationRequest2 = this.s;
            if (locationRequest2 != null) {
                locationRequest2.j(1000);
                LocationRequest locationRequest3 = this.s;
                if (locationRequest3 != null) {
                    locationRequest3.d(100);
                    LocationRequest locationRequest4 = this.s;
                    if (locationRequest4 != null) {
                        locationRequest4.a(z);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final xs4 h() {
        xs4 xs4 = this.v;
        if (xs4 != null) {
            return xs4;
        }
        wg6.d("mDurationUtils");
        throw null;
    }

    @DexIgnore
    public final Location i() {
        return this.e;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.f = z2;
    }

    @DexIgnore
    public void b() {
        FLogger.INSTANCE.getLocal().d(w, "forceStop");
        a();
    }

    @DexIgnore
    public final void a(Location location) {
        wg6.b(location, "location");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "getDurationTime location long=" + location.getLongitude() + " lat=" + location.getLatitude());
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, location, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a() {
        FLogger.INSTANCE.getLocal().d(w, "finish");
        super.a();
        stopSelf();
    }

    @DexIgnore
    public final Location a(Location location, Location location2) {
        wg6.b(location, "location");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = w;
        local.d(str, "isBetterLocation location long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
        if (location2 == null) {
            FLogger.INSTANCE.getLocal().d(w, "isBetterLocation currentBestLocation null");
            return location;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = w;
        local2.d(str2, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
        long time = location.getTime() - location2.getTime();
        boolean z2 = true;
        boolean z3 = time > ((long) x);
        boolean z4 = time < ((long) (-x));
        boolean z5 = time > 0;
        if (z3) {
            FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isSignificantlyNewer");
            return location;
        } else if (z4) {
            FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isSignificantlyOlder");
            return location2;
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = w;
            local3.d(str3, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
            int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
            boolean z6 = accuracy > 0;
            boolean z7 = accuracy < 0;
            if (accuracy <= 200) {
                z2 = false;
            }
            boolean a2 = a(location.getProvider(), location2.getProvider());
            if (z7) {
                FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isMoreAccurate");
                return location;
            } else if (z5 && !z6) {
                FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isNewer && isLessAccurate=false");
                return location;
            } else if (!z5 || z2 || !a2) {
                return location2;
            } else {
                FLogger.INSTANCE.getLocal().d(w, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                return location;
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(w, "isSameProvider");
        if (str == null) {
            return str2 == null;
        }
        return wg6.a((Object) str, (Object) str2);
    }
}
