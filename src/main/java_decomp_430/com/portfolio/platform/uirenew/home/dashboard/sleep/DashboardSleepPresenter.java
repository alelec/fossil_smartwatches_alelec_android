package com.portfolio.platform.uirenew.home.dashboard.sleep;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ag5;
import com.fossil.bg5$b$a;
import com.fossil.bg5$b$b;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gg6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zf5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardSleepPresenter extends zf5 implements vk4.a {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<SleepSummary> f;
    @DexIgnore
    public /* final */ ag5 g;
    @DexIgnore
    public /* final */ SleepSummariesRepository h;
    @DexIgnore
    public /* final */ SleepSessionsRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ SleepDao k;
    @DexIgnore
    public /* final */ SleepDatabase l;
    @DexIgnore
    public /* final */ UserRepository m;
    @DexIgnore
    public /* final */ u04 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1", f = "DashboardSleepPresenter.kt", l = {69}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DashboardSleepPresenter dashboardSleepPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardSleepPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            LiveData pagedList;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                bg5$b$b bg5_b_b = new bg5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, bg5_b_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                Date d = bk4.d(mFUser.getCreatedAt());
                DashboardSleepPresenter dashboardSleepPresenter = this.this$0;
                SleepSummariesRepository h = dashboardSleepPresenter.h;
                SleepSummariesRepository h2 = this.this$0.h;
                SleepSessionsRepository f = this.this$0.i;
                FitnessDataRepository c = this.this$0.j;
                SleepDao d2 = this.this$0.k;
                SleepDatabase e = this.this$0.l;
                wg6.a((Object) d, "createdDate");
                dashboardSleepPresenter.f = h.getSummariesPaging(h2, f, c, d2, e, d, this.this$0.n, this.this$0);
                Listing g = this.this$0.f;
                if (!(g == null || (pagedList = g.getPagedList()) == null)) {
                    ag5 k = this.this$0.k();
                    if (k != null) {
                        pagedList.a((DashboardSleepFragment) k, new bg5$b$a(this));
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DashboardSleepPresenter(ag5 ag5, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDao sleepDao, SleepDatabase sleepDatabase, UserRepository userRepository, u04 u04) {
        wg6.b(ag5, "mView");
        wg6.b(sleepSummariesRepository, "mSleepSummariesRepository");
        wg6.b(sleepSessionsRepository, "mSleepSessionsRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(sleepDao, "mSleepDao");
        wg6.b(sleepDatabase, "mSleepDatabase");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(u04, "mAppExecutors");
        this.g = ag5;
        this.h = sleepSummariesRepository;
        this.i = sleepSessionsRepository;
        this.j = fitnessDataRepository;
        this.k = sleepDao;
        this.l = sleepDatabase;
        this.m = userRepository;
        this.n = u04;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
    }

    @DexIgnore
    public void j() {
        gg6<cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "retry all failed request");
        Listing<SleepSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }

    @DexIgnore
    public final ag5 k() {
        return this.g;
    }

    @DexIgnore
    public void l() {
        this.g.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "start");
        if (!bk4.t(this.e).booleanValue()) {
            this.e = new Date();
            Listing<SleepSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "stop");
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        LiveData<cf<SleepSummary>> pagedList;
        try {
            Listing<SleepSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                ag5 ag5 = this.g;
                if (ag5 != null) {
                    pagedList.a((DashboardSleepFragment) ag5);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("DashboardSleepPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        wg6.b(gVar, "report");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepPresenter", "onStatusChange status=" + gVar);
        if (gVar.a()) {
            this.g.f();
        }
    }
}
