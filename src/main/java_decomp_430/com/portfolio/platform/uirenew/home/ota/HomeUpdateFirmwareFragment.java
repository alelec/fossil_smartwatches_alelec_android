package com.portfolio.platform.uirenew.home.ota;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.hb4;
import com.fossil.hj5;
import com.fossil.ij5;
import com.fossil.k34;
import com.fossil.kb;
import com.fossil.oj3;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeUpdateFirmwareFragment extends BaseFragment implements ij5 {
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String f; // = ThemeManager.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String g; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public /* final */ String h; // = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public int i;
    @DexIgnore
    public hj5 j;
    @DexIgnore
    public k34 o;
    @DexIgnore
    public ax5<hb4> p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeUpdateFirmwareFragment a() {
            return new HomeUpdateFirmwareFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ hb4 a;
        @DexIgnore
        public /* final */ /* synthetic */ HomeUpdateFirmwareFragment b;

        @DexIgnore
        public b(hb4 hb4, HomeUpdateFirmwareFragment homeUpdateFirmwareFragment) {
            this.a = hb4;
            this.b = homeUpdateFirmwareFragment;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            HomeUpdateFirmwareFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.g)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwareFragment", "set icon color " + this.b.g);
                int parseColor = Color.parseColor(this.b.g);
                TabLayout.g b4 = this.a.r.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.f) && this.b.i != i) {
                int parseColor2 = Color.parseColor(this.b.f);
                TabLayout.g b5 = this.a.r.b(this.b.i);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.i = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeUpdateFirmwareFragment a;

        @DexIgnore
        public c(HomeUpdateFirmwareFragment homeUpdateFirmwareFragment) {
            this.a = homeUpdateFirmwareFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.f) && !TextUtils.isEmpty(this.a.g)) {
                int parseColor = Color.parseColor(this.a.f);
                int parseColor2 = Color.parseColor(this.a.g);
                gVar.b(2131231004);
                if (i == this.a.i) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void h(int i2) {
        ProgressBar progressBar;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i2);
        if (isActive()) {
            ax5<hb4> ax5 = this.p;
            if (ax5 != null) {
                hb4 a2 = ax5.a();
                if (a2 != null && (progressBar = a2.s) != null) {
                    progressBar.setProgress(i2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void j1() {
        ax5<hb4> ax5 = this.p;
        if (ax5 != null) {
            hb4 a2 = ax5.a();
            TabLayout tabLayout = a2 != null ? a2.r : null;
            if (tabLayout != null) {
                ax5<hb4> ax52 = this.p;
                if (ax52 != null) {
                    hb4 a3 = ax52.a();
                    ViewPager2 viewPager2 = a3 != null ? a3.u : null;
                    if (viewPager2 != null) {
                        new oj3(tabLayout, viewPager2, new c(this)).a();
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeUpdateFirmwareFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        hb4 a2 = kb.a(layoutInflater, 2131558570, viewGroup, false, e1());
        this.p = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HomeUpdateFirmwareFragment.super.onPause();
        hj5 hj5 = this.j;
        if (hj5 == null) {
            return;
        }
        if (hj5 != null) {
            hj5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        HomeUpdateFirmwareFragment.super.onResume();
        hj5 hj5 = this.j;
        if (hj5 == null) {
            return;
        }
        if (hj5 != null) {
            hj5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        this.o = new k34(new ArrayList());
        ax5<hb4> ax5 = this.p;
        if (ax5 != null) {
            hb4 a2 = ax5.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.s;
                wg6.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                Object r0 = a2.q;
                wg6.a((Object) r0, "binding.ftvUpdateWarning");
                r0.setVisibility(0);
                ViewPager2 viewPager2 = a2.u;
                wg6.a((Object) viewPager2, "binding.rvpTutorial");
                k34 k34 = this.o;
                if (k34 != null) {
                    viewPager2.setAdapter(k34);
                    if (!TextUtils.isEmpty(this.h)) {
                        TabLayout tabLayout = a2.r;
                        wg6.a((Object) tabLayout, "binding.indicator");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.h)));
                    }
                    j1();
                    a2.u.a(new b(a2, this));
                    return;
                }
                wg6.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(hj5 hj5) {
        wg6.b(hj5, "presenter");
        this.j = hj5;
    }

    @DexIgnore
    public void h(List<? extends Explore> list) {
        wg6.b(list, "data");
        k34 k34 = this.o;
        if (k34 != null) {
            k34.a(list);
        } else {
            wg6.d("mAdapterUpdateFirmware");
            throw null;
        }
    }
}
