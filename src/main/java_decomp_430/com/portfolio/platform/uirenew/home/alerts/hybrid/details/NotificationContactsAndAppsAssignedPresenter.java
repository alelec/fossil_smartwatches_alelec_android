package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.loader.app.LoaderManager;
import com.fossil.AppWrapper;
import com.fossil.a35;
import com.fossil.ae;
import com.fossil.af6;
import com.fossil.av6;
import com.fossil.ik6;
import com.fossil.l35;
import com.fossil.ll6;
import com.fossil.m15;
import com.fossil.m24;
import com.fossil.n15;
import com.fossil.o25;
import com.fossil.qg6;
import com.fossil.r15$c$a;
import com.fossil.r15$e$a;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.tr4;
import com.fossil.uh4;
import com.fossil.uv6;
import com.fossil.vd6;
import com.fossil.vx4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.y24;
import com.fossil.yd6;
import com.fossil.z24;
import com.fossil.zd;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.service.BleCommandResultManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsAndAppsAssignedPresenter extends m15 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ a w; // = new a((qg6) null);
    @DexIgnore
    public /* final */ List<Object> f; // = new ArrayList();
    @DexIgnore
    public List<Object> g; // = new ArrayList();
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public /* final */ List<wx4> i; // = new ArrayList();
    @DexIgnore
    public /* final */ List<vx4> j; // = new ArrayList();
    @DexIgnore
    public /* final */ List<wx4> k; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> l; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> m; // = new ArrayList();
    @DexIgnore
    public /* final */ LoaderManager n;
    @DexIgnore
    public /* final */ n15 o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ z24 q;
    @DexIgnore
    public /* final */ a35 r;
    @DexIgnore
    public /* final */ o25 s;
    @DexIgnore
    public /* final */ l35 t;
    @DexIgnore
    public /* final */ tr4 u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsAndAppsAssignedPresenter.v;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.d<o25.b, y24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

        @DexIgnore
        public b(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            this.a = notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(o25.b bVar) {
            wg6.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetApps onSuccess");
            ArrayList arrayList = new ArrayList();
            for (AppWrapper next : bVar.a()) {
                InstalledApp installedApp = next.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected == null) {
                    wg6.a();
                    throw null;
                } else if (isSelected.booleanValue() && next.getCurrentHandGroup() == this.a.p) {
                    arrayList.add(next);
                }
            }
            this.a.o().addAll(arrayList);
            this.a.n().addAll(arrayList);
            List<Object> p = this.a.p();
            Object[] array = arrayList.toArray(new AppWrapper[0]);
            if (array != null) {
                Serializable a2 = av6.a((Serializable) array);
                wg6.a((Object) a2, "SerializationUtils.clone\u2026sSelected.toTypedArray())");
                vd6.a(p, (T[]) (Object[]) a2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = NotificationContactsAndAppsAssignedPresenter.w.a();
                local.d(a3, "mContactAndAppDataFirstLoad.size=" + this.a.p().size());
                this.a.n.a(1, new Bundle(), this.a);
                this.a.o.e(this.a.o());
                this.a.o.d();
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void a(y24.a aVar) {
            wg6.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetApps onError");
            this.a.o.d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.d<a35.d, a35.b> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

        @DexIgnore
        public c(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            this.a = notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(a35.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new r15$c$a(this, dVar, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(a35.b bVar) {
            wg6.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetAllHybridContactGroups onError");
            this.a.o.d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements y24.d<o25.b, y24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        public d(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter, ArrayList arrayList, List list) {
            this.a = notificationContactsAndAppsAssignedPresenter;
            this.b = arrayList;
            this.c = list;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(o25.b bVar) {
            wg6.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetHybridApp onSuccess");
            ArrayList arrayList = new ArrayList();
            for (String str : this.b) {
                Iterator<T> it = bVar.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        AppWrapper appWrapper = (AppWrapper) it.next();
                        if (wg6.a((Object) String.valueOf(appWrapper.getUri()), (Object) str)) {
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            if (installedApp != null) {
                                installedApp.setSelected(true);
                            }
                            appWrapper.setCurrentHandGroup(this.a.p);
                            arrayList.add(appWrapper);
                            List list = this.c;
                            Uri uri = appWrapper.getUri();
                            if (uri != null) {
                                list.add(uri);
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationContactsAndAppsAssignedPresenter.w.a();
            local.d(a2, "queryAppsString: appsSelected=" + arrayList);
            this.a.o().addAll(arrayList);
            NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter = this.a;
            notificationContactsAndAppsAssignedPresenter.a(notificationContactsAndAppsAssignedPresenter.s());
            this.a.o.p(this.a.i());
            int size = this.a.n().size();
            for (int i = 0; i < size; i++) {
                if (!yd6.a(this.c, this.a.n().get(i).getUri())) {
                    AppWrapper appWrapper2 = this.a.n().get(i);
                    InstalledApp installedApp2 = appWrapper2.getInstalledApp();
                    if (installedApp2 != null) {
                        installedApp2.setSelected(false);
                        this.a.o().add(appWrapper2);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            this.a.o.e(this.a.o());
        }

        @DexIgnore
        public void a(y24.a aVar) {
            wg6.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetApps onError");
            this.a.o.d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<tr4.d, tr4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public e(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter, List list, List list2, boolean z) {
            this.a = notificationContactsAndAppsAssignedPresenter;
            this.b = list;
            this.c = list2;
            this.d = z;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tr4.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "saveNotification success!!");
            this.a.q.a(this.a.t, new l35.b(this.b, this.c), new r15$e$a(this));
        }

        @DexIgnore
        public void a(tr4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationContactsAndAppsAssignedPresenter.w.a();
            local.d(a2, "saveNotification fail!! errorValue=" + cVar.a());
            this.a.o.d();
            if (cVar.c() != 1101) {
                this.a.o.j();
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "Bluetooth is disabled");
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(cVar.b());
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            n15 f = this.a.o;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                f.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = NotificationContactsAndAppsAssignedPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationContactsAndA\u2026er::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter(LoaderManager loaderManager, n15 n15, int i2, z24 z24, a35 a35, o25 o25, l35 l35, tr4 tr4) {
        wg6.b(loaderManager, "mLoaderManager");
        wg6.b(n15, "mView");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(a35, "mGetAllHybridContactGroups");
        wg6.b(o25, "mGetHybridApp");
        wg6.b(l35, "mSaveAllHybridNotification");
        wg6.b(tr4, "mSetNotificationFiltersUserCase");
        this.n = loaderManager;
        this.o = n15;
        this.p = i2;
        this.q = z24;
        this.r = a35;
        this.s = o25;
        this.t = l35;
        this.u = tr4;
    }

    @DexIgnore
    public void a(ae<Cursor> aeVar) {
        wg6.b(aeVar, "loader");
    }

    @DexIgnore
    public void j() {
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if (obj instanceof AppWrapper) {
                    AppWrapper appWrapper = (AppWrapper) obj;
                    InstalledApp installedApp = appWrapper.getInstalledApp();
                    Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                    if (isSelected == null) {
                        wg6.a();
                        throw null;
                    } else if (isSelected.booleanValue()) {
                        Uri uri = appWrapper.getUri();
                        if (uri != null) {
                            arrayList.add(uri.toString());
                        }
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.b(this.p, arrayList);
    }

    @DexIgnore
    public void k() {
        wx4 wx4;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof wx4) && ((contact = wx4.getContact()) == null || contact.getContactId() != -100 || (contact2 = wx4.getContact()) == null || contact2.getContactId() != -200)) {
                    if ((wx4 = (wx4) obj).isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.c(this.p, arrayList);
    }

    @DexIgnore
    public void l() {
        wx4 wx4;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof wx4) && (((contact = wx4.getContact()) != null && contact.getContactId() == -100) || ((contact2 = wx4.getContact()) != null && contact2.getContactId() == -200))) {
                    if ((wx4 = (wx4) obj).isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.a(this.p, arrayList);
    }

    @DexIgnore
    public void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "mContactAndAppData.size=" + this.f.size() + " mContactAndAppDataFirstLoad.size=" + this.g.size());
        if (i()) {
            this.o.e();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (T next : this.f) {
                if (next instanceof wx4) {
                    arrayList.add(next);
                } else if (next != null) {
                    arrayList2.add((AppWrapper) next);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                }
            }
            a((List<wx4>) arrayList, (List<vx4>) arrayList2, true);
        }
    }

    @DexIgnore
    public final List<vx4> n() {
        return this.j;
    }

    @DexIgnore
    public final List<Object> o() {
        return this.f;
    }

    @DexIgnore
    public final List<Object> p() {
        return this.g;
    }

    @DexIgnore
    public final List<wx4> q() {
        return this.i;
    }

    @DexIgnore
    public final List<wx4> r() {
        return this.k;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0071 A[EDGE_INSN: B:128:0x0071->B:30:0x0071 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x012e A[EDGE_INSN: B:131:0x012e->B:86:0x012e ?: BREAK  , SYNTHETIC] */
    public final boolean s() {
        List<Object> list;
        List<Object> list2;
        T t2;
        boolean z;
        T t3;
        boolean z2;
        if (this.g.size() >= this.f.size()) {
            list2 = this.g;
            list = this.f;
        } else {
            list2 = this.f;
            list = this.g;
        }
        for (T next : list2) {
            if (next instanceof wx4) {
                wx4 wx4 = (wx4) next;
                if (wx4.getContact() != null) {
                    Contact contact = wx4.getContact();
                    if (contact != null) {
                        int contactId = contact.getContactId();
                        Iterator<T> it = list.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t3 = null;
                                break;
                            }
                            t3 = it.next();
                            if (t3 instanceof wx4) {
                                wx4 wx42 = (wx4) t3;
                                if (wx42.getContact() != null) {
                                    Contact contact2 = wx42.getContact();
                                    if (contact2 != null) {
                                        if (contact2.getContactId() == contactId) {
                                            z2 = true;
                                            continue;
                                            if (z2) {
                                                break;
                                            }
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            }
                            z2 = false;
                            continue;
                            if (z2) {
                            }
                        }
                        if (t3 == null) {
                            return true;
                        }
                        wx4 wx43 = (wx4) t3;
                        if (wx4.isAdded() == wx43.isAdded()) {
                            Contact contact3 = wx4.getContact();
                            if (contact3 != null) {
                                boolean isUseSms = contact3.isUseSms();
                                Contact contact4 = wx43.getContact();
                                if (contact4 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (isUseSms == contact4.isUseSms()) {
                                    Contact contact5 = wx4.getContact();
                                    if (contact5 != null) {
                                        boolean isUseCall = contact5.isUseCall();
                                        Contact contact6 = wx43.getContact();
                                        if (contact6 == null) {
                                            wg6.a();
                                            throw null;
                                        } else if (isUseCall == contact6.isUseCall()) {
                                            Contact contact7 = wx4.getContact();
                                            if (contact7 != null) {
                                                boolean isUseEmail = contact7.isUseEmail();
                                                Contact contact8 = wx43.getContact();
                                                if (contact8 == null) {
                                                    wg6.a();
                                                    throw null;
                                                } else if (isUseEmail == contact8.isUseEmail() && wx4.getCurrentHandGroup() == wx43.getCurrentHandGroup()) {
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        return true;
                    }
                    wg6.a();
                    throw null;
                }
            }
            if (next instanceof AppWrapper) {
                AppWrapper appWrapper = (AppWrapper) next;
                if (appWrapper.getInstalledApp() != null) {
                    Iterator<T> it2 = list.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it2.next();
                        if (t2 instanceof AppWrapper) {
                            AppWrapper appWrapper2 = (AppWrapper) t2;
                            if (wg6.a((Object) String.valueOf(appWrapper2.getUri()), (Object) String.valueOf(appWrapper.getUri())) && appWrapper2.getInstalledApp() != null) {
                                z = true;
                                continue;
                                if (z) {
                                    break;
                                }
                            }
                        }
                        z = false;
                        continue;
                        if (z) {
                        }
                    }
                    if (t2 == null) {
                        return true;
                    }
                    AppWrapper appWrapper3 = (AppWrapper) t2;
                    if (appWrapper3.getCurrentHandGroup() == appWrapper.getCurrentHandGroup()) {
                        InstalledApp installedApp = appWrapper3.getInstalledApp();
                        if (installedApp != null) {
                            Boolean isSelected = installedApp.isSelected();
                            InstalledApp installedApp2 = appWrapper.getInstalledApp();
                            if (installedApp2 == null) {
                                wg6.a();
                                throw null;
                            } else if (!wg6.a((Object) isSelected, (Object) installedApp2.isSelected())) {
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    return true;
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    @DexIgnore
    public final void t() {
        this.q.a(this.s, null, new b(this));
    }

    @DexIgnore
    public final void u() {
        this.o.e();
        this.q.a(this.r, null, new c(this));
    }

    @DexIgnore
    public final void v() {
        if (!this.m.isEmpty()) {
            Collection<T> a2 = uv6.a(this.m, this.l);
            wg6.a((Object) a2, "CollectionUtils.subtract\u2026actIds, mPhoneContactIds)");
            List<T> m2 = yd6.m(a2);
            if (!m2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                int size = this.f.size();
                while (true) {
                    size--;
                    if (size < 0) {
                        break;
                    }
                    Object obj = this.f.get(size);
                    if (obj instanceof wx4) {
                        wx4 wx4 = (wx4) obj;
                        Contact contact = wx4.getContact();
                        Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                        int size2 = m2.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size2) {
                                break;
                            } else if (wg6.a((Object) (Integer) m2.get(i2), (Object) valueOf)) {
                                wx4.setAdded(false);
                                arrayList.add(obj);
                                this.f.remove(size);
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.o.e();
                    a((List<wx4>) arrayList, (List<vx4>) new ArrayList(), false);
                }
            }
        }
    }

    @DexIgnore
    public void w() {
        this.o.a(this);
    }

    @DexIgnore
    public void b(ArrayList<wx4> arrayList) {
        wx4 wx4;
        Contact contact;
        Contact contact2;
        wg6.b(arrayList, "contactWrappersSelected");
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof wx4) && (((contact = (wx4 = (wx4) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = wx4.getContact()) != null && contact2.getContactId() == -200))) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (wx4 wx42 : arrayList) {
            this.f.add(wx42);
            Contact contact3 = wx42.getContact();
            if (contact3 != null) {
                arrayList2.add(Integer.valueOf(contact3.getContactId()));
            } else {
                wg6.a();
                throw null;
            }
        }
        a(s());
        this.o.p(i());
        int size2 = this.k.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact4 = this.k.get(i2).getContact();
            if (!yd6.a(arrayList2, contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                wx4 wx43 = this.k.get(i2);
                wx43.setAdded(false);
                this.f.add(wx43);
            }
        }
        this.o.e(this.f);
    }

    @DexIgnore
    public void c(ArrayList<String> arrayList) {
        wg6.b(arrayList, "stringAppsSelected");
        FLogger.INSTANCE.getLocal().d(v, "queryAppsString = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof AppWrapper) {
                    this.f.remove(size);
                }
            }
        }
        this.q.a(this.s, null, new d(this, arrayList, new ArrayList()));
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(v, "start");
        this.u.e();
        BleCommandResultManager.d.a(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
        n15 n15 = this.o;
        if (n15 != null) {
            Context context = ((NotificationContactsAndAppsAssignedFragment) n15).getContext();
            if (context != null && xm4.a(xm4.d, context, "NOTIFICATION_CONTACTS_ASSIGNMENT", false, false, false, 28, (Object) null)) {
                this.o.i(this.p);
                if (!this.f.isEmpty() || !this.h) {
                    wg6.a((Object) this.n.a(1, new Bundle(), this), "mLoaderManager.initLoade\u2026AndAppsAssignedPresenter)");
                    return;
                }
                this.h = false;
                u();
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(v, "stop");
        this.u.e();
        this.n.a(1);
    }

    @DexIgnore
    public void h() {
        a(s());
        this.o.p(i());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0040, code lost:
        r1 = (com.fossil.wx4) r1;
     */
    @DexIgnore
    public void a(int i2, boolean z, boolean z2) {
        wx4 wx4;
        Contact contact;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "updateContactWrapper: contactId=" + i2 + ", useCall=" + z + ", useText=" + z2);
        Iterator<Object> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if ((next instanceof wx4) && (contact = wx4.getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = wx4.getContact();
                if (contact2 != null) {
                    contact2.setUseCall(z);
                }
                Contact contact3 = wx4.getContact();
                if (contact3 != null) {
                    contact3.setUseSms(z2);
                }
            }
        }
        a(s());
        this.o.p(i());
        this.o.e(this.f);
    }

    @DexIgnore
    public void a(ArrayList<wx4> arrayList) {
        wg6.b(arrayList, "contactWrappersSelected");
        FLogger.INSTANCE.getLocal().d(v, "addContactWrapperList: contactWrappersSelected = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof wx4) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (wx4 wx4 : arrayList) {
            this.f.add(wx4);
            Contact contact = wx4.getContact();
            if (contact != null) {
                arrayList2.add(Integer.valueOf(contact.getContactId()));
            } else {
                wg6.a();
                throw null;
            }
        }
        a(s());
        this.o.p(i());
        int size2 = this.i.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact2 = this.i.get(i2).getContact();
            if (!yd6.a(arrayList2, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                wx4 wx42 = this.i.get(i2);
                wx42.setAdded(false);
                this.f.add(wx42);
            }
        }
        this.o.e(this.f);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public ae<Cursor> a(int i2, Bundle bundle) {
        boolean z;
        this.m.clear();
        this.l.clear();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("has_phone_number != 0 AND mimetype =? ");
        ArrayList arrayList = new ArrayList();
        arrayList.add("vnd.android.cursor.item/phone_v2");
        if (!this.f.isEmpty()) {
            z = false;
            for (wx4 next : this.f) {
                if (next instanceof wx4) {
                    wx4 wx4 = next;
                    if (wx4.getContact() != null) {
                        Contact contact = wx4.getContact();
                        Integer num = null;
                        if (contact == null) {
                            wg6.a();
                            throw null;
                        } else if (contact.getContactId() >= 0) {
                            if (!z) {
                                sb2.append("AND contact_id IN (");
                                z = true;
                            }
                            sb2.append("?, ");
                            Contact contact2 = wx4.getContact();
                            if (contact2 != null) {
                                num = Integer.valueOf(contact2.getContactId());
                            }
                            if (num != null) {
                                this.m.add(num);
                            }
                            arrayList.add(String.valueOf(num));
                        }
                    } else {
                        continue;
                    }
                }
            }
        } else {
            z = false;
        }
        if (z) {
            sb.append(new StringBuilder(sb2.substring(0, sb2.length() - 2)));
            sb.append(")");
        } else {
            sb.append(sb2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, ".Inside onCreateLoader, selectionQuery = " + sb);
        String[] strArr = {"contact_id", "display_name", "photo_thumb_uri"};
        Object instance = PortfolioApp.get.instance();
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String sb3 = sb.toString();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new zd(instance, uri, strArr, sb3, (String[]) array, (String) null);
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void a(ae<Cursor> aeVar, Cursor cursor) {
        wg6.b(aeVar, "loader");
        if (cursor != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = v;
            local.d(str, "onLoadFinished, loader id = " + aeVar.getId() + "cursor is closed = " + cursor.isClosed());
            if (cursor.isClosed()) {
                this.n.b(aeVar.getId(), new Bundle(), this);
                return;
            }
            if (cursor.moveToFirst()) {
                do {
                    String string = cursor.getString(cursor.getColumnIndex("display_name"));
                    String string2 = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
                    int i2 = cursor.getInt(cursor.getColumnIndex("contact_id"));
                    this.l.add(Integer.valueOf(i2));
                    a(i2, string, string2);
                } while (cursor.moveToNext());
            }
            v();
            this.o.e(this.f);
            cursor.close();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        r1 = (com.fossil.wx4) r1;
     */
    @DexIgnore
    public final void a(int i2, String str, String str2) {
        wx4 wx4;
        Contact contact;
        Iterator<Object> it = this.f.iterator();
        while (it.hasNext()) {
            Object next = it.next();
            if ((next instanceof wx4) && (contact = wx4.getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = wx4.getContact();
                if (contact2 != null) {
                    contact2.setFirstName(str);
                }
                Contact contact3 = wx4.getContact();
                if (contact3 != null) {
                    contact3.setPhotoThumbUri(str2);
                    return;
                }
                return;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.fossil.tr4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$e] */
    public final void a(List<wx4> list, List<vx4> list2, boolean z) {
        this.u.a(new tr4.b(list, list2, this.p), new e(this, list, list2, z));
    }
}
