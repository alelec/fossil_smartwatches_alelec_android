package com.portfolio.platform.uirenew.home.profile.theme;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.j34;
import com.fossil.je4;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.mm5;
import com.fossil.nm5;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemesFragment extends BaseFragment implements j34.a, AlertDialogFragment.g {
    @DexIgnore
    public static String o; // = "";
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public ax5<je4> g;
    @DexIgnore
    public ThemesViewModel h;
    @DexIgnore
    public j34 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ThemesFragment a() {
            return new ThemesFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<nm5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment a;

        @DexIgnore
        public b(ThemesFragment themesFragment) {
            this.a = themesFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ThemesViewModel.b bVar) {
            FragmentActivity activity;
            ArrayList<Theme> c;
            if (!(bVar == null || (c = bVar.c()) == null)) {
                this.a.e(c);
                ThemesFragment themesFragment = this.a;
                String e = bVar.e();
                if (e != null) {
                    String a2 = bVar.a();
                    if (a2 != null) {
                        themesFragment.a(e, a2, bVar.d());
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            if (bVar.b() && (activity = this.a.getActivity()) != null) {
                HomeActivity.a aVar = HomeActivity.C;
                wg6.a((Object) activity, "it");
                HomeActivity.a.a(aVar, activity, (Integer) null, 2, (Object) null);
                this.a.o();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment a;

        @DexIgnore
        public c(ThemesFragment themesFragment) {
            this.a = themesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment a;

        @DexIgnore
        public d(ThemesFragment themesFragment) {
            this.a = themesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ThemesFragment.a(this.a).a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment a;

        @DexIgnore
        public e(ThemesFragment themesFragment) {
            this.a = themesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                String valueOf = String.valueOf(System.currentTimeMillis());
                ThemeManager.l.a().a(valueOf);
                UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.B;
                wg6.a((Object) activity, "it");
                aVar.a(activity, valueOf, false);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ThemesViewModel a(ThemesFragment themesFragment) {
        ThemesViewModel themesViewModel = themesFragment.h;
        if (themesViewModel != null) {
            return themesViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void S(String str) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserEditTheme id=" + str);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.B;
            wg6.a((Object) activity, "it");
            aVar.a(activity, str, true);
        }
    }

    @DexIgnore
    public void U(String str) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserChooseTheme id=" + str);
        ThemesViewModel themesViewModel = this.h;
        if (themesViewModel != null) {
            themesViewModel.b(str);
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void V(String str) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserDeleteTheme id=" + str);
        lx5 lx5 = lx5.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        lx5.f(childFragmentManager);
        o = str;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void e(ArrayList<Theme> arrayList) {
        wg6.b(arrayList, "listTheme");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "showListTheme size=" + arrayList.size());
        j34 j34 = this.i;
        if (j34 != null) {
            j34.a(arrayList);
        }
    }

    @DexIgnore
    public final void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        je4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558616, (ViewGroup) null, false, e1());
        this.i = new j34(new ArrayList(), this);
        PortfolioApp.get.instance().g().a(new mm5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            ThemesViewModel a3 = vd.a(this, w04).a(ThemesViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026mesViewModel::class.java)");
            this.h = a3;
            ThemesViewModel themesViewModel = this.h;
            if (themesViewModel != null) {
                themesViewModel.c().a(getViewLifecycleOwner(), new b(this));
                ThemesViewModel themesViewModel2 = this.h;
                if (themesViewModel2 != null) {
                    themesViewModel2.d();
                    this.g = new ax5<>(this, a2);
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        ThemesFragment.super.onResume();
        ThemesViewModel themesViewModel = this.h;
        if (themesViewModel != null) {
            themesViewModel.d();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r7v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r7v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<je4> ax5 = this.g;
        if (ax5 != null) {
            je4 a2 = ax5.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.u.setOnClickListener(new e(this));
                RecyclerView recyclerView = a2.s;
                wg6.a((Object) recyclerView, "binding.rvThemes");
                recyclerView.setAdapter(this.i);
                RecyclerView recyclerView2 = a2.s;
                wg6.a((Object) recyclerView2, "binding.rvThemes");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView3 = a2.s;
                wg6.a((Object) recyclerView3, "binding.rvThemes");
                recyclerView3.setNestedScrollingEnabled(false);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v9, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v10, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v11, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v12, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v10, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v16, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public final void a(String str, String str2, List<Style> list) {
        wg6.b(str, "userSelectedThemeId");
        wg6.b(str2, "currentThemeId");
        ax5<je4> ax5 = this.g;
        if (ax5 != null) {
            je4 a2 = ax5.a();
            if (a2 != null && list != null) {
                j34 j34 = this.i;
                if (j34 != null) {
                    j34.a(list, str);
                }
                String a3 = ThemeManager.l.a().a(Explore.COLUMN_BACKGROUND, list);
                String a4 = ThemeManager.l.a().a("primaryText", list);
                Typeface b2 = ThemeManager.l.a().b("headline2", list);
                if (a3 != null) {
                    a2.r.setBackgroundColor(Color.parseColor(a3));
                }
                if (a4 != null) {
                    a2.v.setTextColor(Color.parseColor(a4));
                    a2.q.setColorFilter(Color.parseColor(a4));
                }
                if (b2 != null) {
                    Object r4 = a2.v;
                    wg6.a((Object) r4, "it.tvTitle");
                    r4.setTypeface(b2);
                }
                String a5 = ThemeManager.l.a().a("onPrimaryButton", list);
                Typeface b3 = ThemeManager.l.a().b("textButtonPrimary", list);
                String a6 = ThemeManager.l.a().a("primaryButton", list);
                if (a5 != null) {
                    a2.t.setTextColor(Color.parseColor(a5));
                    a2.u.setTextColor(Color.parseColor(a5));
                }
                if (b3 != null) {
                    Object r42 = a2.t;
                    wg6.a((Object) r42, "it.tvApply");
                    r42.setTypeface(b3);
                    Object r43 = a2.u;
                    wg6.a((Object) r43, "it.tvCreateNewTheme");
                    r43.setTypeface(b3);
                }
                if (a6 != null) {
                    a2.t.setBackgroundColor(Color.parseColor(a6));
                    a2.u.setBackgroundColor(Color.parseColor(a6));
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d("ThemesFragment", "onDialogFragmentResult");
        if (str.hashCode() == -1478349291 && str.equals("DELETE_THEME") && i2 == 2131363190) {
            ThemesViewModel themesViewModel = this.h;
            if (themesViewModel != null) {
                themesViewModel.a(o);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }
}
