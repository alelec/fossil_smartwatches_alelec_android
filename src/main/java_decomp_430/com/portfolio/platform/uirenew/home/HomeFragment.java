package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.bu4;
import com.fossil.cd6;
import com.fossil.cp5;
import com.fossil.ex4;
import com.fossil.fp5;
import com.fossil.jj5;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.ny5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.sw4;
import com.fossil.tj4;
import com.fossil.tw4;
import com.fossil.uz5;
import com.fossil.vd;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.ww4;
import com.fossil.xa4;
import com.fossil.y04;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeFragment extends BasePermissionFragment implements tw4, AlertDialogFragment.g, cp5.a {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public HomeDashboardPresenter g;
    @DexIgnore
    public HomeDianaCustomizePresenter h;
    @DexIgnore
    public HomeHybridCustomizePresenter i;
    @DexIgnore
    public HomeProfilePresenter j;
    @DexIgnore
    public HomeAlertsPresenter o;
    @DexIgnore
    public HomeAlertsHybridPresenter p;
    @DexIgnore
    public jj5 q;
    @DexIgnore
    public sw4 r;
    @DexIgnore
    public ax5<xa4> s;
    @DexIgnore
    public /* final */ ArrayList<Fragment> t; // = new ArrayList<>();
    @DexIgnore
    public b06 u;
    @DexIgnore
    public fp5 v; // = fp5.b.a();
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeFragment a() {
            return new HomeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;

        @DexIgnore
        public b(HomeFragment homeFragment) {
            this.a = homeFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String p1 = HomeFragment.x;
            local.d(p1, "dashboardTab: " + num);
            int intValue = num != null ? num.intValue() : 0;
            this.a.r(intValue);
            HomeFragment.b(this.a).a(intValue);
            this.a.s(intValue);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements BottomNavigationView.d {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ Typeface b;

        @DexIgnore
        public c(HomeFragment homeFragment, Typeface typeface) {
            this.a = homeFragment;
            this.b = typeface;
        }

        @DexIgnore
        public final boolean a(MenuItem menuItem) {
            int i;
            wg6.b(menuItem, "item");
            xa4 xa4 = (xa4) HomeFragment.a(this.a).a();
            BottomNavigationView bottomNavigationView = xa4 != null ? xa4.q : null;
            if (bottomNavigationView != null) {
                wg6.a((Object) bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                wg6.a((Object) menu, "mBinding.get()?.bottomNavigation!!.menu");
                this.a.a(menu, this.b);
                menu.findItem(2131362146).setIcon(2131231108);
                menu.findItem(2131362134).setIcon(2131231075);
                menu.findItem(2131362819).setIcon(2131231162);
                menu.findItem(2131361885).setIcon(2131231032);
                String b2 = ThemeManager.l.a().b("primaryColor");
                switch (menuItem.getItemId()) {
                    case 2131361885:
                        Context context = this.a.getContext();
                        if (context != null) {
                            menuItem.setIcon(w6.c(context, 2131231033));
                            i = 2;
                            break;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    case 2131362134:
                        Context context2 = this.a.getContext();
                        if (context2 != null) {
                            menuItem.setIcon(w6.c(context2, 2131231076));
                            i = 1;
                            break;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    case 2131362146:
                        Context context3 = this.a.getContext();
                        if (context3 != null) {
                            Drawable c = w6.c(context3, 2131231109);
                            if (!(b2 == null || c == null)) {
                                c.setColorFilter(Color.parseColor(b2), PorterDuff.Mode.SRC_ATOP);
                            }
                            menuItem.setIcon(c);
                            break;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    case 2131362819:
                        Context context4 = this.a.getContext();
                        if (context4 != null) {
                            menuItem.setIcon(w6.c(context4, 2131231163));
                            i = 3;
                            break;
                        } else {
                            wg6.a();
                            throw null;
                        }
                }
                i = 0;
                if (this.a.i != null && DeviceHelper.o.g(PortfolioApp.get.instance().e())) {
                    this.a.l1().b(i);
                }
                if (this.a.h != null && DeviceHelper.o.f(PortfolioApp.get.instance().e())) {
                    this.a.k1().b(i);
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String p1 = HomeFragment.x;
                local.d(p1, "show tab with tab=" + i);
                HomeFragment.b(this.a).a(i);
                this.a.s(i);
                return true;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeFragment a;

        @DexIgnore
        public d(HomeFragment homeFragment) {
            this.a = homeFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String p1 = HomeFragment.x;
            local.d(p1, "activeDeviceSerialLiveData onChange " + str);
            sw4 b = HomeFragment.b(this.a);
            if (str != null) {
                b.a(str);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = HomeFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "HomeFragment::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ax5 a(HomeFragment homeFragment) {
        ax5<xa4> ax5 = homeFragment.s;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ sw4 b(HomeFragment homeFragment) {
        sw4 sw4 = homeFragment.r;
        if (sw4 != null) {
            return sw4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void I() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        sw4 sw4 = this.r;
        if (sw4 != null) {
            sb.append(sw4.h());
            local.d(str, sb.toString());
            sw4 sw42 = this.r;
            if (sw42 != null) {
                s(sw42.h());
                HomeDashboardPresenter homeDashboardPresenter = this.g;
                if (homeDashboardPresenter != null) {
                    homeDashboardPresenter.m();
                } else {
                    wg6.d("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void P() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.p(childFragmentManager);
        }
    }

    @DexIgnore
    public void c(int i2) {
        HomeDashboardPresenter homeDashboardPresenter = this.g;
        if (homeDashboardPresenter != null) {
            homeDashboardPresenter.c(i2);
        } else {
            wg6.d("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final void j1() {
        Resources resources = getResources();
        wg6.a((Object) resources, "resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 34.0f, resources.getDisplayMetrics());
        ax5<xa4> ax5 = this.s;
        if (ax5 != null) {
            xa4 a2 = ax5.a();
            if (a2 != null) {
                int i2 = 0;
                BottomNavigationMenuView childAt = a2.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = childAt;
                    int childCount = bottomNavigationMenuView.getChildCount() - 1;
                    if (childCount >= 0) {
                        while (true) {
                            View findViewById = bottomNavigationMenuView.getChildAt(i2).findViewById(2131362499);
                            wg6.a((Object) findViewById, MicroApp.COLUMN_ICON);
                            findViewById.getLayoutParams().width = applyDimension;
                            findViewById.getLayoutParams().height = applyDimension;
                            if (i2 != childCount) {
                                i2++;
                            } else {
                                return;
                            }
                        }
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final HomeDianaCustomizePresenter k1() {
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.h;
        if (homeDianaCustomizePresenter != null) {
            return homeDianaCustomizePresenter;
        }
        wg6.d("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final HomeHybridCustomizePresenter l1() {
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.i;
        if (homeHybridCustomizePresenter != null) {
            return homeHybridCustomizePresenter;
        }
        wg6.d("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final void m1() {
        n1();
        sw4 sw4 = this.r;
        if (sw4 != null) {
            int h2 = sw4.h();
            if (h2 == 0) {
                ax5<xa4> ax5 = this.s;
                if (ax5 != null) {
                    xa4 a2 = ax5.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView = a2.q;
                        wg6.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(2131362146);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            } else if (h2 == 1) {
                ax5<xa4> ax52 = this.s;
                if (ax52 != null) {
                    xa4 a3 = ax52.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView2 = a3.q;
                        wg6.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(2131362134);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            } else if (h2 == 2) {
                ax5<xa4> ax53 = this.s;
                if (ax53 != null) {
                    xa4 a4 = ax53.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView3 = a4.q;
                        wg6.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(2131361885);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            } else if (h2 == 3) {
                ax5<xa4> ax54 = this.s;
                if (ax54 != null) {
                    xa4 a5 = ax54.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView4 = a5.q;
                        wg6.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(2131362819);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void n1() {
        sw4 sw4 = this.r;
        if (sw4 == null) {
            return;
        }
        if (sw4 != null) {
            int h2 = sw4.h();
            int size = this.t.size();
            int i2 = 0;
            while (i2 < size) {
                if (this.t.get(i2) instanceof bu4) {
                    bu4 bu4 = this.t.get(i2);
                    if (bu4 != null) {
                        bu4.Q(i2 == h2);
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i2++;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void o1() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showNoActiveDeviceFlow");
        HomeDashboardFragment a2 = HomeDashboardFragment.W.a();
        HomeDianaCustomizeFragment b2 = getChildFragmentManager().b("HomeDianaCustomizeFragment");
        HomeHybridCustomizeFragment b3 = getChildFragmentManager().b("HomeHybridCustomizeFragment");
        HomeAlertsFragment b4 = getChildFragmentManager().b("HomeAlertsFragment");
        HomeAlertsHybridFragment b5 = getChildFragmentManager().b("HomeAlertsHybridFragment");
        HomeProfileFragment b6 = getChildFragmentManager().b("HomeProfileFragment");
        HomeUpdateFirmwareFragment b7 = getChildFragmentManager().b("HomeUpdateFirmwareFragment");
        if (b2 == null) {
            b2 = HomeDianaCustomizeFragment.v.a();
        }
        if (b3 == null) {
            b3 = HomeHybridCustomizeFragment.v.a();
        }
        if (b4 == null) {
            b4 = HomeAlertsFragment.q.a();
        }
        if (b5 == null) {
            b5 = HomeAlertsHybridFragment.o.a();
        }
        if (b6 == null) {
            b6 = HomeProfileFragment.p.a();
        }
        if (b7 == null) {
            b7 = HomeUpdateFirmwareFragment.r.a();
        }
        this.t.clear();
        this.t.add(a2);
        this.t.add(b3);
        this.t.add(b4);
        this.t.add(b6);
        this.t.add(b7);
        ax5<xa4> ax5 = this.s;
        if (ax5 != null) {
            xa4 a3 = ax5.a();
            if (a3 != null) {
                ViewPager2 viewPager2 = a3.s;
                wg6.a((Object) viewPager2, "binding.rvTabs");
                viewPager2.setAdapter(new uz5(getChildFragmentManager(), this.t));
                if (a3.s.getChildAt(0) != null) {
                    RecyclerView childAt = a3.s.getChildAt(0);
                    if (childAt != null) {
                        childAt.setItemViewCacheSize(3);
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a3.s;
                wg6.a((Object) viewPager22, "binding.rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            y04 g2 = PortfolioApp.get.instance().g();
            if (b2 != null) {
                HomeDianaCustomizeFragment homeDianaCustomizeFragment = b2;
                if (b3 != null) {
                    HomeHybridCustomizeFragment homeHybridCustomizeFragment = b3;
                    if (b6 != null) {
                        HomeProfileFragment homeProfileFragment = b6;
                        if (b4 != null) {
                            HomeAlertsFragment homeAlertsFragment = b4;
                            if (b5 != null) {
                                HomeAlertsHybridFragment homeAlertsHybridFragment = b5;
                                if (b7 != null) {
                                    g2.a(new ex4(a2, homeDianaCustomizeFragment, homeHybridCustomizeFragment, homeProfileFragment, homeAlertsFragment, homeAlertsHybridFragment, b7)).a(this);
                                    return;
                                }
                                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "onActivityResult " + i2 + ' ' + i2);
        HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.h;
        if (homeDianaCustomizePresenter != null) {
            if (homeDianaCustomizePresenter != null) {
                homeDianaCustomizePresenter.a(i2, i3, intent);
            } else {
                wg6.d("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.i;
        if (homeHybridCustomizePresenter == null) {
            return;
        }
        if (homeHybridCustomizePresenter != null) {
            homeHybridCustomizePresenter.a(i2, i3, intent);
        } else {
            wg6.d("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        xa4 a2 = kb.a(layoutInflater, 2131558563, viewGroup, false, e1());
        this.s = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public void onDestroy() {
        sw4 sw4 = this.r;
        if (sw4 != null) {
            sw4.j();
            HomeFragment.super.onDestroy();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HomeFragment.super.onPause();
        sw4 sw4 = this.r;
        if (sw4 != null) {
            sw4.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        HomeFragment.super.onResume();
        sw4 sw4 = this.r;
        if (sw4 != null) {
            sw4.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            b06 a2 = vd.a(activity).a(b06.class);
            wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            this.u = a2;
            b06 b06 = this.u;
            if (b06 != null) {
                b06.a().a(activity, new b(this));
            } else {
                wg6.d("mHomeDashboardViewModel");
                throw null;
            }
        }
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        Typeface c2 = ThemeManager.l.a().c("nonBrandTextStyle11");
        if (b2 != null) {
            ax5<xa4> ax5 = this.s;
            if (ax5 != null) {
                xa4 a3 = ax5.a();
                BottomNavigationView bottomNavigationView = a3 != null ? a3.q : null;
                if (bottomNavigationView != null) {
                    bottomNavigationView.setBackgroundColor(Color.parseColor(b2));
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
        ax5<xa4> ax52 = this.s;
        if (ax52 != null) {
            xa4 a4 = ax52.a();
            BottomNavigationView bottomNavigationView2 = a4 != null ? a4.q : null;
            if (bottomNavigationView2 != null) {
                wg6.a((Object) bottomNavigationView2, "mBinding.get()?.bottomNavigation!!");
                bottomNavigationView2.setItemIconTintList((ColorStateList) null);
                ax5<xa4> ax53 = this.s;
                if (ax53 != null) {
                    xa4 a5 = ax53.a();
                    BottomNavigationView bottomNavigationView3 = a5 != null ? a5.q : null;
                    if (bottomNavigationView3 != null) {
                        bottomNavigationView3.setOnNavigationItemSelectedListener(new c(this, c2));
                        j1();
                        PortfolioApp.get.instance().f().a(this, new d(this));
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void r(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "showBottomTab currentDashboardTab=" + i2);
        if (i2 == 0) {
            ax5<xa4> ax5 = this.s;
            if (ax5 != null) {
                xa4 a2 = ax5.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView = a2.q;
                    wg6.a((Object) bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(2131362146);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i2 == 1) {
            ax5<xa4> ax52 = this.s;
            if (ax52 != null) {
                xa4 a3 = ax52.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView2 = a3.q;
                    wg6.a((Object) bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(2131362134);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i2 == 2) {
            ax5<xa4> ax53 = this.s;
            if (ax53 != null) {
                xa4 a4 = ax53.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView3 = a4.q;
                    wg6.a((Object) bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(2131361885);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        } else if (i2 == 3) {
            ax5<xa4> ax54 = this.s;
            if (ax54 != null) {
                xa4 a5 = ax54.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView4 = a5.q;
                    wg6.a((Object) bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(2131362819);
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        sw4 sw4 = this.r;
        if (sw4 != null) {
            boolean i3 = sw4.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "scroll to position=" + i2 + " fragment " + this.t.get(i2) + " isOtaing " + i3);
            if (!i3) {
                ax5<xa4> ax5 = this.s;
                if (ax5 != null) {
                    xa4 a2 = ax5.a();
                    if (a2 != null) {
                        a2.s.a(i2, false);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else if (i2 > 0) {
                ax5<xa4> ax52 = this.s;
                if (ax52 != null) {
                    xa4 a3 = ax52.a();
                    if (a3 != null) {
                        a3.s.a(4, false);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                ax5<xa4> ax53 = this.s;
                if (ax53 != null) {
                    xa4 a4 = ax53.a();
                    if (a4 != null) {
                        a4.s.a(i2, false);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            }
            n1();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(InAppNotification inAppNotification) {
        sw4 sw4 = this.r;
        if (sw4 == null) {
            wg6.d("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            sw4.b(inAppNotification);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void b(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        sw4 sw4 = this.r;
        if (sw4 != null) {
            sb.append(sw4.h());
            local.d(str, sb.toString());
            HomeDashboardPresenter homeDashboardPresenter = this.g;
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.b(z);
                sw4 sw42 = this.r;
                if (sw42 != null) {
                    s(sw42.h());
                    if (!z && isActive()) {
                        lx5 lx5 = lx5.c;
                        FragmentManager childFragmentManager = getChildFragmentManager();
                        wg6.a((Object) childFragmentManager, "childFragmentManager");
                        lx5.T(childFragmentManager);
                        return;
                    }
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
            wg6.d("mHomeDashboardPresenter");
            throw null;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(sw4 sw4) {
        wg6.b(sw4, "presenter");
        this.r = sw4;
    }

    @DexIgnore
    public void a(FossilDeviceSerialPatternUtil.DEVICE device, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i2);
        if (i2 != 1) {
            a(device);
        } else {
            o1();
        }
        m1();
    }

    @DexIgnore
    public void a(InAppNotification inAppNotification) {
        wg6.b(inAppNotification, "inAppNotification");
        if (isActive()) {
            fp5 fp5 = this.v;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                fp5.a(activity, inAppNotification);
            } else {
                wg6.a();
                throw null;
            }
        }
        b(inAppNotification);
    }

    @DexIgnore
    public final void a(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(x, "Inside .showMainFlow");
        HomeDashboardFragment a2 = HomeDashboardFragment.W.a();
        HomeDianaCustomizeFragment b2 = getChildFragmentManager().b("HomeDianaCustomizeFragment");
        HomeHybridCustomizeFragment b3 = getChildFragmentManager().b("HomeHybridCustomizeFragment");
        HomeAlertsFragment b4 = getChildFragmentManager().b("HomeAlertsFragment");
        HomeAlertsHybridFragment b5 = getChildFragmentManager().b("HomeAlertsHybridFragment");
        HomeProfileFragment b6 = getChildFragmentManager().b("HomeProfileFragment");
        HomeUpdateFirmwareFragment b7 = getChildFragmentManager().b("HomeUpdateFirmwareFragment");
        if (b2 == null) {
            b2 = HomeDianaCustomizeFragment.v.a();
        }
        if (b3 == null) {
            b3 = HomeHybridCustomizeFragment.v.a();
        }
        if (b4 == null) {
            b4 = HomeAlertsFragment.q.a();
        }
        if (b5 == null) {
            b5 = HomeAlertsHybridFragment.o.a();
        }
        if (b6 == null) {
            b6 = HomeProfileFragment.p.a();
        }
        if (b7 == null) {
            b7 = HomeUpdateFirmwareFragment.r.a();
        }
        this.t.clear();
        this.t.add(a2);
        if (device != null && ww4.a[device.ordinal()] == 1) {
            this.t.add(b2);
            this.t.add(b4);
        } else {
            this.t.add(b3);
            this.t.add(b5);
        }
        this.t.add(b6);
        this.t.add(b7);
        ax5<xa4> ax5 = this.s;
        if (ax5 != null) {
            xa4 a3 = ax5.a();
            if (a3 != null) {
                try {
                    ViewPager2 viewPager2 = a3.s;
                    wg6.a((Object) viewPager2, "binding.rvTabs");
                    viewPager2.setAdapter(new uz5(getChildFragmentManager(), this.t));
                    if (a3.s.getChildAt(0) != null) {
                        RecyclerView childAt = a3.s.getChildAt(0);
                        if (childAt != null) {
                            childAt.setItemViewCacheSize(3);
                        } else {
                            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    ViewPager2 viewPager22 = a3.s;
                    wg6.a((Object) viewPager22, "binding.rvTabs");
                    viewPager22.setUserInputEnabled(false);
                    cd6 cd6 = cd6.a;
                } catch (Exception unused) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        HomeActivity.a aVar = HomeActivity.C;
                        wg6.a((Object) activity, "it");
                        sw4 sw4 = this.r;
                        if (sw4 != null) {
                            aVar.a(activity, Integer.valueOf(sw4.h()));
                            cd6 cd62 = cd6.a;
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                }
            }
            y04 g2 = PortfolioApp.get.instance().g();
            if (b2 != null) {
                HomeDianaCustomizeFragment homeDianaCustomizeFragment = b2;
                if (b3 != null) {
                    HomeHybridCustomizeFragment homeHybridCustomizeFragment = b3;
                    if (b6 != null) {
                        HomeProfileFragment homeProfileFragment = b6;
                        if (b4 != null) {
                            HomeAlertsFragment homeAlertsFragment = b4;
                            if (b5 != null) {
                                HomeAlertsHybridFragment homeAlertsHybridFragment = b5;
                                if (b7 != null) {
                                    g2.a(new ex4(a2, homeDianaCustomizeFragment, homeHybridCustomizeFragment, homeProfileFragment, homeAlertsFragment, homeAlertsHybridFragment, b7)).a(this);
                                    return;
                                }
                                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        wg6.b(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i2 == 2131363190) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.D;
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        wg6.a((Object) activity, "activity!!");
                        aVar.a(activity, PortfolioApp.get.instance().e(), false);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363105) {
                    FLogger.INSTANCE.getLocal().d(x, "Cancel Zendesk feedback");
                    return;
                } else if (i2 == 2131363190) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Zendesk feedback");
                    sw4 sw4 = this.r;
                    if (sw4 != null) {
                        sw4.b("Feedback - From app [Fossil] - [Android] ");
                        return;
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363105) {
                    FLogger.INSTANCE.getLocal().d(x, "Send Zendesk feedback");
                    lx5 lx5 = lx5.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    wg6.a((Object) childFragmentManager, "childFragmentManager");
                    lx5.m(childFragmentManager);
                    return;
                } else if (i2 == 2131363190) {
                    FLogger.INSTANCE.getLocal().d(x, "Open app rating dialog");
                    lx5 lx52 = lx5.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    wg6.a((Object) childFragmentManager2, "childFragmentManager");
                    lx52.d(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363105) {
                    FLogger.INSTANCE.getLocal().d(x, "Stay in app");
                    return;
                } else if (i2 == 2131363190) {
                    FLogger.INSTANCE.getLocal().d(x, "Go to Play Store");
                    Object instance = PortfolioApp.get.instance();
                    tj4.a aVar2 = tj4.f;
                    String packageName = instance.getPackageName();
                    wg6.a((Object) packageName, "packageName");
                    aVar2.b(instance, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public void a(Intent intent) {
        wg6.b(intent, "intent");
        HomeDashboardPresenter homeDashboardPresenter = this.g;
        if (homeDashboardPresenter != null) {
            if (homeDashboardPresenter != null) {
                homeDashboardPresenter.a(intent);
            } else {
                wg6.d("mHomeDashboardPresenter");
                throw null;
            }
        }
        HomeProfilePresenter homeProfilePresenter = this.j;
        if (homeProfilePresenter == null) {
            return;
        }
        if (homeProfilePresenter != null) {
            homeProfilePresenter.b(intent);
        } else {
            wg6.d("mHomeProfilePresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Menu menu, Typeface typeface) {
        if (typeface != null) {
            int size = menu.size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = menu.getItem(i2);
                wg6.a((Object) item, "getItem(index)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(item.getTitle());
                spannableStringBuilder.setSpan(new ny5(typeface), 0, spannableStringBuilder.length(), 0);
                item.setTitle(spannableStringBuilder);
            }
        }
    }
}
