package com.portfolio.platform.uirenew.home.profile.theme.user;

import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.qo5;
import com.fossil.qo5$c$a;
import com.fossil.qo5$d$a;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeViewModel extends td {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public MutableLiveData<qo5.b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b((String) null, 1, (qg6) null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public String a;

        @DexIgnore
        public b() {
            this((String) null, 1, (qg6) null);
        }

        @DexIgnore
        public b(String str) {
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(String str, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : str);
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = UserCustomizeThemeViewModel.d;
            local.d(e, "update themeName=" + str);
            this.a = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateCurrentThemeName$1", f = "UserCustomizeThemeViewModel.kt", l = {30}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(UserCustomizeThemeViewModel userCustomizeThemeViewModel, jh6 jh6, xe6 xe6) {
            super(2, xe6);
            this.this$0 = userCustomizeThemeViewModel;
            this.$id = jh6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$id, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                qo5$c$a qo5_c_a = new qo5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, qo5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.a((String) obj);
            this.this$0.a();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateNewName$1", f = "UserCustomizeThemeViewModel.kt", l = {42}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(UserCustomizeThemeViewModel userCustomizeThemeViewModel, jh6 jh6, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = userCustomizeThemeViewModel;
            this.$id = jh6;
            this.$name = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$id, this.$name, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                qo5$d$a qo5_d_a = new qo5$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, qo5_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.a(this.$name);
            this.this$0.a();
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = UserCustomizeThemeViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "UserCustomizeThemeViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public UserCustomizeThemeViewModel(ThemeRepository themeRepository) {
        wg6.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        jh6 jh6 = new jh6();
        jh6.element = UserCustomizeThemeFragment.p.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "updateCurrentThemeName id=" + ((String) jh6.element));
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new c(this, jh6, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<qo5.b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        d();
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "name");
        jh6 jh6 = new jh6();
        jh6.element = UserCustomizeThemeFragment.p.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = d;
        local.d(str2, "updateNewName id=" + ((String) jh6.element) + " name=" + str);
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new d(this, jh6, str, (xe6) null), 3, (Object) null);
    }
}
