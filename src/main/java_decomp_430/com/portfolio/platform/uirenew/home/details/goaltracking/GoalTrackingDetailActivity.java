package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.hi5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((qg6) null);
    @DexIgnore
    public GoalTrackingDetailPresenter B;
    @DexIgnore
    public Date C; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            wg6.b(context, "context");
            Intent intent = new Intent(context, GoalTrackingDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        GoalTrackingDetailFragment b = getSupportFragmentManager().b(2131362119);
        Intent intent = getIntent();
        if (intent != null) {
            this.C = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (b == null) {
            b = GoalTrackingDetailFragment.y.a(this.C);
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new hi5(b)).a(this);
    }
}
