package com.portfolio.platform.uirenew.home.alerts.hybrid;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.c34;
import com.fossil.f15;
import com.fossil.g15;
import com.fossil.hh4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.va4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.xm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsHybridFragment extends BasePermissionFragment implements g15 {
    @DexIgnore
    public static /* final */ a o; // = new a((qg6) null);
    @DexIgnore
    public ax5<va4> g;
    @DexIgnore
    public f15 h;
    @DexIgnore
    public c34 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeAlertsHybridFragment a() {
            return new HomeAlertsHybridFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c34.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment a;

        @DexIgnore
        public b(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.a = homeAlertsHybridFragment;
        }

        @DexIgnore
        public void a(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsHybridFragment.a(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        public void b(Alarm alarm) {
            wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsHybridFragment.a(this.a).a(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment a;

        @DexIgnore
        public c(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.a = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                wg6.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment a;

        @DexIgnore
        public d(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.a = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsHybridFragment.a(this.a).a((Alarm) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment a;

        @DexIgnore
        public e(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.a = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (xm4.a(xm4.d, this.a.getContext(), "NOTIFICATION_CONTACTS", false, false, false, 28, (Object) null) && xm4.a(xm4.d, this.a.getContext(), "NOTIFICATION_APPS", false, false, false, 28, (Object) null)) {
                NotificationDialLandingActivity.C.a(this.a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment a;

        @DexIgnore
        public f(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.a = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsHybridFragment.a(this.a).h();
        }
    }

    @DexIgnore
    public static final /* synthetic */ f15 a(HomeAlertsHybridFragment homeAlertsHybridFragment) {
        f15 f15 = homeAlertsHybridFragment.h;
        if (f15 != null) {
            return f15;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void c(List<Alarm> list) {
        wg6.b(list, "alarms");
        c34 c34 = this.i;
        if (c34 != null) {
            c34.a(list);
        } else {
            wg6.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void h(boolean z) {
        Context context;
        int i2;
        ax5<va4> ax5 = this.g;
        if (ax5 != null) {
            va4 a2 = ax5.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.A;
                wg6.a((Object) flexibleSwitchCompat, "it.swScheduled");
                flexibleSwitchCompat.setChecked(z);
                Object r0 = a2.v;
                if (z) {
                    context = getContext();
                    if (context != null) {
                        i2 = 2131100009;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    context = getContext();
                    if (context != null) {
                        i2 = 2131100402;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                r0.setTextColor(w6.a(context, i2));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "HomeAlertsHybridFragment";
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void n(boolean z) {
        LinearLayout linearLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeAlertsHybridFragment", "updateAssignNotificationState() - isEnable = " + z);
        ax5<va4> ax5 = this.g;
        if (ax5 != null) {
            va4 a2 = ax5.a();
            if (a2 != null && (linearLayout = a2.x) != null) {
                wg6.a((Object) linearLayout, "it");
                linearLayout.setAlpha(z ? 0.5f : 1.0f);
                linearLayout.setClickable(!z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r6v10, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r7v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v16, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r7v18, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v18, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        va4 a2 = kb.a(layoutInflater, 2131558565, viewGroup, false, e1());
        W("alert_view");
        String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            a2.B.setBackgroundColor(parseColor);
            a2.C.setBackgroundColor(parseColor);
            a2.q.setBackgroundColor(parseColor);
            a2.r.setBackgroundColor(parseColor);
        }
        String b3 = ThemeManager.l.a().b("onPrimaryButton");
        if (!TextUtils.isEmpty(b3)) {
            int parseColor2 = Color.parseColor(b3);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231028);
            if (drawable != null) {
                drawable.setTint(parseColor2);
                a2.s.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        a2.s.setOnClickListener(new d(this));
        a2.x.setOnClickListener(new e(this));
        a2.A.setOnClickListener(new f(this));
        hh4 hh4 = a2.w;
        if (hh4 != null) {
            ConstraintLayout constraintLayout = hh4.q;
            wg6.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            hh4.t.setImageResource(2131230820);
            Object r7 = hh4.r;
            wg6.a((Object) r7, "viewNoDeviceBinding.ftvDescription");
            r7.setText(jm4.a(getContext(), 2131886807));
            hh4.s.setOnClickListener(new c(this));
        }
        c34 c34 = new c34();
        c34.a((c34.b) new b(this));
        this.i = c34;
        RecyclerView recyclerView = a2.z;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        c34 c342 = this.i;
        if (c342 != null) {
            recyclerView.setAdapter(c342);
            this.g = new ax5<>(this, a2);
            ax5<va4> ax5 = this.g;
            if (ax5 != null) {
                va4 a3 = ax5.a();
                if (a3 != null) {
                    wg6.a((Object) a3, "mBinding.get()!!");
                    return a3.d();
                }
                wg6.a();
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        f15 f15 = this.h;
        if (f15 != null) {
            if (f15 != null) {
                f15.g();
                kl4 g1 = g1();
                if (g1 != null) {
                    g1.a("");
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        HomeAlertsHybridFragment.super.onPause();
    }

    @DexIgnore
    public void onResume() {
        HomeAlertsHybridFragment.super.onResume();
        f15 f15 = this.h;
        if (f15 == null) {
            return;
        }
        if (f15 != null) {
            f15.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void r() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.G(childFragmentManager);
        }
    }

    @DexIgnore
    public void s() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsHybridFragment", "notifyListAlarm()");
        c34 c34 = this.i;
        if (c34 != null) {
            c34.notifyDataSetChanged();
        } else {
            wg6.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.P(childFragmentManager);
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v5, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v8, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void r(boolean z) {
        ax5<va4> ax5 = this.g;
        if (ax5 != null) {
            va4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerView recyclerView = a2.z;
                wg6.a((Object) recyclerView, "binding.rvAlarms");
                recyclerView.setVisibility(0);
                Object r5 = a2.u;
                wg6.a((Object) r5, "binding.ftvAlarmsSection");
                r5.setVisibility(0);
                Object r52 = a2.s;
                wg6.a((Object) r52, "binding.btnAdd");
                r52.setVisibility(0);
                return;
            }
            RecyclerView recyclerView2 = a2.z;
            wg6.a((Object) recyclerView2, "binding.rvAlarms");
            recyclerView2.setVisibility(8);
            Object r53 = a2.u;
            wg6.a((Object) r53, "binding.ftvAlarmsSection");
            r53.setVisibility(8);
            Object r54 = a2.s;
            wg6.a((Object) r54, "binding.btnAdd");
            r54.setVisibility(8);
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(f15 f15) {
        wg6.b(f15, "presenter");
        this.h = f15;
    }

    @DexIgnore
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        wg6.b(str, "deviceId");
        wg6.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.C;
        Context context = getContext();
        if (context != null) {
            wg6.a((Object) context, "context!!");
            aVar.a(context, str, arrayList, alarm);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(boolean z) {
        ConstraintLayout constraintLayout;
        ax5<va4> ax5 = this.g;
        if (ax5 != null) {
            va4 a2 = ax5.a();
            if (a2 != null && (constraintLayout = a2.t) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
