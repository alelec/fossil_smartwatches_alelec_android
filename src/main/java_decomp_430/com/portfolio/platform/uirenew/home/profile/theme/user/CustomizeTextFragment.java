package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.ko5;
import com.fossil.l84;
import com.fossil.ld;
import com.fossil.lo5;
import com.fossil.lx5;
import com.fossil.ly5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeTextFragment extends BaseFragment implements ly5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String o;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public CustomizeTextViewModel g;
    @DexIgnore
    public ax5<l84> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CustomizeTextFragment.o;
        }

        @DexIgnore
        public final String b() {
            return CustomizeTextFragment.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<lo5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeTextFragment a;

        @DexIgnore
        public b(CustomizeTextFragment customizeTextFragment) {
            this.a = customizeTextFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CustomizeTextViewModel.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.s(a2.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.u(c.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.r(b.intValue());
                }
                Integer d = bVar.d();
                if (d != null) {
                    this.a.t(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeTextFragment a;

        @DexIgnore
        public c(CustomizeTextFragment customizeTextFragment) {
            this.a = customizeTextFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 101);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeTextFragment a;

        @DexIgnore
        public d(CustomizeTextFragment customizeTextFragment) {
            this.a = customizeTextFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 102);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeTextFragment a;

        @DexIgnore
        public e(CustomizeTextFragment customizeTextFragment) {
            this.a = customizeTextFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.g(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = CustomizeTextFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeTextFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363190) {
            CustomizeTextViewModel customizeTextViewModel = this.g;
            if (customizeTextViewModel != null) {
                customizeTextViewModel.a(UserCustomizeThemeFragment.p.a(), o, p);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf(i3 & 16777215)};
        String format = String.format("#%06X", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        CustomizeTextViewModel customizeTextViewModel = this.g;
        if (customizeTextViewModel != null) {
            customizeTextViewModel.a(i2, Color.parseColor(format));
            if (i2 == 101) {
                o = format;
            } else if (i2 == 102) {
                p = format;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        l84 a2 = kb.a(LayoutInflater.from(getContext()), 2131558533, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new ko5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            CustomizeTextViewModel a3 = vd.a(this, w04).a(CustomizeTextViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026extViewModel::class.java)");
            this.g = a3;
            CustomizeTextViewModel customizeTextViewModel = this.g;
            if (customizeTextViewModel != null) {
                customizeTextViewModel.b().a(getViewLifecycleOwner(), new b(this));
                CustomizeTextViewModel customizeTextViewModel2 = this.g;
                if (customizeTextViewModel2 != null) {
                    customizeTextViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        CustomizeTextFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        o = null;
        p = null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CustomizeTextFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        CustomizeTextViewModel customizeTextViewModel = this.g;
        if (customizeTextViewModel != null) {
            customizeTextViewModel.c();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<l84> ax5 = this.h;
        if (ax5 != null) {
            l84 a2 = ax5.a();
            if (a2 != null) {
                a2.r.setOnClickListener(new c(this));
                a2.s.setOnClickListener(new d(this));
                a2.q.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void r(int i2) {
        ax5<l84> ax5 = this.h;
        if (ax5 != null) {
            l84 a2 = ax5.a();
            if (a2 != null) {
                a2.t.setTextColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        ax5<l84> ax5 = this.h;
        if (ax5 != null) {
            l84 a2 = ax5.a();
            if (a2 != null) {
                a2.v.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void t(int i2) {
        ax5<l84> ax5 = this.h;
        if (ax5 != null) {
            l84 a2 = ax5.a();
            if (a2 != null) {
                a2.u.setTextColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void u(int i2) {
        ax5<l84> ax5 = this.h;
        if (ax5 != null) {
            l84 a2 = ax5.a();
            if (a2 != null) {
                a2.w.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
