package com.portfolio.platform.uirenew.home.profile.goal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.uk5;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileGoalEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((qg6) null);
    @DexIgnore
    public ProfileGoalEditFragment B;
    @DexIgnore
    public ProfileGoalEditPresenter C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            Intent intent = new Intent(context, ProfileGoalEditActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        ProfileGoalEditFragment profileGoalEditFragment = this.B;
        if (profileGoalEditFragment != null) {
            profileGoalEditFragment.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
        ProfileGoalEditFragment profileGoalEditFragment = this.B;
        if (profileGoalEditFragment != null) {
            profileGoalEditFragment.i1();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        this.B = getSupportFragmentManager().b(2131362119);
        if (this.B == null) {
            this.B = ProfileGoalEditFragment.p.b();
            ProfileGoalEditFragment profileGoalEditFragment = this.B;
            if (profileGoalEditFragment != null) {
                a((Fragment) profileGoalEditFragment, ProfileGoalEditFragment.p.a(), 2131362119);
            } else {
                wg6.a();
                throw null;
            }
        }
        y04 g = PortfolioApp.get.instance().g();
        ProfileGoalEditFragment profileGoalEditFragment2 = this.B;
        if (profileGoalEditFragment2 != null) {
            g.a(new uk5(profileGoalEditFragment2)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditContract.View");
    }
}
