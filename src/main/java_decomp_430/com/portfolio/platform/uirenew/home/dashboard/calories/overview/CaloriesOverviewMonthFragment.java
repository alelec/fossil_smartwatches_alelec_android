package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.kb;
import com.fossil.pd5;
import com.fossil.qd5;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.z64;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewMonthFragment extends BaseFragment implements qd5 {
    @DexIgnore
    public ax5<z64> f;
    @DexIgnore
    public pd5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewMonthFragment a;

        @DexIgnore
        public b(CaloriesOverviewMonthFragment caloriesOverviewMonthFragment) {
            this.a = caloriesOverviewMonthFragment;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            wg6.b(calendar, "calendar");
            pd5 a2 = this.a.g;
            if (a2 != null) {
                Date time = calendar.getTime();
                wg6.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewMonthFragment a;

        @DexIgnore
        public c(CaloriesOverviewMonthFragment caloriesOverviewMonthFragment) {
            this.a = caloriesOverviewMonthFragment;
        }

        @DexIgnore
        public void a(int i, Calendar calendar) {
            wg6.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                CaloriesDetailActivity.a aVar = CaloriesDetailActivity.D;
                Date time = calendar.getTime();
                wg6.a((Object) time, "it.time");
                wg6.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "CaloriesOverviewMonthFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        z64 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        ax5<z64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            pd5 pd5 = this.g;
            if ((pd5 != null ? pd5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                recyclerViewCalendar.b("dianaActiveCaloriesTab");
            } else {
                recyclerViewCalendar.b("hybridActiveCaloriesTab");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        z64 a2;
        wg6.b(layoutInflater, "inflater");
        CaloriesOverviewMonthFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onCreateView");
        z64 a3 = kb.a(layoutInflater, 2131558512, viewGroup, false, e1());
        RecyclerViewCalendar recyclerViewCalendar = a3.q;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        a3.q.setOnCalendarMonthChanged(new b(this));
        a3.q.setOnCalendarItemClickListener(new c(this));
        this.f = new ax5<>(this, a3);
        j1();
        ax5<z64> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CaloriesOverviewMonthFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onResume");
        j1();
        pd5 pd5 = this.g;
        if (pd5 != null) {
            pd5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        CaloriesOverviewMonthFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onStop");
        pd5 pd5 = this.g;
        if (pd5 != null) {
            pd5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(TreeMap<Long, Float> treeMap) {
        z64 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        wg6.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ax5<z64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setTintColor(w6.a(PortfolioApp.get.instance(), R.color.activeCalories));
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(true);
        }
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        z64 a2;
        wg6.b(date, "selectDate");
        wg6.b(date2, "startDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ax5<z64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            wg6.a((Object) instance, "selectCalendar");
            instance.setTime(date);
            wg6.a((Object) instance2, "startCalendar");
            instance2.setTime(bk4.o(date2));
            wg6.a((Object) instance3, "endCalendar");
            instance3.setTime(bk4.j(instance3.getTime()));
            a2.q.a(instance, instance2, instance3);
        }
    }

    @DexIgnore
    public void a(pd5 pd5) {
        wg6.b(pd5, "presenter");
        this.g = pd5;
    }
}
