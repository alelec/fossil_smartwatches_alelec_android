package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.os.Bundle;
import android.util.Pair;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.fi5;
import com.fossil.gg6;
import com.fossil.gi5;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ji5$b$a;
import com.fossil.ji5$c$a;
import com.fossil.ji5$d$a;
import com.fossil.ji5$e$a;
import com.fossil.ji5$e$b;
import com.fossil.ji5$f$a;
import com.fossil.ji5$f$b;
import com.fossil.ji5$g$a;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.u04;
import com.fossil.v3;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.xp6;
import com.fossil.yx5;
import com.fossil.zp6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailPresenter extends fi5 implements vk4.a {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<lc6<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ xp6 j; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public List<GoalTrackingSummary> k; // = new ArrayList();
    @DexIgnore
    public GoalTrackingSummary l;
    @DexIgnore
    public List<GoalTrackingData> m;
    @DexIgnore
    public LiveData<yx5<List<GoalTrackingSummary>>> n;
    @DexIgnore
    public Listing<GoalTrackingData> o;
    @DexIgnore
    public /* final */ gi5 p;
    @DexIgnore
    public /* final */ GoalTrackingRepository q;
    @DexIgnore
    public /* final */ GoalTrackingDao r;
    @DexIgnore
    public /* final */ GoalTrackingDatabase s;
    @DexIgnore
    public /* final */ u04 t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {206}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$date, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                String uuid = UUID.randomUUID().toString();
                wg6.a((Object) uuid, "UUID.randomUUID().toString()");
                Date date = this.$date;
                TimeZone timeZone = TimeZone.getDefault();
                wg6.a((Object) timeZone, "TimeZone.getDefault()");
                DateTime a2 = bk4.a(date, timeZone.getRawOffset() / 1000);
                wg6.a((Object) a2, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                wg6.a((Object) timeZone2, "TimeZone.getDefault()");
                GoalTrackingData goalTrackingData = new GoalTrackingData(uuid, a2, timeZone2.getRawOffset() / 1000, this.$date, new Date().getTime(), new Date().getTime());
                dl6 b = this.this$0.c();
                ji5$b$a ji5_b_a = new ji5$b$a(this, goalTrackingData, (xe6) null);
                this.L$0 = il6;
                this.L$1 = goalTrackingData;
                this.label = 1;
                if (gk6.a(b, ji5_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                GoalTrackingData goalTrackingData2 = (GoalTrackingData) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {215}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingData $raw;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(GoalTrackingDetailPresenter goalTrackingDetailPresenter, GoalTrackingData goalTrackingData, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$raw = goalTrackingData;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$raw, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                ji5$c$a ji5_c_a = new ji5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, ji5_c_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<cf<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Date b;

        @DexIgnore
        public d(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date) {
            this.a = goalTrackingDetailPresenter;
            this.b = date;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cf<GoalTrackingData> cfVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("getGoalTrackingDataPaging observer size=");
            sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
            local.d("GoalTrackingDetailPresenter", sb.toString());
            if (cfVar != null) {
                this.a.i = true;
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new ji5$d$a(cfVar, (xe6) null, this), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1", f = "GoalTrackingDetailPresenter.kt", l = {146, 264, 171}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$date, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v30, resolved type: com.fossil.xp6} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0165  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x01a0 A[Catch:{ all -> 0x01eb }, RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x01b1 A[Catch:{ all -> 0x01eb }] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01f0  */
        public final Object invokeSuspend(Object obj) {
            xp6 xp6;
            Object obj2;
            GoalTrackingSummary goalTrackingSummary;
            il6 il6;
            boolean z;
            Boolean bool;
            lc6 lc6;
            Pair<Date, Date> pair;
            il6 il62;
            lc6 lc62;
            Object obj3;
            GoalTrackingDetailPresenter goalTrackingDetailPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il62 = this.p$;
                if (this.this$0.e == null) {
                    goalTrackingDetailPresenter = this.this$0;
                    dl6 a2 = goalTrackingDetailPresenter.b();
                    ji5$e$a ji5_e_a = new ji5$e$a((xe6) null);
                    this.L$0 = il62;
                    this.L$1 = goalTrackingDetailPresenter;
                    this.label = 1;
                    obj3 = gk6.a(a2, ji5_e_a, this);
                    if (obj3 == a) {
                        return a;
                    }
                }
                il6 = il62;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("GoalTrackingDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
                this.this$0.f = this.$date;
                z = bk4.c(this.this$0.e, this.$date);
                bool = bk4.t(this.$date);
                gi5 m = this.this$0.p;
                Date date = this.$date;
                wg6.a((Object) bool, "isToday");
                m.a(date, z, bool.booleanValue(), !bk4.c(new Date(), this.$date));
                pair = bk4.a(this.$date, this.this$0.e);
                wg6.a((Object) pair, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                lc62 = (lc6) this.this$0.g.a();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("GoalTrackingDetailPresenter", "setDate - rangeDateValue=" + lc62 + ", newRange=" + new lc6(pair.first, pair.second));
                if (lc62 == null || !bk4.d((Date) lc62.getFirst(), (Date) pair.first) || !bk4.d((Date) lc62.getSecond(), (Date) pair.second)) {
                    this.this$0.h = false;
                    this.this$0.i = false;
                    GoalTrackingDetailPresenter goalTrackingDetailPresenter2 = this.this$0;
                    goalTrackingDetailPresenter2.d(goalTrackingDetailPresenter2.f);
                    this.this$0.g.a(new lc6(pair.first, pair.second));
                    return cd6.a;
                }
                xp6 = this.this$0.j;
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair;
                this.L$3 = lc62;
                this.L$4 = xp6;
                this.label = 2;
                if (xp6.a((Object) null, this) == a) {
                    return a;
                }
                lc6 = lc62;
                dl6 a3 = this.this$0.b();
                ji5$e$b ji5_e_b = new ji5$e$b((xe6) null, this);
                this.L$0 = il6;
                this.Z$0 = z;
                this.L$1 = bool;
                this.L$2 = pair;
                this.L$3 = lc6;
                this.L$4 = xp6;
                this.label = 3;
                obj2 = gk6.a(a3, ji5_e_b, this);
                if (obj2 == a) {
                }
                goalTrackingSummary = (GoalTrackingSummary) obj2;
                if (!wg6.a((Object) this.this$0.l, (Object) goalTrackingSummary)) {
                }
                this.this$0.p.a(this.this$0.l);
                this.this$0.d(this.this$0.f);
                rm6 unused = this.this$0.m();
                cd6 cd6 = cd6.a;
                xp6.a((Object) null);
                return cd6.a;
            } else if (i == 1) {
                goalTrackingDetailPresenter = (GoalTrackingDetailPresenter) this.L$1;
                il62 = (il6) this.L$0;
                nc6.a(obj);
                obj3 = obj;
            } else if (i == 2) {
                lc6 = (lc6) this.L$3;
                z = this.Z$0;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                xp6 = (xp6) this.L$4;
                pair = (Pair) this.L$2;
                bool = (Boolean) this.L$1;
                try {
                    dl6 a32 = this.this$0.b();
                    ji5$e$b ji5_e_b2 = new ji5$e$b((xe6) null, this);
                    this.L$0 = il6;
                    this.Z$0 = z;
                    this.L$1 = bool;
                    this.L$2 = pair;
                    this.L$3 = lc6;
                    this.L$4 = xp6;
                    this.label = 3;
                    obj2 = gk6.a(a32, ji5_e_b2, this);
                    if (obj2 == a) {
                        return a;
                    }
                    goalTrackingSummary = (GoalTrackingSummary) obj2;
                    if (!wg6.a((Object) this.this$0.l, (Object) goalTrackingSummary)) {
                    }
                    this.this$0.p.a(this.this$0.l);
                    this.this$0.d(this.this$0.f);
                    rm6 unused2 = this.this$0.m();
                    cd6 cd62 = cd6.a;
                    xp6.a((Object) null);
                    return cd6.a;
                } catch (Throwable th) {
                    th = th;
                    xp6.a((Object) null);
                    throw th;
                }
            } else if (i == 3) {
                xp6 xp62 = this.L$4;
                lc6 lc63 = (lc6) this.L$3;
                Pair pair2 = (Pair) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                il6 il63 = (il6) this.L$0;
                try {
                    nc6.a(obj);
                    xp6 = xp62;
                    obj2 = obj;
                    goalTrackingSummary = (GoalTrackingSummary) obj2;
                    if (!wg6.a((Object) this.this$0.l, (Object) goalTrackingSummary)) {
                        this.this$0.l = goalTrackingSummary;
                    }
                    this.this$0.p.a(this.this$0.l);
                    this.this$0.d(this.this$0.f);
                    if (this.this$0.h && this.this$0.i) {
                        rm6 unused3 = this.this$0.m();
                    }
                    cd6 cd622 = cd6.a;
                    xp6.a((Object) null);
                    return cd6.a;
                } catch (Throwable th2) {
                    th = th2;
                    xp6 = xp62;
                    xp6.a((Object) null);
                    throw th;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            goalTrackingDetailPresenter.e = (Date) obj3;
            il6 = il62;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("GoalTrackingDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
            this.this$0.f = this.$date;
            z = bk4.c(this.this$0.e, this.$date);
            bool = bk4.t(this.$date);
            gi5 m2 = this.this$0.p;
            Date date2 = this.$date;
            wg6.a((Object) bool, "isToday");
            m2.a(date2, z, bool.booleanValue(), !bk4.c(new Date(), this.$date));
            pair = bk4.a(this.$date, this.this$0.e);
            wg6.a((Object) pair, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            lc62 = (lc6) this.this$0.g.a();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("GoalTrackingDetailPresenter", "setDate - rangeDateValue=" + lc62 + ", newRange=" + new lc6(pair.first, pair.second));
            if (lc62 == null || !bk4.d((Date) lc62.getFirst(), (Date) pair.first) || !bk4.d((Date) lc62.getSecond(), (Date) pair.second)) {
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1", f = "GoalTrackingDetailPresenter.kt", l = {264, 231, 234}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(GoalTrackingDetailPresenter goalTrackingDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:32:0x00df, code lost:
            r3 = com.fossil.hf6.a(r3.getGoalTarget() / 16);
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00cf A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00d0  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00eb A[Catch:{ all -> 0x0027 }] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x00f0 A[Catch:{ all -> 0x0027 }] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc A[Catch:{ all -> 0x0027 }] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0101 A[Catch:{ all -> 0x0027 }] */
        public final Object invokeSuspend(Object obj) {
            xp6 xp6;
            xp6 xp62;
            lc6 lc6;
            ArrayList arrayList;
            Integer a;
            il6 il6;
            Object a2;
            il6 il62;
            Object a3 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il62 = this.p$;
                xp6 = this.this$0.j;
                this.L$0 = il62;
                this.L$1 = xp6;
                this.label = 1;
                if (xp6.a((Object) null, this) == a3) {
                    return a3;
                }
            } else if (i == 1) {
                xp6 = (xp6) this.L$1;
                nc6.a(obj);
                il62 = (il6) this.L$0;
            } else if (i == 2) {
                xp6 = (xp6) this.L$1;
                il6 = (il6) this.L$0;
                try {
                    nc6.a(obj);
                    lc6 lc62 = (lc6) obj;
                    ArrayList arrayList2 = (ArrayList) lc62.getFirst();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$0.f + ", data=" + arrayList2);
                    dl6 a4 = this.this$0.b();
                    ji5$f$a ji5_f_a = new ji5$f$a(arrayList2, (xe6) null);
                    this.L$0 = il6;
                    this.L$1 = xp6;
                    this.L$2 = lc62;
                    this.L$3 = arrayList2;
                    this.label = 3;
                    a2 = gk6.a(a4, ji5_f_a, this);
                    if (a2 != a3) {
                        return a3;
                    }
                    arrayList = arrayList2;
                    xp6 xp63 = xp6;
                    lc6 = lc62;
                    obj = a2;
                    xp62 = xp63;
                    Integer num = (Integer) obj;
                    GoalTrackingSummary g = this.this$0.l;
                    if (g == null || a == null) {
                    }
                    this.this$0.p.a(new BarChart.c(Math.max(num == null ? num.intValue() : 0, r3 / 16), r3, arrayList), (ArrayList) lc6.getSecond());
                    cd6 cd6 = cd6.a;
                    xp62.a((Object) null);
                    return cd6.a;
                } catch (Throwable th) {
                    th = th;
                }
            } else if (i == 3) {
                arrayList = (ArrayList) this.L$3;
                lc6 = (lc6) this.L$2;
                xp62 = (xp6) this.L$1;
                il6 il63 = (il6) this.L$0;
                try {
                    nc6.a(obj);
                    Integer num2 = (Integer) obj;
                    GoalTrackingSummary g2 = this.this$0.l;
                    int intValue = (g2 == null || a == null) ? 8 : a.intValue();
                    this.this$0.p.a(new BarChart.c(Math.max(num2 == null ? num2.intValue() : 0, intValue / 16), intValue, arrayList), (ArrayList) lc6.getSecond());
                    cd6 cd62 = cd6.a;
                    xp62.a((Object) null);
                    return cd6.a;
                } catch (Throwable th2) {
                    th = th2;
                    xp6 = xp62;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dl6 a5 = this.this$0.b();
            ji5$f$b ji5_f_b = new ji5$f$b((xe6) null, this);
            this.L$0 = il62;
            this.L$1 = xp6;
            this.label = 2;
            Object a6 = gk6.a(a5, ji5_f_b, this);
            if (a6 == a3) {
                return a3;
            }
            Object obj2 = a6;
            il6 = il62;
            obj = obj2;
            lc6 lc622 = (lc6) obj;
            ArrayList arrayList22 = (ArrayList) lc622.getFirst();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("GoalTrackingDetailPresenter", "showDetailChart - date=" + this.this$0.f + ", data=" + arrayList22);
            dl6 a42 = this.this$0.b();
            ji5$f$a ji5_f_a2 = new ji5$f$a(arrayList22, (xe6) null);
            this.L$0 = il6;
            this.L$1 = xp6;
            this.L$2 = lc622;
            this.L$3 = arrayList22;
            this.label = 3;
            a2 = gk6.a(a42, ji5_f_a2, this);
            if (a2 != a3) {
            }
            xp6.a((Object) null);
            throw th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<yx5<? extends List<GoalTrackingSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

        @DexIgnore
        public g(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            this.a = goalTrackingDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<GoalTrackingSummary>> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingDetailPresenter", "start - mGoalTrackingSummary -- goalTrackingSummary=" + yx5);
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                this.a.k = yx5 != null ? (List) yx5.d() : null;
                this.a.h = true;
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new ji5$g$a(this, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

        @DexIgnore
        public h(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            this.a = goalTrackingDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<GoalTrackingSummary>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.q.getSummaries((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GoalTrackingDetailPresenter(gi5 gi5, GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, u04 u04) {
        wg6.b(gi5, "mView");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(u04, "mAppExecutors");
        this.p = gi5;
        this.q = goalTrackingRepository;
        this.r = goalTrackingDao;
        this.s = goalTrackingDatabase;
        this.t = u04;
        LiveData<yx5<List<GoalTrackingSummary>>> b2 = sd.b(this.g, new h(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.n = b2;
    }

    @DexIgnore
    public final void d(Date date) {
        LiveData<cf<GoalTrackingData>> pagedList;
        h();
        GoalTrackingRepository goalTrackingRepository = this.q;
        this.o = goalTrackingRepository.getGoalTrackingDataPaging(date, goalTrackingRepository, this.r, this.s, this.t, this);
        Listing<GoalTrackingData> listing = this.o;
        if (listing != null && (pagedList = listing.getPagedList()) != null) {
            gi5 gi5 = this.p;
            if (gi5 != null) {
                pagedList.a((GoalTrackingDetailFragment) gi5, new d(this, date));
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "start");
        n();
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "stop");
        LiveData<yx5<List<GoalTrackingSummary>>> liveData = this.n;
        gi5 gi5 = this.p;
        if (gi5 != null) {
            liveData.a((GoalTrackingDetailFragment) gi5);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    public void h() {
        LiveData<cf<GoalTrackingData>> pagedList;
        try {
            Listing<GoalTrackingData> listing = this.o;
            if (listing != null && (pagedList = listing.getPagedList()) != null) {
                gi5 gi5 = this.p;
                if (gi5 != null) {
                    pagedList.a((GoalTrackingDetailFragment) gi5);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("GoalTrackingDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        gg6<cd6> retry;
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "retry all failed request");
        Listing<GoalTrackingData> listing = this.o;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }

    @DexIgnore
    public void j() {
        Date m2 = bk4.m(this.f);
        wg6.a((Object) m2, "DateHelper.getNextDate(mDate)");
        c(m2);
    }

    @DexIgnore
    public void k() {
        Date n2 = bk4.n(this.f);
        wg6.a((Object) n2, "DateHelper.getPrevDate(mDate)");
        c(n2);
    }

    @DexIgnore
    public void l() {
        this.p.a(this);
    }

    @DexIgnore
    public final rm6 m() {
        return ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void n() {
        LiveData<yx5<List<GoalTrackingSummary>>> liveData = this.n;
        gi5 gi5 = this.p;
        if (gi5 != null) {
            liveData.a((GoalTrackingDetailFragment) gi5, new g(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    public void c(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, date, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void b(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        d(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wg6.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "addRecord date=" + date);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, date, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(GoalTrackingData goalTrackingData) {
        wg6.b(goalTrackingData, "raw");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "deleteRecord GoalTrackingData=" + goalTrackingData);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, goalTrackingData, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final GoalTrackingSummary a(Date date, List<GoalTrackingSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (bk4.d(((GoalTrackingSummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (GoalTrackingSummary) t2;
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        wg6.b(gVar, "report");
        if (gVar.a()) {
            this.p.f();
        }
    }
}
