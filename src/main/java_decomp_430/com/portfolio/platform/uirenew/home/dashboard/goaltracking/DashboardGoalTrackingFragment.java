package com.portfolio.platform.uirenew.home.dashboard.goaltracking;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ae5;
import com.fossil.av4;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.bu4;
import com.fossil.bv4;
import com.fossil.cf;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.la5;
import com.fossil.pg;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tz5;
import com.fossil.vd;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x84;
import com.fossil.yu4;
import com.fossil.zd5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardGoalTrackingFragment extends BaseFragment implements ae5, bv4, bu4 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<x84> f;
    @DexIgnore
    public zd5 g;
    @DexIgnore
    public yu4 h;
    @DexIgnore
    public GoalTrackingOverviewFragment i;
    @DexIgnore
    public tz5 j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DashboardGoalTrackingFragment.p;
        }

        @DexIgnore
        public final DashboardGoalTrackingFragment b() {
            return new DashboardGoalTrackingFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends tz5 {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardGoalTrackingFragment e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, DashboardGoalTrackingFragment dashboardGoalTrackingFragment, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = dashboardGoalTrackingFragment;
        }

        @DexIgnore
        public void a(int i) {
            DashboardGoalTrackingFragment.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = DashboardGoalTrackingFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "DashboardGoalTrackingFra\u2026::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ zd5 a(DashboardGoalTrackingFragment dashboardGoalTrackingFragment) {
        zd5 zd5 = dashboardGoalTrackingFragment.g;
        if (zd5 != null) {
            return zd5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void Q(boolean z) {
        x84 j1;
        RecyclerView recyclerView;
        View view;
        if (isVisible() && this.f != null && (j1 = j1()) != null && (recyclerView = j1.q) != null) {
            RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
            if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != 0.0f) {
                recyclerView.smoothScrollToPosition(0);
                tz5 tz5 = this.j;
                if (tz5 != null) {
                    tz5.a();
                }
            }
        }
    }

    @DexIgnore
    public void b(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.D;
            wg6.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f() {
        tz5 tz5 = this.j;
        if (tz5 != null) {
            tz5.a();
        }
    }

    @DexIgnore
    public String h1() {
        return p;
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public final x84 j1() {
        ax5<x84> ax5 = this.f;
        if (ax5 != null) {
            return ax5.a();
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        DashboardGoalTrackingFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558539, viewGroup, false, e1()));
        ax5<x84> ax5 = this.f;
        if (ax5 != null) {
            x84 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        DashboardGoalTrackingFragment.super.onDestroy();
        zd5 zd5 = this.g;
        if (zd5 == null) {
            return;
        }
        if (zd5 != null) {
            zd5.i();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onDestroyView() {
        zd5 zd5 = this.g;
        if (zd5 != null) {
            zd5.i();
            super.onDestroyView();
            d1();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        DashboardGoalTrackingFragment.super.onResume();
        zd5 zd5 = this.g;
        if (zd5 != null) {
            zd5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        DashboardGoalTrackingFragment.super.onStop();
        zd5 zd5 = this.g;
        if (zd5 != null) {
            zd5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        this.i = getChildFragmentManager().b("GoalTrackingOverviewFragment");
        if (this.i == null) {
            this.i = new GoalTrackingOverviewFragment();
        }
        av4 av4 = new av4();
        PortfolioApp instance = PortfolioApp.get.instance();
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.i;
        if (goalTrackingOverviewFragment != null) {
            this.h = new yu4(av4, instance, this, childFragmentManager, goalTrackingOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            x84 j1 = j1();
            if (!(j1 == null || (recyclerView2 = j1.q) == null)) {
                wg6.a((Object) recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                yu4 yu4 = this.h;
                if (yu4 != null) {
                    recyclerView2.setAdapter(yu4);
                    LinearLayoutManager layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        this.j = new b(recyclerView2, layoutManager, this, linearLayoutManager);
                        tz5 tz5 = this.j;
                        if (tz5 != null) {
                            recyclerView2.addOnScrollListener(tz5);
                            recyclerView2.setItemViewCacheSize(0);
                            la5 la5 = new la5(linearLayoutManager.Q());
                            Drawable c = w6.c(recyclerView2.getContext(), 2131230876);
                            if (c != null) {
                                wg6.a((Object) c, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                la5.a(c);
                                recyclerView2.addItemDecoration(la5);
                                zd5 zd5 = this.g;
                                if (zd5 != null) {
                                    zd5.h();
                                } else {
                                    wg6.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    wg6.d("mDashboardGoalTrackingAdapter");
                    throw null;
                }
            }
            x84 j12 = j1();
            if (j12 != null) {
                RecyclerView recyclerView3 = j12.q;
            }
            x84 j13 = j1();
            if (!(j13 == null || (recyclerView = j13.q) == null)) {
                wg6.a((Object) recyclerView, "recyclerView");
                pg itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof pg) {
                    itemAnimator.setSupportsChangeAnimations(false);
                }
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                b06 a2 = vd.a(activity).a(b06.class);
                wg6.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                b06 b06 = a2;
                return;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(cf<GoalTrackingSummary> cfVar) {
        yu4 yu4 = this.h;
        if (yu4 != null) {
            yu4.c(cfVar);
        } else {
            wg6.d("mDashboardGoalTrackingAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(zd5 zd5) {
        wg6.b(zd5, "presenter");
        this.g = zd5;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        wg6.b(date, "startWeekDate");
        wg6.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }
}
