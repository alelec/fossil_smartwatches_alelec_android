package com.portfolio.platform.uirenew.home.alerts.diana;

import android.content.Context;
import android.text.SpannableString;
import android.text.format.DateFormat;
import androidx.lifecycle.LiveData;
import com.fossil.AppWrapper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.d15;
import com.fossil.ff6;
import com.fossil.gy4;
import com.fossil.hi4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.nx4;
import com.fossil.ox4;
import com.fossil.p06;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rl6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sx4$d$a;
import com.fossil.sx4$d$b;
import com.fossil.sx4$e$a;
import com.fossil.uh4;
import com.fossil.vx4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.y24;
import com.fossil.yk4;
import com.fossil.z24;
import com.fossil.zv4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsPresenter extends nx4 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a((qg6) null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.get.instance().f();
    @DexIgnore
    public /* final */ LiveData<List<DNDScheduledTimeModel>> f; // = this.v.getDNDScheduledTimeDao().getListDNDScheduledTime();
    @DexIgnore
    public ArrayList<Alarm> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public List<vx4> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ ox4 l;
    @DexIgnore
    public /* final */ z24 m;
    @DexIgnore
    public /* final */ AlarmHelper n;
    @DexIgnore
    public /* final */ d15 o;
    @DexIgnore
    public /* final */ mz4 p;
    @DexIgnore
    public /* final */ gy4 q;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase r;
    @DexIgnore
    public /* final */ SetAlarms s;
    @DexIgnore
    public /* final */ AlarmsRepository t;
    @DexIgnore
    public /* final */ an4 u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HomeAlertsPresenter.w;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<zv4.d, zv4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(HomeAlertsPresenter homeAlertsPresenter, Alarm alarm) {
            this.a = homeAlertsPresenter;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.a.l.a();
            this.a.b(this.b, true);
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.l.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomeAlertsPresenter.x.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.l.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.l.w();
                }
                this.a.b(bVar.a(), false);
                return;
            }
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.b());
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            ox4 o = this.a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                o.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                this.a.b(bVar.a(), false);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.d<gy4.c, y24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public c(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public void onSuccess(gy4.c cVar) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onSuccess");
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886334);
            ox4 o = this.a.l;
            wg6.a((Object) a2, "notificationAppOverView");
            o.h(a2);
            this.a.k();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void a(y24.a aVar) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), ".Inside mSaveAppsNotification onError");
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886753);
            ox4 o = this.a.l;
            wg6.a((Object) a2, "notificationAppOverView");
            o.h(a2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1", f = "HomeAlertsPresenter.kt", l = {430, 432}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HomeAlertsPresenter homeAlertsPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeAlertsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0106  */
        public final Object invokeSuspend(Object obj) {
            long j;
            List list;
            Object obj2;
            List list2;
            rl6 rl6;
            il6 il6;
            rl6 rl62;
            List list3;
            Object obj3;
            List list4;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                list3 = new ArrayList();
                j = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomeAlertsPresenter.x.a();
                local.d(a2, "filter notification, time start=" + j);
                list3.clear();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = HomeAlertsPresenter.x.a();
                local2.d(a3, "mListAppWrapper.size = " + this.this$0.i().size());
                rl6 a4 = ik6.a(il62, this.this$0.b(), (ll6) null, new sx4$d$b(this, (xe6) null), 2, (Object) null);
                rl62 = ik6.a(il62, this.this$0.b(), (ll6) null, new sx4$d$a(this, (xe6) null), 2, (Object) null);
                this.L$0 = il62;
                this.L$1 = list3;
                this.J$0 = j;
                this.L$2 = a4;
                this.L$3 = rl62;
                this.L$4 = list3;
                this.label = 1;
                obj3 = a4.a(this);
                if (obj3 == a) {
                    return a;
                }
                rl6 = a4;
                il6 = il62;
                list4 = list3;
            } else if (i == 1) {
                list4 = (List) this.L$4;
                j = this.J$0;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                rl6 = (rl6) this.L$2;
                list3 = (List) this.L$1;
                rl62 = (rl6) this.L$3;
                obj3 = obj;
            } else if (i == 2) {
                rl6 rl63 = (rl6) this.L$3;
                rl6 rl64 = (rl6) this.L$2;
                long j2 = this.J$0;
                list = (List) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                j = j2;
                obj2 = obj;
                list2 = (List) obj2;
                if (list2 != null) {
                    list.addAll(list2);
                    long b = PortfolioApp.get.instance().b(new AppNotificationFilterSettings(list, System.currentTimeMillis()), PortfolioApp.get.instance().e());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String a5 = HomeAlertsPresenter.x.a();
                    local3.d(a5, "filter notification, time end= " + b);
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a6 = HomeAlertsPresenter.x.a();
                    local4.d(a6, "delayTime =" + (b - j) + " milliseconds");
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list4.addAll((Collection) obj3);
            this.L$0 = il6;
            this.L$1 = list3;
            this.J$0 = j;
            this.L$2 = rl6;
            this.L$3 = rl62;
            this.label = 2;
            obj2 = rl62.a(this);
            if (obj2 == a) {
                return a;
            }
            list = list3;
            list2 = (List) obj2;
            if (list2 != null) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public e(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.a.l.a(true);
            } else {
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new sx4$e$a(this, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ld<String> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public f(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ox4 unused = this.a.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<List<? extends DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsPresenter a;

        @DexIgnore
        public g(HomeAlertsPresenter homeAlertsPresenter) {
            this.a = homeAlertsPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DNDScheduledTimeModel> list) {
            ox4 unused = this.a.l;
        }
    }

    /*
    static {
        String simpleName = HomeAlertsPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "HomeAlertsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public HomeAlertsPresenter(ox4 ox4, z24 z24, AlarmHelper alarmHelper, d15 d15, mz4 mz4, gy4 gy4, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, an4 an4, DNDSettingsDatabase dNDSettingsDatabase) {
        wg6.b(ox4, "mView");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(alarmHelper, "mAlarmHelper");
        wg6.b(d15, "mGetApps");
        wg6.b(mz4, "mGetAllContactGroup");
        wg6.b(gy4, "mSaveAppsNotification");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        wg6.b(setAlarms, "mSetAlarms");
        wg6.b(alarmsRepository, "mAlarmRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.l = ox4;
        this.m = z24;
        this.n = alarmHelper;
        this.o = d15;
        this.p = mz4;
        this.q = gy4;
        this.r = notificationSettingsDatabase;
        this.s = setAlarms;
        this.t = alarmsRepository;
        this.u = an4;
        this.v = dNDSettingsDatabase;
    }

    @DexIgnore
    @p06
    public final void onSetAlarmEventEndComplete(hi4 hi4) {
        if (hi4 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + hi4);
            if (hi4.b()) {
                String a2 = hi4.a();
                Iterator<Alarm> it = this.g.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (wg6.a((Object) next.getUri(), (Object) a2)) {
                        next.setActive(false);
                    }
                }
                this.l.s();
            }
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(w, "start");
        this.s.f();
        PortfolioApp.get.b((Object) this);
        LiveData<String> liveData = this.e;
        ox4 ox4 = this.l;
        if (ox4 != null) {
            liveData.a((HomeAlertsFragment) ox4, new e(this));
            BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        this.e.b(new f(this));
        this.f.b(new g(this));
        this.s.g();
        PortfolioApp.get.c(this);
    }

    @DexIgnore
    public void h() {
        this.h = !this.h;
        this.u.e(this.h);
        this.l.m(this.h);
    }

    @DexIgnore
    public final List<vx4> i() {
        return this.j;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void j() {
        FLogger.INSTANCE.getLocal().d(w, "onSetAlarmsSuccess");
        this.n.d(PortfolioApp.get.instance());
        String str = (String) this.e.a();
        if (str != null) {
            PortfolioApp instance = PortfolioApp.get.instance();
            wg6.a((Object) str, "it");
            instance.l(str);
        }
    }

    @DexIgnore
    public final void k() {
        xm4 xm4 = xm4.d;
        ox4 ox4 = this.l;
        if (ox4 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
        } else if (xm4.a(xm4, ((HomeAlertsFragment) ox4).getContext(), "SET_NOTIFICATION", false, false, false, 28, (Object) null)) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public void l() {
        this.l.a(this);
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        wg6.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.g.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (wg6.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.g;
                ArrayList<Alarm> arrayList2 = arrayList;
                arrayList2.set(arrayList.indexOf(next), Alarm.copy$default(alarm, (String) null, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 4095, (Object) null));
                if (!z) {
                    break;
                }
                j();
            }
            Alarm alarm2 = alarm;
        }
        this.l.c(this.g);
    }

    @DexIgnore
    public final boolean a(List<vx4> list, List<vx4> list2) {
        Boolean bool;
        T t2;
        wg6.b(list, "listDatabaseAppWrapper");
        wg6.b(list2, "listAppWrapper");
        if (list.size() != list2.size()) {
            return true;
        }
        boolean z = false;
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            AppWrapper appWrapper = (AppWrapper) it.next();
            Iterator<T> it2 = list2.iterator();
            while (true) {
                bool = null;
                if (!it2.hasNext()) {
                    t2 = null;
                    break;
                }
                t2 = it2.next();
                InstalledApp installedApp = ((AppWrapper) t2).getInstalledApp();
                String identifier = installedApp != null ? installedApp.getIdentifier() : null;
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                if (wg6.a((Object) identifier, (Object) installedApp2 != null ? installedApp2.getIdentifier() : null)) {
                    break;
                }
            }
            AppWrapper appWrapper2 = (AppWrapper) t2;
            if (appWrapper2 != null) {
                InstalledApp installedApp3 = appWrapper2.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                InstalledApp installedApp4 = appWrapper.getInstalledApp();
                if (installedApp4 != null) {
                    bool = installedApp4.isSelected();
                }
                if (!(!wg6.a((Object) isSelected, (Object) bool))) {
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public void a(Alarm alarm) {
        CharSequence charSequence = (CharSequence) this.e.a();
        if (charSequence == null || charSequence.length() == 0) {
            FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.g.size() < 32) {
            ox4 ox4 = this.l;
            Object a2 = this.e.a();
            if (a2 != null) {
                wg6.a(a2, "mActiveSerial.value!!");
                ox4.a((String) a2, this.g, alarm);
                return;
            }
            wg6.a();
            throw null;
        } else {
            this.l.r();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final SpannableString b(int i2) {
        int i3 = i2 / 60;
        int i4 = i2 % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            StringBuilder sb = new StringBuilder();
            nh6 nh6 = nh6.a;
            Locale locale = Locale.US;
            wg6.a((Object) locale, "Locale.US");
            Object[] objArr = {Integer.valueOf(i3)};
            String format = String.format(locale, "%02d", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
            sb.append(':');
            nh6 nh62 = nh6.a;
            Locale locale2 = Locale.US;
            wg6.a((Object) locale2, "Locale.US");
            Object[] objArr2 = {Integer.valueOf(i4)};
            String format2 = String.format(locale2, "%02d", Arrays.copyOf(objArr2, objArr2.length));
            wg6.a((Object) format2, "java.lang.String.format(locale, format, *args)");
            sb.append(format2);
            return new SpannableString(sb.toString());
        }
        int i5 = 12;
        if (i2 < 720) {
            if (i3 == 0) {
                i3 = 12;
            }
            yk4 yk4 = yk4.b;
            StringBuilder sb2 = new StringBuilder();
            nh6 nh63 = nh6.a;
            Locale locale3 = Locale.US;
            wg6.a((Object) locale3, "Locale.US");
            Object[] objArr3 = {Integer.valueOf(i3)};
            String format3 = String.format(locale3, "%02d", Arrays.copyOf(objArr3, objArr3.length));
            wg6.a((Object) format3, "java.lang.String.format(locale, format, *args)");
            sb2.append(format3);
            sb2.append(':');
            nh6 nh64 = nh6.a;
            Locale locale4 = Locale.US;
            wg6.a((Object) locale4, "Locale.US");
            Object[] objArr4 = {Integer.valueOf(i4)};
            String format4 = String.format(locale4, "%02d", Arrays.copyOf(objArr4, objArr4.length));
            wg6.a((Object) format4, "java.lang.String.format(locale, format, *args)");
            sb2.append(format4);
            String sb3 = sb2.toString();
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886102);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
            return yk4.a(sb3, a2, 1.0f);
        }
        if (i3 > 12) {
            i5 = i3 - 12;
        }
        yk4 yk42 = yk4.b;
        StringBuilder sb4 = new StringBuilder();
        nh6 nh65 = nh6.a;
        Locale locale5 = Locale.US;
        wg6.a((Object) locale5, "Locale.US");
        Object[] objArr5 = {Integer.valueOf(i5)};
        String format5 = String.format(locale5, "%02d", Arrays.copyOf(objArr5, objArr5.length));
        wg6.a((Object) format5, "java.lang.String.format(locale, format, *args)");
        sb4.append(format5);
        sb4.append(':');
        nh6 nh66 = nh6.a;
        Locale locale6 = Locale.US;
        wg6.a((Object) locale6, "Locale.US");
        Object[] objArr6 = {Integer.valueOf(i4)};
        String format6 = String.format(locale6, "%02d", Arrays.copyOf(objArr6, objArr6.length));
        wg6.a((Object) format6, "java.lang.String.format(locale, format, *args)");
        sb4.append(format6);
        String sb5 = sb4.toString();
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886104);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
        return yk42.a(sb5, a3, 1.0f);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.SetAlarms] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$b, com.portfolio.platform.CoroutineUseCase$e] */
    public void a(Alarm alarm, boolean z) {
        wg6.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        CharSequence charSequence = (CharSequence) this.e.a();
        if (!(charSequence == null || charSequence.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "enableAlarm - alarmTotalMinue: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.l.b();
            Object r6 = this.s;
            Object a2 = this.e.a();
            if (a2 != null) {
                wg6.a(a2, "mActiveSerial.value!!");
                r6.a(new SetAlarms.c((String) a2, this.g, alarm), new b(this, alarm));
                return;
            }
            wg6.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    public final void a(List<vx4> list) {
        wg6.b(list, "listAppWrapperNotEnabled");
        Iterator<vx4> it = list.iterator();
        while (it.hasNext()) {
            InstalledApp installedApp = it.next().getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(true);
            }
        }
        this.m.a(this.q, new gy4.b(list), new c(this));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886089);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886091);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886090);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }
}
