package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jh6;
import com.fossil.l55$b$a;
import com.fossil.l55$b$b;
import com.fossil.l55$c$a;
import com.fossil.l55$c$b;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh4;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.y45;
import com.fossil.z45;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsPresenter extends y45 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public Gson f; // = new Gson();
    @DexIgnore
    public CommuteTimeSetting g;
    @DexIgnore
    public ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public MFUser i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ z45 k;
    @DexIgnore
    public /* final */ an4 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2", f = "CommuteTimeSettingsPresenter.kt", l = {161, 168}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address;
        @DexIgnore
        public /* final */ /* synthetic */ String $displayInfo;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsPresenter;
            this.$address = str;
            this.$displayInfo = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$address, this.$displayInfo, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(T t) {
            il6 il6;
            jh6 jh6;
            jh6 jh62;
            T a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(t);
                il6 = this.p$;
                jh62 = new jh6();
                dl6 a2 = this.this$0.c();
                l55$b$b l55_b_b = new l55$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = jh62;
                this.L$2 = jh62;
                this.label = 1;
                t = gk6.a(a2, l55_b_b, this);
                if (t == a) {
                    return a;
                }
                jh6 = jh62;
            } else if (i == 1) {
                jh62 = (jh6) this.L$2;
                jh6 = (jh6) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(t);
            } else if (i == 2) {
                jh6 jh63 = (jh6) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(t);
                this.this$0.k.a();
                this.this$0.k.u(true);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            wg6.a((Object) t, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            jh62.element = (List) t;
            if (!((List) jh6.element).contains(this.$address)) {
                if (this.$displayInfo.length() > 0) {
                    if (this.$address.length() > 0) {
                        ((List) jh6.element).add(0, this.$address);
                        if (((List) jh6.element).size() > 5) {
                            jh6.element = ((List) jh6.element).subList(0, 5);
                        }
                        dl6 a3 = this.this$0.c();
                        l55$b$a l55_b_a = new l55$b$a(this, jh6, (xe6) null);
                        this.L$0 = il6;
                        this.L$1 = jh6;
                        this.label = 2;
                        if (gk6.a(a3, l55_b_a, this) == a) {
                            return a;
                        }
                    }
                }
            }
            this.this$0.k.a();
            this.this$0.k.u(true);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2", f = "CommuteTimeSettingsPresenter.kt", l = {73, 74}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
        public final Object invokeSuspend(Object obj) {
            MFUser e;
            il6 il6;
            CommuteTimeSettingsPresenter commuteTimeSettingsPresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                commuteTimeSettingsPresenter = this.this$0;
                dl6 a2 = commuteTimeSettingsPresenter.c();
                l55$c$a l55_c_a = new l55$c$a(this, (xe6) null);
                this.L$0 = il62;
                this.L$1 = commuteTimeSettingsPresenter;
                this.label = 1;
                Object a3 = gk6.a(a2, l55_c_a, this);
                if (a3 == a) {
                    return a;
                }
                Object obj2 = a3;
                il6 = il62;
                obj = obj2;
            } else if (i == 1) {
                commuteTimeSettingsPresenter = (CommuteTimeSettingsPresenter) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                wg6.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
                this.this$0.h.clear();
                this.this$0.h.addAll((List) obj);
                e = this.this$0.i;
                if (e != null) {
                    z45 g = this.this$0.k;
                    String home = e.getHome();
                    if (home == null) {
                        home = "";
                    }
                    g.z(home);
                    z45 g2 = this.this$0.k;
                    String work = e.getWork();
                    if (work == null) {
                        work = "";
                    }
                    g2.G(work);
                }
                this.this$0.k.j(this.this$0.h);
                this.this$0.k.a(this.this$0.g);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            commuteTimeSettingsPresenter.i = (MFUser) obj;
            dl6 a4 = this.this$0.c();
            l55$c$b l55_c_b = new l55$c$b(this, (xe6) null);
            this.L$0 = il6;
            this.label = 2;
            obj = gk6.a(a4, l55_c_b, this);
            if (obj == a) {
                return a;
            }
            wg6.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$0.h.clear();
            this.this$0.h.addAll((List) obj);
            e = this.this$0.i;
            if (e != null) {
            }
            this.this$0.k.j(this.this$0.h);
            this.this$0.k.a(this.this$0.g);
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = CommuteTimeSettingsPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeSettingsPrese\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsPresenter(z45 z45, an4 an4, UserRepository userRepository) {
        wg6.b(z45, "mView");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(userRepository, "mUserRepository");
        this.k = z45;
        this.l = an4;
        this.m = userRepository;
    }

    @DexIgnore
    public void h() {
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setAddress("");
        }
    }

    @DexIgnore
    public CommuteTimeSetting i() {
        return this.g;
    }

    @DexIgnore
    public void j() {
        String home;
        MFUser mFUser = this.i;
        String str = "";
        if (TextUtils.isEmpty(mFUser != null ? mFUser.getHome() : null)) {
            this.j = false;
            this.k.c("Home", str);
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (!(mFUser2 == null || (home = mFUser2.getHome()) == null)) {
                str = home;
            }
            commuteTimeSetting.setAddress(str);
        }
        this.k.u(true);
    }

    @DexIgnore
    public void k() {
        String str;
        if (this.g != null) {
            this.j = true;
            z45 z45 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (str = mFUser.getHome()) == null) {
                str = "";
            }
            z45.c("Home", str);
        }
    }

    @DexIgnore
    public void l() {
        MFUser mFUser = this.i;
        if (TextUtils.isEmpty(mFUser != null ? mFUser.getWork() : null)) {
            this.j = false;
            this.k.c("Other", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (mFUser2 != null) {
                String work = mFUser2.getWork();
                wg6.a((Object) work, "mUser!!.work");
                commuteTimeSetting.setAddress(work);
            } else {
                wg6.a();
                throw null;
            }
        }
        this.k.u(true);
    }

    @DexIgnore
    public void m() {
        String str;
        if (this.g != null) {
            this.j = true;
            z45 z45 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (str = mFUser.getWork()) == null) {
                str = "";
            }
            z45.c("Other", str);
        }
    }

    @DexIgnore
    public void n() {
        this.k.a(this);
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, DatabaseFieldConfigLoader.FIELD_NAME_FORMAT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateFormatType, format = " + str);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setFormat(str);
        }
        this.k.a(this.g);
    }

    @DexIgnore
    public void c(String str) {
        CommuteTimeSetting commuteTimeSetting;
        wg6.b(str, MicroAppSetting.SETTING);
        try {
            commuteTimeSetting = (CommuteTimeSetting) this.f.a(str, CommuteTimeSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = n;
            local.d(str2, "exception when parse commute time setting " + e2);
            commuteTimeSetting = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (qg6) null);
        }
        this.g = commuteTimeSetting;
        if (this.g == null) {
            this.g = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (qg6) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void f() {
        this.e = Places.createClient(PortfolioApp.get.instance());
        this.k.a(this.e);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                this.k.A(commuteTimeSetting.getAddress());
            }
            this.k.O(commuteTimeSetting.getAvoidTolls());
        }
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        this.e = null;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "address");
        if (this.j) {
            this.j = false;
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null && !TextUtils.isEmpty(str)) {
            commuteTimeSetting.setAddress(str);
            this.k.A(str);
        }
    }

    @DexIgnore
    public void a(String str, String str2, nh4 nh4, boolean z, String str3) {
        wg6.b(str, "displayInfo");
        wg6.b(str2, "address");
        wg6.b(nh4, "directionBy");
        wg6.b(str3, DatabaseFieldConfigLoader.FIELD_NAME_FORMAT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = n;
        local.d(str4, "saveCommuteTimeSetting - displayInfo=" + str + ", address=" + str2 + ", directionBy=" + nh4.getType() + ", avoidTolls=" + z + ", format=" + str3);
        this.k.b();
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            boolean z2 = true;
            if (str.length() == 0) {
                if (str2.length() != 0) {
                    z2 = false;
                }
                if (z2) {
                    commuteTimeSetting.setAddress(str);
                }
            } else {
                commuteTimeSetting.setAddress(str2);
            }
            commuteTimeSetting.setAvoidTolls(z);
            commuteTimeSetting.setFormat(str3);
        }
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, str2, str, (xe6) null), 3, (Object) null);
    }
}
