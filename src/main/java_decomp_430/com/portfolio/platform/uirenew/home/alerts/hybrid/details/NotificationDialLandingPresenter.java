package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.os.Bundle;
import android.util.SparseArray;
import androidx.loader.app.LoaderManager;
import com.fossil.ae;
import com.fossil.af6;
import com.fossil.b25$b$a;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.w15;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wg6;
import com.fossil.x15;
import com.fossil.xe6;
import com.fossil.xm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDialLandingPresenter extends w15 implements LoaderManager.a<SparseArray<List<? extends BaseFeatureModel>>> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ x15 e;
    @DexIgnore
    public /* final */ NotificationsLoader f;
    @DexIgnore
    public /* final */ LoaderManager g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter$start$1", f = "NotificationDialLandingPresenter.kt", l = {37}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationDialLandingPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NotificationDialLandingPresenter notificationDialLandingPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationDialLandingPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                b25$b$a b25_b_a = new b25$b$a((xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, b25_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = NotificationDialLandingPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationDialLandingP\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public NotificationDialLandingPresenter(x15 x15, NotificationsLoader notificationsLoader, LoaderManager loaderManager) {
        wg6.b(x15, "mView");
        wg6.b(notificationsLoader, "mNotificationLoader");
        wg6.b(loaderManager, "mLoaderManager");
        this.e = x15;
        this.f = notificationsLoader;
        this.g = loaderManager;
    }

    @DexIgnore
    public void a(ae<SparseArray<List<BaseFeatureModel>>> aeVar) {
        wg6.b(aeVar, "loader");
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(h, "start");
        xm4 xm4 = xm4.d;
        x15 x15 = this.e;
        if (x15 != null) {
            if (xm4.a(xm4, ((NotificationDialLandingFragment) x15).getContext(), "NOTIFICATION_CONTACTS", false, false, false, 28, (Object) null) && xm4.a(xm4.d, ((NotificationDialLandingFragment) this.e).getContext(), "NOTIFICATION_APPS", false, false, false, 28, (Object) null) && !PortfolioApp.get.instance().w().P()) {
                rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
            }
            this.g.a(3, (Bundle) null, this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingFragment");
    }

    @DexIgnore
    public void g() {
        this.g.a(3);
        FLogger.INSTANCE.getLocal().d(h, "stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }

    @DexIgnore
    public ae<SparseArray<List<BaseFeatureModel>>> a(int i, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "onCreateLoader id=" + i);
        return this.f;
    }

    @DexIgnore
    public void a(ae<SparseArray<List<BaseFeatureModel>>> aeVar, SparseArray<List<BaseFeatureModel>> sparseArray) {
        wg6.b(aeVar, "loader");
        if (sparseArray != null) {
            this.e.a(sparseArray);
        } else {
            FLogger.INSTANCE.getLocal().d(h, "onLoadFinished, data=null");
        }
    }
}
