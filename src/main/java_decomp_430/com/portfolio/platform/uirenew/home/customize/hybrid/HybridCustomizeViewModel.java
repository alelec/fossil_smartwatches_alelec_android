package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.dl6;
import com.fossil.e95$c$a;
import com.fossil.e95$d$a;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rk4;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.tj4;
import com.fossil.v3;
import com.fossil.vi4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yi4;
import com.fossil.zi4;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeViewModel extends td {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public HybridPreset a;
    @DexIgnore
    public MutableLiveData<HybridPreset> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> d; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> i;
    @DexIgnore
    public /* final */ LiveData<MicroApp> j;
    @DexIgnore
    public /* final */ LiveData<Boolean> k;
    @DexIgnore
    public /* final */ ld<HybridPreset> l;
    @DexIgnore
    public /* final */ HybridPresetRepository m;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository n;
    @DexIgnore
    public /* final */ MicroAppRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return HybridCustomizeViewModel.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public b(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(HybridPreset hybridPreset) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.q.a();
            local.d(a2, "current preset change=" + hybridPreset);
            if (hybridPreset != null) {
                String str = (String) this.a.e.a();
                String str2 = (String) this.a.f.a();
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                    boolean z = true;
                    if (!wg6.a((Object) hybridPresetAppSetting.getPosition(), (Object) str) || xj6.b(hybridPresetAppSetting.getAppId(), str2, true)) {
                        z = false;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t;
                if (hybridPresetAppSetting2 != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a3 = HybridCustomizeViewModel.q.a();
                    local2.d(a3, "Update new microapp id=" + hybridPresetAppSetting2.getAppId() + " at position=" + str);
                    this.a.f.a(hybridPresetAppSetting2.getAppId());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1", f = "HybridCustomizeViewModel.kt", l = {103, 108}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(HybridCustomizeViewModel hybridCustomizeViewModel, String str, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
            this.$presetId = str;
            this.$microAppPos = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$presetId, this.$microAppPos, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HybridCustomizeViewModel.q.a();
                local.d(a2, "init presetId=" + this.$presetId + " originalPreset=" + this.this$0.a + " microAppPos=" + this.$microAppPos + ' ');
                if (this.this$0.a == null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.this$0;
                    String str = this.$presetId;
                    this.L$0 = il6;
                    this.label = 1;
                    if (hybridCustomizeViewModel.a(str, (xe6<? super cd6>) this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.a(this.$microAppPos);
            cn6 c = zl6.c();
            e95$c$a e95_c_a = new e95$c$a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 2;
            if (gk6.a(c, e95_c_a, this) == a) {
                return a;
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1", f = "HybridCustomizeViewModel.kt", l = {119, 126}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedCurrentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedOriginalPreset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(HybridCustomizeViewModel hybridCustomizeViewModel, String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
            this.$presetId = str;
            this.$savedOriginalPreset = hybridPreset;
            this.$savedCurrentPreset = hybridPreset2;
            this.$microAppPos = str2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$microAppPos, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x00b9  */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HybridCustomizeViewModel.q.a();
                local.d(a2, "initFromSaveInstanceState presetId " + this.$presetId + " savedOriginalPreset=" + this.$savedOriginalPreset + ',' + " savedCurrentPreset=" + this.$savedCurrentPreset + " microAppPos=" + this.$microAppPos + " currentPreset");
                if (this.$savedOriginalPreset == null) {
                    HybridCustomizeViewModel hybridCustomizeViewModel = this.this$0;
                    String str = this.$presetId;
                    this.L$0 = il6;
                    this.label = 1;
                    if (hybridCustomizeViewModel.a(str, (xe6<? super cd6>) this) == a) {
                        return a;
                    }
                } else {
                    this.this$0.f();
                    this.this$0.a = this.$savedOriginalPreset;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                if (this.$savedCurrentPreset != null) {
                    this.this$0.b.a(this.$savedCurrentPreset);
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.a(this.$microAppPos);
            cn6 c = zl6.c();
            e95$d$a e95_d_a = new e95$d$a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 2;
            if (gk6.a(c, e95_d_a, this) == a) {
                return a;
            }
            if (this.$savedCurrentPreset != null) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel", f = "HybridCustomizeViewModel.kt", l = {266, 267}, m = "initializePreset")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(HybridCustomizeViewModel hybridCustomizeViewModel, xe6 xe6) {
            super(xe6);
            this.this$0 = hybridCustomizeViewModel;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, (xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$2", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(HybridCustomizeViewModel hybridCustomizeViewModel, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.f();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super HybridPreset>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(HybridCustomizeViewModel hybridCustomizeViewModel, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
            this.$presetId = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$presetId, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return this.this$0.m.getPresetById(this.$presetId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public h(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<MicroApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.q.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            HybridCustomizeViewModel hybridCustomizeViewModel = this.a;
            wg6.a((Object) str, "id");
            this.a.g.a(hybridCustomizeViewModel.b(str));
            return this.a.g;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public i(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.q.a();
            local.d(a2, "transformMicroAppPosToId pos=" + str);
            HybridPreset hybridPreset = (HybridPreset) this.a.b.a();
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((HybridPresetAppSetting) t).getPosition(), (Object) str)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t;
                if (hybridPresetAppSetting != null) {
                    this.a.f.b(hybridPresetAppSetting.getAppId());
                }
            }
            return this.a.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public j(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(HybridPreset hybridPreset) {
            ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
            Gson c = this.a.h;
            HybridPreset e = this.a.a;
            if (e != null) {
                boolean b = zi4.b(buttons, c, e.getButtons());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DianaCustomizeViewModel.G.a();
                local.d(a2, "isTheSameHybridAppSetting " + b);
                if (b) {
                    this.a.c.a(false);
                } else {
                    this.a.c.a(true);
                }
                return this.a.c;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = HybridCustomizeViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "HybridCustomizeViewModel::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public HybridCustomizeViewModel(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        wg6.b(hybridPresetRepository, "mHybridPresetRepository");
        wg6.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        wg6.b(microAppRepository, "mMicroAppRepository");
        this.m = hybridPresetRepository;
        this.n = microAppLastSettingRepository;
        this.o = microAppRepository;
        LiveData<String> b2 = sd.b(this.e, new i(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026dMicroAppIdLiveData\n    }");
        this.i = b2;
        LiveData<MicroApp> b3 = sd.b(this.i, new h(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.j = b3;
        LiveData<Boolean> b4 = sd.b(this.b, new j(this));
        wg6.a((Object) b4, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.k = b4;
        this.l = new b(this);
    }

    @DexIgnore
    public void onCleared() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onCleared originalPreset=" + this.a + " currentPreset=" + ((HybridPreset) this.b.a()));
        this.b.b(this.l);
        HybridCustomizeViewModel.super.onCleared();
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        LiveData<Boolean> liveData = this.k;
        if (liveData != null) {
            return liveData;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final HybridPreset c() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<MicroApp> d() {
        LiveData<MicroApp> liveData = this.j;
        if (liveData != null) {
            return liveData;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> e() {
        return this.e;
    }

    @DexIgnore
    public final void f() {
        this.d.clear();
        this.d.addAll(this.o.getAllMicroApp(PortfolioApp.get.instance().e()));
    }

    @DexIgnore
    public final boolean g() {
        Boolean bool = (Boolean) this.c.a();
        if (bool == null || bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final rm6 a(String str, String str2) {
        wg6.b(str, "presetId");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new c(this, str, str2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final MicroApp b(String str) {
        T t;
        wg6.b(str, "microAppId");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) str, (Object) ((MicroApp) t).getId())) {
                break;
            }
        }
        return (MicroApp) t;
    }

    @DexIgnore
    public final Parcelable c(String str) {
        T t;
        T t2;
        String settings;
        wg6.b(str, "appId");
        Ringtone ringtone = null;
        if (rk4.c.d(str)) {
            HybridPreset hybridPreset = (HybridPreset) a().a();
            String str2 = "";
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (wg6.a((Object) ((HybridPresetAppSetting) t2).getAppId(), (Object) str)) {
                        break;
                    }
                }
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) t2;
                if (!(hybridPresetAppSetting == null || (settings = hybridPresetAppSetting.getSettings()) == null)) {
                    str2 = settings;
                }
            }
            if (str2.length() == 0) {
                MicroAppLastSetting microAppLastSetting = this.n.getMicroAppLastSetting(str);
                if (microAppLastSetting != null) {
                    str2 = microAppLastSetting.getSetting();
                }
                if (!TextUtils.isEmpty(str2)) {
                    if (hybridPreset != null) {
                        HybridPreset clone = hybridPreset.clone();
                        Iterator<T> it2 = clone.getButtons().iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it2.next();
                            if (wg6.a((Object) ((HybridPresetAppSetting) t).getAppId(), (Object) str)) {
                                break;
                            }
                        }
                        HybridPresetAppSetting hybridPresetAppSetting2 = (HybridPresetAppSetting) t;
                        if (hybridPresetAppSetting2 != null) {
                            hybridPresetAppSetting2.setSettings(str2);
                            a(clone);
                        }
                    }
                } else if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                    List<Ringtone> d2 = tj4.f.d();
                    if (!d2.isEmpty()) {
                        str2 = yi4.a(d2.get(0));
                    }
                }
            }
            if (!vi4.a(str2)) {
                try {
                    if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                        CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) this.h.a(str2, CommuteTimeSetting.class);
                        if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                            return commuteTimeSetting;
                        }
                    } else if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                        SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) this.h.a(str2, SecondTimezoneSetting.class);
                        if (!TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                            return secondTimezoneSetting;
                        }
                    } else if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                        Ringtone ringtone2 = (Ringtone) this.h.a(str2, Ringtone.class);
                        if (!TextUtils.isEmpty(ringtone2.getRingtoneName())) {
                            ringtone = ringtone2;
                        }
                    }
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "exception when parse setting from json " + e2);
                }
            }
        }
        return ringtone;
    }

    @DexIgnore
    public final boolean d(String str) {
        wg6.b(str, "microAppId");
        HybridPreset hybridPreset = (HybridPreset) this.b.a();
        if (hybridPreset == null) {
            return false;
        }
        Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
        while (it.hasNext()) {
            if (wg6.a((Object) it.next().getAppId(), (Object) str)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void e(String str) {
        wg6.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "setSelectedMicroApp watchAppPos=" + str);
        this.e.a(str);
    }

    @DexIgnore
    public final rm6 a(String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2) {
        wg6.b(str, "presetId");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new d(this, str, hybridPreset, hybridPreset2, str2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final MutableLiveData<HybridPreset> a() {
        return this.b;
    }

    @DexIgnore
    public final void a(HybridPreset hybridPreset) {
        wg6.b(hybridPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "savePreset newPreset=" + hybridPreset);
        this.b.a(hybridPreset.clone());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009c A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(String str, xe6<? super cd6> xe6) {
        e eVar;
        Object a2;
        int i2;
        HybridPreset hybridPreset;
        HybridCustomizeViewModel hybridCustomizeViewModel;
        HybridCustomizeViewModel hybridCustomizeViewModel2;
        dl6 a3;
        f fVar;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i3 = eVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                eVar.label = i3 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                a2 = ff6.a();
                i2 = eVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = p;
                    local.d(str2, "initializePreset presetId=" + str);
                    dl6 b2 = zl6.b();
                    g gVar = new g(this, str, (xe6) null);
                    eVar.L$0 = this;
                    eVar.L$1 = str;
                    eVar.label = 1;
                    obj = gk6.a(b2, gVar, eVar);
                    if (obj == a2) {
                        return a2;
                    }
                    hybridCustomizeViewModel2 = this;
                } else if (i2 == 1) {
                    str = (String) eVar.L$1;
                    hybridCustomizeViewModel2 = (HybridCustomizeViewModel) eVar.L$0;
                    nc6.a(obj);
                } else if (i2 == 2) {
                    hybridPreset = (HybridPreset) eVar.L$2;
                    String str3 = (String) eVar.L$1;
                    hybridCustomizeViewModel = (HybridCustomizeViewModel) eVar.L$0;
                    nc6.a(obj);
                    if (hybridPreset != null) {
                        hybridCustomizeViewModel.a = hybridPreset.clone();
                        hybridCustomizeViewModel.b.a(hybridPreset.clone());
                    }
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                HybridPreset hybridPreset2 = (HybridPreset) obj;
                a3 = zl6.a();
                fVar = new f(hybridCustomizeViewModel2, (xe6) null);
                eVar.L$0 = hybridCustomizeViewModel2;
                eVar.L$1 = str;
                eVar.L$2 = hybridPreset2;
                eVar.label = 2;
                if (gk6.a(a3, fVar, eVar) != a2) {
                    return a2;
                }
                hybridPreset = hybridPreset2;
                hybridCustomizeViewModel = hybridCustomizeViewModel2;
                if (hybridPreset != null) {
                }
                return cd6.a;
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        a2 = ff6.a();
        i2 = eVar.label;
        if (i2 != 0) {
        }
        HybridPreset hybridPreset22 = (HybridPreset) obj2;
        a3 = zl6.a();
        fVar = new f(hybridCustomizeViewModel2, (xe6) null);
        eVar.L$0 = hybridCustomizeViewModel2;
        eVar.L$1 = str;
        eVar.L$2 = hybridPreset22;
        eVar.label = 2;
        if (gk6.a(a3, fVar, eVar) != a2) {
        }
    }

    @DexIgnore
    public final List<MicroApp> a(String str) {
        wg6.b(str, "category");
        ArrayList<MicroApp> arrayList = this.d;
        ArrayList arrayList2 = new ArrayList();
        for (T next : arrayList) {
            if (((MicroApp) next).getCategories().contains(str)) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }
}
