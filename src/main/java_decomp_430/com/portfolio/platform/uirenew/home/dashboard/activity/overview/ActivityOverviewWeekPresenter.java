package com.portfolio.platform.uirenew.home.dashboard.activity.overview;

import android.graphics.RectF;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.sh4;
import com.fossil.uc5;
import com.fossil.vc5;
import com.fossil.wc5$b$a;
import com.fossil.wc5$b$b;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewWeekPresenter extends uc5 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.l.e());
    @DexIgnore
    public Date f;
    @DexIgnore
    public LiveData<yx5<List<ActivitySummary>>> g; // = new MutableLiveData();
    @DexIgnore
    public BarChart.c h;
    @DexIgnore
    public /* final */ vc5 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ SummariesRepository k;
    @DexIgnore
    public /* final */ PortfolioApp l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter$start$1", f = "ActivityOverviewWeekPresenter.kt", l = {57}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ActivityOverviewWeekPresenter activityOverviewWeekPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityOverviewWeekPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00b3  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00e0  */
        public final Object invokeSuspend(Object obj) {
            vc5 g;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.f == null || !bk4.t(ActivityOverviewWeekPresenter.e(this.this$0)).booleanValue()) {
                    this.this$0.f = new Date();
                    dl6 a2 = this.this$0.b();
                    wc5$b$b wc5_b_b = new wc5$b$b(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    obj = gk6.a(a2, wc5_b_b, this);
                    if (obj == a) {
                        return a;
                    }
                }
                LiveData b = this.this$0.g;
                g = this.this$0.i;
                if (g == null) {
                    b.a((ActivityOverviewWeekFragment) g, new wc5$b$a(this));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("ActivityOverviewWeekPresenter", "start - mDate=" + ActivityOverviewWeekPresenter.e(this.this$0));
                    return cd6.a;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("ActivityOverviewWeekPresenter", "start - startDate=" + ((Date) lc6.getFirst()) + ", endDate=" + ((Date) lc6.getSecond()));
            ActivityOverviewWeekPresenter activityOverviewWeekPresenter = this.this$0;
            activityOverviewWeekPresenter.g = activityOverviewWeekPresenter.k.getSummaries((Date) lc6.getFirst(), (Date) lc6.getSecond(), false);
            LiveData b2 = this.this$0.g;
            g = this.this$0.i;
            if (g == null) {
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public ActivityOverviewWeekPresenter(vc5 vc5, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        wg6.b(vc5, "mView");
        wg6.b(userRepository, "userRepository");
        wg6.b(summariesRepository, "mSummariesRepository");
        wg6.b(portfolioApp, "mApp");
        this.i = vc5;
        this.j = userRepository;
        this.k = summariesRepository;
        this.l = portfolioApp;
    }

    @DexIgnore
    public static final /* synthetic */ Date e(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
        Date date = activityOverviewWeekPresenter.f;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void j() {
        this.i.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "start");
        i();
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekPresenter", "stop");
        try {
            LiveData<yx5<List<ActivitySummary>>> liveData = this.g;
            vc5 vc5 = this.i;
            if (vc5 != null) {
                liveData.a((ActivityOverviewWeekFragment) vc5);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<ActivitySummary> list) {
        int i2;
        T t;
        boolean z;
        Date date2 = date;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date2);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("ActivityOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
        Calendar instance = Calendar.getInstance(Locale.US);
        wg6.a((Object) instance, "calendar");
        instance.setTime(date2);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            wg6.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    ActivitySummary activitySummary = (ActivitySummary) t;
                    if (activitySummary.getDay() == instance.get(5) && activitySummary.getMonth() == instance.get(2) + 1 && activitySummary.getYear() == instance.get(1)) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                ActivitySummary activitySummary2 = (ActivitySummary) t;
                if (activitySummary2 != null) {
                    int a2 = ik4.d.a(activitySummary2, sh4.TOTAL_STEPS);
                    int steps = (int) activitySummary2.getSteps();
                    i4 = Math.max(Math.max(a2, steps), i4);
                    cVar.a().add(new BarChart.a(a2, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, steps, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
                    i5 = a2;
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 > 0) {
            i5 = i4;
        }
        cVar.b(i5);
        return cVar;
    }

    @DexIgnore
    public final lc6<Date, Date> a(Date date) {
        Date b2 = bk4.b(date, 6);
        MFUser currentUser = this.j.getCurrentUser();
        if (currentUser != null) {
            Date d = bk4.d(currentUser.getCreatedAt());
            if (!bk4.b(b2, d)) {
                b2 = d;
            }
        }
        return new lc6<>(b2, date);
    }
}
