package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.fd5;
import com.fossil.fi4;
import com.fossil.gd5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.tk4;
import com.fossil.ut4;
import com.fossil.wg6;
import com.fossil.x64;
import com.fossil.yk4;
import com.fossil.zf4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewDayFragment extends BaseFragment implements gd5 {
    @DexIgnore
    public ax5<x64> f;
    @DexIgnore
    public fd5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.D;
            Date date = new Date();
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void b(ut4 ut4, ArrayList<String> arrayList) {
        x64 a2;
        OverviewDayChart overviewDayChart;
        wg6.b(ut4, "baseModel");
        wg6.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewDayFragment", "showDayDetails - baseModel=" + ut4);
        ax5<x64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) yk4.b.a(), false, 2, (Object) null);
            }
            overviewDayChart.a(ut4);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "CaloriesOverviewDayFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        x64 a2;
        OverviewDayChart overviewDayChart;
        ax5<x64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.q) != null) {
            fd5 fd5 = this.g;
            if ((fd5 != null ? fd5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewDayChart.a("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        x64 a2;
        wg6.b(layoutInflater, "inflater");
        CaloriesOverviewDayFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onCreateView");
        x64 a3 = kb.a(layoutInflater, 2131558511, viewGroup, false, e1());
        a3.r.setOnClickListener(b.a);
        this.f = new ax5<>(this, a3);
        j1();
        ax5<x64> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CaloriesOverviewDayFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onResume");
        j1();
        fd5 fd5 = this.g;
        if (fd5 != null) {
            fd5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        CaloriesOverviewDayFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onStop");
        fd5 fd5 = this.g;
        if (fd5 != null) {
            fd5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        x64 a2;
        View d;
        View d2;
        wg6.b(list, "workoutSessions");
        ax5<x64> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                wg6.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    zf4 zf4 = a2.t;
                    if (zf4 != null && (d2 = zf4.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                zf4 zf42 = a2.t;
                if (!(zf42 == null || (d = zf42.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            wg6.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(fd5 fd5) {
        wg6.b(fd5, "presenter");
        this.g = fd5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void a(zf4 zf4, WorkoutSession workoutSession) {
        String str;
        if (zf4 != null) {
            View d = zf4.d();
            wg6.a((Object) d, "binding.root");
            Context context = d.getContext();
            lc6<Integer, Integer> a2 = fi4.Companion.a(workoutSession.getWorkoutType());
            String a3 = jm4.a(context, a2.getSecond().intValue());
            zf4.t.setImageResource(a2.getFirst().intValue());
            Object r1 = zf4.r;
            wg6.a((Object) r1, "it.ftvWorkoutTitle");
            r1.setText(a3);
            Object r12 = zf4.s;
            wg6.a((Object) r12, "it.ftvWorkoutValue");
            nh6 nh6 = nh6.a;
            String a4 = jm4.a(context, 2131886415);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026esToday_Text__NumberCals)");
            Object[] objArr = new Object[1];
            Float totalCalorie = workoutSession.getTotalCalorie();
            objArr[0] = tk4.b(totalCalorie != null ? totalCalorie.floatValue() : 0.0f, 1);
            String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            r12.setText(format);
            Object r0 = zf4.q;
            wg6.a((Object) r0, "it.ftvWorkoutTime");
            r0.setText(bk4.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            fd5 fd5 = this.g;
            if ((fd5 != null ? fd5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                str = ThemeManager.l.a().b("dianaActiveCaloriesTab");
            } else {
                str = ThemeManager.l.a().b("hybridActiveCaloriesTab");
            }
            if (str != null) {
                zf4.t.setColorFilter(Color.parseColor(str));
            }
        }
    }
}
