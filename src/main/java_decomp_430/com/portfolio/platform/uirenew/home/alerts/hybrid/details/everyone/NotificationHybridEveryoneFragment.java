package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.d35;
import com.fossil.e35;
import com.fossil.hc4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.pw6;
import com.fossil.qg6;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wg6;
import com.fossil.wx4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridEveryoneFragment extends BaseFragment implements e35, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public d35 f;
    @DexIgnore
    public ax5<hc4> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridEveryoneFragment.i;
        }

        @DexIgnore
        public final NotificationHybridEveryoneFragment b() {
            return new NotificationHybridEveryoneFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryoneFragment a;

        @DexIgnore
        public b(NotificationHybridEveryoneFragment notificationHybridEveryoneFragment) {
            this.a = notificationHybridEveryoneFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationHybridEveryoneFragment.a(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryoneFragment a;

        @DexIgnore
        public c(NotificationHybridEveryoneFragment notificationHybridEveryoneFragment) {
            this.a = notificationHybridEveryoneFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationHybridEveryoneFragment.a(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryoneFragment a;

        @DexIgnore
        public d(NotificationHybridEveryoneFragment notificationHybridEveryoneFragment) {
            this.a = notificationHybridEveryoneFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationHybridEveryoneFragment.a(this.a).j();
        }
    }

    /*
    static {
        String simpleName = NotificationHybridEveryoneFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationHybridEveryo\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ d35 a(NotificationHybridEveryoneFragment notificationHybridEveryoneFragment) {
        d35 d35 = notificationHybridEveryoneFragment.f;
        if (d35 != null) {
            return d35;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(i, ".Inside onPermissionsGranted");
        d35 d35 = this.f;
        if (d35 != null) {
            d35.l();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        d35 d35 = this.f;
        if (d35 != null) {
            d35.k();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        hc4 a2 = kb.a(layoutInflater, 2131558584, viewGroup, false, e1());
        a2.u.setOnClickListener(new b(this));
        a2.q.setOnClickListener(new c(this));
        a2.r.setOnClickListener(new d(this));
        String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            a2.v.setBackgroundColor(Color.parseColor(b2));
        }
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        d35 d35 = this.f;
        if (d35 != null) {
            d35.g();
            NotificationHybridEveryoneFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationHybridEveryoneFragment.super.onResume();
        d35 d35 = this.f;
        if (d35 != null) {
            d35.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(d35 d35) {
        wg6.b(d35, "presenter");
        this.f = d35;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v10, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r1v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v22, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r1v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v14, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r5v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v26, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r5v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r5v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r4v14, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r4v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
    /* JADX WARNING: type inference failed for: r4v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void b(List<wx4> list, int i2) {
        Object r1;
        Object r12;
        Object r13;
        Object r14;
        Object r15;
        Object r16;
        Object r5;
        Object r52;
        Object r53;
        Object r54;
        Object r55;
        Object r56;
        Object r4;
        Object r42;
        Object r43;
        Object r44;
        Object r45;
        Object r46;
        int i3 = i2;
        wg6.b(list, "listContactWrapper");
        boolean z = false;
        boolean z2 = false;
        for (wx4 next : list) {
            Contact contact = next.getContact();
            if (contact == null || contact.getContactId() != -100) {
                Contact contact2 = next.getContact();
                if (contact2 != null && contact2.getContactId() == -200) {
                    if (next.getCurrentHandGroup() != i3) {
                        ax5<hc4> ax5 = this.g;
                        if (ax5 != null) {
                            hc4 a2 = ax5.a();
                            if (!(a2 == null || (r56 = a2.t) == 0)) {
                                nh6 nh6 = nh6.a;
                                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886150);
                                wg6.a((Object) a3, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                Object[] objArr = {Integer.valueOf(next.getCurrentHandGroup())};
                                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                                r56.setText(format);
                            }
                            ax5<hc4> ax52 = this.g;
                            if (ax52 != null) {
                                hc4 a4 = ax52.a();
                                if (!(a4 == null || (r55 = a4.t) == 0)) {
                                    r55.setVisibility(0);
                                }
                                ax5<hc4> ax53 = this.g;
                                if (ax53 != null) {
                                    hc4 a5 = ax53.a();
                                    if (!(a5 == null || (r54 = a5.r) == 0)) {
                                        r54.setChecked(false);
                                    }
                                } else {
                                    wg6.d("mBinding");
                                    throw null;
                                }
                            } else {
                                wg6.d("mBinding");
                                throw null;
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    } else {
                        ax5<hc4> ax54 = this.g;
                        if (ax54 != null) {
                            hc4 a6 = ax54.a();
                            if (!(a6 == null || (r53 = a6.t) == 0)) {
                                r53.setText("");
                            }
                            ax5<hc4> ax55 = this.g;
                            if (ax55 != null) {
                                hc4 a7 = ax55.a();
                                if (!(a7 == null || (r52 = a7.t) == 0)) {
                                    r52.setVisibility(8);
                                }
                                ax5<hc4> ax56 = this.g;
                                if (ax56 != null) {
                                    hc4 a8 = ax56.a();
                                    if (!(a8 == null || (r5 = a8.r) == 0)) {
                                        r5.setChecked(true);
                                    }
                                } else {
                                    wg6.d("mBinding");
                                    throw null;
                                }
                            } else {
                                wg6.d("mBinding");
                                throw null;
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    }
                    z2 = true;
                }
            } else {
                if (next.getCurrentHandGroup() != i3) {
                    ax5<hc4> ax57 = this.g;
                    if (ax57 != null) {
                        hc4 a9 = ax57.a();
                        if (!(a9 == null || (r46 = a9.s) == 0)) {
                            nh6 nh62 = nh6.a;
                            String a10 = jm4.a((Context) PortfolioApp.get.instance(), 2131886150);
                            wg6.a((Object) a10, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                            Object[] objArr2 = {Integer.valueOf(next.getCurrentHandGroup())};
                            String format2 = String.format(a10, Arrays.copyOf(objArr2, objArr2.length));
                            wg6.a((Object) format2, "java.lang.String.format(format, *args)");
                            r46.setText(format2);
                        }
                        ax5<hc4> ax58 = this.g;
                        if (ax58 != null) {
                            hc4 a11 = ax58.a();
                            if (!(a11 == null || (r45 = a11.s) == 0)) {
                                r45.setVisibility(0);
                            }
                            ax5<hc4> ax59 = this.g;
                            if (ax59 != null) {
                                hc4 a12 = ax59.a();
                                if (!(a12 == null || (r44 = a12.q) == 0)) {
                                    r44.setChecked(false);
                                }
                            } else {
                                wg6.d("mBinding");
                                throw null;
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    } else {
                        wg6.d("mBinding");
                        throw null;
                    }
                } else {
                    ax5<hc4> ax510 = this.g;
                    if (ax510 != null) {
                        hc4 a13 = ax510.a();
                        if (!(a13 == null || (r43 = a13.s) == 0)) {
                            r43.setText("");
                        }
                        ax5<hc4> ax511 = this.g;
                        if (ax511 != null) {
                            hc4 a14 = ax511.a();
                            if (!(a14 == null || (r42 = a14.s) == 0)) {
                                r42.setVisibility(8);
                            }
                            ax5<hc4> ax512 = this.g;
                            if (ax512 != null) {
                                hc4 a15 = ax512.a();
                                if (!(a15 == null || (r4 = a15.q) == 0)) {
                                    r4.setChecked(true);
                                }
                            } else {
                                wg6.d("mBinding");
                                throw null;
                            }
                        } else {
                            wg6.d("mBinding");
                            throw null;
                        }
                    } else {
                        wg6.d("mBinding");
                        throw null;
                    }
                }
                z = true;
            }
        }
        if (!z) {
            ax5<hc4> ax513 = this.g;
            if (ax513 != null) {
                hc4 a16 = ax513.a();
                if (!(a16 == null || (r16 = a16.s) == 0)) {
                    r16.setText("");
                }
                ax5<hc4> ax514 = this.g;
                if (ax514 != null) {
                    hc4 a17 = ax514.a();
                    if (!(a17 == null || (r15 = a17.s) == 0)) {
                        r15.setVisibility(8);
                    }
                    ax5<hc4> ax515 = this.g;
                    if (ax515 != null) {
                        hc4 a18 = ax515.a();
                        if (!(a18 == null || (r14 = a18.q) == 0)) {
                            r14.setChecked(false);
                        }
                    } else {
                        wg6.d("mBinding");
                        throw null;
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
        if (!z2) {
            ax5<hc4> ax516 = this.g;
            if (ax516 != null) {
                hc4 a19 = ax516.a();
                if (!(a19 == null || (r13 = a19.t) == 0)) {
                    r13.setText("");
                }
                ax5<hc4> ax517 = this.g;
                if (ax517 != null) {
                    hc4 a20 = ax517.a();
                    if (!(a20 == null || (r12 = a20.t) == 0)) {
                        r12.setVisibility(8);
                    }
                    ax5<hc4> ax518 = this.g;
                    if (ax518 != null) {
                        hc4 a21 = ax518.a();
                        if (a21 != null && (r1 = a21.r) != 0) {
                            r1.setChecked(false);
                            return;
                        }
                        return;
                    }
                    wg6.d("mBinding");
                    throw null;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(wx4 wx4) {
        wg6.b(wx4, "contactWrapper");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            int currentHandGroup = wx4.getCurrentHandGroup();
            d35 d35 = this.f;
            if (d35 != null) {
                lx5.a(childFragmentManager, wx4, currentHandGroup, d35.h());
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(ArrayList<wx4> arrayList) {
        wg6.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wx4 wx4;
        wg6.b(str, "tag");
        if (str.hashCode() == 1018078562 && str.equals("CONFIRM_REASSIGN_CONTACT") && i2 == 2131363190) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (wx4 = (wx4) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                d35 d35 = this.f;
                if (d35 != null) {
                    d35.a(wx4);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(i, ".Inside onPermissionsDenied");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "Permission Denied : " + str);
        }
        if (pw6.a(this, list) && isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.I(childFragmentManager);
        }
    }
}
