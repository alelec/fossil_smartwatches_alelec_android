package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.l24;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.nd4;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.qz4;
import com.fossil.uh4;
import com.fossil.vd;
import com.fossil.vz4;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.xz4;
import com.fossil.yz4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.view.FlexibleEditText;
import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseFragment extends BasePermissionFragment implements qz4.b {
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public w04 g;
    @DexIgnore
    public ax5<nd4> h;
    @DexIgnore
    public QuickResponseViewModel i;
    @DexIgnore
    public qz4 j;
    @DexIgnore
    public ArrayList<QuickResponseMessage> o; // = new ArrayList<>();
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final QuickResponseFragment a() {
            return new QuickResponseFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<yz4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment a;

        @DexIgnore
        public b(QuickResponseFragment quickResponseFragment) {
            this.a = quickResponseFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(QuickResponseViewModel.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("QuickResponseFragment", "data changed " + bVar);
                this.a.a(bVar);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<l24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment a;

        @DexIgnore
        public c(QuickResponseFragment quickResponseFragment) {
            this.a = quickResponseFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(l24.a aVar) {
            if (aVar != null) {
                if (aVar.a()) {
                    this.a.b();
                }
                if (aVar.b()) {
                    this.a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<l24.b> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment a;

        @DexIgnore
        public d(QuickResponseFragment quickResponseFragment) {
            this.a = quickResponseFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(l24.b bVar) {
            if (bVar != null && (!bVar.a().isEmpty())) {
                ArrayList arrayList = new ArrayList();
                for (uh4 ordinal : bVar.a()) {
                    if (vz4.a[ordinal.ordinal()] == 1) {
                        arrayList.add(1);
                    }
                }
                Intent intent = new Intent(this.a.getActivity(), PermissionActivity.class);
                intent.putExtra("PERMISSION_REQUEST_RESPONSE_FLAG", true);
                intent.putIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS", arrayList);
                this.a.startActivityForResult(intent, 1000);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ nd4 a;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment b;

        @DexIgnore
        public e(nd4 nd4, QuickResponseFragment quickResponseFragment) {
            this.a = nd4;
            this.b = quickResponseFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleEditText flexibleEditText = this.a.s;
            wg6.a((Object) flexibleEditText, "etMessage");
            this.b.k1().a(String.valueOf(flexibleEditText.getText()), 50);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment a;

        @DexIgnore
        public f(QuickResponseFragment quickResponseFragment) {
            this.a = quickResponseFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.k1().a((ArrayList<QuickResponseMessage>) this.a.o);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nd4 a;
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment b;

        @DexIgnore
        public g(nd4 nd4, QuickResponseFragment quickResponseFragment) {
            this.a = nd4;
            this.b = quickResponseFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseViewModel k1 = this.b.k1();
            FlexibleEditText flexibleEditText = this.a.s;
            wg6.a((Object) flexibleEditText, "etMessage");
            k1.a(String.valueOf(flexibleEditText.getText()), this.b.o.size(), 10);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<List<? extends QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ QuickResponseFragment a;

        @DexIgnore
        public h(QuickResponseFragment quickResponseFragment) {
            this.a = quickResponseFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* renamed from: a */
        public final void onChanged(List<QuickResponseMessage> list) {
            nd4 a2;
            Object r0;
            if (list.isEmpty()) {
                this.a.k1().h();
                return;
            }
            if (!(list.size() <= this.a.o.size() || (a2 = this.a.j1().a()) == null || (r0 = a2.s) == 0)) {
                r0.setText("");
            }
            this.a.o.clear();
            this.a.o.addAll(list);
            qz4 a3 = QuickResponseFragment.a(this.a);
            wg6.a((Object) list, "qrs");
            a3.a(list);
        }
    }

    @DexIgnore
    public void a1() {
        View d2;
        ax5<nd4> ax5 = this.h;
        if (ax5 != null) {
            nd4 a2 = ax5.a();
            if (a2 != null && (d2 = a2.d()) != null) {
                d2.requestFocus();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void h(int i2, String str) {
        T t;
        boolean z;
        wg6.b(str, "message");
        Iterator<T> it = this.o.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((QuickResponseMessage) t).getId() == i2) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        QuickResponseMessage quickResponseMessage = (QuickResponseMessage) t;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "changed message " + quickResponseMessage + ", new message " + str);
        if (quickResponseMessage != null) {
            quickResponseMessage.setResponse(str);
        }
    }

    @DexIgnore
    public boolean i1() {
        QuickResponseViewModel quickResponseViewModel = this.i;
        if (quickResponseViewModel != null) {
            quickResponseViewModel.a(this.o);
            return false;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final ax5<nd4> j1() {
        ax5<nd4> ax5 = this.h;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final QuickResponseViewModel k1() {
        QuickResponseViewModel quickResponseViewModel = this.i;
        if (quickResponseViewModel != null) {
            return quickResponseViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1000 && i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("QuickResponseFragment", "onActivityResult result code is " + i3);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        nd4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558601, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new xz4()).a(this);
        w04 w04 = this.g;
        if (w04 != null) {
            QuickResponseViewModel a3 = vd.a(this, w04).a(QuickResponseViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026nseViewModel::class.java)");
            this.i = a3;
            QuickResponseViewModel quickResponseViewModel = this.i;
            if (quickResponseViewModel != null) {
                quickResponseViewModel.g().a(getViewLifecycleOwner(), new b(this));
                QuickResponseViewModel quickResponseViewModel2 = this.i;
                if (quickResponseViewModel2 != null) {
                    quickResponseViewModel2.b().a(getViewLifecycleOwner(), new c(this));
                    QuickResponseViewModel quickResponseViewModel3 = this.i;
                    if (quickResponseViewModel3 != null) {
                        quickResponseViewModel3.d().a(getViewLifecycleOwner(), new d(this));
                        this.p = ThemeManager.l.a().b(Constants.YO_ERROR_POST);
                        this.q = ThemeManager.l.a().b("nonBrandSurface");
                        this.h = new ax5<>(this, a2);
                        wg6.a((Object) a2, "binding");
                        return a2.d();
                    }
                    wg6.d("mViewModel");
                    throw null;
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.TextView, java.lang.Object, com.portfolio.platform.view.FlexibleEditText] */
    /* JADX WARNING: type inference failed for: r0v8, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v9, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<nd4> ax5 = this.h;
        if (ax5 != null) {
            nd4 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.t;
                wg6.a((Object) r0, "ftvLimitValue");
                r0.setText(ShareWebViewClient.RESP_SUCC_CODE);
                Object r02 = a2.s;
                wg6.a((Object) r02, "etMessage");
                r02.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(50)});
                Object r03 = a2.s;
                wg6.a((Object) r03, "etMessage");
                r03.addTextChangedListener(new e(a2, this));
                this.j = new qz4(new ArrayList(), this, 50);
                RecyclerView recyclerView = a2.w;
                wg6.a((Object) recyclerView, "rvMessage");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.w;
                wg6.a((Object) recyclerView2, "rvMessage");
                qz4 qz4 = this.j;
                if (qz4 != null) {
                    recyclerView2.setAdapter(qz4);
                    a2.r.setOnClickListener(new f(this));
                    a2.q.setOnClickListener(new g(a2, this));
                } else {
                    wg6.d("mResponseAdapter");
                    throw null;
                }
            }
            QuickResponseViewModel quickResponseViewModel = this.i;
            if (quickResponseViewModel != null) {
                LiveData<List<QuickResponseMessage>> f2 = quickResponseViewModel.f();
                if (f2 != null) {
                    f2.a(getViewLifecycleOwner(), new h(this));
                    return;
                }
                return;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ qz4 a(QuickResponseFragment quickResponseFragment) {
        qz4 qz4 = quickResponseFragment.j;
        if (qz4 != null) {
            return qz4;
        }
        wg6.d("mResponseAdapter");
        throw null;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        FragmentActivity activity;
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != -4398250 || !str.equals("SET TO WATCH FAIL")) {
                return;
            }
            if (i2 == 2131363105) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363190) {
                QuickResponseViewModel quickResponseViewModel = this.i;
                if (quickResponseViewModel != null) {
                    quickResponseViewModel.c(this.o);
                } else {
                    wg6.d("mViewModel");
                    throw null;
                }
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363190) {
                QuickResponseViewModel quickResponseViewModel2 = this.i;
                if (quickResponseViewModel2 != null) {
                    quickResponseViewModel2.c(this.o);
                } else {
                    wg6.d("mViewModel");
                    throw null;
                }
            } else if (i2 == 2131363105 && (activity = getActivity()) != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v20, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v21, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r0v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r6v7, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r6v8, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public final void a(QuickResponseViewModel.b bVar) {
        FragmentActivity activity;
        Boolean a2 = bVar.a();
        if (a2 != null && a2.booleanValue()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.S(childFragmentManager);
        }
        Boolean f2 = bVar.f();
        if (f2 != null && f2.booleanValue()) {
            lx5 lx52 = lx5.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            wg6.a((Object) childFragmentManager2, "childFragmentManager");
            lx52.Q(childFragmentManager2);
        }
        Boolean c2 = bVar.c();
        if (!(c2 == null || !c2.booleanValue() || (activity = getActivity()) == null)) {
            activity.finish();
        }
        Boolean b2 = bVar.b();
        if (b2 != null && b2.booleanValue()) {
            lx5 lx53 = lx5.c;
            FragmentManager childFragmentManager3 = getChildFragmentManager();
            wg6.a((Object) childFragmentManager3, "childFragmentManager");
            lx53.F(childFragmentManager3);
        }
        Boolean d2 = bVar.d();
        if (d2 != null) {
            if (d2.booleanValue()) {
                ax5<nd4> ax5 = this.h;
                if (ax5 != null) {
                    nd4 a3 = ax5.a();
                    if (a3 != null) {
                        if (!TextUtils.isEmpty(this.p)) {
                            Object r6 = a3.s;
                            wg6.a((Object) r6, "etMessage");
                            r6.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.p)));
                        } else {
                            Object r62 = a3.s;
                            wg6.a((Object) r62, "etMessage");
                            r62.setBackgroundTintList(ColorStateList.valueOf(-65536));
                        }
                        Object r0 = a3.u;
                        wg6.a((Object) r0, "ftvLimitedWarning");
                        r0.setVisibility(0);
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                ax5<nd4> ax52 = this.h;
                if (ax52 != null) {
                    nd4 a4 = ax52.a();
                    if (a4 != null) {
                        Object r63 = a4.u;
                        wg6.a((Object) r63, "ftvLimitedWarning");
                        if (r63.getVisibility() == 0) {
                            Object r64 = a4.u;
                            wg6.a((Object) r64, "ftvLimitedWarning");
                            r64.setVisibility(8);
                            if (!TextUtils.isEmpty(this.q)) {
                                Object r02 = a4.s;
                                wg6.a((Object) r02, "etMessage");
                                r02.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.q)));
                            } else {
                                Object r03 = a4.s;
                                wg6.a((Object) r03, "etMessage");
                                r03.setBackgroundTintList(ColorStateList.valueOf(-1));
                            }
                        }
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            }
        }
        Boolean e2 = bVar.e();
        if (e2 != null && e2.booleanValue()) {
            nh6 nh6 = nh6.a;
            String a5 = jm4.a(getContext(), 2131887294);
            wg6.a((Object) a5, "LanguageHelper.getString\u2026response_limited_warning)");
            Object[] objArr = {String.valueOf(10)};
            String format = String.format(a5, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            Toast.makeText(getContext(), format, 1).show();
        }
        Integer g2 = bVar.g();
        if (g2 != null) {
            int intValue = g2.intValue();
            ax5<nd4> ax53 = this.h;
            if (ax53 != null) {
                nd4 a6 = ax53.a();
                if (a6 != null) {
                    Object r04 = a6.t;
                    wg6.a((Object) r04, "ftvLimitValue");
                    r04.setText(String.valueOf(intValue));
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(QuickResponseMessage quickResponseMessage) {
        wg6.b(quickResponseMessage, "qr");
        QuickResponseViewModel quickResponseViewModel = this.i;
        if (quickResponseViewModel != null) {
            quickResponseViewModel.a(quickResponseMessage);
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }
}
