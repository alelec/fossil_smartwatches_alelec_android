package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.du3;
import com.fossil.h6;
import com.fossil.i6;
import com.fossil.m95;
import com.fossil.o95;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.u8;
import com.fossil.vd;
import com.fossil.vq4;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.y04;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a E; // = new a((qg6) null);
    @DexIgnore
    public o95 B;
    @DexIgnore
    public w04 C;
    @DexIgnore
    public HybridCustomizeViewModel D;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            wg6.b(context, "context");
            wg6.b(str, "presetId");
            wg6.b(str2, "microAppPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditActivity", "start - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(context, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r9v5, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        public final void a(FragmentActivity fragmentActivity, String str, ArrayList<u8<View, String>> arrayList, List<? extends u8<CustomizeWidget, String>> list, String str2) {
            wg6.b(fragmentActivity, "context");
            wg6.b(str, "presetId");
            wg6.b(arrayList, "views");
            wg6.b(list, "customizeWidgetViews");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(fragmentActivity, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            for (u8 next : list) {
                arrayList.add(new u8(next.a, next.b));
                Bundle bundle = new Bundle();
                vq4.a aVar = vq4.b;
                Object obj = next.a;
                if (obj != null) {
                    wg6.a(obj, "wcPair.first!!");
                    aVar.a((CustomizeWidget) obj, bundle);
                    Object obj2 = next.a;
                    if (obj2 != null) {
                        wg6.a(obj2, "wcPair.first!!");
                        intent.putExtra(((CustomizeWidget) obj2).getTransitionName(), bundle);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new u8[0]);
            if (array != null) {
                u8[] u8VarArr = (u8[]) array;
                i6 a = i6.a(fragmentActivity, (u8[]) Arrays.copyOf(u8VarArr, u8VarArr.length));
                wg6.a((Object) a, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                h6.a(fragmentActivity, intent, 100, a.a());
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        HybridCustomizeEditActivity.super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362119);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r13v0, types: [com.portfolio.platform.ui.BaseActivity, com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x012e  */
    public void onCreate(Bundle bundle) {
        HybridPreset hybridPreset;
        HybridPreset hybridPreset2;
        HybridCustomizeViewModel hybridCustomizeViewModel;
        Class<HybridPreset> cls = HybridPreset.class;
        super.onCreate(bundle);
        setContentView(2131558428);
        String stringExtra = getIntent().getStringExtra("KEY_PRESET_ID");
        String stringExtra2 = getIntent().getStringExtra("KEY_PRESET_WATCH_APP_POS_SELECTED");
        HybridCustomizeEditFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = new HybridCustomizeEditFragment();
            a((Fragment) b, "DianaCustomizeEditFragment", 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        wg6.a((Object) stringExtra, "presetId");
        wg6.a((Object) stringExtra2, "microAppPos");
        g.a(new m95(b, stringExtra, stringExtra2)).a(this);
        w04 w04 = this.C;
        if (w04 != null) {
            HybridCustomizeViewModel a2 = vd.a(this, w04).a(HybridCustomizeViewModel.class);
            wg6.a((Object) a2, "ViewModelProviders.of(th\u2026izeViewModel::class.java)");
            this.D = a2;
            if (bundle == null) {
                FLogger.INSTANCE.getLocal().d(f(), "init from initialize state");
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.D;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.a(stringExtra, stringExtra2);
                } else {
                    wg6.d("mHybridCustomizeViewModel");
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(f(), "init from savedInstanceState");
                du3 du3 = new du3();
                du3.a(cls, new HybridPresetDeserializer());
                Gson a3 = du3.a();
                try {
                    if (bundle.containsKey("KEY_CURRENT_PRESET")) {
                        FLogger.INSTANCE.getLocal().d(f(), "parse gson " + bundle.getString("KEY_CURRENT_PRESET"));
                        hybridPreset = (HybridPreset) a3.a(bundle.getString("KEY_CURRENT_PRESET"), cls);
                    } else {
                        hybridPreset = null;
                    }
                    try {
                        if (bundle.containsKey("KEY_ORIGINAL_PRESET")) {
                            hybridPreset2 = (HybridPreset) a3.a(bundle.getString("KEY_ORIGINAL_PRESET"), cls);
                            if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                                stringExtra2 = bundle.getString("KEY_PRESET_WATCH_APP_POS_SELECTED");
                            }
                            hybridCustomizeViewModel = this.D;
                            if (hybridCustomizeViewModel == null) {
                                hybridCustomizeViewModel.a(stringExtra, hybridPreset2, hybridPreset, stringExtra2);
                                return;
                            } else {
                                wg6.d("mHybridCustomizeViewModel");
                                throw null;
                            }
                        }
                    } catch (Exception e) {
                        e = e;
                        FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                        hybridPreset2 = null;
                        if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                        }
                        hybridCustomizeViewModel = this.D;
                        if (hybridCustomizeViewModel == null) {
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    hybridPreset = null;
                    FLogger.INSTANCE.getLocal().d(f(), "exception when parse GSON when retrieve from saveInstanceState " + e);
                    hybridPreset2 = null;
                    if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                    }
                    hybridCustomizeViewModel = this.D;
                    if (hybridCustomizeViewModel == null) {
                    }
                }
                hybridPreset2 = null;
                if (bundle.containsKey("KEY_PRESET_WATCH_APP_POS_SELECTED")) {
                }
                hybridCustomizeViewModel = this.D;
                if (hybridCustomizeViewModel == null) {
                }
            }
        } else {
            wg6.d("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        o95 o95 = this.B;
        if (o95 != null) {
            o95.a(bundle);
            if (bundle != null) {
                HybridCustomizeEditActivity.super.onSaveInstanceState(bundle);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }
}
