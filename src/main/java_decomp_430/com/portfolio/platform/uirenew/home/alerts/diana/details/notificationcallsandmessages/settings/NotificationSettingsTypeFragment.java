package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.bz4;
import com.fossil.ez4;
import com.fossil.fz4;
import com.fossil.hz4;
import com.fossil.jb;
import com.fossil.jc4;
import com.fossil.kb;
import com.fossil.o34;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsTypeFragment extends BaseBottomSheetDialogFragment implements fz4 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a((qg6) null);
    @DexIgnore
    public /* final */ jb j; // = new o34(this);
    @DexIgnore
    public ax5<jc4> o;
    @DexIgnore
    public ez4 p;
    @DexIgnore
    public w04 q;
    @DexIgnore
    public bz4 r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationSettingsTypeFragment.t;
        }

        @DexIgnore
        public final NotificationSettingsTypeFragment b() {
            return new NotificationSettingsTypeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsTypeFragment a;

        @DexIgnore
        public b(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
            this.a = notificationSettingsTypeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationSettingsTypeFragment.a(this.a).a(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsTypeFragment a;

        @DexIgnore
        public c(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
            this.a = notificationSettingsTypeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationSettingsTypeFragment.a(this.a).a(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsTypeFragment a;

        @DexIgnore
        public d(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
            this.a = notificationSettingsTypeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationSettingsTypeFragment.a(this.a).a(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsTypeFragment a;

        @DexIgnore
        public e(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
            this.a = notificationSettingsTypeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.dismissAllowingStateLoss();
        }
    }

    /*
    static {
        String simpleName = NotificationSettingsTypeFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationSettingsType\u2026nt::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ez4 a(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
        ez4 ez4 = notificationSettingsTypeFragment.p;
        if (ez4 != null) {
            return ez4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public final void g1() {
        ax5<jc4> ax5 = this.o;
        if (ax5 != null) {
            jc4 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.w;
                wg6.a((Object) r1, "it.ivEveryoneCheck");
                r1.setVisibility(4);
                Object r12 = a2.x;
                wg6.a((Object) r12, "it.ivFavoriteContactsCheck");
                r12.setVisibility(4);
                Object r0 = a2.y;
                wg6.a((Object) r0, "it.ivNoOneCheck");
                r0.setVisibility(4);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void n(int i) {
        g1();
        ax5<jc4> ax5 = this.o;
        if (ax5 != null) {
            jc4 a2 = ax5.a();
            if (a2 == null) {
                return;
            }
            if (i == 0) {
                Object r4 = a2.w;
                wg6.a((Object) r4, "it.ivEveryoneCheck");
                r4.setVisibility(0);
            } else if (i == 1) {
                Object r42 = a2.x;
                wg6.a((Object) r42, "it.ivFavoriteContactsCheck");
                r42.setVisibility(0);
            } else if (i == 2) {
                Object r43 = a2.y;
                wg6.a((Object) r43, "it.ivNoOneCheck");
                r43.setVisibility(0);
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        NotificationSettingsTypeFragment.super.onActivityCreated(bundle);
        PortfolioApp.get.instance().g().a(new hz4(this)).a(this);
        NotificationCallsAndMessagesActivity activity = getActivity();
        if (activity != null) {
            NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = activity;
            w04 w04 = this.q;
            if (w04 != null) {
                bz4 a2 = vd.a(notificationCallsAndMessagesActivity, w04).a(bz4.class);
                wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                this.r = a2;
                ez4 ez4 = this.p;
                if (ez4 != null) {
                    bz4 bz4 = this.r;
                    if (bz4 != null) {
                        ez4.a(bz4);
                    } else {
                        wg6.d("mNotificationSettingViewModel");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else {
                wg6.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r4v8, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        jc4 a2 = kb.a(layoutInflater, 2131558585, viewGroup, false, this.j);
        String b2 = ThemeManager.l.a().b("nonBrandSurface");
        String b3 = ThemeManager.l.a().b("nonBrandDisableCalendarDay");
        if (!TextUtils.isEmpty(b2)) {
            a2.q.setBackgroundColor(Color.parseColor(b2));
        }
        if (!TextUtils.isEmpty(b3)) {
            int parseColor = Color.parseColor(b3);
            a2.z.setBackgroundColor(parseColor);
            a2.A.setBackgroundColor(parseColor);
            a2.B.setBackgroundColor(parseColor);
        }
        a2.r.setOnClickListener(new b(this));
        a2.s.setOnClickListener(new c(this));
        a2.t.setOnClickListener(new d(this));
        a2.v.setOnClickListener(new e(this));
        this.o = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onPause() {
        ez4 ez4 = this.p;
        if (ez4 != null) {
            ez4.g();
            NotificationSettingsTypeFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationSettingsTypeFragment.super.onResume();
        ez4 ez4 = this.p;
        if (ez4 != null) {
            ez4.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ez4 ez4) {
        wg6.b(ez4, "presenter");
        this.p = ez4;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(String str) {
        Object r0;
        wg6.b(str, Explore.COLUMN_TITLE);
        ax5<jc4> ax5 = this.o;
        if (ax5 != null) {
            jc4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.u) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
