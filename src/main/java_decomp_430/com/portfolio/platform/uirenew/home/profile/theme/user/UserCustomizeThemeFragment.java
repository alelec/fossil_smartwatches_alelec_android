package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.l34;
import com.fossil.ld;
import com.fossil.ly5;
import com.fossil.po5;
import com.fossil.qg6;
import com.fossil.qo5;
import com.fossil.re4;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.RenameThemeDialogFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeFragment extends BaseFragment implements ly5 {
    @DexIgnore
    public static String j; // = "";
    @DexIgnore
    public static boolean o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public UserCustomizeThemeViewModel g;
    @DexIgnore
    public ax5<re4> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return b();
        }

        @DexIgnore
        public final String b() {
            return UserCustomizeThemeFragment.j;
        }

        @DexIgnore
        public final UserCustomizeThemeFragment c() {
            return new UserCustomizeThemeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<qo5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeFragment a;

        @DexIgnore
        public b(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            this.a = userCustomizeThemeFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(UserCustomizeThemeViewModel.b bVar) {
            String a2;
            if (bVar != null && (a2 = bVar.a()) != null) {
                this.a.Z(a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeFragment a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements RenameThemeDialogFragment.b {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void a(String str) {
                wg6.b(str, "themeName");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UserCustomizeThemeFragment", "showRenamePresetDialog - themeName=" + str);
                if (!TextUtils.isEmpty(str)) {
                    UserCustomizeThemeFragment.a(this.a.a).a(str);
                }
            }

            @DexIgnore
            public void onCancel() {
                FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "showRenamePresetDialog - onCancel");
            }
        }

        @DexIgnore
        public c(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            this.a = userCustomizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            RenameThemeDialogFragment.g.a("", new a(this)).show(this.a.getChildFragmentManager(), "RenamePresetDialogFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeFragment a;

        @DexIgnore
        public d(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            this.a = userCustomizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ UserCustomizeThemeViewModel a(UserCustomizeThemeFragment userCustomizeThemeFragment) {
        UserCustomizeThemeViewModel userCustomizeThemeViewModel = userCustomizeThemeFragment.g;
        if (userCustomizeThemeViewModel != null) {
            return userCustomizeThemeViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void Z(String str) {
        wg6.b(str, "name");
        ax5<re4> ax5 = this.h;
        if (ax5 != null) {
            re4 a2 = ax5.a();
            if (a2 != null) {
                a2.s.setText(str);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        wg6.b(layoutInflater, "inflater");
        boolean z = false;
        re4 a2 = kb.a(LayoutInflater.from(getContext()), 2131558621, (ViewGroup) null, false, e1());
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("THEME_ID")) == null) {
            str = "";
        }
        j = str;
        Bundle arguments2 = getArguments();
        if (arguments2 != null) {
            z = arguments2.getBoolean("THEME_MODE_EDIT");
        }
        o = z;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UserCustomizeThemeFragment", "themeId=" + j + " isModeEdit=" + o);
        PortfolioApp.get.instance().g().a(new po5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            UserCustomizeThemeViewModel a3 = vd.a(this, w04).a(UserCustomizeThemeViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026emeViewModel::class.java)");
            this.g = a3;
            UserCustomizeThemeViewModel userCustomizeThemeViewModel = this.g;
            if (userCustomizeThemeViewModel != null) {
                userCustomizeThemeViewModel.b().a(getViewLifecycleOwner(), new b(this));
                UserCustomizeThemeViewModel userCustomizeThemeViewModel2 = this.g;
                if (userCustomizeThemeViewModel2 != null) {
                    userCustomizeThemeViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<re4> ax5 = this.h;
        if (ax5 != null) {
            re4 a2 = ax5.a();
            if (a2 != null) {
                ViewPager viewPager = a2.t;
                wg6.a((Object) viewPager, "it.viewPager");
                a(viewPager);
                a2.r.a(a2.t, false);
                a2.s.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v14, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v17, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v20, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v23, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v26, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v29, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v32, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(ViewPager viewPager) {
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        l34 l34 = new l34(childFragmentManager);
        CustomizeTextFragment customizeTextFragment = new CustomizeTextFragment();
        String string = PortfolioApp.get.instance().getString(2131887330);
        wg6.a((Object) string, "PortfolioApp.instance.getString(R.string.text)");
        l34.a(customizeTextFragment, string);
        CustomizeButtonFragment customizeButtonFragment = new CustomizeButtonFragment();
        String string2 = PortfolioApp.get.instance().getString(2131887052);
        wg6.a((Object) string2, "PortfolioApp.instance.getString(R.string.button)");
        l34.a(customizeButtonFragment, string2);
        CustomizeBackgroundFragment customizeBackgroundFragment = new CustomizeBackgroundFragment();
        String string3 = PortfolioApp.get.instance().getString(2131887040);
        wg6.a((Object) string3, "PortfolioApp.instance.ge\u2026ring(R.string.background)");
        l34.a(customizeBackgroundFragment, string3);
        CustomizeRingChartFragment customizeRingChartFragment = new CustomizeRingChartFragment();
        String string4 = PortfolioApp.get.instance().getString(2131887297);
        wg6.a((Object) string4, "PortfolioApp.instance.getString(R.string.ring)");
        l34.a(customizeRingChartFragment, string4);
        CustomizeActivityChartFragment customizeActivityChartFragment = new CustomizeActivityChartFragment();
        String string5 = PortfolioApp.get.instance().getString(2131887010);
        wg6.a((Object) string5, "PortfolioApp.instance.getString(R.string.activity)");
        l34.a(customizeActivityChartFragment, string5);
        CustomizeActiveMinutesChartFragment customizeActiveMinutesChartFragment = new CustomizeActiveMinutesChartFragment();
        String string6 = PortfolioApp.get.instance().getString(2131887009);
        wg6.a((Object) string6, "PortfolioApp.instance.ge\u2026(R.string.active_minutes)");
        l34.a(customizeActiveMinutesChartFragment, string6);
        CustomizeActiveCaloriesChartFragment customizeActiveCaloriesChartFragment = new CustomizeActiveCaloriesChartFragment();
        String string7 = PortfolioApp.get.instance().getString(2131887008);
        wg6.a((Object) string7, "PortfolioApp.instance.ge\u2026R.string.active_calories)");
        l34.a(customizeActiveCaloriesChartFragment, string7);
        CustomizeHeartRateChartFragment customizeHeartRateChartFragment = new CustomizeHeartRateChartFragment();
        String string8 = PortfolioApp.get.instance().getString(2131887165);
        wg6.a((Object) string8, "PortfolioApp.instance.ge\u2026ring(R.string.heart_rate)");
        l34.a(customizeHeartRateChartFragment, string8);
        CustomizeSleepChartFragment customizeSleepChartFragment = new CustomizeSleepChartFragment();
        String string9 = PortfolioApp.get.instance().getString(2131887320);
        wg6.a((Object) string9, "PortfolioApp.instance.getString(R.string.sleep)");
        l34.a(customizeSleepChartFragment, string9);
        CustomizeGoalTrackingChartFragment customizeGoalTrackingChartFragment = new CustomizeGoalTrackingChartFragment();
        String string10 = PortfolioApp.get.instance().getString(2131887159);
        wg6.a((Object) string10, "PortfolioApp.instance.ge\u2026g(R.string.goal_tracking)");
        l34.a(customizeGoalTrackingChartFragment, string10);
        CustomizeFontFragment customizeFontFragment = new CustomizeFontFragment();
        String string11 = PortfolioApp.get.instance().getString(2131887147);
        wg6.a((Object) string11, "PortfolioApp.instance.getString(R.string.font)");
        l34.a(customizeFontFragment, string11);
        viewPager.setAdapter(l34);
    }
}
