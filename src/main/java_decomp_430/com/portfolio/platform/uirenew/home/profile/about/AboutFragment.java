package com.portfolio.platform.uirenew.home.profile.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.jk3;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.n54;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.uj5;
import com.fossil.vj5;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.help.HelpFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AboutFragment extends BaseFragment implements vj5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public ax5<n54> f;
    @DexIgnore
    public uj5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final AboutFragment a() {
            return new AboutFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AboutFragment a;

        @DexIgnore
        public b(AboutFragment aboutFragment) {
            this.a = aboutFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AboutFragment a;

        @DexIgnore
        public c(AboutFragment aboutFragment) {
            this.a = aboutFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            nh6 nh6 = nh6.a;
            Locale a2 = jm4.a();
            wg6.a((Object) a2, "LanguageHelper.getLocale()");
            Object[] objArr = {a2.getLanguage()};
            String format = String.format("https://help.wearablessupport.com/hc/%s/articles/210312606", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Privacy Policy URL = " + format);
            this.a.Z(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AboutFragment a;

        @DexIgnore
        public d(AboutFragment aboutFragment) {
            this.a = aboutFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            nh6 nh6 = nh6.a;
            Locale a2 = jm4.a();
            wg6.a((Object) a2, "LanguageHelper.getLocale()");
            Object[] objArr = {a2.getLanguage()};
            String format = String.format("https://help.wearablessupport.com/hc/%s/articles/210312186", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Term Of Use URL = " + format);
            this.a.Z(format);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ AboutFragment a;

        @DexIgnore
        public e(AboutFragment aboutFragment) {
            this.a = aboutFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            nh6 nh6 = nh6.a;
            Locale a2 = jm4.a();
            wg6.a((Object) a2, "LanguageHelper.getLocale()");
            Object[] objArr = {a2.getLanguage()};
            String format = String.format("https://c.fossil.com/web/open-source-licenses?locale=%s", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Open Source Licenses URL = " + format);
            this.a.Z(format);
        }
    }

    @DexIgnore
    public final void Z(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), HelpFragment.j.a());
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void o() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        n54 a2 = kb.a(LayoutInflater.from(getContext()), 2131558493, (ViewGroup) null, false, e1());
        this.f = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        AboutFragment.super.onPause();
        uj5 uj5 = this.g;
        if (uj5 != null) {
            uj5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        AboutFragment.super.onResume();
        uj5 uj5 = this.g;
        if (uj5 != null) {
            uj5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<n54> ax5 = this.f;
        if (ax5 != null) {
            n54 a2 = ax5.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(uj5 uj5) {
        wg6.b(uj5, "presenter");
        jk3.a(uj5);
        wg6.a((Object) uj5, "Preconditions.checkNotNull(presenter)");
        this.g = uj5;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AboutFragment", "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.a(str, i2, intent);
        }
    }
}
