package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.my4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationCallsAndMessagesActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public NotificationCallsAndMessagesPresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            wg6.b(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationCallsAndMessagesActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        NotificationCallsAndMessagesActivity.super.onActivityResult(i, i2, intent);
        Fragment b = getSupportFragmentManager().b(2131362119);
        if (b != null) {
            b.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        NotificationCallsAndMessagesFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = NotificationCallsAndMessagesFragment.u.b();
            a((Fragment) b, NotificationCallsAndMessagesFragment.u.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new my4(b)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesContract.View");
    }
}
