package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.fe5;
import com.fossil.ff6;
import com.fossil.ge5;
import com.fossil.gk6;
import com.fossil.he5$b$a;
import com.fossil.he5$b$b;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewDayPresenter extends fe5 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public LiveData<yx5<GoalTrackingSummary>> h; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<GoalTrackingData>>> i; // = new MutableLiveData();
    @DexIgnore
    public /* final */ ge5 j;
    @DexIgnore
    public /* final */ an4 k;
    @DexIgnore
    public /* final */ GoalTrackingRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {99, 102}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x00a4, code lost:
            r2 = com.fossil.hf6.a((r2 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) r2.d()).getGoalTarget() / 16);
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00b0  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00b5  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00c6  */
        public final Object invokeSuspend(Object obj) {
            lc6 lc6;
            ArrayList arrayList;
            GoalTrackingSummary goalTrackingSummary;
            Integer a;
            il6 il6;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 a3 = this.this$0.b();
                he5$b$b he5_b_b = new he5$b$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a3, he5_b_b, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                arrayList = (ArrayList) this.L$2;
                lc6 = (lc6) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                Integer num = (Integer) obj;
                yx5 yx5 = (yx5) this.this$0.h.a();
                int intValue = (yx5 == null || goalTrackingSummary == null || a == null) ? 8 : a.intValue();
                this.this$0.j.a(new BarChart.c(Math.max(num == null ? num.intValue() : 0, intValue / 16), intValue, arrayList), (ArrayList) lc6.getSecond());
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc62 = (lc6) obj;
            ArrayList arrayList2 = (ArrayList) lc62.getFirst();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "showDetailChart - data=" + arrayList2);
            dl6 a4 = this.this$0.b();
            he5$b$a he5_b_a = new he5$b$a(arrayList2, (xe6) null);
            this.L$0 = il6;
            this.L$1 = lc62;
            this.L$2 = arrayList2;
            this.label = 2;
            Object a5 = gk6.a(a4, he5_b_a, this);
            if (a5 == a2) {
                return a2;
            }
            arrayList = arrayList2;
            Object obj2 = a5;
            lc6 = lc62;
            obj = obj2;
            Integer num2 = (Integer) obj;
            yx5 yx52 = (yx5) this.this$0.h.a();
            if (yx52 == null || goalTrackingSummary == null || a == null) {
            }
            this.this$0.j.a(new BarChart.c(Math.max(num2 == null ? num2.intValue() : 0, intValue / 16), intValue, arrayList), (ArrayList) lc6.getSecond());
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<yx5<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter a;

        @DexIgnore
        public c(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            this.a = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<GoalTrackingSummary> yx5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "start - mGoalTrackingSummary -- summary=" + yx5);
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                this.a.f = true;
                if (this.a.f && this.a.g) {
                    rm6 unused = this.a.j();
                }
                this.a.j.c(true ^ this.a.k.I());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<yx5<? extends List<GoalTrackingData>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewDayPresenter a;

        @DexIgnore
        public d(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            this.a = goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<GoalTrackingData>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingData -- goalTrackingSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewDayPresenter", sb.toString());
            if (a2 != wh4.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.f && this.a.g) {
                    rm6 unused = this.a.j();
                }
                this.a.j.c(true ^ this.a.k.I());
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewDayPresenter(ge5 ge5, an4 an4, GoalTrackingRepository goalTrackingRepository) {
        wg6.b(ge5, "mView");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.j = ge5;
        this.k = an4;
        this.l = goalTrackingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
        Date date = goalTrackingOverviewDayPresenter.e;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public final rm6 j() {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", "start");
        h();
        LiveData<yx5<GoalTrackingSummary>> liveData = this.h;
        ge5 ge5 = this.j;
        if (ge5 != null) {
            liveData.a((GoalTrackingOverviewDayFragment) ge5, new c(this));
            this.i.a(this.j, new d(this));
            this.j.c(!this.k.I());
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", "stop");
        try {
            LiveData<yx5<List<GoalTrackingData>>> liveData = this.i;
            ge5 ge5 = this.j;
            if (ge5 != null) {
                liveData.a((GoalTrackingOverviewDayFragment) ge5);
                this.h.a(this.j);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public void h() {
        Date date = this.e;
        if (date != null) {
            if (date == null) {
                wg6.d("mDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.e;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("GoalTrackingOverviewDayPresenter", sb.toString());
                    return;
                }
                wg6.d("mDate");
                throw null;
            }
        }
        this.f = false;
        this.g = false;
        this.e = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.e;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("GoalTrackingOverviewDayPresenter", sb2.toString());
            GoalTrackingRepository goalTrackingRepository = this.l;
            Date date4 = this.e;
            if (date4 != null) {
                this.h = goalTrackingRepository.getSummary(date4);
                GoalTrackingRepository goalTrackingRepository2 = this.l;
                Date date5 = this.e;
                if (date5 == null) {
                    wg6.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.i = goalTrackingRepository2.getGoalTrackingDataList(date5, date5, true);
                } else {
                    wg6.d("mDate");
                    throw null;
                }
            } else {
                wg6.d("mDate");
                throw null;
            }
        } else {
            wg6.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.j.a(this);
    }
}
