package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.HomePresenter;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.yw4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public HomePresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, Integer num, int i, Object obj) {
            if ((i & 2) != 0) {
                num = 0;
            }
            aVar.a(context, num);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, Integer num) {
            wg6.b(context, "context");
            Intent intent = new Intent(context, HomeActivity.class);
            intent.addFlags(268468224);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", num);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        HomeActivity.super.onActivityResult(i, i2, intent);
        HomePresenter homePresenter = this.B;
        if (homePresenter != null) {
            homePresenter.a(i, i2, intent);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.home.HomeActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        HomeFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = HomeFragment.y.a();
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new yw4(b)).a(this);
        if (bundle != null) {
            HomePresenter homePresenter = this.B;
            if (homePresenter != null) {
                homePresenter.a(bundle.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            Intent intent = getIntent();
            if (intent != null && intent.getExtras() != null) {
                Intent intent2 = getIntent();
                if (intent2 != null) {
                    Bundle extras = intent2.getExtras();
                    if (extras == null) {
                        wg6.a();
                        throw null;
                    } else if (extras.containsKey("OUT_STATE_DASHBOARD_CURRENT_TAB")) {
                        HomePresenter homePresenter2 = this.B;
                        if (homePresenter2 != null) {
                            Intent intent3 = getIntent();
                            if (intent3 != null) {
                                Bundle extras2 = intent3.getExtras();
                                if (extras2 != null) {
                                    homePresenter2.a(extras2.getInt("OUT_STATE_DASHBOARD_CURRENT_TAB", 0));
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.d("mPresenter");
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        HomePresenter homePresenter = this.B;
        if (homePresenter != null) {
            bundle.putInt("OUT_STATE_DASHBOARD_CURRENT_TAB", homePresenter.h());
            HomeActivity.super.onSaveInstanceState(bundle);
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        a((Class<? extends T>[]) new Class[]{MFDeviceService.class, ButtonService.class});
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        b((Class<? extends T>[]) new Class[]{MFDeviceService.class, ButtonService.class});
    }
}
