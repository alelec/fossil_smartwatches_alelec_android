package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.le5;
import com.fossil.mg;
import com.fossil.qg6;
import com.fossil.uz5;
import com.fossil.wg6;
import com.fossil.x9;
import com.fossil.y04;
import com.fossil.z94;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewFragment extends BaseFragment {
    @DexIgnore
    public ax5<z94> f;
    @DexIgnore
    public GoalTrackingOverviewDayPresenter g;
    @DexIgnore
    public GoalTrackingOverviewWeekPresenter h;
    @DexIgnore
    public GoalTrackingOverviewMonthPresenter i;
    @DexIgnore
    public GoalTrackingOverviewDayFragment j;
    @DexIgnore
    public GoalTrackingOverviewWeekFragment o;
    @DexIgnore
    public GoalTrackingOverviewMonthFragment p;
    @DexIgnore
    public int q; // = 7;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment a;

        @DexIgnore
        public b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.a = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.a;
            ax5 a2 = goalTrackingOverviewFragment.f;
            goalTrackingOverviewFragment.a(7, a2 != null ? (z94) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment a;

        @DexIgnore
        public c(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.a = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.a;
            ax5 a2 = goalTrackingOverviewFragment.f;
            goalTrackingOverviewFragment.a(4, a2 != null ? (z94) a2.a() : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewFragment a;

        @DexIgnore
        public d(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            this.a = goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewFragment goalTrackingOverviewFragment = this.a;
            ax5 a2 = goalTrackingOverviewFragment.f;
            goalTrackingOverviewFragment.a(2, a2 != null ? (z94) a2.a() : null);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "GoalTrackingOverviewFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void j1() {
        ax5<z94> ax5;
        z94 a2;
        ax5<z94> ax52;
        z94 a3;
        ax5<z94> ax53;
        z94 a4;
        ax5<z94> ax54;
        z94 a5;
        String b2 = ThemeManager.l.a().b("hybridGoalTrackingTab");
        String b3 = ThemeManager.l.a().b("onHybridGoalTrackingTab");
        if (!(b2 == null || (ax54 = this.f) == null || (a5 = ax54.a()) == null)) {
            a5.x.setBackgroundColor(Color.parseColor(b2));
            a5.y.setBackgroundColor(Color.parseColor(b2));
        }
        if (!(b3 == null || (ax53 = this.f) == null || (a4 = ax53.a()) == null)) {
            a4.t.setTextColor(Color.parseColor(b3));
        }
        this.r = ThemeManager.l.a().b("onHybridInactiveTab");
        String b4 = ThemeManager.l.a().b("nonBrandSurface");
        this.s = ThemeManager.l.a().b("primaryText");
        if (!(b4 == null || (ax52 = this.f) == null || (a3 = ax52.a()) == null)) {
            a3.q.setBackgroundColor(Color.parseColor(b4));
        }
        if (!TextUtils.isEmpty(this.r) && !TextUtils.isEmpty(this.s) && (ax5 = this.f) != null && (a2 = ax5.a()) != null) {
            Object r1 = a2.u;
            wg6.a((Object) r1, "it.ftvToday");
            if (r1.isSelected()) {
                a2.u.setTextColor(Color.parseColor(this.s));
            } else {
                a2.u.setTextColor(Color.parseColor(this.r));
            }
            Object r12 = a2.r;
            wg6.a((Object) r12, "it.ftv7Days");
            if (r12.isSelected()) {
                a2.r.setTextColor(Color.parseColor(this.s));
            } else {
                a2.r.setTextColor(Color.parseColor(this.r));
            }
            Object r13 = a2.s;
            wg6.a((Object) r13, "it.ftvMonth");
            if (r13.isSelected()) {
                a2.s.setTextColor(Color.parseColor(this.s));
            } else {
                a2.s.setTextColor(Color.parseColor(this.r));
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        z94 a2;
        wg6.b(layoutInflater, "inflater");
        GoalTrackingOverviewFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "onCreateView");
        z94 a3 = kb.a(layoutInflater, 2131558553, viewGroup, false, e1());
        x9.d(a3.w, false);
        if (bundle != null) {
            this.q = bundle.getInt("CURRENT_TAB", 7);
        }
        wg6.a((Object) a3, "binding");
        a(a3);
        this.f = new ax5<>(this, a3);
        j1();
        ax5<z94> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        GoalTrackingOverviewFragment.super.onSaveInstanceState(bundle);
        bundle.putInt("CURRENT_TAB", this.q);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v24, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r10v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(z94 z94) {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewFragment", "initUI");
        this.j = getChildFragmentManager().b("GoalTrackingOverviewDayFragment");
        this.o = getChildFragmentManager().b("GoalTrackingOverviewWeekFragment");
        this.p = getChildFragmentManager().b("GoalTrackingOverviewMonthFragment");
        if (this.j == null) {
            this.j = new GoalTrackingOverviewDayFragment();
        }
        if (this.o == null) {
            this.o = new GoalTrackingOverviewWeekFragment();
        }
        if (this.p == null) {
            this.p = new GoalTrackingOverviewMonthFragment();
        }
        ArrayList arrayList = new ArrayList();
        GoalTrackingOverviewDayFragment goalTrackingOverviewDayFragment = this.j;
        if (goalTrackingOverviewDayFragment != null) {
            arrayList.add(goalTrackingOverviewDayFragment);
            GoalTrackingOverviewWeekFragment goalTrackingOverviewWeekFragment = this.o;
            if (goalTrackingOverviewWeekFragment != null) {
                arrayList.add(goalTrackingOverviewWeekFragment);
                GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment = this.p;
                if (goalTrackingOverviewMonthFragment != null) {
                    arrayList.add(goalTrackingOverviewMonthFragment);
                    RecyclerView recyclerView = z94.w;
                    wg6.a((Object) recyclerView, "it");
                    recyclerView.setAdapter(new uz5(getChildFragmentManager(), arrayList));
                    recyclerView.setItemViewCacheSize(3);
                    recyclerView.setLayoutManager(new GoalTrackingOverviewFragment$initUI$$inlined$let$lambda$Anon1(getContext(), 0, false, this, arrayList));
                    new mg().a(recyclerView);
                    a(this.q, z94);
                    y04 g2 = PortfolioApp.get.instance().g();
                    GoalTrackingOverviewDayFragment goalTrackingOverviewDayFragment2 = this.j;
                    if (goalTrackingOverviewDayFragment2 != null) {
                        GoalTrackingOverviewWeekFragment goalTrackingOverviewWeekFragment2 = this.o;
                        if (goalTrackingOverviewWeekFragment2 != null) {
                            GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment2 = this.p;
                            if (goalTrackingOverviewMonthFragment2 != null) {
                                g2.a(new le5(goalTrackingOverviewDayFragment2, goalTrackingOverviewWeekFragment2, goalTrackingOverviewMonthFragment2)).a(this);
                                z94.u.setOnClickListener(new b(this));
                                z94.r.setOnClickListener(new c(this));
                                z94.s.setOnClickListener(new d(this));
                                return;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r7v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r7v29, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r8v27, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void a(int i2, z94 z94) {
        ax5<z94> ax5;
        z94 a2;
        z94 a3;
        RecyclerView recyclerView;
        z94 a4;
        RecyclerView recyclerView2;
        z94 a5;
        RecyclerView recyclerView3;
        z94 a6;
        RecyclerView recyclerView4;
        if (z94 != null) {
            Object r0 = z94.u;
            wg6.a((Object) r0, "it.ftvToday");
            r0.setSelected(false);
            Object r02 = z94.r;
            wg6.a((Object) r02, "it.ftv7Days");
            r02.setSelected(false);
            Object r03 = z94.s;
            wg6.a((Object) r03, "it.ftvMonth");
            r03.setSelected(false);
            Object r04 = z94.u;
            wg6.a((Object) r04, "it.ftvToday");
            r04.setPaintFlags(0);
            Object r05 = z94.r;
            wg6.a((Object) r05, "it.ftv7Days");
            r05.setPaintFlags(0);
            Object r06 = z94.s;
            wg6.a((Object) r06, "it.ftvMonth");
            r06.setPaintFlags(0);
            if (i2 == 2) {
                Object r7 = z94.s;
                wg6.a((Object) r7, "it.ftvMonth");
                r7.setSelected(true);
                Object r72 = z94.s;
                wg6.a((Object) r72, "it.ftvMonth");
                Object r8 = z94.r;
                wg6.a((Object) r8, "it.ftv7Days");
                r72.setPaintFlags(r8.getPaintFlags() | 8 | 1);
                ax5<z94> ax52 = this.f;
                if (!(ax52 == null || (a3 = ax52.a()) == null || (recyclerView = a3.w) == null)) {
                    recyclerView.scrollToPosition(2);
                }
            } else if (i2 == 4) {
                Object r73 = z94.r;
                wg6.a((Object) r73, "it.ftv7Days");
                r73.setSelected(true);
                Object r74 = z94.r;
                wg6.a((Object) r74, "it.ftv7Days");
                Object r82 = z94.r;
                wg6.a((Object) r82, "it.ftv7Days");
                r74.setPaintFlags(r82.getPaintFlags() | 8 | 1);
                ax5<z94> ax53 = this.f;
                if (!(ax53 == null || (a4 = ax53.a()) == null || (recyclerView2 = a4.w) == null)) {
                    recyclerView2.scrollToPosition(1);
                }
            } else if (i2 != 7) {
                Object r75 = z94.u;
                wg6.a((Object) r75, "it.ftvToday");
                r75.setSelected(true);
                Object r76 = z94.u;
                wg6.a((Object) r76, "it.ftvToday");
                Object r83 = z94.r;
                wg6.a((Object) r83, "it.ftv7Days");
                r76.setPaintFlags(r83.getPaintFlags() | 8 | 1);
                ax5<z94> ax54 = this.f;
                if (!(ax54 == null || (a6 = ax54.a()) == null || (recyclerView4 = a6.w) == null)) {
                    recyclerView4.scrollToPosition(0);
                }
            } else {
                Object r77 = z94.u;
                wg6.a((Object) r77, "it.ftvToday");
                r77.setSelected(true);
                Object r78 = z94.u;
                wg6.a((Object) r78, "it.ftvToday");
                Object r84 = z94.r;
                wg6.a((Object) r84, "it.ftv7Days");
                r78.setPaintFlags(r84.getPaintFlags() | 8 | 1);
                ax5<z94> ax55 = this.f;
                if (!(ax55 == null || (a5 = ax55.a()) == null || (recyclerView3 = a5.w) == null)) {
                    recyclerView3.scrollToPosition(0);
                }
            }
            if (!TextUtils.isEmpty(this.r) && !TextUtils.isEmpty(this.s) && (ax5 = this.f) != null && (a2 = ax5.a()) != null) {
                Object r85 = a2.u;
                wg6.a((Object) r85, "it.ftvToday");
                if (r85.isSelected()) {
                    a2.u.setTextColor(Color.parseColor(this.s));
                } else {
                    a2.u.setTextColor(Color.parseColor(this.r));
                }
                Object r86 = a2.r;
                wg6.a((Object) r86, "it.ftv7Days");
                if (r86.isSelected()) {
                    a2.r.setTextColor(Color.parseColor(this.s));
                } else {
                    a2.r.setTextColor(Color.parseColor(this.r));
                }
                Object r87 = a2.s;
                wg6.a((Object) r87, "it.ftvMonth");
                if (r87.isSelected()) {
                    a2.s.setTextColor(Color.parseColor(this.s));
                } else {
                    a2.s.setTextColor(Color.parseColor(this.r));
                }
            }
        }
    }
}
