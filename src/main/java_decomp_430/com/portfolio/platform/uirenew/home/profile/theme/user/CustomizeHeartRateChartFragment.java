package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.cd6;
import com.fossil.f84;
import com.fossil.jz5;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.ly5;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.vn5;
import com.fossil.w04;
import com.fossil.wg6;
import com.fossil.wn5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.constant.WBConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeHeartRateChartFragment extends BaseFragment implements ly5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String o;
    @DexIgnore
    public static String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public CustomizeHeartRateChartViewModel g;
    @DexIgnore
    public ax5<f84> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CustomizeHeartRateChartFragment.o;
        }

        @DexIgnore
        public final String b() {
            return CustomizeHeartRateChartFragment.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<wn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeHeartRateChartFragment a;

        @DexIgnore
        public b(CustomizeHeartRateChartFragment customizeHeartRateChartFragment) {
            this.a = customizeHeartRateChartFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CustomizeHeartRateChartViewModel.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.r(a2.intValue());
                }
                Integer c = bVar.c();
                if (c != null) {
                    this.a.t(c.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.s(b.intValue());
                }
                Integer d = bVar.d();
                if (d != null) {
                    this.a.u(d.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeHeartRateChartFragment a;

        @DexIgnore
        public c(CustomizeHeartRateChartFragment customizeHeartRateChartFragment) {
            this.a = customizeHeartRateChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 601);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeHeartRateChartFragment a;

        @DexIgnore
        public d(CustomizeHeartRateChartFragment customizeHeartRateChartFragment) {
            this.a = customizeHeartRateChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, (int) Action.Bolt.TURN_OFF);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeHeartRateChartFragment a;

        @DexIgnore
        public e(CustomizeHeartRateChartFragment customizeHeartRateChartFragment) {
            this.a = customizeHeartRateChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.g(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = CustomizeHeartRateChartFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeHeartRateChartF\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363190) {
            CustomizeHeartRateChartViewModel customizeHeartRateChartViewModel = this.g;
            if (customizeHeartRateChartViewModel != null) {
                customizeHeartRateChartViewModel.a(UserCustomizeThemeFragment.p.a(), o, p);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf(i3 & 16777215)};
        String format = String.format("#%06X", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        CustomizeHeartRateChartViewModel customizeHeartRateChartViewModel = this.g;
        if (customizeHeartRateChartViewModel != null) {
            customizeHeartRateChartViewModel.a(i2, Color.parseColor(format));
            if (i2 == 601) {
                o = format;
            } else if (i2 == 602) {
                p = format;
            }
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void j1() {
        TodayHeartRateChart todayHeartRateChart;
        TodayHeartRateChart todayHeartRateChart2;
        TodayHeartRateChart todayHeartRateChart3;
        ax5<f84> ax5 = this.h;
        if (ax5 != null) {
            f84 a2 = ax5.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new jz5(84, 44, 198, 45, 50, 47));
            arrayList.add(new jz5(93, 86, 108, 50, 55, 52));
            arrayList.add(new jz5(90, 64, 100, 55, 60, 57));
            arrayList.add(new jz5(70, 70, 197, 60, 65, 62));
            arrayList.add(new jz5(92, 92, 100, 65, 70, 67));
            arrayList.add(new jz5(86, 80, 120, 70, 75, 72));
            arrayList.add(new jz5(87, 80, 90, 75, 80, 77));
            arrayList.add(new jz5(78, 60, 140, 80, 85, 82));
            arrayList.add(new jz5(90, 60, 180, 85, 90, 87));
            arrayList.add(new jz5(70, 70, 187, 90, 95, 92));
            arrayList.add(new jz5(70, 70, 187, 95, 100, 97));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 100, 105, 102));
            arrayList.add(new jz5(70, 70, 187, 105, 110, 107));
            arrayList.add(new jz5(65, 60, 70, 110, 115, 112));
            arrayList.add(new jz5(140, 14, DateTimeConstants.HOURS_PER_WEEK, 115, 120, 117));
            arrayList.add(new jz5(90, 80, 100, 120, 125, 122));
            arrayList.add(new jz5(92, 90, 184, 125, 130, 127));
            arrayList.add(new jz5(110, 80, 124, 130, 135, 132));
            arrayList.add(new jz5(65, 60, 70, 135, 140, 137));
            arrayList.add(new jz5(140, 14, DateTimeConstants.HOURS_PER_WEEK, 140, 145, 142));
            arrayList.add(new jz5(65, 60, 70, 145, 150, 147));
            arrayList.add(new jz5(140, 14, DateTimeConstants.HOURS_PER_WEEK, 150, 155, 152));
            arrayList.add(new jz5(108, 90, Action.Music.MUSIC_END_ACTION, 155, 160, 157));
            arrayList.add(new jz5(60, 50, 70, 160, 165, 162));
            arrayList.add(new jz5(60, 60, 80, 165, 170, 167));
            arrayList.add(new jz5(96, 90, 169, 170, 175, 172));
            arrayList.add(new jz5(182, 162, 190, 175, 180, 177));
            arrayList.add(new jz5(96, 90, 169, 180, 185, 182));
            arrayList.add(new jz5(65, 60, 70, 185, 190, 187));
            arrayList.add(new jz5(96, 90, 169, 190, 195, 192));
            arrayList.add(new jz5(87, 80, 187, 195, MFNetworkReturnCode.RESPONSE_OK, 197));
            arrayList.add(new jz5(78, 78, 186, MFNetworkReturnCode.RESPONSE_OK, 205, Action.Selfie.TAKE_BURST));
            arrayList.add(new jz5(84, 44, 98, 205, 210, 207));
            arrayList.add(new jz5(93, 86, 100, 210, 215, 212));
            arrayList.add(new jz5(80, 70, 92, 215, 220, 217));
            arrayList.add(new jz5(72, 72, 180, 220, 225, 222));
            arrayList.add(new jz5(84, 44, 98, 225, 230, 227));
            arrayList.add(new jz5(76, 70, 80, 230, 235, 232));
            arrayList.add(new jz5(75, 80, 90, 235, 240, 237));
            arrayList.add(new jz5(68, 60, 102, 240, 245, 242));
            arrayList.add(new jz5(80, 50, 100, 245, 250, 247));
            arrayList.add(new jz5(84, 44, 98, 250, 255, 252));
            arrayList.add(new jz5(80, 66, 142, 255, 260, 257));
            arrayList.add(new jz5(66, 60, 156, 260, 265, 262));
            arrayList.add(new jz5(78, 70, 87, 265, 270, 267));
            arrayList.add(new jz5(65, 60, 70, 270, 275, 272));
            arrayList.add(new jz5(65, 60, 70, 275, 280, 277));
            arrayList.add(new jz5(90, 90, 184, 280, 285, 282));
            arrayList.add(new jz5(92, 80, 184, 285, 290, 287));
            arrayList.add(new jz5(94, 60, 182, 290, 295, 292));
            arrayList.add(new jz5(94, 66, 96, 295, MFNetworkReturnCode.RESPONSE_OK, 297));
            arrayList.add(new jz5(94, 64, 98, 300, 305, Action.Presenter.PREVIOUS));
            arrayList.add(new jz5(84, 44, 98, 305, 310, 307));
            arrayList.add(new jz5(65, 60, 70, 310, 315, 312));
            arrayList.add(new jz5(80, 70, 100, 315, 320, 317));
            arrayList.add(new jz5(92, 80, 184, 320, 325, 322));
            arrayList.add(new jz5(84, 70, 87, 325, 330, 327));
            arrayList.add(new jz5(80, 80, 155, 330, 335, 332));
            arrayList.add(new jz5(78, 78, 186, 335, 340, 337));
            arrayList.add(new jz5(52, 50, 60, 340, 345, 342));
            arrayList.add(new jz5(69, 60, 80, 345, 350, 347));
            arrayList.add(new jz5(88, 80, 90, 350, 355, 352));
            arrayList.add(new jz5(88, 70, 187, 355, 360, 357));
            arrayList.add(new jz5(92, 80, 92, 360, 365, 362));
            arrayList.add(new jz5(66, 60, 76, 365, 370, 367));
            arrayList.add(new jz5(93, 86, 108, 370, 375, 372));
            arrayList.add(new jz5(87, 80, 90, 375, 380, 377));
            arrayList.add(new jz5(80, 78, 82, 380, 385, 382));
            arrayList.add(new jz5(66, 60, 70, 385, 390, 387));
            arrayList.add(new jz5(70, 70, 70, 390, 395, 392));
            arrayList.add(new jz5(70, 70, 70, 395, MFNetworkReturnCode.BAD_REQUEST, 397));
            arrayList.add(new jz5(76, 70, 80, MFNetworkReturnCode.BAD_REQUEST, 405, Action.ActivityTracker.TAG_ACTIVITY));
            arrayList.add(new jz5(66, 60, 70, 405, 410, 407));
            arrayList.add(new jz5(90, 60, 90, 410, MFNetworkReturnCode.CONTENT_TYPE_ERROR, 412));
            arrayList.add(new jz5(92, 80, 92, MFNetworkReturnCode.CONTENT_TYPE_ERROR, 420, 417));
            arrayList.add(new jz5(76, 70, 80, 420, 425, 422));
            arrayList.add(new jz5(92, 90, 95, 425, 430, 427));
            arrayList.add(new jz5(92, 90, 92, 430, 435, 432));
            arrayList.add(new jz5(65, 60, 70, 435, 440, 437));
            arrayList.add(new jz5(92, 90, 92, 440, 445, 442));
            arrayList.add(new jz5(92, 90, 100, 445, 450, 447));
            arrayList.add(new jz5(88, 80, 90, 450, 455, 452));
            arrayList.add(new jz5(94, 64, 98, 455, 460, 457));
            arrayList.add(new jz5(60, 50, 70, 460, 465, 462));
            arrayList.add(new jz5(60, 60, 80, 465, 470, 467));
            arrayList.add(new jz5(92, 80, 92, 470, 475, 472));
            arrayList.add(new jz5(84, 64, 90, 475, 480, 477));
            arrayList.add(new jz5(78, 78, 80, 480, 485, 482));
            arrayList.add(new jz5(90, 60, 90, 485, 490, 487));
            arrayList.add(new jz5(96, 90, 96, 490, 495, 492));
            arrayList.add(new jz5(92, 80, 92, 495, MFNetworkReturnCode.BAD_REQUEST, 497));
            arrayList.add(new jz5(78, 78, 80, 500, Action.Apps.RING_MY_PHONE, Action.Apps.IF));
            arrayList.add(new jz5(84, 44, 86, Action.Apps.RING_MY_PHONE, 510, 507));
            arrayList.add(new jz5(93, 86, 93, 510, 515, 512));
            arrayList.add(new jz5(80, 70, 92, 515, 520, 517));
            arrayList.add(new jz5(94, 64, 98, 520, 525, 522));
            arrayList.add(new jz5(84, 64, 90, 525, 530, 527));
            arrayList.add(new jz5(76, 70, 80, 530, 535, 532));
            arrayList.add(new jz5(75, 80, 90, 535, 540, 537));
            arrayList.add(new jz5(94, 64, 98, 540, 545, 542));
            arrayList.add(new jz5(80, 50, 80, 545, 550, 547));
            arrayList.add(new jz5(84, 64, 98, 550, 555, 552));
            arrayList.add(new jz5(80, 66, 80, 555, 560, 557));
            arrayList.add(new jz5(94, 64, 98, 560, 565, 562));
            arrayList.add(new jz5(94, 64, 98, 565, 570, 567));
            arrayList.add(new jz5(84, 64, 98, 570, 575, 572));
            arrayList.add(new jz5(84, 64, 90, 575, 580, 577));
            arrayList.add(new jz5(90, 90, 92, 580, 585, 582));
            arrayList.add(new jz5(52, 50, 60, 585, 590, 587));
            arrayList.add(new jz5(94, 64, 98, 590, 595, 592));
            arrayList.add(new jz5(94, 66, 96, 595, 600, 597));
            arrayList.add(new jz5(78, 78, 80, 600, 605, Action.Bolt.TURN_OFF));
            arrayList.add(new jz5(92, 90, 92, 605, 610, 607));
            arrayList.add(new jz5(52, 50, 60, 610, 615, 612));
            arrayList.add(new jz5(80, 70, 90, 615, 620, 617));
            arrayList.add(new jz5(84, 44, 90, 620, 625, 622));
            arrayList.add(new jz5(84, 70, 87, 625, 630, 627));
            arrayList.add(new jz5(80, 80, 82, 630, 635, 632));
            arrayList.add(new jz5(78, 78, 80, 635, 640, 637));
            arrayList.add(new jz5(52, 50, 60, 640, 645, 642));
            arrayList.add(new jz5(69, 60, 80, 645, 650, 647));
            arrayList.add(new jz5(88, 80, 90, 650, 655, 652));
            arrayList.add(new jz5(88, 70, 90, 655, 660, 657));
            arrayList.add(new jz5(70, 60, 70, 660, 665, 662));
            arrayList.add(new jz5(78, 70, 87, 665, 670, 667));
            arrayList.add(new jz5(84, 64, 98, 670, 675, 672));
            arrayList.add(new jz5(90, 60, 100, 675, 680, 677));
            arrayList.add(new jz5(90, 90, 92, 680, 685, 682));
            arrayList.add(new jz5(92, 80, 92, 685, 690, 687));
            arrayList.add(new jz5(94, 60, 94, 690, 695, 692));
            arrayList.add(new jz5(94, 64, 98, 695, 700, 697));
            arrayList.add(new jz5(78, 78, 80, 700, 705, 702));
            arrayList.add(new jz5(82, 80, 92, 705, 710, 707));
            arrayList.add(new jz5(92, 80, 92, 710, 715, 712));
            arrayList.add(new jz5(80, 70, 90, 715, 720, 717));
            arrayList.add(new jz5(94, 64, 98, 720, 725, 722));
            arrayList.add(new jz5(84, 70, 87, 725, 730, 727));
            arrayList.add(new jz5(129, 110, 182, 730, 735, 732));
            arrayList.add(new jz5(80, 78, 80, 735, 740, 737));
            arrayList.add(new jz5(52, 50, 60, 740, 745, 742));
            arrayList.add(new jz5(52, 50, 60, 745, 750, 747));
            arrayList.add(new jz5(88, 80, 90, 750, 755, 752));
            arrayList.add(new jz5(88, 70, 90, 755, 760, 757));
            arrayList.add(new jz5(70, 60, 70, 760, WBConstants.SDK_ACTIVITY_FOR_RESULT_CODE, 762));
            arrayList.add(new jz5(78, 70, 87, WBConstants.SDK_ACTIVITY_FOR_RESULT_CODE, 770, 767));
            arrayList.add(new jz5(129, 110, 182, 770, 775, 772));
            arrayList.add(new jz5(90, 60, 100, 775, 780, 777));
            arrayList.add(new jz5(90, 90, 92, 780, 785, 782));
            arrayList.add(new jz5(92, 80, 92, 785, 790, 787));
            arrayList.add(new jz5(129, 110, 182, 790, 795, 792));
            arrayList.add(new jz5(84, 66, 96, 795, 800, 797));
            arrayList.add(new jz5(94, 64, 98, 800, 805, 802));
            arrayList.add(new jz5(82, 80, 92, 805, 810, 807));
            arrayList.add(new jz5(52, 50, 60, 810, 815, 812));
            arrayList.add(new jz5(80, 70, 90, 815, 820, 817));
            arrayList.add(new jz5(84, 44, 90, 820, 825, 822));
            arrayList.add(new jz5(70, 70, 197, 825, 830, 827));
            arrayList.add(new jz5(80, 80, 82, 830, 835, 832));
            arrayList.add(new jz5(80, 78, 80, 835, Constants.MAXIMUM_SECOND_TIMEZONE_OFFSET, 837));
            arrayList.add(new jz5(94, 64, 98, Constants.MAXIMUM_SECOND_TIMEZONE_OFFSET, 845, 842));
            arrayList.add(new jz5(84, 44, 198, 845, 850, 847));
            arrayList.add(new jz5(93, 86, 108, 850, 855, 852));
            arrayList.add(new jz5(80, 44, 180, 855, 860, 857));
            arrayList.add(new jz5(70, 70, 197, 860, 865, 862));
            arrayList.add(new jz5(72, 72, 180, 865, 870, 867));
            arrayList.add(new jz5(76, 70, 160, 870, 875, 872));
            arrayList.add(new jz5(94, 64, 98, 875, 880, 877));
            arrayList.add(new jz5(78, 60, 140, 880, 885, 882));
            arrayList.add(new jz5(70, 70, 197, 885, 890, 887));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 890, 895, 892));
            arrayList.add(new jz5(94, 64, 98, 895, 900, 897));
            arrayList.add(new jz5(129, 110, 182, 900, 905, 902));
            arrayList.add(new jz5(72, 72, 180, 905, 910, 907));
            arrayList.add(new jz5(72, 72, 180, 910, 915, 912));
            arrayList.add(new jz5(129, 110, 182, 915, 920, 917));
            arrayList.add(new jz5(132, 130, 184, 920, 925, 922));
            arrayList.add(new jz5(72, 72, 180, 925, 930, 927));
            arrayList.add(new jz5(129, 110, 182, 930, 935, 932));
            arrayList.add(new jz5(72, 72, 180, 935, 940, 937));
            arrayList.add(new jz5(94, 64, 98, 940, 945, 942));
            arrayList.add(new jz5(100, 67, 166, 945, 950, 947));
            arrayList.add(new jz5(105, 100, DateTimeConstants.HOURS_PER_WEEK, 950, 955, 952));
            arrayList.add(new jz5(94, 64, 98, 955, 960, 957));
            arrayList.add(new jz5(72, 72, 180, 960, 965, 962));
            arrayList.add(new jz5(84, 84, 157, 965, 970, 967));
            arrayList.add(new jz5(80, 78, 155, 970, 975, 972));
            arrayList.add(new jz5(82, 62, 156, 975, 980, 977));
            arrayList.add(new jz5(129, 110, 182, 980, 985, 982));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 985, 990, 987));
            arrayList.add(new jz5(96, 90, 169, 990, 995, 992));
            arrayList.add(new jz5(87, 80, 187, 995, 1000, 997));
            arrayList.add(new jz5(129, 110, 182, 1000, 1005, 1002));
            arrayList.add(new jz5(80, 50, 100, 1005, 1010, 1007));
            arrayList.add(new jz5(193, 186, 108, 1010, 1015, 1012));
            arrayList.add(new jz5(80, 50, 100, 1015, 1020, 1017));
            arrayList.add(new jz5(170, 170, 197, 1020, 1025, 1022));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1025, 1030, 1027));
            arrayList.add(new jz5(176, 70, 180, 1030, 1035, 1032));
            arrayList.add(new jz5(175, 80, 175, 1035, 1040, 1037));
            arrayList.add(new jz5(68, 60, 102, 1040, 1045, 1042));
            arrayList.add(new jz5(80, 50, 100, 1045, 1050, 1047));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1050, 1055, 1052));
            arrayList.add(new jz5(80, 50, 100, 1055, 1060, 1057));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1060, 1065, 1062));
            arrayList.add(new jz5(78, 70, 87, 1065, 1070, 1067));
            arrayList.add(new jz5(88, 88, 90, 1070, 1075, 1072));
            arrayList.add(new jz5(88, 80, DateTimeConstants.HOURS_PER_WEEK, 1075, 1080, 1077));
            arrayList.add(new jz5(90, 90, 184, 1080, 1085, 1082));
            arrayList.add(new jz5(92, 80, 184, 1085, 1090, 1087));
            arrayList.add(new jz5(94, 60, 182, 1090, 1095, 1092));
            arrayList.add(new jz5(94, 66, 96, 1095, 1100, 1097));
            arrayList.add(new jz5(69, 60, DateTimeConstants.HOURS_PER_WEEK, 1100, 1105, 1102));
            arrayList.add(new jz5(124, 105, 124, 1105, 1110, 1107));
            arrayList.add(new jz5(115, 100, 120, 1110, 1115, FailureCode.LOCATION_SERVICE_DISABLED));
            arrayList.add(new jz5(180, 80, Action.Music.MUSIC_END_ACTION, 1115, 1120, 1117));
            arrayList.add(new jz5(120, 90, 123, 1120, 1125, FailureCode.DEVICE_NOT_FOUND));
            arrayList.add(new jz5(184, 170, 187, 1125, 1130, 1127));
            arrayList.add(new jz5(80, 80, 155, 1130, 1135, 1132));
            arrayList.add(new jz5(182, 90, 189, 1135, 1140, 1137));
            arrayList.add(new jz5(152, 89, 166, 1140, 1145, 1142));
            arrayList.add(new jz5(169, 90, 180, 1145, 1150, 1147));
            arrayList.add(new jz5(88, 80, 90, 1150, 1155, 1152));
            arrayList.add(new jz5(88, 70, 187, 1155, 1160, 1157));
            arrayList.add(new jz5(70, 60, 186, 1160, 1165, 1162));
            if (!(a2 == null || (todayHeartRateChart3 = a2.q) == null)) {
                todayHeartRateChart3.setDayInMinuteWithTimeZone(DateTimeConstants.MINUTES_PER_DAY);
            }
            ArrayList arrayList2 = new ArrayList();
            if (!(a2 == null || (todayHeartRateChart2 = a2.q) == null)) {
                todayHeartRateChart2.setListTimeZoneChange(arrayList2);
            }
            if (a2 != null && (todayHeartRateChart = a2.q) != null) {
                todayHeartRateChart.a(arrayList);
                cd6 cd6 = cd6.a;
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        f84 a2 = kb.a(LayoutInflater.from(getContext()), 2131558530, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new vn5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            CustomizeHeartRateChartViewModel a3 = vd.a(this, w04).a(CustomizeHeartRateChartViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            this.g = a3;
            CustomizeHeartRateChartViewModel customizeHeartRateChartViewModel = this.g;
            if (customizeHeartRateChartViewModel != null) {
                customizeHeartRateChartViewModel.b().a(getViewLifecycleOwner(), new b(this));
                CustomizeHeartRateChartViewModel customizeHeartRateChartViewModel2 = this.g;
                if (customizeHeartRateChartViewModel2 != null) {
                    customizeHeartRateChartViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    j1();
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        CustomizeHeartRateChartFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        o = null;
        p = null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CustomizeHeartRateChartFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        CustomizeHeartRateChartViewModel customizeHeartRateChartViewModel = this.g;
        if (customizeHeartRateChartViewModel != null) {
            customizeHeartRateChartViewModel.c();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<f84> ax5 = this.h;
        if (ax5 != null) {
            f84 a2 = ax5.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void r(int i2) {
        ax5<f84> ax5 = this.h;
        if (ax5 != null) {
            f84 a2 = ax5.a();
            if (a2 != null) {
                a2.q.a("maxHeartRate", i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        ax5<f84> ax5 = this.h;
        if (ax5 != null) {
            f84 a2 = ax5.a();
            if (a2 != null) {
                a2.q.a("lowestHeartRate", i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void t(int i2) {
        ax5<f84> ax5 = this.h;
        if (ax5 != null) {
            f84 a2 = ax5.a();
            if (a2 != null) {
                a2.u.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void u(int i2) {
        ax5<f84> ax5 = this.h;
        if (ax5 != null) {
            f84 a2 = ax5.a();
            if (a2 != null) {
                a2.v.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
