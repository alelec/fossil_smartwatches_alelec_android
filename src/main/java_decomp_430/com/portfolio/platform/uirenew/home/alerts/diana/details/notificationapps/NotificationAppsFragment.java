package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.AppWrapper;
import com.fossil.ax5;
import com.fossil.ay4;
import com.fossil.gv4;
import com.fossil.hz4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tb4;
import com.fossil.vx4;
import com.fossil.wg6;
import com.fossil.y04;
import com.fossil.yf;
import com.fossil.zx4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppsFragment extends BasePermissionFragment implements ay4, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public zx4 g;
    @DexIgnore
    public ax5<tb4> h;
    @DexIgnore
    public NotificationSettingsTypeFragment i;
    @DexIgnore
    public gv4 j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsFragment.p;
        }

        @DexIgnore
        public final NotificationAppsFragment b() {
            return new NotificationAppsFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gv4.b {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public b(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        public void a(AppWrapper appWrapper, boolean z) {
            wg6.b(appWrapper, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsFragment.q.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            NotificationAppsFragment.b(this.a).a(appWrapper, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public c(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(NotificationAppsFragment.q.a(), "press on close button");
            NotificationAppsFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public d(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            NotificationAppsFragment.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ tb4 b;

        @DexIgnore
        public e(NotificationAppsFragment notificationAppsFragment, tb4 tb4) {
            this.a = notificationAppsFragment;
            this.b = tb4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Object r2 = this.b.r;
            wg6.a((Object) r2, "binding.ivClear");
            r2.setVisibility(i3 == 0 ? 4 : 0);
            NotificationAppsFragment.a(this.a).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ tb4 a;

        @DexIgnore
        public f(tb4 tb4) {
            this.a = tb4;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                tb4 tb4 = this.a;
                wg6.a((Object) tb4, "binding");
                tb4.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tb4 a;

        @DexIgnore
        public g(tb4 tb4) {
            this.a = tb4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = NotificationAppsFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationAppsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gv4 a(NotificationAppsFragment notificationAppsFragment) {
        gv4 gv4 = notificationAppsFragment.j;
        if (gv4 != null) {
            return gv4;
        }
        wg6.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ zx4 b(NotificationAppsFragment notificationAppsFragment) {
        zx4 zx4 = notificationAppsFragment.g;
        if (zx4 != null) {
            return zx4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void f(List<vx4> list) {
        wg6.b(list, "listAppWrapper");
        gv4 gv4 = this.j;
        if (gv4 != null) {
            gv4.a(list);
        } else {
            wg6.d("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void g(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<tb4> ax5 = this.h;
        if (ax5 != null) {
            tb4 a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.v) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d(p, "onActivityBackPressed -");
        zx4 zx4 = this.g;
        if (zx4 != null) {
            zx4.h();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void j() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                TroubleshootingActivity.a.a(aVar, context, PortfolioApp.get.instance().e(), false, 4, (Object) null);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        tb4 a2 = kb.a(layoutInflater, 2131558577, viewGroup, false, e1());
        a2.s.setOnClickListener(new c(this));
        a2.v.setOnCheckedChangeListener(new d(this));
        a2.q.addTextChangedListener(new e(this, a2));
        a2.q.setOnFocusChangeListener(new f(a2));
        a2.r.setOnClickListener(new g(a2));
        gv4 gv4 = new gv4();
        gv4.a((gv4.b) new b(this));
        this.j = gv4;
        this.i = getChildFragmentManager().b(NotificationSettingsTypeFragment.u.a());
        if (this.i == null) {
            this.i = NotificationSettingsTypeFragment.u.b();
        }
        RecyclerView recyclerView = a2.u;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        gv4 gv42 = this.j;
        if (gv42 != null) {
            recyclerView.setAdapter(gv42);
            yf itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                itemAnimator.setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                y04 g2 = PortfolioApp.get.instance().g();
                NotificationSettingsTypeFragment notificationSettingsTypeFragment = this.i;
                if (notificationSettingsTypeFragment != null) {
                    g2.a(new hz4(notificationSettingsTypeFragment)).a(this);
                    this.h = new ax5<>(this, a2);
                    W("app_notification_view");
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        wg6.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        zx4 zx4 = this.g;
        if (zx4 != null) {
            zx4.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
            }
            NotificationAppsFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationAppsFragment.super.onResume();
        zx4 zx4 = this.g;
        if (zx4 != null) {
            zx4.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(zx4 zx4) {
        wg6.b(zx4, "presenter");
        this.g = zx4;
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363190) {
                zx4 zx4 = this.g;
                if (zx4 != null) {
                    zx4.i();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i2 == 2131362228) {
                zx4 zx42 = this.g;
                if (zx42 != null) {
                    zx42.h();
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            } else if (i2 != 2131362311) {
                if (i2 == 2131362561) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.C;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    wg6.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                wg6.a();
                throw null;
            }
        }
    }
}
