package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.bz4;
import com.fossil.hv4;
import com.fossil.hz4;
import com.fossil.jm4;
import com.fossil.jy4;
import com.fossil.jz4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.ky4;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.vb4;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wg6;
import com.fossil.y04;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseActivity;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationCallsAndMessagesFragment extends BasePermissionFragment implements ky4, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a((qg6) null);
    @DexIgnore
    public jy4 g;
    @DexIgnore
    public ax5<vb4> h;
    @DexIgnore
    public hv4 i;
    @DexIgnore
    public NotificationSettingsTypeFragment j;
    @DexIgnore
    public jz4 o;
    @DexIgnore
    public bz4 p;
    @DexIgnore
    public w04 q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationCallsAndMessagesFragment.t;
        }

        @DexIgnore
        public final NotificationCallsAndMessagesFragment b() {
            return new NotificationCallsAndMessagesFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements hv4.b {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public b(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public void a() {
            NotificationContactsActivity.C.a(this.a);
        }

        @DexIgnore
        public void a(ContactGroup contactGroup) {
            wg6.b(contactGroup, "contactGroup");
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, contactGroup);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public c(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.B;
            Context context = this.a.getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                aVar.a(context);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public d(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (this.a.g == null) {
                return;
            }
            if (!z) {
                NotificationCallsAndMessagesFragment.b(this.a).a(false);
            } else if (!this.a.j1()) {
                NotificationCallsAndMessagesFragment.b(this.a).k();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public e(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesFragment.u.a(), "press on button back");
            NotificationCallsAndMessagesFragment.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public f(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationCallsAndMessagesFragment.b(this.a).b(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public g(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationCallsAndMessagesFragment.b(this.a).b(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public h(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationContactsActivity.C.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesFragment a;

        @DexIgnore
        public i(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            this.a = notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            QuickResponseActivity.a aVar = QuickResponseActivity.B;
            Context context = this.a.getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                aVar.a(context);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = NotificationCallsAndMessagesFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationCallsAndMess\u2026nt::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ jy4 b(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
        jy4 jy4 = notificationCallsAndMessagesFragment.g;
        if (jy4 != null) {
            return jy4;
        }
        wg6.d("mPresenter");
        throw null;
    }

    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        r0 = r0.r;
     */
    @DexIgnore
    public int G() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        ax5<vb4> ax5 = this.h;
        CharSequence charSequence = null;
        if (ax5 != null) {
            vb4 a2 = ax5.a();
            CharSequence text = (a2 == null || flexibleTextView2 == null) ? null : flexibleTextView2.getText();
            if (wg6.a((Object) text, (Object) jm4.a((Context) PortfolioApp.get.instance(), 2131886089))) {
                return 0;
            }
            ax5<vb4> ax52 = this.h;
            if (ax52 != null) {
                vb4 a3 = ax52.a();
                if (!(a3 == null || (flexibleTextView = a3.r) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return wg6.a((Object) charSequence, (Object) jm4.a((Context) PortfolioApp.get.instance(), 2131886090)) ? 1 : 2;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void L() {
        lx5 lx5 = lx5.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        lx5.Q(childFragmentManager);
    }

    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000f, code lost:
        r0 = r0.s;
     */
    @DexIgnore
    public int M() {
        FlexibleTextView flexibleTextView;
        FlexibleTextView flexibleTextView2;
        ax5<vb4> ax5 = this.h;
        CharSequence charSequence = null;
        if (ax5 != null) {
            vb4 a2 = ax5.a();
            CharSequence text = (a2 == null || flexibleTextView2 == null) ? null : flexibleTextView2.getText();
            if (wg6.a((Object) text, (Object) jm4.a((Context) PortfolioApp.get.instance(), 2131886089))) {
                return 0;
            }
            ax5<vb4> ax52 = this.h;
            if (ax52 != null) {
                vb4 a3 = ax52.a();
                if (!(a3 == null || (flexibleTextView = a3.s) == null)) {
                    charSequence = flexibleTextView.getText();
                }
                return wg6.a((Object) charSequence, (Object) jm4.a((Context) PortfolioApp.get.instance(), 2131886090)) ? 1 : 2;
            }
            wg6.d("mBinding");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void Q() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.clearFlags(16);
        }
    }

    @DexIgnore
    public void V() {
        NotificationSettingsTypeFragment notificationSettingsTypeFragment;
        FLogger.INSTANCE.getLocal().d(t, "showNotificationSettingDialog");
        if (isActive() && (notificationSettingsTypeFragment = this.j) != null) {
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            notificationSettingsTypeFragment.show(childFragmentManager, NotificationSettingsTypeFragment.u.a());
        }
    }

    @DexIgnore
    public List<ContactGroup> W() {
        hv4 hv4 = this.i;
        if (hv4 != null) {
            return hv4.c();
        }
        wg6.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    public void Z() {
        Window window;
        FragmentActivity activity = getActivity();
        if (activity != null && (window = activity.getWindow()) != null) {
            window.setFlags(16, 16);
        }
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void i(String str) {
        Object r0;
        wg6.b(str, "settingsTypeName");
        ax5<vb4> ax5 = this.h;
        if (ax5 != null) {
            vb4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.s) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d(t, "onActivityBackPressed -");
        jy4 jy4 = this.g;
        if (jy4 != null) {
            jy4.l();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final boolean j1() {
        jy4 jy4 = this.g;
        if (jy4 != null) {
            return jy4.h();
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void l(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<vb4> ax5 = this.h;
        if (ax5 != null) {
            vb4 a2 = ax5.a();
            if (!(a2 == null || (flexibleSwitchCompat = a2.z) == null)) {
                flexibleSwitchCompat.setChecked(z);
            }
            jy4 jy4 = this.g;
            if (jy4 != null) {
                jy4.a(z);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void m(List<ContactGroup> list) {
        wg6.b(list, "mListContactGroup");
        hv4 hv4 = this.i;
        if (hv4 != null) {
            hv4.a(list);
        } else {
            wg6.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        NotificationCallsAndMessagesFragment.super.onActivityCreated(bundle);
        y04 g2 = PortfolioApp.get.instance().g();
        NotificationSettingsTypeFragment notificationSettingsTypeFragment = this.j;
        if (notificationSettingsTypeFragment != null) {
            g2.a(new hz4(notificationSettingsTypeFragment)).a(this);
            NotificationCallsAndMessagesActivity activity = getActivity();
            if (activity != null) {
                NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = activity;
                w04 w04 = this.q;
                if (w04 != null) {
                    bz4 a2 = vd.a(notificationCallsAndMessagesActivity, w04).a(bz4.class);
                    wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                    this.p = a2;
                    jy4 jy4 = this.g;
                    if (jy4 != null) {
                        bz4 bz4 = this.p;
                        if (bz4 != null) {
                            jy4.a(bz4);
                        } else {
                            wg6.d("mNotificationSettingViewModel");
                            throw null;
                        }
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                } else {
                    wg6.d("viewModelFactory");
                    throw null;
                }
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        if (i2 == 111 && i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = t;
            local.d(str, "onActivityResult result code is " + i3);
            jy4 jy4 = this.g;
            if (jy4 != null) {
                jy4.j();
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        if (intent != null && i2 == 111 && i3 == 10) {
            this.r = intent.getBooleanExtra("NEED_TO_SHOW_PERMISSION_POPUP", false);
            if (!this.r && (activity = getActivity()) != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v9, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    /* JADX WARNING: type inference failed for: r4v10, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v13, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v22, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v29, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        vb4 a2 = kb.a(layoutInflater, 2131558578, viewGroup, false, e1());
        String b2 = ThemeManager.l.a().b("onPrimaryButton");
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231027);
            if (drawable != null) {
                drawable.setTint(parseColor);
                a2.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        this.j = getChildFragmentManager().b(NotificationSettingsTypeFragment.u.a());
        if (this.j == null) {
            this.j = NotificationSettingsTypeFragment.u.b();
        }
        a2.w.setOnClickListener(new c(this));
        a2.z.setOnCheckedChangeListener(new d(this));
        a2.t.setOnClickListener(new e(this));
        a2.u.setOnClickListener(new f(this));
        a2.v.setOnClickListener(new g(this));
        a2.q.setOnClickListener(new h(this));
        FlexibleSwitchCompat flexibleSwitchCompat = a2.z;
        wg6.a((Object) flexibleSwitchCompat, "binding.swQrEnabled");
        flexibleSwitchCompat.setChecked(j1());
        a2.w.setOnClickListener(new i(this));
        hv4 hv4 = new hv4();
        hv4.a((hv4.b) new b(this));
        this.i = hv4;
        RecyclerView recyclerView = a2.y;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        hv4 hv42 = this.i;
        if (hv42 != null) {
            recyclerView.setAdapter(hv42);
            recyclerView.setHasFixedSize(true);
            y04 g2 = PortfolioApp.get.instance().g();
            NotificationSettingsTypeFragment notificationSettingsTypeFragment = this.j;
            if (notificationSettingsTypeFragment != null) {
                g2.a(new hz4(notificationSettingsTypeFragment)).a(this);
                this.h = new ax5<>(this, a2);
                W("call_message_view");
                wg6.a((Object) a2, "binding");
                return a2.d();
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
        }
        wg6.d("mNotificationFavoriteContactsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        jy4 jy4 = this.g;
        if (jy4 != null) {
            jy4.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
            }
            NotificationCallsAndMessagesFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        FlexibleSwitchCompat flexibleSwitchCompat;
        NotificationCallsAndMessagesFragment.super.onResume();
        jy4 jy4 = this.g;
        if (jy4 != null && this.r) {
            if (jy4 != null) {
                jy4.f();
                ax5<vb4> ax5 = this.h;
                if (ax5 != null) {
                    vb4 a2 = ax5.a();
                    if (!(a2 == null || (flexibleSwitchCompat = a2.z) == null)) {
                        flexibleSwitchCompat.setChecked(j1());
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        kl4 g1 = g1();
        if (g1 != null) {
            g1.d();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void p(String str) {
        Object r0;
        wg6.b(str, "settingsTypeName");
        ax5<vb4> ax5 = this.h;
        if (ax5 != null) {
            vb4 a2 = ax5.a();
            if (a2 != null && (r0 = a2.r) != 0) {
                r0.setText(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
        if (i2 == 102) {
            l(true);
        }
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.l(childFragmentManager);
        }
    }

    @DexIgnore
    public void a(jy4 jy4) {
        wg6.b(jy4, "presenter");
        this.g = jy4;
    }

    @DexIgnore
    public void b(ArrayList<Integer> arrayList) {
        wg6.b(arrayList, "pmsCodes");
        Intent intent = new Intent(getActivity(), PermissionActivity.class);
        intent.putIntegerArrayListExtra("EXTRA_LIST_PERMISSIONS", arrayList);
        startActivity(intent);
    }

    @DexIgnore
    public void a(ContactGroup contactGroup) {
        wg6.b(contactGroup, "contactGroup");
        hv4 hv4 = this.i;
        if (hv4 != null) {
            hv4.a(contactGroup);
        } else {
            wg6.d("mNotificationFavoriteContactsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
        if (i2 == 102) {
            l(false);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        ContactGroup serializable;
        wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -638196028) {
            if (hashCode != -4398250) {
                if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363190) {
                    jy4 jy4 = this.g;
                    if (jy4 != null) {
                        jy4.i();
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            } else if (!str.equals("SET TO WATCH FAIL")) {
            } else {
                if (i2 == 2131363105) {
                    l(false);
                } else if (i2 == 2131363190) {
                    jy4 jy42 = this.g;
                    if (jy42 != null) {
                        jy42.k();
                    } else {
                        wg6.d("mPresenter");
                        throw null;
                    }
                }
            }
        } else if (str.equals("CONFIRM_REMOVE_CONTACT") && i2 == 2131363190) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (serializable = extras.getSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE")) != null) {
                jy4 jy43 = this.g;
                if (jy43 != null) {
                    jy43.a(serializable);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }
}
