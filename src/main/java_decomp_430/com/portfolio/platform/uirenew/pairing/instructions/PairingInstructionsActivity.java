package com.portfolio.platform.uirenew.pairing.instructions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.bs5;
import com.fossil.ds5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingInstructionsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public ds5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, boolean z, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            if ((i & 4) != 0) {
                z2 = false;
            }
            aVar.a(context, z, z2);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, boolean z, boolean z2) {
            wg6.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("", "start isOnboarding=" + z + ", isClearTop=" + z2);
            Intent intent = new Intent(context, PairingInstructionsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            if (!z2) {
                intent.setFlags(536870912);
            } else {
                intent.setFlags(67108864);
            }
            context.startActivity(intent);
        }
    }

    /*
    static {
        wg6.a((Object) PairingInstructionsActivity.class.getSimpleName(), "PairingInstructionsActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        PairingInstructionsFragment b = getSupportFragmentManager().b(2131362119);
        Intent intent = getIntent();
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (b == null) {
            b = PairingInstructionsFragment.o.a(z);
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new bs5(b)).a(this);
    }
}
