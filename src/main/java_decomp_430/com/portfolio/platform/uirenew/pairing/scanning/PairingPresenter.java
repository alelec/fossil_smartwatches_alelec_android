package com.portfolio.platform.uirenew.pairing.scanning;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.as4;
import com.fossil.bs4;
import com.fossil.cd6;
import com.fossil.ce;
import com.fossil.cj4;
import com.fossil.cs4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl4;
import com.fossil.jm4;
import com.fossil.ks5;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.ns5;
import com.fossil.pr4;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.qs5$c$a;
import com.fossil.qs5$d$a;
import com.fossil.qs5$d$b;
import com.fossil.qs5$f$a;
import com.fossil.qs5$f$b;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.rs5;
import com.fossil.sf6;
import com.fossil.uh4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xm4;
import com.fossil.yd6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.PairingFragment;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.sina.weibo.sdk.statistic.StatisticConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingPresenter extends ks5 {
    @DexIgnore
    public static /* final */ a y; // = new a((qg6) null);
    @DexIgnore
    public ShineDevice e;
    @DexIgnore
    public /* final */ List<lc6<ShineDevice, String>> f; // = new ArrayList();
    @DexIgnore
    public /* final */ HashMap<String, List<Integer>> g; // = new HashMap<>();
    @DexIgnore
    public Handler h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ ArrayList<Device> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<SKUModel> l; // = new ArrayList<>();
    @DexIgnore
    public jl4 m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public /* final */ d q; // = new d(this);
    @DexIgnore
    public /* final */ e r; // = new e(this);
    @DexIgnore
    public /* final */ ns5 s;
    @DexIgnore
    public /* final */ LinkDeviceUseCase t;
    @DexIgnore
    public /* final */ DeviceRepository u;
    @DexIgnore
    public /* final */ cj4 v;
    @DexIgnore
    public /* final */ SetNotificationUseCase w;
    @DexIgnore
    public /* final */ an4 x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = PairingPresenter.class.getSimpleName();
            wg6.a((Object) simpleName, "PairingPresenter::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            PairingPresenter.this.c(true);
            if (PairingPresenter.this.s().isEmpty()) {
                PairingPresenter.this.s.S();
                return;
            }
            ns5 g = PairingPresenter.this.s;
            PairingPresenter pairingPresenter = PairingPresenter.this;
            g.l(pairingPresenter.a(pairingPresenter.s()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$addDevice$1", f = "PairingPresenter.kt", l = {421}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ShineDevice $shineDevice;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PairingPresenter pairingPresenter, ShineDevice shineDevice, xe6 xe6) {
            super(2, xe6);
            this.this$0 = pairingPresenter;
            this.$shineDevice = shineDevice;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$shineDevice, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            T t;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                qs5$c$a qs5_c_a = new qs5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, qs5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) obj;
            Iterator<T> it = this.this$0.s().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (hf6.a(wg6.a((Object) ((ShineDevice) ((lc6) t).getFirst()).getSerial(), (Object) this.$shineDevice.getSerial())).booleanValue()) {
                    break;
                }
            }
            lc6 lc6 = (lc6) t;
            if (lc6 == null) {
                this.this$0.s().add(new lc6(this.$shineDevice, str));
            } else {
                ((ShineDevice) lc6.getFirst()).updateRssi(this.$shineDevice.getRssi());
            }
            ns5 g = this.this$0.s;
            PairingPresenter pairingPresenter = this.this$0;
            g.g(pairingPresenter.a(pairingPresenter.s()));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<pr4.j, pr4.i> {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;

        @DexIgnore
        public d(PairingPresenter pairingPresenter) {
            this.a = pairingPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LinkDeviceUseCase.j jVar) {
            wg6.b(jVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "pairDeviceCallback() pairDevice, isSkipOTA=" + this.a.x.V() + ", response=" + jVar.getClass().getSimpleName());
            if (jVar instanceof LinkDeviceUseCase.a) {
                this.a.s.a();
                this.a.s.a(((LinkDeviceUseCase.a) jVar).a(), this.a.q());
            } else if (jVar instanceof LinkDeviceUseCase.f) {
                this.a.s.a();
                this.a.s.b(((LinkDeviceUseCase.f) jVar).a(), this.a.q());
            } else if (jVar instanceof LinkDeviceUseCase.m) {
                LinkDeviceUseCase.m mVar = (LinkDeviceUseCase.m) jVar;
                this.a.s.a(mVar.a(), this.a.q(), mVar.b());
            } else if (jVar instanceof LinkDeviceUseCase.b) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = PairingPresenter.y.a();
                StringBuilder sb = new StringBuilder();
                sb.append("authorizeDeviceResponse, isStartTimer=");
                LinkDeviceUseCase.b bVar = (LinkDeviceUseCase.b) jVar;
                sb.append(bVar.a());
                local2.d(a3, sb.toString());
                if (bVar.a()) {
                    this.a.s.q(true);
                    return;
                }
                this.a.p = false;
                this.a.s.b();
                this.a.s.q(false);
            } else if (jVar instanceof LinkDeviceUseCase.l) {
                String deviceId = ((LinkDeviceUseCase.l) jVar).a().getDeviceId();
                PortfolioApp.get.instance().a(this.a.v, false, 13);
                FLogger.INSTANCE.getRemote().i(FLogger.Component.API, FLogger.Session.PAIR, deviceId, PairingPresenter.y.a(), "Pair Success");
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new qs5$d$b(this, deviceId, (xe6) null), 3, (Object) null);
            }
        }

        @DexIgnore
        public void a(LinkDeviceUseCase.i iVar) {
            wg6.b(iVar, "errorValue");
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new qs5$d$a(this, iVar, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;

        @DexIgnore
        public e(PairingPresenter pairingPresenter) {
            this.a = pairingPresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            T t;
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            ShineDevice shineDevice = (ShineDevice) intent.getParcelableExtra("device");
            if (shineDevice == null) {
                FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "scanReceiver - ShineDevice is NULL!!!");
                return;
            }
            String serial = shineDevice.getSerial();
            if (TextUtils.isEmpty(serial)) {
                FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "scanReceiver - ShineDeviceSerial is NULL => wearOS device=" + shineDevice);
                this.a.e(shineDevice);
                return;
            }
            FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "scanReceiver - receive device serial=" + serial + " allSkuModelSize " + this.a.o().size());
            DeviceHelper e = DeviceHelper.o.e();
            T t2 = null;
            if (serial == null) {
                wg6.a();
                throw null;
            } else if (e.a(serial, this.a.o())) {
                int rssi = shineDevice.getRssi();
                this.a.a(serial, rssi);
                Iterator<T> it = this.a.s().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((ShineDevice) ((lc6) t).getFirst()).getSerial(), (Object) serial)) {
                        break;
                    }
                }
                lc6 lc6 = (lc6) t;
                Iterator<T> it2 = this.a.t().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    T next = it2.next();
                    if (wg6.a((Object) ((Device) next).getDeviceId(), (Object) serial)) {
                        t2 = next;
                        break;
                    }
                }
                Device device = (Device) t2;
                if (device == null && lc6 == null) {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "Add device " + serial + " to list");
                    this.a.d(shineDevice);
                } else if (device != null || lc6 == null || this.a.r()) {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "Device already in list, ignore it " + serial);
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "Pre-scan is not complete, update RSSI for scanned devices");
                    int c = this.a.c(serial);
                    if (c == Integer.MIN_VALUE) {
                        c = rssi;
                    }
                    ((ShineDevice) lc6.getFirst()).updateRssi(c);
                    ns5 g = this.a.s;
                    PairingPresenter pairingPresenter = this.a;
                    g.g(pairingPresenter.a(pairingPresenter.s()));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$start$1", f = "PairingPresenter.kt", l = {97, 99}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(PairingPresenter pairingPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = pairingPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r9v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$d] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00c7  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00fb  */
        public final Object invokeSuspend(Object obj) {
            ArrayList<SKUModel> arrayList;
            il6 il6;
            ArrayList<Device> arrayList2;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                this.this$0.t().clear();
                arrayList2 = this.this$0.t();
                dl6 b = this.this$0.c();
                qs5$f$a qs5_f_a = new qs5$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = arrayList2;
                this.label = 1;
                obj = gk6.a(b, qs5_f_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                arrayList2 = (ArrayList) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                arrayList = (ArrayList) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                arrayList.addAll((Collection) obj);
                ce.a(PortfolioApp.get.instance()).a(this.this$0.r, new IntentFilter("SCAN_DEVICE_FOUND"));
                this.this$0.t.k();
                this.this$0.h = new Handler(Looper.getMainLooper());
                if (this.this$0.e != null) {
                    LinkDeviceUseCase e = this.this$0.t;
                    ShineDevice f = this.this$0.e;
                    if (f != null) {
                        e.a(f, (m24.e<? super pr4.j, ? super pr4.i>) this.this$0.q);
                        BleCommandResultManager.d.a(CommunicateMode.LINK);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (this.this$0.s.U()) {
                    xm4 xm4 = xm4.d;
                    ns5 g = this.this$0.s;
                    if (g == null) {
                        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingFragment");
                    } else if (xm4.a(xm4, ((PairingFragment) g).getContext(), "PAIR_DEVICE", false, true, false, 20, (Object) null)) {
                        this.this$0.w();
                    }
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList2.addAll((Collection) obj);
            this.this$0.o().clear();
            ArrayList<SKUModel> o = this.this$0.o();
            dl6 b2 = this.this$0.c();
            qs5$f$b qs5_f_b = new qs5$f$b(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = o;
            this.label = 2;
            Object a2 = gk6.a(b2, qs5_f_b, this);
            if (a2 == a) {
                return a;
            }
            arrayList = o;
            obj = a2;
            arrayList.addAll((Collection) obj);
            ce.a(PortfolioApp.get.instance()).a(this.this$0.r, new IntentFilter("SCAN_DEVICE_FOUND"));
            this.this$0.t.k();
            this.this$0.h = new Handler(Looper.getMainLooper());
            if (this.this$0.e != null) {
            }
            if (this.this$0.s.U()) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements m24.e<cs4, as4> {
        @DexIgnore
        public /* final */ /* synthetic */ PairingPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public g(PairingPresenter pairingPresenter, String str) {
            this.a = pairingPresenter;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(cs4 cs4) {
            wg6.b(cs4, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            this.a.m();
        }

        @DexIgnore
        public void a(as4 as4) {
            wg6.b(as4, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = PairingPresenter.y.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + as4.a());
            this.a.s.a();
            int i = rs5.a[as4.a().ordinal()];
            if (i != 1) {
                if (i == 2) {
                    this.a.s.v(this.b);
                } else if (i == 3) {
                    this.a.s.n(this.b);
                } else if (i != 4) {
                    this.a.s.c(as4.a().ordinal(), this.b);
                } else {
                    FLogger.INSTANCE.getLocal().d(PairingPresenter.y.a(), "User deny stopping workout");
                }
            } else if (as4.b() != null) {
                List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(new ArrayList(as4.b()));
                wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                ns5 g = this.a.s;
                Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                if (array != null) {
                    uh4[] uh4Arr = (uh4[]) array;
                    g.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                    return;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
    }

    @DexIgnore
    public PairingPresenter(ns5 ns5, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, cj4 cj4, SetNotificationUseCase setNotificationUseCase, an4 an4) {
        wg6.b(ns5, "mPairingView");
        wg6.b(linkDeviceUseCase, "mLinkDeviceUseCase");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(cj4, "mDeviceSettingFactory");
        wg6.b(setNotificationUseCase, "mSetNotificationUseCase");
        wg6.b(an4, "mSharePrefs");
        this.s = ns5;
        this.t = linkDeviceUseCase;
        this.u = deviceRepository;
        this.v = cj4;
        this.w = setNotificationUseCase;
        this.x = an4;
    }

    @DexIgnore
    public final void A() {
        try {
            IButtonConnectivity b2 = PortfolioApp.get.b();
            if (b2 != null) {
                b2.deviceStopScan();
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void n() {
        this.t.m();
        ns5 ns5 = this.s;
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            wg6.a((Object) serial, "mPairingDevice!!.serial");
            ns5.a(serial, this.j);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final ArrayList<SKUModel> o() {
        return this.l;
    }

    @DexIgnore
    public final jl4 p() {
        return this.m;
    }

    @DexIgnore
    public final boolean q() {
        return this.j;
    }

    @DexIgnore
    public final boolean r() {
        return this.n;
    }

    @DexIgnore
    public final List<lc6<ShineDevice, String>> s() {
        return this.f;
    }

    @DexIgnore
    public final ArrayList<Device> t() {
        return this.k;
    }

    @DexIgnore
    public final boolean u() {
        Locale locale = Locale.getDefault();
        wg6.a((Object) locale, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale.getLanguage())) {
            return false;
        }
        Locale locale2 = Locale.getDefault();
        wg6.a((Object) locale2, "Locale.getDefault()");
        if (TextUtils.isEmpty(locale2.getCountry())) {
            return false;
        }
        StringBuilder sb = new StringBuilder();
        Locale locale3 = Locale.getDefault();
        wg6.a((Object) locale3, "Locale.getDefault()");
        sb.append(locale3.getLanguage());
        sb.append("_");
        Locale locale4 = Locale.getDefault();
        wg6.a((Object) locale4, "Locale.getDefault()");
        sb.append(locale4.getCountry());
        String sb2 = sb.toString();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = y.a();
        local.d(a2, "language: " + sb2);
        if (xj6.b(sb2, "zh_CN", true) || xj6.b(sb2, "zh_SG", true)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean v() {
        return this.i;
    }

    @DexIgnore
    public void w() {
        if (!this.i && this.o) {
            if (this.f.isEmpty()) {
                this.s.B();
                this.n = false;
                Handler handler = this.h;
                if (handler != null) {
                    handler.postDelayed(new b(), (long) com.misfit.frameworks.buttonservice.model.Device.DEFAULT_OTA_RESET_TIME);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                this.s.l(a(this.f));
            }
            z();
        }
    }

    @DexIgnore
    public final void x() {
        FLogger.INSTANCE.getLocal().d(y.a(), "onUserContinueToNextStep");
        this.s.a();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
        if (deviceBySerial != null && rs5.b[deviceBySerial.ordinal()] == 1) {
            this.s.t();
        } else {
            this.s.h();
        }
    }

    @DexIgnore
    public void y() {
        this.s.a(this);
    }

    @DexIgnore
    public void z() {
        try {
            IButtonConnectivity b2 = PortfolioApp.get.b();
            if (b2 != null) {
                b2.deviceStartScan();
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void b(boolean z) {
        this.j = z;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.n = z;
    }

    @DexIgnore
    public final HashMap<String, String> d(String str) {
        wg6.b(str, "serial");
        HashMap<String, String> hashMap = new HashMap<>();
        SKUModel skuModelBySerialPrefix = this.u.getSkuModelBySerialPrefix(DeviceHelper.o.b(str));
        if (skuModelBySerialPrefix != null) {
            String sku = skuModelBySerialPrefix.getSku();
            if (sku != null) {
                hashMap.put("Style_Number", sku);
                String deviceName = skuModelBySerialPrefix.getDeviceName();
                if (deviceName != null) {
                    hashMap.put("Device_Name", deviceName);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        return hashMap;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$g] */
    public void e(String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = y.a();
        local.d(a2, "syncDevice - serial=" + str);
        this.v.b(str).a(new bs4(FossilDeviceSerialPatternUtil.getDeviceBySerial(str) != FossilDeviceSerialPatternUtil.DEVICE.DIANA ? 10 : 15, str, false), new g(this, str));
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void g() {
        Handler handler = this.h;
        if (handler != null) {
            if (handler != null) {
                handler.removeCallbacksAndMessages((Object) null);
            } else {
                wg6.a();
                throw null;
            }
        }
        A();
        this.t.n();
        ce.a(PortfolioApp.get.instance()).a(this.r);
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d(y.a(), "cancelPairDevice()");
        PortfolioApp instance = PortfolioApp.get.instance();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            String serial = shineDevice.getSerial();
            wg6.a((Object) serial, "mPairingDevice!!.serial");
            instance.a(serial);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public ShineDevice i() {
        return this.e;
    }

    @DexIgnore
    public boolean j() {
        return this.j;
    }

    @DexIgnore
    public void k() {
        this.s.S();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void l() {
        List d2 = qd6.d(jm4.a((Context) PortfolioApp.get.instance(), 2131886660), jm4.a((Context) PortfolioApp.get.instance(), 2131886661));
        this.p = true;
        this.s.k(d2);
        this.t.a((long) StatisticConfig.MIN_UPLOAD_INTERVAL);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter$d] */
    public void m() {
        this.s.b();
        ShineDevice shineDevice = this.e;
        if (shineDevice != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = y.a();
            local.d(a2, "pairDevice - serial=" + shineDevice.getSerial());
            String serial = shineDevice.getSerial();
            wg6.a((Object) serial, "it.serial");
            String macAddress = shineDevice.getMacAddress();
            wg6.a((Object) macAddress, "it.macAddress");
            LinkDeviceUseCase.h hVar = new LinkDeviceUseCase.h(serial, macAddress);
            jl4 b2 = AnalyticsHelper.f.b("setup_device_session");
            this.m = b2;
            AnalyticsHelper.f.a("setup_device_session", b2);
            PortfolioApp instance = PortfolioApp.get.instance();
            CommunicateMode communicateMode = CommunicateMode.LINK;
            instance.a(communicateMode, "", communicateMode, hVar.a());
            this.t.a(hVar, this.q);
            jl4 jl4 = this.m;
            if (jl4 != null) {
                jl4.d();
            }
        }
    }

    @DexIgnore
    public void b(ShineDevice shineDevice) {
        wg6.b(shineDevice, "pairingDevice");
        this.e = shineDevice;
    }

    @DexIgnore
    public void c(ShineDevice shineDevice) {
        wg6.b(shineDevice, "device");
        this.s.b();
        this.o = false;
        A();
        this.e = shineDevice;
        String e2 = PortfolioApp.get.instance().e();
        if (!xj6.a(e2)) {
            e(e2);
        } else {
            m();
        }
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "serial");
        this.s.b();
        PortfolioApp.get.instance().a(str, false);
    }

    @DexIgnore
    public void a(boolean z) {
        this.o = z;
    }

    @DexIgnore
    public void a(int i2) {
        this.s.o();
    }

    @DexIgnore
    public void a(ShineDevice shineDevice) {
        wg6.b(shineDevice, "device");
        this.s.s(u());
    }

    @DexIgnore
    public final void d(ShineDevice shineDevice) {
        wg6.b(shineDevice, "shineDevice");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = y.a();
        local.d(a2, "addDevice serial=" + shineDevice.getSerial());
        String serial = shineDevice.getSerial();
        wg6.a((Object) serial, "shineDevice.serial");
        int c2 = c(serial);
        if (c2 == Integer.MIN_VALUE) {
            c2 = shineDevice.getRssi();
        }
        shineDevice.updateRssi(c2);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, shineDevice, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "serial");
        this.s.b();
        PortfolioApp.get.instance().a(str, true);
    }

    @DexIgnore
    public final void e(ShineDevice shineDevice) {
        T t2;
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (wg6.a((Object) ((ShineDevice) ((lc6) t2).getFirst()).getMacAddress(), (Object) shineDevice.getMacAddress())) {
                break;
            }
        }
        if (((lc6) t2) == null) {
            ArrayList<SKUModel> arrayList = this.l;
            ArrayList<SKUModel> arrayList2 = new ArrayList<>();
            for (T next : arrayList) {
                if (wg6.a((Object) ((SKUModel) next).getGroupName(), (Object) "WearOS")) {
                    arrayList2.add(next);
                }
            }
            if (!arrayList2.isEmpty()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = y.a();
                local.d(a2, "wearOS devices in DB=" + arrayList2);
                for (SKUModel deviceName : arrayList2) {
                    String name = shineDevice.getName();
                    wg6.a((Object) name, "shineDevice.name");
                    if (yj6.a((CharSequence) name, (CharSequence) String.valueOf(deviceName.getDeviceName()), true)) {
                        FLogger.INSTANCE.getLocal().d(y.a(), "Detected wearOS device is acceptable");
                        this.f.add(new lc6(shineDevice, shineDevice.getName()));
                        this.s.g(a(this.f));
                        return;
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void a(String str, int i2) {
        wg6.b(str, "serial");
        int i3 = (i2 == 0 || i2 == -999999) ? 0 : i2;
        if (this.g.containsKey(str)) {
            List list = this.g.get(str);
            if (list != null && !list.contains(0)) {
                if (list.size() < 5) {
                    list.add(Integer.valueOf(i2));
                    return;
                }
                list.remove(0);
                list.add(Integer.valueOf(i2));
                return;
            }
            return;
        }
        this.g.put(str, qd6.d(Integer.valueOf(i3)));
    }

    @DexIgnore
    public final int c(String str) {
        wg6.b(str, "serial");
        if (!this.g.containsKey(str)) {
            return Integer.MIN_VALUE;
        }
        double d2 = 0.0d;
        List<Number> list = this.g.get(str);
        if (list == null) {
            return Integer.MIN_VALUE;
        }
        for (Number intValue : list) {
            d2 += (double) intValue.intValue();
        }
        int size = list.size();
        if (size <= 0) {
            size = 1;
        }
        return (int) (d2 / ((double) size));
    }

    @DexIgnore
    public void d(boolean z) {
        this.i = z;
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        wg6.b(str, Constants.EVENT);
        wg6.b(map, "values");
        AnalyticsHelper.f.c().a(str, (Map<String, ? extends Object>) map);
    }

    @DexIgnore
    public final void a(Map<String, String> map) {
        wg6.b(map, "properties");
        AnalyticsHelper.f.c().a(map);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0043, code lost:
        if ((r2.length() == 0) != false) goto L_0x0045;
     */
    @DexIgnore
    public final List<lc6<ShineDevice, String>> a(List<lc6<ShineDevice, String>> list) {
        wg6.b(list, Constants.DEVICES);
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            lc6 lc6 = (lc6) next;
            boolean z = false;
            if (((ShineDevice) lc6.getFirst()).getRssi() <= -150) {
                String serial = ((ShineDevice) lc6.getFirst()).getSerial();
                wg6.a((Object) serial, "it.first.serial");
            }
            z = true;
            if (z) {
                arrayList.add(next);
            }
        }
        return yd6.d(arrayList);
    }
}
