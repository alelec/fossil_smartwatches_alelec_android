package com.portfolio.platform.uirenew.permission;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.ft5;
import com.fossil.gt5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.nu4;
import com.fossil.ow6;
import com.fossil.pw6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.qw6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xm4;
import com.fossil.yd6;
import com.fossil.zc4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PermissionFragment extends BaseFragment implements gt5 {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public ax5<zc4> f;
    @DexIgnore
    public ft5 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public nu4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return PermissionFragment.o;
        }

        @DexIgnore
        public final PermissionFragment b() {
            return new PermissionFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final PermissionFragment a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("PMS_SKIP_ABLE", z);
            PermissionFragment permissionFragment = new PermissionFragment();
            permissionFragment.setArguments(bundle);
            return permissionFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ PermissionFragment a;

        @DexIgnore
        public b(PermissionFragment permissionFragment) {
            this.a = permissionFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.j1();
        }
    }

    /*
    static {
        String simpleName = PermissionFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "PermissionFragment::class.java.simpleName!!");
            o = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public void b(int i2, List<String> list) {
        wg6.b(list, "perms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = o;
        local.d(str, "onPermissionsGranted:" + i2 + ':' + list.size());
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        j1();
        return true;
    }

    @DexIgnore
    public final void j1() {
        Intent intent = new Intent();
        intent.putExtra("NEED_TO_SHOW_PERMISSION_POPUP", false);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(10, intent);
        }
        ft5 ft5 = this.g;
        if (ft5 != null) {
            ft5.h();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558594, viewGroup, false, e1()));
        ax5<zc4> ax5 = this.f;
        if (ax5 != null) {
            zc4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ft5 ft5 = this.g;
        if (ft5 != null) {
            ft5.g();
            PermissionFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        PermissionFragment.super.onResume();
        ft5 ft5 = this.g;
        if (ft5 != null) {
            ft5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        ImageView imageView;
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<zc4> ax5 = this.f;
        if (ax5 != null) {
            zc4 a2 = ax5.a();
            if (!(a2 == null || (imageView = a2.r) == null)) {
                imageView.setOnClickListener(new b(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.h = arguments.getBoolean("PMS_SKIP_ABLE");
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void t(List<nu4.c> list) {
        Object r0;
        Object r02;
        wg6.b(list, "listPermissionModel");
        if (this.i == null) {
            int i2 = 0;
            if (list.size() > 1) {
                ax5<zc4> ax5 = this.f;
                if (ax5 != null) {
                    zc4 a2 = ax5.a();
                    if (!(a2 == null || (r02 = a2.q) == 0)) {
                        r02.setVisibility(0);
                    }
                    i2 = 1;
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            } else {
                ax5<zc4> ax52 = this.f;
                if (ax52 != null) {
                    zc4 a3 = ax52.a();
                    if (!(a3 == null || (r0 = a3.q) == 0)) {
                        r0.setVisibility(8);
                    }
                } else {
                    wg6.d("mBinding");
                    throw null;
                }
            }
            nu4 nu4 = new nu4(i2, this.h);
            nu4.a((nu4.b) new c(this));
            this.i = nu4;
            ax5<zc4> ax53 = this.f;
            if (ax53 != null) {
                zc4 a4 = ax53.a();
                RecyclerView recyclerView = a4 != null ? a4.t : null;
                if (recyclerView != null) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    recyclerView.setAdapter(this.i);
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
        nu4 nu42 = this.i;
        if (nu42 != null) {
            nu42.a(list);
        }
    }

    @DexIgnore
    public void a(ft5 ft5) {
        wg6.b(ft5, "presenter");
        this.g = ft5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(int i2, List<String> list) {
        wg6.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(o, "onPermissionsDenied:" + i2 + ':' + list.size());
        if (pw6.a(this, list)) {
            boolean z = !yd6.b(xm4.d.a(100), (ArrayList<String>) list).isEmpty();
            boolean z2 = !yd6.b(xm4.d.a(101), (ArrayList<String>) list).isEmpty();
            if (z) {
                ow6.b bVar = new ow6.b(this);
                bVar.b(jm4.a((Context) PortfolioApp.get.instance(), 2131887058));
                bVar.a(xm4.d.b(100).getSecond());
                bVar.a().b();
            } else if (z2) {
                ow6.b bVar2 = new ow6.b(this);
                bVar2.b(jm4.a((Context) PortfolioApp.get.instance(), 2131887058));
                bVar2.a(xm4.d.b(101).getSecond());
                bVar2.a().b();
            } else {
                new ow6.b(this).a().b();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements nu4.b {
        @DexIgnore
        public /* final */ /* synthetic */ PermissionFragment a;

        @DexIgnore
        public c(PermissionFragment permissionFragment) {
            this.a = permissionFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void a(nu4.c cVar) {
            String[] strArr;
            wg6.b(cVar, "permissionModel");
            if (cVar.d() == 15) {
                if (pw6.a(PortfolioApp.get.instance(), new String[]{"android.permission.ACCESS_FINE_LOCATION"})) {
                    Object[] array = cVar.e().toArray(new String[0]);
                    if (array != null) {
                        strArr = (String[]) array;
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                } else {
                    Object[] array2 = qd6.c("android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_BACKGROUND_LOCATION").toArray(new String[0]);
                    if (array2 != null) {
                        strArr = (String[]) array2;
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                qw6.b bVar = new qw6.b(this.a, cVar.d(), (String[]) Arrays.copyOf(strArr, strArr.length));
                bVar.a(cVar.c());
                bVar.a(2131952014);
                pw6.a(bVar.a());
                return;
            }
            PermissionFragment permissionFragment = this.a;
            int d = cVar.d();
            Object[] array3 = cVar.e().toArray(new String[0]);
            if (array3 != null) {
                String[] strArr2 = (String[]) array3;
                qw6.b bVar2 = new qw6.b(permissionFragment, d, (String[]) Arrays.copyOf(strArr2, strArr2.length));
                bVar2.a(cVar.c());
                bVar2.a(2131952014);
                pw6.a(bVar2.a());
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void b(nu4.c cVar) {
            wg6.b(cVar, "permissionModel");
            int d = cVar.d();
            if (d == 1) {
                this.a.startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), 888);
            } else if (d == 3) {
                this.a.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
            }
        }

        @DexIgnore
        public void a() {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.setResult(1);
                this.a.close();
                return;
            }
            wg6.a();
            throw null;
        }
    }
}
