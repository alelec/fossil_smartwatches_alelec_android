package com.portfolio.platform.uirenew.onboarding.ota;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.br5$b$a;
import com.fossil.br5$c$a;
import com.fossil.br5$f$a;
import com.fossil.br5$g$a;
import com.fossil.br5$g$b;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.cr5;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl4;
import com.fossil.jm4;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mr4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xq5;
import com.fossil.yq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwarePresenter extends xq5 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Device g;
    @DexIgnore
    public jl4 h;
    @DexIgnore
    public /* final */ e i; // = new e(this);
    @DexIgnore
    public /* final */ d j; // = new d(this);
    @DexIgnore
    public /* final */ yq5 k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ cj4 m;
    @DexIgnore
    public /* final */ an4 n;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase o;
    @DexIgnore
    public /* final */ DownloadFirmwareByDeviceModelUsecase p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwarePresenter.q;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<mr4.d, mr4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ Device c;

        @DexIgnore
        public b(UpdateFirmwarePresenter updateFirmwarePresenter, String str, Device device) {
            this.a = updateFirmwarePresenter;
            this.b = str;
            this.c = device;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DownloadFirmwareByDeviceModelUsecase.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new br5$b$a(this, dVar, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(DownloadFirmwareByDeviceModelUsecase.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().e(UpdateFirmwarePresenter.r.a(), "checkFirmware - downloadFw FAILED!!!");
            this.a.o().m0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceId;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, String str2, xe6 xe6, UpdateFirmwarePresenter updateFirmwarePresenter, Device device) {
            super(2, xe6);
            this.$deviceId = str;
            this.$lastFwTemp = str2;
            this.this$0 = updateFirmwarePresenter;
            this.$device$inlined = device;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.$deviceId, this.$lastFwTemp, xe6, this.this$0, this.$device$inlined);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.b();
                br5$c$a br5_c_a = new br5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, br5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) obj;
            FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.r.a(), "checkLastOTASuccess - getDeviceBySerial SUCCESS");
            if (device != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = UpdateFirmwarePresenter.r.a();
                local.d(a2, "checkLastOTASuccess - currentDeviceFw=" + device.getFirmwareRevision());
                if (!xj6.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                    FLogger.INSTANCE.getLocal().d(UpdateFirmwarePresenter.r.a(), "Handle OTA complete on check last OTA success");
                    this.this$0.n.n(this.$deviceId);
                    this.this$0.o().D0();
                } else {
                    this.this$0.a(this.$device$inlined);
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        public d(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.r.a();
            local.d(a2, "otaCompleteReceiver - isSuccess=" + booleanExtra);
            this.a.o().b(booleanExtra);
            this.a.n.n(PortfolioApp.get.instance().e());
            if (booleanExtra) {
                FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.e, UpdateFirmwarePresenter.r.a(), "[Sync Start] AUTO SYNC after OTA");
                PortfolioApp.get.instance().a(this.a.m, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter a;

        @DexIgnore
        public e(UpdateFirmwarePresenter updateFirmwarePresenter) {
            this.a = updateFirmwarePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = UpdateFirmwarePresenter.r.a();
                local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && xj6.b(otaEvent.getSerial(), PortfolioApp.get.instance().e(), true)) {
                    this.a.o().h((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1", f = "UpdateFirmwarePresenter.kt", l = {105}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(UpdateFirmwarePresenter updateFirmwarePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = updateFirmwarePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            String str;
            UpdateFirmwarePresenter updateFirmwarePresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                str = PortfolioApp.get.instance().e();
                UpdateFirmwarePresenter updateFirmwarePresenter2 = this.this$0;
                dl6 b = updateFirmwarePresenter2.b();
                br5$f$a br5_f_a = new br5$f$a(this, str, (xe6) null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = updateFirmwarePresenter2;
                this.label = 1;
                obj = gk6.a(b, br5_f_a, this);
                if (obj == a) {
                    return a;
                }
                updateFirmwarePresenter = updateFirmwarePresenter2;
            } else if (i == 1) {
                updateFirmwarePresenter = (UpdateFirmwarePresenter) this.L$2;
                str = (String) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            updateFirmwarePresenter.g = (Device) obj;
            boolean C = PortfolioApp.get.instance().C();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = UpdateFirmwarePresenter.r.a();
            local.d(a2, "start - activeSerial=" + str + ", isDeviceOtaing=" + C);
            if (!C) {
                UpdateFirmwarePresenter updateFirmwarePresenter3 = this.this$0;
                updateFirmwarePresenter3.b(updateFirmwarePresenter3.g);
            }
            this.this$0.m();
            jl4 b2 = AnalyticsHelper.f.b("ota_session");
            this.this$0.h = b2;
            AnalyticsHelper.f.a("ota_session", b2);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$updateFirmware$1", f = "UpdateFirmwarePresenter.kt", l = {247}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwarePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(UpdateFirmwarePresenter updateFirmwarePresenter, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = updateFirmwarePresenter;
            this.$activeSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$activeSerial, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v6, types: [com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.br5$g$b] */
        public final Object invokeSuspend(Object obj) {
            jl4 c;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.b();
                br5$g$a br5_g_a = new br5$g$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, br5_g_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            String str = (String) lc6.component1();
            String str2 = (String) lc6.component2();
            if (!(str == null || str2 == null || (c = this.this$0.h) == null)) {
                c.a("old_firmware", str);
                if (c != null) {
                    c.a("new_firmware", str2);
                    if (c != null) {
                        c.d();
                    }
                }
            }
            this.this$0.o.a(new UpdateFirmwareUsecase.b(this.$activeSerial, false, 2, (qg6) null), new br5$g$b());
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwarePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "UpdateFirmwarePresenter::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwarePresenter(yq5 yq5, DeviceRepository deviceRepository, UserRepository userRepository, cj4 cj4, an4 an4, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        wg6.b(yq5, "mView");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(cj4, "mDeviceSettingFactory");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(updateFirmwareUsecase, "mUpdateFirmwareUseCase");
        wg6.b(downloadFirmwareByDeviceModelUsecase, "mDownloadFwByDeviceModel");
        this.k = yq5;
        this.l = deviceRepository;
        this.m = cj4;
        this.n = an4;
        this.o = updateFirmwareUsecase;
        this.p = downloadFirmwareByDeviceModelUsecase;
    }

    @DexIgnore
    public boolean j() {
        return this.f;
    }

    @DexIgnore
    public void k() {
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().e());
        if (deviceBySerial != null && cr5.a[deviceBySerial.ordinal()] == 1) {
            this.k.t();
        } else {
            this.k.h();
        }
    }

    @DexIgnore
    public void l() {
        FLogger.INSTANCE.getLocal().d(q, "tryOtaAgain");
        Device device = this.g;
        if (device != null) {
            c(device);
        }
        this.k.v0();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v24, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void m() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (deviceBySerial == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
            explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886684));
            explore.setBackground(2131231385);
            explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886685));
            explore2.setBackground(2131231383);
            explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886682));
            explore3.setBackground(2131231386);
            explore4.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886683));
            explore4.setBackground(2131231382);
        } else {
            explore.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886691));
            explore.setBackground(2131231385);
            explore2.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886690));
            explore2.setBackground(2131231387);
            explore3.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886688));
            explore3.setBackground(2131231386);
            explore4.setDescription(jm4.a((Context) PortfolioApp.get.instance(), 2131886689));
            explore4.setBackground(2131231384);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.k.h((List<? extends Explore>) arrayList);
    }

    @DexIgnore
    public final DeviceRepository n() {
        return this.l;
    }

    @DexIgnore
    public final yq5 o() {
        return this.k;
    }

    @DexIgnore
    public void p() {
        this.k.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "start - mIsOnBoardingFlow=" + this.f);
        if (!this.f) {
            this.k.d0();
        }
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.j, CommunicateMode.OTA);
        Object instance = PortfolioApp.get.instance();
        e eVar = this.i;
        instance.registerReceiver(eVar, new IntentFilter(PortfolioApp.get.instance().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        this.k.g();
        BleCommandResultManager.d.a(CommunicateMode.OTA);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void g() {
        try {
            PortfolioApp.get.instance().unregisterReceiver(this.i);
            BleCommandResultManager.d.b((BleCommandResultManager.b) this.j, CommunicateMode.OTA);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.e(str, "stop - e=" + e2);
        }
    }

    @DexIgnore
    public void h() {
        BleCommandResultManager.d.a(CommunicateMode.OTA);
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d(q, "checkFwAgain");
        b(this.g);
    }

    @DexIgnore
    public final void b(Device device) {
        if (device != null) {
            String deviceId = device.getDeviceId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            local.d(str, "checkLastOTASuccess - deviceId=" + deviceId + ", sku=" + device.getSku());
            if (TextUtils.isEmpty(deviceId)) {
                FLogger.INSTANCE.getLocal().e(q, "checkLastOTASuccess - DEVICE ID IS EMPTY!!!");
                return;
            }
            String i2 = this.n.i(deviceId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = q;
            local2.d(str2, "checkLastOTASuccess - lastTempFw=" + i2);
            if (TextUtils.isEmpty(i2)) {
                a(device);
                return;
            }
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(deviceId, i2, (xe6) null, this, device), 3, (Object) null);
        }
    }

    @DexIgnore
    public final void c(Device device) {
        wg6.b(device, "Device");
        String deviceId = device.getDeviceId();
        boolean C = PortfolioApp.get.instance().C();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "updateFirmware - activeSerial=" + deviceId + ", isDeviceOtaing=" + C);
        if (!C) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new g(this, deviceId, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$b] */
    public final void a(Device device) {
        String deviceId = device.getDeviceId();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.d(str, "checkFirmware - deviceId=" + deviceId + ", sku=" + device.getSku() + ", fw=" + device.getFirmwareRevision());
        if (PortfolioApp.get.instance().F() || !this.n.V()) {
            Object r1 = this.p;
            String sku = device.getSku();
            if (sku == null) {
                sku = "";
            }
            r1.a(new DownloadFirmwareByDeviceModelUsecase.b(sku), new b(this, deviceId, device));
            return;
        }
        this.k.D0();
    }

    @DexIgnore
    public void a(boolean z, String str) {
        wg6.b(str, "deviceSerial");
        this.f = z;
        this.e = str;
    }
}
