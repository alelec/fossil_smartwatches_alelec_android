package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchSettingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ String B;
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return WatchSettingActivity.B;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wg6.b(context, "context");
            wg6.b(str, "serial");
            Intent intent = new Intent(context, WatchSettingActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = a();
            local.d(a, "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    /*
    static {
        String simpleName = WatchSettingActivity.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchSettingActivity::class.java.simpleName");
        B = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.uirenew.watchsetting.WatchSettingActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        WatchSettingFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = WatchSettingFragment.r.b();
            a((Fragment) b, WatchSettingFragment.r.a(), 2131362119);
        }
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f = f();
            local.d(f, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("SERIAL", string);
                b.setArguments(bundle2);
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local2.d(f2, "retrieve serial from intent " + stringExtra);
            Bundle bundle3 = new Bundle();
            bundle3.putString("SERIAL", stringExtra);
            b.setArguments(bundle3);
        }
    }
}
