package com.portfolio.platform.uirenew;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.c06;
import com.fossil.hr5;
import com.fossil.ir5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.ld;
import com.fossil.ld4;
import com.fossil.lx5;
import com.fossil.ox5;
import com.fossil.qg6;
import com.fossil.rh4;
import com.fossil.rr5;
import com.fossil.tr5;
import com.fossil.vd;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.fossil.y04;
import com.fossil.zt4;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupFragment extends BaseFragment implements ir5 {
    @DexIgnore
    public static /* final */ a r; // = new a((qg6) null);
    @DexIgnore
    public hr5 f;
    @DexIgnore
    public ax5<ld4> g;
    @DexIgnore
    public BirthdayFragment h;
    @DexIgnore
    public tr5 i;
    @DexIgnore
    public c06 j;
    @DexIgnore
    public Integer o;
    @DexIgnore
    public /* final */ String p; // = ThemeManager.l.a().b("primaryColor");
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ProfileSetupFragment a() {
            return new ProfileSetupFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public b(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ProfileSetupFragment.a(this.a).e(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public c(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ProfileSetupFragment.a(this.a).d(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public d(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ProfileSetupFragment.a(this.a).c(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public e(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ProfileSetupFragment.a(this.a).b(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public f(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileSetupFragment.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public g(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ProfileSetupFragment.a(this.a).a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public h(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ProfileSetupFragment.a(this.a).b(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public i(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ProfileSetupFragment.a(this.a).c(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public j(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ld4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment b;

        @DexIgnore
        public k(ld4 ld4, ProfileSetupFragment profileSetupFragment) {
            this.a = ld4;
            this.b = profileSetupFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.v;
            wg6.a((Object) flexibleTextInputEditText, "binding.etBirthday");
            Editable text = flexibleTextInputEditText.getText();
            if (text == null || text.length() == 0) {
                ProfileSetupFragment.a(this.b).k();
            } else {
                ProfileSetupFragment.a(this.b).j();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public l(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileSetupFragment.a(this.a).a(rh4.MALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public m(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileSetupFragment.a(this.a).a(rh4.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public n(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileSetupFragment.a(this.a).a(rh4.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public o(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ProfileSetupFragment.a(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements ld<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileSetupFragment a;

        @DexIgnore
        public p(ProfileSetupFragment profileSetupFragment) {
            this.a = profileSetupFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Date date) {
            if (date != null) {
                Calendar i = ProfileSetupFragment.a(this.a).i();
                i.setTime(date);
                ProfileSetupFragment.a(this.a).a(date, i);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ hr5 a(ProfileSetupFragment profileSetupFragment) {
        hr5 hr5 = profileSetupFragment.f;
        if (hr5 != null) {
            return hr5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void B0() {
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.z;
                    wg6.a((Object) r1, "it.fbCreateAccount");
                    r1.setEnabled(true);
                    Object r12 = a2.z;
                    wg6.a((Object) r12, "it.fbCreateAccount");
                    r12.setClickable(true);
                    Object r13 = a2.z;
                    wg6.a((Object) r13, "it.fbCreateAccount");
                    r13.setFocusable(true);
                    a2.z.a("flexible_button_primary");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void D(boolean z) {
        Object r0;
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null && (r0 = a2.M) != 0) {
                    wg6.a((Object) r0, "it");
                    r0.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void E(boolean z) {
        Object r0;
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null && (r0 = a2.L) != 0) {
                    wg6.a((Object) r0, "it");
                    r0.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public void M(String str) {
        Object r0;
        wg6.b(str, "birthdate");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null && (r0 = a2.v) != 0) {
                    r0.setText(str);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void U0() {
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.z;
                    wg6.a((Object) r1, "it.fbCreateAccount");
                    r1.setEnabled(false);
                    Object r12 = a2.z;
                    wg6.a((Object) r12, "it.fbCreateAccount");
                    r12.setClickable(false);
                    Object r13 = a2.z;
                    wg6.a((Object) r13, "it.fbCreateAccount");
                    r13.setFocusable(false);
                    a2.z.a("flexible_button_disabled");
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v1, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    public void b(boolean z, String str) {
        wg6.b(str, "message");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.H;
                    wg6.a((Object) flexibleTextInputLayout, "it.inputBirthday");
                    flexibleTextInputLayout.setErrorEnabled(z);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.H;
                    wg6.a((Object) flexibleTextInputLayout2, "it.inputBirthday");
                    flexibleTextInputLayout2.setError(str);
                    Object r5 = a2.J;
                    wg6.a((Object) r5, "it.ivCheckedBirthday");
                    r5.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void c(Spanned spanned) {
        wg6.b(spanned, "message");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.G;
                    wg6.a((Object) r1, "binding.ftvThree");
                    r1.setText(spanned);
                    Integer num = this.o;
                    if (num != null) {
                        a2.G.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void d(Spanned spanned) {
        wg6.b(spanned, "message");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.F;
                    wg6.a((Object) r1, "binding.ftvOne");
                    r1.setText(spanned);
                    Integer num = this.o;
                    if (num != null) {
                        a2.F.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g() {
        DashBar dashBar;
        ax5<ld4> ax5 = this.g;
        if (ax5 != null) {
            ld4 a2 = ax5.a();
            if (a2 != null && (dashBar = a2.O) != null) {
                ox5.a aVar = ox5.a;
                wg6.a((Object) dashBar, "this");
                aVar.b(dashBar, 500);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "ProfileSetupFragment";
    }

    @DexIgnore
    public void i() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void k() {
        if (isActive()) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886718);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            X(a2);
        }
    }

    @DexIgnore
    public void n0() {
        FragmentActivity activity;
        if (isActive() && (activity = getActivity()) != null) {
            OnboardingHeightWeightActivity.a aVar = OnboardingHeightWeightActivity.C;
            wg6.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ProfileSetupFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.g = new ax5<>(this, kb.a(layoutInflater, 2131558600, viewGroup, false, e1()));
        ax5<ld4> ax5 = this.g;
        if (ax5 != null) {
            ld4 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        ProfileSetupFragment.super.onPause();
        hr5 hr5 = this.f;
        if (hr5 != null) {
            hr5.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        ProfileSetupFragment.super.onResume();
        hr5 hr5 = this.f;
        if (hr5 != null) {
            hr5.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r5v5, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r5v8, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r5v9, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v10, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v11, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v17, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r5v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c06 a2 = vd.a(activity).a(c06.class);
            wg6.a((Object) a2, "ViewModelProviders.of(ac\u2026DayViewModel::class.java)");
            this.j = a2;
            this.h = getChildFragmentManager().b(BirthdayFragment.s.a());
            if (this.h == null) {
                this.h = BirthdayFragment.s.b();
            }
            y04 g2 = PortfolioApp.get.instance().g();
            BirthdayFragment birthdayFragment = this.h;
            if (birthdayFragment != null) {
                g2.a(new rr5(birthdayFragment)).a(this);
                c06 c06 = this.j;
                if (c06 != null) {
                    c06.a().a(this, new p(this));
                    ax5<ld4> ax5 = this.g;
                    if (ax5 != null) {
                        ld4 a3 = ax5.a();
                        if (a3 != null) {
                            if (!TextUtils.isEmpty(this.p)) {
                                this.o = Integer.valueOf(Color.parseColor(this.p));
                                Integer num = this.o;
                                if (num != null) {
                                    int intValue = num.intValue();
                                    AppCompatCheckBox appCompatCheckBox = a3.u;
                                    wg6.a((Object) appCompatCheckBox, "binding.cbTwo");
                                    appCompatCheckBox.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox2 = a3.u;
                                    wg6.a((Object) appCompatCheckBox2, "binding.cbTwo");
                                    appCompatCheckBox2.setButtonTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox3 = a3.s;
                                    wg6.a((Object) appCompatCheckBox3, "binding.cbOne");
                                    appCompatCheckBox3.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox4 = a3.s;
                                    wg6.a((Object) appCompatCheckBox4, "binding.cbOne");
                                    appCompatCheckBox4.setButtonTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox5 = a3.t;
                                    wg6.a((Object) appCompatCheckBox5, "binding.cbThree");
                                    appCompatCheckBox5.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox6 = a3.t;
                                    wg6.a((Object) appCompatCheckBox6, "binding.cbThree");
                                    appCompatCheckBox6.setButtonTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox7 = a3.q;
                                    wg6.a((Object) appCompatCheckBox7, "binding.cbFive");
                                    appCompatCheckBox7.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox8 = a3.q;
                                    wg6.a((Object) appCompatCheckBox8, "binding.cbFive");
                                    appCompatCheckBox8.setButtonTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox9 = a3.r;
                                    wg6.a((Object) appCompatCheckBox9, "binding.cbFour");
                                    appCompatCheckBox9.setBackgroundTintList(ColorStateList.valueOf(intValue));
                                    AppCompatCheckBox appCompatCheckBox10 = a3.r;
                                    wg6.a((Object) appCompatCheckBox10, "binding.cbFour");
                                    appCompatCheckBox10.setButtonTintList(ColorStateList.valueOf(intValue));
                                }
                            }
                            a3.w.addTextChangedListener(new g(this));
                            a3.x.addTextChangedListener(new h(this));
                            a3.y.addTextChangedListener(new i(this));
                            a3.N.setOnClickListener(new j(this));
                            a3.v.setOnClickListener(new k(a3, this));
                            a3.B.setOnClickListener(new l(this));
                            a3.A.setOnClickListener(new m(this));
                            a3.C.setOnClickListener(new n(this));
                            a3.u.setOnCheckedChangeListener(new o(this));
                            a3.s.setOnCheckedChangeListener(new b(this));
                            a3.t.setOnCheckedChangeListener(new c(this));
                            a3.r.setOnCheckedChangeListener(new d(this));
                            a3.q.setOnCheckedChangeListener(new e(this));
                            a3.z.setOnClickListener(new f(this));
                            Object r5 = a3.F;
                            wg6.a((Object) r5, "binding.ftvOne");
                            r5.setMovementMethod(new LinkMovementMethod());
                            Object r52 = a3.G;
                            wg6.a((Object) r52, "binding.ftvThree");
                            r52.setMovementMethod(new LinkMovementMethod());
                            Object r53 = a3.E;
                            wg6.a((Object) r53, "binding.ftvFour");
                            r53.setMovementMethod(new LinkMovementMethod());
                            Object r4 = a3.D;
                            wg6.a((Object) r4, "binding.ftvFive");
                            r4.setMovementMethod(new LinkMovementMethod());
                        }
                        W("profile_setup_view");
                        return;
                    }
                    wg6.d("mBinding");
                    throw null;
                }
                wg6.d("mUserBirthDayViewModel");
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(hr5 hr5) {
        wg6.b(hr5, "presenter");
        this.f = hr5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.LinearLayout, com.portfolio.platform.view.FlexibleTextInputLayout, java.lang.Object] */
    public void a(boolean z, boolean z2, String str) {
        int i2;
        wg6.b(str, "errorMessage");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.I;
                    wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.I;
                    wg6.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                    Object r5 = a2.K;
                    wg6.a((Object) r5, "it.ivCheckedEmail");
                    if (z) {
                        Object r4 = a2.I;
                        wg6.a((Object) r4, "it.inputEmail");
                        if (r4.getVisibility() == 0) {
                            i2 = 0;
                            r5.setVisibility(i2);
                            return;
                        }
                    }
                    i2 = 8;
                    r5.setVisibility(i2);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void g(int i2, String str) {
        wg6.b(str, "errorMessage");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void b(Spanned spanned) {
        wg6.b(spanned, "message");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.D;
                    wg6.a((Object) r1, "binding.ftvFive");
                    r1.setText(spanned);
                    Integer num = this.o;
                    if (num != null) {
                        a2.D.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [android.widget.LinearLayout, com.portfolio.platform.view.FlexibleTextInputLayout, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.ImageView, java.lang.Object, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r7v3, types: [java.lang.Object, android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r7v4, types: [android.widget.LinearLayout, com.portfolio.platform.view.FlexibleTextInputLayout, java.lang.Object] */
    public void c(SignUpEmailAuth signUpEmailAuth) {
        wg6.b(signUpEmailAuth, "auth");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r2 = a2.z;
                    wg6.a((Object) r2, "it.fbCreateAccount");
                    r2.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886725));
                    Object r22 = a2.I;
                    wg6.a((Object) r22, "it.inputEmail");
                    r22.setVisibility(8);
                    Object r23 = a2.K;
                    wg6.a((Object) r23, "it.ivCheckedEmail");
                    r23.setVisibility(8);
                    if (!TextUtils.isEmpty(signUpEmailAuth.getEmail())) {
                        a2.w.setText(signUpEmailAuth.getEmail());
                        Object r7 = a2.w;
                        wg6.a((Object) r7, "it.etEmail");
                        r7.setKeyListener((KeyListener) null);
                        Object r72 = a2.I;
                        wg6.a((Object) r72, "it.inputEmail");
                        r72.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout = a2.I;
                        wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                        flexibleTextInputLayout.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wg6.b(bundle, "data");
        BirthdayFragment birthdayFragment = this.h;
        if (birthdayFragment != null) {
            birthdayFragment.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            birthdayFragment.show(childFragmentManager, BirthdayFragment.s.a());
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void a(Spanned spanned) {
        wg6.b(spanned, "message");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.E;
                    wg6.a((Object) r1, "binding.ftvFour");
                    r1.setText(spanned);
                    Integer num = this.o;
                    if (num != null) {
                        a2.E.setLinkTextColor(num.intValue());
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.widget.TextView, java.lang.Object, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v14, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v15, types: [android.widget.TextView, java.lang.Object, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v16, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v17, types: [android.widget.TextView, java.lang.Object, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v18, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v19, types: [android.widget.TextView, java.lang.Object, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public void b(MFUser mFUser) {
        Date date;
        wg6.b(mFUser, "user");
        if (isActive()) {
            U0();
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r2 = a2.z;
                    wg6.a((Object) r2, "it.fbCreateAccount");
                    r2.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886708));
                    rh4 gender = mFUser.getGender();
                    wg6.a((Object) gender, "user.gender");
                    a(gender);
                    String email = mFUser.getEmail();
                    boolean z = false;
                    if (!(email == null || xj6.a(email))) {
                        a2.w.setText(mFUser.getEmail());
                        Object r22 = a2.w;
                        wg6.a((Object) r22, "it.etEmail");
                        a((TextView) r22);
                    }
                    String firstName = mFUser.getFirstName();
                    wg6.a((Object) firstName, "user.firstName");
                    if (!xj6.a(firstName)) {
                        a2.x.setText(mFUser.getFirstName());
                        Object r23 = a2.x;
                        wg6.a((Object) r23, "it.etFirstName");
                        a((TextView) r23);
                    }
                    String lastName = mFUser.getLastName();
                    wg6.a((Object) lastName, "user.lastName");
                    if (!xj6.a(lastName)) {
                        a2.y.setText(mFUser.getLastName());
                        Object r24 = a2.y;
                        wg6.a((Object) r24, "it.etLastName");
                        a((TextView) r24);
                    }
                    String birthday = mFUser.getBirthday();
                    if (birthday == null || xj6.a(birthday)) {
                        z = true;
                    }
                    if (!z) {
                        Object r0 = a2.v;
                        wg6.a((Object) r0, "it.etBirthday");
                        a((TextView) r0);
                        try {
                            SimpleDateFormat simpleDateFormat = bk4.a.get();
                            if (simpleDateFormat != null) {
                                date = simpleDateFormat.parse(mFUser.getBirthday());
                                hr5 hr5 = this.f;
                                if (hr5 != null) {
                                    Calendar i2 = hr5.i();
                                    i2.setTime(date);
                                    hr5 hr52 = this.f;
                                    if (hr52 == null) {
                                        wg6.d("mPresenter");
                                        throw null;
                                    } else if (date != null) {
                                        hr52.a(date, i2);
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.e("ProfileSetupFragment", "toOffsetDateTime - e=" + e2);
                            date = new Date();
                        }
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r2v3, types: [android.widget.LinearLayout, com.portfolio.platform.view.FlexibleTextInputLayout, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r6v3, types: [java.lang.Object, android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    /* JADX WARNING: type inference failed for: r6v4, types: [android.widget.LinearLayout, com.portfolio.platform.view.FlexibleTextInputLayout, java.lang.Object] */
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object r2 = a2.z;
                    wg6.a((Object) r2, "it.fbCreateAccount");
                    r2.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886725));
                    a2.x.setText(signUpSocialAuth.getFirstName());
                    a2.y.setText(signUpSocialAuth.getLastName());
                    Object r22 = a2.I;
                    wg6.a((Object) r22, "it.inputEmail");
                    r22.setVisibility(0);
                    if (!TextUtils.isEmpty(signUpSocialAuth.getEmail())) {
                        a2.w.setText(signUpSocialAuth.getEmail());
                        Object r6 = a2.w;
                        wg6.a((Object) r6, "it.etEmail");
                        r6.setKeyListener((KeyListener) null);
                        Object r62 = a2.I;
                        wg6.a((Object) r62, "it.inputEmail");
                        r62.setFocusable(false);
                        FlexibleTextInputLayout flexibleTextInputLayout = a2.I;
                        wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                        flexibleTextInputLayout.setEnabled(true);
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(TextView textView) {
        Drawable c2 = w6.c(PortfolioApp.get.instance(), 2131230841);
        textView.setTextColor(w6.a(PortfolioApp.get.instance(), 2131099701));
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setBackground(c2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r9v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r9v6, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r9v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r9v9, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r9v11, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r9v12, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void a(rh4 rh4) {
        wg6.b(rh4, "gender");
        if (isActive()) {
            ax5<ld4> ax5 = this.g;
            if (ax5 != null) {
                ld4 a2 = ax5.a();
                if (a2 != null) {
                    Object instance = PortfolioApp.get.instance();
                    int a3 = w6.a(instance, R.color.activeColorPrimary);
                    int a4 = w6.a(instance, 2131100404);
                    Drawable c2 = w6.c(instance, 2131230841);
                    Drawable c3 = w6.c(instance, 2131230833);
                    a2.C.setTextColor(a3);
                    a2.A.setTextColor(a3);
                    a2.B.setTextColor(a3);
                    Object r2 = a2.C;
                    wg6.a((Object) r2, "it.fbOther");
                    r2.setBackground(c2);
                    Object r22 = a2.A;
                    wg6.a((Object) r22, "it.fbFemale");
                    r22.setBackground(c2);
                    Object r23 = a2.B;
                    wg6.a((Object) r23, "it.fbMale");
                    r23.setBackground(c2);
                    a2.B.a("flexible_button_secondary");
                    a2.A.a("flexible_button_secondary");
                    a2.C.a("flexible_button_secondary");
                    int i2 = zt4.a[rh4.ordinal()];
                    if (i2 == 1) {
                        a2.B.setTextColor(a4);
                        Object r9 = a2.B;
                        wg6.a((Object) r9, "it.fbMale");
                        r9.setBackground(c3);
                        a2.B.a("flexible_button_primary");
                    } else if (i2 == 2) {
                        a2.A.setTextColor(a4);
                        Object r92 = a2.A;
                        wg6.a((Object) r92, "it.fbFemale");
                        r92.setBackground(c3);
                        a2.A.a("flexible_button_primary");
                    } else if (i2 == 3) {
                        a2.C.setTextColor(a4);
                        Object r93 = a2.C;
                        wg6.a((Object) r93, "it.fbOther");
                        r93.setBackground(c3);
                        a2.C.a("flexible_button_primary");
                    }
                }
            } else {
                wg6.d("mBinding");
                throw null;
            }
        }
    }
}
