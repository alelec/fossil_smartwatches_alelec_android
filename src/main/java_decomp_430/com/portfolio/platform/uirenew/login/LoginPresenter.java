package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hw5;
import com.fossil.hx5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ip5;
import com.fossil.it4;
import com.fossil.jm4;
import com.fossil.jp5;
import com.fossil.kn4;
import com.fossil.kt4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.lt4;
import com.fossil.m24;
import com.fossil.mt4;
import com.fossil.nc6;
import com.fossil.np5$b$a;
import com.fossil.np5$d$a;
import com.fossil.np5$d$b;
import com.fossil.np5$k$a;
import com.fossil.nt4;
import com.fossil.ot4;
import com.fossil.pt4;
import com.fossil.qg6;
import com.fossil.qs4;
import com.fossil.rm6;
import com.fossil.rr4;
import com.fossil.sf6;
import com.fossil.sx5;
import com.fossil.tj4;
import com.fossil.ui4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.z24;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginPresenter extends ip5 {
    @DexIgnore
    public static /* final */ String M;
    @DexIgnore
    public static /* final */ a N; // = new a((qg6) null);
    @DexIgnore
    public CheckAuthenticationSocialExisting A;
    @DexIgnore
    public AnalyticsHelper B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public FetchDailyGoalTrackingSummaries F;
    @DexIgnore
    public qs4 G;
    @DexIgnore
    public GetSecretKeyUseCase H;
    @DexIgnore
    public WatchLocalizationRepository I;
    @DexIgnore
    public AlarmsRepository J;
    @DexIgnore
    public /* final */ jp5 K;
    @DexIgnore
    public /* final */ BaseActivity L;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public LoginEmailUseCase g;
    @DexIgnore
    public LoginSocialUseCase h;
    @DexIgnore
    public DownloadUserInfoUseCase i;
    @DexIgnore
    public rr4 j;
    @DexIgnore
    public cj4 k;
    @DexIgnore
    public UserRepository l;
    @DexIgnore
    public DeviceRepository m;
    @DexIgnore
    public an4 n;
    @DexIgnore
    public FetchActivities o;
    @DexIgnore
    public FetchSummaries p;
    @DexIgnore
    public z24 q;
    @DexIgnore
    public FetchSleepSessions r;
    @DexIgnore
    public FetchSleepSummaries s;
    @DexIgnore
    public FetchHeartRateSamples t;
    @DexIgnore
    public FetchDailyHeartRateSummaries u;
    @DexIgnore
    public lt4 v;
    @DexIgnore
    public kn4 w;
    @DexIgnore
    public mt4 x;
    @DexIgnore
    public pt4 y;
    @DexIgnore
    public ot4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LoginPresenter.M;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$checkOnboarding$1", f = "LoginPresenter.kt", l = {513}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(LoginPresenter loginPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = loginPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                np5$b$a np5_b_a = new np5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, np5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "checkOnboarding currentUser=" + mFUser);
            if (mFUser != null) {
                this.this$0.K.h();
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<hw5.d, hw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public c(LoginPresenter loginPresenter, SignUpSocialAuth signUpSocialAuth) {
            this.a = loginPresenter;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(CheckAuthenticationSocialExisting.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.o();
            boolean a2 = dVar.a();
            if (a2) {
                this.a.c(this.b);
            } else if (!a2) {
                zm4.p.a().o();
                this.a.K.c(this.b);
            }
        }

        @DexIgnore
        public void a(CheckAuthenticationSocialExisting.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.K.i();
            this.a.b(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1", f = "LoginPresenter.kt", l = {449, 458}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(LoginPresenter loginPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = loginPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
        public final Object invokeSuspend(Object obj) {
            List list;
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 b = this.this$0.c();
                np5$d$a np5_d_a = new np5$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, np5_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                list = (List) obj;
                if (list == null) {
                    list = new ArrayList();
                }
                PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(list));
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dl6 b2 = this.this$0.c();
            np5$d$b np5_d_b = new np5$d$b(this, (xe6) null);
            this.L$0 = il6;
            this.label = 2;
            obj = gk6.a(b2, np5_d_b, this);
            if (obj == a) {
                return a;
            }
            list = (List) obj;
            if (list == null) {
            }
            PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(list));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<nt4.d, nt4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public e(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginSocialUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            PortfolioApp.get.instance().g().a(this.a);
            this.a.B();
        }

        @DexIgnore
        public void a(LoginSocialUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.K.i();
            this.a.a(bVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements m24.e<kt4.d, kt4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public f(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginEmailUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(LoginPresenter.N.a(), "Inside .loginEmail success ");
            PortfolioApp.get.instance().g().a(this.a);
            this.a.o();
            this.a.B();
        }

        @DexIgnore
        public void a(LoginEmailUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginEmail failed with error=" + bVar.a());
            this.a.K.i();
            this.a.b(bVar.a(), bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements m24.e<lt4.d, lt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public g(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(lt4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(lt4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.a.K.i();
            if (2 != cVar.a()) {
                this.a.b(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements m24.e<mt4.d, mt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public h(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mt4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(mt4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.a.K.i();
            if (2 != cVar.a()) {
                this.a.b(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements m24.e<ot4.d, ot4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public i(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ot4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(ot4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.e(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.a.K.i();
            this.a.b(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements m24.e<pt4.d, pt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public j(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pt4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(pt4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.a.K.i();
            this.a.b(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements m24.e<it4.d, it4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        public k(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DownloadUserInfoUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new np5$k$a(this, dVar.a(), (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(DownloadUserInfoUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.N.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + bVar.a());
            this.a.y().clearAllUser();
            this.a.K.i();
            this.a.a(bVar.a(), bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements m24.e<rr4.e, rr4.d> {
        @DexIgnore
        public void a(rr4.d dVar) {
            wg6.b(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rr4.e eVar) {
            wg6.b(eVar, "responseValue");
        }
    }

    /*
    static {
        String simpleName = LoginPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "LoginPresenter::class.java.simpleName");
        M = simpleName;
    }
    */

    @DexIgnore
    public LoginPresenter(jp5 jp5, BaseActivity baseActivity) {
        wg6.b(jp5, "mView");
        wg6.b(baseActivity, "mContext");
        this.K = jp5;
        this.L = baseActivity;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean A() {
        String str = this.e;
        if (str == null || str.length() == 0) {
            this.K.a(false, "");
            return false;
        } else if (!sx5.a(this.e)) {
            jp5 jp5 = this.K;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886766);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            jp5.a(true, a2);
            return false;
        } else {
            this.K.a(false, "");
            return true;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.login.LoginPresenter$k] */
    public final void B() {
        FLogger.INSTANCE.getLocal().d(M, "onLoginSuccess download user info");
        Object r0 = this.i;
        if (r0 != 0) {
            r0.a(new DownloadUserInfoUseCase.c(), new k(this));
        } else {
            wg6.d("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void C() {
        this.K.a(this);
    }

    @DexIgnore
    public final void D() {
        Locale locale = Locale.getDefault();
        wg6.a((Object) locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            wg6.a((Object) locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                wg6.a((Object) locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append("_");
                Locale locale4 = Locale.getDefault();
                wg6.a((Object) locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = M;
                local.d(str, "language: " + sb2);
                if (xj6.b(sb2, "zh_CN", true) || xj6.b(sb2, "zh_SG", true) || xj6.b(sb2, "zh_TW", true)) {
                    this.K.I(true);
                    return;
                } else {
                    this.K.I(false);
                    return;
                }
            }
        }
        this.K.I(false);
    }

    @DexIgnore
    public final void E() {
        if (!A() || TextUtils.isEmpty(this.f)) {
            this.K.e0();
        } else {
            this.K.y0();
        }
    }

    @DexIgnore
    public void f() {
        E();
        D();
    }

    @DexIgnore
    public void h() {
        if (PortfolioApp.get.instance().y()) {
            this.K.H();
        } else {
            a(601, "");
        }
    }

    @DexIgnore
    public void i() {
        jp5 jp5 = this.K;
        String str = this.e;
        if (str == null) {
            str = "";
        }
        jp5.L(str);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.lt4] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.login.LoginPresenter$g, com.portfolio.platform.CoroutineUseCase$e] */
    public void j() {
        this.K.k();
        Object r0 = this.v;
        if (r0 != 0) {
            r0.a(new lt4.b(new WeakReference(this.L)), new g(this));
        } else {
            wg6.d("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.mt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.login.LoginPresenter$h, com.portfolio.platform.CoroutineUseCase$e] */
    public void k() {
        this.K.k();
        Object r0 = this.x;
        if (r0 != 0) {
            r0.a(new mt4.b(new WeakReference(this.L)), new h(this));
        } else {
            wg6.d("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.fossil.ot4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.login.LoginPresenter$i] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity] */
    public void l() {
        if (!tj4.f.a(this.L, "com.tencent.mm")) {
            tj4.f.b(this.L, "com.tencent.mm");
            return;
        }
        this.K.k();
        Object r0 = this.z;
        if (r0 != 0) {
            r0.a(new ot4.b(new WeakReference(this.L)), new i(this));
        } else {
            wg6.d("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.pt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.login.LoginPresenter$j, com.portfolio.platform.CoroutineUseCase$e] */
    public void m() {
        this.K.k();
        Object r0 = this.y;
        if (r0 != 0) {
            r0.a(new pt4.b(new WeakReference(this.L)), new j(this));
        } else {
            wg6.d("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries] */
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions] */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v7, types: [com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v8, types: [com.fossil.qs4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries] */
    public final void o() {
        FLogger.INSTANCE.getLocal().d(M, "downloadOptionalsResources");
        Date date = new Date();
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
        Object r1 = this.p;
        if (r1 != 0) {
            r1.a(new FetchSummaries.b(date), (CoroutineUseCase.e) null);
            Object r12 = this.o;
            if (r12 != 0) {
                r12.a(new FetchActivities.b(date), (CoroutineUseCase.e) null);
                Object r13 = this.r;
                if (r13 != 0) {
                    r13.a(new FetchSleepSessions.b(date), (CoroutineUseCase.e) null);
                    Object r14 = this.s;
                    if (r14 != 0) {
                        r14.a(new FetchSleepSummaries.b(date), (CoroutineUseCase.e) null);
                        Object r15 = this.t;
                        if (r15 != 0) {
                            r15.a(new FetchHeartRateSamples.b(date), (CoroutineUseCase.e) null);
                            Object r16 = this.u;
                            if (r16 != 0) {
                                r16.a(new FetchDailyHeartRateSummaries.b(date), (CoroutineUseCase.e) null);
                                Object r17 = this.G;
                                if (r17 != 0) {
                                    r17.a(new qs4.b(date), (CoroutineUseCase.e) null);
                                    Object r18 = this.F;
                                    if (r18 != 0) {
                                        r18.a(new FetchDailyGoalTrackingSummaries.b(date), (CoroutineUseCase.e) null);
                                    } else {
                                        wg6.d("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    wg6.d("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                wg6.d("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            wg6.d("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        wg6.d("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    wg6.d("mFetchSleepSessions");
                    throw null;
                }
            } else {
                wg6.d("mFetchActivities");
                throw null;
            }
        } else {
            wg6.d("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final AlarmsRepository p() {
        AlarmsRepository alarmsRepository = this.J;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        wg6.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final AnalyticsHelper q() {
        AnalyticsHelper analyticsHelper = this.B;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wg6.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository r() {
        DeviceRepository deviceRepository = this.m;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wg6.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final cj4 s() {
        cj4 cj4 = this.k;
        if (cj4 != null) {
            return cj4;
        }
        wg6.d("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final GetSecretKeyUseCase t() {
        GetSecretKeyUseCase getSecretKeyUseCase = this.H;
        if (getSecretKeyUseCase != null) {
            return getSecretKeyUseCase;
        }
        wg6.d("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository u() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        wg6.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final an4 v() {
        an4 an4 = this.n;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository w() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        wg6.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository x() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        wg6.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository y() {
        UserRepository userRepository = this.l;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository z() {
        WatchLocalizationRepository watchLocalizationRepository = this.I;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        wg6.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public void a(boolean z2) {
        E();
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "password");
        this.f = str;
        E();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.ui.user.usecase.LoginSocialUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.uirenew.login.LoginPresenter$e, com.portfolio.platform.CoroutineUseCase$e] */
    public final void c(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        Object r0 = this.h;
        if (r0 != 0) {
            r0.a(new LoginSocialUseCase.c(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new e(this));
        } else {
            wg6.d("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "email");
        this.e = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.user.usecase.LoginEmailUseCase] */
    /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.uirenew.login.LoginPresenter$f, com.portfolio.platform.CoroutineUseCase$e] */
    public void a(String str, String str2) {
        wg6.b(str, "email");
        wg6.b(str2, "password");
        if (A()) {
            this.K.k();
            Object r0 = this.g;
            if (r0 != 0) {
                r0.a(new LoginEmailUseCase.c(str, str2), new f(this));
            } else {
                wg6.d("mLoginEmailUseCase");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.usecase.CheckAuthenticationSocialExisting, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.uirenew.login.LoginPresenter$c, com.portfolio.platform.CoroutineUseCase$e] */
    public final void b(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = SignUpPresenter.Q.a();
        local.d(a2, "checkSocialAccountIsExisted " + signUpSocialAuth);
        Object r0 = this.A;
        if (r0 != 0) {
            r0.a(new CheckAuthenticationSocialExisting.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new c(this, signUpSocialAuth));
        } else {
            wg6.d("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.rr4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.login.LoginPresenter$l] */
    public final void c(String str) {
        wg6.b(str, "activeSerial");
        Object r0 = this.j;
        if (r0 != 0) {
            r0.a(new rr4.c(str), new l());
        } else {
            wg6.d("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b(int i2, String str) {
        wg6.b(str, "errorMessage");
        if ((400 <= i2 && 407 >= i2) || (407 <= i2 && 499 >= i2)) {
            this.K.p0();
        } else if (i2 != 408) {
            this.K.b(i2, str);
        } else if (!hx5.b(PortfolioApp.get.instance())) {
            this.K.b(601, "");
        } else {
            this.K.b(i2, "");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(int i2, String str) {
        wg6.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = SignUpPresenter.Q.a();
        local.d(a2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.K.b(i2, str);
        } else if (!hx5.b(PortfolioApp.get.instance())) {
            this.K.b(601, "");
        } else {
            this.K.b(i2, "");
        }
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        this.K.k();
        b(signUpSocialAuth);
    }
}
