package com.portfolio.platform.uirenew.signup;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.dl6;
import com.fossil.dt4;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.gw5;
import com.fossil.hw5;
import com.fossil.hx5;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.it4;
import com.fossil.jm4;
import com.fossil.kn4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.lt4;
import com.fossil.m24;
import com.fossil.mt4;
import com.fossil.nc6;
import com.fossil.nt4;
import com.fossil.ot4;
import com.fossil.pt4;
import com.fossil.pt5;
import com.fossil.qg6;
import com.fossil.qs4;
import com.fossil.qt5;
import com.fossil.rm6;
import com.fossil.rr4;
import com.fossil.sf6;
import com.fossil.sw5;
import com.fossil.sx5;
import com.fossil.tj4;
import com.fossil.ui4;
import com.fossil.ut5$c$a;
import com.fossil.ut5$e$a;
import com.fossil.ut5$e$b;
import com.fossil.ut5$g$a;
import com.fossil.ut5$j$a;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.z24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.RequestEmailOtp;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpPresenter extends pt5 {
    @DexIgnore
    public static /* final */ String O;
    @DexIgnore
    public static /* final */ Pattern P;
    @DexIgnore
    public static /* final */ a Q; // = new a((qg6) null);
    @DexIgnore
    public AnalyticsHelper A;
    @DexIgnore
    public dt4 B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public FetchDailyGoalTrackingSummaries F;
    @DexIgnore
    public qs4 G;
    @DexIgnore
    public RequestEmailOtp H;
    @DexIgnore
    public GetSecretKeyUseCase I;
    @DexIgnore
    public WatchLocalizationRepository J;
    @DexIgnore
    public String K;
    @DexIgnore
    public String L;
    @DexIgnore
    public /* final */ qt5 M;
    @DexIgnore
    public /* final */ BaseActivity N;
    @DexIgnore
    public lt4 e;
    @DexIgnore
    public kn4 f;
    @DexIgnore
    public mt4 g;
    @DexIgnore
    public pt4 h;
    @DexIgnore
    public ot4 i;
    @DexIgnore
    public LoginSocialUseCase j;
    @DexIgnore
    public UserRepository k;
    @DexIgnore
    public DeviceRepository l;
    @DexIgnore
    public z24 m;
    @DexIgnore
    public FetchSleepSessions n;
    @DexIgnore
    public FetchSleepSummaries o;
    @DexIgnore
    public FetchActivities p;
    @DexIgnore
    public FetchSummaries q;
    @DexIgnore
    public FetchHeartRateSamples r;
    @DexIgnore
    public FetchDailyHeartRateSummaries s;
    @DexIgnore
    public AlarmsRepository t;
    @DexIgnore
    public rr4 u;
    @DexIgnore
    public cj4 v;
    @DexIgnore
    public DownloadUserInfoUseCase w;
    @DexIgnore
    public an4 x;
    @DexIgnore
    public CheckAuthenticationEmailExisting y;
    @DexIgnore
    public CheckAuthenticationSocialExisting z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return SignUpPresenter.O;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<dt4.a, m24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public b(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(dt4.a aVar) {
            wg6.b(aVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Get current user success: " + aVar.a());
            MFUser a3 = aVar.a();
            if (a3 != null) {
                this.a.q().b(a3.getUserId());
            }
        }

        @DexIgnore
        public void a(CoroutineUseCase.a aVar) {
            wg6.b(aVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(SignUpPresenter.Q.a(), "Get current user failed");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1", f = "SignUpPresenter.kt", l = {547}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(SignUpPresenter signUpPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = signUpPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                ut5$c$a ut5_c_a = new ut5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, ut5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = SignUpPresenter.Q.a();
            local.d(a3, "checkOnboarding currentUser=" + mFUser);
            if (mFUser != null) {
                this.this$0.M.h();
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<hw5.d, hw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public d(SignUpPresenter signUpPresenter, SignUpSocialAuth signUpSocialAuth) {
            this.a = signUpPresenter;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(CheckAuthenticationSocialExisting.d dVar) {
            wg6.b(dVar, "responseValue");
            boolean a2 = dVar.a();
            if (a2) {
                this.a.c(this.b);
            } else if (!a2) {
                this.a.M.b(this.b);
            }
        }

        @DexIgnore
        public void a(CheckAuthenticationSocialExisting.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.M.i();
            this.a.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1", f = "SignUpPresenter.kt", l = {500, 510}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(SignUpPresenter signUpPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = signUpPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
        public final Object invokeSuspend(Object obj) {
            List list;
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                dl6 b = this.this$0.c();
                ut5$e$a ut5_e_a = new ut5$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, ut5_e_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                list = (List) obj;
                if (list == null) {
                    list = new ArrayList();
                }
                PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(list));
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dl6 a2 = this.this$0.b();
            ut5$e$b ut5_e_b = new ut5$e$b(this, (xe6) null);
            this.L$0 = il6;
            this.label = 2;
            obj = gk6.a(a2, ut5_e_b, this);
            if (obj == a) {
                return a;
            }
            list = (List) obj;
            if (list == null) {
            }
            PortfolioApp.get.instance().a((List<? extends Alarm>) ui4.a(list));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements m24.e<nt4.d, nt4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public f(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginSocialUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            PortfolioApp.get.instance().g().a(this.a);
            this.a.C();
        }

        @DexIgnore
        public void a(LoginSocialUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.M.i();
            this.a.a(bVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements m24.e<it4.d, it4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public g(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DownloadUserInfoUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new ut5$g$a(this, dVar.a(), (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public void a(DownloadUserInfoUseCase.b bVar) {
            wg6.b(bVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + bVar.a());
            this.a.y().clearAllUser();
            this.a.M.i();
            this.a.a(bVar.a(), bVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements m24.e<rr4.e, rr4.d> {
        @DexIgnore
        public void a(rr4.d dVar) {
            wg6.b(dVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rr4.e eVar) {
            wg6.b(eVar, "responseValue");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements m24.e<sw5.d, sw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth b;

        @DexIgnore
        public i(SignUpPresenter signUpPresenter, SignUpEmailAuth signUpEmailAuth) {
            this.a = signUpPresenter;
            this.b = signUpEmailAuth;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(RequestEmailOtp.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.M.i();
            this.a.M.b(this.b);
        }

        @DexIgnore
        public void a(RequestEmailOtp.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.M.i();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "requestOtpCode " + "errorCode=" + cVar.a() + " message=" + cVar.b());
            this.a.M.f(cVar.a(), cVar.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements m24.e<gw5.d, gw5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public j(SignUpPresenter signUpPresenter, String str, String str2) {
            this.a = signUpPresenter;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* renamed from: a */
        public void onSuccess(CheckAuthenticationEmailExisting.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.o();
            boolean a2 = dVar.a();
            if (a2) {
                this.a.M.i();
                qt5 c2 = this.a.M;
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886764);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026_ThisEmailIsAlreadyInUse)");
                c2.P(a3);
                this.a.m();
            } else if (!a2) {
                SignUpEmailAuth signUpEmailAuth = new SignUpEmailAuth();
                signUpEmailAuth.setEmail(this.b);
                signUpEmailAuth.setPassword(this.c);
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new ut5$j$a(this, (xe6) null), 3, (Object) null);
                this.a.a(signUpEmailAuth);
            }
        }

        @DexIgnore
        public void a(CheckAuthenticationEmailExisting.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.M.i();
            this.a.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements m24.e<lt4.d, lt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public k(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(lt4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginFacebook success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(lt4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + cVar.a());
            this.a.M.i();
            if (2 != cVar.a()) {
                this.a.a(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements m24.e<mt4.d, mt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public l(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mt4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginGoogle success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(mt4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + cVar.a());
            this.a.M.i();
            if (2 != cVar.a()) {
                this.a.a(cVar.a(), "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements m24.e<ot4.d, ot4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public m(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ot4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginWechat success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(ot4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginWechat failed with error=" + cVar.a());
            this.a.M.i();
            this.a.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements m24.e<pt4.d, pt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        public n(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pt4.d dVar) {
            wg6.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginWeibo success with result=" + dVar.a());
            this.a.b(dVar.a());
        }

        @DexIgnore
        public void a(pt4.c cVar) {
            wg6.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + cVar.a());
            this.a.M.i();
            this.a.a(cVar.a(), "");
        }
    }

    /*
    static {
        String simpleName = SignUpPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "SignUpPresenter::class.java.simpleName");
        O = simpleName;
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            P = compile;
        } else {
            wg6.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public SignUpPresenter(qt5 qt5, BaseActivity baseActivity) {
        wg6.b(qt5, "mView");
        wg6.b(baseActivity, "mContext");
        this.M = qt5;
        this.N = baseActivity;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean A() {
        String str = this.K;
        if (str == null || str.length() == 0) {
            this.M.a(false, false, "");
            return false;
        } else if (!sx5.a(this.K)) {
            qt5 qt5 = this.M;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886766);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            qt5.a(false, true, a2);
            return false;
        } else {
            this.M.a(true, false, "");
            return true;
        }
    }

    @DexIgnore
    public final boolean B() {
        if (!TextUtils.isEmpty(this.L)) {
            String str = this.L;
            if (str != null) {
                boolean z2 = str.length() >= 7;
                boolean matches = P.matcher(this.L).matches();
                this.M.d(z2, matches);
                if (!z2 || !matches) {
                    return false;
                }
                return true;
            }
            wg6.a();
            throw null;
        }
        this.M.d(false, false);
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.signup.SignUpPresenter$g, com.portfolio.platform.CoroutineUseCase$e] */
    public final void C() {
        FLogger.INSTANCE.getLocal().d(O, "onLoginSuccess download user info");
        Object r0 = this.w;
        if (r0 != 0) {
            r0.a(new DownloadUserInfoUseCase.c(), new g(this));
        } else {
            wg6.d("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void D() {
        this.M.a(this);
    }

    @DexIgnore
    public final void E() {
        Locale locale = Locale.getDefault();
        wg6.a((Object) locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            wg6.a((Object) locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                wg6.a((Object) locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append("_");
                Locale locale4 = Locale.getDefault();
                wg6.a((Object) locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = O;
                local.d(str, "language: " + sb2);
                if (xj6.b(sb2, "zh_CN", true) || xj6.b(sb2, "zh_SG", true) || xj6.b(sb2, "zh_TW", true)) {
                    this.M.C(true);
                    return;
                } else {
                    this.M.C(false);
                    return;
                }
            }
        }
        this.M.C(false);
    }

    @DexIgnore
    public final void F() {
        boolean z2 = TextUtils.isEmpty(this.K) || TextUtils.isEmpty(this.L);
        boolean A2 = A();
        boolean B2 = B();
        if (z2 || !B2 || !A2) {
            this.M.K0();
        } else {
            this.M.r0();
        }
    }

    @DexIgnore
    public void f() {
        this.M.g();
        F();
        E();
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        if (PortfolioApp.get.instance().y()) {
            this.M.H();
        } else {
            a(601, "");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.lt4] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.signup.SignUpPresenter$k] */
    public void i() {
        this.M.k();
        Object r0 = this.e;
        if (r0 != 0) {
            r0.a(new lt4.b(new WeakReference(this.N)), new k(this));
        } else {
            wg6.d("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.mt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.signup.SignUpPresenter$l, com.portfolio.platform.CoroutineUseCase$e] */
    public void j() {
        this.M.k();
        Object r0 = this.g;
        if (r0 != 0) {
            r0.a(new mt4.b(new WeakReference(this.N)), new l(this));
        } else {
            wg6.d("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.fossil.ot4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.signup.SignUpPresenter$m] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.ui.BaseActivity] */
    public void k() {
        if (!tj4.f.a(this.N, "com.tencent.mm")) {
            tj4.f.b(this.N, "com.tencent.mm");
            return;
        }
        this.M.k();
        Object r0 = this.i;
        if (r0 != 0) {
            r0.a(new ot4.b(new WeakReference(this.N)), new m(this));
        } else {
            wg6.d("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.pt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.signup.SignUpPresenter$n] */
    public void l() {
        this.M.k();
        Object r0 = this.h;
        if (r0 != 0) {
            r0.a(new pt4.b(new WeakReference(this.N)), new n(this));
        } else {
            wg6.d("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.fossil.dt4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.signup.SignUpPresenter$b] */
    public final void m() {
        Object r0 = this.B;
        if (r0 != 0) {
            r0.a(null, new b(this));
        } else {
            wg6.d("mGetUser");
            throw null;
        }
    }

    @DexIgnore
    public final rm6 n() {
        return ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries] */
    /* JADX WARNING: type inference failed for: r1v7, types: [com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v8, types: [com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v9, types: [com.fossil.qs4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries] */
    public final void o() {
        FLogger.INSTANCE.getLocal().d(LoginPresenter.N.a(), "downloadOptionalsResources");
        Date date = new Date();
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
        Object r1 = this.q;
        if (r1 != 0) {
            r1.a(new FetchSummaries.b(date), (CoroutineUseCase.e) null);
            Object r12 = this.p;
            if (r12 != 0) {
                r12.a(new FetchActivities.b(date), (CoroutineUseCase.e) null);
                Object r13 = this.n;
                if (r13 != 0) {
                    r13.a(new FetchSleepSessions.b(date), (CoroutineUseCase.e) null);
                    Object r14 = this.o;
                    if (r14 != 0) {
                        r14.a(new FetchSleepSummaries.b(date), (CoroutineUseCase.e) null);
                        Object r15 = this.r;
                        if (r15 != 0) {
                            r15.a(new FetchHeartRateSamples.b(date), (CoroutineUseCase.e) null);
                            Object r16 = this.s;
                            if (r16 != 0) {
                                r16.a(new FetchDailyHeartRateSummaries.b(date), (CoroutineUseCase.e) null);
                                Object r17 = this.G;
                                if (r17 != 0) {
                                    r17.a(new qs4.b(date), (CoroutineUseCase.e) null);
                                    Object r18 = this.F;
                                    if (r18 != 0) {
                                        r18.a(new FetchDailyGoalTrackingSummaries.b(date), (CoroutineUseCase.e) null);
                                    } else {
                                        wg6.d("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    wg6.d("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                wg6.d("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            wg6.d("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        wg6.d("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    wg6.d("mFetchSleepSessions");
                    throw null;
                }
            } else {
                wg6.d("mFetchActivities");
                throw null;
            }
        } else {
            wg6.d("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final AlarmsRepository p() {
        AlarmsRepository alarmsRepository = this.t;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        wg6.d("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final AnalyticsHelper q() {
        AnalyticsHelper analyticsHelper = this.A;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        wg6.d("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository r() {
        DeviceRepository deviceRepository = this.l;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wg6.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final cj4 s() {
        cj4 cj4 = this.v;
        if (cj4 != null) {
            return cj4;
        }
        wg6.d("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final GetSecretKeyUseCase t() {
        GetSecretKeyUseCase getSecretKeyUseCase = this.I;
        if (getSecretKeyUseCase != null) {
            return getSecretKeyUseCase;
        }
        wg6.d("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository u() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        wg6.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final an4 v() {
        an4 an4 = this.x;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository w() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        wg6.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository x() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        wg6.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository y() {
        UserRepository userRepository = this.k;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository z() {
        WatchLocalizationRepository watchLocalizationRepository = this.J;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        wg6.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public void a(boolean z2) {
        F();
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, "password");
        this.L = str;
        F();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.ui.user.usecase.LoginSocialUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.uirenew.signup.SignUpPresenter$f, com.portfolio.platform.CoroutineUseCase$e] */
    public final void c(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        Object r0 = this.j;
        if (r0 != 0) {
            r0.a(new LoginSocialUseCase.c(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new f(this));
        } else {
            wg6.d("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "email");
        this.K = str;
        A();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.usecase.CheckAuthenticationSocialExisting, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.signup.SignUpPresenter$d] */
    public final void b(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = O;
        local.d(str, "checkSocialAccountIsExisted " + signUpSocialAuth);
        Object r0 = this.z;
        if (r0 != 0) {
            r0.a(new CheckAuthenticationSocialExisting.b(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new d(this, signUpSocialAuth));
        } else {
            wg6.d("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.usecase.CheckAuthenticationEmailExisting, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.uirenew.signup.SignUpPresenter$j, com.portfolio.platform.CoroutineUseCase$e] */
    public void a(String str, String str2) {
        wg6.b(str, "email");
        wg6.b(str2, "password");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = O;
        local.d(str3, "signupEmail " + str + ' ' + str2);
        if (A()) {
            this.M.k();
            Object r0 = this.y;
            if (r0 != 0) {
                r0.a(new CheckAuthenticationEmailExisting.b(str), new j(this, str, str2));
            } else {
                wg6.d("mCheckAuthenticationEmailExisting");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.rr4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.uirenew.signup.SignUpPresenter$h, com.portfolio.platform.CoroutineUseCase$e] */
    public final void c(String str) {
        wg6.b(str, "activeSerial");
        Object r0 = this.u;
        if (r0 != 0) {
            r0.a(new rr4.c(str), new h());
        } else {
            wg6.d("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void a(SignUpSocialAuth signUpSocialAuth) {
        wg6.b(signUpSocialAuth, "auth");
        this.M.k();
        b(signUpSocialAuth);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(int i2, String str) {
        wg6.b(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = O;
        local.d(str2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.M.b(i2, str);
        } else if (!hx5.b(PortfolioApp.get.instance())) {
            this.M.b(601, "");
        } else {
            this.M.b(i2, "");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.usecase.RequestEmailOtp, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.uirenew.signup.SignUpPresenter$i, com.portfolio.platform.CoroutineUseCase$e] */
    public final void a(SignUpEmailAuth signUpEmailAuth) {
        wg6.b(signUpEmailAuth, "emailAuth");
        Object r0 = this.H;
        if (r0 != 0) {
            r0.a(new RequestEmailOtp.b(signUpEmailAuth.getEmail()), new i(this, signUpEmailAuth));
        } else {
            wg6.d("mRequestEmailOtp");
            throw null;
        }
    }
}
