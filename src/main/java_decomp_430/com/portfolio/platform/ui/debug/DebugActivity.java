package com.portfolio.platform.ui.debug;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.cn6;
import com.fossil.cp4;
import com.fossil.cr4;
import com.fossil.dl6;
import com.fossil.e90;
import com.fossil.er4;
import com.fossil.ff6;
import com.fossil.fr4;
import com.fossil.fs4;
import com.fossil.gk6;
import com.fossil.gr4;
import com.fossil.hg6;
import com.fossil.hr4;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ir4;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.l0;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mi4;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.oh6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.ul6;
import com.fossil.vd;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.customrequest.ForceBackgroundRequest;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.DebugForceBackgroundRequestData;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.viewmodel.FirmwareDebugViewModel;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DebugActivity extends BaseActivity implements cr4.b {
    @DexIgnore
    public static /* final */ a P; // = new a((qg6) null);
    @DexIgnore
    public an4 B;
    @DexIgnore
    public fs4 C;
    @DexIgnore
    public FirmwareFileRepository D;
    @DexIgnore
    public GuestApiService E;
    @DexIgnore
    public DianaPresetRepository F;
    @DexIgnore
    public ShakeFeedbackService G;
    @DexIgnore
    public cj4 H;
    @DexIgnore
    public /* final */ cr4 I; // = new cr4();
    @DexIgnore
    public /* final */ ArrayList<ir4> J; // = new ArrayList<>();
    @DexIgnore
    public FirmwareDebugViewModel K;
    @DexIgnore
    public String L; // = "";
    @DexIgnore
    public HeartRateMode M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public /* final */ e O; // = new e(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            context.startActivity(new Intent(context, DebugActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements InputFilter {
        @DexIgnore
        public double a;
        @DexIgnore
        public double b;

        @DexIgnore
        public b(DebugActivity debugActivity, double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0018 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        public final boolean a(double d, double d2, double d3) {
            if (d2 > d) {
                return d3 >= d && d3 <= d2;
            }
            if (d3 >= d2 && d3 <= d) {
                return true;
            }
        }

        @DexIgnore
        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            wg6.b(charSequence, "source");
            wg6.b(spanned, "dest");
            try {
                StringBuilder sb = new StringBuilder();
                String obj = spanned.toString();
                if (obj != null) {
                    String substring = obj.substring(0, i3);
                    wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    sb.append(substring);
                    String obj2 = spanned.toString();
                    int length = spanned.toString().length();
                    if (obj2 != null) {
                        String substring2 = obj2.substring(i4, length);
                        wg6.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                        sb.append(substring2);
                        String sb2 = sb.toString();
                        StringBuilder sb3 = new StringBuilder();
                        if (sb2 != null) {
                            String substring3 = sb2.substring(0, i3);
                            wg6.a((Object) substring3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                            sb3.append(substring3);
                            sb3.append(charSequence.toString());
                            int length2 = sb2.length();
                            if (sb2 != null) {
                                String substring4 = sb2.substring(i3, length2);
                                wg6.a((Object) substring4, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                                sb3.append(substring4);
                                if (a(this.a, this.b, Double.parseDouble(sb3.toString()))) {
                                    return null;
                                }
                                return "";
                            }
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.debug.DebugActivity", f = "DebugActivity.kt", l = {268}, m = "loadFirmware")
    public static final class c extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DebugActivity debugActivity, xe6 xe6) {
            super(xe6);
            this.this$0 = debugActivity;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, (xe6<? super List<DebugFirmwareData>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$loadFirmware$repoResponse$1", f = "DebugActivity.kt", l = {268}, m = "invokeSuspend")
    public static final class d extends sf6 implements hg6<xe6<? super rx6<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DebugActivity debugActivity, String str, xe6 xe6) {
            super(1, xe6);
            this.this$0 = debugActivity;
            this.$model = str;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new d(this.this$0, this.$model, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((d) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                GuestApiService y = this.this$0.y();
                String h = PortfolioApp.get.instance().h();
                String str = this.$model;
                this.label = 1;
                obj = y.getFirmwares(h, str, "android", this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements BleCommandResultManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public e(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            T t;
            ArrayList<er4> a2;
            T t2;
            T t3;
            ArrayList<er4> a3;
            T t4;
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            er4 er4 = null;
            if (communicateMode == CommunicateMode.SET_HEART_RATE_MODE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.C().a(this.a.z());
                    Iterator<T> it = this.a.u().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t3 = null;
                            break;
                        }
                        t3 = it.next();
                        if (wg6.a((Object) ((ir4) t3).b(), (Object) "OTHER")) {
                            break;
                        }
                    }
                    ir4 ir4 = (ir4) t3;
                    if (!(ir4 == null || (a3 = ir4.a()) == null)) {
                        Iterator<T> it2 = a3.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t4 = null;
                                break;
                            }
                            t4 = it2.next();
                            if (wg6.a((Object) ((er4) t4).a(), (Object) "SWITCH HEART RATE MODE")) {
                                break;
                            }
                        }
                        er4 = (er4) t4;
                    }
                    hr4 hr4 = (hr4) er4;
                    if (hr4 != null) {
                        hr4.b(this.a.z().name());
                    }
                    this.a.s().notifyDataSetChanged();
                } else {
                    DebugActivity debugActivity = this.a;
                    HeartRateMode d = debugActivity.C().d(PortfolioApp.get.instance().e());
                    wg6.a((Object) d, "mSharedPreferencesManage\u2026tance.activeDeviceSerial)");
                    debugActivity.a(d);
                }
                this.a.h();
            } else if (communicateMode == CommunicateMode.SET_FRONT_LIGHT_ENABLE) {
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    this.a.C().g(this.a.A());
                } else {
                    DebugActivity debugActivity2 = this.a;
                    debugActivity2.d(debugActivity2.C().l(PortfolioApp.get.instance().e()));
                    Iterator<T> it3 = this.a.u().iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it3.next();
                        if (wg6.a((Object) ((ir4) t).b(), (Object) "OTHER")) {
                            break;
                        }
                    }
                    ir4 ir42 = (ir4) t;
                    if (!(ir42 == null || (a2 = ir42.a()) == null)) {
                        Iterator<T> it4 = a2.iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it4.next();
                            if (wg6.a((Object) ((er4) t2).a(), (Object) "FRONT LIGHT ENABLE")) {
                                break;
                            }
                        }
                        er4 = (er4) t2;
                    }
                    gr4 gr4 = (gr4) er4;
                    if (gr4 != null) {
                        gr4.a(this.a.A());
                    }
                    this.a.s().notifyDataSetChanged();
                }
                this.a.h();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$onCreate$4", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends sf6 implements hg6<xe6<? super List<? extends DebugFirmwareData>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(xe6 xe6, f fVar) {
                super(1, xe6);
                this.this$0 = fVar;
            }

            @DexIgnore
            public final xe6<cd6> create(xe6<?> xe6) {
                wg6.b(xe6, "completion");
                return new a(xe6, this.this$0);
            }

            @DexIgnore
            public final Object invoke(Object obj) {
                return ((a) create((xe6) obj)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = ff6.a();
                int i = this.label;
                if (i == 0) {
                    nc6.a(obj);
                    DebugActivity debugActivity = this.this$0.this$0;
                    String a2 = debugActivity.L;
                    this.label = 1;
                    obj = debugActivity.a(a2, (xe6<? super List<DebugFirmwareData>>) this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DebugActivity debugActivity, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = debugActivity;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$activeDeviceSerial, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                Device deviceBySerial = this.this$0.b().getDeviceBySerial(this.$activeDeviceSerial);
                if (deviceBySerial != null) {
                    DebugActivity debugActivity = this.this$0;
                    String sku = deviceBySerial.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    debugActivity.L = sku;
                    Firmware a2 = this.this$0.C().a(this.this$0.L);
                    if (a2 != null) {
                        DebugActivity.b(this.this$0).a(a2);
                    }
                    if (!DebugActivity.b(this.this$0).c()) {
                        DebugActivity.b(this.this$0).a((hg6<? super xe6<? super List<DebugFirmwareData>>, ? extends Object>) new a((xe6) null, this));
                    }
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<Firmware> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public g(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Firmware firmware) {
            er4 er4;
            T t;
            ArrayList<er4> a2;
            T t2;
            if (firmware != null) {
                Iterator<T> it = this.a.u().iterator();
                while (true) {
                    er4 = null;
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((ir4) t).b(), (Object) "OTHER")) {
                        break;
                    }
                }
                ir4 ir4 = (ir4) t;
                if (!(ir4 == null || (a2 = ir4.a()) == null)) {
                    Iterator<T> it2 = a2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        t2 = it2.next();
                        if (wg6.a((Object) ((er4) t2).a(), (Object) "CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                            break;
                        }
                    }
                    er4 = (er4) t2;
                }
                if (er4 != null) {
                    er4.a("CONSIDER AS LATEST BUNDLE FIRMWARE: " + firmware.getVersionNumber());
                }
                this.a.s().notifyDataSetChanged();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements ld<List<? extends DebugFirmwareData>> {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;

        @DexIgnore
        public h(DebugActivity debugActivity) {
            this.a = debugActivity;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DebugFirmwareData> list) {
            T t;
            ArrayList<er4> a2;
            ArrayList<er4> a3;
            if (list != null && (!list.isEmpty())) {
                Iterator<T> it = this.a.u().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((ir4) t).b(), (Object) "FIRMWARE")) {
                        break;
                    }
                }
                ir4 ir4 = (ir4) t;
                if (!(ir4 == null || (a3 = ir4.a()) == null)) {
                    a3.clear();
                }
                for (DebugFirmwareData debugFirmwareData : list) {
                    Firmware firmware = debugFirmwareData.getFirmware();
                    int state = debugFirmwareData.getState();
                    String str = state != 1 ? state != 2 ? "" : "Downloaded" : "Downloading";
                    String versionNumber = firmware.getVersionNumber();
                    wg6.a((Object) versionNumber, "firmware.versionNumber");
                    hr4 hr4 = new hr4("FIRMWARE", versionNumber, str);
                    hr4.a(debugFirmwareData);
                    if (!(ir4 == null || (a2 = ir4.a()) == null)) {
                        a2.add(hr4);
                    }
                }
                this.a.s().notifyDataSetChanged();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1", f = "DebugActivity.kt", l = {302, 303, 304}, m = "invokeSuspend")
    public static final class i extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.ui.debug.DebugActivity$onItemClicked$1$1", f = "DebugActivity.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = iVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    Toast.makeText(this.this$0.this$0, "Log will be sent after 1 second", 1).show();
                    this.this$0.this$0.finish();
                    return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(DebugActivity debugActivity, xe6 xe6) {
            super(2, xe6);
            this.this$0 = debugActivity;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            i iVar = new i(this.this$0, xe6);
            iVar.p$ = (il6) obj;
            return iVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((i) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x006b A[RETURN] */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            cn6 c;
            a aVar;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                ShakeFeedbackService B = this.this$0.B();
                Object r5 = this.this$0;
                this.L$0 = il62;
                this.label = 1;
                if (B.a((Context) r5, (xe6<? super cd6>) this) == a2) {
                    return a2;
                }
                il6 = il62;
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                c = zl6.c();
                aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 3;
                if (gk6.a(c, aVar, this) == a2) {
                    return a2;
                }
                return cd6.a;
            } else if (i == 3) {
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.L$0 = il6;
            this.label = 2;
            if (ul6.a(1000, this) == a2) {
                return a2;
            }
            c = zl6.c();
            aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 3;
            if (gk6.a(c, aVar, this) == a2) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ EditText b;
        @DexIgnore
        public /* final */ /* synthetic */ EditText c;
        @DexIgnore
        public /* final */ /* synthetic */ EditText d;
        @DexIgnore
        public /* final */ /* synthetic */ EditText e;
        @DexIgnore
        public /* final */ /* synthetic */ l0 f;

        @DexIgnore
        public j(DebugActivity debugActivity, EditText editText, EditText editText2, EditText editText3, EditText editText4, l0 l0Var) {
            this.a = debugActivity;
            this.b = editText;
            this.c = editText2;
            this.d = editText3;
            this.e = editText4;
            this.f = l0Var;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v5, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
        public final void onClick(View view) {
            int i;
            int i2;
            int i3;
            int i4;
            EditText editText = this.b;
            wg6.a((Object) editText, "etDelay");
            String obj = editText.getText().toString();
            EditText editText2 = this.c;
            wg6.a((Object) editText2, "etDuration");
            String obj2 = editText2.getText().toString();
            EditText editText3 = this.d;
            wg6.a((Object) editText3, "etRepeat");
            String obj3 = editText3.getText().toString();
            EditText editText4 = this.e;
            wg6.a((Object) editText4, "etDelayEachTime");
            String obj4 = editText4.getText().toString();
            try {
                i = Integer.parseInt(obj);
            } catch (Exception unused) {
                i = 0;
            }
            try {
                i2 = Integer.parseInt(obj2);
            } catch (Exception unused2) {
                i2 = 0;
            }
            try {
                i3 = Integer.parseInt(obj3);
            } catch (Exception unused3) {
                i3 = 0;
            }
            try {
                i4 = Integer.parseInt(obj4);
            } catch (Exception unused4) {
                i4 = 0;
            }
            if (i > 65535 || i2 > 65535 || i3 > 65535 || i4 > 65535) {
                Toast.makeText(this.a, "Value should be in range [0, 65535]", 0).show();
                return;
            }
            PortfolioApp.get.instance().a(i, i2, i3, i4);
            this.f.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ EditText b;

        @DexIgnore
        public k(DebugActivity debugActivity, EditText editText) {
            this.a = debugActivity;
            this.b = editText;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            String obj = this.b.getText().toString();
            if (obj != null) {
                Integer valueOf = Integer.valueOf(yj6.d(obj).toString());
                wg6.a((Object) valueOf, "Integer.valueOf(input.text.toString().trim())");
                int intValue = valueOf.intValue();
                this.a.C().d(intValue);
                for (ir4 ir4 : this.a.u()) {
                    if (wg6.a((Object) ir4.b(), (Object) "SIMULATION")) {
                        for (T next : ir4.a()) {
                            if (wg6.a((Object) ((er4) next).c(), (Object) "TRIGGER LOW BATTERY EVENT")) {
                                if (next != null) {
                                    nh6 nh6 = nh6.a;
                                    Object[] objArr = {Integer.valueOf(intValue)};
                                    String format = String.format("Battery level: %d", Arrays.copyOf(objArr, objArr.length));
                                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                                    ((hr4) next).b(format);
                                    this.a.s().notifyDataSetChanged();
                                    return;
                                }
                                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                            }
                        }
                        throw new NoSuchElementException("Collection contains no element matching the predicate.");
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ l0 a;

        @DexIgnore
        public l(l0 l0Var) {
            this.a = l0Var;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
            if (com.fossil.wg6.a(java.lang.Integer.valueOf(com.fossil.yj6.d(r6).toString()).intValue(), 100) <= 0) goto L_0x006f;
         */
        @DexIgnore
        public void afterTextChanged(Editable editable) {
            wg6.b(editable, "s");
            boolean z = true;
            if (editable.length() == 0) {
                Button b = this.a.b(-1);
                wg6.a((Object) b, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                b.setEnabled(false);
                return;
            }
            Button b2 = this.a.b(-1);
            wg6.a((Object) b2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
            String obj = editable.toString();
            if (obj != null) {
                if (wg6.a(Integer.valueOf(yj6.d(obj).toString()).intValue(), 1) >= 0) {
                    String obj2 = editable.toString();
                    if (obj2 == null) {
                        throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                z = false;
                b2.setEnabled(z);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wg6.b(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Firmware a;
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity b;
        @DexIgnore
        public /* final */ /* synthetic */ DebugFirmwareData c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.ui.debug.DebugActivity$m$a$a")
            /* renamed from: com.portfolio.platform.ui.debug.DebugActivity$m$a$a  reason: collision with other inner class name */
            public static final class C0063a extends sf6 implements ig6<il6, xe6<? super File>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0063a(a aVar, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0063a aVar = new C0063a(this.this$0, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0063a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        FirmwareFileRepository x = this.this$0.this$0.b.x();
                        String versionNumber = this.this$0.this$0.a.getVersionNumber();
                        wg6.a((Object) versionNumber, "firmware.versionNumber");
                        String downloadUrl = this.this$0.this$0.a.getDownloadUrl();
                        wg6.a((Object) downloadUrl, "firmware.downloadUrl");
                        String checksum = this.this$0.this$0.a.getChecksum();
                        wg6.a((Object) checksum, "firmware.checksum");
                        this.L$0 = il6;
                        this.label = 1;
                        obj = x.downloadFirmware(versionNumber, downloadUrl, checksum, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = mVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                DebugFirmwareData debugFirmwareData;
                int i;
                Object a = ff6.a();
                int i2 = this.label;
                if (i2 == 0) {
                    nc6.a(obj);
                    il6 il6 = this.p$;
                    dl6 a2 = zl6.a();
                    C0063a aVar = new C0063a(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    obj = gk6.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (((File) obj) != null) {
                    debugFirmwareData = this.this$0.c;
                    i = 2;
                } else {
                    debugFirmwareData = this.this$0.c;
                    i = 0;
                }
                debugFirmwareData.setState(i);
                DebugActivity.b(this.this$0.b).d();
                return cd6.a;
            }
        }

        @DexIgnore
        public m(Firmware firmware, DebugActivity debugActivity, DebugFirmwareData debugFirmwareData) {
            this.a = firmware;
            this.b = debugActivity;
            this.c = debugFirmwareData;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
            this.c.setState(1);
            DebugActivity.b(this.b).d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ n a; // = new n();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware b;

        @DexIgnore
        public o(DebugActivity debugActivity, Firmware firmware) {
            this.a = debugActivity;
            this.b = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.a.a(this.b);
            dialogInterface.dismiss();
            this.a.finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ p a; // = new p();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements DialogInterface.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ DebugActivity a;
        @DexIgnore
        public /* final */ /* synthetic */ Firmware b;

        @DexIgnore
        public q(DebugActivity debugActivity, Firmware firmware) {
            this.a = debugActivity;
            this.b = firmware;
        }

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            this.a.C().a(this.b, this.a.L);
            DebugActivity.b(this.a).a(this.b);
            dialogInterface.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements DialogInterface.OnClickListener {
        @DexIgnore
        public static /* final */ r a; // = new r();

        @DexIgnore
        public final void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static final /* synthetic */ FirmwareDebugViewModel b(DebugActivity debugActivity) {
        FirmwareDebugViewModel firmwareDebugViewModel = debugActivity.K;
        if (firmwareDebugViewModel != null) {
            return firmwareDebugViewModel;
        }
        wg6.d("mFirmwareViewModel");
        throw null;
    }

    @DexIgnore
    public final boolean A() {
        return this.N;
    }

    @DexIgnore
    public final ShakeFeedbackService B() {
        ShakeFeedbackService shakeFeedbackService = this.G;
        if (shakeFeedbackService != null) {
            return shakeFeedbackService;
        }
        wg6.d("mShakeFeedbackService");
        throw null;
    }

    @DexIgnore
    public final an4 C() {
        an4 an4 = this.B;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
    public final void c(Firmware firmware) {
        l0.a aVar = new l0.a(this);
        aVar.b("Confirm Latest Firmware");
        aVar.a("Are you sure you want to use firmware " + firmware.getVersionNumber() + " as the bundle latest firmware?");
        aVar.b("Confirm", new q(this, firmware));
        aVar.a("Cancel", r.a);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    public final void d(boolean z) {
        this.N = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r28v0, types: [com.portfolio.platform.ui.debug.DebugActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558430);
        PortfolioApp.get.instance().g().a(this);
        String e2 = PortfolioApp.get.instance().e();
        an4 an4 = this.B;
        if (an4 != null) {
            HeartRateMode d2 = an4.d(e2);
            wg6.a((Object) d2, "mSharedPreferencesManage\u2026eMode(activeDeviceSerial)");
            this.M = d2;
            an4 an42 = this.B;
            if (an42 != null) {
                this.N = an42.l(e2);
                ArrayList arrayList = new ArrayList();
                arrayList.add(new er4("VIEW LOG", "VIEW LOG"));
                arrayList.add(new er4("SEND LOG", "SEND LOG"));
                arrayList.add(new er4("RESET UAPP LOG FILES", "RESET UAPP LOG FILES"));
                this.J.add(new ir4("LOG", "LOG", arrayList, false, 8, (qg6) null));
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(new er4("CLEAR DATA", "CLEAR DATA"));
                arrayList2.add(new er4("GENERATE PRESET DATA", "GENERATE PRESET DATA"));
                this.J.add(new ir4("DATA", "DATA", arrayList2, false, 8, (qg6) null));
                ArrayList arrayList3 = new ArrayList();
                arrayList3.add(new er4("BLUETOOTH SETTING", "BLUETOOTH SETTING"));
                this.J.add(new ir4("ANDROID SETTINGS", "ANDROID SETTINGS", arrayList3, false, 8, (qg6) null));
                ArrayList arrayList4 = new ArrayList();
                arrayList4.add(new er4("START MINDFULNESS PRACTICE", "START MINDFULNESS PRACTICE"));
                arrayList4.add(new er4("WATCH APP MUSIC CONTROL", "WATCH APP MUSIC CONTROL"));
                arrayList4.add(new er4("SIMULATE DISCONNECTION", "SIMULATE DISCONNECTION"));
                nh6 nh6 = nh6.a;
                Object[] objArr = new Object[1];
                an4 an43 = this.B;
                if (an43 != null) {
                    objArr[0] = Integer.valueOf(an43.x());
                    String format = String.format("Battery level: %d", Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    arrayList4.add(new hr4("TRIGGER LOW BATTERY EVENT", "TRIGGER LOW BATTERY EVENT", format));
                    this.J.add(new ir4("SIMULATION", "SIMULATION", arrayList4, false, 8, (qg6) null));
                    ArrayList arrayList5 = new ArrayList();
                    arrayList5.add(new er4("FIRMWARE_LOADING", "FIRMWARE_LOADING"));
                    this.J.add(new ir4("FIRMWARE", "FIRMWARE", arrayList5, false, 8, (qg6) null));
                    ArrayList arrayList6 = new ArrayList();
                    arrayList6.add(new er4("SYNC", "SYNC"));
                    List<DebugForceBackgroundRequestData> d3 = qd6.d(new DebugForceBackgroundRequestData("Notification Filter Background", ForceBackgroundRequest.BackgroundRequestType.SET_NOTIFICATION_FILTER), new DebugForceBackgroundRequestData("Alarms Background", ForceBackgroundRequest.BackgroundRequestType.SET_MULTI_ALARM), new DebugForceBackgroundRequestData("Device Configuration Background", ForceBackgroundRequest.BackgroundRequestType.SET_CONFIG_FILE));
                    ArrayList arrayList7 = new ArrayList(rd6.a(d3, 10));
                    for (DebugForceBackgroundRequestData visibleName : d3) {
                        arrayList7.add(visibleName.getVisibleName());
                    }
                    fr4 fr4 = new fr4("FORCE_BACKGROUND_REQUEST", "FORCE_BACKGROUND_REQUEST", yd6.d(arrayList7), "Send");
                    fr4.a(d3);
                    cd6 cd6 = cd6.a;
                    arrayList6.add(fr4);
                    arrayList6.add(new er4("WEATHER_WATCH_APP_TAP", "WEATHER_WATCH_APP_TAP"));
                    arrayList6.add(new er4("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT", "RESET DEVICE SETTING IN FIRMWARE TO DEFAULT"));
                    arrayList6.add(new er4("RESET_DELAY_OTA_TIMESTAMP", "RESET_DELAY_OTA_TIMESTAMP"));
                    an4 an44 = this.B;
                    if (an44 != null) {
                        arrayList6.add(new gr4("SKIP OTA", "SKIP OTA", an44.V()));
                        an4 an45 = this.B;
                        if (an45 != null) {
                            arrayList6.add(new gr4("SHOW ALL DEVICES", "SHOW ALL DEVICES", an45.U()));
                            an4 an46 = this.B;
                            if (an46 != null) {
                                arrayList6.add(new gr4("DISABLE HW_LOG SYNC", "DISABLE HW_LOG SYNC", !an46.J()));
                                an4 an47 = this.B;
                                if (an47 != null) {
                                    arrayList6.add(new gr4("DISABLE AUTO SYNC", "DISABLE AUTO SYNC", true ^ an47.C()));
                                    an4 an48 = this.B;
                                    if (an48 != null) {
                                        arrayList6.add(new gr4("SHOW DISPLAY DEVICE INFO", "SHOW DISPLAY DEVICE INFO", an48.K()));
                                        an4 an49 = this.B;
                                        if (an49 != null) {
                                            arrayList6.add(new gr4("CONSIDER AS LATEST BUNDLE FIRMWARE", "CONSIDER AS LATEST BUNDLE FIRMWARE:No Firmware", an49.D()));
                                            an4 an410 = this.B;
                                            if (an410 != null) {
                                                arrayList6.add(new gr4("APPLY NEW NOTIFICATION FILTER", "APPLY NEW NOTIFICATION FILTER", an410.N()));
                                                if (FossilDeviceSerialPatternUtil.isDianaDevice(e2)) {
                                                    arrayList6.add(new gr4("FRONT LIGHT ENABLE", "FRONT LIGHT ENABLE", this.N));
                                                    HeartRateMode heartRateMode = this.M;
                                                    if (heartRateMode != null) {
                                                        arrayList6.add(new hr4("SWITCH HEART RATE MODE", "SWITCH HEART RATE MODE", heartRateMode.name()));
                                                    } else {
                                                        wg6.d("mHeartRateMode");
                                                        throw null;
                                                    }
                                                }
                                                this.J.add(new ir4("OTHER", "OTHER", arrayList6, false, 8, (qg6) null));
                                                ArrayList arrayList8 = new ArrayList();
                                                arrayList8.add(new er4("CUSTOMIZE THEME", "CUSTOMIZE THEME"));
                                                this.J.add(new ir4("THEME", "THEME", arrayList8, false, 8, (qg6) null));
                                                cr4 cr4 = this.I;
                                                cr4.a(this.J);
                                                cr4.a((cr4.b) this);
                                                cd6 cd62 = cd6.a;
                                                RecyclerView findViewById = findViewById(2131362872);
                                                findViewById.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                                                findViewById.setAdapter(this.I);
                                                RecyclerView.g adapter = findViewById.getAdapter();
                                                if (adapter != null) {
                                                    adapter.notifyDataSetChanged();
                                                    cd6 cd63 = cd6.a;
                                                    FirmwareDebugViewModel a2 = vd.a(this).a(FirmwareDebugViewModel.class);
                                                    wg6.a((Object) a2, "ViewModelProviders.of(th\u2026bugViewModel::class.java)");
                                                    this.K = a2;
                                                    h hVar = new h(this);
                                                    FirmwareDebugViewModel firmwareDebugViewModel = this.K;
                                                    if (firmwareDebugViewModel != null) {
                                                        firmwareDebugViewModel.a().a(this, hVar);
                                                        g gVar = new g(this);
                                                        FirmwareDebugViewModel firmwareDebugViewModel2 = this.K;
                                                        if (firmwareDebugViewModel2 != null) {
                                                            firmwareDebugViewModel2.b().a(this, gVar);
                                                            if (!TextUtils.isEmpty(e2)) {
                                                                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new f(this, e2, (xe6) null), 3, (Object) null);
                                                                return;
                                                            }
                                                            return;
                                                        }
                                                        wg6.d("mFirmwareViewModel");
                                                        throw null;
                                                    }
                                                    wg6.d("mFirmwareViewModel");
                                                    throw null;
                                                }
                                                wg6.a();
                                                throw null;
                                            }
                                            wg6.d("mSharedPreferencesManager");
                                            throw null;
                                        }
                                        wg6.d("mSharedPreferencesManager");
                                        throw null;
                                    }
                                    wg6.d("mSharedPreferencesManager");
                                    throw null;
                                }
                                wg6.d("mSharedPreferencesManager");
                                throw null;
                            }
                            wg6.d("mSharedPreferencesManager");
                            throw null;
                        }
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                    wg6.d("mSharedPreferencesManager");
                    throw null;
                }
                wg6.d("mSharedPreferencesManager");
                throw null;
            }
            wg6.d("mSharedPreferencesManager");
            throw null;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.O, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.O, CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
        BleCommandResultManager.d.a(CommunicateMode.SET_HEART_RATE_MODE, CommunicateMode.SET_FRONT_LIGHT_ENABLE);
    }

    @DexIgnore
    public final cr4 s() {
        return this.I;
    }

    @DexIgnore
    public final LinkedList<lc6<String, Long>> t() {
        Calendar instance = Calendar.getInstance();
        LinkedList<lc6<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 31; i2++) {
            wg6.a((Object) instance, "calendar");
            Date time = instance.getTime();
            wg6.a((Object) time, "calendar.time");
            linkedList.add(a(time));
            instance.add(5, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local.d(f2, "getListDay - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final ArrayList<ir4> u() {
        return this.J;
    }

    @DexIgnore
    public final LinkedList<lc6<String, Long>> v() {
        Calendar instance = Calendar.getInstance();
        LinkedList<lc6<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 12; i2++) {
            wg6.a((Object) instance, "calendar");
            Date time = instance.getTime();
            wg6.a((Object) time, "calendar.time");
            linkedList.add(a(time));
            instance.add(2, -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local.d(f2, "getListMonth - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final LinkedList<lc6<String, Long>> w() {
        Calendar instance = Calendar.getInstance();
        LinkedList<lc6<String, Long>> linkedList = new LinkedList<>();
        for (int i2 = 0; i2 <= 52; i2++) {
            wg6.a((Object) instance, "calendar");
            Date time = instance.getTime();
            wg6.a((Object) time, "calendar.time");
            linkedList.add(b(time));
            instance.add(5, -7);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local.d(f2, "getListWeek - calendar=" + instance.getTime());
        }
        return linkedList;
    }

    @DexIgnore
    public final FirmwareFileRepository x() {
        FirmwareFileRepository firmwareFileRepository = this.D;
        if (firmwareFileRepository != null) {
            return firmwareFileRepository;
        }
        wg6.d("mFirmwareFileRepository");
        throw null;
    }

    @DexIgnore
    public final GuestApiService y() {
        GuestApiService guestApiService = this.E;
        if (guestApiService != null) {
            return guestApiService;
        }
        wg6.d("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final HeartRateMode z() {
        HeartRateMode heartRateMode = this.M;
        if (heartRateMode != null) {
            return heartRateMode;
        }
        wg6.d("mHeartRateMode");
        throw null;
    }

    @DexIgnore
    public final lc6<String, Long> b(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        instance.set(7, instance.getFirstDayOfWeek());
        int i2 = instance.get(5);
        instance.set(7, instance.getFirstDayOfWeek() + 6);
        int i3 = instance.get(5);
        StringBuilder sb = new StringBuilder();
        sb.append(i2);
        sb.append('-');
        sb.append(i3);
        return new lc6<>(sb.toString(), Long.valueOf(date.getTime()));
    }

    @DexIgnore
    public final void a(HeartRateMode heartRateMode) {
        wg6.b(heartRateMode, "<set-?>");
        this.M = heartRateMode;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(String str, xe6<? super List<DebugFirmwareData>> xe6) {
        c cVar;
        int i2;
        ArrayList arrayList;
        Object obj;
        DebugActivity debugActivity;
        ap4 ap4;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i3 = cVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                cVar.label = i3 - Integer.MIN_VALUE;
                Object obj2 = cVar.result;
                Object a2 = ff6.a();
                i2 = cVar.label;
                String str2 = null;
                if (i2 != 0) {
                    nc6.a(obj2);
                    if (str.length() > 0) {
                        arrayList = new ArrayList();
                        d dVar = new d(this, str, (xe6) null);
                        cVar.L$0 = this;
                        cVar.L$1 = str;
                        cVar.L$2 = arrayList;
                        cVar.label = 1;
                        obj = ResponseKt.a(dVar, cVar);
                        if (obj == a2) {
                            return a2;
                        }
                        debugActivity = this;
                    } else {
                        FLogger.INSTANCE.getLocal().d(f(), ".loadFirmware(), device model is empty. Return empty list");
                        return new ArrayList();
                    }
                } else if (i2 == 1) {
                    String str3 = (String) cVar.L$1;
                    debugActivity = (DebugActivity) cVar.L$0;
                    nc6.a(obj2);
                    Object obj3 = obj2;
                    arrayList = (ArrayList) cVar.L$2;
                    obj = obj3;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String f2 = debugActivity.f();
                local.d(f2, ".loadFirmware(), response" + ap4);
                if (!(ap4 instanceof cp4)) {
                    ApiResponse apiResponse = (ApiResponse) ((cp4) ap4).a();
                    List<Firmware> list = apiResponse != null ? apiResponse.get_items() : null;
                    if (list != null) {
                        for (Firmware firmware : list) {
                            FirmwareFileRepository firmwareFileRepository = debugActivity.D;
                            if (firmwareFileRepository != null) {
                                String versionNumber = firmware.getVersionNumber();
                                wg6.a((Object) versionNumber, "it.versionNumber");
                                String checksum = firmware.getChecksum();
                                wg6.a((Object) checksum, "it.checksum");
                                arrayList.add(new DebugFirmwareData(firmware, firmwareFileRepository.isDownloaded(versionNumber, checksum) ? 2 : 0));
                            } else {
                                wg6.d("mFirmwareFileRepository");
                                throw null;
                            }
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String f3 = debugActivity.f();
                    StringBuilder sb = new StringBuilder();
                    sb.append(".loadFirmware(), fail with code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(", message=");
                    ServerError c2 = zo4.c();
                    if (c2 != null) {
                        str2 = c2.getMessage();
                    }
                    sb.append(str2);
                    local2.d(f3, sb.toString());
                }
                return arrayList;
            }
        }
        cVar = new c(this, xe6);
        Object obj22 = cVar.result;
        Object a22 = ff6.a();
        i2 = cVar.label;
        String str22 = null;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj;
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String f22 = debugActivity.f();
        local3.d(f22, ".loadFirmware(), response" + ap4);
        if (!(ap4 instanceof cp4)) {
        }
        return arrayList;
    }

    @DexIgnore
    public void b(String str, int i2, int i3, Object obj, Bundle bundle) {
        DebugFirmwareData debugFirmwareData;
        wg6.b(str, "tagName");
        if (str.hashCode() == 227289531 && str.equals("FIRMWARE") && (debugFirmwareData = (DebugFirmwareData) obj) != null) {
            FirmwareFileRepository firmwareFileRepository = this.D;
            if (firmwareFileRepository != null) {
                String versionNumber = debugFirmwareData.getFirmware().getVersionNumber();
                wg6.a((Object) versionNumber, "it.firmware.versionNumber");
                String checksum = debugFirmwareData.getFirmware().getChecksum();
                wg6.a((Object) checksum, "it.firmware.checksum");
                if (firmwareFileRepository.isDownloaded(versionNumber, checksum)) {
                    c(debugFirmwareData.getFirmware());
                } else {
                    FLogger.INSTANCE.getLocal().d(f(), "Firmware hasn't been downloaded. Could not be set as latest firmware.");
                }
            } else {
                wg6.d("mFirmwareFileRepository");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
    public final void b(Firmware firmware) {
        l0.a aVar = new l0.a(this);
        aVar.b("Confirm OTA");
        aVar.a("Are you sure you want OTA to firmware " + firmware.getVersionNumber() + '?');
        aVar.b("Confirm", new o(this, firmware));
        aVar.a("Cancel", p.a);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity, android.app.Activity] */
    /* JADX WARNING: type inference failed for: r14v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void a(String str, int i2, int i3, Object obj, Bundle bundle) {
        DebugFirmwareData debugFirmwareData;
        String str2 = str;
        Bundle bundle2 = bundle;
        wg6.b(str2, "tagName");
        switch (str.hashCode()) {
            case -2017027426:
                if (str2.equals("BLUETOOTH SETTING")) {
                    Intent intent = new Intent();
                    intent.setAction("android.settings.BLUETOOTH_SETTINGS");
                    startActivity(intent);
                    cd6 cd6 = cd6.a;
                    return;
                }
                return;
            case -1935069302:
                if (str2.equals("WEATHER_WATCH_APP_TAP")) {
                    PortfolioApp.inner inner = PortfolioApp.get;
                    inner.a((Object) new mi4(inner.instance().e(), e90.WEATHER_WATCH_APP.ordinal(), new Bundle()));
                    return;
                }
                return;
            case -1822588397:
                if (str2.equals("TRIGGER LOW BATTERY EVENT")) {
                    l0.a aVar = new l0.a(this);
                    EditText editText = new EditText(this);
                    editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                    editText.setInputType(2);
                    aVar.b(editText);
                    aVar.b("Custom dialog");
                    aVar.a("Enter battery level between 1-100");
                    aVar.b("Done", new k(this, editText));
                    l0 a2 = aVar.a();
                    wg6.a((Object) a2, "dialogBuilder.create()");
                    a2.show();
                    Button b2 = a2.b(-1);
                    wg6.a((Object) b2, "dialog.getButton(AlertDialog.BUTTON_POSITIVE)");
                    b2.setEnabled(false);
                    editText.addTextChangedListener(new l(a2));
                    return;
                }
                return;
            case -1367489317:
                if (str2.equals("SKIP OTA")) {
                    an4 an4 = this.B;
                    if (an4 != null) {
                        Boolean valueOf = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf != null) {
                            an4.z(valueOf.booleanValue());
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1337201641:
                if (str2.equals("SHOW DISPLAY DEVICE INFO")) {
                    an4 an42 = this.B;
                    if (an42 != null) {
                        Boolean valueOf2 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf2 != null) {
                            an42.p(valueOf2.booleanValue());
                            an4 an43 = this.B;
                            if (an43 == null) {
                                wg6.d("mSharedPreferencesManager");
                                throw null;
                            } else if (an43.K()) {
                                a();
                                return;
                            } else {
                                o();
                                return;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -1276486892:
                if (str2.equals("DISABLE AUTO SYNC")) {
                    an4 an44 = this.B;
                    if (an44 != null) {
                        Boolean valueOf3 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf3 != null) {
                            an44.b(!valueOf3.booleanValue());
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -907588345:
                if (str2.equals("CONSIDER AS LATEST BUNDLE FIRMWARE")) {
                    an4 an45 = this.B;
                    if (an45 != null) {
                        Boolean valueOf4 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf4 != null) {
                            an45.c(valueOf4.booleanValue());
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -833549689:
                if (str2.equals("RESET_DELAY_OTA_TIMESTAMP")) {
                    an4 an46 = this.B;
                    if (an46 != null) {
                        an46.a(PortfolioApp.get.instance().e(), -1);
                        return;
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case -558521180:
                if (str2.equals("FRONT LIGHT ENABLE") && bundle2 != null && bundle2.containsKey("DEBUG_BUNDLE_IS_CHECKED")) {
                    boolean z = bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false);
                    p();
                    if (PortfolioApp.get.instance().c(PortfolioApp.get.instance().e(), z) == ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD()) {
                        h();
                        return;
                    }
                    return;
                }
                return;
            case -276828739:
                if (str2.equals("CLEAR DATA")) {
                    AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558481);
                    fVar.a(2131362310, "ARE YOU SURE?");
                    fVar.a(2131362309, "** This will NOT happen on production **");
                    fVar.a(2131362220, "OK");
                    fVar.b(2131362220);
                    AlertDialogFragment a3 = fVar.a("CONFIRM_CLEAR_DATA");
                    wg6.a((Object) a3, "AlertDialogFragment.Buil\u2026Utils.CONFIRM_CLEAR_DATA)");
                    a3.setCancelable(true);
                    a3.setStyle(0, 2131952011);
                    a3.show(getSupportFragmentManager(), "CONFIRM_CLEAR_DATA");
                    return;
                }
                return;
            case -75588064:
                if (str2.equals("GENERATE PRESET DATA")) {
                    t();
                    w();
                    v();
                    return;
                }
                return;
            case -62411653:
                if (str2.equals("SHOW ALL DEVICES")) {
                    an4 an47 = this.B;
                    if (an47 != null) {
                        Boolean valueOf5 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf5 != null) {
                            an47.y(valueOf5.booleanValue());
                            DeviceHelper.o.e().a();
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.d("mSharedPreferencesManager");
                    throw null;
                }
                return;
            case 2560667:
                if (str2.equals("SYNC")) {
                    PortfolioApp instance = PortfolioApp.get.instance();
                    cj4 cj4 = this.H;
                    if (cj4 != null) {
                        instance.a(cj4, false, 12);
                        return;
                    } else {
                        wg6.d("mDeviceSettingFactory");
                        throw null;
                    }
                } else {
                    return;
                }
            case 227289531:
                if (str2.equals("FIRMWARE") && (debugFirmwareData = (DebugFirmwareData) obj) != null) {
                    if (debugFirmwareData.getState() == 2) {
                        b(debugFirmwareData.getFirmware());
                    } else if (debugFirmwareData.getState() == 0) {
                        a(debugFirmwareData);
                    }
                    cd6 cd62 = cd6.a;
                    return;
                }
                return;
            case 334768719:
                if (str2.equals("RESET DEVICE SETTING IN FIRMWARE TO DEFAULT")) {
                    PortfolioApp.get.instance().Q();
                    return;
                }
                return;
            case 345524076:
                if (str2.equals("CUSTOMIZE THEME")) {
                    ThemesActivity.B.a(this);
                    return;
                }
                return;
            case 437897810:
                if (str2.equals("FORCE_BACKGROUND_REQUEST") && bundle2 != null && bundle2.containsKey("DEBUG_BUNDLE_SPINNER_SELECTED_POS")) {
                    int i4 = bundle2.getInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS");
                    List c2 = oh6.c(obj);
                    Object obj2 = c2 != null ? c2.get(i4) : null;
                    if (!(obj2 instanceof DebugForceBackgroundRequestData)) {
                        obj2 = null;
                    }
                    DebugForceBackgroundRequestData debugForceBackgroundRequestData = (DebugForceBackgroundRequestData) obj2;
                    if (debugForceBackgroundRequestData != null) {
                        Long.valueOf(PortfolioApp.get.instance().a(PortfolioApp.get.instance().e(), (CustomRequest) new ForceBackgroundRequest(debugForceBackgroundRequestData.getBackgroundRequestType())));
                        return;
                    }
                    return;
                }
                return;
            case 596314607:
                if (str2.equals("DISABLE HW_LOG SYNC")) {
                    an4 an48 = this.B;
                    if (an48 != null) {
                        Boolean valueOf6 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf6 != null) {
                            an48.i(!valueOf6.booleanValue());
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 967694544:
                if (str2.equals("SIMULATE DISCONNECTION")) {
                    l0.a aVar2 = new l0.a(this);
                    Object systemService = getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558782, (ViewGroup) null);
                        aVar2.b(inflate);
                        l0 a4 = aVar2.a();
                        wg6.a((Object) a4, "builder.create()");
                        a4.show();
                        EditText editText2 = (EditText) inflate.findViewById(2131362182);
                        EditText editText3 = (EditText) inflate.findViewById(2131362184);
                        EditText editText4 = (EditText) inflate.findViewById(2131362194);
                        EditText editText5 = (EditText) inflate.findViewById(2131362183);
                        wg6.a((Object) editText2, "etDelay");
                        double d2 = (double) 0;
                        double d3 = d2;
                        double d4 = d2;
                        double d5 = (double) 65535;
                        editText2.setFilters(new InputFilter[]{new b(this, d3, d5)});
                        wg6.a((Object) editText3, "etDuration");
                        double d6 = d4;
                        editText3.setFilters(new InputFilter[]{new b(this, d6, d5)});
                        wg6.a((Object) editText4, "etRepeat");
                        editText4.setFilters(new InputFilter[]{new b(this, d6, d5)});
                        wg6.a((Object) editText5, "etDelayEachTime");
                        editText5.setFilters(new InputFilter[]{new b(this, d6, (double) 4294967)});
                        ((FlexibleButton) inflate.findViewById(2131361958)).setOnClickListener(new j(this, editText2, editText3, editText4, editText5, a4));
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                }
                return;
            case 999402770:
                if (str2.equals("WATCH APP MUSIC CONTROL")) {
                    finish();
                    return;
                }
                return;
            case 1053818587:
                if (str2.equals("APPLY NEW NOTIFICATION FILTER")) {
                    an4 an49 = this.B;
                    if (an49 != null) {
                        Boolean valueOf7 = bundle2 != null ? Boolean.valueOf(bundle2.getBoolean("DEBUG_BUNDLE_IS_CHECKED", false)) : null;
                        if (valueOf7 != null) {
                            an49.t(valueOf7.booleanValue());
                            return;
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.d("mSharedPreferencesManager");
                        throw null;
                    }
                } else {
                    return;
                }
            case 1484036056:
                if (str2.equals("RESET UAPP LOG FILES")) {
                    MicroAppEventLogger.resetLogFiles();
                    return;
                }
                return;
            case 1642625733:
                if (str2.equals("GENERATE HEART RATE DATA")) {
                    finish();
                    return;
                }
                return;
            case 1977879337:
                if (str2.equals("VIEW LOG")) {
                    LogcatActivity.a((Context) this);
                    return;
                }
                return;
            case 2029483660:
                if (str2.equals("SEND LOG")) {
                    rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new i(this, (xe6) null), 3, (Object) null);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v19, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final lc6<String, Long> a(Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(date);
        long timeInMillis = instance.getTimeInMillis();
        switch (instance.get(7)) {
            case 1:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886537), Long.valueOf(timeInMillis));
            case 2:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886536), Long.valueOf(timeInMillis));
            case 3:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886539), Long.valueOf(timeInMillis));
            case 4:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886541), Long.valueOf(timeInMillis));
            case 5:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886540), Long.valueOf(timeInMillis));
            case 6:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886535), Long.valueOf(timeInMillis));
            case 7:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886538), Long.valueOf(timeInMillis));
            default:
                return new lc6<>(jm4.a((Context) PortfolioApp.get.instance(), 2131886537), Long.valueOf(timeInMillis));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
    public final void a(DebugFirmwareData debugFirmwareData) {
        l0.a aVar = new l0.a(this);
        Firmware firmware = debugFirmwareData.getFirmware();
        aVar.b("Confirm Download");
        aVar.a("Are you sure you want download to firmware " + firmware.getVersionNumber() + '?');
        aVar.b("Confirm", new m(firmware, this, debugFirmwareData));
        aVar.a("Cancel", n.a);
        aVar.a();
        aVar.c();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v3, types: [com.fossil.fs4, com.portfolio.platform.CoroutineUseCase] */
    public final void a(Firmware firmware) {
        String e2 = PortfolioApp.get.instance().e();
        FirmwareData createFirmwareData = FirmwareFactory.getInstance().createFirmwareData(firmware.getVersionNumber(), firmware.getDeviceModel(), firmware.getChecksum());
        wg6.a((Object) createFirmwareData, "FirmwareFactory.getInsta\u2026Model, firmware.checksum)");
        fs4.b bVar = new fs4.b(e2, createFirmwareData);
        Object r6 = this.C;
        if (r6 != 0) {
            r6.a(bVar, (CoroutineUseCase.e) null);
        } else {
            wg6.d("mOTAToSpecificFirmwareUseCase");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.ui.debug.DebugActivity] */
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        if (wg6.a((Object) str, (Object) "CONFIRM_CLEAR_DATA") && i2 == 2131362220) {
            DebugClearDataWarningActivity.a(this);
        } else if (wg6.a((Object) str, (Object) "SWITCH HEART RATE MODE") && i2 == 2131362220 && intent != null && intent.hasExtra("EXTRA_RADIO_GROUPS_RESULTS")) {
            HashMap hashMap = (HashMap) intent.getSerializableExtra("EXTRA_RADIO_GROUPS_RESULTS");
        }
    }
}
