package com.portfolio.platform.ui.user.usecase;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.it4;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DownloadUserInfoUseCase extends m24<it4.c, it4.d, it4.b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            wg6.b(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public d(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase", f = "DownloadUserInfoUseCase.kt", l = {22}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DownloadUserInfoUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(DownloadUserInfoUseCase downloadUserInfoUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = downloadUserInfoUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((it4.c) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = DownloadUserInfoUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "DownloadUserInfoUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public DownloadUserInfoUseCase(UserRepository userRepository) {
        wg6.b(userRepository, "userRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(it4.c cVar, xe6<Object> xe6) {
        e eVar;
        int i;
        ap4 ap4;
        String str;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(e, "running UseCase");
                    UserRepository userRepository = this.d;
                    eVar.L$0 = this;
                    eVar.L$1 = cVar;
                    eVar.label = 1;
                    obj = userRepository.loadUserInfo(eVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    c cVar2 = (c) eVar.L$1;
                    DownloadUserInfoUseCase downloadUserInfoUseCase = (DownloadUserInfoUseCase) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    return new d((MFUser) ((cp4) ap4).a());
                }
                if (!(ap4 instanceof zo4)) {
                    return cd6.a;
                }
                zo4 zo4 = (zo4) ap4;
                int a3 = zo4.a();
                ServerError c2 = zo4.c();
                if (c2 == null || (str = c2.getMessage()) == null) {
                    str = "";
                }
                return new b(a3, str);
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
