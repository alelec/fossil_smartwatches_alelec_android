package com.portfolio.platform.ui.view.chart.overview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.fossil.bk4;
import com.fossil.dx5;
import com.fossil.kc6;
import com.fossil.lc6;
import com.fossil.qg6;
import com.fossil.sh4;
import com.fossil.vt4;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.yj6;
import com.fossil.yk4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OverviewSleepDayChart extends BarChart {
    @DexIgnore
    public Integer A0;
    @DexIgnore
    public int B0;
    @DexIgnore
    public float v0;
    @DexIgnore
    public float w0;
    @DexIgnore
    public float x0;
    @DexIgnore
    public Integer y0;
    @DexIgnore
    public Integer z0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public OverviewSleepDayChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void a(String str, int i) {
        wg6.b(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " color=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1991609013) {
            if (hashCode != -1521618862) {
                if (hashCode == -219887775 && str.equals("lightSleep")) {
                    this.z0 = Integer.valueOf(i);
                }
            } else if (str.equals("awakeSleep")) {
                this.y0 = Integer.valueOf(i);
            }
        } else if (str.equals("deepSleep")) {
            this.A0 = Integer.valueOf(i);
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public void d() {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        ArrayList<BarChart.a> a2 = getMChartModel().a();
        if (a2.size() != 0) {
            setMGoalLinePath(new Path());
            setMGoalIconPoint(new PointF());
            setMGoalIconShow(false);
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            getMGraphHeight();
            float f11 = rectF.left;
            float f12 = 0.0f;
            ArrayList<ArrayList<BarChart.b>> c = a2.get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList next = it.next();
                i += ((BarChart.b) next.get(0)).d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                }
            }
            float f13 = (rectF.right - rectF.left) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = c.iterator();
            while (it2.hasNext()) {
                ArrayList next2 = it2.next();
                float mGraphHeight = ((float) getMGraphHeight()) * this.v0;
                int size = next2.size();
                float f14 = f11;
                for (int i2 = 0; i2 < size; i2++) {
                    if (i2 < next2.size() - 1) {
                        f12 = (((float) (((BarChart.b) next2.get(i2 + 1)).b() - ((BarChart.b) next2.get(i2)).b())) * f13) + f14;
                        int i3 = vt4.b[((BarChart.b) next2.get(i2)).c().ordinal()];
                        if (i3 != 1) {
                            if (i3 == 2) {
                                f10 = (float) getMGraphHeight();
                                f9 = (float) getMGraphHeight();
                                f8 = this.w0;
                            } else if (i3 == 3) {
                                f10 = (float) getMGraphHeight();
                                f9 = (float) getMGraphHeight();
                                f8 = this.x0;
                            } else {
                                throw new kc6();
                            }
                            f6 = (f10 - (f9 * f8)) - mGraphHeight;
                        } else {
                            f6 = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.v0);
                        }
                        int i4 = vt4.c[((BarChart.b) next2.get(i2)).c().ordinal()];
                        if (i4 == 1) {
                            f7 = (float) getMGraphHeight();
                        } else if (i4 == 2 || i4 == 3) {
                            f7 = ((float) getMGraphHeight()) - mGraphHeight;
                        } else {
                            throw new kc6();
                        }
                        ((BarChart.b) next2.get(i2)).a(new RectF(f14, f6, f12, f7));
                        f14 = f12;
                    } else if (i2 == next2.size() - 1) {
                        f12 = (((float) (((BarChart.b) next2.get(i2)).d() - ((BarChart.b) next2.get(i2)).b())) * f13) + f14;
                        int i5 = vt4.d[((BarChart.b) next2.get(i2)).c().ordinal()];
                        if (i5 != 1) {
                            if (i5 == 2) {
                                f5 = (float) getMGraphHeight();
                                f4 = (float) getMGraphHeight();
                                f3 = this.w0;
                            } else if (i5 == 3) {
                                f5 = (float) getMGraphHeight();
                                f4 = (float) getMGraphHeight();
                                f3 = this.x0;
                            } else {
                                throw new kc6();
                            }
                            f = (f5 - (f4 * f3)) - mGraphHeight;
                        } else {
                            f = ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.v0);
                        }
                        int i6 = vt4.e[((BarChart.b) next2.get(i2)).c().ordinal()];
                        if (i6 == 1) {
                            f2 = (float) getMGraphHeight();
                        } else if (i6 == 2 || i6 == 3) {
                            f2 = ((float) getMGraphHeight()) - mGraphHeight;
                        } else {
                            throw new kc6();
                        }
                        ((BarChart.b) next2.get(next2.size() - 1)).a(new RectF(f14, f, f12, f2));
                    } else {
                        continue;
                    }
                }
                f12 += ((float) 10) * f13;
                f11 = f12;
            }
        }
    }

    @DexIgnore
    public void e(Canvas canvas) {
        Canvas canvas2 = canvas;
        wg6.b(canvas2, "canvas");
        canvas2.drawRect(new RectF(0.0f, ((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.v0), (float) getMGraphWidth(), (((float) getMGraphHeight()) - (((float) getMGraphHeight()) * this.v0)) + ((float) 2)), getMLegendLinePaint());
        Iterator<BarChart.a> it = getMChartModel().a().iterator();
        while (it.hasNext()) {
            Iterator<ArrayList<BarChart.b>> it2 = it.next().c().iterator();
            while (it2.hasNext()) {
                Iterator it3 = it2.next().iterator();
                while (it3.hasNext()) {
                    BarChart.b bVar = (BarChart.b) it3.next();
                    int i = vt4.a[bVar.c().ordinal()];
                    if (i == 1) {
                        Paint mGraphPaint = getMGraphPaint();
                        Integer num = this.y0;
                        mGraphPaint.setColor(num != null ? num.intValue() : getMLowestColor());
                        dx5.a(canvas, bVar.a().left, bVar.a().top, bVar.a().right, bVar.a().bottom, getMBarRadius(), getMBarRadius(), false, false, true, true, getMGraphPaint());
                    } else if (i == 2) {
                        Paint mGraphPaint2 = getMGraphPaint();
                        Integer num2 = this.z0;
                        mGraphPaint2.setColor(num2 != null ? num2.intValue() : getMDefaultColor());
                        dx5.a(canvas, bVar.a().left, bVar.a().top, bVar.a().right, bVar.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    } else if (i == 3) {
                        Paint mGraphPaint3 = getMGraphPaint();
                        Integer num3 = this.A0;
                        mGraphPaint3.setColor(num3 != null ? num3.intValue() : getMHighestColor());
                        dx5.a(canvas, bVar.a().left, bVar.a().top, bVar.a().right, bVar.a().bottom, getMBarRadius(), getMBarRadius(), true, true, false, false, getMGraphPaint());
                    }
                }
            }
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        wg6.b(canvas, "canvas");
    }

    @DexIgnore
    public final void setTimeZoneOffsetInSecond(int i) {
        this.B0 = i;
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    @DexIgnore
    public OverviewSleepDayChart(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        this.B0 = -1;
        if (attributeSet != null && context != null && (theme = context.getTheme()) != null && (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, x24.OverviewSleepDayChart, 0, 0)) != null) {
            try {
                this.v0 = obtainStyledAttributes.getFloat(0, 0.1f);
                this.w0 = obtainStyledAttributes.getFloat(2, 0.33f);
                this.x0 = obtainStyledAttributes.getFloat(1, 0.6f);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "constructor - e=" + e);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public void a(float f, float f2) {
        if (getMChartModel().a().size() != 0) {
            ArrayList<ArrayList<BarChart.b>> c = getMChartModel().a().get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            while (it.hasNext()) {
                ArrayList next = it.next();
                Object obj = next.get(0);
                wg6.a(obj, "session[0]");
                BarChart.b bVar = (BarChart.b) obj;
                Object obj2 = next.get(next.size() - 1);
                wg6.a(obj2, "session[session.size - 1]");
                BarChart.b bVar2 = (BarChart.b) obj2;
                long j = (long) 1000;
                long e = ((long) bVar.e()) * j;
                long d = ((((long) bVar2.d()) - 1) * ((long) 60) * j) + e;
                Rect rect = new Rect();
                int i = this.B0;
                String a2 = i != -1 ? bk4.a(e, i) : bk4.b(e);
                int i2 = this.B0;
                String a3 = i2 != -1 ? bk4.a(d, i2) : bk4.b(d);
                yk4 yk4 = yk4.b;
                wg6.a((Object) a2, "startTimeString");
                String b = yk4.b(a2);
                yk4 yk42 = yk4.b;
                wg6.a((Object) a3, "endTimeString");
                String b2 = yk42.b(a3);
                getMLegendPaint().getTextBounds(b, 0, yj6.c(b), rect);
                float mTextMargin = ((float) getMTextMargin()) + ((float) rect.height());
                if (bVar2.a().right - bVar.a().left > ((float) (rect.right * 2))) {
                    getMTextPoint().add(new lc6(b, new PointF(bVar.a().left, mTextMargin)));
                    getMTextPoint().add(new lc6(b2, new PointF((bVar2.a().right - f) - ((float) rect.right), mTextMargin)));
                } else if (c.indexOf(next) == c.size() - 1) {
                    getMTextPoint().add(new lc6(b2, new PointF((bVar2.a().right - f) - ((float) rect.right), mTextMargin)));
                } else {
                    getMTextPoint().add(new lc6(b, new PointF(bVar.a().left, mTextMargin)));
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        if (getMGoalType() == sh4.TOTAL_SLEEP && getMChartModel().a().size() != 0) {
            RectF rectF = new RectF(getMBarMargin(), getMSafeAreaHeight(), ((float) getMGraphWidth()) - getMBarMargin(), (float) getMGraphHeight());
            ArrayList<BarChart.a> a2 = getMChartModel().a();
            int b = getMChartModel().b();
            ArrayList<ArrayList<BarChart.b>> c = a2.get(0).c();
            Iterator<ArrayList<BarChart.b>> it = c.iterator();
            int i = 0;
            while (it.hasNext()) {
                ArrayList next = it.next();
                i += ((BarChart.b) next.get(0)).d();
                if (c.indexOf(next) != 0) {
                    i += 10;
                    b += 10;
                }
            }
            float f = (((float) b) * (rectF.right - rectF.left)) / ((float) i);
            Iterator<ArrayList<BarChart.b>> it2 = c.iterator();
            while (it2.hasNext()) {
                ArrayList next2 = it2.next();
                if (f <= ((BarChart.b) next2.get(next2.size() - 1)).a().right) {
                    Iterator it3 = next2.iterator();
                    while (it3.hasNext()) {
                        BarChart.b bVar = (BarChart.b) it3.next();
                        if (f >= bVar.a().left && f <= bVar.a().right) {
                            getMGoalLinePath().moveTo(f, getMSafeAreaHeight() - 10.0f);
                            getMGoalLinePath().lineTo(f, bVar.a().top);
                            getMGoalIconPoint().set(f - (((float) getMGoalIconSize()) * 0.5f), (getMSafeAreaHeight() * 0.5f) - 20.0f);
                            setMGoalIconShow(false);
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }
}
