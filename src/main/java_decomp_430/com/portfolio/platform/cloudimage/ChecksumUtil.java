package com.portfolio.platform.cloudimage;

import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.FileInputStream;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChecksumUtil {
    @DexIgnore
    public static /* final */ ChecksumUtil INSTANCE; // = new ChecksumUtil();
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + ChecksumUtil.class.getSimpleName());

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        int length = bArr.length;
        int i = 0;
        while (i < length) {
            String num = Integer.toString(((byte) (bArr[i] & ((byte) 255))) + 256, 16);
            wg6.a((Object) num, "Integer.toString((input[\u2026ff.toByte()) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                wg6.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
                i++;
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "ret.toString()");
        if (sb2 != null) {
            String lowerCase = sb2.toLowerCase();
            wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean verifyDownloadFile(String str, String str2) {
        wg6.b(str, "filePath");
        if (str2 == null) {
            return true;
        }
        MessageDigest instance = MessageDigest.getInstance("MD5");
        FileInputStream fileInputStream = new FileInputStream(str);
        byte[] bArr = new byte[2014];
        while (true) {
            try {
                int read = fileInputStream.read(bArr);
                if (!(read != -1)) {
                    byte[] digest = instance.digest();
                    wg6.a((Object) digest, "md5");
                    String bytesToString = bytesToString(digest);
                    String lowerCase = str2.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    boolean a = wg6.a((Object) lowerCase, (Object) bytesToString);
                    fileInputStream.close();
                    return a;
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.e(str3, "VerifyDownloadFileFailed - ex=" + e);
                fileInputStream.close();
                return false;
            } catch (Throwable unused) {
                fileInputStream.close();
                return false;
            }
        }
    }
}
