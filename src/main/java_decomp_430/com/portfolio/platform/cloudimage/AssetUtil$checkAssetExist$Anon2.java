package com.portfolio.platform.cloudimage;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$2", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.OnImageCallbackListener $listener;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$Anon2(CloudImageHelper.OnImageCallbackListener onImageCallbackListener, String str, String str2, xe6 xe6) {
        super(2, xe6);
        this.$listener = onImageCallbackListener;
        this.$serialNumber = str;
        this.$filePath1 = str2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AssetUtil$checkAssetExist$Anon2 assetUtil$checkAssetExist$Anon2 = new AssetUtil$checkAssetExist$Anon2(this.$listener, this.$serialNumber, this.$filePath1, xe6);
        assetUtil$checkAssetExist$Anon2.p$ = (il6) obj;
        return assetUtil$checkAssetExist$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AssetUtil$checkAssetExist$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.$listener;
            if (onImageCallbackListener == null) {
                return null;
            }
            onImageCallbackListener.onImageCallback(this.$serialNumber, this.$filePath1);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
