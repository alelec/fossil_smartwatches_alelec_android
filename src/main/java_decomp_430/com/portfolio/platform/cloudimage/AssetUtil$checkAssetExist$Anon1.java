package com.portfolio.platform.cloudimage;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.AssetUtil", f = "AssetUtil.kt", l = {18, 21, 26, 29}, m = "checkAssetExist")
public final class AssetUtil$checkAssetExist$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$10;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public boolean Z$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ AssetUtil this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$Anon1(AssetUtil assetUtil, xe6 xe6) {
        super(xe6);
        this.this$0 = assetUtil;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.checkAssetExist((File) null, (String) null, (String) null, (String) null, (String) null, (String) null, (CloudImageHelper.OnImageCallbackListener) null, this);
    }
}
