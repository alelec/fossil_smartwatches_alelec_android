package com.portfolio.platform.cloudimage;

import com.fossil.gg6;
import com.fossil.xg6;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper$Companion$instance$Anon2 extends xg6 implements gg6<CloudImageHelper> {
    @DexIgnore
    public static /* final */ CloudImageHelper$Companion$instance$Anon2 INSTANCE; // = new CloudImageHelper$Companion$instance$Anon2();

    @DexIgnore
    public CloudImageHelper$Companion$instance$Anon2() {
        super(0);
    }

    @DexIgnore
    public final CloudImageHelper invoke() {
        return CloudImageHelper.Holder.INSTANCE.getINSTANCE();
    }
}
