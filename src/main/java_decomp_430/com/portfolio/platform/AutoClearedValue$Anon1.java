package com.portfolio.platform;

import androidx.lifecycle.Lifecycle;
import com.fossil.fd;
import com.fossil.i24;
import com.fossil.md;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AutoClearedValue$Anon1 implements fd {
    @DexIgnore
    public /* final */ /* synthetic */ i24 a;

    @DexIgnore
    @md(Lifecycle.a.ON_DESTROY)
    public final void onDestroy() {
        this.a.a(null);
        throw null;
    }
}
