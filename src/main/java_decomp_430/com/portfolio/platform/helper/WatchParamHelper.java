package com.portfolio.platform.helper;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParamHelper {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public /* final */ DeviceRepository a;
    @DexIgnore
    public /* final */ PortfolioApp b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.helper.WatchParamHelper", f = "WatchParamHelper.kt", l = {37}, m = "handleFailureResponseWatchParam")
    public static final class b extends jf6 {
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchParamHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WatchParamHelper watchParamHelper, xe6 xe6) {
            super(xe6);
            this.this$0 = watchParamHelper;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, 0.0f, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.helper.WatchParamHelper", f = "WatchParamHelper.kt", l = {18}, m = "handleSuccessResponseWatchParam")
    public static final class c extends jf6 {
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchParamHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WatchParamHelper watchParamHelper, xe6 xe6) {
            super(xe6);
            this.this$0 = watchParamHelper;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, 0.0f, (WatchParameterResponse) null, this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = WatchParamHelper.class.getSimpleName();
        wg6.a((Object) simpleName, "WatchParamHelper::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public WatchParamHelper(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(portfolioApp, "mApp");
        this.a = deviceRepository;
        this.b = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(String str, float f, WatchParameterResponse watchParameterResponse, xe6<? super cd6> xe6) {
        c cVar;
        int i;
        String str2;
        WatchParam watchParam;
        WatchParamHelper watchParamHelper;
        String versionMajor;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i2 = cVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                cVar.label = i2 - Integer.MIN_VALUE;
                Object obj = cVar.result;
                Object a2 = ff6.a();
                i = cVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = c;
                    local.d(str3, "handleSuccessResponseWatchParam, response=" + watchParameterResponse + ", currentWPVersion=" + f);
                    WatchParam watchParamModel = watchParameterResponse.toWatchParamModel(str);
                    DeviceRepository deviceRepository = this.a;
                    cVar.L$0 = this;
                    cVar.L$1 = str;
                    cVar.F$0 = f;
                    cVar.L$2 = watchParameterResponse;
                    cVar.L$3 = watchParamModel;
                    cVar.label = 1;
                    if (deviceRepository.saveWatchParamModel(watchParamModel, cVar) == a2) {
                        return a2;
                    }
                    watchParamHelper = this;
                    str2 = str;
                    watchParam = watchParamModel;
                } else if (i == 1) {
                    watchParam = (WatchParam) cVar.L$3;
                    WatchParameterResponse watchParameterResponse2 = (WatchParameterResponse) cVar.L$2;
                    f = cVar.F$0;
                    str2 = (String) cVar.L$1;
                    watchParamHelper = (WatchParamHelper) cVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                versionMajor = watchParam.getVersionMajor();
                if (versionMajor == null) {
                    int parseInt = Integer.parseInt(versionMajor);
                    String versionMinor = watchParam.getVersionMinor();
                    if (versionMinor != null) {
                        if (f < watchParamHelper.a(parseInt, Integer.parseInt(versionMinor))) {
                            FLogger.INSTANCE.getLocal().d(c, "Need to update newer WP version from response");
                            String data = watchParam.getData();
                            PortfolioApp portfolioApp = watchParamHelper.b;
                            if (data != null) {
                                portfolioApp.a(str2, true, new WatchParamsFileMapping(data));
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().d(c, "No need to update WP version in device, it's the latest one");
                            watchParamHelper.b.a(str2, true, (WatchParamsFileMapping) null);
                        }
                        return cd6.a;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
        }
        cVar = new c(this, xe6);
        Object obj2 = cVar.result;
        Object a22 = ff6.a();
        i = cVar.label;
        if (i != 0) {
        }
        versionMajor = watchParam.getVersionMajor();
        if (versionMajor == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(String str, float f, xe6<? super cd6> xe6) {
        b bVar;
        int i;
        WatchParamHelper watchParamHelper;
        WatchParam watchParam;
        if (xe6 instanceof b) {
            bVar = (b) xe6;
            int i2 = bVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                bVar.label = i2 - Integer.MIN_VALUE;
                Object obj = bVar.result;
                Object a2 = ff6.a();
                i = bVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = c;
                    local.d(str2, "handleFailureResponseWatchParam, currentWPVersion=" + f);
                    DeviceRepository deviceRepository = this.a;
                    bVar.L$0 = this;
                    bVar.L$1 = str;
                    bVar.F$0 = f;
                    bVar.label = 1;
                    obj = deviceRepository.getWatchParamBySerialId(str, bVar);
                    if (obj == a2) {
                        return a2;
                    }
                    watchParamHelper = this;
                } else if (i == 1) {
                    f = bVar.F$0;
                    str = (String) bVar.L$1;
                    watchParamHelper = (WatchParamHelper) bVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                watchParam = (WatchParam) obj;
                if (watchParam == null) {
                    String versionMajor = watchParam.getVersionMajor();
                    if (versionMajor != null) {
                        int parseInt = Integer.parseInt(versionMajor);
                        String versionMinor = watchParam.getVersionMinor();
                        if (versionMinor == null) {
                            wg6.a();
                            throw null;
                        } else if (f < watchParamHelper.a(parseInt, Integer.parseInt(versionMinor))) {
                            FLogger.INSTANCE.getLocal().d(c, "Newer version is available in database, set to device");
                            String data = watchParam.getData();
                            PortfolioApp portfolioApp = watchParamHelper.b;
                            if (data != null) {
                                portfolioApp.a(str, true, new WatchParamsFileMapping(data));
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().d(c, "The saved version in database is older than the current one, skip it");
                            watchParamHelper.b.a(str, true, (WatchParamsFileMapping) null);
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (f != 0.0f) {
                    FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database, but keep going because the current version is not empty");
                    watchParamHelper.b.a(str, true, (WatchParamsFileMapping) null);
                } else {
                    FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database and the current version is empty -> notify failed");
                    watchParamHelper.b.a(str, false, (WatchParamsFileMapping) null);
                }
                return cd6.a;
            }
        }
        bVar = new b(this, xe6);
        Object obj2 = bVar.result;
        Object a22 = ff6.a();
        i = bVar.label;
        if (i != 0) {
        }
        watchParam = (WatchParam) obj2;
        if (watchParam == null) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final float a(int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append('.');
        sb.append(i2);
        return Float.parseFloat(sb.toString());
    }
}
