package com.portfolio.platform.helper;

import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.nu3;
import com.fossil.ou3;
import com.fossil.pu3;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GsonISOConvertDateTime implements hu3<Date>, pu3<Date> {
    @DexIgnore
    /* renamed from: a */
    public JsonElement serialize(Date date, Type type, ou3 ou3) {
        String str;
        if (date == null) {
            str = "";
        } else {
            SimpleDateFormat simpleDateFormat = bk4.m.get();
            if (simpleDateFormat != null) {
                str = simpleDateFormat.format(date);
                wg6.a((Object) str, "DateHelper.SERVER_DATE_I\u2026ORMAT.get()!!.format(src)");
            } else {
                wg6.a();
                throw null;
            }
        }
        return new nu3(str);
    }

    @DexIgnore
    public Date deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        String f;
        if (!(jsonElement == null || (f = jsonElement.f()) == null)) {
            try {
                SimpleDateFormat simpleDateFormat = bk4.m.get();
                if (simpleDateFormat != null) {
                    Date parse = simpleDateFormat.parse(f);
                    SimpleDateFormat simpleDateFormat2 = bk4.k.get();
                    if (simpleDateFormat2 != null) {
                        SimpleDateFormat simpleDateFormat3 = simpleDateFormat2;
                        SimpleDateFormat simpleDateFormat4 = bk4.k.get();
                        if (simpleDateFormat4 != null) {
                            Date parse2 = simpleDateFormat3.parse(simpleDateFormat4.format(parse));
                            wg6.a((Object) parse2, "DateHelper.LOCAL_DATE_SP\u2026MAT.get()!!.format(date))");
                            return parse2;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            } catch (ParseException e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("deserialize - json=");
                sb.append(jsonElement.f());
                sb.append(", e=");
                e.printStackTrace();
                sb.append(cd6.a);
                local.e("GsonISOConvertDateTime", sb.toString());
            }
        }
        return new Date(0);
    }
}
