package com.portfolio.platform;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityManager;
import androidx.lifecycle.Lifecycle;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.fd;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.md;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.z04;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApplicationEventListener implements fd {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public /* final */ PortfolioApp a;
    @DexIgnore
    public /* final */ an4 b;
    @DexIgnore
    public /* final */ HybridPresetRepository c;
    @DexIgnore
    public /* final */ CategoryRepository d;
    @DexIgnore
    public /* final */ WatchAppRepository e;
    @DexIgnore
    public /* final */ ComplicationRepository f;
    @DexIgnore
    public /* final */ MicroAppRepository g;
    @DexIgnore
    public /* final */ DianaPresetRepository h;
    @DexIgnore
    public /* final */ RingStyleRepository i;
    @DexIgnore
    public /* final */ DeviceRepository j;
    @DexIgnore
    public /* final */ UserRepository o;
    @DexIgnore
    public /* final */ cj4 p;
    @DexIgnore
    public /* final */ AlarmsRepository q;
    @DexIgnore
    public /* final */ WatchFaceRepository r;
    @DexIgnore
    public /* final */ WatchLocalizationRepository s;
    @DexIgnore
    public /* final */ FileRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ApplicationEventListener.u;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$1", f = "ApplicationEventListener.kt", l = {65}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ApplicationEventListener applicationEventListener, xe6 xe6) {
            super(2, xe6);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                PortfolioApp c = this.this$0.a;
                this.L$0 = il6;
                this.label = 1;
                if (c.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2", f = "ApplicationEventListener.kt", l = {68}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $mActiveSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ApplicationEventListener applicationEventListener, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = applicationEventListener;
            this.$mActiveSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$mActiveSerial, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                RingStyleRepository k = this.this$0.i;
                String str = this.$mActiveSerial;
                this.L$0 = il6;
                this.label = 1;
                if (k.downloadRingStyleList(str, true, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$3", f = "ApplicationEventListener.kt", l = {82, 83, 84, 87, 88, 89, 92, 93, 97, 98, 99, 100, 101, 104, 105, 106}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $mActiveSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ApplicationEventListener applicationEventListener, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = applicationEventListener;
            this.$mActiveSerial = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$mActiveSerial, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0164, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.f(r13.this$0);
            r13.L$0 = r6;
            r13.L$1 = r5;
            r13.L$2 = r1;
            r13.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0177, code lost:
            if (r14.downloadDeviceList(r13) != r0) goto L_0x017a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0179, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x017a, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.l(r13.this$0);
            r13.L$0 = r6;
            r13.L$1 = r5;
            r13.L$2 = r1;
            r13.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x018d, code lost:
            if (r14.loadUserInfo(r13) != r0) goto L_0x0190;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x018f, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0190, code lost:
            com.portfolio.platform.ApplicationEventListener.h(r13.this$0).downloadPendingFile();
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER, com.portfolio.platform.PortfolioApp.get.instance().e(), com.portfolio.platform.ApplicationEventListener.v.a(), "[App Open] Start download firmware");
            r14 = com.portfolio.platform.util.DeviceUtils.g.a();
            r13.L$0 = r6;
            r13.L$1 = r5;
            r13.L$2 = r1;
            r13.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x01cb, code lost:
            if (r14.a((com.fossil.xe6<? super com.fossil.cd6>) r13) != r0) goto L_0x01ce;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x01cd, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x01ce, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.f(r13.this$0);
            r13.L$0 = r6;
            r13.L$1 = r5;
            r13.L$2 = r1;
            r13.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x01e1, code lost:
            if (com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r14, 0, r13, 1, (java.lang.Object) null) != r0) goto L_0x01e4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x01e3, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x01e4, code lost:
            r2 = r5;
            r5 = r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x01e6, code lost:
            r14 = com.portfolio.platform.PortfolioApp.get.instance().r();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x01f0, code lost:
            if (r14 == null) goto L_0x031d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x01f2, code lost:
            r14 = r14.a((com.portfolio.platform.localization.LocalizationManager.d) null);
            r13.L$0 = r5;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0203, code lost:
            if (r14.a((com.fossil.xe6<? super com.fossil.cd6>) r13) != r0) goto L_0x0206;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x0205, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0206, code lost:
            r3 = r5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x020d, code lost:
            if (android.text.TextUtils.isEmpty(r13.$mActiveSerial) != false) goto L_0x0317;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x020f, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.d(r13.this$0);
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0222, code lost:
            if (r14.downloadCategories(r13) != r0) goto L_0x0225;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0224, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0225, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.b(r13.this$0);
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0239, code lost:
            if (r14.downloadAlarms(r13) != r0) goto L_0x023c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x023b, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x023c, code lost:
            r14 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r13.$mActiveSerial);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x0242, code lost:
            if (r14 != null) goto L_0x0245;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x024d, code lost:
            if (com.fossil.a14.a[r14.ordinal()] == 1) goto L_0x029a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x024f, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.i(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 14;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x0265, code lost:
            if (r14.downloadRecommendPresetList(r4, r13) != r0) goto L_0x0268;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x0267, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x0268, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.j(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 15;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x027e, code lost:
            if (r14.downloadAllMicroApp(r4, r13) != r0) goto L_0x0281;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x0280, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x0281, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.i(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 16;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x0297, code lost:
            if (r14.downloadPresetList(r4, r13) != r0) goto L_0x0317;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x0299, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x029a, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.m(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x02b0, code lost:
            if (r14.downloadWatchApp(r4, r13) != r0) goto L_0x02b3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x02b2, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x02b3, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.e(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 10;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x02c9, code lost:
            if (r14.downloadAllComplication(r4, r13) != r0) goto L_0x02cc;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x02cb, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x02cc, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.g(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 11;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x02e2, code lost:
            if (r14.downloadPresetList(r4, r13) != r0) goto L_0x02e5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x02e4, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x02e5, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.g(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 12;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x02fb, code lost:
            if (r14.downloadRecommendPresetList(r4, r13) != r0) goto L_0x02fe;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x02fd, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x02fe, code lost:
            r14 = com.portfolio.platform.ApplicationEventListener.n(r13.this$0);
            r4 = r13.$mActiveSerial;
            r13.L$0 = r3;
            r13.L$1 = r2;
            r13.L$2 = r1;
            r13.label = 13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x0314, code lost:
            if (r14.getWatchFacesFromServer(r4, r13) != r0) goto L_0x0317;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x0316, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x0317, code lost:
            com.portfolio.platform.ApplicationEventListener.p(r13.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x031d, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x0320, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0334, code lost:
            return com.fossil.cd6.a;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            MFUser mFUser;
            MFUser mFUser2;
            il6 il62;
            MFUser mFUser3;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il6 il63 = this.p$;
                    mFUser2 = this.this$0.o.getCurrentUser();
                    if (mFUser2 == null) {
                        FLogger.INSTANCE.getLocal().d(ApplicationEventListener.v.a(), "user is not log in yet, do nothing");
                        break;
                    } else {
                        if (!TextUtils.isEmpty(this.$mActiveSerial)) {
                            this.this$0.a();
                            ((MusicControlComponent) MusicControlComponent.o.a(this.this$0.a)).b();
                        }
                        if (PortfolioApp.get.instance().y()) {
                            WatchLocalizationRepository o = this.this$0.s;
                            this.L$0 = il63;
                            this.L$1 = mFUser2;
                            this.L$2 = mFUser2;
                            this.label = 1;
                            if (o.getWatchLocalizationFromServer(false, this) != a) {
                                il62 = il63;
                                mFUser3 = mFUser2;
                                break;
                            } else {
                                return a;
                            }
                        }
                    }
                    break;
                case 1:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser3 = (MFUser) this.L$1;
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser3 = (MFUser) this.L$1;
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser3 = (MFUser) this.L$1;
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 4:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser3 = (MFUser) this.L$1;
                    il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 5:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 il64 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 6:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 7:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 8:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 9:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 10:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 11:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 12:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 13:
                case 16:
                    MFUser mFUser4 = (MFUser) this.L$2;
                    MFUser mFUser5 = (MFUser) this.L$1;
                    il6 il65 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 14:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 15:
                    mFUser2 = (MFUser) this.L$2;
                    mFUser = (MFUser) this.L$1;
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = z04.INSTANCE.getClass().getSimpleName();
        wg6.a((Object) simpleName, "ApplicationEventListener\u2026lass.javaClass.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public ApplicationEventListener(PortfolioApp portfolioApp, an4 an4, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, DianaPresetRepository dianaPresetRepository, RingStyleRepository ringStyleRepository, DeviceRepository deviceRepository, UserRepository userRepository, cj4 cj4, AlarmsRepository alarmsRepository, WatchFaceRepository watchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, FileRepository fileRepository, ThemeRepository themeRepository) {
        PortfolioApp portfolioApp2 = portfolioApp;
        an4 an42 = an4;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        CategoryRepository categoryRepository2 = categoryRepository;
        WatchAppRepository watchAppRepository2 = watchAppRepository;
        ComplicationRepository complicationRepository2 = complicationRepository;
        MicroAppRepository microAppRepository2 = microAppRepository;
        DianaPresetRepository dianaPresetRepository2 = dianaPresetRepository;
        RingStyleRepository ringStyleRepository2 = ringStyleRepository;
        DeviceRepository deviceRepository2 = deviceRepository;
        UserRepository userRepository2 = userRepository;
        cj4 cj42 = cj4;
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        WatchFaceRepository watchFaceRepository2 = watchFaceRepository;
        FileRepository fileRepository2 = fileRepository;
        wg6.b(portfolioApp2, "mApp");
        wg6.b(an42, "mSharedPrefs");
        wg6.b(hybridPresetRepository2, "mHybridPresetRepository");
        wg6.b(categoryRepository2, "mCategoryRepository");
        wg6.b(watchAppRepository2, "mWatchAppRepository");
        wg6.b(complicationRepository2, "mComplicationRepository");
        wg6.b(microAppRepository2, "mMicroAppRepository");
        wg6.b(dianaPresetRepository2, "mDianaPresetRepository");
        wg6.b(ringStyleRepository2, "mRingStyleRepository");
        wg6.b(deviceRepository2, "mDeviceRepository");
        wg6.b(userRepository2, "mUserRepository");
        wg6.b(cj42, "mDeviceSettingFactory");
        wg6.b(alarmsRepository2, "mAlarmRepository");
        wg6.b(watchFaceRepository2, "mWatchFaceRepository");
        wg6.b(watchLocalizationRepository, "watchLocalization");
        wg6.b(fileRepository, "mFileRepository");
        wg6.b(themeRepository, "themeRepository");
        this.a = portfolioApp2;
        this.b = an42;
        this.c = hybridPresetRepository2;
        this.d = categoryRepository2;
        this.e = watchAppRepository2;
        this.f = complicationRepository2;
        this.g = microAppRepository2;
        this.h = dianaPresetRepository2;
        this.i = ringStyleRepository2;
        this.j = deviceRepository2;
        this.o = userRepository2;
        this.p = cj42;
        this.q = alarmsRepository2;
        this.r = watchFaceRepository2;
        this.s = watchLocalizationRepository;
        this.t = fileRepository;
    }

    @DexIgnore
    @md(Lifecycle.a.ON_STOP)
    public final void onAppEnterBackground() {
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.a.e(), u, "[App Close] User put app in background");
    }

    @DexIgnore
    @md(Lifecycle.a.ON_START)
    public final void onAppEnterForeground() {
        boolean m = this.b.m(this.a.h());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "onAppEnterForeground isMigrationComplete " + m);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
        String e2 = this.a.e();
        if (!TextUtils.isEmpty(e2)) {
            rm6 unused2 = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, e2, (xe6) null), 3, (Object) null);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String e3 = this.a.e();
            String str2 = u;
            remote.i(component, session, e3, str2, "[App Open] Is migrate complete " + m + " \n Is Notification Listener Enabled " + PortfolioApp.get.instance().E());
        }
        if (m) {
            rm6 unused3 = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new d(this, e2, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a() {
        FLogger.INSTANCE.getLocal().d(u, "Inside .autoSync");
        String e2 = this.a.e();
        if (TextUtils.isEmpty(e2)) {
            FLogger.INSTANCE.getLocal().d(u, "User has no active device, skip auto sync");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "Inside .autoSync lastSyncSuccess=" + this.b.f(this.a.e()));
        long currentTimeMillis = System.currentTimeMillis() - this.b.g(this.a.e());
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = u;
        remote.i(component, session, e2, str2, "[App Open] Last sync OK interval " + currentTimeMillis);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = u;
        local2.d(str3, "Inside .autoSync, last sync interval " + currentTimeMillis);
        if (currentTimeMillis >= ((long) CommuteTimeService.y) || currentTimeMillis < 0) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp.g(portfolioApp.e()) == CommunicateMode.OTA.getValue()) {
                FLogger.INSTANCE.getLocal().d(u, "Inside .autoSync, device is ota, wait for the next time");
            } else if (this.a.F() || this.b.C()) {
                FLogger.INSTANCE.getLocal().d(u, "Inside .autoSync, start auto-sync.");
                boolean a2 = xm4.a(xm4.d, this.a, "SYNC_DEVICE", false, false, false, 24, (Object) null);
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String str4 = u;
                remote2.i(component2, session2, e2, str4, "[App Open] [Sync Start] AUTO SYNC isBlueToothEnabled " + BluetoothUtils.isBluetoothEnable() + ' ');
                if (a2) {
                    this.a.a(this.p, false, 10);
                } else {
                    FLogger.INSTANCE.getLocal().d(u, "autoSync fail due to lack of permission");
                }
            } else {
                FLogger.INSTANCE.getLocal().d(u, "Inside .autoSync, doesn't start auto-sync.");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void b() {
        Object systemService = this.a.getSystemService("accessibility");
        if (systemService != null) {
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList = ((AccessibilityManager) systemService).getEnabledAccessibilityServiceList(-1);
            StringBuilder sb = new StringBuilder();
            for (AccessibilityServiceInfo next : enabledAccessibilityServiceList) {
                wg6.a((Object) next, "accessibility");
                String id = next.getId();
                wg6.a((Object) id, "accessibility.id");
                sb.append((String) yd6.f(yj6.a((CharSequence) id, new String[]{"."}, false, 0, 6, (Object) null)));
                sb.append(" ");
            }
            String sb2 = sb.toString();
            wg6.a((Object) sb2, "enabledAccessibilityStringBuilder.toString()");
            if (sb2 != null) {
                String obj = yj6.d(sb2).toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = u;
                local.d(str, "Enabled Accessibility: " + obj);
                AnalyticsHelper.f.c().a("accessibility_config_on_launch", obj);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new rc6("null cannot be cast to non-null type android.view.accessibility.AccessibilityManager");
    }
}
