package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn6 {
    @DexIgnore
    public static final <T> void a(xe6<? super T> xe6, T t, int i) {
        wg6.b(xe6, "$this$resumeMode");
        if (i == 0) {
            mc6.a aVar = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(t));
        } else if (i == 1) {
            xl6.a(xe6, t);
        } else if (i == 2) {
            xl6.b(xe6, t);
        } else if (i == 3) {
            vl6 vl6 = (vl6) xe6;
            af6 context = vl6.getContext();
            Object b = yo6.b(context, vl6.f);
            try {
                xe6<T> xe62 = vl6.h;
                mc6.a aVar2 = mc6.Companion;
                xe62.resumeWith(mc6.m1constructorimpl(t));
                cd6 cd6 = cd6.a;
            } finally {
                yo6.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final boolean a(int i) {
        return i == 1;
    }

    @DexIgnore
    public static final <T> void b(xe6<? super T> xe6, Throwable th, int i) {
        wg6.b(xe6, "$this$resumeWithExceptionMode");
        wg6.b(th, "exception");
        if (i == 0) {
            mc6.a aVar = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        } else if (i == 1) {
            xl6.a(xe6, th);
        } else if (i == 2) {
            xl6.b(xe6, th);
        } else if (i == 3) {
            vl6 vl6 = (vl6) xe6;
            af6 context = vl6.getContext();
            Object b = yo6.b(context, vl6.f);
            try {
                xe6<T> xe62 = vl6.h;
                mc6.a aVar2 = mc6.Companion;
                xe62.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(th, (xe6<?>) xe62))));
                cd6 cd6 = cd6.a;
            } finally {
                yo6.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final boolean b(int i) {
        return i == 0 || i == 1;
    }

    @DexIgnore
    public static final <T> void a(xe6<? super T> xe6, Throwable th, int i) {
        wg6.b(xe6, "$this$resumeUninterceptedWithExceptionMode");
        wg6.b(th, "exception");
        if (i == 0) {
            xe6<? super T> a = ef6.a(xe6);
            mc6.a aVar = mc6.Companion;
            a.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        } else if (i == 1) {
            xl6.a(ef6.a(xe6), th);
        } else if (i == 2) {
            mc6.a aVar2 = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        } else if (i == 3) {
            af6 context = xe6.getContext();
            Object b = yo6.b(context, (Object) null);
            try {
                mc6.a aVar3 = mc6.Companion;
                xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
                cd6 cd6 = cd6.a;
            } finally {
                yo6.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }

    @DexIgnore
    public static final <T> void b(xe6<? super T> xe6, T t, int i) {
        wg6.b(xe6, "$this$resumeUninterceptedMode");
        if (i == 0) {
            xe6<? super T> a = ef6.a(xe6);
            mc6.a aVar = mc6.Companion;
            a.resumeWith(mc6.m1constructorimpl(t));
        } else if (i == 1) {
            xl6.a(ef6.a(xe6), t);
        } else if (i == 2) {
            mc6.a aVar2 = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(t));
        } else if (i == 3) {
            af6 context = xe6.getContext();
            Object b = yo6.b(context, (Object) null);
            try {
                mc6.a aVar3 = mc6.Companion;
                xe6.resumeWith(mc6.m1constructorimpl(t));
                cd6 cd6 = cd6.a;
            } finally {
                yo6.a(context, b);
            }
        } else if (i != 4) {
            throw new IllegalStateException(("Invalid mode " + i).toString());
        }
    }
}
