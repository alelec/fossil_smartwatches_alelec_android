package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ki0 implements Parcelable.Creator<ek0> {
    @DexIgnore
    public /* synthetic */ ki0(qg6 qg6) {
    }

    @DexIgnore
    public ek0 createFromParcel(Parcel parcel) {
        return new ek0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ek0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m34createFromParcel(Parcel parcel) {
        return new ek0(parcel, (qg6) null);
    }
}
