package com.fossil;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk6 extends jm6 {
    @DexIgnore
    public static /* final */ int a;
    @DexIgnore
    public static boolean b;
    @DexIgnore
    public static /* final */ tk6 c; // = new tk6();
    @DexIgnore
    public static volatile Executor pool;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicInteger a;

        @DexIgnore
        public a(AtomicInteger atomicInteger) {
            this.a = atomicInteger;
        }

        @DexIgnore
        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "CommonPool-worker-" + this.a.incrementAndGet());
            thread.setDaemon(true);
            return thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void run() {
        }
    }

    /*
    static {
        String str;
        int i;
        try {
            str = System.getProperty("kotlinx.coroutines.default.parallelism");
        } catch (Throwable unused) {
            str = null;
        }
        if (str != null) {
            Integer b2 = wj6.b(str);
            if (b2 == null || b2.intValue() < 1) {
                throw new IllegalStateException(("Expected positive number in kotlinx.coroutines.default.parallelism, but has " + str).toString());
            }
            i = b2.intValue();
        } else {
            i = -1;
        }
        a = i;
    }
    */

    @DexIgnore
    public final synchronized Executor B() {
        Executor executor;
        executor = pool;
        if (executor == null) {
            executor = p();
            pool = executor;
        }
        return executor;
    }

    @DexIgnore
    public final int C() {
        Integer valueOf = Integer.valueOf(a);
        if (!(valueOf.intValue() > 0)) {
            valueOf = null;
        }
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return ci6.a(Runtime.getRuntime().availableProcessors() - 1, 1);
    }

    @DexIgnore
    public final boolean a(Class<?> cls, ExecutorService executorService) {
        Integer num;
        wg6.b(cls, "fjpClass");
        wg6.b(executorService, "executor");
        executorService.submit(b.a);
        try {
            Object invoke = cls.getMethod("getPoolSize", new Class[0]).invoke(executorService, new Object[0]);
            if (!(invoke instanceof Integer)) {
                invoke = null;
            }
            num = (Integer) invoke;
        } catch (Throwable unused) {
            num = null;
        }
        return num != null && num.intValue() >= 1;
    }

    @DexIgnore
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }

    @DexIgnore
    public final ExecutorService o() {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(C(), new a(new AtomicInteger()));
        wg6.a((Object) newFixedThreadPool, "Executors.newFixedThread\u2026Daemon = true }\n        }");
        return newFixedThreadPool;
    }

    @DexIgnore
    public final ExecutorService p() {
        Class<?> cls;
        ExecutorService executorService;
        ExecutorService executorService2;
        if (System.getSecurityManager() != null) {
            return o();
        }
        try {
            cls = Class.forName("java.util.concurrent.ForkJoinPool");
        } catch (Throwable unused) {
            cls = null;
        }
        if (cls == null) {
            return o();
        }
        if (!b && a < 0) {
            try {
                Method method = cls.getMethod("commonPool", new Class[0]);
                Object invoke = method != null ? method.invoke((Object) null, new Object[0]) : null;
                if (!(invoke instanceof ExecutorService)) {
                    invoke = null;
                }
                executorService2 = (ExecutorService) invoke;
            } catch (Throwable unused2) {
                executorService2 = null;
            }
            if (executorService2 != null) {
                if (!c.a(cls, executorService2)) {
                    executorService2 = null;
                }
                if (executorService2 != null) {
                    return executorService2;
                }
            }
        }
        try {
            Object newInstance = cls.getConstructor(new Class[]{Integer.TYPE}).newInstance(new Object[]{Integer.valueOf(c.C())});
            if (!(newInstance instanceof ExecutorService)) {
                newInstance = null;
            }
            executorService = (ExecutorService) newInstance;
        } catch (Throwable unused3) {
            executorService = null;
        }
        if (executorService != null) {
            return executorService;
        }
        return o();
    }

    @DexIgnore
    public String toString() {
        return "CommonPool";
    }

    @DexIgnore
    public void a(af6 af6, Runnable runnable) {
        Runnable runnable2;
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        try {
            Executor executor = pool;
            if (executor == null) {
                executor = B();
            }
            pn6 a2 = qn6.a();
            if (a2 == null || (runnable2 = a2.a(runnable)) == null) {
                runnable2 = runnable;
            }
            executor.execute(runnable2);
        } catch (RejectedExecutionException unused) {
            pn6 a3 = qn6.a();
            if (a3 != null) {
                a3.c();
            }
            pl6.g.a(runnable);
        }
    }
}
