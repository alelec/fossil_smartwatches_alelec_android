package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ml0 extends p41 {
    @DexIgnore
    public /* final */ byte[] E;
    @DexIgnore
    public byte[] F;
    @DexIgnore
    public /* final */ rg1 G;
    @DexIgnore
    public /* final */ rg1 H;
    @DexIgnore
    public /* final */ byte[] I;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ml0(tf1 tf1, lx0 lx0, ue1 ue1, int i, int i2) {
        super(lx0, ue1, (i2 & 8) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(tf1.c.length + 2).put(tf1.a.a()).put(tf1.b.a()).put(tf1.c).array();
        wg6.a(array, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.E = array;
        byte[] array2 = ByteBuffer.allocate(tf1.f.length + 2).put(tf1.d.a()).put(tf1.e.a()).put(tf1.f).array();
        wg6.a(array2, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.F = array2;
        rg1 rg1 = rg1.DC;
        this.G = rg1;
        this.H = rg1;
        this.I = new byte[0];
    }

    @DexIgnore
    public final long a(sg1 sg1) {
        return 0;
    }

    @DexIgnore
    public final sj0 a(byte b) {
        return jj1.f.a(b);
    }

    @DexIgnore
    public final rg1 m() {
        return this.H;
    }

    @DexIgnore
    public byte[] n() {
        return this.I;
    }

    @DexIgnore
    public final byte[] o() {
        return this.E;
    }

    @DexIgnore
    public final rg1 p() {
        return this.G;
    }

    @DexIgnore
    public final byte[] r() {
        return this.F;
    }
}
