package com.fossil;

import androidx.work.ListenableWorker;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cm {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public zn b;
    @DexIgnore
    public Set<String> c;

    @DexIgnore
    public cm(UUID uuid, zn znVar, Set<String> set) {
        this.a = uuid;
        this.b = znVar;
        this.c = set;
    }

    @DexIgnore
    public UUID a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.a.toString();
    }

    @DexIgnore
    public Set<String> c() {
        return this.c;
    }

    @DexIgnore
    public zn d() {
        return this.b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<B extends a, W extends cm> {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public UUID b; // = UUID.randomUUID();
        @DexIgnore
        public zn c;
        @DexIgnore
        public Set<String> d; // = new HashSet();

        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            this.c = new zn(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        @DexIgnore
        public final B a(nl nlVar) {
            this.c.j = nlVar;
            c();
            return this;
        }

        @DexIgnore
        public abstract W b();

        @DexIgnore
        public abstract B c();

        @DexIgnore
        public final B a(String str) {
            this.d.add(str);
            c();
            return this;
        }

        @DexIgnore
        public final W a() {
            W b2 = b();
            this.b = UUID.randomUUID();
            this.c = new zn(this.c);
            this.c.a = this.b.toString();
            return b2;
        }
    }
}
