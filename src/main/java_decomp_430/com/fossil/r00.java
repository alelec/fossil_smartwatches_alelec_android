package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r00 {
    @DexIgnore
    public static /* final */ char[] a; // = "0123456789abcdef".toCharArray();
    @DexIgnore
    public static /* final */ char[] b; // = new char[64];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[Bitmap.Config.ALPHA_8.ordinal()] = 1;
            a[Bitmap.Config.RGB_565.ordinal()] = 2;
            a[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            a[Bitmap.Config.RGBA_F16.ordinal()] = 4;
            a[Bitmap.Config.ARGB_8888.ordinal()] = 5;
        }
        */
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return (i2 * 31) + i;
    }

    @DexIgnore
    public static String a(byte[] bArr) {
        String a2;
        synchronized (b) {
            a2 = a(bArr, b);
        }
        return a2;
    }

    @DexIgnore
    public static boolean b(int i) {
        return i > 0 || i == Integer.MIN_VALUE;
    }

    @DexIgnore
    public static boolean b(int i, int i2) {
        return b(i) && b(i2);
    }

    @DexIgnore
    public static boolean c() {
        return !d();
    }

    @DexIgnore
    public static boolean d() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    @DexIgnore
    public static void b() {
        if (!d()) {
            throw new IllegalArgumentException("You must call this method on the main thread");
        }
    }

    @DexIgnore
    public static String a(byte[] bArr, char[] cArr) {
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i] & 255;
            int i2 = i * 2;
            char[] cArr2 = a;
            cArr[i2] = cArr2[b2 >>> 4];
            cArr[i2 + 1] = cArr2[b2 & 15];
        }
        return new String(cArr);
    }

    @DexIgnore
    public static boolean b(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @DexIgnore
    @TargetApi(19)
    public static int a(Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            if (Build.VERSION.SDK_INT >= 19) {
                try {
                    return bitmap.getAllocationByteCount();
                } catch (NullPointerException unused) {
                }
            }
            return bitmap.getHeight() * bitmap.getRowBytes();
        }
        throw new IllegalStateException("Cannot obtain size for recycled Bitmap: " + bitmap + "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig());
    }

    @DexIgnore
    public static int a(int i, int i2, Bitmap.Config config) {
        return i * i2 * a(config);
    }

    @DexIgnore
    public static int a(Bitmap.Config config) {
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        int i = a.a[config.ordinal()];
        if (i == 1) {
            return 1;
        }
        if (i == 2 || i == 3) {
            return 2;
        }
        return i != 4 ? 4 : 8;
    }

    @DexIgnore
    public static void a() {
        if (!c()) {
            throw new IllegalArgumentException("You must call this method on a background thread");
        }
    }

    @DexIgnore
    public static <T> Queue<T> a(int i) {
        return new ArrayDeque(i);
    }

    @DexIgnore
    public static <T> List<T> a(Collection<T> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (T next : collection) {
            if (next != null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        if (obj instanceof hv) {
            return ((hv) obj).a(obj2);
        }
        return obj.equals(obj2);
    }

    @DexIgnore
    public static int a(float f) {
        return a(f, 17);
    }

    @DexIgnore
    public static int a(float f, int i) {
        return a(Float.floatToIntBits(f), i);
    }

    @DexIgnore
    public static int a(Object obj, int i) {
        return a(obj == null ? 0 : obj.hashCode(), i);
    }

    @DexIgnore
    public static int a(boolean z, int i) {
        return a(z ? 1 : 0, i);
    }
}
