package com.fossil;

import com.fossil.k40;
import com.fossil.q40;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ii1 a;

    @DexIgnore
    public tj1(ii1 ii1) {
        this.a = ii1;
    }

    @DexIgnore
    public final void run() {
        if (this.a.s == q40.c.DISCONNECTED && k40.l.d() == k40.c.ENABLED) {
            fm0 fm0 = fm0.f;
            if1[] b = this.a.f.b();
            int length = b.length;
            int i = 0;
            while (i < length && !(!(b[i] instanceof ub1))) {
                i++;
            }
            fm0.f();
            kj0.i.a(false);
            this.a.b((HashMap<d41, Object>) he6.a(new lc6[]{qc6.a(d41.AUTO_CONNECT, false), qc6.a(d41.CONNECTION_TIME_OUT, 0L)}));
        }
    }
}
