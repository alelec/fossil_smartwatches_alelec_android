package com.fossil;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class fr3 implements zp3 {
    @DexIgnore
    public static /* final */ zp3 a; // = new fr3();

    @DexIgnore
    public final Object a(xp3 xp3) {
        return new FirebaseInstanceId((FirebaseApp) xp3.a(FirebaseApp.class), (rq3) xp3.a(rq3.class), (xt3) xp3.a(xt3.class));
    }
}
