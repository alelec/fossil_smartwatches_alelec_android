package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.fossil.fs;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ts implements fs<InputStream> {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ vs b;
    @DexIgnore
    public InputStream c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements us {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, (String) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements us {
        @DexIgnore
        public static /* final */ String[] b; // = {"_data"};
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public Cursor a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, (String) null);
        }
    }

    @DexIgnore
    public ts(Uri uri, vs vsVar) {
        this.a = uri;
        this.b = vsVar;
    }

    @DexIgnore
    public static ts a(Context context, Uri uri) {
        return a(context, uri, new a(context.getContentResolver()));
    }

    @DexIgnore
    public static ts b(Context context, Uri uri) {
        return a(context, uri, new b(context.getContentResolver()));
    }

    @DexIgnore
    public final InputStream c() throws FileNotFoundException {
        InputStream c2 = this.b.c(this.a);
        int a2 = c2 != null ? this.b.a(this.a) : -1;
        return a2 != -1 ? new is(c2, a2) : c2;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @DexIgnore
    public static ts a(Context context, Uri uri, us usVar) {
        return new ts(uri, new vs(wq.a(context).g().a(), usVar, wq.a(context).b(), context.getContentResolver()));
    }

    @DexIgnore
    public pr b() {
        return pr.LOCAL;
    }

    @DexIgnore
    public void a(br brVar, fs.a<? super InputStream> aVar) {
        try {
            this.c = c();
            aVar.a(this.c);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            aVar.a((Exception) e);
        }
    }

    @DexIgnore
    public void a() {
        InputStream inputStream = this.c;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }
}
