package com.fossil;

import android.os.DeadObjectException;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vg2<T extends IInterface> {
    @DexIgnore
    void a();

    @DexIgnore
    T b() throws DeadObjectException;
}
