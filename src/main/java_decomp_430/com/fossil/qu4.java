package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu4 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public List<lc6<MicroApp, String>> a;
    @DexIgnore
    public List<lc6<MicroApp, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public e d;
    @DexIgnore
    public d e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qu4 qu4, View view) {
            super(view);
            wg6.b(view, "itemView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends a {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qu4 qu4, View view) {
            super(qu4, view);
            wg6.b(view, "itemView");
            View findViewById = view.findViewById(2131363199);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(MicroApp microApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends a {
        @DexIgnore
        public /* final */ CustomizeWidget a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public MicroApp d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ /* synthetic */ qu4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ f a;

            @DexIgnore
            public a(f fVar) {
                this.a = fVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                MicroApp a2 = this.a.a();
                if (a2 != null) {
                    e a3 = this.a.f.d;
                    if (a3 != null) {
                        a3.a(a2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(qu4.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(qu4.f, "itemClick(), MicroApp tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qu4 qu4, View view) {
            super(qu4, view);
            wg6.b(view, "itemView");
            this.f = qu4;
            View findViewById = view.findViewById(2131363336);
            wg6.a((Object) findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363186);
            wg6.a((Object) findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            this.c = (FlexibleTextView) view.findViewById(2131363078);
            ConstraintLayout findViewById3 = view.findViewById(2131362851);
            wg6.a((Object) findViewById3, "itemView.findViewById(R.id.root_background)");
            this.e = findViewById3;
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                this.e.setBackgroundColor(Color.parseColor(b2));
            }
            view.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final MicroApp a() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.a;
        }

        @DexIgnore
        public final void a(MicroApp microApp) {
            this.d = microApp;
        }
    }

    /*
    static {
        new b((qg6) null);
        String name = qu4.class.getName();
        wg6.a((Object) name, "SearchMicroAppAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    public final void b(List<lc6<MicroApp, String>> list) {
        d dVar;
        this.a = list;
        List<lc6<MicroApp, String>> list2 = this.a;
        if (!(list2 == null || !list2.isEmpty() || (dVar = this.e) == null)) {
            dVar.a(this.c);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getItemCount() {
        List<lc6<MicroApp, String>> list = this.a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    public int getItemViewType(int i) {
        return (this.a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final void a(List<lc6<MicroApp, String>> list) {
        wg6.b(list, ServerSetting.VALUE);
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558649, viewGroup, false);
            wg6.a((Object) inflate, "view");
            return new c(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558648, viewGroup, false);
        wg6.a((Object) inflate2, "view");
        return new f(this, inflate2);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r9v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r9v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        if (!(aVar instanceof c)) {
            f fVar = (f) aVar;
            List<lc6<MicroApp, String>> list = this.a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty((CharSequence) this.b.get(i2).getSecond())) {
                        Object b2 = fVar.b();
                        wg6.a((Object) b2, "resultSearchViewHolder.tvAssignedTo");
                        b2.setVisibility(0);
                        Object b3 = fVar.b();
                        wg6.a((Object) b3, "resultSearchViewHolder.tvAssignedTo");
                        b3.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886347));
                    } else {
                        Object b4 = fVar.b();
                        wg6.a((Object) b4, "resultSearchViewHolder.tvAssignedTo");
                        b4.setVisibility(8);
                    }
                    fVar.d().c(((MicroApp) this.b.get(i2).getFirst()).getId());
                    fVar.c().setText(jm4.a(PortfolioApp.get.instance(), ((MicroApp) this.b.get(i2).getFirst()).getNameKey(), ((MicroApp) this.b.get(i2).getFirst()).getName()));
                    fVar.a((MicroApp) this.b.get(i2).getFirst());
                    return;
                }
                fVar.a((MicroApp) null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty((CharSequence) list.get(i).getSecond())) {
                    Object b5 = fVar.b();
                    wg6.a((Object) b5, "resultSearchViewHolder.tvAssignedTo");
                    b5.setVisibility(0);
                    Object b6 = fVar.b();
                    wg6.a((Object) b6, "resultSearchViewHolder.tvAssignedTo");
                    b6.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886347));
                } else {
                    Object b7 = fVar.b();
                    wg6.a((Object) b7, "resultSearchViewHolder.tvAssignedTo");
                    b7.setVisibility(8);
                }
                fVar.d().c(((MicroApp) list.get(i).getFirst()).getId());
                String a2 = jm4.a(PortfolioApp.get.instance(), ((MicroApp) list.get(i).getFirst()).getNameKey(), ((MicroApp) list.get(i).getFirst()).getName());
                Object c2 = fVar.c();
                wg6.a((Object) a2, "name");
                c2.setText(a(a2, this.c));
                fVar.a((MicroApp) list.get(i).getFirst());
            } else {
                fVar.a((MicroApp) null);
            }
        } else if (this.b.isEmpty()) {
            ((c) aVar).a().setVisibility(4);
        } else {
            ((c) aVar).a().setVisibility(0);
        }
    }

    @DexIgnore
    public final SpannableString a(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                mj6 mj6 = new mj6(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    ti6<kj6> findAll$default = mj6.findAll$default(mj6, lowerCase, 0, 2, (Object) null);
                    SpannableString spannableString = new SpannableString(str);
                    for (kj6 kj6 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), kj6.a().e().intValue(), kj6.a().e().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    public final void a(e eVar) {
        wg6.b(eVar, "listener");
        this.d = eVar;
    }

    @DexIgnore
    public final void a(d dVar) {
        wg6.b(dVar, "listener");
        this.e = dVar;
    }
}
