package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t26 extends m26 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    public t26(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        this.c = bundle.getString("_wxapi_getmessage_req_lang");
        this.d = bundle.getString("_wxapi_getmessage_req_country");
    }

    @DexIgnore
    public boolean a() {
        return true;
    }

    @DexIgnore
    public int b() {
        return 3;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        super.b(bundle);
        bundle.putString("_wxapi_getmessage_req_lang", this.c);
        bundle.putString("_wxapi_getmessage_req_country", this.d);
    }
}
