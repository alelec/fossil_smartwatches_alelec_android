package com.fossil;

import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk5 implements MembersInjector<ProfileEditFragment> {
    @DexIgnore
    public static void a(ProfileEditFragment profileEditFragment, w04 w04) {
        profileEditFragment.o = w04;
    }

    @DexIgnore
    public static void a(ProfileEditFragment profileEditFragment, tr5 tr5) {
        profileEditFragment.p = tr5;
    }
}
