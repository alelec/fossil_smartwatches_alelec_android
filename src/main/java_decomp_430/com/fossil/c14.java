package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c14 implements Factory<rj4> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<an4> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> d;

    @DexIgnore
    public c14(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static c14 a(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        return new c14(b14, provider, provider2, provider3);
    }

    @DexIgnore
    public static rj4 b(b14 b14, Provider<an4> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        return a(b14, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static AlarmHelper a(b14 b14, an4 an4, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        AlarmHelper a2 = b14.a(an4, userRepository, alarmsRepository);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public AlarmHelper get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
