package com.fossil;

import android.os.Looper;
import com.fossil.c12;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qx1 implements c12.c {
    @DexIgnore
    public /* final */ WeakReference<ox1> a;
    @DexIgnore
    public /* final */ rv1<?> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public qx1(ox1 ox1, rv1<?> rv1, boolean z) {
        this.a = new WeakReference<>(ox1);
        this.b = rv1;
        this.c = z;
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        ox1 ox1 = (ox1) this.a.get();
        if (ox1 != null) {
            w12.b(Looper.myLooper() == ox1.a.r.f(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            ox1.b.lock();
            try {
                if (ox1.a(0)) {
                    if (!gv1.E()) {
                        ox1.b(gv1, this.b, this.c);
                    }
                    if (ox1.d()) {
                        ox1.e();
                    }
                    ox1.b.unlock();
                }
            } finally {
                ox1.b.unlock();
            }
        }
    }
}
