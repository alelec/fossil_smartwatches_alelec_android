package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.chart.WeekHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pa4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ WeekHeartRateChart q;

    @DexIgnore
    public pa4(Object obj, View view, int i, WeekHeartRateChart weekHeartRateChart) {
        super(obj, view, i);
        this.q = weekHeartRateChart;
    }
}
