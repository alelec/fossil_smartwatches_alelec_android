package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hd extends Service implements LifecycleOwner {
    @DexIgnore
    public /* final */ rd a; // = new rd(this);

    @DexIgnore
    public Lifecycle getLifecycle() {
        return this.a.a();
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        this.a.b();
        return null;
    }

    @DexIgnore
    public void onCreate() {
        this.a.c();
        super.onCreate();
    }

    @DexIgnore
    public void onDestroy() {
        this.a.d();
        super.onDestroy();
    }

    @DexIgnore
    public void onStart(Intent intent, int i) {
        this.a.e();
        super.onStart(intent, i);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        return super.onStartCommand(intent, i, i2);
    }
}
