package com.fossil;

import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.service.usecase.DianaSyncDataProcessing;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1", f = "DianaSyncDataProcessing.kt", l = {256}, m = "invokeSuspend")
public final class nq4$g$a extends sf6 implements ig6<il6, xe6<? super ActivityStatistic>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $fitnessDataList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncDataProcessing.g this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1$1", f = "DianaSyncDataProcessing.kt", l = {257, 258, 260, 261, 263, 264, 266, 267}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $startDate;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nq4$g$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(nq4$g$a nq4_g_a, jh6 jh6, jh6 jh62, xe6 xe6) {
            super(2, xe6);
            this.this$0 = nq4_g_a;
            this.$startDate = jh6;
            this.$endDate = jh62;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$startDate, this.$endDate, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0088, code lost:
            r12 = r11.this$0.this$0.$summaryRepository;
            r3 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$startDate.element, true);
            r4 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$endDate.element, true);
            r11.L$0 = r1;
            r11.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x00ab, code lost:
            if (r12.loadSummaries(r3, r4, r11) != r0) goto L_0x00ae;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x00ad, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ae, code lost:
            r3 = r11.this$0.this$0.$sleepSessionsRepository;
            r4 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$startDate.element, true);
            r5 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$endDate.element, true);
            r11.L$0 = r1;
            r11.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x00d7, code lost:
            if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x00da;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00d9, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00da, code lost:
            r12 = r11.this$0.this$0.$sleepSummariesRepository;
            r3 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$startDate.element, true);
            r4 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$endDate.element, true);
            r11.L$0 = r1;
            r11.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00fd, code lost:
            if (r12.fetchSleepSummaries(r3, r4, r11) != r0) goto L_0x0100;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00ff, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0100, code lost:
            r3 = r11.this$0.this$0.$heartRateSampleRepository;
            r4 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$startDate.element, true);
            r5 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$endDate.element, true);
            r11.L$0 = r1;
            r11.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0129, code lost:
            if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x012c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x012b, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x012c, code lost:
            r12 = r11.this$0.this$0.$heartRateSummaryRepository;
            r3 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$startDate.element, true);
            r4 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$endDate.element, true);
            r11.L$0 = r1;
            r11.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x014f, code lost:
            if (r12.loadSummaries(r3, r4, r11) != r0) goto L_0x0152;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0151, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0152, code lost:
            r3 = r11.this$0.this$0.$workoutSessionRepository;
            r4 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$startDate.element, true);
            r5 = com.fossil.SyncDataExtensions.a((org.joda.time.DateTime) r11.$endDate.element, true);
            r11.L$0 = r1;
            r11.label = 7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x017b, code lost:
            if (com.portfolio.platform.data.source.WorkoutSessionRepository.fetchWorkoutSessions$default(r3, r4, r5, 0, 0, r11, 12, (java.lang.Object) null) != r0) goto L_0x017e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x017d, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x017e, code lost:
            r12 = r11.this$0.this$0.$summaryRepository;
            r11.L$0 = r1;
            r11.label = 8;
            r12 = r12.fetchActivityStatistic(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x018e, code lost:
            if (r12 != r0) goto L_0x0191;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0190, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0191, code lost:
            return r12;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il6 = this.p$;
                    ActivitiesRepository activitiesRepository = this.this$0.this$0.$activityRepository;
                    Date a2 = SyncDataExtensions.a((DateTime) this.$startDate.element, true);
                    Date a3 = SyncDataExtensions.a((DateTime) this.$endDate.element, true);
                    this.L$0 = il6;
                    this.label = 1;
                    if (ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, a2, a3, 0, 0, this, 12, (Object) null) == a) {
                        return a;
                    }
                    break;
                case 1:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 4:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 5:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 6:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 7:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 8:
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nq4$g$a(DianaSyncDataProcessing.g gVar, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
        this.$fitnessDataList = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        nq4$g$a nq4_g_a = new nq4$g$a(this.this$0, this.$fitnessDataList, xe6);
        nq4_g_a.p$ = (il6) obj;
        return nq4_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((nq4$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            jh6 jh6 = new jh6();
            jh6.element = ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getStartTimeTZ();
            jh6 jh62 = new jh6();
            jh62.element = ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getEndTimeTZ();
            for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                if (fitnessDataWrapper.getStartLongTime() < ((DateTime) jh6.element).getMillis()) {
                    jh6.element = fitnessDataWrapper.getStartTimeTZ();
                }
                if (fitnessDataWrapper.getEndLongTime() > ((DateTime) jh62.element).getMillis()) {
                    jh62.element = fitnessDataWrapper.getEndTimeTZ();
                }
            }
            dl6 b = zl6.b();
            a aVar = new a(this, jh6, jh62, (xe6) null);
            this.L$0 = il6;
            this.L$1 = jh6;
            this.L$2 = jh62;
            this.label = 1;
            obj = gk6.a(b, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            jh6 jh63 = (jh6) this.L$2;
            jh6 jh64 = (jh6) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
