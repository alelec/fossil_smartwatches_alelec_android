package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import com.facebook.internal.FileLruCache;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq0 {
    @DexIgnore
    public static /* final */ String a; // = ze0.a("UUID.randomUUID().toString()");
    @DexIgnore
    public static /* final */ Hashtable<String, r40> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> c; // = new Hashtable<>();
    @DexIgnore
    public static bw0 d; // = new bw0(gk0.f.b(), gk0.f.c(), gk0.f.e(), gk0.f.d(), "5.10.2-production-release", (String) null, 32);
    @DexIgnore
    public static /* final */ xq0 e; // = new xq0();

    /*
    static {
        String string;
        SharedPreferences a2 = cw0.a(lp0.SDK_LOG_PREFERENCE);
        if (a2 != null && (string = a2.getString("log.device.mapping", (String) null)) != null) {
            wg6.a(string, "getSharedPreferences(Sha\u2026_MAP_KEY, null) ?: return");
            String decryptAES_CBC = Crypto.instance.decryptAES_CBC(string);
            if (decryptAES_CBC != null) {
                try {
                    JSONObject jSONObject = new JSONObject(decryptAES_CBC);
                    b.clear();
                    b.putAll(e.a(jSONObject));
                } catch (Exception e2) {
                    qs0.h.a(e2);
                }
            }
        }
    }
    */

    @DexIgnore
    public final bw0 a() {
        return d;
    }

    @DexIgnore
    public final String b(String str) {
        String str2 = c.get(str);
        return str2 != null ? str2 : a;
    }

    @DexIgnore
    public final r40 a(String str) {
        return b.get(str);
    }

    @DexIgnore
    public final void a(String str, r40 r40) {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putString;
        r40 r402 = b.get(str);
        Hashtable<String, r40> hashtable = b;
        if (r402 != null) {
            r40 = r40.t.a(r402, r40);
        }
        hashtable.put(str, r40);
        Hashtable<String, r40> hashtable2 = b;
        JSONObject jSONObject = new JSONObject();
        for (String str2 : hashtable2.keySet()) {
            r40 r403 = hashtable2.get(str2);
            if (r403 != null) {
                jSONObject.put(str2, r403.a());
            }
        }
        String jSONObject2 = jSONObject.toString();
        wg6.a(jSONObject2, "convertDeviceInfoMapToJS\u2026formationMaps).toString()");
        String encryptAES_CBC = Crypto.instance.encryptAES_CBC(jSONObject2);
        SharedPreferences a2 = cw0.a(lp0.SDK_LOG_PREFERENCE);
        if (a2 != null && (edit = a2.edit()) != null && (putString = edit.putString("log.device.mapping", encryptAES_CBC)) != null) {
            putString.apply();
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        c.put(str, str2);
    }

    @DexIgnore
    public final HashMap<String, r40> a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        r40 a2;
        HashMap<String, r40> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        wg6.a(keys, "jsonObject.keys()");
        while (keys.hasNext()) {
            String next = keys.next();
            if (!(!BluetoothAdapter.checkBluetoothAddress(next) || (optJSONObject = jSONObject.optJSONObject(next)) == null || (a2 = r40.t.a(optJSONObject)) == null)) {
                wg6.a(next, FileLruCache.HEADER_CACHEKEY_KEY);
                hashMap.put(next, a2);
            }
        }
        return hashMap;
    }
}
