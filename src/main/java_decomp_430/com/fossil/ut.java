package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ut {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Handler.Callback {
        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((rt) message.obj).a();
            return true;
        }
    }

    @DexIgnore
    public synchronized void a(rt<?> rtVar, boolean z) {
        if (!this.a) {
            if (!z) {
                this.a = true;
                rtVar.a();
                this.a = false;
            }
        }
        this.b.obtainMessage(1, rtVar).sendToTarget();
    }
}
