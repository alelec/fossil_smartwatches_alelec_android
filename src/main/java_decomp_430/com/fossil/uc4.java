package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uc4 extends tc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131362561, 1);
        z.put(2131362820, 2);
        z.put(2131362442, 3);
        z.put(2131362879, 4);
        z.put(2131362334, 5);
        z.put(2131362227, 6);
    }
    */

    @DexIgnore
    public uc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public uc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[6], objArr[5], objArr[3], objArr[1], objArr[2], objArr[0], objArr[4]);
        this.x = -1;
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
