package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lx4 implements Factory<ij5> {
    @DexIgnore
    public static ij5 a(ex4 ex4) {
        ij5 g = ex4.g();
        z76.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }
}
