package com.fossil;

import com.portfolio.platform.util.NetworkBoundResource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.util.NetworkBoundResource$fetchFromNetwork$1$1", f = "NetworkBoundResource.kt", l = {}, m = "invokeSuspend")
public final class tx5$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NetworkBoundResource.b this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ld<S> {
        @DexIgnore
        public /* final */ /* synthetic */ tx5$b$a a;

        @DexIgnore
        public a(tx5$b$a tx5_b_a) {
            this.a = tx5_b_a;
        }

        @DexIgnore
        public final void onChanged(ResultType resulttype) {
            this.a.this$0.this$0.setValue(yx5.e.b(resulttype));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tx5$b$a(NetworkBoundResource.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        tx5$b$a tx5_b_a = new tx5$b$a(this.this$0, xe6);
        tx5_b_a.p$ = (il6) obj;
        return tx5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((tx5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.result.a(this.this$0.$dbSource, new a(this));
            this.this$0.this$0.result.a(this.this$0.$dbSource);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
