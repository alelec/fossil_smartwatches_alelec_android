package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eq3 extends tp3 {
    @DexIgnore
    public static /* final */ ys3<Set<Object>> e; // = dq3.a();
    @DexIgnore
    public /* final */ Map<wp3<?>, lq3<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, lq3<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, lq3<Set<?>>> c; // = new HashMap();
    @DexIgnore
    public /* final */ kq3 d;

    @DexIgnore
    public eq3(Executor executor, Iterable<aq3> iterable, wp3<?>... wp3Arr) {
        this.d = new kq3(executor);
        ArrayList<wp3> arrayList = new ArrayList<>();
        arrayList.add(wp3.a(this.d, kq3.class, rq3.class, qq3.class));
        for (aq3 components : iterable) {
            arrayList.addAll(components.getComponents());
        }
        Collections.addAll(arrayList, wp3Arr);
        fq3.a((List<wp3<?>>) arrayList);
        for (wp3 wp3 : arrayList) {
            this.a.put(wp3, new lq3(bq3.a(this, wp3)));
        }
        a();
        b();
    }

    @DexIgnore
    public final void b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry next : this.a.entrySet()) {
            wp3 wp3 = (wp3) next.getKey();
            if (!wp3.g()) {
                lq3 lq3 = (lq3) next.getValue();
                for (Class cls : wp3.c()) {
                    if (!hashMap.containsKey(cls)) {
                        hashMap.put(cls, new HashSet());
                    }
                    ((Set) hashMap.get(cls)).add(lq3);
                }
            }
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            this.c.put((Class) entry.getKey(), new lq3(cq3.a((Set) entry.getValue())));
        }
    }

    @DexIgnore
    public <T> ys3<Set<T>> c(Class<T> cls) {
        lq3 lq3 = this.c.get(cls);
        if (lq3 != null) {
            return lq3;
        }
        return e;
    }

    @DexIgnore
    public final void a() {
        for (Map.Entry next : this.a.entrySet()) {
            wp3 wp3 = (wp3) next.getKey();
            if (wp3.g()) {
                lq3 lq3 = (lq3) next.getValue();
                for (Class put : wp3.c()) {
                    this.b.put(put, lq3);
                }
            }
        }
        c();
    }

    @DexIgnore
    public final void c() {
        for (wp3 next : this.a.keySet()) {
            Iterator<gq3> it = next.a().iterator();
            while (true) {
                if (it.hasNext()) {
                    gq3 next2 = it.next();
                    if (next2.c() && !this.b.containsKey(next2.a())) {
                        throw new mq3(String.format("Unsatisfied dependency for component %s: %s", new Object[]{next, next2.a()}));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ Set a(Set set) {
        HashSet hashSet = new HashSet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            hashSet.add(((lq3) it.next()).get());
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public void a(boolean z) {
        for (Map.Entry next : this.a.entrySet()) {
            wp3 wp3 = (wp3) next.getKey();
            lq3 lq3 = (lq3) next.getValue();
            if (wp3.e() || (wp3.f() && z)) {
                lq3.get();
            }
        }
        this.d.a();
    }

    @DexIgnore
    public <T> ys3<T> b(Class<T> cls) {
        w12.a(cls, (Object) "Null interface requested.");
        return this.b.get(cls);
    }
}
