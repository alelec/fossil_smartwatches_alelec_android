package com.fossil;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga extends ClickableSpan {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ia b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public ga(int i, ia iaVar, int i2) {
        this.a = i;
        this.b = iaVar;
        this.c = i2;
    }

    @DexIgnore
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.a);
        this.b.a(this.c, bundle);
    }
}
