package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.ShakeFeedbackService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vp4 implements Factory<up4> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public vp4(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static vp4 a(Provider<UserRepository> provider) {
        return new vp4(provider);
    }

    @DexIgnore
    public static up4 b(Provider<UserRepository> provider) {
        return new ShakeFeedbackService(provider.get());
    }

    @DexIgnore
    public ShakeFeedbackService get() {
        return b(this.a);
    }
}
