package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jp4 implements Factory<ip4> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<zm4> b;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> c;
    @DexIgnore
    public /* final */ Provider<an4> d;

    @DexIgnore
    public jp4(Provider<PortfolioApp> provider, Provider<zm4> provider2, Provider<AuthApiGuestService> provider3, Provider<an4> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static jp4 a(Provider<PortfolioApp> provider, Provider<zm4> provider2, Provider<AuthApiGuestService> provider3, Provider<an4> provider4) {
        return new jp4(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static ip4 b(Provider<PortfolioApp> provider, Provider<zm4> provider2, Provider<AuthApiGuestService> provider3, Provider<an4> provider4) {
        return new ip4(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public ip4 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
