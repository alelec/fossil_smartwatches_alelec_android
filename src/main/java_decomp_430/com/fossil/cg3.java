package com.fossil;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Property;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cg3 extends Property<Drawable, Integer> {
    @DexIgnore
    public static /* final */ Property<Drawable, Integer> b; // = new cg3();
    @DexIgnore
    public /* final */ WeakHashMap<Drawable, Integer> a; // = new WeakHashMap<>();

    @DexIgnore
    public cg3() {
        super(Integer.class, "drawableAlphaCompat");
    }

    @DexIgnore
    /* renamed from: a */
    public Integer get(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 19) {
            return Integer.valueOf(drawable.getAlpha());
        }
        if (this.a.containsKey(drawable)) {
            return this.a.get(drawable);
        }
        return 255;
    }

    @DexIgnore
    /* renamed from: a */
    public void set(Drawable drawable, Integer num) {
        if (Build.VERSION.SDK_INT < 19) {
            this.a.put(drawable, num);
        }
        drawable.setAlpha(num.intValue());
    }
}
