package com.fossil;

import android.text.TextUtils;
import com.fossil.tj4;
import com.fossil.yq6;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mp4 implements bq6 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((qg6) null);
    @DexIgnore
    public AtomicInteger b; // = new AtomicInteger(0);
    @DexIgnore
    public Auth c;
    @DexIgnore
    public /* final */ zm4 d;
    @DexIgnore
    public /* final */ AuthApiGuestService e;
    @DexIgnore
    public /* final */ an4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mp4.g;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = mp4.class.getSimpleName();
        wg6.a((Object) simpleName, "TokenAuthenticator::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public mp4(zm4 zm4, AuthApiGuestService authApiGuestService, an4 an4) {
        wg6.b(zm4, "mProviderManager");
        wg6.b(authApiGuestService, "mAuthApiGuestService");
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = zm4;
        this.e = authApiGuestService;
        this.f = an4;
    }

    @DexIgnore
    public yq6 authenticate(ar6 ar6, Response response) {
        String str;
        String str2;
        String accessToken;
        wg6.b(response, "response");
        MFUser b2 = this.d.n().b();
        if ((b2 != null ? b2.getUserAccessToken() : null) == null || TextUtils.isEmpty(b2.getUserAccessToken())) {
            FLogger.INSTANCE.getLocal().d(g, "unauthorized: userAccessToken is null or empty");
            return null;
        }
        int i = 0;
        boolean z = this.b.getAndIncrement() == 0;
        this.c = null;
        rx6<Auth> a2 = a(b2, response, z);
        Auth auth = this.c;
        if (this.b.decrementAndGet() == 0) {
            this.c = null;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local.d(str3, "authenticate decrementAndGet mCount=" + this.b + " address=" + this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = g;
        local2.d(str4, "old headers=" + response.I().c().toString());
        if (auth != null && (accessToken = auth.getAccessToken()) != null) {
            b2.setUserAccessToken(accessToken);
            b2.setRefreshToken(auth.getRefreshToken());
            b2.setAccessTokenExpiresAt(bk4.u(auth.getAccessTokenExpiresAt()));
            Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
            if (accessTokenExpiresIn != null) {
                i = accessTokenExpiresIn.intValue();
            }
            b2.setAccessTokenExpiresIn(Integer.valueOf(i));
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str5 = g;
            local3.d(str5, "accessToken = " + accessToken + ", refreshToken = " + auth.getRefreshToken() + ", userOnboardingComplete=" + b2.isOnboardingComplete());
            this.f.w(accessToken);
            this.f.a(System.currentTimeMillis());
            this.d.n().a(b2);
            yq6.a f2 = response.I().f();
            f2.b("Authorization", "Bearer " + accessToken);
            return f2.a();
        } else if (a2 == null || a2.b() < 500 || a2.b() >= 600) {
            if (z && a2 != null) {
                try {
                    Gson gson = new Gson();
                    zq6 k = a2.f().k();
                    if (k == null || (str = k.string()) == null) {
                        str = a2.e();
                    }
                    ServerError serverError = (ServerError) gson.a(str, ServerError.class);
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str6 = g;
                    StringBuilder sb = new StringBuilder();
                    sb.append("forceLogout userMessage=");
                    wg6.a((Object) serverError, "serverError");
                    sb.append(serverError.getUserMessage());
                    local4.d(str6, sb.toString());
                    PortfolioApp.get.instance().a(serverError);
                } catch (Exception e2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str7 = g;
                    local5.d(str7, "forceLogout ex=" + e2.getMessage());
                    e2.printStackTrace();
                    PortfolioApp.get.instance().a((ServerError) null);
                }
            }
            return null;
        } else {
            ServerError serverError2 = new ServerError();
            serverError2.setCode(Integer.valueOf(a2.b()));
            zq6 c2 = a2.c();
            if (c2 == null || (str2 = c2.string()) == null) {
                str2 = a2.e();
            }
            serverError2.setMessage(str2);
            throw new ServerErrorException(serverError2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00bf, code lost:
        r1 = (r1 = r1.d()).a("_error");
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a3 A[SYNTHETIC, Splitter:B:54:0x01a3] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x020d A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0216 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0221 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0226 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0233 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x023a A[Catch:{ Exception -> 0x023f }] */
    public final synchronized rx6<Auth> a(MFUser mFUser, Response response, boolean z) {
        rx6<Auth> rx6;
        rx6<Auth> s;
        ku3 d2;
        JsonElement a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "getAuth needProcess=" + z + " mCount=" + this.b);
        rx6 = null;
        if (z) {
            if (TextUtils.isEmpty(mFUser.getRefreshToken())) {
                ku3 ku3 = new ku3();
                FLogger.INSTANCE.getLocal().d(g, "Exchange Legacy Token");
                ku3.a(Constants.PROFILE_KEY_ACCESS_TOKEN, mFUser.getUserAccessToken());
                tj4.a aVar = tj4.f;
                String userId = mFUser.getUserId();
                wg6.a((Object) userId, "user.userId");
                ku3.a("clientId", aVar.a(userId));
                this.c = (Auth) this.e.tokenExchangeLegacy(ku3).s().a();
            } else {
                zq6 k = response.k();
                if (k != null) {
                    StringBuilder sb = new StringBuilder();
                    String readLine = new BufferedReader(new InputStreamReader(k.byteStream())).readLine();
                    int length = readLine.length();
                    for (int i = 0; i < length; i++) {
                        sb.append(readLine.charAt(i));
                    }
                    String sb2 = sb.toString();
                    wg6.a((Object) sb2, "sb.toString()");
                    JsonElement a3 = new mu3().a(sb2);
                    Integer valueOf = (a3 == null || d2 == null || a2 == null) ? null : Integer.valueOf(a2.b());
                    if (valueOf != null) {
                        if (valueOf.intValue() == 401026) {
                            FLogger.INSTANCE.getLocal().d(g, "Exchange Legacy Token failed");
                        }
                    }
                    if (valueOf == null) {
                        if (valueOf != null) {
                            if (valueOf.intValue() == 401005) {
                                FLogger.INSTANCE.getLocal().d(g, "Token is in black list");
                            }
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = g;
                        local2.d(str2, "unauthorized: error = " + sb2);
                        FLogger.INSTANCE.getLocal().d(g, "Refresh Token");
                        ku3 ku32 = new ku3();
                        ku32.a("refreshToken", mFUser.getRefreshToken());
                        try {
                            s = this.e.tokenRefresh(ku32).s();
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = g;
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Refresh Token error=");
                            sb3.append(s == null ? Integer.valueOf(s.b()) : null);
                            sb3.append(" body=");
                            sb3.append(s == null ? s.c() : null);
                            local3.d(str3, sb3.toString());
                            this.c = s == null ? (Auth) s.a() : null;
                        } catch (Exception e2) {
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String a4 = ip4.h.a();
                            local4.d(a4, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e2.getMessage());
                            e2.printStackTrace();
                            return null;
                        }
                    } else {
                        if (valueOf.intValue() == 401011) {
                            FLogger.INSTANCE.getLocal().d(g, "Refresh Token");
                            ku3 ku33 = new ku3();
                            ku33.a("refreshToken", mFUser.getRefreshToken());
                            try {
                                s = this.e.tokenRefresh(ku33).s();
                                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                String str4 = g;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("Refresh Token error=");
                                sb4.append(s != null ? Integer.valueOf(s.b()) : null);
                                sb4.append(" body=");
                                sb4.append(s != null ? s.c() : null);
                                local5.d(str4, sb4.toString());
                                this.c = s != null ? (Auth) s.a() : null;
                            } catch (Exception e3) {
                                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                String str5 = g;
                                local6.d(str5, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e3.getMessage());
                                e3.printStackTrace();
                                return null;
                            }
                        }
                        if (valueOf != null) {
                        }
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        String str22 = g;
                        local22.d(str22, "unauthorized: error = " + sb2);
                        FLogger.INSTANCE.getLocal().d(g, "Refresh Token");
                        ku3 ku322 = new ku3();
                        ku322.a("refreshToken", mFUser.getRefreshToken());
                        s = this.e.tokenRefresh(ku322).s();
                        ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                        String str32 = g;
                        StringBuilder sb32 = new StringBuilder();
                        sb32.append("Refresh Token error=");
                        sb32.append(s == null ? Integer.valueOf(s.b()) : null);
                        sb32.append(" body=");
                        sb32.append(s == null ? s.c() : null);
                        local32.d(str32, sb32.toString());
                        this.c = s == null ? (Auth) s.a() : null;
                    }
                    rx6 = s;
                }
            }
        }
        return rx6;
    }
}
