package com.fossil;

import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gi2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ int e; // = 5;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ Object h;
    @DexIgnore
    public /* final */ /* synthetic */ Object i;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gi2(ov2 ov2, boolean z, int i2, String str, Object obj, Object obj2, Object obj3) {
        super(false);
        this.j = ov2;
        this.f = str;
        this.g = obj;
        this.h = null;
        this.i = null;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.j.g.logHealthData(this.e, this.f, z52.a(this.g), z52.a(this.h), z52.a(this.i));
    }
}
