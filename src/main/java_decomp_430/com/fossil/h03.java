package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h03 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> a; // = this.b.a.keySet().iterator();
    @DexIgnore
    public /* final */ /* synthetic */ i03 b;

    @DexIgnore
    public h03(i03 i03) {
        this.b = i03;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.a.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
