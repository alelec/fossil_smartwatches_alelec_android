package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji1 extends em0 {
    @DexIgnore
    public static /* final */ og1 CREATOR; // = new og1((qg6) null);
    @DexIgnore
    public /* final */ l70 c;

    @DexIgnore
    public ji1(byte b, l70 l70) {
        super(xh0.MUSIC_EVENT, b);
        this.c = l70;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }

    @DexIgnore
    public /* synthetic */ ji1(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.c = l70.valueOf(readString);
            return;
        }
        wg6.a();
        throw null;
    }
}
