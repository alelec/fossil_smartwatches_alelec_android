package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mz3 extends tz3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (qx3 == qx3.EAN_8) {
            return super.a(str, qx3, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_8, but got " + qx3);
    }

    @DexIgnore
    public boolean[] a(String str) {
        if (str.length() == 8) {
            boolean[] zArr = new boolean[67];
            int a = qz3.a(zArr, 0, sz3.a, true) + 0;
            int i = 0;
            while (i <= 3) {
                int i2 = i + 1;
                a += qz3.a(zArr, a, sz3.d[Integer.parseInt(str.substring(i, i2))], false);
                i = i2;
            }
            int a2 = a + qz3.a(zArr, a, sz3.b, false);
            int i3 = 4;
            while (i3 <= 7) {
                int i4 = i3 + 1;
                a2 += qz3.a(zArr, a2, sz3.d[Integer.parseInt(str.substring(i3, i4))], true);
                i3 = i4;
            }
            qz3.a(zArr, a2, sz3.a, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
