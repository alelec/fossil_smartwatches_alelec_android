package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx0 implements Parcelable.Creator<rz0> {
    @DexIgnore
    public /* synthetic */ yx0(qg6 qg6) {
    }

    @DexIgnore
    public rz0 createFromParcel(Parcel parcel) {
        return new rz0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new rz0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m73createFromParcel(Parcel parcel) {
        return new rz0(parcel, (qg6) null);
    }
}
