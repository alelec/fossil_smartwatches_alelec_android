package com.fossil;

import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu3 extends JsonElement implements Iterable<JsonElement> {
    @DexIgnore
    public /* final */ List<JsonElement> a; // = new ArrayList();

    @DexIgnore
    public void a(String str) {
        this.a.add(str == null ? ju3.a : new nu3(str));
    }

    @DexIgnore
    public int b() {
        if (this.a.size() == 1) {
            return this.a.get(0).b();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof fu3) && ((fu3) obj).a.equals(this.a));
    }

    @DexIgnore
    public String f() {
        if (this.a.size() == 1) {
            return this.a.get(0).f();
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public JsonElement get(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public Iterator<JsonElement> iterator() {
        return this.a.iterator();
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public void a(JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = ju3.a;
        }
        this.a.add(jsonElement);
    }

    @DexIgnore
    public boolean a() {
        if (this.a.size() == 1) {
            return this.a.get(0).a();
        }
        throw new IllegalStateException();
    }
}
