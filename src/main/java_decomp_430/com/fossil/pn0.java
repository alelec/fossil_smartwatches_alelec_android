package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn0 extends xg6 implements hg6<km1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pn0(BluetoothDevice bluetoothDevice) {
        super(1);
        this.a = bluetoothDevice;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        km1 km1 = (km1) obj;
        k40 k40 = k40.l;
        k40.d.remove(this.a.getAddress());
        return cd6.a;
    }
}
