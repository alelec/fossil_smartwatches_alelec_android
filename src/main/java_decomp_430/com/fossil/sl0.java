package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum sl0 {
    ADJUST_NONE((byte) 0),
    ADJUST_MANUAL((byte) 1),
    ADJUST_TIMEZONE((byte) 4);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public sl0(byte b) {
        this.a = b;
    }
}
