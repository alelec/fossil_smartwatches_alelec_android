package com.fossil;

import com.fossil.af6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo6 {
    @DexIgnore
    public static /* final */ uo6 a; // = new uo6("ZERO");
    @DexIgnore
    public static /* final */ ig6<Object, af6.b, Object> b; // = a.INSTANCE;
    @DexIgnore
    public static /* final */ ig6<nn6<?>, af6.b, nn6<?>> c; // = b.INSTANCE;
    @DexIgnore
    public static /* final */ ig6<bp6, af6.b, bp6> d; // = d.INSTANCE;
    @DexIgnore
    public static /* final */ ig6<bp6, af6.b, bp6> e; // = c.INSTANCE;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements ig6<Object, af6.b, Object> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(2);
        }

        @DexIgnore
        public final Object invoke(Object obj, af6.b bVar) {
            wg6.b(bVar, "element");
            if (!(bVar instanceof nn6)) {
                return obj;
            }
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            int intValue = num != null ? num.intValue() : 1;
            return intValue == 0 ? bVar : Integer.valueOf(intValue + 1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements ig6<nn6<?>, af6.b, nn6<?>> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(2);
        }

        @DexIgnore
        public final nn6<?> invoke(nn6<?> nn6, af6.b bVar) {
            wg6.b(bVar, "element");
            if (nn6 != null) {
                return nn6;
            }
            if (!(bVar instanceof nn6)) {
                bVar = null;
            }
            return (nn6) bVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements ig6<bp6, af6.b, bp6> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(2);
        }

        @DexIgnore
        public final bp6 invoke(bp6 bp6, af6.b bVar) {
            wg6.b(bp6, Constants.STATE);
            wg6.b(bVar, "element");
            if (bVar instanceof nn6) {
                ((nn6) bVar).a(bp6.a(), bp6.c());
            }
            return bp6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends xg6 implements ig6<bp6, af6.b, bp6> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(2);
        }

        @DexIgnore
        public final bp6 invoke(bp6 bp6, af6.b bVar) {
            wg6.b(bp6, Constants.STATE);
            wg6.b(bVar, "element");
            if (bVar instanceof nn6) {
                bp6.a(((nn6) bVar).a(bp6.a()));
            }
            return bp6;
        }
    }

    @DexIgnore
    public static final Object a(af6 af6) {
        wg6.b(af6, "context");
        Object fold = af6.fold(0, b);
        if (fold != null) {
            return fold;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public static final Object b(af6 af6, Object obj) {
        wg6.b(af6, "context");
        if (obj == null) {
            obj = a(af6);
        }
        if (obj == 0) {
            return a;
        }
        if (obj instanceof Integer) {
            return af6.fold(new bp6(af6, ((Number) obj).intValue()), d);
        }
        if (obj != null) {
            return ((nn6) obj).a(af6);
        }
        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
    }

    @DexIgnore
    public static final void a(af6 af6, Object obj) {
        wg6.b(af6, "context");
        if (obj != a) {
            if (obj instanceof bp6) {
                ((bp6) obj).b();
                af6.fold(obj, e);
                return;
            }
            Object fold = af6.fold(null, c);
            if (fold != null) {
                ((nn6) fold).a(af6, obj);
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        }
    }
}
