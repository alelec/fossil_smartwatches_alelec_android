package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v60 extends r60 implements Parcelable {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b((qg6) null);
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public enum a {
        CONTINUOUS((byte) 0),
        LOW_POWER((byte) 1),
        DISABLE((byte) 2);
        
        @DexIgnore
        public static /* final */ C0049a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v60$a$a")
        /* renamed from: com.fossil.v60$a$a  reason: collision with other inner class name */
        public static final class C0049a {
            @DexIgnore
            public /* synthetic */ C0049a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }
        }

        /*
        static {
            c = new C0049a((qg6) null);
        }
        */

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<v60> {
        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
        }

        @DexIgnore
        public final v60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new v60(a.c.a(bArr[0]));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        public v60 createFromParcel(Parcel parcel) {
            return new v60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new v60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m63createFromParcel(Parcel parcel) {
            return new v60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public v60(a aVar) {
        super(s60.HEART_RATE_MODE);
        this.b = aVar;
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(v60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((v60) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
    }

    @DexIgnore
    public final a getHeartRateMode() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }

    @DexIgnore
    public String d() {
        return cw0.a((Enum<?>) this.b);
    }

    @DexIgnore
    public /* synthetic */ v60(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.b = a.valueOf(readString);
            return;
        }
        wg6.a();
        throw null;
    }
}
