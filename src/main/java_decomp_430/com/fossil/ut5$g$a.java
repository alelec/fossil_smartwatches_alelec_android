package com.fossil;

import com.fossil.m24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1", f = "SignUpPresenter.kt", l = {413, 421, 426, 430, 443, 451}, m = "invokeSuspend")
public final class ut5$g$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ MFUser $currentUser;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SignUpPresenter.g this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$1", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ut5$g$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ut5$g$a ut5_g_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ut5_g_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.this$0.a.y().clearAllUser();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$2", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        public b(xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                PortfolioApp.get.instance().V();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<ys5, ws5> {
        @DexIgnore
        public /* final */ /* synthetic */ ut5$g$a a;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1", f = "SignUpPresenter.kt", l = {466}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ws5 $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ut5$g$a$c$a$a")
            @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1$1", f = "SignUpPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ut5$g$a$c$a$a  reason: collision with other inner class name */
            public static final class C0043a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0043a(a aVar, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0043a aVar = new C0043a(this.this$0, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0043a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    ff6.a();
                    if (this.label == 0) {
                        nc6.a(obj);
                        this.this$0.this$0.a.this$0.a.y().clearAllUser();
                        return cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, ws5 ws5, xe6 xe6) {
                super(2, xe6);
                this.this$0 = cVar;
                this.$errorValue = ws5;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$errorValue, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = ff6.a();
                int i = this.label;
                if (i == 0) {
                    nc6.a(obj);
                    il6 il6 = this.p$;
                    dl6 a2 = this.this$0.a.this$0.a.b();
                    C0043a aVar = new C0043a(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(a2, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.this$0.a.M.i();
                this.this$0.a.this$0.a.a(this.$errorValue.a(), this.$errorValue.b());
                return cd6.a;
            }
        }

        @DexIgnore
        public c(ut5$g$a ut5_g_a, jh6 jh6) {
            this.a = ut5_g_a;
            this.b = jh6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ys5 ys5) {
            wg6.b(ys5, "responseValue");
            FLogger.INSTANCE.getLocal().d(SignUpPresenter.Q.a(), "onLoginSuccess download device setting success");
            this.a.this$0.a.c((String) this.b.element);
            PortfolioApp.get.instance().a(this.a.this$0.a.s(), false, 13);
            this.a.this$0.a.M.i();
            this.a.this$0.a.n();
        }

        @DexIgnore
        public void a(ws5 ws5) {
            wg6.b(ws5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.Q.a();
            local.d(a2, "onLoginSuccess download device setting fail " + ws5.a());
            rm6 unused = ik6.b(this.a.this$0.a.e(), (af6) null, (ll6) null, new a(this, ws5, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$response$1", f = "SignUpPresenter.kt", l = {430}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super ap4<ApiResponse<Device>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ut5$g$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ut5$g$a ut5_g_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ut5_g_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                DeviceRepository r = this.this$0.this$0.a.r();
                this.L$0 = il6;
                this.label = 1;
                obj = r.downloadDeviceList(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ut5$g$a(SignUpPresenter.g gVar, MFUser mFUser, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
        this.$currentUser = mFUser;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ut5$g$a ut5_g_a = new ut5$g$a(this.this$0, this.$currentUser, xe6);
        ut5_g_a.p$ = (il6) obj;
        return ut5_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ut5$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.ut5$g$a$c] */
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.usecase.GetSecretKeyUseCase] */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b5, code lost:
        r13.this$0.a.M.i();
        r13.this$0.a.a(600, "");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00cb, code lost:
        return com.fossil.cd6.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x010a, code lost:
        r13.this$0.a.o();
        r14 = r13.this$0.a.b();
        r6 = new com.fossil.ut5$g$a.b((com.fossil.xe6) null);
        r13.L$0 = r1;
        r13.label = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0127, code lost:
        if (com.fossil.gk6.a(r14, r6, r13) != r0) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0129, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x012a, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.uirenew.signup.SignUpPresenter.Q.a(), "onLoginSuccess download require device and preset");
        r14 = r13.this$0.a.b();
        r6 = new com.fossil.ut5$g$a.d(r13, (com.fossil.xe6) null);
        r13.L$0 = r1;
        r13.label = 4;
        r14 = com.fossil.gk6.a(r14, r6, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0151, code lost:
        if (r14 != r0) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0153, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0154, code lost:
        r14 = (com.fossil.ap4) r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0158, code lost:
        if ((r14 instanceof com.fossil.cp4) == false) goto L_0x0296;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x015a, code lost:
        r6 = (com.portfolio.platform.data.source.remote.ApiResponse) ((com.fossil.cp4) r14).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0163, code lost:
        if (r6 == null) goto L_0x0169;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0165, code lost:
        r5 = r6.get_items();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0169, code lost:
        r6 = new com.fossil.jh6();
        r6.element = "";
        r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r7 = com.portfolio.platform.uirenew.signup.SignUpPresenter.Q.a();
        r4.d(r7, "onLoginSuccess allDevices " + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0190, code lost:
        if (r5 == null) goto L_0x01fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0192, code lost:
        r7 = r14;
        r8 = r1;
        r1 = r5.iterator();
        r4 = r5;
        r14 = r13;
        r5 = r6;
        r6 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x01a1, code lost:
        if (r1.hasNext() == false) goto L_0x01f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x01a3, code lost:
        r9 = (com.portfolio.platform.data.model.Device) r1.next();
        com.portfolio.platform.PortfolioApp.get.instance().d(r9.getDeviceId(), r9.getMacAddress());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01be, code lost:
        if (r9.isActive() == false) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x01c0, code lost:
        r5.element = r9.getDeviceId();
        com.portfolio.platform.PortfolioApp.get.instance().c(r5.element, r9.getMacAddress());
        r10 = com.portfolio.platform.util.DeviceUtils.g.a();
        r14.L$0 = r8;
        r14.L$1 = r7;
        r14.L$2 = r6;
        r14.L$3 = r5;
        r14.L$4 = r4;
        r14.L$5 = r1;
        r14.L$6 = r9;
        r14.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x01f2, code lost:
        if (r10.a((com.fossil.xe6<? super com.fossil.cd6>) r14) != r0) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01f5, code lost:
        r4 = r0;
        r0 = r5;
        r5 = r6;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x01fa, code lost:
        r7 = r14;
        r4 = r0;
        r0 = r6;
        r14 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0206, code lost:
        if (android.text.TextUtils.isEmpty(r0.element) != false) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0212, code lost:
        if (com.portfolio.platform.helper.DeviceHelper.o.e(r0.element) == false) goto L_0x0283;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0214, code lost:
        r14.this$0.a.v().a(r0.element, 0, false);
        r14.this$0.a.v().b(r0.element, true);
        r2 = r14.this$0.a.t();
        r9 = r14.$currentUser.getUserId();
        com.fossil.wg6.a((java.lang.Object) r9, "currentUser.userId");
        r6 = new com.portfolio.platform.usecase.GetSecretKeyUseCase.b(r0.element, r9);
        r14.L$0 = r1;
        r14.L$1 = r7;
        r14.L$2 = r5;
        r14.L$3 = r0;
        r14.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x025e, code lost:
        if (com.fossil.n24.a(r2, r6, r14) != r4) goto L_0x0261;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0260, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0261, code lost:
        r14.this$0.a.s().a(r0.element).a(new com.fossil.xs5(r0.element), new com.fossil.ut5$g$a.c(r14, r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0283, code lost:
        r14.this$0.a.M.i();
        r14.this$0.a.n();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0298, code lost:
        if ((r14 instanceof com.fossil.zo4) == false) goto L_0x02ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x029a, code lost:
        r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r1 = com.portfolio.platform.uirenew.signup.SignUpPresenter.Q.a();
        r2 = new java.lang.StringBuilder();
        r2.append("onLoginSuccess get all device fail ");
        r14 = (com.fossil.zo4) r14;
        r2.append(r14.a());
        r0.d(r1, r2.toString());
        r13.this$0.a.y().clearAllUser();
        r13.this$0.a.M.i();
        r0 = r13.this$0.a;
        r1 = r14.a();
        r14 = r14.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x02e2, code lost:
        if (r14 == null) goto L_0x02eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x02e4, code lost:
        r14 = r14.getMessage();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x02e8, code lost:
        if (r14 == null) goto L_0x02eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x02eb, code lost:
        r14 = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x02ec, code lost:
        r0.a(r1, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x02f1, code lost:
        return com.fossil.cd6.a;
     */
    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ut5$g$a ut5_g_a;
        il6 il6;
        Object a2 = ff6.a();
        List list = null;
        switch (this.label) {
            case 0:
                nc6.a(obj);
                il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = SignUpPresenter.Q.a();
                local.d(a3, "onLoginSuccess download userInfo success user " + this.$currentUser);
                if (this.$currentUser == null) {
                    dl6 b2 = this.this$0.a.c();
                    a aVar = new a(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(b2, aVar, this) == a2) {
                        return a2;
                    }
                } else {
                    zm4.p.a().o();
                    this.this$0.a.q().b(this.$currentUser.getUserId());
                    PortfolioApp instance = PortfolioApp.get.instance();
                    String userId = this.$currentUser.getUserId();
                    wg6.a((Object) userId, "currentUser.userId");
                    instance.q(userId);
                    PortfolioApp instance2 = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 2;
                    if (instance2.a((xe6<? super cd6>) this) == a2) {
                        return a2;
                    }
                }
                break;
            case 1:
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 2:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 3:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 4:
                il6 = (il6) this.L$0;
                nc6.a(obj);
                break;
            case 5:
                Device device = (Device) this.L$6;
                Iterator it = (Iterator) this.L$5;
                List list2 = (List) this.L$4;
                jh6 jh6 = (jh6) this.L$3;
                List list3 = (List) this.L$2;
                ap4 ap4 = (ap4) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                ut5_g_a = this;
                break;
            case 6:
                jh6 jh62 = (jh6) this.L$3;
                List list4 = (List) this.L$2;
                ap4 ap42 = (ap4) this.L$1;
                il6 il64 = (il6) this.L$0;
                nc6.a(obj);
                ut5_g_a = this;
                break;
            default:
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
