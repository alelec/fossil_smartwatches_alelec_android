package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gd4 extends fd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public long B;

    /*
    static {
        D.put(2131362133, 1);
        D.put(2131362542, 2);
        D.put(2131362520, 3);
        D.put(2131362192, 4);
        D.put(2131362519, 5);
        D.put(2131362191, 6);
        D.put(2131363150, 7);
        D.put(2131363149, 8);
        D.put(2131362522, 9);
        D.put(2131362195, 10);
        D.put(2131362899, 11);
    }
    */

    @DexIgnore
    public gd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public gd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[6], objArr[4], objArr[10], objArr[5], objArr[3], objArr[9], objArr[2], objArr[0], objArr[11], objArr[8], objArr[7]);
        this.B = -1;
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
