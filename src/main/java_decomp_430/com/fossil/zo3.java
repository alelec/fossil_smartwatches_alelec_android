package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zo3 extends AccessibleObject implements Member {
    @DexIgnore
    public /* final */ AccessibleObject a;
    @DexIgnore
    public /* final */ Member b;

    @DexIgnore
    public <M extends AccessibleObject & Member> zo3(M m) {
        jk3.a(m);
        this.a = m;
        this.b = (Member) m;
    }

    @DexIgnore
    public fp3<?> a() {
        return fp3.of(getDeclaringClass());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof zo3)) {
            return false;
        }
        zo3 zo3 = (zo3) obj;
        if (!a().equals(zo3.a()) || !this.b.equals(zo3.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        return this.a.getAnnotation(cls);
    }

    @DexIgnore
    public final Annotation[] getAnnotations() {
        return this.a.getAnnotations();
    }

    @DexIgnore
    public final Annotation[] getDeclaredAnnotations() {
        return this.a.getDeclaredAnnotations();
    }

    @DexIgnore
    public Class<?> getDeclaringClass() {
        return this.b.getDeclaringClass();
    }

    @DexIgnore
    public final int getModifiers() {
        return this.b.getModifiers();
    }

    @DexIgnore
    public final String getName() {
        return this.b.getName();
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final boolean isAccessible() {
        return this.a.isAccessible();
    }

    @DexIgnore
    public final boolean isAnnotationPresent(Class<? extends Annotation> cls) {
        return this.a.isAnnotationPresent(cls);
    }

    @DexIgnore
    public final boolean isSynthetic() {
        return this.b.isSynthetic();
    }

    @DexIgnore
    public final void setAccessible(boolean z) throws SecurityException {
        this.a.setAccessible(z);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
