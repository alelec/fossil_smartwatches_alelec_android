package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ou6 implements eu6 {
    @DexIgnore
    public du6 a;
    @DexIgnore
    public mu6 b;
    @DexIgnore
    public List c;

    @DexIgnore
    public void a(mu6 mu6) {
        this.b = mu6;
        this.c = new ArrayList(mu6.getRequiredOptions());
    }

    @DexIgnore
    public mu6 b() {
        return this.b;
    }

    @DexIgnore
    public abstract String[] b(mu6 mu6, String[] strArr, boolean z);

    @DexIgnore
    public List c() {
        return this.c;
    }

    @DexIgnore
    public du6 a(mu6 mu6, String[] strArr, boolean z) throws nu6 {
        return a(mu6, strArr, (Properties) null, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0080, code lost:
        if (r9 != false) goto L_0x004c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0085 A[LOOP:2: B:27:0x0085->B:39:0x0085, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0037 A[SYNTHETIC] */
    public du6 a(mu6 mu6, String[] strArr, Properties properties, boolean z) throws nu6 {
        for (ju6 clearValues : mu6.helpOptions()) {
            clearValues.clearValues();
        }
        a(mu6);
        this.a = new du6();
        boolean z2 = false;
        if (strArr == null) {
            strArr = new String[0];
        }
        ListIterator listIterator = Arrays.asList(b(b(), strArr, z)).listIterator();
        while (listIterator.hasNext()) {
            String str = (String) listIterator.next();
            if (!"--".equals(str)) {
                if (ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR.equals(str)) {
                    if (!z) {
                        this.a.addArg(str);
                        if (z2) {
                            while (listIterator.hasNext()) {
                                String str2 = (String) listIterator.next();
                                if (!"--".equals(str2)) {
                                    this.a.addArg(str2);
                                }
                            }
                        }
                    }
                } else if (!str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
                    this.a.addArg(str);
                } else if (!z || b().hasOption(str)) {
                    a(str, listIterator);
                    if (z2) {
                    }
                } else {
                    this.a.addArg(str);
                }
            }
            z2 = true;
            if (z2) {
            }
        }
        a(properties);
        a();
        return this.a;
    }

    @DexIgnore
    public void a(Properties properties) {
        if (properties != null) {
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String obj = propertyNames.nextElement().toString();
                if (!this.a.hasOption(obj)) {
                    ju6 option = b().getOption(obj);
                    String property = properties.getProperty(obj);
                    if (option.hasArg()) {
                        if (option.getValues() == null || option.getValues().length == 0) {
                            try {
                                option.addValueForProcessing(property);
                            } catch (RuntimeException unused) {
                            }
                        }
                    } else if (!"yes".equalsIgnoreCase(property) && !"true".equalsIgnoreCase(property) && !"1".equalsIgnoreCase(property)) {
                        return;
                    }
                    this.a.addOption(option);
                }
            }
        }
    }

    @DexIgnore
    public void a() throws iu6 {
        if (!c().isEmpty()) {
            throw new iu6(c());
        }
    }

    @DexIgnore
    public void a(ju6 ju6, ListIterator listIterator) throws nu6 {
        while (true) {
            if (!listIterator.hasNext()) {
                break;
            }
            String str = (String) listIterator.next();
            if (b().hasOption(str) && str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
                listIterator.previous();
                break;
            } else {
                try {
                    ju6.addValueForProcessing(su6.a(str));
                } catch (RuntimeException unused) {
                    listIterator.previous();
                }
            }
        }
        if (ju6.getValues() == null && !ju6.hasOptionalArg()) {
            throw new hu6(ju6);
        }
    }

    @DexIgnore
    public void a(String str, ListIterator listIterator) throws nu6 {
        if (b().hasOption(str)) {
            ju6 ju6 = (ju6) b().getOption(str).clone();
            if (ju6.isRequired()) {
                c().remove(ju6.getKey());
            }
            if (b().getOptionGroup(ju6) != null) {
                ku6 optionGroup = b().getOptionGroup(ju6);
                if (optionGroup.isRequired()) {
                    c().remove(optionGroup);
                }
                optionGroup.setSelected(ju6);
            }
            if (ju6.hasArg()) {
                a(ju6, listIterator);
            }
            this.a.addOption(ju6);
            return;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Unrecognized option: ");
        stringBuffer.append(str);
        throw new ru6(stringBuffer.toString(), str);
    }
}
