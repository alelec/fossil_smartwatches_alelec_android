package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t71 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.FILE_CONFIG}));
    @DexIgnore
    public long C;
    @DexIgnore
    public long D;
    @DexIgnore
    public long E;
    @DexIgnore
    public /* final */ long F; // = cw0.b(this.G.e.length);
    @DexIgnore
    public /* final */ ie1 G;

    @DexIgnore
    public t71(ue1 ue1, q41 q41, ie1 ie1, String str) {
        super(ue1, q41, eh1.FIND_CORRECT_OFFSET, str);
        this.G = ie1;
    }

    @DexIgnore
    public qv0 a(lx0 lx0) {
        if (a41.a[lx0.ordinal()] != 1) {
            return null;
        }
        long j = this.D;
        uj0 uj0 = new uj0(j, this.E - j, this.G.c(), this.w, 0, 16);
        uj0.b((hg6<? super qv0, cd6>) new w51(this));
        return uj0;
    }

    @DexIgnore
    public Object d() {
        return Long.valueOf(this.C);
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        this.D = 0;
        this.E = cw0.b(this.G.e.length);
        if (this.D == this.E) {
            a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        } else {
            if1.a(this, lx0.VERIFY_DATA, (lx0) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.DEVICE_FILE, (Object) this.G.a(false));
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(super.k(), bm0.CORRECT_OFFSET, (Object) Long.valueOf(this.C));
    }
}
