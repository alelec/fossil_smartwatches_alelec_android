package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ak3<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ak3<Object> implements Serializable {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;

        @DexIgnore
        private Object readResolve() {
            return INSTANCE;
        }

        @DexIgnore
        public boolean doEquivalent(Object obj, Object obj2) {
            return obj.equals(obj2);
        }

        @DexIgnore
        public int doHash(Object obj) {
            return obj.hashCode();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements kk3<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ ak3<T> equivalence;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        public c(ak3<T> ak3, T t) {
            jk3.a(ak3);
            this.equivalence = ak3;
            this.target = t;
        }

        @DexIgnore
        public boolean apply(T t) {
            return this.equivalence.equivalent(t, this.target);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (!this.equivalence.equals(cVar.equivalence) || !gk3.a(this.target, cVar.target)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return gk3.a(this.equivalence, this.target);
        }

        @DexIgnore
        public String toString() {
            return this.equivalence + ".equivalentTo(" + this.target + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends ak3<Object> implements Serializable {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 1;

        @DexIgnore
        private Object readResolve() {
            return INSTANCE;
        }

        @DexIgnore
        public boolean doEquivalent(Object obj, Object obj2) {
            return false;
        }

        @DexIgnore
        public int doHash(Object obj) {
            return System.identityHashCode(obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ ak3<? super T> equivalence;
        @DexIgnore
        public /* final */ T reference;

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof e)) {
                return false;
            }
            e eVar = (e) obj;
            if (this.equivalence.equals(eVar.equivalence)) {
                return this.equivalence.equivalent(this.reference, eVar.reference);
            }
            return false;
        }

        @DexIgnore
        public T get() {
            return this.reference;
        }

        @DexIgnore
        public int hashCode() {
            return this.equivalence.hash(this.reference);
        }

        @DexIgnore
        public String toString() {
            return this.equivalence + ".wrap(" + this.reference + ")";
        }

        @DexIgnore
        public e(ak3<? super T> ak3, T t) {
            jk3.a(ak3);
            this.equivalence = ak3;
            this.reference = t;
        }
    }

    @DexIgnore
    public static ak3<Object> equals() {
        return b.INSTANCE;
    }

    @DexIgnore
    public static ak3<Object> identity() {
        return d.INSTANCE;
    }

    @DexIgnore
    public abstract boolean doEquivalent(T t, T t2);

    @DexIgnore
    public abstract int doHash(T t);

    @DexIgnore
    public final boolean equivalent(T t, T t2) {
        if (t == t2) {
            return true;
        }
        if (t == null || t2 == null) {
            return false;
        }
        return doEquivalent(t, t2);
    }

    @DexIgnore
    public final kk3<T> equivalentTo(T t) {
        return new c(this, t);
    }

    @DexIgnore
    public final int hash(T t) {
        if (t == null) {
            return 0;
        }
        return doHash(t);
    }

    @DexIgnore
    public final <F> ak3<F> onResultOf(ck3<F, ? extends T> ck3) {
        return new dk3(ck3, this);
    }

    @DexIgnore
    public final <S extends T> ak3<Iterable<S>> pairwise() {
        return new ik3(this);
    }

    @DexIgnore
    public final <S extends T> e<S> wrap(S s) {
        return new e<>(s);
    }
}
