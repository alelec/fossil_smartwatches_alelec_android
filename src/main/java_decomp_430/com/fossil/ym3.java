package com.fossil;

import com.fossil.bm3;
import com.fossil.ek3;
import com.fossil.yn3;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym3 {
    @DexIgnore
    public static /* final */ ek3.b a; // = cl3.a.b("=");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends uk3<K, V> {
        @DexIgnore
        public /* final */ /* synthetic */ Map.Entry a;

        @DexIgnore
        public a(Map.Entry entry) {
            this.a = entry;
        }

        @DexIgnore
        public K getKey() {
            return this.a.getKey();
        }

        @DexIgnore
        public V getValue() {
            return this.a.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends jo3<Map.Entry<K, V>> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public b(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        public Map.Entry<K, V> next() {
            return ym3.b((Map.Entry) this.a.next());
        }
    }

    @DexIgnore
    public enum c implements ck3<Map.Entry<?, ?>, Object> {
        KEY {
            @DexIgnore
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        },
        VALUE {
            @DexIgnore
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        };
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K, V> extends yn3.a<Map.Entry<K, V>> {
        @DexIgnore
        public abstract Map<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public abstract boolean contains(Object obj);

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            try {
                jk3.a(collection);
                return super.removeAll(collection);
            } catch (UnsupportedOperationException unused) {
                return yn3.a((Set<?>) this, collection.iterator());
            }
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            try {
                jk3.a(collection);
                return super.retainAll(collection);
            } catch (UnsupportedOperationException unused) {
                HashSet a = yn3.a(collection.size());
                for (Object next : collection) {
                    if (contains(next)) {
                        a.add(((Map.Entry) next).getKey());
                    }
                }
                return a().keySet().retainAll(a);
            }
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<K, V> extends yn3.a<K> {
        @DexIgnore
        public /* final */ Map<K, V> a;

        @DexIgnore
        public e(Map<K, V> map) {
            jk3.a(map);
            this.a = map;
        }

        @DexIgnore
        public Map<K, V> a() {
            return this.a;
        }

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return ym3.a(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!contains(obj)) {
                return false;
            }
            a().remove(obj);
            return true;
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<K, V> extends AbstractCollection<V> {
        @DexIgnore
        public /* final */ Map<K, V> a;

        @DexIgnore
        public f(Map<K, V> map) {
            jk3.a(map);
            this.a = map;
        }

        @DexIgnore
        public final Map<K, V> a() {
            return this.a;
        }

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        public Iterator<V> iterator() {
            return ym3.c(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            try {
                return super.remove(obj);
            } catch (UnsupportedOperationException unused) {
                for (Map.Entry entry : a().entrySet()) {
                    if (gk3.a(obj, entry.getValue())) {
                        a().remove(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
        }

        @DexIgnore
        public boolean removeAll(Collection<?> collection) {
            try {
                jk3.a(collection);
                return super.removeAll(collection);
            } catch (UnsupportedOperationException unused) {
                HashSet a2 = yn3.a();
                for (Map.Entry entry : a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        a2.add(entry.getKey());
                    }
                }
                return a().keySet().removeAll(a2);
            }
        }

        @DexIgnore
        public boolean retainAll(Collection<?> collection) {
            try {
                jk3.a(collection);
                return super.retainAll(collection);
            } catch (UnsupportedOperationException unused) {
                HashSet a2 = yn3.a();
                for (Map.Entry entry : a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        a2.add(entry.getKey());
                    }
                }
                return a().keySet().retainAll(a2);
            }
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g<K, V> extends AbstractMap<K, V> {
        @DexIgnore
        public transient Set<Map.Entry<K, V>> a;
        @DexIgnore
        public transient Collection<V> b;

        @DexIgnore
        public abstract Set<Map.Entry<K, V>> a();

        @DexIgnore
        public Collection<V> b() {
            return new f(this);
        }

        @DexIgnore
        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> set = this.a;
            if (set != null) {
                return set;
            }
            Set<Map.Entry<K, V>> a2 = a();
            this.a = a2;
            return a2;
        }

        @DexIgnore
        public Collection<V> values() {
            Collection<V> collection = this.b;
            if (collection != null) {
                return collection;
            }
            Collection<V> b2 = b();
            this.b = b2;
            return b2;
        }
    }

    @DexIgnore
    public static <K> ck3<Map.Entry<K, ?>, K> a() {
        return c.KEY;
    }

    @DexIgnore
    public static <K, V> HashMap<K, V> b() {
        return new HashMap<>();
    }

    @DexIgnore
    public static <V> ck3<Map.Entry<?, V>, V> c() {
        return c.VALUE;
    }

    @DexIgnore
    public static boolean d(Map<?, ?> map, Object obj) {
        jk3.a(map);
        try {
            return map.containsKey(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    @DexIgnore
    public static <V> V e(Map<?, V> map, Object obj) {
        jk3.a(map);
        try {
            return map.get(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    @DexIgnore
    public static <V> V f(Map<?, V> map, Object obj) {
        jk3.a(map);
        try {
            return map.remove(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    @DexIgnore
    public static <K, V> Iterator<K> a(Iterator<Map.Entry<K, V>> it) {
        return qm3.a(it, a());
    }

    @DexIgnore
    public static <K, V> LinkedHashMap<K, V> b(int i) {
        return new LinkedHashMap<>(a(i));
    }

    @DexIgnore
    public static <K, V> Iterator<V> c(Iterator<Map.Entry<K, V>> it) {
        return qm3.a(it, c());
    }

    @DexIgnore
    public static int a(int i) {
        if (i < 3) {
            bl3.a(i, "expectedSize");
            return i + 1;
        } else if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        } else {
            return Integer.MAX_VALUE;
        }
    }

    @DexIgnore
    public static <K, V> Map.Entry<K, V> b(Map.Entry<? extends K, ? extends V> entry) {
        jk3.a(entry);
        return new a(entry);
    }

    @DexIgnore
    public static boolean c(Map<?, ?> map, Object obj) {
        if (map == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return map.entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    @DexIgnore
    public static <K, V> Map.Entry<K, V> a(K k, V v) {
        return new wl3(k, v);
    }

    @DexIgnore
    public static boolean a(Map<?, ?> map, Object obj) {
        return qm3.a((Iterator<?>) a(map.entrySet().iterator()), obj);
    }

    @DexIgnore
    public static <K, V> jo3<Map.Entry<K, V>> b(Iterator<Map.Entry<K, V>> it) {
        return new b(it);
    }

    @DexIgnore
    public static String a(Map<?, ?> map) {
        StringBuilder a2 = cl3.a(map.size());
        a2.append('{');
        a.a(a2, map);
        a2.append('}');
        return a2.toString();
    }

    @DexIgnore
    public static boolean b(Map<?, ?> map, Object obj) {
        return qm3.a((Iterator<?>) c(map.entrySet().iterator()), obj);
    }

    @DexIgnore
    public static <K, V> void a(Map<K, V> map, Map<? extends K, ? extends V> map2) {
        for (Map.Entry next : map2.entrySet()) {
            map.put(next.getKey(), next.getValue());
        }
    }

    @DexIgnore
    public static <K> K a(Map.Entry<K, ?> entry) {
        if (entry == null) {
            return null;
        }
        return entry.getKey();
    }

    @DexIgnore
    public static <E> bm3<E, Integer> a(Collection<E> collection) {
        bm3.b bVar = new bm3.b(collection.size());
        int i = 0;
        for (E a2 : collection) {
            bVar.a(a2, Integer.valueOf(i));
            i++;
        }
        return bVar.a();
    }
}
