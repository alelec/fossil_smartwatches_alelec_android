package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ra3 a;
    @DexIgnore
    public /* final */ /* synthetic */ l83 b;

    @DexIgnore
    public o83(l83 l83, ra3 ra3) {
        this.b = l83;
        this.a = ra3;
    }

    @DexIgnore
    public final void run() {
        l43 d = this.b.d;
        if (d == null) {
            this.b.b().t().a("Failed to reset data on the service; null service");
            return;
        }
        try {
            d.a(this.a);
        } catch (RemoteException e) {
            this.b.b().t().a("Failed to reset data on the service", e);
        }
        this.b.I();
    }
}
