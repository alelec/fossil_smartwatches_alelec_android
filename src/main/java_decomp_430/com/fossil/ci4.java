package com.fossil;

import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ci4 {
    DEVICE("device"),
    MANUAL("manual");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ci4 a(String str) {
            ci4 ci4;
            wg6.b(str, ServerSetting.VALUE);
            ci4[] values = ci4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ci4 = null;
                    break;
                }
                ci4 = values[i];
                String mValue = ci4.getMValue();
                String lowerCase = str.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (wg6.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return ci4 != null ? ci4 : ci4.DEVICE;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public ci4(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        wg6.b(str, "<set-?>");
        this.mValue = str;
    }
}
