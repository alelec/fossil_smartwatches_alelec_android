package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w91 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ if1 a;
    @DexIgnore
    public /* final */ /* synthetic */ gg6 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w91(if1 if1, gg6 gg6) {
        super(1);
        this.a = if1;
        this.b = gg6;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        qv0 qv0 = (qv0) obj;
        if1 if1 = this.a;
        if1.m++;
        Short sh = null;
        if1.v = km1.a(if1.v, (eh1) null, sk1.G.a(qv0.v), qv0.v, 1);
        bn0 bn0 = qv0.v;
        il0 il0 = bn0.c;
        if (il0 == il0.CONNECTION_DROPPED || il0 == il0.BLUETOOTH_OFF || bn0.d.c.a == x11.GATT_NULL) {
            this.a.a(qv0.v);
        } else {
            if (qv0 instanceof vf1) {
                sh = Short.valueOf(((vf1) qv0).J);
            } else if (qv0 instanceof vd1) {
                sh = Short.valueOf(((vd1) qv0).F);
            }
            if (sh != null) {
                if1.a(this.a, (qv0) new ec1(sh.shortValue(), this.a.w, 0, 4), (hg6) new c61(this, qv0), (hg6) new z71(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
            } else {
                this.b.invoke();
            }
        }
        return cd6.a;
    }
}
