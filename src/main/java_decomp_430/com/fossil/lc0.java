package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<lc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new lc0(parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new lc0[i];
        }
    }

    @DexIgnore
    public lc0(int i, int i2) throws IllegalArgumentException {
        this.a = i;
        this.b = i2;
        int i3 = this.a;
        boolean z = true;
        if (i3 >= 0 && 359 >= i3) {
            int i4 = this.b;
            if (!((i4 < 0 || 120 < i4) ? false : z)) {
                throw new IllegalArgumentException(ze0.a(ze0.b("distanceFromCenter("), this.b, ") is out of ", "range [0, 120]."));
            }
            return;
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("angle("), this.a, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("angle", this.a).put("distance", this.b);
        wg6.a(put, "JSONObject()\n           \u2026ANCE, distanceFromCenter)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(lc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            lc0 lc0 = (lc0) obj;
            return this.a == lc0.a && this.b == lc0.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.a;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
