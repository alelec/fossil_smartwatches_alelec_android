package com.fossil;

import android.app.RemoteInput;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t6 {
    @DexIgnore
    public static RemoteInput[] a(t6[] t6VarArr) {
        if (t6VarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[t6VarArr.length];
        if (t6VarArr.length <= 0) {
            return remoteInputArr;
        }
        a(t6VarArr[0]);
        throw null;
    }

    @DexIgnore
    public String a() {
        throw null;
    }

    @DexIgnore
    public static RemoteInput a(t6 t6Var) {
        t6Var.a();
        throw null;
    }
}
