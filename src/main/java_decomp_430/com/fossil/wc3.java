package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wc3<TResult, TContinuationResult> implements kd3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ ic3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ od3<TContinuationResult> c;

    @DexIgnore
    public wc3(Executor executor, ic3<TResult, TContinuationResult> ic3, od3<TContinuationResult> od3) {
        this.a = executor;
        this.b = ic3;
        this.c = od3;
    }

    @DexIgnore
    public final void onComplete(qc3<TResult> qc3) {
        this.a.execute(new xc3(this, qc3));
    }
}
