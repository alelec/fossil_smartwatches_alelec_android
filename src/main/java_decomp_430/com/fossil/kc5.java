package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kc5 implements MembersInjector<ActivityOverviewFragment> {
    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, ActivityOverviewDayPresenter activityOverviewDayPresenter) {
        activityOverviewFragment.g = activityOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
        activityOverviewFragment.h = activityOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
        activityOverviewFragment.i = activityOverviewMonthPresenter;
    }
}
