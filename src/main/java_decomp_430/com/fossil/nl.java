package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl {
    @DexIgnore
    public static /* final */ nl i; // = new a().a();
    @DexIgnore
    public ul a; // = ul.NOT_REQUIRED;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f; // = -1;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public ol h; // = new ol();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public ul c; // = ul.NOT_REQUIRED;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public ol h; // = new ol();

        @DexIgnore
        public a a(ul ulVar) {
            this.c = ulVar;
            return this;
        }

        @DexIgnore
        public nl a() {
            return new nl(this);
        }
    }

    @DexIgnore
    public nl() {
    }

    @DexIgnore
    public void a(ul ulVar) {
        this.a = ulVar;
    }

    @DexIgnore
    public ul b() {
        return this.a;
    }

    @DexIgnore
    public void c(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void d(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public boolean e() {
        return this.h.b() > 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || nl.class != obj.getClass()) {
            return false;
        }
        nl nlVar = (nl) obj;
        if (this.b == nlVar.b && this.c == nlVar.c && this.d == nlVar.d && this.e == nlVar.e && this.f == nlVar.f && this.g == nlVar.g && this.a == nlVar.a) {
            return this.h.equals(nlVar.h);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.b;
    }

    @DexIgnore
    public boolean h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f;
        long j2 = this.g;
        return (((((((((((((this.a.hashCode() * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void b(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public long c() {
        return this.f;
    }

    @DexIgnore
    public long d() {
        return this.g;
    }

    @DexIgnore
    public void a(long j) {
        this.f = j;
    }

    @DexIgnore
    public void b(long j) {
        this.g = j;
    }

    @DexIgnore
    public void a(ol olVar) {
        this.h = olVar;
    }

    @DexIgnore
    public ol a() {
        return this.h;
    }

    @DexIgnore
    public nl(a aVar) {
        this.b = aVar.a;
        this.c = Build.VERSION.SDK_INT >= 23 && aVar.b;
        this.a = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        if (Build.VERSION.SDK_INT >= 24) {
            this.h = aVar.h;
            this.f = aVar.f;
            this.g = aVar.g;
        }
    }

    @DexIgnore
    public nl(nl nlVar) {
        this.b = nlVar.b;
        this.c = nlVar.c;
        this.a = nlVar.a;
        this.d = nlVar.d;
        this.e = nlVar.e;
        this.h = nlVar.h;
    }
}
