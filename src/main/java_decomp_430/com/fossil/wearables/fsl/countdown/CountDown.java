package com.fossil.wearables.fsl.countdown;

import com.j256.ormlite.field.DatabaseField;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CountDown {
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_MODEL; // = "deviceModel";
    @DexIgnore
    public static /* final */ String COLUMN_ENDED_AT; // = "endedAt";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_NOTE; // = "note";
    @DexIgnore
    public static /* final */ String COLUMN_SERIAL; // = "serial";
    @DexIgnore
    public static /* final */ String COLUMN_SERVER_ID; // = "serverId";
    @DexIgnore
    public static /* final */ String COLUMN_STATUS; // = "status";
    @DexIgnore
    public static /* final */ String COLUMN_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATED_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long createdAt;
    @DexIgnore
    @DatabaseField(columnName = "deviceModel")
    public String deviceModel;
    @DexIgnore
    @DatabaseField(columnName = "endedAt")
    public long endedAt;
    @DexIgnore
    @DatabaseField(columnName = "id", generatedId = true)
    public long id;
    @DexIgnore
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = "note")
    public String note;
    @DexIgnore
    @DatabaseField(columnName = "serial")
    public String serial;
    @DexIgnore
    @DatabaseField(columnName = "serverId")
    public String serverId;
    @DexIgnore
    @DatabaseField(columnName = "status")
    public int status;
    @DexIgnore
    @DatabaseField(columnName = "type")
    public int type;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updatedAt;
    @DexIgnore
    @DatabaseField(columnName = "uri")
    public String uri;

    @DexIgnore
    public CountDown() {
    }

    @DexIgnore
    public long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public long getEndedAt() {
        return this.endedAt;
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getNote() {
        return this.note;
    }

    @DexIgnore
    public String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public String getServerId() {
        return this.serverId;
    }

    @DexIgnore
    public CountDownStatus getStatus() {
        return CountDownStatus.fromInt(this.status);
    }

    @DexIgnore
    public CountDownType getType() {
        return CountDownType.fromInt(this.type);
    }

    @DexIgnore
    public long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public void setDeviceModel(String str) {
        this.deviceModel = str;
    }

    @DexIgnore
    public void setEndedAt(long j) {
        this.endedAt = j;
    }

    @DexIgnore
    public void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setNote(String str) {
        this.note = str;
    }

    @DexIgnore
    public void setSerial(String str) {
        this.serial = str;
    }

    @DexIgnore
    public void setServerId(String str) {
        this.serverId = str;
    }

    @DexIgnore
    public void setStatus(CountDownStatus countDownStatus) {
        this.status = countDownStatus.getValue();
    }

    @DexIgnore
    public void setType(CountDownType countDownType) {
        this.type = countDownType.getValue();
    }

    @DexIgnore
    public void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "[CountDown: name=" + this.name + ", uri=" + this.uri + ", endedAt:" + new Date(this.endedAt) + "]";
    }

    @DexIgnore
    public CountDown(String str, CountDownType countDownType, String str2, String str3, long j, CountDownStatus countDownStatus, String str4) {
        this.name = str;
        this.endedAt = j;
        this.createdAt = System.currentTimeMillis();
        this.status = countDownStatus.getValue();
        this.note = str4;
        this.type = countDownType.getValue();
        this.serial = str2;
        this.deviceModel = str3;
        this.uri = CountDownUri.generateURI(this).toASCIIString();
    }
}
