package com.fossil.wearables.fsl.shared;

import com.j256.ormlite.field.DatabaseField;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseModel implements Serializable {
    @DexIgnore
    public /* final */ String TAG; // = getClass().getCanonicalName();
    @DexIgnore
    @DatabaseField(columnName = "id", generatedId = true)
    public int dbRowId; // = 0;

    @DexIgnore
    public int getDbRowId() {
        return this.dbRowId;
    }

    @DexIgnore
    public void setDbRowId(int i) {
        this.dbRowId = i;
    }
}
