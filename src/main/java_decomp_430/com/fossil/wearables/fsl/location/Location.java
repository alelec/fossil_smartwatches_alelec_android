package com.fossil.wearables.fsl.location;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Location extends BaseModel {
    @DexIgnore
    @DatabaseField
    public String address;
    @DexIgnore
    @DatabaseField
    public String city;
    @DexIgnore
    @DatabaseField(columnName = "location_group_id", foreign = true, foreignAutoRefresh = true)
    public LocationGroup group;
    @DexIgnore
    @DatabaseField
    public double latitude;
    @DexIgnore
    @DatabaseField
    public double longitude;
    @DexIgnore
    @DatabaseField
    public String name;
    @DexIgnore
    @DatabaseField
    public String note;
    @DexIgnore
    @DatabaseField
    public String state;
    @DexIgnore
    @DatabaseField
    public long timestamp;
    @DexIgnore
    @DatabaseField
    public boolean verified;
    @DexIgnore
    @DatabaseField
    public String zip;

    @DexIgnore
    public String getAddress() {
        return this.address;
    }

    @DexIgnore
    public String getCity() {
        return this.city;
    }

    @DexIgnore
    public LocationGroup getGroup() {
        return this.group;
    }

    @DexIgnore
    public double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public String getNote() {
        return this.note;
    }

    @DexIgnore
    public String getState() {
        return this.state;
    }

    @DexIgnore
    public long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public String getZip() {
        return this.zip;
    }

    @DexIgnore
    public boolean isVerified() {
        return this.verified;
    }

    @DexIgnore
    public void setAddress(String str) {
        this.address = str;
    }

    @DexIgnore
    public void setCity(String str) {
        this.city = str;
    }

    @DexIgnore
    public void setGroup(LocationGroup locationGroup) {
        this.group = locationGroup;
    }

    @DexIgnore
    public void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public void setLongitude(double d) {
        this.longitude = d;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setNote(String str) {
        this.note = str;
    }

    @DexIgnore
    public void setState(String str) {
        this.state = str;
    }

    @DexIgnore
    public void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public void setVerified(boolean z) {
        this.verified = z;
    }

    @DexIgnore
    public void setZip(String str) {
        this.zip = str;
    }
}
