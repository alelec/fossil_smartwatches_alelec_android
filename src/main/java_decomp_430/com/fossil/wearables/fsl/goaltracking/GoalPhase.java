package com.fossil.wearables.fsl.goaltracking;

import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalPhase {
    @DexIgnore
    public static /* final */ String COLUMN_END_DATE; // = "endDate";
    @DexIgnore
    public static /* final */ String COLUMN_END_DAY; // = "endDay";
    @DexIgnore
    public static /* final */ String COLUMN_GOAL_ID; // = "goalId";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_START_DATE; // = "startDate";
    @DexIgnore
    public static /* final */ String COLUMN_START_DAY; // = "startDay";
    @DexIgnore
    @DatabaseField(columnName = "endDate")
    public long endDate;
    @DexIgnore
    @DatabaseField(columnName = "endDay")
    public String endDay;
    @DexIgnore
    @DatabaseField(columnName = "goalId", foreign = true, foreignAutoRefresh = true)
    public GoalTracking goal;
    @DexIgnore
    @DatabaseField(columnName = "id", generatedId = true)
    public long id;
    @DexIgnore
    @DatabaseField(columnName = "startDate")
    public long startDate;
    @DexIgnore
    @DatabaseField(columnName = "startDay")
    public String startDay;

    @DexIgnore
    public GoalPhase() {
    }

    @DexIgnore
    public long getEndDate() {
        return this.endDate;
    }

    @DexIgnore
    public String getEndDay() {
        return this.endDay;
    }

    @DexIgnore
    public GoalTracking getGoalTracking() {
        return this.goal;
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public long getStartDate() {
        return this.startDate;
    }

    @DexIgnore
    public String getStartDay() {
        return this.startDay;
    }

    @DexIgnore
    public void setEndDate(long j) {
        this.endDate = j;
    }

    @DexIgnore
    public void setEndDay(String str) {
        this.endDay = str;
    }

    @DexIgnore
    public void setGoalTracking(GoalTracking goalTracking) {
        this.goal = goalTracking;
    }

    @DexIgnore
    public void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public void setStartDate(long j) {
        this.startDate = j;
    }

    @DexIgnore
    public void setStartDay(String str) {
        this.startDay = str;
    }

    @DexIgnore
    public GoalPhase(long j, long j2, String str, String str2, GoalTracking goalTracking) {
        this.startDate = j;
        this.endDate = j2;
        this.startDay = str;
        this.endDay = str2;
        this.goal = goalTracking;
    }
}
