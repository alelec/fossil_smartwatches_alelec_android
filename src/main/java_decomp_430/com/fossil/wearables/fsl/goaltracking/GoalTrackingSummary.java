package com.fossil.wearables.fsl.goaltracking;

import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalTrackingSummary {
    @DexIgnore
    public static /* final */ String COLUMN_AVERAGE; // = "average";
    @DexIgnore
    public static /* final */ String COLUMN_BEST_STREAK; // = "bestStreak";
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ String COLUMN_GOALS_MET; // = "goalsMet";
    @DexIgnore
    public static /* final */ String COLUMN_GOAL_ID; // = "goalId";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_TOTAL_DURATION; // = "totalDuration";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATED_AT; // = "updatedAt";
    @DexIgnore
    @DatabaseField(columnName = "average")
    public double average;
    @DexIgnore
    @DatabaseField(columnName = "bestStreak")
    public int bestStreak;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long createdAt;
    @DexIgnore
    @DatabaseField(columnName = "date")
    public long date;
    @DexIgnore
    @DatabaseField(columnName = "goalId", foreign = true, foreignAutoRefresh = true)
    public GoalTracking goalTracking;
    @DexIgnore
    @DatabaseField(columnName = "goalsMet")
    public int goalsMet;
    @DexIgnore
    @DatabaseField(columnName = "id", generatedId = true)
    public long id;
    @DexIgnore
    @DatabaseField(columnName = "totalDuration")
    public int totalDuration;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updatedAt;

    @DexIgnore
    public double getAverage() {
        return this.average;
    }

    @DexIgnore
    public int getBestStreak() {
        return this.bestStreak;
    }

    @DexIgnore
    public long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public long getDate() {
        return this.date;
    }

    @DexIgnore
    public GoalTracking getGoalTracking() {
        return this.goalTracking;
    }

    @DexIgnore
    public int getGoalsMet() {
        return this.goalsMet;
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public int getTotalDuration() {
        return this.totalDuration;
    }

    @DexIgnore
    public long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public void setAverage(double d) {
        this.average = d;
    }

    @DexIgnore
    public void setBestStreak(int i) {
        this.bestStreak = i;
    }

    @DexIgnore
    public void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public void setDate(long j) {
        this.date = j;
    }

    @DexIgnore
    public void setGoalTracking(GoalTracking goalTracking2) {
        this.goalTracking = goalTracking2;
    }

    @DexIgnore
    public void setGoalsMet(int i) {
        this.goalsMet = i;
    }

    @DexIgnore
    public void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public void setTotalDuration(int i) {
        this.totalDuration = i;
    }

    @DexIgnore
    public void setUpdatedAt(long j) {
        this.updatedAt = j;
    }
}
