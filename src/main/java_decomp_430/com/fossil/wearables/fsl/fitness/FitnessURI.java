package com.fossil.wearables.fsl.fitness;

import java.net.URI;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FitnessURI {
    @DexIgnore
    public static String uriSchemePrefix; // = "urn:fsl:fitness";

    @DexIgnore
    public static String formatDST(int i) {
        DecimalFormat decimalFormat = new DecimalFormat("0000");
        decimalFormat.setPositivePrefix("+");
        decimalFormat.setNegativePrefix("-");
        return decimalFormat.format((long) (i / 1000));
    }

    @DexIgnore
    public static String formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(date);
    }

    @DexIgnore
    public static String formatDay(int i, int i2, int i3) {
        DecimalFormat decimalFormat = new DecimalFormat("0000");
        DecimalFormat decimalFormat2 = new DecimalFormat("00");
        return decimalFormat.format((long) i) + "-" + decimalFormat2.format((long) i2) + "-" + decimalFormat2.format((long) i3);
    }

    @DexIgnore
    public static URI generateURI(SampleRaw sampleRaw) {
        return URI.create(uriSchemePrefix + ":" + ("sample:" + sampleRaw.getSourceType().getName().toLowerCase()) + ":" + sampleRaw.getSourceId() + ":" + formatDate(sampleRaw.getStartTime()));
    }

    @DexIgnore
    public static URI generateURI(SampleDay sampleDay) {
        String formatDay = formatDay(sampleDay.year, sampleDay.month, sampleDay.day);
        String timezoneName = sampleDay.getTimezoneName();
        String formatDST = formatDST(sampleDay.getDstOffset());
        return URI.create(uriSchemePrefix + ":" + "sample:day" + ":" + formatDay + ":" + timezoneName + ":" + formatDST);
    }

    @DexIgnore
    public static URI generateURI(DailyGoal dailyGoal) {
        String formatDay = formatDay(dailyGoal.year, dailyGoal.month, dailyGoal.day);
        return URI.create(uriSchemePrefix + ":" + "goal:day" + ":" + formatDay);
    }
}
