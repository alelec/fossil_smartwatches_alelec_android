package com.fossil.wearables.fsl.history;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.BaseModel;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HistoryProviderImpl extends BaseDbProvider implements HistoryProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "history.db";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                try {
                    HistoryProviderImpl.this.databaseHelper.dropTables(HistoryProviderImpl.this.getDbEntities());
                    HistoryProviderImpl.this.databaseHelper.createTables(HistoryProviderImpl.this.getDbEntities());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
        }
    }

    @DexIgnore
    public HistoryProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<HistoryItem, Integer> getHistoryItemDao() throws SQLException {
        return this.databaseHelper.getDao(HistoryItem.class);
    }

    @DexIgnore
    public List<HistoryItem> getAllItems() {
        ArrayList arrayList = new ArrayList();
        try {
            return getHistoryItemDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{HistoryItem.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public List<HistoryItem> getItems(Calendar calendar) {
        ArrayList arrayList = new ArrayList();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        long timeInMillis = calendar.getTimeInMillis();
        calendar.add(5, 1);
        calendar.add(14, -1);
        long timeInMillis2 = calendar.getTimeInMillis();
        try {
            QueryBuilder queryBuilder = getHistoryItemDao().queryBuilder();
            queryBuilder.where().ge("timestamp", Long.valueOf(timeInMillis)).and().le("timestamp", Long.valueOf(timeInMillis2));
            arrayList.addAll(getHistoryItemDao().query(queryBuilder.prepare()));
        } catch (Exception e) {
            String str = this.TAG;
            Log.w(str, "Something bad happened " + e.getMessage());
        }
        return arrayList;
    }

    @DexIgnore
    public List<HistoryItem> getLatestItems(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder queryBuilder = getHistoryItemDao().queryBuilder();
            queryBuilder.limit(Long.valueOf(j));
            queryBuilder.orderBy("timestamp", false);
            return getHistoryItemDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public void removeAllItems() {
        removeItems(getAllItems());
    }

    @DexIgnore
    public void removeItem(HistoryItem historyItem) {
        if (historyItem != null) {
            try {
                getHistoryItemDao().delete(historyItem);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void removeItems(List<HistoryItem> list) {
        if (list != null && list.size() > 0) {
            try {
                getHistoryItemDao().delete(list);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(7:8|9|10|11|12|13|14) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004f */
    public void saveItem(HistoryItem historyItem) {
        if (historyItem != null) {
            String str = this.TAG;
            Log.d(str, "Trying to save history item item " + historyItem.getType());
            try {
                Object queryForSameId = getHistoryItemDao().queryForSameId(historyItem);
                if (queryForSameId != null) {
                    try {
                        historyItem.setDbRowId(((BaseModel) queryForSameId).getDbRowId());
                    } catch (ClassCastException e) {
                        Log.w(this.TAG, "Could not check if id exists. Trying ref...");
                        Field declaredField = queryForSameId.getClass().getDeclaredField("dbRowId");
                        declaredField.setAccessible(true);
                        historyItem.setDbRowId(declaredField.getInt(queryForSameId));
                        String str2 = this.TAG;
                        Log.e(str2, "Reflection failed! " + e.getMessage());
                    }
                }
                getHistoryItemDao().createOrUpdate(historyItem);
            } catch (SQLException e2) {
                String str3 = this.TAG;
                Log.e(str3, "Error saving history item: " + e2.getMessage());
                e2.printStackTrace();
            }
        } else {
            Log.e(this.TAG, "Cannot save a null history item!");
        }
    }
}
