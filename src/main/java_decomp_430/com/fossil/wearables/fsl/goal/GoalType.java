package com.fossil.wearables.fsl.goal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum GoalType {
    CUSTOM,
    LOCATION,
    ACTIVITY,
    SOCIAL_SHARE
}
