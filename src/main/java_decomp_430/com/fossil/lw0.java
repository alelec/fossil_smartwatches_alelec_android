package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lw0 extends em0 {
    @DexIgnore
    public static /* final */ su0 CREATOR; // = new su0((qg6) null);
    @DexIgnore
    public /* final */ bd0 c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore
    public lw0(byte b, bd0 bd0, byte b2) {
        super(xh0.BATTERY_EVENT, b);
        this.c = bd0;
        this.d = b2;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.STATE, (Object) cw0.a((Enum<?>) this.c)), bm0.PERCENTAGE, (Object) Byte.valueOf(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
    }

    @DexIgnore
    public lw0(Parcel parcel) {
        super(parcel);
        bd0 a = bd0.c.a(parcel.readByte());
        this.c = a == null ? bd0.LOW : a;
        this.d = parcel.readByte();
    }
}
