package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sw2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sw2> CREATOR; // = new tw2();
    @DexIgnore
    public boolean a;
    @DexIgnore
    public long b;
    @DexIgnore
    public float c;
    @DexIgnore
    public long d;
    @DexIgnore
    public int e;

    @DexIgnore
    public sw2() {
        this(true, 50, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, RecyclerView.FOREVER_NS, Integer.MAX_VALUE);
    }

    @DexIgnore
    public sw2(boolean z, long j, float f, long j2, int i) {
        this.a = z;
        this.b = j;
        this.c = f;
        this.d = j2;
        this.e = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof sw2)) {
            return false;
        }
        sw2 sw2 = (sw2) obj;
        return this.a == sw2.a && this.b == sw2.b && Float.compare(this.c, sw2.c) == 0 && this.d == sw2.d && this.e == sw2.e;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(Boolean.valueOf(this.a), Long.valueOf(this.b), Float.valueOf(this.c), Long.valueOf(this.d), Integer.valueOf(this.e));
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOrientationRequest[mShouldUseMag=");
        sb.append(this.a);
        sb.append(" mMinimumSamplingPeriodMs=");
        sb.append(this.b);
        sb.append(" mSmallestAngleChangeRadians=");
        sb.append(this.c);
        long j = this.d;
        if (j != RecyclerView.FOREVER_NS) {
            sb.append(" expireIn=");
            sb.append(j - SystemClock.elapsedRealtime());
            sb.append("ms");
        }
        if (this.e != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(this.e);
        }
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d);
        g22.a(parcel, 5, this.e);
        g22.a(parcel, a2);
    }
}
