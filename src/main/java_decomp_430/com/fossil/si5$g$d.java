package com.fossil;

import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class si5$g$d extends sf6 implements ig6<il6, xe6<? super List<HeartRateSample>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si5$g$d(HeartRateDetailPresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        si5$g$d si5_g_d = new si5$g$d(this.this$0, xe6);
        si5_g_d.p$ = (il6) obj;
        return si5_g_d;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((si5$g$d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            HeartRateDetailPresenter heartRateDetailPresenter = this.this$0.this$0;
            return heartRateDetailPresenter.a(heartRateDetailPresenter.f, (List<HeartRateSample>) this.this$0.this$0.i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
