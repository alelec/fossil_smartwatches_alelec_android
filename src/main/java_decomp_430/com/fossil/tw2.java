package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tw2 implements Parcelable.Creator<sw2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 50;
        long j2 = Long.MAX_VALUE;
        boolean z = true;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = Integer.MAX_VALUE;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                z = f22.i(parcel2, a);
            } else if (a2 == 2) {
                j = f22.s(parcel2, a);
            } else if (a2 == 3) {
                f = f22.n(parcel2, a);
            } else if (a2 == 4) {
                j2 = f22.s(parcel2, a);
            } else if (a2 != 5) {
                f22.v(parcel2, a);
            } else {
                i = f22.q(parcel2, a);
            }
        }
        f22.h(parcel2, b);
        return new sw2(z, j, f, j2, i);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new sw2[i];
    }
}
