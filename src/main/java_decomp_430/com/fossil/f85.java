package com.fossil;

import com.google.android.libraries.places.api.net.PlacesClient;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface f85 extends k24<e85> {
    @DexIgnore
    void L0();

    @DexIgnore
    void a();

    @DexIgnore
    void a(PlacesClient placesClient);

    @DexIgnore
    void b();

    @DexIgnore
    void l0();

    @DexIgnore
    void o(List<WeatherLocationWrapper> list);

    @DexIgnore
    void u(boolean z);
}
