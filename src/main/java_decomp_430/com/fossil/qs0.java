package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs0 extends ul0 {
    @DexIgnore
    public static long g; // = 60000;
    @DexIgnore
    public static /* final */ qs0 h; // = new qs0();

    @DexIgnore
    public qs0() {
        super("sdk_log", 204800, 20971520, "sdk_log", "sdklog", new ge0("", "", ""), 1800, new fp0(), lp0.SDK_LOG_PREFERENCE, true);
    }

    @DexIgnore
    public long a() {
        return g;
    }

    @DexIgnore
    public final void a(Exception exc) {
        try {
            JSONObject jSONObject = new JSONObject();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            exc.printStackTrace(new PrintWriter(byteArrayOutputStream, true));
            bm0 bm0 = bm0.STACK_TRACE;
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            wg6.a(byteArray, "byteArrayOutputStream.toByteArray()");
            cw0.a(jSONObject, bm0, (Object) new String(byteArray, mi0.A.f()));
            String localizedMessage = exc.getLocalizedMessage();
            if (localizedMessage == null) {
                localizedMessage = exc.toString();
            }
            String str = localizedMessage;
            og0 og0 = og0.EXCEPTION;
            String canonicalName = exc.getClass().getCanonicalName();
            if (canonicalName == null) {
                canonicalName = "";
            }
            String str2 = canonicalName;
            String uuid = UUID.randomUUID().toString();
            wg6.a(uuid, "UUID.randomUUID().toString()");
            a(new nn0(str, og0, "", str2, uuid, false, (String) null, (r40) null, (bw0) null, jSONObject, 448));
        } catch (Exception unused) {
        }
    }
}
