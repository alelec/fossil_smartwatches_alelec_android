package com.fossil;

import com.fossil.eo3;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn3<E> extends nm3<E> {
    @DexIgnore
    public static /* final */ tn3<Comparable> NATURAL_EMPTY_SET; // = new tn3<>(zl3.of(), jn3.natural());
    @DexIgnore
    public /* final */ transient zl3<E> b;

    @DexIgnore
    public tn3(zl3<E> zl3, Comparator<? super E> comparator) {
        super(comparator);
        this.b = zl3;
    }

    @DexIgnore
    public final int a(Object obj) throws ClassCastException {
        return Collections.binarySearch(this.b, obj, unsafeComparator());
    }

    @DexIgnore
    public E ceiling(E e) {
        int tailIndex = tailIndex(e, true);
        if (tailIndex == size()) {
            return null;
        }
        return this.b.get(tailIndex);
    }

    @DexIgnore
    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return a(obj) >= 0;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof dn3) {
            collection = ((dn3) collection).elementSet();
        }
        if (!do3.a(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        kn3 c = qm3.c(iterator());
        Iterator<?> it = collection.iterator();
        Object next = it.next();
        while (c.hasNext()) {
            try {
                int unsafeCompare = unsafeCompare(c.peek(), next);
                if (unsafeCompare < 0) {
                    c.next();
                } else if (unsafeCompare == 0) {
                    if (!it.hasNext()) {
                        return true;
                    }
                    next = it.next();
                } else if (unsafeCompare > 0) {
                    break;
                }
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        return this.b.copyIntoArray(objArr, i);
    }

    @DexIgnore
    public zl3<E> createAsList() {
        return size() <= 1 ? this.b : new km3(this, this.b);
    }

    @DexIgnore
    public nm3<E> createDescendingSet() {
        jn3<S> reverse = jn3.from(this.comparator).reverse();
        return isEmpty() ? nm3.emptySet(reverse) : new tn3(this.b.reverse(), reverse);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034 A[Catch:{ ClassCastException | NoSuchElementException -> 0x0046 }] */
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Set)) {
            return false;
        }
        Set set = (Set) obj;
        if (size() != set.size()) {
            return false;
        }
        if (isEmpty()) {
            return true;
        }
        if (!do3.a(this.comparator, set)) {
            return containsAll(set);
        }
        Iterator it = set.iterator();
        try {
            jo3 it2 = iterator();
            while (it2.hasNext()) {
                Object next = it2.next();
                Object next2 = it.next();
                if (next2 == null || unsafeCompare(next, next2) != 0) {
                    return false;
                }
                while (it2.hasNext()) {
                }
            }
            return true;
        } catch (ClassCastException | NoSuchElementException unused) {
            return false;
        }
    }

    @DexIgnore
    public E first() {
        if (!isEmpty()) {
            return this.b.get(0);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public E floor(E e) {
        int headIndex = headIndex(e, true) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.b.get(headIndex);
    }

    @DexIgnore
    public tn3<E> getSubSet(int i, int i2) {
        if (i == 0 && i2 == size()) {
            return this;
        }
        if (i < i2) {
            return new tn3<>(this.b.subList(i, i2), this.comparator);
        }
        return nm3.emptySet(this.comparator);
    }

    @DexIgnore
    public int headIndex(E e, boolean z) {
        zl3<E> zl3 = this.b;
        jk3.a(e);
        return eo3.a(zl3, e, comparator(), z ? eo3.c.FIRST_AFTER : eo3.c.FIRST_PRESENT, eo3.b.NEXT_HIGHER);
    }

    @DexIgnore
    public nm3<E> headSetImpl(E e, boolean z) {
        return getSubSet(0, headIndex(e, z));
    }

    @DexIgnore
    public E higher(E e) {
        int tailIndex = tailIndex(e, false);
        if (tailIndex == size()) {
            return null;
        }
        return this.b.get(tailIndex);
    }

    @DexIgnore
    public int indexOf(Object obj) {
        if (obj == null) {
            return -1;
        }
        try {
            int a = eo3.a(this.b, obj, unsafeComparator(), eo3.c.ANY_PRESENT, eo3.b.INVERTED_INSERTION_INDEX);
            if (a >= 0) {
                return a;
            }
            return -1;
        } catch (ClassCastException unused) {
            return -1;
        }
    }

    @DexIgnore
    public boolean isPartialView() {
        return this.b.isPartialView();
    }

    @DexIgnore
    public E last() {
        if (!isEmpty()) {
            return this.b.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public E lower(E e) {
        int headIndex = headIndex(e, false) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.b.get(headIndex);
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }

    @DexIgnore
    public nm3<E> subSetImpl(E e, boolean z, E e2, boolean z2) {
        return tailSetImpl(e, z).headSetImpl(e2, z2);
    }

    @DexIgnore
    public int tailIndex(E e, boolean z) {
        zl3<E> zl3 = this.b;
        jk3.a(e);
        return eo3.a(zl3, e, comparator(), z ? eo3.c.FIRST_PRESENT : eo3.c.FIRST_AFTER, eo3.b.NEXT_HIGHER);
    }

    @DexIgnore
    public nm3<E> tailSetImpl(E e, boolean z) {
        return getSubSet(tailIndex(e, z), size());
    }

    @DexIgnore
    public Comparator<Object> unsafeComparator() {
        return this.comparator;
    }

    @DexIgnore
    public jo3<E> descendingIterator() {
        return this.b.reverse().iterator();
    }

    @DexIgnore
    public jo3<E> iterator() {
        return this.b.iterator();
    }
}
