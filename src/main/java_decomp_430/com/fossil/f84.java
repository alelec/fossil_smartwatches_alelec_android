package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class f84 extends ViewDataBinding {
    @DexIgnore
    public /* final */ TodayHeartRateChart q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ View u;
    @DexIgnore
    public /* final */ View v;

    @DexIgnore
    public f84(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, TodayHeartRateChart todayHeartRateChart, FlexibleButton flexibleButton, ImageView imageView, ImageView imageView2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view2, View view3) {
        super(obj, view, i);
        this.q = todayHeartRateChart;
        this.r = flexibleButton;
        this.s = imageView;
        this.t = imageView2;
        this.u = view2;
        this.v = view3;
    }
}
