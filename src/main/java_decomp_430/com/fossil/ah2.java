package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ah2 extends i12<dg2> {
    @DexIgnore
    public /* final */ String E;
    @DexIgnore
    public /* final */ vg2<dg2> F; // = new bh2(this);

    @DexIgnore
    public ah2(Context context, Looper looper, wv1.b bVar, wv1.c cVar, String str, e12 e12) {
        super(context, looper, 23, e12, bVar, cVar);
        this.E = str;
    }

    @DexIgnore
    public String A() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    @DexIgnore
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof dg2 ? (dg2) queryLocalInterface : new eg2(iBinder);
    }

    @DexIgnore
    public int j() {
        return 11925000;
    }

    @DexIgnore
    public Bundle v() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.E);
        return bundle;
    }

    @DexIgnore
    public String z() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }
}
