package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al3<F, T> extends jn3<F> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ ck3<F, ? extends T> function;
    @DexIgnore
    public /* final */ jn3<T> ordering;

    @DexIgnore
    public al3(ck3<F, ? extends T> ck3, jn3<T> jn3) {
        jk3.a(ck3);
        this.function = ck3;
        jk3.a(jn3);
        this.ordering = jn3;
    }

    @DexIgnore
    public int compare(F f, F f2) {
        return this.ordering.compare(this.function.apply(f), this.function.apply(f2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof al3)) {
            return false;
        }
        al3 al3 = (al3) obj;
        if (!this.function.equals(al3.function) || !this.ordering.equals(al3.ordering)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return gk3.a(this.function, this.ordering);
    }

    @DexIgnore
    public String toString() {
        return this.ordering + ".onResultOf(" + this.function + ")";
    }
}
