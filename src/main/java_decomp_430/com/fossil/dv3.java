package com.fossil;

import java.io.ObjectStreamException;
import java.math.BigDecimal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dv3 extends Number {
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public dv3(String str) {
        this.value = str;
    }

    @DexIgnore
    private Object writeReplace() throws ObjectStreamException {
        return new BigDecimal(this.value);
    }

    @DexIgnore
    public double doubleValue() {
        return Double.parseDouble(this.value);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dv3)) {
            return false;
        }
        String str = this.value;
        String str2 = ((dv3) obj).value;
        if (str == str2 || str.equals(str2)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public float floatValue() {
        return Float.parseFloat(this.value);
    }

    @DexIgnore
    public int hashCode() {
        return this.value.hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        return (int) java.lang.Long.parseLong(r2.value);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        return new java.math.BigDecimal(r2.value).intValue();
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0007 */
    public int intValue() {
        return Integer.parseInt(this.value);
    }

    @DexIgnore
    public long longValue() {
        try {
            return Long.parseLong(this.value);
        } catch (NumberFormatException unused) {
            return new BigDecimal(this.value).longValue();
        }
    }

    @DexIgnore
    public String toString() {
        return this.value;
    }
}
