package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hu6 extends nu6 {
    @DexIgnore
    public ju6 option;

    @DexIgnore
    public hu6(String str) {
        super(str);
    }

    @DexIgnore
    public ju6 getOption() {
        return this.option;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public hu6(ju6 ju6) {
        this(r0.toString());
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Missing argument for option: ");
        stringBuffer.append(ju6.getKey());
        this.option = ju6;
    }
}
