package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl2<T> implements dl2<T> {
    @DexIgnore
    public volatile dl2<T> a;
    @DexIgnore
    public volatile boolean b;
    @DexIgnore
    public T c;

    @DexIgnore
    public jl2(dl2<T> dl2) {
        bl2.a(dl2);
        this.a = dl2;
    }

    @DexIgnore
    public final String toString() {
        Object obj = this.a;
        if (obj == null) {
            String valueOf = String.valueOf(this.c);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(">");
            obj = sb.toString();
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @DexIgnore
    public final T zza() {
        if (!this.b) {
            synchronized (this) {
                if (!this.b) {
                    T zza = this.a.zza();
                    this.c = zza;
                    this.b = true;
                    this.a = null;
                    return zza;
                }
            }
        }
        return this.c;
    }
}
