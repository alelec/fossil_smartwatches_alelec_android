package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l32 implements Parcelable.Creator<h12> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v5, types: [java.lang.Object[]] */
    /* JADX WARNING: type inference failed for: r2v6, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        String str = null;
        IBinder iBinder = null;
        Scope[] scopeArr = null;
        Bundle bundle = null;
        Account account = null;
        iv1[] iv1Arr = null;
        iv1[] iv1Arr2 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel2, a);
                    break;
                case 2:
                    i2 = f22.q(parcel2, a);
                    break;
                case 3:
                    i3 = f22.q(parcel2, a);
                    break;
                case 4:
                    str = f22.e(parcel2, a);
                    break;
                case 5:
                    iBinder = f22.p(parcel2, a);
                    break;
                case 6:
                    scopeArr = f22.b(parcel2, a, Scope.CREATOR);
                    break;
                case 7:
                    bundle = f22.a(parcel2, a);
                    break;
                case 8:
                    account = f22.a(parcel2, a, Account.CREATOR);
                    break;
                case 10:
                    iv1Arr = f22.b(parcel2, a, iv1.CREATOR);
                    break;
                case 11:
                    iv1Arr2 = f22.b(parcel2, a, iv1.CREATOR);
                    break;
                case 12:
                    z = f22.i(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new h12(i, i2, i3, str, iBinder, scopeArr, bundle, account, iv1Arr, iv1Arr2, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new h12[i];
    }
}
