package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import com.fossil.ml;
import com.fossil.tl;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lm extends bm {
    @DexIgnore
    public static lm j;
    @DexIgnore
    public static lm k;
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public Context a;
    @DexIgnore
    public ml b;
    @DexIgnore
    public WorkDatabase c;
    @DexIgnore
    public to d;
    @DexIgnore
    public List<hm> e;
    @DexIgnore
    public gm f;
    @DexIgnore
    public ko g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BroadcastReceiver.PendingResult i;

    @DexIgnore
    public lm(Context context, ml mlVar, to toVar) {
        this(context, mlVar, toVar, context.getResources().getBoolean(yl.workmanager_test_configuration));
    }

    @DexIgnore
    @Deprecated
    public static lm a() {
        synchronized (l) {
            if (j != null) {
                lm lmVar = j;
                return lmVar;
            }
            lm lmVar2 = k;
            return lmVar2;
        }
    }

    @DexIgnore
    public Context b() {
        return this.a;
    }

    @DexIgnore
    public ml c() {
        return this.b;
    }

    @DexIgnore
    public ko d() {
        return this.g;
    }

    @DexIgnore
    public gm e() {
        return this.f;
    }

    @DexIgnore
    public List<hm> f() {
        return this.e;
    }

    @DexIgnore
    public WorkDatabase g() {
        return this.c;
    }

    @DexIgnore
    public to h() {
        return this.d;
    }

    @DexIgnore
    public void i() {
        synchronized (l) {
            this.h = true;
            if (this.i != null) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void j() {
        if (Build.VERSION.SDK_INT >= 23) {
            wm.a(b());
        }
        g().d().d();
        im.a(c(), g(), f());
    }

    @DexIgnore
    public void b(String str) {
        this.d.a(new no(this, str));
    }

    @DexIgnore
    public lm(Context context, ml mlVar, to toVar, boolean z) {
        this(context, mlVar, toVar, WorkDatabase.a(context.getApplicationContext(), toVar.b(), z));
    }

    @DexIgnore
    public static lm a(Context context) {
        lm a2;
        synchronized (l) {
            a2 = a();
            if (a2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof ml.b) {
                    a(applicationContext, ((ml.b) applicationContext).a());
                    a2 = a(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return a2;
    }

    @DexIgnore
    public lm(Context context, ml mlVar, to toVar, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        tl.a((tl) new tl.a(mlVar.g()));
        Context context2 = context;
        ml mlVar2 = mlVar;
        to toVar2 = toVar;
        WorkDatabase workDatabase2 = workDatabase;
        List<hm> a2 = a(applicationContext, toVar);
        a(context2, mlVar2, toVar2, workDatabase2, a2, new gm(context2, mlVar2, toVar2, workDatabase2, a2));
    }

    @DexIgnore
    public static void a(Context context, ml mlVar) {
        synchronized (l) {
            if (j != null) {
                if (k != null) {
                    throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class levelJavadoc for more information.");
                }
            }
            if (j == null) {
                Context applicationContext = context.getApplicationContext();
                if (k == null) {
                    k = new lm(applicationContext, mlVar, new uo(mlVar.h()));
                }
                j = k;
            }
        }
    }

    @DexIgnore
    public wl a(String str, ql qlVar, List<vl> list) {
        return new jm(this, str, qlVar, list).a();
    }

    @DexIgnore
    public void a(String str) {
        a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public void a(String str, WorkerParameters.a aVar) {
        this.d.a(new mo(this, str, aVar));
    }

    @DexIgnore
    public void a(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public final void a(Context context, ml mlVar, to toVar, WorkDatabase workDatabase, List<hm> list, gm gmVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = mlVar;
        this.d = toVar;
        this.c = workDatabase;
        this.e = list;
        this.f = gmVar;
        this.g = new ko(this.a);
        this.h = false;
        this.d.a(new ForceStopRunnable(applicationContext, this));
    }

    @DexIgnore
    public List<hm> a(Context context, to toVar) {
        return Arrays.asList(new hm[]{im.a(context, this), new nm(context, toVar, this)});
    }
}
