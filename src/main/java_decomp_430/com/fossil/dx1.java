package com.fossil;

import android.os.RemoteException;
import com.fossil.rv1;
import com.fossil.rv1.b;
import com.fossil.uw1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dx1<A extends rv1.b, L> {
    @DexIgnore
    public /* final */ uw1.a<L> a;

    @DexIgnore
    public dx1(uw1.a<L> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public uw1.a<L> a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(A a2, rc3<Boolean> rc3) throws RemoteException;
}
