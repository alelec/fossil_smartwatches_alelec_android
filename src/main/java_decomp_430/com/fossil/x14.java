package com.fossil;

import com.portfolio.platform.migration.MigrationHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x14 implements Factory<on4> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<an4> b;
    @DexIgnore
    public /* final */ Provider<t24> c;
    @DexIgnore
    public /* final */ Provider<s24> d;

    @DexIgnore
    public x14(b14 b14, Provider<an4> provider, Provider<t24> provider2, Provider<s24> provider3) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static x14 a(b14 b14, Provider<an4> provider, Provider<t24> provider2, Provider<s24> provider3) {
        return new x14(b14, provider, provider2, provider3);
    }

    @DexIgnore
    public static on4 b(b14 b14, Provider<an4> provider, Provider<t24> provider2, Provider<s24> provider3) {
        return a(b14, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static MigrationHelper a(b14 b14, an4 an4, t24 t24, s24 s24) {
        MigrationHelper a2 = b14.a(an4, t24, s24);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public MigrationHelper get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
