package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(36:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|(3:35|36|38)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(38:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|38) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00ce */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[b.DOUBLE.ordinal()] = 1;
            a[b.FLOAT.ordinal()] = 2;
            a[b.INT64.ordinal()] = 3;
            a[b.UINT64.ordinal()] = 4;
            a[b.INT32.ordinal()] = 5;
            a[b.FIXED64.ordinal()] = 6;
            a[b.FIXED32.ordinal()] = 7;
            a[b.BOOL.ordinal()] = 8;
            a[b.BYTES.ordinal()] = 9;
            a[b.UINT32.ordinal()] = 10;
            a[b.SFIXED32.ordinal()] = 11;
            a[b.SFIXED64.ordinal()] = 12;
            a[b.SINT32.ordinal()] = 13;
            a[b.SINT64.ordinal()] = 14;
            a[b.STRING.ordinal()] = 15;
            a[b.GROUP.ordinal()] = 16;
            a[b.MESSAGE.ordinal()] = 17;
            try {
                a[b.ENUM.ordinal()] = 18;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        DOUBLE(c.DOUBLE, 1),
        FLOAT(c.FLOAT, 5),
        INT64(c.LONG, 0),
        UINT64(c.LONG, 0),
        INT32(c.INT, 0),
        FIXED64(c.LONG, 1),
        FIXED32(c.INT, 5),
        BOOL(c.BOOLEAN, 0),
        STRING(c.STRING, 2) {
            @DexIgnore
            public boolean isPackable() {
                return false;
            }
        },
        GROUP(c.MESSAGE, 3) {
            @DexIgnore
            public boolean isPackable() {
                return false;
            }
        },
        MESSAGE(c.MESSAGE, 2) {
            @DexIgnore
            public boolean isPackable() {
                return false;
            }
        },
        BYTES(c.BYTE_STRING, 2) {
            @DexIgnore
            public boolean isPackable() {
                return false;
            }
        },
        UINT32(c.INT, 0),
        ENUM(c.ENUM, 0),
        SFIXED32(c.INT, 5),
        SFIXED64(c.LONG, 1),
        SINT32(c.INT, 0),
        SINT64(c.LONG, 0);
        
        @DexIgnore
        public /* final */ c javaType;
        @DexIgnore
        public /* final */ int wireType;

        @DexIgnore
        public c getJavaType() {
            return this.javaType;
        }

        @DexIgnore
        public int getWireType() {
            return this.wireType;
        }

        @DexIgnore
        public boolean isPackable() {
            return true;
        }

        @DexIgnore
        public b(c cVar, int i) {
            this.javaType = cVar;
            this.wireType = i;
        }
    }

    @DexIgnore
    public enum c {
        INT(0),
        LONG(0L),
        FLOAT(Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)),
        DOUBLE(Double.valueOf(0.0d)),
        BOOLEAN(false),
        STRING(""),
        BYTE_STRING(sw3.EMPTY),
        ENUM((String) null),
        MESSAGE((String) null);
        
        @DexIgnore
        public /* final */ Object defaultDefault;

        @DexIgnore
        public c(Object obj) {
            this.defaultDefault = obj;
        }

        @DexIgnore
        public Object getDefaultDefault() {
            return this.defaultDefault;
        }
    }

    /*
    static {
        a(1, 3);
        a(1, 4);
        a(2, 0);
        a(3, 2);
    }
    */

    @DexIgnore
    public static int a(int i) {
        return i >>> 3;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    @DexIgnore
    public static int b(int i) {
        return i & 7;
    }
}
