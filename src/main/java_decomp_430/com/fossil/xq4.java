package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.BaseActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xq4 implements MembersInjector<wq4> {
    @DexIgnore
    public static void a(BaseActivity baseActivity, UserRepository userRepository) {
        baseActivity.p = userRepository;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, an4 an4) {
        baseActivity.q = an4;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, DeviceRepository deviceRepository) {
        baseActivity.r = deviceRepository;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, t24 t24) {
        baseActivity.s = t24;
    }

    @DexIgnore
    public static void a(BaseActivity baseActivity, os4 os4) {
        baseActivity.t = os4;
    }
}
