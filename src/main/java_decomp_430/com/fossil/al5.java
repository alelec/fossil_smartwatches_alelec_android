package com.fossil;

import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al5 implements MembersInjector<HelpActivity> {
    @DexIgnore
    public static void a(HelpActivity helpActivity, HelpPresenter helpPresenter) {
        helpActivity.B = helpPresenter;
    }
}
