package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mv1 extends qv1 {
    @DexIgnore
    public /* final */ int zzag;

    @DexIgnore
    public mv1(int i, String str, Intent intent) {
        super(str, intent);
        this.zzag = i;
    }

    @DexIgnore
    public int getConnectionStatusCode() {
        return this.zzag;
    }
}
