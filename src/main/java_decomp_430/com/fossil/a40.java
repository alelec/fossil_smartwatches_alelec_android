package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a40 implements g40 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ g40 b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public String d;

    @DexIgnore
    public a40(Context context, g40 g40) {
        this.a = context;
        this.b = g40;
    }

    @DexIgnore
    public String a() {
        if (!this.c) {
            this.d = z86.o(this.a);
            this.c = true;
        }
        String str = this.d;
        if (str != null) {
            return str;
        }
        g40 g40 = this.b;
        if (g40 != null) {
            return g40.a();
        }
        return null;
    }
}
