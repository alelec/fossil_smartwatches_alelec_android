package com.fossil;

import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$showDetailChart$1$maxValue$1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class gh5$f$a extends sf6 implements ig6<il6, xe6<? super Integer>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $data;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gh5$f$a(ArrayList arrayList, xe6 xe6) {
        super(2, xe6);
        this.$data = arrayList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        gh5$f$a gh5_f_a = new gh5$f$a(this.$data, xe6);
        gh5_f_a.p$ = (il6) obj;
        return gh5_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((gh5$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object obj2;
        ArrayList<ArrayList<BarChart.b>> c;
        ArrayList<BarChart.b> arrayList;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            Iterator it = this.$data.iterator();
            int i = 0;
            if (!it.hasNext()) {
                obj2 = null;
            } else {
                obj2 = it.next();
                if (it.hasNext()) {
                    ArrayList<BarChart.b> arrayList2 = ((BarChart.a) obj2).c().get(0);
                    wg6.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                    int i2 = 0;
                    for (BarChart.b e : arrayList2) {
                        i2 += hf6.a(e.e()).intValue();
                    }
                    Integer a = hf6.a(i2);
                    do {
                        Object next = it.next();
                        ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).c().get(0);
                        wg6.a((Object) arrayList3, "it.mListOfBarPoints[0]");
                        int i3 = 0;
                        for (BarChart.b e2 : arrayList3) {
                            i3 += hf6.a(e2.e()).intValue();
                        }
                        Integer a2 = hf6.a(i3);
                        if (a.compareTo(a2) < 0) {
                            obj2 = next;
                            a = a2;
                        }
                    } while (it.hasNext());
                }
            }
            BarChart.a aVar = (BarChart.a) obj2;
            if (aVar == null || (c = aVar.c()) == null || (arrayList = c.get(0)) == null) {
                return null;
            }
            for (BarChart.b e3 : arrayList) {
                i += hf6.a(e3.e()).intValue();
            }
            return hf6.a(i);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
