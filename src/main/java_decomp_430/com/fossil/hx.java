package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hx {
    @DexIgnore
    public static volatile boolean a; // = true;

    @DexIgnore
    public static Drawable a(Context context, Context context2, int i) {
        return a(context, context2, i, (Resources.Theme) null);
    }

    @DexIgnore
    public static Drawable b(Context context, int i, Resources.Theme theme) {
        return d7.a(context.getResources(), i, theme);
    }

    @DexIgnore
    public static Drawable c(Context context, int i, Resources.Theme theme) {
        if (theme != null) {
            context = new e1(context, theme);
        }
        return u0.c(context, i);
    }

    @DexIgnore
    public static Drawable a(Context context, int i, Resources.Theme theme) {
        return a(context, context, i, theme);
    }

    @DexIgnore
    public static Drawable a(Context context, Context context2, int i, Resources.Theme theme) {
        try {
            if (a) {
                return c(context2, i, theme);
            }
        } catch (NoClassDefFoundError unused) {
            a = false;
        } catch (IllegalStateException e) {
            if (!context.getPackageName().equals(context2.getPackageName())) {
                return w6.c(context2, i);
            }
            throw e;
        } catch (Resources.NotFoundException unused2) {
        }
        if (theme == null) {
            theme = context2.getTheme();
        }
        return b(context2, i, theme);
    }
}
