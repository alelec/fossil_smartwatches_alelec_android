package com.fossil;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w92 extends Binder {
    @DexIgnore
    public /* final */ s92 a;

    @DexIgnore
    public w92(s92 s92) {
        this.a = s92;
    }

    @DexIgnore
    public final void a(u92 u92) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.a.a.execute(new x92(this, u92));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
