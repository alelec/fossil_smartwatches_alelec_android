package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nw4 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nw4> CREATOR; // = new a();
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public CharSequence H;
    @DexIgnore
    public int I;
    @DexIgnore
    public Uri J;
    @DexIgnore
    public Bitmap.CompressFormat K;
    @DexIgnore
    public int L;
    @DexIgnore
    public int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public CropImageView.j O;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public Rect Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public boolean S;
    @DexIgnore
    public boolean T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public int V;
    @DexIgnore
    public boolean W;
    @DexIgnore
    public boolean X;
    @DexIgnore
    public CharSequence Y;
    @DexIgnore
    public int Z;
    @DexIgnore
    public CropImageView.c a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public CropImageView.d d;
    @DexIgnore
    public CropImageView.k e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public float s;
    @DexIgnore
    public int t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public int x;
    @DexIgnore
    public float y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<nw4> {
        @DexIgnore
        public nw4 createFromParcel(Parcel parcel) {
            return new nw4(parcel);
        }

        @DexIgnore
        public nw4[] newArray(int i) {
            return new nw4[i];
        }
    }

    @DexIgnore
    public nw4() {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        this.a = CropImageView.c.RECTANGLE;
        this.b = TypedValue.applyDimension(1, 3.0f, displayMetrics);
        this.c = TypedValue.applyDimension(1, 24.0f, displayMetrics);
        this.d = CropImageView.d.ON_TOUCH;
        this.e = CropImageView.k.FIT_CENTER;
        this.f = true;
        this.g = true;
        this.h = true;
        this.i = false;
        this.j = 4;
        this.o = 0.1f;
        this.p = false;
        this.q = 1;
        this.r = 1;
        this.s = TypedValue.applyDimension(1, 3.0f, displayMetrics);
        this.t = Color.argb(170, 255, 255, 255);
        this.u = TypedValue.applyDimension(1, 2.0f, displayMetrics);
        this.v = TypedValue.applyDimension(1, 5.0f, displayMetrics);
        this.w = TypedValue.applyDimension(1, 14.0f, displayMetrics);
        this.x = -1;
        this.y = TypedValue.applyDimension(1, 1.0f, displayMetrics);
        this.z = Color.argb(170, 255, 255, 255);
        this.A = Color.argb(MFNetworkReturnCode.RESPONSE_OK, 0, 0, 0);
        this.B = (int) TypedValue.applyDimension(1, 42.0f, displayMetrics);
        this.C = (int) TypedValue.applyDimension(1, 42.0f, displayMetrics);
        this.D = 40;
        this.E = 40;
        this.F = 99999;
        this.G = 99999;
        this.H = "";
        this.I = 0;
        this.J = Uri.EMPTY;
        this.K = Bitmap.CompressFormat.JPEG;
        this.L = 90;
        this.M = 0;
        this.N = 0;
        this.O = CropImageView.j.NONE;
        this.P = false;
        this.Q = null;
        this.R = -1;
        this.S = true;
        this.T = true;
        this.U = false;
        this.V = 90;
        this.W = false;
        this.X = false;
        this.Y = null;
        this.Z = 0;
    }

    @DexIgnore
    public void a() {
        if (this.j < 0) {
            throw new IllegalArgumentException("Cannot set max zoom to a number < 1");
        } else if (this.c >= 0.0f) {
            float f2 = this.o;
            if (f2 < 0.0f || ((double) f2) >= 0.5d) {
                throw new IllegalArgumentException("Cannot set initial crop window padding value to a number < 0 or >= 0.5");
            } else if (this.q <= 0) {
                throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
            } else if (this.r <= 0) {
                throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
            } else if (this.s < 0.0f) {
                throw new IllegalArgumentException("Cannot set line thickness value to a number less than 0.");
            } else if (this.u < 0.0f) {
                throw new IllegalArgumentException("Cannot set corner thickness value to a number less than 0.");
            } else if (this.y < 0.0f) {
                throw new IllegalArgumentException("Cannot set guidelines thickness value to a number less than 0.");
            } else if (this.C >= 0) {
                int i2 = this.D;
                if (i2 >= 0) {
                    int i3 = this.E;
                    if (i3 < 0) {
                        throw new IllegalArgumentException("Cannot set min crop result height value to a number < 0 ");
                    } else if (this.F < i2) {
                        throw new IllegalArgumentException("Cannot set max crop result width to smaller value than min crop result width");
                    } else if (this.G < i3) {
                        throw new IllegalArgumentException("Cannot set max crop result height to smaller value than min crop result height");
                    } else if (this.M < 0) {
                        throw new IllegalArgumentException("Cannot set request width value to a number < 0 ");
                    } else if (this.N >= 0) {
                        int i4 = this.V;
                        if (i4 < 0 || i4 > 360) {
                            throw new IllegalArgumentException("Cannot set rotation degrees value to a number < 0 or > 360");
                        }
                    } else {
                        throw new IllegalArgumentException("Cannot set request height value to a number < 0 ");
                    }
                } else {
                    throw new IllegalArgumentException("Cannot set min crop result width value to a number < 0 ");
                }
            } else {
                throw new IllegalArgumentException("Cannot set min crop window height value to a number < 0 ");
            }
        } else {
            throw new IllegalArgumentException("Cannot set touch radius value to a number <= 0 ");
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
        parcel.writeInt(this.d.ordinal());
        parcel.writeInt(this.e.ordinal());
        parcel.writeByte(this.f ? (byte) 1 : 0);
        parcel.writeByte(this.g ? (byte) 1 : 0);
        parcel.writeByte(this.h ? (byte) 1 : 0);
        parcel.writeByte(this.i ? (byte) 1 : 0);
        parcel.writeInt(this.j);
        parcel.writeFloat(this.o);
        parcel.writeByte(this.p ? (byte) 1 : 0);
        parcel.writeInt(this.q);
        parcel.writeInt(this.r);
        parcel.writeFloat(this.s);
        parcel.writeInt(this.t);
        parcel.writeFloat(this.u);
        parcel.writeFloat(this.v);
        parcel.writeFloat(this.w);
        parcel.writeInt(this.x);
        parcel.writeFloat(this.y);
        parcel.writeInt(this.z);
        parcel.writeInt(this.A);
        parcel.writeInt(this.B);
        parcel.writeInt(this.C);
        parcel.writeInt(this.D);
        parcel.writeInt(this.E);
        parcel.writeInt(this.F);
        parcel.writeInt(this.G);
        TextUtils.writeToParcel(this.H, parcel, i2);
        parcel.writeInt(this.I);
        parcel.writeParcelable(this.J, i2);
        parcel.writeString(this.K.name());
        parcel.writeInt(this.L);
        parcel.writeInt(this.M);
        parcel.writeInt(this.N);
        parcel.writeInt(this.O.ordinal());
        parcel.writeInt(this.P ? 1 : 0);
        parcel.writeParcelable(this.Q, i2);
        parcel.writeInt(this.R);
        parcel.writeByte(this.S ? (byte) 1 : 0);
        parcel.writeByte(this.T ? (byte) 1 : 0);
        parcel.writeByte(this.U ? (byte) 1 : 0);
        parcel.writeInt(this.V);
        parcel.writeByte(this.W ? (byte) 1 : 0);
        parcel.writeByte(this.X ? (byte) 1 : 0);
        TextUtils.writeToParcel(this.Y, parcel, i2);
        parcel.writeInt(this.Z);
    }

    @DexIgnore
    public nw4(Parcel parcel) {
        this.a = CropImageView.c.values()[parcel.readInt()];
        this.b = parcel.readFloat();
        this.c = parcel.readFloat();
        this.d = CropImageView.d.values()[parcel.readInt()];
        this.e = CropImageView.k.values()[parcel.readInt()];
        boolean z2 = true;
        this.f = parcel.readByte() != 0;
        this.g = parcel.readByte() != 0;
        this.h = parcel.readByte() != 0;
        this.i = parcel.readByte() != 0;
        this.j = parcel.readInt();
        this.o = parcel.readFloat();
        this.p = parcel.readByte() != 0;
        this.q = parcel.readInt();
        this.r = parcel.readInt();
        this.s = parcel.readFloat();
        this.t = parcel.readInt();
        this.u = parcel.readFloat();
        this.v = parcel.readFloat();
        this.w = parcel.readFloat();
        this.x = parcel.readInt();
        this.y = parcel.readFloat();
        this.z = parcel.readInt();
        this.A = parcel.readInt();
        this.B = parcel.readInt();
        this.C = parcel.readInt();
        this.D = parcel.readInt();
        this.E = parcel.readInt();
        this.F = parcel.readInt();
        this.G = parcel.readInt();
        this.H = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.I = parcel.readInt();
        this.J = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.K = Bitmap.CompressFormat.valueOf(parcel.readString());
        this.L = parcel.readInt();
        this.M = parcel.readInt();
        this.N = parcel.readInt();
        this.O = CropImageView.j.values()[parcel.readInt()];
        this.P = parcel.readByte() != 0;
        this.Q = (Rect) parcel.readParcelable(Rect.class.getClassLoader());
        this.R = parcel.readInt();
        this.S = parcel.readByte() != 0;
        this.T = parcel.readByte() != 0;
        this.U = parcel.readByte() != 0;
        this.V = parcel.readInt();
        this.W = parcel.readByte() != 0;
        this.X = parcel.readByte() == 0 ? false : z2;
        this.Y = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.Z = parcel.readInt();
    }
}
