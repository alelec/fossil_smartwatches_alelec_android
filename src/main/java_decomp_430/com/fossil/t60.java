package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ h70 b;
    @DexIgnore
    public /* final */ c70 c;
    @DexIgnore
    public /* final */ f70 d;
    @DexIgnore
    public /* final */ i70 e;
    @DexIgnore
    public /* final */ e70 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<t60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final t60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                int i = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                h70 a = h70.c.a((i >> 0) & 1);
                c70 a2 = c70.c.a((i >> 1) & 1);
                f70 a3 = f70.c.a((i >> 2) & 1);
                i70 a4 = i70.c.a((i >> 3) & 1);
                e70 a5 = e70.c.a((i >> 4) & 1);
                if (a != null && a2 != null && a3 != null && a4 != null && a5 != null) {
                    return new t60(a, a2, a3, a4, a5);
                }
                throw new IllegalArgumentException("Invalid data properties");
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public t60 createFromParcel(Parcel parcel) {
            return new t60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new t60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m57createFromParcel(Parcel parcel) {
            return new t60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public t60(h70 h70, c70 c70, f70 f70, i70 i70, e70 e70) {
        super(s60.DISPLAY_UNIT);
        this.b = h70;
        this.c = c70;
        this.d = f70;
        this.e = i70;
        this.f = e70;
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((this.f.a() << 4) | (this.b.a() << 0) | 0 | (this.c.a() << 1) | (this.d.a() << 2) | (this.e.a() << 3)).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(t60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            t60 t60 = (t60) obj;
            return this.b == t60.b && this.c == t60.c && this.d == t60.d && this.e == t60.e && this.f == t60.f;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DisplayUnitConfig");
    }

    @DexIgnore
    public final c70 getCaloriesUnit() {
        return this.c;
    }

    @DexIgnore
    public final e70 getDateFormat() {
        return this.f;
    }

    @DexIgnore
    public final f70 getDistanceUnit() {
        return this.d;
    }

    @DexIgnore
    public final h70 getTemperatureUnit() {
        return this.b;
    }

    @DexIgnore
    public final i70 getTimeFormat() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        return this.f.hashCode() + ((hashCode3 + ((hashCode2 + ((hashCode + (this.b.hashCode() * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
        if (parcel != null) {
            parcel.writeString(this.f.name());
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(jSONObject, bm0.TEMPERATURE, (Object) cw0.a((Enum<?>) this.b)), bm0.CALORIES, (Object) cw0.a((Enum<?>) this.c)), bm0.DISTANCE, (Object) cw0.a((Enum<?>) this.d)), bm0.TIME, (Object) cw0.a((Enum<?>) this.e)), bm0.DATE, (Object) cw0.a((Enum<?>) this.f));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ t60(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.b = h70.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                wg6.a(readString2, "parcel.readString()!!");
                this.c = c70.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    wg6.a(readString3, "parcel.readString()!!");
                    this.d = f70.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        wg6.a(readString4, "parcel.readString()!!");
                        this.e = i70.valueOf(readString4);
                        String readString5 = parcel.readString();
                        if (readString5 != null) {
                            wg6.a(readString5, "parcel.readString()!!");
                            this.f = e70.valueOf(readString5);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }
}
