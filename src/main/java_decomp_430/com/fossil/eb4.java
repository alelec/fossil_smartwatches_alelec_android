package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eb4 extends db4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131362885, 1);
        z.put(2131363216, 2);
        z.put(2131362993, 3);
        z.put(2131362086, 4);
        z.put(2131363218, 5);
        z.put(2131363128, 6);
        z.put(2131362781, 7);
        z.put(2131362054, 8);
        z.put(2131362315, 9);
        z.put(2131362541, 10);
        z.put(2131362404, 11);
        z.put(2131362324, 12);
    }
    */

    @DexIgnore
    public eb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 13, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public eb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[8], objArr[4], objArr[9], objArr[12], objArr[11], objArr[10], objArr[7], objArr[0], objArr[1], objArr[3], objArr[6], objArr[2], objArr[5]);
        this.x = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
