package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d51 implements Parcelable.Creator<a71> {
    @DexIgnore
    public /* synthetic */ d51(qg6 qg6) {
    }

    @DexIgnore
    public a71 createFromParcel(Parcel parcel) {
        return new a71(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new a71[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m8createFromParcel(Parcel parcel) {
        return new a71(parcel, (qg6) null);
    }
}
