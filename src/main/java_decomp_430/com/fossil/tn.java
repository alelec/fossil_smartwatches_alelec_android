package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tn {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public tn(String str, int i) {
        this.a = str;
        this.b = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || tn.class != obj.getClass()) {
            return false;
        }
        tn tnVar = (tn) obj;
        if (this.b != tnVar.b) {
            return false;
        }
        return this.a.equals(tnVar.a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }
}
