package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface au {
    @DexIgnore
    Bitmap a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(Bitmap bitmap);

    @DexIgnore
    Bitmap b(int i, int i2, Bitmap.Config config);
}
