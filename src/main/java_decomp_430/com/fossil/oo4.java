package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo4 implements MembersInjector<AppPackageRemoveReceiver> {
    @DexIgnore
    public static void a(AppPackageRemoveReceiver appPackageRemoveReceiver, NotificationsRepository notificationsRepository) {
        appPackageRemoveReceiver.a = notificationsRepository;
    }
}
