package com.fossil;

import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class of6 {
    @DexIgnore
    public static /* final */ a a; // = new a((Method) null, (Method) null, (Method) null);
    @DexIgnore
    public static a b;
    @DexIgnore
    public static /* final */ of6 c; // = new of6();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public a(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }
    }

    @DexIgnore
    public final a a(gf6 gf6) {
        try {
            a aVar = new a(Class.class.getDeclaredMethod("getModule", new Class[0]), gf6.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), gf6.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod("name", new Class[0]));
            b = aVar;
            return aVar;
        } catch (Exception unused) {
            a aVar2 = a;
            b = aVar2;
            return aVar2;
        }
    }

    @DexIgnore
    public final String b(gf6 gf6) {
        Method method;
        Object invoke;
        Method method2;
        Object invoke2;
        wg6.b(gf6, "continuation");
        a aVar = b;
        if (aVar == null) {
            aVar = a(gf6);
        }
        if (aVar == a || (method = aVar.a) == null || (invoke = method.invoke(gf6.getClass(), new Object[0])) == null || (method2 = aVar.b) == null || (invoke2 = method2.invoke(invoke, new Object[0])) == null) {
            return null;
        }
        Method method3 = aVar.c;
        Object invoke3 = method3 != null ? method3.invoke(invoke2, new Object[0]) : null;
        if (!(invoke3 instanceof String)) {
            invoke3 = null;
        }
        return (String) invoke3;
    }
}
