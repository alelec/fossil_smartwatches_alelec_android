package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n04 {
    @DexIgnore
    public g04 a;
    @DexIgnore
    public f04 b;
    @DexIgnore
    public h04 c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public j04 e;

    @DexIgnore
    public static boolean b(int i) {
        return i >= 0 && i < 8;
    }

    @DexIgnore
    public j04 a() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.a);
        sb.append("\n ecLevel: ");
        sb.append(this.b);
        sb.append("\n version: ");
        sb.append(this.c);
        sb.append("\n maskPattern: ");
        sb.append(this.d);
        if (this.e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.e);
        }
        sb.append(">>\n");
        return sb.toString();
    }

    @DexIgnore
    public void a(g04 g04) {
        this.a = g04;
    }

    @DexIgnore
    public void a(f04 f04) {
        this.b = f04;
    }

    @DexIgnore
    public void a(h04 h04) {
        this.c = h04;
    }

    @DexIgnore
    public void a(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(j04 j04) {
        this.e = j04;
    }
}
