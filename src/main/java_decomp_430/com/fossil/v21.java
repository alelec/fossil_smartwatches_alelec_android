package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum v21 {
    GET((byte) 1),
    SET((byte) 2),
    RESPONSE((byte) 3);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public v21(byte b) {
        this.a = b;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
