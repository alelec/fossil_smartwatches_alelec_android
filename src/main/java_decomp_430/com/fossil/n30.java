package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n30 implements e40 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ e40[] b;
    @DexIgnore
    public /* final */ o30 c;

    @DexIgnore
    public n30(int i, e40... e40Arr) {
        this.a = i;
        this.b = e40Arr;
        this.c = new o30(i);
    }

    @DexIgnore
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.a) {
            return stackTraceElementArr;
        }
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (e40 e40 : this.b) {
            if (stackTraceElementArr2.length <= this.a) {
                break;
            }
            stackTraceElementArr2 = e40.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.a ? this.c.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
