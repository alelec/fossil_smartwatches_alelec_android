package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ti extends Drawable implements Animatable {
    @DexIgnore
    public static /* final */ Interpolator g; // = new LinearInterpolator();
    @DexIgnore
    public static /* final */ Interpolator h; // = new nc();
    @DexIgnore
    public static /* final */ int[] i; // = {-16777216};
    @DexIgnore
    public /* final */ c a; // = new c();
    @DexIgnore
    public float b;
    @DexIgnore
    public Resources c;
    @DexIgnore
    public Animator d;
    @DexIgnore
    public float e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ c a;

        @DexIgnore
        public a(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            ti.this.b(floatValue, this.a);
            ti.this.a(floatValue, this.a, false);
            ti.this.invalidateSelf();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ c a;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            ti.this.a(1.0f, this.a, true);
            this.a.l();
            this.a.j();
            ti tiVar = ti.this;
            if (tiVar.f) {
                tiVar.f = false;
                animator.cancel();
                animator.setDuration(1332);
                animator.start();
                this.a.a(false);
                return;
            }
            tiVar.e += 1.0f;
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            ti.this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ RectF a; // = new RectF();
        @DexIgnore
        public /* final */ Paint b; // = new Paint();
        @DexIgnore
        public /* final */ Paint c; // = new Paint();
        @DexIgnore
        public /* final */ Paint d; // = new Paint();
        @DexIgnore
        public float e; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float f; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float g; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float h; // = 5.0f;
        @DexIgnore
        public int[] i;
        @DexIgnore
        public int j;
        @DexIgnore
        public float k;
        @DexIgnore
        public float l;
        @DexIgnore
        public float m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public Path o;
        @DexIgnore
        public float p; // = 1.0f;
        @DexIgnore
        public float q;
        @DexIgnore
        public int r;
        @DexIgnore
        public int s;
        @DexIgnore
        public int t; // = 255;
        @DexIgnore
        public int u;

        @DexIgnore
        public c() {
            this.b.setStrokeCap(Paint.Cap.SQUARE);
            this.b.setAntiAlias(true);
            this.b.setStyle(Paint.Style.STROKE);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setAntiAlias(true);
            this.d.setColor(0);
        }

        @DexIgnore
        public void a(float f2, float f3) {
            this.r = (int) f2;
            this.s = (int) f3;
        }

        @DexIgnore
        public void b(int i2) {
            this.u = i2;
        }

        @DexIgnore
        public void c(int i2) {
            this.j = i2;
            this.u = this.i[this.j];
        }

        @DexIgnore
        public int d() {
            return (this.j + 1) % this.i.length;
        }

        @DexIgnore
        public void e(float f2) {
            this.e = f2;
        }

        @DexIgnore
        public void f(float f2) {
            this.h = f2;
            this.b.setStrokeWidth(f2);
        }

        @DexIgnore
        public float g() {
            return this.l;
        }

        @DexIgnore
        public float h() {
            return this.m;
        }

        @DexIgnore
        public float i() {
            return this.k;
        }

        @DexIgnore
        public void j() {
            c(d());
        }

        @DexIgnore
        public void k() {
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            e(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            c((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            d(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        public void l() {
            this.k = this.e;
            this.l = this.f;
            this.m = this.g;
        }

        @DexIgnore
        public float b() {
            return this.f;
        }

        @DexIgnore
        public void d(float f2) {
            this.g = f2;
        }

        @DexIgnore
        public float e() {
            return this.e;
        }

        @DexIgnore
        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.a;
            float f2 = this.q;
            float f3 = (this.h / 2.0f) + f2;
            if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f3 = (((float) Math.min(rect.width(), rect.height())) / 2.0f) - Math.max((((float) this.r) * this.p) / 2.0f, this.h / 2.0f);
            }
            rectF.set(((float) rect.centerX()) - f3, ((float) rect.centerY()) - f3, ((float) rect.centerX()) + f3, ((float) rect.centerY()) + f3);
            float f4 = this.e;
            float f5 = this.g;
            float f6 = (f4 + f5) * 360.0f;
            float f7 = ((this.f + f5) * 360.0f) - f6;
            this.b.setColor(this.u);
            this.b.setAlpha(this.t);
            float f8 = this.h / 2.0f;
            rectF.inset(f8, f8);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.d);
            float f9 = -f8;
            rectF.inset(f9, f9);
            canvas.drawArc(rectF, f6, f7, false, this.b);
            a(canvas, f6, f7, rectF);
        }

        @DexIgnore
        public void b(float f2) {
            this.q = f2;
        }

        @DexIgnore
        public int c() {
            return this.i[d()];
        }

        @DexIgnore
        public int f() {
            return this.i[this.j];
        }

        @DexIgnore
        public void c(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public void a(Canvas canvas, float f2, float f3, RectF rectF) {
            if (this.n) {
                Path path = this.o;
                if (path == null) {
                    this.o = new Path();
                    this.o.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    path.reset();
                }
                this.o.moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.o.lineTo(((float) this.r) * this.p, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                Path path2 = this.o;
                float f4 = this.p;
                path2.lineTo((((float) this.r) * f4) / 2.0f, ((float) this.s) * f4);
                this.o.offset(((Math.min(rectF.width(), rectF.height()) / 2.0f) + rectF.centerX()) - ((((float) this.r) * this.p) / 2.0f), rectF.centerY() + (this.h / 2.0f));
                this.o.close();
                this.c.setColor(this.u);
                this.c.setAlpha(this.t);
                canvas.save();
                canvas.rotate(f2 + f3, rectF.centerX(), rectF.centerY());
                canvas.drawPath(this.o, this.c);
                canvas.restore();
            }
        }

        @DexIgnore
        public void a(int[] iArr) {
            this.i = iArr;
            c(0);
        }

        @DexIgnore
        public void a(ColorFilter colorFilter) {
            this.b.setColorFilter(colorFilter);
        }

        @DexIgnore
        public void a(int i2) {
            this.t = i2;
        }

        @DexIgnore
        public int a() {
            return this.t;
        }

        @DexIgnore
        public void a(boolean z) {
            if (this.n != z) {
                this.n = z;
            }
        }

        @DexIgnore
        public void a(float f2) {
            if (f2 != this.p) {
                this.p = f2;
            }
        }
    }

    @DexIgnore
    public ti(Context context) {
        y8.a(context);
        this.c = context.getResources();
        this.a.a(i);
        d(2.5f);
        a();
    }

    @DexIgnore
    public final int a(float f2, int i2, int i3) {
        int i4 = (i2 >> 24) & 255;
        int i5 = (i2 >> 16) & 255;
        int i6 = (i2 >> 8) & 255;
        int i7 = i2 & 255;
        return ((i4 + ((int) (((float) (((i3 >> 24) & 255) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & 255) - i5)) * f2))) << 16) | ((i6 + ((int) (((float) (((i3 >> 8) & 255) - i6)) * f2))) << 8) | (i7 + ((int) (f2 * ((float) ((i3 & 255) - i7)))));
    }

    @DexIgnore
    public final void a(float f2, float f3, float f4, float f5) {
        c cVar = this.a;
        float f6 = this.c.getDisplayMetrics().density;
        cVar.f(f3 * f6);
        cVar.b(f2 * f6);
        cVar.c(0);
        cVar.a(f4 * f6, f5 * f6);
    }

    @DexIgnore
    public void b(float f2) {
        this.a.d(f2);
        invalidateSelf();
    }

    @DexIgnore
    public final void c(float f2) {
        this.b = f2;
    }

    @DexIgnore
    public void d(float f2) {
        this.a.f(f2);
        invalidateSelf();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.b, bounds.exactCenterX(), bounds.exactCenterY());
        this.a.a(canvas, bounds);
        canvas.restore();
    }

    @DexIgnore
    public int getAlpha() {
        return this.a.a();
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public boolean isRunning() {
        return this.d.isRunning();
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.a.a(i2);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.a(colorFilter);
        invalidateSelf();
    }

    @DexIgnore
    public void start() {
        this.d.cancel();
        this.a.l();
        if (this.a.b() != this.a.e()) {
            this.f = true;
            this.d.setDuration(666);
            this.d.start();
            return;
        }
        this.a.c(0);
        this.a.k();
        this.d.setDuration(1332);
        this.d.start();
    }

    @DexIgnore
    public void stop() {
        this.d.cancel();
        c(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.a.a(false);
        this.a.c(0);
        this.a.k();
        invalidateSelf();
    }

    @DexIgnore
    public void b(float f2, c cVar) {
        if (f2 > 0.75f) {
            cVar.b(a((f2 - 0.75f) / 0.25f, cVar.f(), cVar.c()));
        } else {
            cVar.b(cVar.f());
        }
    }

    @DexIgnore
    public void a(int i2) {
        if (i2 == 0) {
            a(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            a(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    @DexIgnore
    public void a(boolean z) {
        this.a.a(z);
        invalidateSelf();
    }

    @DexIgnore
    public void a(float f2) {
        this.a.a(f2);
        invalidateSelf();
    }

    @DexIgnore
    public void a(float f2, float f3) {
        this.a.e(f2);
        this.a.c(f3);
        invalidateSelf();
    }

    @DexIgnore
    public void a(int... iArr) {
        this.a.a(iArr);
        this.a.c(0);
        invalidateSelf();
    }

    @DexIgnore
    public final void a(float f2, c cVar) {
        b(f2, cVar);
        cVar.e(cVar.i() + (((cVar.g() - 0.01f) - cVar.i()) * f2));
        cVar.c(cVar.g());
        cVar.d(cVar.h() + ((((float) (Math.floor((double) (cVar.h() / 0.8f)) + 1.0d)) - cVar.h()) * f2));
    }

    @DexIgnore
    public void a(float f2, c cVar, boolean z) {
        float f3;
        float f4;
        if (this.f) {
            a(f2, cVar);
        } else if (f2 != 1.0f || z) {
            float h2 = cVar.h();
            if (f2 < 0.5f) {
                float i2 = cVar.i();
                float f5 = i2;
                f3 = (h.getInterpolation(f2 / 0.5f) * 0.79f) + 0.01f + i2;
                f4 = f5;
            } else {
                f3 = cVar.i() + 0.79f;
                f4 = f3 - (((1.0f - h.getInterpolation((f2 - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            cVar.e(f4);
            cVar.c(f3);
            cVar.d(h2 + (0.20999998f * f2));
            c((f2 + this.e) * 216.0f);
        }
    }

    @DexIgnore
    public final void a() {
        c cVar = this.a;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
        ofFloat.addUpdateListener(new a(cVar));
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(g);
        ofFloat.addListener(new b(cVar));
        this.d = ofFloat;
    }
}
