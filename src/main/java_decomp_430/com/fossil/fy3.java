package com.fossil;

import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy3 {
    @DexIgnore
    public static /* final */ fy3 e; // = new fy3(gy3.b, 0, 0, 0);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ gy3 b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public fy3(gy3 gy3, int i, int i2, int i3) {
        this.b = gy3;
        this.a = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public int a() {
        return this.c;
    }

    @DexIgnore
    public int b() {
        return this.d;
    }

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return String.format("%s bits=%d bytes=%d", new Object[]{dy3.b[this.a], Integer.valueOf(this.d), Integer.valueOf(this.c)});
    }

    @DexIgnore
    public fy3 a(int i, int i2) {
        int i3 = this.d;
        gy3 gy3 = this.b;
        int i4 = this.a;
        if (i != i4) {
            int i5 = dy3.c[i4][i];
            int i6 = 65535 & i5;
            int i7 = i5 >> 16;
            gy3 = gy3.a(i6, i7);
            i3 += i7;
        }
        int i8 = i == 2 ? 4 : 5;
        return new fy3(gy3.a(i2, i8), i, 0, i3 + i8);
    }

    @DexIgnore
    public fy3 b(int i, int i2) {
        gy3 gy3 = this.b;
        int i3 = this.a == 2 ? 4 : 5;
        return new fy3(gy3.a(dy3.e[this.a][i], i3).a(i2, 5), this.a, 0, this.d + i3 + 5);
    }

    @DexIgnore
    public fy3 b(int i) {
        int i2 = this.c;
        if (i2 == 0) {
            return this;
        }
        return new fy3(this.b.b(i - i2, i2), this.a, 0, this.d);
    }

    @DexIgnore
    public fy3 a(int i) {
        gy3 gy3 = this.b;
        int i2 = this.a;
        int i3 = this.d;
        if (i2 == 4 || i2 == 2) {
            int i4 = dy3.c[i2][0];
            int i5 = 65535 & i4;
            int i6 = i4 >> 16;
            gy3 = gy3.a(i5, i6);
            i3 += i6;
            i2 = 0;
        }
        int i7 = this.c;
        fy3 fy3 = new fy3(gy3, i2, this.c + 1, i3 + ((i7 == 0 || i7 == 31) ? 18 : i7 == 62 ? 9 : 8));
        return fy3.c == 2078 ? fy3.b(i + 1) : fy3;
    }

    @DexIgnore
    public boolean a(fy3 fy3) {
        int i;
        int i2 = this.d + (dy3.c[this.a][fy3.a] >> 16);
        int i3 = fy3.c;
        if (i3 > 0 && ((i = this.c) == 0 || i > i3)) {
            i2 += 10;
        }
        return i2 <= fy3.d;
    }

    @DexIgnore
    public hy3 a(byte[] bArr) {
        LinkedList<gy3> linkedList = new LinkedList<>();
        for (gy3 gy3 = b(bArr.length).b; gy3 != null; gy3 = gy3.a()) {
            linkedList.addFirst(gy3);
        }
        hy3 hy3 = new hy3();
        for (gy3 a2 : linkedList) {
            a2.a(hy3, bArr);
        }
        return hy3;
    }
}
