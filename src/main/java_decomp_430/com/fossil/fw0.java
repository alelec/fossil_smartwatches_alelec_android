package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw0 implements Parcelable.Creator<ay0> {
    @DexIgnore
    public /* synthetic */ fw0(qg6 qg6) {
    }

    @DexIgnore
    public ay0 createFromParcel(Parcel parcel) {
        return new ay0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ay0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m15createFromParcel(Parcel parcel) {
        return new ay0(parcel, (qg6) null);
    }
}
