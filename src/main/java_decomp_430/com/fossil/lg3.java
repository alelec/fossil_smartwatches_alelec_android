package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ii3;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lg3 extends Drawable implements ii3.b {
    @DexIgnore
    public static /* final */ int u; // = wf3.Widget_MaterialComponents_Badge;
    @DexIgnore
    public static /* final */ int v; // = nf3.badgeStyle;
    @DexIgnore
    public /* final */ WeakReference<Context> a;
    @DexIgnore
    public /* final */ dj3 b; // = new dj3();
    @DexIgnore
    public /* final */ ii3 c;
    @DexIgnore
    public /* final */ Rect d; // = new Rect();
    @DexIgnore
    public /* final */ float e;
    @DexIgnore
    public /* final */ float f;
    @DexIgnore
    public /* final */ float g;
    @DexIgnore
    public /* final */ a h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public int o;
    @DexIgnore
    public float p;
    @DexIgnore
    public float q;
    @DexIgnore
    public float r;
    @DexIgnore
    public WeakReference<View> s;
    @DexIgnore
    public WeakReference<ViewGroup> t;

    @DexIgnore
    public lg3(Context context) {
        this.a = new WeakReference<>(context);
        ki3.b(context);
        Resources resources = context.getResources();
        this.e = (float) resources.getDimensionPixelSize(pf3.mtrl_badge_radius);
        this.g = (float) resources.getDimensionPixelSize(pf3.mtrl_badge_long_text_horizontal_padding);
        this.f = (float) resources.getDimensionPixelSize(pf3.mtrl_badge_with_text_radius);
        this.c = new ii3(this);
        this.c.b().setTextAlign(Paint.Align.CENTER);
        this.h = new a(context);
        f(wf3.TextAppearance_MaterialComponents_Badge);
    }

    @DexIgnore
    public static lg3 a(Context context, a aVar) {
        lg3 lg3 = new lg3(context);
        lg3.a(aVar);
        return lg3;
    }

    @DexIgnore
    public static lg3 b(Context context, AttributeSet attributeSet, int i2, int i3) {
        lg3 lg3 = new lg3(context);
        lg3.a(context, attributeSet, i2, i3);
        return lg3;
    }

    @DexIgnore
    public void c(int i2) {
        int unused = this.h.b = i2;
        if (this.c.b().getColor() != i2) {
            this.c.b().setColor(i2);
            invalidateSelf();
        }
    }

    @DexIgnore
    public int d() {
        return this.h.e;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (!getBounds().isEmpty() && getAlpha() != 0 && isVisible()) {
            this.b.draw(canvas);
            if (g()) {
                a(canvas);
            }
        }
    }

    @DexIgnore
    public int e() {
        if (!g()) {
            return 0;
        }
        return this.h.d;
    }

    @DexIgnore
    public a f() {
        return this.h;
    }

    @DexIgnore
    public boolean g() {
        return this.h.d != -1;
    }

    @DexIgnore
    public int getAlpha() {
        return this.h.c;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.d.height();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.d.width();
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public final void h() {
        Context context = (Context) this.a.get();
        WeakReference<View> weakReference = this.s;
        ViewGroup viewGroup = null;
        View view = weakReference != null ? (View) weakReference.get() : null;
        if (context != null && view != null) {
            Rect rect = new Rect();
            rect.set(this.d);
            Rect rect2 = new Rect();
            view.getDrawingRect(rect2);
            WeakReference<ViewGroup> weakReference2 = this.t;
            if (weakReference2 != null) {
                viewGroup = (ViewGroup) weakReference2.get();
            }
            if (viewGroup != null || mg3.a) {
                if (viewGroup == null) {
                    viewGroup = (ViewGroup) view.getParent();
                }
                viewGroup.offsetDescendantRectToMyCoords(view, rect2);
            }
            a(context, rect2, view);
            mg3.a(this.d, this.i, this.j, this.q, this.r);
            this.b.a(this.p);
            if (!rect.equals(this.d)) {
                this.b.setBounds(this.d);
            }
        }
    }

    @DexIgnore
    public final void i() {
        this.o = ((int) Math.pow(10.0d, ((double) d()) - 1.0d)) - 1;
    }

    @DexIgnore
    public boolean isStateful() {
        return false;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        return super.onStateChange(iArr);
    }

    @DexIgnore
    public void setAlpha(int i2) {
        int unused = this.h.c = i2;
        this.c.b().setAlpha(i2);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
    }

    @DexIgnore
    public void d(int i2) {
        if (this.h.e != i2) {
            int unused = this.h.e = i2;
            i();
            this.c.a(true);
            h();
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void f(int i2) {
        Context context = (Context) this.a.get();
        if (context != null) {
            a(new qi3(context, i2));
        }
    }

    @DexIgnore
    public static lg3 a(Context context) {
        return b(context, (AttributeSet) null, v, u);
    }

    @DexIgnore
    public void b(int i2) {
        if (this.h.h != i2) {
            int unused = this.h.h = i2;
            WeakReference<View> weakReference = this.s;
            if (weakReference != null && weakReference.get() != null) {
                View view = (View) this.s.get();
                WeakReference<ViewGroup> weakReference2 = this.t;
                a(view, weakReference2 != null ? (ViewGroup) weakReference2.get() : null);
            }
        }
    }

    @DexIgnore
    public void e(int i2) {
        int max = Math.max(0, i2);
        if (this.h.d != max) {
            int unused = this.h.d = max;
            this.c.a(true);
            h();
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void a(a aVar) {
        d(aVar.e);
        if (aVar.d != -1) {
            e(aVar.d);
        }
        a(aVar.a);
        c(aVar.b);
        b(aVar.h);
    }

    @DexIgnore
    public CharSequence c() {
        Context context;
        if (!isVisible()) {
            return null;
        }
        if (!g()) {
            return this.h.f;
        }
        if (this.h.g <= 0 || (context = (Context) this.a.get()) == null) {
            return null;
        }
        return context.getResources().getQuantityString(this.h.g, e(), new Object[]{Integer.valueOf(e())});
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0027a();
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c; // = 255;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public int e;
        @DexIgnore
        public CharSequence f;
        @DexIgnore
        public int g;
        @DexIgnore
        public int h;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lg3$a$a")
        /* renamed from: com.fossil.lg3$a$a  reason: collision with other inner class name */
        public static class C0027a implements Parcelable.Creator<a> {
            @DexIgnore
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a(Context context) {
            this.b = new qi3(context, wf3.TextAppearance_MaterialComponents_Badge).b.getDefaultColor();
            this.f = context.getString(vf3.mtrl_badge_numberless_content_description);
            this.g = uf3.mtrl_badge_content_description;
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d);
            parcel.writeInt(this.e);
            parcel.writeString(this.f.toString());
            parcel.writeInt(this.g);
            parcel.writeInt(this.h);
        }

        @DexIgnore
        public a(Parcel parcel) {
            this.a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            this.d = parcel.readInt();
            this.e = parcel.readInt();
            this.f = parcel.readString();
            this.g = parcel.readInt();
            this.h = parcel.readInt();
        }
    }

    @DexIgnore
    public final String b() {
        if (e() <= this.o) {
            return Integer.toString(e());
        }
        Context context = (Context) this.a.get();
        if (context == null) {
            return "";
        }
        return context.getString(vf3.mtrl_exceed_max_badge_number_suffix, new Object[]{Integer.valueOf(this.o), "+"});
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet, int i2, int i3) {
        TypedArray c2 = ki3.c(context, attributeSet, xf3.Badge, i2, i3, new int[0]);
        d(c2.getInt(xf3.Badge_maxCharacterCount, 4));
        if (c2.hasValue(xf3.Badge_number)) {
            e(c2.getInt(xf3.Badge_number, 0));
        }
        a(a(context, c2, xf3.Badge_backgroundColor));
        if (c2.hasValue(xf3.Badge_badgeTextColor)) {
            c(a(context, c2, xf3.Badge_badgeTextColor));
        }
        b(c2.getInt(xf3.Badge_badgeGravity, 8388661));
        c2.recycle();
    }

    @DexIgnore
    public static int a(Context context, TypedArray typedArray, int i2) {
        return pi3.a(context, typedArray, i2).getDefaultColor();
    }

    @DexIgnore
    public void a(View view, ViewGroup viewGroup) {
        this.s = new WeakReference<>(view);
        this.t = new WeakReference<>(viewGroup);
        h();
        invalidateSelf();
    }

    @DexIgnore
    public void a(int i2) {
        int unused = this.h.a = i2;
        ColorStateList valueOf = ColorStateList.valueOf(i2);
        if (this.b.h() != valueOf) {
            this.b.a(valueOf);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void a() {
        invalidateSelf();
    }

    @DexIgnore
    public final void a(qi3 qi3) {
        Context context;
        if (this.c.a() != qi3 && (context = (Context) this.a.get()) != null) {
            this.c.a(qi3, context);
            h();
        }
    }

    @DexIgnore
    public final void a(Context context, Rect rect, View view) {
        int e2 = this.h.h;
        if (e2 == 8388691 || e2 == 8388693) {
            this.j = (float) rect.bottom;
        } else {
            this.j = (float) rect.top;
        }
        if (e() <= 9) {
            this.p = !g() ? this.e : this.f;
            float f2 = this.p;
            this.r = f2;
            this.q = f2;
        } else {
            this.p = this.f;
            this.r = this.p;
            this.q = (this.c.a(b()) / 2.0f) + this.g;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(g() ? pf3.mtrl_badge_text_horizontal_edge_offset : pf3.mtrl_badge_horizontal_edge_offset);
        int e3 = this.h.h;
        if (e3 == 8388659 || e3 == 8388691) {
            this.i = x9.o(view) == 0 ? (((float) rect.left) - this.q) + ((float) dimensionPixelSize) : (((float) rect.right) + this.q) - ((float) dimensionPixelSize);
        } else {
            this.i = x9.o(view) == 0 ? (((float) rect.right) + this.q) - ((float) dimensionPixelSize) : (((float) rect.left) - this.q) + ((float) dimensionPixelSize);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        Rect rect = new Rect();
        String b2 = b();
        this.c.b().getTextBounds(b2, 0, b2.length(), rect);
        canvas.drawText(b2, this.i, this.j + ((float) (rect.height() / 2)), this.c.b());
    }
}
