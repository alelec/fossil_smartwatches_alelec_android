package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1", f = "SetVibrationStrengthUseCase.kt", l = {49}, m = "invokeSuspend")
public final class wr4$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetVibrationStrengthUseCase.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wr4$e$a(SetVibrationStrengthUseCase.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wr4$e$a wr4_e_a = new wr4$e$a(this.this$0, xe6);
        wr4_e_a.p$ = (il6) obj;
        return wr4_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wr4$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase] */
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            Device deviceBySerial = SetVibrationStrengthUseCase.this.h.getDeviceBySerial(SetVibrationStrengthUseCase.this.e());
            if (deviceBySerial != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = SetVibrationStrengthUseCase.j.a();
                local.d(a2, "Update vibration stregnth " + SetVibrationStrengthUseCase.this.d() + " to db");
                deviceBySerial.setVibrationStrength(hf6.a(SetVibrationStrengthUseCase.this.d()));
                DeviceRepository a3 = SetVibrationStrengthUseCase.this.h;
                this.L$0 = il6;
                this.L$1 = deviceBySerial;
                this.L$2 = deviceBySerial;
                this.label = 1;
                obj = a3.updateDevice(deviceBySerial, false, this);
                if (obj == a) {
                    return a;
                }
            }
            FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
            SetVibrationStrengthUseCase.this.a(new SetVibrationStrengthUseCase.d());
            return cd6.a;
        } else if (i == 1) {
            Device device = (Device) this.L$2;
            Device device2 = (Device) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ap4 ap4 = (ap4) obj;
        FLogger.INSTANCE.getLocal().d(SetVibrationStrengthUseCase.j.a(), "onReceive #getDeviceBySerial success");
        SetVibrationStrengthUseCase.this.a(new SetVibrationStrengthUseCase.d());
        return cd6.a;
    }
}
