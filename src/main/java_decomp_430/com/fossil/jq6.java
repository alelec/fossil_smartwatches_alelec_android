package com.fossil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq6 {
    @DexIgnore
    public static /* final */ gq6[] e; // = {gq6.q, gq6.r, gq6.s, gq6.t, gq6.u, gq6.k, gq6.m, gq6.l, gq6.n, gq6.p, gq6.o};
    @DexIgnore
    public static /* final */ gq6[] f; // = {gq6.q, gq6.r, gq6.s, gq6.t, gq6.u, gq6.k, gq6.m, gq6.l, gq6.n, gq6.p, gq6.o, gq6.i, gq6.j, gq6.g, gq6.h, gq6.e, gq6.f, gq6.d};
    @DexIgnore
    public static /* final */ jq6 g;
    @DexIgnore
    public static /* final */ jq6 h; // = new a(false).a();
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ String[] c;
    @DexIgnore
    public /* final */ String[] d;

    /*
    static {
        a aVar = new a(true);
        aVar.a(e);
        aVar.a(br6.TLS_1_3, br6.TLS_1_2);
        aVar.a(true);
        aVar.a();
        a aVar2 = new a(true);
        aVar2.a(f);
        aVar2.a(br6.TLS_1_3, br6.TLS_1_2, br6.TLS_1_1, br6.TLS_1_0);
        aVar2.a(true);
        g = aVar2.a();
        a aVar3 = new a(true);
        aVar3.a(f);
        aVar3.a(br6.TLS_1_0);
        aVar3.a(true);
        aVar3.a();
    }
    */

    @DexIgnore
    public jq6(a aVar) {
        this.a = aVar.a;
        this.c = aVar.b;
        this.d = aVar.c;
        this.b = aVar.d;
    }

    @DexIgnore
    public List<gq6> a() {
        String[] strArr = this.c;
        if (strArr != null) {
            return gq6.a(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }

    @DexIgnore
    public boolean c() {
        return this.b;
    }

    @DexIgnore
    public List<br6> d() {
        String[] strArr = this.d;
        if (strArr != null) {
            return br6.forJavaNames(strArr);
        }
        return null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof jq6)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        jq6 jq6 = (jq6) obj;
        boolean z = this.a;
        if (z != jq6.a) {
            return false;
        }
        return !z || (Arrays.equals(this.c, jq6.c) && Arrays.equals(this.d, jq6.d) && this.b == jq6.b);
    }

    @DexIgnore
    public int hashCode() {
        if (this.a) {
            return ((((527 + Arrays.hashCode(this.c)) * 31) + Arrays.hashCode(this.d)) * 31) + (this.b ^ true ? 1 : 0);
        }
        return 17;
    }

    @DexIgnore
    public String toString() {
        if (!this.a) {
            return "ConnectionSpec()";
        }
        String str = "[all enabled]";
        String obj = this.c != null ? a().toString() : str;
        if (this.d != null) {
            str = d().toString();
        }
        return "ConnectionSpec(cipherSuites=" + obj + ", tlsVersions=" + str + ", supportsTlsExtensions=" + this.b + ")";
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public String[] b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public a a(gq6... gq6Arr) {
            if (this.a) {
                String[] strArr = new String[gq6Arr.length];
                for (int i = 0; i < gq6Arr.length; i++) {
                    strArr[i] = gq6Arr[i].a;
                }
                a(strArr);
                return this;
            }
            throw new IllegalStateException("no cipher suites for cleartext connections");
        }

        @DexIgnore
        public a b(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no TLS versions for cleartext connections");
            } else if (strArr.length != 0) {
                this.c = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one TLS version is required");
            }
        }

        @DexIgnore
        public a(jq6 jq6) {
            this.a = jq6.a;
            this.b = jq6.c;
            this.c = jq6.d;
            this.d = jq6.b;
        }

        @DexIgnore
        public a a(String... strArr) {
            if (!this.a) {
                throw new IllegalStateException("no cipher suites for cleartext connections");
            } else if (strArr.length != 0) {
                this.b = (String[]) strArr.clone();
                return this;
            } else {
                throw new IllegalArgumentException("At least one cipher suite is required");
            }
        }

        @DexIgnore
        public a a(br6... br6Arr) {
            if (this.a) {
                String[] strArr = new String[br6Arr.length];
                for (int i = 0; i < br6Arr.length; i++) {
                    strArr[i] = br6Arr[i].javaName;
                }
                b(strArr);
                return this;
            }
            throw new IllegalStateException("no TLS versions for cleartext connections");
        }

        @DexIgnore
        public a a(boolean z) {
            if (this.a) {
                this.d = z;
                return this;
            }
            throw new IllegalStateException("no TLS extensions for cleartext connections");
        }

        @DexIgnore
        public jq6 a() {
            return new jq6(this);
        }
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, boolean z) {
        jq6 b2 = b(sSLSocket, z);
        String[] strArr = b2.d;
        if (strArr != null) {
            sSLSocket.setEnabledProtocols(strArr);
        }
        String[] strArr2 = b2.c;
        if (strArr2 != null) {
            sSLSocket.setEnabledCipherSuites(strArr2);
        }
    }

    @DexIgnore
    public final jq6 b(SSLSocket sSLSocket, boolean z) {
        String[] strArr;
        String[] strArr2;
        if (this.c != null) {
            strArr = fr6.a((Comparator<? super String>) gq6.b, sSLSocket.getEnabledCipherSuites(), this.c);
        } else {
            strArr = sSLSocket.getEnabledCipherSuites();
        }
        if (this.d != null) {
            strArr2 = fr6.a((Comparator<? super String>) fr6.p, sSLSocket.getEnabledProtocols(), this.d);
        } else {
            strArr2 = sSLSocket.getEnabledProtocols();
        }
        String[] supportedCipherSuites = sSLSocket.getSupportedCipherSuites();
        int a2 = fr6.a(gq6.b, supportedCipherSuites, "TLS_FALLBACK_SCSV");
        if (z && a2 != -1) {
            strArr = fr6.a(strArr, supportedCipherSuites[a2]);
        }
        a aVar = new a(this);
        aVar.a(strArr);
        aVar.b(strArr2);
        return aVar.a();
    }

    @DexIgnore
    public boolean a(SSLSocket sSLSocket) {
        if (!this.a) {
            return false;
        }
        String[] strArr = this.d;
        if (strArr != null && !fr6.b(fr6.p, strArr, sSLSocket.getEnabledProtocols())) {
            return false;
        }
        String[] strArr2 = this.c;
        if (strArr2 == null || fr6.b(gq6.b, strArr2, sSLSocket.getEnabledCipherSuites())) {
            return true;
        }
        return false;
    }
}
