package com.fossil;

import android.text.SpannableString;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ox4 extends xt4<nx4> {
    @DexIgnore
    void a();

    @DexIgnore
    void a(SpannableString spannableString);

    @DexIgnore
    void a(String str, ArrayList<Alarm> arrayList, Alarm alarm);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b();

    @DexIgnore
    void b(SpannableString spannableString);

    @DexIgnore
    void c();

    @DexIgnore
    void c(List<Alarm> list);

    @DexIgnore
    void h(String str);

    @DexIgnore
    void m(boolean z);

    @DexIgnore
    void r();

    @DexIgnore
    void s();

    @DexIgnore
    void w();
}
