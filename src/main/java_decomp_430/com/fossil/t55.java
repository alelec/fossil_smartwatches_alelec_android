package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t55 implements Factory<r55> {
    @DexIgnore
    public static r55 a(s55 s55) {
        r55 a = s55.a();
        z76.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
