package com.fossil;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp2 {
    @DexIgnore
    public static /* final */ Class<?> a; // = d();
    @DexIgnore
    public static /* final */ wp2<?, ?> b; // = a(false);
    @DexIgnore
    public static /* final */ wp2<?, ?> c; // = a(true);
    @DexIgnore
    public static /* final */ wp2<?, ?> d; // = new yp2();

    @DexIgnore
    public static void a(Class<?> cls) {
        Class<?> cls2;
        if (!fn2.class.isAssignableFrom(cls) && (cls2 = a) != null && !cls2.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
        }
    }

    @DexIgnore
    public static void b(int i, List<Float> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzf(i, list, z);
        }
    }

    @DexIgnore
    public static void c(int i, List<Long> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzc(i, list, z);
        }
    }

    @DexIgnore
    public static void d(int i, List<Long> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzd(i, list, z);
        }
    }

    @DexIgnore
    public static void e(int i, List<Long> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzn(i, list, z);
        }
    }

    @DexIgnore
    public static void f(int i, List<Long> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zze(i, list, z);
        }
    }

    @DexIgnore
    public static void g(int i, List<Long> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzl(i, list, z);
        }
    }

    @DexIgnore
    public static void h(int i, List<Integer> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zza(i, list, z);
        }
    }

    @DexIgnore
    public static void i(int i, List<Integer> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzj(i, list, z);
        }
    }

    @DexIgnore
    public static void j(int i, List<Integer> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzm(i, list, z);
        }
    }

    @DexIgnore
    public static void k(int i, List<Integer> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzb(i, list, z);
        }
    }

    @DexIgnore
    public static void l(int i, List<Integer> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzk(i, list, z);
        }
    }

    @DexIgnore
    public static void m(int i, List<Integer> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzh(i, list, z);
        }
    }

    @DexIgnore
    public static void n(int i, List<Boolean> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzi(i, list, z);
        }
    }

    @DexIgnore
    public static void b(int i, List<yl2> list, tq2 tq2) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzb(i, list);
        }
    }

    @DexIgnore
    public static int c(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fo2) {
            fo2 fo2 = (fo2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.f(fo2.zzb(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof in2) {
            in2 in2 = (in2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.k(in2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.k(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof in2) {
            in2 in2 = (in2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.f(in2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.f(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int f(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof in2) {
            in2 in2 = (in2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.g(in2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.g(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int g(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof in2) {
            in2 in2 = (in2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.h(in2.zzc(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.h(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int h(List<?> list) {
        return list.size() << 2;
    }

    @DexIgnore
    public static int i(List<?> list) {
        return list.size() << 3;
    }

    @DexIgnore
    public static int j(List<?> list) {
        return list.size();
    }

    @DexIgnore
    public static void a(int i, List<Double> list, tq2 tq2, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zzg(i, list, z);
        }
    }

    @DexIgnore
    public static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * pm2.i(i, 0);
    }

    @DexIgnore
    public static int i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * pm2.g(i, 0);
    }

    @DexIgnore
    public static int j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * pm2.b(i, true);
    }

    @DexIgnore
    public static void b(int i, List<?> list, tq2 tq2, fp2 fp2) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.a(i, list, fp2);
        }
    }

    @DexIgnore
    public static void a(int i, List<String> list, tq2 tq2) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.zza(i, list);
        }
    }

    @DexIgnore
    public static int b(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fo2) {
            fo2 fo2 = (fo2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.e(fo2.zzb(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static void a(int i, List<?> list, tq2 tq2, fp2 fp2) throws IOException {
        if (list != null && !list.isEmpty()) {
            tq2.b(i, list, fp2);
        }
    }

    @DexIgnore
    public static int c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return c(list) + (size * pm2.e(i));
    }

    @DexIgnore
    public static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return d(list) + (size * pm2.e(i));
    }

    @DexIgnore
    public static int e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return e(list) + (size * pm2.e(i));
    }

    @DexIgnore
    public static int f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return f(list) + (size * pm2.e(i));
    }

    @DexIgnore
    public static int g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return g(list) + (size * pm2.e(i));
    }

    @DexIgnore
    public static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof fo2) {
            fo2 fo2 = (fo2) list;
            i = 0;
            while (i2 < size) {
                i += pm2.d(fo2.zzb(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + pm2.d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static wp2<?, ?> c() {
        return d;
    }

    @DexIgnore
    public static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static int b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return b(list) + (size * pm2.e(i));
    }

    @DexIgnore
    public static int a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return a(list) + (list.size() * pm2.e(i));
    }

    @DexIgnore
    public static int b(int i, List<yl2> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * pm2.e(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            e += pm2.a(list.get(i2));
        }
        return e;
    }

    @DexIgnore
    public static int a(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int e = pm2.e(i) * size;
        if (list instanceof xn2) {
            xn2 xn2 = (xn2) list;
            while (i4 < size) {
                Object zzb = xn2.zzb(i4);
                if (zzb instanceof yl2) {
                    i3 = pm2.a((yl2) zzb);
                } else {
                    i3 = pm2.a((String) zzb);
                }
                e += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof yl2) {
                    i2 = pm2.a((yl2) obj);
                } else {
                    i2 = pm2.a((String) obj);
                }
                e += i2;
                i4++;
            }
        }
        return e;
    }

    @DexIgnore
    public static int b(int i, List<ro2> list, fp2 fp2) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += pm2.c(i, list.get(i3), fp2);
        }
        return i2;
    }

    @DexIgnore
    public static wp2<?, ?> b() {
        return c;
    }

    @DexIgnore
    public static int a(int i, Object obj, fp2 fp2) {
        if (obj instanceof vn2) {
            return pm2.a(i, (vn2) obj);
        }
        return pm2.b(i, (ro2) obj, fp2);
    }

    @DexIgnore
    public static int a(int i, List<?> list, fp2 fp2) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = pm2.e(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof vn2) {
                i2 = pm2.a((vn2) obj);
            } else {
                i2 = pm2.a((ro2) obj, fp2);
            }
            e += i2;
        }
        return e;
    }

    @DexIgnore
    public static wp2<?, ?> a() {
        return b;
    }

    @DexIgnore
    public static wp2<?, ?> a(boolean z) {
        try {
            Class<?> e = e();
            if (e == null) {
                return null;
            }
            return (wp2) e.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @DexIgnore
    public static <T> void a(ko2 ko2, T t, T t2, long j) {
        cq2.a((Object) t, j, ko2.zza(cq2.f(t, j), cq2.f(t2, j)));
    }

    @DexIgnore
    public static <T, FT extends zm2<FT>> void a(um2<FT> um2, T t, T t2) {
        xm2<FT> a2 = um2.a((Object) t2);
        if (!a2.a.isEmpty()) {
            um2.b(t).a(a2);
        }
    }

    @DexIgnore
    public static <T, UT, UB> void a(wp2<UT, UB> wp2, T t, T t2) {
        wp2.a((Object) t, wp2.c(wp2.a(t), wp2.a(t2)));
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, List<Integer> list, mn2 mn2, UB ub, wp2<UT, UB> wp2) {
        UB ub2;
        if (mn2 == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    if (!mn2.zza(intValue)) {
                        ub = a(i, intValue, ub2, wp2);
                        it.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (mn2.zza(intValue2)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = a(i, intValue2, ub2, wp2);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, int i2, UB ub, wp2<UT, UB> wp2) {
        if (ub == null) {
            ub = wp2.a();
        }
        wp2.a(ub, i, (long) i2);
        return ub;
    }
}
