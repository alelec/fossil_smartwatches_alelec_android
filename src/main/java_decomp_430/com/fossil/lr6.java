package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lr6 extends nt6 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public lr6(yt6 yt6) {
        super(yt6);
    }

    @DexIgnore
    public void a(jt6 jt6, long j) throws IOException {
        if (this.b) {
            jt6.skip(j);
            return;
        }
        try {
            super.a(jt6, j);
        } catch (IOException e) {
            this.b = true;
            a(e);
        }
    }

    @DexIgnore
    public void a(IOException iOException) {
        throw null;
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.b) {
            try {
                super.close();
            } catch (IOException e) {
                this.b = true;
                a(e);
            }
        }
    }

    @DexIgnore
    public void flush() throws IOException {
        if (!this.b) {
            try {
                super.flush();
            } catch (IOException e) {
                this.b = true;
                a(e);
            }
        }
    }
}
