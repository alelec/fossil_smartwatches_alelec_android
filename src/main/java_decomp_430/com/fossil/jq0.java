package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq0 extends x51 {
    @DexIgnore
    public static /* final */ ro0 V; // = new ro0((qg6) null);
    @DexIgnore
    public y11 T;
    @DexIgnore
    public /* final */ boolean U;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ jq0(ue1 ue1, q41 q41, HashMap hashMap, int i) {
        super(ue1, q41, r3, r4, r5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 96);
        HashMap hashMap2 = (i & 4) != 0 ? new HashMap() : hashMap;
        eh1 eh1 = eh1.SYNC_IN_BACKGROUND;
        short a = lk1.b.a(ue1.t, w31.ACTIVITY_FILE);
        V.a(hashMap2);
        this.T = y11.LOW;
        this.U = true;
    }

    @DexIgnore
    public boolean b() {
        return this.U;
    }

    @DexIgnore
    public y11 e() {
        return this.T;
    }
}
