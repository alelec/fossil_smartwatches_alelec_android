package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.fossil.l0;
import pub.devrel.easypermissions.AppSettingsDialogHolderActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ow6 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ow6> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public Object h;
    @DexIgnore
    public Context i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ow6> {
        @DexIgnore
        public ow6 createFromParcel(Parcel parcel) {
            return new ow6(parcel, (a) null);
        }

        @DexIgnore
        public ow6[] newArray(int i) {
            return new ow6[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public int h; // = -1;
        @DexIgnore
        public boolean i; // = false;

        @DexIgnore
        public b(Fragment fragment) {
            this.a = fragment;
            this.b = fragment.getContext();
        }

        @DexIgnore
        public b a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b b(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        public ow6 a() {
            this.d = TextUtils.isEmpty(this.d) ? this.b.getString(rw6.rationale_ask_again) : this.d;
            this.e = TextUtils.isEmpty(this.e) ? this.b.getString(rw6.title_settings_dialog) : this.e;
            this.f = TextUtils.isEmpty(this.f) ? this.b.getString(17039370) : this.f;
            this.g = TextUtils.isEmpty(this.g) ? this.b.getString(17039360) : this.g;
            int i2 = this.h;
            if (i2 <= 0) {
                i2 = 16061;
            }
            this.h = i2;
            return new ow6(this.a, this.c, this.d, this.e, this.f, this.g, this.h, this.i ? 268435456 : 0, (a) null);
        }
    }

    @DexIgnore
    public /* synthetic */ ow6(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public static ow6 a(Intent intent, Activity activity) {
        ow6 ow6 = (ow6) intent.getParcelableExtra("extra_app_settings");
        ow6.a((Object) activity);
        return ow6;
    }

    @DexIgnore
    public void b() {
        a(AppSettingsDialogHolderActivity.a(this.i, this));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
    }

    @DexIgnore
    public /* synthetic */ ow6(Object obj, int i2, String str, String str2, String str3, String str4, int i3, int i4, a aVar) {
        this(obj, i2, str, str2, str3, str4, i3, i4);
    }

    @DexIgnore
    public ow6(Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
    }

    @DexIgnore
    public final void a(Object obj) {
        this.h = obj;
        if (obj instanceof Activity) {
            this.i = (Activity) obj;
        } else if (obj instanceof Fragment) {
            this.i = ((Fragment) obj).getContext();
        } else {
            throw new IllegalStateException("Unknown object: " + obj);
        }
    }

    @DexIgnore
    public final void a(Intent intent) {
        Object obj = this.h;
        if (obj instanceof Activity) {
            ((Activity) obj).startActivityForResult(intent, this.f);
        } else if (obj instanceof Fragment) {
            ((Fragment) obj).startActivityForResult(intent, this.f);
        }
    }

    @DexIgnore
    public ow6(Object obj, int i2, String str, String str2, String str3, String str4, int i3, int i4) {
        a(obj);
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = i3;
        this.g = i4;
    }

    @DexIgnore
    public l0 a(DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        l0.a aVar;
        int i2 = this.a;
        if (i2 > 0) {
            aVar = new l0.a(this.i, i2);
        } else {
            aVar = new l0.a(this.i);
        }
        aVar.a(false);
        aVar.b(this.c);
        aVar.a(this.b);
        aVar.b(this.d, onClickListener);
        aVar.a(this.e, onClickListener2);
        return aVar.c();
    }

    @DexIgnore
    public int a() {
        return this.g;
    }
}
