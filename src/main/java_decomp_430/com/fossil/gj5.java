package com.fossil;

import android.content.Context;
import android.graphics.RectF;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gj5 {
    @DexIgnore
    public static /* final */ gj5 a; // = new gj5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((ActivitySample) t).getStartTime().getMillis()), Long.valueOf(((ActivitySample) t2).getStartTime().getMillis()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((GoalTrackingData) t).getTrackedAt().getMillis()), Long.valueOf(((GoalTrackingData) t2).getTrackedAt().getMillis()));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r7v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(long j, int i, boolean z) {
        boolean z2;
        int i2;
        String str;
        int hourOfDay = new DateTime(j, DateTimeZone.forOffsetMillis(i * 1000)).getHourOfDay();
        if (hourOfDay < 12) {
            if (hourOfDay == 0) {
                hourOfDay = 12;
            }
            i2 = hourOfDay;
            z2 = true;
        } else {
            if (hourOfDay != 12) {
                hourOfDay -= 12;
            }
            i2 = hourOfDay;
            z2 = false;
        }
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), z2 ? 2131887299 : 2131887301);
        if (i >= 0) {
            str = '+' + tk4.a(((float) i) / 3600.0f, 1);
        } else {
            str = tk4.a(((float) i) / 3600.0f, 1);
        }
        if (z) {
            nh6 nh6 = nh6.a;
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887302);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            nh6 nh62 = nh6.a;
            wg6.a((Object) a2, "amPmRes");
            Object[] objArr = {Integer.valueOf(i2)};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            Object[] objArr2 = {format, str};
            String format2 = String.format(a3, Arrays.copyOf(objArr2, objArr2.length));
            wg6.a((Object) format2, "java.lang.String.format(format, *args)");
            return format2;
        } else if (i2 % 6 != 0) {
            return "";
        } else {
            nh6 nh63 = nh6.a;
            wg6.a((Object) a2, "amPmRes");
            Object[] objArr3 = {Integer.valueOf(i2)};
            String format3 = String.format(a2, Arrays.copyOf(objArr3, objArr3.length));
            wg6.a((Object) format3, "java.lang.String.format(format, *args)");
            FLogger.INSTANCE.getLocal().d("SupportedFunction", "temp=" + format3);
            int hashCode = format3.hashCode();
            if (hashCode != 1771) {
                if (hashCode != 1786) {
                    if (hashCode != 48736) {
                        if (hashCode != 48751 || !format3.equals("12p")) {
                            return format3;
                        }
                        String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886456);
                        wg6.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Label__12p)");
                        return a4;
                    } else if (!format3.equals("12a")) {
                        return format3;
                    } else {
                        String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886454);
                        wg6.a((Object) a5, "LanguageHelper.getString\u2026in_StepsToday_Label__12a)");
                        return a5;
                    }
                } else if (!format3.equals("6p")) {
                    return format3;
                } else {
                    String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886458);
                    wg6.a((Object) a6, "LanguageHelper.getString\u2026ain_StepsToday_Label__6p)");
                    return a6;
                }
            } else if (!format3.equals("6a")) {
                return format3;
            } else {
                String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886457);
                wg6.a((Object) a7, "LanguageHelper.getString\u2026ain_StepsToday_Label__6a)");
                return a7;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x03e7  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x03e8  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x04ab  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x013e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x0250 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x02a3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x0377 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0448 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x024d  */
    public final synchronized lc6<ArrayList<BarChart.a>, ArrayList<String>> a(Date date, List<ActivitySample> list, int i) {
        lc6<ArrayList<BarChart.a>, ArrayList<String>> lc6;
        long j;
        long j2;
        Boolean bool;
        boolean z;
        Object obj;
        boolean z2;
        boolean z3;
        double d;
        Object obj2;
        boolean z4;
        boolean z5;
        long j3;
        boolean z6;
        boolean z7;
        ArrayList arrayList;
        double d2;
        boolean z8;
        Date date2 = date;
        int i2 = i;
        synchronized (this) {
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            List<T> d3 = list != null ? yd6.d(list) : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("transferActivitySamplesToDetailChart - date=");
            sb.append(date2);
            sb.append(", copyOfActivitySample=");
            sb.append(d3 != null ? Integer.valueOf(d3.size()) : null);
            local.d("SupportedFunction", sb.toString());
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            HashSet hashSet = new HashSet();
            int i3 = Calendar.getInstance().get(11);
            Boolean t = bk4.t(date);
            if (d3 == null || !(!d3.isEmpty())) {
                arrayList2.add(new BarChart.a(0, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), 0, false, 13, (qg6) null));
            } else {
                if (d3.size() > 1) {
                    ud6.a(d3, new a());
                }
                for (T timeZoneOffsetInSecond : d3) {
                    hashSet.add(Integer.valueOf(timeZoneOffsetInSecond.getTimeZoneOffsetInSecond()));
                }
                int timeZoneOffsetInSecond2 = ((ActivitySample) yd6.d(d3)).getTimeZoneOffsetInSecond();
                int timeZoneOffsetInSecond3 = ((ActivitySample) yd6.f(d3)).getTimeZoneOffsetInSecond();
                Date d4 = bk4.d(date2, bk4.a(timeZoneOffsetInSecond2));
                Date c = bk4.c(date2, bk4.a(timeZoneOffsetInSecond3));
                wg6.a((Object) d4, "startDate");
                long time = d4.getTime();
                wg6.a((Object) c, "endDate");
                long time2 = c.getTime();
                ArrayList arrayList4 = new ArrayList();
                arrayList4.addAll(d3);
                TimeZone timeZone = TimeZone.getDefault();
                wg6.a((Object) timeZone, "TimeZone.getDefault()");
                int a2 = bk4.a(timeZone.getID(), true);
                if (hashSet.size() <= 1) {
                    arrayList3.add(a(time, timeZoneOffsetInSecond2, timeZoneOffsetInSecond2 != a2));
                    int i4 = 0;
                    while (time <= time2) {
                        long j4 = time + 3600000;
                        arrayList3.add(a(j4, timeZoneOffsetInSecond2, false));
                        ArrayList<ActivitySample> arrayList5 = new ArrayList<>();
                        for (Object next : arrayList4) {
                            int i5 = timeZoneOffsetInSecond2;
                            long millis = ((ActivitySample) next).getStartTime().getMillis();
                            if (time <= millis) {
                                if (j4 > millis) {
                                    z8 = true;
                                    if (!z8) {
                                        arrayList5.add(next);
                                    }
                                    timeZoneOffsetInSecond2 = i5;
                                }
                            }
                            z8 = false;
                            if (!z8) {
                            }
                            timeZoneOffsetInSecond2 = i5;
                        }
                        int i6 = timeZoneOffsetInSecond2;
                        arrayList4.removeAll(arrayList5);
                        if (i2 != 0) {
                            if (i2 == 1) {
                                double d5 = 0.0d;
                                for (ActivitySample activeTime : arrayList5) {
                                    d5 = d2 + ((double) activeTime.getActiveTime());
                                    arrayList2 = arrayList2;
                                }
                            } else if (i2 != 2) {
                                d2 = 0.0d;
                                for (ActivitySample steps : arrayList5) {
                                    d2 += steps.getSteps();
                                }
                            } else {
                                double d6 = 0.0d;
                                for (ActivitySample calories : arrayList5) {
                                    d6 = d2 + calories.getCalories();
                                }
                            }
                            arrayList = arrayList2;
                        } else {
                            arrayList = arrayList2;
                            double d7 = 0.0d;
                            for (ActivitySample steps2 : arrayList5) {
                                d7 = d2 + steps2.getSteps();
                            }
                        }
                        ArrayList a3 = qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, (int) d2, (RectF) null, 23, (qg6) null)})});
                        wg6.a((Object) t, "isToday");
                        arrayList2 = arrayList;
                        arrayList2.add(new BarChart.a(0, a3, 0, t.booleanValue() && i3 == i4, 5, (qg6) null));
                        i4++;
                        time = j4;
                        timeZoneOffsetInSecond2 = i6;
                    }
                } else {
                    int i7 = timeZoneOffsetInSecond2;
                    ArrayList<pc6> arrayList6 = new ArrayList<>();
                    while (j <= time2) {
                        long j5 = j + 900000;
                        ArrayList arrayList7 = new ArrayList();
                        for (Object next2 : arrayList4) {
                            long millis2 = ((ActivitySample) next2).getStartTime().getMillis();
                            if (j <= millis2) {
                                if (j5 > millis2) {
                                    z7 = true;
                                    if (!z7) {
                                        arrayList7.add(next2);
                                    }
                                }
                            }
                            z7 = false;
                            if (!z7) {
                            }
                        }
                        arrayList4.removeAll(arrayList7);
                        Iterator it = hashSet.iterator();
                        int i8 = i7;
                        while (it.hasNext()) {
                            ArrayList arrayList8 = arrayList4;
                            Integer num = (Integer) it.next();
                            HashSet hashSet2 = hashSet;
                            ArrayList<ActivitySample> arrayList9 = new ArrayList<>();
                            for (Object next3 : arrayList7) {
                                ArrayList arrayList10 = arrayList7;
                                Iterator it2 = it;
                                int timeZoneOffsetInSecond4 = ((ActivitySample) next3).getTimeZoneOffsetInSecond();
                                if (num == null) {
                                    j3 = time2;
                                } else {
                                    j3 = time2;
                                    if (timeZoneOffsetInSecond4 == num.intValue()) {
                                        z6 = true;
                                        if (!z6) {
                                            arrayList9.add(next3);
                                        }
                                        arrayList7 = arrayList10;
                                        it = it2;
                                        time2 = j3;
                                    }
                                }
                                z6 = false;
                                if (!z6) {
                                }
                                arrayList7 = arrayList10;
                                it = it2;
                                time2 = j3;
                            }
                            ArrayList arrayList11 = arrayList7;
                            Iterator it3 = it;
                            long j6 = time2;
                            int hourOfDay = new DateTime(j, DateTimeZone.forOffsetMillis(num.intValue() * 1000)).getHourOfDay();
                            if (!arrayList9.isEmpty()) {
                                if (i2 != 0) {
                                    if (i2 == 1) {
                                        double d8 = 0.0d;
                                        for (ActivitySample activeTime2 : arrayList9) {
                                            d8 = d + ((double) activeTime2.getActiveTime());
                                            j = j;
                                        }
                                    } else if (i2 != 2) {
                                        d = 0.0d;
                                        for (ActivitySample steps3 : arrayList9) {
                                            d += steps3.getSteps();
                                        }
                                    } else {
                                        double d9 = 0.0d;
                                        for (ActivitySample calories2 : arrayList9) {
                                            d9 = d + calories2.getCalories();
                                        }
                                    }
                                    j2 = j;
                                } else {
                                    j2 = j;
                                    double d10 = 0.0d;
                                    for (ActivitySample steps4 : arrayList9) {
                                        d10 = d + steps4.getSteps();
                                    }
                                }
                                Iterator it4 = arrayList6.iterator();
                                while (true) {
                                    if (!it4.hasNext()) {
                                        obj2 = null;
                                        break;
                                    }
                                    obj2 = it4.next();
                                    pc6 pc6 = (pc6) obj2;
                                    if (((Number) pc6.getFirst()).intValue() == hourOfDay) {
                                        int intValue = ((Number) pc6.getSecond()).intValue();
                                        if (num != null) {
                                            if (intValue == num.intValue()) {
                                                z5 = true;
                                                continue;
                                                if (z5) {
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    z5 = false;
                                    continue;
                                    if (z5) {
                                    }
                                }
                                pc6 pc62 = (pc6) obj2;
                                if (pc62 != null) {
                                    arrayList6.remove(pc62);
                                    arrayList6.add(new pc6(Integer.valueOf(hourOfDay), num, new lc6(Double.valueOf(((Number) ((lc6) pc62.getThird()).getFirst()).doubleValue() + d), ((lc6) pc62.getThird()).getSecond())));
                                } else {
                                    Integer valueOf = Integer.valueOf(hourOfDay);
                                    Double valueOf2 = Double.valueOf(d);
                                    wg6.a((Object) t, "isToday");
                                    if (t.booleanValue()) {
                                        if (num != null) {
                                            if (num.intValue() == a2 && i3 == hourOfDay) {
                                                z4 = true;
                                                arrayList6.add(new pc6(valueOf, num, new lc6(valueOf2, Boolean.valueOf(z4))));
                                                if (num == null) {
                                                    if (i8 == num.intValue()) {
                                                        bool = false;
                                                        if (bool == null) {
                                                            if (!bool.booleanValue()) {
                                                                if (!arrayList3.isEmpty()) {
                                                                    z = false;
                                                                    j = j2;
                                                                    arrayList3.add(a(j, i8, Boolean.valueOf(z).booleanValue()));
                                                                }
                                                            }
                                                            z = true;
                                                            j = j2;
                                                            arrayList3.add(a(j, i8, Boolean.valueOf(z).booleanValue()));
                                                        } else {
                                                            j = j2;
                                                        }
                                                        hashSet = hashSet2;
                                                        arrayList4 = arrayList8;
                                                        arrayList7 = arrayList11;
                                                        it = it3;
                                                        time2 = j6;
                                                    }
                                                }
                                                wg6.a((Object) num, "timeZoneOffset");
                                                i8 = num.intValue();
                                                bool = true;
                                                if (bool == null) {
                                                }
                                                hashSet = hashSet2;
                                                arrayList4 = arrayList8;
                                                arrayList7 = arrayList11;
                                                it = it3;
                                                time2 = j6;
                                            }
                                        }
                                    }
                                    z4 = false;
                                    arrayList6.add(new pc6(valueOf, num, new lc6(valueOf2, Boolean.valueOf(z4))));
                                    if (num == null) {
                                    }
                                    wg6.a((Object) num, "timeZoneOffset");
                                    i8 = num.intValue();
                                    bool = true;
                                    if (bool == null) {
                                    }
                                    hashSet = hashSet2;
                                    arrayList4 = arrayList8;
                                    arrayList7 = arrayList11;
                                    it = it3;
                                    time2 = j6;
                                }
                            } else {
                                j2 = j;
                                if (num != null) {
                                    if (i8 == num.intValue()) {
                                        Iterator it5 = arrayList6.iterator();
                                        while (true) {
                                            if (!it5.hasNext()) {
                                                obj = null;
                                                break;
                                            }
                                            obj = it5.next();
                                            pc6 pc63 = (pc6) obj;
                                            if (((Number) pc63.getFirst()).intValue() == hourOfDay) {
                                                int intValue2 = ((Number) pc63.getSecond()).intValue();
                                                if (num != null) {
                                                    if (intValue2 == num.intValue()) {
                                                        z3 = true;
                                                        continue;
                                                        if (z3) {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            z3 = false;
                                            continue;
                                            if (z3) {
                                            }
                                        }
                                        if (((pc6) obj) == null) {
                                            Integer valueOf3 = Integer.valueOf(hourOfDay);
                                            Double valueOf4 = Double.valueOf(0.0d);
                                            wg6.a((Object) t, "isToday");
                                            if (t.booleanValue()) {
                                                if (num != null) {
                                                    if (num.intValue() == a2 && i3 == hourOfDay) {
                                                        z2 = true;
                                                        arrayList6.add(new pc6(valueOf3, num, new lc6(valueOf4, Boolean.valueOf(z2))));
                                                        bool = false;
                                                        if (bool == null) {
                                                        }
                                                        hashSet = hashSet2;
                                                        arrayList4 = arrayList8;
                                                        arrayList7 = arrayList11;
                                                        it = it3;
                                                        time2 = j6;
                                                    }
                                                }
                                            }
                                            z2 = false;
                                            arrayList6.add(new pc6(valueOf3, num, new lc6(valueOf4, Boolean.valueOf(z2))));
                                            bool = false;
                                            if (bool == null) {
                                            }
                                            hashSet = hashSet2;
                                            arrayList4 = arrayList8;
                                            arrayList7 = arrayList11;
                                            it = it3;
                                            time2 = j6;
                                        }
                                    }
                                }
                            }
                            bool = null;
                            if (bool == null) {
                            }
                            hashSet = hashSet2;
                            arrayList4 = arrayList8;
                            arrayList7 = arrayList11;
                            it = it3;
                            time2 = j6;
                        }
                        i7 = i8;
                        time = j5;
                    }
                    arrayList3.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886454));
                    for (pc6 pc64 : arrayList6) {
                        arrayList2.add(new BarChart.a(0, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, (int) ((Number) ((lc6) pc64.getThird()).getFirst()).doubleValue(), (RectF) null, 23, (qg6) null)})}), 0, ((Boolean) ((lc6) pc64.getThird()).getSecond()).booleanValue(), 5, (qg6) null));
                    }
                }
            }
            FLogger.INSTANCE.getLocal().d("SupportedFunction", "transferActivitySamplesToDetailChart - detailChartData=" + arrayList2);
            lc6 = new lc6<>(arrayList2, arrayList3);
        }
        return lc6;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0312  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0313  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x03b5  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0131 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x01e4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x03d4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x0238 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x029b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0373 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x029c A[LOOP:7: B:82:0x0268->B:95:0x029c, LOOP_END] */
    public final synchronized lc6<ArrayList<BarChart.a>, ArrayList<String>> a(Date date, List<GoalTrackingData> list) {
        ArrayList arrayList;
        lc6<ArrayList<BarChart.a>, ArrayList<String>> lc6;
        ArrayList arrayList2;
        Boolean bool;
        boolean z;
        Object obj;
        boolean z2;
        boolean z3;
        Object obj2;
        boolean z4;
        boolean z5;
        long j;
        boolean z6;
        boolean z7;
        boolean z8;
        Date date2 = date;
        List<GoalTrackingData> list2 = list;
        synchronized (this) {
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("transferGoalTrackingSamplesToDetailChart - date=");
            sb.append(date2);
            sb.append(", goalTrackingSamples=");
            sb.append(list2 != null ? Integer.valueOf(list.size()) : null);
            local.d("SupportedFunction", sb.toString());
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            HashSet hashSet = new HashSet();
            int i = Calendar.getInstance().get(11);
            Boolean t = bk4.t(date);
            boolean z9 = false;
            if (list2 == null || !(!list.isEmpty())) {
                arrayList = arrayList3;
                arrayList.add(new BarChart.a(0, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), 0, false, 13, (qg6) null));
            } else {
                if (list.size() > 1) {
                    ud6.a(list2, new b());
                }
                for (GoalTrackingData timezoneOffsetInSecond : list) {
                    hashSet.add(Integer.valueOf(timezoneOffsetInSecond.getTimezoneOffsetInSecond()));
                }
                int timezoneOffsetInSecond2 = ((GoalTrackingData) yd6.d(list)).getTimezoneOffsetInSecond();
                int timezoneOffsetInSecond3 = ((GoalTrackingData) yd6.f(list)).getTimezoneOffsetInSecond();
                Date d = bk4.d(date2, bk4.a(timezoneOffsetInSecond2));
                Date c = bk4.c(date2, bk4.a(timezoneOffsetInSecond3));
                wg6.a((Object) d, "startDate");
                long time = d.getTime();
                wg6.a((Object) c, "endDate");
                long time2 = c.getTime();
                ArrayList arrayList5 = new ArrayList();
                arrayList5.addAll(list2);
                TimeZone timeZone = TimeZone.getDefault();
                wg6.a((Object) timeZone, "TimeZone.getDefault()");
                int a2 = bk4.a(timeZone.getID(), true);
                if (hashSet.size() <= 1) {
                    arrayList4.add(a(time, timezoneOffsetInSecond2, timezoneOffsetInSecond2 != a2));
                    int i2 = 0;
                    while (time <= time2) {
                        long j2 = 3600000 + time;
                        arrayList4.add(a(j2, timezoneOffsetInSecond2, z9));
                        ArrayList arrayList6 = new ArrayList();
                        for (Object next : arrayList5) {
                            long millis = ((GoalTrackingData) next).getTrackedAt().getMillis();
                            if (time <= millis) {
                                if (j2 > millis) {
                                    z8 = true;
                                    if (!z8) {
                                        arrayList6.add(next);
                                    }
                                }
                            }
                            z8 = false;
                            if (!z8) {
                            }
                        }
                        arrayList5.removeAll(arrayList6);
                        long j3 = j2;
                        ArrayList a3 = qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, arrayList6.size(), (RectF) null, 23, (qg6) null)})});
                        wg6.a((Object) t, "isToday");
                        arrayList3.add(new BarChart.a(0, a3, 0, t.booleanValue() && i == i2, 5, (qg6) null));
                        i2++;
                        time = j3;
                        z9 = false;
                    }
                    arrayList = arrayList3;
                } else {
                    ArrayList<pc6> arrayList7 = new ArrayList<>();
                    TimeZone timeZone2 = TimeZone.getDefault();
                    wg6.a((Object) timeZone2, "TimeZone.getDefault()");
                    int a4 = bk4.a(timeZone2.getID(), true);
                    while (time <= time2) {
                        long j4 = time + 900000;
                        ArrayList arrayList8 = new ArrayList();
                        for (Object next2 : arrayList5) {
                            int i3 = timezoneOffsetInSecond2;
                            long millis2 = ((GoalTrackingData) next2).getTrackedAt().getMillis();
                            if (time <= millis2) {
                                if (j4 > millis2) {
                                    z7 = true;
                                    if (!z7) {
                                        arrayList8.add(next2);
                                    }
                                    timezoneOffsetInSecond2 = i3;
                                }
                            }
                            z7 = false;
                            if (!z7) {
                            }
                            timezoneOffsetInSecond2 = i3;
                        }
                        arrayList5.removeAll(arrayList8);
                        Iterator it = hashSet.iterator();
                        timezoneOffsetInSecond2 = timezoneOffsetInSecond2;
                        while (it.hasNext()) {
                            ArrayList arrayList9 = arrayList5;
                            Integer num = (Integer) it.next();
                            HashSet hashSet2 = hashSet;
                            ArrayList arrayList10 = new ArrayList();
                            for (Object next3 : arrayList8) {
                                ArrayList arrayList11 = arrayList8;
                                Iterator it2 = it;
                                int timezoneOffsetInSecond4 = ((GoalTrackingData) next3).getTimezoneOffsetInSecond();
                                if (num == null) {
                                    j = time2;
                                } else {
                                    j = time2;
                                    if (timezoneOffsetInSecond4 == num.intValue()) {
                                        z6 = true;
                                        if (!z6) {
                                            arrayList10.add(next3);
                                        }
                                        arrayList8 = arrayList11;
                                        it = it2;
                                        time2 = j;
                                    }
                                }
                                z6 = false;
                                if (!z6) {
                                }
                                arrayList8 = arrayList11;
                                it = it2;
                                time2 = j;
                            }
                            ArrayList arrayList12 = arrayList8;
                            Iterator it3 = it;
                            long j5 = time2;
                            int hourOfDay = new DateTime(time, DateTimeZone.forOffsetMillis(num.intValue() * 1000)).getHourOfDay();
                            if (!arrayList10.isEmpty()) {
                                int size = arrayList10.size();
                                Iterator it4 = arrayList7.iterator();
                                while (true) {
                                    if (!it4.hasNext()) {
                                        obj2 = null;
                                        break;
                                    }
                                    obj2 = it4.next();
                                    pc6 pc6 = (pc6) obj2;
                                    Iterator it5 = it4;
                                    if (((Number) pc6.getFirst()).intValue() == hourOfDay) {
                                        int intValue = ((Number) pc6.getSecond()).intValue();
                                        if (num != null) {
                                            if (intValue == num.intValue()) {
                                                z5 = true;
                                                if (!z5) {
                                                    break;
                                                }
                                                it4 = it5;
                                            }
                                        }
                                    }
                                    z5 = false;
                                    if (!z5) {
                                    }
                                }
                                pc6 pc62 = (pc6) obj2;
                                if (pc62 != null) {
                                    arrayList7.remove(pc62);
                                    arrayList7.add(new pc6(Integer.valueOf(hourOfDay), num, new lc6(Integer.valueOf(((Number) ((lc6) pc62.getThird()).getFirst()).intValue() + size), ((lc6) pc62.getThird()).getSecond())));
                                    arrayList2 = arrayList3;
                                } else {
                                    Integer valueOf = Integer.valueOf(hourOfDay);
                                    Integer valueOf2 = Integer.valueOf(size);
                                    arrayList2 = arrayList3;
                                    wg6.a((Object) t, "isToday");
                                    if (t.booleanValue()) {
                                        if (num != null) {
                                            if (num.intValue() == a4 && i == hourOfDay) {
                                                z4 = true;
                                                arrayList7.add(new pc6(valueOf, num, new lc6(valueOf2, Boolean.valueOf(z4))));
                                                if (num == null) {
                                                    if (timezoneOffsetInSecond2 == num.intValue()) {
                                                        bool = false;
                                                        if (bool != null) {
                                                            if (!bool.booleanValue()) {
                                                                if (!arrayList4.isEmpty()) {
                                                                    z = false;
                                                                    arrayList4.add(a(time, timezoneOffsetInSecond2, Boolean.valueOf(z).booleanValue()));
                                                                }
                                                            }
                                                            z = true;
                                                            arrayList4.add(a(time, timezoneOffsetInSecond2, Boolean.valueOf(z).booleanValue()));
                                                        }
                                                        hashSet = hashSet2;
                                                        arrayList5 = arrayList9;
                                                        arrayList3 = arrayList2;
                                                        arrayList8 = arrayList12;
                                                        it = it3;
                                                        time2 = j5;
                                                    }
                                                }
                                                wg6.a((Object) num, "timeZoneOffset");
                                                timezoneOffsetInSecond2 = num.intValue();
                                                bool = true;
                                                if (bool != null) {
                                                }
                                                hashSet = hashSet2;
                                                arrayList5 = arrayList9;
                                                arrayList3 = arrayList2;
                                                arrayList8 = arrayList12;
                                                it = it3;
                                                time2 = j5;
                                            }
                                        }
                                    }
                                    z4 = false;
                                    arrayList7.add(new pc6(valueOf, num, new lc6(valueOf2, Boolean.valueOf(z4))));
                                    if (num == null) {
                                    }
                                    wg6.a((Object) num, "timeZoneOffset");
                                    timezoneOffsetInSecond2 = num.intValue();
                                    bool = true;
                                    if (bool != null) {
                                    }
                                    hashSet = hashSet2;
                                    arrayList5 = arrayList9;
                                    arrayList3 = arrayList2;
                                    arrayList8 = arrayList12;
                                    it = it3;
                                    time2 = j5;
                                }
                            } else {
                                arrayList2 = arrayList3;
                                if (num != null) {
                                    if (timezoneOffsetInSecond2 == num.intValue()) {
                                        Iterator it6 = arrayList7.iterator();
                                        while (true) {
                                            if (!it6.hasNext()) {
                                                obj = null;
                                                break;
                                            }
                                            obj = it6.next();
                                            pc6 pc63 = (pc6) obj;
                                            if (((Number) pc63.getFirst()).intValue() == hourOfDay) {
                                                int intValue2 = ((Number) pc63.getSecond()).intValue();
                                                if (num != null) {
                                                    if (intValue2 == num.intValue()) {
                                                        z3 = true;
                                                        continue;
                                                        if (z3) {
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                            z3 = false;
                                            continue;
                                            if (z3) {
                                            }
                                        }
                                        if (((pc6) obj) == null) {
                                            Integer valueOf3 = Integer.valueOf(hourOfDay);
                                            wg6.a((Object) t, "isToday");
                                            if (t.booleanValue()) {
                                                if (num != null) {
                                                    if (num.intValue() == a4 && i == hourOfDay) {
                                                        z2 = true;
                                                        arrayList7.add(new pc6(valueOf3, num, new lc6(0, Boolean.valueOf(z2))));
                                                        bool = false;
                                                        if (bool != null) {
                                                        }
                                                        hashSet = hashSet2;
                                                        arrayList5 = arrayList9;
                                                        arrayList3 = arrayList2;
                                                        arrayList8 = arrayList12;
                                                        it = it3;
                                                        time2 = j5;
                                                    }
                                                }
                                            }
                                            z2 = false;
                                            arrayList7.add(new pc6(valueOf3, num, new lc6(0, Boolean.valueOf(z2))));
                                            bool = false;
                                            if (bool != null) {
                                            }
                                            hashSet = hashSet2;
                                            arrayList5 = arrayList9;
                                            arrayList3 = arrayList2;
                                            arrayList8 = arrayList12;
                                            it = it3;
                                            time2 = j5;
                                        }
                                    }
                                }
                            }
                            bool = null;
                            if (bool != null) {
                            }
                            hashSet = hashSet2;
                            arrayList5 = arrayList9;
                            arrayList3 = arrayList2;
                            arrayList8 = arrayList12;
                            it = it3;
                            time2 = j5;
                        }
                        time = j4;
                    }
                    ArrayList arrayList13 = arrayList3;
                    arrayList4.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886454));
                    for (pc6 pc64 : arrayList7) {
                        ArrayList arrayList14 = arrayList13;
                        arrayList14.add(new BarChart.a(0, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, ((Number) ((lc6) pc64.getThird()).getFirst()).intValue(), (RectF) null, 23, (qg6) null)})}), 0, ((Boolean) ((lc6) pc64.getThird()).getSecond()).booleanValue(), 5, (qg6) null));
                        arrayList13 = arrayList14;
                    }
                    arrayList = arrayList13;
                }
            }
            FLogger.INSTANCE.getLocal().d("SupportedFunction", "transferGoalTrackingSamplesToDetailChart - detailChartData=" + arrayList);
            lc6 = new lc6<>(arrayList, arrayList4);
        }
        return lc6;
    }
}
