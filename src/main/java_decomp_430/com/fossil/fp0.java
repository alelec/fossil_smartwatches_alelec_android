package com.fossil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fp0 {
    @DexIgnore
    public final JSONArray a(File file) {
        JSONArray jSONArray = new JSONArray();
        String a = sc1.a.a(file);
        if (a != null) {
            List a2 = yj6.a(a, new String[]{mi0.A.y()}, false, 0, 6, (Object) null);
            ArrayList<String> arrayList = new ArrayList<>();
            for (Object next : a2) {
                if (!xj6.a((String) next)) {
                    arrayList.add(next);
                }
            }
            for (String decryptAES_CBC : arrayList) {
                String decryptAES_CBC2 = Crypto.instance.decryptAES_CBC(decryptAES_CBC);
                if (decryptAES_CBC2 != null) {
                    try {
                        jSONArray.put(new JSONObject(decryptAES_CBC2));
                    } catch (Exception unused) {
                        cd6 cd6 = cd6.a;
                    }
                }
            }
        }
        return jSONArray;
    }

    @DexIgnore
    public JSONObject b(File file) {
        JSONObject jSONObject = new JSONObject();
        JSONArray a = a(file);
        if (a.length() > 0) {
            jSONObject.put("data", a);
        }
        return jSONObject;
    }
}
