package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class he6 extends ge6 {
    @DexIgnore
    public static final <K, V> HashMap<K, V> a(lc6<? extends K, ? extends V>... lc6Arr) {
        wg6.b(lc6Arr, "pairs");
        HashMap<K, V> hashMap = new HashMap<>(a(lc6Arr.length));
        a(hashMap, lc6Arr);
        return hashMap;
    }

    @DexIgnore
    public static final <K, V> V b(Map<K, ? extends V> map, K k) {
        wg6.b(map, "$this$getValue");
        return fe6.a(map, k);
    }

    @DexIgnore
    public static final int a(int i) {
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return i + (i / 3);
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public static final <K, V> void a(Map<? super K, ? super V> map, lc6<? extends K, ? extends V>[] lc6Arr) {
        wg6.b(map, "$this$putAll");
        wg6.b(lc6Arr, "pairs");
        for (lc6<? extends K, ? extends V> lc6 : lc6Arr) {
            map.put(lc6.component1(), lc6.component2());
        }
    }
}
