package com.fossil;

import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter$save$2$1", f = "InactivityNudgeTimePresenter.kt", l = {}, m = "invokeSuspend")
public final class c05$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ InactivityNudgeTimePresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ c05$c$a a;

        @DexIgnore
        public a(c05$c$a c05_c_a) {
            this.a = c05_c_a;
        }

        @DexIgnore
        public final void run() {
            InactivityNudgeTimeDao inactivityNudgeTimeDao = this.a.this$0.this$0.j.getInactivityNudgeTimeDao();
            InactivityNudgeTimeModel b = this.a.this$0.this$0.h;
            if (b != null) {
                inactivityNudgeTimeDao.upsertInactivityNudgeTime(b);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c05$c$a(InactivityNudgeTimePresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        c05$c$a c05_c_a = new c05$c$a(this.this$0, xe6);
        c05_c_a.p$ = (il6) obj;
        return c05_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((c05$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.j.runInTransaction(new a(this));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
