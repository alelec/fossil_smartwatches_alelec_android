package com.fossil;

import android.graphics.Bitmap;
import com.portfolio.platform.service.ShakeFeedbackService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$1$1", f = "ShakeFeedbackService.kt", l = {245, 246}, m = "invokeSuspend")
public final class up4$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Bitmap $bitmap;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$showQuestionDialog$1$1$1", f = "ShakeFeedbackService.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ up4$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(up4$d$a up4_d_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = up4_d_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                qg3 c = this.this$0.this$0.a.d;
                if (c != null) {
                    c.dismiss();
                    return cd6.a;
                }
                wg6.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public up4$d$a(ShakeFeedbackService.d dVar, Bitmap bitmap, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$bitmap = bitmap;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        up4$d$a up4_d_a = new up4$d$a(this.this$0, this.$bitmap, xe6);
        up4_d_a.p$ = (il6) obj;
        return up4_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((up4$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            ShakeFeedbackService shakeFeedbackService = this.this$0.a;
            Bitmap bitmap = this.$bitmap;
            this.L$0 = il6;
            this.label = 1;
            if (shakeFeedbackService.a(bitmap, (xe6<? super cd6>) this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        cn6 c = zl6.c();
        a aVar = new a(this, (xe6) null);
        this.L$0 = il6;
        this.label = 2;
        if (gk6.a(c, aVar, this) == a2) {
            return a2;
        }
        return cd6.a;
    }
}
