package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t14 implements Factory<vm4> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> c;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> d;
    @DexIgnore
    public /* final */ Provider<an4> e;
    @DexIgnore
    public /* final */ Provider<tw5> f;

    @DexIgnore
    public t14(b14 b14, Provider<HybridPresetRepository> provider, Provider<QuickResponseRepository> provider2, Provider<AlarmsRepository> provider3, Provider<an4> provider4, Provider<tw5> provider5) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
    }

    @DexIgnore
    public static t14 a(b14 b14, Provider<HybridPresetRepository> provider, Provider<QuickResponseRepository> provider2, Provider<AlarmsRepository> provider3, Provider<an4> provider4, Provider<tw5> provider5) {
        return new t14(b14, provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static vm4 b(b14 b14, Provider<HybridPresetRepository> provider, Provider<QuickResponseRepository> provider2, Provider<AlarmsRepository> provider3, Provider<an4> provider4, Provider<tw5> provider5) {
        return a(b14, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get());
    }

    @DexIgnore
    public static LinkStreamingManager a(b14 b14, HybridPresetRepository hybridPresetRepository, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, an4 an4, SetNotificationUseCase setNotificationUseCase) {
        LinkStreamingManager a2 = b14.a(hybridPresetRepository, quickResponseRepository, alarmsRepository, an4, setNotificationUseCase);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public LinkStreamingManager get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
