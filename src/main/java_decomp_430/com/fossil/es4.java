package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es4 implements Factory<ds4> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> e;

    @DexIgnore
    public es4(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static es4 a(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5) {
        return new es4(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static ds4 b(Provider<DeviceRepository> provider, Provider<HybridPresetRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<PortfolioApp> provider4, Provider<WatchFaceRepository> provider5) {
        return new UnlinkDeviceUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get());
    }

    @DexIgnore
    public UnlinkDeviceUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e);
    }
}
