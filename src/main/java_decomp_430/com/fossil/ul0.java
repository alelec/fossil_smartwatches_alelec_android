package com.fossil;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.recyclerview.widget.RecyclerView;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ul0 extends HandlerThread {
    @DexIgnore
    public /* final */ Handler a;
    @DexIgnore
    public /* final */ ag1 b;
    @DexIgnore
    public /* final */ xe0 c;
    @DexIgnore
    public /* final */ cd6 d; // = cd6.a;
    @DexIgnore
    public /* final */ lp0 e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ul0(String str, int i, long j, String str2, String str3, ge0 ge0, int i2, fp0 fp0, lp0 lp0, boolean z) {
        super(ul0.class.getSimpleName(), 10);
        int i3 = i2;
        this.e = lp0;
        this.f = z;
        super.start();
        this.a = new Handler(getLooper());
        this.b = new ag1(str, i, j, this.a, str2, str3);
        this.c = new xe0(this.b, ge0, fp0, this.a, this.f);
        if (i3 > 0) {
            xe0 xe0 = this.c;
            long j2 = ((long) i3) * ((long) 1000);
            wh1 wh1 = xe0.d;
            if (wh1 != null) {
                wh1.a = true;
                xe0.j.removeCallbacks(wh1);
            }
            if (j2 > 0) {
                xe0.f = j2;
            }
            xe0.d = new wh1(xe0);
            wh1 wh12 = xe0.d;
            if (wh12 != null) {
                xe0.j.post(wh12);
            }
        }
    }

    @DexIgnore
    public abstract long a();

    @DexIgnore
    public boolean a(nn0 nn0) {
        boolean b2 = b(nn0);
        if (a() > 0) {
            a(a());
        }
        return b2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0031, code lost:
        r0 = r4.b;
     */
    @DexIgnore
    public final boolean b(nn0 nn0) {
        ag1 ag1;
        File e2;
        nn0.b = b();
        nn0.l = xq0.e.a();
        r40 a2 = xq0.e.a(nn0.f);
        if (a2 != null) {
            nn0.k = a2;
        }
        nn0.j = xq0.e.b(nn0.f);
        String encryptAES_CBC = Crypto.instance.encryptAES_CBC(nn0.a(0));
        if (encryptAES_CBC == null || (e2 = ag1.e()) == null) {
            return false;
        }
        return ag1.e.post(new jc1(e2, ag1, encryptAES_CBC));
    }

    @DexIgnore
    public final void start() {
        super.start();
    }

    @DexIgnore
    public static /* synthetic */ void a(ul0 ul0, long j, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                j = 0;
            }
            ul0.a(j);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startUploadLog");
    }

    @DexIgnore
    public final void a(long j) {
        xe0 xe0 = this.c;
        dr0 dr0 = xe0.e;
        if (dr0 != null) {
            dr0.a = true;
            xe0.j.removeCallbacks(dr0);
        }
        xe0.e = new dr0(new qj1(xe0));
        dr0 dr02 = xe0.e;
        if (dr02 != null) {
            xe0.j.postDelayed(dr02, j);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004d  */
    @SuppressLint({"ApplySharedPref"})
    public final long b() {
        Long l;
        long longValue;
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putLong;
        synchronized (this.d) {
            SharedPreferences a2 = cw0.a(this.e);
            Long valueOf = a2 != null ? Long.valueOf(a2.getLong("log_line_number", RecyclerView.FOREVER_NS)) : null;
            if (valueOf != null) {
                if (valueOf.longValue() != RecyclerView.FOREVER_NS) {
                    l = Long.valueOf(valueOf.longValue() + 1);
                    if (!(a2 == null || (edit = a2.edit()) == null)) {
                        putLong = edit.putLong("log_line_number", l.longValue());
                        if (putLong != null) {
                            putLong.commit();
                        }
                    }
                    longValue = l.longValue();
                }
            }
            l = 0L;
            putLong = edit.putLong("log_line_number", l.longValue());
            if (putLong != null) {
            }
            longValue = l.longValue();
        }
        return longValue;
    }
}
