package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn3<K, V> extends ul3<K, V> {
    @DexIgnore
    public transient ul3<V, K> inverse;
    @DexIgnore
    public /* final */ transient K singleKey;
    @DexIgnore
    public /* final */ transient V singleValue;

    @DexIgnore
    public zn3(K k, V v) {
        bl3.a((Object) k, (Object) v);
        this.singleKey = k;
        this.singleValue = v;
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return this.singleKey.equals(obj);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return this.singleValue.equals(obj);
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> createEntrySet() {
        return im3.of(ym3.a(this.singleKey, this.singleValue));
    }

    @DexIgnore
    public im3<K> createKeySet() {
        return im3.of(this.singleKey);
    }

    @DexIgnore
    public V get(Object obj) {
        if (this.singleKey.equals(obj)) {
            return this.singleValue;
        }
        return null;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    public ul3<V, K> inverse() {
        ul3<V, K> ul3 = this.inverse;
        if (ul3 != null) {
            return ul3;
        }
        zn3 zn3 = new zn3(this.singleValue, this.singleKey, this);
        this.inverse = zn3;
        return zn3;
    }

    @DexIgnore
    public zn3(K k, V v, ul3<V, K> ul3) {
        this.singleKey = k;
        this.singleValue = v;
        this.inverse = ul3;
    }
}
