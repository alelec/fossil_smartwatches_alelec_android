package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import com.fossil.y2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j2 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode b; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public static j2 c;
    @DexIgnore
    public y2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements y2.e {
        @DexIgnore
        public /* final */ int[] a; // = {e0.abc_textfield_search_default_mtrl_alpha, e0.abc_textfield_default_mtrl_alpha, e0.abc_ab_share_pack_mtrl_alpha};
        @DexIgnore
        public /* final */ int[] b; // = {e0.abc_ic_commit_search_api_mtrl_alpha, e0.abc_seekbar_tick_mark_material, e0.abc_ic_menu_share_mtrl_alpha, e0.abc_ic_menu_copy_mtrl_am_alpha, e0.abc_ic_menu_cut_mtrl_alpha, e0.abc_ic_menu_selectall_mtrl_alpha, e0.abc_ic_menu_paste_mtrl_am_alpha};
        @DexIgnore
        public /* final */ int[] c; // = {e0.abc_textfield_activated_mtrl_alpha, e0.abc_textfield_search_activated_mtrl_alpha, e0.abc_cab_background_top_mtrl_alpha, e0.abc_text_cursor_material, e0.abc_text_select_handle_left_mtrl_dark, e0.abc_text_select_handle_middle_mtrl_dark, e0.abc_text_select_handle_right_mtrl_dark, e0.abc_text_select_handle_left_mtrl_light, e0.abc_text_select_handle_middle_mtrl_light, e0.abc_text_select_handle_right_mtrl_light};
        @DexIgnore
        public /* final */ int[] d; // = {e0.abc_popup_background_mtrl_mult, e0.abc_cab_background_internal_bg, e0.abc_menu_hardkey_panel_mtrl_mult};
        @DexIgnore
        public /* final */ int[] e; // = {e0.abc_tab_indicator_material, e0.abc_textfield_search_material};
        @DexIgnore
        public /* final */ int[] f; // = {e0.abc_btn_check_material, e0.abc_btn_radio_material, e0.abc_btn_check_material_anim, e0.abc_btn_radio_material_anim};

        @DexIgnore
        public final ColorStateList a(Context context) {
            return b(context, 0);
        }

        @DexIgnore
        public final ColorStateList b(Context context) {
            return b(context, d3.b(context, a0.colorAccent));
        }

        @DexIgnore
        public final ColorStateList c(Context context) {
            return b(context, d3.b(context, a0.colorButtonNormal));
        }

        @DexIgnore
        public final ColorStateList d(Context context) {
            int[][] iArr = new int[3][];
            int[] iArr2 = new int[3];
            ColorStateList c2 = d3.c(context, a0.colorSwitchThumbNormal);
            if (c2 == null || !c2.isStateful()) {
                iArr[0] = d3.b;
                iArr2[0] = d3.a(context, a0.colorSwitchThumbNormal);
                iArr[1] = d3.e;
                iArr2[1] = d3.b(context, a0.colorControlActivated);
                iArr[2] = d3.f;
                iArr2[2] = d3.b(context, a0.colorSwitchThumbNormal);
            } else {
                iArr[0] = d3.b;
                iArr2[0] = c2.getColorForState(iArr[0], 0);
                iArr[1] = d3.e;
                iArr2[1] = d3.b(context, a0.colorControlActivated);
                iArr[2] = d3.f;
                iArr2[2] = c2.getDefaultColor();
            }
            return new ColorStateList(iArr, iArr2);
        }

        @DexIgnore
        public Drawable a(y2 y2Var, Context context, int i) {
            if (i != e0.abc_cab_background_top_material) {
                return null;
            }
            return new LayerDrawable(new Drawable[]{y2Var.b(context, e0.abc_cab_background_internal_bg), y2Var.b(context, e0.abc_cab_background_top_mtrl_alpha)});
        }

        @DexIgnore
        public final ColorStateList b(Context context, int i) {
            int b2 = d3.b(context, a0.colorControlHighlight);
            int a2 = d3.a(context, a0.colorButtonNormal);
            return new ColorStateList(new int[][]{d3.b, d3.d, d3.c, d3.f}, new int[]{a2, f7.b(b2, i), f7.b(b2, i), i});
        }

        @DexIgnore
        public final void a(Drawable drawable, int i, PorterDuff.Mode mode) {
            if (s2.a(drawable)) {
                drawable = drawable.mutate();
            }
            if (mode == null) {
                mode = j2.b;
            }
            drawable.setColorFilter(j2.a(i, mode));
        }

        @DexIgnore
        public final boolean a(int[] iArr, int i) {
            for (int i2 : iArr) {
                if (i2 == i) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public ColorStateList a(Context context, int i) {
            if (i == e0.abc_edit_text_material) {
                return u0.b(context, c0.abc_tint_edittext);
            }
            if (i == e0.abc_switch_track_mtrl_alpha) {
                return u0.b(context, c0.abc_tint_switch_track);
            }
            if (i == e0.abc_switch_thumb_material) {
                return d(context);
            }
            if (i == e0.abc_btn_default_mtrl_shape) {
                return c(context);
            }
            if (i == e0.abc_btn_borderless_material) {
                return a(context);
            }
            if (i == e0.abc_btn_colored_material) {
                return b(context);
            }
            if (i == e0.abc_spinner_mtrl_am_alpha || i == e0.abc_spinner_textfield_background_material) {
                return u0.b(context, c0.abc_tint_spinner);
            }
            if (a(this.b, i)) {
                return d3.c(context, a0.colorControlNormal);
            }
            if (a(this.e, i)) {
                return u0.b(context, c0.abc_tint_default);
            }
            if (a(this.f, i)) {
                return u0.b(context, c0.abc_tint_btn_checkable);
            }
            if (i == e0.abc_seekbar_thumb_material) {
                return u0.b(context, c0.abc_tint_seek_thumb);
            }
            return null;
        }

        @DexIgnore
        public boolean b(Context context, int i, Drawable drawable) {
            if (i == e0.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable;
                a(layerDrawable.findDrawableByLayerId(16908288), d3.b(context, a0.colorControlNormal), j2.b);
                a(layerDrawable.findDrawableByLayerId(16908303), d3.b(context, a0.colorControlNormal), j2.b);
                a(layerDrawable.findDrawableByLayerId(16908301), d3.b(context, a0.colorControlActivated), j2.b);
                return true;
            } else if (i != e0.abc_ratingbar_material && i != e0.abc_ratingbar_indicator_material && i != e0.abc_ratingbar_small_material) {
                return false;
            } else {
                LayerDrawable layerDrawable2 = (LayerDrawable) drawable;
                a(layerDrawable2.findDrawableByLayerId(16908288), d3.a(context, a0.colorControlNormal), j2.b);
                a(layerDrawable2.findDrawableByLayerId(16908303), d3.b(context, a0.colorControlActivated), j2.b);
                a(layerDrawable2.findDrawableByLayerId(16908301), d3.b(context, a0.colorControlActivated), j2.b);
                return true;
            }
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x004b  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0066 A[RETURN] */
        public boolean a(Context context, int i, Drawable drawable) {
            boolean z;
            PorterDuff.Mode mode;
            int i2;
            PorterDuff.Mode a2 = j2.b;
            int i3 = 16842801;
            if (a(this.a, i)) {
                i3 = a0.colorControlNormal;
            } else if (a(this.c, i)) {
                i3 = a0.colorControlActivated;
            } else if (a(this.d, i)) {
                a2 = PorterDuff.Mode.MULTIPLY;
            } else {
                if (i == e0.abc_list_divider_mtrl_alpha) {
                    i3 = 16842800;
                    mode = a2;
                    i2 = Math.round(40.8f);
                    z = true;
                } else if (i != e0.abc_dialog_material_background) {
                    mode = a2;
                    z = false;
                    i2 = -1;
                    i3 = 0;
                }
                if (z) {
                    return false;
                }
                if (s2.a(drawable)) {
                    drawable = drawable.mutate();
                }
                drawable.setColorFilter(j2.a(d3.b(context, i3), mode));
                if (i2 != -1) {
                    drawable.setAlpha(i2);
                }
                return true;
            }
            mode = a2;
            z = true;
            i2 = -1;
            if (z) {
            }
        }

        @DexIgnore
        public PorterDuff.Mode a(int i) {
            if (i == e0.abc_switch_thumb_material) {
                return PorterDuff.Mode.MULTIPLY;
            }
            return null;
        }
    }

    @DexIgnore
    public static synchronized j2 b() {
        j2 j2Var;
        synchronized (j2.class) {
            if (c == null) {
                c();
            }
            j2Var = c;
        }
        return j2Var;
    }

    @DexIgnore
    public static synchronized void c() {
        synchronized (j2.class) {
            if (c == null) {
                c = new j2();
                c.a = y2.a();
                c.a.a((y2.e) new a());
            }
        }
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i) {
        return this.a.b(context, i);
    }

    @DexIgnore
    public synchronized Drawable a(Context context, int i, boolean z) {
        return this.a.a(context, i, z);
    }

    @DexIgnore
    public synchronized void a(Context context) {
        this.a.b(context);
    }

    @DexIgnore
    public synchronized ColorStateList b(Context context, int i) {
        return this.a.c(context, i);
    }

    @DexIgnore
    public static void a(Drawable drawable, g3 g3Var, int[] iArr) {
        y2.a(drawable, g3Var, iArr);
    }

    @DexIgnore
    public static synchronized PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter a2;
        synchronized (j2.class) {
            a2 = y2.a(i, mode);
        }
        return a2;
    }
}
