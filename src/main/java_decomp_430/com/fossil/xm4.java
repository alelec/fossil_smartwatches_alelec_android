package com.fossil;

import android.content.Context;
import android.os.Build;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xm4 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT >= 29);
    @DexIgnore
    public static /* final */ HashMap<String, ArrayList<Integer>> b;
    @DexIgnore
    public static an4 c;
    @DexIgnore
    public static /* final */ xm4 d; // = new xm4();

    /*
    static {
        HashMap<String, ArrayList<Integer>> hashMap = new HashMap<>();
        hashMap.put("PAIR_DEVICE", qd6.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("SYNC_DEVICE", qd6.a((T[]) new Integer[]{1}));
        hashMap.put("NOTIFICATION_CONTACTS", qd6.a((T[]) new Integer[]{4, 100, 101}));
        hashMap.put("NOTIFICATION_APPS", qd6.a((T[]) new Integer[]{10}));
        hashMap.put("SET_ALARMS", qd6.a((T[]) new Integer[]{1}));
        hashMap.put("SET_NOTIFICATION", qd6.a((T[]) new Integer[]{1}));
        hashMap.put("SET_COMPLICATION", qd6.a((T[]) new Integer[]{1}));
        hashMap.put("SET_WATCH_APP_COMMUTE_TIME", qd6.a((T[]) new Integer[]{1, 2, 3, 15}));
        hashMap.put("SET_COMPLICATION_WEATHER", qd6.a((T[]) new Integer[]{1, 2, 3, 15}));
        hashMap.put("SET_COMPLICATION_CHANCE_OF_RAIN", qd6.a((T[]) new Integer[]{1, 2, 15}));
        hashMap.put("SET_WATCH_APP", qd6.a((T[]) new Integer[]{1}));
        hashMap.put("SET_WATCH_APP_MUSIC", qd6.a((T[]) new Integer[]{1, 10}));
        hashMap.put("SET_WATCH_APP_WEATHER", qd6.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("FIND_DEVICE", qd6.a((T[]) new Integer[]{2, 3}));
        hashMap.put("EDIT_AVATAR", qd6.a((T[]) new Integer[]{11, 12}));
        hashMap.put("UPDATE_FIRMWARE", qd6.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("NOTIFICATION_CONTACTS_ASSIGNMENT", qd6.a((T[]) new Integer[]{4}));
        hashMap.put("SET_MICRO_APP", qd6.a((T[]) new Integer[]{1}));
        hashMap.put("SET_MICRO_APP_MUSIC", qd6.a((T[]) new Integer[]{1, 10}));
        hashMap.put("SET_MICRO_APP_COMMUTE_TIME", qd6.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("GET_USER_LOCATION", qd6.a((T[]) new Integer[]{2, 3}));
        hashMap.put("CAPTURE_IMAGE", qd6.a((T[]) new Integer[]{11}));
        b = hashMap;
    }
    */

    @DexIgnore
    public final void a(an4 an4) {
        c = an4;
    }

    @DexIgnore
    public final void b(Context context, String str) {
        wg6.b(str, "microAppId");
        a(this, context, b(str), false, false, false, 28, (Object) null);
    }

    @DexIgnore
    public final boolean c(int i) {
        return i == 2 || i == 4 || i == 5 || i == 6 || i == 15 || i == 7 || i == 8 || i == 9 || i == 11 || i == 12 || i == 100 || i == 101 || i == 13 || i == 14;
    }

    @DexIgnore
    public static /* synthetic */ boolean a(xm4 xm4, Context context, String str, boolean z, boolean z2, boolean z3, int i, Object obj) {
        return xm4.a(context, str, (i & 4) != 0 ? true : z, (i & 8) != 0 ? false : z2, (i & 16) != 0 ? false : z3);
    }

    /* JADX WARNING: type inference failed for: r10v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v22, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v34, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v38, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v42, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v46, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v50, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v53, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v56, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v59, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v62, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v65, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v68, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v71, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v75, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v79, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v83, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v86, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0276, code lost:
        r8 = r1;
        r1 = r10;
        r10 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x029a, code lost:
        r10 = r0;
     */
    @DexIgnore
    public final pc6<String, String, String> b(int i) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5 = "";
        String str6 = null;
        if (i == 15) {
            str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886630);
            wg6.a((Object) str5, "LanguageHelper.getString\u2026BackgroundLocationAccess)");
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887041);
            wg6.a((Object) str, "LanguageHelper.getString\u2026_service_general_explain)");
        } else if (i == 100) {
            str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886632);
            wg6.a((Object) str5, "LanguageHelper.getString\u2026_PhonePermissionRequired)");
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887057);
            wg6.a((Object) str, "LanguageHelper.getString\u2026call_and_sms_permissions)");
        } else if (i != 101) {
            switch (i) {
                case 1:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886696);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886696);
                    wg6.a((Object) str2, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    break;
                case 2:
                    str3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886694);
                    wg6.a((Object) str3, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    str4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886694);
                    wg6.a((Object) str4, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    StringBuilder sb = new StringBuilder();
                    sb.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a2 = jm4.a();
                    wg6.a((Object) a2, "LanguageHelper.getLocale()");
                    sb.append(a2.getLanguage());
                    str6 = sb.toString();
                    break;
                case 3:
                    str3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886695);
                    wg6.a((Object) str3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
                    str4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886695);
                    wg6.a((Object) str4, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("https://support.google.com/accounts/answer/3467281?hl=");
                    Locale a3 = jm4.a();
                    wg6.a((Object) a3, "LanguageHelper.getLocale()");
                    sb2.append(a3.getLanguage());
                    str6 = sb2.toString();
                    break;
                case 4:
                    nh6 nh6 = nh6.a;
                    String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886569);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    Object[] objArr = {PortfolioApp.get.instance().i()};
                    str3 = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) str3, "java.lang.String.format(format, *args)");
                    nh6 nh62 = nh6.a;
                    String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886569);
                    wg6.a((Object) a5, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    Object[] objArr2 = {PortfolioApp.get.instance().i()};
                    str4 = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                    wg6.a((Object) str4, "java.lang.String.format(format, *args)");
                    break;
                case 5:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131887282);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026_phone_state_is_required)");
                    str2 = jm4.a((Context) PortfolioApp.get.instance(), 2131887282);
                    wg6.a((Object) str2, "LanguageHelper.getString\u2026_phone_state_is_required)");
                    break;
                case 6:
                    nh6 nh63 = nh6.a;
                    String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886568);
                    wg6.a((Object) a6, "LanguageHelper.getString\u2026owBrandToAccessYourPhone)");
                    Object[] objArr3 = {PortfolioApp.get.instance().i()};
                    str5 = String.format(a6, Arrays.copyOf(objArr3, objArr3.length));
                    wg6.a((Object) str5, "java.lang.String.format(format, *args)");
                    nh6 nh64 = nh6.a;
                    String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886568);
                    wg6.a((Object) a7, "LanguageHelper.getString\u2026owBrandToAccessYourPhone)");
                    Object[] objArr4 = {PortfolioApp.get.instance().i()};
                    str = String.format(a7, Arrays.copyOf(objArr4, objArr4.length));
                    wg6.a((Object) str, "java.lang.String.format(format, *args)");
                    break;
                case 7:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131887283);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026ing.read_sms_is_required)");
                    str = jm4.a((Context) PortfolioApp.get.instance(), 2131887283);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026ing.read_sms_is_required)");
                    break;
                case 8:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131887285);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026.receive_sms_is_required)");
                    str = jm4.a((Context) PortfolioApp.get.instance(), 2131887285);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026.receive_sms_is_required)");
                    break;
                case 9:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131887284);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026.receive_mms_is_required)");
                    str = jm4.a((Context) PortfolioApp.get.instance(), 2131887284);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026.receive_mms_is_required)");
                    break;
                case 10:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131887225);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026ation_access_is_required)");
                    str = jm4.a((Context) PortfolioApp.get.instance(), 2131887225);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026ation_access_is_required)");
                    break;
                case 11:
                    nh6 nh65 = nh6.a;
                    String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886567);
                    wg6.a((Object) a8, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    Object[] objArr5 = {PortfolioApp.get.instance().i()};
                    str5 = String.format(a8, Arrays.copyOf(objArr5, objArr5.length));
                    wg6.a((Object) str5, "java.lang.String.format(format, *args)");
                    nh6 nh66 = nh6.a;
                    String a9 = jm4.a((Context) PortfolioApp.get.instance(), 2131886567);
                    wg6.a((Object) a9, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    Object[] objArr6 = {PortfolioApp.get.instance().i()};
                    str = String.format(a9, Arrays.copyOf(objArr6, objArr6.length));
                    wg6.a((Object) str, "java.lang.String.format(format, *args)");
                    break;
                case 12:
                    str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886631);
                    wg6.a((Object) str5, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str = jm4.a((Context) PortfolioApp.get.instance(), 2131886631);
                    wg6.a((Object) str, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    break;
                default:
                    str = str5;
                    break;
            }
        } else {
            str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886633);
            wg6.a((Object) str5, "LanguageHelper.getString\u2026t__SmsPermissionRequired)");
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131887057);
            wg6.a((Object) str, "LanguageHelper.getString\u2026call_and_sms_permissions)");
        }
        return new pc6<>(str5, str, str6);
    }

    @DexIgnore
    public final boolean a(Context context, String str, boolean z, boolean z2, boolean z3) {
        Context context2;
        int hashCode;
        String str2 = str;
        wg6.b(str, "feature");
        Context context3 = context;
        WeakReference weakReference = new WeakReference(context);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry next : b.entrySet()) {
            if (wg6.a((Object) (String) next.getKey(), (Object) str)) {
                for (Number intValue : (Iterable) next.getValue()) {
                    int intValue2 = intValue.intValue();
                    if (intValue2 == 100) {
                        arrayList2.add(100);
                        Context context4 = (Context) weakReference.get();
                        if (context4 != null) {
                            for (String str3 : d.a()) {
                                if (!pw6.a(context4, new String[]{str3})) {
                                    arrayList.add(100);
                                }
                            }
                            cd6 cd6 = cd6.a;
                        }
                    } else if (intValue2 != 101) {
                        switch (intValue2) {
                            case 1:
                                arrayList2.add(1);
                                if (BluetoothUtils.isBluetoothEnable()) {
                                    break;
                                } else {
                                    arrayList.add(1);
                                    break;
                                }
                            case 2:
                                arrayList2.add(2);
                                Context context5 = (Context) weakReference.get();
                                if (context5 != null) {
                                    if (!LocationUtils.isLocationPermissionGranted(context5)) {
                                        arrayList.add(2);
                                    }
                                    cd6 cd62 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 3:
                                arrayList2.add(3);
                                Context context6 = (Context) weakReference.get();
                                if (context6 != null) {
                                    if (!LocationUtils.isLocationEnable(context6)) {
                                        arrayList.add(3);
                                    }
                                    cd6 cd63 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 4:
                                arrayList2.add(4);
                                Context context7 = (Context) weakReference.get();
                                if (context7 != null) {
                                    if (!pw6.a(context7, new String[]{"android.permission.READ_CONTACTS"})) {
                                        arrayList.add(4);
                                    }
                                    cd6 cd64 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 5:
                                arrayList2.add(5);
                                Context context8 = (Context) weakReference.get();
                                if (context8 != null) {
                                    if (!pw6.a(context8, new String[]{"android.permission.READ_PHONE_STATE"})) {
                                        arrayList.add(5);
                                    }
                                    cd6 cd65 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 6:
                                arrayList2.add(6);
                                Context context9 = (Context) weakReference.get();
                                if (context9 != null) {
                                    if (!pw6.a(context9, new String[]{"android.permission.READ_CALL_LOG"})) {
                                        arrayList.add(6);
                                    }
                                    cd6 cd66 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 7:
                                arrayList2.add(7);
                                Context context10 = (Context) weakReference.get();
                                if (context10 != null) {
                                    if (!pw6.a(context10, new String[]{"android.permission.READ_SMS"})) {
                                        arrayList.add(7);
                                    }
                                    cd6 cd67 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 8:
                                arrayList2.add(8);
                                Context context11 = (Context) weakReference.get();
                                if (context11 != null) {
                                    if (!pw6.a(context11, new String[]{"android.permission.RECEIVE_SMS"})) {
                                        arrayList.add(8);
                                    }
                                    cd6 cd68 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 9:
                                arrayList2.add(9);
                                Context context12 = (Context) weakReference.get();
                                if (context12 != null) {
                                    if (!pw6.a(context12, new String[]{"android.permission.RECEIVE_MMS"})) {
                                        arrayList.add(9);
                                    }
                                    cd6 cd69 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 10:
                                arrayList2.add(10);
                                if (!PortfolioApp.get.instance().E()) {
                                    if ((hashCode = str.hashCode()) == -1624709818 ? str.equals("NOTIFICATION_APPS") : !(hashCode != 216213615 || !str.equals("SET_MICRO_APP_MUSIC"))) {
                                        an4 an4 = c;
                                        if (an4 == null) {
                                            Boolean.valueOf(arrayList.add(10));
                                            break;
                                        } else {
                                            if (an4.j()) {
                                                arrayList.add(10);
                                            }
                                            cd6 cd610 = cd6.a;
                                            break;
                                        }
                                    } else {
                                        arrayList.add(10);
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            case 11:
                                arrayList2.add(11);
                                Context context13 = (Context) weakReference.get();
                                if (context13 != null) {
                                    if (!pw6.a(context13, new String[]{"android.permission.CAMERA"})) {
                                        arrayList.add(11);
                                    }
                                    cd6 cd611 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                arrayList2.add(12);
                                Context context14 = (Context) weakReference.get();
                                if (context14 != null) {
                                    if (!pw6.a(context14, new String[]{"android.permission.READ_EXTERNAL_STORAGE"})) {
                                        arrayList.add(12);
                                    }
                                    cd6 cd612 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 13:
                                arrayList2.add(13);
                                Context context15 = (Context) weakReference.get();
                                if (context15 != null) {
                                    if (!pw6.a(context15, new String[]{"android.permission.CALL_PHONE"})) {
                                        arrayList.add(13);
                                    }
                                    cd6 cd613 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 14:
                                arrayList2.add(14);
                                Context context16 = (Context) weakReference.get();
                                if (context16 != null) {
                                    if (!pw6.a(context16, new String[]{"android.permission.ANSWER_PHONE_CALLS"})) {
                                        arrayList.add(14);
                                    }
                                    cd6 cd614 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 15:
                                arrayList2.add(15);
                                Context context17 = (Context) weakReference.get();
                                if (context17 != null) {
                                    if (a && !LocationUtils.isBackgroundLocationPermissionGranted(context17)) {
                                        arrayList.add(15);
                                    }
                                    cd6 cd615 = cd6.a;
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } else {
                        arrayList2.add(101);
                        Context context18 = (Context) weakReference.get();
                        if (context18 != null) {
                            for (String str4 : d.b()) {
                                if (!pw6.a(context18, new String[]{str4})) {
                                    arrayList.add(101);
                                }
                            }
                            cd6 cd616 = cd6.a;
                        }
                    }
                }
            }
        }
        if (!(!arrayList.isEmpty())) {
            return true;
        }
        if (z && (context2 = (Context) weakReference.get()) != null) {
            if (z2) {
                PermissionActivity.a aVar = PermissionActivity.C;
                wg6.a((Object) context2, "it");
                PermissionActivity.a.a(aVar, context2, arrayList2, false, 4, (Object) null);
            } else if (z3) {
                PermissionActivity.a aVar2 = PermissionActivity.C;
                wg6.a((Object) context2, "it");
                aVar2.a(context2, arrayList2, true);
            } else {
                PermissionActivity.a aVar3 = PermissionActivity.C;
                wg6.a((Object) context2, "it");
                aVar3.a(context2, arrayList2);
            }
            cd6 cd617 = cd6.a;
        }
        return false;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.get.instance().e())) {
            return qd6.a((T[]) new String[]{"android.permission.READ_SMS", "android.permission.RECEIVE_SMS", "android.permission.RECEIVE_MMS", "android.permission.SEND_SMS"});
        }
        return qd6.a((T[]) new String[]{"android.permission.READ_SMS", "android.permission.RECEIVE_SMS", "android.permission.RECEIVE_MMS"});
    }

    @DexIgnore
    public final String b(String str) {
        if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return "SET_MICRO_APP_MUSIC";
        }
        return wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue()) ? "SET_MICRO_APP_COMMUTE_TIME" : "SET_MICRO_APP";
    }

    @DexIgnore
    public final boolean a(Context context, Set<Integer> set) {
        wg6.b(set, "setOfPerms");
        FLogger.INSTANCE.getLocal().d("PermissionManager", "checkPermsGrantedForPreset");
        WeakReference weakReference = new WeakReference(context);
        ArrayList arrayList = new ArrayList();
        for (Number intValue : set) {
            int intValue2 = intValue.intValue();
            if (intValue2 != 1) {
                if (intValue2 == 2) {
                    Context context2 = (Context) weakReference.get();
                    if (context2 != null && !LocationUtils.isLocationPermissionGranted(context2)) {
                        an4 an4 = c;
                        if (an4 == null) {
                            arrayList.add(2);
                        } else if (an4.i()) {
                            arrayList.add(2);
                        }
                    }
                } else if (intValue2 == 3) {
                    Context context3 = (Context) weakReference.get();
                    if (context3 != null && !LocationUtils.isLocationEnable(context3)) {
                        an4 an42 = c;
                        if (an42 == null) {
                            arrayList.add(3);
                        } else if (an42.i()) {
                            arrayList.add(3);
                        }
                    }
                } else if (intValue2 != 10) {
                    if (intValue2 == 15 && a && !LocationUtils.isBackgroundLocationPermissionGranted(context)) {
                        an4 an43 = c;
                        if (an43 == null) {
                            arrayList.add(15);
                        } else if (an43.i()) {
                            arrayList.add(15);
                        }
                    }
                } else if (!PortfolioApp.get.instance().E()) {
                    an4 an44 = c;
                    if (an44 == null) {
                        arrayList.add(10);
                    } else if (an44.j()) {
                        arrayList.add(10);
                    }
                }
            } else if (!BluetoothUtils.isBluetoothEnable()) {
                arrayList.add(1);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "listPermissionsNotGranted=" + arrayList);
        if (!(!arrayList.isEmpty())) {
            return true;
        }
        Context context4 = (Context) weakReference.get();
        if (context4 == null) {
            return false;
        }
        PermissionActivity.a aVar = PermissionActivity.C;
        wg6.a((Object) context4, "it");
        PermissionActivity.a.a(aVar, context4, arrayList, false, 4, (Object) null);
        return false;
    }

    @DexIgnore
    public final Set<Integer> a(DianaPreset dianaPreset) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add(1);
        if (dianaPreset != null) {
            for (DianaPresetComplicationSetting id : dianaPreset.getComplications()) {
                String id2 = id.getId();
                int hashCode = id2.hashCode();
                if (hashCode != -48173007) {
                    if (hashCode == 1223440372 && id2.equals("weather")) {
                        linkedHashSet.add(2);
                        linkedHashSet.add(3);
                        if (a) {
                            linkedHashSet.add(15);
                        }
                    }
                } else if (id2.equals("chance-of-rain")) {
                    linkedHashSet.add(2);
                    linkedHashSet.add(3);
                    if (a) {
                        linkedHashSet.add(15);
                    }
                }
            }
            for (DianaPresetWatchAppSetting id3 : dianaPreset.getWatchapps()) {
                String id4 = id3.getId();
                int hashCode2 = id4.hashCode();
                if (hashCode2 != -829740640) {
                    if (hashCode2 != 104263205) {
                        if (hashCode2 == 1223440372 && id4.equals("weather")) {
                            linkedHashSet.add(2);
                            linkedHashSet.add(3);
                            if (a) {
                                linkedHashSet.add(15);
                            }
                        }
                    } else if (id4.equals(Constants.MUSIC)) {
                        linkedHashSet.add(10);
                    }
                } else if (id4.equals("commute-time")) {
                    linkedHashSet.add(2);
                    linkedHashSet.add(3);
                    if (a) {
                        linkedHashSet.add(15);
                    }
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "setOfPerms=" + linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public final Set<Integer> a(HybridPreset hybridPreset) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        boolean z = true;
        linkedHashSet.add(1);
        if (Build.VERSION.SDK_INT < 29) {
            z = false;
        }
        if (hybridPreset != null) {
            for (HybridPresetAppSetting appId : hybridPreset.getButtons()) {
                String appId2 = appId.getAppId();
                if (wg6.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                    linkedHashSet.add(10);
                } else if (wg6.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                    linkedHashSet.add(2);
                    linkedHashSet.add(3);
                    if (z) {
                        linkedHashSet.add(15);
                    }
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "setOfPerms=" + linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public final boolean a(Context context, ArrayList<uh4> arrayList) {
        wg6.b(arrayList, "permissionErrorCodes");
        WeakReference weakReference = new WeakReference(context);
        ArrayList arrayList2 = new ArrayList();
        for (uh4 ordinal : arrayList) {
            switch (wm4.a[ordinal.ordinal()]) {
                case 1:
                    arrayList2.add(1);
                    break;
                case 2:
                case 3:
                    arrayList2.add(2);
                    break;
                case 4:
                    arrayList2.add(15);
                    break;
                case 5:
                case 6:
                    arrayList2.add(3);
                    break;
            }
        }
        if (!(!arrayList2.isEmpty())) {
            return false;
        }
        Context context2 = (Context) weakReference.get();
        if (context2 != null) {
            PermissionActivity.a aVar = PermissionActivity.C;
            wg6.a((Object) context2, "it");
            aVar.a(context2, arrayList2);
        }
        return true;
    }

    @DexIgnore
    public final void a(Context context, String str) {
        wg6.b(str, "complicationId");
        a(this, context, a(str), false, false, false, 28, (Object) null);
    }

    @DexIgnore
    public final ArrayList<String> a(int i) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (i == 2) {
            arrayList.add("android.permission.ACCESS_FINE_LOCATION");
        } else if (i == 100) {
            arrayList.addAll(a());
        } else if (i != 101) {
            switch (i) {
                case 4:
                    arrayList.add("android.permission.READ_CONTACTS");
                    break;
                case 5:
                    arrayList.add("android.permission.READ_PHONE_STATE");
                    break;
                case 6:
                    arrayList.add("android.permission.READ_CALL_LOG");
                    break;
                case 7:
                    arrayList.add("android.permission.READ_SMS");
                    break;
                case 8:
                    arrayList.add("android.permission.RECEIVE_SMS");
                    break;
                case 9:
                    arrayList.add("android.permission.RECEIVE_MMS");
                    break;
                default:
                    switch (i) {
                        case 11:
                            arrayList.add("android.permission.CAMERA");
                            break;
                        case 12:
                            arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                            break;
                        case 13:
                            arrayList.add("android.permission.CALL_PHONE");
                            break;
                        case 14:
                            arrayList.add("android.permission.ANSWER_PHONE_CALLS");
                            break;
                        case 15:
                            arrayList.add("android.permission.ACCESS_BACKGROUND_LOCATION");
                            break;
                    }
            }
        } else {
            arrayList.addAll(b());
        }
        return arrayList;
    }

    @DexIgnore
    public final boolean a(Context context, int i) {
        WeakReference weakReference = new WeakReference(context);
        switch (i) {
            case 1:
                return BluetoothUtils.isBluetoothEnable();
            case 2:
                Context context2 = (Context) weakReference.get();
                if (context2 != null) {
                    return LocationUtils.isLocationPermissionGranted(context2);
                }
                break;
            case 3:
                Context context3 = (Context) weakReference.get();
                if (context3 != null) {
                    return LocationUtils.isLocationEnable(context3);
                }
                break;
            case 4:
                Context context4 = (Context) weakReference.get();
                if (context4 != null) {
                    return pw6.a(context4, new String[]{"android.permission.READ_CONTACTS"});
                }
                break;
            case 5:
                Context context5 = (Context) weakReference.get();
                if (context5 != null) {
                    return pw6.a(context5, new String[]{"android.permission.READ_PHONE_STATE"});
                }
                break;
            case 6:
                Context context6 = (Context) weakReference.get();
                if (context6 != null) {
                    return pw6.a(context6, new String[]{"android.permission.READ_CALL_LOG"});
                }
                break;
            case 7:
                Context context7 = (Context) weakReference.get();
                if (context7 != null) {
                    return pw6.a(context7, new String[]{"android.permission.READ_SMS"});
                }
                break;
            case 8:
                Context context8 = (Context) weakReference.get();
                if (context8 != null) {
                    return pw6.a(context8, new String[]{"android.permission.RECEIVE_SMS"});
                }
                break;
            case 9:
                Context context9 = (Context) weakReference.get();
                if (context9 != null) {
                    return pw6.a(context9, new String[]{"android.permission.RECEIVE_MMS"});
                }
                break;
            case 10:
                return PortfolioApp.get.instance().E();
            case 11:
                Context context10 = (Context) weakReference.get();
                if (context10 != null) {
                    return pw6.a(context10, new String[]{"android.permission.CAMERA"});
                }
                break;
            case 12:
                Context context11 = (Context) weakReference.get();
                if (context11 != null) {
                    return pw6.a(context11, new String[]{"android.permission.READ_EXTERNAL_STORAGE"});
                }
                break;
            case 13:
                Context context12 = (Context) weakReference.get();
                if (context12 != null) {
                    return pw6.a(context12, new String[]{"android.permission.CALL_PHONE"});
                }
                break;
            case 14:
                Context context13 = (Context) weakReference.get();
                if (context13 != null) {
                    return pw6.a(context13, new String[]{"android.permission.ANSWER_PHONE_CALLS"});
                }
                break;
            case 15:
                return LocationUtils.isBackgroundLocationPermissionGranted(context);
            default:
                switch (i) {
                    case 100:
                        Context context14 = (Context) weakReference.get();
                        if (context14 == null) {
                            return true;
                        }
                        boolean z = true;
                        for (String str : d.a()) {
                            if (!pw6.a(context14, new String[]{str})) {
                                z = false;
                            }
                        }
                        return z;
                    case 101:
                        Context context15 = (Context) weakReference.get();
                        if (context15 == null) {
                            return true;
                        }
                        boolean z2 = true;
                        for (String str2 : d.b()) {
                            if (!pw6.a(context15, new String[]{str2})) {
                                z2 = false;
                            }
                        }
                        return z2;
                    case 102:
                        Context context16 = (Context) weakReference.get();
                        if (context16 != null) {
                            return pw6.a(context16, new String[]{"android.permission.SEND_SMS"});
                        }
                        break;
                }
        }
        return false;
    }

    @DexIgnore
    public final ArrayList<String> a() {
        if (!DeviceHelper.o.l() || !DeviceHelper.o.f(PortfolioApp.get.instance().e())) {
            return qd6.a((T[]) new String[]{"android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG"});
        }
        return qd6.a((T[]) new String[]{"android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG", "android.permission.CALL_PHONE", "android.permission.ANSWER_PHONE_CALLS"});
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007d A[RETURN, SYNTHETIC] */
    public final String a(String str) {
        switch (str.hashCode()) {
            case -829740640:
                return str.equals("commute-time") ? "SET_WATCH_APP_COMMUTE_TIME" : "SET_WATCH_APP";
            case -331239923:
                if (str.equals(Constants.BATTERY)) {
                    return "SET_COMPLICATION";
                }
                return "SET_WATCH_APP";
            case -168965370:
                if (!str.equals(Constants.CALORIES)) {
                    return "SET_WATCH_APP";
                }
                break;
            case -85386984:
                if (!str.equals("active-minutes")) {
                    return "SET_WATCH_APP";
                }
                break;
            case -48173007:
                if (str.equals("chance-of-rain")) {
                    return "SET_COMPLICATION_CHANCE_OF_RAIN";
                }
                return "SET_WATCH_APP";
            case 3076014:
                if (!str.equals(HardwareLog.COLUMN_DATE)) {
                    return "SET_WATCH_APP";
                }
                break;
            case 96634189:
                if (!str.equals("empty")) {
                    return "SET_WATCH_APP";
                }
                break;
            case 104263205:
                if (str.equals(Constants.MUSIC)) {
                    return "SET_WATCH_APP_MUSIC";
                }
                return "SET_WATCH_APP";
            case 109761319:
                if (!str.equals("steps")) {
                    return "SET_WATCH_APP";
                }
                break;
            case 134170930:
                if (!str.equals("second-timezone")) {
                    return "SET_WATCH_APP";
                }
                break;
            case 1223440372:
                if (str.equals("weather")) {
                    return "SET_COMPLICATION_WEATHER";
                }
                if (str.equals("weather")) {
                    return "SET_WATCH_APP_WEATHER";
                }
                return "SET_WATCH_APP";
            case 1884273159:
                if (!str.equals("heart-rate")) {
                    return "SET_WATCH_APP";
                }
                break;
            default:
                return "SET_WATCH_APP";
        }
        return "SET_COMPLICATION";
    }
}
