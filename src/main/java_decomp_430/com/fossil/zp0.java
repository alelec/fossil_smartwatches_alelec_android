package com.fossil;

import com.fossil.crypto.EllipticCurveKeyPair;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp0 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.AUTHENTICATION}));
    @DexIgnore
    public byte[] C;
    @DexIgnore
    public /* final */ EllipticCurveKeyPair D;
    @DexIgnore
    public /* final */ byte[] E;

    @DexIgnore
    public zp0(ue1 ue1, q41 q41, byte[] bArr) {
        super(ue1, q41, eh1.EXCHANGE_SECRET_KEY, (String) null, 8);
        this.E = bArr;
        EllipticCurveKeyPair create = EllipticCurveKeyPair.create();
        wg6.a(create, "EllipticCurveKeyPair.create()");
        this.D = create;
    }

    @DexIgnore
    public static final /* synthetic */ void a(zp0 zp0) {
        ue1 ue1 = zp0.w;
        byte[] publicKey = zp0.D.publicKey();
        wg6.a(publicKey, "keyPair.publicKey()");
        if1.a((if1) zp0, (qv0) new hj1(ue1, publicKey), (hg6) new cj0(zp0), (hg6) new wk0(zp0), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public Object d() {
        byte[] bArr = this.C;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public void h() {
        byte[] bArr = this.E;
        if (bArr.length != 16) {
            a(sk1.INVALID_PARAMETER);
            return;
        }
        if1.a((if1) this, (qv0) new bl1(this.w, nq0.PRE_SHARED_KEY, bArr), (hg6) new pm0(this), (hg6) new ho0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.BOTH_SIDES_RANDOM_NUMBERS, (Object) cw0.a(this.E, (String) null, 1));
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        byte[] bArr = this.C;
        if (bArr != null) {
            cw0.a(k, bm0.SECRET_KEY_CRC, (Object) Long.valueOf(h51.a.a(bArr, q11.CRC32)));
        }
        return k;
    }

    @DexIgnore
    public final void m() {
        byte[] bArr = this.C;
        if (bArr != null) {
            if1.a((if1) this, (if1) new u51(this.w, this.x, mi0.A.e(), md6.a(bArr, 0, 16), this.z), (hg6) tf0.a, (hg6) kh0.a, (ig6) null, (hg6) new wn1(this), (hg6) null, 40, (Object) null);
            return;
        }
        a(km1.a(this.v, (eh1) null, sk1.FLOW_BROKEN, (bn0) null, 5));
    }
}
