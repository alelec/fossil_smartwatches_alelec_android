package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tf3 {
    @DexIgnore
    public static /* final */ int abc_action_bar_title_item; // = 2131558400;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_container; // = 2131558401;
    @DexIgnore
    public static /* final */ int abc_action_menu_item_layout; // = 2131558402;
    @DexIgnore
    public static /* final */ int abc_action_menu_layout; // = 2131558403;
    @DexIgnore
    public static /* final */ int abc_action_mode_bar; // = 2131558404;
    @DexIgnore
    public static /* final */ int abc_action_mode_close_item_material; // = 2131558405;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view; // = 2131558406;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_list_item; // = 2131558407;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_bar_material; // = 2131558408;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_material; // = 2131558409;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_title_material; // = 2131558410;
    @DexIgnore
    public static /* final */ int abc_cascading_menu_item_layout; // = 2131558411;
    @DexIgnore
    public static /* final */ int abc_dialog_title_material; // = 2131558412;
    @DexIgnore
    public static /* final */ int abc_expanded_menu_layout; // = 2131558413;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_checkbox; // = 2131558414;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_icon; // = 2131558415;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_layout; // = 2131558416;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_radio; // = 2131558417;
    @DexIgnore
    public static /* final */ int abc_popup_menu_header_item_layout; // = 2131558418;
    @DexIgnore
    public static /* final */ int abc_popup_menu_item_layout; // = 2131558419;
    @DexIgnore
    public static /* final */ int abc_screen_content_include; // = 2131558420;
    @DexIgnore
    public static /* final */ int abc_screen_simple; // = 2131558421;
    @DexIgnore
    public static /* final */ int abc_screen_simple_overlay_action_mode; // = 2131558422;
    @DexIgnore
    public static /* final */ int abc_screen_toolbar; // = 2131558423;
    @DexIgnore
    public static /* final */ int abc_search_dropdown_item_icons_2line; // = 2131558424;
    @DexIgnore
    public static /* final */ int abc_search_view; // = 2131558425;
    @DexIgnore
    public static /* final */ int abc_select_dialog_material; // = 2131558426;
    @DexIgnore
    public static /* final */ int abc_tooltip; // = 2131558427;
    @DexIgnore
    public static /* final */ int custom_dialog; // = 2131558462;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_item; // = 2131558465;
    @DexIgnore
    public static /* final */ int design_bottom_sheet_dialog; // = 2131558466;
    @DexIgnore
    public static /* final */ int design_layout_snackbar; // = 2131558467;
    @DexIgnore
    public static /* final */ int design_layout_snackbar_include; // = 2131558468;
    @DexIgnore
    public static /* final */ int design_layout_tab_icon; // = 2131558469;
    @DexIgnore
    public static /* final */ int design_layout_tab_text; // = 2131558470;
    @DexIgnore
    public static /* final */ int design_menu_item_action_area; // = 2131558471;
    @DexIgnore
    public static /* final */ int design_navigation_item; // = 2131558472;
    @DexIgnore
    public static /* final */ int design_navigation_item_header; // = 2131558473;
    @DexIgnore
    public static /* final */ int design_navigation_item_separator; // = 2131558474;
    @DexIgnore
    public static /* final */ int design_navigation_item_subheader; // = 2131558475;
    @DexIgnore
    public static /* final */ int design_navigation_menu; // = 2131558476;
    @DexIgnore
    public static /* final */ int design_navigation_menu_item; // = 2131558477;
    @DexIgnore
    public static /* final */ int design_text_input_end_icon; // = 2131558478;
    @DexIgnore
    public static /* final */ int design_text_input_start_icon; // = 2131558479;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog; // = 2131558706;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_actions; // = 2131558707;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_title; // = 2131558708;
    @DexIgnore
    public static /* final */ int mtrl_alert_select_dialog_item; // = 2131558709;
    @DexIgnore
    public static /* final */ int mtrl_alert_select_dialog_multichoice; // = 2131558710;
    @DexIgnore
    public static /* final */ int mtrl_alert_select_dialog_singlechoice; // = 2131558711;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day; // = 2131558712;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_of_week; // = 2131558713;
    @DexIgnore
    public static /* final */ int mtrl_calendar_days_of_week; // = 2131558714;
    @DexIgnore
    public static /* final */ int mtrl_calendar_horizontal; // = 2131558715;
    @DexIgnore
    public static /* final */ int mtrl_calendar_month; // = 2131558716;
    @DexIgnore
    public static /* final */ int mtrl_calendar_month_labeled; // = 2131558717;
    @DexIgnore
    public static /* final */ int mtrl_calendar_month_navigation; // = 2131558718;
    @DexIgnore
    public static /* final */ int mtrl_calendar_months; // = 2131558719;
    @DexIgnore
    public static /* final */ int mtrl_calendar_vertical; // = 2131558720;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year; // = 2131558721;
    @DexIgnore
    public static /* final */ int mtrl_layout_snackbar; // = 2131558722;
    @DexIgnore
    public static /* final */ int mtrl_layout_snackbar_include; // = 2131558723;
    @DexIgnore
    public static /* final */ int mtrl_picker_actions; // = 2131558724;
    @DexIgnore
    public static /* final */ int mtrl_picker_dialog; // = 2131558725;
    @DexIgnore
    public static /* final */ int mtrl_picker_fullscreen; // = 2131558726;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_dialog; // = 2131558727;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_fullscreen; // = 2131558728;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_selection_text; // = 2131558729;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_title_text; // = 2131558730;
    @DexIgnore
    public static /* final */ int mtrl_picker_header_toggle; // = 2131558731;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date; // = 2131558732;
    @DexIgnore
    public static /* final */ int mtrl_picker_text_input_date_range; // = 2131558733;
    @DexIgnore
    public static /* final */ int notification_action; // = 2131558734;
    @DexIgnore
    public static /* final */ int notification_action_tombstone; // = 2131558735;
    @DexIgnore
    public static /* final */ int notification_template_custom_big; // = 2131558743;
    @DexIgnore
    public static /* final */ int notification_template_icon_group; // = 2131558744;
    @DexIgnore
    public static /* final */ int notification_template_part_chronometer; // = 2131558748;
    @DexIgnore
    public static /* final */ int notification_template_part_time; // = 2131558749;
    @DexIgnore
    public static /* final */ int select_dialog_item_material; // = 2131558779;
    @DexIgnore
    public static /* final */ int select_dialog_multichoice_material; // = 2131558780;
    @DexIgnore
    public static /* final */ int select_dialog_singlechoice_material; // = 2131558781;
    @DexIgnore
    public static /* final */ int support_simple_spinner_dropdown_item; // = 2131558783;
    @DexIgnore
    public static /* final */ int test_action_chip; // = 2131558784;
    @DexIgnore
    public static /* final */ int test_design_checkbox; // = 2131558785;
    @DexIgnore
    public static /* final */ int test_reflow_chipgroup; // = 2131558786;
    @DexIgnore
    public static /* final */ int test_toolbar; // = 2131558787;
    @DexIgnore
    public static /* final */ int test_toolbar_custom_background; // = 2131558788;
    @DexIgnore
    public static /* final */ int test_toolbar_elevation; // = 2131558789;
    @DexIgnore
    public static /* final */ int test_toolbar_surface; // = 2131558790;
    @DexIgnore
    public static /* final */ int text_view_with_line_height_from_appearance; // = 2131558791;
    @DexIgnore
    public static /* final */ int text_view_with_line_height_from_layout; // = 2131558792;
    @DexIgnore
    public static /* final */ int text_view_with_line_height_from_style; // = 2131558793;
    @DexIgnore
    public static /* final */ int text_view_with_theme_line_height; // = 2131558794;
    @DexIgnore
    public static /* final */ int text_view_without_line_height; // = 2131558795;
}
