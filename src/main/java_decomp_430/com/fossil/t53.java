package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface t53 {
    @DexIgnore
    BroadcastReceiver.PendingResult a();

    @DexIgnore
    void a(Context context, Intent intent);
}
