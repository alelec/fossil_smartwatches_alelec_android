package com.fossil;

import android.database.Cursor;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ii extends Closeable {
    @DexIgnore
    boolean A();

    @DexIgnore
    Cursor a(li liVar);

    @DexIgnore
    Cursor a(li liVar, CancellationSignal cancellationSignal);

    @DexIgnore
    void b(String str) throws SQLException;

    @DexIgnore
    mi c(String str);

    @DexIgnore
    Cursor d(String str);

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    void u();

    @DexIgnore
    List<Pair<String, String>> v();

    @DexIgnore
    void x();

    @DexIgnore
    void y();

    @DexIgnore
    String z();
}
