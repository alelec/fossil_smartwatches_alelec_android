package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum g11 {
    SUCCESS(0),
    NOT_START(1),
    TIMEOUT(4),
    GATT_ERROR(7),
    BLUETOOTH_OFF(10),
    INTERRUPTED(254);
    
    @DexIgnore
    public static /* final */ mz0 h; // = null;

    /*
    static {
        h = new mz0((qg6) null);
    }
    */

    @DexIgnore
    public g11(int i) {
    }
}
