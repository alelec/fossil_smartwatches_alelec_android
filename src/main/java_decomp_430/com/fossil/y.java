package com.fossil;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y {
    @DexIgnore
    public CopyOnWriteArrayList<x> mCancellables; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public boolean mEnabled;

    @DexIgnore
    public y(boolean z) {
        this.mEnabled = z;
    }

    @DexIgnore
    public void addCancellable(x xVar) {
        this.mCancellables.add(xVar);
    }

    @DexIgnore
    public abstract void handleOnBackPressed();

    @DexIgnore
    public final boolean isEnabled() {
        return this.mEnabled;
    }

    @DexIgnore
    public final void remove() {
        Iterator<x> it = this.mCancellables.iterator();
        while (it.hasNext()) {
            it.next().cancel();
        }
    }

    @DexIgnore
    public void removeCancellable(x xVar) {
        this.mCancellables.remove(xVar);
    }

    @DexIgnore
    public final void setEnabled(boolean z) {
        this.mEnabled = z;
    }
}
