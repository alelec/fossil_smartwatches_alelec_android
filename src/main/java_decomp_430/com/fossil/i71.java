package com.fossil;

import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i71 extends em0 {
    @DexIgnore
    public static /* final */ l51 CREATOR; // = new l51((qg6) null);

    @DexIgnore
    public i71(byte b) {
        super(xh0.HEARTBEAT_EVENT, b);
    }

    @DexIgnore
    public byte[] b() {
        return new byte[]{wp0.FOSSIL.a};
    }

    @DexIgnore
    public /* synthetic */ i71(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
