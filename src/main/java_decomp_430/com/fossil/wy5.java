package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import com.fossil.az5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wy5 {
    @DexIgnore
    public static /* final */ String a; // = "wy5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ zy5 c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ xy5 e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wy5$a$a")
        /* renamed from: com.fossil.wy5$a$a  reason: collision with other inner class name */
        public class C0053a implements az5.b {
            @DexIgnore
            public /* final */ /* synthetic */ ImageView a;

            @DexIgnore
            public C0053a(ImageView imageView) {
                this.a = imageView;
            }

            @DexIgnore
            public void a(BitmapDrawable bitmapDrawable) {
                xy5 xy5 = a.this.e;
                if (xy5 == null) {
                    this.a.setImageDrawable(bitmapDrawable);
                } else {
                    xy5.a(bitmapDrawable);
                }
            }
        }

        @DexIgnore
        public a(Context context, Bitmap bitmap, zy5 zy5, boolean z, xy5 xy5) {
            this.a = context;
            this.b = bitmap;
            this.c = zy5;
            this.d = z;
            this.e = xy5;
        }

        @DexIgnore
        public void a(ImageView imageView) {
            this.c.a = this.b.getWidth();
            this.c.b = this.b.getHeight();
            if (this.d) {
                new az5(imageView.getContext(), this.b, this.c, new C0053a(imageView)).a();
            } else {
                imageView.setImageDrawable(new BitmapDrawable(this.a.getResources(), vy5.a(imageView.getContext(), this.b, this.c)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ zy5 c; // = new zy5();
        @DexIgnore
        public boolean d;
        @DexIgnore
        public xy5 e;

        @DexIgnore
        public b(Context context) {
            this.b = context;
            this.a = new View(context);
            this.a.setTag(wy5.a);
        }

        @DexIgnore
        public b a(int i) {
            this.c.c = i;
            return this;
        }

        @DexIgnore
        public b b(int i) {
            this.c.d = i;
            return this;
        }

        @DexIgnore
        public a a(Bitmap bitmap) {
            return new a(this.b, bitmap, this.c, this.d, this.e);
        }
    }

    @DexIgnore
    public static b a(Context context) {
        return new b(context);
    }
}
