package com.fossil;

import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {64}, m = "invokeSuspend")
public final class rf5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $data;
    @DexIgnore
    public /* final */ /* synthetic */ yx5 $it;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Calendar $calendar;
        @DexIgnore
        public /* final */ /* synthetic */ TreeMap $map;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ rf5$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(rf5$d$a rf5_d_a, Calendar calendar, TreeMap treeMap, xe6 xe6) {
            super(2, xe6);
            this.this$0 = rf5_d_a;
            this.$calendar = calendar;
            this.$map = treeMap;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$calendar, this.$map, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Integer a;
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                for (DailyHeartRateSummary dailyHeartRateSummary : (List) this.this$0.$it.d()) {
                    Calendar calendar = this.$calendar;
                    wg6.a((Object) calendar, "calendar");
                    calendar.setTime(dailyHeartRateSummary.getDate());
                    int i = 0;
                    this.$calendar.set(14, 0);
                    TreeMap treeMap = this.$map;
                    Calendar calendar2 = this.$calendar;
                    wg6.a((Object) calendar2, "calendar");
                    Long a2 = hf6.a(calendar2.getTimeInMillis());
                    Resting resting = dailyHeartRateSummary.getResting();
                    if (!(resting == null || (a = hf6.a(resting.getValue())) == null)) {
                        i = a.intValue();
                    }
                    treeMap.put(a2, hf6.a(i));
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rf5$d$a(HeartRateOverviewMonthPresenter.d dVar, List list, yx5 yx5, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$data = list;
        this.$it = yx5;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        rf5$d$a rf5_d_a = new rf5$d$a(this.this$0, this.$data, this.$it, xe6);
        rf5_d_a.p$ = (il6) obj;
        return rf5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((rf5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        TreeMap treeMap;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            this.this$0.a.k = this.$data;
            TreeMap treeMap2 = new TreeMap();
            Calendar instance = Calendar.getInstance();
            dl6 a3 = this.this$0.a.b();
            a aVar = new a(this, instance, treeMap2, (xe6) null);
            this.L$0 = il6;
            this.L$1 = treeMap2;
            this.L$2 = instance;
            this.label = 1;
            if (gk6.a(a3, aVar, this) == a2) {
                return a2;
            }
            treeMap = treeMap2;
        } else if (i == 1) {
            Calendar calendar = (Calendar) this.L$2;
            treeMap = (TreeMap) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.a.m.a(treeMap);
        return cd6.a;
    }
}
