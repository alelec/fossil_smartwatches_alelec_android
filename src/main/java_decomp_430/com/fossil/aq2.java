package com.fossil;

import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq2 implements ListIterator<String> {
    @DexIgnore
    public ListIterator<String> a; // = this.c.a.listIterator(this.b);
    @DexIgnore
    public /* final */ /* synthetic */ int b;
    @DexIgnore
    public /* final */ /* synthetic */ bq2 c;

    @DexIgnore
    public aq2(bq2 bq2, int i) {
        this.c = bq2;
        this.b = i;
    }

    @DexIgnore
    public final /* synthetic */ void add(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.a.hasPrevious();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.a.next();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.a.nextIndex();
    }

    @DexIgnore
    public final /* synthetic */ Object previous() {
        return this.a.previous();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.a.previousIndex();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final /* synthetic */ void set(Object obj) {
        String str = (String) obj;
        throw new UnsupportedOperationException();
    }
}
