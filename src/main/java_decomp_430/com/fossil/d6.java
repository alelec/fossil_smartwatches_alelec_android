package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d6 {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968745;
    @DexIgnore
    public static /* final */ int font; // = 2130969326;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969331;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969332;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969333;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969334;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969335;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969336;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969337;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969338;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969339;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130970059;
}
