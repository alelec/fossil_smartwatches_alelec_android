package com.fossil;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.model.request.CustomField;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws4 extends m24<c, d, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ long h; // = (wg6.a((Object) "release", (Object) "release") ? 25044663 : 360000146526L);
    @DexIgnore
    public static /* final */ long i; // = (wg6.a((Object) "release", (Object) "release") ? 23451689 : 24435966);
    @DexIgnore
    public static /* final */ long j; // = (wg6.a((Object) "release", (Object) "release") ? 360000024083L : 360000148503L);
    @DexIgnore
    public static /* final */ long k; // = (wg6.a((Object) "release", (Object) "release") ? 24416029 : 24436006);
    @DexIgnore
    public static /* final */ long l; // = (wg6.a((Object) "release", (Object) "release") ? 24504683 : 24881463);
    @DexIgnore
    public static /* final */ long m; // = (wg6.a((Object) "release", (Object) "release") ? 23777683 : 24435986);
    @DexIgnore
    public static /* final */ long n; // = (wg6.a((Object) "release", (Object) "release") ? 24545186 : 24881503);
    @DexIgnore
    public static /* final */ long o; // = (wg6.a((Object) "release", (Object) "release") ? 24545246 : 24945726);
    @DexIgnore
    public static /* final */ long p; // = (wg6.a((Object) "release", (Object) "release") ? 23780847 : 24436086);
    @DexIgnore
    public static /* final */ long q; // = (wg6.a((Object) "release", (Object) "release") ? 24935086 : 24881543);
    @DexIgnore
    public static /* final */ long r; // = (wg6.a((Object) "release", (Object) "release") ? 24506223 : 24881523);
    @DexIgnore
    public static /* final */ long s; // = (wg6.a((Object) "release", (Object) "release") ? 360000156783L : 360000156723L);
    @DexIgnore
    public static /* final */ long t; // = (wg6.a((Object) "release", (Object) "release") ? 360000156803L : 360000156743L);
    @DexIgnore
    public static /* final */ long u; // = (wg6.a((Object) "release", (Object) "release") ? 360000156823L : 360000156763L);
    @DexIgnore
    public static /* final */ long v; // = (wg6.a((Object) "release", (Object) "release") ? 60000157103L : 360000148563L);
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ an4 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            wg6.b(str, "subject");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<CustomField> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ String f;

        @DexIgnore
        public d(List<CustomField> list, List<String> list2, String str, String str2, String str3, String str4) {
            wg6.b(list, "customFieldList");
            wg6.b(list2, "tags");
            wg6.b(str, "email");
            wg6.b(str2, "userName");
            wg6.b(str3, "subject");
            wg6.b(str4, "additionalInfo");
            this.a = list;
            this.b = list2;
            this.c = str;
            this.d = str2;
            this.e = str3;
            this.f = str4;
        }

        @DexIgnore
        public final String a() {
            return this.f;
        }

        @DexIgnore
        public final List<CustomField> b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final List<String> e() {
            return this.b;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ws4.class.getSimpleName();
        wg6.a((Object) simpleName, "GetZendeskInformation::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public ws4(UserRepository userRepository, DeviceRepository deviceRepository, an4 an4) {
        wg6.b(userRepository, "mUserRepository");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = deviceRepository;
        this.f = an4;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.portfolio.platform.CoroutineUseCase} */
    /* JADX WARNING: type inference failed for: r7v0, types: [com.fossil.ws4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0096 A[SYNTHETIC, Splitter:B:28:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ba A[SYNTHETIC, Splitter:B:39:0x00ba] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0140  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x027d  */
    public Object a(c cVar, xe6<? super cd6> xe6) {
        CustomField customField;
        String str;
        CustomField customField2;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        Object systemService;
        String str8;
        String str9;
        String str10;
        MFUser currentUser = this.d.getCurrentUser();
        if (currentUser == null) {
            Object a2 = a(new b());
            if (a2 == ff6.a()) {
                return a2;
            }
            return cd6.a;
        }
        Device deviceBySerial = this.e.getDeviceBySerial(PortfolioApp.get.instance().e());
        Object instance = PortfolioApp.get.instance();
        CustomField customField3 = new CustomField(hf6.a(h), "");
        CustomField customField4 = new CustomField(hf6.a(i), currentUser.getEmail());
        List d2 = qd6.d("android", "app_feedback");
        try {
            String packageName = instance.getPackageName();
            PackageManager packageManager = instance.getPackageManager();
            PackageInfo packageInfo = packageManager != null ? packageManager.getPackageInfo(packageName, 0) : null;
            if (packageInfo == null || (str = packageInfo.versionName) == null) {
                str = "";
            }
            try {
                customField = new CustomField(hf6.a(k), str);
                if (packageManager != null) {
                    try {
                        CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0));
                        if (applicationLabel != null) {
                            str2 = applicationLabel.toString();
                            if (str2 == null) {
                                try {
                                    customField2 = new CustomField(hf6.a(j), str2);
                                } catch (Exception unused) {
                                    customField2 = null;
                                    FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                                    CustomField customField5 = new CustomField(hf6.a(l), "android");
                                    CustomField customField6 = new CustomField(hf6.a(m), Build.VERSION.RELEASE);
                                    Long a3 = hf6.a(n);
                                    str3 = Build.MANUFACTURER;
                                    if (str3 == null) {
                                    }
                                    CustomField customField7 = new CustomField(a3, str4);
                                    CustomField customField8 = new CustomField(hf6.a(o), Build.MODEL);
                                    str5 = Build.MANUFACTURER;
                                    if (str5 == null) {
                                    }
                                    String str11 = Build.MODEL;
                                    List list = d2;
                                    MFUser mFUser = currentUser;
                                    Long a4 = hf6.a(p);
                                    if (deviceBySerial == null) {
                                    }
                                    CustomField customField9 = new CustomField(a4, str7);
                                    systemService = PortfolioApp.get.instance().getSystemService("phone");
                                    if (systemService == null) {
                                    }
                                }
                                try {
                                    String a5 = xj6.a(str2, ' ', '_', false, 4, (Object) null);
                                    if (a5 == null) {
                                        a5 = "";
                                    }
                                    d2.add(a5);
                                } catch (Exception unused2) {
                                    FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                                    CustomField customField52 = new CustomField(hf6.a(l), "android");
                                    CustomField customField62 = new CustomField(hf6.a(m), Build.VERSION.RELEASE);
                                    Long a32 = hf6.a(n);
                                    str3 = Build.MANUFACTURER;
                                    if (str3 == null) {
                                    }
                                    CustomField customField72 = new CustomField(a32, str4);
                                    CustomField customField82 = new CustomField(hf6.a(o), Build.MODEL);
                                    str5 = Build.MANUFACTURER;
                                    if (str5 == null) {
                                    }
                                    String str112 = Build.MODEL;
                                    List list2 = d2;
                                    MFUser mFUser2 = currentUser;
                                    Long a42 = hf6.a(p);
                                    if (deviceBySerial == null) {
                                    }
                                    CustomField customField92 = new CustomField(a42, str7);
                                    systemService = PortfolioApp.get.instance().getSystemService("phone");
                                    if (systemService == null) {
                                    }
                                }
                                CustomField customField522 = new CustomField(hf6.a(l), "android");
                                CustomField customField622 = new CustomField(hf6.a(m), Build.VERSION.RELEASE);
                                Long a322 = hf6.a(n);
                                str3 = Build.MANUFACTURER;
                                if (str3 == null) {
                                    str4 = str3;
                                } else {
                                    str4 = "";
                                }
                                CustomField customField722 = new CustomField(a322, str4);
                                CustomField customField822 = new CustomField(hf6.a(o), Build.MODEL);
                                str5 = Build.MANUFACTURER;
                                if (str5 == null) {
                                    str5 = "";
                                }
                                String str1122 = Build.MODEL;
                                List list22 = d2;
                                MFUser mFUser22 = currentUser;
                                Long a422 = hf6.a(p);
                                if (deviceBySerial == null) {
                                    str6 = str1122;
                                    str7 = deviceBySerial.getDeviceId();
                                } else {
                                    str6 = str1122;
                                    str7 = null;
                                }
                                CustomField customField922 = new CustomField(a422, str7);
                                systemService = PortfolioApp.get.instance().getSystemService("phone");
                                if (systemService == null) {
                                    TelephonyManager telephonyManager = (TelephonyManager) systemService;
                                    String str12 = str5;
                                    String str13 = str;
                                    CustomField customField10 = new CustomField(hf6.a(q), telephonyManager.getNetworkOperatorName());
                                    Long a6 = hf6.a(r);
                                    if (deviceBySerial != null) {
                                        str8 = str2;
                                        str9 = deviceBySerial.getFirmwareRevision();
                                    } else {
                                        str8 = str2;
                                        str9 = null;
                                    }
                                    CustomField customField11 = new CustomField(a6, str9);
                                    Locale locale = Locale.getDefault();
                                    String deviceId = deviceBySerial != null ? deviceBySerial.getDeviceId() : null;
                                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                                    Long a7 = hf6.a(s);
                                    String str14 = deviceId;
                                    StringBuilder sb = new StringBuilder();
                                    CustomField customField12 = customField2;
                                    wg6.a((Object) locale, "currentLocale");
                                    sb.append(locale.getLanguage());
                                    sb.append('-');
                                    sb.append(locale.getScript());
                                    String str15 = "";
                                    List d3 = qd6.d(customField3, customField4, customField522, customField622, customField722, customField822, customField922, customField10, customField11, new CustomField(a7, sb.toString()), new CustomField(hf6.a(t), locale.getCountry()), new CustomField(hf6.a(u), ""), new CustomField(hf6.a(v), ButtonService.Companion.getSDKVersion()));
                                    if (customField != null) {
                                        hf6.a(d3.add(customField));
                                    }
                                    if (customField12 != null) {
                                        hf6.a(d3.add(customField12));
                                    }
                                    StringBuilder a8 = a(str8, str13, str14, str12, str6, networkOperatorName);
                                    if (cVar == null || (str10 = cVar.a()) == null) {
                                        str10 = "Feedback - From app [Fossil] - [Android]";
                                    }
                                    String str16 = str10;
                                    String email = mFUser22.getEmail();
                                    String str17 = email != null ? email : str15;
                                    String username = mFUser22.getUsername();
                                    String str18 = username != null ? username : str15;
                                    String sb2 = a8.toString();
                                    wg6.a((Object) sb2, "additionalInfo.toString()");
                                    Object a9 = a(new d(d3, list22, str17, str18, str16, sb2));
                                    if (a9 == ff6.a()) {
                                        return a9;
                                    }
                                    return cd6.a;
                                }
                                throw new rc6("null cannot be cast to non-null type android.telephony.TelephonyManager");
                            }
                            wg6.a();
                            throw null;
                        }
                    } catch (Exception unused3) {
                        customField2 = null;
                        str2 = "";
                        FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                        CustomField customField5222 = new CustomField(hf6.a(l), "android");
                        CustomField customField6222 = new CustomField(hf6.a(m), Build.VERSION.RELEASE);
                        Long a3222 = hf6.a(n);
                        str3 = Build.MANUFACTURER;
                        if (str3 == null) {
                        }
                        CustomField customField7222 = new CustomField(a3222, str4);
                        CustomField customField8222 = new CustomField(hf6.a(o), Build.MODEL);
                        str5 = Build.MANUFACTURER;
                        if (str5 == null) {
                        }
                        String str11222 = Build.MODEL;
                        List list222 = d2;
                        MFUser mFUser222 = currentUser;
                        Long a4222 = hf6.a(p);
                        if (deviceBySerial == null) {
                        }
                        CustomField customField9222 = new CustomField(a4222, str7);
                        systemService = PortfolioApp.get.instance().getSystemService("phone");
                        if (systemService == null) {
                        }
                    }
                }
                str2 = null;
                if (str2 == null) {
                }
            } catch (Exception unused4) {
                customField2 = null;
                customField = null;
                str2 = "";
                FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                CustomField customField52222 = new CustomField(hf6.a(l), "android");
                CustomField customField62222 = new CustomField(hf6.a(m), Build.VERSION.RELEASE);
                Long a32222 = hf6.a(n);
                str3 = Build.MANUFACTURER;
                if (str3 == null) {
                }
                CustomField customField72222 = new CustomField(a32222, str4);
                CustomField customField82222 = new CustomField(hf6.a(o), Build.MODEL);
                str5 = Build.MANUFACTURER;
                if (str5 == null) {
                }
                String str112222 = Build.MODEL;
                List list2222 = d2;
                MFUser mFUser2222 = currentUser;
                Long a42222 = hf6.a(p);
                if (deviceBySerial == null) {
                }
                CustomField customField92222 = new CustomField(a42222, str7);
                systemService = PortfolioApp.get.instance().getSystemService("phone");
                if (systemService == null) {
                }
            }
        } catch (Exception unused5) {
            customField2 = null;
            customField = null;
            str2 = "";
            str = str2;
            FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
            CustomField customField522222 = new CustomField(hf6.a(l), "android");
            CustomField customField622222 = new CustomField(hf6.a(m), Build.VERSION.RELEASE);
            Long a322222 = hf6.a(n);
            str3 = Build.MANUFACTURER;
            if (str3 == null) {
            }
            CustomField customField722222 = new CustomField(a322222, str4);
            CustomField customField822222 = new CustomField(hf6.a(o), Build.MODEL);
            str5 = Build.MANUFACTURER;
            if (str5 == null) {
            }
            String str1122222 = Build.MODEL;
            List list22222 = d2;
            MFUser mFUser22222 = currentUser;
            Long a422222 = hf6.a(p);
            if (deviceBySerial == null) {
            }
            CustomField customField922222 = new CustomField(a422222, str7);
            systemService = PortfolioApp.get.instance().getSystemService("phone");
            if (systemService == null) {
            }
        }
    }

    @DexIgnore
    public final StringBuilder a(String str, String str2, String str3, String str4, String str5, String str6) {
        StringBuilder sb = new StringBuilder();
        String str7 = Build.DEVICE + " - " + Build.MODEL;
        String str8 = Build.VERSION.RELEASE;
        StringBuilder sb2 = new StringBuilder();
        Locale locale = Locale.getDefault();
        wg6.a((Object) locale, "Locale.getDefault()");
        sb2.append(locale.getLanguage());
        sb2.append("_t");
        String sb3 = sb2.toString();
        String str9 = "";
        String str10 = str9;
        String str11 = str10;
        for (Device device : this.e.getAllDevice()) {
            str10 = str10 + " " + device.getDeviceId();
            if (device.isActive()) {
                long g2 = this.f.g(device.getDeviceId());
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str11);
                nh6 nh6 = nh6.a;
                Object[] objArr = {Long.valueOf(g2 / ((long) 1000)), bk4.c(new Date(g2))};
                String format = String.format(" %d (%s)", Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                sb4.append(format);
                str11 = sb4.toString();
            }
        }
        sb.append("App Name: " + str);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("App Version: " + str2);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Build number: " + "25287-2020-03-24");
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Phone Info: " + str7);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("System version: " + str8);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Host Maker: " + str4);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Host Model: " + str5);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Carrier: " + str6);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Language code: " + sb3);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("_______________");
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Serial ");
        if (str3 != null) {
            str9 = str3;
        }
        sb5.append(str9);
        sb.append(sb5.toString());
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("Last successful sync: " + str11);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        sb.append("List of paired device: " + str10);
        wg6.a((Object) sb, "append(value)");
        tj6.a(sb);
        return sb;
    }
}
