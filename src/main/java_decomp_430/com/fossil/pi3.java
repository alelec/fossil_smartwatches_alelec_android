package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pi3 {
    @DexIgnore
    public static ColorStateList a(Context context, TypedArray typedArray, int i) {
        int color;
        int resourceId;
        ColorStateList b;
        if (typedArray.hasValue(i) && (resourceId = typedArray.getResourceId(i, 0)) != 0 && (b = u0.b(context, resourceId)) != null) {
            return b;
        }
        if (Build.VERSION.SDK_INT > 15 || (color = typedArray.getColor(i, -1)) == -1) {
            return typedArray.getColorStateList(i);
        }
        return ColorStateList.valueOf(color);
    }

    @DexIgnore
    public static Drawable b(Context context, TypedArray typedArray, int i) {
        int resourceId;
        Drawable c;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0 || (c = u0.c(context, resourceId)) == null) {
            return typedArray.getDrawable(i);
        }
        return c;
    }

    @DexIgnore
    public static qi3 c(Context context, TypedArray typedArray, int i) {
        int resourceId;
        if (!typedArray.hasValue(i) || (resourceId = typedArray.getResourceId(i, 0)) == 0) {
            return null;
        }
        return new qi3(context, resourceId);
    }

    @DexIgnore
    public static ColorStateList a(Context context, i3 i3Var, int i) {
        int a;
        int g;
        ColorStateList b;
        if (i3Var.g(i) && (g = i3Var.g(i, 0)) != 0 && (b = u0.b(context, g)) != null) {
            return b;
        }
        if (Build.VERSION.SDK_INT > 15 || (a = i3Var.a(i, -1)) == -1) {
            return i3Var.a(i);
        }
        return ColorStateList.valueOf(a);
    }

    @DexIgnore
    public static int a(Context context, TypedArray typedArray, int i, int i2) {
        TypedValue typedValue = new TypedValue();
        if (!typedArray.getValue(i, typedValue) || typedValue.type != 2) {
            return typedArray.getDimensionPixelSize(i, i2);
        }
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(new int[]{typedValue.data});
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(0, i2);
        obtainStyledAttributes.recycle();
        return dimensionPixelSize;
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i, int i2) {
        return typedArray.hasValue(i) ? i : i2;
    }
}
