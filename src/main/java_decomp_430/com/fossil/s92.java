package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s92 extends Service {
    @DexIgnore
    public /* final */ ExecutorService a; // = ff2.a().a(new y42("EnhancedIntentService"), 9);
    @DexIgnore
    public Binder b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = 0;

    @DexIgnore
    public final void a(Intent intent) {
        if (intent != null) {
            qc.a(intent);
        }
        synchronized (this.c) {
            this.e--;
            if (this.e == 0) {
                stopSelfResult(this.d);
            }
        }
    }

    @DexIgnore
    public abstract void handleIntent(Intent intent);

    @DexIgnore
    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.b == null) {
            this.b = new w92(this);
        }
        return this.b;
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.c) {
            this.d = i2;
            this.e++;
        }
        if (intent == null) {
            a(intent);
            return 2;
        }
        this.a.execute(new t92(this, intent, intent));
        return 3;
    }
}
