package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hy1 extends uy1 {
    @DexIgnore
    public WeakReference<ay1> a;

    @DexIgnore
    public hy1(ay1 ay1) {
        this.a = new WeakReference<>(ay1);
    }

    @DexIgnore
    public final void a() {
        ay1 ay1 = (ay1) this.a.get();
        if (ay1 != null) {
            ay1.l();
        }
    }
}
