package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cm3<K, V> extends wl3<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends b<K, V> {
        @DexIgnore
        public /* final */ transient cm3<K, V> b;

        @DexIgnore
        public a(K k, V v, cm3<K, V> cm3, cm3<K, V> cm32) {
            super(k, v, cm3);
            this.b = cm32;
        }

        @DexIgnore
        public cm3<K, V> getNextInValueBucket() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends cm3<K, V> {
        @DexIgnore
        public /* final */ transient cm3<K, V> a;

        @DexIgnore
        public b(K k, V v, cm3<K, V> cm3) {
            super(k, v);
            this.a = cm3;
        }

        @DexIgnore
        public final cm3<K, V> getNextInKeyBucket() {
            return this.a;
        }

        @DexIgnore
        public final boolean isReusable() {
            return false;
        }
    }

    @DexIgnore
    public cm3(K k, V v) {
        super(k, v);
        bl3.a((Object) k, (Object) v);
    }

    @DexIgnore
    public static <K, V> cm3<K, V>[] createEntryArray(int i) {
        return new cm3[i];
    }

    @DexIgnore
    public cm3<K, V> getNextInKeyBucket() {
        return null;
    }

    @DexIgnore
    public cm3<K, V> getNextInValueBucket() {
        return null;
    }

    @DexIgnore
    public boolean isReusable() {
        return true;
    }

    @DexIgnore
    public cm3(cm3<K, V> cm3) {
        super(cm3.getKey(), cm3.getValue());
    }
}
