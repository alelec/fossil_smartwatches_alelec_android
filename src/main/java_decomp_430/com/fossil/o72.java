package com.fossil;

import com.facebook.places.PlaceManager;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o72 {
    @DexIgnore
    public static /* final */ double c; // = (10.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ double d; // = (1000.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ double e; // = (2000.0d / ((double) TimeUnit.HOURS.toNanos(1)));
    @DexIgnore
    public static /* final */ double f; // = (100.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ Set<String> g; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{PlaceManager.PARAM_ALTITUDE, "duration", "food_item", "meal_type", "repetitions", "resistance", "resistance_type", "debug_session", "google.android.fitness.SessionV2"})));
    @DexIgnore
    public static /* final */ o72 h; // = new o72();
    @DexIgnore
    public /* final */ Map<String, Map<String, p72>> a;
    @DexIgnore
    public /* final */ Map<String, p72> b;

    @DexIgnore
    public o72() {
        HashMap hashMap = new HashMap();
        hashMap.put("latitude", new p72(-90.0d, 90.0d));
        hashMap.put("longitude", new p72(-180.0d, 180.0d));
        hashMap.put(PlaceManager.PARAM_ACCURACY, new p72(0.0d, 10000.0d));
        hashMap.put("bpm", new p72(0.0d, 1000.0d));
        hashMap.put(PlaceManager.PARAM_ALTITUDE, new p72(-100000.0d, 100000.0d));
        hashMap.put("percentage", new p72(0.0d, 100.0d));
        hashMap.put("confidence", new p72(0.0d, 100.0d));
        hashMap.put("duration", new p72(0.0d, 9.223372036854776E18d));
        hashMap.put("height", new p72(0.0d, 3.0d));
        hashMap.put("weight", new p72(0.0d, 1000.0d));
        hashMap.put(PlaceManager.PARAM_SPEED, new p72(0.0d, 11000.0d));
        this.b = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("com.google.step_count.delta", a("steps", new p72(0.0d, c)));
        hashMap2.put("com.google.calories.consumed", a("calories", new p72(0.0d, d)));
        hashMap2.put("com.google.calories.expended", a("calories", new p72(0.0d, e)));
        hashMap2.put("com.google.distance.delta", a("distance", new p72(0.0d, f)));
        this.a = Collections.unmodifiableMap(hashMap2);
    }

    @DexIgnore
    public static <K, V> Map<K, V> a(K k, V v) {
        HashMap hashMap = new HashMap();
        hashMap.put(k, v);
        return hashMap;
    }

    @DexIgnore
    public final p72 a(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public final p72 a(String str, String str2) {
        Map map = this.a.get(str);
        if (map != null) {
            return (p72) map.get(str2);
        }
        return null;
    }

    @DexIgnore
    public static o72 a() {
        return h;
    }
}
