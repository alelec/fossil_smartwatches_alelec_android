package com.fossil;

import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qp4 implements MembersInjector<FossilNotificationListenerService> {
    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, DianaNotificationComponent dianaNotificationComponent) {
        fossilNotificationListenerService.h = dianaNotificationComponent;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, HybridMessageNotificationComponent hybridMessageNotificationComponent) {
        fossilNotificationListenerService.i = hybridMessageNotificationComponent;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, u04 u04) {
        fossilNotificationListenerService.j = u04;
    }

    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, an4 an4) {
        fossilNotificationListenerService.o = an4;
    }
}
