package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t7 extends Drawable.ConstantState {
    @DexIgnore
    public int a;
    @DexIgnore
    public Drawable.ConstantState b;
    @DexIgnore
    public ColorStateList c; // = null;
    @DexIgnore
    public PorterDuff.Mode d; // = r7.g;

    @DexIgnore
    public t7(t7 t7Var) {
        if (t7Var != null) {
            this.a = t7Var.a;
            this.b = t7Var.b;
            this.c = t7Var.c;
            this.d = t7Var.d;
        }
    }

    @DexIgnore
    public boolean a() {
        return this.b != null;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int i = this.a;
        Drawable.ConstantState constantState = this.b;
        return i | (constantState != null ? constantState.getChangingConfigurations() : 0);
    }

    @DexIgnore
    public Drawable newDrawable() {
        return newDrawable((Resources) null);
    }

    @DexIgnore
    public Drawable newDrawable(Resources resources) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new s7(this, resources);
        }
        return new r7(this, resources);
    }
}
