package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn2 extends sh2 implements el2 {
    @DexIgnore
    public bn2(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    @DexIgnore
    public final Bundle d(Bundle bundle) throws RemoteException {
        Parcel q = q();
        li2.a(q, (Parcelable) bundle);
        Parcel a = a(1, q);
        Bundle bundle2 = (Bundle) li2.a(a, Bundle.CREATOR);
        a.recycle();
        return bundle2;
    }
}
