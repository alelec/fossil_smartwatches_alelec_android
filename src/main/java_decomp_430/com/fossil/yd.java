package com.fossil;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yd<D> extends ae<D> {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ String TAG; // = "AsyncTaskLoader";
    @DexIgnore
    public volatile yd<D>.a mCancellingTask;
    @DexIgnore
    public /* final */ Executor mExecutor;
    @DexIgnore
    public Handler mHandler;
    @DexIgnore
    public long mLastLoadCompleteTime;
    @DexIgnore
    public volatile yd<D>.a mTask;
    @DexIgnore
    public long mUpdateThrottle;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends be<Void, Void, D> implements Runnable {
        @DexIgnore
        public /* final */ CountDownLatch j; // = new CountDownLatch(1);
        @DexIgnore
        public boolean o;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void b(D d) {
            try {
                yd.this.dispatchOnCancelled(this, d);
            } finally {
                this.j.countDown();
            }
        }

        @DexIgnore
        public void c(D d) {
            try {
                yd.this.dispatchOnLoadComplete(this, d);
            } finally {
                this.j.countDown();
            }
        }

        @DexIgnore
        public void e() {
            try {
                this.j.await();
            } catch (InterruptedException unused) {
            }
        }

        @DexIgnore
        public void run() {
            this.o = false;
            yd.this.executePendingTask();
        }

        @DexIgnore
        public D a(Void... voidArr) {
            try {
                return yd.this.onLoadInBackground();
            } catch (f8 e) {
                if (a()) {
                    return null;
                }
                throw e;
            }
        }
    }

    @DexIgnore
    public yd(Context context) {
        this(context, be.h);
    }

    @DexIgnore
    public void cancelLoadInBackground() {
    }

    @DexIgnore
    public void dispatchOnCancelled(yd<D>.a aVar, D d) {
        onCanceled(d);
        if (this.mCancellingTask == aVar) {
            rollbackContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mCancellingTask = null;
            deliverCancellation();
            executePendingTask();
        }
    }

    @DexIgnore
    public void dispatchOnLoadComplete(yd<D>.a aVar, D d) {
        if (this.mTask != aVar) {
            dispatchOnCancelled(aVar, d);
        } else if (isAbandoned()) {
            onCanceled(d);
        } else {
            commitContentChanged();
            this.mLastLoadCompleteTime = SystemClock.uptimeMillis();
            this.mTask = null;
            deliverResult(d);
        }
    }

    @DexIgnore
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        if (this.mTask != null) {
            printWriter.print(str);
            printWriter.print("mTask=");
            printWriter.print(this.mTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mTask.o);
        }
        if (this.mCancellingTask != null) {
            printWriter.print(str);
            printWriter.print("mCancellingTask=");
            printWriter.print(this.mCancellingTask);
            printWriter.print(" waiting=");
            printWriter.println(this.mCancellingTask.o);
        }
        if (this.mUpdateThrottle != 0) {
            printWriter.print(str);
            printWriter.print("mUpdateThrottle=");
            z8.a(this.mUpdateThrottle, printWriter);
            printWriter.print(" mLastLoadCompleteTime=");
            z8.a(this.mLastLoadCompleteTime, SystemClock.uptimeMillis(), printWriter);
            printWriter.println();
        }
    }

    @DexIgnore
    public void executePendingTask() {
        if (this.mCancellingTask == null && this.mTask != null) {
            if (this.mTask.o) {
                this.mTask.o = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            if (this.mUpdateThrottle <= 0 || SystemClock.uptimeMillis() >= this.mLastLoadCompleteTime + this.mUpdateThrottle) {
                this.mTask.a(this.mExecutor, (Params[]) null);
                return;
            }
            this.mTask.o = true;
            this.mHandler.postAtTime(this.mTask, this.mLastLoadCompleteTime + this.mUpdateThrottle);
        }
    }

    @DexIgnore
    public boolean isLoadInBackgroundCanceled() {
        return this.mCancellingTask != null;
    }

    @DexIgnore
    public abstract D loadInBackground();

    @DexIgnore
    public boolean onCancelLoad() {
        if (this.mTask == null) {
            return false;
        }
        if (!this.mStarted) {
            this.mContentChanged = true;
        }
        if (this.mCancellingTask != null) {
            if (this.mTask.o) {
                this.mTask.o = false;
                this.mHandler.removeCallbacks(this.mTask);
            }
            this.mTask = null;
            return false;
        } else if (this.mTask.o) {
            this.mTask.o = false;
            this.mHandler.removeCallbacks(this.mTask);
            this.mTask = null;
            return false;
        } else {
            boolean a2 = this.mTask.a(false);
            if (a2) {
                this.mCancellingTask = this.mTask;
                cancelLoadInBackground();
            }
            this.mTask = null;
            return a2;
        }
    }

    @DexIgnore
    public void onCanceled(D d) {
    }

    @DexIgnore
    public void onForceLoad() {
        super.onForceLoad();
        cancelLoad();
        this.mTask = new a();
        executePendingTask();
    }

    @DexIgnore
    public D onLoadInBackground() {
        return loadInBackground();
    }

    @DexIgnore
    public void setUpdateThrottle(long j) {
        this.mUpdateThrottle = j;
        if (j != 0) {
            this.mHandler = new Handler();
        }
    }

    @DexIgnore
    public void waitForLoader() {
        yd<D>.a aVar = this.mTask;
        if (aVar != null) {
            aVar.e();
        }
    }

    @DexIgnore
    public yd(Context context, Executor executor) {
        super(context);
        this.mLastLoadCompleteTime = -10000;
        this.mExecutor = executor;
    }
}
