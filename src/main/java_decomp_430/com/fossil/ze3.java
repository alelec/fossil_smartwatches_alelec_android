package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze3 implements Parcelable.Creator<ye3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i = 0;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 2:
                    i = f22.q(parcel2, a);
                    break;
                case 3:
                    str = f22.e(parcel2, a);
                    break;
                case 4:
                    str2 = f22.e(parcel2, a);
                    break;
                case 5:
                    str3 = f22.e(parcel2, a);
                    break;
                case 6:
                    str4 = f22.e(parcel2, a);
                    break;
                case 7:
                    str5 = f22.e(parcel2, a);
                    break;
                case 8:
                    str6 = f22.e(parcel2, a);
                    break;
                case 9:
                    b2 = f22.k(parcel2, a);
                    break;
                case 10:
                    b3 = f22.k(parcel2, a);
                    break;
                case 11:
                    b4 = f22.k(parcel2, a);
                    break;
                case 12:
                    b5 = f22.k(parcel2, a);
                    break;
                case 13:
                    str7 = f22.e(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new ye3(i, str, str2, str3, str4, str5, str6, b2, b3, b4, b5, str7);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ye3[i];
    }
}
