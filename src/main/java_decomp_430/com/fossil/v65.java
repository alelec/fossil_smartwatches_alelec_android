package com.fossil;

import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v65 implements Factory<u65> {
    @DexIgnore
    public static CustomizeThemePresenter a(r65 r65, WatchFaceRepository watchFaceRepository, DianaPresetRepository dianaPresetRepository) {
        return new CustomizeThemePresenter(r65, watchFaceRepository, dianaPresetRepository);
    }
}
