package com.fossil;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vx6 {
    @DexIgnore
    public static /* final */ Type[] a; // = new Type[0];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements GenericArrayType {
        @DexIgnore
        public /* final */ Type a;

        @DexIgnore
        public a(Type type) {
            this.a = type;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && vx6.a((Type) this, (Type) (GenericArrayType) obj);
        }

        @DexIgnore
        public Type getGenericComponentType() {
            return this.a;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return vx6.d(this.a) + "[]";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ParameterizedType {
        @DexIgnore
        public /* final */ Type a;
        @DexIgnore
        public /* final */ Type b;
        @DexIgnore
        public /* final */ Type[] c;

        @DexIgnore
        public b(Type type, Type type2, Type... typeArr) {
            if (type2 instanceof Class) {
                if ((type == null) != (((Class) type2).getEnclosingClass() != null ? false : true)) {
                    throw new IllegalArgumentException();
                }
            }
            for (Type type3 : typeArr) {
                vx6.a(type3, "typeArgument == null");
                vx6.a(type3);
            }
            this.a = type;
            this.b = type2;
            this.c = (Type[]) typeArr.clone();
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && vx6.a((Type) this, (Type) (ParameterizedType) obj);
        }

        @DexIgnore
        public Type[] getActualTypeArguments() {
            return (Type[]) this.c.clone();
        }

        @DexIgnore
        public Type getOwnerType() {
            return this.a;
        }

        @DexIgnore
        public Type getRawType() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = Arrays.hashCode(this.c) ^ this.b.hashCode();
            Type type = this.a;
            return hashCode ^ (type != null ? type.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            Type[] typeArr = this.c;
            if (typeArr.length == 0) {
                return vx6.d(this.b);
            }
            StringBuilder sb = new StringBuilder((typeArr.length + 1) * 30);
            sb.append(vx6.d(this.b));
            sb.append("<");
            sb.append(vx6.d(this.c[0]));
            for (int i = 1; i < this.c.length; i++) {
                sb.append(", ");
                sb.append(vx6.d(this.c[i]));
            }
            sb.append(">");
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WildcardType {
        @DexIgnore
        public /* final */ Type a;
        @DexIgnore
        public /* final */ Type b;

        @DexIgnore
        public c(Type[] typeArr, Type[] typeArr2) {
            Class<Object> cls = Object.class;
            if (typeArr2.length > 1) {
                throw new IllegalArgumentException();
            } else if (typeArr.length != 1) {
                throw new IllegalArgumentException();
            } else if (typeArr2.length == 1) {
                if (typeArr2[0] != null) {
                    vx6.a(typeArr2[0]);
                    if (typeArr[0] == cls) {
                        this.b = typeArr2[0];
                        this.a = cls;
                        return;
                    }
                    throw new IllegalArgumentException();
                }
                throw new NullPointerException();
            } else if (typeArr[0] != null) {
                vx6.a(typeArr[0]);
                this.b = null;
                this.a = typeArr[0];
            } else {
                throw new NullPointerException();
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && vx6.a((Type) this, (Type) (WildcardType) obj);
        }

        @DexIgnore
        public Type[] getLowerBounds() {
            Type type = this.b;
            if (type == null) {
                return vx6.a;
            }
            return new Type[]{type};
        }

        @DexIgnore
        public Type[] getUpperBounds() {
            return new Type[]{this.a};
        }

        @DexIgnore
        public int hashCode() {
            Type type = this.b;
            return (type != null ? type.hashCode() + 31 : 1) ^ (this.a.hashCode() + 31);
        }

        @DexIgnore
        public String toString() {
            if (this.b != null) {
                return "? super " + vx6.d(this.b);
            } else if (this.a == Object.class) {
                return "?";
            } else {
                return "? extends " + vx6.d(this.a);
            }
        }
    }

    @DexIgnore
    public static RuntimeException a(Method method, String str, Object... objArr) {
        return a(method, (Throwable) null, str, objArr);
    }

    @DexIgnore
    public static Class<?> b(Type type) {
        a(type, "type == null");
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            if (rawType instanceof Class) {
                return (Class) rawType;
            }
            throw new IllegalArgumentException();
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(b(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return b(((WildcardType) type).getUpperBounds()[0]);
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + type.getClass().getName());
        }
    }

    @DexIgnore
    public static boolean c(Type type) {
        String str;
        if (type instanceof Class) {
            return false;
        }
        if (type instanceof ParameterizedType) {
            for (Type c2 : ((ParameterizedType) type).getActualTypeArguments()) {
                if (c(c2)) {
                    return true;
                }
            }
            return false;
        } else if (type instanceof GenericArrayType) {
            return c(((GenericArrayType) type).getGenericComponentType());
        } else {
            if ((type instanceof TypeVariable) || (type instanceof WildcardType)) {
                return true;
            }
            if (type == null) {
                str = "null";
            } else {
                str = type.getClass().getName();
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + str);
        }
    }

    @DexIgnore
    public static String d(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    @DexIgnore
    public static RuntimeException a(Method method, Throwable th, String str, Object... objArr) {
        String format = String.format(str, objArr);
        return new IllegalArgumentException(format + "\n    for method " + method.getDeclaringClass().getSimpleName() + "." + method.getName(), th);
    }

    @DexIgnore
    public static RuntimeException a(Method method, Throwable th, int i, String str, Object... objArr) {
        return a(method, th, str + " (parameter #" + (i + 1) + ")", objArr);
    }

    @DexIgnore
    public static RuntimeException a(Method method, int i, String str, Object... objArr) {
        return a(method, str + " (parameter #" + (i + 1) + ")", objArr);
    }

    @DexIgnore
    public static boolean a(Type type, Type type2) {
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            Type ownerType = parameterizedType.getOwnerType();
            Type ownerType2 = parameterizedType2.getOwnerType();
            if ((ownerType == ownerType2 || (ownerType != null && ownerType.equals(ownerType2))) && parameterizedType.getRawType().equals(parameterizedType2.getRawType()) && Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                return true;
            }
            return false;
        } else if (type instanceof GenericArrayType) {
            if (!(type2 instanceof GenericArrayType)) {
                return false;
            }
            return a(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                return false;
            }
            return true;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                return false;
            }
            return true;
        }
    }

    @DexIgnore
    public static Type b(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return a(type, cls, a(type, cls, cls2));
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public static Type b(int i, ParameterizedType parameterizedType) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (i < 0 || i >= actualTypeArguments.length) {
            throw new IllegalArgumentException("Index " + i + " not in range [0," + actualTypeArguments.length + ") for " + parameterizedType);
        }
        Type type = actualTypeArguments[i];
        return type instanceof WildcardType ? ((WildcardType) type).getUpperBounds()[0] : type;
    }

    @DexIgnore
    public static Type a(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return a(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return a(cls.getGenericSuperclass(), (Class<?>) superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    @DexIgnore
    public static int a(Object[] objArr, Object obj) {
        for (int i = 0; i < objArr.length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.reflect.Type[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v14, resolved type: java.lang.reflect.WildcardType} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v15, resolved type: java.lang.reflect.WildcardType} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v16, resolved type: java.lang.reflect.WildcardType} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static Type a(Type type, Class<?> cls, Type type2) {
        while (true) {
            boolean z = r10 instanceof TypeVariable;
            Type type3 = type2;
            if (z) {
                TypeVariable typeVariable = (TypeVariable) type3;
                Type a2 = a(type, cls, (TypeVariable<?>) typeVariable);
                if (a2 == typeVariable) {
                    return a2;
                }
                type3 = a2;
            } else {
                if (type3 instanceof Class) {
                    Class cls2 = (Class) type3;
                    if (cls2.isArray()) {
                        Type componentType = cls2.getComponentType();
                        Type a3 = a(type, cls, componentType);
                        return componentType == a3 ? cls2 : new a(a3);
                    }
                }
                if (type3 instanceof GenericArrayType) {
                    GenericArrayType genericArrayType = (GenericArrayType) type3;
                    Type genericComponentType = genericArrayType.getGenericComponentType();
                    Type a4 = a(type, cls, genericComponentType);
                    return genericComponentType == a4 ? genericArrayType : new a(a4);
                }
                if (type3 instanceof ParameterizedType) {
                    ParameterizedType parameterizedType = (ParameterizedType) type3;
                    Type ownerType = parameterizedType.getOwnerType();
                    Type a5 = a(type, cls, ownerType);
                    boolean z2 = a5 != ownerType;
                    Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                    int length = actualTypeArguments.length;
                    for (int i = 0; i < length; i++) {
                        Type a6 = a(type, cls, actualTypeArguments[i]);
                        if (a6 != actualTypeArguments[i]) {
                            if (!z2) {
                                actualTypeArguments = actualTypeArguments.clone();
                                z2 = true;
                            }
                            actualTypeArguments[i] = a6;
                        }
                    }
                    return z2 ? new b(a5, parameterizedType.getRawType(), actualTypeArguments) : parameterizedType;
                }
                boolean z3 = type3 instanceof WildcardType;
                Type type4 = type3;
                if (z3) {
                    WildcardType wildcardType = (WildcardType) type3;
                    Type[] lowerBounds = wildcardType.getLowerBounds();
                    Type[] upperBounds = wildcardType.getUpperBounds();
                    if (lowerBounds.length == 1) {
                        Type a7 = a(type, cls, lowerBounds[0]);
                        type4 = wildcardType;
                        if (a7 != lowerBounds[0]) {
                            return new c(new Type[]{Object.class}, new Type[]{a7});
                        }
                    } else {
                        type4 = wildcardType;
                        if (upperBounds.length == 1) {
                            Type a8 = a(type, cls, upperBounds[0]);
                            type4 = wildcardType;
                            if (a8 != upperBounds[0]) {
                                return new c(new Type[]{a8}, a);
                            }
                        }
                    }
                }
                return type4;
            }
        }
    }

    @DexIgnore
    public static Type a(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> a2 = a(typeVariable);
        if (a2 == null) {
            return typeVariable;
        }
        Type a3 = a(type, cls, a2);
        if (!(a3 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) a3).getActualTypeArguments()[a((Object[]) a2.getTypeParameters(), (Object) typeVariable)];
    }

    @DexIgnore
    public static Class<?> a(TypeVariable<?> typeVariable) {
        Object genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }

    @DexIgnore
    public static void a(Type type) {
        if ((type instanceof Class) && ((Class) type).isPrimitive()) {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static boolean a(Annotation[] annotationArr, Class<? extends Annotation> cls) {
        for (Annotation isInstance : annotationArr) {
            if (cls.isInstance(isInstance)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static zq6 a(zq6 zq6) throws IOException {
        jt6 jt6 = new jt6();
        zq6.source().a(jt6);
        return zq6.create(zq6.contentType(), zq6.contentLength(), jt6);
    }

    @DexIgnore
    public static <T> void a(Class<T> cls) {
        if (!cls.isInterface()) {
            throw new IllegalArgumentException("API declarations must be interfaces.");
        } else if (cls.getInterfaces().length > 0) {
            throw new IllegalArgumentException("API interfaces must not extend other interfaces.");
        }
    }

    @DexIgnore
    public static Type a(int i, ParameterizedType parameterizedType) {
        Type type = parameterizedType.getActualTypeArguments()[i];
        return type instanceof WildcardType ? ((WildcardType) type).getLowerBounds()[0] : type;
    }

    @DexIgnore
    public static void a(Throwable th) {
        if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }
}
