package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cl2<T> implements Serializable {
    @DexIgnore
    public static <T> cl2<T> zza(T t) {
        bl2.a(t);
        return new fl2(t);
    }

    @DexIgnore
    public static <T> cl2<T> zzc() {
        return al2.zza;
    }

    @DexIgnore
    public abstract boolean zza();

    @DexIgnore
    public abstract T zzb();
}
