package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i53 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ /* synthetic */ h53 e;

    @DexIgnore
    public i53(h53 h53, String str, long j) {
        this.e = h53;
        w12.b(str);
        this.a = str;
        this.b = j;
    }

    @DexIgnore
    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.A().getLong(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void a(long j) {
        SharedPreferences.Editor edit = this.e.A().edit();
        edit.putLong(this.a, j);
        edit.apply();
        this.d = j;
    }
}
