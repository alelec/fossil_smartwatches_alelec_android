package com.fossil;

import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class si5$g$b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $listTimeZoneChange;
    @DexIgnore
    public /* final */ /* synthetic */ List $listTodayHeartRateModel;
    @DexIgnore
    public /* final */ /* synthetic */ int $maxHR;
    @DexIgnore
    public /* final */ /* synthetic */ hh6 $previousTimeZoneOffset;
    @DexIgnore
    public /* final */ /* synthetic */ long $startOfDay;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si5$g$b(HeartRateDetailPresenter.g gVar, long j, hh6 hh6, List list, List list2, int i, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
        this.$startOfDay = j;
        this.$previousTimeZoneOffset = hh6;
        this.$listTimeZoneChange = list;
        this.$listTodayHeartRateModel = list2;
        this.$maxHR = i;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        si5$g$b si5_g_b = new si5$g$b(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, xe6);
        si5_g_b.p$ = (il6) obj;
        return si5_g_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((si5$g$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Iterator it;
        char c;
        StringBuilder sb;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            List f = this.this$0.this$0.k;
            if (f == null) {
                return null;
            }
            for (Iterator it2 = f.iterator(); it2.hasNext(); it2 = it) {
                HeartRateSample heartRateSample = (HeartRateSample) it2.next();
                long j = (long) 60000;
                long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / j;
                long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / j;
                long j2 = (millis2 + millis) / ((long) 2);
                if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                    int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                    String a = zk4.a(hourOfDay);
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {hf6.a(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / DateTimeConstants.SECONDS_PER_HOUR)), hf6.a(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))};
                    String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    List list = this.$listTimeZoneChange;
                    Integer a2 = hf6.a((int) j2);
                    it = it2;
                    lc6 lc6 = new lc6(hf6.a(hourOfDay), hf6.a(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(a);
                    if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                        sb = new StringBuilder();
                        c = '+';
                    } else {
                        sb = new StringBuilder();
                        c = '-';
                    }
                    sb.append(c);
                    sb.append(format);
                    sb2.append(sb.toString());
                    list.add(new pc6(a2, lc6, sb2.toString()));
                    this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                } else {
                    it = it2;
                }
                if (!this.$listTodayHeartRateModel.isEmpty()) {
                    jz5 jz5 = (jz5) yd6.f(this.$listTodayHeartRateModel);
                    if (millis - ((long) jz5.a()) > 1) {
                        this.$listTodayHeartRateModel.add(new jz5(0, 0, 0, jz5.a(), (int) millis, (int) j2));
                    }
                }
                int average = (int) heartRateSample.getAverage();
                if (heartRateSample.getMax() == this.$maxHR) {
                    average = heartRateSample.getMax();
                }
                this.$listTodayHeartRateModel.add(new jz5(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j2));
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
