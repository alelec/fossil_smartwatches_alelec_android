package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class hk6 {
    @DexIgnore
    public static /* synthetic */ Object a(af6 af6, ig6 ig6, int i, Object obj) throws InterruptedException {
        if ((i & 1) != 0) {
            af6 = bf6.INSTANCE;
        }
        return gk6.a(af6, ig6);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003b, code lost:
        if (r1 != null) goto L_0x0044;
     */
    @DexIgnore
    public static final <T> T a(af6 af6, ig6<? super il6, ? super xe6<? super T>, ? extends Object> ig6) throws InterruptedException {
        af6 af62;
        em6 em6;
        wg6.b(af6, "context");
        wg6.b(ig6, "block");
        Thread currentThread = Thread.currentThread();
        ye6 ye6 = (ye6) af6.get(ye6.l);
        if (ye6 == null) {
            em6 = on6.b.b();
            af62 = cl6.a(km6.a, af6.plus(em6));
        } else {
            if (!(ye6 instanceof em6)) {
                ye6 = null;
            }
            em6 = (em6) ye6;
            if (em6 != null) {
                if (!em6.E()) {
                    em6 = null;
                }
            }
            em6 = on6.b.a();
            af62 = cl6.a(km6.a, af6);
        }
        wg6.a((Object) currentThread, "currentThread");
        ek6 ek6 = new ek6(af62, currentThread, em6);
        ek6.a(ll6.DEFAULT, ek6, ig6);
        return ek6.p();
    }
}
