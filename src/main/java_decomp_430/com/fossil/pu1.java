package com.fossil;

import android.content.Context;
import android.os.Binder;
import com.fossil.wv1;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu1 extends ku1 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public pu1(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void j() {
        q();
        vt1 a2 = vt1.a(this.a);
        GoogleSignInAccount b = a2.b();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.t;
        if (b != null) {
            googleSignInOptions = a2.c();
        }
        wv1.a aVar = new wv1.a(this.a);
        aVar.a(jt1.e, googleSignInOptions);
        wv1 a3 = aVar.a();
        try {
            if (a3.a().E()) {
                if (b != null) {
                    jt1.f.a(a3);
                } else {
                    a3.b();
                }
            }
        } finally {
            a3.d();
        }
    }

    @DexIgnore
    public final void k() {
        q();
        iu1.a(this.a).a();
    }

    @DexIgnore
    public final void q() {
        if (!nv1.isGooglePlayServicesUid(this.a, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }
}
