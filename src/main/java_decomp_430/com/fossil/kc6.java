package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kc6 extends RuntimeException {
    @DexIgnore
    public kc6() {
    }

    @DexIgnore
    public kc6(String str) {
        super(str);
    }

    @DexIgnore
    public kc6(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public kc6(Throwable th) {
        super(th);
    }
}
