package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e52 {
    @DexIgnore
    public static Context a;
    @DexIgnore
    public static Boolean b;

    @DexIgnore
    public static synchronized boolean a(Context context) {
        synchronized (e52.class) {
            Context applicationContext = context.getApplicationContext();
            if (a == null || b == null || a != applicationContext) {
                b = null;
                if (s42.i()) {
                    b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        b = true;
                    } catch (ClassNotFoundException unused) {
                        b = false;
                    }
                }
                a = applicationContext;
                boolean booleanValue = b.booleanValue();
                return booleanValue;
            }
            boolean booleanValue2 = b.booleanValue();
            return booleanValue2;
        }
    }
}
