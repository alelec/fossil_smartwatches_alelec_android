package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f02 implements xy1 {
    @DexIgnore
    public /* final */ /* synthetic */ b02 a;

    @DexIgnore
    public f02(b02 b02) {
        this.a = b02;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        this.a.q.lock();
        try {
            gv1 unused = this.a.o = gv1.e;
            this.a.h();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ f02(b02 b02, e02 e02) {
        this(b02);
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        this.a.q.lock();
        try {
            gv1 unused = this.a.o = gv1;
            this.a.h();
        } finally {
            this.a.q.unlock();
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        this.a.q.lock();
        try {
            if (this.a.p) {
                boolean unused = this.a.p = false;
                this.a.a(i, z);
                return;
            }
            boolean unused2 = this.a.p = true;
            this.a.d.g(i);
            this.a.q.unlock();
        } finally {
            this.a.q.unlock();
        }
    }
}
