package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mn<T> {
    @DexIgnore
    public static /* final */ String f; // = tl.a("ConstraintTracker");
    @DexIgnore
    public /* final */ to a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public /* final */ Set<xm<T>> d; // = new LinkedHashSet();
    @DexIgnore
    public T e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ List a;

        @DexIgnore
        public a(List list) {
            this.a = list;
        }

        @DexIgnore
        public void run() {
            for (xm a2 : this.a) {
                a2.a(mn.this.e);
            }
        }
    }

    @DexIgnore
    public mn(Context context, to toVar) {
        this.b = context.getApplicationContext();
        this.a = toVar;
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(xm<T> xmVar) {
        synchronized (this.c) {
            if (this.d.add(xmVar)) {
                if (this.d.size() == 1) {
                    this.e = a();
                    tl.a().a(f, String.format("%s: initial state = %s", new Object[]{getClass().getSimpleName(), this.e}), new Throwable[0]);
                    b();
                }
                xmVar.a(this.e);
            }
        }
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public void b(xm<T> xmVar) {
        synchronized (this.c) {
            if (this.d.remove(xmVar) && this.d.isEmpty()) {
                c();
            }
        }
    }

    @DexIgnore
    public abstract void c();

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    @DexIgnore
    public void a(T t) {
        synchronized (this.c) {
            if (this.e != t) {
                if (this.e == null || !this.e.equals(t)) {
                    this.e = t;
                    this.a.a().execute(new a(new ArrayList(this.d)));
                }
            }
        }
    }
}
