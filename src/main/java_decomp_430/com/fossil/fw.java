package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fw implements as<Bitmap> {
    @DexIgnore
    public static /* final */ wr<Integer> b; // = wr.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionQuality", 90);
    @DexIgnore
    public static /* final */ wr<Bitmap.CompressFormat> c; // = wr.a("com.bumptech.glide.load.resource.bitmap.BitmapEncoder.CompressionFormat");
    @DexIgnore
    public /* final */ xt a;

    @DexIgnore
    public fw(xt xtVar) {
        this.a = xtVar;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:21|(2:38|39)|40|41) */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0066, code lost:
        if (r6 == null) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x00bf */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[Catch:{ all -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00bc A[SYNTHETIC, Splitter:B:38:0x00bc] */
    public boolean a(rt<Bitmap> rtVar, File file, xr xrVar) {
        Bitmap bitmap = rtVar.get();
        Bitmap.CompressFormat a2 = a(bitmap, xrVar);
        t00.a("encode: [%dx%d] %s", Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()), a2);
        try {
            long a3 = m00.a();
            int intValue = ((Integer) xrVar.a(b)).intValue();
            boolean z = false;
            es esVar = null;
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                try {
                    esVar = this.a != null ? new es(fileOutputStream, this.a) : fileOutputStream;
                    bitmap.compress(a2, intValue, esVar);
                    esVar.close();
                    z = true;
                } catch (IOException e) {
                    e = e;
                    esVar = fileOutputStream;
                    try {
                        if (Log.isLoggable("BitmapEncoder", 3)) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (esVar != null) {
                            esVar.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    esVar = fileOutputStream;
                    if (esVar != null) {
                    }
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                if (Log.isLoggable("BitmapEncoder", 3)) {
                    Log.d("BitmapEncoder", "Failed to encode Bitmap", e);
                }
            }
            try {
                esVar.close();
            } catch (IOException unused) {
            }
            if (Log.isLoggable("BitmapEncoder", 2)) {
                Log.v("BitmapEncoder", "Compressed with type: " + a2 + " of size " + r00.a(bitmap) + " in " + m00.a(a3) + ", options format: " + xrVar.a(c) + ", hasAlpha: " + bitmap.hasAlpha());
            }
            return z;
        } finally {
            t00.a();
        }
    }

    @DexIgnore
    public final Bitmap.CompressFormat a(Bitmap bitmap, xr xrVar) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) xrVar.a(c);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }

    @DexIgnore
    public rr a(xr xrVar) {
        return rr.TRANSFORMED;
    }
}
