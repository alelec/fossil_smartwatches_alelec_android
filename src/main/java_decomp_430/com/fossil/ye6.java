package com.fossil;

import com.fossil.af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ye6 extends af6.b {
    @DexIgnore
    public static final b l = b.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends af6.b> E a(ye6 ye6, af6.c<E> cVar) {
            wg6.b(cVar, "key");
            if (cVar != ye6.l) {
                return null;
            }
            if (ye6 != null) {
                return ye6;
            }
            throw new rc6("null cannot be cast to non-null type E");
        }

        @DexIgnore
        public static void a(ye6 ye6, xe6<?> xe6) {
            wg6.b(xe6, "continuation");
        }

        @DexIgnore
        public static af6 b(ye6 ye6, af6.c<?> cVar) {
            wg6.b(cVar, "key");
            return cVar == ye6.l ? bf6.INSTANCE : ye6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements af6.c<ye6> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();
    }

    @DexIgnore
    void b(xe6<?> xe6);

    @DexIgnore
    <T> xe6<T> c(xe6<? super T> xe6);
}
