package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.fossil.qw1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class az1 {
    @DexIgnore
    public az1(int i) {
    }

    @DexIgnore
    public static Status a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (s42.b() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    @DexIgnore
    public abstract void a(k02 k02, boolean z);

    @DexIgnore
    public abstract void a(qw1.a<?> aVar) throws DeadObjectException;

    @DexIgnore
    public abstract void a(Status status);

    @DexIgnore
    public abstract void a(RuntimeException runtimeException);
}
