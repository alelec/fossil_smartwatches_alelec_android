package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xx implements zr<InputStream, qx> {
    @DexIgnore
    public /* final */ List<ImageHeaderParser> a;
    @DexIgnore
    public /* final */ zr<ByteBuffer, qx> b;
    @DexIgnore
    public /* final */ xt c;

    @DexIgnore
    public xx(List<ImageHeaderParser> list, zr<ByteBuffer, qx> zrVar, xt xtVar) {
        this.a = list;
        this.b = zrVar;
        this.c = xtVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, xr xrVar) throws IOException {
        return !((Boolean) xrVar.a(wx.b)).booleanValue() && ur.b(this.a, inputStream, this.c) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public rt<qx> a(InputStream inputStream, int i, int i2, xr xrVar) throws IOException {
        byte[] a2 = a(inputStream);
        if (a2 == null) {
            return null;
        }
        return this.b.a(ByteBuffer.wrap(a2), i, i2, xrVar);
    }

    @DexIgnore
    public static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (!Log.isLoggable("StreamGifDecoder", 5)) {
                return null;
            }
            Log.w("StreamGifDecoder", "Error reading data from stream", e);
            return null;
        }
    }
}
