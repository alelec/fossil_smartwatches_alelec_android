package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tz0 implements Parcelable.Creator<o11> {
    @DexIgnore
    public /* synthetic */ tz0(qg6 qg6) {
    }

    @DexIgnore
    public o11 createFromParcel(Parcel parcel) {
        return new o11(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new o11[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m59createFromParcel(Parcel parcel) {
        return new o11(parcel, (qg6) null);
    }
}
