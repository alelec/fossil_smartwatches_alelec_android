package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s50 extends p40 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<s50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    return new s50(readString, readInt, readString2);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new s50[i];
        }
    }

    @DexIgnore
    public s50(String str, int i, String str2) throws IllegalArgumentException {
        this.a = str;
        this.b = i;
        this.c = str2;
        if (!(this.c.length() <= 10)) {
            StringBuilder b2 = ze0.b("traffic(");
            b2.append(this.c);
            b2.append(") length must be less than or equal to 10");
            throw new IllegalArgumentException(b2.toString().toString());
        }
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.DESTINATION, (Object) this.a), bm0.COMMUTE_TIME_IN_MINUTE, (Object) Integer.valueOf(this.b)), bm0.TRAFFIC, (Object) this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getCommuteTimeInMinute() {
        return this.b;
    }

    @DexIgnore
    public final String getDestination() {
        return this.a;
    }

    @DexIgnore
    public final String getTraffic() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
    }
}
