package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e04 implements xx3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (qx3 != qx3.QR_CODE) {
            throw new IllegalArgumentException("Can only encode QR_CODE, but got " + qx3);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            f04 f04 = f04.L;
            int i3 = 4;
            if (map != null) {
                if (map.containsKey(sx3.ERROR_CORRECTION)) {
                    f04 = f04.valueOf(map.get(sx3.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(sx3.MARGIN)) {
                    i3 = Integer.parseInt(map.get(sx3.MARGIN).toString());
                }
            }
            return a(k04.a(str, f04, map), i, i2, i3);
        }
    }

    @DexIgnore
    public static iy3 a(n04 n04, int i, int i2, int i3) {
        j04 a = n04.a();
        if (a != null) {
            int c = a.c();
            int b = a.b();
            int i4 = i3 << 1;
            int i5 = c + i4;
            int i6 = i4 + b;
            int max = Math.max(i, i5);
            int max2 = Math.max(i2, i6);
            int min = Math.min(max / i5, max2 / i6);
            int i7 = (max - (c * min)) / 2;
            int i8 = (max2 - (b * min)) / 2;
            iy3 iy3 = new iy3(max, max2);
            int i9 = 0;
            while (i9 < b) {
                int i10 = i7;
                int i11 = 0;
                while (i11 < c) {
                    if (a.a(i11, i9) == 1) {
                        iy3.a(i10, i8, min, min);
                    }
                    i11++;
                    i10 += min;
                }
                i9++;
                i8 += min;
            }
            return iy3;
        }
        throw new IllegalStateException();
    }
}
