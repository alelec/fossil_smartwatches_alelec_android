package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.manager.LinkStreamingManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm4$d$a extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ LinkStreamingManager.d a;

    @DexIgnore
    public vm4$d$a(LinkStreamingManager.d dVar, byte b, QuickResponseMessage quickResponseMessage) {
        this.a = dVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r13v17, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void onReceive(Context context, Intent intent) {
        NotificationBaseObj.NotificationControlActionStatus notificationControlActionStatus;
        boolean z = false;
        if (intent != null) {
            z = intent.getBooleanExtra("SEND_SMS_PERMISSION_REQUIRED", false);
        }
        if (getResultCode() == -1) {
            FLogger.INSTANCE.getLocal().d(LinkStreamingManager.g.a(), "SMS delivered");
            notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.SUCCESS;
        } else if (z) {
            FLogger.INSTANCE.getLocal().d(LinkStreamingManager.g.a(), "SMS permission required");
            notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.NOT_FOUND;
        } else {
            FLogger.INSTANCE.getLocal().d(LinkStreamingManager.g.a(), "SMS failed");
            notificationControlActionStatus = NotificationBaseObj.NotificationControlActionStatus.FAILED;
        }
        PortfolioApp.get.instance().a(PortfolioApp.get.instance().e(), (NotificationBaseObj) new DianaNotificationObj(this.a.$notificationEvent$inlined.getNotificationUid(), NotificationBaseObj.ANotificationType.TEXT, DianaNotificationObj.AApplicationName.Companion.getMESSAGES(), "", "", 1, "", new ArrayList(), Long.valueOf(System.currentTimeMillis()), notificationControlActionStatus, NotificationBaseObj.NotificationControlActionType.REPLY_MESSAGE));
        PortfolioApp.get.instance().unregisterReceiver(this);
    }
}
