package com.fossil;

import com.fossil.k40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ hr0 a;

    @DexIgnore
    public xl1(hr0 hr0) {
        this.a = hr0;
    }

    @DexIgnore
    public final void run() {
        lf0 lf0;
        hr0 hr0 = this.a;
        if (k40.l.d() != k40.c.ENABLED) {
            lf0 = lf0.BLUETOOTH_OFF;
        } else {
            lf0 = lf0.CONNECTION_DROPPED;
        }
        hr0.a(lf0);
    }
}
