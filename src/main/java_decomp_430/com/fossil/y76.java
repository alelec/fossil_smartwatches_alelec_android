package com.fossil;

import com.fossil.u76;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y76<K, V> extends u76<K, V, Provider<V>> implements b76<Map<K, Provider<V>>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends u76.a<K, V, Provider<V>> {
        @DexIgnore
        public b<K, V> a(K k, Provider<V> provider) {
            super.a(k, provider);
            return this;
        }

        @DexIgnore
        public b(int i) {
            super(i);
        }

        @DexIgnore
        public y76<K, V> a() {
            return new y76<>(this.a);
        }
    }

    @DexIgnore
    public static <K, V> b<K, V> a(int i) {
        return new b<>(i);
    }

    @DexIgnore
    public y76(Map<K, Provider<V>> map) {
        super(map);
    }

    @DexIgnore
    public Map<K, Provider<V>> get() {
        return a();
    }
}
