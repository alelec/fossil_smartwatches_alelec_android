package com.fossil;

import android.bluetooth.BluetoothGatt;
import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp0 extends ok0 {
    @DexIgnore
    public /* final */ li1 j; // = li1.DAEMON;
    @DexIgnore
    public /* final */ hn1<p51> k;

    @DexIgnore
    public rp0(at0 at0) {
        super(hm0.CLOSE, at0);
    }

    @DexIgnore
    public void a(ue1 ue1) {
        BluetoothGatt bluetoothGatt = ue1.b;
        if (bluetoothGatt != null) {
            if (ue1.u) {
                ue1.a(bluetoothGatt);
            }
            cc0 cc0 = cc0.DEBUG;
            bluetoothGatt.close();
        }
        ue1.b = null;
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            new Handler(myLooper).postDelayed(new zn0(this), 1000);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public li1 b() {
        return this.j;
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return false;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.k;
    }
}
