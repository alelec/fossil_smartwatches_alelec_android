package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.ExpandedMenuView;
import com.fossil.x1;
import com.fossil.y1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o1 implements x1, AdapterView.OnItemClickListener {
    @DexIgnore
    public Context a;
    @DexIgnore
    public LayoutInflater b;
    @DexIgnore
    public q1 c;
    @DexIgnore
    public ExpandedMenuView d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public x1.a h;
    @DexIgnore
    public a i;
    @DexIgnore
    public int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BaseAdapter {
        @DexIgnore
        public int a; // = -1;

        @DexIgnore
        public a() {
            a();
        }

        @DexIgnore
        public void a() {
            t1 f = o1.this.c.f();
            if (f != null) {
                ArrayList<t1> j = o1.this.c.j();
                int size = j.size();
                for (int i = 0; i < size; i++) {
                    if (j.get(i) == f) {
                        this.a = i;
                        return;
                    }
                }
            }
            this.a = -1;
        }

        @DexIgnore
        public int getCount() {
            int size = o1.this.c.j().size() - o1.this.e;
            return this.a < 0 ? size : size - 1;
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                o1 o1Var = o1.this;
                view = o1Var.b.inflate(o1Var.g, viewGroup, false);
            }
            ((y1.a) view).a(getItem(i), 0);
            return view;
        }

        @DexIgnore
        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }

        @DexIgnore
        public t1 getItem(int i) {
            ArrayList<t1> j = o1.this.c.j();
            int i2 = i + o1.this.e;
            int i3 = this.a;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return j.get(i2);
        }
    }

    @DexIgnore
    public o1(Context context, int i2) {
        this(i2, 0);
        this.a = context;
        this.b = LayoutInflater.from(this.a);
    }

    @DexIgnore
    public void a(Context context, q1 q1Var) {
        int i2 = this.f;
        if (i2 != 0) {
            this.a = new ContextThemeWrapper(context, i2);
            this.b = LayoutInflater.from(this.a);
        } else if (this.a != null) {
            this.a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.a);
            }
        }
        this.c = q1Var;
        a aVar = this.i;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public boolean a(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        SparseArray sparseArray = new SparseArray();
        ExpandedMenuView expandedMenuView = this.d;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @DexIgnore
    public boolean b(q1 q1Var, t1 t1Var) {
        return false;
    }

    @DexIgnore
    public ListAdapter c() {
        if (this.i == null) {
            this.i = new a();
        }
        return this.i;
    }

    @DexIgnore
    public int getId() {
        return this.j;
    }

    @DexIgnore
    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.c.a((MenuItem) this.i.getItem(i2), (x1) this, 0);
    }

    @DexIgnore
    public o1(int i2, int i3) {
        this.g = i2;
        this.f = i3;
    }

    @DexIgnore
    public Parcelable b() {
        if (this.d == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        b(bundle);
        return bundle;
    }

    @DexIgnore
    public y1 a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(g0.abc_expanded_menu_layout, viewGroup, false);
            if (this.i == null) {
                this.i = new a();
            }
            this.d.setAdapter(this.i);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    @DexIgnore
    public void a(boolean z) {
        a aVar = this.i;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(x1.a aVar) {
        this.h = aVar;
    }

    @DexIgnore
    public boolean a(c2 c2Var) {
        if (!c2Var.hasVisibleItems()) {
            return false;
        }
        new r1(c2Var).a((IBinder) null);
        x1.a aVar = this.h;
        if (aVar == null) {
            return true;
        }
        aVar.a(c2Var);
        return true;
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z) {
        x1.a aVar = this.h;
        if (aVar != null) {
            aVar.a(q1Var, z);
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.d.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        a((Bundle) parcelable);
    }
}
