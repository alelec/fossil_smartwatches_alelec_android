package com.fossil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.DatabaseHelper;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko4 extends BaseDbProvider implements jo4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public /* final */ /* synthetic */ ko4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements UpgradeCommand {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ko4$b$a$a")
            /* renamed from: com.fossil.ko4$b$a$a  reason: collision with other inner class name */
            public static final class C0019a<V> implements Callable<T> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ SQLiteDatabase b;

                @DexIgnore
                public C0019a(a aVar, SQLiteDatabase sQLiteDatabase) {
                    this.a = aVar;
                    this.b = sQLiteDatabase;
                }

                @DexIgnore
                public final boolean call() {
                    String str;
                    String str2;
                    String str3;
                    String str4;
                    String str5;
                    String str6;
                    String str7;
                    String str8;
                    String str9;
                    String str10;
                    String str11;
                    String str12;
                    String str13;
                    C0019a aVar;
                    ArrayList arrayList;
                    String str14;
                    zh4 zh4;
                    zh4 zh42;
                    String str15;
                    String str16;
                    String str17;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b2 = this.a.a.this$0.TAG;
                    wg6.a((Object) b2, "TAG");
                    local.d(b2, "Start migration user from version 1 to 2");
                    Cursor query = this.b.query(true, "user", new String[]{MFUser.USER_ACCESS_TOKEN, "uid", "createdAt", "updatedAt", "email", "authType", "username", "activeDeviceId", "firstName", "lastName", "weightInGrams", "heightInCentimeters", MFUser.HEIGHT_UNIT, MFUser.WEIGHT_UNIT, MFUser.DISTANCE_UNIT, "gender", "birthday", "profilePicture", "brand", "registrationComplete", "isOnboardingComplete", "integrations", "emailOptIn", "diagnosticEnabled", "pinType", "registerDate", MFUser.AVERAGE_STEP, MFUser.AVERAGE_SLEEP}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                    ArrayList arrayList2 = new ArrayList();
                    String str18 = MFUser.DISTANCE_UNIT;
                    String str19 = MFUser.WEIGHT_UNIT;
                    ArrayList arrayList3 = arrayList2;
                    String str20 = MFUser.HEIGHT_UNIT;
                    String str21 = "TAG";
                    String str22 = "heightInCentimeters";
                    String str23 = "weightInGrams";
                    String str24 = "registerDate";
                    String str25 = "lastName";
                    String str26 = "diagnosticEnabled";
                    String str27 = "emailOptIn";
                    String str28 = "integrations";
                    String str29 = "isOnboardingComplete";
                    String str30 = "registrationComplete";
                    String str31 = "brand";
                    String str32 = "profilePicture";
                    String str33 = "birthday";
                    String str34 = "gender";
                    if (query != null) {
                        query.moveToFirst();
                        while (!query.isAfterLast()) {
                            String str35 = str18;
                            MFUser mFUser = new MFUser();
                            mFUser.setUserAccessToken(query.getString(query.getColumnIndex(MFUser.USER_ACCESS_TOKEN)));
                            mFUser.setUserId(query.getString(query.getColumnIndex("uid")));
                            mFUser.setCreatedAt(query.getString(query.getColumnIndex("createdAt")));
                            mFUser.setUpdatedAt(query.getString(query.getColumnIndex("updatedAt")));
                            mFUser.setEmail(query.getString(query.getColumnIndex("email")));
                            mFUser.setAuthType(query.getString(query.getColumnIndex("authType")));
                            mFUser.setUsername(query.getString(query.getColumnIndex("username")));
                            mFUser.setActiveDeviceId(query.getString(query.getColumnIndex("activeDeviceId")));
                            mFUser.setFirstName(query.getString(query.getColumnIndex("firstName")));
                            mFUser.setLastName(query.getString(query.getColumnIndex(str25)));
                            mFUser.setWeightInGrams(query.getInt(query.getColumnIndex(str23)));
                            mFUser.setHeightInCentimeters(query.getInt(query.getColumnIndex(str22)));
                            mFUser.setHeightUnit(query.getString(query.getColumnIndex(str20)));
                            String str36 = str19;
                            String str37 = str20;
                            mFUser.setWeightUnit(query.getString(query.getColumnIndex(str36)));
                            String str38 = str35;
                            String str39 = str36;
                            mFUser.setDistanceUnit(query.getString(query.getColumnIndex(str38)));
                            String str40 = str34;
                            String str41 = str38;
                            mFUser.setGender(query.getString(query.getColumnIndex(str40)));
                            String str42 = str33;
                            String str43 = str40;
                            mFUser.setBirthday(query.getString(query.getColumnIndex(str42)));
                            String str44 = str32;
                            String str45 = str42;
                            mFUser.setProfilePicture(query.getString(query.getColumnIndex(str44)));
                            String str46 = str31;
                            String str47 = str44;
                            mFUser.setBrand(query.getString(query.getColumnIndex(str46)));
                            String str48 = str30;
                            String str49 = str46;
                            String str50 = str48;
                            mFUser.setRegistrationComplete(query.getInt(query.getColumnIndex(str48)) == 1);
                            String str51 = str29;
                            String str52 = str22;
                            mFUser.setOnboardingComplete(query.getInt(query.getColumnIndex(str51)) == 1);
                            mFUser.setIntegrations(query.getString(query.getColumnIndex(str28)));
                            mFUser.setEmailOptIn(query.getInt(query.getColumnIndex(str27)) == 1);
                            String str53 = str26;
                            String str54 = str51;
                            mFUser.setDiagnosticEnabled(query.getInt(query.getColumnIndex(str53)) == 1);
                            mFUser.setRegisterDate(query.getString(query.getColumnIndex(str24)));
                            mFUser.setPinType(query.getString(query.getColumnIndex("pinType")));
                            mFUser.setAverageSleep(query.getInt(query.getColumnIndex(MFUser.AVERAGE_SLEEP)));
                            mFUser.setAverageStep(query.getInt(query.getColumnIndex(MFUser.AVERAGE_STEP)));
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str55 = str53;
                            String b3 = this.a.a.this$0.TAG;
                            String str56 = str23;
                            wg6.a((Object) b3, str21);
                            local2.d(b3, "Add user=" + mFUser + " to migration user list");
                            arrayList3.add(mFUser);
                            query.moveToNext();
                            str22 = str52;
                            str18 = str41;
                            str19 = str39;
                            str20 = str37;
                            str23 = str56;
                            str25 = str25;
                            str29 = str54;
                            str34 = str43;
                            str26 = str55;
                            str33 = str45;
                            str32 = str47;
                            str31 = str49;
                            str30 = str50;
                        }
                        str2 = str23;
                        str5 = str20;
                        str = str25;
                        str6 = str19;
                        arrayList = arrayList3;
                        str4 = str26;
                        str13 = str29;
                        str3 = str30;
                        str11 = str31;
                        str10 = str32;
                        str9 = str33;
                        str8 = str34;
                        aVar = this;
                        str12 = str22;
                        str7 = str18;
                        query.close();
                    } else {
                        str2 = str23;
                        str5 = str20;
                        str = str25;
                        str6 = str19;
                        arrayList = arrayList3;
                        str4 = str26;
                        str13 = str29;
                        str3 = str30;
                        str11 = str31;
                        str10 = str32;
                        str9 = str33;
                        str8 = str34;
                        aVar = this;
                        str12 = str22;
                        str7 = str18;
                    }
                    aVar.b.execSQL("CREATE TABLE user_copy (uid VARCHAR PRIMARY KEY, userAccessToken VARCHAR, refreshToken VARCHAR, accessTokenExpiresAt VARCHAR, createdAt VARCHAR, updatedAt INTEGER, email INTEGER, authType INTEGER,username VARCHAR, activeDeviceId INTEGER, firstName VARCHAR, lastName VARCHAR, weightInGrams INTEGER, heightInCentimeters INTEGER, heightUnit INTEGER, weightUnit INTEGER,distanceUnit VARCHAR, birthday VARCHAR, gender INTEGER, profilePicture VARCHAR, brand VARCHAR, registrationComplete INTEGER, isOnboardingComplete INTEGER, integrations INTEGER, emailOptIn INTEGER, diagnosticEnabled INTEGER, registerDate INTEGER, pinType VARCHAR, averageSleep INTEGER, averageStep INTEGER);");
                    if (!arrayList.isEmpty()) {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            MFUser mFUser2 = (MFUser) it.next();
                            ContentValues contentValues = new ContentValues();
                            wg6.a((Object) mFUser2, "user");
                            contentValues.put("uid", mFUser2.getUserId());
                            contentValues.put(MFUser.USER_ACCESS_TOKEN, mFUser2.getUserAccessToken());
                            contentValues.put("createdAt", mFUser2.getCreatedAt());
                            contentValues.put("updatedAt", mFUser2.getUpdatedAt());
                            contentValues.put("email", mFUser2.getEmail());
                            lh4 authType = mFUser2.getAuthType();
                            wg6.a((Object) authType, "user.authType");
                            contentValues.put("authType", authType.getValue());
                            contentValues.put("username", mFUser2.getUsername());
                            contentValues.put("activeDeviceId", mFUser2.getActiveDeviceId());
                            contentValues.put("firstName", mFUser2.getFirstName());
                            String str57 = str;
                            contentValues.put(str57, mFUser2.getLastName());
                            String str58 = str2;
                            contentValues.put(str58, Integer.valueOf(mFUser2.getWeightInGrams()));
                            Iterator it2 = it;
                            String str59 = str12;
                            contentValues.put(str59, Integer.valueOf(mFUser2.getHeightInCentimeters()));
                            if (mFUser2.getHeightUnit() != null) {
                                zh4 heightUnit = mFUser2.getHeightUnit();
                                str17 = str59;
                                wg6.a((Object) heightUnit, "user.heightUnit");
                                str14 = heightUnit.getValue();
                            } else {
                                str17 = str59;
                                str14 = zh4.METRIC.getValue();
                            }
                            String str60 = str5;
                            contentValues.put(str60, str14);
                            if (mFUser2.getWeightUnit() != null) {
                                zh4 = mFUser2.getWeightUnit();
                                str16 = str60;
                                wg6.a((Object) zh4, "user.weightUnit");
                            } else {
                                str16 = str60;
                                zh4 = zh4.METRIC;
                            }
                            String str61 = str6;
                            contentValues.put(str61, zh4.getValue());
                            if (mFUser2.getDistanceUnit() != null) {
                                zh42 = mFUser2.getDistanceUnit();
                                str15 = str61;
                                wg6.a((Object) zh42, "user.distanceUnit");
                            } else {
                                str15 = str61;
                                zh42 = zh4.METRIC;
                            }
                            contentValues.put(str7, zh42.getValue());
                            String str62 = str9;
                            contentValues.put(str62, mFUser2.getBirthday());
                            String str63 = str8;
                            String str64 = str62;
                            String str65 = str63;
                            contentValues.put(str65, mFUser2.getGender() != null ? mFUser2.getGender().toString() : "");
                            contentValues.put(str10, mFUser2.getProfilePicture());
                            contentValues.put(str11, mFUser2.getBrand());
                            contentValues.put(str3, Integer.valueOf(mFUser2.isRegistrationComplete() ? 1 : 0));
                            contentValues.put(str13, Integer.valueOf(mFUser2.isOnboardingComplete() ? 1 : 0));
                            contentValues.put(str28, mFUser2.getIntegrationsRaw());
                            contentValues.put(str27, Integer.valueOf(mFUser2.isEmailOptIn() ? 1 : 0));
                            contentValues.put(str4, Integer.valueOf(mFUser2.isDiagnosticEnabled() ? 1 : 0));
                            contentValues.put(str24, mFUser2.getRegisterDate());
                            contentValues.put("pinType", mFUser2.getPinType());
                            contentValues.put(MFUser.AVERAGE_SLEEP, Integer.valueOf(mFUser2.getAverageSleep()));
                            contentValues.put(MFUser.AVERAGE_STEP, Integer.valueOf(mFUser2.getAverageStep()));
                            aVar.b.insert("user_copy", (String) null, contentValues);
                            str = str57;
                            str2 = str58;
                            it = it2;
                            String str66 = str64;
                            str8 = str65;
                            str9 = str66;
                        }
                    }
                    aVar.b.execSQL("DROP TABLE user;");
                    aVar.b.execSQL("ALTER TABLE user_copy RENAME TO user;");
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String b4 = aVar.a.a.this$0.TAG;
                    wg6.a((Object) b4, str21);
                    local3.d(b4, "Migration user complete");
                    return true;
                }
            }

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void execute(SQLiteDatabase sQLiteDatabase) {
                try {
                    DatabaseHelper a2 = this.a.this$0.databaseHelper;
                    wg6.a((Object) a2, "databaseHelper");
                    TransactionManager.callInTransaction(a2.getConnectionSource(), new C0019a(this, sQLiteDatabase));
                } catch (Exception e) {
                    e.printStackTrace();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = this.a.this$0.TAG;
                    wg6.a((Object) b, "TAG");
                    local.d(b, "Exception when migrate user.");
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ko4$b$b")
        /* renamed from: com.fossil.ko4$b$b  reason: collision with other inner class name */
        public static final class C0020b implements UpgradeCommand {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ko4$b$b$a")
            /* renamed from: com.fossil.ko4$b$b$a */
            public static final class a<V> implements Callable<T> {
                @DexIgnore
                public /* final */ /* synthetic */ C0020b a;
                @DexIgnore
                public /* final */ /* synthetic */ SQLiteDatabase b;

                @DexIgnore
                public a(C0020b bVar, SQLiteDatabase sQLiteDatabase) {
                    this.a = bVar;
                    this.b = sQLiteDatabase;
                }

                @DexIgnore
                public final void call() {
                    String str;
                    String str2;
                    String str3;
                    String str4;
                    String str5;
                    String str6;
                    String str7;
                    String str8;
                    String str9;
                    String str10;
                    String str11;
                    String str12;
                    String str13;
                    String str14;
                    a aVar;
                    ArrayList arrayList;
                    zh4 zh4;
                    zh4 zh42;
                    zh4 zh43;
                    String str15;
                    String str16;
                    String str17;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b2 = this.a.a.this$0.TAG;
                    wg6.a((Object) b2, "TAG");
                    local.d(b2, "start migration from db version 3");
                    Cursor query = this.b.query(true, "user", new String[]{"uid", MFUser.USER_ACCESS_TOKEN, "refreshToken", MFUser.ACCESS_TOKEN_EXPIRED_AT, "createdAt", "updatedAt", "email", "authType", "username", "activeDeviceId", "firstName", "lastName", "weightInGrams", "heightInCentimeters", MFUser.HEIGHT_UNIT, MFUser.WEIGHT_UNIT, MFUser.DISTANCE_UNIT, "gender", "birthday", "profilePicture", "brand", "registrationComplete", "isOnboardingComplete", "integrations", "emailOptIn", "diagnosticEnabled", "registerDate", "pinType", MFUser.AVERAGE_STEP, MFUser.AVERAGE_SLEEP}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                    ArrayList arrayList2 = new ArrayList();
                    String str18 = MFUser.HEIGHT_UNIT;
                    String str19 = "heightInCentimeters";
                    ArrayList arrayList3 = arrayList2;
                    String str20 = "weightInGrams";
                    String str21 = "lastName";
                    String str22 = "diagnosticEnabled";
                    String str23 = "firstName";
                    String str24 = "emailOptIn";
                    String str25 = "integrations";
                    String str26 = "isOnboardingComplete";
                    String str27 = "registrationComplete";
                    String str28 = "brand";
                    String str29 = "profilePicture";
                    String str30 = "birthday";
                    String str31 = "gender";
                    String str32 = MFUser.DISTANCE_UNIT;
                    String str33 = MFUser.WEIGHT_UNIT;
                    if (query != null) {
                        query.moveToFirst();
                        while (!query.isAfterLast()) {
                            String str34 = str18;
                            MFUser mFUser = new MFUser();
                            mFUser.setUserId(query.getString(query.getColumnIndex("uid")));
                            mFUser.setUserAccessToken(query.getString(query.getColumnIndex(MFUser.USER_ACCESS_TOKEN)));
                            mFUser.setRefreshToken(query.getString(query.getColumnIndex("refreshToken")));
                            mFUser.setAccessTokenExpiresAt(query.getString(query.getColumnIndex(MFUser.ACCESS_TOKEN_EXPIRED_AT)));
                            mFUser.setCreatedAt(query.getString(query.getColumnIndex("createdAt")));
                            mFUser.setUpdatedAt(query.getString(query.getColumnIndex("updatedAt")));
                            mFUser.setEmail(query.getString(query.getColumnIndex("email")));
                            mFUser.setAuthType(query.getString(query.getColumnIndex("authType")));
                            mFUser.setUsername(query.getString(query.getColumnIndex("username")));
                            mFUser.setActiveDeviceId(query.getString(query.getColumnIndex("activeDeviceId")));
                            mFUser.setFirstName(query.getString(query.getColumnIndex(str23)));
                            mFUser.setLastName(query.getString(query.getColumnIndex(str21)));
                            mFUser.setWeightInGrams(query.getInt(query.getColumnIndex(str20)));
                            String str35 = str19;
                            String str36 = str20;
                            mFUser.setHeightInCentimeters(query.getInt(query.getColumnIndex(str35)));
                            String str37 = str34;
                            String str38 = str35;
                            mFUser.setHeightUnit(query.getString(query.getColumnIndex(str37)));
                            String str39 = str33;
                            String str40 = str37;
                            mFUser.setWeightUnit(query.getString(query.getColumnIndex(str39)));
                            String str41 = str32;
                            String str42 = str39;
                            mFUser.setDistanceUnit(query.getString(query.getColumnIndex(str41)));
                            String str43 = str31;
                            String str44 = str41;
                            mFUser.setGender(query.getString(query.getColumnIndex(str43)));
                            String str45 = str30;
                            String str46 = str43;
                            mFUser.setBirthday(query.getString(query.getColumnIndex(str45)));
                            String str47 = str29;
                            String str48 = str45;
                            mFUser.setProfilePicture(query.getString(query.getColumnIndex(str47)));
                            String str49 = str28;
                            String str50 = str47;
                            mFUser.setBrand(query.getString(query.getColumnIndex(str49)));
                            String str51 = str27;
                            String str52 = str49;
                            String str53 = str51;
                            mFUser.setRegistrationComplete(query.getInt(query.getColumnIndex(str51)) == 1);
                            String str54 = str26;
                            String str55 = str21;
                            mFUser.setOnboardingComplete(query.getInt(query.getColumnIndex(str54)) == 1);
                            mFUser.setIntegrations(query.getString(query.getColumnIndex(str25)));
                            mFUser.setEmailOptIn(query.getInt(query.getColumnIndex(str24)) == 1);
                            String str56 = str22;
                            String str57 = str54;
                            mFUser.setDiagnosticEnabled(query.getInt(query.getColumnIndex(str56)) == 1);
                            mFUser.setRegisterDate(query.getString(query.getColumnIndex("registerDate")));
                            mFUser.setPinType(query.getString(query.getColumnIndex("pinType")));
                            mFUser.setAverageSleep(query.getInt(query.getColumnIndex(MFUser.AVERAGE_SLEEP)));
                            mFUser.setAverageStep(query.getInt(query.getColumnIndex(MFUser.AVERAGE_STEP)));
                            String b3 = this.a.a.this$0.TAG;
                            String str58 = str56;
                            MFLogger.d(b3, "Add user=" + mFUser + " to migration user list");
                            arrayList3.add(mFUser);
                            query.moveToNext();
                            str21 = str55;
                            str18 = str40;
                            str19 = str38;
                            str20 = str36;
                            str23 = str23;
                            str26 = str57;
                            str33 = str42;
                            str22 = str58;
                            str32 = str44;
                            str31 = str46;
                            str30 = str48;
                            str29 = str50;
                            str28 = str52;
                            str27 = str53;
                        }
                        str4 = str20;
                        str = str23;
                        str5 = str19;
                        str3 = str22;
                        str14 = str26;
                        str2 = str27;
                        str12 = str28;
                        str11 = str29;
                        str10 = str30;
                        str9 = str31;
                        str8 = str32;
                        str7 = str33;
                        aVar = this;
                        str13 = str21;
                        str6 = str18;
                        arrayList = arrayList3;
                        query.close();
                    } else {
                        str4 = str20;
                        str = str23;
                        str5 = str19;
                        str3 = str22;
                        str14 = str26;
                        str2 = str27;
                        str12 = str28;
                        str11 = str29;
                        str10 = str30;
                        str9 = str31;
                        str8 = str32;
                        str7 = str33;
                        aVar = this;
                        str13 = str21;
                        str6 = str18;
                        arrayList = arrayList3;
                    }
                    aVar.b.execSQL("CREATE TABLE user_copy (uid VARCHAR PRIMARY KEY, userAccessToken VARCHAR, refreshToken VARCHAR, accessTokenExpiresAt VARCHAR, createdAt VARCHAR, updatedAt INTEGER, email INTEGER, authType INTEGER,username VARCHAR, activeDeviceId INTEGER, firstName VARCHAR, lastName VARCHAR, weightInGrams INTEGER, heightInCentimeters INTEGER, heightUnit INTEGER, weightUnit INTEGER,distanceUnit VARCHAR, birthday VARCHAR, gender INTEGER, profilePicture VARCHAR, brand VARCHAR, registrationComplete INTEGER, isOnboardingComplete INTEGER, integrations INTEGER, emailOptIn INTEGER, diagnosticEnabled INTEGER, registerDate INTEGER, pinType VARCHAR, averageSleep INTEGER, averageStep INTEGER);");
                    if (!arrayList.isEmpty()) {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            MFUser mFUser2 = (MFUser) it.next();
                            ContentValues contentValues = new ContentValues();
                            wg6.a((Object) mFUser2, "user");
                            contentValues.put("uid", mFUser2.getUserId());
                            contentValues.put(MFUser.USER_ACCESS_TOKEN, mFUser2.getUserAccessToken());
                            contentValues.put("refreshToken", mFUser2.getRefreshToken());
                            contentValues.put(MFUser.ACCESS_TOKEN_EXPIRED_AT, query.getString(query.getColumnIndex(MFUser.ACCESS_TOKEN_EXPIRED_AT)));
                            contentValues.put("createdAt", mFUser2.getCreatedAt());
                            contentValues.put("updatedAt", mFUser2.getUpdatedAt());
                            contentValues.put("email", mFUser2.getEmail());
                            lh4 authType = mFUser2.getAuthType();
                            Iterator it2 = it;
                            wg6.a((Object) authType, "user.authType");
                            contentValues.put("authType", authType.getValue());
                            contentValues.put("username", mFUser2.getUsername());
                            contentValues.put("activeDeviceId", mFUser2.getActiveDeviceId());
                            String str59 = str;
                            contentValues.put(str59, mFUser2.getFirstName());
                            Cursor cursor = query;
                            contentValues.put(str13, mFUser2.getLastName());
                            contentValues.put(str4, Integer.valueOf(mFUser2.getWeightInGrams()));
                            String str60 = str5;
                            contentValues.put(str60, Integer.valueOf(mFUser2.getHeightInCentimeters()));
                            if (mFUser2.getHeightUnit() != null) {
                                zh4 = mFUser2.getHeightUnit();
                                str17 = str60;
                                wg6.a((Object) zh4, "user.heightUnit");
                            } else {
                                str17 = str60;
                                zh4 = zh4.METRIC;
                            }
                            String str61 = str6;
                            contentValues.put(str61, zh4.getValue());
                            if (mFUser2.getWeightUnit() != null) {
                                zh42 = mFUser2.getWeightUnit();
                                str16 = str61;
                                wg6.a((Object) zh42, "user.weightUnit");
                            } else {
                                str16 = str61;
                                zh42 = zh4.METRIC;
                            }
                            String str62 = str7;
                            contentValues.put(str62, zh42.getValue());
                            if (mFUser2.getDistanceUnit() != null) {
                                zh43 = mFUser2.getDistanceUnit();
                                str15 = str62;
                                wg6.a((Object) zh43, "user.distanceUnit");
                            } else {
                                str15 = str62;
                                zh43 = zh4.METRIC;
                            }
                            contentValues.put(str8, zh43.getValue());
                            String str63 = str10;
                            contentValues.put(str63, mFUser2.getBirthday());
                            String str64 = str9;
                            String str65 = str63;
                            String str66 = str64;
                            contentValues.put(str66, mFUser2.getGender() != null ? mFUser2.getGender().toString() : "");
                            String str67 = str66;
                            contentValues.put(str11, mFUser2.getProfilePicture());
                            contentValues.put(str12, mFUser2.getBrand());
                            contentValues.put(str2, Integer.valueOf(mFUser2.isRegistrationComplete() ? 1 : 0));
                            contentValues.put(str14, Integer.valueOf(mFUser2.isOnboardingComplete() ? 1 : 0));
                            contentValues.put(str25, mFUser2.getIntegrationsRaw());
                            contentValues.put(str24, Integer.valueOf(mFUser2.isEmailOptIn() ? 1 : 0));
                            contentValues.put(str3, Integer.valueOf(mFUser2.isDiagnosticEnabled() ? 1 : 0));
                            contentValues.put("registerDate", mFUser2.getRegisterDate());
                            contentValues.put("pinType", mFUser2.getPinType());
                            contentValues.put(MFUser.AVERAGE_SLEEP, Integer.valueOf(mFUser2.getAverageSleep()));
                            contentValues.put(MFUser.AVERAGE_STEP, Integer.valueOf(mFUser2.getAverageStep()));
                            aVar.b.insert("user_copy", (String) null, contentValues);
                            str = str59;
                            it = it2;
                            query = cursor;
                            String str68 = str65;
                            str9 = str67;
                            str10 = str68;
                        }
                    }
                    aVar.b.execSQL("DROP TABLE user;");
                    aVar.b.execSQL("ALTER TABLE user_copy RENAME TO user;");
                }
            }

            @DexIgnore
            public C0020b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void execute(SQLiteDatabase sQLiteDatabase) {
                DatabaseHelper a2 = this.a.this$0.databaseHelper;
                wg6.a((Object) a2, "databaseHelper");
                TransactionManager.callInTransaction(a2.getConnectionSource(), new a(this, sQLiteDatabase));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements UpgradeCommand {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a<V> implements Callable<T> {
                @DexIgnore
                public /* final */ /* synthetic */ c a;
                @DexIgnore
                public /* final */ /* synthetic */ SQLiteDatabase b;

                @DexIgnore
                public a(c cVar, SQLiteDatabase sQLiteDatabase) {
                    this.a = cVar;
                    this.b = sQLiteDatabase;
                }

                @DexIgnore
                public final void call() {
                    String str;
                    String str2;
                    String str3;
                    String str4;
                    String str5;
                    String str6;
                    String str7;
                    String str8;
                    String str9;
                    String str10;
                    ArrayList arrayList;
                    zh4 zh4;
                    zh4 zh42;
                    zh4 zh43;
                    String str11;
                    String str12;
                    String str13;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b2 = this.a.a.this$0.TAG;
                    wg6.a((Object) b2, "TAG");
                    local.d(b2, "start migration from db version 4");
                    Cursor query = this.b.query(true, "user", new String[]{MFUser.USER_ACCESS_TOKEN, "refreshToken", MFUser.ACCESS_TOKEN_EXPIRED_AT, "uid", "createdAt", "updatedAt", "email", "authType", "username", "activeDeviceId", "firstName", "lastName", "weightInGrams", "heightInCentimeters", MFUser.HEIGHT_UNIT, MFUser.WEIGHT_UNIT, MFUser.DISTANCE_UNIT, "gender", "birthday", "profilePicture", "brand", "registrationComplete", "isOnboardingComplete", "integrations", "emailOptIn", "diagnosticEnabled", "pinType", "registerDate", MFUser.AVERAGE_STEP, MFUser.AVERAGE_SLEEP}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                    ArrayList arrayList2 = new ArrayList();
                    String str14 = MFUser.HEIGHT_UNIT;
                    String str15 = "heightInCentimeters";
                    String str16 = "weightInGrams";
                    String str17 = "username";
                    ArrayList arrayList3 = arrayList2;
                    String str18 = "authType";
                    String str19 = "registrationComplete";
                    String str20 = "email";
                    String str21 = "brand";
                    String str22 = "profilePicture";
                    String str23 = "birthday";
                    String str24 = "gender";
                    String str25 = MFUser.DISTANCE_UNIT;
                    String str26 = MFUser.WEIGHT_UNIT;
                    if (query != null) {
                        query.moveToFirst();
                        while (!query.isAfterLast()) {
                            String str27 = str14;
                            MFUser mFUser = new MFUser();
                            mFUser.setUserAccessToken(query.getString(query.getColumnIndex(MFUser.USER_ACCESS_TOKEN)));
                            mFUser.setRefreshToken(query.getString(query.getColumnIndex("refreshToken")));
                            mFUser.setAccessTokenExpiresAt(query.getString(query.getColumnIndex(MFUser.ACCESS_TOKEN_EXPIRED_AT)));
                            mFUser.setUserId(query.getString(query.getColumnIndex("uid")));
                            mFUser.setCreatedAt(query.getString(query.getColumnIndex("createdAt")));
                            mFUser.setUpdatedAt(query.getString(query.getColumnIndex("updatedAt")));
                            mFUser.setEmail(query.getString(query.getColumnIndex(str20)));
                            mFUser.setAuthType(query.getString(query.getColumnIndex(str18)));
                            mFUser.setUsername(query.getString(query.getColumnIndex(str17)));
                            mFUser.setActiveDeviceId(query.getString(query.getColumnIndex("activeDeviceId")));
                            mFUser.setFirstName(query.getString(query.getColumnIndex("firstName")));
                            mFUser.setLastName(query.getString(query.getColumnIndex("lastName")));
                            mFUser.setWeightInGrams(query.getInt(query.getColumnIndex(str16)));
                            String str28 = str15;
                            String str29 = str16;
                            mFUser.setHeightInCentimeters(query.getInt(query.getColumnIndex(str28)));
                            String str30 = str27;
                            String str31 = str28;
                            mFUser.setHeightUnit(query.getString(query.getColumnIndex(str30)));
                            String str32 = str26;
                            String str33 = str30;
                            mFUser.setWeightUnit(query.getString(query.getColumnIndex(str32)));
                            String str34 = str25;
                            String str35 = str32;
                            mFUser.setDistanceUnit(query.getString(query.getColumnIndex(str34)));
                            String str36 = str24;
                            String str37 = str34;
                            mFUser.setGender(query.getString(query.getColumnIndex(str36)));
                            String str38 = str23;
                            String str39 = str36;
                            mFUser.setBirthday(query.getString(query.getColumnIndex(str38)));
                            String str40 = str22;
                            String str41 = str38;
                            mFUser.setProfilePicture(query.getString(query.getColumnIndex(str40)));
                            String str42 = str21;
                            String str43 = str40;
                            mFUser.setBrand(query.getString(query.getColumnIndex(str42)));
                            String str44 = str19;
                            String str45 = str42;
                            String str46 = str44;
                            mFUser.setRegistrationComplete(query.getInt(query.getColumnIndex(str44)) == 1);
                            mFUser.setOnboardingComplete(query.getInt(query.getColumnIndex("isOnboardingComplete")) == 1);
                            mFUser.setIntegrations(query.getString(query.getColumnIndex("integrations")));
                            mFUser.setEmailOptIn(query.getInt(query.getColumnIndex("emailOptIn")) == 1);
                            mFUser.setDiagnosticEnabled(query.getInt(query.getColumnIndex("diagnosticEnabled")) == 1);
                            mFUser.setRegisterDate(query.getString(query.getColumnIndex("registerDate")));
                            mFUser.setPinType(query.getString(query.getColumnIndex("pinType")));
                            mFUser.setAverageSleep(query.getInt(query.getColumnIndex(MFUser.AVERAGE_SLEEP)));
                            mFUser.setAverageStep(query.getInt(query.getColumnIndex(MFUser.AVERAGE_STEP)));
                            arrayList3.add(mFUser);
                            query.moveToNext();
                            str14 = str33;
                            str15 = str31;
                            str16 = str29;
                            str26 = str35;
                            str25 = str37;
                            str24 = str39;
                            str23 = str41;
                            str22 = str43;
                            str21 = str45;
                            str19 = str46;
                        }
                        str3 = str15;
                        str2 = str16;
                        arrayList = arrayList3;
                        str = str19;
                        str10 = str21;
                        str9 = str22;
                        str8 = str23;
                        str7 = str24;
                        str6 = str25;
                        str5 = str26;
                        str4 = str14;
                        query.close();
                    } else {
                        str3 = str15;
                        str2 = str16;
                        arrayList = arrayList3;
                        str = str19;
                        str10 = str21;
                        str9 = str22;
                        str8 = str23;
                        str7 = str24;
                        str6 = str25;
                        str5 = str26;
                        str4 = str14;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("CREATE TABLE user_copy (uid VARCHAR PRIMARY KEY, userAccessToken VARCHAR, refreshToken VARCHAR, accessTokenExpiresAt VARCHAR, accessTokenExpiresIn INTEGER, createdAt VARCHAR, updatedAt INTEGER, email INTEGER, authType INTEGER,username VARCHAR, activeDeviceId INTEGER, firstName VARCHAR, lastName VARCHAR, weightInGrams INTEGER, heightInCentimeters INTEGER, heightUnit INTEGER, weightUnit INTEGER,distanceUnit VARCHAR, birthday VARCHAR, gender INTEGER, profilePicture VARCHAR, brand VARCHAR, registrationComplete INTEGER, isOnboardingComplete INTEGER, integrations INTEGER, emailOptIn INTEGER, diagnosticEnabled INTEGER, registerDate INTEGER, pinType VARCHAR, averageSleep INTEGER, averageStep INTEGER, temperatureUnit VARCHAR DEFAULT '");
                    sb.append(zh4.METRIC.getValue());
                    sb.append("', ");
                    sb.append(MFUser.USE_DEFAULT_GOALS);
                    sb.append(" INTEGER DEFAULT 0, ");
                    sb.append(MFUser.USE_DEFAULT_BIOMETRIC);
                    sb.append(" INTEGER DEFAULT 0, ");
                    sb.append("home");
                    sb.append(" VARCHAR, ");
                    sb.append("work");
                    String str47 = "work";
                    sb.append(" VARCHAR");
                    sb.append(");");
                    String str48 = "home";
                    this.b.execSQL(sb.toString());
                    if (!arrayList.isEmpty()) {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            MFUser mFUser2 = (MFUser) it.next();
                            ContentValues contentValues = new ContentValues();
                            Iterator it2 = it;
                            wg6.a((Object) mFUser2, "user");
                            contentValues.put("uid", mFUser2.getUserId());
                            contentValues.put(MFUser.USER_ACCESS_TOKEN, mFUser2.getUserAccessToken());
                            contentValues.put(MFUser.ACCESS_TOKEN_EXPIRED_AT, mFUser2.getAccessTokenExpiresAt());
                            contentValues.put("refreshToken", mFUser2.getRefreshToken());
                            contentValues.put("createdAt", mFUser2.getCreatedAt());
                            contentValues.put("updatedAt", mFUser2.getUpdatedAt());
                            contentValues.put(str20, mFUser2.getEmail());
                            lh4 authType = mFUser2.getAuthType();
                            String str49 = str20;
                            wg6.a((Object) authType, "user.authType");
                            contentValues.put(str18, authType.getValue());
                            contentValues.put(str17, mFUser2.getUsername());
                            contentValues.put("activeDeviceId", mFUser2.getActiveDeviceId());
                            contentValues.put("firstName", mFUser2.getFirstName());
                            contentValues.put("lastName", mFUser2.getLastName());
                            String str50 = str2;
                            contentValues.put(str50, Integer.valueOf(mFUser2.getWeightInGrams()));
                            String str51 = str17;
                            String str52 = str3;
                            contentValues.put(str52, Integer.valueOf(mFUser2.getHeightInCentimeters()));
                            if (mFUser2.getHeightUnit() != null) {
                                zh4 = mFUser2.getHeightUnit();
                                str13 = str52;
                                wg6.a((Object) zh4, "user.heightUnit");
                            } else {
                                str13 = str52;
                                zh4 = zh4.METRIC;
                            }
                            String value = zh4.getValue();
                            String str53 = str4;
                            contentValues.put(str53, value);
                            if (mFUser2.getWeightUnit() != null) {
                                zh42 = mFUser2.getWeightUnit();
                                str12 = str53;
                                wg6.a((Object) zh42, "user.weightUnit");
                            } else {
                                str12 = str53;
                                zh42 = zh4.METRIC;
                            }
                            String str54 = str5;
                            contentValues.put(str54, zh42.getValue());
                            if (mFUser2.getDistanceUnit() != null) {
                                zh43 = mFUser2.getDistanceUnit();
                                str11 = str54;
                                wg6.a((Object) zh43, "user.distanceUnit");
                            } else {
                                str11 = str54;
                                zh43 = zh4.METRIC;
                            }
                            contentValues.put(str6, zh43.getValue());
                            contentValues.put(str8, mFUser2.getBirthday());
                            String str55 = str7;
                            String str56 = str18;
                            String str57 = str55;
                            contentValues.put(str57, mFUser2.getGender() != null ? mFUser2.getGender().toString() : "");
                            String str58 = str57;
                            contentValues.put(str9, mFUser2.getProfilePicture());
                            contentValues.put(str10, mFUser2.getBrand());
                            contentValues.put(str, Integer.valueOf(mFUser2.isRegistrationComplete() ? 1 : 0));
                            contentValues.put("isOnboardingComplete", Integer.valueOf(mFUser2.isOnboardingComplete() ? 1 : 0));
                            contentValues.put("integrations", mFUser2.getIntegrationsRaw());
                            contentValues.put("emailOptIn", Integer.valueOf(mFUser2.isEmailOptIn() ? 1 : 0));
                            contentValues.put("diagnosticEnabled", Integer.valueOf(mFUser2.isDiagnosticEnabled() ? 1 : 0));
                            contentValues.put("registerDate", mFUser2.getRegisterDate());
                            contentValues.put("pinType", mFUser2.getPinType());
                            contentValues.put(MFUser.AVERAGE_SLEEP, Integer.valueOf(mFUser2.getAverageSleep()));
                            contentValues.put(MFUser.AVERAGE_STEP, Integer.valueOf(mFUser2.getAverageStep()));
                            contentValues.put(str48, "");
                            contentValues.put(str47, "");
                            contentValues.put(MFUser.ACCESS_TOKEN_EXPIRED_IN, Integer.valueOf(DateTimeConstants.SECONDS_PER_DAY));
                            this.b.insert("user_copy", (String) null, contentValues);
                            it = it2;
                            str18 = str56;
                            str17 = str51;
                            str7 = str58;
                            str2 = str50;
                            str20 = str49;
                        }
                    }
                    this.b.execSQL("DROP TABLE user;");
                    this.b.execSQL("ALTER TABLE user_copy RENAME TO user;");
                }
            }

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void execute(SQLiteDatabase sQLiteDatabase) {
                DatabaseHelper a2 = this.a.this$0.databaseHelper;
                wg6.a((Object) a2, "databaseHelper");
                TransactionManager.callInTransaction(a2.getConnectionSource(), new a(this, sQLiteDatabase));
            }
        }

        @DexIgnore
        public b(ko4 ko4) {
            this.this$0 = ko4;
            put(2, new a(this));
            put(3, new C0020b(this));
            put(4, new c(this));
        }

        @DexIgnore
        public /* bridge */ boolean containsKey(Integer num) {
            return super.containsKey(num);
        }

        @DexIgnore
        public /* bridge */ boolean containsValue(UpgradeCommand upgradeCommand) {
            return super.containsValue(upgradeCommand);
        }

        @DexIgnore
        public final /* bridge */ Set<Map.Entry<Integer, UpgradeCommand>> entrySet() {
            return getEntries();
        }

        @DexIgnore
        public /* bridge */ UpgradeCommand get(Integer num) {
            return (UpgradeCommand) super.get(num);
        }

        @DexIgnore
        public /* bridge */ Set getEntries() {
            return super.entrySet();
        }

        @DexIgnore
        public /* bridge */ Set getKeys() {
            return super.keySet();
        }

        @DexIgnore
        public /* bridge */ UpgradeCommand getOrDefault(Integer num, UpgradeCommand upgradeCommand) {
            return (UpgradeCommand) super.getOrDefault(num, upgradeCommand);
        }

        @DexIgnore
        public /* bridge */ int getSize() {
            return super.size();
        }

        @DexIgnore
        public /* bridge */ Collection getValues() {
            return super.values();
        }

        @DexIgnore
        public final /* bridge */ Set<Integer> keySet() {
            return getKeys();
        }

        @DexIgnore
        public /* bridge */ UpgradeCommand remove(Integer num) {
            return (UpgradeCommand) super.remove(num);
        }

        @DexIgnore
        public final /* bridge */ int size() {
            return getSize();
        }

        @DexIgnore
        public final /* bridge */ Collection<UpgradeCommand> values() {
            return getValues();
        }

        @DexIgnore
        public final /* bridge */ boolean containsKey(Object obj) {
            if (obj instanceof Integer) {
                return containsKey((Integer) obj);
            }
            return false;
        }

        @DexIgnore
        public final /* bridge */ boolean containsValue(Object obj) {
            if (obj instanceof UpgradeCommand) {
                return containsValue((UpgradeCommand) obj);
            }
            return false;
        }

        @DexIgnore
        public final /* bridge */ Object get(Object obj) {
            if (obj instanceof Integer) {
                return get((Integer) obj);
            }
            return null;
        }

        @DexIgnore
        public final /* bridge */ Object getOrDefault(Object obj, Object obj2) {
            return obj != null ? obj instanceof Integer : true ? getOrDefault((Integer) obj, (UpgradeCommand) obj2) : obj2;
        }

        @DexIgnore
        public final /* bridge */ Object remove(Object obj) {
            if (obj instanceof Integer) {
                return remove((Integer) obj);
            }
            return null;
        }

        @DexIgnore
        public /* bridge */ boolean remove(Integer num, UpgradeCommand upgradeCommand) {
            return super.remove(num, upgradeCommand);
        }

        @DexIgnore
        public final /* bridge */ boolean remove(Object obj, Object obj2) {
            boolean z = true;
            if (!(obj != null ? obj instanceof Integer : true)) {
                return false;
            }
            if (obj2 != null) {
                z = obj2 instanceof UpgradeCommand;
            }
            if (z) {
                return remove((Integer) obj, (UpgradeCommand) obj2);
            }
            return false;
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ko4(Context context, String str) {
        super(context, str);
        wg6.b(context, "context");
        wg6.b(str, "dbPath");
    }

    @DexIgnore
    public void c(MFUser mFUser) {
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            wg6.a((Object) str, "TAG");
            local.e(str, "Inside .deleteUser user=" + mFUser);
            try {
                g().delete(mFUser);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                wg6.a((Object) str2, "TAG");
                local2.e(str2, "Inside .deleteUser exception=" + e);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            wg6.a((Object) str3, "TAG");
            local3.e(str3, "Inside .deleteUser error user is null");
        }
    }

    @DexIgnore
    public void f() {
        try {
            TableUtils.clearTable(g().getConnectionSource(), MFUser.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public final Dao<MFUser, Integer> g() throws SQLException {
        Dao<MFUser, Integer> dao = this.databaseHelper.getDao(MFUser.class);
        wg6.a((Object) dao, "databaseHelper.getDao(MFUser::class.java)");
        return dao;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{MFUser.class};
    }

    @DexIgnore
    public String getDbPath() {
        DatabaseHelper databaseHelper = this.databaseHelper;
        wg6.a((Object) databaseHelper, "databaseHelper");
        String dbPath = databaseHelper.getDbPath();
        wg6.a((Object) dbPath, "databaseHelper.dbPath");
        return dbPath;
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new b(this);
    }

    @DexIgnore
    public int getDbVersion() {
        return 4;
    }

    @DexIgnore
    public void a(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        wg6.a((Object) str, "TAG");
        local.d(str, "Inside .updateUser " + mFUser);
        if (mFUser != null) {
            try {
                g().update(mFUser);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                wg6.a((Object) str2, "TAG");
                local2.d(str2, "Inside .updateUser exception=" + e);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            wg6.a((Object) str3, "TAG");
            local3.d(str3, "Inside .updateUser error user is null");
        }
    }

    @DexIgnore
    public void b(MFUser mFUser) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        wg6.a((Object) str, "TAG");
        local.d(str, "Inside .insertUser " + mFUser);
        if (mFUser != null) {
            try {
                g().createIfNotExists(mFUser);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                wg6.a((Object) str2, "TAG");
                local2.d(str2, "Inside .insertUser exception=" + e);
            }
        } else {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            wg6.a((Object) str3, "TAG");
            local3.d(str3, "Inside .insertUser error user is null");
        }
    }

    @DexIgnore
    public MFUser b() {
        try {
            List<MFUser> queryForAll = g().queryForAll();
            if (queryForAll == null || !(!queryForAll.isEmpty())) {
                return null;
            }
            return queryForAll.get(0);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            wg6.a((Object) str, "TAG");
            local.e(str, "Inside .getUser exception=" + e);
            return null;
        }
    }
}
