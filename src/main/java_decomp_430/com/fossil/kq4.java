package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq4 implements Factory<iq4> {
    @DexIgnore
    public /* final */ Provider<an4> a;
    @DexIgnore
    public /* final */ Provider<DNDSettingsDatabase> b;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> d;

    @DexIgnore
    public kq4(Provider<an4> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static kq4 a(Provider<an4> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        return new kq4(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static iq4 b(Provider<an4> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        return new DianaNotificationComponent(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public DianaNotificationComponent get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
