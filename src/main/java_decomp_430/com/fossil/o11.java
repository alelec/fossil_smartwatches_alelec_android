package com.fossil;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o11 extends f51 {
    @DexIgnore
    public static /* final */ tz0 CREATOR; // = new tz0((qg6) null);
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public o11(vd0 vd0, w40 w40, boolean z) {
        super(vd0, w40);
        this.c = z;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.SHIP_HANDS_TO_TWELVE, (Object) Integer.valueOf(this.c ? 1 : 0));
    }

    @DexIgnore
    public List<uj1> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new tg0());
        if (this.c) {
            arrayList.add(new dw0(new ge1[]{new ge1(qn0.HOUR, dn1.SHORTEST_PATH, ip0.POSITION, ar0.FULL, 0), new ge1(qn0.HOUR, dn1.SHORTEST_PATH, ip0.POSITION, ar0.FULL, 0)}));
        }
        arrayList.add(new nu0(ts0.ERROR));
        arrayList.add(new rz0());
        return arrayList;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(o11.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((o11) obj).c;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppErrorResponse");
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.c).hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }

    @DexIgnore
    public /* synthetic */ o11(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.c = parcel.readInt() != 0;
    }
}
