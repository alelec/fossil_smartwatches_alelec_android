package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ev2 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService e;

    @DexIgnore
    public ba3(AppMeasurementDynamiteService appMeasurementDynamiteService, ev2 ev2, String str, String str2, boolean z) {
        this.e = appMeasurementDynamiteService;
        this.a = ev2;
        this.b = str;
        this.c = str2;
        this.d = z;
    }

    @DexIgnore
    public final void run() {
        this.e.a.F().a(this.a, this.b, this.c, this.d);
    }
}
