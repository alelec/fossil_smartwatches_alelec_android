package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ly2 {
    @DexIgnore
    public static /* final */ String a; // = "ly2";
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context b;
    @DexIgnore
    public static oy2 c;

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static oy2 a(Context context) throws lv1 {
        oy2 oy2;
        w12.a(context);
        oy2 oy22 = c;
        if (oy22 != null) {
            return oy22;
        }
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context, 13400000);
        if (isGooglePlayServicesAvailable == 0) {
            Log.i(a, "Making Creator dynamically");
            IBinder iBinder = (IBinder) a(b(context).getClassLoader(), "com.google.android.gms.maps.internal.CreatorImpl");
            if (iBinder == null) {
                oy2 = null;
            } else {
                Object queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.internal.ICreator");
                if (queryLocalInterface instanceof oy2) {
                    oy2 = queryLocalInterface;
                } else {
                    oy2 = new py2(iBinder);
                }
            }
            c = oy2;
            try {
                c.a(z52.a(b(context).getResources()), GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
                return c;
            } catch (RemoteException e) {
                throw new bz2(e);
            }
        } else {
            throw new lv1(isGooglePlayServicesAvailable);
        }
    }

    @DexIgnore
    public static Context b(Context context) {
        Context context2 = b;
        if (context2 != null) {
            return context2;
        }
        Context c2 = c(context);
        b = c2;
        return c2;
    }

    @DexIgnore
    public static Context c(Context context) {
        try {
            return DynamiteModule.a(context, DynamiteModule.i, "com.google.android.gms.maps_dynamite").a();
        } catch (Exception e) {
            Log.e(a, "Failed to load maps module, use legacy", e);
            return GooglePlayServicesUtil.getRemoteContext(context);
        }
    }

    @DexIgnore
    public static <T> T a(ClassLoader classLoader, String str) {
        try {
            w12.a(classLoader);
            return a(classLoader.loadClass(str));
        } catch (ClassNotFoundException unused) {
            String valueOf = String.valueOf(str);
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to find dynamic class ".concat(valueOf) : new String("Unable to find dynamic class "));
        }
    }

    @DexIgnore
    public static <T> T a(Class<?> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException unused) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to instantiate the dynamic class ".concat(valueOf) : new String("Unable to instantiate the dynamic class "));
        } catch (IllegalAccessException unused2) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf2.length() != 0 ? "Unable to call the default constructor of ".concat(valueOf2) : new String("Unable to call the default constructor of "));
        }
    }
}
