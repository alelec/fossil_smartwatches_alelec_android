package com.fossil;

import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y85$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter.c this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ y85$c$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(y85$c$a y85_c_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = y85_c_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                HybridPresetRepository e = this.this$0.this$0.c.p;
                HybridPreset hybridPreset = this.this$0.this$0.a;
                if (hybridPreset != null) {
                    String id = hybridPreset.getId();
                    this.L$0 = il6;
                    this.label = 1;
                    if (e.deletePresetById(id, this) == a) {
                        return a;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y85$c$a(HomeHybridCustomizePresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        y85$c$a y85_c_a = new y85$c$a(this.this$0, xe6);
        y85_c_a.p$ = (il6) obj;
        return y85_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((y85$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 b = this.this$0.c.c();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            if (gk6.a(b, aVar, this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        this.this$0.c.n.n();
        this.this$0.c.n.d(this.this$0.c.i());
        this.this$0.c.r.q(true);
        this.this$0.c.r.r(true);
        HomeHybridCustomizePresenter.c cVar = this.this$0;
        boolean unused = cVar.c.b(cVar.b);
        return cd6.a;
    }
}
