package com.fossil;

import com.fossil.zf;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av4 extends zf.d<GoalTrackingSummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        wg6.b(goalTrackingSummary, "oldItem");
        wg6.b(goalTrackingSummary2, "newItem");
        return wg6.a((Object) goalTrackingSummary, (Object) goalTrackingSummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        wg6.b(goalTrackingSummary, "oldItem");
        wg6.b(goalTrackingSummary2, "newItem");
        return bk4.d(goalTrackingSummary.getDate(), goalTrackingSummary2.getDate());
    }
}
