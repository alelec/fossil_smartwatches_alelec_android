package com.fossil;

import android.util.Log;
import com.fossil.g36;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h36 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ g36.b a;

    @DexIgnore
    public h36(g36.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public void run() {
        if (g36.e != null && this.a.a) {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onBackground");
            p36.a(this.a.c, "onBackground_WX", (Properties) null);
            boolean unused = this.a.a = false;
        }
    }
}
