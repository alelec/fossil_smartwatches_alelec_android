package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l11 extends hh<ie1> {
    @DexIgnore
    public l11(w81 w81, oh ohVar) {
        super(ohVar);
    }

    @DexIgnore
    public void bind(mi miVar, Object obj) {
        ie1 ie1 = (ie1) obj;
        miVar.a(1, (long) ie1.a);
        String str = ie1.b;
        if (str == null) {
            miVar.a(2);
        } else {
            miVar.a(2, str);
        }
        miVar.a(3, (long) ie1.c);
        miVar.a(4, (long) ie1.d);
        byte[] bArr = ie1.e;
        if (bArr == null) {
            miVar.a(5);
        } else {
            miVar.a(5, bArr);
        }
        miVar.a(6, ie1.f);
        miVar.a(7, ie1.g);
        miVar.a(8, ie1.b());
        miVar.a(9, ie1.d() ? 1 : 0);
    }

    @DexIgnore
    public String createQuery() {
        return "INSERT OR REPLACE INTO `DeviceFile` (`id`,`deviceMacAddress`,`fileType`,`fileIndex`,`rawData`,`fileLength`,`fileCrc`,`createdTimeStamp`,`isCompleted`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?)";
    }
}
