package com.fossil;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageButton s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ RelativeLayout u;
    @DexIgnore
    public /* final */ ViewPager2 v;
    @DexIgnore
    public /* final */ TabLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bb4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton, ImageButton imageButton, RTLImageView rTLImageView, ProgressBar progressBar, RelativeLayout relativeLayout, ViewPager2 viewPager2, TabLayout tabLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = imageButton;
        this.t = rTLImageView;
        this.u = relativeLayout;
        this.v = viewPager2;
        this.w = tabLayout;
        this.x = flexibleTextView4;
    }
}
