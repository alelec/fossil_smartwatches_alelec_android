package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp6 {
    @DexIgnore
    public static final <T> void a(hg6<? super xe6<? super T>, ? extends Object> hg6, xe6<? super T> xe6) {
        wg6.b(hg6, "$this$startCoroutineCancellable");
        wg6.b(xe6, "completion");
        try {
            xl6.a(ef6.a(ef6.a(hg6, xe6)), cd6.a);
        } catch (Throwable th) {
            mc6.a aVar = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6, R r, xe6<? super T> xe6) {
        wg6.b(ig6, "$this$startCoroutineCancellable");
        wg6.b(xe6, "completion");
        try {
            xl6.a(ef6.a(ef6.a(ig6, r, xe6)), cd6.a);
        } catch (Throwable th) {
            mc6.a aVar = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        }
    }
}
