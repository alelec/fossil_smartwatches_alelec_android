package com.fossil;

import com.facebook.internal.NativeProtocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ao0 {
    SUCCESS(0),
    UNSUPPORTED_FILE_HANDLE(1),
    REQUEST_NOT_SUPPORTED(6),
    START_FAIL(65536),
    HID_PROXY_NOT_CONNECTED(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY),
    HID_FAIL_TO_INVOKE_PRIVATE_METHOD(NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REQUEST),
    HID_INPUT_DEVICE_DISABLED(NativeProtocol.MESSAGE_GET_PROTOCOL_VERSIONS_REPLY),
    HID_UNKNOWN_ERROR(NativeProtocol.MESSAGE_GET_INSTALL_DATA_REQUEST),
    BLUETOOTH_OFF(16777215),
    UNKNOWN(16777215);
    
    @DexIgnore
    public static /* final */ im0 m; // = null;
    @DexIgnore
    public /* final */ int a;

    /*
    static {
        m = new im0((qg6) null);
    }
    */

    @DexIgnore
    public ao0(int i) {
        this.a = i;
    }
}
