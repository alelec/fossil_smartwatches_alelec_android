package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr implements Closeable {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public long f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public long h; // = 0;
    @DexIgnore
    public Writer i;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> j; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int o;
    @DexIgnore
    public long p; // = 0;
    @DexIgnore
    public /* final */ ThreadPoolExecutor q; // = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new b((a) null));
    @DexIgnore
    public /* final */ Callable<Void> r; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {
        @DexIgnore
        public a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
            return null;
         */
        @DexIgnore
        public Void call() throws Exception {
            synchronized (hr.this) {
                if (hr.this.i == null) {
                    return null;
                }
                hr.this.B();
                if (hr.this.m()) {
                    hr.this.p();
                    int unused = hr.this.o = 0;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ThreadFactory {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public synchronized Thread newThread(Runnable runnable) {
            Thread thread;
            thread = new Thread(runnable, "glide-disk-lru-cache-thread");
            thread.setPriority(1);
            return thread;
        }

        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {
        @DexIgnore
        public /* final */ d a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public /* synthetic */ c(hr hrVar, d dVar, a aVar) {
            this(dVar);
        }

        @DexIgnore
        public void c() throws IOException {
            hr.this.a(this, true);
            this.c = true;
        }

        @DexIgnore
        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.e ? null : new boolean[hr.this.g];
        }

        @DexIgnore
        public File a(int i) throws IOException {
            File b2;
            synchronized (hr.this) {
                if (this.a.f == this) {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    b2 = this.a.b(i);
                    if (!hr.this.a.exists()) {
                        hr.this.a.mkdirs();
                    }
                } else {
                    throw new IllegalStateException();
                }
            }
            return b2;
        }

        @DexIgnore
        public void b() {
            if (!this.c) {
                try {
                    a();
                } catch (IOException unused) {
                }
            }
        }

        @DexIgnore
        public void a() throws IOException {
            hr.this.a(this, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public File[] c;
        @DexIgnore
        public File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public /* synthetic */ d(hr hrVar, String str, a aVar) {
            this(str);
        }

        @DexIgnore
        public d(String str) {
            this.a = str;
            this.b = new long[hr.this.g];
            this.c = new File[hr.this.g];
            this.d = new File[hr.this.g];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < hr.this.g; i++) {
                sb.append(i);
                this.c[i] = new File(hr.this.a, sb.toString());
                sb.append(".tmp");
                this.d[i] = new File(hr.this.a, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public final void b(String[] strArr) throws IOException {
            if (strArr.length == hr.this.g) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public String a() throws IOException {
            StringBuilder sb = new StringBuilder();
            for (long append : this.b) {
                sb.append(' ');
                sb.append(append);
            }
            return sb.toString();
        }

        @DexIgnore
        public File b(int i) {
            return this.d[i];
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public File a(int i) {
            return this.c[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e {
        @DexIgnore
        public /* final */ File[] a;

        @DexIgnore
        public /* synthetic */ e(hr hrVar, String str, long j, File[] fileArr, long[] jArr, a aVar) {
            this(hrVar, str, j, fileArr, jArr);
        }

        @DexIgnore
        public File a(int i) {
            return this.a[i];
        }

        @DexIgnore
        public e(hr hrVar, String str, long j, File[] fileArr, long[] jArr) {
            this.a = fileArr;
        }
    }

    @DexIgnore
    public hr(File file, int i2, int i3, long j2) {
        File file2 = file;
        this.a = file2;
        this.e = i2;
        this.b = new File(file2, "journal");
        this.c = new File(file2, "journal.tmp");
        this.d = new File(file2, "journal.bkp");
        this.g = i3;
        this.f = j2;
    }

    @DexIgnore
    public final void B() throws IOException {
        while (this.h > this.f) {
            h((String) this.j.entrySet().iterator().next().getKey());
        }
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        if (this.i != null) {
            Iterator it = new ArrayList(this.j.values()).iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (dVar.f != null) {
                    dVar.f.a();
                }
            }
            B();
            a(this.i);
            this.i = null;
        }
    }

    @DexIgnore
    public final void g(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                str2 = str.substring(i2);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.j.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.j.get(str2);
            if (dVar == null) {
                dVar = new d(this, str2, (a) null);
                this.j.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                boolean unused = dVar.e = true;
                c unused2 = dVar.f = null;
                dVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                c unused3 = dVar.f = new c(this, dVar, (a) null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008c, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        return false;
     */
    @DexIgnore
    public synchronized boolean h(String str) throws IOException {
        k();
        d dVar = this.j.get(str);
        if (dVar != null) {
            if (dVar.f == null) {
                for (int i2 = 0; i2 < this.g; i2++) {
                    File a2 = dVar.a(i2);
                    if (a2.exists()) {
                        if (!a2.delete()) {
                            throw new IOException("failed to delete " + a2);
                        }
                    }
                    this.h -= dVar.b[i2];
                    dVar.b[i2] = 0;
                }
                this.o++;
                this.i.append("REMOVE");
                this.i.append(' ');
                this.i.append(str);
                this.i.append(10);
                this.j.remove(str);
                if (m()) {
                    this.q.submit(this.r);
                }
            }
        }
    }

    @DexIgnore
    public final void k() {
        if (this.i == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    @DexIgnore
    public void l() throws IOException {
        close();
        jr.a(this.a);
    }

    @DexIgnore
    public final boolean m() {
        int i2 = this.o;
        return i2 >= 2000 && i2 >= this.j.size();
    }

    @DexIgnore
    public final void n() throws IOException {
        a(this.c);
        Iterator<d> it = this.j.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i2 = 0;
            if (next.f == null) {
                while (i2 < this.g) {
                    this.h += next.b[i2];
                    i2++;
                }
            } else {
                c unused = next.f = null;
                while (i2 < this.g) {
                    a(next.a(i2));
                    a(next.b(i2));
                    i2++;
                }
                it.remove();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.o = r0 - r9.j.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006c, code lost:
        if (r1.l() != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        p();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0072, code lost:
        r9.i = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(r9.b, true), com.fossil.jr.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005f */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x008c=Splitter:B:23:0x008c, B:16:0x005f=Splitter:B:16:0x005f} */
    public final void o() throws IOException {
        ir irVar = new ir(new FileInputStream(this.b), jr.a);
        try {
            String m = irVar.m();
            String m2 = irVar.m();
            String m3 = irVar.m();
            String m4 = irVar.m();
            String m5 = irVar.m();
            if (!"libcore.io.DiskLruCache".equals(m) || !"1".equals(m2) || !Integer.toString(this.e).equals(m3) || !Integer.toString(this.g).equals(m4) || !"".equals(m5)) {
                throw new IOException("unexpected journal header: [" + m + ", " + m2 + ", " + m4 + ", " + m5 + "]");
            }
            int i2 = 0;
            while (true) {
                g(irVar.m());
                i2++;
            }
        } finally {
            jr.a((Closeable) irVar);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final synchronized void p() throws IOException {
        if (this.i != null) {
            a(this.i);
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c), jr.a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write("\n");
            bufferedWriter.write("1");
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.e));
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.g));
            bufferedWriter.write("\n");
            bufferedWriter.write("\n");
            for (d next : this.j.values()) {
                if (next.f != null) {
                    bufferedWriter.write("DIRTY " + next.a + 10);
                } else {
                    bufferedWriter.write("CLEAN " + next.a + next.a() + 10);
                }
            }
            a((Writer) bufferedWriter);
            if (this.b.exists()) {
                a(this.b, this.d, true);
            }
            a(this.c, this.b, false);
            this.d.delete();
            this.i = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.b, true), jr.a));
        } catch (Throwable th) {
            a((Writer) bufferedWriter);
            throw th;
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void b(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.flush();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.flush();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    public c e(String str) throws IOException {
        return a(str, -1);
    }

    @DexIgnore
    public synchronized e f(String str) throws IOException {
        k();
        d dVar = this.j.get(str);
        if (dVar == null) {
            return null;
        }
        if (!dVar.e) {
            return null;
        }
        for (File exists : dVar.c) {
            if (!exists.exists()) {
                return null;
            }
        }
        this.o++;
        this.i.append("READ");
        this.i.append(' ');
        this.i.append(str);
        this.i.append(10);
        if (m()) {
            this.q.submit(this.r);
        }
        return new e(this, str, dVar.g, dVar.c, dVar.b, (a) null);
    }

    @DexIgnore
    public static hr a(File file, int i2, int i3, long j2) throws IOException {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    a(file2, file3, false);
                }
            }
            hr hrVar = new hr(file, i2, i3, j2);
            if (hrVar.b.exists()) {
                try {
                    hrVar.o();
                    hrVar.n();
                    return hrVar;
                } catch (IOException e2) {
                    PrintStream printStream = System.out;
                    printStream.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    hrVar.l();
                }
            }
            file.mkdirs();
            hr hrVar2 = new hr(file, i2, i3, j2);
            hrVar2.p();
            return hrVar2;
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public static void a(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    @DexIgnore
    public static void a(File file, File file2, boolean z) throws IOException {
        if (z) {
            a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
        return null;
     */
    @DexIgnore
    public final synchronized c a(String str, long j2) throws IOException {
        k();
        d dVar = this.j.get(str);
        if (j2 == -1 || (dVar != null && dVar.g == j2)) {
            if (dVar == null) {
                dVar = new d(this, str, (a) null);
                this.j.put(str, dVar);
            } else if (dVar.f != null) {
                return null;
            }
            c cVar = new c(this, dVar, (a) null);
            c unused = dVar.f = cVar;
            this.i.append("DIRTY");
            this.i.append(' ');
            this.i.append(str);
            this.i.append(10);
            b(this.i);
            return cVar;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0107, code lost:
        return;
     */
    @DexIgnore
    public final synchronized void a(c cVar, boolean z) throws IOException {
        d a2 = cVar.a;
        if (a2.f == cVar) {
            if (z && !a2.e) {
                int i2 = 0;
                while (i2 < this.g) {
                    if (!cVar.b[i2]) {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!a2.b(i2).exists()) {
                        cVar.a();
                        return;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.g; i3++) {
                File b2 = a2.b(i3);
                if (!z) {
                    a(b2);
                } else if (b2.exists()) {
                    File a3 = a2.a(i3);
                    b2.renameTo(a3);
                    long j2 = a2.b[i3];
                    long length = a3.length();
                    a2.b[i3] = length;
                    this.h = (this.h - j2) + length;
                }
            }
            this.o++;
            c unused = a2.f = null;
            if (a2.e || z) {
                boolean unused2 = a2.e = true;
                this.i.append("CLEAN");
                this.i.append(' ');
                this.i.append(a2.a);
                this.i.append(a2.a());
                this.i.append(10);
                if (z) {
                    long j3 = this.p;
                    this.p = 1 + j3;
                    long unused3 = a2.g = j3;
                }
            } else {
                this.j.remove(a2.a);
                this.i.append("REMOVE");
                this.i.append(' ');
                this.i.append(a2.a);
                this.i.append(10);
            }
            b(this.i);
            if (this.h > this.f || m()) {
                this.q.submit(this.r);
            }
        } else {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void a(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.close();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.close();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }
}
