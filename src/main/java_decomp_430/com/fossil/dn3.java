package com.fossil;

import java.util.Collection;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dn3<E> extends Collection<E> {

    @DexIgnore
    public interface a<E> {
        @DexIgnore
        int getCount();

        @DexIgnore
        E getElement();
    }

    @DexIgnore
    int add(E e, int i);

    @DexIgnore
    boolean add(E e);

    @DexIgnore
    boolean contains(Object obj);

    @DexIgnore
    boolean containsAll(Collection<?> collection);

    @DexIgnore
    int count(Object obj);

    @DexIgnore
    Set<E> elementSet();

    @DexIgnore
    Set<a<E>> entrySet();

    @DexIgnore
    int remove(Object obj, int i);

    @DexIgnore
    int setCount(E e, int i);

    @DexIgnore
    boolean setCount(E e, int i, int i2);
}
