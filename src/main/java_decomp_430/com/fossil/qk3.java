package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qk3<T> extends jo3<T> {
    @DexIgnore
    public b a; // = b.NOT_READY;
    @DexIgnore
    public T b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[b.DONE.ordinal()] = 1;
            a[b.READY.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    public enum b {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public final T b() {
        this.a = b.DONE;
        return null;
    }

    @DexIgnore
    public final boolean c() {
        this.a = b.FAILED;
        this.b = a();
        if (this.a == b.DONE) {
            return false;
        }
        this.a = b.READY;
        return true;
    }

    @DexIgnore
    public final boolean hasNext() {
        jk3.b(this.a != b.FAILED);
        int i = a.a[this.a.ordinal()];
        if (i == 1) {
            return false;
        }
        if (i != 2) {
            return c();
        }
        return true;
    }

    @DexIgnore
    public final T next() {
        if (hasNext()) {
            this.a = b.NOT_READY;
            T t = this.b;
            this.b = null;
            return t;
        }
        throw new NoSuchElementException();
    }
}
