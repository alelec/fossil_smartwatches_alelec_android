package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq5$b$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ExploreWatchPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bq5$b$a(ExploreWatchPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        bq5$b$a bq5_b_a = new bq5$b$a(this.this$0, xe6);
        bq5_b_a.p$ = (il6) obj;
        return bq5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((bq5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        String str;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            String e = this.this$0.$portfolioApp.e();
            String deviceNameBySerial = this.this$0.this$0.g.getDeviceNameBySerial(e);
            MisfitDeviceProfile a = DeviceHelper.o.e().a(e);
            if (a == null || (str = a.getFirmwareVersion()) == null) {
                str = "";
            }
            AnalyticsHelper.f.c().a(DeviceHelper.o.b(e), deviceNameBySerial, str);
            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, e, "ExploreWatchPresenter", "[Sync Start] AUTO SYNC after pair new device");
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
