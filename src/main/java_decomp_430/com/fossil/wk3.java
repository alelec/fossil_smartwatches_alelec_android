package com.fossil;

import com.fossil.dn3;
import com.fossil.en3;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wk3<E> extends AbstractCollection<E> implements dn3<E> {
    @DexIgnore
    public transient Set<E> a;
    @DexIgnore
    public transient Set<dn3.a<E>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends en3.c<E> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public dn3<E> a() {
            return wk3.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends en3.d<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public dn3<E> a() {
            return wk3.this;
        }

        @DexIgnore
        public Iterator<dn3.a<E>> iterator() {
            return wk3.this.entryIterator();
        }

        @DexIgnore
        public int size() {
            return wk3.this.distinctElements();
        }
    }

    @DexIgnore
    public boolean add(E e) {
        add(e, 1);
        return true;
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        return en3.a(this, collection);
    }

    @DexIgnore
    public abstract void clear();

    @DexIgnore
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    public abstract int count(Object obj);

    @DexIgnore
    public Set<E> createElementSet() {
        return new a();
    }

    @DexIgnore
    public Set<dn3.a<E>> createEntrySet() {
        return new b();
    }

    @DexIgnore
    public abstract int distinctElements();

    @DexIgnore
    public Set<E> elementSet() {
        Set<E> set = this.a;
        if (set != null) {
            return set;
        }
        Set<E> createElementSet = createElementSet();
        this.a = createElementSet;
        return createElementSet;
    }

    @DexIgnore
    public abstract Iterator<dn3.a<E>> entryIterator();

    @DexIgnore
    public Set<dn3.a<E>> entrySet() {
        Set<dn3.a<E>> set = this.b;
        if (set != null) {
            return set;
        }
        Set<dn3.a<E>> createEntrySet = createEntrySet();
        this.b = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return en3.a((dn3<?>) this, obj);
    }

    @DexIgnore
    public int hashCode() {
        return entrySet().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return entrySet().isEmpty();
    }

    @DexIgnore
    public abstract Iterator<E> iterator();

    @DexIgnore
    public abstract int remove(Object obj, int i);

    @DexIgnore
    public boolean remove(Object obj) {
        return remove(obj, 1) > 0;
    }

    @DexIgnore
    public boolean removeAll(Collection<?> collection) {
        return en3.b(this, collection);
    }

    @DexIgnore
    public boolean retainAll(Collection<?> collection) {
        return en3.c(this, collection);
    }

    @DexIgnore
    public int setCount(E e, int i) {
        return en3.a(this, e, i);
    }

    @DexIgnore
    public int size() {
        return en3.a((dn3<?>) this);
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    public int add(E e, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean setCount(E e, int i, int i2) {
        return en3.a(this, e, i, i2);
    }
}
