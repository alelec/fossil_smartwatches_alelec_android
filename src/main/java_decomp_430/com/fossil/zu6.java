package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zu6 extends RuntimeException {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 4029025366392702726L;

    @DexIgnore
    public zu6() {
    }

    @DexIgnore
    public zu6(String str) {
        super(str);
    }

    @DexIgnore
    public zu6(Throwable th) {
        super(th);
    }

    @DexIgnore
    public zu6(String str, Throwable th) {
        super(str, th);
    }
}
