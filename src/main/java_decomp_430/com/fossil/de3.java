package com.fossil;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.ud3;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class de3 extends Service implements qd3, td3, vd3, ae3 {
    @DexIgnore
    public ComponentName a;
    @DexIgnore
    public c b;
    @DexIgnore
    public IBinder c;
    @DexIgnore
    public Intent d;
    @DexIgnore
    public Looper e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public he3 h; // = new he3(new a());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ud3.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(ud3.a aVar) {
            de3.this.a(aVar);
        }

        @DexIgnore
        public final void b(ud3.a aVar, int i, int i2) {
            de3.this.b(aVar, i, i2);
        }

        @DexIgnore
        public final void c(ud3.a aVar, int i, int i2) {
            de3.this.c(aVar, i, i2);
        }

        @DexIgnore
        public final void a(ud3.a aVar, int i, int i2) {
            de3.this.a(aVar, i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b(de3 de3) {
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
        }
    }

    @DexIgnore
    public Looper a() {
        if (this.e == null) {
            HandlerThread handlerThread = new HandlerThread("WearableListenerService");
            handlerThread.start();
            this.e = handlerThread.getLooper();
        }
        return this.e;
    }

    @DexIgnore
    public void a(af3 af3) {
    }

    @DexIgnore
    public void a(be3 be3) {
    }

    @DexIgnore
    public void a(bf3 bf3) {
    }

    @DexIgnore
    public void a(ce3 ce3) {
    }

    @DexIgnore
    public void a(rd3 rd3) {
    }

    @DexIgnore
    public void a(sd3 sd3) {
    }

    @DexIgnore
    public void a(sd3 sd3, int i, int i2) {
    }

    @DexIgnore
    public void a(ud3.a aVar) {
    }

    @DexIgnore
    public void a(ud3.a aVar, int i, int i2) {
    }

    @DexIgnore
    public void a(xd3 xd3) {
    }

    @DexIgnore
    public void a(List<ce3> list) {
    }

    @DexIgnore
    public void b(ce3 ce3) {
    }

    @DexIgnore
    public void b(sd3 sd3, int i, int i2) {
    }

    @DexIgnore
    public void b(ud3.a aVar, int i, int i2) {
    }

    @DexIgnore
    public void c(sd3 sd3, int i, int i2) {
    }

    @DexIgnore
    public void c(ud3.a aVar, int i, int i2) {
    }

    @DexIgnore
    public final IBinder onBind(Intent intent) {
        if ("com.google.android.gms.wearable.BIND_LISTENER".equals(intent.getAction())) {
            return this.c;
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.a = new ComponentName(this, de3.class.getName());
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.a);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 10);
            sb.append("onCreate: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        this.b = new c(a());
        this.d = new Intent("com.google.android.gms.wearable.BIND_LISTENER");
        this.d.setComponent(this.a);
        this.c = new d();
    }

    @DexIgnore
    public void onDestroy() {
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.a);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 11);
            sb.append("onDestroy: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        synchronized (this.f) {
            this.g = true;
            if (this.b != null) {
                this.b.a();
            } else {
                String valueOf2 = String.valueOf(this.a);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 111);
                sb2.append("onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()? component=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
        super.onDestroy();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends Handler {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ b b; // = new b();

        @DexIgnore
        public c(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void a() {
            getLooper().quit();
            a("quit");
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final synchronized void b() {
            if (!this.a) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(de3.this.a);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
                    sb.append("bindService: ");
                    sb.append(valueOf);
                    Log.v("WearableLS", sb.toString());
                }
                de3.this.bindService(de3.this.d, this.b, 1);
                this.a = true;
            }
        }

        @DexIgnore
        public final void dispatchMessage(Message message) {
            b();
            try {
                super.dispatchMessage(message);
            } finally {
                if (!hasMessages(0)) {
                    a("dispatch");
                }
            }
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final synchronized void a(String str) {
            if (this.a) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(de3.this.a);
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 17 + String.valueOf(valueOf).length());
                    sb.append("unbindService: ");
                    sb.append(str);
                    sb.append(", ");
                    sb.append(valueOf);
                    Log.v("WearableLS", sb.toString());
                }
                try {
                    de3.this.unbindService(this.b);
                } catch (RuntimeException e) {
                    Log.e("WearableLS", "Exception when unbinding from local service", e);
                }
                this.a = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends qe3 {
        @DexIgnore
        public volatile int a;

        @DexIgnore
        public d() {
            this.a = -1;
        }

        @DexIgnore
        public final void a(DataHolder dataHolder) {
            df3 df3 = new df3(this, dataHolder);
            try {
                String valueOf = String.valueOf(dataHolder);
                int count = dataHolder.getCount();
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append(valueOf);
                sb.append(", rows=");
                sb.append(count);
                if (a(df3, "onDataItemChanged", sb.toString())) {
                }
            } finally {
                dataHolder.close();
            }
        }

        @DexIgnore
        public final void b(te3 te3) {
            a(new gf3(this, te3), "onPeerDisconnected", te3);
        }

        @DexIgnore
        public final void b(List<te3> list) {
            a(new hf3(this, list), "onConnectedNodes", list);
        }

        @DexIgnore
        public final void a(re3 re3) {
            a(new ef3(this, re3), "onMessageReceived", re3);
        }

        @DexIgnore
        public final void a(te3 te3) {
            a(new ff3(this, te3), "onPeerConnected", te3);
        }

        @DexIgnore
        public final void a(ee3 ee3) {
            a(new if3(this, ee3), "onConnectedCapabilityChanged", ee3);
        }

        @DexIgnore
        public final void a(ye3 ye3) {
            a(new jf3(this, ye3), "onNotificationReceived", ye3);
        }

        @DexIgnore
        public final void a(we3 we3) {
            a(new kf3(this, we3), "onEntityUpdate", we3);
        }

        @DexIgnore
        public final void a(ie3 ie3) {
            a(new lf3(this, ie3), "onChannelEvent", ie3);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0074 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
        public final boolean a(Runnable runnable, String str, Object obj) {
            boolean z;
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", String.format("%s: %s %s", new Object[]{str, de3.this.a.toString(), obj}));
            }
            int callingUid = Binder.getCallingUid();
            if (callingUid != this.a) {
                if (ve3.a((Context) de3.this).a("com.google.android.wearable.app.cn") && v42.a(de3.this, callingUid, "com.google.android.wearable.app.cn")) {
                    this.a = callingUid;
                } else if (v42.a(de3.this, callingUid)) {
                    this.a = callingUid;
                } else {
                    StringBuilder sb = new StringBuilder(57);
                    sb.append("Caller is not GooglePlayServices; caller UID: ");
                    sb.append(callingUid);
                    Log.e("WearableLS", sb.toString());
                    z = false;
                    if (z) {
                        return false;
                    }
                    synchronized (de3.this.f) {
                        if (de3.this.g) {
                            return false;
                        }
                        de3.this.b.post(runnable);
                        return true;
                    }
                }
            }
            z = true;
            if (z) {
            }
        }
    }
}
