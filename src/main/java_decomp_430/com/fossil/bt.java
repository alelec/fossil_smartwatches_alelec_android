package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bt {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(vr vrVar, Exception exc, fs<?> fsVar, pr prVar);

        @DexIgnore
        void a(vr vrVar, Object obj, fs<?> fsVar, pr prVar, vr vrVar2);

        @DexIgnore
        void b();
    }

    @DexIgnore
    boolean a();

    @DexIgnore
    void cancel();
}
