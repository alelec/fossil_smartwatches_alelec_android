package com.fossil;

import com.fossil.iu;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zt<T extends iu> {
    @DexIgnore
    public /* final */ Queue<T> a; // = r00.a(20);

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public void a(T t) {
        if (this.a.size() < 20) {
            this.a.offer(t);
        }
    }

    @DexIgnore
    public T b() {
        T t = (iu) this.a.poll();
        return t == null ? a() : t;
    }
}
