package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm1 extends p21 {
    @DexIgnore
    public /* final */ nd0[] S;

    @DexIgnore
    public fm1(ue1 ue1, q41 q41, nd0[] nd0Arr) {
        super(ue1, q41, eh1.CONFIGURE_MICRO_APP, true, lk1.b.b(ue1.t, w31.MICRO_APP), new byte[0], LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192);
        this.S = nd0Arr;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.MICRO_APP_MAPPINGS, (Object) cw0.a((p40[]) this.S));
    }

    @DexIgnore
    public byte[] n() {
        w40 microAppVersion = this.x.a().getMicroAppVersion();
        w40 w40 = this.x.a().i().get(Short.valueOf(w31.MICRO_APP.a));
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        wg6.a(w40, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
        yh1 yh1 = new yh1(this.S, microAppVersion);
        byte[] a = cw0.a(cw0.a(cw0.a(new byte[]{yh1.b.getMajor(), yh1.b.getMinor(), af0.CONFIGURATION_FILE.a()}, yh1.a(yh1.e)), yh1.a(yh1.c)), yh1.a(yh1.d));
        int a2 = (int) h51.a.a(a, q11.CRC32);
        ByteBuffer order = ByteBuffer.allocate(a.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        order.put(a);
        order.putInt(a2);
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return kk1.d.a(this.C, w40, array);
    }
}
