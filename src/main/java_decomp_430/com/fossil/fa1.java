package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa1 extends j61 {
    @DexIgnore
    public /* final */ rg1 A;
    @DexIgnore
    public /* final */ boolean B;

    @DexIgnore
    public fa1(rg1 rg1, boolean z, ue1 ue1) {
        super(lx0.SUBSCRIBE_CHARACTERISTIC, ue1);
        this.A = rg1;
        this.B = z;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.CHANNEL_ID, (Object) this.A.a), bm0.ENABLE, (Object) Boolean.valueOf(this.B));
    }

    @DexIgnore
    public ok0 l() {
        return new j91(this.A, this.B, this.y.v);
    }
}
