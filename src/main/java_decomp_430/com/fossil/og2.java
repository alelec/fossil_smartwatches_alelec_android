package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.uw1;
import com.fossil.wv1;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og2 extends ah2 {
    @DexIgnore
    public /* final */ hg2 G;

    @DexIgnore
    public og2(Context context, Looper looper, wv1.b bVar, wv1.c cVar, String str, e12 e12) {
        super(context, looper, bVar, cVar, str, e12);
        this.G = new hg2(context, this.F);
    }

    @DexIgnore
    public final Location H() throws RemoteException {
        return this.G.a();
    }

    @DexIgnore
    public final void a() {
        synchronized (this.G) {
            if (c()) {
                try {
                    this.G.b();
                    this.G.c();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.a();
        }
    }

    @DexIgnore
    public final void a(bw2 bw2, ow1<dw2> ow1, String str) throws RemoteException {
        q();
        boolean z = true;
        w12.a(bw2 != null, (Object) "locationSettingsRequest can't be null nor empty.");
        if (ow1 == null) {
            z = false;
        }
        w12.a(z, (Object) "listener can't be null.");
        ((dg2) y()).a(bw2, new qg2(ow1), str);
    }

    @DexIgnore
    public final void a(rg2 rg2, uw1<yv2> uw1, ag2 ag2) throws RemoteException {
        synchronized (this.G) {
            this.G.a(rg2, uw1, ag2);
        }
    }

    @DexIgnore
    public final void a(uw1.a<zv2> aVar, ag2 ag2) throws RemoteException {
        this.G.a(aVar, ag2);
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, uw1<zv2> uw1, ag2 ag2) throws RemoteException {
        synchronized (this.G) {
            this.G.a(locationRequest, uw1, ag2);
        }
    }

    @DexIgnore
    public final void b(uw1.a<yv2> aVar, ag2 ag2) throws RemoteException {
        this.G.b(aVar, ag2);
    }
}
