package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum or0 {
    SENDER_NAME((byte) 2),
    APP_BUNDLE_CRC32((byte) 4),
    GROUP_ID((byte) 128),
    ICON_IMAGE((byte) 130),
    PRIORITY((byte) 193),
    HAND_MOVING((byte) 194),
    VIBE((byte) 195);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public or0(byte b) {
        this.a = b;
    }
}
