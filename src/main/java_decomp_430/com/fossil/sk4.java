package com.fossil;

import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;
import com.fossil.p6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sk4 {
    @DexIgnore
    public static /* final */ a a; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r8v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final void a(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends p6.a> list) {
            wg6.b(context, "context");
            wg6.b(str, Explore.COLUMN_TITLE);
            wg6.b(str2, "text");
            wg6.b(pendingIntent, "pendingIntent");
            p6.c cVar = new p6.c();
            cVar.a(str2);
            Uri defaultUri = RingtoneManager.getDefaultUri(2);
            BitmapFactory.decodeResource(context.getResources(), 2131689472);
            p6.d dVar = new p6.d(context);
            dVar.b(str);
            dVar.a(str2);
            dVar.e(2131689472);
            dVar.a(cVar);
            dVar.a(new long[]{1000, 1000, 1000});
            dVar.a(defaultUri);
            dVar.a(pendingIntent);
            Notification a = dVar.a();
            a.flags |= 16;
            if (Build.VERSION.SDK_INT >= 21) {
                Resources resources = PortfolioApp.get.instance().getResources();
                Package packageR = R.class.getPackage();
                if (packageR != null) {
                    wg6.a((Object) packageR, "android.R::class.java.`package`!!");
                    int identifier = resources.getIdentifier("right_icon", "id", packageR.getName());
                    if (identifier != 0) {
                        RemoteViews remoteViews = a.contentView;
                        if (remoteViews != null) {
                            remoteViews.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews2 = a.headsUpContentView;
                        if (remoteViews2 != null) {
                            remoteViews2.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews3 = a.bigContentView;
                        if (remoteViews3 != null) {
                            remoteViews3.setViewVisibility(identifier, 4);
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            if (list != null) {
                for (p6.a a2 : list) {
                    dVar.a(a2);
                }
            }
            Object systemService = context.getSystemService("notification");
            if (systemService != null) {
                ((NotificationManager) systemService).notify(i, a);
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.app.NotificationManager");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r9v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final void b(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends p6.a> list) {
            wg6.b(context, "context");
            wg6.b(str, Explore.COLUMN_TITLE);
            wg6.b(str2, "text");
            wg6.b(pendingIntent, "pendingIntent");
            p6.c cVar = new p6.c();
            cVar.a(str2);
            Uri defaultUri = RingtoneManager.getDefaultUri(2);
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), 2131689472);
            p6.d dVar = new p6.d(context);
            dVar.b(str);
            dVar.a(str2);
            dVar.e(2131689472);
            dVar.b(decodeResource);
            dVar.a(cVar);
            dVar.a(new long[]{1000, 1000, 1000});
            dVar.a(defaultUri);
            dVar.a(pendingIntent);
            Notification a = dVar.a();
            a.flags |= 16;
            if (Build.VERSION.SDK_INT >= 21) {
                Resources resources = PortfolioApp.get.instance().getResources();
                Package packageR = R.class.getPackage();
                if (packageR != null) {
                    wg6.a((Object) packageR, "android.R::class.java.`package`!!");
                    int identifier = resources.getIdentifier("right_icon", "id", packageR.getName());
                    if (identifier != 0) {
                        RemoteViews remoteViews = a.contentView;
                        if (remoteViews != null) {
                            remoteViews.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews2 = a.headsUpContentView;
                        if (remoteViews2 != null) {
                            remoteViews2.setViewVisibility(identifier, 4);
                        }
                        RemoteViews remoteViews3 = a.bigContentView;
                        if (remoteViews3 != null) {
                            remoteViews3.setViewVisibility(identifier, 4);
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            if (list != null) {
                for (p6.a a2 : list) {
                    dVar.a(a2);
                }
            }
            Object systemService = context.getSystemService("notification");
            if (systemService != null) {
                ((NotificationManager) systemService).notify(i, a);
                return;
            }
            throw new rc6("null cannot be cast to non-null type android.app.NotificationManager");
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }
}
