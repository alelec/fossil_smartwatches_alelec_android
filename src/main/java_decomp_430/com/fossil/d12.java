package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d12 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<d12> CREATOR; // = new v22();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public d12(int i, String str) {
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof d12)) {
            d12 d12 = (d12) obj;
            return d12.a == this.a && u12.a(d12.b, this.b);
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        int i = this.a;
        String str = this.b;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(i);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b, false);
        g22.a(parcel, a2);
    }
}
