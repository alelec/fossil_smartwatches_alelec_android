package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uj1 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ ai1 CREATOR; // = new ai1((qg6) null);
    @DexIgnore
    public /* final */ ck0 a;

    @DexIgnore
    public uj1(ck0 ck0) {
        this.a = ck0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(new JSONObject(), bm0.INSTRUCTION_ID, (Object) cw0.a((Enum<?>) this.a));
    }

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((uj1) obj).a;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.Instruction");
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.a.a);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public uj1(Parcel parcel) {
        this(ck0.valueOf(r2));
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            return;
        }
        wg6.a();
        throw null;
    }
}
