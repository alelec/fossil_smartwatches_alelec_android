package com.fossil;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View A;
    @DexIgnore
    public /* final */ CustomizeWidget B;
    @DexIgnore
    public /* final */ CustomizeWidget C;
    @DexIgnore
    public /* final */ CustomizeWidget D;
    @DexIgnore
    public /* final */ CardView q;
    @DexIgnore
    public /* final */ ImageButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ViewPager2 u;
    @DexIgnore
    public /* final */ RTLImageView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jb4(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CardView cardView, ImageButton imageButton, View view3, ImageView imageView, View view4, View view5, View view6, ConstraintLayout constraintLayout3, ViewPager2 viewPager2, RTLImageView rTLImageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, View view7, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3) {
        super(obj, view, i);
        this.q = cardView;
        this.r = imageButton;
        this.s = imageView;
        this.t = constraintLayout3;
        this.u = viewPager2;
        this.v = rTLImageView;
        this.w = flexibleTextView;
        this.x = flexibleTextView2;
        this.y = flexibleTextView3;
        this.z = flexibleTextView4;
        this.A = view7;
        this.B = customizeWidget;
        this.C = customizeWidget2;
        this.D = customizeWidget3;
    }
}
