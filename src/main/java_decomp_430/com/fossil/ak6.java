package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ak6 extends zj6 {
    @DexIgnore
    public static final char e(CharSequence charSequence) {
        wg6.b(charSequence, "$this$last");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(yj6.c(charSequence));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
}
