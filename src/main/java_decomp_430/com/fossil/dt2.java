package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dt2 extends dk2 implements eu2 {
    @DexIgnore
    public dt2() {
        super("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    public static eu2 asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
        if (queryLocalInterface instanceof eu2) {
            return (eu2) queryLocalInterface;
        }
        return new fv2(iBinder);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v9, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v20, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v26, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v30, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v34, types: [com.fossil.kv2] */
    /* JADX WARNING: type inference failed for: r3v38, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v42, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v46, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v50, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v55, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v60, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v67, types: [com.fossil.jv2] */
    /* JADX WARNING: type inference failed for: r3v71, types: [com.fossil.jv2] */
    /* JADX WARNING: type inference failed for: r3v75, types: [com.fossil.jv2] */
    /* JADX WARNING: type inference failed for: r3v79, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v84, types: [com.fossil.ev2] */
    /* JADX WARNING: type inference failed for: r3v88 */
    /* JADX WARNING: type inference failed for: r3v89 */
    /* JADX WARNING: type inference failed for: r3v90 */
    /* JADX WARNING: type inference failed for: r3v91 */
    /* JADX WARNING: type inference failed for: r3v92 */
    /* JADX WARNING: type inference failed for: r3v93 */
    /* JADX WARNING: type inference failed for: r3v94 */
    /* JADX WARNING: type inference failed for: r3v95 */
    /* JADX WARNING: type inference failed for: r3v96 */
    /* JADX WARNING: type inference failed for: r3v97 */
    /* JADX WARNING: type inference failed for: r3v98 */
    /* JADX WARNING: type inference failed for: r3v99 */
    /* JADX WARNING: type inference failed for: r3v100 */
    /* JADX WARNING: type inference failed for: r3v101 */
    /* JADX WARNING: type inference failed for: r3v102 */
    /* JADX WARNING: type inference failed for: r3v103 */
    /* JADX WARNING: type inference failed for: r3v104 */
    /* JADX WARNING: type inference failed for: r3v105 */
    /* JADX WARNING: type inference failed for: r3v106 */
    /* JADX WARNING: type inference failed for: r3v107 */
    /* JADX WARNING: type inference failed for: r3v108 */
    /* JADX WARNING: type inference failed for: r3v109 */
    /* JADX WARNING: type inference failed for: r3v110 */
    /* JADX WARNING: type inference failed for: r3v111 */
    /* JADX WARNING: type inference failed for: r3v112 */
    /* JADX WARNING: type inference failed for: r3v113 */
    /* JADX WARNING: type inference failed for: r3v114 */
    /* JADX WARNING: type inference failed for: r3v115 */
    /* JADX WARNING: type inference failed for: r3v116 */
    /* JADX WARNING: type inference failed for: r3v117 */
    /* JADX WARNING: type inference failed for: r3v118 */
    /* JADX WARNING: type inference failed for: r3v119 */
    /* JADX WARNING: type inference failed for: r3v120 */
    /* JADX WARNING: type inference failed for: r3v121 */
    /* JADX WARNING: Multi-variable type inference failed */
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        ev2 ev2;
        ev2 ev22;
        Object r3 = 0;
        switch (i) {
            case 1:
                initialize(x52.a.a(parcel.readStrongBinder()), (mv2) li2.a(parcel, mv2.CREATOR), parcel.readLong());
                break;
            case 2:
                logEvent(parcel.readString(), parcel.readString(), (Bundle) li2.a(parcel, Bundle.CREATOR), li2.a(parcel), li2.a(parcel), parcel.readLong());
                break;
            case 3:
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                Bundle bundle = (Bundle) li2.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    ev2 = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface instanceof ev2) {
                        ev22 = (ev2) queryLocalInterface;
                    } else {
                        ev22 = new gv2(readStrongBinder);
                    }
                    ev2 = ev22;
                }
                logEventAndBundle(readString, readString2, bundle, ev2, parcel.readLong());
                break;
            case 4:
                setUserProperty(parcel.readString(), parcel.readString(), x52.a.a(parcel.readStrongBinder()), li2.a(parcel), parcel.readLong());
                break;
            case 5:
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                boolean a = li2.a(parcel);
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface2 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface2;
                    } else {
                        r3 = new gv2(readStrongBinder2);
                    }
                }
                getUserProperties(readString3, readString4, a, r3);
                break;
            case 6:
                String readString5 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface3 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface3;
                    } else {
                        r3 = new gv2(readStrongBinder3);
                    }
                }
                getMaxUserProperties(readString5, r3);
                break;
            case 7:
                setUserId(parcel.readString(), parcel.readLong());
                break;
            case 8:
                setConditionalUserProperty((Bundle) li2.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 9:
                clearConditionalUserProperty(parcel.readString(), parcel.readString(), (Bundle) li2.a(parcel, Bundle.CREATOR));
                break;
            case 10:
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface4 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface4;
                    } else {
                        r3 = new gv2(readStrongBinder4);
                    }
                }
                getConditionalUserProperties(readString6, readString7, r3);
                break;
            case 11:
                setMeasurementEnabled(li2.a(parcel), parcel.readLong());
                break;
            case 12:
                resetAnalyticsData(parcel.readLong());
                break;
            case 13:
                setMinimumSessionDuration(parcel.readLong());
                break;
            case 14:
                setSessionTimeoutDuration(parcel.readLong());
                break;
            case 15:
                setCurrentScreen(x52.a.a(parcel.readStrongBinder()), parcel.readString(), parcel.readString(), parcel.readLong());
                break;
            case 16:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface5 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface5;
                    } else {
                        r3 = new gv2(readStrongBinder5);
                    }
                }
                getCurrentScreenName(r3);
                break;
            case 17:
                IBinder readStrongBinder6 = parcel.readStrongBinder();
                if (readStrongBinder6 != null) {
                    IInterface queryLocalInterface6 = readStrongBinder6.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface6 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface6;
                    } else {
                        r3 = new gv2(readStrongBinder6);
                    }
                }
                getCurrentScreenClass(r3);
                break;
            case 18:
                IBinder readStrongBinder7 = parcel.readStrongBinder();
                if (readStrongBinder7 != null) {
                    IInterface queryLocalInterface7 = readStrongBinder7.queryLocalInterface("com.google.android.gms.measurement.api.internal.IStringProvider");
                    if (queryLocalInterface7 instanceof kv2) {
                        r3 = (kv2) queryLocalInterface7;
                    } else {
                        r3 = new nv2(readStrongBinder7);
                    }
                }
                setInstanceIdProvider(r3);
                break;
            case 19:
                IBinder readStrongBinder8 = parcel.readStrongBinder();
                if (readStrongBinder8 != null) {
                    IInterface queryLocalInterface8 = readStrongBinder8.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface8 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface8;
                    } else {
                        r3 = new gv2(readStrongBinder8);
                    }
                }
                getCachedAppInstanceId(r3);
                break;
            case 20:
                IBinder readStrongBinder9 = parcel.readStrongBinder();
                if (readStrongBinder9 != null) {
                    IInterface queryLocalInterface9 = readStrongBinder9.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface9 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface9;
                    } else {
                        r3 = new gv2(readStrongBinder9);
                    }
                }
                getAppInstanceId(r3);
                break;
            case 21:
                IBinder readStrongBinder10 = parcel.readStrongBinder();
                if (readStrongBinder10 != null) {
                    IInterface queryLocalInterface10 = readStrongBinder10.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface10 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface10;
                    } else {
                        r3 = new gv2(readStrongBinder10);
                    }
                }
                getGmpAppId(r3);
                break;
            case 22:
                IBinder readStrongBinder11 = parcel.readStrongBinder();
                if (readStrongBinder11 != null) {
                    IInterface queryLocalInterface11 = readStrongBinder11.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface11 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface11;
                    } else {
                        r3 = new gv2(readStrongBinder11);
                    }
                }
                generateEventId(r3);
                break;
            case 23:
                beginAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 24:
                endAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 25:
                onActivityStarted(x52.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 26:
                onActivityStopped(x52.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 27:
                onActivityCreated(x52.a.a(parcel.readStrongBinder()), (Bundle) li2.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 28:
                onActivityDestroyed(x52.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 29:
                onActivityPaused(x52.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 30:
                onActivityResumed(x52.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 31:
                x52 a2 = x52.a.a(parcel.readStrongBinder());
                IBinder readStrongBinder12 = parcel.readStrongBinder();
                if (readStrongBinder12 != null) {
                    IInterface queryLocalInterface12 = readStrongBinder12.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface12 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface12;
                    } else {
                        r3 = new gv2(readStrongBinder12);
                    }
                }
                onActivitySaveInstanceState(a2, r3, parcel.readLong());
                break;
            case 32:
                Bundle bundle2 = (Bundle) li2.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder13 = parcel.readStrongBinder();
                if (readStrongBinder13 != null) {
                    IInterface queryLocalInterface13 = readStrongBinder13.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface13 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface13;
                    } else {
                        r3 = new gv2(readStrongBinder13);
                    }
                }
                performAction(bundle2, r3, parcel.readLong());
                break;
            case 33:
                logHealthData(parcel.readInt(), parcel.readString(), x52.a.a(parcel.readStrongBinder()), x52.a.a(parcel.readStrongBinder()), x52.a.a(parcel.readStrongBinder()));
                break;
            case 34:
                IBinder readStrongBinder14 = parcel.readStrongBinder();
                if (readStrongBinder14 != null) {
                    IInterface queryLocalInterface14 = readStrongBinder14.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface14 instanceof jv2) {
                        r3 = (jv2) queryLocalInterface14;
                    } else {
                        r3 = new lv2(readStrongBinder14);
                    }
                }
                setEventInterceptor(r3);
                break;
            case 35:
                IBinder readStrongBinder15 = parcel.readStrongBinder();
                if (readStrongBinder15 != null) {
                    IInterface queryLocalInterface15 = readStrongBinder15.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface15 instanceof jv2) {
                        r3 = (jv2) queryLocalInterface15;
                    } else {
                        r3 = new lv2(readStrongBinder15);
                    }
                }
                registerOnMeasurementEventListener(r3);
                break;
            case 36:
                IBinder readStrongBinder16 = parcel.readStrongBinder();
                if (readStrongBinder16 != null) {
                    IInterface queryLocalInterface16 = readStrongBinder16.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface16 instanceof jv2) {
                        r3 = (jv2) queryLocalInterface16;
                    } else {
                        r3 = new lv2(readStrongBinder16);
                    }
                }
                unregisterOnMeasurementEventListener(r3);
                break;
            case 37:
                initForTests(li2.b(parcel));
                break;
            case 38:
                IBinder readStrongBinder17 = parcel.readStrongBinder();
                if (readStrongBinder17 != null) {
                    IInterface queryLocalInterface17 = readStrongBinder17.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface17 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface17;
                    } else {
                        r3 = new gv2(readStrongBinder17);
                    }
                }
                getTestFlag(r3, parcel.readInt());
                break;
            case 39:
                setDataCollectionEnabled(li2.a(parcel));
                break;
            case 40:
                IBinder readStrongBinder18 = parcel.readStrongBinder();
                if (readStrongBinder18 != null) {
                    IInterface queryLocalInterface18 = readStrongBinder18.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface18 instanceof ev2) {
                        r3 = (ev2) queryLocalInterface18;
                    } else {
                        r3 = new gv2(readStrongBinder18);
                    }
                }
                isDataCollectionEnabled(r3);
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
