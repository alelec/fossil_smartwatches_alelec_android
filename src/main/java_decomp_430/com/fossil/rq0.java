package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rq0 extends p41 {
    @DexIgnore
    public /* final */ rg1 E;
    @DexIgnore
    public /* final */ rg1 F;
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ short I;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ rq0(short s, ue1 ue1, int i, int i2) {
        super(lx0.LEGACY_ERASE_ACTIVITY_FILE, ue1, (i2 & 4) != 0 ? 3 : i);
        this.I = s;
        rg1 rg1 = rg1.FTC;
        this.E = rg1;
        this.F = rg1;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_ERASE_FILE.a).putShort(this.I).array();
        wg6.a(array, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_ERASE_FILE.a()).putShort(this.I).array();
        wg6.a(array2, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.H = array2;
    }

    @DexIgnore
    public sj0 a(byte b) {
        return zh0.j.a(b);
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.FILE_HANDLE, (Object) cw0.a(this.I));
    }

    @DexIgnore
    public rg1 m() {
        return this.F;
    }

    @DexIgnore
    public byte[] o() {
        return this.G;
    }

    @DexIgnore
    public rg1 p() {
        return this.E;
    }

    @DexIgnore
    public byte[] r() {
        return this.H;
    }
}
