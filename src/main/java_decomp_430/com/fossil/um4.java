package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.manager.LightAndHapticsManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class um4 implements MembersInjector<sm4> {
    @DexIgnore
    public static void a(LightAndHapticsManager lightAndHapticsManager, DeviceRepository deviceRepository) {
        lightAndHapticsManager.a = deviceRepository;
    }

    @DexIgnore
    public static void a(LightAndHapticsManager lightAndHapticsManager, an4 an4) {
        lightAndHapticsManager.b = an4;
    }
}
