package com.fossil;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dc extends bl {
    @DexIgnore
    public /* final */ FragmentManager b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public hc d;
    @DexIgnore
    public Fragment e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    @Deprecated
    public dc(FragmentManager fragmentManager) {
        this(fragmentManager, 0);
    }

    @DexIgnore
    public Object a(ViewGroup viewGroup, int i) {
        if (this.d == null) {
            this.d = this.b.b();
        }
        long d2 = d(i);
        Fragment b2 = this.b.b(a(viewGroup.getId(), d2));
        if (b2 != null) {
            this.d.a(b2);
        } else {
            b2 = c(i);
            this.d.a(viewGroup.getId(), b2, a(viewGroup.getId(), d2));
        }
        if (b2 != this.e) {
            b2.setMenuVisibility(false);
            if (this.c == 1) {
                this.d.a(b2, Lifecycle.State.STARTED);
            } else {
                b2.setUserVisibleHint(false);
            }
        }
        return b2;
    }

    @DexIgnore
    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    @DexIgnore
    public Parcelable b() {
        return null;
    }

    @DexIgnore
    public void b(ViewGroup viewGroup) {
        if (viewGroup.getId() == -1) {
            throw new IllegalStateException("ViewPager with adapter " + this + " requires a view id");
        }
    }

    @DexIgnore
    public abstract Fragment c(int i);

    @DexIgnore
    public long d(int i) {
        return (long) i;
    }

    @DexIgnore
    public dc(FragmentManager fragmentManager, int i) {
        this.d = null;
        this.e = null;
        this.b = fragmentManager;
        this.c = i;
    }

    @DexIgnore
    public void b(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        Fragment fragment2 = this.e;
        if (fragment != fragment2) {
            if (fragment2 != null) {
                fragment2.setMenuVisibility(false);
                if (this.c == 1) {
                    if (this.d == null) {
                        this.d = this.b.b();
                    }
                    this.d.a(this.e, Lifecycle.State.STARTED);
                } else {
                    this.e.setUserVisibleHint(false);
                }
            }
            fragment.setMenuVisibility(true);
            if (this.c == 1) {
                if (this.d == null) {
                    this.d = this.b.b();
                }
                this.d.a(fragment, Lifecycle.State.RESUMED);
            } else {
                fragment.setUserVisibleHint(true);
            }
            this.e = fragment;
        }
    }

    @DexIgnore
    public void a(ViewGroup viewGroup, int i, Object obj) {
        Fragment fragment = (Fragment) obj;
        if (this.d == null) {
            this.d = this.b.b();
        }
        this.d.b(fragment);
        if (fragment.equals(this.e)) {
            this.e = null;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void a(ViewGroup viewGroup) {
        hc hcVar = this.d;
        if (hcVar != null) {
            if (!this.f) {
                try {
                    this.f = true;
                    hcVar.d();
                    this.f = false;
                } catch (Throwable th) {
                    this.f = false;
                    throw th;
                }
            }
            this.d = null;
        }
    }

    @DexIgnore
    public boolean a(View view, Object obj) {
        return ((Fragment) obj).getView() == view;
    }

    @DexIgnore
    public static String a(int i, long j) {
        return "android:switcher:" + i + ":" + j;
    }
}
