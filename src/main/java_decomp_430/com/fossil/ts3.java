package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ts3 {
    @DexIgnore
    public static ts3 e;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ScheduledExecutorService b;
    @DexIgnore
    public us3 c; // = new us3(this);
    @DexIgnore
    public int d; // = 1;

    @DexIgnore
    public ts3(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static synchronized ts3 a(Context context) {
        ts3 ts3;
        synchronized (ts3.class) {
            if (e == null) {
                e = new ts3(context, gb2.a().a(1, new y42("MessengerIpcClient"), lb2.a));
            }
            ts3 = e;
        }
        return ts3;
    }

    @DexIgnore
    public final qc3<Bundle> b(int i, Bundle bundle) {
        return a(new er3(a(), 1, bundle));
    }

    @DexIgnore
    public final qc3<Void> a(int i, Bundle bundle) {
        return a(new zq3(a(), 2, bundle));
    }

    @DexIgnore
    public final synchronized <T> qc3<T> a(cr3<T> cr3) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(cr3);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.c.a((cr3<?>) cr3)) {
            this.c = new us3(this);
            this.c.a((cr3<?>) cr3);
        }
        return cr3.b.a();
    }

    @DexIgnore
    public final synchronized int a() {
        int i;
        i = this.d;
        this.d = i + 1;
        return i;
    }
}
