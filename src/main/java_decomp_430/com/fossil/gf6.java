package com.fossil;

import com.fossil.mc6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gf6 implements xe6<Object>, kf6, Serializable {
    @DexIgnore
    public /* final */ xe6<Object> completion;

    @DexIgnore
    public gf6(xe6<Object> xe6) {
        this.completion = xe6;
    }

    @DexIgnore
    public xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        throw new UnsupportedOperationException("create(Continuation) has not been overridden");
    }

    @DexIgnore
    public kf6 getCallerFrame() {
        xe6<Object> xe6 = this.completion;
        if (!(xe6 instanceof kf6)) {
            xe6 = null;
        }
        return (kf6) xe6;
    }

    @DexIgnore
    public final xe6<Object> getCompletion() {
        return this.completion;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return mf6.c(this);
    }

    @DexIgnore
    public abstract Object invokeSuspend(Object obj);

    @DexIgnore
    public void releaseIntercepted() {
    }

    @DexIgnore
    public final void resumeWith(Object obj) {
        Object obj2 = obj;
        gf6 gf6 = this;
        while (true) {
            nf6.b(gf6);
            xe6 xe6 = gf6.completion;
            if (xe6 != null) {
                try {
                    Object invokeSuspend = gf6.invokeSuspend(obj2);
                    if (invokeSuspend != ff6.a()) {
                        mc6.a aVar = mc6.Companion;
                        obj2 = mc6.m1constructorimpl(invokeSuspend);
                        gf6.releaseIntercepted();
                        if (xe6 instanceof gf6) {
                            gf6 = (gf6) xe6;
                        } else {
                            xe6.resumeWith(obj2);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    mc6.a aVar2 = mc6.Companion;
                    obj2 = mc6.m1constructorimpl(nc6.a(th));
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Continuation at ");
        Object stackTraceElement = getStackTraceElement();
        if (stackTraceElement == null) {
            stackTraceElement = getClass().getName();
        }
        sb.append(stackTraceElement);
        return sb.toString();
    }

    @DexIgnore
    public xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        throw new UnsupportedOperationException("create(Any?;Continuation) has not been overridden");
    }
}
