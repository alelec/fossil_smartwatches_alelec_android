package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa5 implements Factory<af5> {
    @DexIgnore
    public static af5 a(na5 na5) {
        af5 e = na5.e();
        z76.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }
}
