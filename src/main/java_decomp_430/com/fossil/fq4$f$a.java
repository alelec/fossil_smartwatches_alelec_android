package com.fossil;

import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$f$a extends MediaControllerCompat.a {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.f d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$callback$1$onSessionDestroyed$1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fq4$f$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fq4$f$a fq4_f_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = fq4_f_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                MediaBrowserCompat i = this.this$0.d.i();
                if (i != null) {
                    i.b();
                }
                this.this$0.d.e();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public fq4$f$a(MusicControlComponent.f fVar, String str) {
        this.d = fVar;
        this.e = str;
    }

    @DexIgnore
    public void a(PlaybackStateCompat playbackStateCompat) {
        fq4$f$a.super.a(playbackStateCompat);
        int a2 = this.d.a(playbackStateCompat);
        if (a2 != this.d.h()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = MusicControlComponent.o.a();
            StringBuilder sb = new StringBuilder();
            sb.append(".onPlaybackStateChanged, PlaybackStateCompat = ");
            sb.append(playbackStateCompat != null ? Integer.valueOf(playbackStateCompat.getState()) : null);
            local.d(a3, sb.toString());
            int h = this.d.h();
            this.d.a(a2);
            MusicControlComponent.f fVar = this.d;
            fVar.a(h, a2, (MusicControlComponent.b) fVar);
        }
    }

    @DexIgnore
    public void b() {
        fq4$f$a.super.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MusicControlComponent.o.a();
        local.d(a2, ".onSessionReady, package=" + this.d.c());
    }

    @DexIgnore
    public void a(MediaMetadataCompat mediaMetadataCompat) {
        fq4$f$a.super.a(mediaMetadataCompat);
        if (mediaMetadataCompat != null) {
            String a2 = mediaMetadataCompat.a("android.media.metadata.TITLE");
            String a3 = mediaMetadataCompat.a("android.media.metadata.ARTIST");
            String a4 = mediaMetadataCompat.a("android.media.metadata.ALBUM");
            if (a2 == null && a3 == null && a4 == null) {
                FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "Ignore all null metadata");
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a5 = MusicControlComponent.o.a();
            StringBuilder sb = new StringBuilder();
            sb.append(".onMetadataChanged, title=");
            sb.append(a2);
            sb.append(", artist=");
            sb.append(a3);
            sb.append(", album=");
            sb.append(a4);
            sb.append(", state=");
            MediaControllerCompat j = this.d.j();
            sb.append(j != null ? j.b() : null);
            local.d(a5, sb.toString());
            MusicControlComponent.c cVar = new MusicControlComponent.c(this.d.c(), this.e, a2 != null ? a2 : "Unknown", a3 != null ? a3 : "Unknown", a4 != null ? a4 : "Unknown");
            if (!wg6.a((Object) cVar, (Object) this.d.g())) {
                MusicControlComponent.c g = this.d.g();
                this.d.a(cVar);
                this.d.a(g, cVar);
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a6 = MusicControlComponent.o.a();
            local2.d(a6, "Ignore this metadata " + cVar);
        }
    }

    @DexIgnore
    public void a() {
        fq4$f$a.super.a();
        MusicControlComponent.f fVar = this.d;
        fVar.a((MusicControlComponent.b) fVar);
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
    }
}
