package com.fossil;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l62 implements DynamiteModule.b {
    @DexIgnore
    public final DynamiteModule.b.C0061b a(Context context, String str, DynamiteModule.b.a aVar) throws DynamiteModule.a {
        DynamiteModule.b.C0061b bVar = new DynamiteModule.b.C0061b();
        bVar.a = aVar.a(context, str);
        if (bVar.a != 0) {
            bVar.c = -1;
        } else {
            bVar.b = aVar.a(context, str, true);
            if (bVar.b != 0) {
                bVar.c = 1;
            }
        }
        return bVar;
    }
}
