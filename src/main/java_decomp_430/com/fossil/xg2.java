package com.fossil;

import android.os.RemoteException;
import com.fossil.aw2;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg2 extends aw2.a<dw2> {
    @DexIgnore
    public /* final */ /* synthetic */ bw2 s;
    @DexIgnore
    public /* final */ /* synthetic */ String t; // = null;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xg2(wg2 wg2, wv1 wv1, bw2 bw2, String str) {
        super(wv1);
        this.s = bw2;
    }

    @DexIgnore
    public final /* synthetic */ ew1 a(Status status) {
        return new dw2(status);
    }

    @DexIgnore
    public final /* synthetic */ void a(rv1.b bVar) throws RemoteException {
        ((og2) bVar).a(this.s, (ow1<dw2>) this, this.t);
    }
}
