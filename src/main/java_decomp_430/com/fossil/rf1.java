package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rf1 extends p41 {
    @DexIgnore
    public /* final */ byte[] E;
    @DexIgnore
    public byte[] F;
    @DexIgnore
    public /* final */ rg1 G;
    @DexIgnore
    public /* final */ rg1 H;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ rf1(ue1 ue1, sv0 sv0, lx0 lx0, int i, int i2) {
        super(lx0, ue1, (i2 & 8) != 0 ? 3 : i);
        this.E = sv0.a();
        byte[] array = ByteBuffer.allocate(2).put(sv0.c.a()).put(sv0.d.a).array();
        wg6.a(array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        this.F = array;
        rg1 rg1 = rg1.AUTHENTICATION;
        this.G = rg1;
        this.H = rg1;
    }

    @DexIgnore
    public final long a(sg1 sg1) {
        return 0;
    }

    @DexIgnore
    public final sj0 a(byte b) {
        return hz0.f.a(b);
    }

    @DexIgnore
    public final rg1 m() {
        return this.H;
    }

    @DexIgnore
    public final byte[] o() {
        return this.E;
    }

    @DexIgnore
    public final rg1 p() {
        return this.G;
    }

    @DexIgnore
    public final byte[] r() {
        return this.F;
    }
}
