package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i14 implements Factory<AuthApiGuestService> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public i14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static i14 a(b14 b14) {
        return new i14(b14);
    }

    @DexIgnore
    public static AuthApiGuestService b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static AuthApiGuestService c(b14 b14) {
        AuthApiGuestService d = b14.d();
        z76.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    public AuthApiGuestService get() {
        return b(this.a);
    }
}
