package com.fossil;

import com.fossil.fn2;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rm2 implements tq2 {
    @DexIgnore
    public /* final */ pm2 a;

    @DexIgnore
    public rm2(pm2 pm2) {
        hn2.a(pm2, "output");
        this.a = pm2;
        this.a.a = this;
    }

    @DexIgnore
    public static rm2 a(pm2 pm2) {
        rm2 rm2 = pm2.a;
        if (rm2 != null) {
            return rm2;
        }
        return new rm2(pm2);
    }

    @DexIgnore
    public final void b(int i, Object obj, fp2 fp2) throws IOException {
        pm2 pm2 = this.a;
        pm2.a(i, 3);
        fp2.a((ro2) obj, pm2.a);
        pm2.a(i, 4);
    }

    @DexIgnore
    public final int zza() {
        return fn2.e.k;
    }

    @DexIgnore
    public final void zzb(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    public final void zzc(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    public final void zzd(int i, long j) throws IOException {
        this.a.c(i, j);
    }

    @DexIgnore
    public final void zze(int i, int i2) throws IOException {
        this.a.c(i, i2);
    }

    @DexIgnore
    public final void zzf(int i, int i2) throws IOException {
        this.a.d(i, i2);
    }

    @DexIgnore
    public final void zzg(int i, List<Double> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.b(list.get(i4).doubleValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzh(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.k(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzi(int i, List<Boolean> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.b(list.get(i4).booleanValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzj(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.g(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzk(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.j(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzl(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.h(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzm(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.h(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.d(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzn(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.f(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.b(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zza(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    public final void zzc(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    public final void zzd(int i, int i2) throws IOException {
        this.a.e(i, i2);
    }

    @DexIgnore
    public final void zze(int i, long j) throws IOException {
        this.a.b(i, j);
    }

    @DexIgnore
    public final void zzf(int i, List<Float> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.b(list.get(i4).floatValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).floatValue());
            i2++;
        }
    }

    @DexIgnore
    public final void a(int i, yl2 yl2) throws IOException {
        this.a.a(i, yl2);
    }

    @DexIgnore
    public final void zzb(int i, int i2) throws IOException {
        this.a.b(i, i2);
    }

    @DexIgnore
    public final void zzc(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.d(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzd(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.e(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zze(int i, List<Long> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.g(list.get(i4).longValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.c(i, list.get(i2).longValue());
            i2++;
        }
    }

    @DexIgnore
    public final void a(int i, Object obj, fp2 fp2) throws IOException {
        this.a.a(i, (ro2) obj, fp2);
    }

    @DexIgnore
    public final void zza(int i, long j) throws IOException {
        this.a.a(i, j);
    }

    @DexIgnore
    public final void a(int i, List<?> list, fp2 fp2) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            b(i, (Object) list.get(i2), fp2);
        }
    }

    @DexIgnore
    public final void b(int i, List<?> list, fp2 fp2) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a(i, (Object) list.get(i2), fp2);
        }
    }

    @DexIgnore
    public final void zzb(int i) throws IOException {
        this.a.a(i, 4);
    }

    @DexIgnore
    public final void zza(int i, float f) throws IOException {
        this.a.a(i, f);
    }

    @DexIgnore
    public final void zzb(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.i(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.e(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zza(int i, double d) throws IOException {
        this.a.a(i, d);
    }

    @DexIgnore
    public final void zza(int i, boolean z) throws IOException {
        this.a.a(i, z);
    }

    @DexIgnore
    public final void zza(int i, String str) throws IOException {
        this.a.a(i, str);
    }

    @DexIgnore
    public final void zza(int i) throws IOException {
        this.a.a(i, 3);
    }

    @DexIgnore
    public final void zza(int i, Object obj) throws IOException {
        if (obj instanceof yl2) {
            this.a.b(i, (yl2) obj);
        } else {
            this.a.a(i, (ro2) obj);
        }
    }

    @DexIgnore
    public final void zza(int i, List<Integer> list, boolean z) throws IOException {
        int i2 = 0;
        if (z) {
            this.a.a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += pm2.f(list.get(i4).intValue());
            }
            this.a.b(i3);
            while (i2 < list.size()) {
                this.a.a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.b(i, list.get(i2).intValue());
            i2++;
        }
    }

    @DexIgnore
    public final void zzb(int i, List<yl2> list) throws IOException {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.a.a(i, list.get(i2));
        }
    }

    @DexIgnore
    public final void zza(int i, List<String> list) throws IOException {
        int i2 = 0;
        if (list instanceof xn2) {
            xn2 xn2 = (xn2) list;
            while (i2 < list.size()) {
                Object zzb = xn2.zzb(i2);
                if (zzb instanceof String) {
                    this.a.a(i, (String) zzb);
                } else {
                    this.a.a(i, (yl2) zzb);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.a.a(i, list.get(i2));
            i2++;
        }
    }
}
