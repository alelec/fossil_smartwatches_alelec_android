package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.uw1;
import com.google.android.gms.location.LocationRequest;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg2 {
    @DexIgnore
    public /* final */ vg2<dg2> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ Map<uw1.a<zv2>, mg2> d; // = new HashMap();
    @DexIgnore
    public /* final */ Map<uw1.a<Object>, lg2> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<uw1.a<yv2>, ig2> f; // = new HashMap();

    @DexIgnore
    public hg2(Context context, vg2<dg2> vg2) {
        this.b = context;
        this.a = vg2;
    }

    @DexIgnore
    public final Location a() throws RemoteException {
        this.a.a();
        return this.a.b().zza(this.b.getPackageName());
    }

    @DexIgnore
    public final mg2 a(uw1<zv2> uw1) {
        mg2 mg2;
        synchronized (this.d) {
            mg2 = this.d.get(uw1.b());
            if (mg2 == null) {
                mg2 = new mg2(uw1);
            }
            this.d.put(uw1.b(), mg2);
        }
        return mg2;
    }

    @DexIgnore
    public final void a(rg2 rg2, uw1<yv2> uw1, ag2 ag2) throws RemoteException {
        this.a.a();
        this.a.b().a(new tg2(1, rg2, (IBinder) null, (PendingIntent) null, b(uw1).asBinder(), ag2 != null ? ag2.asBinder() : null));
    }

    @DexIgnore
    public final void a(uw1.a<zv2> aVar, ag2 ag2) throws RemoteException {
        this.a.a();
        w12.a(aVar, (Object) "Invalid null listener key");
        synchronized (this.d) {
            mg2 remove = this.d.remove(aVar);
            if (remove != null) {
                remove.q();
                this.a.b().a(tg2.a((ex2) remove, ag2));
            }
        }
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, uw1<zv2> uw1, ag2 ag2) throws RemoteException {
        this.a.a();
        this.a.b().a(new tg2(1, rg2.a(locationRequest), a(uw1).asBinder(), (PendingIntent) null, (IBinder) null, ag2 != null ? ag2.asBinder() : null));
    }

    @DexIgnore
    public final void a(boolean z) throws RemoteException {
        this.a.a();
        this.a.b().e(z);
        this.c = z;
    }

    @DexIgnore
    public final ig2 b(uw1<yv2> uw1) {
        ig2 ig2;
        synchronized (this.f) {
            ig2 = this.f.get(uw1.b());
            if (ig2 == null) {
                ig2 = new ig2(uw1);
            }
            this.f.put(uw1.b(), ig2);
        }
        return ig2;
    }

    @DexIgnore
    public final void b() throws RemoteException {
        synchronized (this.d) {
            for (mg2 next : this.d.values()) {
                if (next != null) {
                    this.a.b().a(tg2.a((ex2) next, (ag2) null));
                }
            }
            this.d.clear();
        }
        synchronized (this.f) {
            for (ig2 next2 : this.f.values()) {
                if (next2 != null) {
                    this.a.b().a(tg2.a((bx2) next2, (ag2) null));
                }
            }
            this.f.clear();
        }
        synchronized (this.e) {
            for (lg2 next3 : this.e.values()) {
                if (next3 != null) {
                    this.a.b().a(new eh2(2, (ch2) null, next3.asBinder(), (IBinder) null));
                }
            }
            this.e.clear();
        }
    }

    @DexIgnore
    public final void b(uw1.a<yv2> aVar, ag2 ag2) throws RemoteException {
        this.a.a();
        w12.a(aVar, (Object) "Invalid null listener key");
        synchronized (this.f) {
            ig2 remove = this.f.remove(aVar);
            if (remove != null) {
                remove.q();
                this.a.b().a(tg2.a((bx2) remove, ag2));
            }
        }
    }

    @DexIgnore
    public final void c() throws RemoteException {
        if (this.c) {
            a(false);
        }
    }
}
