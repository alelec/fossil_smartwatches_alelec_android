package com.fossil;

import com.google.maps.model.TravelMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum nh4 {
    CAR(r2, r3),
    BUS(r2, r3),
    WALK(r2, r3),
    BIKE(r2, r3);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ TravelMode travelMode;
    @DexIgnore
    public /* final */ String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
            r6 = r3.getTravelMode();
         */
        @DexIgnore
        public final TravelMode a(String str) {
            nh4 nh4;
            TravelMode travelMode;
            wg6.b(str, "type");
            nh4[] values = nh4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    nh4 = null;
                    break;
                }
                nh4 = values[i];
                if (wg6.a((Object) nh4.getType(), (Object) str)) {
                    break;
                }
                i++;
            }
            return (nh4 == null || travelMode == null) ? nh4.CAR.getTravelMode() : travelMode;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /* JADX WARNING: type inference failed for: r3v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public nh4(TravelMode travelMode2, String str) {
        this.travelMode = travelMode2;
        this.type = str;
    }

    @DexIgnore
    public static final TravelMode getTravelModeFromType(String str) {
        return Companion.a(str);
    }

    @DexIgnore
    public final TravelMode getTravelMode() {
        return this.travelMode;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }
}
