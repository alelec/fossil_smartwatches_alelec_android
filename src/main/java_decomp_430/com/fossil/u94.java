package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u94 extends t94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public long B;

    /*
    static {
        D.put(2131362065, 1);
        D.put(2131362544, 2);
        D.put(2131362914, 3);
        D.put(2131362442, 4);
        D.put(2131362323, 5);
        D.put(2131362516, 6);
        D.put(2131362185, 7);
        D.put(2131362172, 8);
        D.put(2131362428, 9);
        D.put(2131362225, 10);
        D.put(2131362066, 11);
        D.put(2131362545, 12);
        D.put(2131363218, 13);
        D.put(2131362552, 14);
        D.put(2131362425, 15);
        D.put(2131362400, 16);
        D.put(2131362218, 17);
    }
    */

    @DexIgnore
    public u94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 18, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public u94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[11], objArr[8], objArr[7], objArr[17], objArr[10], objArr[5], objArr[16], objArr[15], objArr[9], objArr[4], objArr[6], objArr[2], objArr[12], objArr[14], objArr[0], objArr[3], objArr[13]);
        this.B = -1;
        this.A.setTag((Object) null);
        a(view);
        f();
    }
}
