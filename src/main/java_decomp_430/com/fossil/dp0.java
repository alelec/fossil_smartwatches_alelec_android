package com.fossil;

import com.facebook.internal.Utility;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp0 {
    @DexIgnore
    public /* synthetic */ dp0(qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(long j, sl0 sl0) {
        ln0 ln0;
        Calendar instance = Calendar.getInstance();
        wg6.a(instance, "time");
        instance.setTimeInMillis(j);
        byte[] bArr = new byte[10];
        int i = instance.get(1);
        bArr[0] = (byte) (i & 255);
        bArr[1] = (byte) ((i >> 8) & 255);
        bArr[2] = (byte) (instance.get(2) + 1);
        bArr[3] = (byte) instance.get(5);
        bArr[4] = (byte) instance.get(11);
        bArr[5] = (byte) instance.get(12);
        bArr[6] = (byte) instance.get(13);
        switch (instance.get(7)) {
            case 1:
                ln0 = ln0.DAY_SUNDAY;
                break;
            case 2:
                ln0 = ln0.DAY_MONDAY;
                break;
            case 3:
                ln0 = ln0.DAY_TUESDAY;
                break;
            case 4:
                ln0 = ln0.DAY_WEDNESDAY;
                break;
            case 5:
                ln0 = ln0.DAY_THURSDAY;
                break;
            case 6:
                ln0 = ln0.DAY_FRIDAY;
                break;
            case 7:
                ln0 = ln0.DAY_SATURDAY;
                break;
            default:
                ln0 = ln0.DAY_UNKNOWN;
                break;
        }
        bArr[7] = ln0.a;
        bArr[8] = (byte) (instance.get(14) / 256);
        bArr[9] = sl0.a;
        return bArr;
    }

    @DexIgnore
    public final byte[] a(long j) {
        vq0 vq0;
        Calendar instance = Calendar.getInstance();
        wg6.a(instance, "time");
        instance.setTimeInMillis(j);
        byte[] bArr = new byte[2];
        bArr[0] = (byte) (instance.get(15) / 900000);
        int i = instance.get(16) / Utility.REFRESH_TIME_FOR_EXTENDED_DEVICE_INFO_MILLIS;
        if (i == 0) {
            vq0 = vq0.DST_STANDARD;
        } else if (i == 1) {
            vq0 = vq0.DST_HALF;
        } else if (i == 2) {
            vq0 = vq0.DST_SINGLE;
        } else if (i != 4) {
            vq0 = vq0.DST_UNKNOWN;
        } else {
            vq0 = vq0.DST_DOUBLE;
        }
        bArr[1] = vq0.a;
        return bArr;
    }
}
