package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c51 extends vh {
    @DexIgnore
    public c51(w81 w81, oh ohVar) {
        super(ohVar);
    }

    @DexIgnore
    public String createQuery() {
        return "delete from DeviceFile where deviceMacAddress = ? and fileType = ?";
    }
}
