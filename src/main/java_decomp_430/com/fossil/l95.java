package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l95 implements MembersInjector<HybridCustomizeEditFragment> {
    @DexIgnore
    public static void a(HybridCustomizeEditFragment hybridCustomizeEditFragment, MicroAppPresenter microAppPresenter) {
        hybridCustomizeEditFragment.o = microAppPresenter;
    }

    @DexIgnore
    public static void a(HybridCustomizeEditFragment hybridCustomizeEditFragment, w04 w04) {
        hybridCustomizeEditFragment.p = w04;
    }
}
