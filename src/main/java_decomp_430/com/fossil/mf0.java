package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mf0 {
    @DexIgnore
    public /* synthetic */ mf0(qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a() {
        return pk0.c;
    }

    @DexIgnore
    public final byte[] b() {
        return pk0.b;
    }

    @DexIgnore
    public final byte[] c() {
        return pk0.a;
    }

    @DexIgnore
    public final vi0 a(UUID uuid) {
        if (wg6.a(uuid, mi0.A.a())) {
            return vi0.CLIENT_CHARACTERISTIC_CONFIGURATION;
        }
        return vi0.UNKNOWN;
    }
}
