package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.InAppNotification;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface tw4 extends zq4<sw4> {
    @DexIgnore
    void I();

    @DexIgnore
    void P();

    @DexIgnore
    void a(Intent intent);

    @DexIgnore
    void a(FossilDeviceSerialPatternUtil.DEVICE device, int i);

    @DexIgnore
    void a(InAppNotification inAppNotification);

    @DexIgnore
    void a(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration);

    @DexIgnore
    void b(boolean z);

    @DexIgnore
    void c(int i);

    @DexIgnore
    void onActivityResult(int i, int i2, Intent intent);
}
