package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oe4 extends ne4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j t; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray u; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public long s;

    /*
    static {
        u.put(2131362561, 2);
        u.put(2131362825, 3);
        u.put(2131362442, 4);
        u.put(2131362311, 5);
        u.put(2131362228, 6);
    }
    */

    @DexIgnore
    public oe4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, t, u));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.s = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.s != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.s = 1;
        }
        g();
    }

    @DexIgnore
    public oe4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[6], objArr[5], objArr[4], objArr[2], objArr[3], objArr[1]);
        this.s = -1;
        this.r = objArr[0];
        this.r.setTag((Object) null);
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
