package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy0 implements Parcelable.Creator<zz0> {
    @DexIgnore
    public /* synthetic */ fy0(qg6 qg6) {
    }

    @DexIgnore
    public zz0 createFromParcel(Parcel parcel) {
        return new zz0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new zz0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m16createFromParcel(Parcel parcel) {
        return new zz0(parcel, (qg6) null);
    }
}
