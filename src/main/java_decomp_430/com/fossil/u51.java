package com.fossil;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u51 extends if1 {
    @DexIgnore
    public static /* final */ b91 H; // = b91.CBC_NO_PADDING;
    @DexIgnore
    public static /* final */ byte[] I; // = new byte[16];
    @DexIgnore
    public static /* final */ zu0 J; // = new zu0((qg6) null);
    @DexIgnore
    public byte[] B; // = new byte[8];
    @DexIgnore
    public byte[] C; // = new byte[8];
    @DexIgnore
    public byte[] D; // = new byte[0];
    @DexIgnore
    public /* final */ ArrayList<hl1> E; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.AUTHENTICATION}));
    @DexIgnore
    public /* final */ nq0 F;
    @DexIgnore
    public /* final */ byte[] G;

    @DexIgnore
    public u51(ue1 ue1, q41 q41, nq0 nq0, byte[] bArr, String str) {
        super(ue1, q41, eh1.AUTHENTICATE, str);
        this.F = nq0;
        this.G = bArr;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.E;
    }

    @DexIgnore
    public void h() {
        new SecureRandom().nextBytes(this.B);
        byte[] bArr = this.G;
        if (bArr == null) {
            a(sk1.SECRET_KEY_IS_REQUIRED);
        } else if (bArr.length < 16) {
            a(sk1.INVALID_PARAMETER);
        } else {
            byte[] copyOf = Arrays.copyOf(bArr, 16);
            wg6.a(copyOf, "java.util.Arrays.copyOf(this, newSize)");
            this.D = copyOf;
            ue1 ue1 = this.w;
            nq0 nq0 = this.F;
            if1.a((if1) this, (qv0) new tm1(ue1, nq0, nq0.f.a(nq0, this.B)), (hg6) new c21(this), (hg6) new y31(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
        }
    }

    @DexIgnore
    public JSONObject i() {
        Object obj;
        JSONObject a = cw0.a(super.i(), bm0.AUTHENTICATION_KEY_TYPE, (Object) cw0.a((Enum<?>) this.F));
        bm0 bm0 = bm0.SECRET_KEY_CRC;
        byte[] bArr = this.G;
        if (bArr != null) {
            obj = Long.valueOf(h51.a.a(bArr, q11.CRC32));
        } else {
            obj = JSONObject.NULL;
        }
        return cw0.a(a, bm0, obj);
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(cw0.a(super.k(), bm0.PHONE_RANDOM_NUMBER, (Object) cw0.a(this.B, (String) null, 1)), bm0.DEVICE_RANDOM_NUMBER, (Object) cw0.a(this.C, (String) null, 1));
    }

    @DexIgnore
    public final void a(byte[] bArr) {
        if (bArr.length != 16) {
            a(km1.a(this.v, (eh1) null, sk1.INVALID_DATA_LENGTH, (bn0) null, 5));
            return;
        }
        byte[] a = nq0.f.a(this.F, xa1.a.a(H, this.D, I, bArr));
        if (a.length != 16) {
            a(km1.a(this.v, (eh1) null, sk1.INVALID_DATA_LENGTH, (bn0) null, 5));
            return;
        }
        this.C = md6.a(a, 0, 8);
        if (Arrays.equals(this.B, md6.a(a, 8, 16))) {
            byte[] a2 = md6.a(this.B, this.C);
            vo0 vo0 = nq0.f;
            nq0 nq0 = this.F;
            b91 b91 = H;
            byte[] bArr2 = this.D;
            byte[] bArr3 = I;
            e71 e71 = e71.ENCRYPT;
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr2, "AES");
            Cipher instance = Cipher.getInstance(b91.a);
            instance.init(e71.a, secretKeySpec, new IvParameterSpec(bArr3));
            byte[] doFinal = instance.doFinal(a2);
            wg6.a(doFinal, "cipher.doFinal(data)");
            byte[] a3 = vo0.a(nq0, doFinal);
            if1.a((if1) this, (qv0) new bl1(this.w, this.F, a3), (hg6) uw0.a, (hg6) oy0.a, (ig6) null, (hg6) new i01(this), (hg6) null, 40, (Object) null);
            return;
        }
        a(km1.a(this.v, (eh1) null, sk1.WRONG_RANDOM_NUMBER, (bn0) null, 5));
    }
}
