package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hd3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qc3 a;
    @DexIgnore
    public /* final */ /* synthetic */ gd3 b;

    @DexIgnore
    public hd3(gd3 gd3, qc3 qc3) {
        this.b = gd3;
        this.a = qc3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onSuccess(this.a.b());
            }
        }
    }
}
