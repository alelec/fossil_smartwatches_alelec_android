package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ri2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ ov2.c f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ri2(ov2.c cVar, Activity activity) {
        super(ov2.this);
        this.f = cVar;
        this.e = activity;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        ov2.this.g.onActivityStopped(z52.a(this.e), this.b);
    }
}
