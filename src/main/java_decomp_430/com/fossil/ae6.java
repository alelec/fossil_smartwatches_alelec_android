package com.fossil;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ae6 implements List, Serializable, RandomAccess, ph6 {
    @DexIgnore
    public static /* final */ ae6 INSTANCE; // = new ae6();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7390468764508069838L;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public /* synthetic */ void add(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void add(int i, Void voidR) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean add(Void voidR) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof Void) {
            return contains((Void) obj);
        }
        return false;
    }

    @DexIgnore
    public boolean contains(Void voidR) {
        wg6.b(voidR, "element");
        return false;
    }

    @DexIgnore
    public boolean containsAll(Collection collection) {
        wg6.b(collection, "elements");
        return collection.isEmpty();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof List) && ((List) obj).isEmpty();
    }

    @DexIgnore
    public int getSize() {
        return 0;
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }

    @DexIgnore
    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof Void) {
            return indexOf((Void) obj);
        }
        return -1;
    }

    @DexIgnore
    public int indexOf(Void voidR) {
        wg6.b(voidR, "element");
        return -1;
    }

    @DexIgnore
    public boolean isEmpty() {
        return true;
    }

    @DexIgnore
    public Iterator iterator() {
        return zd6.a;
    }

    @DexIgnore
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof Void) {
            return lastIndexOf((Void) obj);
        }
        return -1;
    }

    @DexIgnore
    public int lastIndexOf(Void voidR) {
        wg6.b(voidR, "element");
        return -1;
    }

    @DexIgnore
    public ListIterator listIterator() {
        return zd6.a;
    }

    @DexIgnore
    public Void remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public /* synthetic */ Object set(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public Void set(int i, Void voidR) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return getSize();
    }

    @DexIgnore
    public List subList(int i, int i2) {
        if (i == 0 && i2 == 0) {
            return this;
        }
        throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2);
    }

    @DexIgnore
    public Object[] toArray() {
        return pg6.a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return pg6.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return "[]";
    }

    @DexIgnore
    public Void get(int i) {
        throw new IndexOutOfBoundsException("Empty list doesn't contain element at index " + i + '.');
    }

    @DexIgnore
    public ListIterator listIterator(int i) {
        if (i == 0) {
            return zd6.a;
        }
        throw new IndexOutOfBoundsException("Index: " + i);
    }
}
