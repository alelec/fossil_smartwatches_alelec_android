package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m03 implements Parcelable.Creator<j03> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        i03 i03 = null;
        String str2 = null;
        long j = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                str = f22.e(parcel, a);
            } else if (a2 == 3) {
                i03 = f22.a(parcel, a, i03.CREATOR);
            } else if (a2 == 4) {
                str2 = f22.e(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                j = f22.s(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new j03(str, i03, str2, j);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new j03[i];
    }
}
