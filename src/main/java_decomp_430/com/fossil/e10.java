package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.fossil.a20;
import com.fossil.a86;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e10 extends a86.b {
    @DexIgnore
    public /* final */ y10 a;
    @DexIgnore
    public /* final */ i10 b;

    @DexIgnore
    public e10(y10 y10, i10 i10) {
        this.a = y10;
        this.b = i10;
    }

    @DexIgnore
    public void a(Activity activity) {
    }

    @DexIgnore
    public void a(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public void b(Activity activity) {
        this.a.a(activity, a20.c.PAUSE);
        this.b.b();
    }

    @DexIgnore
    public void b(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public void c(Activity activity) {
        this.a.a(activity, a20.c.RESUME);
        this.b.c();
    }

    @DexIgnore
    public void d(Activity activity) {
        this.a.a(activity, a20.c.START);
    }

    @DexIgnore
    public void e(Activity activity) {
        this.a.a(activity, a20.c.STOP);
    }
}
