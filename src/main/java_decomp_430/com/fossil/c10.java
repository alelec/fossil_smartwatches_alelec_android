package com.fossil;

import android.content.Context;
import com.fossil.a20;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c10 implements la6 {
    @DexIgnore
    public /* final */ i86 a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ d10 c;
    @DexIgnore
    public /* final */ d20 d;
    @DexIgnore
    public /* final */ va6 e;
    @DexIgnore
    public /* final */ o10 f;
    @DexIgnore
    public /* final */ ScheduledExecutorService g;
    @DexIgnore
    public z10 h; // = new k10();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ fb6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(fb6 fb6, String str) {
            this.a = fb6;
            this.b = str;
        }

        @DexIgnore
        public void run() {
            try {
                c10.this.h.a(this.a, this.b);
            } catch (Exception e) {
                c86.g().e("Answers", "Failed to set analytics settings data", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            try {
                z10 z10 = c10.this.h;
                c10.this.h = new k10();
                z10.d();
            } catch (Exception e) {
                c86.g().e("Answers", "Failed to disable events", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                c10.this.h.a();
            } catch (Exception e) {
                c86.g().e("Answers", "Failed to send events files", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            try {
                b20 a2 = c10.this.d.a();
                w10 a3 = c10.this.c.a();
                a3.a(c10.this);
                c10.this.h = new l10(c10.this.a, c10.this.b, c10.this.g, a3, c10.this.e, a2, c10.this.f);
            } catch (Exception e) {
                c86.g().e("Answers", "Failed to enable events", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            try {
                c10.this.h.b();
            } catch (Exception e) {
                c86.g().e("Answers", "Failed to flush events", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ a20.b a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public f(a20.b bVar, boolean z) {
            this.a = bVar;
            this.b = z;
        }

        @DexIgnore
        public void run() {
            try {
                c10.this.h.a(this.a);
                if (this.b) {
                    c10.this.h.b();
                }
            } catch (Exception e) {
                c86.g().e("Answers", "Failed to process event", e);
            }
        }
    }

    @DexIgnore
    public c10(i86 i86, Context context, d10 d10, d20 d20, va6 va6, ScheduledExecutorService scheduledExecutorService, o10 o10) {
        this.a = i86;
        this.b = context;
        this.c = d10;
        this.d = d20;
        this.e = va6;
        this.g = scheduledExecutorService;
        this.f = o10;
    }

    @DexIgnore
    public void a(a20.b bVar) {
        a(bVar, false, false);
    }

    @DexIgnore
    public void b(a20.b bVar) {
        a(bVar, false, true);
    }

    @DexIgnore
    public void c(a20.b bVar) {
        a(bVar, true, false);
    }

    @DexIgnore
    public void a(fb6 fb6, String str) {
        a((Runnable) new a(fb6, str));
    }

    @DexIgnore
    public void b() {
        a((Runnable) new d());
    }

    @DexIgnore
    public void c() {
        a((Runnable) new e());
    }

    @DexIgnore
    public void a() {
        a((Runnable) new b());
    }

    @DexIgnore
    public final void b(Runnable runnable) {
        try {
            this.g.submit(runnable).get();
        } catch (Exception e2) {
            c86.g().e("Answers", "Failed to run events task", e2);
        }
    }

    @DexIgnore
    public void a(String str) {
        a((Runnable) new c());
    }

    @DexIgnore
    public void a(a20.b bVar, boolean z, boolean z2) {
        f fVar = new f(bVar, z2);
        if (z) {
            b((Runnable) fVar);
        } else {
            a((Runnable) fVar);
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        try {
            this.g.submit(runnable);
        } catch (Exception e2) {
            c86.g().e("Answers", "Failed to submit events task", e2);
        }
    }
}
