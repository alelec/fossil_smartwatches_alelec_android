package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bq6 {
    @DexIgnore
    public static final bq6 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements bq6 {
        @DexIgnore
        public yq6 authenticate(ar6 ar6, Response response) {
            return null;
        }
    }

    @DexIgnore
    yq6 authenticate(ar6 ar6, Response response) throws IOException;
}
