package com.fossil;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQuery;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ni implements ii {
    @DexIgnore
    public static /* final */ String[] b; // = new String[0];
    @DexIgnore
    public /* final */ SQLiteDatabase a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ li a;

        @DexIgnore
        public a(ni niVar, li liVar) {
            this.a = liVar;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.a(new qi(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements SQLiteDatabase.CursorFactory {
        @DexIgnore
        public /* final */ /* synthetic */ li a;

        @DexIgnore
        public b(ni niVar, li liVar) {
            this.a = liVar;
        }

        @DexIgnore
        public Cursor newCursor(SQLiteDatabase sQLiteDatabase, SQLiteCursorDriver sQLiteCursorDriver, String str, SQLiteQuery sQLiteQuery) {
            this.a.a(new qi(sQLiteQuery));
            return new SQLiteCursor(sQLiteCursorDriver, str, sQLiteQuery);
        }
    }

    /*
    static {
        new String[]{"", " OR ROLLBACK ", " OR ABORT ", " OR FAIL ", " OR IGNORE ", " OR REPLACE "};
    }
    */

    @DexIgnore
    public ni(SQLiteDatabase sQLiteDatabase) {
        this.a = sQLiteDatabase;
    }

    @DexIgnore
    public boolean A() {
        return this.a.inTransaction();
    }

    @DexIgnore
    public Cursor a(li liVar) {
        return this.a.rawQueryWithFactory(new a(this, liVar), liVar.b(), b, (String) null);
    }

    @DexIgnore
    public void b(String str) throws SQLException {
        this.a.execSQL(str);
    }

    @DexIgnore
    public mi c(String str) {
        return new ri(this.a.compileStatement(str));
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public Cursor d(String str) {
        return a((li) new hi(str));
    }

    @DexIgnore
    public boolean isOpen() {
        return this.a.isOpen();
    }

    @DexIgnore
    public void u() {
        this.a.beginTransaction();
    }

    @DexIgnore
    public List<Pair<String, String>> v() {
        return this.a.getAttachedDbs();
    }

    @DexIgnore
    public void x() {
        this.a.setTransactionSuccessful();
    }

    @DexIgnore
    public void y() {
        this.a.endTransaction();
    }

    @DexIgnore
    public String z() {
        return this.a.getPath();
    }

    @DexIgnore
    public Cursor a(li liVar, CancellationSignal cancellationSignal) {
        return this.a.rawQueryWithFactory(new b(this, liVar), liVar.b(), b, (String) null, cancellationSignal);
    }

    @DexIgnore
    public boolean a(SQLiteDatabase sQLiteDatabase) {
        return this.a == sQLiteDatabase;
    }
}
