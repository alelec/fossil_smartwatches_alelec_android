package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<u60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final u60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 4) {
                return new u60(bArr[0], bArr[1], bArr[2], bArr[3]);
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 4"));
        }

        @DexIgnore
        public u60 createFromParcel(Parcel parcel) {
            return new u60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new u60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m61createFromParcel(Parcel parcel) {
            return new u60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public u60(byte b2, byte b3, byte b4, byte b5) throws IllegalArgumentException {
        super(s60.DO_NOT_DISTURB_SCHEDULE);
        this.b = b2;
        this.c = b3;
        this.d = b4;
        this.e = b5;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(this.b).put(this.c).put(this.d).put(this.e).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        byte b2 = this.b;
        boolean z = true;
        if (b2 >= 0 && 23 >= b2) {
            byte b3 = this.c;
            if (b3 >= 0 && 59 >= b3) {
                byte b4 = this.d;
                if (b4 >= 0 && 23 >= b4) {
                    byte b5 = this.e;
                    if (b5 < 0 || 59 < b5) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalArgumentException(ze0.a(ze0.b("stopMinute("), this.e, ") is out of range ", "[0, 59]."));
                    }
                    return;
                }
                throw new IllegalArgumentException(ze0.a(ze0.b("stopHour("), this.d, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("startMinute("), this.c, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("startHour("), this.b, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(u60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            u60 u60 = (u60) obj;
            return this.b == u60.b && this.c == u60.c && this.d == u60.d && this.e == u60.e;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DoNotDisturbScheduleConfig");
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.b;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.c;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.d;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((super.hashCode() * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.b));
            jSONObject.put("start_minute", Byte.valueOf(this.c));
            jSONObject.put("stop_hour", Byte.valueOf(this.d));
            jSONObject.put("stop_minute", Byte.valueOf(this.e));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ u60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        e();
    }
}
