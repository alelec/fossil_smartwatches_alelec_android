package com.fossil;

import android.content.Context;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaControllerCompat;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MediaAppDetails;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$OldVersionMusicController$connect$1", f = "MusicControlComponent.kt", l = {873, 559, 566, 570}, m = "invokeSuspend")
public final class fq4$f$b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.f this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends sf6 implements ig6<il6, xe6<? super MediaBrowserCompat>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ il6 $this_launch$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fq4$f$b this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xe6 xe6, fq4$f$b fq4_f_b, il6 il6) {
            super(2, xe6);
            this.this$0 = fq4_f_b;
            this.$this_launch$inlined = il6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(xe6, this.this$0, this.$this_launch$inlined);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                MusicControlComponent.f fVar = this.this$0.this$0;
                Context b = fVar.h;
                MediaAppDetails c = this.this$0.this$0.i;
                this.L$0 = il6;
                this.label = 1;
                obj = fVar.a(b, c, (xe6<? super MediaBrowserCompat>) this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super MediaControllerCompat>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MediaBrowserCompat $it;
        @DexIgnore
        public /* final */ /* synthetic */ il6 $this_launch$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fq4$f$b this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(MediaBrowserCompat mediaBrowserCompat, xe6 xe6, fq4$f$b fq4_f_b, il6 il6) {
            super(2, xe6);
            this.$it = mediaBrowserCompat;
            this.this$0 = fq4_f_b;
            this.$this_launch$inlined = il6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$it, xe6, this.this$0, this.$this_launch$inlined);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return this.this$0.this$0.b(this.$it);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ il6 $this_launch$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ fq4$f$b this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xe6 xe6, fq4$f$b fq4_f_b, il6 il6) {
            super(2, xe6);
            this.this$0 = fq4_f_b;
            this.$this_launch$inlined = il6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(xe6, this.this$0, this.$this_launch$inlined);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                MediaControllerCompat j = this.this$0.this$0.j();
                if (j != null) {
                    j.a(this.this$0.this$0.f());
                    return cd6.a;
                }
                wg6.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$f$b(MusicControlComponent.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        fq4$f$b fq4_f_b = new fq4$f$b(this.this$0, xe6);
        fq4_f_b.p$ = (il6) obj;
        return fq4_f_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((fq4$f$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e1 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0120 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0141 A[Catch:{ all -> 0x0065 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0148 A[Catch:{ all -> 0x0065 }] */
    public final Object invokeSuspend(Object obj) {
        xp6 xp6;
        xp6 xp62;
        il6 il6;
        rl6 rl6;
        MediaBrowserCompat mediaBrowserCompat;
        MusicControlComponent.f fVar;
        MusicControlComponent.f fVar2;
        MediaBrowserCompat i;
        il6 il62;
        xp6 xp63;
        Object a2 = ff6.a();
        int i2 = this.label;
        if (i2 == 0) {
            nc6.a(obj);
            il62 = this.p$;
            xp63 = ((MusicControlComponent) MusicControlComponent.o.a(PortfolioApp.get.instance())).g();
            this.L$0 = il62;
            this.L$1 = xp63;
            this.label = 1;
            if (xp63.a((Object) null, this) == a2) {
                return a2;
            }
        } else if (i2 == 1) {
            xp63 = (xp6) this.L$1;
            nc6.a(obj);
            il62 = (il6) this.L$0;
        } else if (i2 == 2) {
            fVar2 = (MusicControlComponent.f) this.L$3;
            rl6 = (rl6) this.L$2;
            xp6 = (xp6) this.L$1;
            il6 = (il6) this.L$0;
            nc6.a(obj);
            fVar2.a((MediaBrowserCompat) obj);
            i = this.this$0.i();
            if (i != null) {
                this.this$0.a((MusicControlComponent.f) null);
                xp62 = xp6;
                cd6 cd6 = cd6.a;
                xp62.a((Object) null);
                return cd6.a;
            } else if (!i.d()) {
                this.this$0.a((MusicControlComponent.f) null);
                cd6 cd62 = cd6.a;
                xp6.a((Object) null);
                return cd62;
            } else {
                fVar = this.this$0;
                cn6 c2 = zl6.c();
                b bVar = new b(i, (xe6) null, this, il6);
                this.L$0 = il6;
                this.L$1 = xp6;
                this.L$2 = rl6;
                this.L$3 = i;
                this.L$4 = fVar;
                this.label = 3;
                Object a3 = gk6.a(c2, bVar, this);
                if (a3 == a2) {
                    return a2;
                }
                Object obj2 = a3;
                mediaBrowserCompat = i;
                obj = obj2;
                fVar.a((MediaControllerCompat) obj);
                if (this.this$0.j() == null) {
                }
                xp62 = xp6;
                cd6 cd63 = cd6.a;
                xp62.a((Object) null);
                return cd6.a;
            }
        } else if (i2 == 3) {
            fVar = (MusicControlComponent.f) this.L$4;
            mediaBrowserCompat = (MediaBrowserCompat) this.L$3;
            rl6 = (rl6) this.L$2;
            xp6 = (xp6) this.L$1;
            il6 = (il6) this.L$0;
            try {
                nc6.a(obj);
                fVar.a((MediaControllerCompat) obj);
                if (this.this$0.j() == null) {
                    this.this$0.a(this.this$0);
                    cn6 c3 = zl6.c();
                    c cVar = new c((xe6) null, this, il6);
                    this.L$0 = il6;
                    this.L$1 = xp6;
                    this.L$2 = rl6;
                    this.L$3 = mediaBrowserCompat;
                    this.label = 4;
                    if (gk6.a(c3, cVar, this) == a2) {
                        return a2;
                    }
                } else {
                    this.this$0.a((MusicControlComponent.f) null);
                }
                xp62 = xp6;
                cd6 cd632 = cd6.a;
                xp62.a((Object) null);
                return cd6.a;
            } catch (Throwable th) {
                th = th;
            }
        } else if (i2 == 4) {
            MediaBrowserCompat mediaBrowserCompat2 = (MediaBrowserCompat) this.L$3;
            rl6 rl62 = (rl6) this.L$2;
            xp62 = (xp6) this.L$1;
            il6 il63 = (il6) this.L$0;
            try {
                nc6.a(obj);
                xp6 = xp62;
                xp62 = xp6;
                cd6 cd6322 = cd6.a;
                xp62.a((Object) null);
                return cd6.a;
            } catch (Throwable th2) {
                th = th2;
                xp6 = xp62;
                xp6.a((Object) null);
                throw th;
            }
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            FLogger.INSTANCE.getLocal().d(MusicControlComponent.o.a(), "connecting");
            rl6 a4 = ik6.a(il62, zl6.c(), (ll6) null, new a((xe6) null, this, il62), 2, (Object) null);
            MusicControlComponent.f fVar3 = this.this$0;
            this.L$0 = il62;
            this.L$1 = xp63;
            this.L$2 = a4;
            this.L$3 = fVar3;
            this.label = 2;
            Object a5 = a4.a(this);
            if (a5 == a2) {
                return a2;
            }
            MusicControlComponent.f fVar4 = fVar3;
            il6 = il62;
            obj = a5;
            rl6 = a4;
            xp6 = xp63;
            fVar2 = fVar4;
            fVar2.a((MediaBrowserCompat) obj);
            i = this.this$0.i();
            if (i != null) {
            }
        } catch (Throwable th3) {
            th = th3;
            xp6 = xp63;
            xp6.a((Object) null);
            throw th;
        }
    }
}
