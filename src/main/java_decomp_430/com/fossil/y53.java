package com.fossil;

import android.os.Process;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y53 extends Thread {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ BlockingQueue<v53<?>> b;
    @DexIgnore
    public /* final */ /* synthetic */ u53 c;

    @DexIgnore
    public y53(u53 u53, String str, BlockingQueue<v53<?>> blockingQueue) {
        this.c = u53;
        w12.a(str);
        w12.a(blockingQueue);
        this.b = blockingQueue;
        setName(str);
    }

    @DexIgnore
    public final void a() {
        synchronized (this.a) {
            this.a.notifyAll();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0065, code lost:
        r1 = r6.c.i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006b, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r6.c.j.release();
        r6.c.i.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0084, code lost:
        if (r6 != r6.c.c) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0086, code lost:
        com.fossil.y53 unused = r6.c.c = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0092, code lost:
        if (r6 != r6.c.d) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0094, code lost:
        com.fossil.y53 unused = r6.c.d = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009a, code lost:
        r6.c.b().t().a("Current scheduler thread is neither worker nor network");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00a9, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00aa, code lost:
        return;
     */
    @DexIgnore
    public final void run() {
        boolean z = false;
        while (!z) {
            try {
                this.c.j.acquire();
                z = true;
            } catch (InterruptedException e) {
                a(e);
            }
        }
        try {
            int threadPriority = Process.getThreadPriority(Process.myTid());
            while (true) {
                v53 v53 = (v53) this.b.poll();
                if (v53 != null) {
                    Process.setThreadPriority(v53.b ? threadPriority : 10);
                    v53.run();
                } else {
                    synchronized (this.a) {
                        if (this.b.peek() == null && !this.c.k) {
                            try {
                                this.a.wait(30000);
                            } catch (InterruptedException e2) {
                                a(e2);
                            }
                        }
                    }
                    synchronized (this.c.i) {
                        if (this.b.peek() == null) {
                        }
                    }
                }
            }
        } catch (Throwable th) {
            synchronized (this.c.i) {
                this.c.j.release();
                this.c.i.notifyAll();
                if (this == this.c.c) {
                    y53 unused = this.c.c = null;
                } else if (this == this.c.d) {
                    y53 unused2 = this.c.d = null;
                } else {
                    this.c.b().t().a("Current scheduler thread is neither worker nor network");
                }
                throw th;
            }
        }
    }

    @DexIgnore
    public final void a(InterruptedException interruptedException) {
        this.c.b().w().a(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
    }
}
