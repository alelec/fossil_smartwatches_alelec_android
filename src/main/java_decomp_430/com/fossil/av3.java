package com.fossil;

import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class av3 {
    @DexIgnore
    public /* final */ Map<Type, eu3<?>> a;
    @DexIgnore
    public /* final */ pv3 b; // = pv3.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements fv3<T> {
        @DexIgnore
        public a(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new ConcurrentHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements fv3<T> {
        @DexIgnore
        public b(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new TreeMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements fv3<T> {
        @DexIgnore
        public c(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new LinkedHashMap();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements fv3<T> {
        @DexIgnore
        public d(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new ev3();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements fv3<T> {
        @DexIgnore
        public /* final */ jv3 a; // = jv3.a();
        @DexIgnore
        public /* final */ /* synthetic */ Class b;
        @DexIgnore
        public /* final */ /* synthetic */ Type c;

        @DexIgnore
        public e(av3 av3, Class cls, Type type) {
            this.b = cls;
            this.c = type;
        }

        @DexIgnore
        public T a() {
            try {
                return this.a.a(this.b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.c + ". Registering an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements fv3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ eu3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public f(av3 av3, eu3 eu3, Type type) {
            this.a = eu3;
            this.b = type;
        }

        @DexIgnore
        public T a() {
            return this.a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements fv3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ eu3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Type b;

        @DexIgnore
        public g(av3 av3, eu3 eu3, Type type) {
            this.a = eu3;
            this.b = type;
        }

        @DexIgnore
        public T a() {
            return this.a.createInstance(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements fv3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Constructor a;

        @DexIgnore
        public h(av3 av3, Constructor constructor) {
            this.a = constructor;
        }

        @DexIgnore
        public T a() {
            try {
                return this.a.newInstance((Object[]) null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements fv3<T> {
        @DexIgnore
        public i(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new TreeSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements fv3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;

        @DexIgnore
        public j(av3 av3, Type type) {
            this.a = type;
        }

        @DexIgnore
        public T a() {
            Type type = this.a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return EnumSet.noneOf((Class) type2);
                }
                throw new iu3("Invalid EnumSet type: " + this.a.toString());
            }
            throw new iu3("Invalid EnumSet type: " + this.a.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k implements fv3<T> {
        @DexIgnore
        public k(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new LinkedHashSet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l implements fv3<T> {
        @DexIgnore
        public l(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new ArrayDeque();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m implements fv3<T> {
        @DexIgnore
        public m(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new ArrayList();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n implements fv3<T> {
        @DexIgnore
        public n(av3 av3) {
        }

        @DexIgnore
        public T a() {
            return new ConcurrentSkipListMap();
        }
    }

    @DexIgnore
    public av3(Map<Type, eu3<?>> map) {
        this.a = map;
    }

    @DexIgnore
    public <T> fv3<T> a(TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        eu3 eu3 = this.a.get(type);
        if (eu3 != null) {
            return new f(this, eu3, type);
        }
        eu3 eu32 = this.a.get(rawType);
        if (eu32 != null) {
            return new g(this, eu32, type);
        }
        fv3<T> a2 = a(rawType);
        if (a2 != null) {
            return a2;
        }
        fv3<T> a3 = a(type, rawType);
        if (a3 != null) {
            return a3;
        }
        return b(type, rawType);
    }

    @DexIgnore
    public final <T> fv3<T> b(Type type, Class<? super T> cls) {
        return new e(this, cls, type);
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public final <T> fv3<T> a(Class<? super T> cls) {
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.b.a(declaredConstructor);
            }
            return new h(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    @DexIgnore
    public final <T> fv3<T> a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            if (SortedSet.class.isAssignableFrom(cls)) {
                return new i(this);
            }
            if (EnumSet.class.isAssignableFrom(cls)) {
                return new j(this, type);
            }
            if (Set.class.isAssignableFrom(cls)) {
                return new k(this);
            }
            if (Queue.class.isAssignableFrom(cls)) {
                return new l(this);
            }
            return new m(this);
        } else if (!Map.class.isAssignableFrom(cls)) {
            return null;
        } else {
            if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
                return new n(this);
            }
            if (ConcurrentMap.class.isAssignableFrom(cls)) {
                return new a(this);
            }
            if (SortedMap.class.isAssignableFrom(cls)) {
                return new b(this);
            }
            if (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]).getRawType())) {
                return new d(this);
            }
            return new c(this);
        }
    }
}
