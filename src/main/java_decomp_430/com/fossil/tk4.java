package com.fossil;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tk4 {
    @DexIgnore
    public static /* final */ DecimalFormat a; // = new DecimalFormat();

    @DexIgnore
    public static String a(float f, int i) {
        String str = "###,###";
        if (i > 0) {
            str = str + ".";
        }
        StringBuilder sb = new StringBuilder(str);
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("#");
        }
        a.applyPattern(sb.toString());
        return a.format((double) f);
    }

    @DexIgnore
    public static String b(float f, int i) {
        if (f < 100000.0f) {
            return a(f, i);
        }
        float f2 = f / 1000.0f;
        if (i == 0) {
            i = 1;
        }
        return a(f2, i) + " k";
    }

    @DexIgnore
    public static float c(float f, int i) {
        double d = (double) f;
        double d2 = (double) i;
        return ((float) Math.round(d * Math.pow(10.0d, d2))) / ((float) Math.pow(10.0d, d2));
    }

    @DexIgnore
    public static String c(int i) {
        try {
            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            decimalFormat.applyPattern("#,###,###");
            return decimalFormat.format((long) i);
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(i);
        }
    }

    @DexIgnore
    public static String b(int i) {
        try {
            NumberFormat instance = NumberFormat.getInstance(Locale.US);
            instance.setMaximumFractionDigits(0);
            return instance.format((long) i);
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(i);
        }
    }

    @DexIgnore
    public static String a(int i) {
        String str;
        if (i < 1000) {
            return String.valueOf(i);
        }
        if (i >= 20000) {
            i /= 1000;
            str = " k";
        } else {
            str = "";
        }
        try {
            NumberFormat instance = NumberFormat.getInstance(Locale.US);
            instance.setMaximumFractionDigits(0);
            return instance.format((long) i) + str;
        } catch (Exception e) {
            e.printStackTrace();
            return String.valueOf(i);
        }
    }

    @DexIgnore
    public static int a(String str) {
        try {
            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            decimalFormat.applyPattern("#,###,###");
            return decimalFormat.parse(str).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            return Integer.parseInt(str.replace(",", ""));
        }
    }
}
