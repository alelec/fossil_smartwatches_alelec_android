package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t00 {
    @DexIgnore
    public static void a() {
    }

    @DexIgnore
    public static void a(String str) {
    }

    @DexIgnore
    public static void a(String str, Object obj) {
    }

    @DexIgnore
    public static void a(String str, Object obj, Object obj2, Object obj3) {
    }
}
