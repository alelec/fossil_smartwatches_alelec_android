package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yx5<T> {
    @DexIgnore
    public static /* final */ a e; // = new a((qg6) null);
    @DexIgnore
    public /* final */ wh4 a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ Integer c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T> yx5<T> a(int i, String str, T t) {
            wg6.b(str, "msg");
            return new yx5<>(wh4.ERROR, t, Integer.valueOf(i), str);
        }

        @DexIgnore
        public final <T> yx5<T> b(T t) {
            return new yx5<>(wh4.NETWORK_LOADING, t, (Integer) null, (String) null);
        }

        @DexIgnore
        public final <T> yx5<T> c(T t) {
            return new yx5<>(wh4.SUCCESS, t, (Integer) null, (String) null);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final <T> yx5<T> a(T t) {
            return new yx5<>(wh4.DATABASE_LOADING, t, (Integer) null, (String) null);
        }
    }

    @DexIgnore
    public yx5(wh4 wh4, T t, Integer num, String str) {
        wg6.b(wh4, "status");
        this.a = wh4;
        this.b = t;
        this.c = num;
        this.d = str;
    }

    @DexIgnore
    public final wh4 a() {
        return this.a;
    }

    @DexIgnore
    public final T b() {
        return this.b;
    }

    @DexIgnore
    public final Integer c() {
        return this.c;
    }

    @DexIgnore
    public final T d() {
        return this.b;
    }

    @DexIgnore
    public final String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof yx5)) {
            return false;
        }
        yx5 yx5 = (yx5) obj;
        return wg6.a((Object) this.a, (Object) yx5.a) && wg6.a((Object) this.b, (Object) yx5.b) && wg6.a((Object) this.c, (Object) yx5.c) && wg6.a((Object) this.d, (Object) yx5.d);
    }

    @DexIgnore
    public final wh4 f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        wh4 wh4 = this.a;
        int i = 0;
        int hashCode = (wh4 != null ? wh4.hashCode() : 0) * 31;
        T t = this.b;
        int hashCode2 = (hashCode + (t != null ? t.hashCode() : 0)) * 31;
        Integer num = this.c;
        int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
        String str = this.d;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public String toString() {
        return "Resource(status=" + this.a + ", data=" + this.b + ", code=" + this.c + ", message=" + this.d + ")";
    }
}
