package com.fossil;

import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.OutputFormat;
import com.fossil.imagefilters.OutputSettings;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
public final class b75$f$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ EditPhotoViewModel.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b75$f$a(EditPhotoViewModel.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        b75$f$a b75_f_a = new b75$f$a(this.this$0, xe6);
        b75_f_a.p$ = (il6) obj;
        return b75_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((b75$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            EInkImageFilter create = EInkImageFilter.create();
            wg6.a((Object) create, "EInkImageFilter.create()");
            OutputSettings outputSettings = new OutputSettings(this.this$0.$bitmap.getWidth(), this.this$0.$bitmap.getHeight(), OutputFormat.BACKGROUND);
            EditPhotoViewModel.f fVar = this.this$0;
            FilterResult apply = create.apply(fVar.$bitmap, fVar.$filter, false, false, outputSettings);
            wg6.a((Object) apply, "algorithm.apply(bitmap, \u2026e, false, outputSettings)");
            this.this$0.this$0.a().a(new EditPhotoViewModel.a(apply));
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
