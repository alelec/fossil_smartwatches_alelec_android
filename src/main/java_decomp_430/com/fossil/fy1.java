package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fy1 implements fw1<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ ax1 a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ wv1 c;
    @DexIgnore
    public /* final */ /* synthetic */ ay1 d;

    @DexIgnore
    public fy1(ay1 ay1, ax1 ax1, boolean z, wv1 wv1) {
        this.d = ay1;
        this.a = ax1;
        this.b = z;
        this.c = wv1;
    }

    @DexIgnore
    public final /* synthetic */ void onResult(ew1 ew1) {
        Status status = (Status) ew1;
        vt1.a(this.d.g).e();
        if (status.F() && this.d.g()) {
            this.d.k();
        }
        this.a.a(status);
        if (this.b) {
            this.c.d();
        }
    }
}
