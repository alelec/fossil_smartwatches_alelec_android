package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jz3 extends qz3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (qx3 == qx3.CODE_93) {
            return super.a(str, qx3, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_93, but got " + qx3);
    }

    @DexIgnore
    public boolean[] a(String str) {
        int length = str.length();
        if (length <= 80) {
            int[] iArr = new int[9];
            boolean[] zArr = new boolean[(((str.length() + 2 + 2) * 9) + 1)];
            a(iz3.a[47], iArr);
            int a = a(zArr, 0, iArr, true);
            for (int i = 0; i < length; i++) {
                a(iz3.a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(str.charAt(i))], iArr);
                a += a(zArr, a, iArr, true);
            }
            int a2 = a(str, 20);
            a(iz3.a[a2], iArr);
            int a3 = a + a(zArr, a, iArr, true);
            a(iz3.a[a(str + "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".charAt(a2), 15)], iArr);
            int a4 = a3 + a(zArr, a3, iArr, true);
            a(iz3.a[47], iArr);
            zArr[a4 + a(zArr, a4, iArr, true)] = true;
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length);
    }

    @DexIgnore
    public static void a(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            int i3 = 1;
            if (((1 << (8 - i2)) & i) == 0) {
                i3 = 0;
            }
            iArr[i2] = i3;
        }
    }

    @DexIgnore
    public static int a(boolean[] zArr, int i, int[] iArr, boolean z) {
        int length = iArr.length;
        int i2 = i;
        int i3 = 0;
        while (i3 < length) {
            int i4 = i2 + 1;
            zArr[i2] = iArr[i3] != 0;
            i3++;
            i2 = i4;
        }
        return 9;
    }

    @DexIgnore
    public static int a(String str, int i) {
        int i2 = 0;
        int i3 = 1;
        for (int length = str.length() - 1; length >= 0; length--) {
            i2 += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(str.charAt(length)) * i3;
            i3++;
            if (i3 > i) {
                i3 = 1;
            }
        }
        return i2 % 47;
    }
}
