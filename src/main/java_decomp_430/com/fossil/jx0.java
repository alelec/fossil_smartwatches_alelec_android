package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jx0 extends j61 {
    @DexIgnore
    public long A; // = 10000;
    @DexIgnore
    public mw0 B; // = mw0.BOND_NONE;

    @DexIgnore
    public jx0(ue1 ue1) {
        super(lx0.CREATE_BOND, ue1);
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public long e() {
        return this.A;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) this.y.getBondState()));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NEW_BOND_STATE, (Object) cw0.a((Enum<?>) this.B));
    }

    @DexIgnore
    public ok0 l() {
        return new vu0(this.y.v);
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.B = ((vu0) ok0).j;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.NEW_BOND_STATE, (Object) cw0.a((Enum<?>) this.B)), 7));
    }
}
