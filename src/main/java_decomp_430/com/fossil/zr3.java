package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Intent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zr3 {
    @DexIgnore
    public /* final */ Intent a;
    @DexIgnore
    public /* final */ BroadcastReceiver.PendingResult b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ ScheduledFuture<?> d;

    @DexIgnore
    public zr3(Intent intent, BroadcastReceiver.PendingResult pendingResult, ScheduledExecutorService scheduledExecutorService) {
        this.a = intent;
        this.b = pendingResult;
        this.d = scheduledExecutorService.schedule(new yr3(this, intent), 9000, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public final synchronized void a() {
        if (!this.c) {
            this.b.finish();
            this.d.cancel(false);
            this.c = true;
        }
    }
}
