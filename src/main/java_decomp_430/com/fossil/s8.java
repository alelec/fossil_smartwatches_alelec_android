package com.fossil;

import android.util.Log;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s8 extends Writer {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public StringBuilder b; // = new StringBuilder(128);

    @DexIgnore
    public s8(String str) {
        this.a = str;
    }

    @DexIgnore
    public void close() {
        k();
    }

    @DexIgnore
    public void flush() {
        k();
    }

    @DexIgnore
    public final void k() {
        if (this.b.length() > 0) {
            Log.d(this.a, this.b.toString());
            StringBuilder sb = this.b;
            sb.delete(0, sb.length());
        }
    }

    @DexIgnore
    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                k();
            } else {
                this.b.append(c);
            }
        }
    }
}
