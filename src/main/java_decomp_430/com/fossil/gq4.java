package com.fossil;

import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class gq4 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[NotifyMusicEventResponse.MusicMediaAction.values().length];

    /*
    static {
        a[NotifyMusicEventResponse.MusicMediaAction.PLAY.ordinal()] = 1;
        a[NotifyMusicEventResponse.MusicMediaAction.PAUSE.ordinal()] = 2;
        a[NotifyMusicEventResponse.MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
        a[NotifyMusicEventResponse.MusicMediaAction.NEXT.ordinal()] = 4;
        a[NotifyMusicEventResponse.MusicMediaAction.PREVIOUS.ordinal()] = 5;
        a[NotifyMusicEventResponse.MusicMediaAction.VOLUME_UP.ordinal()] = 6;
        a[NotifyMusicEventResponse.MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
    }
    */
}
