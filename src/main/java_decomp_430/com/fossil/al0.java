package com.fossil;

import com.fossil.q40;
import com.fossil.r40;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al0 extends if1 {
    @DexIgnore
    public static /* final */ ArrayList<rg1> P; // = qd6.a(new rg1[]{rg1.DC, rg1.FTC, rg1.FTD, rg1.AUTHENTICATION, rg1.ASYNC, rg1.FTD_1});
    @DexIgnore
    public r40 B;
    @DexIgnore
    public /* final */ ArrayList<hl1> C; // = new ArrayList<>();
    @DexIgnore
    public long D; // = System.currentTimeMillis();
    @DexIgnore
    public long E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<rg1> G;
    @DexIgnore
    public int H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public boolean K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public /* final */ HashMap<d41, Object> O;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public al0(ue1 ue1, q41 q41, HashMap<d41, Object> hashMap, String str) {
        super(r1, q41, eh1.MAKE_DEVICE_READY, str);
        ue1 ue12 = ue1;
        this.O = hashMap;
        r40 r40 = r2;
        r40 r402 = new r40(ue1.d(), ue12.t, "", "", "", (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262112);
        this.B = r40;
        pd6.a(mi0.A.w());
        qd6.c(new rg1[]{rg1.MODEL_NUMBER, rg1.SERIAL_NUMBER, rg1.FIRMWARE_VERSION, rg1.DC, rg1.FTC, rg1.FTD, rg1.AUTHENTICATION, rg1.ASYNC});
        this.G = new CopyOnWriteArrayList<>();
    }

    @DexIgnore
    public static final /* synthetic */ void b(al0 al0) {
        al0.F++;
        if1.a((if1) al0, (qv0) new n41(al0.w), (hg6) new vf0(al0), (hg6) new mh0(al0), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public boolean a(qv0 qv0) {
        lx0 lx0 = qv0 != null ? qv0.x : null;
        if (lx0 != null && z51.c[lx0.ordinal()] == 1) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public Object d() {
        return this.B;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(super.i(), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) this.w.getBondState())), bm0.BLUETOOTH_DEVICE_TYPE, (Object) Integer.valueOf(this.w.w.getType()));
    }

    @DexIgnore
    public JSONObject k() {
        return cw0.a(cw0.a(cw0.a(super.k(), bm0.CONNECT_DURATION, (Object) Long.valueOf(this.E)), bm0.CONNECT_COUNT, (Object) Integer.valueOf(this.M)), bm0.DISCONNECT_COUNT, (Object) Integer.valueOf(this.N));
    }

    @DexIgnore
    public final void m() {
        if1.a((if1) this, (qv0) new ec1(w31.ALL_FILE.a, this.w, 0, 4), (hg6) w71.a, (hg6) t91.a, (ig6) null, (hg6) new ob1(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public final void n() {
        if1.a((if1) this, (qv0) new jz0(this.w), (hg6) new fj0(this), (hg6) zk0.a, (ig6) null, (hg6) new sm0(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public final void o() {
        if1.a((if1) this, (qv0) new m81(do1.b.b(), this.w), (hg6) o01.a, (hg6) i21.a, (ig6) null, (hg6) new e41(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public final void p() {
        int i;
        cc0 cc0 = cc0.DEBUG;
        if (this.H >= this.G.size()) {
            Hashtable hashtable = new Hashtable();
            hashtable.put(hl1.DEVICE_INFORMATION, Integer.MAX_VALUE);
            for (rg1 rg1 : this.G) {
                hl1 a = rg1.a();
                switch (ve1.b[rg1.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        i = Integer.MAX_VALUE;
                        break;
                    case 8:
                    case 9:
                    case 10:
                        i = 1;
                        break;
                    default:
                        i = 0;
                        break;
                }
                Integer num = (Integer) hashtable.get(a);
                if (num == null) {
                    num = 0;
                }
                wg6.a(num, "resourceQuotas[resourceType] ?: 0");
                hashtable.put(a, Integer.valueOf(num.intValue() + i));
            }
            this.x.b().a(new nj1(hashtable, (qg6) null));
            if1.a((if1) this, (qv0) new fz0(512, this.w), (hg6) new ax0(this), (hg6) new uy0(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
            return;
        }
        rg1 rg12 = this.G.get(this.H);
        wg6.a(rg12, "characteristicsToSubscri\u2026ibingCharacteristicIndex]");
        if1.a((if1) this, (qv0) new fa1(rg12, true, this.w), (hg6) new a61(this), (hg6) new u91(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    public final void q() {
        boolean z;
        long longValue;
        long a;
        if (!this.K) {
            Boolean bool = (Boolean) this.O.get(d41.AUTO_CONNECT);
            if (!(bool != null ? bool.booleanValue() : false)) {
                z = false;
                this.K = false;
                Long l = (Long) this.O.get(d41.CONNECTION_TIME_OUT);
                longValue = l == null ? l.longValue() : 30000;
                a = fm0.f.a(z);
                if (longValue != 0) {
                    a = Math.min(longValue - (System.currentTimeMillis() - this.D), a);
                }
                if (a >= 0) {
                    a(this.v);
                    return;
                }
                this.I = System.currentTimeMillis();
                ov0 ov0 = new ov0(this.w, z, a);
                ov0.w = this.L;
                this.L = false;
                this.M++;
                if1.a((if1) this, (qv0) ov0, (hg6) new kd1(this), (hg6) new rk1(this), (ig6) null, (hg6) new jm1(this), (hg6) zn1.a, 8, (Object) null);
                return;
            }
        }
        z = true;
        this.K = false;
        Long l2 = (Long) this.O.get(d41.CONNECTION_TIME_OUT);
        if (l2 == null) {
        }
        a = fm0.f.a(z);
        if (longValue != 0) {
        }
        if (a >= 0) {
        }
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.C;
    }

    @DexIgnore
    public void h() {
        q41 q41 = this.x;
        if (q41.a.s == q40.c.CONNECTED) {
            this.B = q41.a();
            a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
            return;
        }
        this.D = System.currentTimeMillis();
        this.L = true;
        q();
    }

    @DexIgnore
    public boolean a(if1 if1) {
        return b(if1) || (if1.f().isEmpty() && this.y != if1.y);
    }

    @DexIgnore
    public final void b(km1 km1) {
        if (km1.b == sk1.SUCCESS) {
            a(km1);
            return;
        }
        this.v = km1;
        this.N++;
        if1.a((if1) this, (qv0) new r21(this.w), (hg6) bh1.a, (hg6) wi1.a, (ig6) null, (hg6) new qk1(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public void a(h91 h91) {
        if (z51.a[h91.ordinal()] == 1) {
            qv0 qv0 = this.b;
            if (qv0 == null || qv0.t) {
                if1 if1 = this.n;
                if (if1 == null || if1.t) {
                    a(sk1.CONNECTION_DROPPED);
                }
            }
        }
    }
}
