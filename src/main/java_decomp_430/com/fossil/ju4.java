package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.pj5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju4 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ an4 a; // = this.e.w();
    @DexIgnore
    public /* final */ ArrayList<pj5.b> b;
    @DexIgnore
    public /* final */ fr c;
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ImageView a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ ImageView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ CardView h;
        @DexIgnore
        public /* final */ /* synthetic */ ju4 i;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ju4$a$a")
        /* renamed from: com.fossil.ju4$a$a  reason: collision with other inner class name */
        public static final class C0018a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0018a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b c;
                if (this.a.i.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (c = this.a.i.d) != null) {
                    c.R(((HomeProfilePresenter.b) this.a.i.b.get(this.a.getAdapterPosition())).c());
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public void onImageCallback(String str, String str2) {
                wg6.b(str, "serial");
                wg6.b(str2, "filePath");
                this.a.i.c.a(str2).a(this.a.a);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ju4 ju4, View view) {
            super(view);
            wg6.b(view, "view");
            this.i = ju4;
            this.a = (ImageView) view.findViewById(2131362566);
            this.b = (ImageView) view.findViewById(2131362622);
            this.c = (FlexibleTextView) view.findViewById(2131363139);
            this.d = (FlexibleTextView) view.findViewById(2131363164);
            this.e = view.findViewById(2131362026);
            this.f = (ImageView) view.findViewById(2131362624);
            this.g = (FlexibleTextView) view.findViewById(2131363215);
            this.h = view.findViewById(2131361972);
            this.f.setOnClickListener(new C0018a(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r5v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r4v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r5v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v31, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v36, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r6v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        @SuppressLint({"SetTextI18n"})
        public final void a(HomeProfilePresenter.b bVar) {
            wg6.b(bVar, "device");
            Object r0 = this.c;
            wg6.a((Object) r0, "mTvDeviceName");
            r0.setText(cy5.d(bVar.b()));
            if (bVar.d()) {
                CardView cardView = this.h;
                wg6.a((Object) cardView, "mCardView");
                cardView.setRadius(0.0f);
                CardView cardView2 = this.h;
                wg6.a((Object) cardView2, "mCardView");
                cardView2.setCardElevation(0.0f);
                this.b.setImageResource(2131230992);
                ConstraintLayout constraintLayout = this.e;
                wg6.a((Object) constraintLayout, "mClContainer");
                constraintLayout.setAlpha(1.0f);
                ConstraintLayout constraintLayout2 = this.e;
                wg6.a((Object) constraintLayout2, "mClContainer");
                constraintLayout2.setBackground(w6.c(this.i.e, 2131230837));
                if (bVar.a() >= 0) {
                    Object r02 = this.g;
                    wg6.a((Object) r02, "mTvDeviceStatus");
                    StringBuilder sb = new StringBuilder();
                    sb.append(bVar.a());
                    sb.append('%');
                    r02.setText(sb.toString());
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, w6.c(this.i.e, DeviceHelper.o.a(bVar.a())), (Drawable) null);
                } else {
                    Object r03 = this.g;
                    wg6.a((Object) r03, "mTvDeviceStatus");
                    r03.setText("");
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                v5 v5Var = new v5();
                ImageView imageView = this.f;
                wg6.a((Object) imageView, "mIvSetting");
                imageView.setVisibility(0);
                v5Var.c(this.e);
                Object r1 = this.d;
                wg6.a((Object) r1, "mTvLastSyncedTime");
                int id = r1.getId();
                ImageView imageView2 = this.f;
                wg6.a((Object) imageView2, "mIvSetting");
                v5Var.a(id, 7, imageView2.getId(), 6);
                v5Var.a(this.e);
                if (!bVar.e()) {
                    Object r04 = this.g;
                    wg6.a((Object) r04, "mTvDeviceStatus");
                    r04.setText(jm4.a((Context) this.i.e, 2131886926));
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                Object r05 = this.d;
                wg6.a((Object) r05, "mTvLastSyncedTime");
                r05.setText(this.i.a(bVar.c(), true));
            } else {
                CardView cardView3 = this.h;
                wg6.a((Object) cardView3, "mCardView");
                cardView3.setRadius(2.0f);
                CardView cardView4 = this.h;
                wg6.a((Object) cardView4, "mCardView");
                cardView4.setCardElevation(2.0f);
                this.b.setImageResource(2131230991);
                ConstraintLayout constraintLayout3 = this.e;
                wg6.a((Object) constraintLayout3, "mClContainer");
                constraintLayout3.setAlpha(0.7f);
                ConstraintLayout constraintLayout4 = this.e;
                wg6.a((Object) constraintLayout4, "mClContainer");
                constraintLayout4.setBackground(w6.c(this.i.e, 2131230878));
                Object r06 = this.g;
                wg6.a((Object) r06, "mTvDeviceStatus");
                r06.setText(jm4.a((Context) this.i.e, 2131886926));
                this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.c.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                Object r07 = this.d;
                wg6.a((Object) r07, "mTvLastSyncedTime");
                r07.setText(this.i.a(bVar.c(), false));
            }
            String b2 = ThemeManager.l.a().b("nonBrandSurface");
            if (!TextUtils.isEmpty(b2)) {
                this.h.setBackgroundColor(Color.parseColor(b2));
            }
            CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(bVar.c()).setSerialPrefix(DeviceHelper.o.b(bVar.c())).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView3 = this.a;
            wg6.a((Object) imageView3, "mIvDevice");
            type.setPlaceHolder(imageView3, DeviceHelper.o.b(bVar.c(), DeviceHelper.ImageStyle.SMALL)).setImageCallback(new b(this)).download();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void R(String str);
    }

    @DexIgnore
    public ju4(ArrayList<pj5.b> arrayList, fr frVar, b bVar, PortfolioApp portfolioApp) {
        wg6.b(arrayList, "mData");
        wg6.b(frVar, "mRequestManager");
        wg6.b(portfolioApp, "mApp");
        this.b = arrayList;
        this.c = frVar;
        this.d = bVar;
        this.e = portfolioApp;
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final void c() {
        if (getItemCount() > 0) {
            notifyItemChanged(0);
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558671, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026le_device, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(ArrayList<pj5.b> arrayList) {
        wg6.b(arrayList, "data");
        this.b.clear();
        this.b.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            HomeProfilePresenter.b bVar = this.b.get(i);
            wg6.a((Object) bVar, "mData[position]");
            aVar.a(bVar);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r8v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final String a(String str, boolean z) {
        String str2;
        if (!z || !this.e.i(str)) {
            long g = this.a.g(str);
            if (g == 0) {
                str2 = "";
            } else if (System.currentTimeMillis() - g < 60000) {
                nh6 nh6 = nh6.a;
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886942);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                Object[] objArr = {1};
                str2 = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) str2, "java.lang.String.format(format, *args)");
            } else {
                str2 = bk4.a(g);
            }
            String string = this.e.getString(2131886928, new Object[]{str2});
            wg6.a((Object) string, "mApp.getString(R.string.\u2026ayTime, lastSyncTimeText)");
            return string;
        }
        String string2 = this.e.getString(2131886551);
        wg6.a((Object) string2, "mApp.getString(R.string.\u2026ded_Text__SyncInProgress)");
        return string2;
    }
}
