package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bb implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<bb> CREATOR; // = new b();
    @DexIgnore
    public static /* final */ bb b; // = new a();
    @DexIgnore
    public /* final */ Parcelable a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends bb {
        @DexIgnore
        public a() {
            super((a) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Parcelable.ClassLoaderCreator<bb> {
        @DexIgnore
        public bb[] newArray(int i) {
            return new bb[i];
        }

        @DexIgnore
        public bb createFromParcel(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return bb.b;
            }
            throw new IllegalStateException("superState must be null");
        }

        @DexIgnore
        public bb createFromParcel(Parcel parcel) {
            return createFromParcel(parcel, (ClassLoader) null);
        }
    }

    @DexIgnore
    public /* synthetic */ bb(a aVar) {
        this();
    }

    @DexIgnore
    public final Parcelable a() {
        return this.a;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
    }

    @DexIgnore
    public bb() {
        this.a = null;
    }

    @DexIgnore
    public bb(Parcelable parcelable) {
        if (parcelable != null) {
            this.a = parcelable == b ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    @DexIgnore
    public bb(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.a = readParcelable == null ? b : readParcelable;
    }
}
