package com.fossil;

import androidx.collection.SimpleArrayMap;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sg {
    @DexIgnore
    public /* final */ SimpleArrayMap<RecyclerView.ViewHolder, a> a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ s4<RecyclerView.ViewHolder> b; // = new s4<>();

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        void a(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);

        @DexIgnore
        void b(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);

        @DexIgnore
        void c(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2);
    }

    @DexIgnore
    public void a() {
        this.a.clear();
        this.b.a();
    }

    @DexIgnore
    public boolean b(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.a.get(viewHolder);
        if (aVar == null || (aVar.a & 1) == 0) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void c(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        a aVar = this.a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(viewHolder, aVar);
        }
        aVar.b = cVar;
        aVar.a |= 4;
    }

    @DexIgnore
    public void d(RecyclerView.ViewHolder viewHolder) {
        g(viewHolder);
    }

    @DexIgnore
    public RecyclerView.j.c e(RecyclerView.ViewHolder viewHolder) {
        return a(viewHolder, 8);
    }

    @DexIgnore
    public RecyclerView.j.c f(RecyclerView.ViewHolder viewHolder) {
        return a(viewHolder, 4);
    }

    @DexIgnore
    public void g(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.a.get(viewHolder);
        if (aVar != null) {
            aVar.a &= -2;
        }
    }

    @DexIgnore
    public void h(RecyclerView.ViewHolder viewHolder) {
        int c = this.b.c() - 1;
        while (true) {
            if (c < 0) {
                break;
            } else if (viewHolder == this.b.c(c)) {
                this.b.b(c);
                break;
            } else {
                c--;
            }
        }
        a remove = this.a.remove(viewHolder);
        if (remove != null) {
            a.a(remove);
        }
    }

    @DexIgnore
    public final RecyclerView.j.c a(RecyclerView.ViewHolder viewHolder, int i) {
        a e;
        RecyclerView.j.c cVar;
        int b2 = this.a.b((Object) viewHolder);
        if (b2 >= 0 && (e = this.a.e(b2)) != null) {
            int i2 = e.a;
            if ((i2 & i) != 0) {
                e.a = (~i) & i2;
                if (i == 4) {
                    cVar = e.b;
                } else if (i == 8) {
                    cVar = e.c;
                } else {
                    throw new IllegalArgumentException("Must provide flag PRE or POST");
                }
                if ((e.a & 12) == 0) {
                    this.a.d(b2);
                    a.a(e);
                }
                return cVar;
            }
        }
        return null;
    }

    @DexIgnore
    public void b(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        a aVar = this.a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(viewHolder, aVar);
        }
        aVar.c = cVar;
        aVar.a |= 8;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static v8<a> d; // = new w8(20);
        @DexIgnore
        public int a;
        @DexIgnore
        public RecyclerView.j.c b;
        @DexIgnore
        public RecyclerView.j.c c;

        @DexIgnore
        public static void a(a aVar) {
            aVar.a = 0;
            aVar.b = null;
            aVar.c = null;
            d.a(aVar);
        }

        @DexIgnore
        public static a b() {
            a a2 = d.a();
            return a2 == null ? new a() : a2;
        }

        @DexIgnore
        public static void a() {
            do {
            } while (d.a() != null);
        }
    }

    @DexIgnore
    public boolean c(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.a.get(viewHolder);
        return (aVar == null || (aVar.a & 4) == 0) ? false : true;
    }

    @DexIgnore
    public void b() {
        a.a();
    }

    @DexIgnore
    public void a(long j, RecyclerView.ViewHolder viewHolder) {
        this.b.c(j, viewHolder);
    }

    @DexIgnore
    public void a(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar) {
        a aVar = this.a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(viewHolder, aVar);
        }
        aVar.a |= 2;
        aVar.b = cVar;
    }

    @DexIgnore
    public RecyclerView.ViewHolder a(long j) {
        return this.b.b(j);
    }

    @DexIgnore
    public void a(RecyclerView.ViewHolder viewHolder) {
        a aVar = this.a.get(viewHolder);
        if (aVar == null) {
            aVar = a.b();
            this.a.put(viewHolder, aVar);
        }
        aVar.a |= 1;
    }

    @DexIgnore
    public void a(b bVar) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            RecyclerView.ViewHolder c = this.a.c(size);
            a d = this.a.d(size);
            int i = d.a;
            if ((i & 3) == 3) {
                bVar.a(c);
            } else if ((i & 1) != 0) {
                RecyclerView.j.c cVar = d.b;
                if (cVar == null) {
                    bVar.a(c);
                } else {
                    bVar.b(c, cVar, d.c);
                }
            } else if ((i & 14) == 14) {
                bVar.a(c, d.b, d.c);
            } else if ((i & 12) == 12) {
                bVar.c(c, d.b, d.c);
            } else if ((i & 4) != 0) {
                bVar.b(c, d.b, (RecyclerView.j.c) null);
            } else if ((i & 8) != 0) {
                bVar.a(c, d.b, d.c);
            }
            a.a(d);
        }
    }
}
