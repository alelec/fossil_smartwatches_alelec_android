package com.fossil;

import com.fossil.fj5;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hg5$b$a extends sf6 implements ig6<il6, xe6<? super List<? extends fj5.b>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepOverviewDayPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hg5$b$a(List list, xe6 xe6, SleepOverviewDayPresenter.b bVar) {
        super(2, xe6);
        this.$it = list;
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        hg5$b$a hg5_b_a = new hg5$b$a(this.$it, xe6, this.this$0);
        hg5_b_a.p$ = (il6) obj;
        return hg5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((hg5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            SleepOverviewDayPresenter sleepOverviewDayPresenter = this.this$0.this$0;
            return sleepOverviewDayPresenter.a(SleepOverviewDayPresenter.b(sleepOverviewDayPresenter), (List<MFSleepSession>) this.$it);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
