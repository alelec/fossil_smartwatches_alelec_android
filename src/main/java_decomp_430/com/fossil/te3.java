package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te3 extends e22 implements ce3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<te3> CREATOR; // = new ue3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public te3(String str, String str2, int i, boolean z) {
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = z;
    }

    @DexIgnore
    public final String B() {
        return this.a;
    }

    @DexIgnore
    public final boolean C() {
        return this.d;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof te3)) {
            return false;
        }
        return ((te3) obj).a.equals(this.a);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String p() {
        return this.b;
    }

    @DexIgnore
    public final String toString() {
        String str = this.b;
        String str2 = this.a;
        int i = this.c;
        boolean z = this.d;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 45 + String.valueOf(str2).length());
        sb.append("Node{");
        sb.append(str);
        sb.append(", id=");
        sb.append(str2);
        sb.append(", hops=");
        sb.append(i);
        sb.append(", isNearby=");
        sb.append(z);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 2, B(), false);
        g22.a(parcel, 3, p(), false);
        g22.a(parcel, 4, this.c);
        g22.a(parcel, 5, C());
        g22.a(parcel, a2);
    }
}
