package com.fossil;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import com.fossil.ae;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xd extends LoaderManager {
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public /* final */ LifecycleOwner a;
    @DexIgnore
    public /* final */ c b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<D> implements ld<D> {
        @DexIgnore
        public /* final */ ae<D> a;
        @DexIgnore
        public /* final */ LoaderManager.a<D> b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public b(ae<D> aeVar, LoaderManager.a<D> aVar) {
            this.a = aeVar;
            this.b = aVar;
        }

        @DexIgnore
        public boolean a() {
            return this.c;
        }

        @DexIgnore
        public void b() {
            if (this.c) {
                if (xd.c) {
                    Log.v("LoaderManager", "  Resetting: " + this.a);
                }
                this.b.a(this.a);
            }
        }

        @DexIgnore
        public void onChanged(D d) {
            if (xd.c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.a + ": " + this.a.dataToString(d));
            }
            this.b.a(this.a, d);
            this.c = true;
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }

        @DexIgnore
        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends td {
        @DexIgnore
        public static /* final */ ViewModelProvider.Factory c; // = new a();
        @DexIgnore
        public SparseArrayCompat<a> a; // = new SparseArrayCompat<>();
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a implements ViewModelProvider.Factory {
            @DexIgnore
            public <T extends td> T create(Class<T> cls) {
                return new c();
            }
        }

        @DexIgnore
        public static c a(ViewModelStore viewModelStore) {
            return (c) new ViewModelProvider(viewModelStore, c).a(c.class);
        }

        @DexIgnore
        public boolean b() {
            return this.b;
        }

        @DexIgnore
        public void c() {
            int c2 = this.a.c();
            for (int i = 0; i < c2; i++) {
                this.a.e(i).g();
            }
        }

        @DexIgnore
        public void d() {
            this.b = true;
        }

        @DexIgnore
        public void onCleared() {
            super.onCleared();
            int c2 = this.a.c();
            for (int i = 0; i < c2; i++) {
                this.a.e(i).a(true);
            }
            this.a.a();
        }

        @DexIgnore
        public void a() {
            this.b = false;
        }

        @DexIgnore
        public void b(int i) {
            this.a.d(i);
        }

        @DexIgnore
        public void a(int i, a aVar) {
            this.a.c(i, aVar);
        }

        @DexIgnore
        public <D> a<D> a(int i) {
            return this.a.a(i);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.a.c() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.a.c(); i++) {
                    a e = this.a.e(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.a.c(i));
                    printWriter.print(": ");
                    printWriter.println(e.toString());
                    e.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
    }

    @DexIgnore
    public xd(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        this.a = lifecycleOwner;
        this.b = c.a(viewModelStore);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final <D> ae<D> a(int i, Bundle bundle, LoaderManager.a<D> aVar, ae<D> aeVar) {
        try {
            this.b.d();
            ae<D> a2 = aVar.a(i, bundle);
            if (a2 != null) {
                if (a2.getClass().isMemberClass()) {
                    if (!Modifier.isStatic(a2.getClass().getModifiers())) {
                        throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + a2);
                    }
                }
                a aVar2 = new a(i, bundle, a2, aeVar);
                if (c) {
                    Log.v("LoaderManager", "  Created new loader " + aVar2);
                }
                this.b.a(i, aVar2);
                this.b.a();
                return aVar2.a(this.a, aVar);
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
        } catch (Throwable th) {
            this.b.a();
            throw th;
        }
    }

    @DexIgnore
    public <D> ae<D> b(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.b()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            a a2 = this.b.a(i);
            ae aeVar = null;
            if (a2 != null) {
                aeVar = a2.a(false);
            }
            return a(i, bundle, aVar, aeVar);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        r8.a(this.a, sb);
        sb.append("}}");
        return sb.toString();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<D> extends MutableLiveData<D> implements ae.c<D> {
        @DexIgnore
        public /* final */ int k;
        @DexIgnore
        public /* final */ Bundle l;
        @DexIgnore
        public /* final */ ae<D> m;
        @DexIgnore
        public LifecycleOwner n;
        @DexIgnore
        public b<D> o;
        @DexIgnore
        public ae<D> p;

        @DexIgnore
        public a(int i, Bundle bundle, ae<D> aeVar, ae<D> aeVar2) {
            this.k = i;
            this.l = bundle;
            this.m = aeVar;
            this.p = aeVar2;
            this.m.registerListener(i, this);
        }

        @DexIgnore
        public ae<D> a(LifecycleOwner lifecycleOwner, LoaderManager.a<D> aVar) {
            b<D> bVar = new b<>(this.m, aVar);
            a(lifecycleOwner, bVar);
            b<D> bVar2 = this.o;
            if (bVar2 != null) {
                b(bVar2);
            }
            this.n = lifecycleOwner;
            this.o = bVar;
            return this.m;
        }

        @DexIgnore
        public void b(ld<? super D> ldVar) {
            super.b(ldVar);
            this.n = null;
            this.o = null;
        }

        @DexIgnore
        public void d() {
            if (xd.c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.startLoading();
        }

        @DexIgnore
        public void e() {
            if (xd.c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.stopLoading();
        }

        @DexIgnore
        public ae<D> f() {
            return this.m;
        }

        @DexIgnore
        public void g() {
            LifecycleOwner lifecycleOwner = this.n;
            b<D> bVar = this.o;
            if (lifecycleOwner != null && bVar != null) {
                super.b(bVar);
                a(lifecycleOwner, bVar);
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.k);
            sb.append(" : ");
            r8.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }

        @DexIgnore
        public void b(D d) {
            super.b(d);
            ae<D> aeVar = this.p;
            if (aeVar != null) {
                aeVar.reset();
                this.p = null;
            }
        }

        @DexIgnore
        public ae<D> a(boolean z) {
            if (xd.c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.cancelLoad();
            this.m.abandon();
            b<D> bVar = this.o;
            if (bVar != null) {
                b(bVar);
                if (z) {
                    bVar.b();
                }
            }
            this.m.unregisterListener(this);
            if ((bVar == null || bVar.a()) && !z) {
                return this.m;
            }
            this.m.reset();
            return this.p;
        }

        @DexIgnore
        public void a(ae<D> aeVar, D d) {
            if (xd.c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                b(d);
                return;
            }
            if (xd.c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            a(d);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            ae<D> aeVar = this.m;
            aeVar.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                b<D> bVar = this.o;
                bVar.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(f().dataToString(a()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(c());
        }
    }

    @DexIgnore
    public <D> ae<D> a(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.b()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            a a2 = this.b.a(i);
            if (c) {
                Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (a2 == null) {
                return a(i, bundle, aVar, (ae) null);
            }
            if (c) {
                Log.v("LoaderManager", "  Re-using existing loader " + a2);
            }
            return a2.a(this.a, aVar);
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public void a(int i) {
        if (this.b.b()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            a a2 = this.b.a(i);
            if (a2 != null) {
                a2.a(true);
                this.b.b(i);
            }
        } else {
            throw new IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    @DexIgnore
    public void a() {
        this.b.c();
    }

    @DexIgnore
    @Deprecated
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }
}
