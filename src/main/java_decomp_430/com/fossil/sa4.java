package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sa4 extends ra4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public long B;

    /*
    static {
        D.put(2131362081, 1);
        D.put(2131361851, 2);
        D.put(2131363218, 3);
        D.put(2131362695, 4);
        D.put(2131361937, 5);
        D.put(2131362489, 6);
        D.put(2131363151, 7);
        D.put(2131363270, 8);
        D.put(2131361953, 9);
        D.put(2131362496, 10);
        D.put(2131363265, 11);
        D.put(2131361948, 12);
        D.put(2131362494, 13);
        D.put(2131363112, 14);
        D.put(2131363334, 15);
        D.put(2131363147, 16);
        D.put(2131363337, 17);
        D.put(2131363172, 18);
        D.put(2131363332, 19);
        D.put(2131363104, 20);
        D.put(2131363127, 21);
        D.put(2131363076, 22);
    }
    */

    @DexIgnore
    public sa4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 23, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public sa4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[5], objArr[12], objArr[9], objArr[1], objArr[6], objArr[13], objArr[10], objArr[4], objArr[0], objArr[22], objArr[20], objArr[14], objArr[21], objArr[16], objArr[7], objArr[18], objArr[3], objArr[11], objArr[8], objArr[19], objArr[15], objArr[17]);
        this.B = -1;
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
