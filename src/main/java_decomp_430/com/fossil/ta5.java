package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ta5 implements Factory<ag5> {
    @DexIgnore
    public static ag5 a(na5 na5) {
        ag5 f = na5.f();
        z76.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
