package com.fossil;

import android.database.Cursor;
import com.fossil.ji;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qh extends ji.a {
    @DexIgnore
    public fh b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public a(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(ii iiVar);

        @DexIgnore
        public abstract void dropAllTables(ii iiVar);

        @DexIgnore
        public abstract void onCreate(ii iiVar);

        @DexIgnore
        public abstract void onOpen(ii iiVar);

        @DexIgnore
        public abstract void onPostMigrate(ii iiVar);

        @DexIgnore
        public abstract void onPreMigrate(ii iiVar);

        @DexIgnore
        public b onValidateSchema(ii iiVar) {
            validateMigration(iiVar);
            return new b(true, (String) null);
        }

        @DexIgnore
        @Deprecated
        public void validateMigration(ii iiVar) {
            throw new UnsupportedOperationException("validateMigration is deprecated");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(boolean z, String str) {
            this.a = z;
            this.b = str;
        }
    }

    @DexIgnore
    public qh(fh fhVar, a aVar, String str, String str2) {
        super(aVar.version);
        this.b = fhVar;
        this.c = aVar;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public static boolean h(ii iiVar) {
        Cursor d2 = iiVar.d("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            boolean z = false;
            if (d2.moveToFirst() && d2.getInt(0) == 0) {
                z = true;
            }
            return z;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public static boolean i(ii iiVar) {
        Cursor d2 = iiVar.d("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (d2.moveToFirst() && d2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public void a(ii iiVar) {
        super.a(iiVar);
    }

    @DexIgnore
    public void b(ii iiVar, int i, int i2) {
        boolean z;
        List<xh> a2;
        fh fhVar = this.b;
        if (fhVar == null || (a2 = fhVar.d.a(i, i2)) == null) {
            z = false;
        } else {
            this.c.onPreMigrate(iiVar);
            for (xh migrate : a2) {
                migrate.migrate(iiVar);
            }
            b onValidateSchema = this.c.onValidateSchema(iiVar);
            if (onValidateSchema.a) {
                this.c.onPostMigrate(iiVar);
                g(iiVar);
                z = true;
            } else {
                throw new IllegalStateException("Migration didn't properly handle: " + onValidateSchema.b);
            }
        }
        if (!z) {
            fh fhVar2 = this.b;
            if (fhVar2 == null || fhVar2.a(i, i2)) {
                throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
            }
            this.c.dropAllTables(iiVar);
            this.c.createAllTables(iiVar);
        }
    }

    @DexIgnore
    public void c(ii iiVar) {
        boolean h = h(iiVar);
        this.c.createAllTables(iiVar);
        if (!h) {
            b onValidateSchema = this.c.onValidateSchema(iiVar);
            if (!onValidateSchema.a) {
                throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
            }
        }
        g(iiVar);
        this.c.onCreate(iiVar);
    }

    @DexIgnore
    public void d(ii iiVar) {
        super.d(iiVar);
        e(iiVar);
        this.c.onOpen(iiVar);
        this.b = null;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void e(ii iiVar) {
        if (i(iiVar)) {
            String str = null;
            Cursor a2 = iiVar.a(new hi("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (a2.moveToFirst()) {
                    str = a2.getString(0);
                }
                a2.close();
                if (!this.d.equals(str) && !this.e.equals(str)) {
                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                }
            } catch (Throwable th) {
                a2.close();
                throw th;
            }
        } else {
            b onValidateSchema = this.c.onValidateSchema(iiVar);
            if (onValidateSchema.a) {
                this.c.onPostMigrate(iiVar);
                g(iiVar);
                return;
            }
            throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
        }
    }

    @DexIgnore
    public final void f(ii iiVar) {
        iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    public final void g(ii iiVar) {
        f(iiVar);
        iiVar.b(ph.a(this.d));
    }

    @DexIgnore
    public void a(ii iiVar, int i, int i2) {
        b(iiVar, i, i2);
    }
}
