package com.fossil;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import com.fossil.wg3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xg3 extends wg3.a {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements TypeEvaluator<e> {
        @DexIgnore
        public static /* final */ TypeEvaluator<e> b; // = new b();
        @DexIgnore
        public /* final */ e a; // = new e();

        @DexIgnore
        /* renamed from: a */
        public e evaluate(float f, e eVar, e eVar2) {
            this.a.a(mi3.b(eVar.a, eVar2.a, f), mi3.b(eVar.b, eVar2.b, f), mi3.b(eVar.c, eVar2.c, f));
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Property<xg3, e> {
        @DexIgnore
        public static /* final */ Property<xg3, e> a; // = new c("circularReveal");

        @DexIgnore
        public c(String str) {
            super(e.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public e get(xg3 xg3) {
            return xg3.getRevealInfo();
        }

        @DexIgnore
        /* renamed from: a */
        public void set(xg3 xg3, e eVar) {
            xg3.setRevealInfo(eVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends Property<xg3, Integer> {
        @DexIgnore
        public static /* final */ Property<xg3, Integer> a; // = new d("circularRevealScrimColor");

        @DexIgnore
        public d(String str) {
            super(Integer.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Integer get(xg3 xg3) {
            return Integer.valueOf(xg3.getCircularRevealScrimColor());
        }

        @DexIgnore
        /* renamed from: a */
        public void set(xg3 xg3, Integer num) {
            xg3.setCircularRevealScrimColor(num.intValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public void a(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public e(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public void a(e eVar) {
            a(eVar.a, eVar.b, eVar.c);
        }

        @DexIgnore
        public boolean a() {
            return this.c == Float.MAX_VALUE;
        }

        @DexIgnore
        public e(e eVar) {
            this(eVar.a, eVar.b, eVar.c);
        }
    }

    @DexIgnore
    void a();

    @DexIgnore
    void b();

    @DexIgnore
    int getCircularRevealScrimColor();

    @DexIgnore
    e getRevealInfo();

    @DexIgnore
    void setCircularRevealOverlayDrawable(Drawable drawable);

    @DexIgnore
    void setCircularRevealScrimColor(int i);

    @DexIgnore
    void setRevealInfo(e eVar);
}
