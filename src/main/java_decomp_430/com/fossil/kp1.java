package com.fossil;

import com.fossil.rp1;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp1 extends rp1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ go1 c;

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public byte[] b() {
        return this.b;
    }

    @DexIgnore
    public go1 c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof rp1)) {
            return false;
        }
        rp1 rp1 = (rp1) obj;
        if (this.a.equals(rp1.a())) {
            if (!Arrays.equals(this.b, rp1 instanceof kp1 ? ((kp1) rp1).b : rp1.b()) || !this.c.equals(rp1.c())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b)) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends rp1.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public byte[] b;
        @DexIgnore
        public go1 c;

        @DexIgnore
        public rp1.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null backendName");
        }

        @DexIgnore
        public rp1.a a(byte[] bArr) {
            this.b = bArr;
            return this;
        }

        @DexIgnore
        public rp1.a a(go1 go1) {
            if (go1 != null) {
                this.c = go1;
                return this;
            }
            throw new NullPointerException("Null priority");
        }

        @DexIgnore
        public rp1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " backendName";
            }
            if (this.c == null) {
                str = str + " priority";
            }
            if (str.isEmpty()) {
                return new kp1(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public kp1(String str, byte[] bArr, go1 go1) {
        this.a = str;
        this.b = bArr;
        this.c = go1;
    }
}
