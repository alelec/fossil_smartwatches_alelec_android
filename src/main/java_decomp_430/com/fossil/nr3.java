package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import com.facebook.AccessToken;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nr3 {
    @DexIgnore
    public static /* final */ long d; // = TimeUnit.DAYS.toMillis(7);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    public nr3(String str, String str2, long j) {
        this.a = str;
        this.b = str2;
        this.c = j;
    }

    @DexIgnore
    public static String a(String str, String str2, long j) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(AccessToken.TOKEN_KEY, str);
            jSONObject.put("appVersion", str2);
            jSONObject.put("timestamp", j);
            return jSONObject.toString();
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
            sb.append("Failed to encode token: ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public static nr3 b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (!str.startsWith("{")) {
            return new nr3(str, (String) null, 0);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            return new nr3(jSONObject.getString(AccessToken.TOKEN_KEY), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
        } catch (JSONException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
            sb.append("Failed to parse token: ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        return System.currentTimeMillis() > this.c + d || !str.equals(this.b);
    }
}
