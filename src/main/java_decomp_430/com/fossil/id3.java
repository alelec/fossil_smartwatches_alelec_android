package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class id3<TResult, TContinuationResult> implements jc3, lc3, mc3<TContinuationResult>, kd3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ pc3<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ od3<TContinuationResult> c;

    @DexIgnore
    public id3(Executor executor, pc3<TResult, TContinuationResult> pc3, od3<TContinuationResult> od3) {
        this.a = executor;
        this.b = pc3;
        this.c = od3;
    }

    @DexIgnore
    public final void onCanceled() {
        this.c.f();
    }

    @DexIgnore
    public final void onComplete(qc3<TResult> qc3) {
        this.a.execute(new jd3(this, qc3));
    }

    @DexIgnore
    public final void onFailure(Exception exc) {
        this.c.a(exc);
    }

    @DexIgnore
    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.c.a(tcontinuationresult);
    }
}
