package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z34 {
    @DexIgnore
    public final DateTime a(String str) {
        try {
            return bk4.c(str);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateTimeISOStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final String a(DateTime dateTime) {
        return bk4.a(dateTime);
    }
}
