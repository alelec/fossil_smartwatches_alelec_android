package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jf0 implements Parcelable.Creator<ah0> {
    @DexIgnore
    public /* synthetic */ jf0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        return new ah0(parcel.readLong());
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ah0[i];
    }
}
