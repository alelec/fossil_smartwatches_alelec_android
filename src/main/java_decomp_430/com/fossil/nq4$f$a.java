package com.fossil;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.usecase.DianaSyncDataProcessing;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2$1", f = "DianaSyncDataProcessing.kt", l = {}, m = "invokeSuspend")
public final class nq4$f$a extends sf6 implements ig6<il6, xe6<? super rm6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitHeartRate;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listMFSleepSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listUASample;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncDataProcessing.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nq4$f$a(DianaSyncDataProcessing.f fVar, List list, List list2, jh6 jh6, List list3, List list4, List list5, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$listGFitSample = list;
        this.$listUASample = list2;
        this.$gFitActiveTime = jh6;
        this.$listGFitHeartRate = list3;
        this.$listGFitWorkoutSession = list4;
        this.$listMFSleepSession = list5;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        nq4$f$a nq4_f_a = new nq4$f$a(this.this$0, this.$listGFitSample, this.$listUASample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, xe6);
        nq4_f_a.p$ = (il6) obj;
        return nq4_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((nq4$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$listUASample, (GFitActiveTime) this.$gFitActiveTime.element, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession);
            return ThirdPartyRepository.uploadData$default(this.this$0.$thirdPartyRepository, (ThirdPartyRepository.PushPendingThirdPartyDataCallback) null, 1, (Object) null);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
