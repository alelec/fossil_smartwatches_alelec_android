package com.fossil;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.service.usecase.HybridSyncDataProcessing;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$2$1", f = "HybridSyncDataProcessing.kt", l = {}, m = "invokeSuspend")
public final class oq4$d$a extends sf6 implements ig6<il6, xe6<? super rm6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ List $listSleepSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listUASample;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridSyncDataProcessing.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oq4$d$a(HybridSyncDataProcessing.d dVar, List list, List list2, List list3, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$listGFitSample = list;
        this.$listUASample = list2;
        this.$listSleepSession = list3;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        oq4$d$a oq4_d_a = new oq4$d$a(this.this$0, this.$listGFitSample, this.$listUASample, this.$listSleepSession, xe6);
        oq4_d_a.p$ = (il6) obj;
        return oq4_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((oq4$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$listUASample, (GFitActiveTime) null, qd6.a(), qd6.a(), this.$listSleepSession);
            return ThirdPartyRepository.uploadData$default(this.this$0.$thirdPartyRepository, (ThirdPartyRepository.PushPendingThirdPartyDataCallback) null, 1, (Object) null);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
