package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface eq6 {
    @DexIgnore
    void onFailure(dq6 dq6, IOException iOException);

    @DexIgnore
    void onResponse(dq6 dq6, Response response) throws IOException;
}
