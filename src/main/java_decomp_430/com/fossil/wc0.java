package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ w40 c;
    @DexIgnore
    public /* final */ sd0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(z90.class.getClassLoader());
            if (readParcelable != null) {
                z90 z90 = (z90) readParcelable;
                uc0 uc0 = (uc0) parcel.readParcelable(uc0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(w40.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new wc0(z90, uc0, (w40) readParcelable2, sd0.values()[parcel.readInt()]);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new wc0[i];
        }
    }

    @DexIgnore
    public wc0(z90 z90, w40 w40, sd0 sd0) {
        super(z90, (uc0) null);
        this.d = sd0;
        this.c = w40;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        try {
            kk1 kk1 = kk1.d;
            x90 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return kk1.a(s, w40, new o11(((z90) deviceRequest).d(), new w40(this.c.getMajor(), this.c.getMinor()), this.d.a()).getData());
            }
            throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (sw0 e) {
            qs0.h.a(e);
            return new byte[0];
        }
    }

    @DexIgnore
    public JSONObject b() {
        return cw0.a(cw0.a(super.b(), bm0.MICRO_APP_VERSION, (Object) this.c.toString()), bm0.MICRO_APP_ERROR_TYPE, (Object) cw0.a((Enum<?>) this.d));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(wc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            wc0 wc0 = (wc0) obj;
            return !(wg6.a(this.c, wc0.c) ^ true) && this.d == wc0.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.MicroAppErrorData");
    }

    @DexIgnore
    public final sd0 getErrorType() {
        return this.d;
    }

    @DexIgnore
    public final w40 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.d.ordinal());
        }
    }

    @DexIgnore
    public wc0(z90 z90, uc0 uc0, w40 w40, sd0 sd0) {
        super(z90, uc0);
        this.d = sd0;
        this.c = w40;
    }
}
