package com.fossil;

import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.service.usecase.HybridSyncDataProcessing;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1", f = "HybridSyncDataProcessing.kt", l = {204}, m = "invokeSuspend")
public final class oq4$e$a extends sf6 implements ig6<il6, xe6<? super ActivityStatistic>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ List $fitnessDataList;
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridSyncDataProcessing.e this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.usecase.HybridSyncDataProcessing$saveSyncResult$3$1$1", f = "HybridSyncDataProcessing.kt", l = {205, 206, 208, 209, 210}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super ActivityStatistic>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oq4$e$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(oq4$e$a oq4_e_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = oq4_e_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00c7 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x012e A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x013f A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            SleepSummariesRepository sleepSummariesRepository;
            Date date;
            Date date2;
            il6 il62;
            SummariesRepository summariesRepository;
            Date date3;
            Date date4;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il63 = this.p$;
                oq4$e$a oq4_e_a = this.this$0;
                ActivitiesRepository activitiesRepository = oq4_e_a.this$0.$activityRepository;
                Date date5 = ((DateTime) oq4_e_a.$startDate.element).toDate();
                wg6.a((Object) date5, "startDate.toDate()");
                Date date6 = ((DateTime) this.this$0.$endDate.element).toDate();
                wg6.a((Object) date6, "endDate.toDate()");
                this.L$0 = il63;
                this.label = 1;
                il6 il64 = il63;
                if (ActivitiesRepository.fetchActivitySamples$default(activitiesRepository, date5, date6, 0, 0, this, 12, (Object) null) == a) {
                    return a;
                }
                il62 = il64;
                oq4$e$a oq4_e_a2 = this.this$0;
                summariesRepository = oq4_e_a2.this$0.$summaryRepository;
                date3 = ((DateTime) oq4_e_a2.$startDate.element).toDate();
                wg6.a((Object) date3, "startDate.toDate()");
                date4 = ((DateTime) this.this$0.$endDate.element).toDate();
                wg6.a((Object) date4, "endDate.toDate()");
                this.L$0 = il62;
                this.label = 2;
                if (summariesRepository.loadSummaries(date3, date4, this) == a) {
                }
            } else if (i == 1) {
                il62 = (il6) this.L$0;
                nc6.a(obj);
                oq4$e$a oq4_e_a22 = this.this$0;
                summariesRepository = oq4_e_a22.this$0.$summaryRepository;
                date3 = ((DateTime) oq4_e_a22.$startDate.element).toDate();
                wg6.a((Object) date3, "startDate.toDate()");
                date4 = ((DateTime) this.this$0.$endDate.element).toDate();
                wg6.a((Object) date4, "endDate.toDate()");
                this.L$0 = il62;
                this.label = 2;
                if (summariesRepository.loadSummaries(date3, date4, this) == a) {
                    return a;
                }
            } else if (i == 2) {
                il62 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 3) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                oq4$e$a oq4_e_a3 = this.this$0;
                sleepSummariesRepository = oq4_e_a3.this$0.$sleepSummariesRepository;
                date = ((DateTime) oq4_e_a3.$startDate.element).toDate();
                wg6.a((Object) date, "startDate.toDate()");
                date2 = ((DateTime) this.this$0.$endDate.element).toDate();
                wg6.a((Object) date2, "endDate.toDate()");
                this.L$0 = il6;
                this.label = 4;
                if (sleepSummariesRepository.fetchSleepSummaries(date, date2, this) == a) {
                    return a;
                }
                SummariesRepository summariesRepository2 = this.this$0.this$0.$summaryRepository;
                this.L$0 = il6;
                this.label = 5;
                Object fetchActivityStatistic = summariesRepository2.fetchActivityStatistic(this);
                if (fetchActivityStatistic == a) {
                }
            } else if (i == 4) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                SummariesRepository summariesRepository22 = this.this$0.this$0.$summaryRepository;
                this.L$0 = il6;
                this.label = 5;
                Object fetchActivityStatistic2 = summariesRepository22.fetchActivityStatistic(this);
                return fetchActivityStatistic2 == a ? a : fetchActivityStatistic2;
            } else if (i == 5) {
                il6 il65 = (il6) this.L$0;
                nc6.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            il6 il66 = il62;
            oq4$e$a oq4_e_a4 = this.this$0;
            SleepSessionsRepository sleepSessionsRepository = oq4_e_a4.this$0.$sleepSessionsRepository;
            Date date7 = ((DateTime) oq4_e_a4.$startDate.element).toDate();
            wg6.a((Object) date7, "startDate.toDate()");
            Date date8 = ((DateTime) this.this$0.$endDate.element).toDate();
            wg6.a((Object) date8, "endDate.toDate()");
            this.L$0 = il66;
            this.label = 3;
            if (SleepSessionsRepository.fetchSleepSessions$default(sleepSessionsRepository, date7, date8, 0, 0, this, 12, (Object) null) == a) {
                return a;
            }
            il6 = il66;
            oq4$e$a oq4_e_a32 = this.this$0;
            sleepSummariesRepository = oq4_e_a32.this$0.$sleepSummariesRepository;
            date = ((DateTime) oq4_e_a32.$startDate.element).toDate();
            wg6.a((Object) date, "startDate.toDate()");
            date2 = ((DateTime) this.this$0.$endDate.element).toDate();
            wg6.a((Object) date2, "endDate.toDate()");
            this.L$0 = il6;
            this.label = 4;
            if (sleepSummariesRepository.fetchSleepSummaries(date, date2, this) == a) {
            }
            SummariesRepository summariesRepository222 = this.this$0.this$0.$summaryRepository;
            this.L$0 = il6;
            this.label = 5;
            Object fetchActivityStatistic22 = summariesRepository222.fetchActivityStatistic(this);
            if (fetchActivityStatistic22 == a) {
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oq4$e$a(HybridSyncDataProcessing.e eVar, List list, jh6 jh6, jh6 jh62, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
        this.$fitnessDataList = list;
        this.$startDate = jh6;
        this.$endDate = jh62;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        oq4$e$a oq4_e_a = new oq4$e$a(this.this$0, this.$fitnessDataList, this.$startDate, this.$endDate, xe6);
        oq4_e_a.p$ = (il6) obj;
        return oq4_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((oq4$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                if (fitnessDataWrapper.getStartLongTime() < ((DateTime) this.$startDate.element).getMillis()) {
                    this.$startDate.element = fitnessDataWrapper.getStartTimeTZ();
                }
                if (fitnessDataWrapper.getEndLongTime() > ((DateTime) this.$endDate.element).getMillis()) {
                    this.$endDate.element = fitnessDataWrapper.getEndTimeTZ();
                }
            }
            dl6 b = zl6.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(b, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
