package com.fossil;

import android.content.ContentResolver;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx5 implements Factory<ex5> {
    @DexIgnore
    public static ex5 a(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, u04 u04, ContentResolver contentResolver, ts4 ts4, an4 an4) {
        return new ex5(portfolioApp, notificationsRepository, deviceRepository, notificationSettingsDatabase, u04, contentResolver, ts4, an4);
    }
}
