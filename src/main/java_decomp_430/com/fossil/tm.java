package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tm implements hm {
    @DexIgnore
    public static /* final */ String b; // = tl.a("SystemAlarmScheduler");
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public tm(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public void a(zn... znVarArr) {
        for (zn a2 : znVarArr) {
            a(a2);
        }
    }

    @DexIgnore
    public void a(String str) {
        this.a.startService(pm.c(this.a, str));
    }

    @DexIgnore
    public final void a(zn znVar) {
        tl.a().a(b, String.format("Scheduling work with workSpecId %s", new Object[]{znVar.a}), new Throwable[0]);
        this.a.startService(pm.b(this.a, znVar.a));
    }
}
