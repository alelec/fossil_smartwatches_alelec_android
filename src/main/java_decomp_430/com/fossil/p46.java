package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ int b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ boolean d;
    @DexIgnore
    public /* final */ /* synthetic */ o46 e;

    @DexIgnore
    public p46(o46 o46, List list, int i, boolean z, boolean z2) {
        this.e = o46;
        this.a = list;
        this.b = i;
        this.c = z;
        this.d = z2;
    }

    @DexIgnore
    public void run() {
        this.e.a((List<y46>) this.a, this.b, this.c);
        if (this.d) {
            this.a.clear();
        }
    }
}
