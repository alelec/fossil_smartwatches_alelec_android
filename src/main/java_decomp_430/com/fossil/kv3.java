package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kv3 extends JsonReader {
    @DexIgnore
    public static /* final */ Reader y; // = new a();
    @DexIgnore
    public static /* final */ Object z; // = new Object();
    @DexIgnore
    public Object[] u; // = new Object[32];
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public String[] w; // = new String[32];
    @DexIgnore
    public int[] x; // = new int[32];

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Reader {
        @DexIgnore
        public void close() throws IOException {
            throw new AssertionError();
        }

        @DexIgnore
        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public kv3(JsonElement jsonElement) {
        super(y);
        a((Object) jsonElement);
    }

    @DexIgnore
    private String E() {
        return " at path " + z();
    }

    @DexIgnore
    public void B() throws IOException {
        a(rv3.END_OBJECT);
        V();
        V();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public boolean C() throws IOException {
        rv3 N = N();
        return (N == rv3.END_OBJECT || N == rv3.END_ARRAY) ? false : true;
    }

    @DexIgnore
    public boolean F() throws IOException {
        a(rv3.BOOLEAN);
        boolean a2 = ((nu3) V()).a();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return a2;
    }

    @DexIgnore
    public double G() throws IOException {
        rv3 N = N();
        if (N == rv3.NUMBER || N == rv3.STRING) {
            double p = ((nu3) U()).p();
            if (D() || (!Double.isNaN(p) && !Double.isInfinite(p))) {
                V();
                int i = this.v;
                if (i > 0) {
                    int[] iArr = this.x;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return p;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + p);
        }
        throw new IllegalStateException("Expected " + rv3.NUMBER + " but was " + N + E());
    }

    @DexIgnore
    public int H() throws IOException {
        rv3 N = N();
        if (N == rv3.NUMBER || N == rv3.STRING) {
            int b = ((nu3) U()).b();
            V();
            int i = this.v;
            if (i > 0) {
                int[] iArr = this.x;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return b;
        }
        throw new IllegalStateException("Expected " + rv3.NUMBER + " but was " + N + E());
    }

    @DexIgnore
    public long I() throws IOException {
        rv3 N = N();
        if (N == rv3.NUMBER || N == rv3.STRING) {
            long q = ((nu3) U()).q();
            V();
            int i = this.v;
            if (i > 0) {
                int[] iArr = this.x;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return q;
        }
        throw new IllegalStateException("Expected " + rv3.NUMBER + " but was " + N + E());
    }

    @DexIgnore
    public String J() throws IOException {
        a(rv3.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) U()).next();
        String str = (String) entry.getKey();
        this.w[this.v - 1] = str;
        a(entry.getValue());
        return str;
    }

    @DexIgnore
    public void K() throws IOException {
        a(rv3.NULL);
        V();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public String L() throws IOException {
        rv3 N = N();
        if (N == rv3.STRING || N == rv3.NUMBER) {
            String f = ((nu3) V()).f();
            int i = this.v;
            if (i > 0) {
                int[] iArr = this.x;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return f;
        }
        throw new IllegalStateException("Expected " + rv3.STRING + " but was " + N + E());
    }

    @DexIgnore
    public rv3 N() throws IOException {
        if (this.v == 0) {
            return rv3.END_DOCUMENT;
        }
        Object U = U();
        if (U instanceof Iterator) {
            boolean z2 = this.u[this.v - 2] instanceof ku3;
            Iterator it = (Iterator) U;
            if (!it.hasNext()) {
                return z2 ? rv3.END_OBJECT : rv3.END_ARRAY;
            }
            if (z2) {
                return rv3.NAME;
            }
            a(it.next());
            return N();
        } else if (U instanceof ku3) {
            return rv3.BEGIN_OBJECT;
        } else {
            if (U instanceof fu3) {
                return rv3.BEGIN_ARRAY;
            }
            if (U instanceof nu3) {
                nu3 nu3 = (nu3) U;
                if (nu3.u()) {
                    return rv3.STRING;
                }
                if (nu3.s()) {
                    return rv3.BOOLEAN;
                }
                if (nu3.t()) {
                    return rv3.NUMBER;
                }
                throw new AssertionError();
            } else if (U instanceof ju3) {
                return rv3.NULL;
            } else {
                if (U == z) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    @DexIgnore
    public void T() throws IOException {
        if (N() == rv3.NAME) {
            J();
            this.w[this.v - 2] = "null";
        } else {
            V();
            int i = this.v;
            if (i > 0) {
                this.w[i - 1] = "null";
            }
        }
        int i2 = this.v;
        if (i2 > 0) {
            int[] iArr = this.x;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    @DexIgnore
    public final Object U() {
        return this.u[this.v - 1];
    }

    @DexIgnore
    public final Object V() {
        Object[] objArr = this.u;
        int i = this.v - 1;
        this.v = i;
        Object obj = objArr[i];
        objArr[this.v] = null;
        return obj;
    }

    @DexIgnore
    public void W() throws IOException {
        a(rv3.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) U()).next();
        a(entry.getValue());
        a((Object) new nu3((String) entry.getKey()));
    }

    @DexIgnore
    public final void a(rv3 rv3) throws IOException {
        if (N() != rv3) {
            throw new IllegalStateException("Expected " + rv3 + " but was " + N() + E());
        }
    }

    @DexIgnore
    public void close() throws IOException {
        this.u = new Object[]{z};
        this.v = 1;
    }

    @DexIgnore
    public void k() throws IOException {
        a(rv3.BEGIN_ARRAY);
        a((Object) ((fu3) U()).iterator());
        this.x[this.v - 1] = 0;
    }

    @DexIgnore
    public void l() throws IOException {
        a(rv3.BEGIN_OBJECT);
        a((Object) ((ku3) U()).entrySet().iterator());
    }

    @DexIgnore
    public void p() throws IOException {
        a(rv3.END_ARRAY);
        V();
        V();
        int i = this.v;
        if (i > 0) {
            int[] iArr = this.x;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    @DexIgnore
    public String toString() {
        return kv3.class.getSimpleName();
    }

    @DexIgnore
    public String z() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.v) {
            Object[] objArr = this.u;
            if (objArr[i] instanceof fu3) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.x[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof ku3) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('.');
                    String[] strArr = this.w;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    @DexIgnore
    public final void a(Object obj) {
        int i = this.v;
        Object[] objArr = this.u;
        if (i == objArr.length) {
            Object[] objArr2 = new Object[(i * 2)];
            int[] iArr = new int[(i * 2)];
            String[] strArr = new String[(i * 2)];
            System.arraycopy(objArr, 0, objArr2, 0, i);
            System.arraycopy(this.x, 0, iArr, 0, this.v);
            System.arraycopy(this.w, 0, strArr, 0, this.v);
            this.u = objArr2;
            this.x = iArr;
            this.w = strArr;
        }
        Object[] objArr3 = this.u;
        int i2 = this.v;
        this.v = i2 + 1;
        objArr3[i2] = obj;
    }
}
