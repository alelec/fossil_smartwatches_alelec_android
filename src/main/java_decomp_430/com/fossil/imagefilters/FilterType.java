package com.fossil.imagefilters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum FilterType {
    DIRECT_MAPPING,
    FLOYD_DITHERING,
    SIERRA_DITHERING,
    SIERRA_TWO_ROW_DITHERING,
    SIERRA_LITE_DITHERING,
    BURKES_DITHERING,
    ORDERED_DITHERING,
    ATKINSON_DITHERING,
    STUCKI_DITHERING,
    JAJUNI_DITHERING
}
