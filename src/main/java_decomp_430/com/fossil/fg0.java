package com.fossil;

import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fg0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ii1 a;
    @DexIgnore
    public /* final */ /* synthetic */ q40.c b;
    @DexIgnore
    public /* final */ /* synthetic */ q40.c c;

    @DexIgnore
    public fg0(ii1 ii1, q40.c cVar, q40.c cVar2) {
        this.a = ii1;
        this.b = cVar;
        this.c = cVar2;
    }

    @DexIgnore
    public final void run() {
        ii1 ii1 = this.a;
        q40.b bVar = ii1.t;
        if (bVar != null) {
            bVar.onDeviceStateChanged(ii1, this.b, this.c);
        }
    }
}
