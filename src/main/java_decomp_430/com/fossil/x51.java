package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.SdkDatabase;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x51 extends v01 {
    @DexIgnore
    public /* final */ g01 D;
    @DexIgnore
    public /* final */ ArrayList<byte[]> E;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<ie1> F;
    @DexIgnore
    public /* final */ ArrayList<ie1> G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public float J;
    @DexIgnore
    public int K;
    @DexIgnore
    public long L;
    @DexIgnore
    public int M;
    @DexIgnore
    public /* final */ boolean N;
    @DexIgnore
    public /* final */ boolean O;
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public /* final */ int Q;
    @DexIgnore
    public /* final */ boolean R;
    @DexIgnore
    public /* final */ float S;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ x51(ue1 ue1, q41 q41, eh1 eh1, short s, HashMap hashMap, float f, String str, int i) {
        super(ue1, q41, r3, s, (i & 64) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
        eh1 eh12 = (i & 4) != 0 ? eh1.GET_FILE : eh1;
        hashMap = (i & 16) != 0 ? new HashMap() : hashMap;
        f = (i & 32) != 0 ? 0.001f : f;
        this.S = f;
        this.D = new g01(s);
        this.E = new ArrayList<>();
        this.F = new CopyOnWriteArrayList<>();
        this.G = new ArrayList<>();
        this.K = -1;
        Boolean bool = (Boolean) hashMap.get(io0.SKIP_LIST);
        boolean z = false;
        this.N = bool != null ? bool.booleanValue() : false;
        Boolean bool2 = (Boolean) hashMap.get(io0.SKIP_ERASE);
        this.O = bool2 != null ? bool2.booleanValue() : false;
        Boolean bool3 = (Boolean) hashMap.get(io0.SKIP_ERASE_CACHE_AFTER_SUCCESS);
        this.P = bool3 != null ? bool3.booleanValue() : false;
        Integer num = (Integer) hashMap.get(io0.NUMBER_OF_FILE_REQUIRED);
        this.Q = num != null ? num.intValue() : 0;
        Boolean bool4 = (Boolean) hashMap.get(io0.ERASE_CACHE_FILE_BEFORE_GET);
        this.R = bool4 != null ? bool4.booleanValue() : z;
    }

    @DexIgnore
    public void h() {
        if (this.R) {
            m();
        }
        if (!this.P) {
            c(new b41(this));
        }
        if (this.N) {
            this.F.add(new ie1(this.w.t, this.C, 4294967295L, 0));
            p();
            return;
        }
        if1.a(this, lx0.LIST_FILE, (lx0) null, 2, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        JSONObject put = super.i().put(cw0.a((Enum<?>) io0.SKIP_LIST), this.N).put(cw0.a((Enum<?>) io0.SKIP_ERASE), this.O).put(cw0.a((Enum<?>) io0.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.P).put(cw0.a((Enum<?>) io0.NUMBER_OF_FILE_REQUIRED), this.Q).put(cw0.a((Enum<?>) io0.ERASE_CACHE_FILE_BEFORE_GET), this.R);
        wg6.a(put, "super.optionDescription(\u2026 eraseCacheFileBeforeGet)");
        return put;
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject put = super.k().put(cw0.a((Enum<?>) io0.SKIP_ERASE), this.O);
        wg6.a(put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        bm0 bm0 = bm0.FILES;
        Object[] array = this.G.toArray(new ie1[0]);
        if (array != null) {
            return cw0.a(put, bm0, (Object) cw0.a((p40[]) array));
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void m() {
        Integer num;
        String str;
        boolean z;
        w81 a;
        qz0 qz0 = qz0.a;
        String str2 = this.w.t;
        byte b = this.D.a;
        SdkDatabase a2 = SdkDatabase.e.a();
        if (a2 == null || (a = a2.a()) == null) {
            num = null;
        } else {
            a.a.assertNotSuspendingTransaction();
            mi acquire = a.d.acquire();
            if (str2 == null) {
                acquire.a(1);
            } else {
                acquire.a(1, str2);
            }
            acquire.a(2, (long) b);
            a.a.beginTransaction();
            try {
                int s = acquire.s();
                a.a.setTransactionSuccessful();
                a.a.endTransaction();
                a.d.release(acquire);
                num = Integer.valueOf(s);
            } catch (Throwable th) {
                a.a.endTransaction();
                a.d.release(acquire);
                throw th;
            }
        }
        Integer num2 = num;
        if (num2 == null) {
            str = "Database instance is null";
            z = false;
        } else {
            str = "";
            z = true;
        }
        qz0.a(xx0.DELETE, z, str2, cw0.a(cw0.a(new g01(b, (byte) 255).a(), bm0.IS_COMPLETED, JSONObject.NULL), bm0.NUMBER_OF_DELETED_ROW, (Object) num2 != null ? num2 : -1), str);
        oa1 oa1 = oa1.a;
        Object[] objArr = {str2, Byte.valueOf(b), num2};
        if (num2 != null) {
            num2.intValue();
        }
    }

    @DexIgnore
    public final ie1 n() {
        return (ie1) yd6.a(this.F, this.K);
    }

    @DexIgnore
    public final void o() {
        ie1 n = n();
        if (n == null) {
            wg6.a();
            throw null;
        } else if (n.f > 0) {
            a(lx0.GET_FILE, (gg6<cd6>) new f21(this));
        } else if (this.O) {
            p();
        } else {
            if1.a(this, lx0.ERASE_FILE, (lx0) null, 2, (Object) null);
        }
    }

    @DexIgnore
    public final void p() {
        this.K++;
        if (this.K < this.F.size()) {
            ie1 n = n();
            if (n != null) {
                g01 g01 = new g01(n.c());
                ie1 a = a(g01.a, g01.b);
                ue1 ue1 = this.w;
                cc0 cc0 = cc0.DEBUG;
                Object[] objArr = new Object[4];
                objArr[0] = ue1.t;
                objArr[1] = Byte.valueOf(g01.a);
                objArr[2] = Byte.valueOf(g01.b);
                objArr[3] = a != null ? a.toString() : null;
                ie1 n2 = n();
                if (n2 == null) {
                    wg6.a();
                    throw null;
                } else if (n2.f <= 0) {
                    p();
                } else if (a != null) {
                    ie1 n3 = n();
                    if (n3 != null) {
                        long j = n3.f;
                        ie1 n4 = n();
                        if (n4 != null) {
                            d(ie1.a(a, (String) null, (byte) 0, (byte) 0, (byte[]) null, j, n4.g, 0, false, 207));
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    this.L = 0;
                    o();
                }
            } else {
                wg6.a();
                throw null;
            }
        } else if (this.E.size() < this.Q) {
            a(km1.a(this.v, (eh1) null, sk1.NOT_ENOUGH_FILE_TO_PROCESS, (bn0) null, 5));
        } else {
            this.G.clear();
            this.G.addAll(qz0.a.a(this.w.t, this.D.a));
            a(this.G);
        }
    }

    @DexIgnore
    public final void q() {
        T t;
        for (ie1 ie1 : qz0.a.b(this.w.t, this.D.a)) {
            Iterator<T> it = this.F.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                boolean z = false;
                if (((ie1) t).c() == ByteBuffer.allocate(2).put(ie1.c).put(ie1.d).getShort(0)) {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t == null && h51.a.a(ie1.e, q11.CRC32) != ie1.g) {
                qz0.a.a(this.w.t, ie1.c, ie1.d);
            }
        }
    }

    @DexIgnore
    public final void b(ie1 ie1) {
        if1.a((if1) this, (if1) new t71(this.w, this.x, ie1, this.z), (hg6) new ry0(this), (hg6) new l01(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }

    @DexIgnore
    public void c(ie1 ie1) {
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = ie1.a(2);
    }

    @DexIgnore
    public Object d() {
        Object[] array = this.G.toArray(new ie1[0]);
        if (array != null) {
            return array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void a(t71 t71) {
        t71 t712 = t71;
        long j = t712.C;
        this.L = j;
        ie1 ie1 = t712.G;
        a(ie1.a(ie1, (String) null, (byte) 0, (byte) 0, md6.a(ie1.e, 0, (int) j), 0, 0, 0, false, 119));
        long j2 = this.H;
        float f = j2 > 0 ? (((float) (this.I + this.L)) * 1.0f) / ((float) j2) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (Math.abs(f - this.J) > this.S || f == 1.0f) {
            this.J = f;
            a(f);
        }
        o();
    }

    @DexIgnore
    public final void d(ie1 ie1) {
        ie1 ie12 = ie1;
        if (ie12.g != h51.a.a(ie12.e, q11.CRC32) || ((int) ie12.f) != ie12.e.length) {
            int i = this.M;
            if (i < 3) {
                this.M = i + 1;
                b(ie1);
                return;
            }
            a(km1.a(this.v, (eh1) null, sk1.INVALID_FILE_CRC, (bn0) null, 5));
        } else if (a(ie1.a(ie1, (String) null, (byte) 0, (byte) 0, (byte[]) null, 0, 0, 0, true, 127)) < 0) {
            a(km1.a(this.v, (eh1) null, sk1.DATABASE_ERROR, (bn0) null, 5));
        } else {
            this.I += ie12.f;
            this.E.add(ie12.e);
            c(ie1);
            if (this.O) {
                p();
            } else {
                if1.a(this, lx0.ERASE_FILE, (lx0) null, 2, (Object) null);
            }
        }
    }

    @DexIgnore
    public qv0 a(lx0 lx0) {
        int i = aq0.a[lx0.ordinal()];
        if (i == 1) {
            te0 te0 = new te0(this.C, this.w, 0, 4);
            te0.b((hg6<? super qv0, cd6>) new xw0(this));
            return te0;
        } else if (i == 2) {
            ie1 n = n();
            if (n != null) {
                fl1 fl1 = new fl1(n.c(), this.L, 4294967295L, this.w, 0, 16);
                lt0 lt0 = new lt0(this, n);
                if (!fl1.t) {
                    fl1.n.add(lt0);
                } else {
                    lt0.invoke(fl1);
                }
                cv0 cv0 = new cv0(this, n);
                if (!fl1.t) {
                    fl1.o.add(cv0);
                }
                fl1.s = c();
                return fl1;
            }
            wg6.a();
            throw null;
        } else if (i != 3) {
            return null;
        } else {
            ie1 n2 = n();
            if (n2 != null) {
                zd1 zd1 = new zd1(n2.c(), this.w, 0, 4);
                zd1.b((hg6<? super qv0, cd6>) new sr0(this));
                return zd1;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<ie1> arrayList) {
        a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
    }

    @DexIgnore
    public final long a(ie1 ie1) {
        qz0.a.a(ie1.b, ie1.c, ie1.d);
        if (!(ie1.e.length == 0)) {
            return qz0.a.a(ie1);
        }
        ue1 ue1 = this.w;
        cc0 cc0 = cc0.DEBUG;
        Object[] objArr = {ue1.t, Byte.valueOf(ie1.c), Byte.valueOf(ie1.d)};
        return -1;
    }

    @DexIgnore
    public final ie1 a(byte b, byte b2) {
        return (ie1) yd6.a(qz0.a.b(this.w.t, b, b2), 0);
    }
}
