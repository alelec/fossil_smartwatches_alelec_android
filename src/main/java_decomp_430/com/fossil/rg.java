package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rg {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public a b; // = new a();

    @DexIgnore
    public interface b {
        @DexIgnore
        int a();

        @DexIgnore
        int a(View view);

        @DexIgnore
        View a(int i);

        @DexIgnore
        int b();

        @DexIgnore
        int b(View view);
    }

    @DexIgnore
    public rg(b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public View a(int i, int i2, int i3, int i4) {
        int a2 = this.a.a();
        int b2 = this.a.b();
        int i5 = i2 > i ? 1 : -1;
        View view = null;
        while (i != i2) {
            View a3 = this.a.a(i);
            this.b.a(a2, b2, this.a.a(a3), this.a.b(a3));
            if (i3 != 0) {
                this.b.b();
                this.b.a(i3);
                if (this.b.a()) {
                    return a3;
                }
            }
            if (i4 != 0) {
                this.b.b();
                this.b.a(i4);
                if (this.b.a()) {
                    view = a3;
                }
            }
            i += i5;
        }
        return view;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public int a; // = 0;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public int a(int i, int i2) {
            if (i > i2) {
                return 1;
            }
            return i == i2 ? 2 : 4;
        }

        @DexIgnore
        public void a(int i, int i2, int i3, int i4) {
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }

        @DexIgnore
        public void b() {
            this.a = 0;
        }

        @DexIgnore
        public void a(int i) {
            this.a = i | this.a;
        }

        @DexIgnore
        public boolean a() {
            int i = this.a;
            if ((i & 7) != 0 && (i & (a(this.d, this.b) << 0)) == 0) {
                return false;
            }
            int i2 = this.a;
            if ((i2 & 112) != 0 && (i2 & (a(this.d, this.c) << 4)) == 0) {
                return false;
            }
            int i3 = this.a;
            if ((i3 & 1792) != 0 && (i3 & (a(this.e, this.b) << 8)) == 0) {
                return false;
            }
            int i4 = this.a;
            if ((i4 & 28672) == 0 || (i4 & (a(this.e, this.c) << 12)) != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public boolean a(View view, int i) {
        this.b.a(this.a.a(), this.a.b(), this.a.a(view), this.a.b(view));
        if (i == 0) {
            return false;
        }
        this.b.b();
        this.b.a(i);
        return this.b.a();
    }
}
