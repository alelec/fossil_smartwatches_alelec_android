package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cx3 {
    @DexIgnore
    public sw3 a;
    @DexIgnore
    public ww3 b;
    @DexIgnore
    public volatile dx3 c;

    /*
    static {
        ww3.a();
    }
    */

    /* JADX WARNING: Can't wrap try/catch for region: R(2:14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r3.c = r4;
        r4 = com.fossil.sw3.EMPTY;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0026 */
    public void a(dx3 dx3) {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    if (this.a != null) {
                        this.c = (dx3) dx3.e().a(this.a, this.b);
                    } else {
                        this.c = dx3;
                        sw3 sw3 = sw3.EMPTY;
                    }
                }
            }
        }
    }

    @DexIgnore
    public dx3 b(dx3 dx3) {
        a(dx3);
        return this.c;
    }

    @DexIgnore
    public dx3 c(dx3 dx3) {
        dx3 dx32 = this.c;
        this.a = null;
        this.c = dx3;
        return dx32;
    }
}
