package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pq6 {
    @DexIgnore
    public static /* final */ pq6 a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends pq6 {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public pq6 a(dq6 dq6) {
            return pq6.this;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        pq6 a(dq6 dq6);
    }

    @DexIgnore
    public static c a(pq6 pq6) {
        return new b();
    }

    @DexIgnore
    public void a(dq6 dq6) {
    }

    @DexIgnore
    public void a(dq6 dq6, long j) {
    }

    @DexIgnore
    public void a(dq6 dq6, hq6 hq6) {
    }

    @DexIgnore
    public void a(dq6 dq6, rq6 rq6) {
    }

    @DexIgnore
    public void a(dq6 dq6, yq6 yq6) {
    }

    @DexIgnore
    public void a(dq6 dq6, IOException iOException) {
    }

    @DexIgnore
    public void a(dq6 dq6, String str) {
    }

    @DexIgnore
    public void a(dq6 dq6, String str, List<InetAddress> list) {
    }

    @DexIgnore
    public void a(dq6 dq6, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    @DexIgnore
    public void a(dq6 dq6, InetSocketAddress inetSocketAddress, Proxy proxy, wq6 wq6) {
    }

    @DexIgnore
    public void a(dq6 dq6, InetSocketAddress inetSocketAddress, Proxy proxy, wq6 wq6, IOException iOException) {
    }

    @DexIgnore
    public void a(dq6 dq6, Response response) {
    }

    @DexIgnore
    public void b(dq6 dq6) {
    }

    @DexIgnore
    public void b(dq6 dq6, long j) {
    }

    @DexIgnore
    public void b(dq6 dq6, hq6 hq6) {
    }

    @DexIgnore
    public void c(dq6 dq6) {
    }

    @DexIgnore
    public void d(dq6 dq6) {
    }

    @DexIgnore
    public void e(dq6 dq6) {
    }

    @DexIgnore
    public void f(dq6 dq6) {
    }

    @DexIgnore
    public void g(dq6 dq6) {
    }
}
