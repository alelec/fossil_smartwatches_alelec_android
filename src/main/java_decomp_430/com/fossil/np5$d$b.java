package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$alarms$1", f = "LoginPresenter.kt", l = {}, m = "invokeSuspend")
public final class np5$d$b extends sf6 implements ig6<il6, xe6<? super List<Alarm>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LoginPresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public np5$d$b(LoginPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        np5$d$b np5_d_b = new np5$d$b(this.this$0, xe6);
        np5_d_b.p$ = (il6) obj;
        return np5_d_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((np5$d$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.p().getActiveAlarms();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
