package com.fossil;

import com.facebook.UserSettingsManager;
import com.facebook.internal.Utility;
import com.facebook.login.LoginStatusClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr2 implements jr2 {
    @DexIgnore
    public static /* final */ pk2<Long> A;
    @DexIgnore
    public static /* final */ pk2<Long> B;
    @DexIgnore
    public static /* final */ pk2<Long> C;
    @DexIgnore
    public static /* final */ pk2<Long> D;
    @DexIgnore
    public static /* final */ pk2<Long> E;
    @DexIgnore
    public static /* final */ pk2<Long> F;
    @DexIgnore
    public static /* final */ pk2<Long> G;
    @DexIgnore
    public static /* final */ pk2<Long> H;
    @DexIgnore
    public static /* final */ pk2<String> I;
    @DexIgnore
    public static /* final */ pk2<Long> J;
    @DexIgnore
    public static /* final */ pk2<Long> a;
    @DexIgnore
    public static /* final */ pk2<Long> b;
    @DexIgnore
    public static /* final */ pk2<String> c;
    @DexIgnore
    public static /* final */ pk2<String> d;
    @DexIgnore
    public static /* final */ pk2<String> e;
    @DexIgnore
    public static /* final */ pk2<Long> f;
    @DexIgnore
    public static /* final */ pk2<Long> g;
    @DexIgnore
    public static /* final */ pk2<Long> h;
    @DexIgnore
    public static /* final */ pk2<Long> i;
    @DexIgnore
    public static /* final */ pk2<Long> j;
    @DexIgnore
    public static /* final */ pk2<Long> k;
    @DexIgnore
    public static /* final */ pk2<Long> l;
    @DexIgnore
    public static /* final */ pk2<Long> m;
    @DexIgnore
    public static /* final */ pk2<Long> n;
    @DexIgnore
    public static /* final */ pk2<Long> o;
    @DexIgnore
    public static /* final */ pk2<Long> p;
    @DexIgnore
    public static /* final */ pk2<Long> q;
    @DexIgnore
    public static /* final */ pk2<String> r;
    @DexIgnore
    public static /* final */ pk2<Long> s;
    @DexIgnore
    public static /* final */ pk2<Long> t;
    @DexIgnore
    public static /* final */ pk2<Long> u;
    @DexIgnore
    public static /* final */ pk2<Long> v;
    @DexIgnore
    public static /* final */ pk2<Long> w;
    @DexIgnore
    public static /* final */ pk2<Long> x;
    @DexIgnore
    public static /* final */ pk2<Long> y;
    @DexIgnore
    public static /* final */ pk2<Long> z;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.ad_id_cache_time", 10000);
        b = vk2.a("measurement.config.cache_time", 86400000);
        c = vk2.a("measurement.log_tag", "FA");
        d = vk2.a("measurement.config.url_authority", "app-measurement.com");
        e = vk2.a("measurement.config.url_scheme", Utility.URL_SCHEME);
        f = vk2.a("measurement.upload.debug_upload_interval", 1000);
        g = vk2.a("measurement.lifetimevalue.max_currency_tracked", 4);
        h = vk2.a("measurement.store.max_stored_events_per_app", 100000);
        i = vk2.a("measurement.experiment.max_ids", 50);
        j = vk2.a("measurement.audience.filter_result_max_count", 200);
        k = vk2.a("measurement.alarm_manager.minimum_interval", 60000);
        l = vk2.a("measurement.upload.minimum_delay", 500);
        m = vk2.a("measurement.monitoring.sample_period_millis", 86400000);
        n = vk2.a("measurement.upload.realtime_upload_interval", 10000);
        o = vk2.a("measurement.upload.refresh_blacklisted_config_interval", (long) UserSettingsManager.TIMEOUT_7D);
        p = vk2.a("measurement.config.cache_time.service", 3600000);
        q = vk2.a("measurement.service_client.idle_disconnect_millis", (long) LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
        r = vk2.a("measurement.log_tag.service", "FA-SVC");
        s = vk2.a("measurement.upload.stale_data_deletion_interval", 86400000);
        t = vk2.a("measurement.upload.backoff_period", 43200000);
        u = vk2.a("measurement.upload.initial_upload_delay_time", 15000);
        v = vk2.a("measurement.upload.interval", 3600000);
        w = vk2.a("measurement.upload.max_bundle_size", 65536);
        x = vk2.a("measurement.upload.max_bundles", 100);
        y = vk2.a("measurement.upload.max_conversions_per_day", 500);
        z = vk2.a("measurement.upload.max_error_events_per_day", 1000);
        A = vk2.a("measurement.upload.max_events_per_bundle", 1000);
        B = vk2.a("measurement.upload.max_events_per_day", 100000);
        C = vk2.a("measurement.upload.max_public_events_per_day", 50000);
        D = vk2.a("measurement.upload.max_queue_time", 2419200000L);
        E = vk2.a("measurement.upload.max_realtime_events_per_day", 10);
        F = vk2.a("measurement.upload.max_batch_size", 65536);
        G = vk2.a("measurement.upload.retry_count", 6);
        H = vk2.a("measurement.upload.retry_time", 1800000);
        I = vk2.a("measurement.upload.url", "https://app-measurement.com/a");
        J = vk2.a("measurement.upload.window_interval", 3600000);
    }
    */

    @DexIgnore
    public final long a() {
        return q.b().longValue();
    }

    @DexIgnore
    public final String b() {
        return r.b();
    }

    @DexIgnore
    public final long c() {
        return n.b().longValue();
    }

    @DexIgnore
    public final long d() {
        return u.b().longValue();
    }

    @DexIgnore
    public final long e() {
        return i.b().longValue();
    }

    @DexIgnore
    public final long f() {
        return p.b().longValue();
    }

    @DexIgnore
    public final long g() {
        return y.b().longValue();
    }

    @DexIgnore
    public final long h() {
        return G.b().longValue();
    }

    @DexIgnore
    public final long i() {
        return z.b().longValue();
    }

    @DexIgnore
    public final long j() {
        return j.b().longValue();
    }

    @DexIgnore
    public final long k() {
        return k.b().longValue();
    }

    @DexIgnore
    public final long l() {
        return H.b().longValue();
    }

    @DexIgnore
    public final long m() {
        return w.b().longValue();
    }

    @DexIgnore
    public final long n() {
        return E.b().longValue();
    }

    @DexIgnore
    public final long o() {
        return o.b().longValue();
    }

    @DexIgnore
    public final long p() {
        return x.b().longValue();
    }

    @DexIgnore
    public final long q() {
        return F.b().longValue();
    }

    @DexIgnore
    public final long r() {
        return C.b().longValue();
    }

    @DexIgnore
    public final long s() {
        return v.b().longValue();
    }

    @DexIgnore
    public final long t() {
        return D.b().longValue();
    }

    @DexIgnore
    public final long u() {
        return s.b().longValue();
    }

    @DexIgnore
    public final long v() {
        return J.b().longValue();
    }

    @DexIgnore
    public final long w() {
        return A.b().longValue();
    }

    @DexIgnore
    public final String x() {
        return I.b();
    }

    @DexIgnore
    public final long y() {
        return B.b().longValue();
    }

    @DexIgnore
    public final long z() {
        return t.b().longValue();
    }

    @DexIgnore
    public final long zza() {
        return a.b().longValue();
    }

    @DexIgnore
    public final long zzb() {
        return b.b().longValue();
    }

    @DexIgnore
    public final String zzc() {
        return c.b();
    }

    @DexIgnore
    public final String zzd() {
        return d.b();
    }

    @DexIgnore
    public final String zze() {
        return e.b();
    }

    @DexIgnore
    public final long zzf() {
        return f.b().longValue();
    }

    @DexIgnore
    public final long zzg() {
        return g.b().longValue();
    }

    @DexIgnore
    public final long zzh() {
        return h.b().longValue();
    }

    @DexIgnore
    public final long zzl() {
        return l.b().longValue();
    }

    @DexIgnore
    public final long zzm() {
        return m.b().longValue();
    }
}
