package com.fossil;

import com.fossil.i5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public ArrayList<a> e; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public i5 a;
        @DexIgnore
        public i5 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public i5.c d;
        @DexIgnore
        public int e;

        @DexIgnore
        public a(i5 i5Var) {
            this.a = i5Var;
            this.b = i5Var.g();
            this.c = i5Var.b();
            this.d = i5Var.f();
            this.e = i5Var.a();
        }

        @DexIgnore
        public void a(j5 j5Var) {
            j5Var.a(this.a.h()).a(this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        public void b(j5 j5Var) {
            this.a = j5Var.a(this.a.h());
            i5 i5Var = this.a;
            if (i5Var != null) {
                this.b = i5Var.g();
                this.c = this.a.b();
                this.d = this.a.f();
                this.e = this.a.a();
                return;
            }
            this.b = null;
            this.c = 0;
            this.d = i5.c.STRONG;
            this.e = 0;
        }
    }

    @DexIgnore
    public t5(j5 j5Var) {
        this.a = j5Var.w();
        this.b = j5Var.x();
        this.c = j5Var.t();
        this.d = j5Var.j();
        ArrayList<i5> c2 = j5Var.c();
        int size = c2.size();
        for (int i = 0; i < size; i++) {
            this.e.add(new a(c2.get(i)));
        }
    }

    @DexIgnore
    public void a(j5 j5Var) {
        j5Var.s(this.a);
        j5Var.t(this.b);
        j5Var.p(this.c);
        j5Var.h(this.d);
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(j5Var);
        }
    }

    @DexIgnore
    public void b(j5 j5Var) {
        this.a = j5Var.w();
        this.b = j5Var.x();
        this.c = j5Var.t();
        this.d = j5Var.j();
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).b(j5Var);
        }
    }
}
