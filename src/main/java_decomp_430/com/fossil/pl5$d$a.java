package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
public final class pl5$d$a extends sf6 implements ig6<il6, xe6<? super SKUModel>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeleteAccountPresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pl5$d$a(DeleteAccountPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pl5$d$a pl5_d_a = new pl5$d$a(this.this$0, xe6);
        pl5_d_a.p$ = (il6) obj;
        return pl5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pl5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.get.instance().e());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
