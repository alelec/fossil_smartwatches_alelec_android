package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j24 {
    @DexIgnore
    public /* final */ dl6 a; // = zl6.b();
    @DexIgnore
    public /* final */ dl6 b; // = zl6.a();
    @DexIgnore
    public /* final */ cn6 c; // = zl6.c();
    @DexIgnore
    public /* final */ il6 d; // = jl6.a();

    @DexIgnore
    public final dl6 b() {
        return this.b;
    }

    @DexIgnore
    public final dl6 c() {
        return this.a;
    }

    @DexIgnore
    public final cn6 d() {
        return this.c;
    }

    @DexIgnore
    public final il6 e() {
        return this.d;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public void g() {
    }
}
