package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i56 {
    @DexIgnore
    public static /* final */ /* synthetic */ boolean a; // = (!i56.class.desiredAssertionStatus());

    @DexIgnore
    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        k56 k56 = new k56(i3, new byte[((i2 * 3) / 4)]);
        if (k56.a(bArr, i, i2, true)) {
            int i4 = k56.b;
            byte[] bArr2 = k56.a;
            if (i4 == bArr2.length) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr3, 0, i4);
            return bArr3;
        }
        throw new IllegalArgumentException("bad base-64");
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    @DexIgnore
    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        l56 l56 = new l56(i3, (byte[]) null);
        int i4 = (i2 / 3) * 4;
        int i5 = 2;
        if (!l56.f) {
            int i6 = i2 % 3;
            if (i6 != 0) {
                if (i6 == 1) {
                    i4 += 2;
                } else if (i6 == 2) {
                    i4 += 3;
                }
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (l56.g && i2 > 0) {
            int i7 = ((i2 - 1) / 57) + 1;
            if (!l56.h) {
                i5 = 1;
            }
            i4 += i7 * i5;
        }
        l56.a = new byte[i4];
        l56.a(bArr, i, i2, true);
        if (a || l56.b == i4) {
            return l56.a;
        }
        throw new AssertionError();
    }
}
