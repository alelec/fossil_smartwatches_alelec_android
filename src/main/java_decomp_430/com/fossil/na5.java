package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na5 {
    @DexIgnore
    public /* final */ ac5 a;
    @DexIgnore
    public /* final */ ab5 b;
    @DexIgnore
    public /* final */ ad5 c;
    @DexIgnore
    public /* final */ af5 d;
    @DexIgnore
    public /* final */ ag5 e;
    @DexIgnore
    public /* final */ ae5 f;

    @DexIgnore
    public na5(ac5 ac5, ab5 ab5, ad5 ad5, af5 af5, ag5 ag5, ae5 ae5) {
        wg6.b(ac5, "mActivityView");
        wg6.b(ab5, "mActiveTimeView");
        wg6.b(ad5, "mCaloriesView");
        wg6.b(af5, "mHeartrateView");
        wg6.b(ag5, "mSleepView");
        wg6.b(ae5, "mGoalTrackingView");
        this.a = ac5;
        this.b = ab5;
        this.c = ad5;
        this.d = af5;
        this.e = ag5;
        this.f = ae5;
    }

    @DexIgnore
    public final ab5 a() {
        return this.b;
    }

    @DexIgnore
    public final ac5 b() {
        return this.a;
    }

    @DexIgnore
    public final ad5 c() {
        return this.c;
    }

    @DexIgnore
    public final ae5 d() {
        return this.f;
    }

    @DexIgnore
    public final af5 e() {
        return this.d;
    }

    @DexIgnore
    public final ag5 f() {
        return this.e;
    }
}
