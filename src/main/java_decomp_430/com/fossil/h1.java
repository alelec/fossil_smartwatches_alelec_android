package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h1 extends MenuInflater {
    @DexIgnore
    public static /* final */ Class<?>[] e; // = {Context.class};
    @DexIgnore
    public static /* final */ Class<?>[] f; // = e;
    @DexIgnore
    public /* final */ Object[] a;
    @DexIgnore
    public /* final */ Object[] b; // = this.a;
    @DexIgnore
    public Context c;
    @DexIgnore
    public Object d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements MenuItem.OnMenuItemClickListener {
        @DexIgnore
        public static /* final */ Class<?>[] c; // = {MenuItem.class};
        @DexIgnore
        public Object a;
        @DexIgnore
        public Method b;

        @DexIgnore
        public a(Object obj, String str) {
            this.a = obj;
            Class<?> cls = obj.getClass();
            try {
                this.b = cls.getMethod(str, c);
            } catch (Exception e) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e);
                throw inflateException;
            }
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.b.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.b.invoke(this.a, new Object[]{menuItem})).booleanValue();
                }
                this.b.invoke(this.a, new Object[]{menuItem});
                return true;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @DexIgnore
    public h1(Context context) {
        super(context);
        this.c = context;
        this.a = new Object[]{context};
    }

    @DexIgnore
    public final void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) throws XmlPullParserException, IOException {
        b bVar = new b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        int i = eventType;
        String str = null;
        boolean z = false;
        boolean z2 = false;
        while (!z) {
            if (i != 1) {
                if (i != 2) {
                    if (i == 3) {
                        String name2 = xmlPullParser.getName();
                        if (z2 && name2.equals(str)) {
                            str = null;
                            z2 = false;
                        } else if (name2.equals("group")) {
                            bVar.d();
                        } else if (name2.equals("item")) {
                            if (!bVar.c()) {
                                b9 b9Var = bVar.A;
                                if (b9Var == null || !b9Var.hasSubMenu()) {
                                    bVar.a();
                                } else {
                                    bVar.b();
                                }
                            }
                        } else if (name2.equals("menu")) {
                            z = true;
                        }
                    }
                } else if (!z2) {
                    String name3 = xmlPullParser.getName();
                    if (name3.equals("group")) {
                        bVar.a(attributeSet);
                    } else if (name3.equals("item")) {
                        bVar.b(attributeSet);
                    } else if (name3.equals("menu")) {
                        a(xmlPullParser, attributeSet, bVar.b());
                    } else {
                        str = name3;
                        z2 = true;
                    }
                }
                i = xmlPullParser.next();
            } else {
                throw new RuntimeException("Unexpected end of document");
            }
        }
    }

    @DexIgnore
    public void inflate(int i, Menu menu) {
        if (!(menu instanceof u7)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.c.getResources().getLayout(i);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {
        @DexIgnore
        public b9 A;
        @DexIgnore
        public CharSequence B;
        @DexIgnore
        public CharSequence C;
        @DexIgnore
        public ColorStateList D; // = null;
        @DexIgnore
        public PorterDuff.Mode E; // = null;
        @DexIgnore
        public Menu a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public int i;
        @DexIgnore
        public int j;
        @DexIgnore
        public CharSequence k;
        @DexIgnore
        public CharSequence l;
        @DexIgnore
        public int m;
        @DexIgnore
        public char n;
        @DexIgnore
        public int o;
        @DexIgnore
        public char p;
        @DexIgnore
        public int q;
        @DexIgnore
        public int r;
        @DexIgnore
        public boolean s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public boolean u;
        @DexIgnore
        public int v;
        @DexIgnore
        public int w;
        @DexIgnore
        public String x;
        @DexIgnore
        public String y;
        @DexIgnore
        public String z;

        @DexIgnore
        public b(Menu menu) {
            this.a = menu;
            d();
        }

        @DexIgnore
        public void a(AttributeSet attributeSet) {
            TypedArray obtainStyledAttributes = h1.this.c.obtainStyledAttributes(attributeSet, j0.MenuGroup);
            this.b = obtainStyledAttributes.getResourceId(j0.MenuGroup_android_id, 0);
            this.c = obtainStyledAttributes.getInt(j0.MenuGroup_android_menuCategory, 0);
            this.d = obtainStyledAttributes.getInt(j0.MenuGroup_android_orderInCategory, 0);
            this.e = obtainStyledAttributes.getInt(j0.MenuGroup_android_checkableBehavior, 0);
            this.f = obtainStyledAttributes.getBoolean(j0.MenuGroup_android_visible, true);
            this.g = obtainStyledAttributes.getBoolean(j0.MenuGroup_android_enabled, true);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        public void b(AttributeSet attributeSet) {
            i3 a2 = i3.a(h1.this.c, attributeSet, j0.MenuItem);
            this.i = a2.g(j0.MenuItem_android_id, 0);
            this.j = (a2.d(j0.MenuItem_android_menuCategory, this.c) & -65536) | (a2.d(j0.MenuItem_android_orderInCategory, this.d) & 65535);
            this.k = a2.e(j0.MenuItem_android_title);
            this.l = a2.e(j0.MenuItem_android_titleCondensed);
            this.m = a2.g(j0.MenuItem_android_icon, 0);
            this.n = a(a2.d(j0.MenuItem_android_alphabeticShortcut));
            this.o = a2.d(j0.MenuItem_alphabeticModifiers, 4096);
            this.p = a(a2.d(j0.MenuItem_android_numericShortcut));
            this.q = a2.d(j0.MenuItem_numericModifiers, 4096);
            if (a2.g(j0.MenuItem_android_checkable)) {
                this.r = a2.a(j0.MenuItem_android_checkable, false) ? 1 : 0;
            } else {
                this.r = this.e;
            }
            this.s = a2.a(j0.MenuItem_android_checked, false);
            this.t = a2.a(j0.MenuItem_android_visible, this.f);
            this.u = a2.a(j0.MenuItem_android_enabled, this.g);
            this.v = a2.d(j0.MenuItem_showAsAction, -1);
            this.z = a2.d(j0.MenuItem_android_onClick);
            this.w = a2.g(j0.MenuItem_actionLayout, 0);
            this.x = a2.d(j0.MenuItem_actionViewClass);
            this.y = a2.d(j0.MenuItem_actionProviderClass);
            boolean z2 = this.y != null;
            if (z2 && this.w == 0 && this.x == null) {
                this.A = (b9) a(this.y, h1.f, h1.this.b);
            } else {
                if (z2) {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                }
                this.A = null;
            }
            this.B = a2.e(j0.MenuItem_contentDescription);
            this.C = a2.e(j0.MenuItem_tooltipText);
            if (a2.g(j0.MenuItem_iconTintMode)) {
                this.E = s2.a(a2.d(j0.MenuItem_iconTintMode, -1), this.E);
            } else {
                this.E = null;
            }
            if (a2.g(j0.MenuItem_iconTint)) {
                this.D = a2.a(j0.MenuItem_iconTint);
            } else {
                this.D = null;
            }
            a2.a();
            this.h = false;
        }

        @DexIgnore
        public boolean c() {
            return this.h;
        }

        @DexIgnore
        public void d() {
            this.b = 0;
            this.c = 0;
            this.d = 0;
            this.e = 0;
            this.f = true;
            this.g = true;
        }

        @DexIgnore
        public final char a(String str) {
            if (str == null) {
                return 0;
            }
            return str.charAt(0);
        }

        @DexIgnore
        public final void a(MenuItem menuItem) {
            boolean z2 = false;
            menuItem.setChecked(this.s).setVisible(this.t).setEnabled(this.u).setCheckable(this.r >= 1).setTitleCondensed(this.l).setIcon(this.m);
            int i2 = this.v;
            if (i2 >= 0) {
                menuItem.setShowAsAction(i2);
            }
            if (this.z != null) {
                if (!h1.this.c.isRestricted()) {
                    menuItem.setOnMenuItemClickListener(new a(h1.this.a(), this.z));
                } else {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
            }
            boolean z3 = menuItem instanceof t1;
            if (z3) {
                t1 t1Var = (t1) menuItem;
            }
            if (this.r >= 2) {
                if (z3) {
                    ((t1) menuItem).c(true);
                } else if (menuItem instanceof u1) {
                    ((u1) menuItem).a(true);
                }
            }
            String str = this.x;
            if (str != null) {
                menuItem.setActionView((View) a(str, h1.e, h1.this.a));
                z2 = true;
            }
            int i3 = this.w;
            if (i3 > 0) {
                if (!z2) {
                    menuItem.setActionView(i3);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            b9 b9Var = this.A;
            if (b9Var != null) {
                i9.a(menuItem, b9Var);
            }
            i9.a(menuItem, this.B);
            i9.b(menuItem, this.C);
            i9.a(menuItem, this.n, this.o);
            i9.b(menuItem, this.p, this.q);
            PorterDuff.Mode mode = this.E;
            if (mode != null) {
                i9.a(menuItem, mode);
            }
            ColorStateList colorStateList = this.D;
            if (colorStateList != null) {
                i9.a(menuItem, colorStateList);
            }
        }

        @DexIgnore
        public SubMenu b() {
            this.h = true;
            SubMenu addSubMenu = this.a.addSubMenu(this.b, this.i, this.j, this.k);
            a(addSubMenu.getItem());
            return addSubMenu;
        }

        @DexIgnore
        public void a() {
            this.h = true;
            a(this.a.add(this.b, this.i, this.j, this.k));
        }

        @DexIgnore
        public final <T> T a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = Class.forName(str, false, h1.this.c.getClassLoader()).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }
    }

    @DexIgnore
    public Object a() {
        if (this.d == null) {
            this.d = a(this.c);
        }
        return this.d;
    }

    @DexIgnore
    public final Object a(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? a(((ContextWrapper) obj).getBaseContext()) : obj;
    }
}
