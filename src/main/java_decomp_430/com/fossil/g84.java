package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g84 extends f84 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public /* final */ NestedScrollView w;
    @DexIgnore
    public long x;

    /*
    static {
        z.put(2131362049, 1);
        z.put(2131363177, 2);
        z.put(2131363294, 3);
        z.put(2131362580, 4);
        z.put(2131362052, 5);
        z.put(2131363184, 6);
        z.put(2131363296, 7);
        z.put(2131362582, 8);
        z.put(2131362149, 9);
        z.put(2131362206, 10);
    }
    */

    @DexIgnore
    public g84(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 11, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g84(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[5], objArr[9], objArr[10], objArr[4], objArr[8], objArr[2], objArr[6], objArr[3], objArr[7]);
        this.x = -1;
        this.w = objArr[0];
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
