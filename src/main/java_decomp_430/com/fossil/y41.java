package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y41 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (context != null && intent != null && (action = intent.getAction()) != null && action.hashCode() == -1021360715 && action.equals("android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED")) {
            BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
            int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
            int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
            if (bluetoothDevice != null) {
                s81.d.a(bluetoothDevice, intExtra, intExtra2);
            }
        }
    }
}
