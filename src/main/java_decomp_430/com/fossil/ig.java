package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.uf;
import com.fossil.vf;
import com.fossil.zf;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ig<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ vf<T> mDiffer;
    @DexIgnore
    public /* final */ vf.b<T> mListener; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements vf.b<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(List<T> list, List<T> list2) {
            ig.this.onCurrentListChanged(list, list2);
        }
    }

    @DexIgnore
    public ig(zf.d<T> dVar) {
        this.mDiffer = new vf<>(new tf(this), new uf.a(dVar).a());
        this.mDiffer.a(this.mListener);
    }

    @DexIgnore
    public List<T> getCurrentList() {
        return this.mDiffer.a();
    }

    @DexIgnore
    public T getItem(int i) {
        return this.mDiffer.a().get(i);
    }

    @DexIgnore
    public int getItemCount() {
        return this.mDiffer.a().size();
    }

    @DexIgnore
    public void onCurrentListChanged(List<T> list, List<T> list2) {
    }

    @DexIgnore
    public void submitList(List<T> list) {
        this.mDiffer.a(list);
    }

    @DexIgnore
    public void submitList(List<T> list, Runnable runnable) {
        this.mDiffer.b(list, runnable);
    }

    @DexIgnore
    public ig(uf<T> ufVar) {
        this.mDiffer = new vf<>(new tf(this), ufVar);
        this.mDiffer.a(this.mListener);
    }
}
