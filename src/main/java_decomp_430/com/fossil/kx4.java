package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx4 implements Factory<oj5> {
    @DexIgnore
    public static oj5 a(ex4 ex4) {
        oj5 f = ex4.f();
        z76.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
