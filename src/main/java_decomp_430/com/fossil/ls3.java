package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ls3 implements Runnable {
    @DexIgnore
    public /* final */ js3 a;
    @DexIgnore
    public /* final */ Bundle b;
    @DexIgnore
    public /* final */ rc3 c;

    @DexIgnore
    public ls3(js3 js3, Bundle bundle, rc3 rc3) {
        this.a = js3;
        this.b = bundle;
        this.c = rc3;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
