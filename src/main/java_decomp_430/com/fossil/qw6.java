package com.fossil;

import androidx.fragment.app.Fragment;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw6 {
    @DexIgnore
    public /* final */ zw6 a;
    @DexIgnore
    public /* final */ String[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ zw6 a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String[] c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public int g; // = -1;

        @DexIgnore
        public b(Fragment fragment, int i, String... strArr) {
            this.a = zw6.a(fragment);
            this.b = i;
            this.c = strArr;
        }

        @DexIgnore
        public b a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b a(int i) {
            this.g = i;
            return this;
        }

        @DexIgnore
        public qw6 a() {
            if (this.d == null) {
                this.d = this.a.a().getString(rw6.rationale_ask);
            }
            if (this.e == null) {
                this.e = this.a.a().getString(17039370);
            }
            if (this.f == null) {
                this.f = this.a.a().getString(17039360);
            }
            return new qw6(this.a, this.c, this.b, this.d, this.e, this.f, this.g);
        }
    }

    @DexIgnore
    public zw6 a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.f;
    }

    @DexIgnore
    public String[] c() {
        return (String[]) this.b.clone();
    }

    @DexIgnore
    public String d() {
        return this.e;
    }

    @DexIgnore
    public String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || qw6.class != obj.getClass()) {
            return false;
        }
        qw6 qw6 = (qw6) obj;
        if (!Arrays.equals(this.b, qw6.b) || this.c != qw6.c) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int f() {
        return this.c;
    }

    @DexIgnore
    public int g() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        return (Arrays.hashCode(this.b) * 31) + this.c;
    }

    @DexIgnore
    public String toString() {
        return "PermissionRequest{mHelper=" + this.a + ", mPerms=" + Arrays.toString(this.b) + ", mRequestCode=" + this.c + ", mRationale='" + this.d + '\'' + ", mPositiveButtonText='" + this.e + '\'' + ", mNegativeButtonText='" + this.f + '\'' + ", mTheme=" + this.g + '}';
    }

    @DexIgnore
    public qw6(zw6 zw6, String[] strArr, int i, String str, String str2, String str3, int i2) {
        this.a = zw6;
        this.b = (String[]) strArr.clone();
        this.c = i;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = i2;
    }
}
