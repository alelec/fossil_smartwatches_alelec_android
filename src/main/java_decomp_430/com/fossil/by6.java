package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class by6 implements fx6<zq6, Character> {
    @DexIgnore
    public static /* final */ by6 a; // = new by6();

    @DexIgnore
    public Character a(zq6 zq6) throws IOException {
        String string = zq6.string();
        if (string.length() == 1) {
            return Character.valueOf(string.charAt(0));
        }
        throw new IOException("Expected body of length 1 for Character conversion but was " + string.length());
    }
}
