package com.fossil;

import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl5 implements MembersInjector<DeleteAccountActivity> {
    @DexIgnore
    public static void a(DeleteAccountActivity deleteAccountActivity, DeleteAccountPresenter deleteAccountPresenter) {
        deleteAccountActivity.B = deleteAccountPresenter;
    }
}
