package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ko {
    @DexIgnore
    public Context a;
    @DexIgnore
    public SharedPreferences b;

    @DexIgnore
    public ko(Context context) {
        this.a = context;
    }

    @DexIgnore
    public void a(boolean z) {
        a().edit().putBoolean("reschedule_needed", z).apply();
    }

    @DexIgnore
    public boolean b() {
        return a().getBoolean("reschedule_needed", false);
    }

    @DexIgnore
    public final SharedPreferences a() {
        SharedPreferences sharedPreferences;
        synchronized (ko.class) {
            if (this.b == null) {
                this.b = this.a.getSharedPreferences("androidx.work.util.preferences", 0);
            }
            sharedPreferences = this.b;
        }
        return sharedPreferences;
    }
}
