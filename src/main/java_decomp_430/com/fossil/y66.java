package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y66 {
    @DexIgnore
    public static String a(Long l) {
        return a(l, true);
    }

    @DexIgnore
    public static String a(Long l, boolean z) {
        String str = "";
        if (l == null || l.longValue() < 0) {
            return str;
        }
        int i = z ? 1000 : 1024;
        if (l.longValue() < ((long) i)) {
            return l + " B";
        }
        double d = (double) i;
        int log = (int) (Math.log((double) l.longValue()) / Math.log(d));
        StringBuilder sb = new StringBuilder();
        sb.append((z ? "kMGTPE" : "KMGTPE").charAt(log - 1));
        if (!z) {
            str = "i";
        }
        sb.append(str);
        return String.format(Locale.US, "%.1f %sB", new Object[]{Double.valueOf(((double) l.longValue()) / Math.pow(d, (double) log)), sb.toString()});
    }
}
