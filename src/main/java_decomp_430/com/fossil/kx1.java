package com.fossil;

import com.fossil.wv1;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kx1 extends wv1 {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public kx1(String str) {
        this.b = str;
    }

    @DexIgnore
    public gv1 a() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public yv1<Status> b() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void c() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void d() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public boolean g() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void a(wv1.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void b(wv1.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }
}
