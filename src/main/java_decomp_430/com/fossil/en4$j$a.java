package com.fossil;

import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.manager.WeatherManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en4$j$a extends sf6 implements ig6<il6, xe6<? super lc6<? extends Weather, ? extends Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ WeatherLocationWrapper $location;
    @DexIgnore
    public /* final */ /* synthetic */ String $tempUnit$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ il6 $this_launch$inlined;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager.j this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public en4$j$a(WeatherLocationWrapper weatherLocationWrapper, xe6 xe6, String str, WeatherManager.j jVar, il6 il6) {
        super(2, xe6);
        this.$location = weatherLocationWrapper;
        this.$tempUnit$inlined = str;
        this.this$0 = jVar;
        this.$this_launch$inlined = il6;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        en4$j$a en4_j_a = new en4$j$a(this.$location, xe6, this.$tempUnit$inlined, this.this$0, this.$this_launch$inlined);
        en4_j_a.p$ = (il6) obj;
        return en4_j_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((en4$j$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            if (this.$location.isUseCurrentLocation()) {
                WeatherManager weatherManager = this.this$0.this$0;
                String str = this.$tempUnit$inlined;
                this.L$0 = il6;
                this.label = 1;
                obj = weatherManager.a("weather", str, (xe6<? super lc6<Weather, Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            } else {
                WeatherManager weatherManager2 = this.this$0.this$0;
                double lat = this.$location.getLat();
                double lng = this.$location.getLng();
                String str2 = this.$tempUnit$inlined;
                this.L$0 = il6;
                this.label = 2;
                obj = weatherManager2.a(lat, lng, str2, (xe6<? super lc6<Weather, Boolean>>) this);
                if (obj == a) {
                    return a;
                }
            }
        } else if (i == 1 || i == 2) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return (lc6) obj;
    }
}
