package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n92<T> implements q92<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public n92(T t) {
        this.a = t;
    }

    @DexIgnore
    public final T get() {
        return this.a;
    }
}
