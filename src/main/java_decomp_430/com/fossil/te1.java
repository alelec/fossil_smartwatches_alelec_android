package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te1 extends xg6 implements hg6<BluetoothGattService, String> {
    @DexIgnore
    public static /* final */ te1 a; // = new te1();

    @DexIgnore
    public te1() {
        super(1);
    }

    @DexIgnore
    /* renamed from: a */
    public final String invoke(BluetoothGattService bluetoothGattService) {
        StringBuilder sb = new StringBuilder();
        wg6.a(bluetoothGattService, "service");
        sb.append(bluetoothGattService.getUuid().toString());
        sb.append("\n");
        List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
        wg6.a(characteristics, "service.characteristics");
        sb.append(yd6.a(characteristics, "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, yc1.a, 30, (Object) null));
        return sb.toString();
    }
}
