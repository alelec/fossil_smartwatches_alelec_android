package com.fossil;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lo implements Executor {
    @DexIgnore
    public /* final */ ArrayDeque<a> a; // = new ArrayDeque<>();
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Runnable d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ lo a;
        @DexIgnore
        public /* final */ Runnable b;

        @DexIgnore
        public a(lo loVar, Runnable runnable) {
            this.a = loVar;
            this.b = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.b.run();
            } finally {
                this.a.b();
            }
        }
    }

    @DexIgnore
    public lo(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.c) {
            z = !this.a.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public void b() {
        synchronized (this.c) {
            Runnable poll = this.a.poll();
            this.d = poll;
            if (poll != null) {
                this.b.execute(this.d);
            }
        }
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        synchronized (this.c) {
            this.a.add(new a(this, runnable));
            if (this.d == null) {
                b();
            }
        }
    }
}
