package com.fossil;

import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bf5$b$a<T> implements ld<cf<DailyHeartRateSummary>> {
    @DexIgnore
    public /* final */ /* synthetic */ DashboardHeartRatePresenter.b a;

    @DexIgnore
    public bf5$b$a(DashboardHeartRatePresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(cf<DailyHeartRateSummary> cfVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("getSummariesPaging observer size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        MFLogger.d("DashboardHeartRatePresenter", sb.toString());
        if (cfVar != null) {
            this.a.this$0.g.b(cfVar);
        }
    }
}
