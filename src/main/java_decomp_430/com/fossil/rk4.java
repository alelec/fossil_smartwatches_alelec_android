package com.fossil;

import android.content.Context;
import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rk4 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = qd6.a((T[]) new String[]{MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue()});
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = qd6.a((T[]) new String[]{MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue()});
    @DexIgnore
    public static /* final */ rk4 c; // = new rk4();

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(String str) {
        wg6.b(str, "microAppId");
        if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886364);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
            return a2;
        } else if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886202);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        } else if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.getValue())) {
            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886925);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026naProfile_List__SetGoals)");
            return a4;
        } else if (!wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return "";
        } else {
            String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886256);
            wg6.a((Object) a5, "LanguageHelper.getString\u2026ingtone_Title__RingPhone)");
            return a5;
        }
    }

    @DexIgnore
    public final List<String> b(String str) {
        wg6.b(str, "microAppId");
        if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            int i = Build.VERSION.SDK_INT;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppHelper", "android.os.Build.VERSION.SDK_INT=" + i);
            if (i >= 29) {
                return qd6.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
            }
            return qd6.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
        } else if (wg6.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return qd6.a((T[]) new String[]{InAppPermission.NOTIFICATION_ACCESS});
        } else {
            return new ArrayList();
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        wg6.b(str, "microAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        wg6.b(str, "microAppId");
        return a.contains(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        wg6.b(str, "microAppId");
        List<String> b2 = b(str);
        String[] a2 = xx5.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppHelper", "isPermissionGrantedForMicroApp " + str + " granted=" + a2 + " required=" + b2);
        for (String a3 : b2) {
            if (!nd6.a((T[]) a2, a3)) {
                return false;
            }
        }
        return true;
    }
}
