package com.fossil;

import android.os.Bundle;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a02 implements wv1.b, wv1.c {
    @DexIgnore
    public /* final */ rv1<?> a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public c02 c;

    @DexIgnore
    public a02(rv1<?> rv1, boolean z) {
        this.a = rv1;
        this.b = z;
    }

    @DexIgnore
    public final void a(gv1 gv1) {
        a();
        this.c.a(gv1, this.a, this.b);
    }

    @DexIgnore
    public final void f(Bundle bundle) {
        a();
        this.c.f(bundle);
    }

    @DexIgnore
    public final void g(int i) {
        a();
        this.c.g(i);
    }

    @DexIgnore
    public final void a(c02 c02) {
        this.c = c02;
    }

    @DexIgnore
    public final void a() {
        w12.a(this.c, (Object) "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
