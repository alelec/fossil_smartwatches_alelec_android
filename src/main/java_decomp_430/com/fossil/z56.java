package com.fossil;

import com.sina.weibo.sdk.statistic.StatisticConfig;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.protocol.HttpContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z56 extends DefaultConnectionKeepAliveStrategy {
    @DexIgnore
    public z56(y56 y56) {
    }

    @DexIgnore
    public long getKeepAliveDuration(HttpResponse httpResponse, HttpContext httpContext) {
        long keepAliveDuration = z56.super.getKeepAliveDuration(httpResponse, httpContext);
        return keepAliveDuration == -1 ? StatisticConfig.MIN_UPLOAD_INTERVAL : keepAliveDuration;
    }
}
