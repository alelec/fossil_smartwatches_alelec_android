package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lt<Z> implements rt<Z> {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ rt<Z> c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ vr e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(vr vrVar, lt<?> ltVar);
    }

    @DexIgnore
    public lt(rt<Z> rtVar, boolean z, boolean z2, vr vrVar, a aVar) {
        q00.a(rtVar);
        this.c = rtVar;
        this.a = z;
        this.b = z2;
        this.e = vrVar;
        q00.a(aVar);
        this.d = aVar;
    }

    @DexIgnore
    public synchronized void a() {
        if (this.f > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (!this.g) {
            this.g = true;
            if (this.b) {
                this.c.a();
            }
        } else {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }

    @DexIgnore
    public int b() {
        return this.c.b();
    }

    @DexIgnore
    public Class<Z> c() {
        return this.c.c();
    }

    @DexIgnore
    public synchronized void d() {
        if (!this.g) {
            this.f++;
        } else {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    @DexIgnore
    public rt<Z> e() {
        return this.c;
    }

    @DexIgnore
    public boolean f() {
        return this.a;
    }

    @DexIgnore
    public void g() {
        boolean z;
        synchronized (this) {
            if (this.f > 0) {
                z = true;
                int i = this.f - 1;
                this.f = i;
                if (i != 0) {
                    z = false;
                }
            } else {
                throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.d.a(this.e, this);
        }
    }

    @DexIgnore
    public Z get() {
        return this.c.get();
    }

    @DexIgnore
    public synchronized String toString() {
        return "EngineResource{isMemoryCacheable=" + this.a + ", listener=" + this.d + ", key=" + this.e + ", acquired=" + this.f + ", isRecycled=" + this.g + ", resource=" + this.c + '}';
    }
}
