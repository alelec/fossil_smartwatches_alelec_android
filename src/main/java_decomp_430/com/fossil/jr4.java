package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum jr4 {
    CHILD,
    CHILD_ITEM_WITH_SWITCH,
    CHILD_ITEM_WITH_TEXT,
    CHILD_ITEM_WITH_SPINNER
}
