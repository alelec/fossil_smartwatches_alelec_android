package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o55 implements MembersInjector<SearchRingPhoneActivity> {
    @DexIgnore
    public static void a(SearchRingPhoneActivity searchRingPhoneActivity, SearchRingPhonePresenter searchRingPhonePresenter) {
        searchRingPhoneActivity.B = searchRingPhonePresenter;
    }
}
