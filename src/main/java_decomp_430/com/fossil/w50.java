package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w50 extends x50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public w50 createFromParcel(Parcel parcel) {
            return new w50(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new w50[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m65createFromParcel(Parcel parcel) {
            return new w50(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public w50() {
        super(z50.CHANCE_OF_RAIN, (ic0) null, (lc0) null, (mc0) null, 14);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ w50(lc0 lc0, mc0 mc0, int i, qg6 qg6) {
        this(lc0, (i & 2) != 0 ? new mc0(mc0.CREATOR.a()) : mc0);
    }

    @DexIgnore
    public w50(lc0 lc0, mc0 mc0) {
        super(z50.CHANCE_OF_RAIN, (ic0) null, lc0, mc0, 2);
    }

    @DexIgnore
    public /* synthetic */ w50(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
