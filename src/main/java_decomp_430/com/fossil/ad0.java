package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ z80[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ad0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new ad0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ad0[i];
        }
    }

    @DexIgnore
    public ad0(da0 da0, uc0 uc0, z80[] z80Arr) throws IllegalArgumentException {
        super(da0, uc0);
        this.c = z80Arr;
        boolean z = true;
        if (this.c.length < 1 && getDeviceMessage() == null) {
            z = false;
        }
        if (!z) {
            throw new IllegalArgumentException("weatherInfoArray must have at least 1 elements.".toString());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00b1  */
    public byte[] a(short s, w40 w40) {
        String str;
        String jSONObject;
        x90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (!(this.c.length == 0)) {
                JSONArray jSONArray = new JSONArray();
                int min = Math.min(this.c.length, 3);
                for (int i = 0; i < min; i++) {
                    jSONArray.put(this.c[i].b());
                }
                jSONObject2.put("weatherApp._.config.locations", jSONArray);
                JSONObject jSONObject3 = new JSONObject();
                str = valueOf != null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject4 = new JSONObject();
                    jSONObject4.put("id", valueOf);
                    jSONObject4.put("set", jSONObject2);
                    jSONObject3.put(str, jSONObject4);
                } catch (JSONException e) {
                    qs0.h.a(e);
                }
                jSONObject = jSONObject3.toString();
                wg6.a(jSONObject, "deviceResponseJSONObject.toString()");
                Charset f = mi0.A.f();
                if (jSONObject == null) {
                    byte[] bytes = jSONObject.getBytes(f);
                    wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
            JSONObject jSONObject5 = new JSONObject();
            uc0 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject5.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
            }
            jSONObject2.put("weatherApp._.config.locations", jSONObject5);
            JSONObject jSONObject32 = new JSONObject();
            if (valueOf != null) {
            }
            JSONObject jSONObject42 = new JSONObject();
            jSONObject42.put("id", valueOf);
            jSONObject42.put("set", jSONObject2);
            jSONObject32.put(str, jSONObject42);
            jSONObject = jSONObject32.toString();
            wg6.a(jSONObject, "deviceResponseJSONObject.toString()");
            Charset f2 = mi0.A.f();
            if (jSONObject == null) {
            }
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
    }

    @DexIgnore
    public JSONObject b() {
        return cw0.a(super.b(), bm0.WEATHER_CONFIGS, (Object) cw0.a((p40[]) this.c));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(ad0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !Arrays.equals(this.c, ((ad0) obj).c);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherWatchAppData");
    }

    @DexIgnore
    public final z80[] getWeatherInfoArray() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + Arrays.hashCode(this.c);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }

    @DexIgnore
    public ad0(da0 da0, z80[] z80Arr) throws IllegalArgumentException {
        this(da0, (uc0) null, z80Arr);
    }

    @DexIgnore
    public ad0(da0 da0, uc0 uc0) throws IllegalArgumentException {
        this(da0, uc0, new z80[0]);
    }

    @DexIgnore
    public ad0(uc0 uc0) throws IllegalArgumentException {
        this((da0) null, uc0, new z80[0]);
    }

    @DexIgnore
    public ad0(z80[] z80Arr) throws IllegalArgumentException {
        this((da0) null, (uc0) null, z80Arr);
    }

    @DexIgnore
    public /* synthetic */ ad0(Parcel parcel, qg6 qg6) {
        super((x90) parcel.readParcelable(da0.class.getClassLoader()), (uc0) parcel.readParcelable(uc0.class.getClassLoader()));
        Object[] createTypedArray = parcel.createTypedArray(z80.CREATOR);
        if (createTypedArray != null) {
            this.c = (z80[]) createTypedArray;
        } else {
            wg6.a();
            throw null;
        }
    }
}
