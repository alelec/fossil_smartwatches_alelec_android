package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.receiver.AlarmReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo4 implements MembersInjector<AlarmReceiver> {
    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, UserRepository userRepository) {
        alarmReceiver.a = userRepository;
    }

    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, an4 an4) {
        alarmReceiver.b = an4;
    }

    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, DeviceRepository deviceRepository) {
        alarmReceiver.c = deviceRepository;
    }

    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, AlarmHelper alarmHelper) {
        alarmReceiver.d = alarmHelper;
    }

    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, AlarmsRepository alarmsRepository) {
        alarmReceiver.e = alarmsRepository;
    }
}
