package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mu1 extends ha2 implements lu1 {
    @DexIgnore
    public mu1() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 101:
                a((GoogleSignInAccount) ia2.a(parcel, GoogleSignInAccount.CREATOR), (Status) ia2.a(parcel, Status.CREATOR));
                throw null;
            case 102:
                a((Status) ia2.a(parcel, Status.CREATOR));
                break;
            case 103:
                b((Status) ia2.a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
