package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pp1 implements Factory<Executor> {
    @DexIgnore
    public static /* final */ pp1 a; // = new pp1();

    @DexIgnore
    public static pp1 a() {
        return a;
    }

    @DexIgnore
    public static Executor b() {
        Executor a2 = op1.a();
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public Executor get() {
        return b();
    }
}
