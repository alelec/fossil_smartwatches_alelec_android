package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vk6 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(vk6.class, "_handled");
    @DexIgnore
    public volatile int _handled;
    @DexIgnore
    public /* final */ Throwable a;

    @DexIgnore
    public vk6(Throwable th, boolean z) {
        wg6.b(th, "cause");
        this.a = th;
        this._handled = z ? 1 : 0;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [int, boolean] */
    public final boolean a() {
        return this._handled;
    }

    @DexIgnore
    public final boolean b() {
        return b.compareAndSet(this, 0, 1);
    }

    @DexIgnore
    public String toString() {
        return ol6.a((Object) this) + '[' + this.a + ']';
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ vk6(Throwable th, boolean z, int i, qg6 qg6) {
        this(th, (i & 2) != 0 ? false : z);
    }
}
