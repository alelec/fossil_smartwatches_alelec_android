package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm5 implements Factory<sm5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public tm5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static tm5 a(Provider<ThemeRepository> provider) {
        return new tm5(provider);
    }

    @DexIgnore
    public static sm5 b(Provider<ThemeRepository> provider) {
        return new CustomizeActiveCaloriesChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeActiveCaloriesChartViewModel get() {
        return b(this.a);
    }
}
