package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw3 {
    @DexIgnore
    public static /* final */ String[] a; // = new String[0];

    @DexIgnore
    public static int a(int i) {
        return i >>> 3;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    @DexIgnore
    public static final int a(iw3 iw3, int i) throws IOException {
        int a2 = iw3.a();
        iw3.f(i);
        int i2 = 1;
        while (iw3.j() == i) {
            iw3.f(i);
            i2++;
        }
        iw3.e(a2);
        return i2;
    }

    @DexIgnore
    public static int b(int i) {
        return i & 7;
    }

    @DexIgnore
    public static boolean b(iw3 iw3, int i) throws IOException {
        return iw3.f(i);
    }
}
