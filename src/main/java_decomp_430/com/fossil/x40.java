package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x40 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ w40 a;
    @DexIgnore
    public /* final */ w40 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x40> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(w40.class.getClassLoader());
            if (readParcelable != null) {
                w40 w40 = (w40) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(w40.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new x40(w40, (w40) readParcelable2);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new x40[i];
        }
    }

    @DexIgnore
    public x40(w40 w40, w40 w402) {
        this.a = w40;
        this.b = w402;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.CURRENT_VERSION, (Object) this.a), bm0.SUPPORTED_VERSION, (Object) this.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(x40.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x40 x40 = (x40) obj;
            return !(wg6.a(this.a, x40.a) ^ true) && !(wg6.a(this.b, x40.b) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.VersionInformation");
    }

    @DexIgnore
    public final w40 getCurrentVersion() {
        return this.a;
    }

    @DexIgnore
    public final w40 getSupportedVersion() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }
}
