package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i81 extends qv0 {
    @DexIgnore
    public hg6<? super em0, cd6> A;
    @DexIgnore
    public ig6<? super byte[], ? super rg1, cd6> B;
    @DexIgnore
    public long C;

    @DexIgnore
    public i81(ue1 ue1) {
        super(lx0.STREAMING, ue1, 0, 4);
    }

    @DexIgnore
    public void a(ni1 ni1) {
        if (ni1.a == h91.DISCONNECTED) {
            o91.b.a(this.y.t);
        }
    }

    @DexIgnore
    public void b(sg1 sg1) {
        Object obj;
        hg6<? super em0, cd6> hg6;
        sg1 sg12 = sg1;
        byte[] bArr = sg12.b;
        rg1 rg1 = sg12.a;
        ig6<? super byte[], ? super rg1, cd6> ig6 = this.B;
        if (ig6 != null) {
            cd6 cd6 = (cd6) ig6.invoke(bArr, rg1);
        }
        if (rg1 == rg1.ASYNC) {
            em0 a = o91.b.a(this.y.t, bArr);
            qs0 qs0 = qs0.h;
            og0 og0 = og0.RESPONSE;
            String str = this.y.t;
            String str2 = this.c;
            String str3 = this.d;
            JSONObject a2 = cw0.a(new JSONObject(), bm0.RAW_DATA, (Object) cw0.a(bArr, (String) null, 1));
            bm0 bm0 = bm0.DEVICE_EVENT;
            if (a == null || (obj = a.a()) == null) {
                obj = JSONObject.NULL;
            }
            nn0 nn0 = r4;
            nn0 nn02 = new nn0("streaming", og0, str, str2, str3, true, (String) null, (r40) null, (bw0) null, cw0.a(a2, bm0, obj), 448);
            qs0.a(nn0);
            if (a != null && (hg6 = this.A) != null) {
                cd6 cd62 = (cd6) hg6.invoke(a);
            }
        }
    }

    @DexIgnore
    public ok0 d() {
        return null;
    }

    @DexIgnore
    public long e() {
        return this.C;
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void a(long j) {
        this.C = j;
    }
}
