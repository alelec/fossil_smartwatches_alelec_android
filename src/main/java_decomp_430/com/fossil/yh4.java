package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yh4 {
    SYNC_IS_IN_PROGRESS,
    FAIL_DUE_TO_LACK_PERMISSION,
    FAIL_DUE_TO_PENDING_WORKOUT,
    FAIL_DUE_TO_INVALID_REQUEST,
    FAIL_DUE_TO_SYNC_FAIL,
    FAIL_DUE_TO_USER_DENY_STOP_WORKOUT
}
