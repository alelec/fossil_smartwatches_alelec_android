package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class to0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[il0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[lf0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[il0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[il0.values().length];

    /*
    static {
        a[il0.SUCCESS.ordinal()] = 1;
        a[il0.BLUETOOTH_OFF.ordinal()] = 2;
        a[il0.REQUEST_TIMEOUT.ordinal()] = 3;
        a[il0.INTERRUPTED.ordinal()] = 4;
        a[il0.HID_PROXY_NOT_CONNECTED.ordinal()] = 5;
        a[il0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD.ordinal()] = 6;
        a[il0.HID_INPUT_DEVICE_DISABLED.ordinal()] = 7;
        a[il0.HID_UNKNOWN_ERROR.ordinal()] = 8;
        a[il0.CONNECTION_DROPPED.ordinal()] = 9;
        a[il0.INVALID_RESPONSE_DATA.ordinal()] = 10;
        a[il0.INVALID_RESPONSE_LENGTH.ordinal()] = 11;
        a[il0.RESPONSE_ERROR.ordinal()] = 12;
        a[il0.WRONG_AUTHENTICATION_KEY_TYPE.ordinal()] = 13;
        a[il0.MISS_PACKAGE.ordinal()] = 14;
        a[il0.INVALID_DATA_LENGTH.ordinal()] = 15;
        a[il0.RECEIVED_DATA_CRC_MISS_MATCH.ordinal()] = 16;
        a[il0.REQUEST_UNSUPPORTED.ordinal()] = 17;
        a[il0.UNSUPPORTED_FILE_HANDLE.ordinal()] = 18;
        a[il0.COMMAND_ERROR.ordinal()] = 19;
        a[il0.NOT_START.ordinal()] = 20;
        a[il0.UNKNOWN_ERROR.ordinal()] = 21;
        b[lf0.GATT_ERROR.ordinal()] = 1;
        c[il0.INVALID_RESPONSE_DATA.ordinal()] = 1;
        c[il0.INVALID_RESPONSE_LENGTH.ordinal()] = 2;
        c[il0.RESPONSE_ERROR.ordinal()] = 3;
        c[il0.WRONG_AUTHENTICATION_KEY_TYPE.ordinal()] = 4;
        c[il0.MISS_PACKAGE.ordinal()] = 5;
        c[il0.INVALID_DATA_LENGTH.ordinal()] = 6;
        c[il0.RECEIVED_DATA_CRC_MISS_MATCH.ordinal()] = 7;
        c[il0.EOF_TIME_OUT.ordinal()] = 8;
        c[il0.RESPONSE_TIMEOUT.ordinal()] = 9;
        d[il0.REQUEST_TIMEOUT.ordinal()] = 1;
        d[il0.CONNECTION_DROPPED.ordinal()] = 2;
        d[il0.BLUETOOTH_OFF.ordinal()] = 3;
    }
    */
}
