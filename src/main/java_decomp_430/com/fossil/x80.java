package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x80 extends p40 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ k70 a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ j70 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                k70 valueOf = k70.valueOf(readString);
                float readFloat = parcel.readFloat();
                float readFloat2 = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    return new x80(valueOf, readFloat, readFloat2, j70.valueOf(readString2));
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new x80[i];
        }
    }

    @DexIgnore
    public x80(k70 k70, float f, float f2, j70 j70) {
        this.a = k70;
        this.b = cw0.a(f, 2);
        this.c = cw0.a(f2, 2);
        this.d = j70;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.WEEKDAY, (Object) cw0.a((Enum<?>) this.a)), bm0.HIGH_TEMPERATURE, (Object) Float.valueOf(this.b)), bm0.LOW_TEMPERATURE, (Object) Float.valueOf(this.c)), bm0.WEATHER_CONDITION, (Object) cw0.a((Enum<?>) this.d));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject put = new JSONObject().put("day", this.a.a()).put("high", Float.valueOf(this.b)).put("low", Float.valueOf(this.c)).put("cond_id", this.d.a());
        wg6.a(put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(x80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x80 x80 = (x80) obj;
            return this.a == x80.a && this.b == x80.b && this.c == x80.c && this.d == x80.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherDayForecast");
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.b;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.c;
    }

    @DexIgnore
    public final j70 getWeatherCondition() {
        return this.d;
    }

    @DexIgnore
    public final k70 getWeekday() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
