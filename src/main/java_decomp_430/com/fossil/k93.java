package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k93 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ea3 a;
    @DexIgnore
    public /* final */ /* synthetic */ Runnable b;

    @DexIgnore
    public k93(j93 j93, ea3 ea3, Runnable runnable) {
        this.a = ea3;
        this.b = runnable;
    }

    @DexIgnore
    public final void run() {
        this.a.t();
        this.a.a(this.b);
        this.a.s();
    }
}
