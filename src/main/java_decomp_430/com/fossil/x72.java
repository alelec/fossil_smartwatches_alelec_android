package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x72 implements Parcelable.Creator<f72> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        DataType dataType = null;
        String str = null;
        g72 g72 = null;
        t72 t72 = null;
        String str2 = null;
        int[] iArr = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    dataType = f22.a(parcel, a, DataType.CREATOR);
                    break;
                case 2:
                    str = f22.e(parcel, a);
                    break;
                case 3:
                    i = f22.q(parcel, a);
                    break;
                case 4:
                    g72 = f22.a(parcel, a, g72.CREATOR);
                    break;
                case 5:
                    t72 = f22.a(parcel, a, t72.CREATOR);
                    break;
                case 6:
                    str2 = f22.e(parcel, a);
                    break;
                case 8:
                    iArr = f22.d(parcel, a);
                    break;
                default:
                    f22.v(parcel, a);
                    break;
            }
        }
        f22.h(parcel, b);
        return new f72(dataType, str, i, g72, t72, str2, iArr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new f72[i];
    }
}
