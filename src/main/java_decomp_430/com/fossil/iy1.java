package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iy1 {
    @DexIgnore
    public /* final */ gy1 a;

    @DexIgnore
    public iy1(gy1 gy1) {
        this.a = gy1;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void a(jy1 jy1) {
        jy1.a.lock();
        try {
            if (jy1.o == this.a) {
                a();
                jy1.a.unlock();
            }
        } finally {
            jy1.a.unlock();
        }
    }
}
