package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr0 implements Parcelable.Creator<it0> {
    @DexIgnore
    public /* synthetic */ pr0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        return new it0(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new it0[i];
    }
}
