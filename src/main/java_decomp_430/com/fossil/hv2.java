package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hv2 extends dk2 implements ev2 {
    @DexIgnore
    public hv2() {
        super("com.google.android.gms.measurement.api.internal.IBundleReceiver");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        d((Bundle) li2.a(parcel, Bundle.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
