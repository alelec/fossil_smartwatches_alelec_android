package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi1 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ hm1 a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vi1(hm1 hm1, long j) {
        super(1);
        this.a = hm1;
        this.b = j;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        if (this.b == ((th1) obj).J) {
            hm1 hm1 = this.a;
            hm1.H = hm1.G;
            hm1 hm12 = this.a;
            long j = hm12.F;
            long j2 = hm12.H;
            if (j != j2) {
                if1.a((if1) hm12, (qv0) new ks0(Math.max(0, hm12.F - ((long) 4)), hm12.O, hm12.w, 0, 8), (hg6) new qm0(hm12), (hg6) new jo0(hm12), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
            } else if (j2 == hm12.C) {
                hm12.n();
            } else {
                hm12.m();
            }
        } else {
            hm1 hm13 = this.a;
            hm13.G = Math.max(0, hm13.G - 6144);
            hm1 hm14 = this.a;
            if (hm14.G == 0) {
                hm14.m();
            } else {
                hm14.o();
            }
        }
        return cd6.a;
    }
}
