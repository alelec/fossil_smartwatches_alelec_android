package com.fossil;

import android.os.Process;
import com.fossil.ip;
import com.fossil.up;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jp extends Thread {
    @DexIgnore
    public static /* final */ boolean g; // = cq.b;
    @DexIgnore
    public /* final */ BlockingQueue<up<?>> a;
    @DexIgnore
    public /* final */ BlockingQueue<up<?>> b;
    @DexIgnore
    public /* final */ ip c;
    @DexIgnore
    public /* final */ xp d;
    @DexIgnore
    public volatile boolean e; // = false;
    @DexIgnore
    public /* final */ b f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ up a;

        @DexIgnore
        public a(up upVar) {
            this.a = upVar;
        }

        @DexIgnore
        public void run() {
            try {
                jp.this.b.put(this.a);
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements up.b {
        @DexIgnore
        public /* final */ Map<String, List<up<?>>> a; // = new HashMap();
        @DexIgnore
        public /* final */ jp b;

        @DexIgnore
        public b(jp jpVar) {
            this.b = jpVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0039, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
            return false;
         */
        @DexIgnore
        public final synchronized boolean b(up<?> upVar) {
            String cacheKey = upVar.getCacheKey();
            if (this.a.containsKey(cacheKey)) {
                List list = this.a.get(cacheKey);
                if (list == null) {
                    list = new ArrayList();
                }
                upVar.addMarker("waiting-for-response");
                list.add(upVar);
                this.a.put(cacheKey, list);
                if (cq.b) {
                    cq.b("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                }
            } else {
                this.a.put(cacheKey, (Object) null);
                upVar.setNetworkRequestCompleteListener(this);
                if (cq.b) {
                    cq.b("new request, sending to network %s", cacheKey);
                }
            }
        }

        @DexIgnore
        public void a(up<?> upVar, wp<?> wpVar) {
            List<up> remove;
            ip.a aVar = wpVar.b;
            if (aVar == null || aVar.a()) {
                a(upVar);
                return;
            }
            String cacheKey = upVar.getCacheKey();
            synchronized (this) {
                remove = this.a.remove(cacheKey);
            }
            if (remove != null) {
                if (cq.b) {
                    cq.d("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), cacheKey);
                }
                for (up a2 : remove) {
                    this.b.d.a((up<?>) a2, wpVar);
                }
            }
        }

        @DexIgnore
        public synchronized void a(up<?> upVar) {
            String cacheKey = upVar.getCacheKey();
            List remove = this.a.remove(cacheKey);
            if (remove != null && !remove.isEmpty()) {
                if (cq.b) {
                    cq.d("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(remove.size()), cacheKey);
                }
                up upVar2 = (up) remove.remove(0);
                this.a.put(cacheKey, remove);
                upVar2.setNetworkRequestCompleteListener(this);
                try {
                    this.b.b.put(upVar2);
                } catch (InterruptedException e) {
                    cq.c("Couldn't add request to queue. %s", e.toString());
                    Thread.currentThread().interrupt();
                    this.b.b();
                }
            }
            return;
        }
    }

    @DexIgnore
    public jp(BlockingQueue<up<?>> blockingQueue, BlockingQueue<up<?>> blockingQueue2, ip ipVar, xp xpVar) {
        this.a = blockingQueue;
        this.b = blockingQueue2;
        this.c = ipVar;
        this.d = xpVar;
        this.f = new b(this);
    }

    @DexIgnore
    public void run() {
        if (g) {
            cq.d("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.c.d();
        while (true) {
            try {
                a();
            } catch (InterruptedException unused) {
                if (this.e) {
                    Thread.currentThread().interrupt();
                    return;
                }
                cq.c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }

    @DexIgnore
    public final void a() throws InterruptedException {
        a(this.a.take());
    }

    @DexIgnore
    public void b() {
        this.e = true;
        interrupt();
    }

    @DexIgnore
    public void a(up<?> upVar) throws InterruptedException {
        upVar.addMarker("cache-queue-take");
        if (upVar.isCanceled()) {
            upVar.finish("cache-discard-canceled");
            return;
        }
        ip.a a2 = this.c.a(upVar.getCacheKey());
        if (a2 == null) {
            upVar.addMarker("cache-miss");
            if (!this.f.b(upVar)) {
                this.b.put(upVar);
            }
        } else if (a2.a()) {
            upVar.addMarker("cache-hit-expired");
            upVar.setCacheEntry(a2);
            if (!this.f.b(upVar)) {
                this.b.put(upVar);
            }
        } else {
            upVar.addMarker("cache-hit");
            wp<?> parseNetworkResponse = upVar.parseNetworkResponse(new rp(a2.a, a2.g));
            upVar.addMarker("cache-hit-parsed");
            if (!a2.b()) {
                this.d.a(upVar, parseNetworkResponse);
                return;
            }
            upVar.addMarker("cache-hit-refresh-needed");
            upVar.setCacheEntry(a2);
            parseNetworkResponse.d = true;
            if (!this.f.b(upVar)) {
                this.d.a(upVar, parseNetworkResponse, new a(upVar));
            } else {
                this.d.a(upVar, parseNetworkResponse);
            }
        }
    }
}
