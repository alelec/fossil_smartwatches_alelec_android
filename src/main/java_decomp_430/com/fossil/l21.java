package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l21 extends xk1 {
    @DexIgnore
    public /* final */ tc0 R;
    @DexIgnore
    public /* final */ w31 S;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ l21(ue1 ue1, q41 q41, tc0 tc0, w31 w31, String str, int i) {
        super(ue1, q41, eh1.SEND_DEVICE_DATA, true, lk1.b.b(ue1.t, w31), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? ze0.a("UUID.randomUUID().toString()") : str, 32);
        this.R = tc0;
        this.S = w31;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.DEVICE_DATA, (Object) this.R.a());
    }

    @DexIgnore
    public byte[] n() {
        tc0 tc0 = this.R;
        short b = lk1.b.b(this.w.t, this.S);
        w40 w40 = this.x.a().i().get(Short.valueOf(this.S.a));
        if (w40 == null) {
            w40 = mi0.A.g();
        }
        return tc0.a(b, w40);
    }
}
