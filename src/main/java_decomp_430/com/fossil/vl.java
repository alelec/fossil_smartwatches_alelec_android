package com.fossil;

import android.os.Build;
import androidx.work.ListenableWorker;
import androidx.work.OverwritingInputMerger;
import com.fossil.cm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl extends cm {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends cm.a<a, vl> {
        @DexIgnore
        public a(Class<? extends ListenableWorker> cls) {
            super(cls);
            this.c.d = OverwritingInputMerger.class.getName();
        }

        @DexIgnore
        public a c() {
            return this;
        }

        @DexIgnore
        public vl b() {
            if (!this.a || Build.VERSION.SDK_INT < 23 || !this.c.j.h()) {
                return new vl(this);
            }
            throw new IllegalArgumentException("Cannot set backoff criteria on an idle mode job");
        }
    }

    @DexIgnore
    public vl(a aVar) {
        super(aVar.b, aVar.c, aVar.d);
    }
}
