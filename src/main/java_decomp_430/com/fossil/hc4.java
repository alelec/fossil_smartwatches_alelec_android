package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hc4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ FlexibleCheckBox r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ View v;
    @DexIgnore
    public /* final */ ConstraintLayout w;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hc4(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, FlexibleCheckBox flexibleCheckBox2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, RTLImageView rTLImageView, View view2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, ConstraintLayout constraintLayout5) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = flexibleCheckBox2;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = rTLImageView;
        this.v = view2;
        this.w = constraintLayout5;
    }
}
