package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g52 {
    @DexIgnore
    public static g52 b; // = new g52();
    @DexIgnore
    public f52 a; // = null;

    @DexIgnore
    public static f52 b(Context context) {
        return b.a(context);
    }

    @DexIgnore
    public final synchronized f52 a(Context context) {
        if (this.a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.a = new f52(context);
        }
        return this.a;
    }
}
