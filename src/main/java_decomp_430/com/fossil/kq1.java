package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq1 implements Factory<jq1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<hq1> b;

    @DexIgnore
    public kq1(Provider<Context> provider, Provider<hq1> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static kq1 a(Provider<Context> provider, Provider<hq1> provider2) {
        return new kq1(provider, provider2);
    }

    @DexIgnore
    public jq1 get() {
        return new jq1((Context) this.a.get(), (hq1) this.b.get());
    }
}
