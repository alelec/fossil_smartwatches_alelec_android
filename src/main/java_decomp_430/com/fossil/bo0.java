package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bo0 {
    GET((byte) 0),
    SET((byte) 1);
    
    @DexIgnore
    public static /* final */ jm0 e; // = null;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        e = new jm0((qg6) null);
    }
    */

    @DexIgnore
    public bo0(byte b) {
        this.a = b;
    }
}
