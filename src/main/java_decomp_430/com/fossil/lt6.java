package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lt6 extends zt6, ReadableByteChannel {
    @DexIgnore
    long a(byte b) throws IOException;

    @DexIgnore
    long a(yt6 yt6) throws IOException;

    @DexIgnore
    jt6 a();

    @DexIgnore
    String a(Charset charset) throws IOException;

    @DexIgnore
    boolean a(long j, mt6 mt6) throws IOException;

    @DexIgnore
    mt6 e(long j) throws IOException;

    @DexIgnore
    byte[] e() throws IOException;

    @DexIgnore
    String f(long j) throws IOException;

    @DexIgnore
    boolean f() throws IOException;

    @DexIgnore
    long g() throws IOException;

    @DexIgnore
    boolean g(long j) throws IOException;

    @DexIgnore
    String h() throws IOException;

    @DexIgnore
    byte[] h(long j) throws IOException;

    @DexIgnore
    int i() throws IOException;

    @DexIgnore
    void i(long j) throws IOException;

    @DexIgnore
    short j() throws IOException;

    @DexIgnore
    long q() throws IOException;

    @DexIgnore
    InputStream r();

    @DexIgnore
    byte readByte() throws IOException;

    @DexIgnore
    void readFully(byte[] bArr) throws IOException;

    @DexIgnore
    int readInt() throws IOException;

    @DexIgnore
    short readShort() throws IOException;

    @DexIgnore
    void skip(long j) throws IOException;
}
