package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class re4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ TabLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ViewPager t;

    @DexIgnore
    public re4(Object obj, View view, int i, ImageView imageView, TabLayout tabLayout, FlexibleTextView flexibleTextView, View view2, ViewPager viewPager) {
        super(obj, view, i);
        this.q = imageView;
        this.r = tabLayout;
        this.s = flexibleTextView;
        this.t = viewPager;
    }
}
