package com.fossil;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nx3 {
    @DexIgnore
    public static /* final */ Unsafe a; // = c();
    @DexIgnore
    public static /* final */ boolean b; // = g();
    @DexIgnore
    public static /* final */ boolean c; // = f();
    @DexIgnore
    public static /* final */ long d; // = ((long) a());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements PrivilegedExceptionAction<Unsafe> {
        @DexIgnore
        public Unsafe run() throws Exception {
            Class<Unsafe> cls = Unsafe.class;
            for (Field field : cls.getDeclaredFields()) {
                field.setAccessible(true);
                Object obj = field.get((Object) null);
                if (cls.isInstance(obj)) {
                    return cls.cast(obj);
                }
            }
            return null;
        }
    }

    /*
    static {
        a(a((Class<?>) Buffer.class, "address"));
    }
    */

    @DexIgnore
    public static byte a(byte[] bArr, long j) {
        return a.getByte(bArr, j);
    }

    @DexIgnore
    public static long b() {
        return d;
    }

    @DexIgnore
    public static Unsafe c() {
        try {
            return (Unsafe) AccessController.doPrivileged(new a());
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean d() {
        return c;
    }

    @DexIgnore
    public static boolean e() {
        return b;
    }

    @DexIgnore
    public static boolean f() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = a;
        if (unsafe != null) {
            try {
                Class<?> cls2 = unsafe.getClass();
                cls2.getMethod("arrayBaseOffset", new Class[]{Class.class});
                cls2.getMethod("getByte", new Class[]{cls, Long.TYPE});
                cls2.getMethod("putByte", new Class[]{cls, Long.TYPE, Byte.TYPE});
                cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
                cls2.getMethod("copyMemory", new Class[]{cls, Long.TYPE, cls, Long.TYPE, Long.TYPE});
                return true;
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean g() {
        Unsafe unsafe = a;
        if (unsafe != null) {
            try {
                Class<?> cls = unsafe.getClass();
                cls.getMethod("objectFieldOffset", new Class[]{Field.class});
                cls.getMethod("getByte", new Class[]{Long.TYPE});
                cls.getMethod("getLong", new Class[]{Object.class, Long.TYPE});
                cls.getMethod("putByte", new Class[]{Long.TYPE, Byte.TYPE});
                cls.getMethod("getLong", new Class[]{Long.TYPE});
                cls.getMethod("copyMemory", new Class[]{Long.TYPE, Long.TYPE, Long.TYPE});
                return true;
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    @DexIgnore
    public static void a(byte[] bArr, long j, byte b2) {
        a.putByte(bArr, j, b2);
    }

    @DexIgnore
    public static long b(byte[] bArr, long j) {
        return a.getLong(bArr, j);
    }

    @DexIgnore
    public static int a() {
        if (c) {
            return a.arrayBaseOffset(byte[].class);
        }
        return -1;
    }

    @DexIgnore
    public static long a(Field field) {
        Unsafe unsafe;
        if (field == null || (unsafe = a) == null) {
            return -1;
        }
        return unsafe.objectFieldOffset(field);
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }
}
