package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj6 implements ti6<wh6> {
    @DexIgnore
    public /* final */ CharSequence a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ ig6<CharSequence, Integer, lc6<Integer, Integer>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<wh6>, ph6 {
        @DexIgnore
        public int a; // = -1;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public wh6 d;
        @DexIgnore
        public int e;
        @DexIgnore
        public /* final */ /* synthetic */ fj6 f;

        @DexIgnore
        public a(fj6 fj6) {
            this.f = fj6;
            this.b = ci6.a(fj6.b, 0, fj6.a.length());
            this.c = this.b;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0023, code lost:
            if (r6.e < com.fossil.fj6.c(r6.f)) goto L_0x0025;
         */
        @DexIgnore
        public final void a() {
            int i = 0;
            if (this.c < 0) {
                this.a = 0;
                this.d = null;
                return;
            }
            if (this.f.c > 0) {
                this.e++;
            }
            if (this.c <= this.f.a.length()) {
                lc6 lc6 = (lc6) this.f.d.invoke(this.f.a, Integer.valueOf(this.c));
                if (lc6 == null) {
                    this.d = new wh6(this.b, yj6.c(this.f.a));
                    this.c = -1;
                } else {
                    int intValue = ((Number) lc6.component1()).intValue();
                    int intValue2 = ((Number) lc6.component2()).intValue();
                    this.d = ci6.d(this.b, intValue);
                    this.b = intValue + intValue2;
                    int i2 = this.b;
                    if (intValue2 == 0) {
                        i = 1;
                    }
                    this.c = i2 + i;
                }
                this.a = 1;
            }
            this.d = new wh6(this.b, yj6.c(this.f.a));
            this.c = -1;
            this.a = 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.a == -1) {
                a();
            }
            return this.a == 1;
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public wh6 next() {
            if (this.a == -1) {
                a();
            }
            if (this.a != 0) {
                wh6 wh6 = this.d;
                if (wh6 != null) {
                    this.d = null;
                    this.a = -1;
                    return wh6;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    public fj6(CharSequence charSequence, int i, int i2, ig6<? super CharSequence, ? super Integer, lc6<Integer, Integer>> ig6) {
        wg6.b(charSequence, "input");
        wg6.b(ig6, "getNextMatch");
        this.a = charSequence;
        this.b = i;
        this.c = i2;
        this.d = ig6;
    }

    @DexIgnore
    public Iterator<wh6> iterator() {
        return new a(this);
    }
}
