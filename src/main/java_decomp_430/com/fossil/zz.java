package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class zz<T extends View, Z> extends rz<Z> {
    @DexIgnore
    public static int f; // = cr.glide_custom_view_target_tag;
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public View.OnAttachStateChangeListener c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public zz(T t) {
        q00.a(t);
        this.a = (View) t;
        this.b = new a(t);
    }

    @DexIgnore
    public void a(xz xzVar) {
        this.b.b(xzVar);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        super.b(drawable);
        f();
    }

    @DexIgnore
    public void c(Drawable drawable) {
        super.c(drawable);
        this.b.b();
        if (!this.d) {
            g();
        }
    }

    @DexIgnore
    public jz d() {
        Object e2 = e();
        if (e2 == null) {
            return null;
        }
        if (e2 instanceof jz) {
            return (jz) e2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @DexIgnore
    public final Object e() {
        return this.a.getTag(f);
    }

    @DexIgnore
    public final void f() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.c;
        if (onAttachStateChangeListener != null && !this.e) {
            this.a.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.e = true;
        }
    }

    @DexIgnore
    public final void g() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.c;
        if (onAttachStateChangeListener != null && this.e) {
            this.a.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.e = false;
        }
    }

    @DexIgnore
    public String toString() {
        return "Target for: " + this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static Integer e;
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ List<xz> b; // = new ArrayList();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public C0060a d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.zz$a$a")
        /* renamed from: com.fossil.zz$a$a  reason: collision with other inner class name */
        public static final class C0060a implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public /* final */ WeakReference<a> a;

            @DexIgnore
            public C0060a(a aVar) {
                this.a = new WeakReference<>(aVar);
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                a aVar = (a) this.a.get();
                if (aVar == null) {
                    return true;
                }
                aVar.a();
                return true;
            }
        }

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public static int a(Context context) {
            if (e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                q00.a(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        @DexIgnore
        public final boolean a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        @DexIgnore
        public final void b(int i, int i2) {
            Iterator it = new ArrayList(this.b).iterator();
            while (it.hasNext()) {
                ((xz) it.next()).a(i, i2);
            }
        }

        @DexIgnore
        public final int c() {
            int paddingTop = this.a.getPaddingTop() + this.a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        @DexIgnore
        public final int d() {
            int paddingLeft = this.a.getPaddingLeft() + this.a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        @DexIgnore
        public void b(xz xzVar) {
            this.b.remove(xzVar);
        }

        @DexIgnore
        public void b() {
            ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        @DexIgnore
        public void a() {
            if (!this.b.isEmpty()) {
                int d2 = d();
                int c2 = c();
                if (a(d2, c2)) {
                    b(d2, c2);
                    b();
                }
            }
        }

        @DexIgnore
        public void a(xz xzVar) {
            int d2 = d();
            int c2 = c();
            if (a(d2, c2)) {
                xzVar.a(d2, c2);
                return;
            }
            if (!this.b.contains(xzVar)) {
                this.b.add(xzVar);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
                this.d = new C0060a(this);
                viewTreeObserver.addOnPreDrawListener(this.d);
            }
        }

        @DexIgnore
        public final boolean a(int i, int i2) {
            return a(i) && a(i2);
        }

        @DexIgnore
        public final int a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return a(this.a.getContext());
        }
    }

    @DexIgnore
    public void a(jz jzVar) {
        a((Object) jzVar);
    }

    @DexIgnore
    public final void a(Object obj) {
        this.a.setTag(f, obj);
    }

    @DexIgnore
    public void b(xz xzVar) {
        this.b.a(xzVar);
    }
}
