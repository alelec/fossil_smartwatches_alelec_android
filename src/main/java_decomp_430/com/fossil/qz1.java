package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.qw1;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz1<ResultT> extends ey1 {
    @DexIgnore
    public /* final */ bx1<rv1.b, ResultT> a;
    @DexIgnore
    public /* final */ rc3<ResultT> b;
    @DexIgnore
    public /* final */ zw1 c;

    @DexIgnore
    public qz1(int i, bx1<rv1.b, ResultT> bx1, rc3<ResultT> rc3, zw1 zw1) {
        super(i);
        this.b = rc3;
        this.a = bx1;
        this.c = zw1;
    }

    @DexIgnore
    public final void a(qw1.a<?> aVar) throws DeadObjectException {
        try {
            this.a.a(aVar.f(), this.b);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            a(az1.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    @DexIgnore
    public final iv1[] b(qw1.a<?> aVar) {
        return this.a.b();
    }

    @DexIgnore
    public final boolean c(qw1.a<?> aVar) {
        return this.a.a();
    }

    @DexIgnore
    public final void a(Status status) {
        this.b.b(this.c.a(status));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.b.b((Exception) runtimeException);
    }

    @DexIgnore
    public final void a(k02 k02, boolean z) {
        k02.a(this.b, z);
    }
}
