package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class en extends dn<ym> {
    @DexIgnore
    public en(Context context, to toVar) {
        super(pn.a(context, toVar).c());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.b() == ul.CONNECTED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(ym ymVar) {
        if (Build.VERSION.SDK_INT < 26) {
            return !ymVar.a();
        }
        if (!ymVar.a() || !ymVar.d()) {
            return true;
        }
        return false;
    }
}
