package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.fonts.FontVariationAxis;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.a7;
import com.fossil.j8;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k7 extends i7 {
    @DexIgnore
    public /* final */ Class g;
    @DexIgnore
    public /* final */ Constructor h;
    @DexIgnore
    public /* final */ Method i;
    @DexIgnore
    public /* final */ Method j;
    @DexIgnore
    public /* final */ Method k;
    @DexIgnore
    public /* final */ Method l;
    @DexIgnore
    public /* final */ Method m;

    @DexIgnore
    public k7() {
        Method method;
        Method method2;
        Method method3;
        Method method4;
        Constructor constructor;
        Method method5;
        Class cls = null;
        try {
            Class d = d();
            constructor = e(d);
            method4 = b(d);
            method3 = c(d);
            method2 = f(d);
            method = a(d);
            Class cls2 = d;
            method5 = d(d);
            cls = cls2;
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            Log.e("TypefaceCompatApi26Impl", "Unable to collect necessary methods for class " + e.getClass().getName(), e);
            method5 = null;
            constructor = null;
            method4 = null;
            method3 = null;
            method2 = null;
            method = null;
        }
        this.g = cls;
        this.h = constructor;
        this.i = method4;
        this.j = method3;
        this.k = method2;
        this.l = method;
        this.m = method5;
    }

    @DexIgnore
    private Object b() {
        try {
            return this.h.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            return null;
        }
    }

    @DexIgnore
    public final boolean a(Context context, Object obj, String str, int i2, int i3, int i4, FontVariationAxis[] fontVariationAxisArr) {
        try {
            return ((Boolean) this.i.invoke(obj, new Object[]{context.getAssets(), str, 0, false, Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), fontVariationAxisArr})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return false;
        }
    }

    @DexIgnore
    public final boolean c() {
        if (this.i == null) {
            Log.w("TypefaceCompatApi26Impl", "Unable to collect necessary private methods. Fallback to legacy implementation.");
        }
        return this.i != null;
    }

    @DexIgnore
    public Class d() throws ClassNotFoundException {
        return Class.forName("android.graphics.FontFamily");
    }

    @DexIgnore
    public Constructor e(Class cls) throws NoSuchMethodException {
        return cls.getConstructor(new Class[0]);
    }

    @DexIgnore
    public Method f(Class cls) throws NoSuchMethodException {
        return cls.getMethod("freeze", new Class[0]);
    }

    @DexIgnore
    public final void b(Object obj) {
        try {
            this.l.invoke(obj, new Object[0]);
        } catch (IllegalAccessException | InvocationTargetException unused) {
        }
    }

    @DexIgnore
    public Method d(Class cls) throws NoSuchMethodException {
        Class cls2 = Integer.TYPE;
        Method declaredMethod = Typeface.class.getDeclaredMethod("createFromFamiliesWithDefault", new Class[]{Array.newInstance(cls, 1).getClass(), cls2, cls2});
        declaredMethod.setAccessible(true);
        return declaredMethod;
    }

    @DexIgnore
    public Method b(Class cls) throws NoSuchMethodException {
        Class cls2 = Integer.TYPE;
        return cls.getMethod("addFontFromAssetManager", new Class[]{AssetManager.class, String.class, Integer.TYPE, Boolean.TYPE, cls2, cls2, cls2, FontVariationAxis[].class});
    }

    @DexIgnore
    public final boolean c(Object obj) {
        try {
            return ((Boolean) this.k.invoke(obj, new Object[0])).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return false;
        }
    }

    @DexIgnore
    public final boolean a(Object obj, ByteBuffer byteBuffer, int i2, int i3, int i4) {
        try {
            return ((Boolean) this.j.invoke(obj, new Object[]{byteBuffer, Integer.valueOf(i2), null, Integer.valueOf(i3), Integer.valueOf(i4)})).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return false;
        }
    }

    @DexIgnore
    public Method c(Class cls) throws NoSuchMethodException {
        Class cls2 = Integer.TYPE;
        return cls.getMethod("addFontFromBuffer", new Class[]{ByteBuffer.class, cls2, FontVariationAxis[].class, cls2, cls2});
    }

    @DexIgnore
    public Typeface a(Object obj) {
        try {
            Object newInstance = Array.newInstance(this.g, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) this.m.invoke((Object) null, new Object[]{newInstance, -1, -1});
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return null;
        }
    }

    @DexIgnore
    public Typeface a(Context context, a7.b bVar, Resources resources, int i2) {
        if (!c()) {
            return super.a(context, bVar, resources, i2);
        }
        Object b = b();
        if (b == null) {
            return null;
        }
        for (a7.c cVar : bVar.a()) {
            if (!a(context, b, cVar.a(), cVar.c(), cVar.e(), cVar.f() ? 1 : 0, FontVariationAxis.fromFontVariationSettings(cVar.d()))) {
                b(b);
                return null;
            }
        }
        if (!c(b)) {
            return null;
        }
        return a(b);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004b, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        if (r11 != null) goto L_0x004e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        throw r13;
     */
    @DexIgnore
    public Typeface a(Context context, CancellationSignal cancellationSignal, j8.f[] fVarArr, int i2) {
        Typeface a;
        if (fVarArr.length < 1) {
            return null;
        }
        if (!c()) {
            j8.f a2 = a(fVarArr, i2);
            try {
                ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(a2.c(), "r", cancellationSignal);
                if (openFileDescriptor == null) {
                    if (openFileDescriptor != null) {
                        openFileDescriptor.close();
                    }
                    return null;
                }
                Typeface build = new Typeface.Builder(openFileDescriptor.getFileDescriptor()).setWeight(a2.d()).setItalic(a2.e()).build();
                if (openFileDescriptor != null) {
                    openFileDescriptor.close();
                }
                return build;
            } catch (IOException unused) {
                return null;
            } catch (Throwable th) {
                r12.addSuppressed(th);
            }
        } else {
            Map<Uri, ByteBuffer> a3 = j8.a(context, fVarArr, cancellationSignal);
            Object b = b();
            if (b == null) {
                return null;
            }
            boolean z = false;
            for (j8.f fVar : fVarArr) {
                ByteBuffer byteBuffer = a3.get(fVar.c());
                if (byteBuffer != null) {
                    if (!a(b, byteBuffer, fVar.b(), fVar.d(), fVar.e() ? 1 : 0)) {
                        b(b);
                        return null;
                    }
                    z = true;
                }
            }
            if (!z) {
                b(b);
                return null;
            } else if (c(b) && (a = a(b)) != null) {
                return Typeface.create(a, i2);
            } else {
                return null;
            }
        }
    }

    @DexIgnore
    public Typeface a(Context context, Resources resources, int i2, String str, int i3) {
        if (!c()) {
            return super.a(context, resources, i2, str, i3);
        }
        Object b = b();
        if (b == null) {
            return null;
        }
        if (!a(context, b, str, 0, -1, -1, (FontVariationAxis[]) null)) {
            b(b);
            return null;
        } else if (!c(b)) {
            return null;
        } else {
            return a(b);
        }
    }

    @DexIgnore
    public Method a(Class cls) throws NoSuchMethodException {
        return cls.getMethod("abortCreation", new Class[0]);
    }
}
