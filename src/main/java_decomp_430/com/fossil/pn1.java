package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pn1 {
    PROPERTY_NOTIFY(16),
    PROPERTY_INDICATE(32);
    
    @DexIgnore
    public static /* final */ zl1 e; // = null;
    @DexIgnore
    public /* final */ int a;

    /*
    static {
        e = new zl1((qg6) null);
    }
    */

    @DexIgnore
    public pn1(int i) {
        this.a = i;
    }
}
