package com.fossil;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d03 extends s63 {
    @DexIgnore
    public long c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;
    @DexIgnore
    public AccountManager f;
    @DexIgnore
    public Boolean g;
    @DexIgnore
    public long h;

    @DexIgnore
    public d03(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final boolean a(Context context) {
        if (this.e == null) {
            d();
            this.e = false;
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    packageManager.getPackageInfo("com.google.android.gms", 128);
                    this.e = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return this.e.booleanValue();
    }

    @DexIgnore
    public final boolean q() {
        Calendar instance = Calendar.getInstance();
        this.c = TimeUnit.MINUTES.convert((long) (instance.get(15) + instance.get(16)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String lowerCase = locale.getLanguage().toLowerCase(Locale.ENGLISH);
        String lowerCase2 = locale.getCountry().toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder(String.valueOf(lowerCase).length() + 1 + String.valueOf(lowerCase2).length());
        sb.append(lowerCase);
        sb.append("-");
        sb.append(lowerCase2);
        this.d = sb.toString();
        return false;
    }

    @DexIgnore
    public final long s() {
        n();
        return this.c;
    }

    @DexIgnore
    public final String t() {
        n();
        return this.d;
    }

    @DexIgnore
    public final long u() {
        g();
        return this.h;
    }

    @DexIgnore
    public final void v() {
        g();
        this.g = null;
        this.h = 0;
    }

    @DexIgnore
    public final boolean w() {
        g();
        long b = zzm().b();
        if (b - this.h > 86400000) {
            this.g = null;
        }
        Boolean bool = this.g;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (w6.a(c(), "android.permission.GET_ACCOUNTS") != 0) {
            b().x().a("Permission error checking for dasher/unicorn accounts");
            this.h = b;
            this.g = false;
            return false;
        }
        if (this.f == null) {
            this.f = AccountManager.get(c());
        }
        try {
            Account[] result = this.f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_HOSTED"}, (AccountManagerCallback) null, (Handler) null).getResult();
            if (result == null || result.length <= 0) {
                Account[] result2 = this.f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_uca"}, (AccountManagerCallback) null, (Handler) null).getResult();
                if (result2 != null && result2.length > 0) {
                    this.g = true;
                    this.h = b;
                    return true;
                }
                this.h = b;
                this.g = false;
                return false;
            }
            this.g = true;
            this.h = b;
            return true;
        } catch (AuthenticatorException | OperationCanceledException | IOException e2) {
            b().u().a("Exception checking account types", e2);
        }
    }
}
