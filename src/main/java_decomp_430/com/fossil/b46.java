package com.fossil;

import android.content.Context;
import android.content.IntentFilter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b46 {
    @DexIgnore
    public static b46 i;
    @DexIgnore
    public List<String> a; // = null;
    @DexIgnore
    public volatile int b; // = 2;
    @DexIgnore
    public volatile String c; // = "";
    @DexIgnore
    public volatile HttpHost d; // = null;
    @DexIgnore
    public g56 e; // = null;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public Context g; // = null;
    @DexIgnore
    public b56 h; // = null;

    @DexIgnore
    public b46(Context context) {
        this.g = context.getApplicationContext();
        this.e = new g56();
        y56.a(context);
        this.h = m56.b();
        l();
        i();
        g();
    }

    @DexIgnore
    public static b46 a(Context context) {
        if (i == null) {
            synchronized (b46.class) {
                if (i == null) {
                    i = new b46(context);
                }
            }
        }
        return i;
    }

    @DexIgnore
    public HttpHost a() {
        return this.d;
    }

    @DexIgnore
    public void a(String str) {
        if (n36.q()) {
            this.h.e("updateIpList " + str);
        }
        try {
            if (m56.c(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.length() > 0) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String string = jSONObject.getString(keys.next());
                        if (m56.c(string)) {
                            for (String str2 : string.split(";")) {
                                if (m56.c(str2)) {
                                    String[] split = str2.split(":");
                                    if (split.length > 1) {
                                        String str3 = split[0];
                                        if (b(str3) && !this.a.contains(str3)) {
                                            if (n36.q()) {
                                                this.h.e("add new ip:" + str3);
                                            }
                                            this.a.add(str3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            this.h.a((Throwable) e2);
        }
        this.f = new Random().nextInt(this.a.size());
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public final boolean b(String str) {
        return Pattern.compile("(2[5][0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})").matcher(str).matches();
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public void d() {
        this.f = (this.f + 1) % this.a.size();
    }

    @DexIgnore
    public boolean e() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean f() {
        return this.b != 0;
    }

    @DexIgnore
    public void g() {
        if (r56.f(this.g)) {
            if (n36.v) {
                k();
            }
            this.c = m56.o(this.g);
            if (n36.q()) {
                b56 b56 = this.h;
                b56.e("NETWORK name:" + this.c);
            }
            if (m56.c(this.c)) {
                this.b = "WIFI".equalsIgnoreCase(this.c) ? 1 : 2;
                this.d = m56.d(this.g);
            }
            if (q36.a()) {
                q36.d(this.g);
                return;
            }
            return;
        }
        if (n36.q()) {
            this.h.e("NETWORK TYPE: network is close.");
        }
        l();
    }

    @DexIgnore
    public void h() {
        this.g.getApplicationContext().registerReceiver(new u46(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @DexIgnore
    public final void i() {
        this.a = new ArrayList(10);
        this.a.add("117.135.169.101");
        this.a.add("140.207.54.125");
        this.a.add("180.153.8.53");
        this.a.add("120.198.203.175");
        this.a.add("14.17.43.18");
        this.a.add("163.177.71.186");
        this.a.add("111.30.131.31");
        this.a.add("123.126.121.167");
        this.a.add("123.151.152.111");
        this.a.add("113.142.45.79");
        this.a.add("123.138.162.90");
        this.a.add("103.7.30.94");
    }

    @DexIgnore
    public final String j() {
        try {
            return !b("pingma.qq.com") ? InetAddress.getByName("pingma.qq.com").getHostAddress() : "";
        } catch (Exception e2) {
            this.h.a((Throwable) e2);
            return "";
        }
    }

    @DexIgnore
    public final void k() {
        String j = j();
        if (n36.q()) {
            b56 b56 = this.h;
            b56.e("remoteIp ip is " + j);
        }
        if (m56.c(j)) {
            if (!this.a.contains(j)) {
                String str = this.a.get(this.f);
                if (n36.q()) {
                    b56 b562 = this.h;
                    b562.g(j + " not in ip list, change to:" + str);
                }
                j = str;
            }
            n36.c("http://" + j + ":80/mstat/report");
        }
    }

    @DexIgnore
    public final void l() {
        this.b = 0;
        this.d = null;
        this.c = null;
    }
}
