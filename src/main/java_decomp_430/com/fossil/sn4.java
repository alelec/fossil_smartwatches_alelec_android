package com.fossil;

import com.portfolio.platform.helper.AlarmHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn4 implements MembersInjector<rn4> {
    @DexIgnore
    public static void a(rn4 rn4, an4 an4) {
        rn4.a = an4;
    }

    @DexIgnore
    public static void a(rn4 rn4, iw5 iw5) {
        rn4.b = iw5;
    }

    @DexIgnore
    public static void a(rn4 rn4, AlarmHelper alarmHelper) {
        rn4.c = alarmHelper;
    }
}
