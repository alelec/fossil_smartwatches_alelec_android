package com.fossil;

import android.text.TextUtils;
import com.fossil.m24;
import com.fossil.mz4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oy4$h$b implements m24.e<mz4.d, mz4.b> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter.h a;

    @DexIgnore
    public oy4$h$b(NotificationCallsAndMessagesPresenter.h hVar) {
        this.a = hVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(mz4.d dVar) {
        wg6.b(dVar, "responseValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = NotificationCallsAndMessagesPresenter.E.a();
        local.d(a2, "GetAllContactGroup onSuccess, size = " + dVar.a().size());
        this.a.this$0.t.m(yd6.d(dVar.a()));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String a3 = NotificationCallsAndMessagesPresenter.E.a();
        local2.d(a3, "mFlagGetListFavoriteFirstLoad=" + this.a.this$0.o());
        if (this.a.this$0.o()) {
            this.a.this$0.b((List<ContactGroup>) yd6.d(dVar.a()));
            for (ContactGroup contactGroup : dVar.a()) {
                List contacts = contactGroup.getContacts();
                wg6.a((Object) contacts, "contactGroup.contacts");
                if (!contacts.isEmpty()) {
                    Contact contact = (Contact) contactGroup.getContacts().get(0);
                    wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
                    wx4.setAdded(true);
                    Contact contact2 = wx4.getContact();
                    if (contact2 != null) {
                        wg6.a((Object) contact, "contact");
                        contact2.setDbRowId(contact.getDbRowId());
                    }
                    Contact contact3 = wx4.getContact();
                    if (contact3 != null) {
                        wg6.a((Object) contact, "contact");
                        contact3.setUseSms(contact.isUseSms());
                    }
                    Contact contact4 = wx4.getContact();
                    if (contact4 != null) {
                        wg6.a((Object) contact, "contact");
                        contact4.setUseCall(contact.isUseCall());
                    }
                    wg6.a((Object) contact, "contact");
                    List phoneNumbers = contact.getPhoneNumbers();
                    wg6.a((Object) phoneNumbers, "contact.phoneNumbers");
                    if (!phoneNumbers.isEmpty()) {
                        Object obj = contact.getPhoneNumbers().get(0);
                        wg6.a(obj, "contact.phoneNumbers[0]");
                        if (!TextUtils.isEmpty(((PhoneNumber) obj).getNumber())) {
                            wx4.setHasPhoneNumber(true);
                            Object obj2 = contact.getPhoneNumbers().get(0);
                            wg6.a(obj2, "contact.phoneNumbers[0]");
                            wx4.setPhoneNumber(((PhoneNumber) obj2).getNumber());
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = NotificationCallsAndMessagesPresenter.E.a();
                            StringBuilder sb = new StringBuilder();
                            sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                            Object obj3 = contact.getPhoneNumbers().get(0);
                            wg6.a(obj3, "contact.phoneNumbers[0]");
                            sb.append(((PhoneNumber) obj3).getNumber());
                            local3.d(a4, sb.toString());
                        }
                    }
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a5 = NotificationCallsAndMessagesPresenter.E.a();
                    local4.d(a5, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                    this.a.this$0.q().add(wx4);
                }
            }
            this.a.this$0.c(false);
        }
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String a6 = NotificationCallsAndMessagesPresenter.E.a();
        local5.d(a6, "start, mListFavoriteContactWrapperFirstLoad=" + this.a.this$0.q() + " size=" + this.a.this$0.q().size());
    }

    @DexIgnore
    public void a(mz4.b bVar) {
        wg6.b(bVar, "errorValue");
        FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetAllContactGroup onError");
    }
}
