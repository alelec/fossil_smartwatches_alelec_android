package com.fossil;

import android.os.Build;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dh extends ch {
    @DexIgnore
    public dh(long j, RenderScript renderScript) {
        super(j, renderScript);
    }

    @DexIgnore
    public static dh a(RenderScript renderScript, vg vgVar) {
        if (vgVar.a(vg.h(renderScript)) || vgVar.a(vg.g(renderScript))) {
            boolean z = renderScript.d() && Build.VERSION.SDK_INT < 19;
            dh dhVar = new dh(renderScript.a(5, vgVar.a(renderScript), z), renderScript);
            dhVar.a(z);
            dhVar.a(5.0f);
            return dhVar;
        }
        throw new yg("Unsupported element type.");
    }

    @DexIgnore
    public void b(tg tgVar) {
        if (tgVar.e().i() != 0) {
            a(0, (tg) null, tgVar, (wg) null);
            return;
        }
        throw new yg("Output is a 1D Allocation");
    }

    @DexIgnore
    public void c(tg tgVar) {
        if (tgVar.e().i() != 0) {
            a(1, (ug) tgVar);
            return;
        }
        throw new yg("Input set to a 1D Allocation");
    }

    @DexIgnore
    public void a(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f > 25.0f) {
            throw new yg("Radius out of range (0 < r <= 25).");
        }
        a(0, f);
    }
}
