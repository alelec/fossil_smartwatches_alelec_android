package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rx3 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof rx3) {
            rx3 rx3 = (rx3) obj;
            if (this.a == rx3.a && this.b == rx3.b) {
                return true;
            }
            return false;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 32713) + this.b;
    }

    @DexIgnore
    public String toString() {
        return this.a + "x" + this.b;
    }
}
