package com.fossil;

import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h01 extends xg6 implements hg6<cd6, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ em0 a;
    @DexIgnore
    public /* final */ /* synthetic */ ii1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h01(em0 em0, ii1 ii1) {
        super(1);
        this.a = em0;
        this.b = ii1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        cd6 cd6 = (cd6) obj;
        for (lr0 lr0 : ((zs0) this.a).c) {
            w31 a2 = w31.x.a(lr0.b);
            if (a2 != null) {
                switch (ny0.a[a2.ordinal()]) {
                    case 1:
                        if (lr0.a != bo0.GET) {
                            break;
                        } else {
                            ii1.b(this.b, false, true, (y11) null, 4);
                            break;
                        }
                    case 2:
                        ii1 ii1 = this.b;
                        g90 g90 = new g90(this.a.b, lr0.a);
                        q40.b v = ii1.v();
                        if (v == null) {
                            break;
                        } else {
                            v.onEventReceived(ii1, g90);
                            break;
                        }
                    case 3:
                        if (lr0.a != bo0.GET) {
                            break;
                        } else {
                            this.b.t();
                            break;
                        }
                    case 4:
                        ii1 ii12 = this.b;
                        q90 q90 = new q90(this.a.b, lr0.a);
                        q40.b v2 = ii12.v();
                        if (v2 == null) {
                            break;
                        } else {
                            v2.onEventReceived(ii12, q90);
                            break;
                        }
                    case 5:
                        ii1 ii13 = this.b;
                        m90 m90 = new m90(this.a.b, lr0.a);
                        q40.b v3 = ii13.v();
                        if (v3 == null) {
                            break;
                        } else {
                            v3.onEventReceived(ii13, m90);
                            break;
                        }
                    case 6:
                        if (lr0.a != bo0.GET) {
                            break;
                        } else {
                            ii1.a(this.b, false, true, (y11) null, 4);
                            break;
                        }
                }
            }
        }
        return cd6.a;
    }
}
