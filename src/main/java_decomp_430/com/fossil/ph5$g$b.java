package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ph5$g$b<T> implements ld<yx5<? extends List<ActivitySummary>>> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivityDetailPresenter.g a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1", f = "ActivityDetailPresenter.kt", l = {131}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ph5$g$b this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ph5$g$b$a$a")
        @lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1$summary$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.ph5$g$b$a$a  reason: collision with other inner class name */
        public static final class C0035a extends sf6 implements ig6<il6, xe6<? super ActivitySummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0035a(a aVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = aVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                C0035a aVar = new C0035a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((C0035a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    ActivityDetailPresenter activityDetailPresenter = this.this$0.this$0.a.this$0;
                    return activityDetailPresenter.b(activityDetailPresenter.g, (List<ActivitySummary>) this.this$0.this$0.a.this$0.k);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ph5$g$b ph5_g_b, xe6 xe6) {
            super(2, xe6);
            this.this$0 = ph5_g_b;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.a.this$0.b();
                C0035a aVar = new C0035a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, aVar, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ActivitySummary activitySummary = (ActivitySummary) obj;
            if (this.this$0.a.this$0.m == null || (!wg6.a((Object) this.this$0.a.this$0.m, (Object) activitySummary))) {
                this.this$0.a.this$0.m = activitySummary;
                this.this$0.a.this$0.s.a(this.this$0.a.this$0.o, this.this$0.a.this$0.m);
                if (this.this$0.a.this$0.i && this.this$0.a.this$0.j) {
                    rm6 unused = this.this$0.a.this$0.m();
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore
    public ph5$g$b(ActivityDetailPresenter.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<? extends List<ActivitySummary>> yx5) {
        wh4 a2 = yx5.a();
        List list = (List) yx5.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - summaryTransformations -- activitySummaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        sb.append(", status=");
        sb.append(a2);
        local.d("ActivityDetailPresenter", sb.toString());
        if (a2 == wh4.NETWORK_LOADING || a2 == wh4.SUCCESS) {
            this.a.this$0.k = list;
            this.a.this$0.i = true;
            rm6 unused = ik6.b(this.a.this$0.e(), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
        }
    }
}
