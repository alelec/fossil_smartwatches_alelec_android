package com.fossil;

import android.graphics.Color;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f7 {
    @DexIgnore
    public static /* final */ ThreadLocal<double[]> a; // = new ThreadLocal<>();

    @DexIgnore
    public static int a(int i, int i2) {
        return 255 - (((255 - i2) * (255 - i)) / 255);
    }

    @DexIgnore
    public static int b(int i, int i2) {
        int alpha = Color.alpha(i2);
        int alpha2 = Color.alpha(i);
        int a2 = a(alpha2, alpha);
        return Color.argb(a2, a(Color.red(i), alpha2, Color.red(i2), alpha, a2), a(Color.green(i), alpha2, Color.green(i2), alpha, a2), a(Color.blue(i), alpha2, Color.blue(i2), alpha, a2));
    }

    @DexIgnore
    public static int c(int i, int i2) {
        if (i2 >= 0 && i2 <= 255) {
            return (i & 16777215) | (i2 << 24);
        }
        throw new IllegalArgumentException("alpha must be between 0 and 255.");
    }

    @DexIgnore
    public static int a(int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            return 0;
        }
        return (((i * 255) * i2) + ((i3 * i4) * (255 - i2))) / (i5 * 255);
    }

    @DexIgnore
    public static double a(int i) {
        double[] a2 = a();
        a(i, a2);
        return a2[1] / 100.0d;
    }

    @DexIgnore
    public static void a(int i, double[] dArr) {
        a(Color.red(i), Color.green(i), Color.blue(i), dArr);
    }

    @DexIgnore
    public static void a(int i, int i2, int i3, double[] dArr) {
        double d;
        double d2;
        double d3;
        double[] dArr2 = dArr;
        if (dArr2.length == 3) {
            double d4 = ((double) i) / 255.0d;
            if (d4 < 0.04045d) {
                d = d4 / 12.92d;
            } else {
                d = Math.pow((d4 + 0.055d) / 1.055d, 2.4d);
            }
            double d5 = d;
            double d6 = ((double) i2) / 255.0d;
            if (d6 < 0.04045d) {
                d2 = d6 / 12.92d;
            } else {
                d2 = Math.pow((d6 + 0.055d) / 1.055d, 2.4d);
            }
            double d7 = d2;
            double d8 = ((double) i3) / 255.0d;
            if (d8 < 0.04045d) {
                d3 = d8 / 12.92d;
            } else {
                d3 = Math.pow((d8 + 0.055d) / 1.055d, 2.4d);
            }
            dArr2[0] = ((0.4124d * d5) + (0.3576d * d7) + (0.1805d * d3)) * 100.0d;
            dArr2[1] = ((0.2126d * d5) + (0.7152d * d7) + (0.0722d * d3)) * 100.0d;
            dArr2[2] = ((d5 * 0.0193d) + (d7 * 0.1192d) + (d3 * 0.9505d)) * 100.0d;
            return;
        }
        throw new IllegalArgumentException("outXyz must have a length of 3.");
    }

    @DexIgnore
    public static double[] a() {
        double[] dArr = a.get();
        if (dArr != null) {
            return dArr;
        }
        double[] dArr2 = new double[3];
        a.set(dArr2);
        return dArr2;
    }
}
