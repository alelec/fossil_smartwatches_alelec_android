package com.fossil;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface dg2 extends IInterface {
    @DexIgnore
    void a(bw2 bw2, fg2 fg2, String str) throws RemoteException;

    @DexIgnore
    void a(eh2 eh2) throws RemoteException;

    @DexIgnore
    void a(tg2 tg2) throws RemoteException;

    @DexIgnore
    void e(boolean z) throws RemoteException;

    @DexIgnore
    Location zza(String str) throws RemoteException;
}
