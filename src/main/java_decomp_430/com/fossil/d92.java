package com.fossil;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import com.fossil.p6;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.util.MissingFormatArgumentException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d92 {
    @DexIgnore
    public static d92 d;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public String b;
    @DexIgnore
    public /* final */ AtomicInteger c; // = new AtomicInteger((int) SystemClock.elapsedRealtime());

    @DexIgnore
    public d92(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static synchronized d92 a(Context context) {
        d92 d92;
        synchronized (d92.class) {
            if (d == null) {
                d = new d92(context);
            }
            d92 = d;
        }
        return d92;
    }

    @DexIgnore
    public static String b(Bundle bundle, String str) {
        String string = bundle.getString(str);
        return string == null ? bundle.getString(str.replace("gcm.n.", "gcm.notification.")) : string;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0255  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x027d  */
    public final boolean a(Bundle bundle) {
        int i;
        String b2;
        String b3;
        String str;
        Uri uri;
        String b4;
        PendingIntent pendingIntent;
        String b5;
        String b6;
        Intent intent;
        CharSequence a2 = a(bundle, "gcm.n.title");
        if (TextUtils.isEmpty(a2)) {
            a2 = this.a.getApplicationInfo().loadLabel(this.a.getPackageManager());
        }
        String a3 = a(bundle, "gcm.n.body");
        String b7 = b(bundle, "gcm.n.icon");
        if (!TextUtils.isEmpty(b7)) {
            Resources resources = this.a.getResources();
            i = resources.getIdentifier(b7, "drawable", this.a.getPackageName());
            if (i == 0 && (i = resources.getIdentifier(b7, "mipmap", this.a.getPackageName())) == 0) {
                StringBuilder sb = new StringBuilder(String.valueOf(b7).length() + 57);
                sb.append("Icon resource ");
                sb.append(b7);
                sb.append(" not found. Notification will use app icon.");
                Log.w("GcmNotification", sb.toString());
            }
            b2 = b(bundle, "gcm.n.color");
            b3 = b(bundle, "gcm.n.sound2");
            str = null;
            if (!TextUtils.isEmpty(b3)) {
                uri = null;
            } else if ("default".equals(b3) || this.a.getResources().getIdentifier(b3, OrmLiteConfigUtil.RAW_DIR_NAME, this.a.getPackageName()) == 0) {
                uri = RingtoneManager.getDefaultUri(2);
            } else {
                String packageName = this.a.getPackageName();
                StringBuilder sb2 = new StringBuilder(String.valueOf(packageName).length() + 24 + String.valueOf(b3).length());
                sb2.append("android.resource://");
                sb2.append(packageName);
                sb2.append("/raw/");
                sb2.append(b3);
                uri = Uri.parse(sb2.toString());
            }
            b4 = b(bundle, "gcm.n.click_action");
            if (TextUtils.isEmpty(b4)) {
                intent = new Intent(b4);
                intent.setPackage(this.a.getPackageName());
                intent.setFlags(268435456);
            } else {
                intent = this.a.getPackageManager().getLaunchIntentForPackage(this.a.getPackageName());
                if (intent == null) {
                    Log.w("GcmNotification", "No activity found to launch app");
                    pendingIntent = null;
                    b5 = b(bundle, "gcm.n.android_channel_id");
                    if (s42.i() && this.a.getApplicationInfo().targetSdkVersion >= 26) {
                        NotificationManager notificationManager = (NotificationManager) this.a.getSystemService(NotificationManager.class);
                        if (!TextUtils.isEmpty(b5)) {
                            if (notificationManager.getNotificationChannel(b5) == null) {
                                StringBuilder sb3 = new StringBuilder(String.valueOf(b5).length() + 122);
                                sb3.append("Notification Channel requested (");
                                sb3.append(b5);
                                sb3.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                                Log.w("GcmNotification", sb3.toString());
                            }
                            str = b5;
                        }
                        b5 = this.b;
                        if (b5 == null) {
                            this.b = a().getString("com.google.android.gms.gcm.default_notification_channel_id");
                            if (TextUtils.isEmpty(this.b)) {
                                Log.w("GcmNotification", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
                            } else if (notificationManager.getNotificationChannel(this.b) != null) {
                                str = this.b;
                            } else {
                                Log.w("GcmNotification", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
                            }
                            if (notificationManager.getNotificationChannel("fcm_fallback_notification_channel") == null) {
                                notificationManager.createNotificationChannel(new NotificationChannel("fcm_fallback_notification_channel", this.a.getString(b92.gcm_fallback_notification_channel_label), 3));
                            }
                            this.b = "fcm_fallback_notification_channel";
                            str = this.b;
                        }
                        str = b5;
                    }
                    p6.d dVar = new p6.d(this.a);
                    dVar.a(true);
                    dVar.e(i);
                    if (!TextUtils.isEmpty(a2)) {
                        dVar.b(a2);
                    }
                    if (!TextUtils.isEmpty(a3)) {
                        dVar.a((CharSequence) a3);
                        p6.c cVar = new p6.c();
                        cVar.a((CharSequence) a3);
                        dVar.a((p6.e) cVar);
                    }
                    if (!TextUtils.isEmpty(b2)) {
                        dVar.a(Color.parseColor(b2));
                    }
                    if (uri != null) {
                        dVar.a(uri);
                    }
                    if (pendingIntent != null) {
                        dVar.a(pendingIntent);
                    }
                    if (str != null) {
                        dVar.b(str);
                    }
                    Notification a4 = dVar.a();
                    b6 = b(bundle, "gcm.n.tag");
                    if (Log.isLoggable("GcmNotification", 3)) {
                        Log.d("GcmNotification", "Showing notification");
                    }
                    NotificationManager notificationManager2 = (NotificationManager) this.a.getSystemService("notification");
                    if (TextUtils.isEmpty(b6)) {
                        long uptimeMillis = SystemClock.uptimeMillis();
                        StringBuilder sb4 = new StringBuilder(37);
                        sb4.append("GCM-Notification:");
                        sb4.append(uptimeMillis);
                        b6 = sb4.toString();
                    }
                    notificationManager2.notify(b6, 0, a4);
                    return true;
                }
            }
            Bundle bundle2 = new Bundle(bundle);
            x82.a(bundle2);
            intent.putExtras(bundle2);
            for (String str2 : bundle2.keySet()) {
                if (str2.startsWith("gcm.n.") || str2.startsWith("gcm.notification.")) {
                    intent.removeExtra(str2);
                }
            }
            pendingIntent = PendingIntent.getActivity(this.a, this.c.getAndIncrement(), intent, 1073741824);
            b5 = b(bundle, "gcm.n.android_channel_id");
            NotificationManager notificationManager3 = (NotificationManager) this.a.getSystemService(NotificationManager.class);
            if (!TextUtils.isEmpty(b5)) {
            }
            b5 = this.b;
            if (b5 == null) {
            }
            str = b5;
            p6.d dVar2 = new p6.d(this.a);
            dVar2.a(true);
            dVar2.e(i);
            if (!TextUtils.isEmpty(a2)) {
            }
            if (!TextUtils.isEmpty(a3)) {
            }
            if (!TextUtils.isEmpty(b2)) {
            }
            if (uri != null) {
            }
            if (pendingIntent != null) {
            }
            if (str != null) {
            }
            Notification a42 = dVar2.a();
            b6 = b(bundle, "gcm.n.tag");
            if (Log.isLoggable("GcmNotification", 3)) {
            }
            NotificationManager notificationManager22 = (NotificationManager) this.a.getSystemService("notification");
            if (TextUtils.isEmpty(b6)) {
            }
            notificationManager22.notify(b6, 0, a42);
            return true;
        }
        int i2 = this.a.getApplicationInfo().icon;
        i = i2 == 0 ? 17301651 : i2;
        b2 = b(bundle, "gcm.n.color");
        b3 = b(bundle, "gcm.n.sound2");
        str = null;
        if (!TextUtils.isEmpty(b3)) {
        }
        b4 = b(bundle, "gcm.n.click_action");
        if (TextUtils.isEmpty(b4)) {
        }
        Bundle bundle22 = new Bundle(bundle);
        x82.a(bundle22);
        intent.putExtras(bundle22);
        while (r6.hasNext()) {
        }
        pendingIntent = PendingIntent.getActivity(this.a, this.c.getAndIncrement(), intent, 1073741824);
        b5 = b(bundle, "gcm.n.android_channel_id");
        NotificationManager notificationManager32 = (NotificationManager) this.a.getSystemService(NotificationManager.class);
        if (!TextUtils.isEmpty(b5)) {
        }
        b5 = this.b;
        if (b5 == null) {
        }
        str = b5;
        p6.d dVar22 = new p6.d(this.a);
        dVar22.a(true);
        dVar22.e(i);
        if (!TextUtils.isEmpty(a2)) {
        }
        if (!TextUtils.isEmpty(a3)) {
        }
        if (!TextUtils.isEmpty(b2)) {
        }
        if (uri != null) {
        }
        if (pendingIntent != null) {
        }
        if (str != null) {
        }
        Notification a422 = dVar22.a();
        b6 = b(bundle, "gcm.n.tag");
        if (Log.isLoggable("GcmNotification", 3)) {
        }
        NotificationManager notificationManager222 = (NotificationManager) this.a.getSystemService("notification");
        if (TextUtils.isEmpty(b6)) {
        }
        notificationManager222.notify(b6, 0, a422);
        return true;
    }

    @DexIgnore
    public final String a(Bundle bundle, String str) {
        String b2 = b(bundle, str);
        if (!TextUtils.isEmpty(b2)) {
            return b2;
        }
        String valueOf = String.valueOf(str);
        String b3 = b(bundle, "_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf));
        if (TextUtils.isEmpty(b3)) {
            return null;
        }
        Resources resources = this.a.getResources();
        int identifier = resources.getIdentifier(b3, LegacyTokenHelper.TYPE_STRING, this.a.getPackageName());
        if (identifier == 0) {
            String valueOf2 = String.valueOf(str);
            String substring = ("_loc_key".length() != 0 ? valueOf2.concat("_loc_key") : new String(valueOf2)).substring(6);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 49 + String.valueOf(b3).length());
            sb.append(substring);
            sb.append(" resource not found: ");
            sb.append(b3);
            sb.append(" Default value will be used.");
            Log.w("GcmNotification", sb.toString());
            return null;
        }
        String valueOf3 = String.valueOf(str);
        String b4 = b(bundle, "_loc_args".length() != 0 ? valueOf3.concat("_loc_args") : new String(valueOf3));
        if (TextUtils.isEmpty(b4)) {
            return resources.getString(identifier);
        }
        try {
            JSONArray jSONArray = new JSONArray(b4);
            Object[] objArr = new String[jSONArray.length()];
            for (int i = 0; i < objArr.length; i++) {
                objArr[i] = jSONArray.opt(i);
            }
            return resources.getString(identifier, objArr);
        } catch (JSONException unused) {
            String valueOf4 = String.valueOf(str);
            String substring2 = ("_loc_args".length() != 0 ? valueOf4.concat("_loc_args") : new String(valueOf4)).substring(6);
            StringBuilder sb2 = new StringBuilder(String.valueOf(substring2).length() + 41 + String.valueOf(b4).length());
            sb2.append("Malformed ");
            sb2.append(substring2);
            sb2.append(": ");
            sb2.append(b4);
            sb2.append("  Default value will be used.");
            Log.w("GcmNotification", sb2.toString());
            return null;
        } catch (MissingFormatArgumentException e) {
            StringBuilder sb3 = new StringBuilder(String.valueOf(b3).length() + 58 + String.valueOf(b4).length());
            sb3.append("Missing format argument for ");
            sb3.append(b3);
            sb3.append(": ");
            sb3.append(b4);
            sb3.append(" Default value will be used.");
            Log.w("GcmNotification", sb3.toString(), e);
            return null;
        }
    }

    @DexIgnore
    public final Bundle a() {
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = this.a.getPackageManager().getApplicationInfo(this.a.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException unused) {
            applicationInfo = null;
        }
        if (applicationInfo == null || applicationInfo.metaData == null) {
            return Bundle.EMPTY;
        }
        return applicationInfo.metaData;
    }
}
