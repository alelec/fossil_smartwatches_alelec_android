package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oo6 extends cn6 implements tl6 {
    @DexIgnore
    public /* final */ Throwable a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ oo6(Throwable th, String str, int i, qg6 qg6) {
        this(th, (i & 2) != 0 ? null : str);
    }

    @DexIgnore
    public boolean b(af6 af6) {
        wg6.b(af6, "context");
        p();
        throw null;
    }

    @DexIgnore
    public cn6 o() {
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0023, code lost:
        if (r1 != null) goto L_0x0028;
     */
    @DexIgnore
    public final Void p() {
        String str;
        if (this.a != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Module with the Main dispatcher had failed to initialize");
            String str2 = this.b;
            if (str2 != null) {
                str = ". " + str2;
            }
            str = "";
            sb.append(str);
            throw new IllegalStateException(sb.toString(), this.a);
        }
        throw new IllegalStateException("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android'");
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Main[missing");
        if (this.a != null) {
            str = ", cause=" + this.a;
        } else {
            str = "";
        }
        sb.append(str);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public oo6(Throwable th, String str) {
        this.a = th;
        this.b = str;
    }

    @DexIgnore
    public am6 a(long j, Runnable runnable) {
        wg6.b(runnable, "block");
        p();
        throw null;
    }

    @DexIgnore
    public Void a(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        p();
        throw null;
    }

    @DexIgnore
    public Void a(long j, lk6<? super cd6> lk6) {
        wg6.b(lk6, "continuation");
        p();
        throw null;
    }
}
