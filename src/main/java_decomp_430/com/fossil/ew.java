package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ew implements as<BitmapDrawable> {
    @DexIgnore
    public /* final */ au a;
    @DexIgnore
    public /* final */ as<Bitmap> b;

    @DexIgnore
    public ew(au auVar, as<Bitmap> asVar) {
        this.a = auVar;
        this.b = asVar;
    }

    @DexIgnore
    public boolean a(rt<BitmapDrawable> rtVar, File file, xr xrVar) {
        return this.b.a(new hw(rtVar.get().getBitmap(), this.a), file, xrVar);
    }

    @DexIgnore
    public rr a(xr xrVar) {
        return this.b.a(xrVar);
    }
}
