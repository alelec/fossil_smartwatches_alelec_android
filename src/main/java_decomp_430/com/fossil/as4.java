package com.fossil;

import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class as4 implements CoroutineUseCase.a {
    @DexIgnore
    public /* final */ yh4 a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public as4(yh4 yh4, ArrayList<Integer> arrayList) {
        wg6.b(yh4, "lastErrorCode");
        this.a = yh4;
        this.b = arrayList;
    }

    @DexIgnore
    public final yh4 a() {
        return this.a;
    }

    @DexIgnore
    public final ArrayList<Integer> b() {
        return this.b;
    }
}
