package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb3 implements Parcelable.Creator<xb3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        gv1 gv1 = null;
        int i = 0;
        y12 y12 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                gv1 = (gv1) f22.a(parcel, a, gv1.CREATOR);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                y12 = (y12) f22.a(parcel, a, y12.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new xb3(i, gv1, y12);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new xb3[i];
    }
}
