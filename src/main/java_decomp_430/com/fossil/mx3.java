package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mx3 {
    @DexIgnore
    public static /* final */ mx3 d; // = new mx3(0, new int[0], new Object[0], false);
    @DexIgnore
    public int a;
    @DexIgnore
    public int[] b;
    @DexIgnore
    public Object[] c;

    @DexIgnore
    public mx3() {
        this(0, new int[8], new Object[8], true);
    }

    @DexIgnore
    public static mx3 a(mx3 mx3, mx3 mx32) {
        int i = mx3.a + mx32.a;
        int[] copyOf = Arrays.copyOf(mx3.b, i);
        System.arraycopy(mx32.b, 0, copyOf, mx3.a, mx32.a);
        Object[] copyOf2 = Arrays.copyOf(mx3.c, i);
        System.arraycopy(mx32.c, 0, copyOf2, mx3.a, mx32.a);
        return new mx3(i, copyOf, copyOf2, true);
    }

    @DexIgnore
    public static mx3 b() {
        return d;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof mx3)) {
            return false;
        }
        mx3 mx3 = (mx3) obj;
        return this.a == mx3.a && Arrays.equals(this.b, mx3.b) && Arrays.deepEquals(this.c, mx3.c);
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.a) * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.deepHashCode(this.c);
    }

    @DexIgnore
    public mx3(int i, int[] iArr, Object[] objArr, boolean z) {
        this.a = i;
        this.b = iArr;
        this.c = objArr;
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            fx3.a(sb, i, String.valueOf(px3.a(this.b[i2])), this.c[i2]);
        }
    }
}
