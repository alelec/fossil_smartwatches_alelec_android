package com.fossil;

import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class yw5 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ AppNotificationFilterSettings a;
    @DexIgnore
    private /* final */ /* synthetic */ String b;

    @DexIgnore
    public /* synthetic */ yw5(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        this.a = appNotificationFilterSettings;
        this.b = str;
    }

    @DexIgnore
    public final void run() {
        PortfolioApp.T.a(this.a, this.b);
    }
}
