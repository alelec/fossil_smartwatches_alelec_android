package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol1 implements Parcelable.Creator<fn1> {
    @DexIgnore
    public /* synthetic */ ol1(qg6 qg6) {
    }

    @DexIgnore
    public fn1 createFromParcel(Parcel parcel) {
        return new fn1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new fn1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m50createFromParcel(Parcel parcel) {
        return new fn1(parcel, (qg6) null);
    }
}
