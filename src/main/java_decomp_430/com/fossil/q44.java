package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<SleepSessionHeartRate> {
    }

    @DexIgnore
    public final String a(SleepSessionHeartRate sleepSessionHeartRate) {
        if (sleepSessionHeartRate == null) {
            return null;
        }
        try {
            return yi4.a(sleepSessionHeartRate);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("SleepSessionHeartRateConverter.toString()", String.valueOf(cd6.a));
            return null;
        }
    }

    @DexIgnore
    public final SleepSessionHeartRate a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (SleepSessionHeartRate) new Gson().a(str, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("SleepSessionHeartRateConverter.toHeartRate()", String.valueOf(cd6.a));
            return null;
        }
    }
}
