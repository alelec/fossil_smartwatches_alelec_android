package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nw2 implements Parcelable.Creator<bw2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        boolean z = false;
        ArrayList<LocationRequest> arrayList = null;
        lw2 lw2 = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                arrayList = f22.c(parcel, a, LocationRequest.CREATOR);
            } else if (a2 == 2) {
                z = f22.i(parcel, a);
            } else if (a2 == 3) {
                z2 = f22.i(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                lw2 = (lw2) f22.a(parcel, a, lw2.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new bw2(arrayList, z, z2, lw2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new bw2[i];
    }
}
