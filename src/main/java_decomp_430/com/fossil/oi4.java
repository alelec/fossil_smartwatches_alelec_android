package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oi4 extends pi4 {
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public oi4(String str, int i) {
        super(str);
        this.b = i;
    }

    @DexIgnore
    public int b() {
        return this.b;
    }
}
