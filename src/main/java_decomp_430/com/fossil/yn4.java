package com.fossil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yn4 extends BaseDbProvider implements xn4 {
    @DexIgnore
    public static /* final */ String a; // = "yn4";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.yn4$a$a")
        /* renamed from: com.fossil.yn4$a$a  reason: collision with other inner class name */
        public class C0057a implements UpgradeCommand {
            @DexIgnore
            public C0057a(a aVar) {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                new b(sQLiteDatabase).execute(new Void[0]);
            }
        }

        @DexIgnore
        public a() {
            put(2, new C0057a(this));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public /* final */ SQLiteDatabase a;

        @DexIgnore
        public b(SQLiteDatabase sQLiteDatabase) {
            this.a = sQLiteDatabase;
        }

        @DexIgnore
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            try {
                FLogger.INSTANCE.getLocal().d(yn4.a, "Inside upgrade db from 1 to 2");
                Cursor query = this.a.query(true, "hourNotification", new String[]{"extraId", "createdAt", "hour", "isVibrationOnly"}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                List<wn4> arrayList = new ArrayList<>();
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex("extraId"));
                        String string2 = query.getString(query.getColumnIndex("createdAt"));
                        int i = query.getInt(query.getColumnIndex("hour"));
                        int i2 = query.getInt(query.getColumnIndex("isVibrationOnly"));
                        wn4 wn4 = new wn4();
                        wn4.b(string);
                        wn4.c(Long.valueOf(string2).longValue());
                        wn4.a(i);
                        boolean z = true;
                        if (i2 != 1) {
                            z = false;
                        }
                        wn4.a(z);
                        arrayList.add(wn4);
                        query.moveToNext();
                    }
                    query.close();
                }
                FLogger.INSTANCE.getLocal().d(yn4.a, "Inside upgrade db from 1 to 2, creating hour notification copy table");
                this.a.execSQL("CREATE TABLE hour_notification_copy (id VARCHAR PRIMARY KEY, extraId VARCHAR, hour INTEGER, createdAt VARCHAR, isVibrationOnly INTEGER, deviceFamily VARCHAR);");
                if (!arrayList.isEmpty()) {
                    arrayList = yn4.b(arrayList);
                }
                if (!arrayList.isEmpty()) {
                    for (wn4 wn42 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("extraId", wn42.c());
                        contentValues.put("hour", Integer.valueOf(wn42.d()));
                        contentValues.put("createdAt", Long.valueOf(wn42.a()));
                        contentValues.put("deviceFamily", wn42.b());
                        contentValues.put("isVibrationOnly", Boolean.valueOf(wn42.f()));
                        contentValues.put("id", wn42.e());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = yn4.a;
                        local.d(str, "Insert new values " + contentValues + " into copy table");
                        this.a.insert("hour_notification_copy", (String) null, contentValues);
                    }
                }
                this.a.execSQL("DROP TABLE hourNotification;");
                this.a.execSQL("ALTER TABLE hour_notification_copy RENAME TO hourNotification;");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = yn4.a;
                local2.e(str2, "Error inside " + yn4.a + ".upgrade - e=" + e);
            }
            return null;
        }
    }

    @DexIgnore
    public yn4(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public static wn4 a(String str, List<wn4> list) {
        for (wn4 next : list) {
            if (next.c().equalsIgnoreCase(str)) {
                return next;
            }
        }
        return null;
    }

    @DexIgnore
    public static List<wn4> b(List<wn4> list) {
        wn4 a2;
        ArrayList arrayList = new ArrayList();
        if (list != null && !list.isEmpty()) {
            for (MFDeviceFamily mFDeviceFamily : DeviceHelper.o.a()) {
                if (mFDeviceFamily != MFDeviceFamily.DEVICE_FAMILY_Q_MOTION) {
                    List<AppFilter> a3 = NotificationAppHelper.b.a(mFDeviceFamily);
                    List<ContactGroup> allContactGroups = zm4.p.a().b().getAllContactGroups(mFDeviceFamily.ordinal());
                    if (a3 != null && !a3.isEmpty()) {
                        for (AppFilter next : a3) {
                            wn4 a4 = a(next.getType(), list);
                            if (a4 != null) {
                                wn4 wn4 = new wn4(a4.d(), a4.f(), a4.c(), a4.b());
                                FLogger.INSTANCE.getLocal().d(a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + " Found hands setting of app filter=" + next.getType());
                                wn4.a(mFDeviceFamily.name());
                                StringBuilder sb = new StringBuilder();
                                sb.append(next.getType());
                                sb.append(mFDeviceFamily);
                                wn4.c(sb.toString());
                                arrayList.add(wn4);
                            }
                        }
                    }
                    if (allContactGroups != null && !allContactGroups.isEmpty()) {
                        for (ContactGroup contactGroup : allContactGroups) {
                            if (!(contactGroup.getContacts() == null || contactGroup.getContacts().isEmpty() || (a2 = a(String.valueOf(((Contact) contactGroup.getContacts().get(0)).getContactId()), list)) == null)) {
                                FLogger.INSTANCE.getLocal().d(a, "Migrating 1.10.3 ... Checking deviceFamily=" + mFDeviceFamily.name() + "Found hands setting of contactId=" + ((Contact) contactGroup.getContacts().get(0)).getContactId());
                                wn4 wn42 = new wn4(a2.d(), a2.f(), a2.c(), a2.b());
                                wn42.a(mFDeviceFamily.name());
                                wn42.c(((Contact) contactGroup.getContacts().get(0)).getContactId() + mFDeviceFamily.name());
                                arrayList.add(wn42);
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final wn4 d(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "getHourNotificationByExtraIdOnly() id = " + str);
        try {
            QueryBuilder<wn4, Integer> queryBuilder = g().queryBuilder();
            queryBuilder.where().eq("extraId", str);
            wn4 queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst != null) {
                return queryForFirst;
            }
            FLogger.INSTANCE.getLocal().d(a, "getHourNotificationByExtraIdOnly() - notification is null - return default notification for this action");
            return new wn4(1, false, str, DeviceHelper.o.a(PortfolioApp.T.e()).name());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local2.e(str3, "Error inside " + a + ".getHourNotificationByExtraIdOnly - e=" + e);
            return new wn4(1, false, str, DeviceHelper.o.a(PortfolioApp.T.e()).name());
        }
    }

    @DexIgnore
    public final Dao<wn4, Integer> g() throws SQLException {
        return this.databaseHelper.getDao(wn4.class);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{wn4.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new a();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public wn4 a(String str, String str2) {
        try {
            QueryBuilder<wn4, Integer> queryBuilder = g().queryBuilder();
            queryBuilder.where().eq("extraId", str).and().eq("deviceFamily", str2);
            wn4 queryForFirst = queryBuilder.queryForFirst();
            return queryForFirst == null ? new wn4(1, false, str, DeviceHelper.o.a(PortfolioApp.T.e()).name()) : queryForFirst;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a;
            local.e(str3, "Error inside " + a + ".getHourNotificationByExtraId - e=" + e);
            return d(str);
        }
    }
}
