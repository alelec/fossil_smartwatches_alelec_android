package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha3 implements Callable<String> {
    @DexIgnore
    public /* final */ /* synthetic */ ra3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ea3 b;

    @DexIgnore
    public ha3(ea3 ea3, ra3 ra3) {
        this.b = ea3;
        this.a = ra3;
    }

    @DexIgnore
    public final /* synthetic */ Object call() throws Exception {
        a63 c = this.b.c(this.a);
        if (c != null) {
            return c.m();
        }
        this.b.b().w().a("App info was null when attempting to get app instance id");
        return null;
    }
}
