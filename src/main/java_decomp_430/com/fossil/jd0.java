package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jd0 extends p40 implements Parcelable, Serializable {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore
    public jd0(String str, byte[] bArr) {
        this.b = bArr;
        this.c = h51.a.a(bArr, q11.CRC32);
        this.a = str.length() == 0 ? cw0.a((int) this.c) : str;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.NAME, (Object) this.a);
            cw0.a(jSONObject, bm0.FILE_SIZE, (Object) Integer.valueOf(this.b.length));
            cw0.a(jSONObject, bm0.FILE_CRC, (Object) Long.valueOf(this.c));
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final byte[] b() {
        if (!(!(this.b.length == 0))) {
            return new byte[0];
        }
        String str = this.a;
        Charset defaultCharset = Charset.defaultCharset();
        wg6.a(defaultCharset, "Charset.defaultCharset()");
        if (str != null) {
            byte[] bytes = str.getBytes(defaultCharset);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            byte[] a2 = md6.a(bytes, (byte) 0);
            int length = this.b.length;
            ByteBuffer allocate = ByteBuffer.allocate(a2.length + 2 + length);
            wg6.a(allocate, "ByteBuffer.allocate(totalLen)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            allocate.putShort((short) (a2.length + length));
            allocate.put(a2);
            allocate.put(this.b);
            byte[] array = allocate.array();
            wg6.a(array, "result.array()");
            return array;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final long c() {
        return this.c;
    }

    @DexIgnore
    public final Object d() {
        Object obj;
        if (e()) {
            obj = JSONObject.NULL;
        } else {
            obj = this.a;
        }
        wg6.a(obj, "if (isEmptyFile()) {\n   \u2026   fileName\n            }");
        return obj;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final boolean e() {
        return this.b.length == 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            l50 l50 = (l50) obj;
            if (!wg6.a(this.a, l50.getFileName()) || !Arrays.equals(this.b, l50.getFileData())) {
                return false;
            }
            return true;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImage");
    }

    @DexIgnore
    public final byte[] getFileData() {
        return this.b;
    }

    @DexIgnore
    public final String getFileName() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.b) + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.b);
        }
    }
}
