package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g83 extends z33 {
    @DexIgnore
    public h83 c;
    @DexIgnore
    public volatile h83 d;
    @DexIgnore
    public h83 e;
    @DexIgnore
    public /* final */ Map<Activity, h83> f; // = new p4();
    @DexIgnore
    public String g;

    @DexIgnore
    public g83(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final h83 A() {
        w();
        g();
        return this.c;
    }

    @DexIgnore
    public final h83 B() {
        e();
        return this.d;
    }

    @DexIgnore
    public final void a(Activity activity, String str, String str2) {
        if (this.d == null) {
            b().y().a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f.get(activity) == null) {
            b().y().a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = a(activity.getClass().getCanonicalName());
            }
            boolean equals = this.d.b.equals(str2);
            boolean d2 = ma3.d(this.d.a, str);
            if (equals && d2) {
                b().y().a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                b().y().a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                b().B().a("Setting current screen to name, class", str == null ? "null" : str, str2);
                h83 h83 = new h83(str, str2, j().s());
                this.f.put(activity, h83);
                a(activity, h83, true);
            } else {
                b().y().a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    @DexIgnore
    public final void b(Activity activity) {
        h83 d2 = d(activity);
        this.e = this.d;
        this.d = null;
        a().a((Runnable) new i83(this, d2));
    }

    @DexIgnore
    public final void c(Activity activity) {
        this.f.remove(activity);
    }

    @DexIgnore
    public final h83 d(Activity activity) {
        w12.a(activity);
        h83 h83 = this.f.get(activity);
        if (h83 != null) {
            return h83;
        }
        h83 h832 = new h83((String) null, a(activity.getClass().getCanonicalName()), j().s());
        this.f.put(activity, h832);
        return h832;
    }

    @DexIgnore
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final void b(Activity activity, Bundle bundle) {
        h83 h83;
        if (bundle != null && (h83 = this.f.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", h83.c);
            bundle2.putString("name", h83.a);
            bundle2.putString("referrer_name", h83.b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    @DexIgnore
    public final void a(Activity activity, h83 h83, boolean z) {
        h83 h832 = this.d == null ? this.e : this.d;
        if (h83.b == null) {
            h83 = new h83(h83.a, a(activity.getClass().getCanonicalName()), h83.c);
        }
        this.e = this.d;
        this.d = h83;
        a().a((Runnable) new j83(this, z, h832, h83));
    }

    @DexIgnore
    public final void a(h83 h83, boolean z) {
        n().a(zzm().c());
        if (t().a(h83.d, z)) {
            h83.d = false;
        }
    }

    @DexIgnore
    public static void a(h83 h83, Bundle bundle, boolean z) {
        if (bundle != null && h83 != null && (!bundle.containsKey("_sc") || z)) {
            String str = h83.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", h83.b);
            bundle.putLong("_si", h83.c);
        } else if (bundle != null && h83 == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    @DexIgnore
    public final void a(String str, h83 h83) {
        g();
        synchronized (this) {
            if (this.g == null || this.g.equals(str) || h83 != null) {
                this.g = str;
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    @DexIgnore
    public final void a(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.f.put(activity, new h83(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        a(activity, d(activity), false);
        w03 n = n();
        n.a().a((Runnable) new x13(n, n.zzm().c()));
    }
}
