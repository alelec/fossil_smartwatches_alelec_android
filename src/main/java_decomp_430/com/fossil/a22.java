package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import com.fossil.b62;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a22 extends b62<s12> {
    @DexIgnore
    public static /* final */ a22 c; // = new a22();

    @DexIgnore
    public a22() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    @DexIgnore
    public static View b(Context context, int i, int i2) throws b62.a {
        return c.a(context, i, i2);
    }

    @DexIgnore
    public final View a(Context context, int i, int i2) throws b62.a {
        try {
            z12 z12 = new z12(i, i2, (Scope[]) null);
            return (View) z52.e(((s12) a(context)).a(z52.a(context), z12));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new b62.a(sb.toString(), e);
        }
    }

    @DexIgnore
    public final s12 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        if (queryLocalInterface instanceof s12) {
            return (s12) queryLocalInterface;
        }
        return new b32(iBinder);
    }
}
