package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr4 implements Factory<rr4> {
    @DexIgnore
    public static /* final */ sr4 a; // = new sr4();

    @DexIgnore
    public static sr4 a() {
        return a;
    }

    @DexIgnore
    public static rr4 b() {
        return new rr4();
    }

    @DexIgnore
    public rr4 get() {
        return b();
    }
}
