package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pf6 extends gf6 {
    @DexIgnore
    public pf6(xe6<Object> xe6) {
        super(xe6);
        if (xe6 != null) {
            if (!(xe6.getContext() == bf6.INSTANCE)) {
                throw new IllegalArgumentException("Coroutines with restricted suspension must have EmptyCoroutineContext".toString());
            }
        }
    }

    @DexIgnore
    public af6 getContext() {
        return bf6.INSTANCE;
    }
}
