package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj6<T, R> implements ti6<R> {
    @DexIgnore
    public /* final */ ti6<T> a;
    @DexIgnore
    public /* final */ hg6<T, R> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<R>, ph6 {
        @DexIgnore
        public /* final */ Iterator<T> a;
        @DexIgnore
        public /* final */ /* synthetic */ bj6 b;

        @DexIgnore
        public a(bj6 bj6) {
            this.b = bj6;
            this.a = bj6.a.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        public R next() {
            return this.b.b.invoke(this.a.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    public bj6(ti6<? extends T> ti6, hg6<? super T, ? extends R> hg6) {
        wg6.b(ti6, "sequence");
        wg6.b(hg6, "transformer");
        this.a = ti6;
        this.b = hg6;
    }

    @DexIgnore
    public Iterator<R> iterator() {
        return new a(this);
    }
}
