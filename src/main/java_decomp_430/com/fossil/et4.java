package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class et4 implements Factory<dt4> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public et4(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static et4 a(Provider<UserRepository> provider) {
        return new et4(provider);
    }

    @DexIgnore
    public static dt4 b(Provider<UserRepository> provider) {
        return new dt4(provider.get());
    }

    @DexIgnore
    public dt4 get() {
        return b(this.a);
    }
}
