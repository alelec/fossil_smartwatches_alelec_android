package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im0 {
    @DexIgnore
    public /* synthetic */ im0(qg6 qg6) {
    }

    @DexIgnore
    public final ao0 a(int i) {
        ao0 ao0;
        ao0[] values = ao0.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                ao0 = null;
                break;
            }
            ao0 = values[i2];
            if (ao0.a == i) {
                break;
            }
            i2++;
        }
        return ao0 != null ? ao0 : ao0.UNKNOWN;
    }
}
