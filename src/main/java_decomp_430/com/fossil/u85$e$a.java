package com.fossil;

import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u85$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $errorCode$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $it;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $permissionErrorCodes$inlined;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetDianaPresetToWatchUseCase.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u85$e$a(DianaPreset dianaPreset, xe6 xe6, SetDianaPresetToWatchUseCase.e eVar, int i, ArrayList arrayList) {
        super(2, xe6);
        this.$it = dianaPreset;
        this.this$0 = eVar;
        this.$errorCode$inlined = i;
        this.$permissionErrorCodes$inlined = arrayList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        u85$e$a u85_e_a = new u85$e$a(this.$it, xe6, this.this$0, this.$errorCode$inlined, this.$permissionErrorCodes$inlined);
        u85_e_a.p$ = (il6) obj;
        return u85_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((u85$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v2, types: [com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase = SetDianaPresetToWatchUseCase.this;
            DianaPreset dianaPreset = this.$it;
            this.L$0 = il6;
            this.label = 1;
            if (setDianaPresetToWatchUseCase.a(dianaPreset, (xe6<? super cd6>) this) == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SetDianaPresetToWatchUseCase.this.a(new SetDianaPresetToWatchUseCase.b(this.$errorCode$inlined, this.$permissionErrorCodes$inlined));
        return cd6.a;
    }
}
