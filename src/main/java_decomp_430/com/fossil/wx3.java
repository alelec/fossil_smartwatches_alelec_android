package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wx3 extends Exception {
    @DexIgnore
    public static /* final */ StackTraceElement[] NO_TRACE; // = new StackTraceElement[0];
    @DexIgnore
    public static /* final */ boolean isStackTrace; // = (System.getProperty("surefire.test.class.path") != null);

    @DexIgnore
    public wx3() {
    }

    @DexIgnore
    public final synchronized Throwable fillInStackTrace() {
        return null;
    }

    @DexIgnore
    public wx3(Throwable th) {
        super(th);
    }
}
