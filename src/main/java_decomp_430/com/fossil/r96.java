package com.fossil;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface r96<T> {
    @DexIgnore
    void a(T t);

    @DexIgnore
    boolean b();

    @DexIgnore
    Collection<T> c();
}
