package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.fossil.wq;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yq extends ContextWrapper {
    @DexIgnore
    public static /* final */ gr<?, ?> k; // = new vq();
    @DexIgnore
    public /* final */ xt a;
    @DexIgnore
    public /* final */ dr b;
    @DexIgnore
    public /* final */ wz c;
    @DexIgnore
    public /* final */ wq.a d;
    @DexIgnore
    public /* final */ List<mz<Object>> e;
    @DexIgnore
    public /* final */ Map<Class<?>, gr<?, ?>> f;
    @DexIgnore
    public /* final */ gt g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public nz j;

    @DexIgnore
    public yq(Context context, xt xtVar, dr drVar, wz wzVar, wq.a aVar, Map<Class<?>, gr<?, ?>> map, List<mz<Object>> list, gt gtVar, boolean z, int i2) {
        super(context.getApplicationContext());
        this.a = xtVar;
        this.b = drVar;
        this.c = wzVar;
        this.d = aVar;
        this.e = list;
        this.f = map;
        this.g = gtVar;
        this.h = z;
        this.i = i2;
    }

    @DexIgnore
    public <T> gr<?, T> a(Class<T> cls) {
        gr<?, T> grVar = this.f.get(cls);
        if (grVar == null) {
            for (Map.Entry next : this.f.entrySet()) {
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    grVar = (gr) next.getValue();
                }
            }
        }
        return grVar == null ? k : grVar;
    }

    @DexIgnore
    public List<mz<Object>> b() {
        return this.e;
    }

    @DexIgnore
    public synchronized nz c() {
        if (this.j == null) {
            this.j = (nz) this.d.build().J();
        }
        return this.j;
    }

    @DexIgnore
    public gt d() {
        return this.g;
    }

    @DexIgnore
    public int e() {
        return this.i;
    }

    @DexIgnore
    public dr f() {
        return this.b;
    }

    @DexIgnore
    public boolean g() {
        return this.h;
    }

    @DexIgnore
    public <X> zz<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    @DexIgnore
    public xt a() {
        return this.a;
    }
}
