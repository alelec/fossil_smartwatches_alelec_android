package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.facebook.internal.ServerProtocol;
import com.facebook.login.LoginStatusClient;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ov2 {
    @DexIgnore
    public static volatile ov2 h; // = null;
    @DexIgnore
    public static Boolean i; // = null;
    @DexIgnore
    public static Boolean j; // = null;
    @DexIgnore
    public static boolean k; // = false;
    @DexIgnore
    public static Boolean l; // = null;
    @DexIgnore
    public static String m; // = "use_dynamite_api";
    @DexIgnore
    public static String n; // = "allow_remote_dynamite";
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ k42 b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public List<Pair<c73, b>> d;
    @DexIgnore
    public int e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public eu2 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class a implements Runnable {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public a(ov2 ov2) {
            this(true);
        }

        @DexIgnore
        public abstract void a() throws RemoteException;

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void run() {
            if (ov2.this.f) {
                b();
                return;
            }
            try {
                a();
            } catch (Exception e) {
                ov2.this.a(e, false, this.c);
                b();
            }
        }

        @DexIgnore
        public a(boolean z) {
            this.a = ov2.this.b.b();
            this.b = ov2.this.b.c();
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends iv2 {
        @DexIgnore
        public /* final */ c73 a;

        @DexIgnore
        public b(c73 c73) {
            this.a = c73;
        }

        @DexIgnore
        public final void a(String str, String str2, Bundle bundle, long j) {
            this.a.onEvent(str, str2, bundle, j);
        }

        @DexIgnore
        public final int zza() {
            return System.identityHashCode(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
            ov2.this.a((a) new oi2(this, activity, bundle));
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
            ov2.this.a((a) new ti2(this, activity));
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            ov2.this.a((a) new si2(this, activity));
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            ov2.this.a((a) new pi2(this, activity));
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            cs2 cs2 = new cs2();
            ov2.this.a((a) new ui2(this, activity, cs2));
            Bundle c = cs2.c(50);
            if (c != null) {
                bundle.putAll(c);
            }
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
            ov2.this.a((a) new qi2(this, activity));
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
            ov2.this.a((a) new ri2(this, activity));
        }
    }

    @DexIgnore
    public ov2(Context context, String str, String str2, String str3, Bundle bundle) {
        if (str == null || !c(str2, str3)) {
            this.a = "FA";
        } else {
            this.a = str;
        }
        this.b = n42.d();
        this.c = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue());
        new uz2(this);
        boolean z = false;
        if (!(!f(context) || g())) {
            this.f = true;
            Log.w(this.a, "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection.");
            return;
        }
        if (!c(str2, str3)) {
            if (str2 == null || str3 == null) {
                if ((str2 == null) ^ (str3 == null ? true : z)) {
                    Log.w(this.a, "Specified origin or custom app id is null. Both parameters will be ignored.");
                }
            } else {
                Log.v(this.a, "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy");
                this.f = true;
                return;
            }
        }
        a((a) new th2(this, str2, str3, context, bundle));
        Application application = (Application) context.getApplicationContext();
        if (application == null) {
            Log.w(this.a, "Unable to register lifecycle notifications. Application null.");
        } else {
            application.registerActivityLifecycleCallbacks(new c());
        }
    }

    @DexIgnore
    public static ov2 a(Context context) {
        return a(context, (String) null, (String) null, (String) null, (Bundle) null);
    }

    @DexIgnore
    public static boolean c(String str, String str2) {
        return (str2 == null || str == null || g()) ? false : true;
    }

    @DexIgnore
    public static boolean f(Context context) {
        try {
            rw1.a(context);
            if (rw1.a() != null) {
                return true;
            }
            return false;
        } catch (IllegalStateException unused) {
        }
    }

    @DexIgnore
    public static int g(Context context) {
        return DynamiteModule.b(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static int h(Context context) {
        return DynamiteModule.a(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static void i(Context context) {
        synchronized (ov2.class) {
            try {
                if (i != null && j != null) {
                    return;
                }
                if (a(context, "app_measurement_internal_disable_startup_flags")) {
                    i = false;
                    j = false;
                    return;
                }
                SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
                i = Boolean.valueOf(sharedPreferences.getBoolean(m, false));
                j = Boolean.valueOf(sharedPreferences.getBoolean(n, false));
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.remove(m);
                edit.remove(n);
                edit.apply();
            } catch (Exception e2) {
                Log.e("FA", "Exception reading flag from SharedPreferences.", e2);
                i = false;
                j = false;
            }
        }
    }

    @DexIgnore
    public final void b(String str, String str2, Bundle bundle) {
        a((a) new qv2(this, str, str2, bundle));
    }

    @DexIgnore
    public final String d() {
        cs2 cs2 = new cs2();
        a((a) new ci2(this, cs2));
        return cs2.b(500);
    }

    @DexIgnore
    public final String e() {
        cs2 cs2 = new cs2();
        a((a) new fi2(this, cs2));
        return cs2.b(500);
    }

    @DexIgnore
    public static ov2 a(Context context, String str, String str2, String str3, Bundle bundle) {
        w12.a(context);
        if (h == null) {
            synchronized (ov2.class) {
                if (h == null) {
                    h = new ov2(context, str, str2, str3, bundle);
                }
            }
        }
        return h;
    }

    @DexIgnore
    public static boolean g() {
        try {
            Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public final List<Bundle> b(String str, String str2) {
        cs2 cs2 = new cs2();
        a((a) new vh2(this, str, str2, cs2));
        List<Bundle> list = (List) cs2.a(cs2.c(LoginStatusClient.DEFAULT_TOAST_DURATION_MS), List.class);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public final void c(String str) {
        a((a) new zh2(this, str));
    }

    @DexIgnore
    public final long c() {
        cs2 cs2 = new cs2();
        a((a) new di2(this, cs2));
        Long l2 = (Long) cs2.a(cs2.c(500), Long.class);
        if (l2 != null) {
            return l2.longValue();
        }
        long nextLong = new Random(System.nanoTime() ^ this.b.b()).nextLong();
        int i2 = this.e + 1;
        this.e = i2;
        return nextLong + ((long) i2);
    }

    @DexIgnore
    public final int d(String str) {
        cs2 cs2 = new cs2();
        a((a) new hi2(this, str, cs2));
        Integer num = (Integer) cs2.a(cs2.c(10000), Integer.class);
        if (num == null) {
            return 25;
        }
        return num.intValue();
    }

    @DexIgnore
    public final void b(String str) {
        a((a) new yh2(this, str));
    }

    @DexIgnore
    public final String b() {
        cs2 cs2 = new cs2();
        a((a) new ai2(this, cs2));
        return cs2.b(50);
    }

    @DexIgnore
    public final void a(a aVar) {
        this.c.execute(aVar);
    }

    @DexIgnore
    public final eu2 a(Context context, boolean z) {
        DynamiteModule.b bVar;
        if (z) {
            try {
                bVar = DynamiteModule.l;
            } catch (DynamiteModule.a e2) {
                a((Exception) e2, true, false);
                return null;
            }
        } else {
            bVar = DynamiteModule.j;
        }
        return dt2.asInterface(DynamiteModule.a(context, bVar, ModuleDescriptor.MODULE_ID).a("com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"));
    }

    @DexIgnore
    public final void b(boolean z) {
        a((a) new ji2(this, z));
    }

    @DexIgnore
    public static boolean b(Context context) {
        i(context);
        synchronized (ov2.class) {
            if (!k) {
                try {
                    String str = (String) Class.forName("android.os.SystemProperties").getMethod("get", new Class[]{String.class, String.class}).invoke((Object) null, new Object[]{"measurement.dynamite.enabled", ""});
                    if (ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(str)) {
                        l = true;
                    } else if ("false".equals(str)) {
                        l = false;
                    } else {
                        l = null;
                    }
                    k = true;
                } catch (Exception e2) {
                    try {
                        Log.e("FA", "Unable to call SystemProperties.get()", e2);
                        l = null;
                    } finally {
                        k = true;
                    }
                }
            }
        }
        Boolean bool = l;
        if (bool == null) {
            bool = i;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final void a(Exception exc, boolean z, boolean z2) {
        this.f |= z;
        if (z) {
            Log.w(this.a, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z2) {
            a(5, "Error with data collection. Data lost.", (Object) exc, (Object) null, (Object) null);
        }
        Log.w(this.a, "Error with data collection. Data lost.", exc);
    }

    @DexIgnore
    public final void a(c73 c73) {
        w12.a(c73);
        a((a) new ii2(this, c73));
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        a((String) null, str, bundle, false, true, (Long) null);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, true, (Long) null);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, Long l2) {
        a((a) new mi2(this, l2, str, str2, bundle, z, z2));
    }

    @DexIgnore
    public final void a(String str, String str2) {
        a((String) null, str, (Object) str2, false);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj) {
        a(str, str2, obj, true);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z) {
        a((a) new ki2(this, str, str2, obj, z));
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        a((a) new ni2(this, bundle));
    }

    @DexIgnore
    public final void a(String str) {
        a((a) new uh2(this, str));
    }

    @DexIgnore
    public final void a(Activity activity, String str, String str2) {
        a((a) new xh2(this, activity, str, str2));
    }

    @DexIgnore
    public final void a(boolean z) {
        a((a) new wh2(this, z));
    }

    @DexIgnore
    public final String a() {
        cs2 cs2 = new cs2();
        a((a) new bi2(this, cs2));
        return cs2.b(500);
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, boolean z) {
        cs2 cs2 = new cs2();
        a((a) new ei2(this, str, str2, z, cs2));
        Bundle c2 = cs2.c(LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
        if (c2 == null || c2.size() == 0) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(c2.size());
        for (String str3 : c2.keySet()) {
            Object obj = c2.get(str3);
            if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                hashMap.put(str3, obj);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void a(int i2, String str, Object obj, Object obj2, Object obj3) {
        a((a) new gi2(this, false, 5, str, obj, (Object) null, (Object) null));
    }

    @DexIgnore
    public static boolean a(Context context, String str) {
        w12.b(str);
        try {
            ApplicationInfo a2 = g52.b(context).a(context.getPackageName(), 128);
            if (a2 != null) {
                if (a2.metaData != null) {
                    return a2.metaData.getBoolean(str);
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }
}
