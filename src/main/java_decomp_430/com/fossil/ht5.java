package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht5 {
    @DexIgnore
    public /* final */ gt5 a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public ht5(gt5 gt5, ArrayList<Integer> arrayList) {
        wg6.b(gt5, "mView");
        this.a = gt5;
        this.b = arrayList;
    }

    @DexIgnore
    public final ArrayList<Integer> a() {
        ArrayList<Integer> arrayList = this.b;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final gt5 b() {
        return this.a;
    }
}
