package com.fossil;

import com.fossil.NotificationAppsPresenter;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$listNotificationSettings$1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class dy4$c$f extends sf6 implements ig6<il6, xe6<? super List<? extends NotificationSettingsModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dy4$c$f(NotificationAppsPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        dy4$c$f dy4_c_f = new dy4$c$f(this.this$0, xe6);
        dy4_c_f.p$ = (il6) obj;
        return dy4_c_f;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((dy4$c$f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.q.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
