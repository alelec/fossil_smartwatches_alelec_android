package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r44 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public r44() {
        du3 du3 = new du3();
        du3.a(Date.class, new GsonConverterShortDate());
        Gson a2 = du3.a();
        wg6.a((Object) a2, "GsonBuilder().registerTy\u2026rterShortDate()).create()");
        this.a = a2;
    }

    @DexIgnore
    public final String a(SleepStatistic.SleepDailyBest sleepDailyBest) {
        if (sleepDailyBest == null) {
            return null;
        }
        try {
            return this.a.a(sleepDailyBest);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepStatisticConverter", "fromSleepDailyBest - e=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final SleepStatistic.SleepDailyBest a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (SleepStatistic.SleepDailyBest) this.a.a(str, SleepStatistic.SleepDailyBest.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepStatisticConverter", "toSleepDailyBest - e=" + e);
            e.printStackTrace();
            return null;
        }
    }
}
