package com.fossil;

import androidx.lifecycle.ViewModelStore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wd {
    @DexIgnore
    ViewModelStore getViewModelStore();
}
