package com.fossil;

import com.fossil.sq6;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ls6 implements wr6 {
    @DexIgnore
    public static /* final */ List<String> f; // = fr6.a((T[]) new String[]{"connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority"});
    @DexIgnore
    public static /* final */ List<String> g; // = fr6.a((T[]) new String[]{"connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade"});
    @DexIgnore
    public /* final */ Interceptor.Chain a;
    @DexIgnore
    public /* final */ tr6 b;
    @DexIgnore
    public /* final */ ms6 c;
    @DexIgnore
    public os6 d;
    @DexIgnore
    public /* final */ wq6 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ot6 {
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public long c; // = 0;

        @DexIgnore
        public a(zt6 zt6) {
            super(zt6);
        }

        @DexIgnore
        public final void a(IOException iOException) {
            if (!this.b) {
                this.b = true;
                ls6 ls6 = ls6.this;
                ls6.b.a(false, ls6, this.c, iOException);
            }
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            try {
                long b2 = c().b(jt6, j);
                if (b2 > 0) {
                    this.c += b2;
                }
                return b2;
            } catch (IOException e) {
                a(e);
                throw e;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            super.close();
            a((IOException) null);
        }
    }

    @DexIgnore
    public ls6(OkHttpClient okHttpClient, Interceptor.Chain chain, tr6 tr6, ms6 ms6) {
        wq6 wq6;
        this.a = chain;
        this.b = tr6;
        this.c = ms6;
        if (okHttpClient.r().contains(wq6.H2_PRIOR_KNOWLEDGE)) {
            wq6 = wq6.H2_PRIOR_KNOWLEDGE;
        } else {
            wq6 = wq6.HTTP_2;
        }
        this.e = wq6;
    }

    @DexIgnore
    public yt6 a(yq6 yq6, long j) {
        return this.d.d();
    }

    @DexIgnore
    public void b() throws IOException {
        this.c.flush();
    }

    @DexIgnore
    public void cancel() {
        os6 os6 = this.d;
        if (os6 != null) {
            os6.c(hs6.CANCEL);
        }
    }

    @DexIgnore
    public static List<is6> b(yq6 yq6) {
        sq6 c2 = yq6.c();
        ArrayList arrayList = new ArrayList(c2.b() + 4);
        arrayList.add(new is6(is6.f, yq6.e()));
        arrayList.add(new is6(is6.g, cs6.a(yq6.g())));
        String a2 = yq6.a("Host");
        if (a2 != null) {
            arrayList.add(new is6(is6.i, a2));
        }
        arrayList.add(new is6(is6.h, yq6.g().n()));
        int b2 = c2.b();
        for (int i = 0; i < b2; i++) {
            mt6 encodeUtf8 = mt6.encodeUtf8(c2.a(i).toLowerCase(Locale.US));
            if (!f.contains(encodeUtf8.utf8())) {
                arrayList.add(new is6(encodeUtf8, c2.b(i)));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void a(yq6 yq6) throws IOException {
        if (this.d == null) {
            this.d = this.c.a(b(yq6), yq6.a() != null);
            this.d.h().a((long) this.a.a(), TimeUnit.MILLISECONDS);
            this.d.l().a((long) this.a.b(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    public void a() throws IOException {
        this.d.d().close();
    }

    @DexIgnore
    public Response.a a(boolean z) throws IOException {
        Response.a a2 = a(this.d.j(), this.e);
        if (!z || dr6.a.a(a2) != 100) {
            return a2;
        }
        return null;
    }

    @DexIgnore
    public static Response.a a(sq6 sq6, wq6 wq6) throws IOException {
        sq6.a aVar = new sq6.a();
        int b2 = sq6.b();
        es6 es6 = null;
        for (int i = 0; i < b2; i++) {
            String a2 = sq6.a(i);
            String b3 = sq6.b(i);
            if (a2.equals(":status")) {
                es6 = es6.a("HTTP/1.1 " + b3);
            } else if (!g.contains(a2)) {
                dr6.a.a(aVar, a2, b3);
            }
        }
        if (es6 != null) {
            Response.a aVar2 = new Response.a();
            aVar2.a(wq6);
            aVar2.a(es6.b);
            aVar2.a(es6.c);
            aVar2.a(aVar.a());
            return aVar2;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    public zq6 a(Response response) throws IOException {
        tr6 tr6 = this.b;
        tr6.f.e(tr6.e);
        return new bs6(response.e("Content-Type"), yr6.a(response), st6.a((zt6) new a(this.d.e())));
    }
}
