package com.fossil;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.fossil.p6;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht3 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ot3 c;

    @DexIgnore
    public ht3(Context context, ot3 ot3, Executor executor) {
        this.a = executor;
        this.b = context;
        this.c = ot3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c  */
    public final boolean a() {
        boolean z;
        if (this.c.b("gcm.n.noui")) {
            return true;
        }
        if (!((KeyguardManager) this.b.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            if (!s42.g()) {
                SystemClock.sleep(10);
            }
            int myPid = Process.myPid();
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.b.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses != null) {
                Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ActivityManager.RunningAppProcessInfo next = it.next();
                    if (next.pid == myPid) {
                        if (next.importance == 100) {
                            z = true;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
                nt3 e = nt3.e(this.c.a("gcm.n.image"));
                if (e != null) {
                    e.a(this.a);
                }
                et3 a2 = ft3.a(this.b, this.c);
                p6.d dVar = a2.a;
                if (e != null) {
                    try {
                        Bitmap bitmap = (Bitmap) tc3.a(e.k(), 5, TimeUnit.SECONDS);
                        dVar.b(bitmap);
                        p6.b bVar = new p6.b();
                        bVar.b(bitmap);
                        bVar.a((Bitmap) null);
                        dVar.a((p6.e) bVar);
                    } catch (ExecutionException e2) {
                        String valueOf = String.valueOf(e2.getCause());
                        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 26);
                        sb.append("Failed to download image: ");
                        sb.append(valueOf);
                        Log.w("FirebaseMessaging", sb.toString());
                    } catch (InterruptedException unused) {
                        Log.w("FirebaseMessaging", "Interrupted while downloading image, showing notification without it");
                        e.close();
                        Thread.currentThread().interrupt();
                    } catch (TimeoutException unused2) {
                        Log.w("FirebaseMessaging", "Failed to download image in time, showing notification without it");
                        e.close();
                    }
                }
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "Showing notification");
                }
                ((NotificationManager) this.b.getSystemService("notification")).notify(a2.b, 0, a2.a.a());
                return true;
            }
        }
        z = false;
        if (!z) {
        }
    }
}
