package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv4 extends RecyclerView.g<c> implements Filterable {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ a e; // = new a((qg6) null);
    @DexIgnore
    public List<vx4> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public List<vx4> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gv4.d;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ tf4 a;
        @DexIgnore
        public /* final */ /* synthetic */ gv4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.a.b.c;
                    if (b == null) {
                        wg6.a();
                        throw null;
                    } else if (((AppWrapper) b.get(adapterPosition)).getUri() != null) {
                        List b2 = this.a.b.c;
                        if (b2 != null) {
                            InstalledApp installedApp = ((AppWrapper) b2.get(adapterPosition)).getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected != null) {
                                boolean booleanValue = isSelected.booleanValue();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a2 = gv4.e.a();
                                local.d(a2, "isSelected = " + booleanValue);
                                b c = this.a.b.b;
                                if (c != null) {
                                    List b3 = this.a.b.c;
                                    if (b3 != null) {
                                        c.a((AppWrapper) b3.get(adapterPosition), !booleanValue);
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gv4 gv4, tf4 tf4) {
            super(tf4.d());
            wg6.b(tf4, "binding");
            this.b = gv4;
            this.a = tf4;
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (b2 != null) {
                this.a.q.setBackgroundColor(Color.parseColor(b2));
            }
            if (b3 != null) {
                this.a.t.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.u.setOnClickListener(new a(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v16, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v18, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v23, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v25, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
        public final void a(AppWrapper appWrapper) {
            wg6.b(appWrapper, "appWrapper");
            ImageView imageView = this.a.s;
            wg6.a((Object) imageView, "binding.ivAppIcon");
            mj4 a2 = jj4.a(imageView.getContext());
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new gj4(installedApp)).b(appWrapper.getIconResourceId()).c().a(this.a.s);
                Object r0 = this.a.r;
                wg6.a((Object) r0, "binding.ftvAppName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                r0.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                if (appWrapper.getUri() != null) {
                    Object r02 = this.a.r;
                    wg6.a((Object) r02, "binding.ftvAppName");
                    r02.setTextColor(r02.getTextColors().withAlpha(255));
                    Object r03 = this.a.u;
                    wg6.a((Object) r03, "binding.swEnabled");
                    r03.setEnabled(true);
                    Object r04 = this.a.u;
                    wg6.a((Object) r04, "binding.swEnabled");
                    Drawable background = r04.getBackground();
                    wg6.a((Object) background, "binding.swEnabled.background");
                    background.setAlpha(255);
                } else {
                    Object r05 = this.a.r;
                    wg6.a((Object) r05, "binding.ftvAppName");
                    r05.setTextColor(r05.getTextColors().withAlpha(100));
                    Object r06 = this.a.u;
                    wg6.a((Object) r06, "binding.swEnabled");
                    r06.setEnabled(false);
                    Object r07 = this.a.u;
                    wg6.a((Object) r07, "binding.swEnabled");
                    Drawable background2 = r07.getBackground();
                    wg6.a((Object) background2, "binding.swEnabled.background");
                    background2.setAlpha(100);
                }
                FlexibleSwitchCompat flexibleSwitchCompat = this.a.u;
                wg6.a((Object) flexibleSwitchCompat, "binding.swEnabled");
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected != null) {
                    flexibleSwitchCompat.setChecked(isSelected.booleanValue());
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ gv4 a;

        @DexIgnore
        public d(gv4 gv4) {
            this.a = gv4;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            wg6.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.a;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.a) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            wg6.a((Object) str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && yj6.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                            arrayList.add(appWrapper);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            wg6.b(charSequence, "charSequence");
            wg6.b(filterResults, "results");
            this.a.c = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    /*
    static {
        String simpleName = gv4.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationAppsAdapter::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public gv4() {
        setHasStableIds(true);
    }

    @DexIgnore
    public Filter getFilter() {
        return new d(this);
    }

    @DexIgnore
    public int getItemCount() {
        List<vx4> list = this.c;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        tf4 a2 = tf4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemAppNotificationBindi\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "holder");
        List<vx4> list = this.c;
        if (list != null) {
            cVar.a(list.get(i));
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<vx4> list) {
        wg6.b(list, "listAppWrapper");
        this.a.clear();
        this.a.addAll(list);
        this.c = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.b = bVar;
    }
}
