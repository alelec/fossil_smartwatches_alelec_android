package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow2 implements Parcelable.Creator<dw2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        Status status = null;
        ew2 ew2 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                status = (Status) f22.a(parcel, a, Status.CREATOR);
            } else if (a2 != 2) {
                f22.v(parcel, a);
            } else {
                ew2 = (ew2) f22.a(parcel, a, ew2.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new dw2(status, ew2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new dw2[i];
    }
}
