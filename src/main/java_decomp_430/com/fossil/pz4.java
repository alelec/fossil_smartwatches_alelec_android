package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pz4 extends y24<b, c, y24.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements y24.b {
        @DexIgnore
        public /* final */ List<wx4> a;
        @DexIgnore
        public /* final */ List<wx4> b;

        @DexIgnore
        public b(List<wx4> list, List<wx4> list2) {
            wg6.b(list, "contactWrapperListRemoved");
            wg6.b(list2, "contactWrapperListAdded");
            this.a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<wx4> a() {
            return this.b;
        }

        @DexIgnore
        public final List<wx4> b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = pz4.class.getSimpleName();
        wg6.a((Object) simpleName, "SaveContactGroupsNotific\u2026on::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public pz4(NotificationsRepository notificationsRepository) {
        wg6.b(notificationsRepository, "notificationsRepository");
        jk3.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        wg6.a((Object) notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact removePhoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(removePhoneFavoritesContact);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0274 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01cd  */
    public final void c(List<wx4> list) {
        Iterator<wx4> it;
        Contact contact;
        if (!list.isEmpty()) {
            List<ContactGroup> allContactGroups = this.d.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ContactProvider b2 = zm4.p.a().b();
            Iterator<wx4> it2 = list.iterator();
            while (it2.hasNext()) {
                wx4 next = it2.next();
                if (allContactGroups != null) {
                    Iterator<ContactGroup> it3 = allContactGroups.iterator();
                    boolean z = false;
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        ContactGroup next2 = it3.next();
                        wg6.a((Object) next2, "contactGroup");
                        Iterator it4 = next2.getContacts().iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                it = it2;
                                break;
                            }
                            Contact contact2 = (Contact) it4.next();
                            if (!(next.getContact() == null || contact2 == null)) {
                                int contactId = contact2.getContactId();
                                Contact contact3 = next.getContact();
                                if (contact3 != null && contactId == contact3.getContactId()) {
                                    Contact contact4 = next.getContact();
                                    Boolean valueOf = contact4 != null ? Boolean.valueOf(contact4.isUseCall()) : null;
                                    if (valueOf != null) {
                                        contact2.setUseCall(valueOf.booleanValue());
                                        Contact contact5 = next.getContact();
                                        Boolean valueOf2 = contact5 != null ? Boolean.valueOf(contact5.isUseSms()) : null;
                                        if (valueOf2 != null) {
                                            contact2.setUseSms(valueOf2.booleanValue());
                                            Contact contact6 = next.getContact();
                                            contact2.setFirstName(contact6 != null ? contact6.getFirstName() : null);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String str = e;
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("Contact Id = ");
                                            Contact contact7 = next.getContact();
                                            sb.append(contact7 != null ? Integer.valueOf(contact7.getContactId()) : null);
                                            sb.append(", ");
                                            it = it2;
                                            sb.append("Contact name = ");
                                            Contact contact8 = next.getContact();
                                            sb.append(contact8 != null ? contact8.getFirstName() : null);
                                            sb.append(',');
                                            sb.append("Contact db row = ");
                                            Contact contact9 = next.getContact();
                                            sb.append(contact9 != null ? Integer.valueOf(contact9.getDbRowId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact phone = ");
                                            sb.append(next.getPhoneNumber());
                                            local.d(str, sb.toString());
                                            next.setContact(contact2);
                                            b2.removeContactGroup(next2);
                                            allContactGroups.remove(next2);
                                            z = true;
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            }
                            it2 = it2;
                        }
                        if (z) {
                            break;
                        }
                        it2 = it;
                    }
                    ContactGroup contactGroup = new ContactGroup();
                    contactGroup.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                    contactGroup.setHour(next.getCurrentHandGroup());
                    arrayList2.add(contactGroup);
                    contact = next.getContact();
                    if (contact != null) {
                        contact.setContactGroup(contactGroup);
                    }
                    if (contact != null) {
                        Contact contact10 = next.getContact();
                        Boolean valueOf3 = contact10 != null ? Boolean.valueOf(contact10.isUseCall()) : null;
                        if (valueOf3 != null) {
                            contact.setUseCall(valueOf3.booleanValue());
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    if (contact != null) {
                        Contact contact11 = next.getContact();
                        Boolean valueOf4 = contact11 != null ? Boolean.valueOf(contact11.isUseSms()) : null;
                        if (valueOf4 != null) {
                            contact.setUseSms(valueOf4.booleanValue());
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    if (contact != null) {
                        contact.setUseEmail(false);
                    }
                    if (contact == null) {
                        arrayList.add(contact);
                        ContentResolver contentResolver = PortfolioApp.get.instance().getContentResolver();
                        if (next.hasPhoneNumber()) {
                            Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, "contact_id=" + contact.getContactId(), (String[]) null, (String) null);
                            if (query != null) {
                                while (query.moveToNext()) {
                                    try {
                                        PhoneNumber phoneNumber = new PhoneNumber();
                                        phoneNumber.setNumber(query.getString(query.getColumnIndex("data1")));
                                        phoneNumber.setContact(contact);
                                        if (!gy5.a((List<PhoneNumber>) arrayList3, phoneNumber).booleanValue()) {
                                            arrayList3.add(phoneNumber);
                                        }
                                        if (next.isFavorites()) {
                                            arrayList4.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                        }
                                    } catch (Exception e2) {
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String str2 = e;
                                        local2.e(str2, "Error Inside " + e + ".saveContactToFSL - ex=" + e2);
                                    } catch (Throwable th) {
                                        query.close();
                                        throw th;
                                    }
                                }
                                query.close();
                            }
                        }
                        it2 = it;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                it = it2;
                ContactGroup contactGroup2 = new ContactGroup();
                contactGroup2.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                contactGroup2.setHour(next.getCurrentHandGroup());
                arrayList2.add(contactGroup2);
                contact = next.getContact();
                if (contact != null) {
                }
                if (contact != null) {
                }
                if (contact != null) {
                }
                if (contact != null) {
                }
                if (contact == null) {
                }
            }
            this.d.saveContactGroupList(arrayList2);
            this.d.saveListContact(arrayList);
            this.d.saveListPhoneNumber(arrayList3);
            d(arrayList4);
        }
    }

    @DexIgnore
    public final void d(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact savePhoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(savePhoneFavoritesContact);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        wg6.b(bVar, "requestValues");
        a(bVar.b());
        c(bVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveContactGroupsNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<wx4> list) {
        if (!list.isEmpty()) {
            ContactProvider b2 = zm4.p.a().b();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (wx4 contact : list) {
                Contact contact2 = contact.getContact();
                if (contact2 != null) {
                    arrayList2.add(contact2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    local.d(str, "Removed contact = " + contact2.getFirstName() + " row id = " + contact2.getDbRowId());
                    for (ContactGroup contactGroup : b2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue())) {
                        wg6.a((Object) contactGroup, "contactGroupItem");
                        Iterator it = contactGroup.getContacts().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Contact contact3 = (Contact) it.next();
                            int contactId = contact2.getContactId();
                            wg6.a((Object) contact3, "contactItem");
                            if (contactId == contact3.getContactId()) {
                                contact2.setDbRowId(contact3.getDbRowId());
                                arrayList.add(contactGroup);
                                break;
                            }
                        }
                    }
                    for (PhoneNumber phoneNumber : contact2.getPhoneNumbers()) {
                        wg6.a((Object) phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                        arrayList3.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            b(arrayList3);
        }
    }
}
