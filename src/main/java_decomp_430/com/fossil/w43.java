package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w43 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;
    @DexIgnore
    public /* final */ /* synthetic */ Object d;
    @DexIgnore
    public /* final */ /* synthetic */ Object e;
    @DexIgnore
    public /* final */ /* synthetic */ t43 f;

    @DexIgnore
    public w43(t43 t43, int i, String str, Object obj, Object obj2, Object obj3) {
        this.f = t43;
        this.a = i;
        this.b = str;
        this.c = obj;
        this.d = obj2;
        this.e = obj3;
    }

    @DexIgnore
    public final void run() {
        h53 q = this.f.a.q();
        if (q.r()) {
            if (this.f.c == 0) {
                if (this.f.l().o()) {
                    t43 t43 = this.f;
                    t43.d();
                    char unused = t43.c = 'C';
                } else {
                    t43 t432 = this.f;
                    t432.d();
                    char unused2 = t432.c = 'c';
                }
            }
            if (this.f.d < 0) {
                t43 t433 = this.f;
                long unused3 = t433.d = t433.l().n();
            }
            char charAt = "01VDIWEA?".charAt(this.a);
            char a2 = this.f.c;
            long b2 = this.f.d;
            String a3 = t43.a(true, this.b, this.c, this.d, this.e);
            StringBuilder sb = new StringBuilder(String.valueOf(a3).length() + 24);
            sb.append("2");
            sb.append(charAt);
            sb.append(a2);
            sb.append(b2);
            sb.append(":");
            sb.append(a3);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.b.substring(0, 1024);
            }
            q.d.a(sb2, 1);
            return;
        }
        this.f.a(6, "Persisted config not initialized. Not logging error/warn");
    }
}
