package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum og0 {
    PHASE_INIT,
    PHASE_START,
    PHASE_END,
    REQUEST,
    RESPONSE,
    SYSTEM_EVENT,
    CENTRAL_EVENT,
    DEVICE_EVENT,
    GATT_SERVER_EVENT,
    EXCEPTION,
    DATABASE
}
