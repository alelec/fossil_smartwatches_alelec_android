package com.fossil;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt3 extends Exception {
    @DexIgnore
    public static /* final */ int ERROR_INVALID_PARAMETERS; // = 1;
    @DexIgnore
    public static /* final */ int ERROR_SIZE; // = 2;
    @DexIgnore
    public static /* final */ int ERROR_TOO_MANY_MESSAGES; // = 4;
    @DexIgnore
    public static /* final */ int ERROR_TTL_EXCEEDED; // = 3;
    @DexIgnore
    public static /* final */ int ERROR_UNKNOWN; // = 0;
    @DexIgnore
    public /* final */ int zza;

    @DexIgnore
    public dt3(String str) {
        super(str);
        int i = 0;
        if (str != null) {
            String lowerCase = str.toLowerCase(Locale.US);
            char c = 65535;
            switch (lowerCase.hashCode()) {
                case -1743242157:
                    if (lowerCase.equals("service_not_available")) {
                        c = 3;
                        break;
                    }
                    break;
                case -1290953729:
                    if (lowerCase.equals("toomanymessages")) {
                        c = 4;
                        break;
                    }
                    break;
                case -920906446:
                    if (lowerCase.equals("invalid_parameters")) {
                        c = 0;
                        break;
                    }
                    break;
                case -617027085:
                    if (lowerCase.equals("messagetoobig")) {
                        c = 2;
                        break;
                    }
                    break;
                case -95047692:
                    if (lowerCase.equals("missing_to")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0 || c == 1) {
                i = 1;
            } else if (c == 2) {
                i = 2;
            } else if (c == 3) {
                i = 3;
            } else if (c == 4) {
                i = 4;
            }
        }
        this.zza = i;
    }

    @DexIgnore
    public final int getErrorCode() {
        return this.zza;
    }
}
