package com.fossil;

import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rs6 {
    @DexIgnore
    public static final rs6 a = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements rs6 {
        @DexIgnore
        public void a(int i, hs6 hs6) {
        }

        @DexIgnore
        public boolean a(int i, lt6 lt6, int i2, boolean z) throws IOException {
            lt6.skip((long) i2);
            return true;
        }

        @DexIgnore
        public boolean a(int i, List<is6> list) {
            return true;
        }

        @DexIgnore
        public boolean a(int i, List<is6> list, boolean z) {
            return true;
        }
    }

    @DexIgnore
    void a(int i, hs6 hs6);

    @DexIgnore
    boolean a(int i, lt6 lt6, int i2, boolean z) throws IOException;

    @DexIgnore
    boolean a(int i, List<is6> list);

    @DexIgnore
    boolean a(int i, List<is6> list, boolean z);
}
