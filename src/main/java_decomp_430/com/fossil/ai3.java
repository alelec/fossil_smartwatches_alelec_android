package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ai3 {
    @DexIgnore
    void a();

    @DexIgnore
    void a(fg3 fg3);

    @DexIgnore
    void a(ExtendedFloatingActionButton.h hVar);

    @DexIgnore
    int b();

    @DexIgnore
    void c();

    @DexIgnore
    fg3 d();

    @DexIgnore
    boolean e();

    @DexIgnore
    void f();

    @DexIgnore
    AnimatorSet g();

    @DexIgnore
    List<Animator.AnimatorListener> h();

    @DexIgnore
    void onAnimationStart(Animator animator);
}
