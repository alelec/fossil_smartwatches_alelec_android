package com.fossil;

import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p53 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ el2 a;
    @DexIgnore
    public /* final */ /* synthetic */ ServiceConnection b;
    @DexIgnore
    public /* final */ /* synthetic */ m53 c;

    @DexIgnore
    public p53(m53 m53, el2 el2, ServiceConnection serviceConnection) {
        this.c = m53;
        this.a = el2;
        this.b = serviceConnection;
    }

    @DexIgnore
    public final void run() {
        m53 m53 = this.c;
        n53 n53 = m53.b;
        String a2 = m53.a;
        el2 el2 = this.a;
        ServiceConnection serviceConnection = this.b;
        Bundle a3 = n53.a(a2, el2);
        n53.a.a().g();
        if (a3 != null) {
            long j = a3.getLong("install_begin_timestamp_seconds", 0) * 1000;
            if (j == 0) {
                n53.a.b().t().a("Service response is missing Install Referrer install timestamp");
            } else {
                String string = a3.getString("install_referrer");
                if (string == null || string.isEmpty()) {
                    n53.a.b().t().a("No referrer defined in install referrer response");
                } else {
                    n53.a.b().B().a("InstallReferrer API result", string);
                    ma3 w = n53.a.w();
                    String valueOf = String.valueOf(string);
                    Bundle a4 = w.a(Uri.parse(valueOf.length() != 0 ? "?".concat(valueOf) : new String("?")));
                    if (a4 == null) {
                        n53.a.b().t().a("No campaign params defined in install referrer result");
                    } else {
                        String string2 = a4.getString("medium");
                        if (string2 != null && !"(not set)".equalsIgnoreCase(string2) && !"organic".equalsIgnoreCase(string2)) {
                            long j2 = a3.getLong("referrer_click_timestamp_seconds", 0) * 1000;
                            if (j2 == 0) {
                                n53.a.b().t().a("Install Referrer is missing click timestamp for ad campaign");
                            } else {
                                a4.putLong("click_timestamp", j2);
                            }
                        }
                        if (j == n53.a.q().k.a()) {
                            n53.a.d();
                            n53.a.b().B().a("Campaign has already been logged");
                        } else {
                            n53.a.q().k.a(j);
                            n53.a.d();
                            n53.a.b().B().a("Logging Install Referrer campaign from sdk with ", "referrer API");
                            a4.putString("_cis", "referrer API");
                            n53.a.v().a("auto", "_cmp", a4);
                        }
                    }
                }
            }
        }
        if (serviceConnection != null) {
            b42.a().a(n53.a.c(), serviceConnection);
        }
    }
}
