package com.fossil;

import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u21 implements j71 {
    @DexIgnore
    public /* final */ /* synthetic */ ii1 a;

    @DexIgnore
    public u21(ii1 ii1) {
        this.a = ii1;
    }

    @DexIgnore
    public void a(ue1 ue1, h91 h91) {
        if1 if1;
        if (lv0.a[h91.ordinal()] == 1) {
            this.a.b(false);
        }
        if (ii1.a(this.a)) {
            this.a.s();
        }
        if1[] c = this.a.f.c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                if1 = null;
                break;
            }
            if1 = c[i];
            eh1 eh1 = if1.y;
            if (eh1 == eh1.OTA || eh1 == eh1.MAKE_DEVICE_READY) {
                break;
            }
            i++;
        }
        if (if1 == null) {
            int i2 = lv0.b[h91.ordinal()];
            if (i2 == 1) {
                this.a.a(q40.c.DISCONNECTED);
            } else if (i2 == 2) {
                this.a.a(q40.c.CONNECTING);
            } else if (i2 != 3 && i2 == 4) {
                this.a.a(q40.c.DISCONNECTING);
            }
        }
    }

    @DexIgnore
    public void a(ue1 ue1, m51 m51, m51 m512) {
        if (wg6.a(ue1, this.a.c)) {
            this.a.a(q40.d.f.a(m51), q40.d.f.a(m512));
        }
    }
}
