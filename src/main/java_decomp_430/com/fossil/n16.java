package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n16 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Picasso.LoadedFrom a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ InputStream c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
            this(bitmap, (InputStream) null, loadedFrom, 0);
            t16.a(bitmap, "bitmap == null");
        }

        @DexIgnore
        public Bitmap a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.d;
        }

        @DexIgnore
        public Picasso.LoadedFrom c() {
            return this.a;
        }

        @DexIgnore
        public InputStream d() {
            return this.c;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public a(InputStream inputStream, Picasso.LoadedFrom loadedFrom) {
            this((Bitmap) null, inputStream, loadedFrom, 0);
            t16.a(inputStream, "stream == null");
        }

        @DexIgnore
        public a(Bitmap bitmap, InputStream inputStream, Picasso.LoadedFrom loadedFrom, int i) {
            boolean z = true;
            if ((inputStream == null ? false : z) ^ (bitmap != null)) {
                this.b = bitmap;
                this.c = inputStream;
                t16.a(loadedFrom, "loadedFrom == null");
                this.a = loadedFrom;
                this.d = i;
                return;
            }
            throw new AssertionError();
        }
    }

    @DexIgnore
    public static boolean a(BitmapFactory.Options options) {
        return options != null && options.inJustDecodeBounds;
    }

    @DexIgnore
    public static BitmapFactory.Options b(l16 l16) {
        boolean c = l16.c();
        boolean z = l16.q != null;
        BitmapFactory.Options options = null;
        if (c || z) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = c;
            if (z) {
                options.inPreferredConfig = l16.q;
            }
        }
        return options;
    }

    @DexIgnore
    public int a() {
        return 0;
    }

    @DexIgnore
    public abstract a a(l16 l16, int i) throws IOException;

    @DexIgnore
    public abstract boolean a(l16 l16);

    @DexIgnore
    public boolean a(boolean z, NetworkInfo networkInfo) {
        return false;
    }

    @DexIgnore
    public boolean b() {
        return false;
    }

    @DexIgnore
    public static void a(int i, int i2, BitmapFactory.Options options, l16 l16) {
        a(i, i2, options.outWidth, options.outHeight, options, l16);
    }

    @DexIgnore
    public static void a(int i, int i2, int i3, int i4, BitmapFactory.Options options, l16 l16) {
        int i5;
        double floor;
        if (i4 > i2 || i3 > i) {
            if (i2 == 0) {
                floor = Math.floor((double) (((float) i3) / ((float) i)));
            } else if (i == 0) {
                floor = Math.floor((double) (((float) i4) / ((float) i2)));
            } else {
                int floor2 = (int) Math.floor((double) (((float) i4) / ((float) i2)));
                int floor3 = (int) Math.floor((double) (((float) i3) / ((float) i)));
                i5 = l16.k ? Math.max(floor2, floor3) : Math.min(floor2, floor3);
            }
            i5 = (int) floor;
        } else {
            i5 = 1;
        }
        options.inSampleSize = i5;
        options.inJustDecodeBounds = false;
    }
}
