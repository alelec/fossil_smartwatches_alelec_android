package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c76 {
    @DexIgnore
    public static void a(Activity activity) {
        z76.a(activity, Constants.ACTIVITY);
        Application application = activity.getApplication();
        if (application instanceof l76) {
            d76<Activity> d = ((l76) application).d();
            z76.a(d, "%s.activityInjector() returned null", application.getClass());
            d.a(activity);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), l76.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(Service service) {
        z76.a(service, Constants.SERVICE);
        Application application = service.getApplication();
        if (application instanceof p76) {
            d76<Service> b = ((p76) application).b();
            z76.a(b, "%s.serviceInjector() returned null", application.getClass());
            b.a(service);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), p76.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(BroadcastReceiver broadcastReceiver, Context context) {
        z76.a(broadcastReceiver, "broadcastReceiver");
        z76.a(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof m76) {
            d76<BroadcastReceiver> a = ((m76) application).a();
            z76.a(a, "%s.broadcastReceiverInjector() returned null", application.getClass());
            a.a(broadcastReceiver);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), m76.class.getCanonicalName()}));
    }

    @DexIgnore
    public static void a(ContentProvider contentProvider) {
        z76.a(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof n76) {
            d76<ContentProvider> c = ((n76) application).c();
            z76.a(c, "%s.contentProviderInjector() returned null", application.getClass());
            c.a(contentProvider);
            return;
        }
        throw new RuntimeException(String.format("%s does not implement %s", new Object[]{application.getClass().getCanonicalName(), n76.class.getCanonicalName()}));
    }
}
