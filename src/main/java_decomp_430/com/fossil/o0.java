package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import com.fossil.f9;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o0 extends Dialog implements m0 {
    @DexIgnore
    public AppCompatDelegate a;
    @DexIgnore
    public /* final */ f9.a b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements f9.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
            return o0.this.a(keyEvent);
        }
    }

    @DexIgnore
    public o0(Context context, int i) {
        super(context, a(context, i));
        AppCompatDelegate a2 = a();
        a2.d(a(context, i));
        a2.a((Bundle) null);
    }

    @DexIgnore
    public boolean a(int i) {
        return a().b(i);
    }

    @DexIgnore
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    @DexIgnore
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return f9.a(this.b, getWindow().getDecorView(), this, keyEvent);
    }

    @DexIgnore
    public <T extends View> T findViewById(int i) {
        return a().a(i);
    }

    @DexIgnore
    public void invalidateOptionsMenu() {
        a().f();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        a().e();
        super.onCreate(bundle);
        a().a(bundle);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        a().j();
    }

    @DexIgnore
    public void onSupportActionModeFinished(ActionMode actionMode) {
    }

    @DexIgnore
    public void onSupportActionModeStarted(ActionMode actionMode) {
    }

    @DexIgnore
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    @DexIgnore
    public void setContentView(int i) {
        a().c(i);
    }

    @DexIgnore
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }

    @DexIgnore
    public AppCompatDelegate a() {
        if (this.a == null) {
            this.a = AppCompatDelegate.a((Dialog) this, (m0) this);
        }
        return this.a;
    }

    @DexIgnore
    public void setContentView(View view) {
        a().a(view);
    }

    @DexIgnore
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    @DexIgnore
    public void setTitle(int i) {
        super.setTitle(i);
        a().a((CharSequence) getContext().getString(i));
    }

    @DexIgnore
    public static int a(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(a0.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
}
