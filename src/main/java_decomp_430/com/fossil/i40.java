package com.fossil;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i40 {
    @DexIgnore
    public static /* final */ FilenameFilter a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return true;
        }
    }

    @DexIgnore
    public static int a(File file, int i, Comparator<File> comparator) {
        return a(file, a, i, comparator);
    }

    @DexIgnore
    public static int a(File file, FilenameFilter filenameFilter, int i, Comparator<File> comparator) {
        File[] listFiles = file.listFiles(filenameFilter);
        if (listFiles == null) {
            return 0;
        }
        int length = listFiles.length;
        Arrays.sort(listFiles, comparator);
        for (File file2 : listFiles) {
            if (length <= i) {
                return length;
            }
            file2.delete();
            length--;
        }
        return length;
    }
}
