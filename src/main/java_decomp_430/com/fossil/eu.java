package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu implements wt<int[]> {
    @DexIgnore
    public int a() {
        return 4;
    }

    @DexIgnore
    public String b() {
        return "IntegerArrayPool";
    }

    @DexIgnore
    public int a(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public int[] newArray(int i) {
        return new int[i];
    }
}
