package com.fossil;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s15 implements Factory<r15> {
    @DexIgnore
    public static NotificationContactsAndAppsAssignedPresenter a(LoaderManager loaderManager, n15 n15, int i, z24 z24, a35 a35, o25 o25, l35 l35, tr4 tr4) {
        return new NotificationContactsAndAppsAssignedPresenter(loaderManager, n15, i, z24, a35, o25, l35, tr4);
    }
}
