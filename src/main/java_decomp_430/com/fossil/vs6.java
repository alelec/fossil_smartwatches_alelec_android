package com.fossil;

import android.os.Build;
import android.util.Log;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vs6 extends at6 {
    @DexIgnore
    public /* final */ zs6<Socket> c;
    @DexIgnore
    public /* final */ zs6<Socket> d;
    @DexIgnore
    public /* final */ zs6<Socket> e;
    @DexIgnore
    public /* final */ zs6<Socket> f;
    @DexIgnore
    public /* final */ c g; // = c.a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends et6 {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public a(Object obj, Method method) {
            this.a = obj;
            this.b = method;
        }

        @DexIgnore
        public List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException {
            try {
                return (List) this.b.invoke(this.a, new Object[]{(X509Certificate[]) list.toArray(new X509Certificate[list.size()]), "RSA", str});
            } catch (InvocationTargetException e) {
                SSLPeerUnverifiedException sSLPeerUnverifiedException = new SSLPeerUnverifiedException(e.getMessage());
                sSLPeerUnverifiedException.initCause(e);
                throw sSLPeerUnverifiedException;
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return obj instanceof a;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gt6 {
        @DexIgnore
        public /* final */ X509TrustManager a;
        @DexIgnore
        public /* final */ Method b;

        @DexIgnore
        public b(X509TrustManager x509TrustManager, Method method) {
            this.b = method;
            this.a = x509TrustManager;
        }

        @DexIgnore
        public X509Certificate a(X509Certificate x509Certificate) {
            try {
                TrustAnchor trustAnchor = (TrustAnchor) this.b.invoke(this.a, new Object[]{x509Certificate});
                if (trustAnchor != null) {
                    return trustAnchor.getTrustedCert();
                }
                return null;
            } catch (IllegalAccessException e) {
                throw fr6.a("unable to get issues and signature", (Exception) e);
            } catch (InvocationTargetException unused) {
                return null;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (!this.a.equals(bVar.a) || !this.b.equals(bVar.b)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode() + (this.b.hashCode() * 31);
        }
    }

    @DexIgnore
    public vs6(Class<?> cls, zs6<Socket> zs6, zs6<Socket> zs62, zs6<Socket> zs63, zs6<Socket> zs64) {
        this.c = zs6;
        this.d = zs62;
        this.e = zs63;
        this.f = zs64;
    }

    @DexIgnore
    public static at6 f() {
        Class<?> cls;
        zs6 zs6;
        zs6 zs62;
        Class<byte[]> cls2 = byte[].class;
        try {
            cls = Class.forName("com.android.org.conscrypt.SSLParametersImpl");
        } catch (ClassNotFoundException unused) {
            try {
                cls = Class.forName("org.apache.harmony.xnet.provider.jsse.SSLParametersImpl");
            } catch (ClassNotFoundException unused2) {
                return null;
            }
        }
        Class<?> cls3 = cls;
        zs6 zs63 = new zs6((Class<?>) null, "setUseSessionTickets", Boolean.TYPE);
        zs6 zs64 = new zs6((Class<?>) null, "setHostname", String.class);
        if (g()) {
            zs6 zs65 = new zs6(cls2, "getAlpnSelectedProtocol", new Class[0]);
            zs6 = new zs6((Class<?>) null, "setAlpnProtocols", cls2);
            zs62 = zs65;
        } else {
            zs62 = null;
            zs6 = null;
        }
        return new vs6(cls3, zs63, zs64, zs62, zs6);
    }

    @DexIgnore
    public static boolean g() {
        if (Security.getProvider("GMSCore_OpenSSL") != null) {
            return true;
        }
        try {
            Class.forName("android.net.Network");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public void a(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        try {
            socket.connect(inetSocketAddress, i);
        } catch (AssertionError e2) {
            if (fr6.a(e2)) {
                throw new IOException(e2);
            }
            throw e2;
        } catch (SecurityException e3) {
            IOException iOException = new IOException("Exception in connect");
            iOException.initCause(e3);
            throw iOException;
        } catch (ClassCastException e4) {
            if (Build.VERSION.SDK_INT == 26) {
                IOException iOException2 = new IOException("Exception in connect");
                iOException2.initCause(e4);
                throw iOException2;
            }
            throw e4;
        }
    }

    @DexIgnore
    public String b(SSLSocket sSLSocket) {
        byte[] bArr;
        zs6<Socket> zs6 = this.e;
        if (zs6 == null || !zs6.a(sSLSocket) || (bArr = (byte[]) this.e.d(sSLSocket, new Object[0])) == null) {
            return null;
        }
        return new String(bArr, fr6.i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ Method b;
        @DexIgnore
        public /* final */ Method c;

        @DexIgnore
        public c(Method method, Method method2, Method method3) {
            this.a = method;
            this.b = method2;
            this.c = method3;
        }

        @DexIgnore
        public Object a(String str) {
            Method method = this.a;
            if (method != null) {
                try {
                    Object invoke = method.invoke((Object) null, new Object[0]);
                    this.b.invoke(invoke, new Object[]{str});
                    return invoke;
                } catch (Exception unused) {
                }
            }
            return null;
        }

        @DexIgnore
        public boolean a(Object obj) {
            if (obj == null) {
                return false;
            }
            try {
                this.c.invoke(obj, new Object[0]);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        @DexIgnore
        public static c a() {
            Method method;
            Method method2;
            Method method3 = null;
            try {
                Class<?> cls = Class.forName("dalvik.system.CloseGuard");
                Method method4 = cls.getMethod("get", new Class[0]);
                method = cls.getMethod("open", new Class[]{String.class});
                method2 = cls.getMethod("warnIfOpen", new Class[0]);
                method3 = method4;
            } catch (Exception unused) {
                method2 = null;
                method = null;
            }
            return new c(method3, method, method2);
        }
    }

    @DexIgnore
    public boolean b(String str) {
        try {
            Class<?> cls = Class.forName("android.security.NetworkSecurityPolicy");
            return b(str, cls, cls.getMethod("getInstance", new Class[0]).invoke((Object) null, new Object[0]));
        } catch (ClassNotFoundException | NoSuchMethodException unused) {
            return super.b(str);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
            throw fr6.a("unable to determine cleartext support", e2);
        }
    }

    @DexIgnore
    public final boolean b(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[]{String.class}).invoke(obj, new Object[]{str})).booleanValue();
        } catch (NoSuchMethodException unused) {
            return a(str, cls, obj);
        }
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket, String str, List<wq6> list) {
        if (str != null) {
            this.c.c(sSLSocket, true);
            this.d.c(sSLSocket, str);
        }
        zs6<Socket> zs6 = this.f;
        if (zs6 != null && zs6.a(sSLSocket)) {
            this.f.d(sSLSocket, at6.b(list));
        }
    }

    @DexIgnore
    public gt6 b(X509TrustManager x509TrustManager) {
        try {
            Method declaredMethod = x509TrustManager.getClass().getDeclaredMethod("findTrustAnchorByIssuerAndSignature", new Class[]{X509Certificate.class});
            declaredMethod.setAccessible(true);
            return new b(x509TrustManager, declaredMethod);
        } catch (NoSuchMethodException unused) {
            return super.b(x509TrustManager);
        }
    }

    @DexIgnore
    public void a(int i, String str, Throwable th) {
        int min;
        int i2 = 5;
        if (i != 5) {
            i2 = 3;
        }
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        int i3 = 0;
        int length = str.length();
        while (i3 < length) {
            int indexOf = str.indexOf(10, i3);
            if (indexOf == -1) {
                indexOf = length;
            }
            while (true) {
                min = Math.min(indexOf, i3 + 4000);
                Log.println(i2, "OkHttp", str.substring(i3, min));
                if (min >= indexOf) {
                    break;
                }
                i3 = min;
            }
            i3 = min + 1;
        }
    }

    @DexIgnore
    public SSLContext b() {
        boolean z = true;
        try {
            if (Build.VERSION.SDK_INT < 16 || Build.VERSION.SDK_INT >= 22) {
                z = false;
            }
        } catch (NoClassDefFoundError unused) {
        }
        if (z) {
            try {
                return SSLContext.getInstance("TLSv1.2");
            } catch (NoSuchAlgorithmException unused2) {
            }
        }
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException("No TLS provider", e2);
        }
    }

    @DexIgnore
    public Object a(String str) {
        return this.g.a(str);
    }

    @DexIgnore
    public void a(String str, Object obj) {
        if (!this.g.a(obj)) {
            a(5, str, (Throwable) null);
        }
    }

    @DexIgnore
    public final boolean a(String str, Class<?> cls, Object obj) throws InvocationTargetException, IllegalAccessException {
        try {
            return ((Boolean) cls.getMethod("isCleartextTrafficPermitted", new Class[0]).invoke(obj, new Object[0])).booleanValue();
        } catch (NoSuchMethodException unused) {
            return super.b(str);
        }
    }

    @DexIgnore
    public et6 a(X509TrustManager x509TrustManager) {
        try {
            Class<?> cls = Class.forName("android.net.http.X509TrustManagerExtensions");
            return new a(cls.getConstructor(new Class[]{X509TrustManager.class}).newInstance(new Object[]{x509TrustManager}), cls.getMethod("checkServerTrusted", new Class[]{X509Certificate[].class, String.class, String.class}));
        } catch (Exception unused) {
            return super.a(x509TrustManager);
        }
    }
}
