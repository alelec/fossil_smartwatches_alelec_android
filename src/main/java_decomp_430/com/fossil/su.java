package com.fossil;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ int i; // = (Build.VERSION.SDK_INT < 26 ? 4 : 1);
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public ActivityManager b;
        @DexIgnore
        public c c;
        @DexIgnore
        public float d; // = 2.0f;
        @DexIgnore
        public float e; // = ((float) i);
        @DexIgnore
        public float f; // = 0.4f;
        @DexIgnore
        public float g; // = 0.33f;
        @DexIgnore
        public int h; // = 4194304;

        @DexIgnore
        public a(Context context) {
            this.a = context;
            this.b = (ActivityManager) context.getSystemService("activity");
            this.c = new b(context.getResources().getDisplayMetrics());
            if (Build.VERSION.SDK_INT >= 26 && su.a(this.b)) {
                this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
        }

        @DexIgnore
        public su a() {
            return new su(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements c {
        @DexIgnore
        public /* final */ DisplayMetrics a;

        @DexIgnore
        public b(DisplayMetrics displayMetrics) {
            this.a = displayMetrics;
        }

        @DexIgnore
        public int a() {
            return this.a.heightPixels;
        }

        @DexIgnore
        public int b() {
            return this.a.widthPixels;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        int a();

        @DexIgnore
        int b();
    }

    @DexIgnore
    public su(a aVar) {
        int i;
        this.c = aVar.a;
        if (a(aVar.b)) {
            i = aVar.h / 2;
        } else {
            i = aVar.h;
        }
        this.d = i;
        int a2 = a(aVar.b, aVar.f, aVar.g);
        float b2 = (float) (aVar.c.b() * aVar.c.a() * 4);
        int round = Math.round(aVar.e * b2);
        int round2 = Math.round(b2 * aVar.d);
        int i2 = a2 - this.d;
        int i3 = round2 + round;
        if (i3 <= i2) {
            this.b = round2;
            this.a = round;
        } else {
            float f = (float) i2;
            float f2 = aVar.e;
            float f3 = aVar.d;
            float f4 = f / (f2 + f3);
            this.b = Math.round(f3 * f4);
            this.a = Math.round(f4 * aVar.e);
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Calculation complete, Calculated memory cache size: ");
            sb.append(a(this.b));
            sb.append(", pool size: ");
            sb.append(a(this.a));
            sb.append(", byte array size: ");
            sb.append(a(this.d));
            sb.append(", memory class limited? ");
            sb.append(i3 > a2);
            sb.append(", max size: ");
            sb.append(a(a2));
            sb.append(", memoryClass: ");
            sb.append(aVar.b.getMemoryClass());
            sb.append(", isLowMemoryDevice: ");
            sb.append(a(aVar.b));
            Log.d("MemorySizeCalculator", sb.toString());
        }
    }

    @DexIgnore
    public int a() {
        return this.d;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public static int a(ActivityManager activityManager, float f, float f2) {
        boolean a2 = a(activityManager);
        float memoryClass = (float) (activityManager.getMemoryClass() * 1024 * 1024);
        if (a2) {
            f = f2;
        }
        return Math.round(memoryClass * f);
    }

    @DexIgnore
    public final String a(int i) {
        return Formatter.formatFileSize(this.c, (long) i);
    }

    @DexIgnore
    @TargetApi(19)
    public static boolean a(ActivityManager activityManager) {
        if (Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return true;
    }
}
