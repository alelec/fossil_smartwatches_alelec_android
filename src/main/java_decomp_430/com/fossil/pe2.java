package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pe2 {
    @DexIgnore
    public static /* final */ String[] a;

    /*
    static {
        String[] strArr = new String[123];
        a = strArr;
        strArr[9] = "aerobics";
        String[] strArr2 = a;
        strArr2[119] = "archery";
        strArr2[10] = "badminton";
        strArr2[11] = "baseball";
        strArr2[12] = "basketball";
        strArr2[13] = "biathlon";
        strArr2[1] = "biking";
        strArr2[14] = "biking.hand";
        strArr2[15] = "biking.mountain";
        strArr2[16] = "biking.road";
        strArr2[17] = "biking.spinning";
        strArr2[18] = "biking.stationary";
        strArr2[19] = "biking.utility";
        strArr2[20] = "boxing";
        strArr2[21] = "calisthenics";
        strArr2[22] = "circuit_training";
        strArr2[23] = "cricket";
        strArr2[113] = "crossfit";
        strArr2[106] = "curling";
        strArr2[24] = "dancing";
        strArr2[102] = "diving";
        strArr2[117] = "elevator";
        strArr2[25] = "elliptical";
        strArr2[103] = "ergometer";
        strArr2[118] = "escalator";
        strArr2[6] = "exiting_vehicle";
        strArr2[26] = "fencing";
        strArr2[121] = "flossing";
        strArr2[27] = "football.american";
        strArr2[28] = "football.australian";
        strArr2[29] = "football.soccer";
        strArr2[30] = "frisbee_disc";
        strArr2[31] = "gardening";
        strArr2[32] = "golf";
        strArr2[122] = "guided_breathing";
        strArr2[33] = "gymnastics";
        strArr2[34] = "handball";
        strArr2[114] = "interval_training.high_intensity";
        strArr2[35] = "hiking";
        strArr2[36] = "hockey";
        strArr2[37] = "horseback_riding";
        strArr2[38] = "housework";
        strArr2[104] = "ice_skating";
        strArr2[0] = "in_vehicle";
        strArr2[115] = "interval_training";
        strArr2[39] = "jump_rope";
        strArr2[40] = "kayaking";
        strArr2[41] = "kettlebell_training";
        strArr2[107] = "kick_scooter";
        strArr2[42] = "kickboxing";
        strArr2[43] = "kitesurfing";
        strArr2[44] = "martial_arts";
        strArr2[45] = "meditation";
        strArr2[46] = "martial_arts.mixed";
        strArr2[2] = "on_foot";
        strArr2[108] = FacebookRequestErrorClassification.KEY_OTHER;
        strArr2[47] = "p90x";
        strArr2[48] = "paragliding";
        strArr2[49] = "pilates";
        strArr2[50] = "polo";
        strArr2[51] = "racquetball";
        strArr2[52] = "rock_climbing";
        strArr2[53] = "rowing";
        strArr2[54] = "rowing.machine";
        strArr2[55] = "rugby";
        strArr2[8] = "running";
        strArr2[56] = "running.jogging";
        strArr2[57] = "running.sand";
        strArr2[58] = "running.treadmill";
        strArr2[59] = "sailing";
        strArr2[60] = "scuba_diving";
        strArr2[61] = "skateboarding";
        strArr2[62] = "skating";
        strArr2[63] = "skating.cross";
        strArr2[105] = "skating.indoor";
        strArr2[64] = "skating.inline";
        strArr2[65] = "skiing";
        strArr2[66] = "skiing.back_country";
        strArr2[67] = "skiing.cross_country";
        strArr2[68] = "skiing.downhill";
        strArr2[69] = "skiing.kite";
        strArr2[70] = "skiing.roller";
        strArr2[71] = "sledding";
        strArr2[72] = "sleep";
        strArr2[109] = "sleep.light";
        strArr2[110] = "sleep.deep";
        strArr2[111] = "sleep.rem";
        strArr2[112] = "sleep.awake";
        strArr2[73] = "snowboarding";
        strArr2[74] = "snowmobile";
        strArr2[75] = "snowshoeing";
        strArr2[120] = "softball";
        strArr2[76] = "squash";
        strArr2[77] = "stair_climbing";
        strArr2[78] = "stair_climbing.machine";
        strArr2[79] = "standup_paddleboarding";
        strArr2[3] = "still";
        strArr2[80] = "strength_training";
        strArr2[81] = "surfing";
        strArr2[82] = "swimming";
        strArr2[83] = "swimming.pool";
        strArr2[84] = "swimming.open_water";
        strArr2[85] = "table_tennis";
        strArr2[86] = "team_sports";
        strArr2[87] = "tennis";
        strArr2[5] = "tilting";
        strArr2[88] = "treadmill";
        strArr2[4] = "unknown";
        strArr2[89] = "volleyball";
        strArr2[90] = "volleyball.beach";
        strArr2[91] = "volleyball.indoor";
        strArr2[92] = "wakeboarding";
        strArr2[7] = "walking";
        strArr2[93] = "walking.fitness";
        strArr2[94] = "walking.nordic";
        strArr2[95] = "walking.treadmill";
        strArr2[116] = "walking.stroller";
        strArr2[96] = "water_polo";
        strArr2[97] = "weightlifting";
        strArr2[98] = "wheelchair";
        strArr2[99] = "windsurfing";
        strArr2[100] = "yoga";
        strArr2[101] = "zumba";
    }
    */

    @DexIgnore
    public static int a(String str) {
        int i = 0;
        while (true) {
            String[] strArr = a;
            if (i >= strArr.length) {
                return 4;
            }
            if (strArr[i].equals(str)) {
                return i;
            }
            i++;
        }
    }
}
