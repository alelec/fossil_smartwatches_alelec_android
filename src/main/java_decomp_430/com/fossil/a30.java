package com.fossil;

import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a30 implements xa6 {
    @DexIgnore
    public /* final */ s30 a;

    @DexIgnore
    public a30(s30 s30) {
        this.a = s30;
    }

    @DexIgnore
    public String a() {
        return this.a.a();
    }

    @DexIgnore
    public InputStream b() {
        return this.a.b();
    }

    @DexIgnore
    public String[] c() {
        return this.a.c();
    }

    @DexIgnore
    public long d() {
        return -1;
    }
}
