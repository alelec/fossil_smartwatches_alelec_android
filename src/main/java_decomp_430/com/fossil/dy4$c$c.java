package com.fossil;

import com.fossil.NotificationAppsPresenter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class dy4$c$c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $listNotificationSettings;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppsPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dy4$c$c(NotificationAppsPresenter.c cVar, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
        this.$listNotificationSettings = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        dy4$c$c dy4_c_c = new dy4$c$c(this.this$0, this.$listNotificationSettings, xe6);
        dy4_c_c.p$ = (il6) obj;
        return dy4_c_c;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((dy4$c$c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                int component2 = notificationSettingsModel.component2();
                if (notificationSettingsModel.component3()) {
                    String a = this.this$0.this$0.a(component2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = NotificationAppsPresenter.s.a();
                    local.d(a2, "CALL settingsTypeName=" + a);
                    if (component2 == 0) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = NotificationAppsPresenter.s.a();
                        local2.d(a3, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                        DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                        this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                    } else if (component2 == 1) {
                        int size = this.this$0.this$0.i.size();
                        for (int i = 0; i < size; i++) {
                            ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.i.get(i);
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = NotificationAppsPresenter.s.a();
                            StringBuilder sb = new StringBuilder();
                            sb.append("mListAppNotificationFilter add PHONE item - ");
                            sb.append(i);
                            sb.append(" name = ");
                            Object obj2 = contactGroup.getContacts().get(0);
                            wg6.a(obj2, "item.contacts[0]");
                            sb.append(((Contact) obj2).getDisplayName());
                            local3.d(a4, sb.toString());
                            DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                            Object obj3 = contactGroup.getContacts().get(0);
                            wg6.a(obj3, "item.contacts[0]");
                            appNotificationFilter.setSender(((Contact) obj3).getDisplayName());
                            this.this$0.this$0.h.add(appNotificationFilter);
                        }
                    }
                } else {
                    String a5 = this.this$0.this$0.a(component2);
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a6 = NotificationAppsPresenter.s.a();
                    local4.d(a6, "MESSAGE settingsTypeName=" + a5);
                    if (component2 == 0) {
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        String a7 = NotificationAppsPresenter.s.a();
                        local5.d(a7, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                        DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                        this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                    } else if (component2 == 1) {
                        int size2 = this.this$0.this$0.i.size();
                        for (int i2 = 0; i2 < size2; i2++) {
                            ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.i.get(i2);
                            ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                            String a8 = NotificationAppsPresenter.s.a();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                            sb2.append(i2);
                            sb2.append(" name = ");
                            Object obj4 = contactGroup2.getContacts().get(0);
                            wg6.a(obj4, "item.contacts[0]");
                            sb2.append(((Contact) obj4).getDisplayName());
                            local6.d(a8, sb2.toString());
                            DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                            Object obj5 = contactGroup2.getContacts().get(0);
                            wg6.a(obj5, "item.contacts[0]");
                            appNotificationFilter2.setSender(((Contact) obj5).getDisplayName());
                            this.this$0.this$0.h.add(appNotificationFilter2);
                        }
                    }
                }
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
