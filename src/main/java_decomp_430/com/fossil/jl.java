package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager2.widget.ViewPager2;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl extends ViewPager2.i {
    @DexIgnore
    public /* final */ LinearLayoutManager a;
    @DexIgnore
    public ViewPager2.k b;

    @DexIgnore
    public jl(LinearLayoutManager linearLayoutManager) {
        this.a = linearLayoutManager;
    }

    @DexIgnore
    public ViewPager2.k a() {
        return this.b;
    }

    @DexIgnore
    public void a(int i) {
    }

    @DexIgnore
    public void b(int i) {
    }

    @DexIgnore
    public void a(ViewPager2.k kVar) {
        this.b = kVar;
    }

    @DexIgnore
    public void a(int i, float f, int i2) {
        if (this.b != null) {
            float f2 = -f;
            int i3 = 0;
            while (i3 < this.a.e()) {
                View d = this.a.d(i3);
                if (d != null) {
                    this.b.a(d, ((float) (this.a.l(d) - i)) + f2);
                    i3++;
                } else {
                    throw new IllegalStateException(String.format(Locale.US, "LayoutManager returned a null child at pos %d/%d while transforming pages", new Object[]{Integer.valueOf(i3), Integer.valueOf(this.a.e())}));
                }
            }
        }
    }
}
