package com.fossil;

import com.fossil.u60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ft0 extends tg6 implements hg6<byte[], u60> {
    @DexIgnore
    public ft0(u60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(u60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DoNotDisturbScheduleConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((u60.a) this.receiver).a((byte[]) obj);
    }
}
