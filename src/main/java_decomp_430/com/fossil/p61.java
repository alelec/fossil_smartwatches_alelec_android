package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p61 extends ml0 {
    @DexIgnore
    public fo0 J; // = new fo0(0, 0, 0);
    @DexIgnore
    public long K; // = 10000;
    @DexIgnore
    public it0 L;

    @DexIgnore
    public p61(it0 it0, ue1 ue1) {
        super(tf1.REQUEST_CONNECTION_PRIORITY, lx0.SET_CONNECTION_PARAMS, ue1, 0, 8);
        this.L = it0;
    }

    @DexIgnore
    public void a(long j) {
        this.K = j;
    }

    @DexIgnore
    public long e() {
        return this.K;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), this.L.a());
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), this.J.a());
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.L.a).putShort((short) this.L.b).putShort((short) this.L.c).putShort((short) this.L.d).array();
        wg6.a(array, "ByteBuffer.allocate(8).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final fo0 s() {
        return this.J;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.J = new fo0(cw0.b(order.getShort(0)), cw0.b(order.getShort(2)), cw0.b(order.getShort(4)));
        return cw0.a(jSONObject, this.J.a());
    }
}
