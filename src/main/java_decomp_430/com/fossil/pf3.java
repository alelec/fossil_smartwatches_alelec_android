package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pf3 {
    @DexIgnore
    public static /* final */ int abc_action_bar_content_inset_material; // = 2131165184;
    @DexIgnore
    public static /* final */ int abc_action_bar_content_inset_with_nav; // = 2131165185;
    @DexIgnore
    public static /* final */ int abc_action_bar_default_height_material; // = 2131165186;
    @DexIgnore
    public static /* final */ int abc_action_bar_default_padding_end_material; // = 2131165187;
    @DexIgnore
    public static /* final */ int abc_action_bar_default_padding_start_material; // = 2131165188;
    @DexIgnore
    public static /* final */ int abc_action_bar_elevation_material; // = 2131165189;
    @DexIgnore
    public static /* final */ int abc_action_bar_icon_vertical_padding_material; // = 2131165190;
    @DexIgnore
    public static /* final */ int abc_action_bar_overflow_padding_end_material; // = 2131165191;
    @DexIgnore
    public static /* final */ int abc_action_bar_overflow_padding_start_material; // = 2131165192;
    @DexIgnore
    public static /* final */ int abc_action_bar_stacked_max_height; // = 2131165193;
    @DexIgnore
    public static /* final */ int abc_action_bar_stacked_tab_max_width; // = 2131165194;
    @DexIgnore
    public static /* final */ int abc_action_bar_subtitle_bottom_margin_material; // = 2131165195;
    @DexIgnore
    public static /* final */ int abc_action_bar_subtitle_top_margin_material; // = 2131165196;
    @DexIgnore
    public static /* final */ int abc_action_button_min_height_material; // = 2131165197;
    @DexIgnore
    public static /* final */ int abc_action_button_min_width_material; // = 2131165198;
    @DexIgnore
    public static /* final */ int abc_action_button_min_width_overflow_material; // = 2131165199;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_bar_height; // = 2131165200;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_dimen; // = 2131165201;
    @DexIgnore
    public static /* final */ int abc_button_inset_horizontal_material; // = 2131165202;
    @DexIgnore
    public static /* final */ int abc_button_inset_vertical_material; // = 2131165203;
    @DexIgnore
    public static /* final */ int abc_button_padding_horizontal_material; // = 2131165204;
    @DexIgnore
    public static /* final */ int abc_button_padding_vertical_material; // = 2131165205;
    @DexIgnore
    public static /* final */ int abc_cascading_menus_min_smallest_width; // = 2131165206;
    @DexIgnore
    public static /* final */ int abc_config_prefDialogWidth; // = 2131165207;
    @DexIgnore
    public static /* final */ int abc_control_corner_material; // = 2131165208;
    @DexIgnore
    public static /* final */ int abc_control_inset_material; // = 2131165209;
    @DexIgnore
    public static /* final */ int abc_control_padding_material; // = 2131165210;
    @DexIgnore
    public static /* final */ int abc_dialog_corner_radius_material; // = 2131165211;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_height_major; // = 2131165212;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_height_minor; // = 2131165213;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_width_major; // = 2131165214;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_width_minor; // = 2131165215;
    @DexIgnore
    public static /* final */ int abc_dialog_list_padding_bottom_no_buttons; // = 2131165216;
    @DexIgnore
    public static /* final */ int abc_dialog_list_padding_top_no_title; // = 2131165217;
    @DexIgnore
    public static /* final */ int abc_dialog_min_width_major; // = 2131165218;
    @DexIgnore
    public static /* final */ int abc_dialog_min_width_minor; // = 2131165219;
    @DexIgnore
    public static /* final */ int abc_dialog_padding_material; // = 2131165220;
    @DexIgnore
    public static /* final */ int abc_dialog_padding_top_material; // = 2131165221;
    @DexIgnore
    public static /* final */ int abc_dialog_title_divider_material; // = 2131165222;
    @DexIgnore
    public static /* final */ int abc_disabled_alpha_material_dark; // = 2131165223;
    @DexIgnore
    public static /* final */ int abc_disabled_alpha_material_light; // = 2131165224;
    @DexIgnore
    public static /* final */ int abc_dropdownitem_icon_width; // = 2131165225;
    @DexIgnore
    public static /* final */ int abc_dropdownitem_text_padding_left; // = 2131165226;
    @DexIgnore
    public static /* final */ int abc_dropdownitem_text_padding_right; // = 2131165227;
    @DexIgnore
    public static /* final */ int abc_edit_text_inset_bottom_material; // = 2131165228;
    @DexIgnore
    public static /* final */ int abc_edit_text_inset_horizontal_material; // = 2131165229;
    @DexIgnore
    public static /* final */ int abc_edit_text_inset_top_material; // = 2131165230;
    @DexIgnore
    public static /* final */ int abc_floating_window_z; // = 2131165231;
    @DexIgnore
    public static /* final */ int abc_list_item_height_large_material; // = 2131165232;
    @DexIgnore
    public static /* final */ int abc_list_item_height_material; // = 2131165233;
    @DexIgnore
    public static /* final */ int abc_list_item_height_small_material; // = 2131165234;
    @DexIgnore
    public static /* final */ int abc_list_item_padding_horizontal_material; // = 2131165235;
    @DexIgnore
    public static /* final */ int abc_panel_menu_list_width; // = 2131165236;
    @DexIgnore
    public static /* final */ int abc_progress_bar_height_material; // = 2131165237;
    @DexIgnore
    public static /* final */ int abc_search_view_preferred_height; // = 2131165238;
    @DexIgnore
    public static /* final */ int abc_search_view_preferred_width; // = 2131165239;
    @DexIgnore
    public static /* final */ int abc_seekbar_track_background_height_material; // = 2131165240;
    @DexIgnore
    public static /* final */ int abc_seekbar_track_progress_height_material; // = 2131165241;
    @DexIgnore
    public static /* final */ int abc_select_dialog_padding_start_material; // = 2131165242;
    @DexIgnore
    public static /* final */ int abc_switch_padding; // = 2131165243;
    @DexIgnore
    public static /* final */ int abc_text_size_body_1_material; // = 2131165244;
    @DexIgnore
    public static /* final */ int abc_text_size_body_2_material; // = 2131165245;
    @DexIgnore
    public static /* final */ int abc_text_size_button_material; // = 2131165246;
    @DexIgnore
    public static /* final */ int abc_text_size_caption_material; // = 2131165247;
    @DexIgnore
    public static /* final */ int abc_text_size_display_1_material; // = 2131165248;
    @DexIgnore
    public static /* final */ int abc_text_size_display_2_material; // = 2131165249;
    @DexIgnore
    public static /* final */ int abc_text_size_display_3_material; // = 2131165250;
    @DexIgnore
    public static /* final */ int abc_text_size_display_4_material; // = 2131165251;
    @DexIgnore
    public static /* final */ int abc_text_size_headline_material; // = 2131165252;
    @DexIgnore
    public static /* final */ int abc_text_size_large_material; // = 2131165253;
    @DexIgnore
    public static /* final */ int abc_text_size_medium_material; // = 2131165254;
    @DexIgnore
    public static /* final */ int abc_text_size_menu_header_material; // = 2131165255;
    @DexIgnore
    public static /* final */ int abc_text_size_menu_material; // = 2131165256;
    @DexIgnore
    public static /* final */ int abc_text_size_small_material; // = 2131165257;
    @DexIgnore
    public static /* final */ int abc_text_size_subhead_material; // = 2131165258;
    @DexIgnore
    public static /* final */ int abc_text_size_subtitle_material_toolbar; // = 2131165259;
    @DexIgnore
    public static /* final */ int abc_text_size_title_material; // = 2131165260;
    @DexIgnore
    public static /* final */ int abc_text_size_title_material_toolbar; // = 2131165261;
    @DexIgnore
    public static /* final */ int action_bar_size; // = 2131165262;
    @DexIgnore
    public static /* final */ int appcompat_dialog_background_inset; // = 2131165267;
    @DexIgnore
    public static /* final */ int cardview_compat_inset_shadow; // = 2131165282;
    @DexIgnore
    public static /* final */ int cardview_default_elevation; // = 2131165283;
    @DexIgnore
    public static /* final */ int cardview_default_radius; // = 2131165284;
    @DexIgnore
    public static /* final */ int compat_button_inset_horizontal_material; // = 2131165301;
    @DexIgnore
    public static /* final */ int compat_button_inset_vertical_material; // = 2131165302;
    @DexIgnore
    public static /* final */ int compat_button_padding_horizontal_material; // = 2131165303;
    @DexIgnore
    public static /* final */ int compat_button_padding_vertical_material; // = 2131165304;
    @DexIgnore
    public static /* final */ int compat_control_corner_material; // = 2131165305;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_height; // = 2131165306;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_width; // = 2131165307;
    @DexIgnore
    public static /* final */ int default_dimension; // = 2131165317;
    @DexIgnore
    public static /* final */ int design_appbar_elevation; // = 2131165321;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_active_item_max_width; // = 2131165322;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_active_item_min_width; // = 2131165323;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_active_text_size; // = 2131165324;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_elevation; // = 2131165325;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_height; // = 2131165326;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_icon_size; // = 2131165327;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_item_max_width; // = 2131165328;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_item_min_width; // = 2131165329;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_margin; // = 2131165330;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_shadow_height; // = 2131165331;
    @DexIgnore
    public static /* final */ int design_bottom_navigation_text_size; // = 2131165332;
    @DexIgnore
    public static /* final */ int design_bottom_sheet_elevation; // = 2131165333;
    @DexIgnore
    public static /* final */ int design_bottom_sheet_modal_elevation; // = 2131165334;
    @DexIgnore
    public static /* final */ int design_bottom_sheet_peek_height_min; // = 2131165335;
    @DexIgnore
    public static /* final */ int design_fab_border_width; // = 2131165336;
    @DexIgnore
    public static /* final */ int design_fab_elevation; // = 2131165337;
    @DexIgnore
    public static /* final */ int design_fab_image_size; // = 2131165338;
    @DexIgnore
    public static /* final */ int design_fab_size_mini; // = 2131165339;
    @DexIgnore
    public static /* final */ int design_fab_size_normal; // = 2131165340;
    @DexIgnore
    public static /* final */ int design_fab_translation_z_hovered_focused; // = 2131165341;
    @DexIgnore
    public static /* final */ int design_fab_translation_z_pressed; // = 2131165342;
    @DexIgnore
    public static /* final */ int design_navigation_elevation; // = 2131165343;
    @DexIgnore
    public static /* final */ int design_navigation_icon_padding; // = 2131165344;
    @DexIgnore
    public static /* final */ int design_navigation_icon_size; // = 2131165345;
    @DexIgnore
    public static /* final */ int design_navigation_item_horizontal_padding; // = 2131165346;
    @DexIgnore
    public static /* final */ int design_navigation_item_icon_padding; // = 2131165347;
    @DexIgnore
    public static /* final */ int design_navigation_max_width; // = 2131165348;
    @DexIgnore
    public static /* final */ int design_navigation_padding_bottom; // = 2131165349;
    @DexIgnore
    public static /* final */ int design_navigation_separator_vertical_padding; // = 2131165350;
    @DexIgnore
    public static /* final */ int design_snackbar_action_inline_max_width; // = 2131165351;
    @DexIgnore
    public static /* final */ int design_snackbar_action_text_color_alpha; // = 2131165352;
    @DexIgnore
    public static /* final */ int design_snackbar_background_corner_radius; // = 2131165353;
    @DexIgnore
    public static /* final */ int design_snackbar_elevation; // = 2131165354;
    @DexIgnore
    public static /* final */ int design_snackbar_extra_spacing_horizontal; // = 2131165355;
    @DexIgnore
    public static /* final */ int design_snackbar_max_width; // = 2131165356;
    @DexIgnore
    public static /* final */ int design_snackbar_min_width; // = 2131165357;
    @DexIgnore
    public static /* final */ int design_snackbar_padding_horizontal; // = 2131165358;
    @DexIgnore
    public static /* final */ int design_snackbar_padding_vertical; // = 2131165359;
    @DexIgnore
    public static /* final */ int design_snackbar_padding_vertical_2lines; // = 2131165360;
    @DexIgnore
    public static /* final */ int design_snackbar_text_size; // = 2131165361;
    @DexIgnore
    public static /* final */ int design_tab_max_width; // = 2131165362;
    @DexIgnore
    public static /* final */ int design_tab_scrollable_min_width; // = 2131165363;
    @DexIgnore
    public static /* final */ int design_tab_text_size; // = 2131165364;
    @DexIgnore
    public static /* final */ int design_tab_text_size_2line; // = 2131165365;
    @DexIgnore
    public static /* final */ int design_textinput_caption_translate_y; // = 2131165366;
    @DexIgnore
    public static /* final */ int disabled_alpha_material_dark; // = 2131165367;
    @DexIgnore
    public static /* final */ int disabled_alpha_material_light; // = 2131165368;
    @DexIgnore
    public static /* final */ int fastscroll_default_thickness; // = 2131165442;
    @DexIgnore
    public static /* final */ int fastscroll_margin; // = 2131165443;
    @DexIgnore
    public static /* final */ int fastscroll_minimum_range; // = 2131165444;
    @DexIgnore
    public static /* final */ int highlight_alpha_material_colored; // = 2131165485;
    @DexIgnore
    public static /* final */ int highlight_alpha_material_dark; // = 2131165486;
    @DexIgnore
    public static /* final */ int highlight_alpha_material_light; // = 2131165487;
    @DexIgnore
    public static /* final */ int hint_alpha_material_dark; // = 2131165488;
    @DexIgnore
    public static /* final */ int hint_alpha_material_light; // = 2131165489;
    @DexIgnore
    public static /* final */ int hint_pressed_alpha_material_dark; // = 2131165490;
    @DexIgnore
    public static /* final */ int hint_pressed_alpha_material_light; // = 2131165491;
    @DexIgnore
    public static /* final */ int item_touch_helper_max_drag_scroll_per_frame; // = 2131165492;
    @DexIgnore
    public static /* final */ int item_touch_helper_swipe_escape_max_velocity; // = 2131165493;
    @DexIgnore
    public static /* final */ int item_touch_helper_swipe_escape_velocity; // = 2131165494;
    @DexIgnore
    public static /* final */ int material_emphasis_disabled; // = 2131165495;
    @DexIgnore
    public static /* final */ int material_emphasis_high_type; // = 2131165496;
    @DexIgnore
    public static /* final */ int material_emphasis_medium; // = 2131165497;
    @DexIgnore
    public static /* final */ int material_text_view_test_line_height; // = 2131165498;
    @DexIgnore
    public static /* final */ int material_text_view_test_line_height_override; // = 2131165499;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_background_inset_bottom; // = 2131165500;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_background_inset_end; // = 2131165501;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_background_inset_start; // = 2131165502;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_background_inset_top; // = 2131165503;
    @DexIgnore
    public static /* final */ int mtrl_alert_dialog_picker_background_inset; // = 2131165504;
    @DexIgnore
    public static /* final */ int mtrl_badge_horizontal_edge_offset; // = 2131165505;
    @DexIgnore
    public static /* final */ int mtrl_badge_long_text_horizontal_padding; // = 2131165506;
    @DexIgnore
    public static /* final */ int mtrl_badge_radius; // = 2131165507;
    @DexIgnore
    public static /* final */ int mtrl_badge_text_horizontal_edge_offset; // = 2131165508;
    @DexIgnore
    public static /* final */ int mtrl_badge_text_size; // = 2131165509;
    @DexIgnore
    public static /* final */ int mtrl_badge_with_text_radius; // = 2131165510;
    @DexIgnore
    public static /* final */ int mtrl_bottomappbar_fabOffsetEndMode; // = 2131165511;
    @DexIgnore
    public static /* final */ int mtrl_bottomappbar_fab_bottom_margin; // = 2131165512;
    @DexIgnore
    public static /* final */ int mtrl_bottomappbar_fab_cradle_margin; // = 2131165513;
    @DexIgnore
    public static /* final */ int mtrl_bottomappbar_fab_cradle_rounded_corner_radius; // = 2131165514;
    @DexIgnore
    public static /* final */ int mtrl_bottomappbar_fab_cradle_vertical_offset; // = 2131165515;
    @DexIgnore
    public static /* final */ int mtrl_bottomappbar_height; // = 2131165516;
    @DexIgnore
    public static /* final */ int mtrl_btn_corner_radius; // = 2131165517;
    @DexIgnore
    public static /* final */ int mtrl_btn_dialog_btn_min_width; // = 2131165518;
    @DexIgnore
    public static /* final */ int mtrl_btn_disabled_elevation; // = 2131165519;
    @DexIgnore
    public static /* final */ int mtrl_btn_disabled_z; // = 2131165520;
    @DexIgnore
    public static /* final */ int mtrl_btn_elevation; // = 2131165521;
    @DexIgnore
    public static /* final */ int mtrl_btn_focused_z; // = 2131165522;
    @DexIgnore
    public static /* final */ int mtrl_btn_hovered_z; // = 2131165523;
    @DexIgnore
    public static /* final */ int mtrl_btn_icon_btn_padding_left; // = 2131165524;
    @DexIgnore
    public static /* final */ int mtrl_btn_icon_padding; // = 2131165525;
    @DexIgnore
    public static /* final */ int mtrl_btn_inset; // = 2131165526;
    @DexIgnore
    public static /* final */ int mtrl_btn_letter_spacing; // = 2131165527;
    @DexIgnore
    public static /* final */ int mtrl_btn_padding_bottom; // = 2131165528;
    @DexIgnore
    public static /* final */ int mtrl_btn_padding_left; // = 2131165529;
    @DexIgnore
    public static /* final */ int mtrl_btn_padding_right; // = 2131165530;
    @DexIgnore
    public static /* final */ int mtrl_btn_padding_top; // = 2131165531;
    @DexIgnore
    public static /* final */ int mtrl_btn_pressed_z; // = 2131165532;
    @DexIgnore
    public static /* final */ int mtrl_btn_stroke_size; // = 2131165533;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_btn_icon_padding; // = 2131165534;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_btn_padding_left; // = 2131165535;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_btn_padding_right; // = 2131165536;
    @DexIgnore
    public static /* final */ int mtrl_btn_text_size; // = 2131165537;
    @DexIgnore
    public static /* final */ int mtrl_btn_z; // = 2131165538;
    @DexIgnore
    public static /* final */ int mtrl_calendar_action_height; // = 2131165539;
    @DexIgnore
    public static /* final */ int mtrl_calendar_action_padding; // = 2131165540;
    @DexIgnore
    public static /* final */ int mtrl_calendar_bottom_padding; // = 2131165541;
    @DexIgnore
    public static /* final */ int mtrl_calendar_content_padding; // = 2131165542;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_corner; // = 2131165543;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_height; // = 2131165544;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_horizontal_padding; // = 2131165545;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_today_stroke; // = 2131165546;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_vertical_padding; // = 2131165547;
    @DexIgnore
    public static /* final */ int mtrl_calendar_day_width; // = 2131165548;
    @DexIgnore
    public static /* final */ int mtrl_calendar_days_of_week_height; // = 2131165549;
    @DexIgnore
    public static /* final */ int mtrl_calendar_dialog_background_inset; // = 2131165550;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_content_padding; // = 2131165551;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_content_padding_fullscreen; // = 2131165552;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_divider_thickness; // = 2131165553;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_height; // = 2131165554;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_height_fullscreen; // = 2131165555;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_selection_line_height; // = 2131165556;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_text_padding; // = 2131165557;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_toggle_margin_bottom; // = 2131165558;
    @DexIgnore
    public static /* final */ int mtrl_calendar_header_toggle_margin_top; // = 2131165559;
    @DexIgnore
    public static /* final */ int mtrl_calendar_landscape_header_width; // = 2131165560;
    @DexIgnore
    public static /* final */ int mtrl_calendar_maximum_default_fullscreen_minor_axis; // = 2131165561;
    @DexIgnore
    public static /* final */ int mtrl_calendar_month_horizontal_padding; // = 2131165562;
    @DexIgnore
    public static /* final */ int mtrl_calendar_month_vertical_padding; // = 2131165563;
    @DexIgnore
    public static /* final */ int mtrl_calendar_navigation_bottom_padding; // = 2131165564;
    @DexIgnore
    public static /* final */ int mtrl_calendar_navigation_height; // = 2131165565;
    @DexIgnore
    public static /* final */ int mtrl_calendar_navigation_top_padding; // = 2131165566;
    @DexIgnore
    public static /* final */ int mtrl_calendar_pre_l_text_clip_padding; // = 2131165567;
    @DexIgnore
    public static /* final */ int mtrl_calendar_selection_baseline_to_top_fullscreen; // = 2131165568;
    @DexIgnore
    public static /* final */ int mtrl_calendar_selection_text_baseline_to_bottom; // = 2131165569;
    @DexIgnore
    public static /* final */ int mtrl_calendar_selection_text_baseline_to_bottom_fullscreen; // = 2131165570;
    @DexIgnore
    public static /* final */ int mtrl_calendar_selection_text_baseline_to_top; // = 2131165571;
    @DexIgnore
    public static /* final */ int mtrl_calendar_text_input_padding_top; // = 2131165572;
    @DexIgnore
    public static /* final */ int mtrl_calendar_title_baseline_to_top; // = 2131165573;
    @DexIgnore
    public static /* final */ int mtrl_calendar_title_baseline_to_top_fullscreen; // = 2131165574;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year_corner; // = 2131165575;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year_height; // = 2131165576;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year_horizontal_padding; // = 2131165577;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year_vertical_padding; // = 2131165578;
    @DexIgnore
    public static /* final */ int mtrl_calendar_year_width; // = 2131165579;
    @DexIgnore
    public static /* final */ int mtrl_card_checked_icon_margin; // = 2131165580;
    @DexIgnore
    public static /* final */ int mtrl_card_checked_icon_size; // = 2131165581;
    @DexIgnore
    public static /* final */ int mtrl_card_corner_radius; // = 2131165582;
    @DexIgnore
    public static /* final */ int mtrl_card_dragged_z; // = 2131165583;
    @DexIgnore
    public static /* final */ int mtrl_card_elevation; // = 2131165584;
    @DexIgnore
    public static /* final */ int mtrl_card_spacing; // = 2131165585;
    @DexIgnore
    public static /* final */ int mtrl_chip_pressed_translation_z; // = 2131165586;
    @DexIgnore
    public static /* final */ int mtrl_chip_text_size; // = 2131165587;
    @DexIgnore
    public static /* final */ int mtrl_exposed_dropdown_menu_popup_elevation; // = 2131165588;
    @DexIgnore
    public static /* final */ int mtrl_exposed_dropdown_menu_popup_vertical_offset; // = 2131165589;
    @DexIgnore
    public static /* final */ int mtrl_exposed_dropdown_menu_popup_vertical_padding; // = 2131165590;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_bottom_padding; // = 2131165591;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_corner_radius; // = 2131165592;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_disabled_elevation; // = 2131165593;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_disabled_translation_z; // = 2131165594;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_elevation; // = 2131165595;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_end_padding; // = 2131165596;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_end_padding_icon; // = 2131165597;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_icon_size; // = 2131165598;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_icon_text_spacing; // = 2131165599;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_min_height; // = 2131165600;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_min_width; // = 2131165601;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_start_padding; // = 2131165602;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_start_padding_icon; // = 2131165603;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_top_padding; // = 2131165604;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_translation_z_base; // = 2131165605;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_translation_z_hovered_focused; // = 2131165606;
    @DexIgnore
    public static /* final */ int mtrl_extended_fab_translation_z_pressed; // = 2131165607;
    @DexIgnore
    public static /* final */ int mtrl_fab_elevation; // = 2131165608;
    @DexIgnore
    public static /* final */ int mtrl_fab_min_touch_target; // = 2131165609;
    @DexIgnore
    public static /* final */ int mtrl_fab_translation_z_hovered_focused; // = 2131165610;
    @DexIgnore
    public static /* final */ int mtrl_fab_translation_z_pressed; // = 2131165611;
    @DexIgnore
    public static /* final */ int mtrl_high_ripple_default_alpha; // = 2131165612;
    @DexIgnore
    public static /* final */ int mtrl_high_ripple_focused_alpha; // = 2131165613;
    @DexIgnore
    public static /* final */ int mtrl_high_ripple_hovered_alpha; // = 2131165614;
    @DexIgnore
    public static /* final */ int mtrl_high_ripple_pressed_alpha; // = 2131165615;
    @DexIgnore
    public static /* final */ int mtrl_large_touch_target; // = 2131165616;
    @DexIgnore
    public static /* final */ int mtrl_low_ripple_default_alpha; // = 2131165617;
    @DexIgnore
    public static /* final */ int mtrl_low_ripple_focused_alpha; // = 2131165618;
    @DexIgnore
    public static /* final */ int mtrl_low_ripple_hovered_alpha; // = 2131165619;
    @DexIgnore
    public static /* final */ int mtrl_low_ripple_pressed_alpha; // = 2131165620;
    @DexIgnore
    public static /* final */ int mtrl_min_touch_target_size; // = 2131165621;
    @DexIgnore
    public static /* final */ int mtrl_navigation_elevation; // = 2131165622;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_horizontal_padding; // = 2131165623;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_icon_padding; // = 2131165624;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_icon_size; // = 2131165625;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_shape_horizontal_margin; // = 2131165626;
    @DexIgnore
    public static /* final */ int mtrl_navigation_item_shape_vertical_margin; // = 2131165627;
    @DexIgnore
    public static /* final */ int mtrl_shape_corner_size_large_component; // = 2131165628;
    @DexIgnore
    public static /* final */ int mtrl_shape_corner_size_medium_component; // = 2131165629;
    @DexIgnore
    public static /* final */ int mtrl_shape_corner_size_small_component; // = 2131165630;
    @DexIgnore
    public static /* final */ int mtrl_snackbar_action_text_color_alpha; // = 2131165631;
    @DexIgnore
    public static /* final */ int mtrl_snackbar_background_corner_radius; // = 2131165632;
    @DexIgnore
    public static /* final */ int mtrl_snackbar_background_overlay_color_alpha; // = 2131165633;
    @DexIgnore
    public static /* final */ int mtrl_snackbar_margin; // = 2131165634;
    @DexIgnore
    public static /* final */ int mtrl_switch_thumb_elevation; // = 2131165635;
    @DexIgnore
    public static /* final */ int mtrl_textinput_box_corner_radius_medium; // = 2131165636;
    @DexIgnore
    public static /* final */ int mtrl_textinput_box_corner_radius_small; // = 2131165637;
    @DexIgnore
    public static /* final */ int mtrl_textinput_box_label_cutout_padding; // = 2131165638;
    @DexIgnore
    public static /* final */ int mtrl_textinput_box_stroke_width_default; // = 2131165639;
    @DexIgnore
    public static /* final */ int mtrl_textinput_box_stroke_width_focused; // = 2131165640;
    @DexIgnore
    public static /* final */ int mtrl_textinput_end_icon_margin_start; // = 2131165641;
    @DexIgnore
    public static /* final */ int mtrl_textinput_outline_box_expanded_padding; // = 2131165642;
    @DexIgnore
    public static /* final */ int mtrl_textinput_start_icon_margin_end; // = 2131165643;
    @DexIgnore
    public static /* final */ int mtrl_toolbar_default_height; // = 2131165644;
    @DexIgnore
    public static /* final */ int notification_action_icon_size; // = 2131165657;
    @DexIgnore
    public static /* final */ int notification_action_text_size; // = 2131165658;
    @DexIgnore
    public static /* final */ int notification_big_circle_margin; // = 2131165659;
    @DexIgnore
    public static /* final */ int notification_content_margin_start; // = 2131165660;
    @DexIgnore
    public static /* final */ int notification_large_icon_height; // = 2131165661;
    @DexIgnore
    public static /* final */ int notification_large_icon_width; // = 2131165662;
    @DexIgnore
    public static /* final */ int notification_main_column_padding_top; // = 2131165663;
    @DexIgnore
    public static /* final */ int notification_media_narrow_margin; // = 2131165664;
    @DexIgnore
    public static /* final */ int notification_right_icon_size; // = 2131165665;
    @DexIgnore
    public static /* final */ int notification_right_side_padding_top; // = 2131165666;
    @DexIgnore
    public static /* final */ int notification_small_icon_background_padding; // = 2131165667;
    @DexIgnore
    public static /* final */ int notification_small_icon_size_as_large; // = 2131165668;
    @DexIgnore
    public static /* final */ int notification_subtext_size; // = 2131165669;
    @DexIgnore
    public static /* final */ int notification_top_pad; // = 2131165670;
    @DexIgnore
    public static /* final */ int notification_top_pad_large_text; // = 2131165671;
    @DexIgnore
    public static /* final */ int test_mtrl_calendar_day_cornerSize; // = 2131165740;
    @DexIgnore
    public static /* final */ int tooltip_corner_radius; // = 2131165741;
    @DexIgnore
    public static /* final */ int tooltip_horizontal_padding; // = 2131165742;
    @DexIgnore
    public static /* final */ int tooltip_margin; // = 2131165743;
    @DexIgnore
    public static /* final */ int tooltip_precise_anchor_extra_offset; // = 2131165744;
    @DexIgnore
    public static /* final */ int tooltip_precise_anchor_threshold; // = 2131165745;
    @DexIgnore
    public static /* final */ int tooltip_vertical_padding; // = 2131165746;
    @DexIgnore
    public static /* final */ int tooltip_y_offset_non_touch; // = 2131165747;
    @DexIgnore
    public static /* final */ int tooltip_y_offset_touch; // = 2131165748;
}
