package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vf1 extends p41 {
    @DexIgnore
    public /* final */ byte[] E;
    @DexIgnore
    public byte[] F;
    @DexIgnore
    public /* final */ rg1 G;
    @DexIgnore
    public /* final */ rg1 H;
    @DexIgnore
    public /* final */ dl1 I;
    @DexIgnore
    public /* final */ short J;

    @DexIgnore
    public vf1(dl1 dl1, short s, lx0 lx0, ue1 ue1, int i) {
        super(lx0, ue1, i);
        this.I = dl1;
        this.J = s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.I.a).putShort(this.J).array();
        wg6.a(array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.E = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(this.I.a()).putShort(this.J).array();
        wg6.a(array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.F = array2;
        rg1 rg1 = rg1.FTC;
        this.G = rg1;
        this.H = rg1;
    }

    @DexIgnore
    public final long a(sg1 sg1) {
        if (sg1.a == rg1.FTC) {
            byte[] bArr = sg1.b;
            if (bArr.length >= 9) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                short s = order.getShort(1);
                byte b2 = order.get(3);
                byte b3 = order.get(4);
                long b4 = cw0.b(order.getInt(5));
                if (b == dl1.WAITING_REQUEST.a() && this.J == s && b2 == re0.SUCCESS.b && b3 == this.I.a && b4 > 0) {
                    return b4;
                }
                return 0;
            }
        }
        return 0;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.FILE_HANDLE, (Object) cw0.a(this.J));
    }

    @DexIgnore
    public final rg1 m() {
        return this.H;
    }

    @DexIgnore
    public final byte[] o() {
        return this.E;
    }

    @DexIgnore
    public final rg1 p() {
        return this.G;
    }

    @DexIgnore
    public final byte[] r() {
        return this.F;
    }

    @DexIgnore
    public final sj0 a(byte b) {
        return re0.g.a(b);
    }
}
