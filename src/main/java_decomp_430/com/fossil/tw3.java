package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tw3 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public /* final */ InputStream f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h; // = false;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j; // = Integer.MAX_VALUE;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l; // = 100;
    @DexIgnore
    public int m; // = 67108864;
    @DexIgnore
    public a n; // = null;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public tw3(byte[] bArr, int i2, int i3, boolean z) {
        this.a = bArr;
        this.c = i3 + i2;
        this.e = i2;
        this.i = -i2;
        this.f = null;
        this.b = z;
    }

    @DexIgnore
    public static long a(long j2) {
        return (-(j2 & 1)) ^ (j2 >>> 1);
    }

    @DexIgnore
    public static tw3 a(InputStream inputStream) {
        return new tw3(inputStream, 4096);
    }

    @DexIgnore
    public sw3 b() throws IOException {
        sw3 sw3;
        int g2 = g();
        int i2 = this.c;
        int i3 = this.e;
        if (g2 <= i2 - i3 && g2 > 0) {
            if (!this.b || !this.h) {
                sw3 = sw3.copyFrom(this.a, this.e, g2);
            } else {
                sw3 = sw3.wrap(this.a, i3, g2);
            }
            this.e += g2;
            return sw3;
        } else if (g2 == 0) {
            return sw3.EMPTY;
        } else {
            return sw3.wrap(d(g2));
        }
    }

    @DexIgnore
    public int c() throws IOException {
        return g();
    }

    @DexIgnore
    public int d() throws IOException {
        return g();
    }

    @DexIgnore
    public long e() throws IOException {
        return h();
    }

    @DexIgnore
    public boolean f(int i2) throws IOException {
        int b2 = px3.b(i2);
        if (b2 == 0) {
            o();
            return true;
        } else if (b2 == 1) {
            g(8);
            return true;
        } else if (b2 == 2) {
            g(g());
            return true;
        } else if (b2 == 3) {
            n();
            a(px3.a(px3.a(i2), 4));
            return true;
        } else if (b2 == 4) {
            return false;
        } else {
            if (b2 == 5) {
                g(4);
                return true;
            }
            throw ax3.invalidWireType();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        if (r2[r3] < 0) goto L_0x006a;
     */
    @DexIgnore
    public int g() throws IOException {
        byte b2;
        int i2 = this.e;
        int i3 = this.c;
        if (i3 != i2) {
            byte[] bArr = this.a;
            int i4 = i2 + 1;
            byte b3 = bArr[i2];
            if (b3 >= 0) {
                this.e = i4;
                return b3;
            } else if (i3 - i4 >= 9) {
                int i5 = i4 + 1;
                byte b4 = b3 ^ (bArr[i4] << 7);
                if (b4 < 0) {
                    b2 = b4 ^ Byte.MIN_VALUE;
                } else {
                    int i6 = i5 + 1;
                    byte b5 = b4 ^ (bArr[i5] << 14);
                    if (b5 >= 0) {
                        b2 = b5 ^ 16256;
                    } else {
                        i5 = i6 + 1;
                        byte b6 = b5 ^ (bArr[i6] << 21);
                        if (b6 < 0) {
                            b2 = b6 ^ -2080896;
                        } else {
                            i6 = i5 + 1;
                            byte b7 = bArr[i5];
                            b2 = (b6 ^ (b7 << 28)) ^ 266354560;
                            if (b7 < 0) {
                                i5 = i6 + 1;
                                if (bArr[i6] < 0) {
                                    i6 = i5 + 1;
                                    if (bArr[i5] < 0) {
                                        i5 = i6 + 1;
                                        if (bArr[i6] < 0) {
                                            i6 = i5 + 1;
                                            if (bArr[i5] < 0) {
                                                i5 = i6 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i5 = i6;
                }
                this.e = i5;
                return b2;
            }
        }
        return (int) i();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00b4, code lost:
        if (((long) r2[r0]) < 0) goto L_0x00b6;
     */
    @DexIgnore
    public long h() throws IOException {
        long j2;
        int i2;
        long j3;
        long j4;
        byte b2;
        int i3 = this.e;
        int i4 = this.c;
        if (i4 != i3) {
            byte[] bArr = this.a;
            int i5 = i3 + 1;
            byte b3 = bArr[i3];
            if (b3 >= 0) {
                this.e = i5;
                return (long) b3;
            } else if (i4 - i5 >= 9) {
                int i6 = i5 + 1;
                byte b4 = b3 ^ (bArr[i5] << 7);
                if (b4 < 0) {
                    b2 = b4 ^ Byte.MIN_VALUE;
                } else {
                    int i7 = i6 + 1;
                    byte b5 = b4 ^ (bArr[i6] << 14);
                    if (b5 >= 0) {
                        i2 = i7;
                        j2 = (long) (b5 ^ 16256);
                    } else {
                        i6 = i7 + 1;
                        byte b6 = b5 ^ (bArr[i7] << 21);
                        if (b6 < 0) {
                            b2 = b6 ^ -2080896;
                        } else {
                            long j5 = (long) b6;
                            int i8 = i6 + 1;
                            long j6 = j5 ^ (((long) bArr[i6]) << 28);
                            if (j6 >= 0) {
                                j4 = 266354560;
                            } else {
                                int i9 = i8 + 1;
                                long j7 = j6 ^ (((long) bArr[i8]) << 35);
                                if (j7 < 0) {
                                    j3 = -34093383808L;
                                } else {
                                    i8 = i9 + 1;
                                    j6 = j7 ^ (((long) bArr[i9]) << 42);
                                    if (j6 >= 0) {
                                        j4 = 4363953127296L;
                                    } else {
                                        i9 = i8 + 1;
                                        j7 = j6 ^ (((long) bArr[i8]) << 49);
                                        if (j7 < 0) {
                                            j3 = -558586000294016L;
                                        } else {
                                            int i10 = i9 + 1;
                                            long j8 = (j7 ^ (((long) bArr[i9]) << 56)) ^ 71499008037633920L;
                                            if (j8 < 0) {
                                                i2 = i10 + 1;
                                            } else {
                                                i2 = i10;
                                            }
                                            j2 = j8;
                                        }
                                    }
                                }
                                j2 = j7 ^ j3;
                            }
                            j2 = j6 ^ j4;
                            i2 = i8;
                        }
                    }
                    this.e = i2;
                    return j2;
                }
                j2 = (long) b2;
                this.e = i2;
                return j2;
            }
        }
        return i();
    }

    @DexIgnore
    public long i() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte f2 = f();
            j2 |= ((long) (f2 & Byte.MAX_VALUE)) << i2;
            if ((f2 & 128) == 0) {
                return j2;
            }
        }
        throw ax3.malformedVarint();
    }

    @DexIgnore
    public long j() throws IOException {
        return a(h());
    }

    @DexIgnore
    public String k() throws IOException {
        byte[] bArr;
        int g2 = g();
        int i2 = this.e;
        int i3 = 0;
        if (g2 <= this.c - i2 && g2 > 0) {
            bArr = this.a;
            this.e = i2 + g2;
            i3 = i2;
        } else if (g2 == 0) {
            return "";
        } else {
            if (g2 <= this.c) {
                e(g2);
                bArr = this.a;
                this.e = g2 + 0;
            } else {
                bArr = d(g2);
            }
        }
        if (ox3.c(bArr, i3, i3 + g2)) {
            return new String(bArr, i3, g2, zw3.a);
        }
        throw ax3.invalidUtf8();
    }

    @DexIgnore
    public int l() throws IOException {
        if (a()) {
            this.g = 0;
            return 0;
        }
        this.g = g();
        if (px3.a(this.g) != 0) {
            return this.g;
        }
        throw ax3.invalidTag();
    }

    @DexIgnore
    public final void m() {
        this.c += this.d;
        int i2 = this.i;
        int i3 = this.c;
        int i4 = i2 + i3;
        int i5 = this.j;
        if (i4 > i5) {
            this.d = i4 - i5;
            this.c = i3 - this.d;
            return;
        }
        this.d = 0;
    }

    /*  JADX ERROR: StackOverflow in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @DexIgnore
    public void n() throws java.io.IOException {
        /*
            r1 = this;
        L_0x0000:
            int r0 = r1.l()
            if (r0 == 0) goto L_0x000c
            boolean r0 = r1.f(r0)
            if (r0 != 0) goto L_0x0000
        L_0x000c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tw3.n():void");
    }

    @DexIgnore
    public final void o() throws IOException {
        int i2 = this.c;
        int i3 = this.e;
        if (i2 - i3 >= 10) {
            byte[] bArr = this.a;
            int i4 = 0;
            while (i4 < 10) {
                int i5 = i3 + 1;
                if (bArr[i3] >= 0) {
                    this.e = i5;
                    return;
                } else {
                    i4++;
                    i3 = i5;
                }
            }
        }
        p();
    }

    @DexIgnore
    public final void p() throws IOException {
        int i2 = 0;
        while (i2 < 10) {
            if (f() < 0) {
                i2++;
            } else {
                return;
            }
        }
        throw ax3.malformedVarint();
    }

    @DexIgnore
    public static tw3 a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public int c(int i2) throws ax3 {
        if (i2 >= 0) {
            int i3 = i2 + this.i + this.e;
            int i4 = this.j;
            if (i3 <= i4) {
                this.j = i3;
                m();
                return i4;
            }
            throw ax3.truncatedMessage();
        }
        throw ax3.negativeSize();
    }

    @DexIgnore
    public final byte[] d(int i2) throws IOException {
        if (i2 > 0) {
            int i3 = this.i;
            int i4 = this.e;
            int i5 = i3 + i4 + i2;
            if (i5 <= this.m) {
                int i6 = this.j;
                if (i5 <= i6) {
                    InputStream inputStream = this.f;
                    if (inputStream != null) {
                        int i7 = this.c;
                        int i8 = i7 - i4;
                        this.i = i3 + i7;
                        this.e = 0;
                        this.c = 0;
                        int i9 = i2 - i8;
                        if (i9 < 4096 || i9 <= inputStream.available()) {
                            byte[] bArr = new byte[i2];
                            System.arraycopy(this.a, i4, bArr, 0, i8);
                            while (i8 < bArr.length) {
                                int read = this.f.read(bArr, i8, i2 - i8);
                                if (read != -1) {
                                    this.i += read;
                                    i8 += read;
                                } else {
                                    throw ax3.truncatedMessage();
                                }
                            }
                            return bArr;
                        }
                        ArrayList<byte[]> arrayList = new ArrayList<>();
                        while (i9 > 0) {
                            byte[] bArr2 = new byte[Math.min(i9, 4096)];
                            int i10 = 0;
                            while (i10 < bArr2.length) {
                                int read2 = this.f.read(bArr2, i10, bArr2.length - i10);
                                if (read2 != -1) {
                                    this.i += read2;
                                    i10 += read2;
                                } else {
                                    throw ax3.truncatedMessage();
                                }
                            }
                            i9 -= bArr2.length;
                            arrayList.add(bArr2);
                        }
                        byte[] bArr3 = new byte[i2];
                        System.arraycopy(this.a, i4, bArr3, 0, i8);
                        for (byte[] bArr4 : arrayList) {
                            System.arraycopy(bArr4, 0, bArr3, i8, bArr4.length);
                            i8 += bArr4.length;
                        }
                        return bArr3;
                    }
                    throw ax3.truncatedMessage();
                }
                g((i6 - i3) - i4);
                throw ax3.truncatedMessage();
            }
            throw ax3.sizeLimitExceeded();
        } else if (i2 == 0) {
            return zw3.b;
        } else {
            throw ax3.negativeSize();
        }
    }

    @DexIgnore
    public final void e(int i2) throws IOException {
        if (!i(i2)) {
            throw ax3.truncatedMessage();
        }
    }

    @DexIgnore
    public static tw3 a(byte[] bArr, int i2, int i3) {
        return a(bArr, i2, i3, false);
    }

    @DexIgnore
    public final boolean i(int i2) throws IOException {
        int i3 = this.e;
        if (i3 + i2 <= this.c) {
            throw new IllegalStateException("refillBuffer() called when " + i2 + " bytes were already available in buffer");
        } else if (this.i + i3 + i2 > this.j) {
            return false;
        } else {
            a aVar = this.n;
            if (aVar != null) {
                aVar.a();
            }
            if (this.f != null) {
                int i4 = this.e;
                if (i4 > 0) {
                    int i5 = this.c;
                    if (i5 > i4) {
                        byte[] bArr = this.a;
                        System.arraycopy(bArr, i4, bArr, 0, i5 - i4);
                    }
                    this.i += i4;
                    this.c -= i4;
                    this.e = 0;
                }
                InputStream inputStream = this.f;
                byte[] bArr2 = this.a;
                int i6 = this.c;
                int read = inputStream.read(bArr2, i6, bArr2.length - i6);
                if (read == 0 || read < -1 || read > this.a.length) {
                    throw new IllegalStateException("InputStream#read(byte[]) returned invalid result: " + read + "\nThe InputStream implementation is buggy.");
                } else if (read > 0) {
                    this.c += read;
                    if ((this.i + i2) - this.m <= 0) {
                        m();
                        if (this.c >= i2) {
                            return true;
                        }
                        return i(i2);
                    }
                    throw ax3.sizeLimitExceeded();
                }
            }
            return false;
        }
    }

    @DexIgnore
    public static tw3 a(byte[] bArr, int i2, int i3, boolean z) {
        tw3 tw3 = new tw3(bArr, i2, i3, z);
        try {
            tw3.c(i3);
            return tw3;
        } catch (ax3 e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public void a(int i2) throws ax3 {
        if (this.g != i2) {
            throw ax3.invalidEndTag();
        }
    }

    @DexIgnore
    public <T extends dx3> T a(gx3<T> gx3, ww3 ww3) throws IOException {
        int g2 = g();
        if (this.k < this.l) {
            int c2 = c(g2);
            this.k++;
            T t = (dx3) gx3.a(this, ww3);
            a(0);
            this.k--;
            b(c2);
            return t;
        }
        throw ax3.recursionLimitExceeded();
    }

    @DexIgnore
    public void b(int i2) {
        this.j = i2;
        m();
    }

    @DexIgnore
    public byte f() throws IOException {
        if (this.e == this.c) {
            e(1);
        }
        byte[] bArr = this.a;
        int i2 = this.e;
        this.e = i2 + 1;
        return bArr[i2];
    }

    @DexIgnore
    public tw3(InputStream inputStream, int i2) {
        this.a = new byte[i2];
        this.e = 0;
        this.i = 0;
        this.f = inputStream;
        this.b = false;
    }

    @DexIgnore
    public void g(int i2) throws IOException {
        int i3 = this.c;
        int i4 = this.e;
        if (i2 > i3 - i4 || i2 < 0) {
            h(i2);
        } else {
            this.e = i4 + i2;
        }
    }

    @DexIgnore
    public final void h(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.i;
            int i4 = this.e;
            int i5 = i3 + i4 + i2;
            int i6 = this.j;
            if (i5 <= i6) {
                int i7 = this.c;
                int i8 = i7 - i4;
                this.e = i7;
                e(1);
                while (true) {
                    int i9 = i2 - i8;
                    int i10 = this.c;
                    if (i9 > i10) {
                        i8 += i10;
                        this.e = i10;
                        e(1);
                    } else {
                        this.e = i9;
                        return;
                    }
                }
            } else {
                g((i6 - i3) - i4);
                throw ax3.truncatedMessage();
            }
        } else {
            throw ax3.negativeSize();
        }
    }

    @DexIgnore
    public boolean a() throws IOException {
        return this.e == this.c && !i(1);
    }
}
