package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i04 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public i04(byte[] bArr, byte[] bArr2) {
        this.a = bArr;
        this.b = bArr2;
    }

    @DexIgnore
    public byte[] a() {
        return this.a;
    }

    @DexIgnore
    public byte[] b() {
        return this.b;
    }
}
