package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl1 extends lj1 {
    @DexIgnore
    public byte[] V;
    @DexIgnore
    public long W;
    @DexIgnore
    public long X;
    @DexIgnore
    public /* final */ long Y;
    @DexIgnore
    public /* final */ long Z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ fl1(short s, long j, long j2, ue1 ue1, int i, int i2) {
        super(dl1.GET_FILE, s, lx0.GET_FILE, ue1, (i2 & 16) != 0 ? 3 : i);
        this.Y = j;
        this.Z = j2;
        this.V = new byte[0];
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.V = bArr;
    }

    @DexIgnore
    public void c(long j) {
        this.X = j;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.OFFSET, (Object) Long.valueOf(this.Y)), bm0.LENGTH, (Object) Long.valueOf(this.Z));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.Y).putInt((int) this.Z).array();
        wg6.a(array, "ByteBuffer.allocate(8).o\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public long s() {
        return this.W;
    }

    @DexIgnore
    public long t() {
        return this.X;
    }

    @DexIgnore
    public byte[] u() {
        return this.V;
    }

    @DexIgnore
    public void b(long j) {
        this.W = j;
    }
}
