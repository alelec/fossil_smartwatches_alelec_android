package com.fossil;

import android.os.IBinder;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nz1 implements IBinder.DeathRecipient, mz1 {
    @DexIgnore
    public /* final */ WeakReference<BasePendingResult<?>> a;
    @DexIgnore
    public /* final */ WeakReference<p02> b;
    @DexIgnore
    public /* final */ WeakReference<IBinder> c;

    @DexIgnore
    public nz1(BasePendingResult<?> basePendingResult, p02 p02, IBinder iBinder) {
        this.b = new WeakReference<>(p02);
        this.a = new WeakReference<>(basePendingResult);
        this.c = new WeakReference<>(iBinder);
    }

    @DexIgnore
    public final void a(BasePendingResult<?> basePendingResult) {
        a();
    }

    @DexIgnore
    public final void binderDied() {
        a();
    }

    @DexIgnore
    public final void a() {
        BasePendingResult basePendingResult = (BasePendingResult) this.a.get();
        p02 p02 = (p02) this.b.get();
        if (!(p02 == null || basePendingResult == null)) {
            p02.a(basePendingResult.e().intValue());
        }
        IBinder iBinder = (IBinder) this.c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    @DexIgnore
    public /* synthetic */ nz1(BasePendingResult basePendingResult, p02 p02, IBinder iBinder, kz1 kz1) {
        this(basePendingResult, (p02) null, iBinder);
    }
}
