package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm6 extends jk6 {
    @DexIgnore
    public /* final */ am6 a;

    @DexIgnore
    public bm6(am6 am6) {
        wg6.b(am6, "handle");
        this.a = am6;
    }

    @DexIgnore
    public void a(Throwable th) {
        this.a.dispose();
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        a((Throwable) obj);
        return cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "DisposeOnCancel[" + this.a + ']';
    }
}
