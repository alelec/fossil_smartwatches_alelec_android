package com.fossil;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface va5 extends xt4<ua5> {
    @DexIgnore
    void A();

    @DexIgnore
    void E();

    @DexIgnore
    void Y();

    @DexIgnore
    void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z);

    @DexIgnore
    void a(String str, String str2);

    @DexIgnore
    void a(Date date);

    @DexIgnore
    void a(boolean z, boolean z2, boolean z3);

    @DexIgnore
    void a0();

    @DexIgnore
    void b(boolean z);

    @DexIgnore
    void b0();

    @DexIgnore
    void c(int i);

    @DexIgnore
    void i(boolean z);

    @DexIgnore
    void o(boolean z);
}
