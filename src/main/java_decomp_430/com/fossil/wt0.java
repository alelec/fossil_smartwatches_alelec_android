package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wt0 extends j61 {
    @DexIgnore
    public long A;
    @DexIgnore
    public m51 B; // = m51.DISCONNECTED;

    @DexIgnore
    public wt0(ue1 ue1, long j) {
        super(lx0.CONNECT_HID, ue1);
        this.A = j;
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public void a(ni1 ni1) {
    }

    @DexIgnore
    public long e() {
        return this.A;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.CURRENT_HID_STATE, (Object) cw0.a((Enum<?>) this.y.e())), bm0.MAC_ADDRESS, (Object) this.y.t);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NEW_HID_STATE, (Object) cw0.a((Enum<?>) this.B));
    }

    @DexIgnore
    public ok0 l() {
        return new ct0(this.y.v);
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.B = ((ct0) ok0).k;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.NEW_HID_STATE, (Object) cw0.a((Enum<?>) this.B)), 7));
    }
}
