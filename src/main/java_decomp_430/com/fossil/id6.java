package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class id6<E> extends fd6<E> implements List<E>, ph6 {
    @DexIgnore
    public static /* final */ a a; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(int i, int i2) {
            if (i < 0 || i >= i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public final void b(int i, int i2) {
            if (i < 0 || i > i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            if (i < 0 || i2 > i3) {
                throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2 + ", size: " + i3);
            } else if (i > i2) {
                throw new IllegalArgumentException("fromIndex: " + i + " > toIndex: " + i2);
            }
        }

        @DexIgnore
        public final int a(Collection<?> collection) {
            wg6.b(collection, "c");
            Iterator<?> it = collection.iterator();
            int i = 1;
            while (it.hasNext()) {
                Object next = it.next();
                i = (i * 31) + (next != null ? next.hashCode() : 0);
            }
            return i;
        }

        @DexIgnore
        public final boolean a(Collection<?> collection, Collection<?> collection2) {
            wg6.b(collection, "c");
            wg6.b(collection2, "other");
            if (collection.size() != collection2.size()) {
                return false;
            }
            Iterator<?> it = collection2.iterator();
            for (Object a : collection) {
                if (!wg6.a((Object) a, (Object) it.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Iterator<E>, ph6 {
        @DexIgnore
        public int a;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < id6.this.size();
        }

        @DexIgnore
        public E next() {
            if (hasNext()) {
                id6 id6 = id6.this;
                int i = this.a;
                this.a = i + 1;
                return id6.get(i);
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends id6<E>.b implements ListIterator<E>, ph6 {
        @DexIgnore
        public c(int i) {
            super();
            id6.a.b(i, id6.this.size());
            a(i);
        }

        @DexIgnore
        public void add(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public boolean hasPrevious() {
            return a() > 0;
        }

        @DexIgnore
        public int nextIndex() {
            return a();
        }

        @DexIgnore
        public E previous() {
            if (hasPrevious()) {
                id6 id6 = id6.this;
                a(a() - 1);
                return id6.get(a());
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return a() - 1;
        }

        @DexIgnore
        public void set(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<E> extends id6<E> implements RandomAccess {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ id6<E> c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore
        public d(id6<? extends E> id6, int i, int i2) {
            wg6.b(id6, "list");
            this.c = id6;
            this.d = i;
            id6.a.a(this.d, i2, this.c.size());
            this.b = i2 - this.d;
        }

        @DexIgnore
        public int a() {
            return this.b;
        }

        @DexIgnore
        public E get(int i) {
            id6.a.a(i, this.b);
            return this.c.get(this.d + i);
        }
    }

    @DexIgnore
    public void add(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        return a.a((Collection<?>) this, (Collection<?>) (Collection) obj);
    }

    @DexIgnore
    public abstract E get(int i);

    @DexIgnore
    public int hashCode() {
        return a.a(this);
    }

    @DexIgnore
    public int indexOf(Object obj) {
        int i = 0;
        for (Object a2 : this) {
            if (wg6.a(a2, obj)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public Iterator<E> iterator() {
        return new b();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        ListIterator listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (wg6.a(listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    @DexIgnore
    public ListIterator<E> listIterator() {
        return new c(0);
    }

    @DexIgnore
    public E remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public E set(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public List<E> subList(int i, int i2) {
        return new d(this, i, i2);
    }

    @DexIgnore
    public ListIterator<E> listIterator(int i) {
        return new c(i);
    }
}
