package com.fossil;

import dagger.internal.Factory;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x04 implements Factory<w04> {
    @DexIgnore
    public /* final */ Provider<Map<Class<? extends td>, Provider<td>>> a;

    @DexIgnore
    public x04(Provider<Map<Class<? extends td>, Provider<td>>> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static x04 a(Provider<Map<Class<? extends td>, Provider<td>>> provider) {
        return new x04(provider);
    }

    @DexIgnore
    public static w04 b(Provider<Map<Class<? extends td>, Provider<td>>> provider) {
        return new w04(provider.get());
    }

    @DexIgnore
    public w04 get() {
        return b(this.a);
    }
}
