package com.fossil;

import com.fossil.bm3;
import com.fossil.zl3;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.security.AccessControlException;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp3 {
    @DexIgnore
    public static /* final */ ck3<Type, String> a; // = new a();
    @DexIgnore
    public static /* final */ ek3 b; // = ek3.c(", ").a("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ck3<Type, String> {
        @DexIgnore
        /* renamed from: a */
        public String apply(Type type) {
            return e.CURRENT.typeName(type);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends gp3 {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicReference b;

        @DexIgnore
        public b(AtomicReference atomicReference) {
            this.b = atomicReference;
        }

        @DexIgnore
        public void a(TypeVariable<?> typeVariable) {
            this.b.set(hp3.b(typeVariable.getBounds()));
        }

        @DexIgnore
        public void a(WildcardType wildcardType) {
            this.b.set(hp3.b(wildcardType.getUpperBounds()));
        }

        @DexIgnore
        public void a(GenericArrayType genericArrayType) {
            this.b.set(genericArrayType.getGenericComponentType());
        }

        @DexIgnore
        public void a(Class<?> cls) {
            this.b.set(cls.getComponentType());
        }
    }

    @DexIgnore
    public enum c {
        OWNED_BY_ENCLOSING_CLASS {
            @DexIgnore
            public Class<?> getOwnerType(Class<?> cls) {
                return cls.getEnclosingClass();
            }
        },
        LOCAL_CLASS_HAS_NO_OWNER {
            @DexIgnore
            public Class<?> getOwnerType(Class<?> cls) {
                if (cls.isLocalClass()) {
                    return null;
                }
                return cls.getEnclosingClass();
            }
        };
        
        @DexIgnore
        public static /* final */ c JVM_BEHAVIOR; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b<T> {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class d extends b<String> {
        }

        /*
        static {
            JVM_BEHAVIOR = a();
        }
        */

        @DexIgnore
        public static c a() {
            new d();
            ParameterizedType parameterizedType = (ParameterizedType) d.class.getGenericSuperclass();
            for (c cVar : values()) {
                if (cVar.getOwnerType(b.class) == parameterizedType.getOwnerType()) {
                    return cVar;
                }
            }
            throw new AssertionError();
        }

        @DexIgnore
        public abstract Class<?> getOwnerType(Class<?> cls);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements GenericArrayType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Type componentType;

        @DexIgnore
        public d(Type type) {
            this.componentType = e.CURRENT.usedInGenericType(type);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof GenericArrayType) {
                return gk3.a(getGenericComponentType(), ((GenericArrayType) obj).getGenericComponentType());
            }
            return false;
        }

        @DexIgnore
        public Type getGenericComponentType() {
            return this.componentType;
        }

        @DexIgnore
        public int hashCode() {
            return this.componentType.hashCode();
        }

        @DexIgnore
        public String toString() {
            return hp3.e(this.componentType) + "[]";
        }
    }

    @DexIgnore
    public enum e {
        JAVA6 {
            @DexIgnore
            public Type usedInGenericType(Type type) {
                jk3.a(type);
                if (!(type instanceof Class)) {
                    return type;
                }
                Class cls = (Class) type;
                return cls.isArray() ? new d(cls.getComponentType()) : type;
            }

            @DexIgnore
            public GenericArrayType newArrayType(Type type) {
                return new d(type);
            }
        },
        JAVA7 {
            @DexIgnore
            public Type newArrayType(Type type) {
                if (type instanceof Class) {
                    return hp3.a((Class<?>) (Class) type);
                }
                return new d(type);
            }

            @DexIgnore
            public Type usedInGenericType(Type type) {
                jk3.a(type);
                return type;
            }
        },
        JAVA8 {
            @DexIgnore
            public Type newArrayType(Type type) {
                return e.JAVA7.newArrayType(type);
            }

            @DexIgnore
            public String typeName(Type type) {
                try {
                    return (String) Type.class.getMethod("getTypeName", new Class[0]).invoke(type, new Object[0]);
                } catch (NoSuchMethodException unused) {
                    throw new AssertionError("Type.getTypeName should be available in Java 8");
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                }
            }

            @DexIgnore
            public Type usedInGenericType(Type type) {
                return e.JAVA7.usedInGenericType(type);
            }
        };
        
        @DexIgnore
        public static /* final */ e CURRENT; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class d extends cp3<int[]> {
        }

        @DexIgnore
        public abstract Type newArrayType(Type type);

        @DexIgnore
        public String typeName(Type type) {
            return hp3.e(type);
        }

        @DexIgnore
        public final zl3<Type> usedInGenericType(Type[] typeArr) {
            zl3.b builder = zl3.builder();
            for (Type usedInGenericType : typeArr) {
                builder.a((Object) usedInGenericType(usedInGenericType));
            }
            return builder.a();
        }

        @DexIgnore
        public abstract Type usedInGenericType(Type type);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<X> {
        @DexIgnore
        public static /* final */ boolean a;

        /*
        static {
            Class<f> cls = f.class;
            a = !cls.getTypeParameters()[0].equals(hp3.a(cls, "X", new Type[0]));
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ParameterizedType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ zl3<Type> argumentsList;
        @DexIgnore
        public /* final */ Type ownerType;
        @DexIgnore
        public /* final */ Class<?> rawType;

        @DexIgnore
        public g(Type type, Class<?> cls, Type[] typeArr) {
            jk3.a(cls);
            jk3.a(typeArr.length == cls.getTypeParameters().length);
            hp3.b(typeArr, "type parameter");
            this.ownerType = type;
            this.rawType = cls;
            this.argumentsList = e.CURRENT.usedInGenericType(typeArr);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) obj;
            if (!getRawType().equals(parameterizedType.getRawType()) || !gk3.a(getOwnerType(), parameterizedType.getOwnerType()) || !Arrays.equals(getActualTypeArguments(), parameterizedType.getActualTypeArguments())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public Type[] getActualTypeArguments() {
            return hp3.b((Collection<Type>) this.argumentsList);
        }

        @DexIgnore
        public Type getOwnerType() {
            return this.ownerType;
        }

        @DexIgnore
        public Type getRawType() {
            return this.rawType;
        }

        @DexIgnore
        public int hashCode() {
            Type type = this.ownerType;
            return ((type == null ? 0 : type.hashCode()) ^ this.argumentsList.hashCode()) ^ this.rawType.hashCode();
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Type type = this.ownerType;
            if (type != null) {
                sb.append(e.CURRENT.typeName(type));
                sb.append('.');
            }
            sb.append(this.rawType.getName());
            sb.append('<');
            sb.append(hp3.b.a((Iterable<?>) pm3.a(this.argumentsList, hp3.a)));
            sb.append('>');
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<D extends GenericDeclaration> {
        @DexIgnore
        public /* final */ D a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ zl3<Type> c;

        @DexIgnore
        public h(D d, String str, Type[] typeArr) {
            hp3.b(typeArr, "bound for type variable");
            jk3.a(d);
            this.a = (GenericDeclaration) d;
            jk3.a(str);
            this.b = str;
            this.c = zl3.copyOf((E[]) typeArr);
        }

        @DexIgnore
        public D a() {
            return this.a;
        }

        @DexIgnore
        public String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (f.a) {
                if (obj == null || !Proxy.isProxyClass(obj.getClass()) || !(Proxy.getInvocationHandler(obj) instanceof i)) {
                    return false;
                }
                h a2 = ((i) Proxy.getInvocationHandler(obj)).a;
                if (!this.b.equals(a2.b()) || !this.a.equals(a2.a()) || !this.c.equals(a2.c)) {
                    return false;
                }
                return true;
            } else if (!(obj instanceof TypeVariable)) {
                return false;
            } else {
                TypeVariable typeVariable = (TypeVariable) obj;
                if (!this.b.equals(typeVariable.getName()) || !this.a.equals(typeVariable.getGenericDeclaration())) {
                    return false;
                }
                return true;
            }
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements InvocationHandler {
        @DexIgnore
        public static /* final */ bm3<String, Method> b;
        @DexIgnore
        public /* final */ h<?> a;

        /*
        static {
            Class<h> cls = h.class;
            bm3.b builder = bm3.builder();
            for (Method method : cls.getMethods()) {
                if (method.getDeclaringClass().equals(cls)) {
                    try {
                        method.setAccessible(true);
                    } catch (AccessControlException unused) {
                    }
                    builder.a(method.getName(), method);
                }
            }
            b = builder.a();
        }
        */

        @DexIgnore
        public i(h<?> hVar) {
            this.a = hVar;
        }

        @DexIgnore
        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Method method2 = b.get(name);
            if (method2 != null) {
                try {
                    return method2.invoke(this.a, objArr);
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            } else {
                throw new UnsupportedOperationException(name);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements WildcardType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ zl3<Type> lowerBounds;
        @DexIgnore
        public /* final */ zl3<Type> upperBounds;

        @DexIgnore
        public j(Type[] typeArr, Type[] typeArr2) {
            hp3.b(typeArr, "lower bound for wildcard");
            hp3.b(typeArr2, "upper bound for wildcard");
            this.lowerBounds = e.CURRENT.usedInGenericType(typeArr);
            this.upperBounds = e.CURRENT.usedInGenericType(typeArr2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) obj;
            if (!this.lowerBounds.equals(Arrays.asList(wildcardType.getLowerBounds())) || !this.upperBounds.equals(Arrays.asList(wildcardType.getUpperBounds()))) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public Type[] getLowerBounds() {
            return hp3.b((Collection<Type>) this.lowerBounds);
        }

        @DexIgnore
        public Type[] getUpperBounds() {
            return hp3.b((Collection<Type>) this.upperBounds);
        }

        @DexIgnore
        public int hashCode() {
            return this.lowerBounds.hashCode() ^ this.upperBounds.hashCode();
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder("?");
            jo3<Type> it = this.lowerBounds.iterator();
            while (it.hasNext()) {
                sb.append(" super ");
                sb.append(e.CURRENT.typeName(it.next()));
            }
            for (Type typeName : hp3.b((Iterable<Type>) this.upperBounds)) {
                sb.append(" extends ");
                sb.append(e.CURRENT.typeName(typeName));
            }
            return sb.toString();
        }
    }

    @DexIgnore
    public static WildcardType c(Type type) {
        return new j(new Type[0], new Type[]{type});
    }

    @DexIgnore
    public static WildcardType d(Type type) {
        return new j(new Type[]{type}, new Type[]{Object.class});
    }

    @DexIgnore
    public static String e(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    @DexIgnore
    public static Type b(Type type) {
        if (!(type instanceof WildcardType)) {
            return e.CURRENT.newArrayType(type);
        }
        WildcardType wildcardType = (WildcardType) type;
        Type[] lowerBounds = wildcardType.getLowerBounds();
        boolean z = true;
        jk3.a(lowerBounds.length <= 1, (Object) "Wildcard cannot have more than one lower bounds.");
        if (lowerBounds.length == 1) {
            return d(b(lowerBounds[0]));
        }
        Type[] upperBounds = wildcardType.getUpperBounds();
        if (upperBounds.length != 1) {
            z = false;
        }
        jk3.a(z, (Object) "Wildcard should have only one upper bound.");
        return c(b(upperBounds[0]));
    }

    @DexIgnore
    public static ParameterizedType a(Type type, Class<?> cls, Type... typeArr) {
        if (type == null) {
            return a(cls, typeArr);
        }
        jk3.a(typeArr);
        jk3.a(cls.getEnclosingClass() != null, "Owner type for unenclosed %s", (Object) cls);
        return new g(type, cls, typeArr);
    }

    @DexIgnore
    public static ParameterizedType a(Class<?> cls, Type... typeArr) {
        return new g(c.JVM_BEHAVIOR.getOwnerType(cls), cls, typeArr);
    }

    @DexIgnore
    public static <D extends GenericDeclaration> TypeVariable<D> a(D d2, String str, Type... typeArr) {
        if (typeArr.length == 0) {
            typeArr = new Type[]{Object.class};
        }
        return b(d2, str, typeArr);
    }

    @DexIgnore
    public static Type a(Type type) {
        jk3.a(type);
        AtomicReference atomicReference = new AtomicReference();
        new b(atomicReference).a(type);
        return (Type) atomicReference.get();
    }

    @DexIgnore
    public static Type b(Type[] typeArr) {
        for (Type a2 : typeArr) {
            Type a3 = a(a2);
            if (a3 != null) {
                if (a3 instanceof Class) {
                    Class cls = (Class) a3;
                    if (cls.isPrimitive()) {
                        return cls;
                    }
                }
                return c(a3);
            }
        }
        return null;
    }

    @DexIgnore
    public static Class<?> a(Class<?> cls) {
        return Array.newInstance(cls, 0).getClass();
    }

    @DexIgnore
    public static <D extends GenericDeclaration> TypeVariable<D> b(D d2, String str, Type[] typeArr) {
        return (TypeVariable) bp3.a(TypeVariable.class, new i(new h(d2, str, typeArr)));
    }

    @DexIgnore
    public static Type[] b(Collection<Type> collection) {
        return (Type[]) collection.toArray(new Type[collection.size()]);
    }

    @DexIgnore
    public static Iterable<Type> b(Iterable<Type> iterable) {
        return pm3.b(iterable, lk3.a(lk3.a(Object.class)));
    }

    @DexIgnore
    public static void b(Type[] typeArr, String str) {
        for (Class cls : typeArr) {
            if (cls instanceof Class) {
                Class cls2 = cls;
                jk3.a(!cls2.isPrimitive(), "Primitive type '%s' used as %s", (Object) cls2, (Object) str);
            }
        }
    }
}
