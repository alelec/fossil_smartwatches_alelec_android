package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.fossil.e12;
import com.fossil.rv1;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g02 implements wy1 {
    @DexIgnore
    public /* final */ Map<rv1.c<?>, h02<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<rv1.c<?>, h02<?>> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<rv1<?>, Boolean> c;
    @DexIgnore
    public /* final */ qw1 d;
    @DexIgnore
    public /* final */ ay1 e;
    @DexIgnore
    public /* final */ Lock f;
    @DexIgnore
    public /* final */ Looper g;
    @DexIgnore
    public /* final */ kv1 h;
    @DexIgnore
    public /* final */ Condition i;
    @DexIgnore
    public /* final */ e12 j;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public /* final */ boolean p;
    @DexIgnore
    public /* final */ Queue<nw1<?, ?>> q; // = new LinkedList();
    @DexIgnore
    public boolean r;
    @DexIgnore
    public Map<lw1<?>, gv1> s;
    @DexIgnore
    public Map<lw1<?>, gv1> t;
    @DexIgnore
    public ex1 u;
    @DexIgnore
    public gv1 v;

    @DexIgnore
    public g02(Context context, Lock lock, Looper looper, kv1 kv1, Map<rv1.c<?>, rv1.f> map, e12 e12, Map<rv1<?>, Boolean> map2, rv1.a<? extends ac3, lb3> aVar, ArrayList<a02> arrayList, ay1 ay1, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.f = lock;
        this.g = looper;
        this.i = lock.newCondition();
        this.h = kv1;
        this.e = ay1;
        this.c = map2;
        this.j = e12;
        this.o = z;
        HashMap hashMap = new HashMap();
        for (rv1 next : map2.keySet()) {
            hashMap.put(next.a(), next);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            a02 a02 = arrayList.get(i2);
            i2++;
            a02 a022 = a02;
            hashMap2.put(a022.a, a022);
        }
        boolean z5 = true;
        boolean z6 = false;
        boolean z7 = true;
        boolean z8 = false;
        for (Map.Entry next2 : map.entrySet()) {
            rv1 rv1 = (rv1) hashMap.get(next2.getKey());
            rv1.f fVar = (rv1.f) next2.getValue();
            if (fVar.i()) {
                z3 = z7;
                z4 = !this.c.get(rv1).booleanValue() ? true : z8;
                z2 = true;
            } else {
                z2 = z6;
                z4 = z8;
                z3 = false;
            }
            h02 h02 = r1;
            h02 h022 = new h02(context, rv1, looper, fVar, (a02) hashMap2.get(rv1), e12, aVar);
            this.a.put((rv1.c) next2.getKey(), h02);
            if (fVar.m()) {
                this.b.put((rv1.c) next2.getKey(), h02);
            }
            z8 = z4;
            z7 = z3;
            z6 = z2;
        }
        this.p = (!z6 || z7 || z8) ? false : z5;
        this.d = qw1.e();
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t2) {
        rv1.c i2 = t2.i();
        if (this.o && c(t2)) {
            return t2;
        }
        this.e.y.a(t2);
        this.a.get(i2).b(t2);
        return t2;
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t2) {
        if (this.o && c(t2)) {
            return t2;
        }
        if (!c()) {
            this.q.add(t2);
            return t2;
        }
        this.e.y.a(t2);
        this.a.get(t2.i()).a(t2);
        return t2;
    }

    @DexIgnore
    public final <T extends nw1<? extends ew1, ? extends rv1.b>> boolean c(T t2) {
        rv1.c i2 = t2.i();
        gv1 a2 = a((rv1.c<?>) i2);
        if (a2 == null || a2.p() != 4) {
            return false;
        }
        t2.c(new Status(4, (String) null, this.d.a((lw1<?>) this.a.get(i2).a(), System.identityHashCode(this.e))));
        return true;
    }

    @DexIgnore
    public final void d() {
        this.f.lock();
        try {
            this.d.a();
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            if (this.t == null) {
                this.t = new p4(this.b.size());
            }
            gv1 gv1 = new gv1(4);
            for (h02<?> a2 : this.b.values()) {
                this.t.put(a2.a(), gv1);
            }
            if (this.s != null) {
                this.s.putAll(this.t);
            }
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final void e() {
    }

    @DexIgnore
    public final gv1 f() {
        b();
        while (g()) {
            try {
                this.i.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new gv1(15, (PendingIntent) null);
            }
        }
        if (c()) {
            return gv1.e;
        }
        gv1 gv1 = this.v;
        if (gv1 != null) {
            return gv1;
        }
        return new gv1(13, (PendingIntent) null);
    }

    @DexIgnore
    public final boolean g() {
        this.f.lock();
        try {
            return this.s == null && this.r;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final void h() {
        e12 e12 = this.j;
        if (e12 == null) {
            this.e.q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(e12.i());
        Map<rv1<?>, e12.b> f2 = this.j.f();
        for (rv1 next : f2.keySet()) {
            gv1 a2 = a((rv1<?>) next);
            if (a2 != null && a2.E()) {
                hashSet.addAll(f2.get(next).a);
            }
        }
        this.e.q = hashSet;
    }

    @DexIgnore
    public final void i() {
        while (!this.q.isEmpty()) {
            a(this.q.remove());
        }
        this.e.a((Bundle) null);
    }

    @DexIgnore
    public final gv1 j() {
        gv1 gv1 = null;
        gv1 gv12 = null;
        int i2 = 0;
        int i3 = 0;
        for (h02 next : this.a.values()) {
            rv1 d2 = next.d();
            gv1 gv13 = this.s.get(next.a());
            if (!gv13.E() && (!this.c.get(d2).booleanValue() || gv13.D() || this.h.c(gv13.p()))) {
                if (gv13.p() != 4 || !this.o) {
                    int a2 = d2.c().a();
                    if (gv1 == null || i2 > a2) {
                        gv1 = gv13;
                        i2 = a2;
                    }
                } else {
                    int a3 = d2.c().a();
                    if (gv12 == null || i3 > a3) {
                        gv12 = gv13;
                        i3 = a3;
                    }
                }
            }
        }
        return (gv1 == null || gv12 == null || i2 <= i3) ? gv1 : gv12;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f A[Catch:{ all -> 0x0044 }] */
    public final boolean k() {
        this.f.lock();
        try {
            if (this.r) {
                if (this.o) {
                    for (rv1.c<?> a2 : this.b.keySet()) {
                        gv1 a3 = a(a2);
                        if (a3 == null || !a3.E()) {
                            this.f.unlock();
                            return false;
                        }
                        while (r0.hasNext()) {
                        }
                    }
                    this.f.unlock();
                    return true;
                }
            }
            return false;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final void a() {
        this.f.lock();
        try {
            this.r = false;
            this.s = null;
            this.t = null;
            if (this.u != null) {
                this.u.a();
                this.u = null;
            }
            this.v = null;
            while (!this.q.isEmpty()) {
                nw1 remove = this.q.remove();
                remove.a((mz1) null);
                remove.a();
            }
            this.i.signalAll();
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final void b() {
        this.f.lock();
        try {
            if (!this.r) {
                this.r = true;
                this.s = null;
                this.t = null;
                this.u = null;
                this.v = null;
                this.d.c();
                this.d.a((Iterable<? extends xv1<?>>) this.a.values()).a((Executor) new x42(this.g), new i02(this));
                this.f.unlock();
            }
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final boolean c() {
        this.f.lock();
        try {
            return this.s != null && this.v == null;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    public final gv1 a(rv1<?> rv1) {
        return a(rv1.a());
    }

    @DexIgnore
    public final gv1 a(rv1.c<?> cVar) {
        this.f.lock();
        try {
            h02 h02 = this.a.get(cVar);
            if (this.s != null && h02 != null) {
                return this.s.get(h02.a());
            }
            this.f.unlock();
            return null;
        } finally {
            this.f.unlock();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final boolean a(yw1 yw1) {
        this.f.lock();
        try {
            if (!this.r || k()) {
                this.f.unlock();
                return false;
            }
            this.d.c();
            this.u = new ex1(this, yw1);
            this.d.a((Iterable<? extends xv1<?>>) this.b.values()).a((Executor) new x42(this.g), this.u);
            this.f.unlock();
            return true;
        } catch (Throwable th) {
            this.f.unlock();
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(h02<?> h02, gv1 gv1) {
        return !gv1.E() && !gv1.D() && this.c.get(h02.d()).booleanValue() && h02.i().i() && this.h.c(gv1.p());
    }
}
