package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rw implements bs<Drawable> {
    @DexIgnore
    public /* final */ bs<Bitmap> b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public rw(bs<Bitmap> bsVar, boolean z) {
        this.b = bsVar;
        this.c = z;
    }

    @DexIgnore
    public bs<BitmapDrawable> a() {
        return this;
    }

    @DexIgnore
    public rt<Drawable> a(Context context, rt<Drawable> rtVar, int i, int i2) {
        au c2 = wq.a(context).c();
        Drawable drawable = rtVar.get();
        rt<Bitmap> a = qw.a(c2, drawable, i, i2);
        if (a != null) {
            rt<Bitmap> a2 = this.b.a(context, a, i, i2);
            if (!a2.equals(a)) {
                return a(context, a2);
            }
            a2.a();
            return rtVar;
        } else if (!this.c) {
            return rtVar;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof rw) {
            return this.b.equals(((rw) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final rt<Drawable> a(Context context, rt<Bitmap> rtVar) {
        return xw.a(context.getResources(), rtVar);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
