package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n72 implements Parcelable.Creator<k72> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        t72 t72 = null;
        Long l = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    j = f22.s(parcel2, a);
                    break;
                case 2:
                    j2 = f22.s(parcel2, a);
                    break;
                case 3:
                    str = f22.e(parcel2, a);
                    break;
                case 4:
                    str2 = f22.e(parcel2, a);
                    break;
                case 5:
                    str3 = f22.e(parcel2, a);
                    break;
                case 7:
                    i = f22.q(parcel2, a);
                    break;
                case 8:
                    t72 = f22.a(parcel2, a, t72.CREATOR);
                    break;
                case 9:
                    l = f22.t(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new k72(j, j2, str, str2, str3, i, t72, l);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new k72[i];
    }
}
