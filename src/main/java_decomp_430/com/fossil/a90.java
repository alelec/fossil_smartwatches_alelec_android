package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum a90 {
    IDLE((byte) 0),
    RUNNING((byte) 1),
    CYCLING((byte) 2),
    TREADMILL((byte) 3),
    ELLIPTICAL((byte) 4),
    WEIGHTS((byte) 5),
    WORKOUT((byte) 6);
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final a90 a(byte b) {
            for (a90 a90 : a90.values()) {
                if (a90.a() == b) {
                    return a90;
                }
            }
            return null;
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public a90(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
