package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import com.google.firebase.FirebaseApp;
import java.io.IOException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js3 {
    @DexIgnore
    public /* final */ FirebaseApp a;
    @DexIgnore
    public /* final */ dr3 b;
    @DexIgnore
    public /* final */ jr3 c;
    @DexIgnore
    public /* final */ Executor d;
    @DexIgnore
    public /* final */ xt3 e;

    @DexIgnore
    public js3(FirebaseApp firebaseApp, dr3 dr3, Executor executor, xt3 xt3) {
        this(firebaseApp, dr3, executor, new jr3(firebaseApp.b(), dr3), xt3);
    }

    @DexIgnore
    public final qc3<String> a(String str, String str2, String str3) {
        return b(a(str, str2, str3, new Bundle()));
    }

    @DexIgnore
    public final qc3<Void> b(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    @DexIgnore
    public final qc3<Void> c(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    @DexIgnore
    public js3(FirebaseApp firebaseApp, dr3 dr3, Executor executor, jr3 jr3, xt3 xt3) {
        this.a = firebaseApp;
        this.b = dr3;
        this.c = jr3;
        this.d = executor;
        this.e = xt3;
    }

    @DexIgnore
    public final qc3<Void> a(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("iid-operation", "delete");
        bundle.putString("delete", "1");
        return a(b(a(str, "*", "*", bundle)));
    }

    @DexIgnore
    public final qc3<String> b(qc3<Bundle> qc3) {
        return qc3.a(this.d, (ic3<Bundle, TContinuationResult>) new ns3(this));
    }

    @DexIgnore
    public final qc3<Bundle> a(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString("sender", str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.a.d().a());
        bundle.putString("gmsv", Integer.toString(this.b.d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.b.b());
        bundle.putString("app_ver_name", this.b.c());
        String a2 = t12.a().a("firebase-iid");
        if ("UNKNOWN".equals(a2)) {
            int i = kv1.a;
            StringBuilder sb = new StringBuilder(19);
            sb.append("unknown_");
            sb.append(i);
            a2 = sb.toString();
        }
        String valueOf = String.valueOf(a2);
        bundle.putString("cliv", valueOf.length() != 0 ? "fiid-".concat(valueOf) : new String("fiid-"));
        bundle.putString("Firebase-Client", this.e.a());
        rc3 rc3 = new rc3();
        this.d.execute(new ls3(this, bundle, rc3));
        return rc3.a();
    }

    @DexIgnore
    public static String a(Bundle bundle) throws IOException {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null) {
                return string;
            }
            String string2 = bundle.getString("unregistered");
            if (string2 != null) {
                return string2;
            }
            String string3 = bundle.getString("error");
            if ("RST".equals(string3)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string3 != null) {
                throw new IOException(string3);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    @DexIgnore
    public final <T> qc3<Void> a(qc3<T> qc3) {
        return qc3.a(uq3.a(), (ic3<T, TContinuationResult>) new ks3(this));
    }

    @DexIgnore
    public final /* synthetic */ void a(Bundle bundle, rc3 rc3) {
        try {
            rc3.a(this.c.a(bundle));
        } catch (IOException e2) {
            rc3.a((Exception) e2);
        }
    }
}
