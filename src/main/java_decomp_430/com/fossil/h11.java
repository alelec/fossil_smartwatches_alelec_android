package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h11 extends BluetoothGattService {
    @DexIgnore
    public /* final */ HashMap<BluetoothDevice, vg0> a; // = new HashMap<>();
    @DexIgnore
    public Hashtable<BluetoothDevice, Integer> b; // = new Hashtable<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ jn0 e;

    @DexIgnore
    public h11(String str, UUID uuid, int i, jn0 jn0) {
        super(uuid, i);
        this.d = str;
        this.e = jn0;
    }

    @DexIgnore
    public boolean a(mg0 mg0) {
        if (!wg6.a(mg0.d.getUuid(), mi0.A.a())) {
            return false;
        }
        synchronized (this.a) {
            vg0 vg0 = this.a.get(mg0.a);
            if (vg0 == null) {
                vg0 = new vg0();
            }
            byte[] bArr = vg0.a.get(mg0.d);
            if (bArr == null) {
                bArr = pk0.d.a();
            }
            a((q81) new vh1(mg0, 0, 0, bArr, this.e.b));
        }
        return true;
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(q81 q81) {
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public void c(q81 q81) {
    }

    @DexIgnore
    public final void d() {
        synchronized (Boolean.valueOf(this.c)) {
            if (!this.c) {
                this.c = true;
                b();
            }
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public void d(q81 q81) {
    }

    @DexIgnore
    public final void e() {
        synchronized (Boolean.valueOf(this.c)) {
            if (this.c) {
                c();
                this.c = false;
            }
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            h11 h11 = (h11) obj;
            return !(wg6.a(getUuid(), h11.getUuid()) ^ true) && getType() == h11.getType();
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.gattserver.service.GattService");
    }

    @DexIgnore
    public int hashCode() {
        return Integer.valueOf(getType()).hashCode() + (getUuid().hashCode() * 31);
    }

    @DexIgnore
    public boolean a(xj0 xj0) {
        if (!wg6.a(xj0.c.getUuid(), mi0.A.a())) {
            return false;
        }
        synchronized (this.a) {
            vg0 vg0 = this.a.get(xj0.a);
            if (vg0 == null) {
                vg0 = new vg0();
            }
            vg0.a.put(xj0.c, xj0.g);
            this.a.put(xj0.a, vg0);
            a((q81) new vh1(xj0, 0, 0, (byte[]) null, this.e.b));
        }
        return true;
    }

    @DexIgnore
    public final jn0 a() {
        return this.e;
    }

    @DexIgnore
    public void a(BluetoothDevice bluetoothDevice, int i) {
        if (i == 0) {
            this.a.remove(bluetoothDevice);
        }
    }

    @DexIgnore
    public void a(int i, BluetoothGattService bluetoothGattService) {
        if (i == 0 && wg6.a(this, bluetoothGattService)) {
            d();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    public boolean a(an1 an1) {
        byte[] bArr;
        byte[] a2;
        int i = 0;
        if (an1 instanceof xj0) {
            return a((xj0) an1);
        }
        if (an1 instanceof mg0) {
            return a((mg0) an1);
        }
        if (an1 instanceof di0) {
            di0 di0 = (di0) an1;
            return false;
        } else if (!(an1 instanceof ve0)) {
            return false;
        } else {
            ve0 ve0 = (ve0) an1;
            hu0 hu0 = (hu0) this;
            UUID uuid = ve0.d.getUuid();
            if (wg6.a(uuid, mi0.A.b())) {
                a2 = hu0.g.a(System.currentTimeMillis(), sl0.ADJUST_NONE);
            } else if (wg6.a(uuid, mi0.A.c())) {
                a2 = hu0.g.a(System.currentTimeMillis());
            } else {
                i = null;
                bArr = null;
                if (i != null) {
                    return false;
                }
                hu0.a((q81) new vh1(ve0, 0, 0, bArr, hu0.e.b));
                return true;
            }
            bArr = a2;
            if (i != null) {
            }
        }
    }

    @DexIgnore
    public final void a(q81 q81) {
        q81.g = new zv0(this);
        q81.h = new ux0(this);
        q81.i = new nz0(this);
        fu0 fu0 = this.e.c;
        if (fu0.a != null) {
            fu0.d.a(q81);
        }
    }
}
