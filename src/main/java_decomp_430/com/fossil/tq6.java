package com.fossil;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq6 {
    @DexIgnore
    public static /* final */ char[] j; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<String> g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b; // = "";
        @DexIgnore
        public String c; // = "";
        @DexIgnore
        public String d;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public /* final */ List<String> f; // = new ArrayList();
        @DexIgnore
        public List<String> g;
        @DexIgnore
        public String h;

        @DexIgnore
        public a() {
            this.f.add("");
        }

        @DexIgnore
        public a a(int i) {
            if (i <= 0 || i > 65535) {
                throw new IllegalArgumentException("unexpected port: " + i);
            }
            this.e = i;
            return this;
        }

        @DexIgnore
        public a b(String str) {
            if (str != null) {
                String b2 = b(str, 0, str.length());
                if (b2 != null) {
                    this.d = b2;
                    return this;
                }
                throw new IllegalArgumentException("unexpected host: " + str);
            }
            throw new NullPointerException("host == null");
        }

        @DexIgnore
        public final boolean c(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        @DexIgnore
        public a d() {
            int size = this.f.size();
            for (int i = 0; i < size; i++) {
                this.f.set(i, tq6.a(this.f.get(i), "[]", true, true, false, true));
            }
            List<String> list = this.g;
            if (list != null) {
                int size2 = list.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    String str = this.g.get(i2);
                    if (str != null) {
                        this.g.set(i2, tq6.a(str, "\\^`{|}", true, true, true, true));
                    }
                }
            }
            String str2 = this.h;
            if (str2 != null) {
                this.h = tq6.a(str2, " \"#<>\\^`{|}", true, true, false, false);
            }
            return this;
        }

        @DexIgnore
        public a e(String str) {
            if (str != null) {
                this.c = tq6.a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
                return this;
            }
            throw new NullPointerException("password == null");
        }

        @DexIgnore
        public a f(String str) {
            if (str != null) {
                if (str.equalsIgnoreCase("http")) {
                    this.a = "http";
                } else if (str.equalsIgnoreCase("https")) {
                    this.a = "https";
                } else {
                    throw new IllegalArgumentException("unexpected scheme: " + str);
                }
                return this;
            }
            throw new NullPointerException("scheme == null");
        }

        @DexIgnore
        public a g(String str) {
            if (str != null) {
                this.b = tq6.a(str, " \"':;<=>@[]^`{}|/\\?#", false, false, false, true);
                return this;
            }
            throw new NullPointerException("username == null");
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            String str = this.a;
            if (str != null) {
                sb.append(str);
                sb.append("://");
            } else {
                sb.append("//");
            }
            if (!this.b.isEmpty() || !this.c.isEmpty()) {
                sb.append(this.b);
                if (!this.c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.c);
                }
                sb.append('@');
            }
            String str2 = this.d;
            if (str2 != null) {
                if (str2.indexOf(58) != -1) {
                    sb.append('[');
                    sb.append(this.d);
                    sb.append(']');
                } else {
                    sb.append(this.d);
                }
            }
            if (!(this.e == -1 && this.a == null)) {
                int b2 = b();
                String str3 = this.a;
                if (str3 == null || b2 != tq6.c(str3)) {
                    sb.append(':');
                    sb.append(b2);
                }
            }
            tq6.b(sb, this.f);
            if (this.g != null) {
                sb.append('?');
                tq6.a(sb, this.g);
            }
            if (this.h != null) {
                sb.append('#');
                sb.append(this.h);
            }
            return sb.toString();
        }

        @DexIgnore
        public final void c() {
            List<String> list = this.f;
            if (!list.remove(list.size() - 1).isEmpty() || this.f.isEmpty()) {
                this.f.add("");
                return;
            }
            List<String> list2 = this.f;
            list2.set(list2.size() - 1, "");
        }

        @DexIgnore
        public static int e(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt >= 'a' && charAt <= 'z') || (charAt >= 'A' && charAt <= 'Z')) {
                while (true) {
                    i++;
                    if (i >= i2) {
                        break;
                    }
                    char charAt2 = str.charAt(i);
                    if ((charAt2 < 'a' || charAt2 > 'z') && ((charAt2 < 'A' || charAt2 > 'Z') && !((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                        if (charAt2 == ':') {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        @DexIgnore
        public a a(String str) {
            this.g = str != null ? tq6.f(tq6.a(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        @DexIgnore
        public int b() {
            int i = this.e;
            return i != -1 ? i : tq6.c(this.a);
        }

        @DexIgnore
        public static int c(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(tq6.a(str, i, i2, "", false, false, false, true, (Charset) null));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str != null) {
                if (this.g == null) {
                    this.g = new ArrayList();
                }
                this.g.add(tq6.a(str, " \"'<>#&=", true, false, true, true));
                this.g.add(str2 != null ? tq6.a(str2, " \"'<>#&=", true, false, true, true) : null);
                return this;
            }
            throw new NullPointerException("encodedName == null");
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str != null) {
                if (this.g == null) {
                    this.g = new ArrayList();
                }
                this.g.add(tq6.a(str, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, true));
                this.g.add(str2 != null ? tq6.a(str2, " !\"#$&'(),/:;<=>?@[]\\^`{|}~", false, false, true, true) : null);
                return this;
            }
            throw new NullPointerException("name == null");
        }

        @DexIgnore
        public static int f(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        @DexIgnore
        public static String b(String str, int i, int i2) {
            return fr6.a(tq6.a(str, i, i2, false));
        }

        @DexIgnore
        public tq6 a() {
            if (this.a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.d != null) {
                return new tq6(this);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        @DexIgnore
        public final boolean d(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        @DexIgnore
        public static int d(String str, int i, int i2) {
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt == ':') {
                    return i;
                }
                if (charAt == '[') {
                    do {
                        i++;
                        if (i >= i2) {
                            break;
                        }
                    } while (str.charAt(i) == ']');
                }
                i++;
            }
            return i2;
        }

        @DexIgnore
        public a a(tq6 tq6, String str) {
            int a2;
            int i;
            tq6 tq62 = tq6;
            String str2 = str;
            int b2 = fr6.b(str2, 0, str.length());
            int c2 = fr6.c(str2, b2, str.length());
            int e2 = e(str2, b2, c2);
            if (e2 != -1) {
                if (str.regionMatches(true, b2, "https:", 0, 6)) {
                    this.a = "https";
                    b2 += 6;
                } else if (str.regionMatches(true, b2, "http:", 0, 5)) {
                    this.a = "http";
                    b2 += 5;
                } else {
                    throw new IllegalArgumentException("Expected URL scheme 'http' or 'https' but was '" + str2.substring(0, e2) + "'");
                }
            } else if (tq62 != null) {
                this.a = tq62.a;
            } else {
                throw new IllegalArgumentException("Expected URL scheme 'http' or 'https' but no colon was found");
            }
            int f2 = f(str2, b2, c2);
            char c3 = '?';
            char c4 = '#';
            if (f2 >= 2 || tq62 == null || !tq62.a.equals(this.a)) {
                int i2 = b2 + f2;
                boolean z = false;
                boolean z2 = false;
                while (true) {
                    a2 = fr6.a(str2, i2, c2, "@/\\?#");
                    char charAt = a2 != c2 ? str2.charAt(a2) : 65535;
                    if (charAt == 65535 || charAt == c4 || charAt == '/' || charAt == '\\' || charAt == c3) {
                        int i3 = a2;
                        int d2 = d(str2, i2, i3);
                        int i4 = d2 + 1;
                    } else {
                        if (charAt == '@') {
                            if (!z) {
                                int a3 = fr6.a(str2, i2, a2, ':');
                                int i5 = a3;
                                String str3 = "%40";
                                i = a2;
                                String a4 = tq6.a(str, i2, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset) null);
                                if (z2) {
                                    a4 = this.b + str3 + a4;
                                }
                                this.b = a4;
                                if (i5 != i) {
                                    this.c = tq6.a(str, i5 + 1, i, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset) null);
                                    z = true;
                                }
                                z2 = true;
                            } else {
                                i = a2;
                                this.c += "%40" + tq6.a(str, i2, i, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, (Charset) null);
                            }
                            i2 = i + 1;
                        }
                        c3 = '?';
                        c4 = '#';
                    }
                }
                int i32 = a2;
                int d22 = d(str2, i2, i32);
                int i42 = d22 + 1;
                if (i42 < i32) {
                    this.d = b(str2, i2, d22);
                    this.e = c(str2, i42, i32);
                    if (this.e == -1) {
                        throw new IllegalArgumentException("Invalid URL port: \"" + str2.substring(i42, i32) + '\"');
                    }
                } else {
                    this.d = b(str2, i2, d22);
                    this.e = tq6.c(this.a);
                }
                if (this.d != null) {
                    b2 = i32;
                } else {
                    throw new IllegalArgumentException("Invalid URL host: \"" + str2.substring(i2, d22) + '\"');
                }
            } else {
                this.b = tq6.f();
                this.c = tq6.b();
                this.d = tq62.d;
                this.e = tq62.e;
                this.f.clear();
                this.f.addAll(tq6.d());
                if (b2 == c2 || str2.charAt(b2) == '#') {
                    a(tq6.e());
                }
            }
            int a5 = fr6.a(str2, b2, c2, "?#");
            a(str2, b2, a5);
            if (a5 < c2 && str2.charAt(a5) == '?') {
                int a6 = fr6.a(str2, a5, c2, '#');
                this.g = tq6.f(tq6.a(str, a5 + 1, a6, " \"'<>#", true, false, true, true, (Charset) null));
                a5 = a6;
            }
            if (a5 < c2 && str2.charAt(a5) == '#') {
                this.h = tq6.a(str, 1 + a5, c2, "", true, false, false, false, (Charset) null);
            }
            return this;
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0044 A[SYNTHETIC] */
        public final void a(java.lang.String r11, int r12, int r13) {
            /*
                r10 = this;
                if (r12 != r13) goto L_0x0003
                return
            L_0x0003:
                char r0 = r11.charAt(r12)
                r1 = 47
                java.lang.String r2 = ""
                r3 = 1
                if (r0 == r1) goto L_0x001e
                r1 = 92
                if (r0 != r1) goto L_0x0013
                goto L_0x001e
            L_0x0013:
                java.util.List<java.lang.String> r0 = r10.f
                int r1 = r0.size()
                int r1 = r1 - r3
                r0.set(r1, r2)
                goto L_0x0029
            L_0x001e:
                java.util.List<java.lang.String> r0 = r10.f
                r0.clear()
                java.util.List<java.lang.String> r0 = r10.f
                r0.add(r2)
                goto L_0x0041
            L_0x0029:
                r6 = r12
                if (r6 >= r13) goto L_0x0044
                java.lang.String r12 = "/\\"
                int r12 = com.fossil.fr6.a((java.lang.String) r11, (int) r6, (int) r13, (java.lang.String) r12)
                if (r12 >= r13) goto L_0x0036
                r0 = 1
                goto L_0x0037
            L_0x0036:
                r0 = 0
            L_0x0037:
                r9 = 1
                r4 = r10
                r5 = r11
                r7 = r12
                r8 = r0
                r4.a(r5, r6, r7, r8, r9)
                if (r0 == 0) goto L_0x0029
            L_0x0041:
                int r12 = r12 + 1
                goto L_0x0029
            L_0x0044:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.tq6.a.a(java.lang.String, int, int):void");
        }

        @DexIgnore
        public final void a(String str, int i, int i2, boolean z, boolean z2) {
            String a2 = tq6.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true, (Charset) null);
            if (!c(a2)) {
                if (d(a2)) {
                    c();
                    return;
                }
                List<String> list = this.f;
                if (list.get(list.size() - 1).isEmpty()) {
                    List<String> list2 = this.f;
                    list2.set(list2.size() - 1, a2);
                } else {
                    this.f.add(a2);
                }
                if (z) {
                    this.f.add("");
                }
            }
        }
    }

    @DexIgnore
    public tq6(a aVar) {
        this.a = aVar.a;
        this.b = a(aVar.b, false);
        this.c = a(aVar.c, false);
        this.d = aVar.d;
        this.e = aVar.b();
        this.f = a(aVar.f, false);
        List<String> list = aVar.g;
        String str = null;
        this.g = list != null ? a(list, true) : null;
        String str2 = aVar.h;
        this.h = str2 != null ? a(str2, false) : str;
        this.i = aVar.toString();
    }

    @DexIgnore
    public static void a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2 += 2) {
            String str = list.get(i2);
            String str2 = list.get(i2 + 1);
            if (i2 > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    @DexIgnore
    public static int c(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals("https") ? 443 : -1;
    }

    @DexIgnore
    public String b() {
        if (this.c.isEmpty()) {
            return "";
        }
        int indexOf = this.i.indexOf(64);
        return this.i.substring(this.i.indexOf(58, this.a.length() + 3) + 1, indexOf);
    }

    @DexIgnore
    public List<String> d() {
        int indexOf = this.i.indexOf(47, this.a.length() + 3);
        String str = this.i;
        int a2 = fr6.a(str, indexOf, str.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < a2) {
            int i2 = indexOf + 1;
            int a3 = fr6.a(this.i, i2, a2, '/');
            arrayList.add(this.i.substring(i2, a3));
            indexOf = a3;
        }
        return arrayList;
    }

    @DexIgnore
    public String e() {
        if (this.g == null) {
            return null;
        }
        int indexOf = this.i.indexOf(63) + 1;
        String str = this.i;
        return this.i.substring(indexOf, fr6.a(str, indexOf, str.length(), '#'));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof tq6) && ((tq6) obj).i.equals(this.i);
    }

    @DexIgnore
    public String f() {
        if (this.b.isEmpty()) {
            return "";
        }
        int length = this.a.length() + 3;
        String str = this.i;
        return this.i.substring(length, fr6.a(str, length, str.length(), ":@"));
    }

    @DexIgnore
    public String g() {
        return this.d;
    }

    @DexIgnore
    public boolean h() {
        return this.a.equals("https");
    }

    @DexIgnore
    public int hashCode() {
        return this.i.hashCode();
    }

    @DexIgnore
    public a i() {
        a aVar = new a();
        aVar.a = this.a;
        aVar.b = f();
        aVar.c = b();
        aVar.d = this.d;
        aVar.e = this.e != c(this.a) ? this.e : -1;
        aVar.f.clear();
        aVar.f.addAll(d());
        aVar.a(e());
        aVar.h = a();
        return aVar;
    }

    @DexIgnore
    public List<String> j() {
        return this.f;
    }

    @DexIgnore
    public int k() {
        return this.e;
    }

    @DexIgnore
    public String l() {
        if (this.g == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        a(sb, this.g);
        return sb.toString();
    }

    @DexIgnore
    public String m() {
        a a2 = a("/...");
        a2.g("");
        a2.e("");
        return a2.a().toString();
    }

    @DexIgnore
    public String n() {
        return this.a;
    }

    @DexIgnore
    public URI o() {
        a i2 = i();
        i2.d();
        String aVar = i2.toString();
        try {
            return new URI(aVar);
        } catch (URISyntaxException e2) {
            try {
                return URI.create(aVar.replaceAll("[\\u0000-\\u001F\\u007F-\\u009F\\p{javaWhitespace}]", ""));
            } catch (Exception unused) {
                throw new RuntimeException(e2);
            }
        }
    }

    @DexIgnore
    public String toString() {
        return this.i;
    }

    @DexIgnore
    public String c() {
        int indexOf = this.i.indexOf(47, this.a.length() + 3);
        String str = this.i;
        return this.i.substring(indexOf, fr6.a(str, indexOf, str.length(), "?#"));
    }

    @DexIgnore
    public static void b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append('/');
            sb.append(list.get(i2));
        }
    }

    @DexIgnore
    public static tq6 e(String str) {
        try {
            return d(str);
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    @DexIgnore
    public static List<String> f(String str) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 <= str.length()) {
            int indexOf = str.indexOf(38, i2);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i2);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i2, indexOf));
                arrayList.add((Object) null);
            } else {
                arrayList.add(str.substring(i2, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i2 = indexOf + 1;
        }
        return arrayList;
    }

    @DexIgnore
    public static tq6 d(String str) {
        a aVar = new a();
        aVar.a((tq6) null, str);
        return aVar.a();
    }

    @DexIgnore
    public String a() {
        if (this.h == null) {
            return null;
        }
        return this.i.substring(this.i.indexOf(35) + 1);
    }

    @DexIgnore
    public tq6 b(String str) {
        a a2 = a(str);
        if (a2 != null) {
            return a2.a();
        }
        return null;
    }

    @DexIgnore
    public a a(String str) {
        try {
            a aVar = new a();
            aVar.a(this, str);
            return aVar;
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }

    @DexIgnore
    public static String a(String str, boolean z) {
        return a(str, 0, str.length(), z);
    }

    @DexIgnore
    public final List<String> a(List<String> list, boolean z) {
        int size = list.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i2 = 0; i2 < size; i2++) {
            String str = list.get(i2);
            arrayList.add(str != null ? a(str, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    @DexIgnore
    public static String a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (charAt == '%' || (charAt == '+' && z)) {
                jt6 jt6 = new jt6();
                jt6.a(str, i2, i4);
                a(jt6, str, i4, i3, z);
                return jt6.n();
            }
        }
        return str.substring(i2, i3);
    }

    @DexIgnore
    public static void a(jt6 jt6, String str, int i2, int i3, boolean z) {
        int i4;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt == 37 && (i4 = i2 + 2) < i3) {
                int a2 = fr6.a(str.charAt(i2 + 1));
                int a3 = fr6.a(str.charAt(i4));
                if (!(a2 == -1 || a3 == -1)) {
                    jt6.writeByte((a2 << 4) + a3);
                    i2 = i4;
                    i2 += Character.charCount(codePointAt);
                }
            } else if (codePointAt == 43 && z) {
                jt6.writeByte(32);
                i2 += Character.charCount(codePointAt);
            }
            jt6.c(codePointAt);
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public static boolean a(String str, int i2, int i3) {
        int i4 = i2 + 2;
        if (i4 >= i3 || str.charAt(i2) != '%' || fr6.a(str.charAt(i2 + 1)) == -1 || fr6.a(str.charAt(i4)) == -1) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static String a(String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        String str3 = str;
        int i4 = i3;
        int i5 = i2;
        while (i5 < i4) {
            int codePointAt = str.codePointAt(i5);
            if (codePointAt < 32 || codePointAt == 127 || (codePointAt >= 128 && z4)) {
                String str4 = str2;
            } else {
                String str5 = str2;
                if (str2.indexOf(codePointAt) == -1 && ((codePointAt != 37 || (z && (!z2 || a(str, i5, i3)))) && (codePointAt != 43 || !z3))) {
                    i5 += Character.charCount(codePointAt);
                }
            }
            jt6 jt6 = new jt6();
            int i6 = i2;
            jt6.a(str, i2, i5);
            a(jt6, str, i5, i3, str2, z, z2, z3, z4, charset);
            return jt6.n();
        }
        int i7 = i2;
        return str.substring(i2, i3);
    }

    @DexIgnore
    public static void a(jt6 jt6, String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        jt6 jt62 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    jt6.a(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !a(str, i2, i3)))))) {
                    if (jt62 == null) {
                        jt62 = new jt6();
                    }
                    if (charset == null || charset.equals(fr6.i)) {
                        jt62.c(codePointAt);
                    } else {
                        jt62.a(str, i2, Character.charCount(codePointAt) + i2, charset);
                    }
                    while (!jt62.f()) {
                        byte readByte = jt62.readByte() & 255;
                        jt6.writeByte(37);
                        jt6.writeByte((int) j[(readByte >> 4) & 15]);
                        jt6.writeByte((int) j[readByte & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]);
                    }
                } else {
                    jt6.c(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public static String a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, Charset charset) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4, charset);
    }

    @DexIgnore
    public static String a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4, (Charset) null);
    }
}
