package com.fossil;

import android.graphics.Bitmap;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tw extends iw {
    @DexIgnore
    public static /* final */ byte[] b; // = "com.bumptech.glide.load.resource.bitmap.FitCenter".getBytes(vr.a);

    @DexIgnore
    public Bitmap a(au auVar, Bitmap bitmap, int i, int i2) {
        return cx.c(auVar, bitmap, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof tw;
    }

    @DexIgnore
    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.FitCenter".hashCode();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
