package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc6<T> implements ic6<T>, Serializable {
    @DexIgnore
    public volatile Object _value;
    @DexIgnore
    public gg6<? extends T> initializer;
    @DexIgnore
    public /* final */ Object lock;

    @DexIgnore
    public oc6(gg6<? extends T> gg6, Object obj) {
        wg6.b(gg6, "initializer");
        this.initializer = gg6;
        this._value = yc6.a;
        this.lock = obj == null ? this : obj;
    }

    @DexIgnore
    private final Object writeReplace() {
        return new gc6(getValue());
    }

    @DexIgnore
    public T getValue() {
        T t;
        T t2 = this._value;
        if (t2 != yc6.a) {
            return t2;
        }
        synchronized (this.lock) {
            t = this._value;
            if (t == yc6.a) {
                gg6 gg6 = this.initializer;
                if (gg6 != null) {
                    t = gg6.invoke();
                    this._value = t;
                    this.initializer = null;
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        return t;
    }

    @DexIgnore
    public boolean isInitialized() {
        return this._value != yc6.a;
    }

    @DexIgnore
    public String toString() {
        return isInitialized() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ oc6(gg6 gg6, Object obj, int i, qg6 qg6) {
        this(gg6, (i & 2) != 0 ? null : obj);
    }
}
