package com.fossil;

import android.graphics.Bitmap;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mh2 extends IInterface {
    @DexIgnore
    x52 zza(Bitmap bitmap) throws RemoteException;
}
