package com.fossil;

import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i85$d$a extends sf6 implements ig6<il6, xe6<? super List<? extends WeatherLocationWrapper>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherSettingPresenter.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i85$d$a(xe6 xe6, WeatherSettingPresenter.d dVar) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        i85$d$a i85_d_a = new i85$d$a(xe6, this.this$0);
        i85_d_a.p$ = (il6) obj;
        return i85_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((i85$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            List b = this.this$0.this$0.g;
            ArrayList arrayList = new ArrayList();
            for (Object next : b) {
                if (hf6.a(((WeatherLocationWrapper) next).isEnableLocation()).booleanValue()) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
