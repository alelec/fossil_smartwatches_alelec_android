package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cl6 {
    @DexIgnore
    public static /* final */ boolean a;

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        if (r0.equals("on") != false) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        if (r0.equals("") != false) goto L_0x0033;
     */
    /*
    static {
        boolean z;
        String a2 = vo6.a("kotlinx.coroutines.scheduler");
        if (a2 != null) {
            int hashCode = a2.hashCode();
            if (hashCode != 0) {
                if (hashCode != 3551) {
                    if (hashCode == 109935 && a2.equals("off")) {
                        z = false;
                        a = z;
                    }
                }
            }
            throw new IllegalStateException(("System property 'kotlinx.coroutines.scheduler' has unrecognized value '" + a2 + '\'').toString());
        }
        z = true;
        a = z;
    }
    */

    @DexIgnore
    public static final dl6 a() {
        return a ? gp6.g : tk6.c;
    }

    @DexIgnore
    public static final af6 a(il6 il6, af6 af6) {
        wg6.b(il6, "$this$newCoroutineContext");
        wg6.b(af6, "context");
        af6 plus = il6.m().plus(af6);
        af6 plus2 = nl6.c() ? plus.plus(new gl6(nl6.b().incrementAndGet())) : plus;
        return (plus == zl6.a() || plus.get(ye6.l) != null) ? plus2 : plus2.plus(zl6.a());
    }

    @DexIgnore
    public static final String a(af6 af6) {
        gl6 gl6;
        String str;
        wg6.b(af6, "$this$coroutineName");
        if (!nl6.c() || (gl6 = (gl6) af6.get(gl6.b)) == null) {
            return null;
        }
        hl6 hl6 = (hl6) af6.get(hl6.b);
        if (hl6 == null || (str = hl6.o()) == null) {
            str = "coroutine";
        }
        return str + '#' + gl6.o();
    }
}
