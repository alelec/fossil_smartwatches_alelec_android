package com.fossil;

import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.manager.WeatherManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en4$h$a extends sf6 implements ig6<il6, xe6<? super lc6<? extends Weather, ? extends Boolean>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $tempUnit;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager.h this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public en4$h$a(String str, xe6 xe6, WeatherManager.h hVar) {
        super(2, xe6);
        this.$tempUnit = str;
        this.this$0 = hVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        en4$h$a en4_h_a = new en4$h$a(this.$tempUnit, xe6, this.this$0);
        en4_h_a.p$ = (il6) obj;
        return en4_h_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((en4$h$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            WeatherManager weatherManager = this.this$0.this$0;
            String str = this.$tempUnit;
            this.L$0 = il6;
            this.label = 1;
            obj = weatherManager.a("chance-of-rain", str, (xe6<? super lc6<Weather, Boolean>>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
