package com.fossil;

import java.io.File;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ka6 {
    @DexIgnore
    int a();

    @DexIgnore
    List<File> a(int i);

    @DexIgnore
    void a(String str) throws IOException;

    @DexIgnore
    void a(List<File> list);

    @DexIgnore
    void a(byte[] bArr) throws IOException;

    @DexIgnore
    boolean a(int i, int i2);

    @DexIgnore
    boolean b();

    @DexIgnore
    List<File> c();

    @DexIgnore
    void d();
}
