package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ la3 b;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 c;
    @DexIgnore
    public /* final */ /* synthetic */ l83 d;

    @DexIgnore
    public m83(l83 l83, boolean z, la3 la3, ra3 ra3) {
        this.d = l83;
        this.a = z;
        this.b = la3;
        this.c = ra3;
    }

    @DexIgnore
    public final void run() {
        l43 d2 = this.d.d;
        if (d2 == null) {
            this.d.b().t().a("Discarding data. Failed to set user attribute");
            return;
        }
        this.d.a(d2, (e22) this.a ? null : this.b, this.c);
        this.d.I();
    }
}
