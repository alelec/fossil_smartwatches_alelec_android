package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kg5 implements MembersInjector<SleepOverviewFragment> {
    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewDayPresenter sleepOverviewDayPresenter) {
        sleepOverviewFragment.g = sleepOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
        sleepOverviewFragment.h = sleepOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(SleepOverviewFragment sleepOverviewFragment, SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        sleepOverviewFragment.i = sleepOverviewMonthPresenter;
    }
}
