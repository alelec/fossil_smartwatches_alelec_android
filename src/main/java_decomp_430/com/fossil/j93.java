package com.fossil;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.facebook.internal.NativeProtocol;
import com.fossil.n93;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j93<T extends Context & n93> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public j93(T t) {
        w12.a(t);
        this.a = t;
    }

    @DexIgnore
    public final void a() {
        x53 a2 = x53.a((Context) this.a, (mv2) null);
        t43 b = a2.b();
        a2.d();
        b.B().a("Local AppMeasurementService is starting up");
    }

    @DexIgnore
    public final void b() {
        x53 a2 = x53.a((Context) this.a, (mv2) null);
        t43 b = a2.b();
        a2.d();
        b.B().a("Local AppMeasurementService is shutting down");
    }

    @DexIgnore
    public final void c(Intent intent) {
        if (intent == null) {
            c().t().a("onRebind called with null intent");
            return;
        }
        c().B().a("onRebind called. action", intent.getAction());
    }

    @DexIgnore
    public final t43 c() {
        return x53.a((Context) this.a, (mv2) null).b();
    }

    @DexIgnore
    public final int a(Intent intent, int i, int i2) {
        x53 a2 = x53.a((Context) this.a, (mv2) null);
        t43 b = a2.b();
        if (intent == null) {
            b.w().a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        a2.d();
        b.B().a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            a((Runnable) new i93(this, i2, b, intent));
        }
        return 2;
    }

    @DexIgnore
    public final boolean b(Intent intent) {
        if (intent == null) {
            c().t().a("onUnbind called with null intent");
            return true;
        }
        c().B().a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        ea3 a2 = ea3.a((Context) this.a);
        a2.a().a((Runnable) new k93(this, a2, runnable));
    }

    @DexIgnore
    public final IBinder a(Intent intent) {
        if (intent == null) {
            c().t().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new d63(ea3.a((Context) this.a));
        }
        c().w().a("onBind received unknown action", action);
        return null;
    }

    @DexIgnore
    @TargetApi(24)
    public final boolean a(JobParameters jobParameters) {
        x53 a2 = x53.a((Context) this.a, (mv2) null);
        t43 b = a2.b();
        String string = jobParameters.getExtras().getString(NativeProtocol.WEB_DIALOG_ACTION);
        a2.d();
        b.B().a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        a((Runnable) new l93(this, b, jobParameters));
        return true;
    }

    @DexIgnore
    public final /* synthetic */ void a(t43 t43, JobParameters jobParameters) {
        t43.B().a("AppMeasurementJobService processed last upload request.");
        ((n93) this.a).a(jobParameters, false);
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, t43 t43, Intent intent) {
        if (((n93) this.a).zza(i)) {
            t43.B().a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            c().B().a("Completed wakeful intent.");
            ((n93) this.a).a(intent);
        }
    }
}
