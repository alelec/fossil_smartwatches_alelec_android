package com.fossil;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Looper;
import android.util.Log;
import com.fossil.ji;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oh {
    @DexIgnore
    public static /* final */ String DB_IMPL_SUFFIX; // = "_Impl";
    @DexIgnore
    public static /* final */ int MAX_BIND_PARAMETER_CNT; // = 999;
    @DexIgnore
    public boolean mAllowMainThreadQueries;
    @DexIgnore
    public /* final */ Map<String, Object> mBackingFieldMap; // = new ConcurrentHashMap();
    @DexIgnore
    @Deprecated
    public List<b> mCallbacks;
    @DexIgnore
    public /* final */ ReentrantReadWriteLock mCloseLock; // = new ReentrantReadWriteLock();
    @DexIgnore
    @Deprecated
    public volatile ii mDatabase;
    @DexIgnore
    public /* final */ lh mInvalidationTracker; // = createInvalidationTracker();
    @DexIgnore
    public ji mOpenHelper;
    @DexIgnore
    public Executor mQueryExecutor;
    @DexIgnore
    public /* final */ ThreadLocal<Integer> mSuspendingTransactionId; // = new ThreadLocal<>();
    @DexIgnore
    public Executor mTransactionExecutor;
    @DexIgnore
    public boolean mWriteAheadLoggingEnabled;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public void a(ii iiVar) {
        }

        @DexIgnore
        public void b(ii iiVar) {
        }

        @DexIgnore
        public void c(ii iiVar) {
        }
    }

    @DexIgnore
    public enum c {
        AUTOMATIC,
        TRUNCATE,
        WRITE_AHEAD_LOGGING;

        @DexIgnore
        public static boolean a(ActivityManager activityManager) {
            if (Build.VERSION.SDK_INT >= 19) {
                return activityManager.isLowRamDevice();
            }
            return false;
        }

        @DexIgnore
        @SuppressLint({"NewApi"})
        public c resolve(Context context) {
            ActivityManager activityManager;
            if (this != AUTOMATIC) {
                return this;
            }
            if (Build.VERSION.SDK_INT < 16 || (activityManager = (ActivityManager) context.getSystemService("activity")) == null || a(activityManager)) {
                return TRUNCATE;
            }
            return WRITE_AHEAD_LOGGING;
        }
    }

    @DexIgnore
    public static boolean isMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    @DexIgnore
    public void assertNotMainThread() {
        if (!this.mAllowMainThreadQueries && isMainThread()) {
            throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
        }
    }

    @DexIgnore
    public void assertNotSuspendingTransaction() {
        if (!inTransaction() && this.mSuspendingTransactionId.get() != null) {
            throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
        }
    }

    @DexIgnore
    @Deprecated
    public void beginTransaction() {
        assertNotMainThread();
        ii a2 = this.mOpenHelper.a();
        this.mInvalidationTracker.b(a2);
        a2.u();
    }

    @DexIgnore
    public abstract void clearAllTables();

    @DexIgnore
    public void close() {
        if (isOpen()) {
            ReentrantReadWriteLock.WriteLock writeLock = this.mCloseLock.writeLock();
            try {
                writeLock.lock();
                this.mInvalidationTracker.d();
                this.mOpenHelper.close();
            } finally {
                writeLock.unlock();
            }
        }
    }

    @DexIgnore
    public mi compileStatement(String str) {
        assertNotMainThread();
        assertNotSuspendingTransaction();
        return this.mOpenHelper.a().c(str);
    }

    @DexIgnore
    public abstract lh createInvalidationTracker();

    @DexIgnore
    public abstract ji createOpenHelper(fh fhVar);

    @DexIgnore
    @Deprecated
    public void endTransaction() {
        this.mOpenHelper.a().y();
        if (!inTransaction()) {
            this.mInvalidationTracker.b();
        }
    }

    @DexIgnore
    public Map<String, Object> getBackingFieldMap() {
        return this.mBackingFieldMap;
    }

    @DexIgnore
    public Lock getCloseLock() {
        return this.mCloseLock.readLock();
    }

    @DexIgnore
    public lh getInvalidationTracker() {
        return this.mInvalidationTracker;
    }

    @DexIgnore
    public ji getOpenHelper() {
        return this.mOpenHelper;
    }

    @DexIgnore
    public Executor getQueryExecutor() {
        return this.mQueryExecutor;
    }

    @DexIgnore
    public ThreadLocal<Integer> getSuspendingTransactionId() {
        return this.mSuspendingTransactionId;
    }

    @DexIgnore
    public Executor getTransactionExecutor() {
        return this.mTransactionExecutor;
    }

    @DexIgnore
    public boolean inTransaction() {
        return this.mOpenHelper.a().A();
    }

    @DexIgnore
    public void init(fh fhVar) {
        this.mOpenHelper = createOpenHelper(fhVar);
        ji jiVar = this.mOpenHelper;
        if (jiVar instanceof th) {
            ((th) jiVar).a(fhVar);
        }
        boolean z = false;
        if (Build.VERSION.SDK_INT >= 16) {
            if (fhVar.g == c.WRITE_AHEAD_LOGGING) {
                z = true;
            }
            this.mOpenHelper.a(z);
        }
        this.mCallbacks = fhVar.e;
        this.mQueryExecutor = fhVar.h;
        this.mTransactionExecutor = new wh(fhVar.i);
        this.mAllowMainThreadQueries = fhVar.f;
        this.mWriteAheadLoggingEnabled = z;
        if (fhVar.j) {
            this.mInvalidationTracker.a(fhVar.b, fhVar.c);
        }
    }

    @DexIgnore
    public void internalInitInvalidationTracker(ii iiVar) {
        this.mInvalidationTracker.a(iiVar);
    }

    @DexIgnore
    public boolean isOpen() {
        ii iiVar = this.mDatabase;
        return iiVar != null && iiVar.isOpen();
    }

    @DexIgnore
    public Cursor query(String str, Object[] objArr) {
        return this.mOpenHelper.a().a(new hi(str, objArr));
    }

    @DexIgnore
    public void runInTransaction(Runnable runnable) {
        beginTransaction();
        try {
            runnable.run();
            setTransactionSuccessful();
        } finally {
            endTransaction();
        }
    }

    @DexIgnore
    @Deprecated
    public void setTransactionSuccessful() {
        this.mOpenHelper.a().x();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public HashMap<Integer, TreeMap<Integer, xh>> a; // = new HashMap<>();

        @DexIgnore
        public void a(xh... xhVarArr) {
            for (xh a2 : xhVarArr) {
                a(a2);
            }
        }

        @DexIgnore
        public final void a(xh xhVar) {
            int i = xhVar.startVersion;
            int i2 = xhVar.endVersion;
            TreeMap treeMap = this.a.get(Integer.valueOf(i));
            if (treeMap == null) {
                treeMap = new TreeMap();
                this.a.put(Integer.valueOf(i), treeMap);
            }
            xh xhVar2 = (xh) treeMap.get(Integer.valueOf(i2));
            if (xhVar2 != null) {
                Log.w("ROOM", "Overriding migration " + xhVar2 + " with " + xhVar);
            }
            treeMap.put(Integer.valueOf(i2), xhVar);
        }

        @DexIgnore
        public List<xh> a(int i, int i2) {
            if (i == i2) {
                return Collections.emptyList();
            }
            return a(new ArrayList(), i2 > i, i, i2);
        }

        @DexIgnore
        public final List<xh> a(List<xh> list, boolean z, int i, int i2) {
            Set set;
            boolean z2;
            do {
                if (z) {
                    if (i >= i2) {
                        return list;
                    }
                } else if (i <= i2) {
                    return list;
                }
                TreeMap treeMap = this.a.get(Integer.valueOf(i));
                if (treeMap != null) {
                    if (z) {
                        set = treeMap.descendingKeySet();
                    } else {
                        set = treeMap.keySet();
                    }
                    Iterator it = set.iterator();
                    while (true) {
                        z2 = true;
                        boolean z3 = false;
                        if (!it.hasNext()) {
                            z2 = false;
                            continue;
                            break;
                        }
                        int intValue = ((Integer) it.next()).intValue();
                        if (!z ? !(intValue < i2 || intValue >= i) : !(intValue > i2 || intValue <= i)) {
                            z3 = true;
                            continue;
                        }
                        if (z3) {
                            list.add(treeMap.get(Integer.valueOf(intValue)));
                            i = intValue;
                            continue;
                            break;
                        }
                    }
                } else {
                    return null;
                }
            } while (z2);
            return null;
        }
    }

    @DexIgnore
    public Cursor query(li liVar) {
        return query(liVar, (CancellationSignal) null);
    }

    @DexIgnore
    public Cursor query(li liVar, CancellationSignal cancellationSignal) {
        assertNotMainThread();
        assertNotSuspendingTransaction();
        if (cancellationSignal == null || Build.VERSION.SDK_INT < 16) {
            return this.mOpenHelper.a().a(liVar);
        }
        return this.mOpenHelper.a().a(liVar, cancellationSignal);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<T extends oh> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ Context c;
        @DexIgnore
        public ArrayList<b> d;
        @DexIgnore
        public Executor e;
        @DexIgnore
        public Executor f;
        @DexIgnore
        public ji.c g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public c i; // = c.AUTOMATIC;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k; // = true;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public /* final */ d m; // = new d();
        @DexIgnore
        public Set<Integer> n;
        @DexIgnore
        public Set<Integer> o;
        @DexIgnore
        public String p;
        @DexIgnore
        public File q;

        @DexIgnore
        public a(Context context, Class<T> cls, String str) {
            this.c = context;
            this.a = cls;
            this.b = str;
        }

        @DexIgnore
        public a<T> a(xh... xhVarArr) {
            if (this.o == null) {
                this.o = new HashSet();
            }
            for (xh xhVar : xhVarArr) {
                this.o.add(Integer.valueOf(xhVar.startVersion));
                this.o.add(Integer.valueOf(xhVar.endVersion));
            }
            this.m.a(xhVarArr);
            return this;
        }

        @DexIgnore
        @SuppressLint({"RestrictedApi"})
        public T b() {
            Executor executor;
            if (this.c == null) {
                throw new IllegalArgumentException("Cannot provide null context for the database.");
            } else if (this.a != null) {
                if (this.e == null && this.f == null) {
                    Executor b2 = q3.b();
                    this.f = b2;
                    this.e = b2;
                } else {
                    Executor executor2 = this.e;
                    if (executor2 != null && this.f == null) {
                        this.f = executor2;
                    } else if (this.e == null && (executor = this.f) != null) {
                        this.e = executor;
                    }
                }
                Set<Integer> set = this.o;
                if (!(set == null || this.n == null)) {
                    for (Integer next : set) {
                        if (this.n.contains(next)) {
                            throw new IllegalArgumentException("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: " + next);
                        }
                    }
                }
                if (this.g == null) {
                    this.g = new pi();
                }
                if (!(this.p == null && this.q == null)) {
                    if (this.b == null) {
                        throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.");
                    } else if (this.p == null || this.q == null) {
                        this.g = new uh(this.p, this.q, this.g);
                    } else {
                        throw new IllegalArgumentException("Both createFromAsset() and createFromFile() was called on this Builder but the database can only be created using one of the two configurations.");
                    }
                }
                Context context = this.c;
                String str = this.b;
                ji.c cVar = this.g;
                d dVar = this.m;
                ArrayList<b> arrayList = this.d;
                boolean z = this.h;
                c resolve = this.i.resolve(context);
                Executor executor3 = this.e;
                Executor executor4 = this.f;
                boolean z2 = this.j;
                boolean z3 = this.k;
                boolean z4 = this.l;
                boolean z5 = z3;
                boolean z6 = z4;
                fh fhVar = new fh(context, str, cVar, dVar, arrayList, z, resolve, executor3, executor4, z2, z5, z6, this.n, this.p, this.q);
                T t = (oh) nh.a(this.a, oh.DB_IMPL_SUFFIX);
                t.init(fhVar);
                return t;
            } else {
                throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
            }
        }

        @DexIgnore
        public a<T> c() {
            this.j = this.b != null;
            return this;
        }

        @DexIgnore
        public a<T> d() {
            this.k = false;
            this.l = true;
            return this;
        }

        @DexIgnore
        public a<T> a() {
            this.h = true;
            return this;
        }

        @DexIgnore
        public a<T> a(Executor executor) {
            this.e = executor;
            return this;
        }

        @DexIgnore
        public a<T> a(b bVar) {
            if (this.d == null) {
                this.d = new ArrayList<>();
            }
            this.d.add(bVar);
            return this;
        }
    }

    @DexIgnore
    public <V> V runInTransaction(Callable<V> callable) {
        beginTransaction();
        try {
            V call = callable.call();
            setTransactionSuccessful();
            endTransaction();
            return call;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            di.a(e2);
            throw null;
        } catch (Throwable th) {
            endTransaction();
            throw th;
        }
    }
}
