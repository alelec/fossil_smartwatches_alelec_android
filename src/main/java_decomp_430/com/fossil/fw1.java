package com.fossil;

import com.fossil.ew1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fw1<R extends ew1> {
    @DexIgnore
    void onResult(R r);
}
