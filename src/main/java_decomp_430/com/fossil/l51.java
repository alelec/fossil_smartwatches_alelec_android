package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l51 implements Parcelable.Creator<i71> {
    @DexIgnore
    public /* synthetic */ l51(qg6 qg6) {
    }

    @DexIgnore
    public i71 createFromParcel(Parcel parcel) {
        return new i71(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new i71[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m36createFromParcel(Parcel parcel) {
        return new i71(parcel, (qg6) null);
    }
}
