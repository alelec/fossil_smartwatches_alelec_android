package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fw6 extends ew6 {
    @DexIgnore
    public static /* final */ fw6 NOP_LOGGER; // = new fw6();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -517220405410904473L;

    @DexIgnore
    public final void debug(String str) {
    }

    @DexIgnore
    public final void debug(String str, Object obj) {
    }

    @DexIgnore
    public final void debug(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    public final void debug(String str, Throwable th) {
    }

    @DexIgnore
    public final void debug(String str, Object... objArr) {
    }

    @DexIgnore
    public final void error(String str) {
    }

    @DexIgnore
    public final void error(String str, Object obj) {
    }

    @DexIgnore
    public final void error(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    public final void error(String str, Throwable th) {
    }

    @DexIgnore
    public final void error(String str, Object... objArr) {
    }

    @DexIgnore
    public String getName() {
        return "NOP";
    }

    @DexIgnore
    public final void info(String str) {
    }

    @DexIgnore
    public final void info(String str, Object obj) {
    }

    @DexIgnore
    public final void info(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    public final void info(String str, Throwable th) {
    }

    @DexIgnore
    public final void info(String str, Object... objArr) {
    }

    @DexIgnore
    public final boolean isDebugEnabled() {
        return false;
    }

    @DexIgnore
    public final boolean isErrorEnabled() {
        return false;
    }

    @DexIgnore
    public final boolean isInfoEnabled() {
        return false;
    }

    @DexIgnore
    public final boolean isTraceEnabled() {
        return false;
    }

    @DexIgnore
    public final boolean isWarnEnabled() {
        return false;
    }

    @DexIgnore
    public final void trace(String str) {
    }

    @DexIgnore
    public final void trace(String str, Object obj) {
    }

    @DexIgnore
    public final void trace(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    public final void trace(String str, Throwable th) {
    }

    @DexIgnore
    public final void trace(String str, Object... objArr) {
    }

    @DexIgnore
    public final void warn(String str) {
    }

    @DexIgnore
    public final void warn(String str, Object obj) {
    }

    @DexIgnore
    public final void warn(String str, Object obj, Object obj2) {
    }

    @DexIgnore
    public final void warn(String str, Throwable th) {
    }

    @DexIgnore
    public final void warn(String str, Object... objArr) {
    }
}
