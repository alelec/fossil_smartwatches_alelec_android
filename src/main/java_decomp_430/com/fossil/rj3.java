package com.fossil;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rj3 extends dj3 {
    @DexIgnore
    public /* final */ Paint A;
    @DexIgnore
    public /* final */ RectF B;
    @DexIgnore
    public int C;

    @DexIgnore
    public rj3() {
        this((hj3) null);
    }

    @DexIgnore
    public boolean D() {
        return !this.B.isEmpty();
    }

    @DexIgnore
    public void E() {
        a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void F() {
        this.A.setStyle(Paint.Style.FILL_AND_STROKE);
        this.A.setColor(-1);
        this.A.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
    }

    @DexIgnore
    public void a(float f, float f2, float f3, float f4) {
        RectF rectF = this.B;
        if (f != rectF.left || f2 != rectF.top || f3 != rectF.right || f4 != rectF.bottom) {
            this.B.set(f, f2, f3, f4);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        f(canvas);
        super.draw(canvas);
        canvas.drawRect(this.B, this.A);
        e(canvas);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        if (!a(getCallback())) {
            canvas.restoreToCount(this.C);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        Drawable.Callback callback = getCallback();
        if (a(callback)) {
            View view = (View) callback;
            if (view.getLayerType() != 2) {
                view.setLayerType(2, (Paint) null);
                return;
            }
            return;
        }
        g(canvas);
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.C = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), (Paint) null);
            return;
        }
        this.C = canvas.saveLayer(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) canvas.getWidth(), (float) canvas.getHeight(), (Paint) null, 31);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rj3(hj3 hj3) {
        super(hj3 == null ? new hj3() : hj3);
        this.A = new Paint(1);
        F();
        this.B = new RectF();
    }

    @DexIgnore
    public void a(RectF rectF) {
        a(rectF.left, rectF.top, rectF.right, rectF.bottom);
    }

    @DexIgnore
    public final boolean a(Drawable.Callback callback) {
        return callback instanceof View;
    }
}
