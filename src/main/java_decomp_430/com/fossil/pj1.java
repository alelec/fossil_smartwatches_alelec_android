package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pj1 {
    SERVICE_ADDED,
    CHARACTERISTIC_READ,
    CHARACTERISTIC_WRITE,
    DESCRIPTOR_READ,
    DESCRIPTOR_WRITE,
    EXECUTE_WRITE,
    MTU_CHANGED,
    GATT_SERVER_STARTED,
    GATT_SERVER_STOPPED
}
