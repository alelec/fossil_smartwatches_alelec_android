package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py4 implements Factory<oy4> {
    @DexIgnore
    public static NotificationCallsAndMessagesPresenter a(ky4 ky4, z24 z24, mz4 mz4, oz4 oz4, pz4 pz4, d15 d15, an4 an4, NotificationSettingsDao notificationSettingsDao, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationCallsAndMessagesPresenter(ky4, z24, mz4, oz4, pz4, d15, an4, notificationSettingsDao, setReplyMessageMappingUseCase, quickResponseRepository, notificationSettingsDatabase);
    }
}
