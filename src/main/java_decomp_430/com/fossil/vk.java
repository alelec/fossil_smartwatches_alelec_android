package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.util.AttributeSet;
import android.view.InflateException;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;
import org.xmlpull.v1.XmlPullParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vk implements Interpolator {
    @DexIgnore
    public float[] a;
    @DexIgnore
    public float[] b;

    @DexIgnore
    public vk(Context context, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        this(context.getResources(), context.getTheme(), attributeSet, xmlPullParser);
    }

    @DexIgnore
    public final void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
        if (e7.a(xmlPullParser, "pathData")) {
            String a2 = e7.a(typedArray, xmlPullParser, "pathData", 4);
            Path b2 = g7.b(a2);
            if (b2 != null) {
                a(b2);
                return;
            }
            throw new InflateException("The path is null, which is created from " + a2);
        } else if (!e7.a(xmlPullParser, "controlX1")) {
            throw new InflateException("pathInterpolator requires the controlX1 attribute");
        } else if (e7.a(xmlPullParser, "controlY1")) {
            float a3 = e7.a(typedArray, xmlPullParser, "controlX1", 0, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a4 = e7.a(typedArray, xmlPullParser, "controlY1", 1, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            boolean a5 = e7.a(xmlPullParser, "controlX2");
            if (a5 != e7.a(xmlPullParser, "controlY2")) {
                throw new InflateException("pathInterpolator requires both controlX2 and controlY2 for cubic Beziers.");
            } else if (!a5) {
                a(a3, a4);
            } else {
                a(a3, a4, e7.a(typedArray, xmlPullParser, "controlX2", 2, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES), e7.a(typedArray, xmlPullParser, "controlY2", 3, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            }
        } else {
            throw new InflateException("pathInterpolator requires the controlY1 attribute");
        }
    }

    @DexIgnore
    public float getInterpolation(float f) {
        if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        if (f >= 1.0f) {
            return 1.0f;
        }
        int i = 0;
        int length = this.a.length - 1;
        while (length - i > 1) {
            int i2 = (i + length) / 2;
            if (f < this.a[i2]) {
                length = i2;
            } else {
                i = i2;
            }
        }
        float[] fArr = this.a;
        float f2 = fArr[length] - fArr[i];
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return this.b[i];
        }
        float[] fArr2 = this.b;
        float f3 = fArr2[i];
        return f3 + (((f - fArr[i]) / f2) * (fArr2[length] - f3));
    }

    @DexIgnore
    public vk(Resources resources, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        TypedArray a2 = e7.a(resources, theme, attributeSet, pk.l);
        a(a2, xmlPullParser);
        a2.recycle();
    }

    @DexIgnore
    public final void a(float f, float f2) {
        Path path = new Path();
        path.moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        path.quadTo(f, f2, 1.0f, 1.0f);
        a(path);
    }

    @DexIgnore
    public final void a(float f, float f2, float f3, float f4) {
        Path path = new Path();
        path.moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        path.cubicTo(f, f2, f3, f4, 1.0f, 1.0f);
        a(path);
    }

    @DexIgnore
    public final void a(Path path) {
        int i = 0;
        PathMeasure pathMeasure = new PathMeasure(path, false);
        float length = pathMeasure.getLength();
        int min = Math.min(3000, ((int) (length / 0.002f)) + 1);
        if (min > 0) {
            this.a = new float[min];
            this.b = new float[min];
            float[] fArr = new float[2];
            for (int i2 = 0; i2 < min; i2++) {
                pathMeasure.getPosTan((((float) i2) * length) / ((float) (min - 1)), fArr, (float[]) null);
                this.a[i2] = fArr[0];
                this.b[i2] = fArr[1];
            }
            if (((double) Math.abs(this.a[0])) <= 1.0E-5d && ((double) Math.abs(this.b[0])) <= 1.0E-5d) {
                int i3 = min - 1;
                if (((double) Math.abs(this.a[i3] - 1.0f)) <= 1.0E-5d && ((double) Math.abs(this.b[i3] - 1.0f)) <= 1.0E-5d) {
                    int i4 = 0;
                    float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    while (i < min) {
                        float[] fArr2 = this.a;
                        int i5 = i4 + 1;
                        float f2 = fArr2[i4];
                        if (f2 >= f) {
                            fArr2[i] = f2;
                            i++;
                            f = f2;
                            i4 = i5;
                        } else {
                            throw new IllegalArgumentException("The Path cannot loop back on itself, x :" + f2);
                        }
                    }
                    if (pathMeasure.nextContour()) {
                        throw new IllegalArgumentException("The Path should be continuous, can't have 2+ contours");
                    }
                    return;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("The Path must start at (0,0) and end at (1,1) start: ");
            sb.append(this.a[0]);
            sb.append(",");
            sb.append(this.b[0]);
            sb.append(" end:");
            int i6 = min - 1;
            sb.append(this.a[i6]);
            sb.append(",");
            sb.append(this.b[i6]);
            throw new IllegalArgumentException(sb.toString());
        }
        throw new IllegalArgumentException("The Path has a invalid length " + length);
    }
}
