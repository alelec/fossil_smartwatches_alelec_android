package com.fossil;

import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q20 implements Flushable {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public /* final */ OutputStream d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends IOException {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = -6947486886997889499L;

        @DexIgnore
        public a() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
    }

    @DexIgnore
    public q20(OutputStream outputStream, byte[] bArr) {
        this.d = outputStream;
        this.a = bArr;
        this.b = bArr.length;
    }

    @DexIgnore
    public static q20 a(OutputStream outputStream) {
        return a(outputStream, 4096);
    }

    @DexIgnore
    public static int b(float f) {
        return 4;
    }

    @DexIgnore
    public static int b(boolean z) {
        return 1;
    }

    @DexIgnore
    public static int c(n20 n20) {
        return j(n20.b()) + n20.b();
    }

    @DexIgnore
    public static int e(int i, int i2) {
        return l(i) + h(i2);
    }

    @DexIgnore
    public static int e(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (j & Long.MIN_VALUE) == 0 ? 9 : 10;
    }

    @DexIgnore
    public static int h(int i) {
        return i(i);
    }

    @DexIgnore
    public static int i(int i) {
        if (i >= 0) {
            return j(i);
        }
        return 10;
    }

    @DexIgnore
    public static int j(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    @DexIgnore
    public static int k(int i) {
        return j(n(i));
    }

    @DexIgnore
    public static int l(int i) {
        return j(j40.a(i, 0));
    }

    @DexIgnore
    public static int m(int i) {
        return j(i);
    }

    @DexIgnore
    public static int n(int i) {
        return (i >> 31) ^ (i << 1);
    }

    @DexIgnore
    public void b(int i, int i2) throws IOException {
        c(i, 0);
        f(i2);
    }

    @DexIgnore
    public void d(int i, int i2) throws IOException {
        c(i, 0);
        g(i2);
    }

    @DexIgnore
    public void f(int i) throws IOException {
        e(n(i));
    }

    @DexIgnore
    public void flush() throws IOException {
        if (this.d != null) {
            e();
        }
    }

    @DexIgnore
    public void g(int i) throws IOException {
        e(i);
    }

    @DexIgnore
    public static q20 a(OutputStream outputStream, int i) {
        return new q20(outputStream, new byte[i]);
    }

    @DexIgnore
    public static int f(int i, int i2) {
        return l(i) + k(i2);
    }

    @DexIgnore
    public static int g(int i, int i2) {
        return l(i) + m(i2);
    }

    @DexIgnore
    public final void e() throws IOException {
        OutputStream outputStream = this.d;
        if (outputStream != null) {
            outputStream.write(this.a, 0, this.c);
            this.c = 0;
            return;
        }
        throw new a();
    }

    @DexIgnore
    public static int f(long j) {
        return e(j);
    }

    @DexIgnore
    public void a(int i, float f) throws IOException {
        c(i, 5);
        a(f);
    }

    @DexIgnore
    public void b(long j) throws IOException {
        a(j);
    }

    @DexIgnore
    public void c(int i) throws IOException {
        a((byte) i);
    }

    @DexIgnore
    public void d(int i) throws IOException {
        c(i & 255);
        c((i >> 8) & 255);
        c((i >> 16) & 255);
        c((i >> 24) & 255);
    }

    @DexIgnore
    public void b(int i) throws IOException {
        if (i >= 0) {
            e(i);
        } else {
            a((long) i);
        }
    }

    @DexIgnore
    public void c(int i, int i2) throws IOException {
        e(j40.a(i, i2));
    }

    @DexIgnore
    public void a(int i, long j) throws IOException {
        c(i, 0);
        b(j);
    }

    @DexIgnore
    public static int b(int i, float f) {
        return l(i) + b(f);
    }

    @DexIgnore
    public void e(int i) throws IOException {
        while ((i & -128) != 0) {
            c((i & 127) | 128);
            i >>>= 7;
        }
        c(i);
    }

    @DexIgnore
    public static int b(int i, long j) {
        return l(i) + f(j);
    }

    @DexIgnore
    public void a(int i, boolean z) throws IOException {
        c(i, 0);
        a(z);
    }

    @DexIgnore
    public static int b(int i, boolean z) {
        return l(i) + b(z);
    }

    @DexIgnore
    public static int b(int i, n20 n20) {
        return l(i) + c(n20);
    }

    @DexIgnore
    public void a(int i, n20 n20) throws IOException {
        c(i, 2);
        a(n20);
    }

    @DexIgnore
    public void b(n20 n20) throws IOException {
        a(n20, 0, n20.b());
    }

    @DexIgnore
    public void a(int i, int i2) throws IOException {
        c(i, 0);
        a(i2);
    }

    @DexIgnore
    public void a(float f) throws IOException {
        d(Float.floatToRawIntBits(f));
    }

    @DexIgnore
    public void a(boolean z) throws IOException {
        c(z ? 1 : 0);
    }

    @DexIgnore
    public void a(n20 n20) throws IOException {
        e(n20.b());
        b(n20);
    }

    @DexIgnore
    public void a(int i) throws IOException {
        b(i);
    }

    @DexIgnore
    public void a(byte b2) throws IOException {
        if (this.c == this.b) {
            e();
        }
        byte[] bArr = this.a;
        int i = this.c;
        this.c = i + 1;
        bArr[i] = b2;
    }

    @DexIgnore
    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void a(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.b;
        int i4 = this.c;
        if (i3 - i4 >= i2) {
            System.arraycopy(bArr, i, this.a, i4, i2);
            this.c += i2;
            return;
        }
        int i5 = i3 - i4;
        System.arraycopy(bArr, i, this.a, i4, i5);
        int i6 = i + i5;
        int i7 = i2 - i5;
        this.c = this.b;
        e();
        if (i7 <= this.b) {
            System.arraycopy(bArr, i6, this.a, 0, i7);
            this.c = i7;
            return;
        }
        this.d.write(bArr, i6, i7);
    }

    @DexIgnore
    public void a(n20 n20, int i, int i2) throws IOException {
        int i3 = this.b;
        int i4 = this.c;
        if (i3 - i4 >= i2) {
            n20.a(this.a, i, i4, i2);
            this.c += i2;
            return;
        }
        int i5 = i3 - i4;
        n20.a(this.a, i, i4, i5);
        int i6 = i + i5;
        int i7 = i2 - i5;
        this.c = this.b;
        e();
        if (i7 <= this.b) {
            n20.a(this.a, i6, 0, i7);
            this.c = i7;
            return;
        }
        InputStream a2 = n20.a();
        long j = (long) i6;
        if (j == a2.skip(j)) {
            while (i7 > 0) {
                int min = Math.min(i7, this.b);
                int read = a2.read(this.a, 0, min);
                if (read == min) {
                    this.d.write(this.a, 0, read);
                    i7 -= read;
                } else {
                    throw new IllegalStateException("Read failed.");
                }
            }
            return;
        }
        throw new IllegalStateException("Skip failed.");
    }

    @DexIgnore
    public void a(long j) throws IOException {
        while ((-128 & j) != 0) {
            c((((int) j) & 127) | 128);
            j >>>= 7;
        }
        c((int) j);
    }
}
