package com.fossil;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationResult;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jw2 implements Parcelable.Creator<LocationResult> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        List<Location> list = LocationResult.b;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 1) {
                f22.v(parcel, a);
            } else {
                list = f22.c(parcel, a, Location.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new LocationResult(list);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationResult[i];
    }
}
