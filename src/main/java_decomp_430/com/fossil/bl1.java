package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl1 extends rf1 {
    @DexIgnore
    public /* final */ nq0 I;
    @DexIgnore
    public /* final */ byte[] J;

    @DexIgnore
    public bl1(ue1 ue1, nq0 nq0, byte[] bArr) {
        super(ue1, sv0.SEND_BOTH_SIDES_RANDOM_NUMBERS, lx0.SEND_BOTH_SIDES_RANDOM_NUMBERS, 0, 8);
        this.I = nq0;
        this.J = bArr;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.AUTHENTICATION_KEY_TYPE, (Object) cw0.a((Enum<?>) this.I)), bm0.BOTH_SIDES_RANDOM_NUMBERS, (Object) cw0.a(this.J, (String) null, 1));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(this.J.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.I.a).put(this.J).array();
        wg6.a(array, "ByteBuffer.allocate(KEY_\u2026\n                .array()");
        return array;
    }
}
