package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import com.fossil.up;
import com.fossil.wp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oq extends up<Bitmap> {
    @DexIgnore
    public static /* final */ float DEFAULT_IMAGE_BACKOFF_MULT; // = 2.0f;
    @DexIgnore
    public static /* final */ int DEFAULT_IMAGE_MAX_RETRIES; // = 2;
    @DexIgnore
    public static /* final */ int DEFAULT_IMAGE_TIMEOUT_MS; // = 1000;
    @DexIgnore
    public static /* final */ Object sDecodeLock; // = new Object();
    @DexIgnore
    public /* final */ Bitmap.Config mDecodeConfig;
    @DexIgnore
    public wp.b<Bitmap> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ int mMaxHeight;
    @DexIgnore
    public /* final */ int mMaxWidth;
    @DexIgnore
    public /* final */ ImageView.ScaleType mScaleType;

    @DexIgnore
    public oq(String str, wp.b<Bitmap> bVar, int i, int i2, ImageView.ScaleType scaleType, Bitmap.Config config, wp.a aVar) {
        super(0, str, aVar);
        this.mLock = new Object();
        setRetryPolicy(new lp(1000, 2, 2.0f));
        this.mListener = bVar;
        this.mDecodeConfig = config;
        this.mMaxWidth = i;
        this.mMaxHeight = i2;
        this.mScaleType = scaleType;
    }

    @DexIgnore
    private wp<Bitmap> doParse(rp rpVar) {
        Bitmap bitmap;
        byte[] bArr = rpVar.b;
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (this.mMaxWidth == 0 && this.mMaxHeight == 0) {
            options.inPreferredConfig = this.mDecodeConfig;
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i = options.outWidth;
            int i2 = options.outHeight;
            int resizedDimension = getResizedDimension(this.mMaxWidth, this.mMaxHeight, i, i2, this.mScaleType);
            int resizedDimension2 = getResizedDimension(this.mMaxHeight, this.mMaxWidth, i2, i, this.mScaleType);
            options.inJustDecodeBounds = false;
            options.inSampleSize = findBestSampleSize(i, i2, resizedDimension, resizedDimension2);
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (bitmap != null && (bitmap.getWidth() > resizedDimension || bitmap.getHeight() > resizedDimension2)) {
                Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, resizedDimension, resizedDimension2, true);
                bitmap.recycle();
                bitmap = createScaledBitmap;
            }
        }
        if (bitmap == null) {
            return wp.a(new tp(rpVar));
        }
        return wp.a(bitmap, jq.a(rpVar));
    }

    @DexIgnore
    public static int findBestSampleSize(int i, int i2, int i3, int i4) {
        double min = Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4));
        float f = 1.0f;
        while (true) {
            float f2 = 2.0f * f;
            if (((double) f2) > min) {
                return (int) f;
            }
            f = f2;
        }
    }

    @DexIgnore
    public static int getResizedDimension(int i, int i2, int i3, int i4, ImageView.ScaleType scaleType) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (scaleType == ImageView.ScaleType.FIT_XY) {
            return i == 0 ? i3 : i;
        }
        if (i == 0) {
            return (int) (((double) i3) * (((double) i2) / ((double) i4)));
        } else if (i2 == 0) {
            return i;
        } else {
            double d = ((double) i4) / ((double) i3);
            if (scaleType == ImageView.ScaleType.CENTER_CROP) {
                double d2 = (double) i2;
                return ((double) i) * d < d2 ? (int) (d2 / d) : i;
            }
            double d3 = (double) i2;
            return ((double) i) * d > d3 ? (int) (d3 / d) : i;
        }
    }

    @DexIgnore
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    public up.c getPriority() {
        return up.c.LOW;
    }

    @DexIgnore
    public wp<Bitmap> parseNetworkResponse(rp rpVar) {
        wp<Bitmap> doParse;
        synchronized (sDecodeLock) {
            try {
                doParse = doParse(rpVar);
            } catch (OutOfMemoryError e) {
                cq.c("Caught OOM for %d byte image, url=%s", Integer.valueOf(rpVar.b.length), getUrl());
                return wp.a(new tp((Throwable) e));
            } catch (Throwable th) {
                throw th;
            }
        }
        return doParse;
    }

    @DexIgnore
    public void deliverResponse(Bitmap bitmap) {
        wp.b<Bitmap> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(bitmap);
        }
    }

    @DexIgnore
    @Deprecated
    public oq(String str, wp.b<Bitmap> bVar, int i, int i2, Bitmap.Config config, wp.a aVar) {
        this(str, bVar, i, i2, ImageView.ScaleType.CENTER_INSIDE, config, aVar);
    }
}
