package com.fossil;

import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rd {
    @DexIgnore
    public /* final */ LifecycleRegistry a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public a c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ LifecycleRegistry a;
        @DexIgnore
        public /* final */ Lifecycle.a b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public a(LifecycleRegistry lifecycleRegistry, Lifecycle.a aVar) {
            this.a = lifecycleRegistry;
            this.b = aVar;
        }

        @DexIgnore
        public void run() {
            if (!this.c) {
                this.a.a(this.b);
                this.c = true;
            }
        }
    }

    @DexIgnore
    public rd(LifecycleOwner lifecycleOwner) {
        this.a = new LifecycleRegistry(lifecycleOwner);
    }

    @DexIgnore
    public final void a(Lifecycle.a aVar) {
        a aVar2 = this.c;
        if (aVar2 != null) {
            aVar2.run();
        }
        this.c = new a(this.a, aVar);
        this.b.postAtFrontOfQueue(this.c);
    }

    @DexIgnore
    public void b() {
        a(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public void c() {
        a(Lifecycle.a.ON_CREATE);
    }

    @DexIgnore
    public void d() {
        a(Lifecycle.a.ON_STOP);
        a(Lifecycle.a.ON_DESTROY);
    }

    @DexIgnore
    public void e() {
        a(Lifecycle.a.ON_START);
    }

    @DexIgnore
    public Lifecycle a() {
        return this.a;
    }
}
