package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vj3 extends tj3 {
    @DexIgnore
    public vj3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public void a() {
        this.a.setEndIconOnClickListener((View.OnClickListener) null);
        this.a.setEndIconDrawable((Drawable) null);
        this.a.setEndIconContentDescription((CharSequence) null);
    }
}
