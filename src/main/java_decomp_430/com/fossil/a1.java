package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a1 {
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableCompat; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableCompat_android_visible; // = 1;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableItem; // = {16842960, 16843161};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_drawable; // = 1;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableItem_android_id; // = 0;
    @DexIgnore
    public static /* final */ int[] AnimatedStateListDrawableTransition; // = {16843161, 16843849, 16843850, 16843851};
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_fromId; // = 2;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_reversible; // = 3;
    @DexIgnore
    public static /* final */ int AnimatedStateListDrawableTransition_android_toId; // = 1;
    @DexIgnore
    public static /* final */ int[] ColorStateListItem; // = {16843173, 16843551, 2130968745};
    @DexIgnore
    public static /* final */ int ColorStateListItem_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_alpha; // = 1;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int[] FontFamily; // = {2130969331, 2130969332, 2130969333, 2130969334, 2130969335, 2130969336};
    @DexIgnore
    public static /* final */ int[] FontFamilyFont; // = {16844082, 16844083, 16844095, 16844143, 16844144, 2130969326, 2130969337, 2130969338, 2130969339, 2130970059};
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_font; // = 0;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontStyle; // = 2;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontVariationSettings; // = 4;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontWeight; // = 1;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_ttcIndex; // = 3;
    @DexIgnore
    public static /* final */ int FontFamilyFont_font; // = 5;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontVariationSettings; // = 7;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontWeight; // = 8;
    @DexIgnore
    public static /* final */ int FontFamilyFont_ttcIndex; // = 9;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderAuthority; // = 0;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderCerts; // = 1;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchStrategy; // = 2;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchTimeout; // = 3;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderPackage; // = 4;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderQuery; // = 5;
    @DexIgnore
    public static /* final */ int[] GradientColor; // = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    @DexIgnore
    public static /* final */ int[] GradientColorItem; // = {16843173, 16844052};
    @DexIgnore
    public static /* final */ int GradientColorItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int GradientColorItem_android_offset; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerColor; // = 7;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerX; // = 3;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerY; // = 4;
    @DexIgnore
    public static /* final */ int GradientColor_android_endColor; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_endX; // = 10;
    @DexIgnore
    public static /* final */ int GradientColor_android_endY; // = 11;
    @DexIgnore
    public static /* final */ int GradientColor_android_gradientRadius; // = 5;
    @DexIgnore
    public static /* final */ int GradientColor_android_startColor; // = 0;
    @DexIgnore
    public static /* final */ int GradientColor_android_startX; // = 8;
    @DexIgnore
    public static /* final */ int GradientColor_android_startY; // = 9;
    @DexIgnore
    public static /* final */ int GradientColor_android_tileMode; // = 6;
    @DexIgnore
    public static /* final */ int GradientColor_android_type; // = 2;
    @DexIgnore
    public static /* final */ int[] StateListDrawable; // = {16843036, 16843156, 16843157, 16843158, 16843532, 16843533};
    @DexIgnore
    public static /* final */ int[] StateListDrawableItem; // = {16843161};
    @DexIgnore
    public static /* final */ int StateListDrawableItem_android_drawable; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_constantSize; // = 3;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_dither; // = 0;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_enterFadeDuration; // = 4;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_exitFadeDuration; // = 5;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_variablePadding; // = 2;
    @DexIgnore
    public static /* final */ int StateListDrawable_android_visible; // = 1;
}
