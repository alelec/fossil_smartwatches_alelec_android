package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x76<T> implements Factory<T>, b76<T> {
    @DexIgnore
    public /* final */ T a;

    /*
    static {
        new x76((Object) null);
    }
    */

    @DexIgnore
    public x76(T t) {
        this.a = t;
    }

    @DexIgnore
    public static <T> Factory<T> a(T t) {
        z76.a(t, "instance cannot be null");
        return new x76(t);
    }

    @DexIgnore
    public T get() {
        return this.a;
    }
}
