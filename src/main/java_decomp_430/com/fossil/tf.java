package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tf implements jg {
    @DexIgnore
    public /* final */ RecyclerView.g a;

    @DexIgnore
    public tf(RecyclerView.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    public void a(int i, int i2) {
        this.a.notifyItemMoved(i, i2);
    }

    @DexIgnore
    public void b(int i, int i2) {
        this.a.notifyItemRangeInserted(i, i2);
    }

    @DexIgnore
    public void c(int i, int i2) {
        this.a.notifyItemRangeRemoved(i, i2);
    }

    @DexIgnore
    public void a(int i, int i2, Object obj) {
        this.a.notifyItemRangeChanged(i, i2, obj);
    }
}
