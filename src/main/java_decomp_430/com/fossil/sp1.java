package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sp1 implements jo1 {
    @DexIgnore
    public /* final */ rp1 a;
    @DexIgnore
    public /* final */ vp1 b;

    @DexIgnore
    public sp1(rp1 rp1, vp1 vp1) {
        this.a = rp1;
        this.b = vp1;
    }

    @DexIgnore
    public <T> io1<T> a(String str, Class<T> cls, ho1<T, byte[]> ho1) {
        return new up1(this.a, str, ho1, this.b);
    }
}
