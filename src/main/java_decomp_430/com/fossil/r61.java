package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r61 extends ml0 {
    @DexIgnore
    public r61(ue1 ue1) {
        super(tf1.LEGACY_OTA_RESET, lx0.LEGACY_OTA_RESET, ue1, 0, 8);
    }

    @DexIgnore
    public void a(ni1 ni1) {
        if (ni1.a != h91.DISCONNECTED) {
            a(bn0.a(this.v, (lx0) null, (String) null, il0.UNKNOWN_ERROR, (ch0) null, (sj0) null, 27));
        } else if (ni1.b == 19 || this.B) {
            a(this.v);
        } else {
            a(bn0.a(this.v, (lx0) null, (String) null, il0.CONNECTION_DROPPED, (ch0) null, (sj0) null, 27));
        }
    }
}
