package com.fossil;

import android.media.session.MediaSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ne implements me {
    @DexIgnore
    public /* final */ MediaSessionManager.RemoteUserInfo a;

    @DexIgnore
    public ne(String str, int i, int i2) {
        this.a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ne)) {
            return false;
        }
        return this.a.equals(((ne) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return t8.a(this.a);
    }
}
