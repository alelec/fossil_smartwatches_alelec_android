package com.fossil;

import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
public final class l55$c$a extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CommuteTimeSettingsPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l55$c$a(CommuteTimeSettingsPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l55$c$a l55_c_a = new l55$c$a(this.this$0, xe6);
        l55_c_a.p$ = (il6) obj;
        return l55_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l55$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.m.getCurrentUser();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
