package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n52 extends l52 {
    @DexIgnore
    public static /* final */ WeakReference<byte[]> c; // = new WeakReference<>((Object) null);
    @DexIgnore
    public WeakReference<byte[]> b; // = c;

    @DexIgnore
    public n52(byte[] bArr) {
        super(bArr);
    }

    @DexIgnore
    public final byte[] q() {
        byte[] bArr;
        synchronized (this) {
            bArr = (byte[]) this.b.get();
            if (bArr == null) {
                bArr = r();
                this.b = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    @DexIgnore
    public abstract byte[] r();
}
