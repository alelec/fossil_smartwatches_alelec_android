package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AlphabetIndexer;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv4 extends lv4<c> implements SectionIndexer {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Integer> i; // = new ArrayList<>();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int o; // = -1;
    @DexIgnore
    public b p;
    @DexIgnore
    public AlphabetIndexer q;
    @DexIgnore
    public /* final */ List<wx4> r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public /* final */ bg4 h;
        @DexIgnore
        public /* final */ /* synthetic */ jv4 i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.b();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.a.b();
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v9, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(jv4 jv4, bg4 bg4) {
            super(bg4.d());
            wg6.b(bg4, "binding");
            this.i = jv4;
            this.h = bg4;
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                this.h.s.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.h.x.setBackgroundColor(Color.parseColor(b3));
            }
            this.h.r.setOnClickListener(new a(this));
            this.h.q.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void b() {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                Iterator it = this.i.r.iterator();
                int i2 = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    Contact contact = ((wx4) it.next()).getContact();
                    if (contact != null && contact.getContactId() == this.e) {
                        break;
                    }
                    i2++;
                }
                if (i2 != -1) {
                    this.i.r.remove(i2);
                } else {
                    this.i.r.add(a());
                }
                b c2 = this.i.p;
                if (c2 != null) {
                    c2.a();
                }
                this.i.notifyItemChanged(adapterPosition);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r10v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v8, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r10v25, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v26, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(Cursor cursor, int i2) {
            Object obj;
            boolean z;
            wg6.b(cursor, "cursor");
            cursor.moveToPosition(i2);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d2 = jv4.s;
            local.d(d2, ".Inside renderData, cursor move position=" + i2);
            this.a = cursor.getString(cursor.getColumnIndex("display_name"));
            this.b = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
            this.e = cursor.getInt(cursor.getColumnIndex("contact_id"));
            this.f = cursor.getInt(cursor.getColumnIndex("has_phone_number"));
            this.c = cursor.getString(cursor.getColumnIndex("data1"));
            this.d = cursor.getString(cursor.getColumnIndex("sort_key"));
            boolean z2 = true;
            this.g = cursor.getInt(cursor.getColumnIndex("starred")) == 1;
            if (this.i.o < i2) {
                this.i.o = i2;
            }
            String b2 = gy5.b(this.c);
            if (this.i.i.contains(Integer.valueOf(i2)) || (this.i.o < i2 && this.i.j == this.e && this.i.h.contains(b2))) {
                if (!this.i.i.contains(Integer.valueOf(i2))) {
                    this.i.i.add(Integer.valueOf(i2));
                }
                a(8);
            } else {
                if (i2 > this.i.o) {
                    this.i.o = i2;
                }
                if (this.i.j != this.e) {
                    this.i.h.clear();
                    this.i.j = this.e;
                }
                this.i.h.add(b2);
                a(0);
                if (cursor.moveToPrevious() && cursor.getInt(cursor.getColumnIndex("contact_id")) == this.e) {
                    Object r0 = this.h.q;
                    wg6.a((Object) r0, "binding.accbSelect");
                    r0.setVisibility(4);
                    Object r02 = this.h.w;
                    wg6.a((Object) r02, "binding.pickContactTitle");
                    r02.setVisibility(8);
                }
                cursor.moveToNext();
            }
            Object r10 = this.h.w;
            wg6.a((Object) r10, "binding.pickContactTitle");
            r10.setText(this.a);
            if (this.f == 1) {
                Object r102 = this.h.v;
                wg6.a((Object) r102, "binding.pickContactPhone");
                r102.setText(this.c);
                Object r103 = this.h.v;
                wg6.a((Object) r103, "binding.pickContactPhone");
                r103.setVisibility(0);
            } else {
                Object r104 = this.h.v;
                wg6.a((Object) r104, "binding.pickContactPhone");
                r104.setVisibility(8);
            }
            Iterator it = this.i.r.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                Contact contact = ((wx4) obj).getContact();
                if (contact == null || contact.getContactId() != this.e) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            wx4 wx4 = (wx4) obj;
            Object r105 = this.h.q;
            wg6.a((Object) r105, "binding.accbSelect");
            if (wx4 == null) {
                z2 = false;
            }
            r105.setChecked(z2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String d3 = jv4.s;
            local2.d(d3, "Inside renderData, contactId = " + this.e + ", displayName = " + this.a + ", hasPhoneNumber = " + this.f + ',' + " phoneNumber = " + this.c + ", newSortKey = " + this.d);
            if (i2 == this.i.getPositionForSection(this.i.getSectionForPosition(i2))) {
                Object r106 = this.h.t;
                wg6.a((Object) r106, "binding.ftvAlphabet");
                r106.setVisibility(0);
                Object r107 = this.h.t;
                wg6.a((Object) r107, "binding.ftvAlphabet");
                r107.setText(Character.toString(yk4.b.a(this.d)));
                return;
            }
            Object r108 = this.h.t;
            wg6.a((Object) r108, "binding.ftvAlphabet");
            r108.setVisibility(8);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleCheckBox, android.widget.CheckBox, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(int i2) {
            Object r0 = this.h.q;
            wg6.a((Object) r0, "binding.accbSelect");
            r0.setVisibility(i2);
            ConstraintLayout constraintLayout = this.h.r;
            wg6.a((Object) constraintLayout, "binding.clMainContainer");
            constraintLayout.setVisibility(i2);
            Object r02 = this.h.t;
            wg6.a((Object) r02, "binding.ftvAlphabet");
            r02.setVisibility(i2);
            LinearLayout linearLayout = this.h.u;
            wg6.a((Object) linearLayout, "binding.llTextContainer");
            linearLayout.setVisibility(i2);
            Object r03 = this.h.v;
            wg6.a((Object) r03, "binding.pickContactPhone");
            r03.setVisibility(i2);
            Object r04 = this.h.w;
            wg6.a((Object) r04, "binding.pickContactTitle");
            r04.setVisibility(i2);
            View view = this.h.x;
            wg6.a((Object) view, "binding.vLineSeparation");
            view.setVisibility(i2);
            ConstraintLayout constraintLayout2 = this.h.s;
            wg6.a((Object) constraintLayout2, "binding.clRoot");
            constraintLayout2.setVisibility(i2);
        }

        @DexIgnore
        public final wx4 a() {
            Contact contact = new Contact();
            contact.setContactId(this.e);
            contact.setFirstName(this.a);
            contact.setPhotoThumbUri(this.b);
            wx4 wx4 = new wx4(contact, (String) null, 2, (qg6) null);
            if (this.f == 1) {
                wx4.setHasPhoneNumber(true);
                wx4.setPhoneNumber(this.c);
            } else {
                wx4.setHasPhoneNumber(false);
            }
            Contact contact2 = wx4.getContact();
            if (contact2 != null) {
                contact2.setUseSms(true);
                Contact contact3 = wx4.getContact();
                if (contact3 != null) {
                    contact3.setUseCall(true);
                    wx4.setFavorites(this.g);
                    wx4.setAdded(true);
                    return wx4;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = jv4.class.getSimpleName();
        wg6.a((Object) simpleName, "CursorContactSearchAdapter::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jv4(Cursor cursor, List<wx4> list) {
        super(cursor);
        wg6.b(list, "mContactWrapperList");
        this.r = list;
    }

    @DexIgnore
    public int getPositionForSection(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getPositionForSection(i2);
            }
            wg6.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public int getSectionForPosition(int i2) {
        try {
            AlphabetIndexer alphabetIndexer = this.q;
            if (alphabetIndexer != null) {
                return alphabetIndexer.getSectionForPosition(i2);
            }
            wg6.a();
            throw null;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @DexIgnore
    public Object[] getSections() {
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            return alphabetIndexer.getSections();
        }
        return null;
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return null;
        }
        this.q = new AlphabetIndexer(cursor, cursor.getColumnIndex("display_name"), "#ABCDEFGHIJKLMNOPQRTSUVWXYZ");
        AlphabetIndexer alphabetIndexer = this.q;
        if (alphabetIndexer != null) {
            alphabetIndexer.setCursor(cursor);
            this.h.clear();
            this.i.clear();
            this.j = -1;
            this.o = -1;
            return super.c(cursor);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        bg4 a2 = bg4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemContactBinding.infla\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public void a(c cVar, Cursor cursor, int i2) {
        wg6.b(cVar, "holder");
        if (cursor != null) {
            cVar.a(cursor, i2);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "constraint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "filter: constraint = " + str);
        getFilter().filter(str);
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "itemClickListener");
        this.p = bVar;
    }
}
