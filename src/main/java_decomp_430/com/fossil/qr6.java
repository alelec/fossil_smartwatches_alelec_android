package com.fossil;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr6 {
    @DexIgnore
    public /* final */ Set<ar6> a; // = new LinkedHashSet();

    @DexIgnore
    public synchronized void a(ar6 ar6) {
        this.a.remove(ar6);
    }

    @DexIgnore
    public synchronized void b(ar6 ar6) {
        this.a.add(ar6);
    }

    @DexIgnore
    public synchronized boolean c(ar6 ar6) {
        return this.a.contains(ar6);
    }
}
