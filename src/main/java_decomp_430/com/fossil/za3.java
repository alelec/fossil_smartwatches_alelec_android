package com.fossil;

import com.fossil.aj2;
import com.fossil.yi2;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class za3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Boolean c;
    @DexIgnore
    public Boolean d;
    @DexIgnore
    public Long e;
    @DexIgnore
    public Long f;

    @DexIgnore
    public za3(String str, int i) {
        this.a = str;
        this.b = i;
    }

    @DexIgnore
    public static Boolean a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public static Boolean a(String str, aj2 aj2, t43 t43) {
        String str2;
        List<String> list;
        w12.a(aj2);
        if (str == null || !aj2.n() || aj2.o() == aj2.b.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (aj2.o() == aj2.b.IN_LIST) {
            if (aj2.v() == 0) {
                return null;
            }
        } else if (!aj2.p()) {
            return null;
        }
        aj2.b o = aj2.o();
        boolean s = aj2.s();
        if (s || o == aj2.b.REGEXP || o == aj2.b.IN_LIST) {
            str2 = aj2.q();
        } else {
            str2 = aj2.q().toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        if (aj2.v() == 0) {
            list = null;
        } else {
            List<String> t = aj2.t();
            if (!s) {
                ArrayList arrayList = new ArrayList(t.size());
                for (String upperCase : t) {
                    arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
                }
                t = Collections.unmodifiableList(arrayList);
            }
            list = t;
        }
        return a(str, o, s, str3, list, o == aj2.b.REGEXP ? str3 : null, t43);
    }

    @DexIgnore
    public static Boolean a(String str, aj2.b bVar, boolean z, String str2, List<String> list, String str3, t43 t43) {
        if (str == null) {
            return null;
        }
        if (bVar == aj2.b.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && bVar != aj2.b.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (va3.a[bVar.ordinal()]) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    if (t43 != null) {
                        t43.w().a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    }
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    @DexIgnore
    public static Boolean a(long j, yi2 yi2) {
        try {
            return a(new BigDecimal(j), yi2, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean a(double d2, yi2 yi2) {
        try {
            return a(new BigDecimal(d2), yi2, Math.ulp(d2));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean a(String str, yi2 yi2) {
        if (!ia3.a(str)) {
            return null;
        }
        try {
            return a(new BigDecimal(str), yi2, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0085, code lost:
        if (r2 != null) goto L_0x0087;
     */
    @DexIgnore
    public static Boolean a(BigDecimal bigDecimal, yi2 yi2, double d2) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        w12.a(yi2);
        if (yi2.n() && yi2.o() != yi2.a.UNKNOWN_COMPARISON_TYPE) {
            if (yi2.o() == yi2.a.BETWEEN) {
                if (!yi2.t() || !yi2.w()) {
                    return null;
                }
            } else if (!yi2.r()) {
                return null;
            }
            yi2.a o = yi2.o();
            if (yi2.o() == yi2.a.BETWEEN) {
                if (ia3.a(yi2.v()) && ia3.a(yi2.x())) {
                    try {
                        BigDecimal bigDecimal5 = new BigDecimal(yi2.v());
                        bigDecimal3 = new BigDecimal(yi2.x());
                        bigDecimal2 = bigDecimal5;
                        bigDecimal4 = null;
                    } catch (NumberFormatException unused) {
                    }
                }
                return null;
            } else if (!ia3.a(yi2.s())) {
                return null;
            } else {
                try {
                    bigDecimal4 = new BigDecimal(yi2.s());
                    bigDecimal2 = null;
                    bigDecimal3 = null;
                } catch (NumberFormatException unused2) {
                }
            }
            if (o == yi2.a.BETWEEN) {
                if (bigDecimal2 == null) {
                    return null;
                }
            }
            int i = va3.b[o.ordinal()];
            boolean z = false;
            if (i == 1) {
                if (bigDecimal.compareTo(bigDecimal4) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            } else if (i == 2) {
                if (bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            } else if (i != 3) {
                if (i == 4) {
                    if (!(bigDecimal.compareTo(bigDecimal2) == -1 || bigDecimal.compareTo(bigDecimal3) == 1)) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                }
            } else if (d2 != 0.0d) {
                if (bigDecimal.compareTo(bigDecimal4.subtract(new BigDecimal(d2).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal4.add(new BigDecimal(d2).multiply(new BigDecimal(2)))) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            } else {
                if (bigDecimal.compareTo(bigDecimal4) == 0) {
                    z = true;
                }
                return Boolean.valueOf(z);
            }
        }
        return null;
    }
}
