package com.fossil;

import com.fossil.dn3;
import com.fossil.im3;
import com.fossil.vl3;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hm3<E> extends vl3<E> implements dn3<E> {
    @DexIgnore
    public transient zl3<E> a;
    @DexIgnore
    public transient im3<dn3.a<E>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jo3<E> {
        @DexIgnore
        public int a;
        @DexIgnore
        public E b;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator c;

        @DexIgnore
        public a(hm3 hm3, Iterator it) {
            this.c = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a > 0 || this.c.hasNext();
        }

        @DexIgnore
        public E next() {
            if (this.a <= 0) {
                dn3.a aVar = (dn3.a) this.c.next();
                this.b = aVar.getElement();
                this.a = aVar.getCount();
            }
            this.a--;
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> extends vl3.b<E> {
        @DexIgnore
        public /* final */ dn3<E> a;

        @DexIgnore
        public b() {
            this(sm3.create());
        }

        @DexIgnore
        public b(dn3<E> dn3) {
            this.a = dn3;
        }

        @DexIgnore
        public b<E> a(E e) {
            dn3<E> dn3 = this.a;
            jk3.a(e);
            dn3.add(e);
            return this;
        }

        @DexIgnore
        public b<E> a(E... eArr) {
            super.a(eArr);
            return this;
        }

        @DexIgnore
        public hm3<E> a() {
            return hm3.copyOf(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends im3.b<dn3.a<E>> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof dn3.a)) {
                return false;
            }
            dn3.a aVar = (dn3.a) obj;
            if (aVar.getCount() > 0 && hm3.this.count(aVar.getElement()) == aVar.getCount()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return hm3.this.hashCode();
        }

        @DexIgnore
        public boolean isPartialView() {
            return hm3.this.isPartialView();
        }

        @DexIgnore
        public int size() {
            return hm3.this.elementSet().size();
        }

        @DexIgnore
        public Object writeReplace() {
            return new d(hm3.this);
        }

        @DexIgnore
        public /* synthetic */ c(hm3 hm3, a aVar) {
            this();
        }

        @DexIgnore
        public dn3.a<E> get(int i) {
            return hm3.this.getEntry(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<E> implements Serializable {
        @DexIgnore
        public /* final */ hm3<E> multiset;

        @DexIgnore
        public d(hm3<E> hm3) {
            this.multiset = hm3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.multiset.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int[] counts;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public e(dn3<?> dn3) {
            int size = dn3.entrySet().size();
            this.elements = new Object[size];
            this.counts = new int[size];
            int i = 0;
            for (dn3.a next : dn3.entrySet()) {
                this.elements[i] = next.getElement();
                this.counts[i] = next.getCount();
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            sm3 create = sm3.create(this.elements.length);
            int i = 0;
            while (true) {
                Object[] objArr = this.elements;
                if (i >= objArr.length) {
                    return hm3.copyOf(create);
                }
                create.add(objArr[i], this.counts[i]);
                i++;
            }
        }
    }

    @DexIgnore
    public static <E> hm3<E> a(E... eArr) {
        sm3 create = sm3.create();
        Collections.addAll(create, eArr);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> b<E> builder() {
        return new b<>();
    }

    @DexIgnore
    public static <E> hm3<E> copyFromEntries(Collection<? extends dn3.a<? extends E>> collection) {
        if (collection.isEmpty()) {
            return of();
        }
        return new rn3(collection);
    }

    @DexIgnore
    public static <E> hm3<E> copyOf(E[] eArr) {
        return a(eArr);
    }

    @DexIgnore
    public static <E> hm3<E> of() {
        return rn3.EMPTY;
    }

    @DexIgnore
    @Deprecated
    public final int add(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public zl3<E> asList() {
        zl3<E> zl3 = this.a;
        if (zl3 != null) {
            return zl3;
        }
        zl3<E> createAsList = createAsList();
        this.a = createAsList;
        return createAsList;
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return count(obj) > 0;
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        Iterator it = entrySet().iterator();
        while (it.hasNext()) {
            dn3.a aVar = (dn3.a) it.next();
            Arrays.fill(objArr, i, aVar.getCount() + i, aVar.getElement());
            i += aVar.getCount();
        }
        return i;
    }

    @DexIgnore
    public zl3<E> createAsList() {
        if (isEmpty()) {
            return zl3.of();
        }
        return new nn3(this, toArray());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return en3.a((dn3<?>) this, obj);
    }

    @DexIgnore
    public abstract dn3.a<E> getEntry(int i);

    @DexIgnore
    public int hashCode() {
        return yn3.a((Set<?>) entrySet());
    }

    @DexIgnore
    @Deprecated
    public final int remove(Object obj, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final int setCount(E e2, int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return entrySet().toString();
    }

    @DexIgnore
    public Object writeReplace() {
        return new e(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Iterable<? extends E>, java.lang.Iterable] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public static <E> hm3<E> copyOf(Iterable<? extends E> r2) {
        if (r2 instanceof hm3) {
            hm3<E> hm3 = r2;
            if (!hm3.isPartialView()) {
                return hm3;
            }
        }
        return copyFromEntries((r2 instanceof dn3 ? en3.a(r2) : sm3.create(r2)).entrySet());
    }

    @DexIgnore
    public static <E> hm3<E> of(E e2) {
        return a(e2);
    }

    @DexIgnore
    public im3<dn3.a<E>> entrySet() {
        im3<dn3.a<E>> im3 = this.b;
        if (im3 != null) {
            return im3;
        }
        im3<dn3.a<E>> a2 = a();
        this.b = a2;
        return a2;
    }

    @DexIgnore
    public jo3<E> iterator() {
        return new a(this, entrySet().iterator());
    }

    @DexIgnore
    @Deprecated
    public final boolean setCount(E e2, int i, int i2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <E> hm3<E> of(E e2, E e3) {
        return a(e2, e3);
    }

    @DexIgnore
    public static <E> hm3<E> of(E e2, E e3, E e4) {
        return a(e2, e3, e4);
    }

    @DexIgnore
    public final im3<dn3.a<E>> a() {
        return isEmpty() ? im3.of() : new c(this, (a) null);
    }

    @DexIgnore
    public static <E> hm3<E> of(E e2, E e3, E e4, E e5) {
        return a(e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E> hm3<E> of(E e2, E e3, E e4, E e5, E e6) {
        return a(e2, e3, e4, e5, e6);
    }

    @DexIgnore
    public static <E> hm3<E> copyOf(Iterator<? extends E> it) {
        sm3 create = sm3.create();
        qm3.a(create, it);
        return copyFromEntries(create.entrySet());
    }

    @DexIgnore
    public static <E> hm3<E> of(E e2, E e3, E e4, E e5, E e6, E e7, E... eArr) {
        b bVar = new b();
        bVar.a(e2);
        bVar.a(e3);
        bVar.a(e4);
        bVar.a(e5);
        bVar.a(e6);
        bVar.a(e7);
        bVar.a(eArr);
        return bVar.a();
    }
}
