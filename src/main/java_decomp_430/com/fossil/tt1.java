package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tt1 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<tt1> CREATOR; // = new wt1();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Bundle c;

    @DexIgnore
    public tt1(int i, int i2, Bundle bundle) {
        this.a = i;
        this.b = i2;
        this.c = bundle;
    }

    @DexIgnore
    public int p() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, p());
        g22.a(parcel, 3, this.c, false);
        g22.a(parcel, a2);
    }
}
