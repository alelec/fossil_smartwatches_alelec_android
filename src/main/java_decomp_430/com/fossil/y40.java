package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y40 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ nf0 a;
    @DexIgnore
    public /* final */ a50[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y40> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final y40[] a(byte[] bArr) throws IllegalArgumentException {
            Object obj;
            b50 b50;
            k50 k50;
            c50 c50;
            byte[] bArr2 = bArr;
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < (bArr2.length - 1) - 2) {
                nf0 a = nf0.e.a(bArr2[i]);
                int i2 = i + 1;
                int i3 = i2 + 2;
                ByteBuffer order = ByteBuffer.wrap(md6.a(bArr2, i2, i3)).order(ByteOrder.LITTLE_ENDIAN);
                wg6.a(order, "ByteBuffer.wrap(rawData\n\u2026(ByteOrder.LITTLE_ENDIAN)");
                int b = cw0.b(order.getShort());
                a50[] a2 = a50.CREATOR.a(md6.a(bArr2, i3, i3 + b));
                int length = a2.length;
                int i4 = 0;
                while (true) {
                    obj = null;
                    if (i4 >= length) {
                        b50 = null;
                        break;
                    }
                    b50 = a2[i4];
                    if (b50 instanceof b50) {
                        break;
                    }
                    i4++;
                }
                b50 b502 = b50;
                if (b502 != null) {
                    int length2 = a2.length;
                    int i5 = 0;
                    while (true) {
                        if (i5 >= length2) {
                            k50 = null;
                            break;
                        }
                        k50 = a2[i5];
                        if (k50 instanceof k50) {
                            break;
                        }
                        i5++;
                    }
                    k50 k502 = k50;
                    int length3 = a2.length;
                    int i6 = 0;
                    while (true) {
                        if (i6 >= length3) {
                            c50 = null;
                            break;
                        }
                        c50 = a2[i6];
                        if (c50 instanceof c50) {
                            break;
                        }
                        i6++;
                    }
                    c50 c502 = c50;
                    if (a != null) {
                        int i7 = am1.a[a.ordinal()];
                        if (i7 != 1) {
                            if (i7 != 2) {
                                throw new kc6();
                            } else if (b502 instanceof e50) {
                                obj = new f50((e50) b502, k502, c502);
                            } else if (b502 instanceof i50) {
                                obj = new j50((i50) b502, k502, c502);
                            }
                        } else if (b502 instanceof e50) {
                            obj = new d50((e50) b502, k502, c502);
                        } else if (b502 instanceof i50) {
                            obj = new h50((i50) b502, k502, c502);
                        }
                    }
                    if (obj != null) {
                        arrayList.add(obj);
                    }
                    i += b + 3;
                } else {
                    throw new IllegalArgumentException("Fire Time is required.");
                }
            }
            Object[] array = arrayList.toArray(new y40[0]);
            if (array != null) {
                return (y40[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new y40[i];
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.lang.Object[]} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: com.fossil.k50} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: com.fossil.k50} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: com.fossil.c50} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: com.fossil.c50} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: com.fossil.c50} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: com.fossil.k50} */
        /* JADX WARNING: Multi-variable type inference failed */
        public y40 createFromParcel(Parcel parcel) {
            k50 k50;
            c50 c50;
            nf0 nf0 = nf0.values()[parcel.readInt()];
            Object[] createTypedArray = parcel.createTypedArray(a50.CREATOR);
            c50 c502 = null;
            if (createTypedArray != null) {
                wg6.a(createTypedArray, "parcel.createTypedArray(AlarmSubEntry.CREATOR)!!");
                a50[] a50Arr = (a50[]) createTypedArray;
                int length = a50Arr.length;
                int i = 0;
                int i2 = 0;
                while (i2 < length) {
                    a50 a50 = a50Arr[i2];
                    if (!(a50 instanceof b50)) {
                        i2++;
                    } else if (a50 != null) {
                        b50 b50 = (b50) a50;
                        int length2 = a50Arr.length;
                        int i3 = 0;
                        while (true) {
                            if (i3 >= length2) {
                                k50 = null;
                                break;
                            }
                            a50 a502 = a50Arr[i3];
                            if (a502 instanceof k50) {
                                k50 = a502;
                                break;
                            }
                            i3++;
                        }
                        k50 k502 = k50 != null ? k50 : null;
                        int length3 = a50Arr.length;
                        while (true) {
                            if (i >= length3) {
                                c50 = null;
                                break;
                            }
                            a50 a503 = a50Arr[i];
                            if (a503 instanceof c50) {
                                c50 = a503;
                                break;
                            }
                            i++;
                        }
                        if (c50 != null) {
                            c502 = c50;
                        }
                        int i4 = am1.b[nf0.ordinal()];
                        if (i4 != 1) {
                            if (i4 != 2) {
                                throw new kc6();
                            } else if (b50 instanceof e50) {
                                return new f50((e50) b50, k502, c502);
                            } else {
                                return new j50((i50) b50, k502, c502);
                            }
                        } else if (b50 instanceof e50) {
                            return new d50((e50) b50, k502, c502);
                        } else {
                            return new h50((i50) b50, k502, c502);
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
                    }
                }
                throw new NoSuchElementException("Array contains no element matching the predicate.");
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public y40(nf0 nf0, a50[] a50Arr) {
        this.a = nf0;
        this.b = a50Arr;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = cw0.a(new JSONObject(), bm0.TYPE, (Object) cw0.a((Enum<?>) this.a));
        for (a50 a3 : this.b) {
            cw0.a(a2, a3.a(), false, 2);
        }
        return a2;
    }

    @DexIgnore
    public final byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (a50 b2 : this.b) {
            byteArrayOutputStream.write(b2.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "byteArrayOutputStreamWriter.toByteArray()");
        byte[] array = ByteBuffer.allocate(byteArray.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.a.a).putShort((short) byteArray.length).put(byteArray).array();
        wg6.a(array, "ByteBuffer.allocate(ENTR\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final a50[] c() {
        return this.b;
    }

    @DexIgnore
    public final nf0 d() {
        return this.a;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            z40 z40 = (z40) obj;
            return this.a == z40.d() && ld6.a(this.b, z40.c());
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.Alarm");
    }

    @DexIgnore
    public b50 getFireTime() {
        a50[] a50Arr = this.b;
        int length = a50Arr.length;
        int i = 0;
        while (i < length) {
            a50 a50 = a50Arr[i];
            if (!(a50 instanceof k50)) {
                i++;
            } else if (a50 != null) {
                return (b50) a50;
            } else {
                throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public final c50 getMessage() {
        c50 c50;
        a50[] a50Arr = this.b;
        int length = a50Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                c50 = null;
                break;
            }
            c50 = a50Arr[i];
            if (c50 instanceof c50) {
                break;
            }
            i++;
        }
        return c50;
    }

    @DexIgnore
    public final k50 getTitle() {
        k50 k50;
        a50[] a50Arr = this.b;
        int length = a50Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                k50 = null;
                break;
            }
            k50 = a50Arr[i];
            if (k50 instanceof k50) {
                break;
            }
            i++;
        }
        return k50;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + kd6.a(this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }
}
