package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yo4 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = yo4.class.getSimpleName();
        wg6.a((Object) simpleName, "TimeZoneReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        String action = intent.getAction();
        if (wg6.a((Object) "android.intent.action.TIME_SET", (Object) action) || wg6.a((Object) "android.intent.action.TIMEZONE_CHANGED", (Object) action)) {
            FLogger.INSTANCE.getLocal().d(a, "Inside .timeZoneChangeReceiver");
            bk4.b();
        } else if (wg6.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
            TimeZone timeZone = TimeZone.getDefault();
            try {
                Calendar instance = Calendar.getInstance();
                wg6.a((Object) instance, "calendar");
                Calendar instance2 = Calendar.getInstance();
                wg6.a((Object) instance2, "Calendar.getInstance()");
                instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                Calendar instance3 = Calendar.getInstance();
                wg6.a((Object) instance3, "Calendar.getInstance()");
                if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                    FLogger.INSTANCE.getLocal().d(a, "Inside .timeZoneChangeReceiver - DST change");
                    bk4.b();
                    PortfolioApp.get.instance().P();
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.e(str, ".timeZoneChangeReceiver - ex=" + e);
            }
        }
    }
}
