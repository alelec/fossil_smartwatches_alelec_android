package com.fossil;

import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$1$pair$1", f = "CaloriesOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
public final class hd5$b$c extends sf6 implements ig6<il6, xe6<? super lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CaloriesOverviewDayPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hd5$b$c(CaloriesOverviewDayPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        hd5$b$c hd5_b_c = new hd5$b$c(this.this$0, xe6);
        hd5_b_c.p$ = (il6) obj;
        return hd5_b_c;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((hd5$b$c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            gj5 gj5 = gj5.a;
            Date d = CaloriesOverviewDayPresenter.d(this.this$0.this$0);
            yx5 yx5 = (yx5) this.this$0.this$0.j.a();
            return gj5.a(d, (List<ActivitySample>) yx5 != null ? (List) yx5.d() : null, 2);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
