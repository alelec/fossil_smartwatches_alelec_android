package com.fossil;

import com.fossil.wp;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qq<T> extends up<T> {
    @DexIgnore
    public static /* final */ String PROTOCOL_CHARSET; // = "utf-8";
    @DexIgnore
    public static /* final */ String PROTOCOL_CONTENT_TYPE; // = String.format("application/json; charset=%s", new Object[]{PROTOCOL_CHARSET});
    @DexIgnore
    public wp.b<T> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ String mRequestBody;

    @DexIgnore
    @Deprecated
    public qq(String str, String str2, wp.b<T> bVar, wp.a aVar) {
        this(-1, str, str2, bVar, aVar);
    }

    @DexIgnore
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    public void deliverResponse(T t) {
        wp.b<T> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(t);
        }
    }

    @DexIgnore
    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException unused) {
            cq.e("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @DexIgnore
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @DexIgnore
    @Deprecated
    public byte[] getPostBody() {
        return getBody();
    }

    @DexIgnore
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    public abstract wp<T> parseNetworkResponse(rp rpVar);

    @DexIgnore
    public qq(int i, String str, String str2, wp.b<T> bVar, wp.a aVar) {
        super(i, str, aVar);
        this.mLock = new Object();
        this.mListener = bVar;
        this.mRequestBody = str2;
    }
}
