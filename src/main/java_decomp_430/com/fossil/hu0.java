package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.Context;
import android.content.IntentFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu0 extends h11 {
    @DexIgnore
    public static /* final */ dp0 g; // = new dp0((qg6) null);
    @DexIgnore
    public /* final */ os0 f; // = new os0(this);

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public hu0(jn0 jn0) {
        super("CurrentTimeService", r0, 0, jn0);
        UUID d = mi0.A.d();
        wg6.a(d, "Constant.CURRENT_TIME_SERVICE_UUID");
        BluetoothGattCharacteristic bluetoothGattCharacteristic = new BluetoothGattCharacteristic(mi0.A.b(), 18, 1);
        bluetoothGattCharacteristic.addDescriptor(new BluetoothGattDescriptor(mi0.A.a(), 17));
        BluetoothGattCharacteristic bluetoothGattCharacteristic2 = new BluetoothGattCharacteristic(mi0.A.c(), 2, 1);
        addCharacteristic(bluetoothGattCharacteristic);
        addCharacteristic(bluetoothGattCharacteristic2);
    }

    @DexIgnore
    public void b() {
        super.b();
        Context a = a().a();
        os0 os0 = this.f;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
        a.registerReceiver(os0, intentFilter);
    }

    @DexIgnore
    public void c() {
        super.c();
        try {
            a().a().unregisterReceiver(this.f);
        } catch (Exception e) {
            qs0.h.a(e);
        }
    }

    @DexIgnore
    public final void a(long j, sl0 sl0) {
        boolean z;
        int i;
        BluetoothGattCharacteristic characteristic = getCharacteristic(mi0.A.b());
        if (characteristic != null) {
            characteristic.setValue(g.a(j, sl0));
            HashMap<BluetoothDevice, vg0> hashMap = this.a;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            Iterator<Map.Entry<BluetoothDevice, vg0>> it = hashMap.entrySet().iterator();
            while (true) {
                z = true;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                Iterator<Map.Entry<BluetoothGattDescriptor, byte[]>> it2 = ((vg0) next.getValue()).a.entrySet().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z = false;
                        break;
                    }
                    Map.Entry next2 = it2.next();
                    Object key = next2.getKey();
                    wg6.a(key, "descriptorSubscription.key");
                    BluetoothGattCharacteristic characteristic2 = ((BluetoothGattDescriptor) key).getCharacteristic();
                    wg6.a(characteristic2, "descriptor.characteristic");
                    if (wg6.a(characteristic2.getUuid(), characteristic.getUuid())) {
                        byte[] c = pk0.d.c();
                        Object value = next2.getValue();
                        wg6.a(value, "descriptorSubscription.value");
                        if (Arrays.equals(c, (byte[]) value)) {
                            break;
                        }
                        byte[] b = pk0.d.b();
                        Object value2 = next2.getValue();
                        wg6.a(value2, "descriptorSubscription.value");
                        if (Arrays.equals(b, (byte[]) value2)) {
                            break;
                        }
                    }
                }
                if (z) {
                    linkedHashMap.put(next.getKey(), next.getValue());
                }
            }
            Object[] array = linkedHashMap.keySet().toArray(new BluetoothDevice[0]);
            if (array != null) {
                BluetoothDevice[] bluetoothDeviceArr = (BluetoothDevice[]) array;
                if (!(bluetoothDeviceArr.length == 0)) {
                    if ((characteristic.getProperties() & 32) == 0) {
                        z = false;
                    }
                    for (BluetoothDevice yf1 : bluetoothDeviceArr) {
                        jn0 jn0 = this.e;
                        jn0.a(new yf1(yf1, characteristic, z, jn0.b));
                    }
                    return;
                }
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
