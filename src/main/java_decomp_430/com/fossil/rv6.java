package com.fossil;

import com.sina.weibo.sdk.web.client.ShareWebViewClient;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rv6 implements Comparable<rv6> {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public d c;

    @DexIgnore
    public interface c {
        @DexIgnore
        int compareTo(c cVar);

        @DexIgnore
        int getType();

        @DexIgnore
        boolean isNull();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends ArrayList<c> implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public int compareTo(c cVar) {
            int i;
            if (cVar != null) {
                int type = cVar.getType();
                if (type == 0) {
                    return -1;
                }
                if (type == 1) {
                    return 1;
                }
                if (type == 2) {
                    Iterator it = iterator();
                    Iterator it2 = ((d) cVar).iterator();
                    do {
                        if (!it.hasNext() && !it2.hasNext()) {
                            return 0;
                        }
                        c cVar2 = it.hasNext() ? (c) it.next() : null;
                        c cVar3 = it2.hasNext() ? (c) it2.next() : null;
                        if (cVar2 != null) {
                            i = cVar2.compareTo(cVar3);
                            continue;
                        } else if (cVar3 == null) {
                            i = 0;
                            continue;
                        } else {
                            i = cVar3.compareTo(cVar2) * -1;
                            continue;
                        }
                    } while (i == 0);
                    return i;
                }
                throw new RuntimeException("invalid item: " + cVar.getClass());
            } else if (size() == 0) {
                return 0;
            } else {
                return ((c) get(0)).compareTo((c) null);
            }
        }

        @DexIgnore
        public int getType() {
            return 2;
        }

        @DexIgnore
        public boolean isNull() {
            return size() == 0;
        }

        @DexIgnore
        public void normalize() {
            for (int size = size() - 1; size >= 0; size--) {
                c cVar = (c) get(size);
                if (cVar.isNull()) {
                    remove(size);
                } else if (!(cVar instanceof d)) {
                    return;
                }
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator it = iterator();
            while (it.hasNext()) {
                c cVar = (c) it.next();
                if (sb.length() > 0) {
                    sb.append(cVar instanceof d ? '-' : '.');
                }
                sb.append(cVar);
            }
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements c {
        @DexIgnore
        public static /* final */ String[] b; // = {"alpha", "beta", "milestone", "rc", "snapshot", "", "sp"};
        @DexIgnore
        public static /* final */ List<String> c; // = Arrays.asList(b);
        @DexIgnore
        public static /* final */ Properties d; // = new Properties();
        @DexIgnore
        public static /* final */ String e; // = String.valueOf(c.indexOf(""));
        @DexIgnore
        public String a;

        /*
        static {
            d.put("ga", "");
            d.put("final", "");
            d.put("cr", "rc");
        }
        */

        @DexIgnore
        public e(String str, boolean z) {
            if (z && str.length() == 1) {
                char charAt = str.charAt(0);
                if (charAt == 'a') {
                    str = "alpha";
                } else if (charAt == 'b') {
                    str = "beta";
                } else if (charAt == 'm') {
                    str = "milestone";
                }
            }
            this.a = d.getProperty(str, str);
        }

        @DexIgnore
        public static String a(String str) {
            int indexOf = c.indexOf(str);
            if (indexOf != -1) {
                return String.valueOf(indexOf);
            }
            return c.size() + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + str;
        }

        @DexIgnore
        public int compareTo(c cVar) {
            if (cVar == null) {
                return a(this.a).compareTo(e);
            }
            int type = cVar.getType();
            if (type == 0) {
                return -1;
            }
            if (type == 1) {
                return a(this.a).compareTo(a(((e) cVar).a));
            }
            if (type == 2) {
                return -1;
            }
            throw new RuntimeException("invalid item: " + cVar.getClass());
        }

        @DexIgnore
        public int getType() {
            return 1;
        }

        @DexIgnore
        public boolean isNull() {
            return a(this.a).compareTo(e) == 0;
        }

        @DexIgnore
        public String toString() {
            return this.a;
        }
    }

    @DexIgnore
    public rv6(String str) {
        a(str);
    }

    @DexIgnore
    public final void a(String str) {
        int i;
        d dVar;
        int i2;
        d dVar2;
        this.a = str;
        this.c = new d();
        String lowerCase = str.toLowerCase(Locale.ENGLISH);
        d dVar3 = this.c;
        Stack stack = new Stack();
        stack.push(dVar3);
        d dVar4 = dVar3;
        int i3 = 0;
        boolean z = false;
        for (int i4 = 0; i4 < lowerCase.length(); i4++) {
            char charAt = lowerCase.charAt(i4);
            if (charAt == '.') {
                if (i4 == i3) {
                    dVar4.add(b.c);
                } else {
                    dVar4.add(a(z, lowerCase.substring(i3, i4)));
                }
                i3 = i4 + 1;
            } else if (charAt == '-') {
                if (i4 == i3) {
                    dVar4.add(b.c);
                } else {
                    dVar4.add(a(z, lowerCase.substring(i3, i4)));
                }
                i3 = i4 + 1;
                d dVar5 = new d();
                dVar4.add(dVar5);
                stack.push(dVar5);
                dVar4 = dVar5;
            } else {
                if (Character.isDigit(charAt)) {
                    if (z || i4 <= i3) {
                        d dVar6 = dVar4;
                        i = i3;
                        dVar = dVar6;
                    } else {
                        dVar4.add(new e(lowerCase.substring(i3, i4), true));
                        dVar = new d();
                        dVar4.add(dVar);
                        stack.push(dVar);
                        i = i4;
                    }
                    z = true;
                } else {
                    if (!z || i4 <= i3) {
                        d dVar7 = dVar4;
                        i2 = i3;
                        dVar2 = dVar7;
                    } else {
                        dVar4.add(a(true, lowerCase.substring(i3, i4)));
                        dVar2 = new d();
                        dVar4.add(dVar2);
                        stack.push(dVar2);
                        i2 = i4;
                    }
                    z = false;
                }
                int i5 = i;
                dVar4 = dVar;
                i3 = i5;
            }
        }
        if (lowerCase.length() > i3) {
            dVar4.add(a(z, lowerCase.substring(i3)));
        }
        while (!stack.isEmpty()) {
            ((d) stack.pop()).normalize();
        }
        this.b = this.c.toString();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof rv6) && this.b.equals(((rv6) obj).b);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {
        @DexIgnore
        public static /* final */ BigInteger b; // = new BigInteger(ShareWebViewClient.RESP_SUCC_CODE);
        @DexIgnore
        public static /* final */ b c; // = new b();
        @DexIgnore
        public /* final */ BigInteger a;

        @DexIgnore
        public b() {
            this.a = b;
        }

        @DexIgnore
        public int compareTo(c cVar) {
            if (cVar == null) {
                return b.equals(this.a) ^ true ? 1 : 0;
            }
            int type = cVar.getType();
            if (type == 0) {
                return this.a.compareTo(((b) cVar).a);
            }
            if (type == 1 || type == 2) {
                return 1;
            }
            throw new RuntimeException("invalid item: " + cVar.getClass());
        }

        @DexIgnore
        public int getType() {
            return 0;
        }

        @DexIgnore
        public boolean isNull() {
            return b.equals(this.a);
        }

        @DexIgnore
        public String toString() {
            return this.a.toString();
        }

        @DexIgnore
        public b(String str) {
            this.a = new BigInteger(str);
        }
    }

    @DexIgnore
    public static c a(boolean z, String str) {
        return z ? new b(str) : new e(str, false);
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(rv6 rv6) {
        return this.c.compareTo(rv6.c);
    }
}
