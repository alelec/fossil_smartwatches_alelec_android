package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface hu {
    @DexIgnore
    Bitmap a();

    @DexIgnore
    Bitmap a(int i, int i2, Bitmap.Config config);

    @DexIgnore
    void a(Bitmap bitmap);

    @DexIgnore
    int b(Bitmap bitmap);

    @DexIgnore
    String b(int i, int i2, Bitmap.Config config);

    @DexIgnore
    String c(Bitmap bitmap);
}
