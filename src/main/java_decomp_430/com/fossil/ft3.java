package com.fossil;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import com.fossil.p6;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft3 {
    @DexIgnore
    public static /* final */ AtomicInteger a; // = new AtomicInteger((int) SystemClock.elapsedRealtime());

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01bc  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01cd  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0216  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0256  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x026d  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0288  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0292  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x02a6  */
    public static et3 a(Context context, ot3 ot3) {
        Uri uri;
        Intent intent;
        PendingIntent pendingIntent;
        PendingIntent pendingIntent2;
        Integer c;
        Integer c2;
        Long d;
        long[] c3;
        int[] d2;
        String a2;
        Bundle a3 = a(context.getPackageManager(), context.getPackageName());
        String packageName = context.getPackageName();
        String b = b(context, ot3.a("gcm.n.android_channel_id"), a3);
        Resources resources = context.getResources();
        PackageManager packageManager = context.getPackageManager();
        p6.d dVar = new p6.d(context, b);
        dVar.b(a(packageName, ot3, packageManager, resources));
        String a4 = ot3.a(resources, packageName, "gcm.n.body");
        if (!TextUtils.isEmpty(a4)) {
            dVar.a((CharSequence) a4);
            p6.c cVar = new p6.c();
            cVar.a((CharSequence) a4);
            dVar.a((p6.e) cVar);
        }
        dVar.e(a(packageManager, resources, packageName, ot3.a("gcm.n.icon"), a3));
        String b2 = ot3.b();
        Integer num = null;
        if (TextUtils.isEmpty(b2)) {
            uri = null;
        } else if ("default".equals(b2) || resources.getIdentifier(b2, OrmLiteConfigUtil.RAW_DIR_NAME, packageName) == 0) {
            uri = RingtoneManager.getDefaultUri(2);
        } else {
            StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 24 + String.valueOf(b2).length());
            sb.append("android.resource://");
            sb.append(packageName);
            sb.append("/raw/");
            sb.append(b2);
            uri = Uri.parse(sb.toString());
        }
        if (uri != null) {
            dVar.a(uri);
        }
        String a5 = ot3.a("gcm.n.click_action");
        if (!TextUtils.isEmpty(a5)) {
            intent = new Intent(a5);
            intent.setPackage(packageName);
            intent.setFlags(268435456);
        } else {
            Uri a6 = ot3.a();
            if (a6 != null) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setPackage(packageName);
                intent.setData(a6);
            } else {
                Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(packageName);
                if (launchIntentForPackage == null) {
                    Log.w("FirebaseMessaging", "No activity found to launch app");
                }
                intent = launchIntentForPackage;
            }
        }
        if (intent == null) {
            pendingIntent = null;
        } else {
            intent.addFlags(67108864);
            intent.putExtras(ot3.e());
            pendingIntent = PendingIntent.getActivity(context, a.incrementAndGet(), intent, 1073741824);
            if (ot3.b("google.c.a.e")) {
                pendingIntent = a(context, new Intent("com.google.firebase.messaging.NOTIFICATION_OPEN").putExtras(ot3.f()).putExtra("pending_intent", pendingIntent));
            }
        }
        dVar.a(pendingIntent);
        if (!ot3.b("google.c.a.e")) {
            pendingIntent2 = null;
        } else {
            pendingIntent2 = a(context, new Intent("com.google.firebase.messaging.NOTIFICATION_DISMISS").putExtras(ot3.f()));
        }
        if (pendingIntent2 != null) {
            dVar.b(pendingIntent2);
        }
        Integer a7 = a(context, ot3.a("gcm.n.color"), a3);
        if (a7 != null) {
            dVar.a(a7.intValue());
        }
        int i = 1;
        dVar.a(!ot3.b("gcm.n.sticky"));
        dVar.b(ot3.b("gcm.n.local_only"));
        String a8 = ot3.a("gcm.n.ticker");
        if (a8 != null) {
            dVar.c((CharSequence) a8);
        }
        Integer c4 = ot3.c("gcm.n.notification_priority");
        if (c4 != null) {
            if (c4.intValue() < -2 || c4.intValue() > 2) {
                String valueOf = String.valueOf(c4);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 72);
                sb2.append("notificationPriority is invalid ");
                sb2.append(valueOf);
                sb2.append(". Skipping setting notificationPriority.");
                Log.w("FirebaseMessaging", sb2.toString());
            }
            if (c4 != null) {
                dVar.d(c4.intValue());
            }
            c = ot3.c("gcm.n.visibility");
            if (c != null) {
                if (c.intValue() < -1 || c.intValue() > 1) {
                    String valueOf2 = String.valueOf(c);
                    StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 53);
                    sb3.append("visibility is invalid: ");
                    sb3.append(valueOf2);
                    sb3.append(". Skipping setting visibility.");
                    Log.w("NotificationParams", sb3.toString());
                }
                if (c != null) {
                    dVar.f(c.intValue());
                }
                c2 = ot3.c("gcm.n.notification_count");
                if (c2 != null) {
                    if (c2.intValue() < 0) {
                        String valueOf3 = String.valueOf(c2);
                        StringBuilder sb4 = new StringBuilder(String.valueOf(valueOf3).length() + 67);
                        sb4.append("notificationCount is invalid: ");
                        sb4.append(valueOf3);
                        sb4.append(". Skipping setting notificationCount.");
                        Log.w("FirebaseMessaging", sb4.toString());
                    } else {
                        num = c2;
                    }
                }
                if (num != null) {
                    dVar.c(num.intValue());
                }
                d = ot3.d("gcm.n.event_time");
                if (d != null) {
                    dVar.a(d.longValue());
                }
                c3 = ot3.c();
                if (c3 != null) {
                    dVar.a(c3);
                }
                d2 = ot3.d();
                if (d2 != null) {
                    dVar.a(d2[0], d2[1], d2[2]);
                }
                if (!ot3.b("gcm.n.default_sound")) {
                    i = 0;
                }
                if (ot3.b("gcm.n.default_vibrate_timings")) {
                    i |= 2;
                }
                if (ot3.b("gcm.n.default_light_settings")) {
                    i |= 4;
                }
                dVar.b(i);
                a2 = ot3.a("gcm.n.tag");
                if (TextUtils.isEmpty(a2)) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    StringBuilder sb5 = new StringBuilder(37);
                    sb5.append("FCM-Notification:");
                    sb5.append(uptimeMillis);
                    a2 = sb5.toString();
                }
                return new et3(dVar, a2, 0);
            }
            c = null;
            if (c != null) {
            }
            c2 = ot3.c("gcm.n.notification_count");
            if (c2 != null) {
            }
            if (num != null) {
            }
            d = ot3.d("gcm.n.event_time");
            if (d != null) {
            }
            c3 = ot3.c();
            if (c3 != null) {
            }
            d2 = ot3.d();
            if (d2 != null) {
            }
            if (!ot3.b("gcm.n.default_sound")) {
            }
            if (ot3.b("gcm.n.default_vibrate_timings")) {
            }
            if (ot3.b("gcm.n.default_light_settings")) {
            }
            dVar.b(i);
            a2 = ot3.a("gcm.n.tag");
            if (TextUtils.isEmpty(a2)) {
            }
            return new et3(dVar, a2, 0);
        }
        c4 = null;
        if (c4 != null) {
        }
        c = ot3.c("gcm.n.visibility");
        if (c != null) {
        }
        c = null;
        if (c != null) {
        }
        c2 = ot3.c("gcm.n.notification_count");
        if (c2 != null) {
        }
        if (num != null) {
        }
        d = ot3.d("gcm.n.event_time");
        if (d != null) {
        }
        c3 = ot3.c();
        if (c3 != null) {
        }
        d2 = ot3.d();
        if (d2 != null) {
        }
        if (!ot3.b("gcm.n.default_sound")) {
        }
        if (ot3.b("gcm.n.default_vibrate_timings")) {
        }
        if (ot3.b("gcm.n.default_light_settings")) {
        }
        dVar.b(i);
        a2 = ot3.a("gcm.n.tag");
        if (TextUtils.isEmpty(a2)) {
        }
        return new et3(dVar, a2, 0);
    }

    @DexIgnore
    @TargetApi(26)
    public static String b(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 26) {
            return null;
        }
        try {
            if (context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).targetSdkVersion < 26) {
                return null;
            }
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(NotificationManager.class);
            if (!TextUtils.isEmpty(str)) {
                if (notificationManager.getNotificationChannel(str) != null) {
                    return str;
                }
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 122);
                sb.append("Notification Channel requested (");
                sb.append(str);
                sb.append(") has not been created by the app. Manifest configuration, or default, value will be used.");
                Log.w("FirebaseMessaging", sb.toString());
            }
            String string = bundle.getString("com.google.firebase.messaging.default_notification_channel_id");
            if (TextUtils.isEmpty(string)) {
                Log.w("FirebaseMessaging", "Missing Default Notification Channel metadata in AndroidManifest. Default value will be used.");
            } else if (notificationManager.getNotificationChannel(string) != null) {
                return string;
            } else {
                Log.w("FirebaseMessaging", "Notification Channel set in AndroidManifest.xml has not been created by the app. Default value will be used.");
            }
            if (notificationManager.getNotificationChannel("fcm_fallback_notification_channel") == null) {
                notificationManager.createNotificationChannel(new NotificationChannel("fcm_fallback_notification_channel", context.getString(context.getResources().getIdentifier("fcm_fallback_notification_channel_label", LegacyTokenHelper.TYPE_STRING, context.getPackageName())), 3));
            }
            return "fcm_fallback_notification_channel";
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    @DexIgnore
    public static CharSequence a(String str, ot3 ot3, PackageManager packageManager, Resources resources) {
        String a2 = ot3.a(resources, str, "gcm.n.title");
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        try {
            return packageManager.getApplicationInfo(str, 0).loadLabel(packageManager);
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 35);
            sb.append("Couldn't get own application info: ");
            sb.append(valueOf);
            Log.e("FirebaseMessaging", sb.toString());
            return "";
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static boolean a(Resources resources, int i) {
        if (Build.VERSION.SDK_INT != 26) {
            return true;
        }
        try {
            if (!(resources.getDrawable(i, (Resources.Theme) null) instanceof AdaptiveIconDrawable)) {
                return true;
            }
            StringBuilder sb = new StringBuilder(77);
            sb.append("Adaptive icons cannot be used in notifications. Ignoring icon id: ");
            sb.append(i);
            Log.e("FirebaseMessaging", sb.toString());
            return false;
        } catch (Resources.NotFoundException unused) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Couldn't find resource ");
            sb2.append(i);
            sb2.append(", treating it as an invalid icon");
            Log.e("FirebaseMessaging", sb2.toString());
            return false;
        }
    }

    @DexIgnore
    public static int a(PackageManager packageManager, Resources resources, String str, String str2, Bundle bundle) {
        if (!TextUtils.isEmpty(str2)) {
            int identifier = resources.getIdentifier(str2, "drawable", str);
            if (identifier != 0 && a(resources, identifier)) {
                return identifier;
            }
            int identifier2 = resources.getIdentifier(str2, "mipmap", str);
            if (identifier2 != 0 && a(resources, identifier2)) {
                return identifier2;
            }
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 61);
            sb.append("Icon resource ");
            sb.append(str2);
            sb.append(" not found. Notification will use default icon.");
            Log.w("FirebaseMessaging", sb.toString());
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_icon", 0);
        if (i == 0 || !a(resources, i)) {
            try {
                i = packageManager.getApplicationInfo(str, 0).icon;
            } catch (PackageManager.NameNotFoundException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 35);
                sb2.append("Couldn't get own application info: ");
                sb2.append(valueOf);
                Log.w("FirebaseMessaging", sb2.toString());
            }
        }
        if (i == 0 || !a(resources, i)) {
            return 17301651;
        }
        return i;
    }

    @DexIgnore
    public static Integer a(Context context, String str, Bundle bundle) {
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        if (!TextUtils.isEmpty(str)) {
            try {
                return Integer.valueOf(Color.parseColor(str));
            } catch (IllegalArgumentException unused) {
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 56);
                sb.append("Color is invalid: ");
                sb.append(str);
                sb.append(". Notification will use default color.");
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        int i = bundle.getInt("com.google.firebase.messaging.default_notification_color", 0);
        if (i != 0) {
            try {
                return Integer.valueOf(w6.a(context, i));
            } catch (Resources.NotFoundException unused2) {
                Log.w("FirebaseMessaging", "Cannot find the color resource referenced in AndroidManifest.");
            }
        }
        return null;
    }

    @DexIgnore
    public static Bundle a(PackageManager packageManager, String str) {
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                return applicationInfo.metaData;
            }
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 35);
            sb.append("Couldn't get own application info: ");
            sb.append(valueOf);
            Log.w("FirebaseMessaging", sb.toString());
        }
        return Bundle.EMPTY;
    }

    @DexIgnore
    public static PendingIntent a(Context context, Intent intent) {
        return PendingIntent.getBroadcast(context, a.incrementAndGet(), new Intent("com.google.firebase.MESSAGING_EVENT").setComponent(new ComponentName(context, "com.google.firebase.iid.FirebaseInstanceIdReceiver")).putExtra("wrapped_intent", intent), 1073741824);
    }
}
