package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum q80 {
    DIAGNOSTICS("diagnosticsApp"),
    WELLNESS("wellnessApp"),
    WORKOUT("workoutApp"),
    MUSIC("musicApp"),
    NOTIFICATIONS_PANEL("notificationsPanelApp"),
    EMPTY("empty"),
    STOP_WATCH("stopwatchApp"),
    ASSISTANT("assistantApp"),
    TIMER("timerApp"),
    WEATHER("weatherApp"),
    COMMUTE("commuteApp"),
    BUDDY_CHALLENGE("buddyChallengeApp");
    
    @DexIgnore
    public static /* final */ a c; // = null;
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }
    }

    /*
    static {
        c = new a((qg6) null);
    }
    */

    @DexIgnore
    public q80(String str) {
        this.a = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj;
        if (tw0.a[ordinal()] != 1) {
            obj = this.a;
        } else {
            obj = JSONObject.NULL;
        }
        wg6.a(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
