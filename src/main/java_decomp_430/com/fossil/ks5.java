package com.fossil;

import com.misfit.frameworks.buttonservice.model.ShineDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ks5 extends j24 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(ShineDevice shineDevice);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(ShineDevice shineDevice);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public abstract void c(ShineDevice shineDevice);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract ShineDevice i();

    @DexIgnore
    public abstract boolean j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();

    @DexIgnore
    public abstract void n();
}
