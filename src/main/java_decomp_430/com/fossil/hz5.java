package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz5 {
    @DexIgnore
    public short a;
    @DexIgnore
    public short b;

    @DexIgnore
    public final short a() {
        return this.a;
    }

    @DexIgnore
    public final short b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof hz5)) {
            return false;
        }
        hz5 hz5 = (hz5) obj;
        return this.a == hz5.a && this.b == hz5.b;
    }

    @DexIgnore
    public int hashCode() {
        return (f.a(this.a) * 31) + f.a(this.b);
    }

    @DexIgnore
    public String toString() {
        return "HeartRateDWMModel(minValue=" + this.a + ", maxValue=" + this.b + ")";
    }
}
