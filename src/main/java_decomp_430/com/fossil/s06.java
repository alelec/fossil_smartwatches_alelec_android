package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.fossil.n16;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s06 extends n16 {
    @DexIgnore
    public static /* final */ int b; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;

    @DexIgnore
    public s06(Context context) {
        this.a = context.getAssets();
    }

    @DexIgnore
    public static String c(l16 l16) {
        return l16.d.toString().substring(b);
    }

    @DexIgnore
    public boolean a(l16 l16) {
        Uri uri = l16.d;
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public n16.a a(l16 l16, int i) throws IOException {
        return new n16.a(this.a.open(c(l16)), Picasso.LoadedFrom.DISK);
    }
}
