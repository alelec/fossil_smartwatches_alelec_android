package com.fossil;

import android.os.Parcelable;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$checkSettingOfSelectedComplication$1$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
public final class t45$b$a extends sf6 implements ig6<il6, xe6<? super Parcelable>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t45$b$a(ComplicationsPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        t45$b$a t45_b_a = new t45$b$a(this.this$0, xe6);
        t45_b_a.p$ = (il6) obj;
        return t45_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((t45$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return ComplicationsPresenter.g(this.this$0.this$0).f(this.this$0.$complicationId);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
