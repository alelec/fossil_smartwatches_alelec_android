package com.fossil;

import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q14 implements Factory<GuestApiService> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public q14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static q14 a(b14 b14) {
        return new q14(b14);
    }

    @DexIgnore
    public static GuestApiService b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static GuestApiService c(b14 b14) {
        GuestApiService i = b14.i();
        z76.a(i, "Cannot return null from a non-@Nullable @Provides method");
        return i;
    }

    @DexIgnore
    public GuestApiService get() {
        return b(this.a);
    }
}
