package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p14 implements Factory<kk4> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<Context> b;
    @DexIgnore
    public /* final */ Provider<u04> c;
    @DexIgnore
    public /* final */ Provider<an4> d;

    @DexIgnore
    public p14(b14 b14, Provider<Context> provider, Provider<u04> provider2, Provider<an4> provider3) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static p14 a(b14 b14, Provider<Context> provider, Provider<u04> provider2, Provider<an4> provider3) {
        return new p14(b14, provider, provider2, provider3);
    }

    @DexIgnore
    public static kk4 b(b14 b14, Provider<Context> provider, Provider<u04> provider2, Provider<an4> provider3) {
        return a(b14, provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public static kk4 a(b14 b14, Context context, u04 u04, an4 an4) {
        kk4 a2 = b14.a(context, u04, an4);
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public kk4 get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
