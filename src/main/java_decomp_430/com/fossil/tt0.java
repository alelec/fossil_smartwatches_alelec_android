package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tt0 extends if1 {
    @DexIgnore
    public byte[] B;
    @DexIgnore
    public /* final */ ArrayList<hl1> C; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.AUTHENTICATION}));
    @DexIgnore
    public /* final */ nq0 D;
    @DexIgnore
    public /* final */ byte[] E;

    @DexIgnore
    public tt0(ue1 ue1, q41 q41, nq0 nq0, byte[] bArr) {
        super(ue1, q41, eh1.START_AUTHENTICATION, (String) null, 8);
        this.D = nq0;
        this.E = bArr;
    }

    @DexIgnore
    public Object d() {
        byte[] bArr = this.B;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.C;
    }

    @DexIgnore
    public void h() {
        byte[] bArr = this.E;
        if (bArr.length != 8) {
            a(sk1.INVALID_PARAMETER);
            return;
        }
        if1.a((if1) this, (qv0) new tm1(this.w, this.D, bArr), (hg6) new po0(this), (hg6) hq0.a, (ig6) null, (hg6) new as0(this), (hg6) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(super.i(), bm0.AUTHENTICATION_KEY_TYPE, (Object) cw0.a((Enum<?>) this.D)), bm0.PHONE_RANDOM_NUMBER, (Object) cw0.a(this.E, (String) null, 1));
    }

    @DexIgnore
    public JSONObject k() {
        JSONObject k = super.k();
        bm0 bm0 = bm0.BOTH_SIDES_RANDOM_NUMBERS;
        byte[] bArr = this.B;
        String str = null;
        if (bArr != null) {
            str = cw0.a(bArr, (String) null, 1);
        }
        return cw0.a(k, bm0, (Object) str);
    }
}
