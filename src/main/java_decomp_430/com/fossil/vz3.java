package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vz3 extends tz3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (qx3 == qx3.UPC_E) {
            return super.a(str, qx3, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode UPC_E, but got " + qx3);
    }

    @DexIgnore
    public boolean[] a(String str) {
        if (str.length() == 8) {
            int i = uz3.f[Integer.parseInt(str.substring(7, 8))];
            boolean[] zArr = new boolean[51];
            int a = qz3.a(zArr, 0, sz3.a, true) + 0;
            int i2 = 1;
            while (i2 <= 6) {
                int i3 = i2 + 1;
                int parseInt = Integer.parseInt(str.substring(i2, i3));
                if (((i >> (6 - i2)) & 1) == 1) {
                    parseInt += 10;
                }
                a += qz3.a(zArr, a, sz3.e[parseInt], false);
                i2 = i3;
            }
            qz3.a(zArr, a, sz3.c, false);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
