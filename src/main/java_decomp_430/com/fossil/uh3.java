package com.fossil;

import android.os.Bundle;
import android.view.View;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uh3 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public int c; // = 0;

    @DexIgnore
    public uh3(th3 th3) {
        this.a = (View) th3;
    }

    @DexIgnore
    public void a(Bundle bundle) {
        this.b = bundle.getBoolean("expanded", false);
        this.c = bundle.getInt("expandedComponentIdHint", 0);
        if (this.b) {
            a();
        }
    }

    @DexIgnore
    public int b() {
        return this.c;
    }

    @DexIgnore
    public boolean c() {
        return this.b;
    }

    @DexIgnore
    public Bundle d() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("expanded", this.b);
        bundle.putInt("expandedComponentIdHint", this.c);
        return bundle;
    }

    @DexIgnore
    public void a(int i) {
        this.c = i;
    }

    @DexIgnore
    public final void a() {
        ViewParent parent = this.a.getParent();
        if (parent instanceof CoordinatorLayout) {
            ((CoordinatorLayout) parent).a(this.a);
        }
    }
}
