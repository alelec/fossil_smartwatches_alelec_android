package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.jv;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tv<Data> implements jv<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"http", Utility.URL_SCHEME})));
    @DexIgnore
    public /* final */ jv<cv, Data> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements kv<Uri, InputStream> {
        @DexIgnore
        public jv<Uri, InputStream> a(nv nvVar) {
            return new tv(nvVar.a(cv.class, InputStream.class));
        }
    }

    @DexIgnore
    public tv(jv<cv, Data> jvVar) {
        this.a = jvVar;
    }

    @DexIgnore
    public jv.a<Data> a(Uri uri, int i, int i2, xr xrVar) {
        return this.a.a(new cv(uri.toString()), i, i2, xrVar);
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
