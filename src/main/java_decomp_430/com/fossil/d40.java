package com.fossil;

import com.fossil.m20;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d40 implements m20.a {
    @DexIgnore
    public String a(File file) throws IOException {
        return a(file.getPath());
    }

    @DexIgnore
    public static String a(String str) throws IOException {
        BufferedInputStream bufferedInputStream = null;
        try {
            BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(str));
            try {
                String a = z86.a(bufferedInputStream2);
                z86.a(bufferedInputStream2);
                return a;
            } catch (Throwable th) {
                th = th;
                bufferedInputStream = bufferedInputStream2;
                z86.a(bufferedInputStream);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            z86.a(bufferedInputStream);
            throw th;
        }
    }
}
