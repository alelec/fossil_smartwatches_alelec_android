package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr3 extends BroadcastReceiver {
    @DexIgnore
    public qr3 a;

    @DexIgnore
    public pr3(qr3 qr3) {
        this.a = qr3;
    }

    @DexIgnore
    public final void a() {
        if (FirebaseInstanceId.m()) {
            Log.d("FirebaseInstanceId", "Connectivity change received registered");
        }
        this.a.a().registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @DexIgnore
    public final void onReceive(Context context, Intent intent) {
        qr3 qr3 = this.a;
        if (qr3 != null && qr3.b()) {
            if (FirebaseInstanceId.m()) {
                Log.d("FirebaseInstanceId", "Connectivity changed. Starting background sync.");
            }
            FirebaseInstanceId.a((Runnable) this.a, 0);
            this.a.a().unregisterReceiver(this);
            this.a = null;
        }
    }
}
