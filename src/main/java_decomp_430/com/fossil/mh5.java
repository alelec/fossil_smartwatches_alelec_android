package com.fossil;

import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mh5 extends k24<lh5> {
    @DexIgnore
    void a(ut4 ut4, ArrayList<String> arrayList);

    @DexIgnore
    void a(zh4 zh4, ActivitySummary activitySummary);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void a(boolean z, zh4 zh4, cf<WorkoutSession> cfVar);
}
