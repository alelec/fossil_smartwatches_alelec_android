package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r82 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<r82> CREATOR; // = new t82();
    @DexIgnore
    public /* final */ f72 a;
    @DexIgnore
    public /* final */ d82 b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public r82(f72 f72, IBinder iBinder, long j, long j2) {
        this.a = f72;
        this.b = c82.a(iBinder);
        this.c = j;
        this.d = j2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof r82)) {
            return false;
        }
        r82 r82 = (r82) obj;
        return u12.a(this.a, r82.a) && this.c == r82.c && this.d == r82.d;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(this.a, Long.valueOf(this.c), Long.valueOf(this.d));
    }

    @DexIgnore
    public f72 p() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return String.format("FitnessSensorServiceRequest{%s}", new Object[]{this.a});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) p(), i, false);
        g22.a(parcel, 2, this.b.asBinder(), false);
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d);
        g22.a(parcel, a2);
    }
}
