package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r53 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ x53 a;
    @DexIgnore
    public /* final */ /* synthetic */ t43 b;

    @DexIgnore
    public r53(o53 o53, x53 x53, t43 t43) {
        this.a = x53;
        this.b = t43;
    }

    @DexIgnore
    public final void run() {
        if (this.a.t() == null) {
            this.b.t().a("Install Referrer Reporter is null");
            return;
        }
        n53 t = this.a.t();
        t.a.i();
        t.a(t.a.c().getPackageName());
    }
}
