package com.fossil;

import androidx.collection.SimpleArrayMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p4<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    @DexIgnore
    public u4<K, V> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends u4<K, V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Object a(int i, int i2) {
            return p4.this.b[(i << 1) + i2];
        }

        @DexIgnore
        public int b(Object obj) {
            return p4.this.c(obj);
        }

        @DexIgnore
        public int c() {
            return p4.this.c;
        }

        @DexIgnore
        public int a(Object obj) {
            return p4.this.b(obj);
        }

        @DexIgnore
        public Map<K, V> b() {
            return p4.this;
        }

        @DexIgnore
        public void a(K k, V v) {
            p4.this.put(k, v);
        }

        @DexIgnore
        public V a(int i, V v) {
            return p4.this.a(i, v);
        }

        @DexIgnore
        public void a(int i) {
            p4.this.d(i);
        }

        @DexIgnore
        public void a() {
            p4.this.clear();
        }
    }

    @DexIgnore
    public p4() {
    }

    @DexIgnore
    public boolean a(Collection<?> collection) {
        return u4.c(this, collection);
    }

    @DexIgnore
    public final u4<K, V> b() {
        if (this.h == null) {
            this.h = new a();
        }
        return this.h;
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        return b().d();
    }

    @DexIgnore
    public Set<K> keySet() {
        return b().e();
    }

    @DexIgnore
    public void putAll(Map<? extends K, ? extends V> map) {
        b(this.c + map.size());
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    @DexIgnore
    public Collection<V> values() {
        return b().f();
    }

    @DexIgnore
    public p4(int i) {
        super(i);
    }

    @DexIgnore
    public p4(SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }
}
