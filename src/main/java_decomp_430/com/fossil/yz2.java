package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.ViewIndexer;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.mj2;
import com.fossil.oj2;
import com.fossil.qj2;
import com.fossil.sj2;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wi2;
import com.fossil.zi2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yz2 extends aa3 {
    @DexIgnore
    public static /* final */ String[] f; // = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] g; // = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    @DexIgnore
    public static /* final */ String[] h; // = {ViewIndexer.APP_VERSION_PARAM, "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;", "ga_app_id", "ALTER TABLE apps ADD COLUMN ga_app_id TEXT;"};
    @DexIgnore
    public static /* final */ String[] i; // = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    @DexIgnore
    public static /* final */ String[] j; // = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] k; // = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};
    @DexIgnore
    public static /* final */ String[] l; // = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};
    @DexIgnore
    public static /* final */ String[] m; // = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    @DexIgnore
    public /* final */ zz2 d; // = new zz2(this, c(), "google_app_measurement.db");
    @DexIgnore
    public /* final */ w93 e; // = new w93(zzm());

    @DexIgnore
    public yz2(ea3 ea3) {
        super(ea3);
    }

    @DexIgnore
    public final void A() {
        r();
        v().endTransaction();
    }

    @DexIgnore
    public final boolean B() {
        return b("select count(1) > 0 from queue where has_realtime = 1", (String[]) null) != 0;
    }

    @DexIgnore
    public final void C() {
        int delete;
        g();
        r();
        if (y()) {
            long a = k().h.a();
            long c = zzm().c();
            if (Math.abs(c - a) > l03.D.a(null).longValue()) {
                k().h.a(c);
                g();
                r();
                if (y() && (delete = v().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(zzm().b()), String.valueOf(cb3.w())})) > 0) {
                    b().B().a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                }
            }
        }
    }

    @DexIgnore
    public final long D() {
        return a("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    @DexIgnore
    public final long E() {
        return a("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    @DexIgnore
    public final boolean F() {
        return b("select count(1) > 0 from raw_events", (String[]) null) != 0;
    }

    @DexIgnore
    public final boolean G() {
        return b("select count(1) > 0 from raw_events where realtime = 1", (String[]) null) != 0;
    }

    @DexIgnore
    public final long a(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j3 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j3;
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            b().t().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long b(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j2 = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j2;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            b().t().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9  */
    public final na3 c(String str, String str2) {
        Cursor cursor;
        String str3 = str2;
        w12.b(str);
        w12.b(str2);
        g();
        r();
        try {
            cursor = v().query("user_attributes", new String[]{"set_timestamp", "value", "origin"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    na3 na3 = new na3(str, cursor.getString(2), str2, cursor.getLong(0), a(cursor, 1));
                    if (cursor.moveToNext()) {
                        b().t().a("Got multiple records for user property, expected one. appId", t43.a(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return na3;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        b().t().a("Error querying user property. appId", t43.a(str), i().c(str3), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                b().t().a("Error querying user property. appId", t43.a(str), i().c(str3), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Error querying user property. appId", t43.a(str), i().c(str3), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0125  */
    public final ab3 d(String str, String str2) {
        Cursor cursor;
        String str3 = str2;
        w12.b(str);
        w12.b(str2);
        g();
        r();
        try {
            cursor = v().query("conditional_properties", new String[]{"origin", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, "app_id=? and name=?", new String[]{str, str3}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                try {
                    Object a = a(cursor, 1);
                    boolean z = cursor.getInt(2) != 0;
                    String str4 = str;
                    ab3 ab3 = new ab3(str4, string, new la3(str2, cursor.getLong(8), a, string), cursor.getLong(6), z, cursor.getString(3), (j03) n().a(cursor.getBlob(5), j03.CREATOR), cursor.getLong(4), (j03) n().a(cursor.getBlob(7), j03.CREATOR), cursor.getLong(9), (j03) n().a(cursor.getBlob(10), j03.CREATOR));
                    if (cursor.moveToNext()) {
                        b().t().a("Got multiple records for conditional property, expected one", t43.a(str), i().c(str3));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return ab3;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        b().t().a("Error querying conditional property", t43.a(str), i().c(str3), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                b().t().a("Error querying conditional property", t43.a(str), i().c(str3), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Error querying conditional property", t43.a(str), i().c(str3), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final int e(String str, String str2) {
        w12.b(str);
        w12.b(str2);
        g();
        r();
        try {
            return v().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            b().t().a("Error deleting conditional property", t43.a(str), i().c(str2), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b6  */
    public final Map<Integer, List<wi2>> f(String str, String str2) {
        Cursor cursor;
        r();
        g();
        w12.b(str);
        w12.b(str2);
        p4 p4Var = new p4();
        try {
            cursor = v().query("event_filters", new String[]{"audience_id", "data"}, "app_id=? AND event_name=?", new String[]{str, str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<wi2>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    try {
                        wi2.a z = wi2.z();
                        ia3.a(z, blob);
                        wi2 wi2 = (wi2) ((fn2) z.i());
                        int i2 = cursor.getInt(0);
                        List list = (List) p4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            p4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(wi2);
                    } catch (IOException e2) {
                        b().t().a("Failed to merge filter. appId", t43.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return p4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    b().t().a("Database error querying filters. appId", t43.a(str), e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Database error querying filters. appId", t43.a(str), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b6  */
    public final Map<Integer, List<zi2>> g(String str, String str2) {
        Cursor cursor;
        r();
        g();
        w12.b(str);
        w12.b(str2);
        p4 p4Var = new p4();
        try {
            cursor = v().query("property_filters", new String[]{"audience_id", "data"}, "app_id=? AND property_name=?", new String[]{str, str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<zi2>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    byte[] blob = cursor.getBlob(1);
                    try {
                        zi2.a w = zi2.w();
                        ia3.a(w, blob);
                        zi2 zi2 = (zi2) ((fn2) w.i());
                        int i2 = cursor.getInt(0);
                        List list = (List) p4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            p4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(zi2);
                    } catch (IOException e2) {
                        b().t().a("Failed to merge filter", t43.a(str), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return p4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    b().t().a("Database error querying filters. appId", t43.a(str), e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Database error querying filters. appId", t43.a(str), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final long h(String str, String str2) {
        long j2;
        String str3 = str;
        String str4 = str2;
        w12.b(str);
        w12.b(str2);
        g();
        r();
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str4);
            sb.append(" from app2 where app_id=?");
            try {
                j2 = a(sb.toString(), new String[]{str3}, -1);
                if (j2 == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str3);
                    contentValues.put("first_open_count", 0);
                    contentValues.put("previous_install_count", 0);
                    if (v.insertWithOnConflict("app2", (String) null, contentValues, 5) == -1) {
                        b().t().a("Failed to insert column (got -1). appId", t43.a(str), str4);
                        v.endTransaction();
                        return -1;
                    }
                    j2 = 0;
                }
            } catch (SQLiteException e2) {
                e = e2;
                j2 = 0;
                try {
                    b().t().a("Error inserting column. appId", t43.a(str), str4, e);
                    v.endTransaction();
                    return j2;
                } catch (Throwable th) {
                    th = th;
                    v.endTransaction();
                    throw th;
                }
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str3);
                contentValues2.put(str4, Long.valueOf(1 + j2));
                if (((long) v.update("app2", contentValues2, "app_id = ?", new String[]{str3})) == 0) {
                    b().t().a("Failed to update column (got 0). appId", t43.a(str), str4);
                    v.endTransaction();
                    return -1;
                }
                v.setTransactionSuccessful();
                v.endTransaction();
                return j2;
            } catch (SQLiteException e3) {
                e = e3;
                b().t().a("Error inserting column. appId", t43.a(str), str4, e);
                v.endTransaction();
                return j2;
            }
        } catch (SQLiteException e4) {
            e = e4;
            j2 = 0;
            b().t().a("Error inserting column. appId", t43.a(str), str4, e);
            v.endTransaction();
            return j2;
        } catch (Throwable th2) {
            th = th2;
            v.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    public final boolean t() {
        return false;
    }

    @DexIgnore
    public final void u() {
        r();
        v().setTransactionSuccessful();
    }

    @DexIgnore
    public final SQLiteDatabase v() {
        g();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            b().w().a("Error opening database", e2);
            throw e2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    public final String w() {
        Cursor cursor;
        try {
            cursor = v().rawQuery("select app_id from queue order by has_realtime desc, rowid asc limit 1;", (String[]) null);
            try {
                if (cursor.moveToFirst()) {
                    String string = cursor.getString(0);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return string;
                }
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    b().t().a("Database error getting next bundle app id", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            b().t().a("Database error getting next bundle app id", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long x() {
        Cursor cursor = null;
        try {
            cursor = v().rawQuery("select rowid from raw_events order by rowid desc limit 1;", (String[]) null);
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            long j2 = cursor.getLong(0);
            if (cursor != null) {
                cursor.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            b().t().a("Error querying raw events", e2);
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean y() {
        return c().getDatabasePath("google_app_measurement.db").exists();
    }

    @DexIgnore
    public final void z() {
        r();
        v().beginTransaction();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x015b  */
    public final f03 a(String str, String str2) {
        Cursor cursor;
        Boolean bool;
        String str3 = str;
        String str4 = str2;
        w12.b(str);
        w12.b(str2);
        g();
        r();
        boolean e2 = l().e(str3, l03.v0);
        ArrayList arrayList = new ArrayList(Arrays.asList(new String[]{"lifetime_count", "current_bundle_count", "last_fire_timestamp", "last_bundled_timestamp", "last_bundled_day", "last_sampled_complex_event_id", "last_sampling_rate", "last_exempt_from_sampling"}));
        if (e2) {
            arrayList.add("current_session_count");
        }
        try {
            boolean z = false;
            Cursor query = v().query("events", (String[]) arrayList.toArray(new String[0]), "app_id=? and name=?", new String[]{str3, str4}, (String) null, (String) null, (String) null);
            try {
                if (!query.moveToFirst()) {
                    if (query != null) {
                        query.close();
                    }
                    return null;
                }
                long j2 = query.getLong(0);
                long j3 = query.getLong(1);
                long j4 = query.getLong(2);
                long j5 = 0;
                long j6 = query.isNull(3) ? 0 : query.getLong(3);
                Long valueOf = query.isNull(4) ? null : Long.valueOf(query.getLong(4));
                Long valueOf2 = query.isNull(5) ? null : Long.valueOf(query.getLong(5));
                Long valueOf3 = query.isNull(6) ? null : Long.valueOf(query.getLong(6));
                if (!query.isNull(7)) {
                    if (query.getLong(7) == 1) {
                        z = true;
                    }
                    bool = Boolean.valueOf(z);
                } else {
                    bool = null;
                }
                if (e2 && !query.isNull(8)) {
                    j5 = query.getLong(8);
                }
                cursor = query;
                try {
                    f03 f03 = new f03(str, str2, j2, j3, j5, j4, j6, valueOf, valueOf2, valueOf3, bool);
                    if (cursor.moveToNext()) {
                        b().t().a("Got multiple records for event aggregates, expected one. appId", t43.a(str));
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    return f03;
                } catch (SQLiteException e3) {
                    e = e3;
                    try {
                        b().t().a("Error querying events. appId", t43.a(str), i().a(str2), e);
                        if (cursor != null) {
                            cursor.close();
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e4) {
                e = e4;
                cursor = query;
                b().t().a("Error querying events. appId", t43.a(str), i().a(str2), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor = query;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e5) {
            e = e5;
            cursor = null;
            b().t().a("Error querying events. appId", t43.a(str), i().a(str2), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void b(String str, String str2) {
        w12.b(str);
        w12.b(str2);
        g();
        r();
        try {
            b().B().a("Deleted user attribute rows", Integer.valueOf(v().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2})));
        } catch (SQLiteException e2) {
            b().t().a("Error deleting user attribute. appId", t43.a(str), i().c(str2), e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0086  */
    public final Map<Integer, List<Integer>> e(String str) {
        Cursor cursor;
        r();
        g();
        w12.b(str);
        p4 p4Var = new p4();
        try {
            cursor = v().rawQuery("select audience_id, filter_id from event_filters where app_id = ? and session_scoped = 1 UNION select audience_id, filter_id from property_filters where app_id = ? and session_scoped = 1;", new String[]{str, str});
            try {
                if (!cursor.moveToFirst()) {
                    Map<Integer, List<Integer>> emptyMap = Collections.emptyMap();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return emptyMap;
                }
                do {
                    int i2 = cursor.getInt(0);
                    List list = (List) p4Var.get(Integer.valueOf(i2));
                    if (list == null) {
                        list = new ArrayList();
                        p4Var.put(Integer.valueOf(i2), list);
                    }
                    list.add(Integer.valueOf(cursor.getInt(1)));
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return p4Var;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    b().t().a("Database error querying scoped filters. appId", t43.a(str), e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            b().t().a("Database error querying scoped filters. appId", t43.a(str), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final List<ab3> b(String str, String str2, String str3) {
        w12.b(str);
        g();
        r();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return a(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    @DexIgnore
    public final long c(String str) {
        w12.b(str);
        g();
        r();
        try {
            return (long) v().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, l().b(str, l03.u))))});
        } catch (SQLiteException e2) {
            b().t().a("Error deleting over the limit events. appId", t43.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    public final long g(String str) {
        w12.b(str);
        return a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009c  */
    public final Map<Integer, sj2> f(String str) {
        Cursor cursor;
        r();
        g();
        w12.b(str);
        try {
            cursor = v().query("audience_filter_values", new String[]{"audience_id", "current_results"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                p4 p4Var = new p4();
                do {
                    int i2 = cursor.getInt(0);
                    byte[] blob = cursor.getBlob(1);
                    try {
                        sj2.a A = sj2.A();
                        ia3.a(A, blob);
                        p4Var.put(Integer.valueOf(i2), (sj2) ((fn2) A.i()));
                    } catch (IOException e2) {
                        b().t().a("Failed to merge filter results. appId, audienceId, error", t43.a(str), Integer.valueOf(i2), e2);
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return p4Var;
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    b().t().a("Database error querying filter results. appId", t43.a(str), e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Database error querying filter results. appId", t43.a(str), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x011b A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x011f A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0155 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0157 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0166 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x017b A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0197 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0198 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01a7 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01dd A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x021a  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0221  */
    public final a63 b(String str) {
        Cursor cursor;
        boolean z;
        boolean z2;
        String str2 = str;
        w12.b(str);
        g();
        r();
        try {
            boolean z3 = true;
            cursor = v().query("apps", new String[]{"app_instance_id", "gmp_app_id", "resettable_device_id_hash", "last_bundle_index", "last_bundle_start_timestamp", "last_bundle_end_timestamp", ViewIndexer.APP_VERSION_PARAM, "app_store", "gmp_version", "dev_cert_hash", "measurement_enabled", "day", "daily_public_events_count", "daily_events_count", "daily_conversions_count", "config_fetched_time", "failed_config_fetch_time", "app_version_int", "firebase_instance_id", "daily_error_events_count", "daily_realtime_events_count", "health_monitor_sample", "android_id", "adid_reporting_enabled", "ssaid_reporting_enabled", "admob_app_id", "dynamite_version", "safelisted_events", "ga_app_id"}, "app_id=?", new String[]{str2}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                try {
                    a63 a63 = new a63(this.b.v(), str2);
                    a63.a(cursor.getString(0));
                    a63.b(cursor.getString(1));
                    a63.e(cursor.getString(2));
                    a63.g(cursor.getLong(3));
                    a63.a(cursor.getLong(4));
                    a63.b(cursor.getLong(5));
                    a63.g(cursor.getString(6));
                    a63.h(cursor.getString(7));
                    a63.d(cursor.getLong(8));
                    a63.e(cursor.getLong(9));
                    if (!cursor.isNull(10)) {
                        if (cursor.getInt(10) == 0) {
                            z = false;
                            a63.a(z);
                            a63.j(cursor.getLong(11));
                            a63.k(cursor.getLong(12));
                            a63.l(cursor.getLong(13));
                            a63.m(cursor.getLong(14));
                            a63.h(cursor.getLong(15));
                            a63.i(cursor.getLong(16));
                            a63.c(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                            a63.f(cursor.getString(18));
                            a63.o(cursor.getLong(19));
                            a63.n(cursor.getLong(20));
                            a63.i(cursor.getString(21));
                            long j2 = 0;
                            a63.p(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                            if (!cursor.isNull(23)) {
                                if (cursor.getInt(23) == 0) {
                                    z2 = false;
                                    a63.b(z2);
                                    if (!cursor.isNull(24)) {
                                        if (cursor.getInt(24) == 0) {
                                            z3 = false;
                                        }
                                    }
                                    a63.c(z3);
                                    a63.c(cursor.getString(25));
                                    if (!cursor.isNull(26)) {
                                        j2 = cursor.getLong(26);
                                    }
                                    a63.f(j2);
                                    if (!cursor.isNull(27)) {
                                        a63.a((List<String>) Arrays.asList(cursor.getString(27).split(",", -1)));
                                    }
                                    if (bt2.a() && l().e(str2, l03.K0)) {
                                        a63.d(cursor.getString(28));
                                    }
                                    a63.k();
                                    if (cursor.moveToNext()) {
                                        b().t().a("Got multiple records for app, expected one. appId", t43.a(str));
                                    }
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return a63;
                                }
                            }
                            z2 = true;
                            a63.b(z2);
                            if (!cursor.isNull(24)) {
                            }
                            a63.c(z3);
                            a63.c(cursor.getString(25));
                            if (!cursor.isNull(26)) {
                            }
                            a63.f(j2);
                            if (!cursor.isNull(27)) {
                            }
                            a63.d(cursor.getString(28));
                            a63.k();
                            if (cursor.moveToNext()) {
                            }
                            if (cursor != null) {
                            }
                            return a63;
                        }
                    }
                    z = true;
                    a63.a(z);
                    a63.j(cursor.getLong(11));
                    a63.k(cursor.getLong(12));
                    a63.l(cursor.getLong(13));
                    a63.m(cursor.getLong(14));
                    a63.h(cursor.getLong(15));
                    a63.i(cursor.getLong(16));
                    a63.c(!cursor.isNull(17) ? -2147483648L : (long) cursor.getInt(17));
                    a63.f(cursor.getString(18));
                    a63.o(cursor.getLong(19));
                    a63.n(cursor.getLong(20));
                    a63.i(cursor.getString(21));
                    long j22 = 0;
                    a63.p(!cursor.isNull(22) ? 0 : cursor.getLong(22));
                    if (!cursor.isNull(23)) {
                    }
                    z2 = true;
                    a63.b(z2);
                    if (!cursor.isNull(24)) {
                    }
                    a63.c(z3);
                    a63.c(cursor.getString(25));
                    if (!cursor.isNull(26)) {
                    }
                    a63.f(j22);
                    if (!cursor.isNull(27)) {
                    }
                    a63.d(cursor.getString(28));
                    a63.k();
                    if (cursor.moveToNext()) {
                    }
                    if (cursor != null) {
                    }
                    return a63;
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        b().t().a("Error querying app. appId", t43.a(str), e);
                        if (cursor != null) {
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                b().t().a("Error querying app. appId", t43.a(str), e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Error querying app. appId", t43.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
    public final byte[] d(String str) {
        Cursor cursor;
        w12.b(str);
        g();
        r();
        try {
            cursor = v().query("apps", new String[]{"remote_config"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                if (cursor.moveToNext()) {
                    b().t().a("Got multiple records for app config, expected one. appId", t43.a(str));
                }
                if (cursor != null) {
                    cursor.close();
                }
                return blob;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    b().t().a("Error querying remote config. appId", t43.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            b().t().a("Error querying remote config. appId", t43.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean c(String str, List<Integer> list) {
        w12.b(str);
        r();
        g();
        SQLiteDatabase v = v();
        try {
            long b = b("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(RecyclerView.MAX_SCROLL_DURATION, l().b(str, l03.K)));
            if (b <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return v.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            b().t().a("Database error querying filters. appId", t43.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(f03 f03) {
        w12.a(f03);
        g();
        r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", f03.a);
        contentValues.put("name", f03.b);
        contentValues.put("lifetime_count", Long.valueOf(f03.c));
        contentValues.put("current_bundle_count", Long.valueOf(f03.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(f03.f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(f03.g));
        contentValues.put("last_bundled_day", f03.h);
        contentValues.put("last_sampled_complex_event_id", f03.i);
        contentValues.put("last_sampling_rate", f03.j);
        if (l().e(f03.a, l03.v0)) {
            contentValues.put("current_session_count", Long.valueOf(f03.e));
        }
        Boolean bool = f03.k;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (v().insertWithOnConflict("events", (String) null, contentValues, 5) == -1) {
                b().t().a("Failed to insert/update event aggregates (got -1). appId", t43.a(f03.a));
            }
        } catch (SQLiteException e2) {
            b().t().a("Error storing event aggregates. appId", t43.a(f03.a), e2);
        }
    }

    @DexIgnore
    public final boolean a(na3 na3) {
        w12.a(na3);
        g();
        r();
        if (c(na3.a, na3.c) == null) {
            if (ma3.e(na3.c)) {
                if (b("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{na3.a}) >= 25) {
                    return false;
                }
            } else if (!l().e(na3.a, l03.i0)) {
                if (b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{na3.a, na3.b}) >= 25) {
                    return false;
                }
            } else if (!"_npa".equals(na3.c)) {
                if (b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{na3.a, na3.b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", na3.a);
        contentValues.put("origin", na3.b);
        contentValues.put("name", na3.c);
        contentValues.put("set_timestamp", Long.valueOf(na3.d));
        a(contentValues, "value", na3.e);
        try {
            if (v().insertWithOnConflict("user_attributes", (String) null, contentValues, 5) == -1) {
                b().t().a("Failed to insert/update user property (got -1). appId", t43.a(na3.a));
            }
        } catch (SQLiteException e2) {
            b().t().a("Error storing user property. appId", t43.a(na3.a), e2);
        }
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ca  */
    public final Map<Integer, List<Integer>> b(String str, List<String> list) {
        Cursor cursor;
        r();
        g();
        w12.b(str);
        w12.a(list);
        p4 p4Var = new p4();
        if (list.isEmpty()) {
            return p4Var;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("app_id=? AND property_name in (");
        for (int i2 = 0; i2 < list.size(); i2++) {
            if (i2 > 0) {
                sb.append(",");
            }
            sb.append("?");
        }
        sb.append(")");
        ArrayList arrayList = new ArrayList(list);
        arrayList.add(0, str);
        try {
            cursor = v().query("property_filters", new String[]{"audience_id", "filter_id"}, sb.toString(), (String[]) arrayList.toArray(new String[0]), (String) null, (String) null, (String) null);
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return p4Var;
                }
                do {
                    int i3 = cursor.getInt(0);
                    List list2 = (List) p4Var.get(Integer.valueOf(i3));
                    if (list2 == null) {
                        list2 = new ArrayList();
                        p4Var.put(Integer.valueOf(i3), list2);
                    }
                    list2.add(Integer.valueOf(cursor.getInt(1)));
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return p4Var;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    b().t().a("Database error querying filters. appId", t43.a(str), e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            b().t().a("Database error querying filters. appId", t43.a(str), e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1  */
    public final List<na3> a(String str) {
        Cursor cursor;
        w12.b(str);
        g();
        r();
        ArrayList arrayList = new ArrayList();
        try {
            cursor = v().query("user_attributes", new String[]{"name", "origin", "set_timestamp", "value"}, "app_id=?", new String[]{str}, (String) null, (String) null, "rowid", "1000");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                do {
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    if (string2 == null) {
                        string2 = "";
                    }
                    String str2 = string2;
                    long j2 = cursor.getLong(2);
                    Object a = a(cursor, 3);
                    if (a == null) {
                        b().t().a("Read invalid user property value, ignoring it. appId", t43.a(str));
                    } else {
                        arrayList.add(new na3(str, str2, string, j2, a));
                    }
                } while (cursor.moveToNext());
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    b().t().a("Error querying user properties. appId", t43.a(str), e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            b().t().a("Error querying user properties. appId", t43.a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f9, code lost:
        r12 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0100, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0101, code lost:
        r12 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0104, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0105, code lost:
        r12 = r21;
        r11 = r22;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0100 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0127  */
    public final List<na3> a(String str, String str2, String str3) {
        Cursor cursor;
        String str4;
        w12.b(str);
        g();
        r();
        ArrayList arrayList = new ArrayList();
        Cursor cursor2 = null;
        try {
            ArrayList arrayList2 = new ArrayList(3);
            arrayList2.add(str);
            StringBuilder sb = new StringBuilder("app_id=?");
            if (!TextUtils.isEmpty(str2)) {
                str4 = str2;
                arrayList2.add(str4);
                sb.append(" and origin=?");
            } else {
                str4 = str2;
            }
            if (!TextUtils.isEmpty(str3)) {
                arrayList2.add(String.valueOf(str3).concat("*"));
                sb.append(" and name glob ?");
            }
            cursor = v().query("user_attributes", new String[]{"name", "set_timestamp", "value", "origin"}, sb.toString(), (String[]) arrayList2.toArray(new String[arrayList2.size()]), (String) null, (String) null, "rowid", "1001");
            try {
                if (!cursor.moveToFirst()) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                }
                while (true) {
                    if (arrayList.size() >= 1000) {
                        b().t().a("Read more than the max allowed user properties, ignoring excess", 1000);
                        break;
                    }
                    String string = cursor.getString(0);
                    long j2 = cursor.getLong(1);
                    try {
                        Object a = a(cursor, 2);
                        String string2 = cursor.getString(3);
                        if (a == null) {
                            try {
                                b().t().a("(2)Read invalid user property value, ignoring it", t43.a(str), string2, str3);
                            } catch (SQLiteException e2) {
                                e = e2;
                                str4 = string2;
                                try {
                                    b().t().a("(2)Error querying user properties", t43.a(str), str4, e);
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    return null;
                                } catch (Throwable th) {
                                    th = th;
                                    cursor2 = cursor;
                                    if (cursor2 != null) {
                                        cursor2.close();
                                    }
                                    throw th;
                                }
                            }
                        } else {
                            String str5 = str3;
                            arrayList.add(new na3(str, string2, string, j2, a));
                        }
                        if (!cursor.moveToNext()) {
                            break;
                        }
                        str4 = string2;
                    } catch (SQLiteException e3) {
                        e = e3;
                        b().t().a("(2)Error querying user properties", t43.a(str), str4, e);
                        if (cursor != null) {
                        }
                        return null;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            } catch (SQLiteException e4) {
                e = e4;
                b().t().a("(2)Error querying user properties", t43.a(str), str4, e);
                if (cursor != null) {
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                cursor2 = cursor;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e5) {
            e = e5;
            str4 = str2;
            cursor = null;
            b().t().a("(2)Error querying user properties", t43.a(str), str4, e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th3) {
        }
    }

    @DexIgnore
    public final boolean a(ab3 ab3) {
        w12.a(ab3);
        g();
        r();
        if (c(ab3.a, ab3.c.b) == null) {
            if (b("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{ab3.a}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", ab3.a);
        contentValues.put("origin", ab3.b);
        contentValues.put("name", ab3.c.b);
        a(contentValues, "value", ab3.c.zza());
        contentValues.put("active", Boolean.valueOf(ab3.e));
        contentValues.put("trigger_event_name", ab3.f);
        contentValues.put("trigger_timeout", Long.valueOf(ab3.h));
        j();
        contentValues.put("timed_out_event", ma3.a((Parcelable) ab3.g));
        contentValues.put("creation_timestamp", Long.valueOf(ab3.d));
        j();
        contentValues.put("triggered_event", ma3.a((Parcelable) ab3.i));
        contentValues.put("triggered_timestamp", Long.valueOf(ab3.c.c));
        contentValues.put("time_to_live", Long.valueOf(ab3.j));
        j();
        contentValues.put("expired_event", ma3.a((Parcelable) ab3.o));
        try {
            if (v().insertWithOnConflict("conditional_properties", (String) null, contentValues, 5) == -1) {
                b().t().a("Failed to insert/update conditional user property (got -1)", t43.a(ab3.a));
            }
        } catch (SQLiteException e2) {
            b().t().a("Error storing conditional user property", t43.a(ab3.a), e2);
        }
        return true;
    }

    @DexIgnore
    public final List<ab3> a(String str, String[] strArr) {
        g();
        r();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = v().query("conditional_properties", new String[]{"app_id", "origin", "name", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, (String) null, (String) null, "rowid", "1001");
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            }
            while (true) {
                if (arrayList.size() < 1000) {
                    boolean z = false;
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    Object a = a(cursor, 3);
                    if (cursor.getInt(4) != 0) {
                        z = true;
                    }
                    String string4 = cursor.getString(5);
                    long j2 = cursor.getLong(6);
                    long j3 = cursor.getLong(8);
                    long j4 = cursor.getLong(10);
                    boolean z2 = z;
                    ab3 ab3 = r3;
                    ab3 ab32 = new ab3(string, string2, new la3(string3, j4, a, string2), j3, z2, string4, (j03) n().a(cursor.getBlob(7), j03.CREATOR), j2, (j03) n().a(cursor.getBlob(9), j03.CREATOR), cursor.getLong(11), (j03) n().a(cursor.getBlob(12), j03.CREATOR));
                    arrayList.add(ab3);
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } else {
                    b().t().a("Read more than the max allowed conditional properties, ignoring extra", 1000);
                    break;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e2) {
            b().t().a("Error querying conditional user property value", e2);
            List<ab3> emptyList = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void a(a63 a63) {
        w12.a(a63);
        g();
        r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", a63.l());
        contentValues.put("app_instance_id", a63.m());
        contentValues.put("gmp_app_id", a63.n());
        contentValues.put("resettable_device_id_hash", a63.q());
        contentValues.put("last_bundle_index", Long.valueOf(a63.B()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(a63.s()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(a63.t()));
        contentValues.put(ViewIndexer.APP_VERSION_PARAM, a63.u());
        contentValues.put("app_store", a63.w());
        contentValues.put("gmp_version", Long.valueOf(a63.x()));
        contentValues.put("dev_cert_hash", Long.valueOf(a63.y()));
        contentValues.put("measurement_enabled", Boolean.valueOf(a63.A()));
        contentValues.put("day", Long.valueOf(a63.F()));
        contentValues.put("daily_public_events_count", Long.valueOf(a63.G()));
        contentValues.put("daily_events_count", Long.valueOf(a63.H()));
        contentValues.put("daily_conversions_count", Long.valueOf(a63.I()));
        contentValues.put("config_fetched_time", Long.valueOf(a63.C()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(a63.D()));
        contentValues.put("app_version_int", Long.valueOf(a63.v()));
        contentValues.put("firebase_instance_id", a63.r());
        contentValues.put("daily_error_events_count", Long.valueOf(a63.c()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(a63.b()));
        contentValues.put("health_monitor_sample", a63.d());
        contentValues.put("android_id", Long.valueOf(a63.f()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(a63.g()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(a63.h()));
        contentValues.put("admob_app_id", a63.o());
        contentValues.put("dynamite_version", Long.valueOf(a63.z()));
        if (a63.j() != null) {
            if (a63.j().size() == 0) {
                b().w().a("Safelisted events should not be an empty list. appId", a63.l());
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", a63.j()));
            }
        }
        if (bt2.a() && l().e(a63.l(), l03.K0)) {
            contentValues.put("ga_app_id", a63.p());
        }
        try {
            SQLiteDatabase v = v();
            if (((long) v.update("apps", contentValues, "app_id = ?", new String[]{a63.l()})) == 0 && v.insertWithOnConflict("apps", (String) null, contentValues, 5) == -1) {
                b().t().a("Failed to insert/update app (got -1). appId", t43.a(a63.l()));
            }
        } catch (SQLiteException e2) {
            b().t().a("Error storing app. appId", t43.a(a63.l()), e2);
        }
    }

    @DexIgnore
    public final xz2 a(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        w12.b(str);
        g();
        r();
        String[] strArr = {str};
        xz2 xz2 = new xz2();
        Cursor cursor = null;
        try {
            SQLiteDatabase v = v();
            cursor = v.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, (String) null, (String) null, (String) null);
            if (!cursor.moveToFirst()) {
                b().w().a("Not updating daily counts, app is not known. appId", t43.a(str));
                if (cursor != null) {
                    cursor.close();
                }
                return xz2;
            }
            if (cursor.getLong(0) == j2) {
                xz2.b = cursor.getLong(1);
                xz2.a = cursor.getLong(2);
                xz2.c = cursor.getLong(3);
                xz2.d = cursor.getLong(4);
                xz2.e = cursor.getLong(5);
            }
            if (z) {
                xz2.b++;
            }
            if (z2) {
                xz2.a++;
            }
            if (z3) {
                xz2.c++;
            }
            if (z4) {
                xz2.d++;
            }
            if (z5) {
                xz2.e++;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("day", Long.valueOf(j2));
            contentValues.put("daily_public_events_count", Long.valueOf(xz2.a));
            contentValues.put("daily_events_count", Long.valueOf(xz2.b));
            contentValues.put("daily_conversions_count", Long.valueOf(xz2.c));
            contentValues.put("daily_error_events_count", Long.valueOf(xz2.d));
            contentValues.put("daily_realtime_events_count", Long.valueOf(xz2.e));
            v.update("apps", contentValues, "app_id=?", strArr);
            if (cursor != null) {
                cursor.close();
            }
            return xz2;
        } catch (SQLiteException e2) {
            b().t().a("Error updating daily counts. appId", t43.a(str), e2);
            if (cursor != null) {
                cursor.close();
            }
            return xz2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(qj2 qj2, boolean z) {
        g();
        r();
        w12.a(qj2);
        w12.b(qj2.w0());
        w12.b(qj2.k0());
        C();
        long b = zzm().b();
        if (qj2.l0() < b - cb3.w() || qj2.l0() > cb3.w() + b) {
            b().w().a("Storing bundle outside of the max uploading time span. appId, now, timestamp", t43.a(qj2.w0()), Long.valueOf(b), Long.valueOf(qj2.l0()));
        }
        try {
            byte[] c = n().c(qj2.f());
            b().B().a("Saving bundle, size", Integer.valueOf(c.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", qj2.w0());
            contentValues.put("bundle_end_timestamp", Long.valueOf(qj2.l0()));
            contentValues.put("data", c);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            if (qj2.O()) {
                contentValues.put("retry_count", Integer.valueOf(qj2.Q()));
            }
            try {
                if (v().insert("queue", (String) null, contentValues) != -1) {
                    return true;
                }
                b().t().a("Failed to insert bundle (got -1). appId", t43.a(qj2.w0()));
                return false;
            } catch (SQLiteException e2) {
                b().t().a("Error storing bundle. appId", t43.a(qj2.w0()), e2);
                return false;
            }
        } catch (IOException e3) {
            b().t().a("Data loss. Failed to serialize bundle. appId", t43.a(qj2.w0()), e3);
            return false;
        }
    }

    @DexIgnore
    public final List<Pair<qj2, Long>> a(String str, int i2, int i3) {
        g();
        r();
        w12.a(i2 > 0);
        w12.a(i3 > 0);
        w12.b(str);
        Cursor cursor = null;
        try {
            cursor = v().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, (String) null, (String) null, "rowid", String.valueOf(i2));
            if (!cursor.moveToFirst()) {
                List<Pair<qj2, Long>> emptyList = Collections.emptyList();
                if (cursor != null) {
                    cursor.close();
                }
                return emptyList;
            }
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            do {
                long j2 = cursor.getLong(0);
                try {
                    byte[] b = n().b(cursor.getBlob(1));
                    if (!arrayList.isEmpty() && b.length + i4 > i3) {
                        break;
                    }
                    try {
                        qj2.a z0 = qj2.z0();
                        ia3.a(z0, b);
                        qj2.a aVar = z0;
                        if (!cursor.isNull(2)) {
                            aVar.i(cursor.getInt(2));
                        }
                        i4 += b.length;
                        arrayList.add(Pair.create((qj2) ((fn2) aVar.i()), Long.valueOf(j2)));
                    } catch (IOException e2) {
                        b().t().a("Failed to merge queued bundle. appId", t43.a(str), e2);
                    }
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } catch (IOException e3) {
                    b().t().a("Failed to unzip queued bundle. appId", t43.a(str), e3);
                }
            } while (i4 <= i3);
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e4) {
            b().t().a("Error querying bundles. appId", t43.a(str), e4);
            List<Pair<qj2, Long>> emptyList2 = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void a(List<Long> list) {
        g();
        r();
        w12.a(list);
        w12.a(list.size());
        if (y()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (b(sb3.toString(), (String[]) null) > 0) {
                b().w().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase v = v();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                v.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                b().t().a("Error incrementing retry count. error", e2);
            }
        }
    }

    @DexIgnore
    public final void a(String str, List<vi2> list) {
        boolean z;
        r();
        g();
        w12.b(str);
        w12.a(list);
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            r();
            g();
            w12.b(str);
            SQLiteDatabase v2 = v();
            v2.delete("property_filters", "app_id=?", new String[]{str});
            v2.delete("event_filters", "app_id=?", new String[]{str});
            for (vi2 next : list) {
                r();
                g();
                w12.b(str);
                w12.a(next);
                if (!next.n()) {
                    b().w().a("Audience with no ID. appId", t43.a(str));
                } else {
                    int o = next.o();
                    Iterator<wi2> it = next.r().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!it.next().n()) {
                                b().w().a("Event filter with no ID. Audience definition ignored. appId, audienceId", t43.a(str), Integer.valueOf(o));
                                break;
                            }
                        } else {
                            Iterator<zi2> it2 = next.p().iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (!it2.next().n()) {
                                        b().w().a("Property filter with no ID. Audience definition ignored. appId, audienceId", t43.a(str), Integer.valueOf(o));
                                        break;
                                    }
                                } else {
                                    Iterator<wi2> it3 = next.r().iterator();
                                    while (true) {
                                        if (it3.hasNext()) {
                                            if (!a(str, o, it3.next())) {
                                                z = false;
                                                break;
                                            }
                                        } else {
                                            z = true;
                                            break;
                                        }
                                    }
                                    if (z) {
                                        Iterator<zi2> it4 = next.p().iterator();
                                        while (true) {
                                            if (it4.hasNext()) {
                                                if (!a(str, o, it4.next())) {
                                                    z = false;
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        r();
                                        g();
                                        w12.b(str);
                                        SQLiteDatabase v3 = v();
                                        v3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(o)});
                                        v3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(o)});
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (vi2 next2 : list) {
                arrayList.add(next2.n() ? Integer.valueOf(next2.o()) : null);
            }
            c(str, (List<Integer>) arrayList);
            v.setTransactionSuccessful();
        } finally {
            v.endTransaction();
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, wi2 wi2) {
        r();
        g();
        w12.b(str);
        w12.a(wi2);
        Integer num = null;
        if (TextUtils.isEmpty(wi2.p())) {
            v43 w = b().w();
            Object a = t43.a(str);
            Integer valueOf = Integer.valueOf(i2);
            if (wi2.n()) {
                num = Integer.valueOf(wi2.o());
            }
            w.a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] f2 = wi2.f();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", wi2.n() ? Integer.valueOf(wi2.o()) : null);
        contentValues.put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, wi2.p());
        if (l().e(str, l03.u0)) {
            contentValues.put("session_scoped", wi2.x() ? Boolean.valueOf(wi2.y()) : null);
        }
        contentValues.put("data", f2);
        try {
            if (v().insertWithOnConflict("event_filters", (String) null, contentValues, 5) != -1) {
                return true;
            }
            b().t().a("Failed to insert event filter (got -1). appId", t43.a(str));
            return true;
        } catch (SQLiteException e2) {
            b().t().a("Error storing event filter. appId", t43.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, zi2 zi2) {
        r();
        g();
        w12.b(str);
        w12.a(zi2);
        Integer num = null;
        if (TextUtils.isEmpty(zi2.p())) {
            v43 w = b().w();
            Object a = t43.a(str);
            Integer valueOf = Integer.valueOf(i2);
            if (zi2.n()) {
                num = Integer.valueOf(zi2.o());
            }
            w.a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] f2 = zi2.f();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", zi2.n() ? Integer.valueOf(zi2.o()) : null);
        contentValues.put("property_name", zi2.p());
        if (l().e(str, l03.u0)) {
            contentValues.put("session_scoped", zi2.t() ? Boolean.valueOf(zi2.v()) : null);
        }
        contentValues.put("data", f2);
        try {
            if (v().insertWithOnConflict("property_filters", (String) null, contentValues, 5) != -1) {
                return true;
            }
            b().t().a("Failed to insert property filter (got -1). appId", t43.a(str));
            return false;
        } catch (SQLiteException e2) {
            b().t().a("Error storing property filter. appId", t43.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public static void a(ContentValues contentValues, String str, Object obj) {
        w12.b(str);
        w12.a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @DexIgnore
    public final Object a(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            b().t().a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                b().t().a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            b().t().a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    @DexIgnore
    public final long a(qj2 qj2) throws IOException {
        g();
        r();
        w12.a(qj2);
        w12.b(qj2.w0());
        byte[] f2 = qj2.f();
        long a = n().a(f2);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", qj2.w0());
        contentValues.put("metadata_fingerprint", Long.valueOf(a));
        contentValues.put("metadata", f2);
        try {
            v().insertWithOnConflict("raw_events_metadata", (String) null, contentValues, 4);
            return a;
        } catch (SQLiteException e2) {
            b().t().a("Error storing raw event metadata. appId", t43.a(qj2.w0()), e2);
            throw e2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    public final String a(long j2) {
        Cursor cursor;
        g();
        r();
        try {
            cursor = v().rawQuery("select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;", new String[]{String.valueOf(j2)});
            try {
                if (!cursor.moveToFirst()) {
                    b().B().a("No expired configs for apps with pending events");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                String string = cursor.getString(0);
                if (cursor != null) {
                    cursor.close();
                }
                return string;
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    b().t().a("Error selecting expired configs", e);
                    if (cursor != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e3) {
            e = e3;
            cursor = null;
            b().t().a("Error selecting expired configs", e);
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0093  */
    public final Pair<mj2, Long> a(String str, Long l2) {
        Cursor cursor;
        g();
        r();
        try {
            cursor = v().rawQuery("select main_event, children_to_process from main_event_params where app_id=? and event_id=?", new String[]{str, String.valueOf(l2)});
            try {
                if (!cursor.moveToFirst()) {
                    b().B().a("Main event not found");
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
                byte[] blob = cursor.getBlob(0);
                Long valueOf = Long.valueOf(cursor.getLong(1));
                try {
                    mj2.a y = mj2.y();
                    ia3.a(y, blob);
                    Pair<mj2, Long> create = Pair.create((mj2) ((fn2) y.i()), valueOf);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return create;
                } catch (IOException e2) {
                    b().t().a("Failed to merge main event. appId, eventId", t43.a(str), l2, e2);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                }
            } catch (SQLiteException e3) {
                e = e3;
                try {
                    b().t().a("Error selecting main event", e);
                    if (cursor != null) {
                        cursor.close();
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e4) {
            e = e4;
            cursor = null;
            b().t().a("Error selecting main event", e);
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(String str, Long l2, long j2, mj2 mj2) {
        g();
        r();
        w12.a(mj2);
        w12.b(str);
        w12.a(l2);
        byte[] f2 = mj2.f();
        b().B().a("Saving complex main event, appId, data size", i().a(str), Integer.valueOf(f2.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("event_id", l2);
        contentValues.put("children_to_process", Long.valueOf(j2));
        contentValues.put("main_event", f2);
        try {
            if (v().insertWithOnConflict("main_event_params", (String) null, contentValues, 5) != -1) {
                return true;
            }
            b().t().a("Failed to insert complex main event (got -1). appId", t43.a(str));
            return false;
        } catch (SQLiteException e2) {
            b().t().a("Error storing complex main event. appId", t43.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(g03 g03, long j2, boolean z) {
        g();
        r();
        w12.a(g03);
        w12.b(g03.a);
        mj2.a y = mj2.y();
        y.b(g03.e);
        Iterator<String> it = g03.f.iterator();
        while (it.hasNext()) {
            String next = it.next();
            oj2.a y2 = oj2.y();
            y2.a(next);
            n().a(y2, g03.f.e(next));
            y.a(y2);
        }
        byte[] f2 = ((mj2) y.i()).f();
        b().B().a("Saving event, name, data size", i().a(g03.b), Integer.valueOf(f2.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", g03.a);
        contentValues.put("name", g03.b);
        contentValues.put("timestamp", Long.valueOf(g03.d));
        contentValues.put("metadata_fingerprint", Long.valueOf(j2));
        contentValues.put("data", f2);
        contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
        try {
            if (v().insert("raw_events", (String) null, contentValues) != -1) {
                return true;
            }
            b().t().a("Failed to insert raw event (got -1). appId", t43.a(g03.a));
            return false;
        } catch (SQLiteException e2) {
            b().t().a("Error storing raw event. appId", t43.a(g03.a), e2);
            return false;
        }
    }
}
