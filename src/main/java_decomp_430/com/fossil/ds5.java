package com.fossil;

import com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds5 extends zr5 {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ as5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public ds5(as5 as5) {
        wg6.b(as5, "mPairingInstructionsView");
        this.f = as5;
    }

    @DexIgnore
    public void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void f() {
        this.f.g();
        this.f.v(!this.e);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        xm4 xm4 = xm4.d;
        as5 as5 = this.f;
        if (as5 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment");
        } else if (xm4.a(xm4, ((PairingInstructionsFragment) as5).getContext(), "PAIR_DEVICE", false, false, false, 28, (Object) null)) {
            this.f.j0();
        }
    }

    @DexIgnore
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }
}
