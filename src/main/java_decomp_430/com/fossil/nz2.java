package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nz2 implements Parcelable.Creator<StreetViewPanoramaOptions> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        StreetViewPanoramaCamera streetViewPanoramaCamera = null;
        String str = null;
        LatLng latLng = null;
        Integer num = null;
        dz2 dz2 = null;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        byte b6 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 2:
                    streetViewPanoramaCamera = f22.a(parcel, a, StreetViewPanoramaCamera.CREATOR);
                    break;
                case 3:
                    str = f22.e(parcel, a);
                    break;
                case 4:
                    latLng = f22.a(parcel, a, LatLng.CREATOR);
                    break;
                case 5:
                    num = f22.r(parcel, a);
                    break;
                case 6:
                    b2 = f22.k(parcel, a);
                    break;
                case 7:
                    b3 = f22.k(parcel, a);
                    break;
                case 8:
                    b4 = f22.k(parcel, a);
                    break;
                case 9:
                    b5 = f22.k(parcel, a);
                    break;
                case 10:
                    b6 = f22.k(parcel, a);
                    break;
                case 11:
                    dz2 = f22.a(parcel, a, dz2.CREATOR);
                    break;
                default:
                    f22.v(parcel, a);
                    break;
            }
        }
        f22.h(parcel, b);
        return new StreetViewPanoramaOptions(streetViewPanoramaCamera, str, latLng, num, b2, b3, b4, b5, b6, dz2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new StreetViewPanoramaOptions[i];
    }
}
