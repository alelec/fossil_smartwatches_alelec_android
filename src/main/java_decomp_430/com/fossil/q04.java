package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q04 {
    @DexIgnore
    public static /* final */ String a; // = "q04";
    @DexIgnore
    public static /* final */ String b; // = q04.class.getPackage().getName();

    @DexIgnore
    public static SharedPreferences a(Context context) {
        return context.getSharedPreferences(b, 0);
    }

    @DexIgnore
    public static String a(Context context, String str) {
        SharedPreferences a2 = a(context);
        if (a2 != null) {
            String str2 = a;
            Log.d(str2, "getString: " + str);
            return a2.getString(str, "");
        }
        String str3 = a;
        Log.d(str3, "getString: " + str);
        return "";
    }
}
