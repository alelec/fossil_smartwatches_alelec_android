package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kc4 extends jc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E; // = new SparseIntArray();
    @DexIgnore
    public long C;

    /*
    static {
        E.put(2131362442, 1);
        E.put(2131362343, 2);
        E.put(2131362588, 3);
        E.put(2131363267, 4);
        E.put(2131362347, 5);
        E.put(2131362592, 6);
        E.put(2131363268, 7);
        E.put(2131362396, 8);
        E.put(2131362610, 9);
        E.put(2131363269, 10);
        E.put(2131362561, 11);
    }
    */

    @DexIgnore
    public kc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, D, E));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public kc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[2], objArr[5], objArr[8], objArr[1], objArr[11], objArr[3], objArr[6], objArr[9], objArr[4], objArr[7], objArr[10]);
        this.C = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
