package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum k81 {
    REQUEST_HANDS(new byte[]{1}),
    RELEASE_HANDS(new byte[]{2}),
    MOVE_HANDS(new byte[]{3}),
    SET_CALIBRATION_POSITION(new byte[]{14});
    
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public k81(byte[] bArr) {
        this.a = bArr;
    }
}
