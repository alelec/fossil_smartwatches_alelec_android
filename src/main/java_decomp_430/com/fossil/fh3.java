package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fh3 extends BaseAdapter {
    @DexIgnore
    public static /* final */ int d; // = (Build.VERSION.SDK_INT >= 26 ? 4 : 1);
    @DexIgnore
    public /* final */ Calendar a; // = nh3.d();
    @DexIgnore
    public /* final */ int b; // = this.a.getMaximum(7);
    @DexIgnore
    public /* final */ int c; // = this.a.getFirstDayOfWeek();

    @DexIgnore
    public final int a(int i) {
        int i2 = i + this.c;
        int i3 = this.b;
        return i2 > i3 ? i2 - i3 : i2;
    }

    @DexIgnore
    public int getCount() {
        return this.b;
    }

    @DexIgnore
    public long getItemId(int i) {
        return 0;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v8, types: [android.view.View] */
    /* JADX WARNING: Multi-variable type inference failed */
    @SuppressLint({"WrongConstant"})
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) view;
        if (view == null) {
            textView = LayoutInflater.from(viewGroup.getContext()).inflate(tf3.mtrl_calendar_day_of_week, viewGroup, false);
        }
        this.a.set(7, a(i));
        textView.setText(this.a.getDisplayName(7, d, Locale.getDefault()));
        textView.setContentDescription(String.format(viewGroup.getContext().getString(vf3.mtrl_picker_day_of_week_column_header), new Object[]{this.a.getDisplayName(7, 2, Locale.getDefault())}));
        return textView;
    }

    @DexIgnore
    public Integer getItem(int i) {
        if (i >= this.b) {
            return null;
        }
        return Integer.valueOf(a(i));
    }
}
