package com.fossil;

import com.fossil.vl3;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class im3<E> extends vl3<E> implements Set<E> {
    @DexIgnore
    public static /* final */ int MAX_TABLE_SIZE; // = 1073741824;
    @DexIgnore
    public transient zl3<E> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<E> extends vl3.a<E> {
        @DexIgnore
        public a() {
            this(4);
        }

        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        public a<E> a(E e) {
            super.a(e);
            return this;
        }

        @DexIgnore
        public a<E> a(E... eArr) {
            super.a(eArr);
            return this;
        }

        @DexIgnore
        public a<E> a(Iterator<? extends E> it) {
            super.a(it);
            return this;
        }

        @DexIgnore
        public im3<E> a() {
            im3<E> access$000 = im3.a(this.b, this.a);
            this.b = access$000.size();
            return access$000;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> extends im3<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends tl3<E> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public E get(int i) {
                return b.this.get(i);
            }

            @DexIgnore
            public b<E> delegateCollection() {
                return b.this;
            }
        }

        @DexIgnore
        public zl3<E> createAsList() {
            return new a();
        }

        @DexIgnore
        public abstract E get(int i);

        @DexIgnore
        public jo3<E> iterator() {
            return asList().iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public c(Object[] objArr) {
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            return im3.copyOf((E[]) this.elements);
        }
    }

    @DexIgnore
    public static <E> im3<E> a(int i, Object... objArr) {
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return of(objArr[0]);
        }
        int chooseTableSize = chooseTableSize(i);
        Object[] objArr2 = new Object[chooseTableSize];
        int i2 = chooseTableSize - 1;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i; i5++) {
            Object obj = objArr[i5];
            in3.a(obj, i5);
            int hashCode = obj.hashCode();
            int a2 = sl3.a(hashCode);
            while (true) {
                int i6 = a2 & i2;
                Object obj2 = objArr2[i6];
                if (obj2 == null) {
                    objArr[i3] = obj;
                    objArr2[i6] = obj;
                    i4 += hashCode;
                    i3++;
                    break;
                } else if (obj2.equals(obj)) {
                    break;
                } else {
                    a2++;
                }
            }
        }
        Arrays.fill(objArr, i3, i, (Object) null);
        if (i3 == 1) {
            return new bo3(objArr[0], i4);
        }
        if (chooseTableSize != chooseTableSize(i3)) {
            return a(i3, objArr);
        }
        if (i3 < objArr.length) {
            objArr = in3.a((T[]) objArr, i3);
        }
        return new sn3(objArr, i4, objArr2, i2);
    }

    @DexIgnore
    public static <E> a<E> builder() {
        return new a<>();
    }

    @DexIgnore
    public static int chooseTableSize(int i) {
        boolean z = true;
        if (i < 751619276) {
            int highestOneBit = Integer.highestOneBit(i - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) i)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (i >= 1073741824) {
            z = false;
        }
        jk3.a(z, (Object) "collection too large");
        return 1073741824;
    }

    @DexIgnore
    public static <E> im3<E> copyOf(Collection<? extends E> collection) {
        if ((collection instanceof im3) && !(collection instanceof nm3)) {
            im3<E> im3 = (im3) collection;
            if (!im3.isPartialView()) {
                return im3;
            }
        } else if (collection instanceof EnumSet) {
            return a((EnumSet) collection);
        }
        Object[] array = collection.toArray();
        return a(array.length, array);
    }

    @DexIgnore
    public static <E> im3<E> of() {
        return sn3.EMPTY;
    }

    @DexIgnore
    public zl3<E> asList() {
        zl3<E> zl3 = this.a;
        if (zl3 != null) {
            return zl3;
        }
        zl3<E> createAsList = createAsList();
        this.a = createAsList;
        return createAsList;
    }

    @DexIgnore
    public zl3<E> createAsList() {
        return new nn3(this, toArray());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof im3) || !isHashCodeFast() || !((im3) obj).isHashCodeFast() || hashCode() == obj.hashCode()) {
            return yn3.a((Set<?>) this, obj);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return yn3.a((Set<?>) this);
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    public abstract jo3<E> iterator();

    @DexIgnore
    public Object writeReplace() {
        return new c(toArray());
    }

    @DexIgnore
    public static <E> im3<E> of(E e) {
        return new bo3(e);
    }

    @DexIgnore
    public static <E> im3<E> of(E e, E e2) {
        return a(2, e, e2);
    }

    @DexIgnore
    public static <E> im3<E> of(E e, E e2, E e3) {
        return a(3, e, e2, e3);
    }

    @DexIgnore
    public static <E> im3<E> of(E e, E e2, E e3, E e4) {
        return a(4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E> im3<E> of(E e, E e2, E e3, E e4, E e5) {
        return a(5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    @SafeVarargs
    public static <E> im3<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        Object[] objArr = new Object[(eArr.length + 6)];
        objArr[0] = e;
        objArr[1] = e2;
        objArr[2] = e3;
        objArr[3] = e4;
        objArr[4] = e5;
        objArr[5] = e6;
        System.arraycopy(eArr, 0, objArr, 6, eArr.length);
        return a(objArr.length, objArr);
    }

    @DexIgnore
    public static <E> im3<E> copyOf(Iterable<? extends E> iterable) {
        return iterable instanceof Collection ? copyOf((Collection) iterable) : copyOf(iterable.iterator());
    }

    @DexIgnore
    public static <E> im3<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        return new a().a(next).a(it).a();
    }

    @DexIgnore
    public static <E> im3<E> copyOf(E[] eArr) {
        int length = eArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return a(eArr.length, (Object[]) eArr.clone());
        }
        return of(eArr[0]);
    }

    @DexIgnore
    public static im3 a(EnumSet enumSet) {
        return yl3.asImmutable(EnumSet.copyOf(enumSet));
    }
}
