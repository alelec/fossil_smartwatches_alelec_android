package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.vk4;
import com.portfolio.platform.data.NetworkState;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements vk4.a {
        @DexIgnore
        public /* final */ /* synthetic */ MutableLiveData a;

        @DexIgnore
        public a(MutableLiveData mutableLiveData) {
            this.a = mutableLiveData;
        }

        @DexIgnore
        public final void a(vk4.g gVar) {
            wg6.b(gVar, "report");
            if (gVar.b()) {
                this.a.a(NetworkState.Companion.getLOADING());
            } else if (gVar.a()) {
                this.a.a(NetworkState.Companion.error(wk4.b(gVar)));
            } else {
                this.a.a(NetworkState.Companion.getLOADED());
            }
        }
    }

    @DexIgnore
    public static final String b(vk4.g gVar) {
        vk4.d[] values = vk4.d.values();
        ArrayList arrayList = new ArrayList();
        for (vk4.d a2 : values) {
            Throwable a3 = gVar.a(a2);
            String message = a3 != null ? a3.getMessage() : null;
            if (message != null) {
                arrayList.add(message);
            }
        }
        return arrayList.isEmpty() ^ true ? (String) yd6.d(arrayList) : "";
    }

    @DexIgnore
    public static final LiveData<NetworkState> a(vk4 vk4) {
        wg6.b(vk4, "$this$createStatusLiveData");
        MutableLiveData mutableLiveData = new MutableLiveData();
        vk4.a((vk4.a) new a(mutableLiveData));
        return mutableLiveData;
    }
}
