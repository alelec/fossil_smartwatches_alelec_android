package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.Surface;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.eh;
import com.fossil.vg;
import java.util.concurrent.locks.ReentrantReadWriteLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tg extends ug {
    @DexIgnore
    public static BitmapFactory.Options m; // = new BitmapFactory.Options();
    @DexIgnore
    public eh d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public long k;
    @DexIgnore
    public boolean l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[Bitmap.Config.ALPHA_8.ordinal()] = 1;
            a[Bitmap.Config.ARGB_8888.ordinal()] = 2;
            a[Bitmap.Config.RGB_565.ordinal()] = 3;
            try {
                a[Bitmap.Config.ARGB_4444.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public enum b {
        MIPMAP_NONE(0),
        MIPMAP_FULL(1),
        MIPMAP_ON_SYNC_TO_TEXTURE(2);
        
        @DexIgnore
        public int mID;

        @DexIgnore
        public b(int i) {
            this.mID = i;
        }
    }

    /*
    static {
        m.inScaled = false;
    }
    */

    @DexIgnore
    public tg(long j2, RenderScript renderScript, eh ehVar, int i2) {
        super(j2, renderScript);
        eh.b bVar = eh.b.POSITIVE_X;
        if ((i2 & -228) != 0) {
            throw new yg("Unknown usage specified.");
        } else if ((i2 & 32) == 0 || (i2 & -36) == 0) {
            this.d = ehVar;
            this.e = i2;
            this.k = 0;
            this.l = false;
            if (ehVar != null) {
                this.f = this.d.f() * this.d.g().e();
                a(ehVar);
            }
            if (RenderScript.x) {
                try {
                    RenderScript.z.invoke(RenderScript.y, new Object[]{Integer.valueOf(this.f)});
                } catch (Exception e2) {
                    Log.e("RenderScript_jni", "Couldn't invoke registerNativeAllocation:" + e2);
                    throw new ah("Couldn't invoke registerNativeAllocation:" + e2);
                }
            }
        } else {
            throw new yg("Invalid usage combination.");
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.k = j2;
    }

    @DexIgnore
    public void b(Bitmap bitmap) {
        this.c.j();
        d(bitmap);
        e(bitmap);
        RenderScript renderScript = this.c;
        renderScript.b(a(renderScript), bitmap);
    }

    @DexIgnore
    public final void c(Bitmap bitmap) {
    }

    @DexIgnore
    public final void d(Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config != null) {
            int i2 = a.a[config.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            if (this.d.g().f != vg.b.PIXEL_RGBA || this.d.g().e() != 2) {
                                throw new yg("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
                            }
                        }
                    } else if (this.d.g().f != vg.b.PIXEL_RGB || this.d.g().e() != 2) {
                        throw new yg("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
                    }
                } else if (this.d.g().f != vg.b.PIXEL_RGBA || this.d.g().e() != 4) {
                    throw new yg("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
                }
            } else if (this.d.g().f != vg.b.PIXEL_A) {
                throw new yg("Allocation kind is " + this.d.g().f + ", type " + this.d.g().e + " of " + this.d.g().e() + " bytes, passed bitmap was " + config);
            }
        } else {
            throw new yg("Bitmap has an unsupported format for this operation");
        }
    }

    @DexIgnore
    public eh e() {
        return this.d;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        if (RenderScript.x) {
            RenderScript.A.invoke(RenderScript.y, new Object[]{Integer.valueOf(this.f)});
        }
        super.finalize();
    }

    @DexIgnore
    public final void a(eh ehVar) {
        this.g = ehVar.h();
        this.h = ehVar.i();
        this.i = ehVar.j();
        this.j = this.g;
        int i2 = this.h;
        if (i2 > 1) {
            this.j *= i2;
        }
        int i3 = this.i;
        if (i3 > 1) {
            this.j *= i3;
        }
    }

    @DexIgnore
    public final void e(Bitmap bitmap) {
        if (this.g != bitmap.getWidth() || this.h != bitmap.getHeight()) {
            throw new yg("Cannot update allocation from bitmap, sizes mismatch");
        }
    }

    @DexIgnore
    public void b() {
        if (this.k != 0) {
            boolean z = false;
            synchronized (this) {
                if (!this.l) {
                    this.l = true;
                    z = true;
                }
            }
            if (z) {
                ReentrantReadWriteLock.ReadLock readLock = this.c.k.readLock();
                readLock.lock();
                if (this.c.c()) {
                    this.c.a(this.k);
                }
                readLock.unlock();
                this.k = 0;
            }
        }
        if ((this.e & 96) != 0) {
            a((Surface) null);
        }
        super.b();
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        this.c.j();
        if (bitmap.getConfig() == null) {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            a(createBitmap);
            return;
        }
        e(bitmap);
        d(bitmap);
        RenderScript renderScript = this.c;
        renderScript.a(a(renderScript), bitmap);
    }

    @DexIgnore
    public static tg a(RenderScript renderScript, eh ehVar, b bVar, int i2) {
        renderScript.j();
        if (ehVar.a(renderScript) == 0) {
            throw new zg("Bad Type");
        } else if (renderScript.i() || (i2 & 32) == 0) {
            long a2 = renderScript.a(ehVar.a(renderScript), bVar.mID, i2, 0);
            if (a2 != 0) {
                return new tg(a2, renderScript, ehVar, i2);
            }
            throw new ah("Allocation creation failed.");
        } else {
            throw new ah("USAGE_IO not supported, Allocation creation failed.");
        }
    }

    @DexIgnore
    public static tg a(RenderScript renderScript, eh ehVar) {
        return a(renderScript, ehVar, b.MIPMAP_NONE, 1);
    }

    @DexIgnore
    public static vg a(RenderScript renderScript, Bitmap bitmap) {
        Bitmap.Config config = bitmap.getConfig();
        if (config == Bitmap.Config.ALPHA_8) {
            return vg.c(renderScript);
        }
        if (config == Bitmap.Config.ARGB_4444) {
            return vg.d(renderScript);
        }
        if (config == Bitmap.Config.ARGB_8888) {
            return vg.e(renderScript);
        }
        if (config == Bitmap.Config.RGB_565) {
            return vg.f(renderScript);
        }
        throw new zg("Bad bitmap type: " + config);
    }

    @DexIgnore
    public static eh a(RenderScript renderScript, Bitmap bitmap, b bVar) {
        eh.a aVar = new eh.a(renderScript, a(renderScript, bitmap));
        aVar.a(bitmap.getWidth());
        aVar.b(bitmap.getHeight());
        aVar.a(bVar == b.MIPMAP_FULL);
        return aVar.a();
    }

    @DexIgnore
    public static tg a(RenderScript renderScript, Bitmap bitmap, b bVar, int i2) {
        renderScript.j();
        if (bitmap.getConfig() != null) {
            eh a2 = a(renderScript, bitmap, bVar);
            if (bVar == b.MIPMAP_NONE && a2.g().a(vg.e(renderScript)) && i2 == 131) {
                long a3 = renderScript.a(a2.a(renderScript), bVar.mID, bitmap, i2);
                if (a3 != 0) {
                    tg tgVar = new tg(a3, renderScript, a2, i2);
                    tgVar.c(bitmap);
                    return tgVar;
                }
                throw new ah("Load failed.");
            }
            long b2 = renderScript.b(a2.a(renderScript), bVar.mID, bitmap, i2);
            if (b2 != 0) {
                return new tg(b2, renderScript, a2, i2);
            }
            throw new ah("Load failed.");
        } else if ((i2 & 128) == 0) {
            Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            new Canvas(createBitmap).drawBitmap(bitmap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (Paint) null);
            return a(renderScript, createBitmap, bVar, i2);
        } else {
            throw new yg("USAGE_SHARED cannot be used with a Bitmap that has a null config.");
        }
    }

    @DexIgnore
    public void a(Surface surface) {
        this.c.j();
        if ((this.e & 64) != 0) {
            RenderScript renderScript = this.c;
            renderScript.a(a(renderScript), surface);
            return;
        }
        throw new zg("Allocation is not USAGE_IO_OUTPUT.");
    }
}
