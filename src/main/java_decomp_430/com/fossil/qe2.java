package com.fossil;

import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.wv1;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qe2<T extends IInterface> extends i12<T> {
    @DexIgnore
    public qe2(Context context, Looper looper, je2 je2, wv1.b bVar, wv1.c cVar, e12 e12) {
        super(context, looper, je2.zzc(), e12, bVar, cVar);
    }

    @DexIgnore
    public boolean C() {
        return true;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return w82.a(set);
    }

    @DexIgnore
    public Set<Scope> e() {
        return x();
    }

    @DexIgnore
    public boolean m() {
        return !o42.b(u());
    }
}
