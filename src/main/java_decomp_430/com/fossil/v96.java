package com.fossil;

import com.fossil.p96;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v96<Params, Progress, Result> extends p96<Params, Progress, Result> implements r96<ba6>, y96, ba6, q96 {
    @DexIgnore
    public /* final */ z96 r; // = new z96();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Result> implements Executor {
        @DexIgnore
        public /* final */ Executor a;
        @DexIgnore
        public /* final */ v96 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v96$a$a")
        /* renamed from: com.fossil.v96$a$a  reason: collision with other inner class name */
        public class C0044a extends x96<Result> {
            @DexIgnore
            public C0044a(Runnable runnable, Object obj) {
                super(runnable, obj);
            }

            @DexIgnore
            public <T extends r96<ba6> & y96 & ba6> T d() {
                return a.this.b;
            }
        }

        @DexIgnore
        public a(Executor executor, v96 v96) {
            this.a = executor;
            this.b = v96;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.a.execute(new C0044a(runnable, (Object) null));
        }
    }

    @DexIgnore
    public boolean b() {
        return ((r96) ((y96) g())).b();
    }

    @DexIgnore
    public Collection<ba6> c() {
        return ((r96) ((y96) g())).c();
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return u96.compareTo(this, obj);
    }

    @DexIgnore
    public <T extends r96<ba6> & y96 & ba6> T g() {
        return this.r;
    }

    @DexIgnore
    public final void a(ExecutorService executorService, Params... paramsArr) {
        super.a((Executor) new a(executorService, this), paramsArr);
    }

    @DexIgnore
    public void a(ba6 ba6) {
        if (d() == p96.h.PENDING) {
            ((r96) ((y96) g())).a(ba6);
            return;
        }
        throw new IllegalStateException("Must not add Dependency after task is running");
    }

    @DexIgnore
    public void a(boolean z) {
        ((ba6) ((y96) g())).a(z);
    }

    @DexIgnore
    public boolean a() {
        return ((ba6) ((y96) g())).a();
    }

    @DexIgnore
    public void a(Throwable th) {
        ((ba6) ((y96) g())).a(th);
    }
}
