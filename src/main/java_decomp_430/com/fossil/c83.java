package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class c83 implements Runnable {
    @DexIgnore
    public /* final */ d83 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Exception c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ Map e;

    @DexIgnore
    public c83(d83 d83, int i, Exception exc, byte[] bArr, Map map) {
        this.a = d83;
        this.b = i;
        this.c = exc;
        this.d = bArr;
        this.e = map;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c, this.d, this.e);
    }
}
