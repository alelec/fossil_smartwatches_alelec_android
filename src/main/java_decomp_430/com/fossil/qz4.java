package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz4 extends RecyclerView.g<c> {
    @DexIgnore
    public ArrayList<QuickResponseMessage> a;
    @DexIgnore
    public b b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(QuickResponseMessage quickResponseMessage);

        @DexIgnore
        void a1();

        @DexIgnore
        void h(int i, String str);
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public qz4(ArrayList<QuickResponseMessage> arrayList, b bVar, int i) {
        wg6.b(arrayList, "mData");
        wg6.b(bVar, "mListener");
        this.a = arrayList;
        this.b = bVar;
        this.c = i;
    }

    @DexIgnore
    public final void a(List<QuickResponseMessage> list) {
        wg6.b(list, "data");
        this.a.clear();
        for (QuickResponseMessage quickResponseMessage : list) {
            QuickResponseMessage quickResponseMessage2 = new QuickResponseMessage(quickResponseMessage.getResponse());
            quickResponseMessage2.setId(quickResponseMessage.getId());
            this.a.add(quickResponseMessage2);
        }
        notifyDataSetChanged();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "notifyDataSetChanged " + this.a);
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final b d() {
        return this.b;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public RTLImageView a;
        @DexIgnore
        public FlexibleEditText b;
        @DexIgnore
        public ConstraintLayout c;
        @DexIgnore
        public View d;
        @DexIgnore
        public QuickResponseMessage e;
        @DexIgnore
        public RTLImageView f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public /* final */ /* synthetic */ qz4 j;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements TextWatcher {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
            /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
            /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
            /* JADX WARNING: type inference failed for: r3v13, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
            /* JADX WARNING: type inference failed for: r3v17, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
            public void afterTextChanged(Editable editable) {
                if (this.a.b.isEnabled()) {
                    int length = String.valueOf(editable).length();
                    if (length >= this.a.j.c()) {
                        this.a.g = true;
                        if (!TextUtils.isEmpty(this.a.h)) {
                            this.a.b.setBackgroundResource(2131230882);
                            this.a.b.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.a.h)));
                            return;
                        }
                        this.a.b.setBackgroundResource(2131230882);
                        this.a.b.setBackgroundTintList(ColorStateList.valueOf(-65536));
                    } else if (length != 0) {
                        this.a.a();
                    } else {
                        this.a.b.setHint(jm4.a((Context) PortfolioApp.get.instance(), 2131886126));
                    }
                }
            }

            @DexIgnore
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (this.a.b.hasFocus()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onTextChanged id ");
                    QuickResponseMessage b = this.a.e;
                    sb.append(b != null ? Integer.valueOf(b.getId()) : null);
                    sb.append(" message ");
                    QuickResponseMessage b2 = this.a.e;
                    sb.append(b2 != null ? b2.getResponse() : null);
                    local.d("QuickResponseAdapter", sb.toString());
                    b d = this.a.j.d();
                    QuickResponseMessage b3 = this.a.e;
                    Integer valueOf = b3 != null ? Integer.valueOf(b3.getId()) : null;
                    if (valueOf != null) {
                        d.h(valueOf.intValue(), String.valueOf(charSequence));
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnFocusChangeListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
            public final void onFocusChange(View view, boolean z) {
                if (!z) {
                    this.a.a();
                    this.a.b.setEnabled(false);
                    this.a.j.d().a1();
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v17, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r5v18, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r5v19, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r5v20, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r4v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qz4 qz4, View view) {
            super(view);
            wg6.b(view, "view");
            this.j = qz4;
            View findViewById = view.findViewById(2131362618);
            if (findViewById != null) {
                this.a = (RTLImageView) findViewById;
                View findViewById2 = view.findViewById(2131362239);
                if (findViewById2 != null) {
                    this.b = (FlexibleEditText) findViewById2;
                    ConstraintLayout findViewById3 = view.findViewById(2131362118);
                    if (findViewById3 != null) {
                        this.c = findViewById3;
                        View findViewById4 = view.findViewById(2131362653);
                        if (findViewById4 != null) {
                            this.d = findViewById4;
                            View findViewById5 = view.findViewById(2131361936);
                            wg6.a((Object) findViewById5, "view.findViewById(R.id.bt_edit)");
                            this.f = (RTLImageView) findViewById5;
                            this.h = ThemeManager.l.a().b(Constants.YO_ERROR_POST);
                            this.i = ThemeManager.l.a().b("nonBrandSurface");
                            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
                            if (!TextUtils.isEmpty(b2)) {
                                this.c.setBackgroundColor(Color.parseColor(b2));
                            }
                            if (!TextUtils.isEmpty(b3)) {
                                this.d.setBackgroundColor(Color.parseColor(b3));
                            }
                            this.b.setEnabled(false);
                            this.a.setOnClickListener(this);
                            this.f.setOnClickListener(this);
                            this.b.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(qz4.c())});
                            this.b.addTextChangedListener(new a(this));
                            this.b.setOnFocusChangeListener(new b(this));
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type android.view.View");
                    }
                    throw new rc6("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout");
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleEditText");
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.view.RTLImageView");
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public void onClick(View view) {
            Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
            if (valueOf != null && valueOf.intValue() == 2131362618) {
                QuickResponseMessage quickResponseMessage = this.e;
                if (quickResponseMessage != null) {
                    this.j.d().a(quickResponseMessage);
                }
            } else if (valueOf != null && valueOf.intValue() == 2131361936) {
                this.b.setEnabled(true);
                this.b.requestFocus();
                this.b.a();
                AppCompatEditText appCompatEditText = this.b;
                Editable text = appCompatEditText.getText();
                Integer valueOf2 = text != null ? Integer.valueOf(text.length()) : null;
                if (valueOf2 != null) {
                    appCompatEditText.setSelection(valueOf2.intValue());
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void a() {
            if (this.g) {
                if (!TextUtils.isEmpty(this.i)) {
                    this.b.setBackground((Drawable) null);
                    this.b.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.i)));
                } else {
                    this.b.setBackground((Drawable) null);
                    this.b.setBackgroundTintList(ColorStateList.valueOf(-1));
                }
                this.g = false;
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void a(QuickResponseMessage quickResponseMessage) {
            wg6.b(quickResponseMessage, "response");
            this.e = quickResponseMessage;
            if (quickResponseMessage.getResponse().length() > 0) {
                this.b.setText(quickResponseMessage.getResponse());
                quickResponseMessage.getResponse();
                return;
            }
            this.b.setHint(jm4.a((Context) PortfolioApp.get.instance(), 2131886126));
        }
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558675, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026_response, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "holder");
        QuickResponseMessage quickResponseMessage = this.a.get(i);
        wg6.a((Object) quickResponseMessage, "mData[position]");
        cVar.a(quickResponseMessage);
    }
}
