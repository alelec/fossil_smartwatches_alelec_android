package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw2 implements Parcelable.Creator<LocationAvailability> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        long j = 0;
        qw2[] qw2Arr = null;
        int i = 1000;
        int i2 = 1;
        int i3 = 1;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i2 = f22.q(parcel, a);
            } else if (a2 == 2) {
                i3 = f22.q(parcel, a);
            } else if (a2 == 3) {
                j = f22.s(parcel, a);
            } else if (a2 == 4) {
                i = f22.q(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                qw2Arr = f22.b(parcel, a, qw2.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new LocationAvailability(i, i2, i3, j, qw2Arr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationAvailability[i];
    }
}
