package com.fossil;

import com.google.gson.Gson;
import com.portfolio.platform.data.model.ServerError;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kp4<T> implements dx6<T> {
    @DexIgnore
    public abstract void a(Call<T> call, rx6<T> rx6);

    @DexIgnore
    public abstract void a(Call<T> call, ServerError serverError);

    @DexIgnore
    public abstract void a(Call<T> call, Throwable th);

    @DexIgnore
    public void onFailure(Call<T> call, Throwable th) {
        wg6.b(call, "call");
        wg6.b(th, "t");
        a(call, th);
    }

    @DexIgnore
    public void onResponse(Call<T> call, rx6<T> rx6) {
        String str;
        String str2;
        String str3;
        wg6.b(call, "call");
        wg6.b(rx6, "response");
        if (rx6.d()) {
            a(call, rx6);
            return;
        }
        int b = rx6.b();
        if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            zq6 c = rx6.c();
            if (c == null || (str = c.string()) == null) {
                str = rx6.e();
            }
            serverError.setMessage(str);
            a(call, serverError);
            return;
        }
        try {
            Gson gson = new Gson();
            zq6 c2 = rx6.c();
            if (c2 == null || (str3 = c2.string()) == null) {
                str3 = rx6.e();
            }
            ServerError serverError2 = (ServerError) gson.a(str3, ServerError.class);
            wg6.a((Object) serverError2, "serverError");
            a(call, serverError2);
        } catch (Exception unused) {
            ServerError serverError3 = new ServerError();
            serverError3.setCode(Integer.valueOf(b));
            zq6 c3 = rx6.c();
            if (c3 == null || (str2 = c3.string()) == null) {
                str2 = rx6.e();
            }
            serverError3.setMessage(str2);
            a(call, serverError3);
        }
    }
}
