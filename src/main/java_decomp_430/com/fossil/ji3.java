package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.Transition;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ji3 extends Transition {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ TextView a;

        @DexIgnore
        public a(ji3 ji3, TextView textView) {
            this.a = textView;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            this.a.setScaleX(floatValue);
            this.a.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public void a(tj tjVar) {
        d(tjVar);
    }

    @DexIgnore
    public void c(tj tjVar) {
        d(tjVar);
    }

    @DexIgnore
    public final void d(tj tjVar) {
        View view = tjVar.b;
        if (view instanceof TextView) {
            tjVar.a.put("android:textscale:scale", Float.valueOf(((TextView) view).getScaleX()));
        }
    }

    @DexIgnore
    public Animator a(ViewGroup viewGroup, tj tjVar, tj tjVar2) {
        if (tjVar == null || tjVar2 == null || !(tjVar.b instanceof TextView)) {
            return null;
        }
        View view = tjVar2.b;
        if (!(view instanceof TextView)) {
            return null;
        }
        TextView textView = (TextView) view;
        Map<String, Object> map = tjVar.a;
        Map<String, Object> map2 = tjVar2.a;
        float f = 1.0f;
        float floatValue = map.get("android:textscale:scale") != null ? ((Float) map.get("android:textscale:scale")).floatValue() : 1.0f;
        if (map2.get("android:textscale:scale") != null) {
            f = ((Float) map2.get("android:textscale:scale")).floatValue();
        }
        if (floatValue == f) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{floatValue, f});
        ofFloat.addUpdateListener(new a(this, textView));
        return ofFloat;
    }
}
