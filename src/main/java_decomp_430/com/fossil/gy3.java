package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gy3 {
    @DexIgnore
    public static /* final */ gy3 b; // = new ey3((gy3) null, 0, 0);
    @DexIgnore
    public /* final */ gy3 a;

    @DexIgnore
    public gy3(gy3 gy3) {
        this.a = gy3;
    }

    @DexIgnore
    public final gy3 a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(hy3 hy3, byte[] bArr);

    @DexIgnore
    public final gy3 b(int i, int i2) {
        return new by3(this, i, i2);
    }

    @DexIgnore
    public final gy3 a(int i, int i2) {
        return new ey3(this, i, i2);
    }
}
