package com.fossil;

import com.fossil.dr;
import com.fossil.dt;
import com.fossil.jv;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct<Transcode> {
    @DexIgnore
    public /* final */ List<jv.a<?>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<vr> b; // = new ArrayList();
    @DexIgnore
    public yq c;
    @DexIgnore
    public Object d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Class<?> g;
    @DexIgnore
    public dt.e h;
    @DexIgnore
    public xr i;
    @DexIgnore
    public Map<Class<?>, bs<?>> j;
    @DexIgnore
    public Class<Transcode> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public vr n;
    @DexIgnore
    public br o;
    @DexIgnore
    public ft p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore
    public <R> void a(yq yqVar, Object obj, vr vrVar, int i2, int i3, ft ftVar, Class<?> cls, Class<R> cls2, br brVar, xr xrVar, Map<Class<?>, bs<?>> map, boolean z, boolean z2, dt.e eVar) {
        this.c = yqVar;
        this.d = obj;
        this.n = vrVar;
        this.e = i2;
        this.f = i3;
        this.p = ftVar;
        this.g = cls;
        this.h = eVar;
        this.k = cls2;
        this.o = brVar;
        this.i = xrVar;
        this.j = map;
        this.q = z;
        this.r = z2;
    }

    @DexIgnore
    public xt b() {
        return this.c.a();
    }

    @DexIgnore
    public boolean c(Class<?> cls) {
        return a(cls) != null;
    }

    @DexIgnore
    public ku d() {
        return this.h.a();
    }

    @DexIgnore
    public ft e() {
        return this.p;
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public List<jv.a<?>> g() {
        if (!this.l) {
            this.l = true;
            this.a.clear();
            List a2 = this.c.f().a(this.d);
            int size = a2.size();
            for (int i2 = 0; i2 < size; i2++) {
                jv.a a3 = ((jv) a2.get(i2)).a(this.d, this.e, this.f, this.i);
                if (a3 != null) {
                    this.a.add(a3);
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public Class<?> h() {
        return this.d.getClass();
    }

    @DexIgnore
    public xr i() {
        return this.i;
    }

    @DexIgnore
    public br j() {
        return this.o;
    }

    @DexIgnore
    public List<Class<?>> k() {
        return this.c.f().c(this.d.getClass(), this.g, this.k);
    }

    @DexIgnore
    public vr l() {
        return this.n;
    }

    @DexIgnore
    public Class<?> m() {
        return this.k;
    }

    @DexIgnore
    public int n() {
        return this.e;
    }

    @DexIgnore
    public boolean o() {
        return this.r;
    }

    @DexIgnore
    public <Z> bs<Z> b(Class<Z> cls) {
        bs<Z> bsVar = this.j.get(cls);
        if (bsVar == null) {
            Iterator<Map.Entry<Class<?>, bs<?>>> it = this.j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    bsVar = (bs) next.getValue();
                    break;
                }
            }
        }
        if (bsVar != null) {
            return bsVar;
        }
        if (!this.j.isEmpty() || !this.q) {
            return cw.a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    public List<vr> c() {
        if (!this.m) {
            this.m = true;
            this.b.clear();
            List<jv.a<?>> g2 = g();
            int size = g2.size();
            for (int i2 = 0; i2 < size; i2++) {
                jv.a aVar = g2.get(i2);
                if (!this.b.contains(aVar.a)) {
                    this.b.add(aVar.a);
                }
                for (int i3 = 0; i3 < aVar.b.size(); i3++) {
                    if (!this.b.contains(aVar.b.get(i3))) {
                        this.b.add(aVar.b.get(i3));
                    }
                }
            }
        }
        return this.b;
    }

    @DexIgnore
    public boolean b(rt<?> rtVar) {
        return this.c.f().b(rtVar);
    }

    @DexIgnore
    public void a() {
        this.c = null;
        this.d = null;
        this.n = null;
        this.g = null;
        this.k = null;
        this.i = null;
        this.o = null;
        this.j = null;
        this.p = null;
        this.a.clear();
        this.l = false;
        this.b.clear();
        this.m = false;
    }

    @DexIgnore
    public <Data> pt<Data, ?, Transcode> a(Class<Data> cls) {
        return this.c.f().b(cls, this.g, this.k);
    }

    @DexIgnore
    public <Z> as<Z> a(rt<Z> rtVar) {
        return this.c.f().a(rtVar);
    }

    @DexIgnore
    public List<jv<File, ?>> a(File file) throws dr.c {
        return this.c.f().a(file);
    }

    @DexIgnore
    public boolean a(vr vrVar) {
        List<jv.a<?>> g2 = g();
        int size = g2.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (g2.get(i2).a.equals(vrVar)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public <X> sr<X> a(X x) throws dr.e {
        return this.c.f().c(x);
    }
}
