package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl4 {
    @DexIgnore
    public static /* final */ bl4 a; // = new bl4();

    @DexIgnore
    public final String a(Float f) {
        String b = tk4.b(f != null ? f.floatValue() : 0.0f, 0);
        wg6.a((Object) b, "NumberHelper.decimalForm\u2026Number(calories ?: 0F, 0)");
        return b;
    }

    @DexIgnore
    public final String b(Integer num) {
        String b = tk4.b(num != null ? (float) num.intValue() : 0.0f, 2);
        wg6.a((Object) b, "NumberHelper.decimalForm\u2026teps?.toFloat() ?: 0F, 2)");
        return b;
    }

    @DexIgnore
    public final String a(Integer num) {
        String b = tk4.b(num != null ? (float) num.intValue() : 0.0f, 0);
        wg6.a((Object) b, "NumberHelper.decimalForm\u2026Time?.toFloat() ?: 0F, 0)");
        return b;
    }

    @DexIgnore
    public final String a(Float f, zh4 zh4) {
        wg6.b(zh4, Constants.PROFILE_KEY_UNIT);
        float f2 = 0.0f;
        if (zh4 == zh4.IMPERIAL) {
            if (f != null) {
                f2 = f.floatValue();
            }
            String b = tk4.b(zj4.j(f2), 2);
            wg6.a((Object) b, "NumberHelper.decimalForm\u2026Miles(distance ?: 0F), 2)");
            return b;
        }
        if (f != null) {
            f2 = f.floatValue();
        }
        String b2 = tk4.b(zj4.i(f2), 2);
        wg6.a((Object) b2, "NumberHelper.decimalForm\u2026eters(distance ?: 0F), 2)");
        return b2;
    }
}
