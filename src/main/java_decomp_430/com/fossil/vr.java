package com.fossil;

import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vr {
    @DexIgnore
    public static final Charset a = Charset.forName("UTF-8");

    @DexIgnore
    void a(MessageDigest messageDigest);

    @DexIgnore
    boolean equals(Object obj);

    @DexIgnore
    int hashCode();
}
