package com.fossil;

import android.net.Uri;
import com.fossil.am;
import com.fossil.ol;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fo {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[am.a.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[ll.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c; // = new int[ul.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|(2:23|24)|25|27|28|29|30|31|32|33|34|35|36|(3:37|38|40)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|(2:23|24)|25|27|28|29|30|31|32|33|34|35|36|(3:37|38|40)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|21|22|(2:23|24)|25|27|28|29|30|31|32|33|34|35|36|37|38|40) */
        /* JADX WARNING: Can't wrap try/catch for region: R(34:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|24|25|27|28|29|30|31|32|33|34|35|36|37|38|40) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0070 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0084 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x008e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0098 */
        /*
        static {
            try {
                c[ul.NOT_REQUIRED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                c[ul.CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                c[ul.UNMETERED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                c[ul.NOT_ROAMING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                c[ul.METERED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            b[ll.EXPONENTIAL.ordinal()] = 1;
            try {
                b[ll.LINEAR.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            a[am.a.ENQUEUED.ordinal()] = 1;
            a[am.a.RUNNING.ordinal()] = 2;
            a[am.a.SUCCEEDED.ordinal()] = 3;
            a[am.a.FAILED.ordinal()] = 4;
            a[am.a.BLOCKED.ordinal()] = 5;
            try {
                a[am.a.CANCELLED.ordinal()] = 6;
            } catch (NoSuchFieldError unused7) {
            }
        }
        */
    }

    @DexIgnore
    public static int a(am.a aVar) {
        switch (a.a[aVar.ordinal()]) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 4;
            case 6:
                return 5;
            default:
                throw new IllegalArgumentException("Could not convert " + aVar + " to int");
        }
    }

    @DexIgnore
    public static ul b(int i) {
        if (i == 0) {
            return ul.NOT_REQUIRED;
        }
        if (i == 1) {
            return ul.CONNECTED;
        }
        if (i == 2) {
            return ul.UNMETERED;
        }
        if (i == 3) {
            return ul.NOT_ROAMING;
        }
        if (i == 4) {
            return ul.METERED;
        }
        throw new IllegalArgumentException("Could not convert " + i + " to NetworkType");
    }

    @DexIgnore
    public static am.a c(int i) {
        if (i == 0) {
            return am.a.ENQUEUED;
        }
        if (i == 1) {
            return am.a.RUNNING;
        }
        if (i == 2) {
            return am.a.SUCCEEDED;
        }
        if (i == 3) {
            return am.a.FAILED;
        }
        if (i == 4) {
            return am.a.BLOCKED;
        }
        if (i == 5) {
            return am.a.CANCELLED;
        }
        throw new IllegalArgumentException("Could not convert " + i + " to State");
    }

    @DexIgnore
    public static int a(ll llVar) {
        int i = a.b[llVar.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        throw new IllegalArgumentException("Could not convert " + llVar + " to int");
    }

    @DexIgnore
    public static ll a(int i) {
        if (i == 0) {
            return ll.EXPONENTIAL;
        }
        if (i == 1) {
            return ll.LINEAR;
        }
        throw new IllegalArgumentException("Could not convert " + i + " to BackoffPolicy");
    }

    @DexIgnore
    public static int a(ul ulVar) {
        int i = a.c[ulVar.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i == 4) {
            return 3;
        }
        if (i == 5) {
            return 4;
        }
        throw new IllegalArgumentException("Could not convert " + ulVar + " to int");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a A[SYNTHETIC, Splitter:B:26:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0071 A[SYNTHETIC, Splitter:B:37:0x0071] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:30:0x0062=Splitter:B:30:0x0062, B:15:0x0048=Splitter:B:15:0x0048} */
    public static byte[] a(ol olVar) {
        ObjectOutputStream objectOutputStream = null;
        if (olVar.b() == 0) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream2.writeInt(olVar.b());
                for (ol.a next : olVar.a()) {
                    objectOutputStream2.writeUTF(next.a().toString());
                    objectOutputStream2.writeBoolean(next.b());
                }
                try {
                    objectOutputStream2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } catch (IOException e3) {
                e = e3;
                objectOutputStream = objectOutputStream2;
                try {
                    e.printStackTrace();
                    if (objectOutputStream != null) {
                        try {
                            objectOutputStream.close();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    }
                    byteArrayOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                } catch (Throwable th) {
                    th = th;
                    objectOutputStream2 = objectOutputStream;
                    if (objectOutputStream2 != null) {
                        try {
                            objectOutputStream2.close();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (objectOutputStream2 != null) {
                }
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (IOException e7) {
            e = e7;
            e.printStackTrace();
            if (objectOutputStream != null) {
            }
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        }
        return byteArrayOutputStream.toByteArray();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0046 A[SYNTHETIC, Splitter:B:25:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x005a A[SYNTHETIC, Splitter:B:36:0x005a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0033=Splitter:B:15:0x0033, B:29:0x004e=Splitter:B:29:0x004e} */
    public static ol a(byte[] bArr) {
        ObjectInputStream objectInputStream;
        IOException e;
        ol olVar = new ol();
        if (bArr == null) {
            return olVar;
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            objectInputStream = new ObjectInputStream(byteArrayInputStream);
            try {
                for (int readInt = objectInputStream.readInt(); readInt > 0; readInt--) {
                    olVar.a(Uri.parse(objectInputStream.readUTF()), objectInputStream.readBoolean());
                }
                try {
                    objectInputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                try {
                    byteArrayInputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (IOException e4) {
                e = e4;
                try {
                    e.printStackTrace();
                    if (objectInputStream != null) {
                        try {
                            objectInputStream.close();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                    byteArrayInputStream.close();
                    return olVar;
                } catch (Throwable th) {
                    th = th;
                    if (objectInputStream != null) {
                        try {
                            objectInputStream.close();
                        } catch (IOException e6) {
                            e6.printStackTrace();
                        }
                    }
                    try {
                        byteArrayInputStream.close();
                    } catch (IOException e7) {
                        e7.printStackTrace();
                    }
                    throw th;
                }
            }
        } catch (IOException e8) {
            IOException iOException = e8;
            objectInputStream = null;
            e = iOException;
            e.printStackTrace();
            if (objectInputStream != null) {
            }
            byteArrayInputStream.close();
            return olVar;
        } catch (Throwable th2) {
            objectInputStream = null;
            th = th2;
            if (objectInputStream != null) {
            }
            byteArrayInputStream.close();
            throw th;
        }
        return olVar;
    }
}
