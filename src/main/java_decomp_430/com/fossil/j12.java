package com.fossil;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.wv1;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j12 implements Handler.Callback {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ ArrayList<wv1.b> b; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<wv1.b> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<wv1.c> d; // = new ArrayList<>();
    @DexIgnore
    public volatile boolean e; // = false;
    @DexIgnore
    public /* final */ AtomicInteger f; // = new AtomicInteger(0);
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public /* final */ Handler h;
    @DexIgnore
    public /* final */ Object i; // = new Object();

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean c();

        @DexIgnore
        Bundle o();
    }

    @DexIgnore
    public j12(Looper looper, a aVar) {
        this.a = aVar;
        this.h = new bb2(looper, this);
    }

    @DexIgnore
    public final void a() {
        this.e = false;
        this.f.incrementAndGet();
    }

    @DexIgnore
    public final void b() {
        this.e = true;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        int i2 = message.what;
        if (i2 == 1) {
            wv1.b bVar = (wv1.b) message.obj;
            synchronized (this.i) {
                if (this.e && this.a.c() && this.b.contains(bVar)) {
                    bVar.f(this.a.o());
                }
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i2);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }

    @DexIgnore
    public final void b(wv1.c cVar) {
        w12.a(cVar);
        synchronized (this.i) {
            if (!this.d.remove(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        w12.a(this.h, "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.i) {
            boolean z = true;
            w12.b(!this.g);
            this.h.removeMessages(1);
            this.g = true;
            if (this.c.size() != 0) {
                z = false;
            }
            w12.b(z);
            ArrayList arrayList = new ArrayList(this.b);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                wv1.b bVar = (wv1.b) obj;
                if (!this.e || !this.a.c() || this.f.get() != i2) {
                    break;
                } else if (!this.c.contains(bVar)) {
                    bVar.f(bundle);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    @DexIgnore
    public final void a(int i2) {
        w12.a(this.h, "onUnintentionalDisconnection must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            this.g = true;
            ArrayList arrayList = new ArrayList(this.b);
            int i3 = this.f.get();
            int size = arrayList.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList.get(i4);
                i4++;
                wv1.b bVar = (wv1.b) obj;
                if (!this.e || this.f.get() != i3) {
                    break;
                } else if (this.b.contains(bVar)) {
                    bVar.g(i2);
                }
            }
            this.c.clear();
            this.g = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        return;
     */
    @DexIgnore
    public final void a(gv1 gv1) {
        w12.a(this.h, "onConnectionFailure must only be called on the Handler thread");
        this.h.removeMessages(1);
        synchronized (this.i) {
            ArrayList arrayList = new ArrayList(this.d);
            int i2 = this.f.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                wv1.c cVar = (wv1.c) obj;
                if (this.e) {
                    if (this.f.get() == i2) {
                        if (this.d.contains(cVar)) {
                            cVar.a(gv1);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void a(wv1.b bVar) {
        w12.a(bVar);
        synchronized (this.i) {
            if (this.b.contains(bVar)) {
                String valueOf = String.valueOf(bVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.b.add(bVar);
            }
        }
        if (this.a.c()) {
            Handler handler = this.h;
            handler.sendMessage(handler.obtainMessage(1, bVar));
        }
    }

    @DexIgnore
    public final void a(wv1.c cVar) {
        w12.a(cVar);
        synchronized (this.i) {
            if (this.d.contains(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.d.add(cVar);
            }
        }
    }
}
