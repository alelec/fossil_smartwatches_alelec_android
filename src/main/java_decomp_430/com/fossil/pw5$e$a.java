package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.usecase.GetWeather;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.usecase.GetWeather$executeUseCase$1$response$1", f = "GetWeather.kt", l = {31}, m = "invokeSuspend")
public final class pw5$e$a extends sf6 implements hg6<xe6<? super rx6<ku3>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ GetWeather.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pw5$e$a(GetWeather.e eVar, xe6 xe6) {
        super(1, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new pw5$e$a(this.this$0, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((pw5$e$a) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 a2 = this.this$0.this$0.d;
            String valueOf = String.valueOf(this.this$0.$requestValues.a().a);
            String valueOf2 = String.valueOf(this.this$0.$requestValues.a().b);
            String value = this.this$0.$requestValues.b().getValue();
            this.label = 1;
            obj = a2.getWeather(valueOf, valueOf2, value, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
