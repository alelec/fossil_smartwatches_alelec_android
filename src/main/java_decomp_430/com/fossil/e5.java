package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.i5;
import com.fossil.j5;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e5 {
    @DexIgnore
    public static void a(k5 k5Var) {
        if ((k5Var.N() & 32) != 32) {
            b(k5Var);
            return;
        }
        k5Var.D0 = true;
        k5Var.x0 = false;
        k5Var.y0 = false;
        k5Var.z0 = false;
        ArrayList<j5> arrayList = k5Var.k0;
        List<l5> list = k5Var.w0;
        boolean z = k5Var.k() == j5.b.WRAP_CONTENT;
        boolean z2 = k5Var.r() == j5.b.WRAP_CONTENT;
        boolean z3 = z || z2;
        list.clear();
        for (j5 next : arrayList) {
            next.p = null;
            next.d0 = false;
            next.G();
        }
        for (j5 next2 : arrayList) {
            if (next2.p == null && !a(next2, list, z3)) {
                b(k5Var);
                k5Var.D0 = false;
                return;
            }
        }
        int i = 0;
        int i2 = 0;
        for (l5 next3 : list) {
            i = Math.max(i, a(next3, 0));
            i2 = Math.max(i2, a(next3, 1));
        }
        if (z) {
            k5Var.a(j5.b.FIXED);
            k5Var.p(i);
            k5Var.x0 = true;
            k5Var.y0 = true;
            k5Var.A0 = i;
        }
        if (z2) {
            k5Var.b(j5.b.FIXED);
            k5Var.h(i2);
            k5Var.x0 = true;
            k5Var.z0 = true;
            k5Var.B0 = i2;
        }
        a(list, 0, k5Var.t());
        a(list, 1, k5Var.j());
    }

    @DexIgnore
    public static void b(k5 k5Var) {
        k5Var.w0.clear();
        k5Var.w0.add(0, new l5(k5Var.k0));
    }

    @DexIgnore
    public static boolean a(j5 j5Var, List<l5> list, boolean z) {
        l5 l5Var = new l5(new ArrayList(), true);
        list.add(l5Var);
        return a(j5Var, l5Var, list, z);
    }

    @DexIgnore
    public static boolean a(j5 j5Var, l5 l5Var, List<l5> list, boolean z) {
        i5 i5Var;
        i5 i5Var2;
        i5 i5Var3;
        j5 j5Var2;
        i5 i5Var4;
        i5 i5Var5;
        i5 i5Var6;
        i5 i5Var7;
        j5 j5Var3;
        i5 i5Var8;
        if (j5Var == null) {
            return true;
        }
        j5Var.c0 = false;
        k5 k5Var = (k5) j5Var.l();
        l5 l5Var2 = j5Var.p;
        if (l5Var2 == null) {
            j5Var.b0 = true;
            l5Var.a.add(j5Var);
            j5Var.p = l5Var;
            if (j5Var.s.d == null && j5Var.u.d == null && j5Var.t.d == null && j5Var.v.d == null && j5Var.w.d == null && j5Var.z.d == null) {
                a(k5Var, j5Var, l5Var);
                if (z) {
                    return false;
                }
            }
            if (!(j5Var.t.d == null || j5Var.v.d == null)) {
                j5.b r = k5Var.r();
                j5.b bVar = j5.b.WRAP_CONTENT;
                if (z) {
                    a(k5Var, j5Var, l5Var);
                    return false;
                } else if (!(j5Var.t.d.b == j5Var.l() && j5Var.v.d.b == j5Var.l())) {
                    a(k5Var, j5Var, l5Var);
                }
            }
            if (!(j5Var.s.d == null || j5Var.u.d == null)) {
                j5.b k = k5Var.k();
                j5.b bVar2 = j5.b.WRAP_CONTENT;
                if (z) {
                    a(k5Var, j5Var, l5Var);
                    return false;
                } else if (!(j5Var.s.d.b == j5Var.l() && j5Var.u.d.b == j5Var.l())) {
                    a(k5Var, j5Var, l5Var);
                }
            }
            if (((j5Var.k() == j5.b.MATCH_CONSTRAINT) ^ (j5Var.r() == j5.b.MATCH_CONSTRAINT)) && j5Var.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                a(j5Var);
            } else if (j5Var.k() == j5.b.MATCH_CONSTRAINT || j5Var.r() == j5.b.MATCH_CONSTRAINT) {
                a(k5Var, j5Var, l5Var);
                if (z) {
                    return false;
                }
            }
            if (((j5Var.s.d == null && j5Var.u.d == null) || (((i5Var5 = j5Var.s.d) != null && i5Var5.b == j5Var.D && j5Var.u.d == null) || (((i5Var6 = j5Var.u.d) != null && i5Var6.b == j5Var.D && j5Var.s.d == null) || ((i5Var7 = j5Var.s.d) != null && i5Var7.b == (j5Var3 = j5Var.D) && (i5Var8 = j5Var.u.d) != null && i5Var8.b == j5Var3)))) && j5Var.z.d == null && !(j5Var instanceof m5) && !(j5Var instanceof n5)) {
                l5Var.f.add(j5Var);
            }
            if (((j5Var.t.d == null && j5Var.v.d == null) || (((i5Var = j5Var.t.d) != null && i5Var.b == j5Var.D && j5Var.v.d == null) || (((i5Var2 = j5Var.v.d) != null && i5Var2.b == j5Var.D && j5Var.t.d == null) || ((i5Var3 = j5Var.t.d) != null && i5Var3.b == (j5Var2 = j5Var.D) && (i5Var4 = j5Var.v.d) != null && i5Var4.b == j5Var2)))) && j5Var.z.d == null && j5Var.w.d == null && !(j5Var instanceof m5) && !(j5Var instanceof n5)) {
                l5Var.g.add(j5Var);
            }
            if (j5Var instanceof n5) {
                a(k5Var, j5Var, l5Var);
                if (z) {
                    return false;
                }
                n5 n5Var = (n5) j5Var;
                for (int i = 0; i < n5Var.l0; i++) {
                    if (!a(n5Var.k0[i], l5Var, list, z)) {
                        return false;
                    }
                }
            }
            for (i5 i5Var9 : j5Var.A) {
                i5 i5Var10 = i5Var9.d;
                if (!(i5Var10 == null || i5Var10.b == j5Var.l())) {
                    if (i5Var9.c == i5.d.CENTER) {
                        a(k5Var, j5Var, l5Var);
                        if (z) {
                            return false;
                        }
                    } else {
                        a(i5Var9);
                    }
                    if (!a(i5Var9.d.b, l5Var, list, z)) {
                        return false;
                    }
                }
            }
            return true;
        }
        if (l5Var2 != l5Var) {
            l5Var.a.addAll(l5Var2.a);
            l5Var.f.addAll(j5Var.p.f);
            l5Var.g.addAll(j5Var.p.g);
            if (!j5Var.p.d) {
                l5Var.d = false;
            }
            list.remove(j5Var.p);
            for (j5 j5Var4 : j5Var.p.a) {
                j5Var4.p = l5Var;
            }
        }
        return true;
    }

    @DexIgnore
    public static void a(k5 k5Var, j5 j5Var, l5 l5Var) {
        l5Var.d = false;
        k5Var.D0 = false;
        j5Var.b0 = false;
    }

    @DexIgnore
    public static int a(l5 l5Var, int i) {
        int i2 = i * 2;
        List<j5> a = l5Var.a(i);
        int size = a.size();
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            j5 j5Var = a.get(i4);
            i5[] i5VarArr = j5Var.A;
            int i5 = i2 + 1;
            i3 = Math.max(i3, a(j5Var, i, i5VarArr[i5].d == null || !(i5VarArr[i2].d == null || i5VarArr[i5].d == null), 0));
        }
        l5Var.e[i] = i3;
        return i3;
    }

    @DexIgnore
    public static int a(j5 j5Var, int i, boolean z, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int t;
        int i9;
        int i10;
        int i11;
        j5 j5Var2 = j5Var;
        int i12 = i;
        boolean z2 = z;
        int i13 = 0;
        if (!j5Var2.b0) {
            return 0;
        }
        boolean z3 = j5Var2.w.d != null && i12 == 1;
        if (z2) {
            i6 = j5Var.d();
            i5 = j5Var.j() - j5Var.d();
            i4 = i12 * 2;
            i3 = i4 + 1;
        } else {
            i6 = j5Var.j() - j5Var.d();
            i5 = j5Var.d();
            i3 = i12 * 2;
            i4 = i3 + 1;
        }
        i5[] i5VarArr = j5Var2.A;
        if (i5VarArr[i3].d == null || i5VarArr[i4].d != null) {
            i7 = i3;
            i8 = 1;
        } else {
            i7 = i4;
            i4 = i3;
            i8 = -1;
        }
        int i14 = z3 ? i2 - i6 : i2;
        int b = (j5Var2.A[i4].b() * i8) + a(j5Var, i);
        int i15 = i14 + b;
        int t2 = (i12 == 0 ? j5Var.t() : j5Var.j()) * i8;
        Iterator<s5> it = j5Var2.A[i4].d().a.iterator();
        while (it.hasNext()) {
            i13 = Math.max(i13, a(((q5) it.next()).c.b, i12, z2, i15));
        }
        int i16 = 0;
        for (Iterator<s5> it2 = j5Var2.A[i7].d().a.iterator(); it2.hasNext(); it2 = it2) {
            i16 = Math.max(i16, a(((q5) it2.next()).c.b, i12, z2, t2 + i15));
        }
        if (z3) {
            i13 -= i6;
            t = i16 + i5;
        } else {
            t = i16 + ((i12 == 0 ? j5Var.t() : j5Var.j()) * i8);
        }
        int i17 = 1;
        if (i12 == 1) {
            Iterator<s5> it3 = j5Var2.w.d().a.iterator();
            int i18 = 0;
            while (it3.hasNext()) {
                Iterator<s5> it4 = it3;
                q5 q5Var = (q5) it3.next();
                if (i8 == i17) {
                    i18 = Math.max(i18, a(q5Var.c.b, i12, z2, i6 + i15));
                    i11 = i7;
                } else {
                    i11 = i7;
                    i18 = Math.max(i18, a(q5Var.c.b, i12, z2, (i5 * i8) + i15));
                }
                it3 = it4;
                i7 = i11;
                i17 = 1;
            }
            i9 = i7;
            int i19 = i18;
            i10 = (j5Var2.w.d().a.size() <= 0 || z3) ? i19 : i8 == 1 ? i19 + i6 : i19 - i5;
        } else {
            i9 = i7;
            i10 = 0;
        }
        int max = b + Math.max(i13, Math.max(t, i10));
        int i20 = i15 + t2;
        if (i8 != -1) {
            int i21 = i15;
            i15 = i20;
            i20 = i21;
        }
        if (z2) {
            o5.a(j5Var2, i12, i20);
            j5Var2.a(i20, i15, i12);
        } else {
            j5Var2.p.a(j5Var2, i12);
            j5Var2.d(i20, i12);
        }
        if (j5Var.c(i) == j5.b.MATCH_CONSTRAINT && j5Var2.G != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            j5Var2.p.a(j5Var2, i12);
        }
        i5[] i5VarArr2 = j5Var2.A;
        if (!(i5VarArr2[i4].d == null || i5VarArr2[i9].d == null)) {
            j5 l = j5Var.l();
            i5[] i5VarArr3 = j5Var2.A;
            if (i5VarArr3[i4].d.b == l && i5VarArr3[i9].d.b == l) {
                j5Var2.p.a(j5Var2, i12);
            }
        }
        return max;
    }

    @DexIgnore
    public static void a(i5 i5Var) {
        q5 d = i5Var.d();
        i5 i5Var2 = i5Var.d;
        if (i5Var2 != null && i5Var2.d != i5Var) {
            i5Var2.d().a(d);
        }
    }

    @DexIgnore
    public static void a(List<l5> list, int i, int i2) {
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            for (j5 next : list.get(i3).b(i)) {
                if (next.b0) {
                    a(next, i, i2);
                }
            }
        }
    }

    @DexIgnore
    public static void a(j5 j5Var, int i, int i2) {
        int i3 = i * 2;
        i5[] i5VarArr = j5Var.A;
        i5 i5Var = i5VarArr[i3];
        i5 i5Var2 = i5VarArr[i3 + 1];
        if ((i5Var.d == null || i5Var2.d == null) ? false : true) {
            o5.a(j5Var, i, a(j5Var, i) + i5Var.b());
        } else if (j5Var.G == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || j5Var.c(i) != j5.b.MATCH_CONSTRAINT) {
            int e = i2 - j5Var.e(i);
            int d = e - j5Var.d(i);
            j5Var.a(d, e, i);
            o5.a(j5Var, i, d);
        } else {
            int a = a(j5Var);
            int i4 = (int) j5Var.A[i3].d().g;
            i5Var2.d().f = i5Var.d();
            i5Var2.d().g = (float) a;
            i5Var2.d().b = 1;
            j5Var.a(i4, i4 + a, i);
        }
    }

    @DexIgnore
    public static int a(j5 j5Var, int i) {
        j5 j5Var2;
        i5 i5Var;
        int i2 = i * 2;
        i5[] i5VarArr = j5Var.A;
        i5 i5Var2 = i5VarArr[i2];
        i5 i5Var3 = i5VarArr[i2 + 1];
        i5 i5Var4 = i5Var2.d;
        if (i5Var4 == null || i5Var4.b != (j5Var2 = j5Var.D) || (i5Var = i5Var3.d) == null || i5Var.b != j5Var2) {
            return 0;
        }
        return (int) (((float) (((j5Var2.d(i) - i5Var2.b()) - i5Var3.b()) - j5Var.d(i))) * (i == 0 ? j5Var.V : j5Var.W));
    }

    @DexIgnore
    public static int a(j5 j5Var) {
        float f;
        float f2;
        if (j5Var.k() == j5.b.MATCH_CONSTRAINT) {
            if (j5Var.H == 0) {
                f2 = ((float) j5Var.j()) * j5Var.G;
            } else {
                f2 = ((float) j5Var.j()) / j5Var.G;
            }
            int i = (int) f2;
            j5Var.p(i);
            return i;
        } else if (j5Var.r() != j5.b.MATCH_CONSTRAINT) {
            return -1;
        } else {
            if (j5Var.H == 1) {
                f = ((float) j5Var.t()) * j5Var.G;
            } else {
                f = ((float) j5Var.t()) / j5Var.G;
            }
            int i2 = (int) f;
            j5Var.h(i2);
            return i2;
        }
    }
}
