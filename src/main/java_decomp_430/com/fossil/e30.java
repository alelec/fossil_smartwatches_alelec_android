package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e30 implements l20 {
    @DexIgnore
    public static /* final */ List<Class<?>> c; // = Collections.unmodifiableList(Arrays.asList(new Class[]{String.class, String.class, Bundle.class, Long.class}));
    @DexIgnore
    public /* final */ v20 a;
    @DexIgnore
    public Object b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements InvocationHandler {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean a(Object obj, Object obj2) {
            if (obj == obj2) {
                return true;
            }
            if (obj2 == null || !Proxy.isProxyClass(obj2.getClass()) || !super.equals(Proxy.getInvocationHandler(obj2))) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            if (objArr == null) {
                objArr = new Object[0];
            }
            if (objArr.length == 1 && name.equals("equals")) {
                return Boolean.valueOf(a(obj, objArr[0]));
            }
            if (objArr.length == 0 && name.equals("hashCode")) {
                return Integer.valueOf(super.hashCode());
            }
            if (objArr.length == 0 && name.equals("toString")) {
                return super.toString();
            }
            if (objArr.length == 4 && name.equals("onEvent") && e30.a(objArr)) {
                String str = (String) objArr[0];
                String str2 = (String) objArr[1];
                Bundle bundle = (Bundle) objArr[2];
                if (str != null && !str.equals(CrashDumperPlugin.NAME)) {
                    e30.b(e30.this.a, str2, bundle);
                    return null;
                }
            }
            StringBuilder sb = new StringBuilder("Unexpected method invoked on AppMeasurement.EventListener: " + name + "(");
            for (int i = 0; i < objArr.length; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(objArr[i].getClass().getName());
            }
            sb.append("); returning null");
            c86.g().e("CrashlyticsCore", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public e30(v20 v20) {
        this.a = v20;
    }

    @DexIgnore
    public synchronized Object b(Class cls) {
        if (this.b == null) {
            this.b = Proxy.newProxyInstance(this.a.d().getClassLoader(), new Class[]{cls}, new a());
        }
        return this.b;
    }

    @DexIgnore
    public boolean register() {
        Class<?> a2 = a("com.google.android.gms.measurement.AppMeasurement");
        if (a2 == null) {
            c86.g().d("CrashlyticsCore", "Firebase Analytics is not present; you will not see automatic logging of events before a crash occurs.");
            return false;
        }
        Object a3 = a(a2);
        if (a3 == null) {
            c86.g().w("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not create an instance of Firebase Analytics.");
            return false;
        }
        Class<?> a4 = a("com.google.android.gms.measurement.AppMeasurement$OnEventListener");
        if (a4 == null) {
            c86.g().w("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not get class com.google.android.gms.measurement.AppMeasurement$OnEventListener");
            return false;
        }
        try {
            a2.getDeclaredMethod("registerOnMeasurementEventListener", new Class[]{a4}).invoke(a3, new Object[]{b(a4)});
        } catch (NoSuchMethodException e) {
            c86.g().a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Method registerOnMeasurementEventListener not found.", e);
            return false;
        } catch (Exception e2) {
            l86 g = c86.g();
            g.a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: " + e2.getMessage(), e2);
        }
        return true;
    }

    @DexIgnore
    public final Class<?> a(String str) {
        try {
            return this.a.d().getClassLoader().loadClass(str);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final Object a(Class<?> cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke(cls, new Object[]{this.a.d()});
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object[] objArr) {
        if (objArr.length != c.size()) {
            return false;
        }
        Iterator<Class<?>> it = c.iterator();
        for (Object obj : objArr) {
            if (!obj.getClass().equals(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static void b(v20 v20, String str, Bundle bundle) {
        try {
            v20.a("$A$:" + a(str, bundle));
        } catch (JSONException unused) {
            l86 g = c86.g();
            g.w("CrashlyticsCore", "Unable to serialize Firebase Analytics event; " + str);
        }
    }

    @DexIgnore
    public static String a(String str, Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (String str2 : bundle.keySet()) {
            jSONObject2.put(str2, bundle.get(str2));
        }
        jSONObject.put("name", str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }
}
