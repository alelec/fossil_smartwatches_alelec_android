package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy6 implements fx6<zq6, Float> {
    @DexIgnore
    public static /* final */ dy6 a; // = new dy6();

    @DexIgnore
    public Float a(zq6 zq6) throws IOException {
        return Float.valueOf(zq6.string());
    }
}
