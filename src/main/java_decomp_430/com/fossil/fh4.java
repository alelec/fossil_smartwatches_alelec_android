package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fh4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ ImageView E;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fh4(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, RTLImageView rTLImageView5, RTLImageView rTLImageView6, ImageView imageView, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = flexibleTextView4;
        this.v = flexibleTextView5;
        this.w = flexibleTextView6;
        this.x = flexibleTextView7;
        this.y = flexibleTextView8;
        this.z = rTLImageView;
        this.A = rTLImageView2;
        this.B = rTLImageView4;
        this.C = rTLImageView5;
        this.D = rTLImageView6;
        this.E = imageView;
    }

    @DexIgnore
    public static fh4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2) {
        return a(layoutInflater, viewGroup, z2, kb.a());
    }

    @DexIgnore
    @Deprecated
    public static fh4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2, Object obj) {
        return ViewDataBinding.a(layoutInflater, 2131558693, viewGroup, z2, obj);
    }
}
