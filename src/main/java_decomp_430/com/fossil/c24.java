package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c24 implements Factory<z24> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public c24(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static c24 a(b14 b14) {
        return new c24(b14);
    }

    @DexIgnore
    public static z24 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static z24 c(b14 b14) {
        z24 n = b14.n();
        z76.a(n, "Cannot return null from a non-@Nullable @Provides method");
        return n;
    }

    @DexIgnore
    public z24 get() {
        return b(this.a);
    }
}
