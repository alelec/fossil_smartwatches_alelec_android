package com.fossil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zx5 {
    @DexIgnore
    public static /* final */ a a; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T extends Service> void a(Context context, Class<T> cls, ServiceConnection serviceConnection, int i) {
            wg6.b(context, "context");
            wg6.b(cls, Constants.SERVICE);
            wg6.b(serviceConnection, "serviceConnection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "bindButtonServiceConnection() - serviceConnection=" + serviceConnection + ", flags=" + i);
            a(context, new Intent(context, cls), serviceConnection, i);
        }

        @DexIgnore
        public final <T extends Service> void b(Context context, Class<T> cls, ServiceConnection serviceConnection, int i) {
            wg6.b(context, "context");
            wg6.b(cls, Constants.SERVICE);
            wg6.b(serviceConnection, "serviceConnection");
            String str = !TextUtils.isEmpty(PortfolioApp.get.instance().e()) ? com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION : com.misfit.frameworks.buttonservice.utils.Constants.STOP_FOREGROUND_ACTION;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "startAndBindServiceConnection() - serviceConnection=" + serviceConnection + " - service = " + cls + " - flag = " + i + " - action = " + str);
            Intent intent = new Intent(context, cls);
            intent.setAction(str);
            a(context, intent);
            a(context, intent, serviceConnection, i);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, Class cls, String str, int i, Object obj) {
            if ((i & 4) != 0) {
                str = "";
            }
            aVar.a(context, cls, str);
        }

        @DexIgnore
        public final <T extends Service> void a(Context context, Class<T> cls, String str) {
            wg6.b(context, "context");
            wg6.b(cls, Constants.SERVICE);
            wg6.b(str, "foregroundAction");
            String str2 = !TextUtils.isEmpty(PortfolioApp.get.instance().e()) ? com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION : com.misfit.frameworks.buttonservice.utils.Constants.STOP_FOREGROUND_ACTION;
            if (TextUtils.isEmpty(str)) {
                str = str2;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "startForegroundService() - context = " + context + ", service = " + cls + " - foregroundAction = " + str);
            Intent intent = new Intent(context, cls);
            intent.setAction(str);
            a(context, intent);
        }

        @DexIgnore
        public final void a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "bindService - context=" + context + ", intent=" + intent + ", serviceConnection=" + serviceConnection + ", flags=" + i);
            try {
                context.bindService(intent, serviceConnection, i);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "bindService() - e=" + e);
            }
        }

        @DexIgnore
        public final void a(Context context, ServiceConnection serviceConnection) {
            wg6.b(context, "context");
            wg6.b(serviceConnection, "serviceConnection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "unbindService() - serviceConnection = " + serviceConnection);
            try {
                context.unbindService(serviceConnection);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "unbindService() - e=" + e);
            }
        }

        @DexIgnore
        public final void a(Context context, Intent intent) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "startForegroundServiceByIntent - intent=" + intent);
            try {
                w6.a(context, intent);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "startForegroundServiceByIntent() - e=" + e);
            }
        }
    }
}
