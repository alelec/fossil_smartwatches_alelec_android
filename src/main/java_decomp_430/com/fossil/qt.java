package com.fossil;

import com.fossil.s00;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt<Z> implements rt<Z>, s00.f {
    @DexIgnore
    public static /* final */ v8<qt<?>> e; // = s00.a(20, new a());
    @DexIgnore
    public /* final */ u00 a; // = u00.b();
    @DexIgnore
    public rt<Z> b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements s00.d<qt<?>> {
        @DexIgnore
        public qt<?> create() {
            return new qt<>();
        }
    }

    @DexIgnore
    public static <Z> qt<Z> b(rt<Z> rtVar) {
        qt<Z> a2 = e.a();
        q00.a(a2);
        qt<Z> qtVar = a2;
        qtVar.a(rtVar);
        return qtVar;
    }

    @DexIgnore
    public final void a(rt<Z> rtVar) {
        this.d = false;
        this.c = true;
        this.b = rtVar;
    }

    @DexIgnore
    public Class<Z> c() {
        return this.b.c();
    }

    @DexIgnore
    public u00 d() {
        return this.a;
    }

    @DexIgnore
    public final void e() {
        this.b = null;
        e.a(this);
    }

    @DexIgnore
    public synchronized void f() {
        this.a.a();
        if (this.c) {
            this.c = false;
            if (this.d) {
                a();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }

    @DexIgnore
    public Z get() {
        return this.b.get();
    }

    @DexIgnore
    public int b() {
        return this.b.b();
    }

    @DexIgnore
    public synchronized void a() {
        this.a.a();
        this.d = true;
        if (!this.c) {
            this.b.a();
            e();
        }
    }
}
