package com.fossil;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bz4 extends td {
    @DexIgnore
    public MutableLiveData<a> a; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a(int i, boolean z) {
            this.a = i;
            this.b = z;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.b == aVar.b;
        }

        @DexIgnore
        public int hashCode() {
            int a2 = d.a(this.a) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            return a2 + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "NotificationSetting(type=" + this.a + ", isCall=" + this.b + ")";
        }
    }

    @DexIgnore
    public final MutableLiveData<a> a() {
        return this.a;
    }
}
