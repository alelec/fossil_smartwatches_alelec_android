package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum StatusCode {
    SUCCESS(0),
    UNSUPPORT_FILE_FORMAT(1),
    UNSUPPORT_SPECIAL_ENTRY(2),
    INVALID_BINARY_FILE(3),
    INVALID_ENTRY(4),
    INVALID_HEADER(5);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public StatusCode(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
