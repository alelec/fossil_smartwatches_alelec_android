package com.fossil.fitness;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FitnessAlgorithm {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends FitnessAlgorithm {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native FitnessAlgorithm create();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native Result native_parse(long j, ArrayList<BinaryFile> arrayList, UserProfile userProfile);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        public Result parse(ArrayList<BinaryFile> arrayList, UserProfile userProfile) {
            return native_parse(this.nativeRef, arrayList, userProfile);
        }
    }

    @DexIgnore
    public static FitnessAlgorithm create() {
        return CppProxy.create();
    }

    @DexIgnore
    public abstract Result parse(ArrayList<BinaryFile> arrayList, UserProfile userProfile);
}
