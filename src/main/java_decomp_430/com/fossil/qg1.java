package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qg1 extends ok0 {
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ rg1 k;

    @DexIgnore
    public qg1(hm0 hm0, rg1 rg1, at0 at0) {
        super(hm0, at0);
        this.k = rg1;
    }

    @DexIgnore
    public void a(p51 p51) {
        super.a(p51);
        this.j = false;
    }

    @DexIgnore
    public boolean d() {
        return this.d.b == lf0.INTERRUPTED && this.j;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return cw0.a(super.a(z), bm0.CHANNEL_ID, (Object) this.k.a);
    }
}
