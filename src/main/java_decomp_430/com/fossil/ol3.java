package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ol3<K, V> extends pl3 implements Map<K, V> {
    @DexIgnore
    public void clear() {
        delegate().clear();
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return delegate().containsKey(obj);
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return delegate().containsValue(obj);
    }

    @DexIgnore
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    public abstract Map<K, V> delegate();

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        return delegate().entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || delegate().equals(obj);
    }

    @DexIgnore
    public V get(Object obj) {
        return delegate().get(obj);
    }

    @DexIgnore
    public int hashCode() {
        return delegate().hashCode();
    }

    @DexIgnore
    public boolean isEmpty() {
        return delegate().isEmpty();
    }

    @DexIgnore
    public Set<K> keySet() {
        return delegate().keySet();
    }

    @DexIgnore
    public V put(K k, V v) {
        return delegate().put(k, v);
    }

    @DexIgnore
    public void putAll(Map<? extends K, ? extends V> map) {
        delegate().putAll(map);
    }

    @DexIgnore
    public V remove(Object obj) {
        return delegate().remove(obj);
    }

    @DexIgnore
    public int size() {
        return delegate().size();
    }

    @DexIgnore
    public void standardClear() {
        qm3.a((Iterator<?>) entrySet().iterator());
    }

    @DexIgnore
    public boolean standardContainsKey(Object obj) {
        return ym3.a((Map<?, ?>) this, obj);
    }

    @DexIgnore
    public boolean standardContainsValue(Object obj) {
        return ym3.b(this, obj);
    }

    @DexIgnore
    public boolean standardEquals(Object obj) {
        return ym3.c(this, obj);
    }

    @DexIgnore
    public int standardHashCode() {
        return yn3.a((Set<?>) entrySet());
    }

    @DexIgnore
    public boolean standardIsEmpty() {
        return !entrySet().iterator().hasNext();
    }

    @DexIgnore
    public void standardPutAll(Map<? extends K, ? extends V> map) {
        ym3.a(this, map);
    }

    @DexIgnore
    public V standardRemove(Object obj) {
        Iterator it = entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (gk3.a(entry.getKey(), obj)) {
                V value = entry.getValue();
                it.remove();
                return value;
            }
        }
        return null;
    }

    @DexIgnore
    public String standardToString() {
        return ym3.a((Map<?, ?>) this);
    }

    @DexIgnore
    public Collection<V> values() {
        return delegate().values();
    }
}
