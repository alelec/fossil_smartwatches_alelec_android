package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.news.notifications.FossilNotificationBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1", f = "FossilNotificationBar.kt", l = {38}, m = "invokeSuspend")
public final class pn4$a$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.news.notifications.FossilNotificationBar$Companion$updateData$1$1", f = "FossilNotificationBar.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FossilNotificationBar $fossilNotificationBar;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pn4$a$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(pn4$a$a pn4_a_a, FossilNotificationBar fossilNotificationBar, xe6 xe6) {
            super(2, xe6);
            this.this$0 = pn4_a_a;
            this.$fossilNotificationBar = fossilNotificationBar;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$fossilNotificationBar, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                FossilNotificationBar.a.a(FossilNotificationBar.c, this.this$0.$context, this.$fossilNotificationBar, false, 4, (Object) null);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pn4$a$a(Context context, xe6 xe6) {
        super(2, xe6);
        this.$context = context;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        pn4$a$a pn4_a_a = new pn4$a$a(this.$context, xe6);
        pn4_a_a.p$ = (il6) obj;
        return pn4_a_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((pn4$a$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            String d = PortfolioApp.get.instance().d();
            FossilNotificationBar fossilNotificationBar = new FossilNotificationBar(d, (String) null, 2, (qg6) null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "content " + d);
            cn6 c = zl6.c();
            a aVar = new a(this, fossilNotificationBar, (xe6) null);
            this.L$0 = il6;
            this.L$1 = d;
            this.L$2 = fossilNotificationBar;
            this.label = 1;
            if (gk6.a(c, aVar, this) == a2) {
                return a2;
            }
        } else if (i == 1) {
            FossilNotificationBar fossilNotificationBar2 = (FossilNotificationBar) this.L$2;
            String str = (String) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
