package com.fossil;

import java.util.Locale;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qo<V> implements ip3<V> {
    @DexIgnore
    public static /* final */ boolean d; // = Boolean.parseBoolean(System.getProperty("guava.concurrent.generate_cancellation_cause", "false"));
    @DexIgnore
    public static /* final */ Logger e; // = Logger.getLogger(qo.class.getName());
    @DexIgnore
    public static /* final */ b f;
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public volatile Object a;
    @DexIgnore
    public volatile e b;
    @DexIgnore
    public volatile i c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public abstract void a(i iVar, i iVar2);

        @DexIgnore
        public abstract void a(i iVar, Thread thread);

        @DexIgnore
        public abstract boolean a(qo<?> qoVar, e eVar, e eVar2);

        @DexIgnore
        public abstract boolean a(qo<?> qoVar, i iVar, i iVar2);

        @DexIgnore
        public abstract boolean a(qo<?> qoVar, Object obj, Object obj2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ c c;
        @DexIgnore
        public static /* final */ c d;
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ Throwable b;

        /*
        static {
            if (qo.d) {
                d = null;
                c = null;
                return;
            }
            d = new c(false, (Throwable) null);
            c = new c(true, (Throwable) null);
        }
        */

        @DexIgnore
        public c(boolean z, Throwable th) {
            this.a = z;
            this.b = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public static /* final */ d b; // = new d(new a("Failure occurred while trying to finish a future."));
        @DexIgnore
        public /* final */ Throwable a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class a extends Throwable {
            @DexIgnore
            public a(String str) {
                super(str);
            }

            @DexIgnore
            public synchronized Throwable fillInStackTrace() {
                return this;
            }
        }

        @DexIgnore
        public d(Throwable th) {
            qo.d(th);
            this.a = th;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e {
        @DexIgnore
        public static /* final */ e d; // = new e((Runnable) null, (Executor) null);
        @DexIgnore
        public /* final */ Runnable a;
        @DexIgnore
        public /* final */ Executor b;
        @DexIgnore
        public e c;

        @DexIgnore
        public e(Runnable runnable, Executor executor) {
            this.a = runnable;
            this.b = executor;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends b {
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<i, Thread> a;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<i, i> b;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<qo, i> c;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<qo, e> d;
        @DexIgnore
        public /* final */ AtomicReferenceFieldUpdater<qo, Object> e;

        @DexIgnore
        public f(AtomicReferenceFieldUpdater<i, Thread> atomicReferenceFieldUpdater, AtomicReferenceFieldUpdater<i, i> atomicReferenceFieldUpdater2, AtomicReferenceFieldUpdater<qo, i> atomicReferenceFieldUpdater3, AtomicReferenceFieldUpdater<qo, e> atomicReferenceFieldUpdater4, AtomicReferenceFieldUpdater<qo, Object> atomicReferenceFieldUpdater5) {
            super();
            this.a = atomicReferenceFieldUpdater;
            this.b = atomicReferenceFieldUpdater2;
            this.c = atomicReferenceFieldUpdater3;
            this.d = atomicReferenceFieldUpdater4;
            this.e = atomicReferenceFieldUpdater5;
        }

        @DexIgnore
        public void a(i iVar, Thread thread) {
            this.a.lazySet(iVar, thread);
        }

        @DexIgnore
        public void a(i iVar, i iVar2) {
            this.b.lazySet(iVar, iVar2);
        }

        @DexIgnore
        public boolean a(qo<?> qoVar, i iVar, i iVar2) {
            return this.c.compareAndSet(qoVar, iVar, iVar2);
        }

        @DexIgnore
        public boolean a(qo<?> qoVar, e eVar, e eVar2) {
            return this.d.compareAndSet(qoVar, eVar, eVar2);
        }

        @DexIgnore
        public boolean a(qo<?> qoVar, Object obj, Object obj2) {
            return this.e.compareAndSet(qoVar, obj, obj2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<V> implements Runnable {
        @DexIgnore
        public /* final */ qo<V> a;
        @DexIgnore
        public /* final */ ip3<? extends V> b;

        @DexIgnore
        public g(qo<V> qoVar, ip3<? extends V> ip3) {
            this.a = qoVar;
            this.b = ip3;
        }

        @DexIgnore
        public void run() {
            if (this.a.a == this) {
                if (qo.f.a((qo<?>) this.a, (Object) this, qo.b((ip3<?>) this.b))) {
                    qo.a((qo<?>) this.a);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends b {
        @DexIgnore
        public h() {
            super();
        }

        @DexIgnore
        public void a(i iVar, Thread thread) {
            iVar.a = thread;
        }

        @DexIgnore
        public void a(i iVar, i iVar2) {
            iVar.b = iVar2;
        }

        @DexIgnore
        public boolean a(qo<?> qoVar, i iVar, i iVar2) {
            synchronized (qoVar) {
                if (qoVar.c != iVar) {
                    return false;
                }
                qoVar.c = iVar2;
                return true;
            }
        }

        @DexIgnore
        public boolean a(qo<?> qoVar, e eVar, e eVar2) {
            synchronized (qoVar) {
                if (qoVar.b != eVar) {
                    return false;
                }
                qoVar.b = eVar2;
                return true;
            }
        }

        @DexIgnore
        public boolean a(qo<?> qoVar, Object obj, Object obj2) {
            synchronized (qoVar) {
                if (qoVar.a != obj) {
                    return false;
                }
                qoVar.a = obj2;
                return true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public static /* final */ i c; // = new i(false);
        @DexIgnore
        public volatile Thread a;
        @DexIgnore
        public volatile i b;

        @DexIgnore
        public i(boolean z) {
        }

        @DexIgnore
        public void a(i iVar) {
            qo.f.a(this, iVar);
        }

        @DexIgnore
        public i() {
            qo.f.a(this, Thread.currentThread());
        }

        @DexIgnore
        public void a() {
            Thread thread = this.a;
            if (thread != null) {
                this.a = null;
                LockSupport.unpark(thread);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v6, resolved type: com.fossil.qo$f} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: com.fossil.qo$h} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: com.fossil.qo$f} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: com.fossil.qo$f} */
    /* JADX WARNING: Multi-variable type inference failed */
    /*
    static {
        f fVar;
        Class<i> cls = i.class;
        try {
            AtomicReferenceFieldUpdater<U, W> newUpdater = AtomicReferenceFieldUpdater.newUpdater(cls, Thread.class, "a");
            AtomicReferenceFieldUpdater<U, W> newUpdater2 = AtomicReferenceFieldUpdater.newUpdater(cls, cls, "b");
            AtomicReferenceFieldUpdater<U, W> newUpdater3 = AtomicReferenceFieldUpdater.newUpdater(qo.class, cls, "c");
            th = null;
            fVar = new f(newUpdater, newUpdater2, newUpdater3, AtomicReferenceFieldUpdater.newUpdater(qo.class, e.class, "b"), AtomicReferenceFieldUpdater.newUpdater(qo.class, Object.class, "a"));
        } catch (Throwable th) {
            th = th;
            fVar = new h();
        }
        f = fVar;
        Class<LockSupport> cls2 = LockSupport.class;
        if (th != null) {
            e.log(Level.SEVERE, "SafeAtomicHelper is broken!", th);
        }
    }
    */

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final void a(i iVar) {
        iVar.a = null;
        while (true) {
            i iVar2 = this.c;
            if (iVar2 != i.c) {
                i iVar3 = null;
                while (iVar2 != null) {
                    i iVar4 = iVar2.b;
                    if (iVar2.a != null) {
                        iVar3 = iVar2;
                    } else if (iVar3 != null) {
                        iVar3.b = iVar4;
                        if (iVar3.a == null) {
                        }
                    } else if (!f.a((qo<?>) this, iVar2, iVar4)) {
                    }
                    iVar2 = iVar4;
                }
                return;
            }
            return;
        }
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public boolean b(V v) {
        if (v == null) {
            v = g;
        }
        if (!f.a((qo<?>) this, (Object) null, (Object) v)) {
            return false;
        }
        a((qo<?>) this);
        return true;
    }

    @DexIgnore
    public String c() {
        Object obj = this.a;
        if (obj instanceof g) {
            return "setFuture=[" + c(((g) obj).b) + "]";
        } else if (!(this instanceof ScheduledFuture)) {
            return null;
        } else {
            return "remaining delay=[" + ((ScheduledFuture) this).getDelay(TimeUnit.MILLISECONDS) + " ms]";
        }
    }

    @DexIgnore
    public final boolean cancel(boolean z) {
        Object obj = this.a;
        if (!(obj == null) && !(obj instanceof g)) {
            return false;
        }
        c cVar = d ? new c(z, new CancellationException("Future.cancel() was called.")) : z ? c.c : c.d;
        boolean z2 = false;
        Object obj2 = obj;
        qo qoVar = this;
        while (true) {
            if (f.a((qo<?>) qoVar, obj2, (Object) cVar)) {
                if (z) {
                    qoVar.b();
                }
                a((qo<?>) qoVar);
                if (!(obj2 instanceof g)) {
                    return true;
                }
                ip3<? extends V> ip3 = ((g) obj2).b;
                if (ip3 instanceof qo) {
                    qoVar = (qo) ip3;
                    obj2 = qoVar.a;
                    if (!(obj2 == null) && !(obj2 instanceof g)) {
                        return true;
                    }
                    z2 = true;
                } else {
                    ip3.cancel(z);
                    return true;
                }
            } else {
                obj2 = qoVar.a;
                if (!(obj2 instanceof g)) {
                    return z2;
                }
            }
        }
    }

    @DexIgnore
    public final void d() {
        i iVar;
        do {
            iVar = this.c;
        } while (!f.a((qo<?>) this, iVar, i.c));
        while (iVar != null) {
            iVar.a();
            iVar = iVar.b;
        }
    }

    @DexIgnore
    public final V get(long j, TimeUnit timeUnit) throws InterruptedException, TimeoutException, ExecutionException {
        long j2 = j;
        TimeUnit timeUnit2 = timeUnit;
        long nanos = timeUnit2.toNanos(j2);
        if (!Thread.interrupted()) {
            Object obj = this.a;
            if ((obj != null) && (!(obj instanceof g))) {
                return a(obj);
            }
            long nanoTime = nanos > 0 ? System.nanoTime() + nanos : 0;
            if (nanos >= 1000) {
                i iVar = this.c;
                if (iVar != i.c) {
                    i iVar2 = new i();
                    do {
                        iVar2.a(iVar);
                        if (f.a((qo<?>) this, iVar, iVar2)) {
                            do {
                                LockSupport.parkNanos(this, nanos);
                                if (!Thread.interrupted()) {
                                    Object obj2 = this.a;
                                    if ((obj2 != null) && (!(obj2 instanceof g))) {
                                        return a(obj2);
                                    }
                                    nanos = nanoTime - System.nanoTime();
                                } else {
                                    a(iVar2);
                                    throw new InterruptedException();
                                }
                            } while (nanos >= 1000);
                            a(iVar2);
                        } else {
                            iVar = this.c;
                        }
                    } while (iVar != i.c);
                }
                return a(this.a);
            }
            while (nanos > 0) {
                Object obj3 = this.a;
                if ((obj3 != null) && (!(obj3 instanceof g))) {
                    return a(obj3);
                }
                if (!Thread.interrupted()) {
                    nanos = nanoTime - System.nanoTime();
                } else {
                    throw new InterruptedException();
                }
            }
            String qoVar = toString();
            String lowerCase = timeUnit.toString().toLowerCase(Locale.ROOT);
            String str = "Waited " + j2 + " " + timeUnit.toString().toLowerCase(Locale.ROOT);
            if (nanos + 1000 < 0) {
                String str2 = str + " (plus ";
                long j3 = -nanos;
                long convert = timeUnit2.convert(j3, TimeUnit.NANOSECONDS);
                long nanos2 = j3 - timeUnit2.toNanos(convert);
                int i2 = (convert > 0 ? 1 : (convert == 0 ? 0 : -1));
                boolean z = i2 == 0 || nanos2 > 1000;
                if (i2 > 0) {
                    String str3 = str2 + convert + " " + lowerCase;
                    if (z) {
                        str3 = str3 + ",";
                    }
                    str2 = str3 + " ";
                }
                if (z) {
                    str2 = str2 + nanos2 + " nanoseconds ";
                }
                str = str2 + "delay)";
            }
            if (isDone()) {
                throw new TimeoutException(str + " but future completed as timeout expired");
            }
            throw new TimeoutException(str + " for " + qoVar);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    public final boolean isCancelled() {
        return this.a instanceof c;
    }

    @DexIgnore
    public final boolean isDone() {
        Object obj = this.a;
        return (!(obj instanceof g)) & (obj != null);
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[status=");
        if (isCancelled()) {
            sb.append("CANCELLED");
        } else if (isDone()) {
            a(sb);
        } else {
            try {
                str = c();
            } catch (RuntimeException e2) {
                str = "Exception thrown from implementation: " + e2.getClass();
            }
            if (str != null && !str.isEmpty()) {
                sb.append("PENDING, info=[");
                sb.append(str);
                sb.append("]");
            } else if (isDone()) {
                a(sb);
            } else {
                sb.append("PENDING");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public static Object b(ip3<?> ip3) {
        if (ip3 instanceof qo) {
            Object obj = ((qo) ip3).a;
            if (!(obj instanceof c)) {
                return obj;
            }
            c cVar = (c) obj;
            if (!cVar.a) {
                return obj;
            }
            Throwable th = cVar.b;
            return th != null ? new c(false, th) : c.d;
        }
        boolean isCancelled = ip3.isCancelled();
        if ((!d) && isCancelled) {
            return c.d;
        }
        try {
            Object a2 = a(ip3);
            return a2 == null ? g : a2;
        } catch (ExecutionException e2) {
            return new d(e2.getCause());
        } catch (CancellationException e3) {
            if (isCancelled) {
                return new c(false, e3);
            }
            return new d(new IllegalArgumentException("get() threw CancellationException, despite reporting isCancelled() == false: " + ip3, e3));
        } catch (Throwable th2) {
            return new d(th2);
        }
    }

    @DexIgnore
    public static <T> T d(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final String c(Object obj) {
        return obj == this ? "this future" : String.valueOf(obj);
    }

    @DexIgnore
    public final V a(Object obj) throws ExecutionException {
        if (obj instanceof c) {
            throw a("Task was cancelled.", ((c) obj).b);
        } else if (obj instanceof d) {
            throw new ExecutionException(((d) obj).a);
        } else if (obj == g) {
            return null;
        } else {
            return obj;
        }
    }

    @DexIgnore
    public final void a(Runnable runnable, Executor executor) {
        d(runnable);
        d(executor);
        e eVar = this.b;
        if (eVar != e.d) {
            e eVar2 = new e(runnable, executor);
            do {
                eVar2.c = eVar;
                if (!f.a((qo<?>) this, eVar, eVar2)) {
                    eVar = this.b;
                } else {
                    return;
                }
            } while (eVar != e.d);
        }
        b(runnable, executor);
    }

    @DexIgnore
    public static void b(Runnable runnable, Executor executor) {
        try {
            executor.execute(runnable);
        } catch (RuntimeException e2) {
            Logger logger = e;
            Level level = Level.SEVERE;
            logger.log(level, "RuntimeException while executing runnable " + runnable + " with executor " + executor, e2);
        }
    }

    @DexIgnore
    public boolean a(Throwable th) {
        d(th);
        if (!f.a((qo<?>) this, (Object) null, (Object) new d(th))) {
            return false;
        }
        a((qo<?>) this);
        return true;
    }

    @DexIgnore
    public boolean a(ip3<? extends V> ip3) {
        g gVar;
        d dVar;
        d(ip3);
        Object obj = this.a;
        if (obj == null) {
            if (ip3.isDone()) {
                if (!f.a((qo<?>) this, (Object) null, b((ip3<?>) ip3))) {
                    return false;
                }
                a((qo<?>) this);
                return true;
            }
            gVar = new g(this, ip3);
            if (f.a((qo<?>) this, (Object) null, (Object) gVar)) {
                try {
                    ip3.a(gVar, ro.INSTANCE);
                } catch (Throwable unused) {
                    dVar = d.b;
                }
                return true;
            }
            obj = this.a;
        }
        if (obj instanceof c) {
            ip3.cancel(((c) obj).a);
        }
        return false;
        f.a((qo<?>) this, (Object) gVar, (Object) dVar);
        return true;
    }

    @DexIgnore
    public static <V> V a(Future<V> future) throws ExecutionException {
        V v;
        boolean z = false;
        while (true) {
            try {
                v = future.get();
                break;
            } catch (InterruptedException unused) {
                z = true;
            } catch (Throwable th) {
                if (z) {
                    Thread.currentThread().interrupt();
                }
                throw th;
            }
        }
        if (z) {
            Thread.currentThread().interrupt();
        }
        return v;
    }

    @DexIgnore
    public static void a(qo<?> qoVar) {
        e eVar = null;
        qo<V> qoVar2 = qoVar;
        while (true) {
            qoVar2.d();
            qoVar2.a();
            e a2 = qoVar2.a(eVar);
            while (true) {
                if (a2 != null) {
                    eVar = a2.c;
                    Runnable runnable = a2.a;
                    if (runnable instanceof g) {
                        g gVar = (g) runnable;
                        qo<V> qoVar3 = gVar.a;
                        if (qoVar3.a == gVar) {
                            if (f.a((qo<?>) qoVar3, (Object) gVar, b((ip3<?>) gVar.b))) {
                                qoVar2 = qoVar3;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        b(runnable, a2.b);
                    }
                    a2 = eVar;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final V get() throws InterruptedException, ExecutionException {
        Object obj;
        if (!Thread.interrupted()) {
            Object obj2 = this.a;
            if ((obj2 != null) && (!(obj2 instanceof g))) {
                return a(obj2);
            }
            i iVar = this.c;
            if (iVar != i.c) {
                i iVar2 = new i();
                do {
                    iVar2.a(iVar);
                    if (f.a((qo<?>) this, iVar, iVar2)) {
                        do {
                            LockSupport.park(this);
                            if (!Thread.interrupted()) {
                                obj = this.a;
                            } else {
                                a(iVar2);
                                throw new InterruptedException();
                            }
                        } while (!((obj != null) & (!(obj instanceof g))));
                        return a(obj);
                    }
                    iVar = this.c;
                } while (iVar != i.c);
            }
            return a(this.a);
        }
        throw new InterruptedException();
    }

    @DexIgnore
    public final e a(e eVar) {
        e eVar2;
        do {
            eVar2 = this.b;
        } while (!f.a((qo<?>) this, eVar2, e.d));
        e eVar3 = eVar2;
        e eVar4 = eVar;
        e eVar5 = eVar3;
        while (eVar5 != null) {
            e eVar6 = eVar5.c;
            eVar5.c = eVar4;
            eVar4 = eVar5;
            eVar5 = eVar6;
        }
        return eVar4;
    }

    @DexIgnore
    public final void a(StringBuilder sb) {
        try {
            Object a2 = a(this);
            sb.append("SUCCESS, result=[");
            sb.append(c(a2));
            sb.append("]");
        } catch (ExecutionException e2) {
            sb.append("FAILURE, cause=[");
            sb.append(e2.getCause());
            sb.append("]");
        } catch (CancellationException unused) {
            sb.append("CANCELLED");
        } catch (RuntimeException e3) {
            sb.append("UNKNOWN, cause=[");
            sb.append(e3.getClass());
            sb.append(" thrown from get()]");
        }
    }

    @DexIgnore
    public static CancellationException a(String str, Throwable th) {
        CancellationException cancellationException = new CancellationException(str);
        cancellationException.initCause(th);
        return cancellationException;
    }
}
