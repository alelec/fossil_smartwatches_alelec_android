package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o90 extends n90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ fd0 c;
    @DexIgnore
    public /* final */ ed0 d;
    @DexIgnore
    public /* final */ gd0 e;
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<o90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new o90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new o90[i];
        }
    }

    @DexIgnore
    public o90(byte b, fd0 fd0, ed0 ed0, gd0 gd0, byte[] bArr) {
        super(e90.ENCRYPTED_DATA, b);
        this.c = fd0;
        this.d = ed0;
        this.e = gd0;
        this.f = bArr;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(super.a(), bm0.DATA_TYPE, (Object) cw0.a((Enum<?>) this.c)), bm0.ENCRYPT_METHOD, (Object) cw0.a((Enum<?>) this.d)), bm0.KEY_TYPE, (Object) cw0.a((Enum<?>) this.e)), bm0.RAW_DATA_CRC, (Object) Long.valueOf(h51.a.a(this.f, q11.CRC32)));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(o90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            o90 o90 = (o90) obj;
            return this.c == o90.c && this.d == o90.d && this.e == o90.e && Arrays.equals(this.f, o90.f);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.EncryptedDataNotification");
    }

    @DexIgnore
    public final ed0 getEncryptMethod() {
        return this.d;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.f;
    }

    @DexIgnore
    public final fd0 getEncryptedDataType() {
        return this.c;
    }

    @DexIgnore
    public final gd0 getKeyType() {
        return this.e;
    }

    @DexIgnore
    public final short getSequence() {
        return cw0.b(b());
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        return this.f.hashCode() + ((hashCode3 + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
    }

    @DexIgnore
    public /* synthetic */ o90(Parcel parcel, qg6 qg6) {
        super(parcel);
        fd0 a2 = fd0.c.a(parcel.readByte());
        if (a2 != null) {
            this.c = a2;
            ed0 a3 = ed0.c.a(parcel.readByte());
            if (a3 != null) {
                this.d = a3;
                gd0 a4 = gd0.c.a(parcel.readByte());
                if (a4 != null) {
                    this.e = a4;
                    byte[] createByteArray = parcel.createByteArray();
                    this.f = createByteArray == null ? new byte[0] : createByteArray;
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }
}
