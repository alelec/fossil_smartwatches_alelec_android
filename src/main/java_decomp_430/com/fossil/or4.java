package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class or4 implements Factory<nr4> {
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> a;
    @DexIgnore
    public /* final */ Provider<an4> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> e;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> f;
    @DexIgnore
    public /* final */ Provider<sj4> g;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> h;

    @DexIgnore
    public or4(Provider<MicroAppRepository> provider, Provider<an4> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<sj4> provider7, Provider<AlarmsRepository> provider8) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
    }

    @DexIgnore
    public static or4 a(Provider<MicroAppRepository> provider, Provider<an4> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<sj4> provider7, Provider<AlarmsRepository> provider8) {
        return new or4(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static nr4 b(Provider<MicroAppRepository> provider, Provider<an4> provider2, Provider<DeviceRepository> provider3, Provider<PortfolioApp> provider4, Provider<HybridPresetRepository> provider5, Provider<NotificationsRepository> provider6, Provider<sj4> provider7, Provider<AlarmsRepository> provider8) {
        return new HybridSyncUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get());
    }

    @DexIgnore
    public HybridSyncUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h);
    }
}
