package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class da1 extends l61 {
    @DexIgnore
    public /* final */ n70 A;

    @DexIgnore
    public da1(n70 n70, ue1 ue1) {
        super(lx0.NOTIFY_MUSIC_EVENT, ue1);
        this.A = n70;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.MUSIC_EVENT, (Object) this.A.a());
    }

    @DexIgnore
    public ok0 l() {
        rg1 rg1 = rg1.ASYNC;
        n70 n70 = this.A;
        ByteBuffer order = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(4).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(kl0.NOTIFY.a);
        order.put(xh0.MUSIC_EVENT.a);
        order.put(n70.getAction().a());
        order.put(n70.getActionStatus().a());
        byte[] array = order.array();
        wg6.a(array, "musicEventData.array()");
        return new fb1(rg1, array, this.y.v);
    }
}
