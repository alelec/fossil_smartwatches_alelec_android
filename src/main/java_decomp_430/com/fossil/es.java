package com.fossil;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es extends OutputStream {
    @DexIgnore
    public /* final */ OutputStream a;
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public xt c;
    @DexIgnore
    public int d;

    @DexIgnore
    public es(OutputStream outputStream, xt xtVar) {
        this(outputStream, xtVar, 65536);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void close() throws IOException {
        try {
            flush();
            this.a.close();
            m();
        } catch (Throwable th) {
            this.a.close();
            throw th;
        }
    }

    @DexIgnore
    public void flush() throws IOException {
        k();
        this.a.flush();
    }

    @DexIgnore
    public final void k() throws IOException {
        int i = this.d;
        if (i > 0) {
            this.a.write(this.b, 0, i);
            this.d = 0;
        }
    }

    @DexIgnore
    public final void l() throws IOException {
        if (this.d == this.b.length) {
            k();
        }
    }

    @DexIgnore
    public final void m() {
        byte[] bArr = this.b;
        if (bArr != null) {
            this.c.put(bArr);
            this.b = null;
        }
    }

    @DexIgnore
    public void write(int i) throws IOException {
        byte[] bArr = this.b;
        int i2 = this.d;
        this.d = i2 + 1;
        bArr[i2] = (byte) i;
        l();
    }

    @DexIgnore
    public es(OutputStream outputStream, xt xtVar, int i) {
        this.a = outputStream;
        this.c = xtVar;
        this.b = (byte[]) xtVar.b(i, byte[].class);
    }

    @DexIgnore
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.d != 0 || i4 < this.b.length) {
                int min = Math.min(i4, this.b.length - this.d);
                System.arraycopy(bArr, i5, this.b, this.d, min);
                this.d += min;
                i3 += min;
                l();
            } else {
                this.a.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
