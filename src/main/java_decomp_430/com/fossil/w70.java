package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w70 extends r70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w70> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new w70(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new w70[i];
        }
    }

    @DexIgnore
    public w70(byte b2, int i) {
        super(t70.REPLY_MESSAGE);
        this.b = b2;
        this.c = i;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(super.a(), bm0.MESSAGE, (Object) Byte.valueOf(this.b)), bm0.SENDER_ID, (Object) Integer.valueOf(this.c));
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.b;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.c;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeByte(this.b);
        parcel.writeInt(this.c);
    }

    @DexIgnore
    public /* synthetic */ w70(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readInt();
    }
}
