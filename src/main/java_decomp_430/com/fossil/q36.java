package com.fossil;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q36 {
    @DexIgnore
    public static g56 a;
    @DexIgnore
    public static volatile Map<String, Properties> b; // = new ConcurrentHashMap();
    @DexIgnore
    public static volatile Map<Integer, Integer> c; // = new ConcurrentHashMap(10);
    @DexIgnore
    public static volatile long d; // = 0;
    @DexIgnore
    public static volatile long e; // = 0;
    @DexIgnore
    public static volatile long f; // = 0;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static volatile int h; // = 0;
    @DexIgnore
    public static volatile String i; // = "";
    @DexIgnore
    public static volatile String j; // = "";
    @DexIgnore
    public static Map<String, Long> k; // = new ConcurrentHashMap();
    @DexIgnore
    public static Map<String, Long> l; // = new ConcurrentHashMap();
    @DexIgnore
    public static b56 m; // = m56.b();
    @DexIgnore
    public static Thread.UncaughtExceptionHandler n; // = null;
    @DexIgnore
    public static volatile boolean o; // = true;
    @DexIgnore
    public static volatile int p; // = 0;
    @DexIgnore
    public static volatile long q; // = 0;
    @DexIgnore
    public static Context r; // = null;
    @DexIgnore
    public static volatile long s; // = 0;

    /*
    static {
        new ConcurrentHashMap();
    }
    */

    @DexIgnore
    public static int a(Context context, boolean z, r36 r36) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = z && currentTimeMillis - e >= ((long) n36.m());
        e = currentTimeMillis;
        if (f == 0) {
            f = m56.c();
        }
        if (currentTimeMillis >= f) {
            f = m56.c();
            if (o46.b(context).a(context).d() != 1) {
                o46.b(context).a(context).a(1);
            }
            n36.b(0);
            p = 0;
            g = m56.a(0);
            z2 = true;
        }
        String str = g;
        if (m56.a(r36)) {
            str = r36.a() + g;
        }
        if (!l.containsKey(str)) {
            z2 = true;
        }
        if (z2) {
            if (m56.a(r36)) {
                a(context, r36);
            } else if (n36.c() < n36.f()) {
                m56.A(context);
                a(context, (r36) null);
            } else {
                m.c("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            l.put(str, 1L);
        }
        if (o) {
            h(context);
            o = false;
        }
        return h;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        return;
     */
    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (q36.class) {
            if (context != null) {
                if (a == null) {
                    if (b(context)) {
                        Context applicationContext = context.getApplicationContext();
                        r = applicationContext;
                        a = new g56();
                        g = m56.a(0);
                        d = System.currentTimeMillis() + n36.x;
                        a.a(new b66(applicationContext));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(Context context, int i2) {
        b56 b56;
        String str;
        if (n36.s()) {
            if (n36.q()) {
                b56 b562 = m;
                b562.e("commitEvents, maxNumber=" + i2);
            }
            Context g2 = g(context);
            if (g2 == null) {
                b56 = m;
                str = "The Context of StatService.commitEvents() can not be null!";
            } else if (i2 < -1 || i2 == 0) {
                b56 = m;
                str = "The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.";
            } else if (b46.a(r).f() && c(g2) != null) {
                a.a(new c46(g2, i2));
                return;
            } else {
                return;
            }
            b56.d(str);
        }
    }

    @DexIgnore
    public static void a(Context context, r36 r36) {
        if (c(context) != null) {
            if (n36.q()) {
                m.a((Object) "start new session.");
            }
            if (r36 == null || h == 0) {
                h = m56.a();
            }
            n36.a(0);
            n36.b();
            new k46(new a46(context, h, b(), r36)).a();
        }
    }

    @DexIgnore
    public static void a(Context context, String str, r36 r36) {
        if (n36.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new g66(str2, g2, r36));
            }
        }
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties, r36 r36) {
        b56 b56;
        String str2;
        if (n36.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                b56 = m;
                str2 = "The Context of StatService.trackCustomEvent() can not be null!";
            } else if (a(str)) {
                b56 = m;
                str2 = "The event_id of StatService.trackCustomEvent() can not be null or empty.";
            } else {
                t36 t36 = new t36(str, (String[]) null, properties);
                if (c(g2) != null) {
                    a.a(new f66(g2, r36, t36));
                    return;
                }
                return;
            }
            b56.d(str2);
        }
    }

    @DexIgnore
    public static void a(Context context, Throwable th) {
        if (n36.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (c(g2) != null) {
                a.a(new d66(g2, th));
            }
        }
    }

    @DexIgnore
    public static boolean a() {
        if (p < 2) {
            return false;
        }
        q = System.currentTimeMillis();
        return true;
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2, r36 r36) {
        try {
            if (!n36.s()) {
                m.d("MTA StatService is disable.");
                return false;
            }
            if (n36.q()) {
                m.a((Object) "MTA SDK version, current: " + "2.0.3" + " ,required: " + str2);
            }
            if (context != null) {
                if (str2 != null) {
                    if (m56.b("2.0.3") < m56.b(str2)) {
                        m.d(("MTA SDK version conflicted, current: " + "2.0.3" + ",required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                        n36.b(false);
                        return false;
                    }
                    String d2 = n36.d(context);
                    if (d2 == null || d2.length() == 0) {
                        n36.b(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    }
                    if (str != null) {
                        n36.b(context, str);
                    }
                    if (c(context) == null) {
                        return true;
                    }
                    a.a(new h46(context, r36));
                    return true;
                }
            }
            m.d("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
            n36.b(false);
            return false;
        } catch (Throwable th) {
            m.a(th);
            return false;
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (n36.c.d != 0) {
                jSONObject2.put("v", n36.c.d);
            }
            jSONObject.put(Integer.toString(n36.c.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (n36.b.d != 0) {
                jSONObject3.put("v", n36.b.d);
            }
            jSONObject.put(Integer.toString(n36.b.a), jSONObject3);
        } catch (JSONException e2) {
            m.a((Throwable) e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public static void b(Context context, r36 r36) {
        if (n36.s() && c(context) != null) {
            a.a(new c66(context, r36));
        }
    }

    @DexIgnore
    public static void b(Context context, String str, r36 r36) {
        if (n36.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new f46(g2, str2, r36));
            }
        }
    }

    @DexIgnore
    public static boolean b(Context context) {
        boolean z;
        long a2 = q56.a(context, n36.n, 0);
        long b2 = m56.b("2.0.3");
        boolean z2 = false;
        if (b2 <= a2) {
            b56 b56 = m;
            b56.d("MTA is disable for current version:" + b2 + ",wakeup version:" + a2);
            z = false;
        } else {
            z = true;
        }
        long a3 = q56.a(context, n36.o, 0);
        if (a3 > System.currentTimeMillis()) {
            b56 b562 = m;
            b562.d("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + a3);
        } else {
            z2 = z;
        }
        n36.b(z2);
        return z2;
    }

    @DexIgnore
    public static g56 c(Context context) {
        if (a == null) {
            synchronized (q36.class) {
                if (a == null) {
                    try {
                        a(context);
                    } catch (Throwable th) {
                        m.b(th);
                        n36.b(false);
                    }
                }
            }
        }
        return a;
    }

    @DexIgnore
    public static void c() {
        p = 0;
        q = 0;
    }

    @DexIgnore
    public static void c(Context context, r36 r36) {
        if (n36.s() && c(context) != null) {
            a.a(new g46(context, r36));
        }
    }

    @DexIgnore
    public static Properties d(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static void d() {
        p++;
        q = System.currentTimeMillis();
        f(r);
    }

    @DexIgnore
    public static void d(Context context) {
        if (n36.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                y56.b(g2).a((v36) new x36(g2), (x56) new e66());
            } catch (Throwable th) {
                m.a(th);
            }
        }
    }

    @DexIgnore
    public static void e(Context context) {
        s = System.currentTimeMillis() + ((long) (n36.l() * 60000));
        q56.b(context, "last_period_ts", s);
        a(context, -1);
    }

    @DexIgnore
    public static void f(Context context) {
        if (n36.s() && n36.J > 0) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else {
                o46.b(g2).b();
            }
        }
    }

    @DexIgnore
    public static Context g(Context context) {
        return context != null ? context : r;
    }

    @DexIgnore
    public static void h(Context context) {
        if (n36.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else if (c(g2) != null) {
                a.a(new d46(g2));
            }
        }
    }
}
