package com.fossil;

import android.content.DialogInterface;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.fossil.l0;
import com.fossil.x1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r1 implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, x1.a {
    @DexIgnore
    public q1 a;
    @DexIgnore
    public l0 b;
    @DexIgnore
    public o1 c;
    @DexIgnore
    public x1.a d;

    @DexIgnore
    public r1(q1 q1Var) {
        this.a = q1Var;
    }

    @DexIgnore
    public void a(IBinder iBinder) {
        q1 q1Var = this.a;
        l0.a aVar = new l0.a(q1Var.e());
        this.c = new o1(aVar.b(), g0.abc_list_menu_item_layout);
        this.c.a((x1.a) this);
        this.a.a((x1) this.c);
        aVar.a(this.c.c(), (DialogInterface.OnClickListener) this);
        View i = q1Var.i();
        if (i != null) {
            aVar.a(i);
        } else {
            aVar.a(q1Var.g());
            aVar.b(q1Var.h());
        }
        aVar.a((DialogInterface.OnKeyListener) this);
        this.b = aVar.a();
        this.b.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.b.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.b.show();
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a((MenuItem) (t1) this.c.c().getItem(i), 0);
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        this.c.a(this.a, true);
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.b.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.b.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.a.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.a.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    public void a() {
        l0 l0Var = this.b;
        if (l0Var != null) {
            l0Var.dismiss();
        }
    }

    @DexIgnore
    public void a(q1 q1Var, boolean z) {
        if (z || q1Var == this.a) {
            a();
        }
        x1.a aVar = this.d;
        if (aVar != null) {
            aVar.a(q1Var, z);
        }
    }

    @DexIgnore
    public boolean a(q1 q1Var) {
        x1.a aVar = this.d;
        if (aVar != null) {
            return aVar.a(q1Var);
        }
        return false;
    }
}
