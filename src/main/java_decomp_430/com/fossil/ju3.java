package com.fossil;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ju3 extends JsonElement {
    @DexIgnore
    public static /* final */ ju3 a; // = new ju3();

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof ju3);
    }

    @DexIgnore
    public int hashCode() {
        return ju3.class.hashCode();
    }
}
