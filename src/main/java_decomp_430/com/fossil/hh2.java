package com.fossil;

import android.os.RemoteException;
import com.fossil.rv1;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh2 extends vf2 {
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest s;
    @DexIgnore
    public /* final */ /* synthetic */ zv2 t;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hh2(gh2 gh2, wv1 wv1, LocationRequest locationRequest, zv2 zv2) {
        super(wv1);
        this.s = locationRequest;
        this.t = zv2;
    }

    @DexIgnore
    public final /* synthetic */ void a(rv1.b bVar) throws RemoteException {
        ((og2) bVar).a(this.s, (uw1<zv2>) vw1.a(this.t, yg2.a(), zv2.class.getSimpleName()), (ag2) new wf2(this));
    }
}
