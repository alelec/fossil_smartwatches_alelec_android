package com.fossil;

import android.view.View;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk4 implements View.OnClickListener {
    @DexIgnore
    public AtomicBoolean a; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ long b; // = 1000;
    @DexIgnore
    public /* final */ View.OnClickListener c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ uk4 a;

        @DexIgnore
        public a(uk4 uk4, View view) {
            this.a = uk4;
        }

        @DexIgnore
        public final void run() {
            this.a.a.set(true);
        }
    }

    @DexIgnore
    public uk4(View.OnClickListener onClickListener) {
        wg6.b(onClickListener, "clickListener");
        this.c = onClickListener;
    }

    @DexIgnore
    public void onClick(View view) {
        if (this.a.getAndSet(false) && view != null) {
            view.postDelayed(new a(this, view), this.b);
            this.c.onClick(view);
        }
    }
}
