package com.fossil;

import android.annotation.TargetApi;
import android.os.Trace;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i92 implements Closeable {
    @DexIgnore
    public static /* final */ q92<Boolean> b; // = p92.a().a("nts.enable_tracing", true);
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    @TargetApi(18)
    public i92(String str) {
        this.a = s42.d() && b.get().booleanValue();
        if (this.a) {
            Trace.beginSection(str.length() > 127 ? str.substring(0, 127) : str);
        }
    }

    @DexIgnore
    @TargetApi(18)
    public final void close() {
        if (this.a) {
            Trace.endSection();
        }
    }
}
