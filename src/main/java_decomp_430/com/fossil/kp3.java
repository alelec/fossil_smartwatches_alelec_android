package com.fossil;

import android.content.Context;
import com.google.firebase.FirebaseApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kp3 implements ys3 {
    @DexIgnore
    public /* final */ FirebaseApp a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public kp3(FirebaseApp firebaseApp, Context context) {
        this.a = firebaseApp;
        this.b = context;
    }

    @DexIgnore
    public static ys3 a(FirebaseApp firebaseApp, Context context) {
        return new kp3(firebaseApp, context);
    }

    @DexIgnore
    public Object get() {
        return FirebaseApp.a(this.a, this.b);
    }
}
