package com.fossil;

import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u53 extends s63 {
    @DexIgnore
    public static /* final */ AtomicLong l; // = new AtomicLong(Long.MIN_VALUE);
    @DexIgnore
    public y53 c;
    @DexIgnore
    public y53 d;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<v53<?>> e; // = new PriorityBlockingQueue<>();
    @DexIgnore
    public /* final */ BlockingQueue<v53<?>> f; // = new LinkedBlockingQueue();
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler g; // = new w53(this, "Thread death: Uncaught exception on worker thread");
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler h; // = new w53(this, "Thread death: Uncaught exception on network thread");
    @DexIgnore
    public /* final */ Object i; // = new Object();
    @DexIgnore
    public /* final */ Semaphore j; // = new Semaphore(2);
    @DexIgnore
    public volatile boolean k;

    @DexIgnore
    public u53(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final <V> Future<V> a(Callable<V> callable) throws IllegalStateException {
        n();
        w12.a(callable);
        v53 v53 = new v53(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            if (!this.e.isEmpty()) {
                b().w().a("Callable skipped the worker queue.");
            }
            v53.run();
        } else {
            a((v53<?>) v53);
        }
        return v53;
    }

    @DexIgnore
    public final <V> Future<V> b(Callable<V> callable) throws IllegalStateException {
        n();
        w12.a(callable);
        v53 v53 = new v53(this, callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.c) {
            v53.run();
        } else {
            a((v53<?>) v53);
        }
        return v53;
    }

    @DexIgnore
    public final void f() {
        if (Thread.currentThread() != this.d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    @DexIgnore
    public final void g() {
        if (Thread.currentThread() != this.c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    @DexIgnore
    public final boolean q() {
        return false;
    }

    @DexIgnore
    public final boolean s() {
        return Thread.currentThread() == this.c;
    }

    @DexIgnore
    public final void b(Runnable runnable) throws IllegalStateException {
        n();
        w12.a(runnable);
        v53 v53 = new v53(this, runnable, false, "Task exception on network thread");
        synchronized (this.i) {
            this.f.add(v53);
            if (this.d == null) {
                this.d = new y53(this, "Measurement Network", this.f);
                this.d.setUncaughtExceptionHandler(this.h);
                this.d.start();
            } else {
                this.d.a();
            }
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) throws IllegalStateException {
        n();
        w12.a(runnable);
        a((v53<?>) new v53(this, runnable, false, "Task exception on worker thread"));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:16|17|(1:19)(1:20)|21|22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0014, code lost:
        r2 = b().w();
        r4 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r4.length() == 0) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0028, code lost:
        r3 = "Timed out waiting for ".concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r3 = new java.lang.String("Timed out waiting for ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        r2.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r2 = b().w();
        r4 = java.lang.String.valueOf(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        if (r4.length() != 0) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        r3 = "Interrupted waiting for ".concat(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0050, code lost:
        r3 = new java.lang.String("Interrupted waiting for ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0056, code lost:
        r2.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005b, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        r1 = r1.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r1 != null) goto L_0x0036;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0037 */
    public final <T> T a(AtomicReference<T> atomicReference, long j2, String str, Runnable runnable) {
        synchronized (atomicReference) {
            a().a(runnable);
            atomicReference.wait(15000);
        }
    }

    @DexIgnore
    public final void a(v53<?> v53) {
        synchronized (this.i) {
            this.e.add(v53);
            if (this.c == null) {
                this.c = new y53(this, "Measurement Worker", this.e);
                this.c.setUncaughtExceptionHandler(this.g);
                this.c.start();
            } else {
                this.c.a();
            }
        }
    }
}
