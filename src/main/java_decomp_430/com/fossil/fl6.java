package com.fossil;

import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl6 {
    @DexIgnore
    public static final void a(af6 af6, Throwable th) {
        wg6.b(af6, "context");
        wg6.b(th, "exception");
        try {
            CoroutineExceptionHandler coroutineExceptionHandler = (CoroutineExceptionHandler) af6.get(CoroutineExceptionHandler.m);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(af6, th);
            } else {
                el6.a(af6, th);
            }
        } catch (Throwable th2) {
            el6.a(af6, a(th, th2));
        }
    }

    @DexIgnore
    public static final Throwable a(Throwable th, Throwable th2) {
        wg6.b(th, "originalException");
        wg6.b(th2, "thrownException");
        if (th == th2) {
            return th;
        }
        RuntimeException runtimeException = new RuntimeException("Exception while trying to handle coroutine exception", th2);
        ec6.a(runtimeException, th);
        return runtimeException;
    }
}
