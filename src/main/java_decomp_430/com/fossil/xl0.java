package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum xl0 {
    GOAL_TRACKING((byte) 2);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public xl0(byte b) {
        this.a = b;
    }
}
