package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q9 {
    @DexIgnore
    boolean onNestedFling(View view, float f, float f2, boolean z);

    @DexIgnore
    boolean onNestedPreFling(View view, float f, float f2);

    @DexIgnore
    void onNestedPreScroll(View view, int i, int i2, int[] iArr);

    @DexIgnore
    void onNestedScroll(View view, int i, int i2, int i3, int i4);

    @DexIgnore
    void onNestedScrollAccepted(View view, View view2, int i);

    @DexIgnore
    boolean onStartNestedScroll(View view, View view2, int i);

    @DexIgnore
    void onStopNestedScroll(View view);
}
