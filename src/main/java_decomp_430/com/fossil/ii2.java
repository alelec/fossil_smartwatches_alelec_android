package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ii2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ c73 e;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ii2(ov2 ov2, c73 c73) {
        super(ov2);
        this.f = ov2;
        this.e = c73;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        for (int i = 0; i < this.f.d.size(); i++) {
            if (this.e.equals(((Pair) this.f.d.get(i)).first)) {
                Log.w(this.f.a, "OnEventListener already registered.");
                return;
            }
        }
        ov2.b bVar = new ov2.b(this.e);
        this.f.d.add(new Pair(this.e, bVar));
        this.f.g.registerOnMeasurementEventListener(bVar);
    }
}
