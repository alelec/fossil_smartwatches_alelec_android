package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kc0 extends ic0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public kc0 createFromParcel(Parcel parcel) {
            return new kc0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new kc0[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m33createFromParcel(Parcel parcel) {
            return new kc0(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public /* synthetic */ kc0(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.c = readString;
            this.d = parcel.readInt();
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = super.a().put("loc", this.c).put("utc", this.d);
        wg6.a(put, "super.toJSONObject()\n   \u2026.UTC, utcOffsetInMinutes)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(kc0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            kc0 kc0 = (kc0) obj;
            return !(wg6.a(this.c, kc0.c) ^ true) && this.d == kc0.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    public final String getLocation() {
        return this.c;
    }

    @DexIgnore
    public final int getUtcOffsetInMinutes() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.c.hashCode() + (super.hashCode() * 31)) * 31) + this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public kc0(String str, int i) {
        super(pz0.SECOND_TIMEZONE, false, 2);
        this.c = str;
        this.d = i;
    }
}
