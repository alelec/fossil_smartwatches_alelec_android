package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn2 implements oo2 {
    @DexIgnore
    public static /* final */ gn2 a; // = new gn2();

    @DexIgnore
    public static gn2 a() {
        return a;
    }

    @DexIgnore
    public final boolean zza(Class<?> cls) {
        return fn2.class.isAssignableFrom(cls);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [java.lang.Class<?>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final po2 zzb(Class<?> r5) {
        Class cls = fn2.class;
        if (!cls.isAssignableFrom(r5)) {
            String valueOf = String.valueOf(r5.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (po2) fn2.a(r5.asSubclass(cls)).a(fn2.e.c, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(r5.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
