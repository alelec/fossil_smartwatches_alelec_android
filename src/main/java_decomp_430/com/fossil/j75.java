package com.fossil;

import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j75 implements Factory<i75> {
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> a;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> b;

    @DexIgnore
    public j75(Provider<WatchFaceRepository> provider, Provider<RingStyleRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static j75 a(Provider<WatchFaceRepository> provider, Provider<RingStyleRepository> provider2) {
        return new j75(provider, provider2);
    }

    @DexIgnore
    public static i75 b(Provider<WatchFaceRepository> provider, Provider<RingStyleRepository> provider2) {
        return new PreviewViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public PreviewViewModel get() {
        return b(this.a, this.b);
    }
}
