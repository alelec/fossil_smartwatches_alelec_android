package com.fossil;

import android.media.RemoteController;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$e$a implements RemoteController.OnClientUpdateListener {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.e a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;

    @DexIgnore
    public fq4$e$a(MusicControlComponent.e eVar, String str) {
        this.a = eVar;
        this.b = str;
    }

    @DexIgnore
    public void onClientChange(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MusicControlComponent.o.a();
        local.d(a2, "onClientChange(), clearing=" + z);
    }

    @DexIgnore
    public void onClientMetadataUpdate(RemoteController.MetadataEditor metadataEditor) {
        if (metadataEditor != null) {
            String a2 = xi4.a(metadataEditor.getString(7, "Unknown"), "Unknown");
            String a3 = xi4.a(metadataEditor.getString(2, "Unknown"), "Unknown");
            String a4 = xi4.a(metadataEditor.getString(1, "Unknown"), "Unknown");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a5 = MusicControlComponent.o.a();
            local.d(a5, ".onMetadataChanged callback, title=" + a2 + ", artist=" + a3 + ", album=" + a4);
            MusicControlComponent.c cVar = new MusicControlComponent.c(this.a.c(), this.b, a2, a3, a4);
            if (true ^ wg6.a((Object) cVar, (Object) this.a.e())) {
                MusicControlComponent.c e = this.a.e();
                this.a.a(cVar);
                this.a.a(e, cVar);
            }
        }
    }

    @DexIgnore
    public void onClientPlaybackStateUpdate(int i) {
        int a2 = this.a.a(i);
        if (a2 != this.a.f()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = MusicControlComponent.o.a();
            local.d(a3, "onClientPlaybackStateUpdate(), state=" + i);
            int f = this.a.f();
            this.a.b(a2);
            MusicControlComponent.e eVar = this.a;
            eVar.a(f, a2, eVar);
        }
    }

    @DexIgnore
    public void onClientTransportControlUpdate(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MusicControlComponent.o.a();
        local.d(a2, "onClientTransportControlUpdate(), transportControlFlags=" + i);
    }

    @DexIgnore
    public void onClientPlaybackStateUpdate(int i, long j, long j2, float f) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MusicControlComponent.o.a();
        local.d(a2, "onClientPlaybackStateUpdate(), state=" + i + ", stateChangeTimeMs=" + j + ", currentPosMs=" + j2 + ", speed=" + f);
    }
}
