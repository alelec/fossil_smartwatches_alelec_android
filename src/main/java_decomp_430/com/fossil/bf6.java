package com.fossil;

import com.fossil.af6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bf6 implements af6, Serializable {
    @DexIgnore
    public static /* final */ bf6 INSTANCE; // = new bf6();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public <R> R fold(R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
        wg6.b(ig6, "operation");
        return r;
    }

    @DexIgnore
    public <E extends af6.b> E get(af6.c<E> cVar) {
        wg6.b(cVar, "key");
        return null;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public af6 minusKey(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        return this;
    }

    @DexIgnore
    public af6 plus(af6 af6) {
        wg6.b(af6, "context");
        return af6;
    }

    @DexIgnore
    public String toString() {
        return "EmptyCoroutineContext";
    }
}
