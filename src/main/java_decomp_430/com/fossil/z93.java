package com.fossil;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import com.facebook.internal.NativeProtocol;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z93 extends aa3 {
    @DexIgnore
    public /* final */ AlarmManager d; // = ((AlarmManager) c().getSystemService("alarm"));
    @DexIgnore
    public /* final */ b03 e;
    @DexIgnore
    public Integer f;

    @DexIgnore
    public z93(ea3 ea3) {
        super(ea3);
        this.e = new y93(this, ea3.v(), ea3);
    }

    @DexIgnore
    public final void a(long j) {
        r();
        d();
        Context c = c();
        if (!o53.a(c)) {
            b().A().a("Receiver not registered/enabled");
        }
        if (!ma3.a(c, false)) {
            b().A().a("Service not registered/enabled");
        }
        u();
        long c2 = zzm().c() + j;
        if (j < Math.max(0, l03.C.a(null).longValue()) && !this.e.b()) {
            b().B().a("Scheduling upload with DelayedRunnable");
            this.e.a(j);
        }
        d();
        if (Build.VERSION.SDK_INT >= 24) {
            b().B().a("Scheduling upload with JobScheduler");
            Context c3 = c();
            ComponentName componentName = new ComponentName(c3, "com.google.android.gms.measurement.AppMeasurementJobService");
            int w = w();
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString(NativeProtocol.WEB_DIALOG_ACTION, "com.google.android.gms.measurement.UPLOAD");
            JobInfo build = new JobInfo.Builder(w, componentName).setMinimumLatency(j).setOverrideDeadline(j << 1).setExtras(persistableBundle).build();
            b().B().a("Scheduling job. JobID", Integer.valueOf(w));
            bp2.a(c3, build, "com.google.android.gms", "UploadAlarm");
            return;
        }
        b().B().a("Scheduling upload with AlarmManager");
        this.d.setInexactRepeating(2, c2, Math.max(l03.x.a(null).longValue(), j), x());
    }

    @DexIgnore
    public final boolean t() {
        this.d.cancel(x());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        v();
        return false;
    }

    @DexIgnore
    public final void u() {
        r();
        this.d.cancel(x());
        this.e.c();
        if (Build.VERSION.SDK_INT >= 24) {
            v();
        }
    }

    @DexIgnore
    @TargetApi(24)
    public final void v() {
        int w = w();
        b().B().a("Cancelling job. JobID", Integer.valueOf(w));
        ((JobScheduler) c().getSystemService("jobscheduler")).cancel(w);
    }

    @DexIgnore
    public final int w() {
        if (this.f == null) {
            String valueOf = String.valueOf(c().getPackageName());
            this.f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final PendingIntent x() {
        Context c = c();
        return PendingIntent.getBroadcast(c, 0, new Intent().setClassName(c, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }
}
