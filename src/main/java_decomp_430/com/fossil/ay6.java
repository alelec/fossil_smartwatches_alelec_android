package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay6 implements fx6<zq6, Byte> {
    @DexIgnore
    public static /* final */ ay6 a; // = new ay6();

    @DexIgnore
    public Byte a(zq6 zq6) throws IOException {
        return Byte.valueOf(zq6.string());
    }
}
