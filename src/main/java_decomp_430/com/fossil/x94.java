package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x94 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ View D;
    @DexIgnore
    public /* final */ FlexibleProgressBar E;
    @DexIgnore
    public /* final */ ConstraintLayout F;
    @DexIgnore
    public /* final */ RecyclerViewEmptySupport G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ OverviewDayGoalChart t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x94(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, CoordinatorLayout coordinatorLayout, OverviewDayGoalChart overviewDayGoalChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, ConstraintLayout constraintLayout7, View view2, View view3, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout8, RecyclerViewEmptySupport recyclerViewEmptySupport, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, View view4) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = overviewDayGoalChart;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = flexibleTextView4;
        this.y = flexibleTextView5;
        this.z = flexibleTextView6;
        this.A = rTLImageView;
        this.B = rTLImageView2;
        this.C = rTLImageView3;
        this.D = view2;
        this.E = flexibleProgressBar;
        this.F = constraintLayout8;
        this.G = recyclerViewEmptySupport;
        this.H = flexibleTextView7;
    }
}
