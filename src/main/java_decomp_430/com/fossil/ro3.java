package com.fossil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements to3<List<String>> {
        @DexIgnore
        public /* final */ List<String> a; // = um3.a();

        @DexIgnore
        public boolean a(String str) {
            this.a.add(str);
            return true;
        }

        @DexIgnore
        public List<String> getResult() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends io3<File> {
        @DexIgnore
        public String toString() {
            return "Files.fileTreeTraverser()";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends mo3 {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public /* synthetic */ c(File file, a aVar) {
            this(file);
        }

        @DexIgnore
        public String toString() {
            return "Files.asByteSource(" + this.a + ")";
        }

        @DexIgnore
        public c(File file) {
            jk3.a(file);
            this.a = file;
        }

        @DexIgnore
        public FileInputStream a() throws IOException {
            return new FileInputStream(this.a);
        }
    }

    /*
    static {
        new b();
    }
    */

    @DexIgnore
    public static mo3 a(File file) {
        return new c(file, (a) null);
    }

    @DexIgnore
    public static List<String> b(File file, Charset charset) throws IOException {
        return (List) a(file, charset, new a());
    }

    @DexIgnore
    public static no3 a(File file, Charset charset) {
        return a(file).a(charset);
    }

    @DexIgnore
    public static <T> T a(File file, Charset charset, to3<T> to3) throws IOException {
        return a(file, charset).a(to3);
    }
}
