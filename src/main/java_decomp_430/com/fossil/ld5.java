package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ld5 {
    @DexIgnore
    public /* final */ gd5 a;
    @DexIgnore
    public /* final */ vd5 b;
    @DexIgnore
    public /* final */ qd5 c;

    @DexIgnore
    public ld5(gd5 gd5, vd5 vd5, qd5 qd5) {
        wg6.b(gd5, "mCaloriesOverviewDayView");
        wg6.b(vd5, "mCaloriesOverviewWeekView");
        wg6.b(qd5, "mCaloriesOverviewMonthView");
        this.a = gd5;
        this.b = vd5;
        this.c = qd5;
    }

    @DexIgnore
    public final gd5 a() {
        return this.a;
    }

    @DexIgnore
    public final qd5 b() {
        return this.c;
    }

    @DexIgnore
    public final vd5 c() {
        return this.b;
    }
}
