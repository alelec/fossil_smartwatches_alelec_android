package com.fossil;

import com.fossil.dq6;
import com.fossil.vx6;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.KotlinExtensions;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ix6<ResponseT, ReturnT> extends sx6<ReturnT> {
    @DexIgnore
    public /* final */ qx6 a;
    @DexIgnore
    public /* final */ dq6.a b;
    @DexIgnore
    public /* final */ fx6<zq6, ResponseT> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<ResponseT, ReturnT> extends ix6<ResponseT, ReturnT> {
        @DexIgnore
        public /* final */ cx6<ResponseT, ReturnT> d;

        @DexIgnore
        public a(qx6 qx6, dq6.a aVar, fx6<zq6, ResponseT> fx6, cx6<ResponseT, ReturnT> cx6) {
            super(qx6, aVar, fx6);
            this.d = cx6;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [retrofit2.Call, retrofit2.Call<ResponseT>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        public ReturnT a(Call<ResponseT> r1, Object[] objArr) {
            return this.d.a(r1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<ResponseT> extends ix6<ResponseT, Object> {
        @DexIgnore
        public /* final */ cx6<ResponseT, Call<ResponseT>> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public b(qx6 qx6, dq6.a aVar, fx6<zq6, ResponseT> fx6, cx6<ResponseT, Call<ResponseT>> cx6, boolean z) {
            super(qx6, aVar, fx6);
            this.d = cx6;
            this.e = z;
        }

        @DexIgnore
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            xe6 xe6 = objArr[objArr.length - 1];
            try {
                if (this.e) {
                    return KotlinExtensions.b(call2, xe6);
                }
                return KotlinExtensions.a(call2, xe6);
            } catch (Exception e2) {
                return KotlinExtensions.a(e2, (xe6<?>) xe6);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<ResponseT> extends ix6<ResponseT, Object> {
        @DexIgnore
        public /* final */ cx6<ResponseT, Call<ResponseT>> d;

        @DexIgnore
        public c(qx6 qx6, dq6.a aVar, fx6<zq6, ResponseT> fx6, cx6<ResponseT, Call<ResponseT>> cx6) {
            super(qx6, aVar, fx6);
            this.d = cx6;
        }

        @DexIgnore
        public Object a(Call<ResponseT> call, Object[] objArr) {
            Call call2 = (Call) this.d.a(call);
            xe6 xe6 = objArr[objArr.length - 1];
            try {
                return KotlinExtensions.c(call2, xe6);
            } catch (Exception e) {
                return KotlinExtensions.a(e, (xe6<?>) xe6);
            }
        }
    }

    @DexIgnore
    public ix6(qx6 qx6, dq6.a aVar, fx6<zq6, ResponseT> fx6) {
        this.a = qx6;
        this.b = aVar;
        this.c = fx6;
    }

    @DexIgnore
    public static <ResponseT, ReturnT> ix6<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, qx6 qx6) {
        Type type;
        boolean z;
        Class<rx6> cls = rx6.class;
        boolean z2 = qx6.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type a2 = vx6.a(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (vx6.b(a2) != cls || !(a2 instanceof ParameterizedType)) {
                z = false;
            } else {
                a2 = vx6.b(0, (ParameterizedType) a2);
                z = true;
            }
            type = new vx6.b((Type) null, Call.class, a2);
            annotations = ux6.a(annotations);
        } else {
            type = method.getGenericReturnType();
            z = false;
        }
        cx6 a3 = a(retrofit3, method, type, annotations);
        Type a4 = a3.a();
        if (a4 == Response.class) {
            throw vx6.a(method, "'" + vx6.b(a4).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
        } else if (a4 == cls) {
            throw vx6.a(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        } else if (!qx6.c.equals("HEAD") || Void.class.equals(a4)) {
            fx6 a5 = a(retrofit3, method, a4);
            dq6.a aVar = retrofit3.b;
            if (!z2) {
                return new a(qx6, aVar, a5, a3);
            }
            if (z) {
                return new c(qx6, aVar, a5, a3);
            }
            return new b(qx6, aVar, a5, a3, false);
        } else {
            throw vx6.a(method, "HEAD method must use Void as response type.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract ReturnT a(Call<ResponseT> call, Object[] objArr);

    @DexIgnore
    public static <ResponseT, ReturnT> cx6<ResponseT, ReturnT> a(Retrofit retrofit3, Method method, Type type, Annotation[] annotationArr) {
        try {
            return retrofit3.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw vx6.a(method, (Throwable) e, "Unable to create call adapter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT> fx6<zq6, ResponseT> a(Retrofit retrofit3, Method method, Type type) {
        try {
            return retrofit3.b(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw vx6.a(method, (Throwable) e, "Unable to create converter for %s", type);
        }
    }

    @DexIgnore
    public final ReturnT a(Object[] objArr) {
        return a(new lx6(this.a, objArr, this.b, this.c), objArr);
    }
}
