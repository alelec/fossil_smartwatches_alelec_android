package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yh1 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ cg1 CREATOR; // = new cg1((qg6) null);
    @DexIgnore
    public /* final */ nd0[] a;
    @DexIgnore
    public /* final */ w40 b;
    @DexIgnore
    public /* final */ List<byte[]> c; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> d; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> e; // = new ArrayList();

    @DexIgnore
    public yh1(nd0[] nd0Arr, w40 w40) {
        this.a = nd0Arr;
        this.b = w40;
        for (nd0 nd0 : nd0Arr) {
            ArrayList<sj1> arrayList = new ArrayList<>();
            for (qd0 qd0 : nd0.getMicroAppDeclarations()) {
                u81 u81 = new u81(qd0.b(), qd0.getMicroAppVersion(), qd0.getVariationNumber());
                arrayList.add(new sj1(u81));
                this.c.add(qd0.getData());
                pd0 customization = qd0.getCustomization();
                if (customization != null) {
                    customization.a(u81);
                    this.d.add(customization.b());
                }
            }
            rd0 microAppButton = nd0.getMicroAppButton();
            byte[] bArr = new byte[0];
            for (sj1 sj1 : arrayList) {
                bArr = cw0.a(bArr, sj1.a);
            }
            ByteBuffer order = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN);
            wg6.a(order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
            order.put(microAppButton.a());
            order.put((byte) arrayList.size());
            order.put(bArr);
            byte[] array = order.array();
            wg6.a(array, "byteBuffer.array()");
            this.e.add(array);
        }
    }

    @DexIgnore
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (nd0 a2 : this.a) {
            jSONArray.put(a2.a());
        }
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.SYSTEM_VERSION, (Object) this.b.a()), bm0.MICRO_APP_MAPPINGS, (Object) jSONArray), bm0.TOTAL_MAPPINGS, (Object) Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(yh1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            yh1 yh1 = (yh1) obj;
            return !(wg6.a(this.b, yh1.b) ^ true) && Arrays.equals(this.a, yh1.a);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.configuration.MicroAppConfiguration");
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeTypedArray(this.a, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
    }

    @DexIgnore
    public final byte[] a(List<byte[]> list) {
        byte[] bArr = new byte[0];
        for (byte[] a2 : list) {
            bArr = cw0.a(bArr, a2);
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 1).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) list.size());
        order.put(bArr);
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }
}
