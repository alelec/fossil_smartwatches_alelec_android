package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tp3 implements xp3 {
    @DexIgnore
    public <T> T a(Class<T> cls) {
        ys3<T> b = b(cls);
        if (b == null) {
            return null;
        }
        return b.get();
    }

    @DexIgnore
    public <T> Set<T> d(Class<T> cls) {
        return c(cls).get();
    }
}
