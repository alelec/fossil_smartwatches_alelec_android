package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b93 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 f;
    @DexIgnore
    public /* final */ /* synthetic */ l83 g;

    @DexIgnore
    public b93(l83 l83, AtomicReference atomicReference, String str, String str2, String str3, boolean z, ra3 ra3) {
        this.g = l83;
        this.a = atomicReference;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = z;
        this.f = ra3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.a) {
            try {
                l43 d2 = this.g.d;
                if (d2 == null) {
                    this.g.b().t().a("Failed to get user properties", t43.a(this.b), this.c, this.d);
                    this.a.set(Collections.emptyList());
                    this.a.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.b)) {
                    this.a.set(d2.a(this.c, this.d, this.e, this.f));
                } else {
                    this.a.set(d2.a(this.b, this.c, this.d, this.e));
                }
                this.g.I();
                this.a.notify();
            } catch (RemoteException e2) {
                try {
                    this.g.b().t().a("Failed to get user properties", t43.a(this.b), this.c, e2);
                    this.a.set(Collections.emptyList());
                    this.a.notify();
                } catch (Throwable th) {
                    this.a.notify();
                    throw th;
                }
            }
        }
    }
}
