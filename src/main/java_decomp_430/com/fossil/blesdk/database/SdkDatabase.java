package com.fossil.blesdk.database;

import android.content.Context;
import android.database.Cursor;
import com.fossil.cw0;
import com.fossil.gk0;
import com.fossil.h51;
import com.fossil.ie1;
import com.fossil.ii;
import com.fossil.nh;
import com.fossil.oh;
import com.fossil.q11;
import com.fossil.qg6;
import com.fossil.w81;
import com.fossil.wg6;
import com.fossil.xh;
import com.fossil.ze0;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SdkDatabase extends oh {
    @DexIgnore
    public static SdkDatabase a;
    @DexIgnore
    public static /* final */ xh b; // = new a(1, 2);
    @DexIgnore
    public static /* final */ xh c; // = new b(2, 3);
    @DexIgnore
    public static /* final */ xh d; // = new c(3, 4);
    @DexIgnore
    public static /* final */ d e; // = new d((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xh {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            iiVar.b("DROP TABLE ActivityFile");
            iiVar.b("CREATE TABLE DeviceFile(deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, PRIMARY KEY(deviceMacAddress, fileType, fileIndex))");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xh {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
            ii iiVar2 = iiVar;
            iiVar2.b("CREATE TABLE DeviceFile_New(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, isCompleted INTEGER NOT NULL)");
            Cursor d = iiVar2.d("SELECT * from DeviceFile");
            wg6.a(d, "database.query(\"SELECT * from DeviceFile\")");
            int columnIndex = d.getColumnIndex("deviceMacAddress");
            int columnIndex2 = d.getColumnIndex("fileType");
            int columnIndex3 = d.getColumnIndex("fileIndex");
            int columnIndex4 = d.getColumnIndex("rawData");
            int columnIndex5 = d.getColumnIndex("fileLength");
            int columnIndex6 = d.getColumnIndex("fileCrc");
            int columnIndex7 = d.getColumnIndex("createdTimeStamp");
            while (d.moveToNext()) {
                String string = d.getString(columnIndex);
                wg6.a(string, "oldDataCursor.getString(\u2026iceMacAddressColumnIndex)");
                byte b = (byte) d.getShort(columnIndex2);
                byte b2 = (byte) d.getShort(columnIndex3);
                byte[] blob = d.getBlob(columnIndex4);
                wg6.a(blob, "oldDataCursor.getBlob(rawDataColumnIndex)");
                long j = d.getLong(columnIndex5);
                int i = columnIndex;
                int i2 = columnIndex2;
                long j2 = d.getLong(columnIndex6);
                int i3 = columnIndex3;
                int i4 = columnIndex4;
                long j3 = d.getLong(columnIndex7);
                Cursor cursor = d;
                int i5 = columnIndex5;
                int i6 = j2 == h51.a.a(blob, q11.CRC32) ? 1 : 0;
                iiVar2.b("Insert into DeviceFile_New(deviceMacAddress, fileType, fileIndex, rawData, fileLength, fileCrc, createdTimeStamp, isCompleted) values ('" + string + "', " + b + ", " + b2 + ", X'" + cw0.a(blob, (String) null, 1) + "', " + j + ", " + j2 + ", " + j3 + ", " + i6 + ')');
                columnIndex = i;
                columnIndex2 = i2;
                columnIndex3 = i3;
                columnIndex4 = i4;
                d = cursor;
                columnIndex5 = i5;
                columnIndex6 = columnIndex6;
            }
            iiVar2.b("DROP TABLE DeviceFile");
            iiVar2.b("ALTER TABLE DeviceFile_New RENAME TO DeviceFile");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xh {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(ii iiVar) {
        }
    }

    @DexIgnore
    public abstract w81 a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* synthetic */ d(qg6 qg6) {
        }

        @DexIgnore
        public final SdkDatabase a() {
            Context a;
            Class<SdkDatabase> cls = SdkDatabase.class;
            if (SdkDatabase.a == null && (a = gk0.f.a()) != null) {
                StringBuilder b = ze0.b("com.fossil.blesdk.database.");
                b.append(gk0.f.e());
                oh.a<SdkDatabase> a2 = nh.a(a, cls, b.toString());
                a2.a(SdkDatabase.e.b(), SdkDatabase.e.c(), SdkDatabase.e.d());
                a2.a();
                SdkDatabase b2 = a2.b();
                wg6.a(b2, "Room.databaseBuilder(app\u2026                 .build()");
                SdkDatabase sdkDatabase = b2;
                oh.a<SdkDatabase> a3 = nh.a(a, cls, "device.sdbk");
                a3.a(SdkDatabase.e.b(), SdkDatabase.e.c(), SdkDatabase.e.d());
                a3.a();
                SdkDatabase b3 = a3.b();
                wg6.a(b3, "Room.databaseBuilder(app\u2026                 .build()");
                SdkDatabase sdkDatabase2 = b3;
                SdkDatabase.e.a(sdkDatabase, sdkDatabase2);
                sdkDatabase.close();
                SdkDatabase.a = sdkDatabase2;
            }
            return SdkDatabase.a;
        }

        @DexIgnore
        public final xh b() {
            return SdkDatabase.b;
        }

        @DexIgnore
        public final xh c() {
            return SdkDatabase.c;
        }

        @DexIgnore
        public final xh d() {
            return SdkDatabase.d;
        }

        @DexIgnore
        public final void a(SdkDatabase sdkDatabase, SdkDatabase sdkDatabase2) {
            List<ie1> a = sdkDatabase.a().a();
            for (ie1 a2 : a) {
                sdkDatabase2.a().a(a2);
            }
            for (ie1 ie1 : a) {
                sdkDatabase.a().a(ie1.b);
            }
        }
    }
}
