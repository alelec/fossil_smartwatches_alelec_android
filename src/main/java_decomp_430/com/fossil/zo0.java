package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo0 extends p41 {
    @DexIgnore
    public /* final */ rg1 E;
    @DexIgnore
    public /* final */ rg1 F;
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ boolean I;
    @DexIgnore
    public rg1 J;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zo0(short s, ue1 ue1, int i, int i2) {
        super(lx0.LEGACY_CLOSE_CURRENT_ACTIVITY_FILE, ue1, (i2 & 4) != 0 ? 3 : i);
        rg1 rg1 = rg1.FTC;
        this.E = rg1;
        this.F = rg1;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_GET_FILE.a).putShort(s).putInt(0).putInt(1).array();
        wg6.a(array, "ByteBuffer.allocate(11)\n\u2026ILE)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(du0.LEGACY_GET_FILE.a()).put((byte) 0).putShort(s).array();
        wg6.a(array2, "ByteBuffer.allocate(4)\n \u2026dle)\n            .array()");
        this.H = array2;
        this.J = rg1.FTD;
    }

    @DexIgnore
    public sj0 a(byte b) {
        return zh0.j.a(b);
    }

    @DexIgnore
    public boolean c(sg1 sg1) {
        return sg1.a == this.J;
    }

    @DexIgnore
    public void f(sg1 sg1) {
        byte[] bArr;
        if (this.s) {
            bArr = lg0.b.a(this.y.t, this.J, sg1.b);
        } else {
            bArr = sg1.b;
        }
        if (bArr.length == 0) {
            this.v = bn0.a(this.v, (lx0) null, (String) null, il0.RESPONSE_ERROR, (ch0) null, (sj0) null, 27);
        }
        this.C = true;
    }

    @DexIgnore
    public rg1 m() {
        return this.F;
    }

    @DexIgnore
    public byte[] o() {
        return this.G;
    }

    @DexIgnore
    public rg1 p() {
        return this.E;
    }

    @DexIgnore
    public boolean q() {
        return this.I;
    }

    @DexIgnore
    public byte[] r() {
        return this.H;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        this.C = this.v.c != il0.SUCCESS;
        return jSONObject;
    }
}
