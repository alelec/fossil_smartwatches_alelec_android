package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sg2 implements Parcelable.Creator<rg2> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        List<d12> list = rg2.h;
        LocationRequest locationRequest = null;
        String str = null;
        String str2 = null;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 != 1) {
                switch (a2) {
                    case 5:
                        list = f22.c(parcel, a, d12.CREATOR);
                        break;
                    case 6:
                        str = f22.e(parcel, a);
                        break;
                    case 7:
                        z = f22.i(parcel, a);
                        break;
                    case 8:
                        z2 = f22.i(parcel, a);
                        break;
                    case 9:
                        z3 = f22.i(parcel, a);
                        break;
                    case 10:
                        str2 = f22.e(parcel, a);
                        break;
                    default:
                        f22.v(parcel, a);
                        break;
                }
            } else {
                locationRequest = f22.a(parcel, a, LocationRequest.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new rg2(locationRequest, list, str, z, z2, z3, str2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new rg2[i];
    }
}
