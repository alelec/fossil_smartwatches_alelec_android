package com.fossil;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.lg3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mg3 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT < 18);

    @DexIgnore
    public static void a(Rect rect, float f, float f2, float f3, float f4) {
        rect.set((int) (f - f3), (int) (f2 - f4), (int) (f + f3), (int) (f2 + f4));
    }

    @DexIgnore
    public static void b(lg3 lg3, View view, FrameLayout frameLayout) {
        if (lg3 != null) {
            if (a) {
                frameLayout.setForeground((Drawable) null);
            } else {
                view.getOverlay().remove(lg3);
            }
        }
    }

    @DexIgnore
    public static void c(lg3 lg3, View view, FrameLayout frameLayout) {
        Rect rect = new Rect();
        (a ? frameLayout : view).getDrawingRect(rect);
        lg3.setBounds(rect);
        lg3.a(view, (ViewGroup) frameLayout);
    }

    @DexIgnore
    public static void a(lg3 lg3, View view, FrameLayout frameLayout) {
        c(lg3, view, frameLayout);
        if (a) {
            frameLayout.setForeground(lg3);
        } else {
            view.getOverlay().add(lg3);
        }
    }

    @DexIgnore
    public static gi3 a(SparseArray<lg3> sparseArray) {
        gi3 gi3 = new gi3();
        int i = 0;
        while (i < sparseArray.size()) {
            int keyAt = sparseArray.keyAt(i);
            lg3 valueAt = sparseArray.valueAt(i);
            if (valueAt != null) {
                gi3.put(keyAt, valueAt.f());
                i++;
            } else {
                throw new IllegalArgumentException("badgeDrawable cannot be null");
            }
        }
        return gi3;
    }

    @DexIgnore
    public static SparseArray<lg3> a(Context context, gi3 gi3) {
        SparseArray<lg3> sparseArray = new SparseArray<>(gi3.size());
        int i = 0;
        while (i < gi3.size()) {
            int keyAt = gi3.keyAt(i);
            lg3.a aVar = (lg3.a) gi3.valueAt(i);
            if (aVar != null) {
                sparseArray.put(keyAt, lg3.a(context, aVar));
                i++;
            } else {
                throw new IllegalArgumentException("BadgeDrawable's savedState cannot be null");
            }
        }
        return sparseArray;
    }
}
