package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum tf1 {
    GET_CONNECTION_PARAMETERS(v21.GET, r41.CURRENT_CONNECTION_PARAMETERS, (v21) null, (r41) null, (byte[]) null, (v21) null, 60),
    REQUEST_CONNECTION_PRIORITY(v21.SET, r41.CONNECTION_PARAMETERS_REQUEST, (v21) null, (r41) null, r41.CURRENT_CONNECTION_PARAMETERS, (v21) null, 44),
    PLAY_ANIMATION(v21.SET, r41.DIAGNOSTIC_FUNCTIONS, n61.PAIR_ANIMATION.a, (r41) null, (byte[]) null, (v21) null, 56),
    GET_OPTIMAL_PAYLOAD(r6, r7, r11, (r41) null, (byte[]) null, r11, 24),
    REQUEST_HANDS(v21.SET, r41.CALIBRATION_SETTING, k81.REQUEST_HANDS.a, (r41) null, (byte[]) null, (v21) null, 56),
    RELEASE_HANDS(v21.SET, r41.CALIBRATION_SETTING, k81.RELEASE_HANDS.a, (r41) null, (byte[]) null, (v21) null, 56),
    MOVE_HANDS(v21.SET, r41.CALIBRATION_SETTING, k81.MOVE_HANDS.a, (r41) null, (byte[]) null, (v21) null, 56),
    SET_CALIBRATION_POSITION(v21.SET, r41.HARDWARE_TEST, k81.SET_CALIBRATION_POSITION.a, (r41) null, (byte[]) null, (v21) null, 56),
    GET_CURRENT_WORKOUT_SESSION(r6, r7, r11, (r41) null, (byte[]) null, r11, 24),
    STOP_CURRENT_WORKOUT_SESSION(v21.SET, r41.WORKOUT_SESSION_PRIMARY_ID, xd1.WORKOUT_SESSION_CONTROL.a, (r41) null, (byte[]) null, (v21) null, 56),
    SET_HEARTBEAT_INTERVAL(v21.SET, r41.STREAMING_CONFIG, cc1.HEARTBEAT_INTERVAL.a, (r41) null, (byte[]) null, (v21) null, 56),
    REQUEST_DISCOVER_SERVICE(v21.SET, r41.ADVANCE_BLE_REQUEST, b11.REQUEST_DISCOVER_SERVICE.a, (r41) null, (byte[]) null, (v21) null, 56),
    CLEAN_UP_DEVICE(v21.SET, r41.DIAGNOSTIC_FUNCTIONS, b11.CLEAN_UP_DEVICE.a, (r41) null, (byte[]) null, (v21) null, 56),
    LEGACY_OTA_ENTER(v21.SET, r41.DIAGNOSTIC_FUNCTIONS, ha1.LEGACY_OTA_ENTER.a, (r41) null, (byte[]) null, ha1.LEGACY_OTA_ENTER_RESPONSE.a, 24),
    LEGACY_OTA_RESET(v21.SET, r41.DIAGNOSTIC_FUNCTIONS, ha1.LEGACY_OTA_RESET.a, (r41) null, (byte[]) null, (v21) null, 56);
    
    @DexIgnore
    public /* final */ v21 a;
    @DexIgnore
    public /* final */ r41 b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ v21 d;
    @DexIgnore
    public /* final */ r41 e;
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore
    public tf1(v21 v21, r41 r41, byte[] bArr, v21 v212, r41 r412, byte[] bArr2) {
        this.a = v21;
        this.b = r41;
        this.c = bArr;
        this.d = v212;
        this.e = r412;
        this.f = bArr2;
    }
}
