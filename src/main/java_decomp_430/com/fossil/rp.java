package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rp {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ byte[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;
    @DexIgnore
    public /* final */ List<np> d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    @Deprecated
    public rp(int i, byte[] bArr, Map<String, String> map, boolean z, long j) {
        this(i, bArr, map, a(map), z, j);
    }

    @DexIgnore
    public static Map<String, String> a(List<np> list) {
        if (list == null) {
            return null;
        }
        if (list.isEmpty()) {
            return Collections.emptyMap();
        }
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (np next : list) {
            treeMap.put(next.a(), next.b());
        }
        return treeMap;
    }

    @DexIgnore
    public rp(int i, byte[] bArr, boolean z, long j, List<np> list) {
        this(i, bArr, a(list), list, z, j);
    }

    @DexIgnore
    @Deprecated
    public rp(byte[] bArr, Map<String, String> map) {
        this(200, bArr, map, false, 0);
    }

    @DexIgnore
    public rp(int i, byte[] bArr, Map<String, String> map, List<np> list, boolean z, long j) {
        this.a = i;
        this.b = bArr;
        this.c = map;
        if (list == null) {
            this.d = null;
        } else {
            this.d = Collections.unmodifiableList(list);
        }
        this.e = z;
    }

    @DexIgnore
    public static List<np> a(Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry next : map.entrySet()) {
            arrayList.add(new np((String) next.getKey(), (String) next.getValue()));
        }
        return arrayList;
    }
}
