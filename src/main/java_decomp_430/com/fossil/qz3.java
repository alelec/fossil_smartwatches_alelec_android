package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qz3 implements xx3 {
    @DexIgnore
    public int a() {
        return 10;
    }

    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) throws yx3 {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Negative size is not allowed. Input: " + i + 'x' + i2);
        } else {
            int a = a();
            if (map != null && map.containsKey(sx3.MARGIN)) {
                a = Integer.parseInt(map.get(sx3.MARGIN).toString());
            }
            return a(a(str), i, i2, a);
        }
    }

    @DexIgnore
    public abstract boolean[] a(String str);

    @DexIgnore
    public static iy3 a(boolean[] zArr, int i, int i2, int i3) {
        int length = zArr.length;
        int i4 = i3 + length;
        int max = Math.max(i, i4);
        int max2 = Math.max(1, i2);
        int i5 = max / i4;
        iy3 iy3 = new iy3(max, max2);
        int i6 = (max - (length * i5)) / 2;
        int i7 = 0;
        while (i7 < length) {
            if (zArr[i7]) {
                iy3.a(i6, 0, i5, max2);
            }
            i7++;
            i6 += i5;
        }
        return iy3;
    }

    @DexIgnore
    public static int a(boolean[] zArr, int i, int[] iArr, boolean z) {
        int length = iArr.length;
        int i2 = i;
        boolean z2 = z;
        int i3 = 0;
        int i4 = 0;
        while (i3 < length) {
            int i5 = iArr[i3];
            int i6 = i2;
            int i7 = 0;
            while (i7 < i5) {
                zArr[i6] = z2;
                i7++;
                i6++;
            }
            i4 += i5;
            z2 = !z2;
            i3++;
            i2 = i6;
        }
        return i4;
    }
}
