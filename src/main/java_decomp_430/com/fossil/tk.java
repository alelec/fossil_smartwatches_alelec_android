package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.util.Xml;
import android.view.InflateException;
import com.facebook.LegacyTokenHelper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.g7;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tk {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements TypeEvaluator<g7.b[]> {
        @DexIgnore
        public g7.b[] a;

        @DexIgnore
        /* renamed from: a */
        public g7.b[] evaluate(float f, g7.b[] bVarArr, g7.b[] bVarArr2) {
            if (g7.a(bVarArr, bVarArr2)) {
                if (!g7.a(this.a, bVarArr)) {
                    this.a = g7.a(bVarArr);
                }
                for (int i = 0; i < bVarArr.length; i++) {
                    this.a[i].a(bVarArr[i], bVarArr2[i], f);
                }
                return this.a;
            }
            throw new IllegalArgumentException("Can't interpolate between two incompatible pathData");
        }
    }

    @DexIgnore
    public static Animator a(Context context, int i) throws Resources.NotFoundException {
        if (Build.VERSION.SDK_INT >= 24) {
            return AnimatorInflater.loadAnimator(context, i);
        }
        return a(context, context.getResources(), context.getTheme(), i);
    }

    @DexIgnore
    public static boolean a(int i) {
        return i >= 28 && i <= 31;
    }

    @DexIgnore
    public static Animator a(Context context, Resources resources, Resources.Theme theme, int i) throws Resources.NotFoundException {
        return a(context, resources, theme, i, 1.0f);
    }

    @DexIgnore
    public static Animator a(Context context, Resources resources, Resources.Theme theme, int i, float f) throws Resources.NotFoundException {
        XmlResourceParser xmlResourceParser = null;
        try {
            XmlResourceParser animation = resources.getAnimation(i);
            Animator a2 = a(context, resources, theme, (XmlPullParser) animation, f);
            if (animation != null) {
                animation.close();
            }
            return a2;
        } catch (XmlPullParserException e) {
            Resources.NotFoundException notFoundException = new Resources.NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException.initCause(e);
            throw notFoundException;
        } catch (IOException e2) {
            Resources.NotFoundException notFoundException2 = new Resources.NotFoundException("Can't load animation resource ID #0x" + Integer.toHexString(i));
            notFoundException2.initCause(e2);
            throw notFoundException2;
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v6, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v26, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v29, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static PropertyValuesHolder a(TypedArray typedArray, int i, int i2, int i3, String str) {
        int i4;
        int i5;
        int i6;
        PropertyValuesHolder propertyValuesHolder;
        float f;
        float f2;
        float f3;
        PropertyValuesHolder propertyValuesHolder2;
        TypedValue peekValue = typedArray.peekValue(i2);
        boolean z = peekValue != null;
        int i7 = z ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i3);
        boolean z2 = peekValue2 != null;
        int i8 = z2 ? peekValue2.type : 0;
        if (i == 4) {
            i = ((!z || !a(i7)) && (!z2 || !a(i8))) ? 0 : 3;
        }
        boolean z3 = i == 0;
        PropertyValuesHolder propertyValuesHolder3 = null;
        if (i == 2) {
            String string = typedArray.getString(i2);
            String string2 = typedArray.getString(i3);
            g7.b[] a2 = g7.a(string);
            g7.b[] a3 = g7.a(string2);
            if (a2 == null && a3 == null) {
                return null;
            }
            if (a2 != null) {
                a aVar = new a();
                if (a3 == null) {
                    propertyValuesHolder2 = PropertyValuesHolder.ofObject(str, aVar, new Object[]{a2});
                } else if (g7.a(a2, a3)) {
                    propertyValuesHolder2 = PropertyValuesHolder.ofObject(str, aVar, new Object[]{a2, a3});
                } else {
                    throw new InflateException(" Can't morph from " + string + " to " + string2);
                }
                return propertyValuesHolder2;
            } else if (a3 == null) {
                return null;
            } else {
                return PropertyValuesHolder.ofObject(str, new a(), new Object[]{a3});
            }
        } else {
            uk a4 = i == 3 ? uk.a() : null;
            if (z3) {
                if (z) {
                    if (i7 == 5) {
                        f2 = typedArray.getDimension(i2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else {
                        f2 = typedArray.getFloat(i2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    if (z2) {
                        if (i8 == 5) {
                            f3 = typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            f3 = typedArray.getFloat(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        }
                        propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{f2, f3});
                    } else {
                        propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{f2});
                    }
                } else {
                    if (i8 == 5) {
                        f = typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else {
                        f = typedArray.getFloat(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    propertyValuesHolder = PropertyValuesHolder.ofFloat(str, new float[]{f});
                }
                propertyValuesHolder3 = propertyValuesHolder;
            } else if (z) {
                if (i7 == 5) {
                    i5 = (int) typedArray.getDimension(i2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (a(i7)) {
                    i5 = typedArray.getColor(i2, 0);
                } else {
                    i5 = typedArray.getInt(i2, 0);
                }
                if (z2) {
                    if (i8 == 5) {
                        i6 = (int) typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    } else if (a(i8)) {
                        i6 = typedArray.getColor(i3, 0);
                    } else {
                        i6 = typedArray.getInt(i3, 0);
                    }
                    propertyValuesHolder3 = PropertyValuesHolder.ofInt(str, new int[]{i5, i6});
                } else {
                    propertyValuesHolder3 = PropertyValuesHolder.ofInt(str, new int[]{i5});
                }
            } else if (z2) {
                if (i8 == 5) {
                    i4 = (int) typedArray.getDimension(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                } else if (a(i8)) {
                    i4 = typedArray.getColor(i3, 0);
                } else {
                    i4 = typedArray.getInt(i3, 0);
                }
                propertyValuesHolder3 = PropertyValuesHolder.ofInt(str, new int[]{i4});
            }
            if (propertyValuesHolder3 == null || a4 == null) {
                return propertyValuesHolder3;
            }
            propertyValuesHolder3.setEvaluator(a4);
            return propertyValuesHolder3;
        }
    }

    @DexIgnore
    public static void a(ValueAnimator valueAnimator, TypedArray typedArray, TypedArray typedArray2, float f, XmlPullParser xmlPullParser) {
        long b = (long) e7.b(typedArray, xmlPullParser, "duration", 1, 300);
        long b2 = (long) e7.b(typedArray, xmlPullParser, "startOffset", 2, 0);
        int b3 = e7.b(typedArray, xmlPullParser, LegacyTokenHelper.JSON_VALUE_TYPE, 7, 4);
        if (e7.a(xmlPullParser, "valueFrom") && e7.a(xmlPullParser, "valueTo")) {
            if (b3 == 4) {
                b3 = a(typedArray, 5, 6);
            }
            PropertyValuesHolder a2 = a(typedArray, b3, 5, 6, "");
            if (a2 != null) {
                valueAnimator.setValues(new PropertyValuesHolder[]{a2});
            }
        }
        valueAnimator.setDuration(b);
        valueAnimator.setStartDelay(b2);
        valueAnimator.setRepeatCount(e7.b(typedArray, xmlPullParser, "repeatCount", 3, 0));
        valueAnimator.setRepeatMode(e7.b(typedArray, xmlPullParser, "repeatMode", 4, 1));
        if (typedArray2 != null) {
            a(valueAnimator, typedArray2, b3, f, xmlPullParser);
        }
    }

    @DexIgnore
    public static void a(ValueAnimator valueAnimator, TypedArray typedArray, int i, float f, XmlPullParser xmlPullParser) {
        ObjectAnimator objectAnimator = (ObjectAnimator) valueAnimator;
        String a2 = e7.a(typedArray, xmlPullParser, "pathData", 1);
        if (a2 != null) {
            String a3 = e7.a(typedArray, xmlPullParser, "propertyXName", 2);
            String a4 = e7.a(typedArray, xmlPullParser, "propertyYName", 3);
            if (i != 2) {
            }
            if (a3 == null && a4 == null) {
                throw new InflateException(typedArray.getPositionDescription() + " propertyXName or propertyYName is needed for PathData");
            }
            a(g7.b(a2), objectAnimator, f * 0.5f, a3, a4);
            return;
        }
        objectAnimator.setPropertyName(e7.a(typedArray, xmlPullParser, "propertyName", 0));
    }

    @DexIgnore
    public static void a(Path path, ObjectAnimator objectAnimator, float f, String str, String str2) {
        PropertyValuesHolder propertyValuesHolder;
        Path path2 = path;
        ObjectAnimator objectAnimator2 = objectAnimator;
        String str3 = str;
        String str4 = str2;
        PathMeasure pathMeasure = new PathMeasure(path2, false);
        ArrayList arrayList = new ArrayList();
        arrayList.add(Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        do {
            f2 += pathMeasure.getLength();
            arrayList.add(Float.valueOf(f2));
        } while (pathMeasure.nextContour());
        PathMeasure pathMeasure2 = new PathMeasure(path2, false);
        int min = Math.min(100, ((int) (f2 / f)) + 1);
        float[] fArr = new float[min];
        float[] fArr2 = new float[min];
        float[] fArr3 = new float[2];
        float f3 = f2 / ((float) (min - 1));
        int i = 0;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i2 = 0;
        while (true) {
            propertyValuesHolder = null;
            if (i >= min) {
                break;
            }
            pathMeasure2.getPosTan(f4 - ((Float) arrayList.get(i2)).floatValue(), fArr3, (float[]) null);
            fArr[i] = fArr3[0];
            fArr2[i] = fArr3[1];
            f4 += f3;
            int i3 = i2 + 1;
            if (i3 < arrayList.size() && f4 > ((Float) arrayList.get(i3)).floatValue()) {
                pathMeasure2.nextContour();
                i2 = i3;
            }
            i++;
        }
        PropertyValuesHolder ofFloat = str3 != null ? PropertyValuesHolder.ofFloat(str3, fArr) : null;
        if (str4 != null) {
            propertyValuesHolder = PropertyValuesHolder.ofFloat(str4, fArr2);
        }
        if (ofFloat == null) {
            objectAnimator2.setValues(new PropertyValuesHolder[]{propertyValuesHolder});
        } else if (propertyValuesHolder == null) {
            objectAnimator2.setValues(new PropertyValuesHolder[]{ofFloat});
        } else {
            objectAnimator2.setValues(new PropertyValuesHolder[]{ofFloat, propertyValuesHolder});
        }
    }

    @DexIgnore
    public static Animator a(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, float f) throws XmlPullParserException, IOException {
        return a(context, resources, theme, xmlPullParser, Xml.asAttributeSet(xmlPullParser), (AnimatorSet) null, 0, f);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b8  */
    public static Animator a(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, AttributeSet attributeSet, AnimatorSet animatorSet, int i, float f) throws XmlPullParserException, IOException {
        int i2;
        Resources resources2 = resources;
        Resources.Theme theme2 = theme;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        AnimatorSet animatorSet2 = animatorSet;
        int depth = xmlPullParser.getDepth();
        AnimatorSet animatorSet3 = null;
        ArrayList arrayList = null;
        while (true) {
            int next = xmlPullParser.next();
            i2 = 0;
            if ((next != 3 || xmlPullParser.getDepth() > depth) && next != 1) {
                if (next == 2) {
                    String name = xmlPullParser.getName();
                    if (name.equals("objectAnimator")) {
                        animatorSet3 = a(context, resources, theme, attributeSet, f, xmlPullParser);
                    } else if (name.equals("animator")) {
                        animatorSet3 = a(context, resources, theme, attributeSet, (ValueAnimator) null, f, xmlPullParser);
                    } else {
                        if (name.equals("set")) {
                            AnimatorSet animatorSet4 = new AnimatorSet();
                            TypedArray a2 = e7.a(resources2, theme2, attributeSet, pk.h);
                            a(context, resources, theme, xmlPullParser, attributeSet, animatorSet4, e7.b(a2, xmlPullParser2, "ordering", 0, 0), f);
                            a2.recycle();
                            Context context2 = context;
                            animatorSet3 = animatorSet4;
                        } else if (name.equals("propertyValuesHolder")) {
                            PropertyValuesHolder[] a3 = a(context, resources2, theme2, xmlPullParser2, Xml.asAttributeSet(xmlPullParser));
                            if (a3 != null && (animatorSet3 instanceof ValueAnimator)) {
                                ((ValueAnimator) animatorSet3).setValues(a3);
                            }
                            i2 = 1;
                        } else {
                            throw new RuntimeException("Unknown animator name: " + xmlPullParser.getName());
                        }
                        if (animatorSet2 != null && i2 == 0) {
                            if (arrayList == null) {
                                arrayList = new ArrayList();
                            }
                            arrayList.add(animatorSet3);
                        }
                    }
                    Context context3 = context;
                    if (arrayList == null) {
                    }
                    arrayList.add(animatorSet3);
                }
            }
        }
        if (!(animatorSet2 == null || arrayList == null)) {
            Animator[] animatorArr = new Animator[arrayList.size()];
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                animatorArr[i2] = (Animator) it.next();
                i2++;
            }
            if (i == 0) {
                animatorSet2.playTogether(animatorArr);
            } else {
                animatorSet2.playSequentially(animatorArr);
            }
        }
        return animatorSet3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
    public static PropertyValuesHolder[] a(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        PropertyValuesHolder[] propertyValuesHolderArr = null;
        ArrayList arrayList = null;
        while (true) {
            int eventType = xmlPullParser.getEventType();
            if (eventType == 3 || eventType == 1) {
                if (arrayList != null) {
                    int size = arrayList.size();
                    propertyValuesHolderArr = new PropertyValuesHolder[size];
                    for (int i = 0; i < size; i++) {
                        propertyValuesHolderArr[i] = (PropertyValuesHolder) arrayList.get(i);
                    }
                }
            } else if (eventType != 2) {
                xmlPullParser.next();
            } else {
                if (xmlPullParser.getName().equals("propertyValuesHolder")) {
                    TypedArray a2 = e7.a(resources, theme, attributeSet, pk.i);
                    String a3 = e7.a(a2, xmlPullParser2, "propertyName", 3);
                    int b = e7.b(a2, xmlPullParser2, LegacyTokenHelper.JSON_VALUE_TYPE, 2, 4);
                    int i2 = b;
                    PropertyValuesHolder a4 = a(context, resources, theme, xmlPullParser, a3, b);
                    if (a4 == null) {
                        a4 = a(a2, i2, 0, 1, a3);
                    }
                    if (a4 != null) {
                        if (arrayList == null) {
                            arrayList = new ArrayList();
                        }
                        arrayList.add(a4);
                    }
                    a2.recycle();
                } else {
                    Resources resources2 = resources;
                    Resources.Theme theme2 = theme;
                    AttributeSet attributeSet2 = attributeSet;
                }
                xmlPullParser.next();
            }
        }
        if (arrayList != null) {
        }
        return propertyValuesHolderArr;
    }

    @DexIgnore
    public static int a(Resources resources, Resources.Theme theme, AttributeSet attributeSet, XmlPullParser xmlPullParser) {
        TypedArray a2 = e7.a(resources, theme, attributeSet, pk.j);
        int i = 0;
        TypedValue b = e7.b(a2, xmlPullParser, "value", 0);
        if ((b != null) && a(b.type)) {
            i = 3;
        }
        a2.recycle();
        return i;
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i, int i2) {
        TypedValue peekValue = typedArray.peekValue(i);
        boolean z = true;
        boolean z2 = peekValue != null;
        int i3 = z2 ? peekValue.type : 0;
        TypedValue peekValue2 = typedArray.peekValue(i2);
        if (peekValue2 == null) {
            z = false;
        }
        int i4 = z ? peekValue2.type : 0;
        if ((!z2 || !a(i3)) && (!z || !a(i4))) {
            return 0;
        }
        return 3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00e3  */
    public static PropertyValuesHolder a(Context context, Resources resources, Resources.Theme theme, XmlPullParser xmlPullParser, String str, int i) throws XmlPullParserException, IOException {
        int size;
        int i2;
        float fraction;
        float fraction2;
        PropertyValuesHolder propertyValuesHolder = null;
        int i3 = i;
        ArrayList arrayList = null;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 3 || next == 1) {
                if (arrayList != null && (size = arrayList.size()) > 0) {
                    Keyframe keyframe = (Keyframe) arrayList.get(0);
                    Keyframe keyframe2 = (Keyframe) arrayList.get(size - 1);
                    fraction = keyframe2.getFraction();
                    if (fraction < 1.0f) {
                        if (fraction < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            keyframe2.setFraction(1.0f);
                        } else {
                            arrayList.add(arrayList.size(), a(keyframe2, 1.0f));
                            size++;
                        }
                    }
                    fraction2 = keyframe.getFraction();
                    if (fraction2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (fraction2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            keyframe.setFraction(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        } else {
                            arrayList.add(0, a(keyframe, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
                            size++;
                        }
                    }
                    Keyframe[] keyframeArr = new Keyframe[size];
                    arrayList.toArray(keyframeArr);
                    for (i2 = 0; i2 < size; i2++) {
                        Keyframe keyframe3 = keyframeArr[i2];
                        if (keyframe3.getFraction() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            if (i2 == 0) {
                                keyframe3.setFraction(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            } else {
                                int i4 = size - 1;
                                if (i2 == i4) {
                                    keyframe3.setFraction(1.0f);
                                } else {
                                    int i5 = i2 + 1;
                                    int i6 = i2;
                                    while (i5 < i4 && keyframeArr[i5].getFraction() < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                        i6 = i5;
                                        i5++;
                                    }
                                    a(keyframeArr, keyframeArr[i6 + 1].getFraction() - keyframeArr[i2 - 1].getFraction(), i2, i6);
                                }
                            }
                        }
                    }
                    propertyValuesHolder = PropertyValuesHolder.ofKeyframe(str, keyframeArr);
                    if (i3 == 3) {
                        propertyValuesHolder.setEvaluator(uk.a());
                    }
                }
            } else if (xmlPullParser.getName().equals("keyframe")) {
                if (i3 == 4) {
                    i3 = a(resources, theme, Xml.asAttributeSet(xmlPullParser), xmlPullParser);
                }
                Keyframe a2 = a(context, resources, theme, Xml.asAttributeSet(xmlPullParser), i3, xmlPullParser);
                if (a2 != null) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(a2);
                }
                xmlPullParser.next();
            }
        }
        Keyframe keyframe4 = (Keyframe) arrayList.get(0);
        Keyframe keyframe22 = (Keyframe) arrayList.get(size - 1);
        fraction = keyframe22.getFraction();
        if (fraction < 1.0f) {
        }
        fraction2 = keyframe4.getFraction();
        if (fraction2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
        }
        Keyframe[] keyframeArr2 = new Keyframe[size];
        arrayList.toArray(keyframeArr2);
        while (i2 < size) {
        }
        propertyValuesHolder = PropertyValuesHolder.ofKeyframe(str, keyframeArr2);
        if (i3 == 3) {
        }
        return propertyValuesHolder;
    }

    @DexIgnore
    public static Keyframe a(Keyframe keyframe, float f) {
        if (keyframe.getType() == Float.TYPE) {
            return Keyframe.ofFloat(f);
        }
        if (keyframe.getType() == Integer.TYPE) {
            return Keyframe.ofInt(f);
        }
        return Keyframe.ofObject(f);
    }

    @DexIgnore
    public static void a(Keyframe[] keyframeArr, float f, int i, int i2) {
        float f2 = f / ((float) ((i2 - i) + 2));
        while (i <= i2) {
            keyframeArr[i].setFraction(keyframeArr[i - 1].getFraction() + f2);
            i++;
        }
    }

    @DexIgnore
    public static Keyframe a(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, int i, XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        Keyframe keyframe;
        TypedArray a2 = e7.a(resources, theme, attributeSet, pk.j);
        float a3 = e7.a(a2, xmlPullParser, "fraction", 3, -1.0f);
        TypedValue b = e7.b(a2, xmlPullParser, "value", 0);
        boolean z = b != null;
        if (i == 4) {
            i = (!z || !a(b.type)) ? 0 : 3;
        }
        if (z) {
            if (i == 0) {
                keyframe = Keyframe.ofFloat(a3, e7.a(a2, xmlPullParser, "value", 0, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            } else if (i == 1 || i == 3) {
                keyframe = Keyframe.ofInt(a3, e7.b(a2, xmlPullParser, "value", 0, 0));
            } else {
                keyframe = null;
            }
        } else if (i == 0) {
            keyframe = Keyframe.ofFloat(a3);
        } else {
            keyframe = Keyframe.ofInt(a3);
        }
        int c = e7.c(a2, xmlPullParser, "interpolator", 1, 0);
        if (c > 0) {
            keyframe.setInterpolator(sk.a(context, c));
        }
        a2.recycle();
        return keyframe;
    }

    @DexIgnore
    public static ObjectAnimator a(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, float f, XmlPullParser xmlPullParser) throws Resources.NotFoundException {
        ObjectAnimator objectAnimator = new ObjectAnimator();
        a(context, resources, theme, attributeSet, objectAnimator, f, xmlPullParser);
        return objectAnimator;
    }

    @DexIgnore
    public static ValueAnimator a(Context context, Resources resources, Resources.Theme theme, AttributeSet attributeSet, ValueAnimator valueAnimator, float f, XmlPullParser xmlPullParser) throws Resources.NotFoundException {
        TypedArray a2 = e7.a(resources, theme, attributeSet, pk.g);
        TypedArray a3 = e7.a(resources, theme, attributeSet, pk.k);
        if (valueAnimator == null) {
            valueAnimator = new ValueAnimator();
        }
        a(valueAnimator, a2, a3, f, xmlPullParser);
        int c = e7.c(a2, xmlPullParser, "interpolator", 0, 0);
        if (c > 0) {
            valueAnimator.setInterpolator(sk.a(context, c));
        }
        a2.recycle();
        if (a3 != null) {
            a3.recycle();
        }
        return valueAnimator;
    }
}
