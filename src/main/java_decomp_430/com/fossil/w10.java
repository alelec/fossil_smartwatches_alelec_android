package com.fossil;

import android.content.Context;
import java.io.IOException;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w10 extends ja6<a20> {
    @DexIgnore
    public fb6 g;

    @DexIgnore
    public w10(Context context, c20 c20, b96 b96, ka6 ka6) throws IOException {
        super(context, c20, b96, ka6, 100);
    }

    @DexIgnore
    public void a(fb6 fb6) {
        this.g = fb6;
    }

    @DexIgnore
    public String c() {
        UUID randomUUID = UUID.randomUUID();
        return "sa" + "_" + randomUUID.toString() + "_" + this.c.a() + ".tap";
    }

    @DexIgnore
    public int e() {
        fb6 fb6 = this.g;
        return fb6 == null ? w10.super.e() : fb6.c;
    }

    @DexIgnore
    public int f() {
        fb6 fb6 = this.g;
        return fb6 == null ? w10.super.f() : fb6.d;
    }
}
