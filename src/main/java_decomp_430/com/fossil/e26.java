package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e26 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Bundle d;
    }

    @DexIgnore
    public static boolean a(Context context, a aVar) {
        String str;
        if (context == null) {
            str = "send fail, invalid argument";
        } else if (j26.a(aVar.b)) {
            str = "send fail, action is null";
        } else {
            String str2 = null;
            if (!j26.a(aVar.a)) {
                str2 = aVar.a + ".permission.MM_MESSAGE";
            }
            Intent intent = new Intent(aVar.b);
            Bundle bundle = aVar.d;
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra("_mmessage_sdkVersion", 587268097);
            intent.putExtra("_mmessage_appPackage", packageName);
            intent.putExtra("_mmessage_content", aVar.c);
            intent.putExtra("_mmessage_checksum", f26.a(aVar.c, 587268097, packageName));
            context.sendBroadcast(intent, str2);
            h26.d("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str2);
            return true;
        }
        h26.a("MicroMsg.SDK.MMessage", str);
        return false;
    }
}
