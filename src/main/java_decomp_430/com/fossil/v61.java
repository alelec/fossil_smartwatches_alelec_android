package com.fossil;

import android.bluetooth.BluetoothProfile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v61 implements BluetoothProfile.ServiceListener {
    @DexIgnore
    public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
        oa1 oa1 = oa1.a;
        Object[] objArr = {Integer.valueOf(i), bluetoothProfile};
        if (i == 4) {
            s81 s81 = s81.d;
            s81.b = bluetoothProfile;
        }
    }

    @DexIgnore
    public void onServiceDisconnected(int i) {
        oa1 oa1 = oa1.a;
        new Object[1][0] = Integer.valueOf(i);
        if (i == 4) {
            s81 s81 = s81.d;
            s81.b = null;
        }
    }
}
