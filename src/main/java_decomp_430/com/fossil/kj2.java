package com.fossil;

import com.fossil.fn2;
import com.fossil.sj2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj2 extends fn2<kj2, a> implements to2 {
    @DexIgnore
    public static /* final */ kj2 zzh;
    @DexIgnore
    public static volatile yo2<kj2> zzi;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public sj2 zze;
    @DexIgnore
    public sj2 zzf;
    @DexIgnore
    public boolean zzg;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<kj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(kj2.zzh);
        }

        @DexIgnore
        public final a a(int i) {
            f();
            ((kj2) this.b).b(i);
            return this;
        }

        @DexIgnore
        public final boolean j() {
            return ((kj2) this.b).q();
        }

        @DexIgnore
        public final sj2 k() {
            return ((kj2) this.b).r();
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(sj2.a aVar) {
            f();
            ((kj2) this.b).a(aVar);
            return this;
        }

        @DexIgnore
        public final a a(sj2 sj2) {
            f();
            ((kj2) this.b).a(sj2);
            return this;
        }

        @DexIgnore
        public final a a(boolean z) {
            f();
            ((kj2) this.b).a(z);
            return this;
        }
    }

    /*
    static {
        kj2 kj2 = new kj2();
        zzh = kj2;
        fn2.a(kj2.class, kj2);
    }
    */

    @DexIgnore
    public static a v() {
        return (a) zzh.h();
    }

    @DexIgnore
    public final void a(sj2.a aVar) {
        this.zze = (sj2) aVar.i();
        this.zzc |= 2;
    }

    @DexIgnore
    public final void b(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int o() {
        return this.zzd;
    }

    @DexIgnore
    public final sj2 p() {
        sj2 sj2 = this.zze;
        return sj2 == null ? sj2.B() : sj2;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final sj2 r() {
        sj2 sj2 = this.zzf;
        return sj2 == null ? sj2.B() : sj2;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final boolean t() {
        return this.zzg;
    }

    @DexIgnore
    public final void a(sj2 sj2) {
        if (sj2 != null) {
            this.zzf = sj2;
            this.zzc |= 4;
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void a(boolean z) {
        this.zzc |= 8;
        this.zzg = z;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new kj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\t\u0001\u0003\t\u0002\u0004\u0007\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                yo2<kj2> yo2 = zzi;
                if (yo2 == null) {
                    synchronized (kj2.class) {
                        yo2 = zzi;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzh);
                            zzi = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
