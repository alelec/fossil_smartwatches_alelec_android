package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol6 {
    @DexIgnore
    public static final String a(xe6<?> xe6) {
        Object obj;
        wg6.b(xe6, "$this$toDebugString");
        if (xe6 instanceof vl6) {
            return xe6.toString();
        }
        try {
            mc6.a aVar = mc6.Companion;
            obj = mc6.m1constructorimpl(xe6 + '@' + b(xe6));
        } catch (Throwable th) {
            mc6.a aVar2 = mc6.Companion;
            obj = mc6.m1constructorimpl(nc6.a(th));
        }
        Throwable r2 = mc6.m4exceptionOrNullimpl(obj);
        String str = obj;
        if (r2 != null) {
            str = xe6.getClass().getName() + '@' + b(xe6);
        }
        return (String) str;
    }

    @DexIgnore
    public static final String b(Object obj) {
        wg6.b(obj, "$this$hexAddress");
        String hexString = Integer.toHexString(System.identityHashCode(obj));
        wg6.a((Object) hexString, "Integer.toHexString(System.identityHashCode(this))");
        return hexString;
    }

    @DexIgnore
    public static final String a(Object obj) {
        wg6.b(obj, "$this$classSimpleName");
        String simpleName = obj.getClass().getSimpleName();
        wg6.a((Object) simpleName, "this::class.java.simpleName");
        return simpleName;
    }
}
