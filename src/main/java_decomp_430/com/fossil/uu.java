package com.fossil;

import android.os.Process;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu implements ExecutorService {
    @DexIgnore
    public static /* final */ long b; // = TimeUnit.SECONDS.toMillis(10);
    @DexIgnore
    public static volatile int c;
    @DexIgnore
    public /* final */ ExecutorService a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ThreadFactory {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ c b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends Thread {
            @DexIgnore
            public a(Runnable runnable, String str) {
                super(runnable, str);
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(9);
                if (b.this.c) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    b.this.b.a(th);
                }
            }
        }

        @DexIgnore
        public b(String str, c cVar, boolean z) {
            this.a = str;
            this.b = cVar;
            this.c = z;
        }

        @DexIgnore
        public synchronized Thread newThread(Runnable runnable) {
            a aVar;
            aVar = new a(runnable, "glide-" + this.a + "-thread-" + this.d);
            this.d = this.d + 1;
            return aVar;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        public static final c a = new b();
        @DexIgnore
        public static final c b = a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements c {
            @DexIgnore
            public void a(Throwable th) {
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements c {
            @DexIgnore
            public void a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uu$c$c")
        /* renamed from: com.fossil.uu$c$c  reason: collision with other inner class name */
        public class C0047c implements c {
            @DexIgnore
            public void a(Throwable th) {
                if (th != null) {
                    throw new RuntimeException("Request threw uncaught throwable", th);
                }
            }
        }

        /*
        static {
            new a();
            new C0047c();
        }
        */

        @DexIgnore
        void a(Throwable th);
    }

    @DexIgnore
    public uu(ExecutorService executorService) {
        this.a = executorService;
    }

    @DexIgnore
    public static int a() {
        if (c == 0) {
            c = Math.min(4, vu.a());
        }
        return c;
    }

    @DexIgnore
    public static a b() {
        int i = a() >= 4 ? 2 : 1;
        a aVar = new a(true);
        aVar.a(i);
        aVar.a("animation");
        return aVar;
    }

    @DexIgnore
    public static uu c() {
        return b().a();
    }

    @DexIgnore
    public static a d() {
        a aVar = new a(true);
        aVar.a(1);
        aVar.a("disk-cache");
        return aVar;
    }

    @DexIgnore
    public static uu e() {
        return d().a();
    }

    @DexIgnore
    public static a f() {
        a aVar = new a(false);
        aVar.a(a());
        aVar.a("source");
        return aVar;
    }

    @DexIgnore
    public static uu g() {
        return f().a();
    }

    @DexIgnore
    public static uu h() {
        return new uu(new ThreadPoolExecutor(0, Integer.MAX_VALUE, b, TimeUnit.MILLISECONDS, new SynchronousQueue(), new b("source-unlimited", c.b, false)));
    }

    @DexIgnore
    public boolean awaitTermination(long j, TimeUnit timeUnit) throws InterruptedException {
        return this.a.awaitTermination(j, timeUnit);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.a.execute(runnable);
    }

    @DexIgnore
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
        return this.a.invokeAll(collection);
    }

    @DexIgnore
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
        return this.a.invokeAny(collection);
    }

    @DexIgnore
    public boolean isShutdown() {
        return this.a.isShutdown();
    }

    @DexIgnore
    public boolean isTerminated() {
        return this.a.isTerminated();
    }

    @DexIgnore
    public void shutdown() {
        this.a.shutdown();
    }

    @DexIgnore
    public List<Runnable> shutdownNow() {
        return this.a.shutdownNow();
    }

    @DexIgnore
    public Future<?> submit(Runnable runnable) {
        return this.a.submit(runnable);
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public c d; // = c.b;
        @DexIgnore
        public String e;
        @DexIgnore
        public long f;

        @DexIgnore
        public a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public a a(int i) {
            this.b = i;
            this.c = i;
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        public uu a() {
            if (!TextUtils.isEmpty(this.e)) {
                ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(this.b, this.c, this.f, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new b(this.e, this.d, this.a));
                if (this.f != 0) {
                    threadPoolExecutor.allowCoreThreadTimeOut(true);
                }
                return new uu(threadPoolExecutor);
            }
            throw new IllegalArgumentException("Name must be non-null and non-empty, but given: " + this.e);
        }
    }

    @DexIgnore
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException {
        return this.a.invokeAll(collection, j, timeUnit);
    }

    @DexIgnore
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return this.a.invokeAny(collection, j, timeUnit);
    }

    @DexIgnore
    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.a.submit(runnable, t);
    }

    @DexIgnore
    public <T> Future<T> submit(Callable<T> callable) {
        return this.a.submit(callable);
    }
}
