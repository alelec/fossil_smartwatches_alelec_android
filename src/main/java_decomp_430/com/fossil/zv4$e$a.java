package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.SetAlarms$SetAlarmsBroadcastReceiver$receive$1", f = "SetAlarms.kt", l = {43, 45, 58}, m = "invokeSuspend")
public final class zv4$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Intent $intent;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SetAlarms.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public zv4$e$a(SetAlarms.e eVar, Intent intent, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
        this.$intent = intent;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        zv4$e$a zv4_e_a = new zv4$e$a(this.this$0, this.$intent, xe6);
        zv4_e_a.p$ = (il6) obj;
        return zv4_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((zv4$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v8, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.SetAlarms] */
    public final Object invokeSuspend(Object obj) {
        Alarm alarm;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            alarm = SetAlarms.this.h.getAlarmById(SetAlarms.this.e().a().getUri());
            if (this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive success");
                if (alarm == null) {
                    AlarmsRepository a2 = SetAlarms.this.h;
                    Alarm a3 = SetAlarms.this.e().a();
                    this.L$0 = il6;
                    this.L$1 = alarm;
                    this.label = 1;
                    if (a2.insertAlarm(a3, this) == a) {
                        return a;
                    }
                } else {
                    AlarmsRepository a4 = SetAlarms.this.h;
                    Alarm a5 = SetAlarms.this.e().a();
                    this.L$0 = il6;
                    this.L$1 = alarm;
                    this.label = 2;
                    if (a4.updateAlarm(a5, this) == a) {
                        return a;
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().d(SetAlarms.j.a(), "onReceive failed");
                Alarm copy$default = Alarm.copy$default(SetAlarms.this.e().a(), (String) null, (String) null, (String) null, (String) null, 0, 0, (int[]) null, false, false, (String) null, (String) null, 0, 4095, (Object) null);
                if (alarm == null) {
                    copy$default.setActive(false);
                    AlarmsRepository a6 = SetAlarms.this.h;
                    this.L$0 = il6;
                    this.L$1 = alarm;
                    this.L$2 = copy$default;
                    this.label = 3;
                    if (a6.insertAlarm(copy$default, this) == a) {
                        return a;
                    }
                    alarm = copy$default;
                }
                SetAlarms.this.a(this.$intent, alarm);
                return cd6.a;
            }
        } else if (i == 1 || i == 2) {
            Alarm alarm2 = (Alarm) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 3) {
            Alarm alarm3 = (Alarm) this.L$1;
            il6 il63 = (il6) this.L$0;
            nc6.a(obj);
            alarm = (Alarm) this.L$2;
            SetAlarms.this.a(this.$intent, alarm);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        jl4 c = AnalyticsHelper.f.c("set_alarms");
        if (c != null) {
            c.a("");
        }
        AnalyticsHelper.f.e("set_alarms");
        Object r1 = SetAlarms.this;
        r1.a(new SetAlarms.d(r1.e().a()));
        return cd6.a;
    }
}
