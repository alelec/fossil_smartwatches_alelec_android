package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseEditDialogFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rz4 {
    @DexIgnore
    void a(QuickResponseEditDialogFragment quickResponseEditDialogFragment);

    @DexIgnore
    void a(QuickResponseFragment quickResponseFragment);
}
