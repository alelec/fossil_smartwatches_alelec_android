package com.fossil;

import android.app.Activity;
import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot4 extends m24<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public /* final */ MFLoginWechatManager d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ot4.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ WeakReference<wq4> a;

        @DexIgnore
        public b(WeakReference<wq4> weakReference) {
            wg6.b(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<wq4> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, gv1 gv1) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public d(SignUpSocialAuth signUpSocialAuth) {
            wg6.b(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    /*
    static {
        String simpleName = ot4.class.getSimpleName();
        wg6.a((Object) simpleName, "LoginWechatUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public ot4(MFLoginWechatManager mFLoginWechatManager) {
        wg6.b(mFLoginWechatManager, "mLoginWechatManager");
        this.d = mFLoginWechatManager;
    }

    @DexIgnore
    public String c() {
        return e;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ln4 {
        @DexIgnore
        public /* final */ /* synthetic */ ot4 a;

        @DexIgnore
        public e(ot4 ot4) {
            this.a = ot4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.fossil.ot4, com.portfolio.platform.CoroutineUseCase] */
        public void a(SignUpSocialAuth signUpSocialAuth) {
            wg6.b(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ot4.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.a(new d(signUpSocialAuth));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v3, types: [com.fossil.ot4, com.portfolio.platform.CoroutineUseCase] */
        public void a(int i, gv1 gv1, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ot4.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + gv1);
            this.a.a(new c(i, gv1));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public Object a(b bVar, xe6<Object> xe6) {
        String str;
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            MFLoginWechatManager mFLoginWechatManager = this.d;
            Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.get.instance());
            if (a2 == null || (str = a2.getD()) == null) {
                str = "";
            }
            mFLoginWechatManager.b(str);
            MFLoginWechatManager mFLoginWechatManager2 = this.d;
            WeakReference<wq4> a3 = bVar != null ? bVar.a() : null;
            if (a3 != null) {
                Object obj = a3.get();
                if (obj != null) {
                    wg6.a(obj, "requestValues?.activityContext!!.get()!!");
                    mFLoginWechatManager2.a((Activity) obj, new e(this));
                    return cd6.a;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = e;
            local.d(str2, "Inside .run failed with exception=" + e2);
            return new c(600, (gv1) null);
        }
    }
}
