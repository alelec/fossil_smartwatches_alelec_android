package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
public final class e95$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HybridCustomizeViewModel.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e95$d$a(HybridCustomizeViewModel.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        e95$d$a e95_d_a = new e95$d$a(this.this$0, xe6);
        e95_d_a.p$ = (il6) obj;
        return e95_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((e95$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.b.a(this.this$0.this$0.l);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
