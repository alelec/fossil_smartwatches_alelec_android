package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.o4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k4 implements m4 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements o4.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(Canvas canvas, RectF rectF, float f, Paint paint) {
            Canvas canvas2 = canvas;
            RectF rectF2 = rectF;
            float f2 = 2.0f * f;
            float width = (rectF.width() - f2) - 1.0f;
            float height = (rectF.height() - f2) - 1.0f;
            if (f >= 1.0f) {
                float f3 = f + 0.5f;
                float f4 = -f3;
                k4.this.a.set(f4, f4, f3, f3);
                int save = canvas.save();
                canvas2.translate(rectF2.left + f3, rectF2.top + f3);
                Paint paint2 = paint;
                canvas.drawArc(k4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(k4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(height, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(k4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.translate(width, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                canvas2.rotate(90.0f);
                canvas.drawArc(k4.this.a, 180.0f, 90.0f, true, paint2);
                canvas2.restoreToCount(save);
                float f5 = rectF2.top;
                canvas.drawRect((rectF2.left + f3) - 1.0f, f5, (rectF2.right - f3) + 1.0f, f5 + f3, paint2);
                float f6 = rectF2.bottom;
                Canvas canvas3 = canvas;
                canvas3.drawRect((rectF2.left + f3) - 1.0f, f6 - f3, (rectF2.right - f3) + 1.0f, f6, paint2);
            }
            canvas.drawRect(rectF2.left, rectF2.top + f, rectF2.right, rectF2.bottom - f, paint);
        }
    }

    @DexIgnore
    public void a() {
        o4.r = new a();
    }

    @DexIgnore
    public float b(l4 l4Var) {
        return j(l4Var).c();
    }

    @DexIgnore
    public void c(l4 l4Var) {
    }

    @DexIgnore
    public void c(l4 l4Var, float f) {
        j(l4Var).b(f);
        f(l4Var);
    }

    @DexIgnore
    public float d(l4 l4Var) {
        return j(l4Var).d();
    }

    @DexIgnore
    public ColorStateList e(l4 l4Var) {
        return j(l4Var).b();
    }

    @DexIgnore
    public void f(l4 l4Var) {
        Rect rect = new Rect();
        j(l4Var).b(rect);
        l4Var.a((int) Math.ceil((double) h(l4Var)), (int) Math.ceil((double) g(l4Var)));
        l4Var.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public float g(l4 l4Var) {
        return j(l4Var).e();
    }

    @DexIgnore
    public float h(l4 l4Var) {
        return j(l4Var).f();
    }

    @DexIgnore
    public void i(l4 l4Var) {
        j(l4Var).a(l4Var.a());
        f(l4Var);
    }

    @DexIgnore
    public final o4 j(l4 l4Var) {
        return (o4) l4Var.c();
    }

    @DexIgnore
    public void a(l4 l4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        o4 a2 = a(context, colorStateList, f, f2, f3);
        a2.a(l4Var.a());
        l4Var.a(a2);
        f(l4Var);
    }

    @DexIgnore
    public void b(l4 l4Var, float f) {
        j(l4Var).c(f);
    }

    @DexIgnore
    public final o4 a(Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        return new o4(context.getResources(), colorStateList, f, f2, f3);
    }

    @DexIgnore
    public void a(l4 l4Var, ColorStateList colorStateList) {
        j(l4Var).b(colorStateList);
    }

    @DexIgnore
    public void a(l4 l4Var, float f) {
        j(l4Var).a(f);
        f(l4Var);
    }

    @DexIgnore
    public float a(l4 l4Var) {
        return j(l4Var).g();
    }
}
