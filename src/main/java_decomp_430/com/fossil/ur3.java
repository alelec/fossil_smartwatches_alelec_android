package com.fossil;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur3 extends Binder {
    @DexIgnore
    public /* final */ xr3 a;

    @DexIgnore
    public ur3(xr3 xr3) {
        this.a = xr3;
    }

    @DexIgnore
    public final void a(zr3 zr3) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "service received new intent via bind strategy");
            }
            this.a.a(zr3.a).a(uq3.a(), new tr3(zr3));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
