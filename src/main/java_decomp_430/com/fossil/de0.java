package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de0 extends ee0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<de0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public de0 createFromParcel(Parcel parcel) {
            return new de0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new de0[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m10createFromParcel(Parcel parcel) {
            return new de0(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public /* synthetic */ de0(Parcel parcel, qg6 qg6) {
        super(fe0.values()[parcel.readInt()]);
        String[] createStringArray = parcel.createStringArray();
        if (createStringArray != null) {
            this.b = createStringArray;
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("commuteApp._.config.destinations", cw0.a(this.b));
        wg6.a(put, "JSONObject()\n           \u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(de0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.b, ((de0) obj).b);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public final String[] getDestinations() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeStringArray(this.b);
        }
    }

    @DexIgnore
    public de0(String[] strArr) throws IllegalArgumentException {
        super(fe0.COMMUTE);
        this.b = strArr;
        if (this.b.length > 10) {
            StringBuilder b2 = ze0.b("Destinations(");
            b2.append(this.b);
            b2.append(") must be less than or equal to ");
            b2.append("10");
            throw new IllegalArgumentException(b2.toString());
        }
    }
}
