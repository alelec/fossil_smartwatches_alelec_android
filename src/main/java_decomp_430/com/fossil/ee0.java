package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ee0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ fe0 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ee0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            fe0 fe0 = fe0.values()[parcel.readInt()];
            parcel.setDataPosition(0);
            int i = le1.a[fe0.ordinal()];
            if (i == 1) {
                return ci1.CREATOR.createFromParcel(parcel);
            }
            if (i == 2) {
                return de0.CREATOR.createFromParcel(parcel);
            }
            throw new kc6();
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ee0[i];
        }
    }

    @DexIgnore
    public ee0(fe0 fe0) {
        this.a = fe0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.a == ((ee0) obj).a;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.WatchAppDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a.ordinal());
        }
    }
}
