package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rb3 extends IInterface {
    @DexIgnore
    void a(n12 n12, int i, boolean z) throws RemoteException;

    @DexIgnore
    void a(vb3 vb3, pb3 pb3) throws RemoteException;

    @DexIgnore
    void d(int i) throws RemoteException;
}
