package com.fossil;

import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase$verifySecretKey$1$onSuccess$1", f = "DianaSyncUseCase.kt", l = {}, m = "invokeSuspend")
public final class kr4$e$b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaSyncUseCase.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kr4$e$b(DianaSyncUseCase.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        kr4$e$b kr4_e_b = new kr4$e$b(this.this$0, xe6);
        kr4_e_b.p$ = (il6) obj;
        return kr4_e_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((kr4$e$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            DianaSyncUseCase.e eVar = this.this$0;
            eVar.a.a(eVar.c, eVar.d, eVar.e, eVar.b);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
