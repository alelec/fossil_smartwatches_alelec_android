package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ae1 extends qv0 {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B; // = 10000;
    @DexIgnore
    public /* final */ long C; // = 10000;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ gg6<cd6> E;
    @DexIgnore
    public ka1 F;
    @DexIgnore
    public /* final */ short G;
    @DexIgnore
    public /* final */ kq0 H;

    @DexIgnore
    public ae1(short s, kq0 kq0, ue1 ue1) {
        super(lx0.LEGACY_TRANSFER_DATA, ue1, 0, 4);
        this.G = s;
        this.H = kq0;
        this.E = new fc1(this, ue1);
    }

    @DexIgnore
    public void a(long j) {
        this.B = j;
    }

    @DexIgnore
    public void b(sg1 sg1) {
        if (sg1.a == rg1.FTC) {
            byte[] bArr = sg1.b;
            if (bArr.length >= 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                byte b = order.get(0);
                if (this.G != order.getShort(1)) {
                    return;
                }
                if (du0.LEGACY_PUT_FILE_EOF.a == b) {
                    l();
                    bn0 a = bn0.f.a((sj0) zh0.j.a(order.get(3)));
                    this.v = bn0.a(this.v, (lx0) null, (String) null, a.c, (ch0) null, a.e, 11);
                    this.A = cw0.b(order.getInt(5));
                    this.g.add(new ne0(0, sg1.a, sg1.b, cw0.a(new JSONObject(), bm0.WRITTEN_SIZE, (Object) Long.valueOf(this.A)), 1));
                    a(this.v);
                    return;
                }
                this.g.add(new ne0(0, sg1.a, sg1.b, (JSONObject) null, 9));
                a(bn0.a(this.v, (lx0) null, (String) null, il0.RESPONSE_ERROR, (ch0) null, (sj0) null, 27));
            }
        }
    }

    @DexIgnore
    public void c(ok0 ok0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        this.v = bn0.a(this.v, (lx0) null, (String) null, bn0.f.a(ok0.d).c, ok0.d, (sj0) null, 19);
        nn0 nn0 = this.f;
        if (nn0 != null) {
            nn0.i = true;
        }
        nn0 nn02 = this.f;
        if (!(nn02 == null || (jSONObject2 = nn02.m) == null)) {
            cw0.a(jSONObject2, bm0.MESSAGE, (Object) cw0.a((Enum<?>) il0.SUCCESS));
        }
        il0 il0 = this.v.c;
        il0 il02 = il0.SUCCESS;
        a(this.p);
        nn0 nn03 = this.f;
        if (!(nn03 == null || (jSONObject = nn03.m) == null)) {
            cw0.a(jSONObject, bm0.TRANSFERRED_DATA_SIZE, (Object) Integer.valueOf(this.H.c()));
        }
        float min = Math.min((((float) this.H.c()) * 1.0f) / ((float) this.H.c), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.H.c() >= this.H.c) {
            this.D = min;
            a(this.D);
        }
        b();
    }

    @DexIgnore
    public ok0 d() {
        if (!(this.H.b.remaining() > 0)) {
            return null;
        }
        byte[] a = this.H.a();
        if (this.s) {
            a = lg0.b.b(this.y.t, this.H.f, a);
        }
        return new fb1(this.H.f, a, this.y.v);
    }

    @DexIgnore
    public long e() {
        return this.B;
    }

    @DexIgnore
    public void g() {
        j();
        k();
        l();
        this.F = new ka1(this, this.E);
        ka1 ka1 = this.F;
        if (ka1 != null) {
            c().postDelayed(ka1, this.C);
        }
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.TRANSFERRED_DATA_SIZE, (Object) Integer.valueOf(this.H.c)), bm0.HEADER_LENGTH, (Object) Integer.valueOf(((ds0) this.H).g));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.ACTUAL_WRITTEN_SIZE, (Object) Long.valueOf(this.A));
    }

    @DexIgnore
    public final void l() {
        ka1 ka1 = this.F;
        if (ka1 != null) {
            this.j.removeCallbacks(ka1);
        }
        ka1 ka12 = this.F;
        if (ka12 != null) {
            ka12.a = true;
        }
        this.F = null;
    }
}
