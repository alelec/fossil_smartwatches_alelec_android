package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ny3 implements xx3 {
    @DexIgnore
    public iy3 a(String str, qx3 qx3, int i, int i2, Map<sx3, ?> map) {
        rx3 rx3;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (qx3 != qx3.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got " + qx3);
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            zy3 zy3 = zy3.FORCE_NONE;
            rx3 rx32 = null;
            if (map != null) {
                zy3 zy32 = (zy3) map.get(sx3.DATA_MATRIX_SHAPE);
                if (zy32 != null) {
                    zy3 = zy32;
                }
                rx3 = (rx3) map.get(sx3.MIN_SIZE);
                if (rx3 == null) {
                    rx3 = null;
                }
                rx3 rx33 = (rx3) map.get(sx3.MAX_SIZE);
                if (rx33 != null) {
                    rx32 = rx33;
                }
            } else {
                rx3 = null;
            }
            String a = xy3.a(str, zy3, rx3, rx32);
            yy3 a2 = yy3.a(a.length(), zy3, rx3, rx32, true);
            sy3 sy3 = new sy3(wy3.a(a, a2), a2.f(), a2.e());
            sy3.a();
            return a(sy3, a2);
        }
    }

    @DexIgnore
    public static iy3 a(sy3 sy3, yy3 yy3) {
        int f = yy3.f();
        int e = yy3.e();
        j04 j04 = new j04(yy3.h(), yy3.g());
        int i = 0;
        for (int i2 = 0; i2 < e; i2++) {
            if (i2 % yy3.e == 0) {
                int i3 = 0;
                for (int i4 = 0; i4 < yy3.h(); i4++) {
                    j04.a(i3, i, i4 % 2 == 0);
                    i3++;
                }
                i++;
            }
            int i5 = 0;
            for (int i6 = 0; i6 < f; i6++) {
                if (i6 % yy3.d == 0) {
                    j04.a(i5, i, true);
                    i5++;
                }
                j04.a(i5, i, sy3.a(i6, i2));
                i5++;
                int i7 = yy3.d;
                if (i6 % i7 == i7 - 1) {
                    j04.a(i5, i, i2 % 2 == 0);
                    i5++;
                }
            }
            i++;
            int i8 = yy3.e;
            if (i2 % i8 == i8 - 1) {
                int i9 = 0;
                for (int i10 = 0; i10 < yy3.h(); i10++) {
                    j04.a(i9, i, true);
                    i9++;
                }
                i++;
            }
        }
        return a(j04);
    }

    @DexIgnore
    public static iy3 a(j04 j04) {
        int c = j04.c();
        int b = j04.b();
        iy3 iy3 = new iy3(c, b);
        iy3.a();
        for (int i = 0; i < c; i++) {
            for (int i2 = 0; i2 < b; i2++) {
                if (j04.a(i, i2) == 1) {
                    iy3.b(i, i2);
                }
            }
        }
        return iy3;
    }
}
