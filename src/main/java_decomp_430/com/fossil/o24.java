package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import androidx.work.ListenableWorker;
import com.fossil.y76;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.CloudImageHelper_MembersInjector;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper_MembersInjector;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.LocationSource_Factory;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.RingStyleRepository_Factory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository_Factory;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.ActivitiesRepository_Factory;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.AlarmsRepository_Factory;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.CategoryRepository_Factory;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository_Factory;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.ComplicationRepository_Factory;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceDatabase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DeviceRepository_Factory;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaPresetRepository_Factory;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FileRepository_Factory;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.FitnessDataRepository_Factory;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository_Factory;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository_Factory;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.MicroAppRepository_Factory;
import com.portfolio.platform.data.source.NotificationsDataSource;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.NotificationsRepositoryModule;
import com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory;
import com.portfolio.platform.data.source.NotificationsRepository_Factory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideActivitySampleDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAddressDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaPresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFileDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessHelperFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHeartRateDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidePresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSampleRawDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSkuDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSleepDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSleepDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThemeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWorkoutDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesAlarmDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesRingStyleDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.QuickResponseRepository_Factory;
import com.portfolio.platform.data.source.RepositoriesModule;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideComplicationRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideUserRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.SkuDao;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository_Factory;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository_Factory;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository_Factory;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.ThemeRepository_Factory;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository_Factory;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.UserDataSource;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserRepository_Factory;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchAppRepository_Factory;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository_Factory;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository_Factory;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository_Factory;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.data.source.loader.NotificationsLoader_Factory;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository_Factory;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.FileDownloadManager;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.ProfileSetupFragment;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.HomeDashboardFragment;
import com.portfolio.platform.uirenew.home.HomeFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseEditDialogFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseEditDialogViewModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.CustomizeTutorialFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.CommuteTimeSettingsDetailFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.CommuteTimeWatchAppSettingsFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditFragment;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesFragment;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveMinutesChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveMinutesChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActivityChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActivityChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeBackgroundViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeButtonFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeButtonViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeGoalTrackingChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeGoalTrackingChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.uirenew.mappicker.MapPickerFragment;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingFragment;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.GetWeather;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.portfolio.platform.workers.TimeChangeReceiver;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o24 implements y04 {
    @DexIgnore
    public Provider<UserRepository> A;
    @DexIgnore
    public Provider<HeartRateSampleDao> A0;
    @DexIgnore
    public Provider<jn4> A1;
    @DexIgnore
    public Provider<on4> A2;
    @DexIgnore
    public Provider<AlarmDatabase> B;
    @DexIgnore
    public Provider<HeartRateSampleRepository> B0;
    @DexIgnore
    public MicroAppLastSettingRepository_Factory B1;
    @DexIgnore
    public Provider<ServerSettingDataSource> B2;
    @DexIgnore
    public Provider<AlarmDao> C;
    @DexIgnore
    public Provider<HeartRateDailySummaryDao> C0;
    @DexIgnore
    public Provider<t24> C1;
    @DexIgnore
    public Provider<ServerSettingDataSource> C2;
    @DexIgnore
    public Provider<AlarmsLocalDataSource> D;
    @DexIgnore
    public Provider<HeartRateSummaryRepository> D0;
    @DexIgnore
    public Provider<FirmwareFileRepository> D1;
    @DexIgnore
    public Provider<ey5> D2;
    @DexIgnore
    public Provider<AlarmsRemoteDataSource> E;
    @DexIgnore
    public Provider<WorkoutDao> E0;
    @DexIgnore
    public Provider<xs4> E1;
    @DexIgnore
    public Provider<AlarmsRepository> F;
    @DexIgnore
    public Provider<WorkoutSessionRepository> F0;
    @DexIgnore
    public Provider<AddressDatabase> F1;
    @DexIgnore
    public Provider<rj4> G;
    @DexIgnore
    public Provider<RemindersSettingsDatabase> G0;
    @DexIgnore
    public Provider<AddressDao> G1;
    @DexIgnore
    public Provider<FitnessDatabase> H;
    @DexIgnore
    public Provider<ThirdPartyDatabase> H0;
    @DexIgnore
    public Provider<LocationSource> H1;
    @DexIgnore
    public Provider<ActivitySummaryDao> I;
    @DexIgnore
    public Provider<RingStyleDao> I0;
    @DexIgnore
    public Provider<InAppNotificationDatabase> I1;
    @DexIgnore
    public Provider<FitnessDataDao> J;
    @DexIgnore
    public Provider<FileDatabase> J0;
    @DexIgnore
    public Provider<InAppNotificationDao> J1;
    @DexIgnore
    public Provider<ik4> K;
    @DexIgnore
    public Provider<FileDao> K0;
    @DexIgnore
    public Provider<InAppNotificationRepository> K1;
    @DexIgnore
    public Provider<SummariesRepository> L;
    @DexIgnore
    public Provider<qm4> L0;
    @DexIgnore
    public Provider<dp5> L1;
    @DexIgnore
    public Provider<SleepDatabase> M;
    @DexIgnore
    public Provider<FileRepository> M0;
    @DexIgnore
    public Provider<ce> M1;
    @DexIgnore
    public Provider<SleepDao> N;
    @DexIgnore
    public Provider<WatchFaceDao> N0;
    @DexIgnore
    public Provider<GoogleApiService> N1;
    @DexIgnore
    public Provider<SleepSummariesRepository> O;
    @DexIgnore
    public Provider<sj4> O0;
    @DexIgnore
    public Provider<el4> O1;
    @DexIgnore
    public Provider<GuestApiService> P;
    @DexIgnore
    public Provider<CategoryDatabase> P0;
    @DexIgnore
    public Provider<s04> P1;
    @DexIgnore
    public Provider<u04> Q;
    @DexIgnore
    public Provider<CategoryDao> Q0;
    @DexIgnore
    public ComplicationLastSettingRepository_Factory Q1;
    @DexIgnore
    public Provider<NotificationsDataSource> R;
    @DexIgnore
    public CategoryRemoteDataSource_Factory R0;
    @DexIgnore
    public WatchAppLastSettingRepository_Factory R1;
    @DexIgnore
    public Provider<NotificationsRepository> S;
    @DexIgnore
    public CategoryRepository_Factory S0;
    @DexIgnore
    public i45 S1;
    @DexIgnore
    public Provider<DeviceDatabase> T;
    @DexIgnore
    public WatchAppRepository_Factory T0;
    @DexIgnore
    public f95 T1;
    @DexIgnore
    public Provider<DeviceDao> U;
    @DexIgnore
    public ComplicationRepository_Factory U0;
    @DexIgnore
    public gt4 U1;
    @DexIgnore
    public Provider<SkuDao> V;
    @DexIgnore
    public WatchFaceRemoteDataSource_Factory V0;
    @DexIgnore
    public et4 V1;
    @DexIgnore
    public DeviceRemoteDataSource_Factory W;
    @DexIgnore
    public WatchFaceRepository_Factory W0;
    @DexIgnore
    public ok5 W1;
    @DexIgnore
    public Provider<DeviceRepository> X;
    @DexIgnore
    public RingStyleRemoteDataSource_Factory X0;
    @DexIgnore
    public j75 X1;
    @DexIgnore
    public Provider<ContentResolver> Y;
    @DexIgnore
    public RingStyleRepository_Factory Y0;
    @DexIgnore
    public xr4 Y1;
    @DexIgnore
    public Provider<z24> Z;
    @DexIgnore
    public WatchLocalizationRepository_Factory Z0;
    @DexIgnore
    public es4 Z1;
    @DexIgnore
    public b14 a;
    @DexIgnore
    public Provider<HybridCustomizeDatabase> a0;
    @DexIgnore
    public at5 a1;
    @DexIgnore
    public zr4 a2;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider<HybridPresetDao> b0;
    @DexIgnore
    public ct5 b1;
    @DexIgnore
    public dv5 b2;
    @DexIgnore
    public Provider<an4> c;
    @DexIgnore
    public HybridPresetRemoteDataSource_Factory c0;
    @DexIgnore
    public or4 c1;
    @DexIgnore
    public b85 c2;
    @DexIgnore
    public Provider<PortfolioApp> d;
    @DexIgnore
    public Provider<HybridPresetRepository> d0;
    @DexIgnore
    public lw5 d1;
    @DexIgnore
    public x75 d2;
    @DexIgnore
    public Provider<DNDSettingsDatabase> e;
    @DexIgnore
    public Provider<SampleRawDao> e0;
    @DexIgnore
    public xw5 e1;
    @DexIgnore
    public om5 e2;
    @DexIgnore
    public Provider<QuickResponseDatabase> f;
    @DexIgnore
    public Provider<ActivitySampleDao> f0;
    @DexIgnore
    public hs4 f1;
    @DexIgnore
    public ms4 f2;
    @DexIgnore
    public Provider<QuickResponseMessageDao> g;
    @DexIgnore
    public Provider<ActivitiesRepository> g0;
    @DexIgnore
    public lr4 g1;
    @DexIgnore
    public js4 g2;
    @DexIgnore
    public Provider<QuickResponseSenderDao> h;
    @DexIgnore
    public Provider<ShortcutApiService> h0;
    @DexIgnore
    public dj4 h1;
    @DexIgnore
    public up5 h2;
    @DexIgnore
    public Provider<QuickResponseRepository> i;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> i0;
    @DexIgnore
    public Provider<ThemeDatabase> i1;
    @DexIgnore
    public vr4 i2;
    @DexIgnore
    public Provider<NotificationSettingsDatabase> j;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> j0;
    @DexIgnore
    public Provider<ThemeDao> j1;
    @DexIgnore
    public zz4 j2;
    @DexIgnore
    public Provider<iq4> k;
    @DexIgnore
    public Provider<MicroAppSettingRepository> k0;
    @DexIgnore
    public Provider<ThemeRepository> k1;
    @DexIgnore
    public mo5 k2;
    @DexIgnore
    public Provider<DianaCustomizeDatabase> l;
    @DexIgnore
    public Provider<SleepSessionsRepository> l0;
    @DexIgnore
    public Provider<ApplicationEventListener> l1;
    @DexIgnore
    public nn5 l2;
    @DexIgnore
    public Provider<DianaPresetDao> m;
    @DexIgnore
    public Provider<GoalTrackingDatabase> m0;
    @DexIgnore
    public Provider<up4> m1;
    @DexIgnore
    public in5 m2;
    @DexIgnore
    public Provider<zm4> n;
    @DexIgnore
    public Provider<GoalTrackingDao> n0;
    @DexIgnore
    public FitnessDataRepository_Factory n1;
    @DexIgnore
    public co5 n2;
    @DexIgnore
    public Provider<AuthApiGuestService> o;
    @DexIgnore
    public Provider<GoalTrackingRepository> o0;
    @DexIgnore
    public p14 o1;
    @DexIgnore
    public dn5 o2;
    @DexIgnore
    public Provider<ip4> p;
    @DexIgnore
    public Provider<in4> p0;
    @DexIgnore
    public Provider<ThirdPartyRepository> p1;
    @DexIgnore
    public ym5 p2;
    @DexIgnore
    public Provider<mp4> q;
    @DexIgnore
    public Provider<WatchAppDao> q0;
    @DexIgnore
    public g06 q1;
    @DexIgnore
    public tm5 q2;
    @DexIgnore
    public Provider<ApiServiceV2> r;
    @DexIgnore
    public Provider<WatchAppRemoteDataSource> r0;
    @DexIgnore
    public Provider<mn4> r1;
    @DexIgnore
    public sn5 r2;
    @DexIgnore
    public Provider<DianaPresetRemoteDataSource> s;
    @DexIgnore
    public Provider<ComplicationDao> s0;
    @DexIgnore
    public Provider<so4> s1;
    @DexIgnore
    public xn5 s2;
    @DexIgnore
    public Provider<DianaPresetRepository> t;
    @DexIgnore
    public Provider<ComplicationRemoteDataSource> t0;
    @DexIgnore
    public Provider<vo4> t1;
    @DexIgnore
    public ho5 t2;
    @DexIgnore
    public Provider<CustomizeRealDataDatabase> u;
    @DexIgnore
    public Provider<MicroAppDao> u0;
    @DexIgnore
    public uw5 u1;
    @DexIgnore
    public ro5 u2;
    @DexIgnore
    public Provider<CustomizeRealDataDao> v;
    @DexIgnore
    public MicroAppRemoteDataSource_Factory v0;
    @DexIgnore
    public Provider<vm4> v1;
    @DexIgnore
    public uz4 v2;
    @DexIgnore
    public Provider<CustomizeRealDataRepository> w;
    @DexIgnore
    public Provider<MicroAppRepository> w0;
    @DexIgnore
    public Provider<gl4> w1;
    @DexIgnore
    public Provider<Map<Class<? extends td>, Provider<td>>> w2;
    @DexIgnore
    public Provider<AuthApiUserService> x;
    @DexIgnore
    public Provider<MicroAppLastSettingDao> x0;
    @DexIgnore
    public Provider<lq4> x1;
    @DexIgnore
    public Provider<w04> x2;
    @DexIgnore
    public Provider<UserDataSource> y;
    @DexIgnore
    public Provider<ComplicationLastSettingDao> y0;
    @DexIgnore
    public Provider<hn4> y1;
    @DexIgnore
    public Provider<NotificationSettingsDao> y2;
    @DexIgnore
    public Provider<UserDataSource> z;
    @DexIgnore
    public Provider<WatchAppLastSettingDao> z0;
    @DexIgnore
    public Provider<kn4> z1;
    @DexIgnore
    public Provider<s24> z2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a0 implements tn5 {
        @DexIgnore
        public void a(CustomizeHeartRateChartFragment customizeHeartRateChartFragment) {
            b(customizeHeartRateChartFragment);
        }

        @DexIgnore
        public final CustomizeHeartRateChartFragment b(CustomizeHeartRateChartFragment customizeHeartRateChartFragment) {
            un5.a(customizeHeartRateChartFragment, (w04) o24.this.x2.get());
            return customizeHeartRateChartFragment;
        }

        @DexIgnore
        public a0(vn5 vn5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a1 implements r95 {
        @DexIgnore
        public void a(MicroAppFragment microAppFragment) {
            b(microAppFragment);
        }

        @DexIgnore
        public final MicroAppFragment b(MicroAppFragment microAppFragment) {
            g95.a(microAppFragment, (w04) o24.this.x2.get());
            return microAppFragment;
        }

        @DexIgnore
        public a1(u95 u95) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a2 implements y55 {
        @DexIgnore
        public b65 a;

        @DexIgnore
        public final SearchSecondTimezonePresenter a() {
            SearchSecondTimezonePresenter a2 = e65.a(c65.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchSecondTimezoneActivity b(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            xq4.a((BaseActivity) searchSecondTimezoneActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) searchSecondTimezoneActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) searchSecondTimezoneActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) searchSecondTimezoneActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) searchSecondTimezoneActivity, new os4());
            x55.a(searchSecondTimezoneActivity, a());
            return searchSecondTimezoneActivity;
        }

        @DexIgnore
        public a2(b65 b65) {
            a(b65);
        }

        @DexIgnore
        public final void a(b65 b65) {
            z76.a(b65);
            this.a = b65;
        }

        @DexIgnore
        public void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            b(searchSecondTimezoneActivity);
        }

        @DexIgnore
        public final SearchSecondTimezonePresenter a(SearchSecondTimezonePresenter searchSecondTimezonePresenter) {
            f65.a(searchSecondTimezonePresenter);
            return searchSecondTimezonePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements tj5 {
        @DexIgnore
        public wj5 a;

        @DexIgnore
        public final yj5 a() {
            yj5 a2 = zj5.a(xj5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final AboutActivity b(AboutActivity aboutActivity) {
            xq4.a((BaseActivity) aboutActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) aboutActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) aboutActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) aboutActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) aboutActivity, new os4());
            sj5.a(aboutActivity, a());
            return aboutActivity;
        }

        @DexIgnore
        public b(wj5 wj5) {
            a(wj5);
        }

        @DexIgnore
        public final void a(wj5 wj5) {
            z76.a(wj5);
            this.a = wj5;
        }

        @DexIgnore
        public void a(AboutActivity aboutActivity) {
            b(aboutActivity);
        }

        @DexIgnore
        public final yj5 a(yj5 yj5) {
            ak5.a(yj5);
            return yj5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements yn5 {
        @DexIgnore
        public void a(CustomizeRingChartFragment customizeRingChartFragment) {
            b(customizeRingChartFragment);
        }

        @DexIgnore
        public final CustomizeRingChartFragment b(CustomizeRingChartFragment customizeRingChartFragment) {
            zn5.a(customizeRingChartFragment, (w04) o24.this.x2.get());
            return customizeRingChartFragment;
        }

        @DexIgnore
        public b0(ao5 ao5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b1 implements yx4 {
        @DexIgnore
        public by4 a;

        @DexIgnore
        public final NotificationAppsPresenter a() {
            NotificationAppsPresenter a2 = ey4.a(cy4.a(this.a), (z24) o24.this.Z.get(), new d15(), new mz4(), b(), (an4) o24.this.c.get(), (NotificationSettingsDatabase) o24.this.j.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final gy4 b() {
            return new gy4((NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public b1(by4 by4) {
            a(by4);
        }

        @DexIgnore
        public final NotificationAppsActivity b(NotificationAppsActivity notificationAppsActivity) {
            xq4.a((BaseActivity) notificationAppsActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationAppsActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationAppsActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationAppsActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationAppsActivity, new os4());
            xx4.a(notificationAppsActivity, a());
            return notificationAppsActivity;
        }

        @DexIgnore
        public final void a(by4 by4) {
            z76.a(by4);
            this.a = by4;
        }

        @DexIgnore
        public void a(NotificationAppsActivity notificationAppsActivity) {
            b(notificationAppsActivity);
        }

        @DexIgnore
        public final NotificationAppsPresenter a(NotificationAppsPresenter notificationAppsPresenter) {
            fy4.a(notificationAppsPresenter);
            return notificationAppsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b2 implements ot5 {
        @DexIgnore
        public rt5 a;

        @DexIgnore
        public final SignUpPresenter a() {
            SignUpPresenter a2 = vt5.a(tt5.a(this.a), st5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SignUpActivity b(SignUpActivity signUpActivity) {
            xq4.a((BaseActivity) signUpActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) signUpActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) signUpActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) signUpActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) signUpActivity, new os4());
            nt5.a(signUpActivity, a());
            nt5.a(signUpActivity, (hn4) o24.this.y1.get());
            nt5.a(signUpActivity, (in4) o24.this.p0.get());
            nt5.a(signUpActivity, (kn4) o24.this.z1.get());
            nt5.a(signUpActivity, (MFLoginWechatManager) o24.this.A1.get());
            return signUpActivity;
        }

        @DexIgnore
        public b2(rt5 rt5) {
            a(rt5);
        }

        @DexIgnore
        public final void a(rt5 rt5) {
            z76.a(rt5);
            this.a = rt5;
        }

        @DexIgnore
        public void a(SignUpActivity signUpActivity) {
            b(signUpActivity);
        }

        @DexIgnore
        public final SignUpPresenter a(SignUpPresenter signUpPresenter) {
            wt5.a(signUpPresenter, o24.this.G());
            wt5.a(signUpPresenter, (kn4) o24.this.z1.get());
            wt5.a(signUpPresenter, o24.this.H());
            wt5.a(signUpPresenter, o24.this.K());
            wt5.a(signUpPresenter, o24.this.J());
            wt5.a(signUpPresenter, o24.this.I());
            wt5.a(signUpPresenter, (UserRepository) o24.this.A.get());
            wt5.a(signUpPresenter, (DeviceRepository) o24.this.X.get());
            wt5.a(signUpPresenter, (z24) o24.this.Z.get());
            wt5.a(signUpPresenter, o24.this.u());
            wt5.a(signUpPresenter, o24.this.v());
            wt5.a(signUpPresenter, o24.this.p());
            wt5.a(signUpPresenter, o24.this.w());
            wt5.a(signUpPresenter, o24.this.t());
            wt5.a(signUpPresenter, o24.this.r());
            wt5.a(signUpPresenter, (AlarmsRepository) o24.this.F.get());
            wt5.a(signUpPresenter, new rr4());
            wt5.a(signUpPresenter, o24.this.l());
            wt5.a(signUpPresenter, o24.this.n());
            wt5.a(signUpPresenter, (an4) o24.this.c.get());
            wt5.a(signUpPresenter, o24.this.c());
            wt5.a(signUpPresenter, o24.this.d());
            wt5.a(signUpPresenter, (AnalyticsHelper) o24.this.O0.get());
            wt5.a(signUpPresenter, o24.this.B());
            wt5.a(signUpPresenter, (SummariesRepository) o24.this.L.get());
            wt5.a(signUpPresenter, (SleepSummariesRepository) o24.this.O.get());
            wt5.a(signUpPresenter, (GoalTrackingRepository) o24.this.o0.get());
            wt5.a(signUpPresenter, o24.this.q());
            wt5.a(signUpPresenter, o24.this.s());
            wt5.a(signUpPresenter, o24.this.N());
            wt5.a(signUpPresenter, o24.this.A());
            wt5.a(signUpPresenter, o24.this.d0());
            wt5.a(signUpPresenter);
            return signUpPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements bh5 {
        @DexIgnore
        public eh5 a;

        @DexIgnore
        public final ActiveTimeDetailPresenter a() {
            ActiveTimeDetailPresenter a2 = hh5.a(fh5.a(this.a), (SummariesRepository) o24.this.L.get(), (ActivitiesRepository) o24.this.g0.get(), (UserRepository) o24.this.A.get(), (WorkoutSessionRepository) o24.this.F0.get(), (FitnessDataDao) o24.this.J.get(), (WorkoutDao) o24.this.E0.get(), (FitnessDatabase) o24.this.H.get(), (u04) o24.this.Q.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeDetailActivity b(ActiveTimeDetailActivity activeTimeDetailActivity) {
            xq4.a((BaseActivity) activeTimeDetailActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) activeTimeDetailActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) activeTimeDetailActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) activeTimeDetailActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) activeTimeDetailActivity, new os4());
            ah5.a(activeTimeDetailActivity, a());
            return activeTimeDetailActivity;
        }

        @DexIgnore
        public c(eh5 eh5) {
            a(eh5);
        }

        @DexIgnore
        public final void a(eh5 eh5) {
            z76.a(eh5);
            this.a = eh5;
        }

        @DexIgnore
        public void a(ActiveTimeDetailActivity activeTimeDetailActivity) {
            b(activeTimeDetailActivity);
        }

        @DexIgnore
        public final ActiveTimeDetailPresenter a(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            ih5.a(activeTimeDetailPresenter);
            return activeTimeDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements do5 {
        @DexIgnore
        public void a(CustomizeSleepChartFragment customizeSleepChartFragment) {
            b(customizeSleepChartFragment);
        }

        @DexIgnore
        public final CustomizeSleepChartFragment b(CustomizeSleepChartFragment customizeSleepChartFragment) {
            eo5.a(customizeSleepChartFragment, (w04) o24.this.x2.get());
            return customizeSleepChartFragment;
        }

        @DexIgnore
        public c0(fo5 fo5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c1 implements iy4 {
        @DexIgnore
        public my4 a;

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter a() {
            NotificationCallsAndMessagesPresenter a2 = py4.a(ny4.a(this.a), (z24) o24.this.Z.get(), new mz4(), b(), c(), new d15(), (an4) o24.this.c.get(), (NotificationSettingsDao) o24.this.y2.get(), o24.this.R(), (QuickResponseRepository) o24.this.i.get(), (NotificationSettingsDatabase) o24.this.j.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final oz4 b() {
            return new oz4((NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public final pz4 c() {
            return new pz4((NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public c1(my4 my4) {
            a(my4);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesActivity b(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            xq4.a((BaseActivity) notificationCallsAndMessagesActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationCallsAndMessagesActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationCallsAndMessagesActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationCallsAndMessagesActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationCallsAndMessagesActivity, new os4());
            hy4.a(notificationCallsAndMessagesActivity, a());
            return notificationCallsAndMessagesActivity;
        }

        @DexIgnore
        public final void a(my4 my4) {
            z76.a(my4);
            this.a = my4;
        }

        @DexIgnore
        public void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            b(notificationCallsAndMessagesActivity);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter a(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            qy4.a(notificationCallsAndMessagesPresenter);
            return notificationCallsAndMessagesPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c2 implements wi5 {
        @DexIgnore
        public zi5 a;

        @DexIgnore
        public final SleepDetailPresenter a() {
            SleepDetailPresenter a2 = cj5.a(aj5.a(this.a), (SleepSummariesRepository) o24.this.O.get(), (SleepSessionsRepository) o24.this.l0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepDetailActivity b(SleepDetailActivity sleepDetailActivity) {
            xq4.a((BaseActivity) sleepDetailActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) sleepDetailActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) sleepDetailActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) sleepDetailActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) sleepDetailActivity, new os4());
            vi5.a(sleepDetailActivity, a());
            return sleepDetailActivity;
        }

        @DexIgnore
        public c2(zi5 zi5) {
            a(zi5);
        }

        @DexIgnore
        public final void a(zi5 zi5) {
            z76.a(zi5);
            this.a = zi5;
        }

        @DexIgnore
        public void a(SleepDetailActivity sleepDetailActivity) {
            b(sleepDetailActivity);
        }

        @DexIgnore
        public final SleepDetailPresenter a(SleepDetailPresenter sleepDetailPresenter) {
            dj5.a(sleepDetailPresenter);
            return sleepDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements eb5 {
        @DexIgnore
        public lb5 a;

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter a() {
            ActiveTimeOverviewDayPresenter a2 = ib5.a(mb5.a(this.a), (SummariesRepository) o24.this.L.get(), (ActivitiesRepository) o24.this.g0.get(), (WorkoutSessionRepository) o24.this.F0.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter b() {
            ActiveTimeOverviewMonthPresenter a2 = sb5.a(nb5.a(this.a), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter c() {
            ActiveTimeOverviewWeekPresenter a2 = xb5.a(ob5.a(this.a), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public d(lb5 lb5) {
            a(lb5);
        }

        @DexIgnore
        public final ActiveTimeOverviewFragment b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            kb5.a(activeTimeOverviewFragment, a());
            kb5.a(activeTimeOverviewFragment, c());
            kb5.a(activeTimeOverviewFragment, b());
            return activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void a(lb5 lb5) {
            z76.a(lb5);
            this.a = lb5;
        }

        @DexIgnore
        public void a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            b(activeTimeOverviewFragment);
        }

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter a(ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
            jb5.a(activeTimeOverviewDayPresenter);
            return activeTimeOverviewDayPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter a(ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter) {
            yb5.a(activeTimeOverviewWeekPresenter);
            return activeTimeOverviewWeekPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter a(ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
            tb5.a(activeTimeOverviewMonthPresenter);
            return activeTimeOverviewMonthPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 implements io5 {
        @DexIgnore
        public void a(CustomizeTextFragment customizeTextFragment) {
            b(customizeTextFragment);
        }

        @DexIgnore
        public final CustomizeTextFragment b(CustomizeTextFragment customizeTextFragment) {
            jo5.a(customizeTextFragment, (w04) o24.this.x2.get());
            return customizeTextFragment;
        }

        @DexIgnore
        public d0(ko5 ko5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d1 implements l15 {
        @DexIgnore
        public o15 a;

        @DexIgnore
        public final o25 a() {
            return new o25((Context) o24.this.b.get());
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter b() {
            NotificationContactsAndAppsAssignedPresenter a2 = s15.a(p15.a(this.a), q15.a(this.a), this.a.a(), (z24) o24.this.Z.get(), new a35(), a(), c(), d());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final l35 c() {
            return new l35((NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public final tr4 d() {
            return new tr4((NotificationsRepository) o24.this.S.get(), (DeviceRepository) o24.this.X.get());
        }

        @DexIgnore
        public d1(o15 o15) {
            a(o15);
        }

        @DexIgnore
        public final void a(o15 o15) {
            z76.a(o15);
            this.a = o15;
        }

        @DexIgnore
        public void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            b(notificationContactsAndAppsAssignedActivity);
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter a(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            t15.a(notificationContactsAndAppsAssignedPresenter);
            return notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedActivity b(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            xq4.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationContactsAndAppsAssignedActivity, new os4());
            k15.a(notificationContactsAndAppsAssignedActivity, b());
            return notificationContactsAndAppsAssignedActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d2 implements eg5 {
        @DexIgnore
        public lg5 a;

        @DexIgnore
        public final SleepOverviewDayPresenter a() {
            SleepOverviewDayPresenter a2 = ig5.a(mg5.a(this.a), (SleepSummariesRepository) o24.this.O.get(), (SleepSessionsRepository) o24.this.l0.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter b() {
            SleepOverviewMonthPresenter a2 = sg5.a(ng5.a(this.a), (UserRepository) o24.this.A.get(), (SleepSummariesRepository) o24.this.O.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter c() {
            SleepOverviewWeekPresenter a2 = xg5.a(og5.a(this.a), (UserRepository) o24.this.A.get(), (SleepSummariesRepository) o24.this.O.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public d2(lg5 lg5) {
            a(lg5);
        }

        @DexIgnore
        public final void a(lg5 lg5) {
            z76.a(lg5);
            this.a = lg5;
        }

        @DexIgnore
        public final SleepOverviewFragment b(SleepOverviewFragment sleepOverviewFragment) {
            kg5.a(sleepOverviewFragment, a());
            kg5.a(sleepOverviewFragment, c());
            kg5.a(sleepOverviewFragment, b());
            return sleepOverviewFragment;
        }

        @DexIgnore
        public void a(SleepOverviewFragment sleepOverviewFragment) {
            b(sleepOverviewFragment);
        }

        @DexIgnore
        public final SleepOverviewDayPresenter a(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            jg5.a(sleepOverviewDayPresenter);
            return sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter a(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
            yg5.a(sleepOverviewWeekPresenter);
            return sleepOverviewWeekPresenter;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter a(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            tg5.a(sleepOverviewMonthPresenter);
            return sleepOverviewMonthPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements kh5 {
        @DexIgnore
        public nh5 a;

        @DexIgnore
        public final ActivityDetailPresenter a() {
            ActivityDetailPresenter a2 = qh5.a(oh5.a(this.a), (SummariesRepository) o24.this.L.get(), (ActivitiesRepository) o24.this.g0.get(), (UserRepository) o24.this.A.get(), (WorkoutSessionRepository) o24.this.F0.get(), (FitnessDataDao) o24.this.J.get(), (WorkoutDao) o24.this.E0.get(), (FitnessDatabase) o24.this.H.get(), (u04) o24.this.Q.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityDetailActivity b(ActivityDetailActivity activityDetailActivity) {
            xq4.a((BaseActivity) activityDetailActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) activityDetailActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) activityDetailActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) activityDetailActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) activityDetailActivity, new os4());
            jh5.a(activityDetailActivity, a());
            return activityDetailActivity;
        }

        @DexIgnore
        public e(nh5 nh5) {
            a(nh5);
        }

        @DexIgnore
        public final void a(nh5 nh5) {
            z76.a(nh5);
            this.a = nh5;
        }

        @DexIgnore
        public void a(ActivityDetailActivity activityDetailActivity) {
            b(activityDetailActivity);
        }

        @DexIgnore
        public final ActivityDetailPresenter a(ActivityDetailPresenter activityDetailPresenter) {
            rh5.a(activityDetailPresenter);
            return activityDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 implements p65 {
        @DexIgnore
        public void a(CustomizeThemeFragment customizeThemeFragment) {
            b(customizeThemeFragment);
        }

        @DexIgnore
        public final CustomizeThemeFragment b(CustomizeThemeFragment customizeThemeFragment) {
            s65.a(customizeThemeFragment, (w04) o24.this.x2.get());
            return customizeThemeFragment;
        }

        @DexIgnore
        public e0(t65 t65) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e1 implements sy4 {
        @DexIgnore
        public vy4 a;

        @DexIgnore
        public final NotificationContactsPresenter a() {
            NotificationContactsPresenter a2 = zy4.a(xy4.a(this.a), (z24) o24.this.Z.get(), new mz4(), b(), wy4.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final pz4 b() {
            return new pz4((NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public e1(vy4 vy4) {
            a(vy4);
        }

        @DexIgnore
        public final NotificationContactsActivity b(NotificationContactsActivity notificationContactsActivity) {
            xq4.a((BaseActivity) notificationContactsActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationContactsActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationContactsActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationContactsActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationContactsActivity, new os4());
            ry4.a(notificationContactsActivity, a());
            return notificationContactsActivity;
        }

        @DexIgnore
        public final void a(vy4 vy4) {
            z76.a(vy4);
            this.a = vy4;
        }

        @DexIgnore
        public void a(NotificationContactsActivity notificationContactsActivity) {
            b(notificationContactsActivity);
        }

        @DexIgnore
        public final NotificationContactsPresenter a(NotificationContactsPresenter notificationContactsPresenter) {
            az4.a(notificationContactsPresenter);
            return notificationContactsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e2 implements gu5 {
        @DexIgnore
        public ju5 a;

        @DexIgnore
        public final SplashPresenter a() {
            SplashPresenter a2 = mu5.a(ku5.a(this.a), (UserRepository) o24.this.A.get(), (MigrationHelper) o24.this.A2.get(), (ThemeRepository) o24.this.k1.get(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SplashScreenActivity b(SplashScreenActivity splashScreenActivity) {
            xq4.a((BaseActivity) splashScreenActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) splashScreenActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) splashScreenActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) splashScreenActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) splashScreenActivity, new os4());
            ou5.a(splashScreenActivity, a());
            return splashScreenActivity;
        }

        @DexIgnore
        public e2(ju5 ju5) {
            a(ju5);
        }

        @DexIgnore
        public final void a(ju5 ju5) {
            z76.a(ju5);
            this.a = ju5;
        }

        @DexIgnore
        public void a(SplashScreenActivity splashScreenActivity) {
            b(splashScreenActivity);
        }

        @DexIgnore
        public final SplashPresenter a(SplashPresenter splashPresenter) {
            nu5.a(splashPresenter);
            return splashPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements ec5 {
        @DexIgnore
        public lc5 a;

        @DexIgnore
        public final ActivityOverviewDayPresenter a() {
            ActivityOverviewDayPresenter a2 = ic5.a(mc5.a(this.a), (SummariesRepository) o24.this.L.get(), (ActivitiesRepository) o24.this.g0.get(), (WorkoutSessionRepository) o24.this.F0.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter b() {
            ActivityOverviewMonthPresenter a2 = sc5.a(nc5.a(this.a), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter c() {
            ActivityOverviewWeekPresenter a2 = xc5.a(oc5.a(this.a), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public f(lc5 lc5) {
            a(lc5);
        }

        @DexIgnore
        public final ActivityOverviewFragment b(ActivityOverviewFragment activityOverviewFragment) {
            kc5.a(activityOverviewFragment, a());
            kc5.a(activityOverviewFragment, c());
            kc5.a(activityOverviewFragment, b());
            return activityOverviewFragment;
        }

        @DexIgnore
        public final void a(lc5 lc5) {
            z76.a(lc5);
            this.a = lc5;
        }

        @DexIgnore
        public void a(ActivityOverviewFragment activityOverviewFragment) {
            b(activityOverviewFragment);
        }

        @DexIgnore
        public final ActivityOverviewDayPresenter a(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            jc5.a(activityOverviewDayPresenter);
            return activityOverviewDayPresenter;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter a(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
            yc5.a(activityOverviewWeekPresenter);
            return activityOverviewWeekPresenter;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter a(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            tc5.a(activityOverviewMonthPresenter);
            return activityOverviewMonthPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f0 implements ha5 {
        @DexIgnore
        public void a(CustomizeTutorialFragment customizeTutorialFragment) {
            b(customizeTutorialFragment);
        }

        @DexIgnore
        public final CustomizeTutorialFragment b(CustomizeTutorialFragment customizeTutorialFragment) {
            v35.a(customizeTutorialFragment, (w04) o24.this.x2.get());
            return customizeTutorialFragment;
        }

        @DexIgnore
        public f0(ia5 ia5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f1 implements v15 {
        @DexIgnore
        public y15 a;

        @DexIgnore
        public final NotificationDialLandingPresenter a() {
            NotificationDialLandingPresenter a2 = c25.a(a25.a(this.a), b(), z15.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationsLoader b() {
            return NotificationsLoader_Factory.newNotificationsLoader((Context) o24.this.b.get(), (NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public f1(y15 y15) {
            a(y15);
        }

        @DexIgnore
        public final NotificationDialLandingActivity b(NotificationDialLandingActivity notificationDialLandingActivity) {
            xq4.a((BaseActivity) notificationDialLandingActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationDialLandingActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationDialLandingActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationDialLandingActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationDialLandingActivity, new os4());
            u15.a(notificationDialLandingActivity, a());
            return notificationDialLandingActivity;
        }

        @DexIgnore
        public final void a(y15 y15) {
            z76.a(y15);
            this.a = y15;
        }

        @DexIgnore
        public void a(NotificationDialLandingActivity notificationDialLandingActivity) {
            b(notificationDialLandingActivity);
        }

        @DexIgnore
        public final NotificationDialLandingPresenter a(NotificationDialLandingPresenter notificationDialLandingPresenter) {
            d25.a(notificationDialLandingPresenter);
            return notificationDialLandingPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f2 implements km5 {
        @DexIgnore
        public void a(ThemesFragment themesFragment) {
            b(themesFragment);
        }

        @DexIgnore
        public final ThemesFragment b(ThemesFragment themesFragment) {
            lm5.a(themesFragment, (w04) o24.this.x2.get());
            return themesFragment;
        }

        @DexIgnore
        public f2(mm5 mm5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements ov4 {
        @DexIgnore
        public rv4 a;

        @DexIgnore
        public final AlarmPresenter a() {
            AlarmPresenter a2 = wv4.a(uv4.a(this.a), tv4.a(this.a), sv4.a(this.a), this.a.a(), c(), (AlarmHelper) o24.this.G.get(), b(), (UserRepository) o24.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DeleteAlarm b() {
            return new DeleteAlarm((AlarmsRepository) o24.this.F.get());
        }

        @DexIgnore
        public final SetAlarms c() {
            return new SetAlarms((PortfolioApp) o24.this.d.get(), (AlarmsRepository) o24.this.F.get());
        }

        @DexIgnore
        public g(rv4 rv4) {
            a(rv4);
        }

        @DexIgnore
        public final AlarmActivity b(AlarmActivity alarmActivity) {
            xq4.a((BaseActivity) alarmActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) alarmActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) alarmActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) alarmActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) alarmActivity, new os4());
            nv4.a(alarmActivity, a());
            return alarmActivity;
        }

        @DexIgnore
        public final void a(rv4 rv4) {
            z76.a(rv4);
            this.a = rv4;
        }

        @DexIgnore
        public void a(AlarmActivity alarmActivity) {
            b(alarmActivity);
        }

        @DexIgnore
        public final AlarmPresenter a(AlarmPresenter alarmPresenter) {
            xv4.a(alarmPresenter);
            return alarmPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements ma5 {
        @DexIgnore
        public na5 a;

        @DexIgnore
        public final DashboardActiveTimePresenter a() {
            DashboardActiveTimePresenter a2 = cb5.a(oa5.a(this.a), (SummariesRepository) o24.this.L.get(), o24.this.x(), (ActivitySummaryDao) o24.this.I.get(), (FitnessDatabase) o24.this.H.get(), (UserRepository) o24.this.A.get(), (u04) o24.this.Q.get(), (ik4) o24.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardActivityPresenter b() {
            DashboardActivityPresenter a2 = cc5.a(pa5.a(this.a), (SummariesRepository) o24.this.L.get(), o24.this.x(), (ActivitySummaryDao) o24.this.I.get(), (FitnessDatabase) o24.this.H.get(), (UserRepository) o24.this.A.get(), (u04) o24.this.Q.get(), (ik4) o24.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter c() {
            DashboardCaloriesPresenter a2 = cd5.a(qa5.a(this.a), (SummariesRepository) o24.this.L.get(), o24.this.x(), (ActivitySummaryDao) o24.this.I.get(), (FitnessDatabase) o24.this.H.get(), (UserRepository) o24.this.A.get(), (u04) o24.this.Q.get(), (ik4) o24.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter d() {
            DashboardGoalTrackingPresenter a2 = ce5.a(ra5.a(this.a), (GoalTrackingRepository) o24.this.o0.get(), (UserRepository) o24.this.A.get(), (GoalTrackingDao) o24.this.n0.get(), (GoalTrackingDatabase) o24.this.m0.get(), (u04) o24.this.Q.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter e() {
            DashboardHeartRatePresenter a2 = cf5.a(sa5.a(this.a), (HeartRateSummaryRepository) o24.this.D0.get(), o24.this.x(), (HeartRateDailySummaryDao) o24.this.C0.get(), (FitnessDatabase) o24.this.H.get(), (UserRepository) o24.this.A.get(), (u04) o24.this.Q.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardSleepPresenter f() {
            DashboardSleepPresenter a2 = cg5.a(ta5.a(this.a), (SleepSummariesRepository) o24.this.O.get(), (SleepSessionsRepository) o24.this.l0.get(), o24.this.x(), (SleepDao) o24.this.N.get(), (SleepDatabase) o24.this.M.get(), (UserRepository) o24.this.A.get(), (u04) o24.this.Q.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public g0(na5 na5) {
            a(na5);
        }

        @DexIgnore
        public final void a(na5 na5) {
            z76.a(na5);
            this.a = na5;
        }

        @DexIgnore
        public final HomeDashboardFragment b(HomeDashboardFragment homeDashboardFragment) {
            vw4.a(homeDashboardFragment, b());
            vw4.a(homeDashboardFragment, a());
            vw4.a(homeDashboardFragment, c());
            vw4.a(homeDashboardFragment, e());
            vw4.a(homeDashboardFragment, f());
            vw4.a(homeDashboardFragment, d());
            return homeDashboardFragment;
        }

        @DexIgnore
        public void a(HomeDashboardFragment homeDashboardFragment) {
            b(homeDashboardFragment);
        }

        @DexIgnore
        public final DashboardActivityPresenter a(DashboardActivityPresenter dashboardActivityPresenter) {
            dc5.a(dashboardActivityPresenter);
            return dashboardActivityPresenter;
        }

        @DexIgnore
        public final DashboardActiveTimePresenter a(DashboardActiveTimePresenter dashboardActiveTimePresenter) {
            db5.a(dashboardActiveTimePresenter);
            return dashboardActiveTimePresenter;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter a(DashboardCaloriesPresenter dashboardCaloriesPresenter) {
            dd5.a(dashboardCaloriesPresenter);
            return dashboardCaloriesPresenter;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter a(DashboardHeartRatePresenter dashboardHeartRatePresenter) {
            df5.a(dashboardHeartRatePresenter);
            return dashboardHeartRatePresenter;
        }

        @DexIgnore
        public final DashboardSleepPresenter a(DashboardSleepPresenter dashboardSleepPresenter) {
            dg5.a(dashboardSleepPresenter);
            return dashboardSleepPresenter;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter a(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
            de5.a(dashboardGoalTrackingPresenter);
            return dashboardGoalTrackingPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g1 implements f25 {
        @DexIgnore
        public i25 a;

        @DexIgnore
        public final o25 a() {
            return new o25((Context) o24.this.b.get());
        }

        @DexIgnore
        public final l25 b() {
            l25 a2 = m25.a(k25.a(this.a), this.a.a(), j25.a(this.a), (z24) o24.this.Z.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public g1(i25 i25) {
            a(i25);
        }

        @DexIgnore
        public final void a(i25 i25) {
            z76.a(i25);
            this.a = i25;
        }

        @DexIgnore
        public void a(NotificationHybridAppActivity notificationHybridAppActivity) {
            b(notificationHybridAppActivity);
        }

        @DexIgnore
        public final l25 a(l25 l25) {
            n25.a(l25);
            return l25;
        }

        @DexIgnore
        public final NotificationHybridAppActivity b(NotificationHybridAppActivity notificationHybridAppActivity) {
            xq4.a((BaseActivity) notificationHybridAppActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationHybridAppActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationHybridAppActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationHybridAppActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationHybridAppActivity, new os4());
            e25.a(notificationHybridAppActivity, b());
            return notificationHybridAppActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g2 implements qu5 {
        @DexIgnore
        public tu5 a;

        @DexIgnore
        public final vu5 a() {
            vu5 a2 = wu5.a(uu5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final TroubleshootingActivity b(TroubleshootingActivity troubleshootingActivity) {
            xq4.a((BaseActivity) troubleshootingActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) troubleshootingActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) troubleshootingActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) troubleshootingActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) troubleshootingActivity, new os4());
            pu5.a(troubleshootingActivity, a());
            return troubleshootingActivity;
        }

        @DexIgnore
        public g2(tu5 tu5) {
            a(tu5);
        }

        @DexIgnore
        public final void a(tu5 tu5) {
            z76.a(tu5);
            this.a = tu5;
        }

        @DexIgnore
        public void a(TroubleshootingActivity troubleshootingActivity) {
            b(troubleshootingActivity);
        }

        @DexIgnore
        public final vu5 a(vu5 vu5) {
            xu5.a(vu5);
            return vu5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h implements or5 {
        @DexIgnore
        public rr5 a;

        @DexIgnore
        public final tr5 a() {
            tr5 a2 = ur5.a(sr5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileSetupFragment b(ProfileSetupFragment profileSetupFragment) {
            au4.a(profileSetupFragment, a());
            return profileSetupFragment;
        }

        @DexIgnore
        public h(rr5 rr5) {
            a(rr5);
        }

        @DexIgnore
        public final ProfileEditFragment b(ProfileEditFragment profileEditFragment) {
            lk5.a(profileEditFragment, (w04) o24.this.x2.get());
            lk5.a(profileEditFragment, a());
            return profileEditFragment;
        }

        @DexIgnore
        public final void a(rr5 rr5) {
            z76.a(rr5);
            this.a = rr5;
        }

        @DexIgnore
        public void a(ProfileSetupFragment profileSetupFragment) {
            b(profileSetupFragment);
        }

        @DexIgnore
        public void a(ProfileEditFragment profileEditFragment) {
            b(profileEditFragment);
        }

        @DexIgnore
        public final tr5 a(tr5 tr5) {
            vr5.a(tr5);
            return tr5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements kl5 {
        @DexIgnore
        public nl5 a;

        @DexIgnore
        public final DeleteAccountPresenter a() {
            DeleteAccountPresenter a2 = ql5.a(ol5.a(this.a), (DeviceRepository) o24.this.X.get(), (AnalyticsHelper) o24.this.O0.get(), b(), o24.this.k());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ws4 b() {
            return new ws4((UserRepository) o24.this.A.get(), (DeviceRepository) o24.this.X.get(), (an4) o24.this.c.get());
        }

        @DexIgnore
        public h0(nl5 nl5) {
            a(nl5);
        }

        @DexIgnore
        public final DeleteAccountActivity b(DeleteAccountActivity deleteAccountActivity) {
            xq4.a((BaseActivity) deleteAccountActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) deleteAccountActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) deleteAccountActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) deleteAccountActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) deleteAccountActivity, new os4());
            jl5.a(deleteAccountActivity, a());
            return deleteAccountActivity;
        }

        @DexIgnore
        public final void a(nl5 nl5) {
            z76.a(nl5);
            this.a = nl5;
        }

        @DexIgnore
        public void a(DeleteAccountActivity deleteAccountActivity) {
            b(deleteAccountActivity);
        }

        @DexIgnore
        public final DeleteAccountPresenter a(DeleteAccountPresenter deleteAccountPresenter) {
            rl5.a(deleteAccountPresenter);
            return deleteAccountPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h1 implements q25 {
        @DexIgnore
        public t25 a;

        @DexIgnore
        public final NotificationHybridContactPresenter a() {
            NotificationHybridContactPresenter a2 = y25.a(w25.a(this.a), this.a.b(), u25.a(this.a), v25.a(this.a), (z24) o24.this.Z.get(), new a35());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridContactActivity b(NotificationHybridContactActivity notificationHybridContactActivity) {
            xq4.a((BaseActivity) notificationHybridContactActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationHybridContactActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationHybridContactActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationHybridContactActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationHybridContactActivity, new os4());
            p25.a(notificationHybridContactActivity, a());
            return notificationHybridContactActivity;
        }

        @DexIgnore
        public h1(t25 t25) {
            a(t25);
        }

        @DexIgnore
        public final void a(t25 t25) {
            z76.a(t25);
            this.a = t25;
        }

        @DexIgnore
        public void a(NotificationHybridContactActivity notificationHybridContactActivity) {
            b(notificationHybridContactActivity);
        }

        @DexIgnore
        public final NotificationHybridContactPresenter a(NotificationHybridContactPresenter notificationHybridContactPresenter) {
            z25.a(notificationHybridContactPresenter);
            return notificationHybridContactPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h2 implements wq5 {
        @DexIgnore
        public zq5 a;

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase a() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) o24.this.d.get(), (GuestApiService) o24.this.P.get());
        }

        @DexIgnore
        public final UpdateFirmwarePresenter b() {
            UpdateFirmwarePresenter a2 = dr5.a(ar5.a(this.a), (DeviceRepository) o24.this.X.get(), (UserRepository) o24.this.A.get(), o24.this.l(), (an4) o24.this.c.get(), o24.this.W(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public h2(zq5 zq5) {
            a(zq5);
        }

        @DexIgnore
        public final void a(zq5 zq5) {
            z76.a(zq5);
            this.a = zq5;
        }

        @DexIgnore
        public void a(UpdateFirmwareActivity updateFirmwareActivity) {
            b(updateFirmwareActivity);
        }

        @DexIgnore
        public final UpdateFirmwarePresenter a(UpdateFirmwarePresenter updateFirmwarePresenter) {
            er5.a(updateFirmwarePresenter);
            return updateFirmwarePresenter;
        }

        @DexIgnore
        public final UpdateFirmwareActivity b(UpdateFirmwareActivity updateFirmwareActivity) {
            xq4.a((BaseActivity) updateFirmwareActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) updateFirmwareActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) updateFirmwareActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) updateFirmwareActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) updateFirmwareActivity, new os4());
            wr5.a(updateFirmwareActivity, b());
            return updateFirmwareActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public b14 a;
        @DexIgnore
        public PortfolioDatabaseModule b;
        @DexIgnore
        public RepositoriesModule c;
        @DexIgnore
        public NotificationsRepositoryModule d;
        @DexIgnore
        public MicroAppSettingRepositoryModule e;
        @DexIgnore
        public UAppSystemVersionRepositoryModule f;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public y04 a() {
            if (this.a != null) {
                if (this.b == null) {
                    this.b = new PortfolioDatabaseModule();
                }
                if (this.c == null) {
                    this.c = new RepositoriesModule();
                }
                if (this.d == null) {
                    this.d = new NotificationsRepositoryModule();
                }
                if (this.e == null) {
                    this.e = new MicroAppSettingRepositoryModule();
                }
                if (this.f == null) {
                    this.f = new UAppSystemVersionRepositoryModule();
                }
                return new o24(this);
            }
            throw new IllegalStateException(b14.class.getCanonicalName() + " must be set");
        }

        @DexIgnore
        public i a(b14 b14) {
            z76.a(b14);
            this.a = b14;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i0 implements x35 {
        @DexIgnore
        public b45 a;

        @DexIgnore
        public final om4 a() {
            return new om4((PortfolioApp) o24.this.d.get());
        }

        @DexIgnore
        public final DianaCustomizeEditPresenter b() {
            DianaCustomizeEditPresenter a2 = e45.a(c45.a(this.a), (UserRepository) o24.this.A.get(), this.a.a(), c(), a(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SetDianaPresetToWatchUseCase c() {
            return new SetDianaPresetToWatchUseCase((DianaPresetRepository) o24.this.t.get(), o24.this.e(), o24.this.Z(), o24.this.c0());
        }

        @DexIgnore
        public i0(b45 b45) {
            a(b45);
        }

        @DexIgnore
        public final void a(b45 b45) {
            z76.a(b45);
            this.a = b45;
        }

        @DexIgnore
        public void a(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            b(dianaCustomizeEditActivity);
        }

        @DexIgnore
        public final DianaCustomizeEditPresenter a(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            f45.a(dianaCustomizeEditPresenter);
            return dianaCustomizeEditPresenter;
        }

        @DexIgnore
        public final DianaCustomizeEditActivity b(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            xq4.a((BaseActivity) dianaCustomizeEditActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) dianaCustomizeEditActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) dianaCustomizeEditActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) dianaCustomizeEditActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) dianaCustomizeEditActivity, new os4());
            w35.a(dianaCustomizeEditActivity, b());
            w35.a(dianaCustomizeEditActivity, (w04) o24.this.x2.get());
            return dianaCustomizeEditActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i1 implements c35 {
        @DexIgnore
        public f35 a;

        @DexIgnore
        public final NotificationHybridEveryonePresenter a() {
            NotificationHybridEveryonePresenter a2 = j35.a(h35.a(this.a), this.a.b(), g35.a(this.a), (z24) o24.this.Z.get(), new a35());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridEveryoneActivity b(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            xq4.a((BaseActivity) notificationHybridEveryoneActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationHybridEveryoneActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationHybridEveryoneActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationHybridEveryoneActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationHybridEveryoneActivity, new os4());
            b35.a(notificationHybridEveryoneActivity, a());
            return notificationHybridEveryoneActivity;
        }

        @DexIgnore
        public i1(f35 f35) {
            a(f35);
        }

        @DexIgnore
        public final void a(f35 f35) {
            z76.a(f35);
            this.a = f35;
        }

        @DexIgnore
        public void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            b(notificationHybridEveryoneActivity);
        }

        @DexIgnore
        public final NotificationHybridEveryonePresenter a(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter) {
            k35.a(notificationHybridEveryonePresenter);
            return notificationHybridEveryonePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i2 implements no5 {
        @DexIgnore
        public void a(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            b(userCustomizeThemeFragment);
        }

        @DexIgnore
        public final UserCustomizeThemeFragment b(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            oo5.a(userCustomizeThemeFragment, (w04) o24.this.x2.get());
            return userCustomizeThemeFragment;
        }

        @DexIgnore
        public i2(po5 po5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements fv5 {
        @DexIgnore
        public iv5 a;

        @DexIgnore
        public final kv5 a() {
            kv5 a2 = mv5.a((PortfolioApp) o24.this.d.get(), jv5.a(this.a), (ce) o24.this.M1.get(), (el4) o24.this.O1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CalibrationActivity b(CalibrationActivity calibrationActivity) {
            xq4.a((BaseActivity) calibrationActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) calibrationActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) calibrationActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) calibrationActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) calibrationActivity, new os4());
            ev5.a(calibrationActivity, a());
            return calibrationActivity;
        }

        @DexIgnore
        public j(iv5 iv5) {
            a(iv5);
        }

        @DexIgnore
        public final void a(iv5 iv5) {
            z76.a(iv5);
            this.a = iv5;
        }

        @DexIgnore
        public void a(CalibrationActivity calibrationActivity) {
            b(calibrationActivity);
        }

        @DexIgnore
        public final kv5 a(kv5 kv5) {
            nv5.a(kv5);
            return kv5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j0 implements y65 {
        @DexIgnore
        public void a(EditPhotoFragment editPhotoFragment) {
            b(editPhotoFragment);
        }

        @DexIgnore
        public final EditPhotoFragment b(EditPhotoFragment editPhotoFragment) {
            z65.a(editPhotoFragment, (w04) o24.this.x2.get());
            return editPhotoFragment;
        }

        @DexIgnore
        public j0(a75 a75) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j1 implements dz4 {
        @DexIgnore
        public hz4 a;

        @DexIgnore
        public final jz4 a() {
            jz4 a2 = kz4.a(iz4.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public void a(NotificationAppsFragment notificationAppsFragment) {
        }

        @DexIgnore
        public final NotificationCallsAndMessagesFragment b(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            ly4.a(notificationCallsAndMessagesFragment, a());
            ly4.a(notificationCallsAndMessagesFragment, (w04) o24.this.x2.get());
            return notificationCallsAndMessagesFragment;
        }

        @DexIgnore
        public j1(hz4 hz4) {
            a(hz4);
        }

        @DexIgnore
        public final void a(hz4 hz4) {
            z76.a(hz4);
            this.a = hz4;
        }

        @DexIgnore
        public void a(NotificationCallsAndMessagesFragment notificationCallsAndMessagesFragment) {
            b(notificationCallsAndMessagesFragment);
        }

        @DexIgnore
        public final NotificationSettingsTypeFragment b(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
            gz4.a(notificationSettingsTypeFragment, (w04) o24.this.x2.get());
            return notificationSettingsTypeFragment;
        }

        @DexIgnore
        public void a(NotificationSettingsTypeFragment notificationSettingsTypeFragment) {
            b(notificationSettingsTypeFragment);
        }

        @DexIgnore
        public final jz4 a(jz4 jz4) {
            lz4.a(jz4);
            return jz4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j2 implements m85 {
        @DexIgnore
        public p85 a;

        @DexIgnore
        public final WatchAppSearchPresenter a() {
            WatchAppSearchPresenter a2 = s85.a(q85.a(this.a), o24.this.a0(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppSearchActivity b(WatchAppSearchActivity watchAppSearchActivity) {
            xq4.a((BaseActivity) watchAppSearchActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) watchAppSearchActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) watchAppSearchActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) watchAppSearchActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) watchAppSearchActivity, new os4());
            l85.a(watchAppSearchActivity, a());
            return watchAppSearchActivity;
        }

        @DexIgnore
        public j2(p85 p85) {
            a(p85);
        }

        @DexIgnore
        public final void a(p85 p85) {
            z76.a(p85);
            this.a = p85;
        }

        @DexIgnore
        public void a(WatchAppSearchActivity watchAppSearchActivity) {
            b(watchAppSearchActivity);
        }

        @DexIgnore
        public final WatchAppSearchPresenter a(WatchAppSearchPresenter watchAppSearchPresenter) {
            t85.a(watchAppSearchPresenter);
            return watchAppSearchPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k implements vh5 {
        @DexIgnore
        public yh5 a;

        @DexIgnore
        public final CaloriesDetailPresenter a() {
            CaloriesDetailPresenter a2 = bi5.a(zh5.a(this.a), (SummariesRepository) o24.this.L.get(), (ActivitiesRepository) o24.this.g0.get(), (UserRepository) o24.this.A.get(), (WorkoutSessionRepository) o24.this.F0.get(), (FitnessDataDao) o24.this.J.get(), (WorkoutDao) o24.this.E0.get(), (FitnessDatabase) o24.this.H.get(), (u04) o24.this.Q.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesDetailActivity b(CaloriesDetailActivity caloriesDetailActivity) {
            xq4.a((BaseActivity) caloriesDetailActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) caloriesDetailActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) caloriesDetailActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) caloriesDetailActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) caloriesDetailActivity, new os4());
            uh5.a(caloriesDetailActivity, a());
            return caloriesDetailActivity;
        }

        @DexIgnore
        public k(yh5 yh5) {
            a(yh5);
        }

        @DexIgnore
        public final void a(yh5 yh5) {
            z76.a(yh5);
            this.a = yh5;
        }

        @DexIgnore
        public void a(CaloriesDetailActivity caloriesDetailActivity) {
            b(caloriesDetailActivity);
        }

        @DexIgnore
        public final CaloriesDetailPresenter a(CaloriesDetailPresenter caloriesDetailPresenter) {
            ci5.a(caloriesDetailPresenter);
            return caloriesDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k0 implements du5 {
        @DexIgnore
        public eu5 a;

        @DexIgnore
        public final au5 a() {
            au5 a2 = bu5.a(fu5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final VerifyEmailOtp b() {
            return new VerifyEmailOtp((UserRepository) o24.this.A.get());
        }

        @DexIgnore
        public k0(eu5 eu5) {
            a(eu5);
        }

        @DexIgnore
        public final EmailOtpVerificationActivity b(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            xq4.a((BaseActivity) emailOtpVerificationActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) emailOtpVerificationActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) emailOtpVerificationActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) emailOtpVerificationActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) emailOtpVerificationActivity, new os4());
            xt5.a(emailOtpVerificationActivity, a());
            return emailOtpVerificationActivity;
        }

        @DexIgnore
        public final void a(eu5 eu5) {
            z76.a(eu5);
            this.a = eu5;
        }

        @DexIgnore
        public void a(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            b(emailOtpVerificationActivity);
        }

        @DexIgnore
        public final au5 a(au5 au5) {
            cu5.a(au5, o24.this.N());
            cu5.a(au5, b());
            cu5.a(au5);
            return au5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k1 implements g05 {
        @DexIgnore
        public k05 a;

        @DexIgnore
        public final NotificationWatchRemindersPresenter a() {
            NotificationWatchRemindersPresenter a2 = n05.a(l05.a(this.a), (an4) o24.this.c.get(), (RemindersSettingsDatabase) o24.this.G0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationWatchRemindersActivity b(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            xq4.a((BaseActivity) notificationWatchRemindersActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) notificationWatchRemindersActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) notificationWatchRemindersActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) notificationWatchRemindersActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) notificationWatchRemindersActivity, new os4());
            f05.a(notificationWatchRemindersActivity, a());
            return notificationWatchRemindersActivity;
        }

        @DexIgnore
        public k1(k05 k05) {
            a(k05);
        }

        @DexIgnore
        public final void a(k05 k05) {
            z76.a(k05);
            this.a = k05;
        }

        @DexIgnore
        public void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            b(notificationWatchRemindersActivity);
        }

        @DexIgnore
        public final NotificationWatchRemindersPresenter a(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            o05.a(notificationWatchRemindersPresenter);
            return notificationWatchRemindersPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k2 implements k75 {
        @DexIgnore
        public void a(WatchAppsFragment watchAppsFragment) {
            b(watchAppsFragment);
        }

        @DexIgnore
        public final WatchAppsFragment b(WatchAppsFragment watchAppsFragment) {
            n75.a(watchAppsFragment, (w04) o24.this.x2.get());
            return watchAppsFragment;
        }

        @DexIgnore
        public k2(o75 o75) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l implements ed5 {
        @DexIgnore
        public ld5 a;

        @DexIgnore
        public final CaloriesOverviewDayPresenter a() {
            CaloriesOverviewDayPresenter a2 = id5.a(md5.a(this.a), (SummariesRepository) o24.this.L.get(), (ActivitiesRepository) o24.this.g0.get(), (WorkoutSessionRepository) o24.this.F0.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter b() {
            CaloriesOverviewMonthPresenter a2 = sd5.a(nd5.a(this.a), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter c() {
            CaloriesOverviewWeekPresenter a2 = xd5.a(od5.a(this.a), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public l(ld5 ld5) {
            a(ld5);
        }

        @DexIgnore
        public final CaloriesOverviewFragment b(CaloriesOverviewFragment caloriesOverviewFragment) {
            kd5.a(caloriesOverviewFragment, a());
            kd5.a(caloriesOverviewFragment, c());
            kd5.a(caloriesOverviewFragment, b());
            return caloriesOverviewFragment;
        }

        @DexIgnore
        public final void a(ld5 ld5) {
            z76.a(ld5);
            this.a = ld5;
        }

        @DexIgnore
        public void a(CaloriesOverviewFragment caloriesOverviewFragment) {
            b(caloriesOverviewFragment);
        }

        @DexIgnore
        public final CaloriesOverviewDayPresenter a(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            jd5.a(caloriesOverviewDayPresenter);
            return caloriesOverviewDayPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter a(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
            yd5.a(caloriesOverviewWeekPresenter);
            return caloriesOverviewWeekPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter a(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
            td5.a(caloriesOverviewMonthPresenter);
            return caloriesOverviewMonthPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l0 implements wp5 {
        @DexIgnore
        public zp5 a;

        @DexIgnore
        public final ExploreWatchPresenter a() {
            ExploreWatchPresenter a2 = cq5.a(aq5.a(this.a), o24.this.l(), (DeviceRepository) o24.this.X.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ExploreWatchActivity b(ExploreWatchActivity exploreWatchActivity) {
            xq4.a((BaseActivity) exploreWatchActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) exploreWatchActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) exploreWatchActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) exploreWatchActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) exploreWatchActivity, new os4());
            vp5.a(exploreWatchActivity, a());
            return exploreWatchActivity;
        }

        @DexIgnore
        public l0(zp5 zp5) {
            a(zp5);
        }

        @DexIgnore
        public final void a(zp5 zp5) {
            z76.a(zp5);
            this.a = zp5;
        }

        @DexIgnore
        public void a(ExploreWatchActivity exploreWatchActivity) {
            b(exploreWatchActivity);
        }

        @DexIgnore
        public final ExploreWatchPresenter a(ExploreWatchPresenter exploreWatchPresenter) {
            dq5.a(exploreWatchPresenter);
            return exploreWatchPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l1 implements oq5 {
        @DexIgnore
        public rq5 a;

        @DexIgnore
        public final GetRecommendedGoalUseCase a() {
            return new GetRecommendedGoalUseCase((SummariesRepository) o24.this.L.get(), (SleepSummariesRepository) o24.this.O.get(), (SleepSessionsRepository) o24.this.l0.get());
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter b() {
            OnboardingHeightWeightPresenter a2 = uq5.a(sq5.a(this.a), (UserRepository) o24.this.A.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public l1(rq5 rq5) {
            a(rq5);
        }

        @DexIgnore
        public final void a(rq5 rq5) {
            z76.a(rq5);
            this.a = rq5;
        }

        @DexIgnore
        public void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            b(onboardingHeightWeightActivity);
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter a(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
            vq5.a(onboardingHeightWeightPresenter);
            return onboardingHeightWeightPresenter;
        }

        @DexIgnore
        public final OnboardingHeightWeightActivity b(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            xq4.a((BaseActivity) onboardingHeightWeightActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) onboardingHeightWeightActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) onboardingHeightWeightActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) onboardingHeightWeightActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) onboardingHeightWeightActivity, new os4());
            nq5.a(onboardingHeightWeightActivity, b());
            return onboardingHeightWeightActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l2 implements yu5 {
        @DexIgnore
        public void a(WatchSettingFragment watchSettingFragment) {
            b(watchSettingFragment);
        }

        @DexIgnore
        public final WatchSettingFragment b(WatchSettingFragment watchSettingFragment) {
            zu5.a(watchSettingFragment, (w04) o24.this.x2.get());
            return watchSettingFragment;
        }

        @DexIgnore
        public l2(av5 av5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m implements x45 {
        @DexIgnore
        public j55 a;

        @DexIgnore
        public final CommuteTimeSettingsPresenter a() {
            CommuteTimeSettingsPresenter a2 = m55.a(k55.a(this.a), (an4) o24.this.c.get(), (UserRepository) o24.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsActivity b(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            xq4.a((BaseActivity) commuteTimeSettingsActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) commuteTimeSettingsActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) commuteTimeSettingsActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) commuteTimeSettingsActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) commuteTimeSettingsActivity, new os4());
            w45.a(commuteTimeSettingsActivity, a());
            return commuteTimeSettingsActivity;
        }

        @DexIgnore
        public m(j55 j55) {
            a(j55);
        }

        @DexIgnore
        public final void a(j55 j55) {
            z76.a(j55);
            this.a = j55;
        }

        @DexIgnore
        public void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            b(commuteTimeSettingsActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsPresenter a(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter) {
            n55.a(commuteTimeSettingsPresenter);
            return commuteTimeSettingsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m0 implements pv5 {
        @DexIgnore
        public sv5 a;

        @DexIgnore
        public final FindDevicePresenter a() {
            FindDevicePresenter a2 = vv5.a((ce) o24.this.M1.get(), (DeviceRepository) o24.this.X.get(), (an4) o24.this.c.get(), tv5.a(this.a), new ks4(), c(), b(), new os4(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GetAddress b() {
            return new GetAddress((GoogleApiService) o24.this.N1.get());
        }

        @DexIgnore
        public final ns4 c() {
            return new ns4((s04) o24.this.P1.get());
        }

        @DexIgnore
        public m0(sv5 sv5) {
            a(sv5);
        }

        @DexIgnore
        public final FindDeviceActivity b(FindDeviceActivity findDeviceActivity) {
            xq4.a((BaseActivity) findDeviceActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) findDeviceActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) findDeviceActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) findDeviceActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) findDeviceActivity, new os4());
            ov5.a(findDeviceActivity, a());
            return findDeviceActivity;
        }

        @DexIgnore
        public final void a(sv5 sv5) {
            z76.a(sv5);
            this.a = sv5;
        }

        @DexIgnore
        public void a(FindDeviceActivity findDeviceActivity) {
            b(findDeviceActivity);
        }

        @DexIgnore
        public final FindDevicePresenter a(FindDevicePresenter findDevicePresenter) {
            wv5.a(findDevicePresenter);
            return findDevicePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m1 implements hs5 {
        @DexIgnore
        public os5 a;

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase a() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) o24.this.d.get(), (GuestApiService) o24.this.P.get());
        }

        @DexIgnore
        public final LinkDeviceUseCase b() {
            return new LinkDeviceUseCase((DeviceRepository) o24.this.X.get(), a(), (an4) o24.this.c.get(), (UserRepository) o24.this.A.get(), o24.this.j(), o24.this.l());
        }

        @DexIgnore
        public final PairingPresenter c() {
            PairingPresenter a2 = ss5.a(ps5.a(this.a), b(), (DeviceRepository) o24.this.X.get(), o24.this.l(), o24.this.Q(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public m1(os5 os5) {
            a(os5);
        }

        @DexIgnore
        public final void a(os5 os5) {
            z76.a(os5);
            this.a = os5;
        }

        @DexIgnore
        public void a(PairingActivity pairingActivity) {
            b(pairingActivity);
        }

        @DexIgnore
        public final PairingPresenter a(PairingPresenter pairingPresenter) {
            ts5.a(pairingPresenter);
            return pairingPresenter;
        }

        @DexIgnore
        public final PairingActivity b(PairingActivity pairingActivity) {
            xq4.a((BaseActivity) pairingActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) pairingActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) pairingActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) pairingActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) pairingActivity, new os4());
            gs5.a(pairingActivity, c());
            return pairingActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m2 implements d85 {
        @DexIgnore
        public g85 a;

        @DexIgnore
        public final WeatherSettingPresenter a() {
            WeatherSettingPresenter a2 = j85.a(h85.a(this.a), (GoogleApiService) o24.this.N1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WeatherSettingActivity b(WeatherSettingActivity weatherSettingActivity) {
            xq4.a((BaseActivity) weatherSettingActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) weatherSettingActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) weatherSettingActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) weatherSettingActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) weatherSettingActivity, new os4());
            c85.a(weatherSettingActivity, a());
            return weatherSettingActivity;
        }

        @DexIgnore
        public m2(g85 g85) {
            a(g85);
        }

        @DexIgnore
        public final void a(g85 g85) {
            z76.a(g85);
            this.a = g85;
        }

        @DexIgnore
        public void a(WeatherSettingActivity weatherSettingActivity) {
            b(weatherSettingActivity);
        }

        @DexIgnore
        public final WeatherSettingPresenter a(WeatherSettingPresenter weatherSettingPresenter) {
            k85.a(weatherSettingPresenter);
            return weatherSettingPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n implements b55 {
        @DexIgnore
        public e55 a;

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter a() {
            CommuteTimeSettingsDefaultAddressPresenter a2 = h55.a(f55.a(this.a), (an4) o24.this.c.get(), (UserRepository) o24.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressActivity b(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            xq4.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, new os4());
            a55.a(commuteTimeSettingsDefaultAddressActivity, a());
            return commuteTimeSettingsDefaultAddressActivity;
        }

        @DexIgnore
        public n(e55 e55) {
            a(e55);
        }

        @DexIgnore
        public final void a(e55 e55) {
            z76.a(e55);
            this.a = e55;
        }

        @DexIgnore
        public void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            b(commuteTimeSettingsDefaultAddressActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter a(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
            i55.a(commuteTimeSettingsDefaultAddressPresenter);
            return commuteTimeSettingsDefaultAddressPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n0 implements fq5 {
        @DexIgnore
        public iq5 a;

        @DexIgnore
        public final kq5 a() {
            kq5 a2 = lq5.a(jq5.a(this.a), b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ResetPasswordUseCase b() {
            return new ResetPasswordUseCase((AuthApiGuestService) o24.this.o.get());
        }

        @DexIgnore
        public n0(iq5 iq5) {
            a(iq5);
        }

        @DexIgnore
        public final ForgotPasswordActivity b(ForgotPasswordActivity forgotPasswordActivity) {
            xq4.a((BaseActivity) forgotPasswordActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) forgotPasswordActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) forgotPasswordActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) forgotPasswordActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) forgotPasswordActivity, new os4());
            eq5.a(forgotPasswordActivity, a());
            return forgotPasswordActivity;
        }

        @DexIgnore
        public final void a(iq5 iq5) {
            z76.a(iq5);
            this.a = iq5;
        }

        @DexIgnore
        public void a(ForgotPasswordActivity forgotPasswordActivity) {
            b(forgotPasswordActivity);
        }

        @DexIgnore
        public final kq5 a(kq5 kq5) {
            mq5.a(kq5);
            return kq5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n1 implements yr5 {
        @DexIgnore
        public bs5 a;

        @DexIgnore
        public final ds5 a() {
            ds5 a2 = es5.a(cs5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PairingInstructionsActivity b(PairingInstructionsActivity pairingInstructionsActivity) {
            xq4.a((BaseActivity) pairingInstructionsActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) pairingInstructionsActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) pairingInstructionsActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) pairingInstructionsActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) pairingInstructionsActivity, new os4());
            xr5.a(pairingInstructionsActivity, a());
            return pairingInstructionsActivity;
        }

        @DexIgnore
        public n1(bs5 bs5) {
            a(bs5);
        }

        @DexIgnore
        public final void a(bs5 bs5) {
            z76.a(bs5);
            this.a = bs5;
        }

        @DexIgnore
        public void a(PairingInstructionsActivity pairingInstructionsActivity) {
            b(pairingInstructionsActivity);
        }

        @DexIgnore
        public final ds5 a(ds5 ds5) {
            fs5.a(ds5);
            return ds5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n2 implements yv5 {
        @DexIgnore
        public cw5 a;

        @DexIgnore
        public final bw5 a() {
            bw5 a2 = ew5.a(dw5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WelcomeActivity b(WelcomeActivity welcomeActivity) {
            xq4.a((BaseActivity) welcomeActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) welcomeActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) welcomeActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) welcomeActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) welcomeActivity, new os4());
            xv5.a(welcomeActivity, a());
            return welcomeActivity;
        }

        @DexIgnore
        public n2(cw5 cw5) {
            a(cw5);
        }

        @DexIgnore
        public final void a(cw5 cw5) {
            z76.a(cw5);
            this.a = cw5;
        }

        @DexIgnore
        public void a(WelcomeActivity welcomeActivity) {
            b(welcomeActivity);
        }

        @DexIgnore
        public final bw5 a(bw5 bw5) {
            fw5.a(bw5);
            return bw5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o implements u75 {
        @DexIgnore
        public void a(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            b(commuteTimeSettingsDetailFragment);
        }

        @DexIgnore
        public final CommuteTimeSettingsDetailFragment b(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            s75.a(commuteTimeSettingsDetailFragment, (w04) o24.this.x2.get());
            return commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public o(v75 v75) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o0 implements ei5 {
        @DexIgnore
        public hi5 a;

        @DexIgnore
        public final GoalTrackingDetailPresenter a() {
            GoalTrackingDetailPresenter a2 = ki5.a(ii5.a(this.a), (GoalTrackingRepository) o24.this.o0.get(), (GoalTrackingDao) o24.this.n0.get(), (GoalTrackingDatabase) o24.this.m0.get(), (u04) o24.this.Q.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingDetailActivity b(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            xq4.a((BaseActivity) goalTrackingDetailActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) goalTrackingDetailActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) goalTrackingDetailActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) goalTrackingDetailActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) goalTrackingDetailActivity, new os4());
            di5.a(goalTrackingDetailActivity, a());
            return goalTrackingDetailActivity;
        }

        @DexIgnore
        public o0(hi5 hi5) {
            a(hi5);
        }

        @DexIgnore
        public final void a(hi5 hi5) {
            z76.a(hi5);
            this.a = hi5;
        }

        @DexIgnore
        public void a(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            b(goalTrackingDetailActivity);
        }

        @DexIgnore
        public final GoalTrackingDetailPresenter a(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            li5.a(goalTrackingDetailPresenter);
            return goalTrackingDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o1 implements et5 {
        @DexIgnore
        public ht5 a;

        @DexIgnore
        public final kt5 a() {
            kt5 a2 = lt5.a(jt5.a(this.a), it5.a(this.a), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PermissionActivity b(PermissionActivity permissionActivity) {
            xq4.a((BaseActivity) permissionActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) permissionActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) permissionActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) permissionActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) permissionActivity, new os4());
            dt5.a(permissionActivity, a());
            return permissionActivity;
        }

        @DexIgnore
        public o1(ht5 ht5) {
            a(ht5);
        }

        @DexIgnore
        public final void a(ht5 ht5) {
            z76.a(ht5);
            this.a = ht5;
        }

        @DexIgnore
        public void a(PermissionActivity permissionActivity) {
            b(permissionActivity);
        }

        @DexIgnore
        public final kt5 a(kt5 kt5) {
            mt5.a(kt5);
            return kt5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p implements y75 {
        @DexIgnore
        public void a(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            b(commuteTimeWatchAppSettingsFragment);
        }

        @DexIgnore
        public final CommuteTimeWatchAppSettingsFragment b(CommuteTimeWatchAppSettingsFragment commuteTimeWatchAppSettingsFragment) {
            t75.a(commuteTimeWatchAppSettingsFragment, (w04) o24.this.x2.get());
            return commuteTimeWatchAppSettingsFragment;
        }

        @DexIgnore
        public p(z75 z75) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p0 implements ee5 {
        @DexIgnore
        public le5 a;

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter a() {
            GoalTrackingOverviewDayPresenter a2 = ie5.a(me5.a(this.a), (an4) o24.this.c.get(), (GoalTrackingRepository) o24.this.o0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter b() {
            GoalTrackingOverviewMonthPresenter a2 = se5.a(ne5.a(this.a), (UserRepository) o24.this.A.get(), (an4) o24.this.c.get(), (GoalTrackingRepository) o24.this.o0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter c() {
            GoalTrackingOverviewWeekPresenter a2 = xe5.a(oe5.a(this.a), (UserRepository) o24.this.A.get(), (an4) o24.this.c.get(), (GoalTrackingRepository) o24.this.o0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public p0(le5 le5) {
            a(le5);
        }

        @DexIgnore
        public final void a(le5 le5) {
            z76.a(le5);
            this.a = le5;
        }

        @DexIgnore
        public void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            b(goalTrackingOverviewFragment);
        }

        @DexIgnore
        public final GoalTrackingOverviewFragment b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            ke5.a(goalTrackingOverviewFragment, a());
            ke5.a(goalTrackingOverviewFragment, c());
            ke5.a(goalTrackingOverviewFragment, b());
            return goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter a(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            je5.a(goalTrackingOverviewDayPresenter);
            return goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter a(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
            ye5.a(goalTrackingOverviewWeekPresenter);
            return goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter a(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            te5.a(goalTrackingOverviewMonthPresenter);
            return goalTrackingOverviewMonthPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p1 implements to5 {
        @DexIgnore
        public vo5 a;

        @DexIgnore
        public final xo5 a() {
            xo5 a2 = yo5.a(wo5.a(this.a), o24.this.X(), o24.this.B());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PreferredUnitActivity b(PreferredUnitActivity preferredUnitActivity) {
            xq4.a((BaseActivity) preferredUnitActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) preferredUnitActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) preferredUnitActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) preferredUnitActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) preferredUnitActivity, new os4());
            so5.a(preferredUnitActivity, a());
            return preferredUnitActivity;
        }

        @DexIgnore
        public p1(vo5 vo5) {
            a(vo5);
        }

        @DexIgnore
        public final void a(vo5 vo5) {
            z76.a(vo5);
            this.a = vo5;
        }

        @DexIgnore
        public void a(PreferredUnitActivity preferredUnitActivity) {
            b(preferredUnitActivity);
        }

        @DexIgnore
        public final xo5 a(xo5 xo5) {
            zo5.a(xo5);
            return xo5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q implements h65 {
        @DexIgnore
        public k65 a;

        @DexIgnore
        public final ComplicationSearchPresenter a() {
            ComplicationSearchPresenter a2 = n65.a(l65.a(this.a), o24.this.f(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ComplicationSearchActivity b(ComplicationSearchActivity complicationSearchActivity) {
            xq4.a((BaseActivity) complicationSearchActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) complicationSearchActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) complicationSearchActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) complicationSearchActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) complicationSearchActivity, new os4());
            g65.a(complicationSearchActivity, a());
            return complicationSearchActivity;
        }

        @DexIgnore
        public q(k65 k65) {
            a(k65);
        }

        @DexIgnore
        public final void a(k65 k65) {
            z76.a(k65);
            this.a = k65;
        }

        @DexIgnore
        public void a(ComplicationSearchActivity complicationSearchActivity) {
            b(complicationSearchActivity);
        }

        @DexIgnore
        public final ComplicationSearchPresenter a(ComplicationSearchPresenter complicationSearchPresenter) {
            o65.a(complicationSearchPresenter);
            return complicationSearchPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q0 implements ni5 {
        @DexIgnore
        public qi5 a;

        @DexIgnore
        public final HeartRateDetailPresenter a() {
            HeartRateDetailPresenter a2 = ti5.a(ri5.a(this.a), (HeartRateSummaryRepository) o24.this.D0.get(), (HeartRateSampleRepository) o24.this.B0.get(), (UserRepository) o24.this.A.get(), (WorkoutSessionRepository) o24.this.F0.get(), (FitnessDataDao) o24.this.J.get(), (WorkoutDao) o24.this.E0.get(), (FitnessDatabase) o24.this.H.get(), (u04) o24.this.Q.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateDetailActivity b(HeartRateDetailActivity heartRateDetailActivity) {
            xq4.a((BaseActivity) heartRateDetailActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) heartRateDetailActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) heartRateDetailActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) heartRateDetailActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) heartRateDetailActivity, new os4());
            mi5.a(heartRateDetailActivity, a());
            return heartRateDetailActivity;
        }

        @DexIgnore
        public q0(qi5 qi5) {
            a(qi5);
        }

        @DexIgnore
        public final void a(qi5 qi5) {
            z76.a(qi5);
            this.a = qi5;
        }

        @DexIgnore
        public void a(HeartRateDetailActivity heartRateDetailActivity) {
            b(heartRateDetailActivity);
        }

        @DexIgnore
        public final HeartRateDetailPresenter a(HeartRateDetailPresenter heartRateDetailPresenter) {
            ui5.a(heartRateDetailPresenter);
            return heartRateDetailPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q1 implements f75 {
        @DexIgnore
        public void a(PreviewFragment previewFragment) {
            b(previewFragment);
        }

        @DexIgnore
        public final PreviewFragment b(PreviewFragment previewFragment) {
            g75.a(previewFragment, (w04) o24.this.x2.get());
            return previewFragment;
        }

        @DexIgnore
        public q1(h75 h75) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r implements p35 {
        @DexIgnore
        public q35 a;

        @DexIgnore
        public final ComplicationsPresenter a() {
            ComplicationsPresenter a2 = u45.a(r35.a(this.a), o24.this.b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CustomizeThemePresenter b() {
            CustomizeThemePresenter a2 = v65.a(s35.a(this.a), o24.this.c0(), (DianaPresetRepository) o24.this.t.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppsPresenter c() {
            WatchAppsPresenter a2 = q75.a(t35.a(this.a), o24.this.b(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public r(q35 q35) {
            a(q35);
        }

        @DexIgnore
        public final void a(q35 q35) {
            z76.a(q35);
            this.a = q35;
        }

        @DexIgnore
        public void a(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            b(dianaCustomizeEditFragment);
        }

        @DexIgnore
        public final DianaCustomizeEditFragment b(DianaCustomizeEditFragment dianaCustomizeEditFragment) {
            a45.a(dianaCustomizeEditFragment, a());
            a45.a(dianaCustomizeEditFragment, c());
            a45.a(dianaCustomizeEditFragment, b());
            a45.a(dianaCustomizeEditFragment, (w04) o24.this.x2.get());
            return dianaCustomizeEditFragment;
        }

        @DexIgnore
        public final ComplicationsPresenter a(ComplicationsPresenter complicationsPresenter) {
            v45.a(complicationsPresenter);
            return complicationsPresenter;
        }

        @DexIgnore
        public final WatchAppsPresenter a(WatchAppsPresenter watchAppsPresenter) {
            r75.a(watchAppsPresenter);
            return watchAppsPresenter;
        }

        @DexIgnore
        public final CustomizeThemePresenter a(CustomizeThemePresenter customizeThemePresenter) {
            w65.a(customizeThemePresenter);
            return customizeThemePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r0 implements ef5 {
        @DexIgnore
        public lf5 a;

        @DexIgnore
        public final HeartRateOverviewDayPresenter a() {
            HeartRateOverviewDayPresenter a2 = if5.a(mf5.a(this.a), (HeartRateSampleRepository) o24.this.B0.get(), (WorkoutSessionRepository) o24.this.F0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter b() {
            HeartRateOverviewMonthPresenter a2 = sf5.a(nf5.a(this.a), (UserRepository) o24.this.A.get(), (HeartRateSummaryRepository) o24.this.D0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter c() {
            HeartRateOverviewWeekPresenter a2 = xf5.a(of5.a(this.a), (UserRepository) o24.this.A.get(), (HeartRateSummaryRepository) o24.this.D0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public r0(lf5 lf5) {
            a(lf5);
        }

        @DexIgnore
        public final void a(lf5 lf5) {
            z76.a(lf5);
            this.a = lf5;
        }

        @DexIgnore
        public final HeartRateOverviewFragment b(HeartRateOverviewFragment heartRateOverviewFragment) {
            kf5.a(heartRateOverviewFragment, a());
            kf5.a(heartRateOverviewFragment, c());
            kf5.a(heartRateOverviewFragment, b());
            return heartRateOverviewFragment;
        }

        @DexIgnore
        public void a(HeartRateOverviewFragment heartRateOverviewFragment) {
            b(heartRateOverviewFragment);
        }

        @DexIgnore
        public final HeartRateOverviewDayPresenter a(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            jf5.a(heartRateOverviewDayPresenter);
            return heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter a(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
            yf5.a(heartRateOverviewWeekPresenter);
            return heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter a(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            tf5.a(heartRateOverviewMonthPresenter);
            return heartRateOverviewMonthPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r1 implements cm5 {
        @DexIgnore
        public fm5 a;

        @DexIgnore
        public final vs4 a() {
            return new vs4((AuthApiUserService) o24.this.x.get());
        }

        @DexIgnore
        public final hm5 b() {
            hm5 a2 = im5.a(gm5.a(this.a), a(), (z24) o24.this.Z.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public r1(fm5 fm5) {
            a(fm5);
        }

        @DexIgnore
        public final void a(fm5 fm5) {
            z76.a(fm5);
            this.a = fm5;
        }

        @DexIgnore
        public void a(ProfileChangePasswordActivity profileChangePasswordActivity) {
            b(profileChangePasswordActivity);
        }

        @DexIgnore
        public final hm5 a(hm5 hm5) {
            jm5.a(hm5);
            return hm5;
        }

        @DexIgnore
        public final ProfileChangePasswordActivity b(ProfileChangePasswordActivity profileChangePasswordActivity) {
            xq4.a((BaseActivity) profileChangePasswordActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) profileChangePasswordActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) profileChangePasswordActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) profileChangePasswordActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) profileChangePasswordActivity, new os4());
            bm5.a(profileChangePasswordActivity, b());
            return profileChangePasswordActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s implements o45 {
        @DexIgnore
        public void a(ComplicationsFragment complicationsFragment) {
            b(complicationsFragment);
        }

        @DexIgnore
        public final ComplicationsFragment b(ComplicationsFragment complicationsFragment) {
            r45.a(complicationsFragment, (w04) o24.this.x2.get());
            return complicationsFragment;
        }

        @DexIgnore
        public s(s45 s45) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s0 implements bl5 {
        @DexIgnore
        public el5 a;

        @DexIgnore
        public final ws4 a() {
            return new ws4((UserRepository) o24.this.A.get(), (DeviceRepository) o24.this.X.get(), (an4) o24.this.c.get());
        }

        @DexIgnore
        public final HelpPresenter b() {
            HelpPresenter a2 = hl5.a(fl5.a(this.a), (DeviceRepository) o24.this.X.get(), a(), (AnalyticsHelper) o24.this.O0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public s0(el5 el5) {
            a(el5);
        }

        @DexIgnore
        public final void a(el5 el5) {
            z76.a(el5);
            this.a = el5;
        }

        @DexIgnore
        public void a(HelpActivity helpActivity) {
            b(helpActivity);
        }

        @DexIgnore
        public final HelpPresenter a(HelpPresenter helpPresenter) {
            il5.a(helpPresenter);
            return helpPresenter;
        }

        @DexIgnore
        public final HelpActivity b(HelpActivity helpActivity) {
            xq4.a((BaseActivity) helpActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) helpActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) helpActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) helpActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) helpActivity, new os4());
            al5.a(helpActivity, b());
            return helpActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s1 implements qk5 {
        @DexIgnore
        public uk5 a;

        @DexIgnore
        public final ProfileGoalEditPresenter a() {
            ProfileGoalEditPresenter a2 = yk5.a(vk5.a(this.a), (SummariesRepository) o24.this.L.get(), (SleepSummariesRepository) o24.this.O.get(), (GoalTrackingRepository) o24.this.o0.get(), (UserRepository) o24.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileGoalEditActivity b(ProfileGoalEditActivity profileGoalEditActivity) {
            xq4.a((BaseActivity) profileGoalEditActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) profileGoalEditActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) profileGoalEditActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) profileGoalEditActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) profileGoalEditActivity, new os4());
            pk5.a(profileGoalEditActivity, a());
            return profileGoalEditActivity;
        }

        @DexIgnore
        public s1(uk5 uk5) {
            a(uk5);
        }

        @DexIgnore
        public final void a(uk5 uk5) {
            z76.a(uk5);
            this.a = uk5;
        }

        @DexIgnore
        public void a(ProfileGoalEditActivity profileGoalEditActivity) {
            b(profileGoalEditActivity);
        }

        @DexIgnore
        public final ProfileGoalEditPresenter a(ProfileGoalEditPresenter profileGoalEditPresenter) {
            zk5.a(profileGoalEditPresenter);
            return profileGoalEditPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t implements bw4 {
        @DexIgnore
        public ew4 a;

        @DexIgnore
        public final ConnectedAppsPresenter a() {
            ConnectedAppsPresenter a2 = hw4.a(fw4.a(this.a), (UserRepository) o24.this.A.get(), (PortfolioApp) o24.this.d.get(), o24.this.D());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ConnectedAppsActivity b(ConnectedAppsActivity connectedAppsActivity) {
            xq4.a((BaseActivity) connectedAppsActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) connectedAppsActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) connectedAppsActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) connectedAppsActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) connectedAppsActivity, new os4());
            aw4.a(connectedAppsActivity, a());
            return connectedAppsActivity;
        }

        @DexIgnore
        public t(ew4 ew4) {
            a(ew4);
        }

        @DexIgnore
        public final void a(ew4 ew4) {
            z76.a(ew4);
            this.a = ew4;
        }

        @DexIgnore
        public void a(ConnectedAppsActivity connectedAppsActivity) {
            b(connectedAppsActivity);
        }

        @DexIgnore
        public final ConnectedAppsPresenter a(ConnectedAppsPresenter connectedAppsPresenter) {
            iw4.a(connectedAppsPresenter);
            return connectedAppsPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t0 implements mx4 {
        @DexIgnore
        public qx4 a;

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter a() {
            DoNotDisturbScheduledTimePresenter a2 = b15.a(rx4.a(this.a), (DNDSettingsDatabase) o24.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeAlertsFragment b(HomeAlertsFragment homeAlertsFragment) {
            px4.a(homeAlertsFragment, a());
            return homeAlertsFragment;
        }

        @DexIgnore
        public t0(qx4 qx4) {
            a(qx4);
        }

        @DexIgnore
        public final void a(qx4 qx4) {
            z76.a(qx4);
            this.a = qx4;
        }

        @DexIgnore
        public void a(HomeAlertsFragment homeAlertsFragment) {
            b(homeAlertsFragment);
        }

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter a(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter) {
            c15.a(doNotDisturbScheduledTimePresenter);
            return doNotDisturbScheduledTimePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t1 implements tl5 {
        @DexIgnore
        public wl5 a;

        @DexIgnore
        public final ProfileOptInPresenter a() {
            ProfileOptInPresenter a2 = zl5.a(xl5.a(this.a), o24.this.X(), (UserRepository) o24.this.A.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileOptInActivity b(ProfileOptInActivity profileOptInActivity) {
            xq4.a((BaseActivity) profileOptInActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) profileOptInActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) profileOptInActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) profileOptInActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) profileOptInActivity, new os4());
            sl5.a(profileOptInActivity, a());
            return profileOptInActivity;
        }

        @DexIgnore
        public t1(wl5 wl5) {
            a(wl5);
        }

        @DexIgnore
        public final void a(wl5 wl5) {
            z76.a(wl5);
            this.a = wl5;
        }

        @DexIgnore
        public void a(ProfileOptInActivity profileOptInActivity) {
            b(profileOptInActivity);
        }

        @DexIgnore
        public final ProfileOptInPresenter a(ProfileOptInPresenter profileOptInPresenter) {
            am5.a(profileOptInPresenter);
            return profileOptInPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements pm5 {
        @DexIgnore
        public void a(CustomizeActiveCaloriesChartFragment customizeActiveCaloriesChartFragment) {
            b(customizeActiveCaloriesChartFragment);
        }

        @DexIgnore
        public final CustomizeActiveCaloriesChartFragment b(CustomizeActiveCaloriesChartFragment customizeActiveCaloriesChartFragment) {
            qm5.a(customizeActiveCaloriesChartFragment, (w04) o24.this.x2.get());
            return customizeActiveCaloriesChartFragment;
        }

        @DexIgnore
        public u(rm5 rm5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u0 implements rw4 {
        @DexIgnore
        public yw4 a;

        @DexIgnore
        public final jt4 a() {
            return new jt4(d());
        }

        @DexIgnore
        public final ws4 b() {
            return new ws4((UserRepository) o24.this.A.get(), (DeviceRepository) o24.this.X.get(), (an4) o24.this.c.get());
        }

        @DexIgnore
        public final HomePresenter c() {
            HomePresenter a2 = bx4.a(zw4.a(this.a), (an4) o24.this.c.get(), (DeviceRepository) o24.this.X.get(), (PortfolioApp) o24.this.d.get(), o24.this.W(), (z24) o24.this.Z.get(), o24.this.l(), a(), (ey5) o24.this.D2.get(), d(), b(), (dp5) o24.this.L1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository d() {
            return new ServerSettingRepository((ServerSettingDataSource) o24.this.B2.get(), (ServerSettingDataSource) o24.this.C2.get());
        }

        @DexIgnore
        public u0(yw4 yw4) {
            a(yw4);
        }

        @DexIgnore
        public final void a(yw4 yw4) {
            z76.a(yw4);
            this.a = yw4;
        }

        @DexIgnore
        public void a(HomeActivity homeActivity) {
            b(homeActivity);
        }

        @DexIgnore
        public final HomePresenter a(HomePresenter homePresenter) {
            cx4.a(homePresenter);
            return homePresenter;
        }

        @DexIgnore
        public final HomeActivity b(HomeActivity homeActivity) {
            xq4.a((BaseActivity) homeActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) homeActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) homeActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) homeActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) homeActivity, new os4());
            qw4.a(homeActivity, c());
            return homeActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u1 implements gr5 {
        @DexIgnore
        public jr5 a;

        @DexIgnore
        public final GetRecommendedGoalUseCase a() {
            return new GetRecommendedGoalUseCase((SummariesRepository) o24.this.L.get(), (SleepSummariesRepository) o24.this.O.get(), (SleepSessionsRepository) o24.this.l0.get());
        }

        @DexIgnore
        public final ProfileSetupPresenter b() {
            ProfileSetupPresenter a2 = mr5.a(kr5.a(this.a), a(), (UserRepository) o24.this.A.get(), c());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository c() {
            return new ServerSettingRepository((ServerSettingDataSource) o24.this.B2.get(), (ServerSettingDataSource) o24.this.C2.get());
        }

        @DexIgnore
        public u1(jr5 jr5) {
            a(jr5);
        }

        @DexIgnore
        public final void a(jr5 jr5) {
            z76.a(jr5);
            this.a = jr5;
        }

        @DexIgnore
        public void a(ProfileSetupActivity profileSetupActivity) {
            b(profileSetupActivity);
        }

        @DexIgnore
        public final ProfileSetupPresenter a(ProfileSetupPresenter profileSetupPresenter) {
            nr5.a(profileSetupPresenter, o24.this.S());
            nr5.a(profileSetupPresenter, o24.this.T());
            nr5.a(profileSetupPresenter, o24.this.B());
            nr5.a(profileSetupPresenter, (AnalyticsHelper) o24.this.O0.get());
            nr5.a(profileSetupPresenter);
            return profileSetupPresenter;
        }

        @DexIgnore
        public final ProfileSetupActivity b(ProfileSetupActivity profileSetupActivity) {
            xq4.a((BaseActivity) profileSetupActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) profileSetupActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) profileSetupActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) profileSetupActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) profileSetupActivity, new os4());
            fr5.a(profileSetupActivity, b());
            return profileSetupActivity;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v implements um5 {
        @DexIgnore
        public void a(CustomizeActiveMinutesChartFragment customizeActiveMinutesChartFragment) {
            b(customizeActiveMinutesChartFragment);
        }

        @DexIgnore
        public final CustomizeActiveMinutesChartFragment b(CustomizeActiveMinutesChartFragment customizeActiveMinutesChartFragment) {
            vm5.a(customizeActiveMinutesChartFragment, (w04) o24.this.x2.get());
            return customizeActiveMinutesChartFragment;
        }

        @DexIgnore
        public v(wm5 wm5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v0 implements dx4 {
        @DexIgnore
        public ex4 a;

        @DexIgnore
        public final om4 a() {
            return new om4((PortfolioApp) o24.this.d.get());
        }

        @DexIgnore
        public final HomeAlertsHybridPresenter b() {
            HomeAlertsHybridPresenter a2 = i15.a(fx4.a(this.a), (AlarmHelper) o24.this.G.get(), j(), (AlarmsRepository) o24.this.F.get(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeAlertsPresenter c() {
            HomeAlertsPresenter a2 = tx4.a(gx4.a(this.a), (z24) o24.this.Z.get(), (AlarmHelper) o24.this.G.get(), new d15(), new mz4(), i(), (NotificationSettingsDatabase) o24.this.j.get(), j(), (AlarmsRepository) o24.this.F.get(), (an4) o24.this.c.get(), (DNDSettingsDatabase) o24.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDashboardPresenter d() {
            HomeDashboardPresenter a2 = xa5.a(hx4.a(this.a), (PortfolioApp) o24.this.d.get(), (DeviceRepository) o24.this.X.get(), o24.this.l(), (SummariesRepository) o24.this.L.get(), (GoalTrackingRepository) o24.this.o0.get(), (SleepSummariesRepository) o24.this.O.get(), o24.this.R(), (QuickResponseRepository) o24.this.i.get(), (HeartRateSampleRepository) o24.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter e() {
            HomeDianaCustomizePresenter a2 = m45.a(ix4.a(this.a), o24.this.a0(), o24.this.f(), o24.this.P(), (DianaPresetRepository) o24.this.t.get(), k(), a(), (CustomizeRealDataRepository) o24.this.w.get(), (UserRepository) o24.this.A.get(), o24.this.c0(), (an4) o24.this.c.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter f() {
            HomeHybridCustomizePresenter a2 = z85.a((PortfolioApp) o24.this.d.get(), jx4.a(this.a), (MicroAppRepository) o24.this.w0.get(), (HybridPresetRepository) o24.this.d0.get(), l(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeProfilePresenter g() {
            HomeProfilePresenter a2 = qj5.a(kx4.a(this.a), (PortfolioApp) o24.this.d.get(), o24.this.B(), o24.this.X(), (DeviceRepository) o24.this.X.get(), (UserRepository) o24.this.A.get(), (SummariesRepository) o24.this.L.get(), (SleepSummariesRepository) o24.this.O.get(), o24.this.k());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final jj5 h() {
            jj5 a2 = kj5.a(lx4.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final gy4 i() {
            return new gy4((NotificationsRepository) o24.this.S.get());
        }

        @DexIgnore
        public final SetAlarms j() {
            return new SetAlarms((PortfolioApp) o24.this.d.get(), (AlarmsRepository) o24.this.F.get());
        }

        @DexIgnore
        public final SetDianaPresetToWatchUseCase k() {
            return new SetDianaPresetToWatchUseCase((DianaPresetRepository) o24.this.t.get(), o24.this.e(), o24.this.Z(), o24.this.c0());
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase l() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) o24.this.X.get(), (HybridPresetRepository) o24.this.d0.get(), (MicroAppRepository) o24.this.w0.get(), o24.this.M());
        }

        @DexIgnore
        public v0(ex4 ex4) {
            a(ex4);
        }

        @DexIgnore
        public final void a(ex4 ex4) {
            z76.a(ex4);
            this.a = ex4;
        }

        @DexIgnore
        public void a(HomeFragment homeFragment) {
            b(homeFragment);
        }

        @DexIgnore
        public final HomeDashboardPresenter a(HomeDashboardPresenter homeDashboardPresenter) {
            ya5.a(homeDashboardPresenter);
            return homeDashboardPresenter;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter a(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            n45.a(homeDianaCustomizePresenter);
            return homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter a(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            a95.a(homeHybridCustomizePresenter);
            return homeHybridCustomizePresenter;
        }

        @DexIgnore
        public final HomeProfilePresenter a(HomeProfilePresenter homeProfilePresenter) {
            rj5.a(homeProfilePresenter);
            return homeProfilePresenter;
        }

        @DexIgnore
        public final HomeAlertsPresenter a(HomeAlertsPresenter homeAlertsPresenter) {
            ux4.a(homeAlertsPresenter);
            return homeAlertsPresenter;
        }

        @DexIgnore
        public final HomeFragment b(HomeFragment homeFragment) {
            xw4.a(homeFragment, d());
            xw4.a(homeFragment, e());
            xw4.a(homeFragment, f());
            xw4.a(homeFragment, g());
            xw4.a(homeFragment, c());
            xw4.a(homeFragment, b());
            xw4.a(homeFragment, h());
            return homeFragment;
        }

        @DexIgnore
        public final HomeAlertsHybridPresenter a(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            j15.a(homeAlertsHybridPresenter);
            return homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final jj5 a(jj5 jj5) {
            lj5.a(jj5);
            return jj5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v1 implements rz4 {
        @DexIgnore
        public void a(QuickResponseFragment quickResponseFragment) {
            b(quickResponseFragment);
        }

        @DexIgnore
        public final QuickResponseFragment b(QuickResponseFragment quickResponseFragment) {
            wz4.a(quickResponseFragment, (w04) o24.this.x2.get());
            return quickResponseFragment;
        }

        @DexIgnore
        public v1(xz4 xz4) {
        }

        @DexIgnore
        public void a(QuickResponseEditDialogFragment quickResponseEditDialogFragment) {
            b(quickResponseEditDialogFragment);
        }

        @DexIgnore
        public final QuickResponseEditDialogFragment b(QuickResponseEditDialogFragment quickResponseEditDialogFragment) {
            sz4.a(quickResponseEditDialogFragment, (w04) o24.this.x2.get());
            return quickResponseEditDialogFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w implements zm5 {
        @DexIgnore
        public void a(CustomizeActivityChartFragment customizeActivityChartFragment) {
            b(customizeActivityChartFragment);
        }

        @DexIgnore
        public final CustomizeActivityChartFragment b(CustomizeActivityChartFragment customizeActivityChartFragment) {
            an5.a(customizeActivityChartFragment, (w04) o24.this.x2.get());
            return customizeActivityChartFragment;
        }

        @DexIgnore
        public w(bn5 bn5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w0 implements i95 {
        @DexIgnore
        public m95 a;

        @DexIgnore
        public final o95 a() {
            o95 a2 = p95.a(n95.a(this.a), b(), (an4) o24.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase b() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) o24.this.X.get(), (HybridPresetRepository) o24.this.d0.get(), (MicroAppRepository) o24.this.w0.get(), o24.this.M());
        }

        @DexIgnore
        public w0(m95 m95) {
            a(m95);
        }

        @DexIgnore
        public final HybridCustomizeEditActivity b(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            xq4.a((BaseActivity) hybridCustomizeEditActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) hybridCustomizeEditActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) hybridCustomizeEditActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) hybridCustomizeEditActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) hybridCustomizeEditActivity, new os4());
            h95.a(hybridCustomizeEditActivity, a());
            h95.a(hybridCustomizeEditActivity, (w04) o24.this.x2.get());
            return hybridCustomizeEditActivity;
        }

        @DexIgnore
        public final void a(m95 m95) {
            z76.a(m95);
            this.a = m95;
        }

        @DexIgnore
        public void a(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            b(hybridCustomizeEditActivity);
        }

        @DexIgnore
        public final o95 a(o95 o95) {
            q95.a(o95);
            return o95;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w1 implements u05 {
        @DexIgnore
        public v05 a;

        @DexIgnore
        public final InactivityNudgeTimePresenter a() {
            InactivityNudgeTimePresenter a2 = d05.a(w05.a(this.a), (RemindersSettingsDatabase) o24.this.G0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final RemindTimePresenter b() {
            RemindTimePresenter a2 = s05.a(x05.a(this.a), (RemindersSettingsDatabase) o24.this.G0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public w1(v05 v05) {
            a(v05);
        }

        @DexIgnore
        public final void a(v05 v05) {
            z76.a(v05);
            this.a = v05;
        }

        @DexIgnore
        public final NotificationWatchRemindersFragment b(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            j05.a(notificationWatchRemindersFragment, a());
            j05.a(notificationWatchRemindersFragment, b());
            return notificationWatchRemindersFragment;
        }

        @DexIgnore
        public void a(NotificationWatchRemindersFragment notificationWatchRemindersFragment) {
            b(notificationWatchRemindersFragment);
        }

        @DexIgnore
        public final InactivityNudgeTimePresenter a(InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
            e05.a(inactivityNudgeTimePresenter);
            return inactivityNudgeTimePresenter;
        }

        @DexIgnore
        public final RemindTimePresenter a(RemindTimePresenter remindTimePresenter) {
            t05.a(remindTimePresenter);
            return remindTimePresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x implements en5 {
        @DexIgnore
        public void a(CustomizeBackgroundFragment customizeBackgroundFragment) {
            b(customizeBackgroundFragment);
        }

        @DexIgnore
        public final CustomizeBackgroundFragment b(CustomizeBackgroundFragment customizeBackgroundFragment) {
            fn5.a(customizeBackgroundFragment, (w04) o24.this.x2.get());
            return customizeBackgroundFragment;
        }

        @DexIgnore
        public x(gn5 gn5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x0 implements b95 {
        @DexIgnore
        public c95 a;

        @DexIgnore
        public final MicroAppPresenter a() {
            MicroAppPresenter a2 = w95.a(d95.a(this.a), o24.this.b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HybridCustomizeEditFragment b(HybridCustomizeEditFragment hybridCustomizeEditFragment) {
            l95.a(hybridCustomizeEditFragment, a());
            l95.a(hybridCustomizeEditFragment, (w04) o24.this.x2.get());
            return hybridCustomizeEditFragment;
        }

        @DexIgnore
        public x0(c95 c95) {
            a(c95);
        }

        @DexIgnore
        public final void a(c95 c95) {
            z76.a(c95);
            this.a = c95;
        }

        @DexIgnore
        public void a(HybridCustomizeEditFragment hybridCustomizeEditFragment) {
            b(hybridCustomizeEditFragment);
        }

        @DexIgnore
        public final MicroAppPresenter a(MicroAppPresenter microAppPresenter) {
            x95.a(microAppPresenter);
            return microAppPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x1 implements ck5 {
        @DexIgnore
        public fk5 a;

        @DexIgnore
        public final hk5 a() {
            hk5 a2 = ik5.a(gk5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ReplaceBatteryActivity b(ReplaceBatteryActivity replaceBatteryActivity) {
            xq4.a((BaseActivity) replaceBatteryActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) replaceBatteryActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) replaceBatteryActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) replaceBatteryActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) replaceBatteryActivity, new os4());
            bk5.a(replaceBatteryActivity, a());
            return replaceBatteryActivity;
        }

        @DexIgnore
        public x1(fk5 fk5) {
            a(fk5);
        }

        @DexIgnore
        public final void a(fk5 fk5) {
            z76.a(fk5);
            this.a = fk5;
        }

        @DexIgnore
        public void a(ReplaceBatteryActivity replaceBatteryActivity) {
            b(replaceBatteryActivity);
        }

        @DexIgnore
        public final hk5 a(hk5 hk5) {
            jk5.a(hk5);
            return hk5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y implements jn5 {
        @DexIgnore
        public void a(CustomizeButtonFragment customizeButtonFragment) {
            b(customizeButtonFragment);
        }

        @DexIgnore
        public final CustomizeButtonFragment b(CustomizeButtonFragment customizeButtonFragment) {
            kn5.a(customizeButtonFragment, (w04) o24.this.x2.get());
            return customizeButtonFragment;
        }

        @DexIgnore
        public y(ln5 ln5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y0 implements hp5 {
        @DexIgnore
        public kp5 a;

        @DexIgnore
        public final LoginPresenter a() {
            LoginPresenter a2 = op5.a(mp5.a(this.a), lp5.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final LoginActivity b(LoginActivity loginActivity) {
            xq4.a((BaseActivity) loginActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) loginActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) loginActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) loginActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) loginActivity, new os4());
            gp5.a(loginActivity, (hn4) o24.this.y1.get());
            gp5.a(loginActivity, (in4) o24.this.p0.get());
            gp5.a(loginActivity, (kn4) o24.this.z1.get());
            gp5.a(loginActivity, (MFLoginWechatManager) o24.this.A1.get());
            gp5.a(loginActivity, a());
            return loginActivity;
        }

        @DexIgnore
        public y0(kp5 kp5) {
            a(kp5);
        }

        @DexIgnore
        public final void a(kp5 kp5) {
            z76.a(kp5);
            this.a = kp5;
        }

        @DexIgnore
        public void a(LoginActivity loginActivity) {
            b(loginActivity);
        }

        @DexIgnore
        public final LoginPresenter a(LoginPresenter loginPresenter) {
            pp5.a(loginPresenter, o24.this.F());
            pp5.a(loginPresenter, o24.this.I());
            pp5.a(loginPresenter, o24.this.n());
            pp5.a(loginPresenter, new rr4());
            pp5.a(loginPresenter, o24.this.l());
            pp5.a(loginPresenter, (UserRepository) o24.this.A.get());
            pp5.a(loginPresenter, (DeviceRepository) o24.this.X.get());
            pp5.a(loginPresenter, (an4) o24.this.c.get());
            pp5.a(loginPresenter, o24.this.p());
            pp5.a(loginPresenter, o24.this.w());
            pp5.a(loginPresenter, (z24) o24.this.Z.get());
            pp5.a(loginPresenter, o24.this.u());
            pp5.a(loginPresenter, o24.this.v());
            pp5.a(loginPresenter, o24.this.t());
            pp5.a(loginPresenter, o24.this.r());
            pp5.a(loginPresenter, o24.this.G());
            pp5.a(loginPresenter, (kn4) o24.this.z1.get());
            pp5.a(loginPresenter, o24.this.H());
            pp5.a(loginPresenter, o24.this.K());
            pp5.a(loginPresenter, o24.this.J());
            pp5.a(loginPresenter, o24.this.d());
            pp5.a(loginPresenter, (AnalyticsHelper) o24.this.O0.get());
            pp5.a(loginPresenter, (SummariesRepository) o24.this.L.get());
            pp5.a(loginPresenter, (SleepSummariesRepository) o24.this.O.get());
            pp5.a(loginPresenter, (GoalTrackingRepository) o24.this.o0.get());
            pp5.a(loginPresenter, o24.this.q());
            pp5.a(loginPresenter, o24.this.s());
            pp5.a(loginPresenter, o24.this.A());
            pp5.a(loginPresenter, o24.this.d0());
            pp5.a(loginPresenter, (AlarmsRepository) o24.this.F.get());
            pp5.a(loginPresenter);
            return loginPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y1 implements z95 {
        @DexIgnore
        public ca5 a;

        @DexIgnore
        public final SearchMicroAppPresenter a() {
            SearchMicroAppPresenter a2 = fa5.a(da5.a(this.a), (MicroAppRepository) o24.this.w0.get(), (an4) o24.this.c.get(), (PortfolioApp) o24.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchMicroAppActivity b(SearchMicroAppActivity searchMicroAppActivity) {
            xq4.a((BaseActivity) searchMicroAppActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) searchMicroAppActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) searchMicroAppActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) searchMicroAppActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) searchMicroAppActivity, new os4());
            y95.a(searchMicroAppActivity, a());
            return searchMicroAppActivity;
        }

        @DexIgnore
        public y1(ca5 ca5) {
            a(ca5);
        }

        @DexIgnore
        public final void a(ca5 ca5) {
            z76.a(ca5);
            this.a = ca5;
        }

        @DexIgnore
        public void a(SearchMicroAppActivity searchMicroAppActivity) {
            b(searchMicroAppActivity);
        }

        @DexIgnore
        public final SearchMicroAppPresenter a(SearchMicroAppPresenter searchMicroAppPresenter) {
            ga5.a(searchMicroAppPresenter);
            return searchMicroAppPresenter;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z implements on5 {
        @DexIgnore
        public void a(CustomizeGoalTrackingChartFragment customizeGoalTrackingChartFragment) {
            b(customizeGoalTrackingChartFragment);
        }

        @DexIgnore
        public final CustomizeGoalTrackingChartFragment b(CustomizeGoalTrackingChartFragment customizeGoalTrackingChartFragment) {
            pn5.a(customizeGoalTrackingChartFragment, (w04) o24.this.x2.get());
            return customizeGoalTrackingChartFragment;
        }

        @DexIgnore
        public z(qn5 qn5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z0 implements qp5 {
        @DexIgnore
        public void a(MapPickerFragment mapPickerFragment) {
            b(mapPickerFragment);
        }

        @DexIgnore
        public final MapPickerFragment b(MapPickerFragment mapPickerFragment) {
            rp5.a(mapPickerFragment, (w04) o24.this.x2.get());
            return mapPickerFragment;
        }

        @DexIgnore
        public z0(sp5 sp5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z1 implements p55 {
        @DexIgnore
        public s55 a;

        @DexIgnore
        public final SearchRingPhonePresenter a() {
            SearchRingPhonePresenter a2 = v55.a(t55.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchRingPhoneActivity b(SearchRingPhoneActivity searchRingPhoneActivity) {
            xq4.a((BaseActivity) searchRingPhoneActivity, (UserRepository) o24.this.A.get());
            xq4.a((BaseActivity) searchRingPhoneActivity, (an4) o24.this.c.get());
            xq4.a((BaseActivity) searchRingPhoneActivity, (DeviceRepository) o24.this.X.get());
            xq4.a((BaseActivity) searchRingPhoneActivity, (t24) o24.this.C1.get());
            xq4.a((BaseActivity) searchRingPhoneActivity, new os4());
            o55.a(searchRingPhoneActivity, a());
            return searchRingPhoneActivity;
        }

        @DexIgnore
        public z1(s55 s55) {
            a(s55);
        }

        @DexIgnore
        public final void a(s55 s55) {
            z76.a(s55);
            this.a = s55;
        }

        @DexIgnore
        public void a(SearchRingPhoneActivity searchRingPhoneActivity) {
            b(searchRingPhoneActivity);
        }

        @DexIgnore
        public final SearchRingPhonePresenter a(SearchRingPhonePresenter searchRingPhonePresenter) {
            w55.a(searchRingPhonePresenter);
            return searchRingPhonePresenter;
        }
    }

    @DexIgnore
    public void a(LocaleChangedReceiver localeChangedReceiver) {
    }

    @DexIgnore
    public o24(i iVar) {
        a(iVar);
        b(iVar);
    }

    @DexIgnore
    public static i e0() {
        return new i();
    }

    @DexIgnore
    public final GetSecretKeyUseCase A() {
        return new GetSecretKeyUseCase(o(), this.X.get());
    }

    @DexIgnore
    public final dt4 B() {
        return new dt4(this.A.get());
    }

    @DexIgnore
    public final GetWeather C() {
        return new GetWeather(this.r.get());
    }

    @DexIgnore
    public final kk4 D() {
        return p14.a(this.a, this.b.get(), this.Q.get(), this.c.get());
    }

    @DexIgnore
    public final HybridSyncUseCase E() {
        return new HybridSyncUseCase(this.w0.get(), this.c.get(), this.X.get(), this.d.get(), this.d0.get(), this.S.get(), this.O0.get(), this.F.get());
    }

    @DexIgnore
    public final LoginEmailUseCase F() {
        return new LoginEmailUseCase(this.A.get());
    }

    @DexIgnore
    public final lt4 G() {
        return new lt4(this.y1.get());
    }

    @DexIgnore
    public final mt4 H() {
        return new mt4(this.p0.get());
    }

    @DexIgnore
    public final LoginSocialUseCase I() {
        return new LoginSocialUseCase(this.A.get());
    }

    @DexIgnore
    public final ot4 J() {
        return new ot4(this.A1.get());
    }

    @DexIgnore
    public final pt4 K() {
        return new pt4(this.z1.get());
    }

    @DexIgnore
    public final Map<Class<? extends ListenableWorker>, Provider<e06<? extends ListenableWorker>>> L() {
        return bm3.of(PushPendingDataWorker.class, this.q1);
    }

    @DexIgnore
    public final MicroAppLastSettingRepository M() {
        return new MicroAppLastSettingRepository(this.x0.get());
    }

    @DexIgnore
    public final RequestEmailOtp N() {
        return new RequestEmailOtp(this.A.get());
    }

    @DexIgnore
    public final RingStyleRemoteDataSource O() {
        return new RingStyleRemoteDataSource(this.r.get());
    }

    @DexIgnore
    public final RingStyleRepository P() {
        return new RingStyleRepository(this.I0.get(), O(), this.M0.get());
    }

    @DexIgnore
    public final SetNotificationUseCase Q() {
        return new SetNotificationUseCase(new d15(), new mz4(), this.j.get(), this.S.get(), this.X.get(), this.c.get());
    }

    @DexIgnore
    public final SetReplyMessageMappingUseCase R() {
        return new SetReplyMessageMappingUseCase(this.i.get());
    }

    @DexIgnore
    public final SignUpEmailUseCase S() {
        return new SignUpEmailUseCase(this.A.get(), this.c.get());
    }

    @DexIgnore
    public final SignUpSocialUseCase T() {
        return new SignUpSocialUseCase(this.A.get(), this.c.get());
    }

    @DexIgnore
    public final ts4 U() {
        return us4.a(this.S.get());
    }

    @DexIgnore
    public final fs4 V() {
        return new fs4(this.X.get(), this.c.get());
    }

    @DexIgnore
    public final UpdateFirmwareUsecase W() {
        return new UpdateFirmwareUsecase(this.X.get(), this.c.get());
    }

    @DexIgnore
    public final UpdateUser X() {
        return new UpdateUser(this.A.get());
    }

    @DexIgnore
    public final VerifySecretKeyUseCase Y() {
        return new VerifySecretKeyUseCase(this.X.get(), this.A.get(), j(), this.d.get());
    }

    @DexIgnore
    public final WatchAppLastSettingRepository Z() {
        return new WatchAppLastSettingRepository(this.z0.get());
    }

    @DexIgnore
    public final CategoryRemoteDataSource a() {
        return new CategoryRemoteDataSource(this.r.get());
    }

    @DexIgnore
    public final WatchAppRepository a0() {
        return new WatchAppRepository(this.q0.get(), this.r0.get(), this.d.get());
    }

    @DexIgnore
    public final CategoryRepository b() {
        return new CategoryRepository(this.Q0.get(), a());
    }

    @DexIgnore
    public final WatchFaceRemoteDataSource b0() {
        return new WatchFaceRemoteDataSource(this.r.get());
    }

    @DexIgnore
    public final CheckAuthenticationEmailExisting c() {
        return new CheckAuthenticationEmailExisting(this.A.get());
    }

    @DexIgnore
    public final WatchFaceRepository c0() {
        return new WatchFaceRepository(this.b.get(), this.N0.get(), b0(), this.M0.get());
    }

    @DexIgnore
    public final CheckAuthenticationSocialExisting d() {
        return new CheckAuthenticationSocialExisting(this.A.get());
    }

    @DexIgnore
    public final WatchLocalizationRepository d0() {
        return new WatchLocalizationRepository(this.r.get(), this.c.get());
    }

    @DexIgnore
    public final ComplicationLastSettingRepository e() {
        return new ComplicationLastSettingRepository(this.y0.get());
    }

    @DexIgnore
    public final ComplicationRepository f() {
        return new ComplicationRepository(this.s0.get(), this.t0.get(), this.d.get());
    }

    @DexIgnore
    public final ex5 g() {
        ex5 a3 = fx5.a(this.d.get(), this.S.get(), this.X.get(), this.j.get(), this.Q.get(), this.Y.get(), U(), this.c.get());
        a(a3);
        return a3;
    }

    @DexIgnore
    public final iw5 h() {
        return new iw5(this.t.get(), this.w.get());
    }

    @DexIgnore
    public final d06 i() {
        return new d06(L());
    }

    @DexIgnore
    public final kw5 j() {
        return new kw5(this.c.get());
    }

    @DexIgnore
    public final DeleteLogoutUserUseCase k() {
        return new DeleteLogoutUserUseCase(this.A.get(), this.F.get(), this.c.get(), this.d0.get(), this.g0.get(), this.L.get(), this.k0.get(), this.S.get(), this.X.get(), this.l0.get(), this.o0.get(), this.p0.get(), D(), this.O.get(), this.t.get(), a0(), f(), this.j.get(), this.e.get(), this.w0.get(), M(), e(), Z(), x(), this.B0.get(), this.D0.get(), this.F0.get(), this.G0.get(), this.H0.get(), P(), this.M0.get(), this.i.get(), c0(), this.d.get());
    }

    @DexIgnore
    public final cj4 l() {
        return new cj4(y(), z(), E(), m());
    }

    @DexIgnore
    public final DianaSyncUseCase m() {
        return new DianaSyncUseCase(this.t.get(), this.c.get(), new d15(), this.d.get(), this.X.get(), this.j.get(), new mz4(), Y(), W(), this.O0.get(), c0(), this.F.get());
    }

    @DexIgnore
    public final DownloadUserInfoUseCase n() {
        return new DownloadUserInfoUseCase(this.A.get());
    }

    @DexIgnore
    public final mw5 o() {
        return new mw5(this.c.get());
    }

    @DexIgnore
    public final FetchActivities p() {
        return new FetchActivities(this.g0.get(), this.A.get(), x());
    }

    @DexIgnore
    public final FetchDailyGoalTrackingSummaries q() {
        return new FetchDailyGoalTrackingSummaries(this.o0.get(), this.A.get());
    }

    @DexIgnore
    public final FetchDailyHeartRateSummaries r() {
        return new FetchDailyHeartRateSummaries(this.D0.get(), this.A.get(), x());
    }

    @DexIgnore
    public final qs4 s() {
        return new qs4(this.o0.get(), this.A.get());
    }

    @DexIgnore
    public final FetchHeartRateSamples t() {
        return new FetchHeartRateSamples(this.B0.get(), x(), this.A.get());
    }

    @DexIgnore
    public final FetchSleepSessions u() {
        return new FetchSleepSessions(this.l0.get(), this.A.get(), x());
    }

    @DexIgnore
    public final FetchSleepSummaries v() {
        return new FetchSleepSummaries(this.O.get(), this.A.get(), this.l0.get(), x());
    }

    @DexIgnore
    public final FetchSummaries w() {
        return new FetchSummaries(this.L.get(), x(), this.A.get(), this.g0.get());
    }

    @DexIgnore
    public final FitnessDataRepository x() {
        return new FitnessDataRepository(this.J.get(), this.r.get());
    }

    @DexIgnore
    public final GetDianaDeviceSettingUseCase y() {
        return new GetDianaDeviceSettingUseCase(a0(), f(), this.t.get(), b(), c0(), P(), new d15(), new mz4(), this.j.get(), this.c.get(), d0(), this.F.get());
    }

    @DexIgnore
    public final GetHybridDeviceSettingUseCase z() {
        return new GetHybridDeviceSettingUseCase(this.d0.get(), this.w0.get(), this.X.get(), this.S.get(), b(), this.F.get());
    }

    @DexIgnore
    public final void a(i iVar) {
        this.b = w76.a(f14.a(iVar.a));
        this.c = w76.a(a24.a(iVar.a, this.b));
        this.d = w76.a(h14.a(iVar.a));
        this.e = w76.a(PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory.create(iVar.b, this.d));
        this.f = w76.a(PortfolioDatabaseModule_ProvideQuickResponseDatabaseFactory.create(iVar.b, this.d));
        this.g = w76.a(PortfolioDatabaseModule_ProvideQuickResponseMessageDaoFactory.create(iVar.b, this.f));
        this.h = w76.a(PortfolioDatabaseModule_ProvideQuickResponseSenderDaoFactory.create(iVar.b, this.f));
        this.i = w76.a(QuickResponseRepository_Factory.create(this.g, this.h));
        this.j = w76.a(PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory.create(iVar.b, this.d));
        this.k = w76.a(kq4.a(this.c, this.e, this.i, this.j));
        this.l = w76.a(PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory.create(iVar.b, this.d));
        this.m = w76.a(PortfolioDatabaseModule_ProvideDianaPresetDaoFactory.create(iVar.b, this.l));
        this.n = w76.a(z14.a(iVar.a));
        this.o = w76.a(i14.a(iVar.a));
        this.p = w76.a(jp4.a(this.d, this.n, this.o, this.c));
        this.q = w76.a(np4.a(this.n, this.o, this.c));
        this.r = w76.a(e14.a(iVar.a, this.p, this.q));
        this.s = w76.a(RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory.create(iVar.c, this.r));
        this.t = w76.a(DianaPresetRepository_Factory.create(this.m, this.s));
        this.u = w76.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory.create(iVar.b, this.d));
        this.v = w76.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory.create(iVar.b, this.u));
        this.w = w76.a(CustomizeRealDataRepository_Factory.create(this.v));
        this.x = w76.a(j14.a(iVar.a, this.p, this.q));
        this.y = w76.a(RepositoriesModule_ProvideUserRemoteDataSourceFactory.create(iVar.c, this.r, this.o, this.x));
        this.z = w76.a(RepositoriesModule_ProvideUserLocalDataSourceFactory.create(iVar.c));
        this.A = w76.a(UserRepository_Factory.create(this.y, this.z, this.c));
        this.B = w76.a(PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory.create(iVar.b, this.d));
        this.C = w76.a(PortfolioDatabaseModule_ProvidesAlarmDaoFactory.create(iVar.b, this.B));
        this.D = w76.a(PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory.create(iVar.b, this.C));
        this.E = w76.a(PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory.create(iVar.b, this.r));
        this.F = w76.a(AlarmsRepository_Factory.create(this.D, this.E));
        this.G = w76.a(c14.a(iVar.a, this.c, this.A, this.F));
        this.H = w76.a(PortfolioDatabaseModule_ProvideFitnessDatabaseFactory.create(iVar.b, this.d));
        this.I = w76.a(PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory.create(iVar.b, this.H));
        this.J = w76.a(PortfolioDatabaseModule_ProvideFitnessDataDaoFactory.create(iVar.b, this.H));
        this.K = w76.a(PortfolioDatabaseModule_ProvideFitnessHelperFactory.create(iVar.b, this.c, this.I));
        this.L = w76.a(SummariesRepository_Factory.create(this.r, this.I, this.J, this.H, this.K));
        this.M = w76.a(PortfolioDatabaseModule_ProvideSleepDatabaseFactory.create(iVar.b, this.d));
        this.N = w76.a(PortfolioDatabaseModule_ProvideSleepDaoFactory.create(iVar.b, this.M));
        this.O = w76.a(SleepSummariesRepository_Factory.create(this.N, this.r, this.J));
        this.P = w76.a(q14.a(iVar.a));
        this.Q = w76.a(v04.a());
        this.R = w76.a(NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory.create(iVar.d));
        this.S = w76.a(NotificationsRepository_Factory.create(this.R));
        this.T = w76.a(PortfolioDatabaseModule_ProvideDeviceDatabaseFactory.create(iVar.b, this.d));
        this.U = w76.a(PortfolioDatabaseModule_ProvideDeviceDaoFactory.create(iVar.b, this.T));
        this.V = w76.a(PortfolioDatabaseModule_ProvideSkuDaoFactory.create(iVar.b, this.T));
        this.W = DeviceRemoteDataSource_Factory.create(this.r);
        this.X = w76.a(DeviceRepository_Factory.create(this.U, this.V, this.W));
        this.Y = w76.a(k14.a(iVar.a));
        this.Z = w76.a(c24.a(iVar.a));
        this.a0 = w76.a(PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory.create(iVar.b, this.d));
        this.b0 = w76.a(PortfolioDatabaseModule_ProvidePresetDaoFactory.create(iVar.b, this.a0));
        this.c0 = HybridPresetRemoteDataSource_Factory.create(this.r);
        this.d0 = w76.a(HybridPresetRepository_Factory.create(this.b0, this.c0));
        this.e0 = w76.a(PortfolioDatabaseModule_ProvideSampleRawDaoFactory.create(iVar.b, this.H));
        this.f0 = w76.a(PortfolioDatabaseModule_ProvideActivitySampleDaoFactory.create(iVar.b, this.H));
        this.g0 = w76.a(ActivitiesRepository_Factory.create(this.r, this.e0, this.f0, this.H, this.J, this.A, this.K));
        this.h0 = w76.a(b24.a(iVar.a, this.p, this.q));
        this.i0 = w76.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory.create(iVar.e, this.h0, this.Q));
        this.j0 = w76.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory.create(iVar.e));
        this.k0 = w76.a(MicroAppSettingRepository_Factory.create(this.i0, this.j0, this.Q));
        this.l0 = w76.a(SleepSessionsRepository_Factory.create(this.N, this.r, this.M, this.J));
        this.m0 = w76.a(PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory.create(iVar.b, this.d));
        this.n0 = w76.a(PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory.create(iVar.b, this.m0));
        this.o0 = w76.a(GoalTrackingRepository_Factory.create(this.m0, this.n0, this.A, this.c, this.r));
        this.p0 = w76.a(v14.a(iVar.a));
        this.a = iVar.a;
        this.q0 = w76.a(PortfolioDatabaseModule_ProvideWatchAppDaoFactory.create(iVar.b, this.l));
        this.r0 = w76.a(RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory.create(iVar.c, this.r));
        this.s0 = w76.a(PortfolioDatabaseModule_ProvideComplicationDaoFactory.create(iVar.b, this.l));
        this.t0 = w76.a(RepositoriesModule_ProvideComplicationRemoteDataSourceFactory.create(iVar.c, this.r));
        this.u0 = w76.a(PortfolioDatabaseModule_ProvideMicroAppDaoFactory.create(iVar.b, this.a0));
        this.v0 = MicroAppRemoteDataSource_Factory.create(this.r);
        this.w0 = w76.a(MicroAppRepository_Factory.create(this.u0, this.v0, this.d));
        this.x0 = w76.a(PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory.create(iVar.b, this.a0));
        this.y0 = w76.a(PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory.create(iVar.b, this.l));
        this.z0 = w76.a(PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory.create(iVar.b, this.l));
        this.A0 = w76.a(PortfolioDatabaseModule_ProvideHeartRateDaoFactory.create(iVar.b, this.H));
        this.B0 = w76.a(HeartRateSampleRepository_Factory.create(this.A0, this.J, this.r));
        this.C0 = w76.a(PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory.create(iVar.b, this.H));
        this.D0 = w76.a(HeartRateSummaryRepository_Factory.create(this.C0, this.J, this.r));
        this.E0 = w76.a(PortfolioDatabaseModule_ProvideWorkoutDaoFactory.create(iVar.b, this.H));
        this.F0 = w76.a(WorkoutSessionRepository_Factory.create(this.E0, this.J, this.r));
        this.G0 = w76.a(PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory.create(iVar.b, this.d));
        this.H0 = w76.a(PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory.create(iVar.b, this.d));
        this.I0 = w76.a(PortfolioDatabaseModule_ProvidesRingStyleDaoFactory.create(iVar.b, this.l));
        this.J0 = w76.a(PortfolioDatabaseModule_ProvideFileDatabaseFactory.create(iVar.b, this.d));
        this.K0 = w76.a(PortfolioDatabaseModule_ProvideFileDaoFactory.create(iVar.b, this.J0));
        this.L0 = w76.a(m14.a(iVar.a));
        this.M0 = w76.a(FileRepository_Factory.create(this.K0, this.L0, this.d));
        this.N0 = w76.a(PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory.create(iVar.b, this.l));
        this.O0 = w76.a(d14.a(iVar.a));
        this.P0 = w76.a(PortfolioDatabaseModule_ProvideCategoryDatabaseFactory.create(iVar.b, this.d));
        this.Q0 = w76.a(PortfolioDatabaseModule_ProvideCategoryDaoFactory.create(iVar.b, this.P0));
        this.R0 = CategoryRemoteDataSource_Factory.create(this.r);
        this.S0 = CategoryRepository_Factory.create(this.Q0, this.R0);
        this.T0 = WatchAppRepository_Factory.create(this.q0, this.r0, this.d);
        this.U0 = ComplicationRepository_Factory.create(this.s0, this.t0, this.d);
        this.V0 = WatchFaceRemoteDataSource_Factory.create(this.r);
    }

    @DexIgnore
    public final void b(i iVar) {
        this.W0 = WatchFaceRepository_Factory.create(this.b, this.N0, this.V0, this.M0);
        this.X0 = RingStyleRemoteDataSource_Factory.create(this.r);
        this.Y0 = RingStyleRepository_Factory.create(this.I0, this.X0, this.M0);
        this.Z0 = WatchLocalizationRepository_Factory.create(this.r, this.c);
        this.a1 = at5.a(this.T0, this.U0, this.t, this.S0, this.W0, this.Y0, e15.a(), nz4.a(), this.j, this.c, this.Z0, this.F);
        this.b1 = ct5.a(this.d0, this.w0, this.X, this.S, this.S0, this.F);
        this.c1 = or4.a(this.w0, this.c, this.X, this.d, this.d0, this.S, this.O0, this.F);
        this.d1 = lw5.a(this.c);
        this.e1 = xw5.a(this.X, this.A, this.d1, this.d);
        this.f1 = hs4.a(this.X, this.c);
        this.g1 = lr4.a(this.t, this.c, e15.a(), this.d, this.X, this.j, nz4.a(), this.e1, this.f1, this.O0, this.W0, this.F);
        this.h1 = dj4.a(this.a1, this.b1, this.c1, this.g1);
        this.i1 = w76.a(PortfolioDatabaseModule_ProvideThemeDatabaseFactory.create(iVar.b, this.d));
        this.j1 = w76.a(PortfolioDatabaseModule_ProvideThemeDaoFactory.create(iVar.b, this.i1));
        this.k1 = w76.a(ThemeRepository_Factory.create(this.j1, this.d));
        this.l1 = w76.a(g14.a(iVar.a, this.d, this.c, this.d0, (Provider<CategoryRepository>) this.S0, (Provider<WatchAppRepository>) this.T0, (Provider<ComplicationRepository>) this.U0, this.w0, this.t, this.X, this.A, this.F, (Provider<cj4>) this.h1, (Provider<WatchFaceRepository>) this.W0, (Provider<WatchLocalizationRepository>) this.Z0, this.M0, this.k1, (Provider<RingStyleRepository>) this.Y0));
        this.m1 = w76.a(vp4.a(this.A));
        this.n1 = FitnessDataRepository_Factory.create(this.J, this.r);
        this.o1 = p14.a(iVar.a, this.b, this.Q, this.c);
        this.p1 = w76.a(ThirdPartyRepository_Factory.create(this.o1, this.H0, this.g0, this.d));
        this.q1 = g06.a(this.g0, this.L, this.l0, this.O, this.o0, this.B0, this.D0, this.n1, this.F, this.c, this.t, this.d0, this.p1, this.M0, this.d);
        this.r1 = w76.a(nn4.a(this.t, this.c, this.W0));
        this.s1 = w76.a(to4.a(this.k, this.c));
        this.t1 = w76.a(wo4.a(this.k, this.c));
        this.u1 = uw5.a(e15.a(), nz4.a(), this.j, this.S, this.X, this.c);
        this.v1 = w76.a(t14.a(iVar.a, this.d0, this.i, this.F, this.c, (Provider<tw5>) this.u1));
        this.w1 = w76.a(f24.a(iVar.a, this.X, this.d));
        this.x1 = w76.a(mq4.a());
        this.y1 = w76.a(l14.a(iVar.a));
        this.z1 = w76.a(h24.a(iVar.a));
        this.A1 = w76.a(g24.a(iVar.a));
        w76.a(UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory.create(iVar.f));
        this.B1 = MicroAppLastSettingRepository_Factory.create(this.x0);
        this.C1 = w76.a(y14.a(iVar.a, this.c, this.A, (Provider<bt5>) this.b1, this.S, this.d, this.o0, this.m0, this.U, this.a0, (Provider<MicroAppLastSettingRepository>) this.B1, (Provider<cj4>) this.h1, this.F));
        this.D1 = w76.a(n14.a(iVar.a));
        this.E1 = w76.a(ys4.a());
        this.F1 = w76.a(PortfolioDatabaseModule_ProvideAddressDatabaseFactory.create(iVar.b, this.d));
        this.G1 = w76.a(PortfolioDatabaseModule_ProvideAddressDaoFactory.create(iVar.b, this.F1));
        this.H1 = w76.a(LocationSource_Factory.create(this.G1));
        this.I1 = w76.a(PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory.create(iVar.b, this.d));
        this.J1 = w76.a(PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory.create(iVar.b, this.I1));
        this.K1 = w76.a(InAppNotificationRepository_Factory.create(this.J1));
        this.L1 = w76.a(r14.a(iVar.a, this.K1));
        this.M1 = w76.a(u14.a(iVar.a));
        this.N1 = w76.a(o14.a(iVar.a, this.p, this.q));
        this.O1 = w76.a(e24.a(iVar.a));
        this.P1 = w76.a(w14.a(iVar.a));
        this.Q1 = ComplicationLastSettingRepository_Factory.create(this.y0);
        this.R1 = WatchAppLastSettingRepository_Factory.create(this.z0);
        this.S1 = i45.a(this.t, this.U0, this.Q1, this.T0, this.Y0, this.R1, this.W0);
        this.T1 = f95.a(this.d0, this.B1, this.w0);
        this.U1 = gt4.a(this.A);
        this.V1 = et4.a(this.A);
        this.W1 = ok5.a(this.U1, this.V1);
        this.X1 = j75.a(this.W0, this.Y0);
        this.Y1 = xr4.a(this.X, this.c);
        this.Z1 = es4.a(this.X, this.d0, this.t, this.d, this.W0);
        this.a2 = zr4.a(this.A, this.X, this.h1, this.d, this.c);
        this.b2 = dv5.a(this.X, this.Y1, this.Z1, this.h1, this.c, this.a2, sr4.a(), this.d);
        this.c2 = b85.a(this.c, this.A);
        this.d2 = x75.a(this.c, this.A);
        this.e2 = om5.a(this.k1);
        this.f2 = ms4.a(this.H1, this.d);
        this.g2 = js4.a(this.N1);
        this.h2 = up5.a(this.d, this.f2, this.g2);
        this.i2 = vr4.a(this.i);
        this.j2 = zz4.a(this.i, this.i2);
        this.k2 = mo5.a(this.k1);
        this.l2 = nn5.a(this.k1);
        this.m2 = in5.a(this.k1);
        this.n2 = co5.a(this.k1);
        this.o2 = dn5.a(this.k1);
        this.p2 = ym5.a(this.k1);
        this.q2 = tm5.a(this.k1);
        this.r2 = sn5.a(this.k1);
        this.s2 = xn5.a(this.k1);
        this.t2 = ho5.a(this.k1);
        this.u2 = ro5.a(this.k1);
        this.v2 = uz4.a(this.i);
        y76.b a3 = y76.a(25);
        a3.a(DianaCustomizeViewModel.class, this.S1);
        a3.a(HybridCustomizeViewModel.class, this.T1);
        a3.a(ProfileEditViewModel.class, this.W1);
        a3.a(EditPhotoViewModel.class, c75.a());
        a3.a(PreviewViewModel.class, this.X1);
        a3.a(bv5.class, this.b2);
        a3.a(CommuteTimeWatchAppSettingsViewModel.class, this.c2);
        a3.a(CommuteTimeSettingsDetailViewModel.class, this.d2);
        a3.a(ja5.class, ka5.a());
        a3.a(bz4.class, cz4.a());
        a3.a(ThemesViewModel.class, this.e2);
        a3.a(tp5.class, this.h2);
        a3.a(QuickResponseViewModel.class, this.j2);
        a3.a(CustomizeTextViewModel.class, this.k2);
        a3.a(CustomizeButtonViewModel.class, this.l2);
        a3.a(CustomizeBackgroundViewModel.class, this.m2);
        a3.a(CustomizeRingChartViewModel.class, this.n2);
        a3.a(CustomizeActivityChartViewModel.class, this.o2);
        a3.a(CustomizeActiveMinutesChartViewModel.class, this.p2);
        a3.a(CustomizeActiveCaloriesChartViewModel.class, this.q2);
        a3.a(CustomizeGoalTrackingChartViewModel.class, this.r2);
        a3.a(CustomizeHeartRateChartViewModel.class, this.s2);
        a3.a(CustomizeSleepChartViewModel.class, this.t2);
        a3.a(UserCustomizeThemeViewModel.class, this.u2);
        a3.a(QuickResponseEditDialogViewModel.class, this.v2);
        this.w2 = a3.a();
        this.x2 = w76.a(x04.a(this.w2));
        this.y2 = w76.a(PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory.create(iVar.b, this.j));
        this.z2 = w76.a(s14.a(iVar.a, this.c, this.A, this.b0, this.S, this.U, this.d));
        this.A2 = w76.a(x14.a(iVar.a, this.c, this.C1, this.z2));
        this.B2 = w76.a(RepositoriesModule_ProvideServerSettingLocalDataSourceFactory.create(iVar.c));
        this.C2 = w76.a(RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory.create(iVar.c, this.r));
        this.D2 = w76.a(d24.a(iVar.a));
    }

    @DexIgnore
    public final rn4 b(rn4 rn4) {
        sn4.a(rn4, this.c.get());
        sn4.a(rn4, h());
        sn4.a(rn4, this.G.get());
        return rn4;
    }

    @DexIgnore
    public final PortfolioApp b(PortfolioApp portfolioApp) {
        v24.a(portfolioApp, this.c.get());
        v24.a(portfolioApp, this.A.get());
        v24.a(portfolioApp, this.L.get());
        v24.a(portfolioApp, this.O.get());
        v24.a(portfolioApp, this.G.get());
        v24.a(portfolioApp, this.P.get());
        v24.a(portfolioApp, this.Q.get());
        v24.a(portfolioApp, this.r.get());
        v24.a(portfolioApp, g());
        v24.a(portfolioApp, this.Z.get());
        v24.a(portfolioApp, k());
        v24.a(portfolioApp, this.O0.get());
        v24.a(portfolioApp, this.l1.get());
        v24.a(portfolioApp, this.X.get());
        v24.a(portfolioApp, this.K.get());
        v24.a(portfolioApp, this.m1.get());
        v24.a(portfolioApp, this.t.get());
        v24.a(portfolioApp, i());
        v24.a(portfolioApp, d0());
        v24.a(portfolioApp, this.r1.get());
        v24.a(portfolioApp, c0());
        v24.a(portfolioApp, this.s1.get());
        v24.a(portfolioApp, this.t1.get());
        v24.a(portfolioApp, this.k1.get());
        return portfolioApp;
    }

    @DexIgnore
    public void a(rn4 rn4) {
        b(rn4);
    }

    @DexIgnore
    public void a(PortfolioApp portfolioApp) {
        b(portfolioApp);
    }

    @DexIgnore
    public void a(DeviceHelper deviceHelper) {
        b(deviceHelper);
    }

    @DexIgnore
    public void a(el4 el4) {
        b(el4);
    }

    @DexIgnore
    public void a(MFDeviceService mFDeviceService) {
        b(mFDeviceService);
    }

    @DexIgnore
    public final DeviceHelper b(DeviceHelper deviceHelper) {
        ek4.a(deviceHelper, this.c.get());
        ek4.a(deviceHelper, this.X.get());
        return deviceHelper;
    }

    @DexIgnore
    public void a(FossilNotificationListenerService fossilNotificationListenerService) {
        b(fossilNotificationListenerService);
    }

    @DexIgnore
    public void a(AlarmReceiver alarmReceiver) {
        b(alarmReceiver);
    }

    @DexIgnore
    public void a(NetworkChangedReceiver networkChangedReceiver) {
        b(networkChangedReceiver);
    }

    @DexIgnore
    public void a(NotificationReceiver notificationReceiver) {
        b(notificationReceiver);
    }

    @DexIgnore
    public final el4 b(el4 el4) {
        fl4.a(el4, this.c.get());
        return el4;
    }

    @DexIgnore
    public void a(LoginPresenter loginPresenter) {
        b(loginPresenter);
    }

    @DexIgnore
    public void a(SignUpPresenter signUpPresenter) {
        b(signUpPresenter);
    }

    @DexIgnore
    public void a(ProfileSetupPresenter profileSetupPresenter) {
        b(profileSetupPresenter);
    }

    @DexIgnore
    public final MFDeviceService b(MFDeviceService mFDeviceService) {
        sp4.a(mFDeviceService, this.c.get());
        sp4.a(mFDeviceService, this.X.get());
        sp4.a(mFDeviceService, this.g0.get());
        sp4.a(mFDeviceService, this.L.get());
        sp4.a(mFDeviceService, this.l0.get());
        sp4.a(mFDeviceService, this.O.get());
        sp4.a(mFDeviceService, this.A.get());
        sp4.a(mFDeviceService, this.d0.get());
        sp4.a(mFDeviceService, this.w0.get());
        sp4.a(mFDeviceService, this.B0.get());
        sp4.a(mFDeviceService, this.D0.get());
        sp4.a(mFDeviceService, this.F0.get());
        sp4.a(mFDeviceService, x());
        sp4.a(mFDeviceService, this.o0.get());
        sp4.a(mFDeviceService, this.Q.get());
        sp4.a(mFDeviceService, this.O0.get());
        sp4.a(mFDeviceService, this.d.get());
        sp4.a(mFDeviceService, this.v1.get());
        sp4.a(mFDeviceService, this.p1.get());
        sp4.a(mFDeviceService, o());
        sp4.a(mFDeviceService, Y());
        sp4.a(mFDeviceService, this.K.get());
        sp4.a(mFDeviceService, this.w1.get());
        return mFDeviceService;
    }

    @DexIgnore
    public void a(BaseActivity baseActivity) {
        b(baseActivity);
    }

    @DexIgnore
    public void a(DebugActivity debugActivity) {
        b(debugActivity);
    }

    @DexIgnore
    public void a(CommuteTimeService commuteTimeService) {
        b(commuteTimeService);
    }

    @DexIgnore
    public void a(BootReceiver bootReceiver) {
        b(bootReceiver);
    }

    @DexIgnore
    public void a(URLRequestTaskHelper uRLRequestTaskHelper) {
        b(uRLRequestTaskHelper);
    }

    @DexIgnore
    public void a(ComplicationWeatherService complicationWeatherService) {
        b(complicationWeatherService);
    }

    @DexIgnore
    public void a(DeviceUtils deviceUtils) {
        b(deviceUtils);
    }

    @DexIgnore
    public void a(qw5 qw5) {
        b(qw5);
    }

    @DexIgnore
    public void a(CloudImageHelper cloudImageHelper) {
        b(cloudImageHelper);
    }

    @DexIgnore
    public void a(ey5 ey5) {
        b(ey5);
    }

    @DexIgnore
    public void a(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService) {
        b(fossilFirebaseInstanceIDService);
    }

    @DexIgnore
    public void a(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        b(fossilFirebaseMessagingService);
    }

    @DexIgnore
    public void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        b(watchAppCommuteTimeManager);
    }

    @DexIgnore
    public void a(tj4 tj4) {
        b(tj4);
    }

    @DexIgnore
    public void a(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        b(appPackageRemoveReceiver);
    }

    @DexIgnore
    public void a(LightAndHapticsManager lightAndHapticsManager) {
        b(lightAndHapticsManager);
    }

    @DexIgnore
    public void a(WeatherManager weatherManager) {
        b(weatherManager);
    }

    @DexIgnore
    public void a(ThemeManager themeManager) {
        b(themeManager);
    }

    @DexIgnore
    public void a(FileDownloadManager fileDownloadManager) {
        b(fileDownloadManager);
    }

    @DexIgnore
    public void a(TimeChangeReceiver timeChangeReceiver) {
        b(timeChangeReceiver);
    }

    @DexIgnore
    public void a(AppPackageInstallReceiver appPackageInstallReceiver) {
        b(appPackageInstallReceiver);
    }

    @DexIgnore
    public fv5 a(iv5 iv5) {
        return new j(iv5);
    }

    @DexIgnore
    public pv5 a(sv5 sv5) {
        return new m0(sv5);
    }

    @DexIgnore
    public wq5 a(zq5 zq5) {
        return new h2(zq5);
    }

    @DexIgnore
    public x35 a(b45 b45) {
        return new i0(b45);
    }

    @DexIgnore
    public h65 a(k65 k65) {
        return new q(k65);
    }

    @DexIgnore
    public m85 a(p85 p85) {
        return new j2(p85);
    }

    @DexIgnore
    public kh5 a(nh5 nh5) {
        return new e(nh5);
    }

    @DexIgnore
    public vh5 a(yh5 yh5) {
        return new k(yh5);
    }

    @DexIgnore
    public bh5 a(eh5 eh5) {
        return new c(eh5);
    }

    @DexIgnore
    public wi5 a(zi5 zi5) {
        return new c2(zi5);
    }

    @DexIgnore
    public ei5 a(hi5 hi5) {
        return new o0(hi5);
    }

    @DexIgnore
    public ni5 a(qi5 qi5) {
        return new q0(qi5);
    }

    @DexIgnore
    public yx4 a(by4 by4) {
        return new b1(by4);
    }

    @DexIgnore
    public iy4 a(my4 my4) {
        return new c1(my4);
    }

    @DexIgnore
    public dz4 a(hz4 hz4) {
        return new j1(hz4);
    }

    @DexIgnore
    public sy4 a(vy4 vy4) {
        return new e1(vy4);
    }

    @DexIgnore
    public mx4 a(qx4 qx4) {
        return new t0(qx4);
    }

    @DexIgnore
    public g05 a(k05 k05) {
        return new k1(k05);
    }

    @DexIgnore
    public u05 a(v05 v05) {
        return new w1(v05);
    }

    @DexIgnore
    public ec5 a(lc5 lc5) {
        return new f(lc5);
    }

    @DexIgnore
    public eg5 a(lg5 lg5) {
        return new d2(lg5);
    }

    @DexIgnore
    public ee5 a(le5 le5) {
        return new p0(le5);
    }

    @DexIgnore
    public ed5 a(ld5 ld5) {
        return new l(ld5);
    }

    @DexIgnore
    public eb5 a(lb5 lb5) {
        return new d(lb5);
    }

    @DexIgnore
    public ef5 a(lf5 lf5) {
        return new r0(lf5);
    }

    @DexIgnore
    public et5 a(ht5 ht5) {
        return new o1(ht5);
    }

    @DexIgnore
    public qk5 a(uk5 uk5) {
        return new s1(uk5);
    }

    @DexIgnore
    public qp5 a(sp5 sp5) {
        return new z0(sp5);
    }

    @DexIgnore
    public y75 a(z75 z75) {
        return new p(z75);
    }

    @DexIgnore
    public u75 a(v75 v75) {
        return new o(v75);
    }

    @DexIgnore
    public to5 a(vo5 vo5) {
        return new p1(vo5);
    }

    @DexIgnore
    public bl5 a(el5 el5) {
        return new s0(el5);
    }

    @DexIgnore
    public ck5 a(fk5 fk5) {
        return new x1(fk5);
    }

    @DexIgnore
    public tj5 a(wj5 wj5) {
        return new b(wj5);
    }

    @DexIgnore
    public km5 a(mm5 mm5) {
        return new f2(mm5);
    }

    @DexIgnore
    public io5 a(ko5 ko5) {
        return new d0(ko5);
    }

    @DexIgnore
    public jn5 a(ln5 ln5) {
        return new y(ln5);
    }

    @DexIgnore
    public en5 a(gn5 gn5) {
        return new x(gn5);
    }

    @DexIgnore
    public yn5 a(ao5 ao5) {
        return new b0(ao5);
    }

    @DexIgnore
    public final FossilNotificationListenerService b(FossilNotificationListenerService fossilNotificationListenerService) {
        qp4.a(fossilNotificationListenerService, this.k.get());
        qp4.a(fossilNotificationListenerService, this.x1.get());
        qp4.a(fossilNotificationListenerService, this.Q.get());
        qp4.a(fossilNotificationListenerService, this.c.get());
        return fossilNotificationListenerService;
    }

    @DexIgnore
    public zm5 a(bn5 bn5) {
        return new w(bn5);
    }

    @DexIgnore
    public um5 a(wm5 wm5) {
        return new v(wm5);
    }

    @DexIgnore
    public pm5 a(rm5 rm5) {
        return new u(rm5);
    }

    @DexIgnore
    public on5 a(qn5 qn5) {
        return new z(qn5);
    }

    @DexIgnore
    public tn5 a(vn5 vn5) {
        return new a0(vn5);
    }

    @DexIgnore
    public do5 a(fo5 fo5) {
        return new c0(fo5);
    }

    @DexIgnore
    public no5 a(po5 po5) {
        return new i2(po5);
    }

    @DexIgnore
    public kl5 a(nl5 nl5) {
        return new h0(nl5);
    }

    @DexIgnore
    public tl5 a(wl5 wl5) {
        return new t1(wl5);
    }

    @DexIgnore
    public yv5 a(cw5 cw5) {
        return new n2(cw5);
    }

    @DexIgnore
    public v15 a(y15 y15) {
        return new f1(y15);
    }

    @DexIgnore
    public l15 a(o15 o15) {
        return new d1(o15);
    }

    @DexIgnore
    public final AlarmReceiver b(AlarmReceiver alarmReceiver) {
        lo4.a(alarmReceiver, this.A.get());
        lo4.a(alarmReceiver, this.c.get());
        lo4.a(alarmReceiver, this.X.get());
        lo4.a(alarmReceiver, this.G.get());
        lo4.a(alarmReceiver, this.F.get());
        return alarmReceiver;
    }

    @DexIgnore
    public q25 a(t25 t25) {
        return new h1(t25);
    }

    @DexIgnore
    public f25 a(i25 i25) {
        return new g1(i25);
    }

    @DexIgnore
    public c35 a(f35 f35) {
        return new i1(f35);
    }

    @DexIgnore
    public ha5 a(ia5 ia5) {
        return new f0(ia5);
    }

    @DexIgnore
    public rz4 a(xz4 xz4) {
        return new v1(xz4);
    }

    @DexIgnore
    public yr5 a(bs5 bs5) {
        return new n1(bs5);
    }

    @DexIgnore
    public hs5 a(os5 os5) {
        return new m1(os5);
    }

    @DexIgnore
    public final NetworkChangedReceiver b(NetworkChangedReceiver networkChangedReceiver) {
        ro4.a(networkChangedReceiver, this.A.get());
        return networkChangedReceiver;
    }

    @DexIgnore
    public gu5 a(ju5 ju5) {
        return new e2(ju5);
    }

    @DexIgnore
    public dx4 a(ex4 ex4) {
        return new v0(ex4);
    }

    @DexIgnore
    public rw4 a(yw4 yw4) {
        return new u0(yw4);
    }

    @DexIgnore
    public final NotificationReceiver b(NotificationReceiver notificationReceiver) {
        qn4.a(notificationReceiver, this.c.get());
        qn4.a(notificationReceiver, l());
        return notificationReceiver;
    }

    @DexIgnore
    public ma5 a(na5 na5) {
        return new g0(na5);
    }

    @DexIgnore
    public p35 a(q35 q35) {
        return new r(q35);
    }

    @DexIgnore
    public o45 a(s45 s45) {
        return new s(s45);
    }

    @DexIgnore
    public k75 a(o75 o75) {
        return new k2(o75);
    }

    @DexIgnore
    public p65 a(t65 t65) {
        return new e0(t65);
    }

    @DexIgnore
    public final LoginPresenter b(LoginPresenter loginPresenter) {
        pp5.a(loginPresenter, F());
        pp5.a(loginPresenter, I());
        pp5.a(loginPresenter, n());
        pp5.a(loginPresenter, new rr4());
        pp5.a(loginPresenter, l());
        pp5.a(loginPresenter, this.A.get());
        pp5.a(loginPresenter, this.X.get());
        pp5.a(loginPresenter, this.c.get());
        pp5.a(loginPresenter, p());
        pp5.a(loginPresenter, w());
        pp5.a(loginPresenter, this.Z.get());
        pp5.a(loginPresenter, u());
        pp5.a(loginPresenter, v());
        pp5.a(loginPresenter, t());
        pp5.a(loginPresenter, r());
        pp5.a(loginPresenter, G());
        pp5.a(loginPresenter, this.z1.get());
        pp5.a(loginPresenter, H());
        pp5.a(loginPresenter, K());
        pp5.a(loginPresenter, J());
        pp5.a(loginPresenter, d());
        pp5.a(loginPresenter, this.O0.get());
        pp5.a(loginPresenter, this.L.get());
        pp5.a(loginPresenter, this.O.get());
        pp5.a(loginPresenter, this.o0.get());
        pp5.a(loginPresenter, q());
        pp5.a(loginPresenter, s());
        pp5.a(loginPresenter, A());
        pp5.a(loginPresenter, d0());
        pp5.a(loginPresenter, this.F.get());
        pp5.a(loginPresenter);
        return loginPresenter;
    }

    @DexIgnore
    public x45 a(j55 j55) {
        return new m(j55);
    }

    @DexIgnore
    public b55 a(e55 e55) {
        return new n(e55);
    }

    @DexIgnore
    public y55 a(b65 b65) {
        return new a2(b65);
    }

    @DexIgnore
    public d85 a(g85 g85) {
        return new m2(g85);
    }

    @DexIgnore
    public ov4 a(rv4 rv4) {
        return new g(rv4);
    }

    @DexIgnore
    public qu5 a(tu5 tu5) {
        return new g2(tu5);
    }

    @DexIgnore
    public ot5 a(rt5 rt5) {
        return new b2(rt5);
    }

    @DexIgnore
    public hp5 a(kp5 kp5) {
        return new y0(kp5);
    }

    @DexIgnore
    public fq5 a(iq5 iq5) {
        return new n0(iq5);
    }

    @DexIgnore
    public oq5 a(rq5 rq5) {
        return new l1(rq5);
    }

    @DexIgnore
    public gr5 a(jr5 jr5) {
        return new u1(jr5);
    }

    @DexIgnore
    public or5 a(rr5 rr5) {
        return new h(rr5);
    }

    @DexIgnore
    public wp5 a(zp5 zp5) {
        return new l0(zp5);
    }

    @DexIgnore
    public yu5 a(av5 av5) {
        return new l2(av5);
    }

    @DexIgnore
    public bw4 a(ew4 ew4) {
        return new t(ew4);
    }

    @DexIgnore
    public cm5 a(fm5 fm5) {
        return new r1(fm5);
    }

    @DexIgnore
    public i95 a(m95 m95) {
        return new w0(m95);
    }

    @DexIgnore
    public r95 a(u95 u95) {
        return new a1(u95);
    }

    @DexIgnore
    public b95 a(c95 c95) {
        return new x0(c95);
    }

    @DexIgnore
    public z95 a(ca5 ca5) {
        return new y1(ca5);
    }

    @DexIgnore
    public p55 a(s55 s55) {
        return new z1(s55);
    }

    @DexIgnore
    public du5 a(eu5 eu5) {
        return new k0(eu5);
    }

    @DexIgnore
    public y65 a(a75 a75) {
        return new j0(a75);
    }

    @DexIgnore
    public f75 a(h75 h75) {
        return new q1(h75);
    }

    @DexIgnore
    public final ex5 a(ex5 ex5) {
        gx5.a(ex5, this.X.get());
        gx5.a(ex5, this.j.get());
        return ex5;
    }

    @DexIgnore
    public final SignUpPresenter b(SignUpPresenter signUpPresenter) {
        wt5.a(signUpPresenter, G());
        wt5.a(signUpPresenter, this.z1.get());
        wt5.a(signUpPresenter, H());
        wt5.a(signUpPresenter, K());
        wt5.a(signUpPresenter, J());
        wt5.a(signUpPresenter, I());
        wt5.a(signUpPresenter, this.A.get());
        wt5.a(signUpPresenter, this.X.get());
        wt5.a(signUpPresenter, this.Z.get());
        wt5.a(signUpPresenter, u());
        wt5.a(signUpPresenter, v());
        wt5.a(signUpPresenter, p());
        wt5.a(signUpPresenter, w());
        wt5.a(signUpPresenter, t());
        wt5.a(signUpPresenter, r());
        wt5.a(signUpPresenter, this.F.get());
        wt5.a(signUpPresenter, new rr4());
        wt5.a(signUpPresenter, l());
        wt5.a(signUpPresenter, n());
        wt5.a(signUpPresenter, this.c.get());
        wt5.a(signUpPresenter, c());
        wt5.a(signUpPresenter, d());
        wt5.a(signUpPresenter, this.O0.get());
        wt5.a(signUpPresenter, B());
        wt5.a(signUpPresenter, this.L.get());
        wt5.a(signUpPresenter, this.O.get());
        wt5.a(signUpPresenter, this.o0.get());
        wt5.a(signUpPresenter, q());
        wt5.a(signUpPresenter, s());
        wt5.a(signUpPresenter, N());
        wt5.a(signUpPresenter, A());
        wt5.a(signUpPresenter, d0());
        wt5.a(signUpPresenter);
        return signUpPresenter;
    }

    @DexIgnore
    public final ProfileSetupPresenter b(ProfileSetupPresenter profileSetupPresenter) {
        nr5.a(profileSetupPresenter, S());
        nr5.a(profileSetupPresenter, T());
        nr5.a(profileSetupPresenter, B());
        nr5.a(profileSetupPresenter, this.O0.get());
        nr5.a(profileSetupPresenter);
        return profileSetupPresenter;
    }

    @DexIgnore
    public final BaseActivity b(BaseActivity baseActivity) {
        xq4.a(baseActivity, this.A.get());
        xq4.a(baseActivity, this.c.get());
        xq4.a(baseActivity, this.X.get());
        xq4.a(baseActivity, this.C1.get());
        xq4.a(baseActivity, new os4());
        return baseActivity;
    }

    @DexIgnore
    public final DebugActivity b(DebugActivity debugActivity) {
        xq4.a((BaseActivity) debugActivity, this.A.get());
        xq4.a((BaseActivity) debugActivity, this.c.get());
        xq4.a((BaseActivity) debugActivity, this.X.get());
        xq4.a((BaseActivity) debugActivity, this.C1.get());
        xq4.a((BaseActivity) debugActivity, new os4());
        br4.a(debugActivity, this.c.get());
        br4.a(debugActivity, V());
        br4.a(debugActivity, this.D1.get());
        br4.a(debugActivity, this.P.get());
        br4.a(debugActivity, this.t.get());
        br4.a(debugActivity, this.m1.get());
        br4.a(debugActivity, l());
        return debugActivity;
    }

    @DexIgnore
    public final CommuteTimeService b(CommuteTimeService commuteTimeService) {
        dq4.a(commuteTimeService, this.E1.get());
        return commuteTimeService;
    }

    @DexIgnore
    public final BootReceiver b(BootReceiver bootReceiver) {
        qo4.a(bootReceiver, this.G.get());
        return bootReceiver;
    }

    @DexIgnore
    public final URLRequestTaskHelper b(URLRequestTaskHelper uRLRequestTaskHelper) {
        URLRequestTaskHelper_MembersInjector.injectMApiService(uRLRequestTaskHelper, this.r.get());
        return uRLRequestTaskHelper;
    }

    @DexIgnore
    public final ComplicationWeatherService b(ComplicationWeatherService complicationWeatherService) {
        xp4.a(complicationWeatherService, this.H1.get());
        zp4.a(complicationWeatherService, C());
        zp4.a(complicationWeatherService, this.Z.get());
        zp4.a(complicationWeatherService, this.c.get());
        zp4.a(complicationWeatherService, this.d.get());
        zp4.a(complicationWeatherService, this.w.get());
        zp4.a(complicationWeatherService, this.A.get());
        return complicationWeatherService;
    }

    @DexIgnore
    public final DeviceUtils b(DeviceUtils deviceUtils) {
        jx5.a(deviceUtils, this.X.get());
        jx5.a(deviceUtils, this.c.get());
        jx5.a(deviceUtils, this.P.get());
        jx5.a(deviceUtils, this.D1.get());
        return deviceUtils;
    }

    @DexIgnore
    public final qw5 b(qw5 qw5) {
        rw5.a(qw5, this.g0.get());
        return qw5;
    }

    @DexIgnore
    public final CloudImageHelper b(CloudImageHelper cloudImageHelper) {
        CloudImageHelper_MembersInjector.injectMAppExecutors(cloudImageHelper, this.Q.get());
        CloudImageHelper_MembersInjector.injectMApp(cloudImageHelper, this.d.get());
        return cloudImageHelper;
    }

    @DexIgnore
    public final ey5 b(ey5 ey5) {
        fy5.a(ey5, this.c.get());
        return ey5;
    }

    @DexIgnore
    public final FossilFirebaseInstanceIDService b(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService) {
        aq4.a(fossilFirebaseInstanceIDService, this.c.get());
        return fossilFirebaseInstanceIDService;
    }

    @DexIgnore
    public final FossilFirebaseMessagingService b(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        bq4.a(fossilFirebaseMessagingService, this.L1.get());
        return fossilFirebaseMessagingService;
    }

    @DexIgnore
    public final WatchAppCommuteTimeManager b(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        rq4.a(watchAppCommuteTimeManager, this.d.get());
        rq4.a(watchAppCommuteTimeManager, this.r.get());
        rq4.a(watchAppCommuteTimeManager, this.H1.get());
        rq4.a(watchAppCommuteTimeManager, this.A.get());
        rq4.a(watchAppCommuteTimeManager, this.c.get());
        rq4.a(watchAppCommuteTimeManager, this.M1.get());
        rq4.a(watchAppCommuteTimeManager, this.E1.get());
        rq4.a(watchAppCommuteTimeManager, this.t.get());
        return watchAppCommuteTimeManager;
    }

    @DexIgnore
    public final tj4 b(tj4 tj4) {
        uj4.a(tj4, this.c.get());
        uj4.a(tj4, this.X.get());
        uj4.a(tj4, this.A.get());
        return tj4;
    }

    @DexIgnore
    public final AppPackageRemoveReceiver b(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        oo4.a(appPackageRemoveReceiver, this.S.get());
        return appPackageRemoveReceiver;
    }

    @DexIgnore
    public final LightAndHapticsManager b(LightAndHapticsManager lightAndHapticsManager) {
        um4.a(lightAndHapticsManager, this.X.get());
        um4.a(lightAndHapticsManager, this.c.get());
        return lightAndHapticsManager;
    }

    @DexIgnore
    public final WeatherManager b(WeatherManager weatherManager) {
        gn4.a(weatherManager, this.d.get());
        gn4.a(weatherManager, this.r.get());
        gn4.a(weatherManager, this.H1.get());
        gn4.a(weatherManager, this.A.get());
        gn4.a(weatherManager, this.w.get());
        gn4.a(weatherManager, this.t.get());
        gn4.a(weatherManager, this.N1.get());
        return weatherManager;
    }

    @DexIgnore
    public final ThemeManager b(ThemeManager themeManager) {
        dn4.a(themeManager, this.k1.get());
        return themeManager;
    }

    @DexIgnore
    public final FileDownloadManager b(FileDownloadManager fileDownloadManager) {
        rm4.a(fileDownloadManager, this.r.get());
        return fileDownloadManager;
    }

    @DexIgnore
    public final TimeChangeReceiver b(TimeChangeReceiver timeChangeReceiver) {
        h06.a(timeChangeReceiver, this.G.get());
        h06.a(timeChangeReceiver, this.c.get());
        return timeChangeReceiver;
    }

    @DexIgnore
    public final AppPackageInstallReceiver b(AppPackageInstallReceiver appPackageInstallReceiver) {
        no4.a(appPackageInstallReceiver, this.S.get());
        no4.a(appPackageInstallReceiver, this.c.get());
        no4.a(appPackageInstallReceiver, l());
        no4.a(appPackageInstallReceiver, new d15());
        no4.a(appPackageInstallReceiver, new mz4());
        no4.a(appPackageInstallReceiver, this.j.get());
        return appPackageInstallReceiver;
    }
}
