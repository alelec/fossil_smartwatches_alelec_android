package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bd6 extends RuntimeException {
    @DexIgnore
    public bd6() {
    }

    @DexIgnore
    public bd6(String str) {
        super(str);
    }

    @DexIgnore
    public bd6(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public bd6(Throwable th) {
        super(th);
    }
}
