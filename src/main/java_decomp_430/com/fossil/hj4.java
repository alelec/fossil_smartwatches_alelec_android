package com.fossil;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.fossil.fs;
import com.fossil.jv;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj4 implements jv<ij4, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements kv<ij4, InputStream> {
        @DexIgnore
        public hj4 a(nv nvVar) {
            wg6.b(nvVar, "multiFactory");
            return new hj4();
        }
    }

    @DexIgnore
    public boolean a(ij4 ij4) {
        wg6.b(ij4, "avatarModel");
        return true;
    }

    @DexIgnore
    public jv.a<InputStream> a(ij4 ij4, int i, int i2, xr xrVar) {
        wg6.b(ij4, "avatarModel");
        wg6.b(xrVar, "options");
        return new jv.a<>(ij4, new a(this, ij4));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements fs<InputStream> {
        @DexIgnore
        public fs<InputStream> a;
        @DexIgnore
        public volatile boolean b;
        @DexIgnore
        public /* final */ ij4 c;

        @DexIgnore
        public a(hj4 hj4, ij4 ij4) {
            wg6.b(ij4, "mAvatarModel");
            this.c = ij4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void a(br brVar, fs.a<? super InputStream> aVar) {
            wg6.b(brVar, "priority");
            wg6.b(aVar, Constants.CALLBACK);
            try {
                if (!TextUtils.isEmpty(this.c.c())) {
                    this.a = new ls(new cv(this.c.c()), 100);
                } else if (this.c.b() != null) {
                    this.a = new qs(PortfolioApp.get.instance().getContentResolver(), this.c.b());
                }
                fs<InputStream> fsVar = this.a;
                if (fsVar != null) {
                    fsVar.a(brVar, aVar);
                }
            } catch (Exception unused) {
                fs<InputStream> fsVar2 = this.a;
                if (fsVar2 != null) {
                    fsVar2.a();
                }
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String a2 = this.c.a();
            if (a2 == null) {
                a2 = "";
            }
            Bitmap b2 = cx5.b(a2);
            if (b2 != null) {
                b2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            }
            aVar.a(this.b ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }

        @DexIgnore
        public pr b() {
            return pr.REMOTE;
        }

        @DexIgnore
        public void cancel() {
            this.b = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public void a() {
            fs<InputStream> fsVar = this.a;
            if (fsVar != null) {
                fsVar.a();
            }
        }
    }
}
