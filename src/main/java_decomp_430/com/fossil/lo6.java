package com.fossil;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lo6<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e;
    @DexIgnore
    public static /* final */ /* synthetic */ AtomicLongFieldUpdater f;
    @DexIgnore
    public static /* final */ uo6 g; // = new uo6("REMOVE_FROZEN");
    @DexIgnore
    public static /* final */ a h; // = new a((qg6) null);
    @DexIgnore
    public volatile Object _next; // = null;
    @DexIgnore
    public volatile /* synthetic */ long _state$internal; // = 0;
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* synthetic */ AtomicReferenceArray b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(long j) {
            return (j & 2305843009213693952L) != 0 ? 2 : 1;
        }

        @DexIgnore
        public final long a(long j, int i) {
            return a(j, 1073741823) | (((long) i) << 0);
        }

        @DexIgnore
        public final long a(long j, long j2) {
            return j & (~j2);
        }

        @DexIgnore
        public final long b(long j, int i) {
            return a(j, 1152921503533105152L) | (((long) i) << 30);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public b(int i) {
            this.a = i;
        }
    }

    /*
    static {
        Class<lo6> cls = lo6.class;
        e = AtomicReferenceFieldUpdater.newUpdater(cls, Object.class, "_next");
        f = AtomicLongFieldUpdater.newUpdater(cls, "_state$internal");
    }
    */

    @DexIgnore
    public lo6(int i, boolean z) {
        this.c = i;
        this.d = z;
        int i2 = this.c;
        this.a = i2 - 1;
        this.b = new AtomicReferenceArray(i2);
        boolean z2 = false;
        if (this.a <= 1073741823) {
            if (!((this.c & this.a) == 0 ? true : z2)) {
                throw new IllegalStateException("Check failed.".toString());
            }
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    public final boolean c() {
        long j = this._state$internal;
        return ((int) ((1073741823 & j) >> 0)) == ((int) ((j & 1152921503533105152L) >> 30));
    }

    @DexIgnore
    public final long d() {
        long j;
        long j2;
        do {
            j = this._state$internal;
            if ((j & 1152921504606846976L) != 0) {
                return j;
            }
            j2 = j | 1152921504606846976L;
        } while (!f.compareAndSet(this, j, j2));
        return j2;
    }

    @DexIgnore
    public final lo6<E> e() {
        return b(d());
    }

    @DexIgnore
    public final Object f() {
        Object obj;
        while (true) {
            long j = this._state$internal;
            if ((1152921504606846976L & j) == 0) {
                int i = (int) ((1073741823 & j) >> 0);
                if ((this.a & ((int) ((1152921503533105152L & j) >> 30))) != (this.a & i)) {
                    obj = this.b.get(this.a & i);
                    if (obj != null) {
                        if (!(obj instanceof b)) {
                            int i2 = (i + 1) & 1073741823;
                            if (!f.compareAndSet(this, j, h.a(j, i2))) {
                                if (this.d) {
                                    lo6 lo6 = this;
                                    do {
                                        lo6 = lo6.a(i, i2);
                                    } while (lo6 != null);
                                    break;
                                }
                            } else {
                                this.b.set(this.a & i, (Object) null);
                                break;
                            }
                        } else {
                            return null;
                        }
                    } else if (this.d) {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return g;
            }
        }
        return obj;
    }

    @DexIgnore
    public final int b() {
        long j = this._state$internal;
        return 1073741823 & (((int) ((j & 1152921503533105152L) >> 30)) - ((int) ((1073741823 & j) >> 0)));
    }

    @DexIgnore
    public final lo6<E> a(int i, E e2) {
        Object obj = this.b.get(this.a & i);
        if (!(obj instanceof b) || ((b) obj).a != i) {
            return null;
        }
        this.b.set(i & this.a, e2);
        return this;
    }

    @DexIgnore
    public final lo6<E> b(long j) {
        while (true) {
            lo6<E> lo6 = (lo6) this._next;
            if (lo6 != null) {
                return lo6;
            }
            e.compareAndSet(this, (Object) null, a(j));
        }
    }

    @DexIgnore
    public final lo6<E> a(long j) {
        lo6<E> lo6 = new lo6<>(this.c * 2, this.d);
        int i = (int) ((1073741823 & j) >> 0);
        int i2 = (int) ((1152921503533105152L & j) >> 30);
        while (true) {
            int i3 = this.a;
            if ((i & i3) != (i2 & i3)) {
                Object obj = this.b.get(i3 & i);
                if (obj == null) {
                    obj = new b(i);
                }
                lo6.b.set(lo6.a & i, obj);
                i++;
            } else {
                lo6._state$internal = h.a(j, 1152921504606846976L);
                return lo6;
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        long j;
        do {
            j = this._state$internal;
            if ((j & 2305843009213693952L) != 0) {
                return true;
            }
            if ((1152921504606846976L & j) != 0) {
                return false;
            }
        } while (!f.compareAndSet(this, j, j | 2305843009213693952L));
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x006c A[LOOP:1: B:20:0x006c->B:23:0x007e, LOOP_START, PHI: r1 
  PHI: (r1v7 com.fossil.lo6) = (r1v6 com.fossil.lo6), (r1v9 com.fossil.lo6) binds: [B:19:0x0064, B:23:0x007e] A[DONT_GENERATE, DONT_INLINE]] */
    @DexIgnore
    public final int a(E e2) {
        wg6.b(e2, "element");
        while (true) {
            long j = this._state$internal;
            if ((3458764513820540928L & j) != 0) {
                return h.a(j);
            }
            int i = (int) ((1073741823 & j) >> 0);
            int i2 = (int) ((1152921503533105152L & j) >> 30);
            int i3 = this.a;
            if (((i2 + 2) & i3) == (i & i3)) {
                return 1;
            }
            if (this.d || this.b.get(i2 & i3) == null) {
                if (f.compareAndSet(this, j, h.b(j, (i2 + 1) & 1073741823))) {
                    this.b.set(i2 & i3, e2);
                    lo6 lo6 = this;
                    while ((lo6._state$internal & 1152921504606846976L) != 0 && (lo6 = lo6.e().a(i2, e2)) != null) {
                        while ((lo6._state$internal & 1152921504606846976L) != 0 && (lo6 = lo6.e().a(i2, e2)) != null) {
                        }
                    }
                    return 0;
                }
            } else {
                int i4 = this.c;
                if (i4 < 1024 || ((i2 - i) & 1073741823) > (i4 >> 1)) {
                    return 1;
                }
            }
        }
        return 1;
    }

    @DexIgnore
    public final lo6<E> a(int i, int i2) {
        long j;
        int i3;
        do {
            j = this._state$internal;
            boolean z = false;
            i3 = (int) ((1073741823 & j) >> 0);
            if (nl6.a()) {
                if (i3 == i) {
                    z = true;
                }
                if (!z) {
                    throw new AssertionError();
                }
            }
            if ((1152921504606846976L & j) != 0) {
                return e();
            }
        } while (!f.compareAndSet(this, j, h.a(j, i2)));
        this.b.set(this.a & i3, (Object) null);
        return null;
    }
}
