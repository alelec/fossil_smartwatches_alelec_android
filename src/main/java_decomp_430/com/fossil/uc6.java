package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uc6 implements Comparable<uc6> {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public /* synthetic */ uc6(int i) {
        this.a = i;
    }

    @DexIgnore
    public static boolean a(int i, Object obj) {
        if (obj instanceof uc6) {
            if (i == ((uc6) obj).a()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static final /* synthetic */ uc6 b(int i) {
        return new uc6(i);
    }

    @DexIgnore
    public static int c(int i) {
        return i;
    }

    @DexIgnore
    public static int d(int i) {
        return i;
    }

    @DexIgnore
    public static String e(int i) {
        return String.valueOf(((long) i) & 4294967295L);
    }

    @DexIgnore
    public final /* synthetic */ int a() {
        return this.a;
    }

    @DexIgnore
    public final int a(int i) {
        return a(this.a, i);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return a(((uc6) obj).a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        int i = this.a;
        d(i);
        return i;
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return dd6.a(i, i2);
    }
}
