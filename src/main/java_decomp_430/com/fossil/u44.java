package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.PinObject;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Background> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Background> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends TypeToken<ArrayList<RingStyleItem>> {
    }

    @DexIgnore
    public final String a(ArrayList<RingStyleItem> arrayList) {
        if (l42.a(arrayList)) {
            return "";
        }
        String a2 = new Gson().a(arrayList, new d().getType());
        wg6.a((Object) a2, "Gson().toJson(ringStyles, type)");
        return a2;
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> b(String str) {
        wg6.b(str, PinObject.COLUMN_JSON_DATA);
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object a2 = new Gson().a(str, new c().getType());
        wg6.a(a2, "Gson().fromJson(json, type)");
        return (ArrayList) a2;
    }

    @DexIgnore
    public final String a(Background background) {
        if (background == null) {
            return "";
        }
        String a2 = new Gson().a(background, new a().getType());
        wg6.a((Object) a2, "Gson().toJson(background, type)");
        return a2;
    }

    @DexIgnore
    public final Background a(String str) {
        wg6.b(str, PinObject.COLUMN_JSON_DATA);
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return (Background) new Gson().a(str, new b().getType());
    }
}
