package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kf6 {
    @DexIgnore
    kf6 getCallerFrame();

    @DexIgnore
    StackTraceElement getStackTraceElement();
}
