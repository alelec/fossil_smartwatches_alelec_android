package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg1 extends ui1 {
    @DexIgnore
    public y11 T; // = y11.LOW;

    @DexIgnore
    public zg1(ue1 ue1, q41 q41, HashMap<io0, Object> hashMap) {
        super(ue1, q41, eh1.GET_DATA_COLLECTION_FILE_IN_BACKGROUND, hashMap, ze0.a("UUID.randomUUID().toString()"));
    }

    @DexIgnore
    public void a(y11 y11) {
        this.T = y11;
    }

    @DexIgnore
    public y11 e() {
        return this.T;
    }
}
