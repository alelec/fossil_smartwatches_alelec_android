package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText q;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout t;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout u;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public /* final */ ProgressButton y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public fd4(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, RTLImageView rTLImageView, ConstraintLayout constraintLayout2, ProgressButton progressButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleTextInputEditText;
        this.r = flexibleTextInputEditText2;
        this.s = flexibleTextInputEditText3;
        this.t = flexibleTextInputLayout;
        this.u = flexibleTextInputLayout2;
        this.v = flexibleTextInputLayout3;
        this.w = rTLImageView;
        this.x = constraintLayout2;
        this.y = progressButton;
        this.z = flexibleTextView;
        this.A = flexibleTextView2;
    }
}
