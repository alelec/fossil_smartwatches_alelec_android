package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v22 implements Parcelable.Creator<d12> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 != 2) {
                f22.v(parcel, a);
            } else {
                str = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new d12(i, str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new d12[i];
    }
}
