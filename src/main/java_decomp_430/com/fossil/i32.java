package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i32 implements Parcelable.Creator<z12> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        int i = 0;
        Scope[] scopeArr = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                i2 = f22.q(parcel, a);
            } else if (a2 == 3) {
                i3 = f22.q(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                scopeArr = (Scope[]) f22.b(parcel, a, Scope.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new z12(i, i2, i3, scopeArr);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new z12[i];
    }
}
