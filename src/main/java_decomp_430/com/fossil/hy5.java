package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.facebook.GraphRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hy5 implements c36 {
    @DexIgnore
    public static volatile hy5 d;
    @DexIgnore
    public a a;
    @DexIgnore
    public b36 b;
    @DexIgnore
    public String c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);

        @DexIgnore
        void b();

        @DexIgnore
        void c();
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(Unknown Source)
        	at java.util.ArrayList.get(Unknown Source)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    @DexIgnore
    public static synchronized com.fossil.hy5 a() {
        /*
            java.lang.Class<com.fossil.hy5> r0 = com.fossil.hy5.class
            monitor-enter(r0)
            com.fossil.hy5 r1 = d     // Catch:{ all -> 0x001c }
            if (r1 != 0) goto L_0x0018
            monitor-enter(r0)     // Catch:{ all -> 0x001c }
            com.fossil.hy5 r1 = d     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x0013
            com.fossil.hy5 r1 = new com.fossil.hy5     // Catch:{ all -> 0x0015 }
            r1.<init>()     // Catch:{ all -> 0x0015 }
            d = r1     // Catch:{ all -> 0x0015 }
        L_0x0013:
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            goto L_0x0018
        L_0x0015:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0015 }
            throw r1     // Catch:{ all -> 0x001c }
        L_0x0018:
            com.fossil.hy5 r1 = d     // Catch:{ all -> 0x001c }
            monitor-exit(r0)
            return r1
        L_0x001c:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hy5.a():com.fossil.hy5");
    }

    @DexIgnore
    public void a(m26 m26) {
    }

    @DexIgnore
    public final void b(n26 n26) {
        if (n26.a() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize canceled!");
            a aVar = this.a;
            if (aVar != null) {
                aVar.c();
            }
        }
    }

    @DexIgnore
    public final void c(n26 n26) {
        if (n26.a() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize denied!");
            a aVar = this.a;
            if (aVar != null) {
                aVar.a();
            }
        }
    }

    @DexIgnore
    public final void d(n26 n26) {
        if (n26.a() == 1) {
            w26 w26 = (w26) n26;
            if (w26.d.equals("com.fossil.wearables.fossil.tag_wechat_login")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = GraphRequest.TAG;
                local.d(str, "Wechat authorize succeed, data = " + w26.c + ", openid: " + w26.b);
                a aVar = this.a;
                if (aVar != null) {
                    aVar.a(w26.c);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        this.c = str;
    }

    @DexIgnore
    public void a(Context context) {
        this.b = e36.a(context, this.c, false);
        this.b.a(this.c);
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, initialize");
    }

    @DexIgnore
    public void a(a aVar) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, login...");
        this.a = aVar;
        if (!this.b.a()) {
            this.a.b();
            return;
        }
        v26 v26 = new v26();
        v26.c = "snsapi_userinfo";
        v26.d = "com.fossil.wearables.fossil.tag_wechat_login";
        if (!this.b.a((m26) v26)) {
            this.a.c();
        }
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, start authorize...");
    }

    @DexIgnore
    public void a(n26 n26) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, onResp...");
        int i = n26.a;
        if (i == -4) {
            c(n26);
        } else if (i == -2) {
            b(n26);
        } else if (i != 0) {
            a aVar = this.a;
            if (aVar != null) {
                aVar.a();
            }
        } else {
            d(n26);
        }
    }

    @DexIgnore
    public void a(Intent intent, c36 c36) {
        b36 b36 = this.b;
        if (b36 != null) {
            b36.a(intent, c36);
        }
    }
}
