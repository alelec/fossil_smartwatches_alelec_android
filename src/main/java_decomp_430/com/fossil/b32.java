package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b32 extends ta2 implements s12 {
    @DexIgnore
    public b32(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @DexIgnore
    public final x52 a(x52 x52, z12 z12) throws RemoteException {
        Parcel q = q();
        ua2.a(q, (IInterface) x52);
        ua2.a(q, (Parcelable) z12);
        Parcel a = a(2, q);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
