package com.fossil;

import android.database.sqlite.SQLiteProgram;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qi implements ki {
    @DexIgnore
    public /* final */ SQLiteProgram a;

    @DexIgnore
    public qi(SQLiteProgram sQLiteProgram) {
        this.a = sQLiteProgram;
    }

    @DexIgnore
    public void a(int i) {
        this.a.bindNull(i);
    }

    @DexIgnore
    public void close() {
        this.a.close();
    }

    @DexIgnore
    public void a(int i, long j) {
        this.a.bindLong(i, j);
    }

    @DexIgnore
    public void a(int i, double d) {
        this.a.bindDouble(i, d);
    }

    @DexIgnore
    public void a(int i, String str) {
        this.a.bindString(i, str);
    }

    @DexIgnore
    public void a(int i, byte[] bArr) {
        this.a.bindBlob(i, bArr);
    }
}
