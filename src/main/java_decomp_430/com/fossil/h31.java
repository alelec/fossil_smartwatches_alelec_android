package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h31 extends uj1 {
    @DexIgnore
    public static /* final */ m11 CREATOR; // = new m11((qg6) null);
    @DexIgnore
    public /* final */ double b;

    @DexIgnore
    public h31(double d) {
        super(ck0.DELAY);
        this.b = d;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.DELAY_IN_SECOND, (Object) Double.valueOf(this.b));
    }

    @DexIgnore
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort((short) ((int) ((this.b * ((double) 1000)) / 100.0d)));
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(h31.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((h31) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.DelayInstr");
    }

    @DexIgnore
    public int hashCode() {
        return (int) this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.b);
        }
    }

    @DexIgnore
    public /* synthetic */ h31(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readDouble();
    }
}
