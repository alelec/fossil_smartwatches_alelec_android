package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn3 extends jn3<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ fn3 INSTANCE; // = new fn3();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public transient jn3<Comparable> a;
    @DexIgnore
    public transient jn3<Comparable> b;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public <S extends Comparable> jn3<S> nullsFirst() {
        jn3<Comparable> jn3 = this.a;
        if (jn3 != null) {
            return jn3;
        }
        jn3<Comparable> nullsFirst = super.nullsFirst();
        this.a = nullsFirst;
        return nullsFirst;
    }

    @DexIgnore
    public <S extends Comparable> jn3<S> nullsLast() {
        jn3<Comparable> jn3 = this.b;
        if (jn3 != null) {
            return jn3;
        }
        jn3<Comparable> nullsLast = super.nullsLast();
        this.b = nullsLast;
        return nullsLast;
    }

    @DexIgnore
    public <S extends Comparable> jn3<S> reverse() {
        return un3.INSTANCE;
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural()";
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        jk3.a(comparable);
        jk3.a(comparable2);
        return comparable.compareTo(comparable2);
    }
}
