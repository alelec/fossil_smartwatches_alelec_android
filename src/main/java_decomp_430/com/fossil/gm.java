package com.fossil;

import android.content.Context;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import com.fossil.mm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gm implements em {
    @DexIgnore
    public static /* final */ String j; // = tl.a("Processor");
    @DexIgnore
    public Context a;
    @DexIgnore
    public ml b;
    @DexIgnore
    public to c;
    @DexIgnore
    public WorkDatabase d;
    @DexIgnore
    public Map<String, mm> e; // = new HashMap();
    @DexIgnore
    public List<hm> f;
    @DexIgnore
    public Set<String> g;
    @DexIgnore
    public /* final */ List<em> h;
    @DexIgnore
    public /* final */ Object i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public em a;
        @DexIgnore
        public String b;
        @DexIgnore
        public ip3<Boolean> c;

        @DexIgnore
        public a(em emVar, String str, ip3<Boolean> ip3) {
            this.a = emVar;
            this.b = str;
            this.c = ip3;
        }

        @DexIgnore
        public void run() {
            boolean z;
            try {
                z = this.c.get().booleanValue();
            } catch (InterruptedException | ExecutionException unused) {
                z = true;
            }
            this.a.a(this.b, z);
        }
    }

    @DexIgnore
    public gm(Context context, ml mlVar, to toVar, WorkDatabase workDatabase, List<hm> list) {
        this.a = context;
        this.b = mlVar;
        this.c = toVar;
        this.d = workDatabase;
        this.f = list;
        this.g = new HashSet();
        this.h = new ArrayList();
        this.i = new Object();
    }

    @DexIgnore
    public boolean a(String str, WorkerParameters.a aVar) {
        synchronized (this.i) {
            if (this.e.containsKey(str)) {
                tl.a().a(j, String.format("Work %s is already enqueued for processing", new Object[]{str}), new Throwable[0]);
                return false;
            }
            mm.c cVar = new mm.c(this.a, this.b, this.c, this.d, str);
            cVar.a(this.f);
            cVar.a(aVar);
            mm a2 = cVar.a();
            ip3<Boolean> a3 = a2.a();
            a3.a(new a(this, str, a3), this.c.a());
            this.e.put(str, a2);
            this.c.b().execute(a2);
            tl.a().a(j, String.format("%s: processing %s", new Object[]{gm.class.getSimpleName(), str}), new Throwable[0]);
            return true;
        }
    }

    @DexIgnore
    public boolean b(String str) {
        boolean containsKey;
        synchronized (this.i) {
            containsKey = this.e.containsKey(str);
        }
        return containsKey;
    }

    @DexIgnore
    public boolean c(String str) {
        return a(str, (WorkerParameters.a) null);
    }

    @DexIgnore
    public boolean d(String str) {
        synchronized (this.i) {
            tl.a().a(j, String.format("Processor cancelling %s", new Object[]{str}), new Throwable[0]);
            this.g.add(str);
            mm remove = this.e.remove(str);
            if (remove != null) {
                remove.a(true);
                tl.a().a(j, String.format("WorkerWrapper cancelled for %s", new Object[]{str}), new Throwable[0]);
                return true;
            }
            tl.a().a(j, String.format("WorkerWrapper could not be found for %s", new Object[]{str}), new Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    public boolean e(String str) {
        synchronized (this.i) {
            tl.a().a(j, String.format("Processor stopping %s", new Object[]{str}), new Throwable[0]);
            mm remove = this.e.remove(str);
            if (remove != null) {
                remove.a(false);
                tl.a().a(j, String.format("WorkerWrapper stopped for %s", new Object[]{str}), new Throwable[0]);
                return true;
            }
            tl.a().a(j, String.format("WorkerWrapper could not be found for %s", new Object[]{str}), new Throwable[0]);
            return false;
        }
    }

    @DexIgnore
    public void b(em emVar) {
        synchronized (this.i) {
            this.h.remove(emVar);
        }
    }

    @DexIgnore
    public boolean a(String str) {
        boolean contains;
        synchronized (this.i) {
            contains = this.g.contains(str);
        }
        return contains;
    }

    @DexIgnore
    public void a(em emVar) {
        synchronized (this.i) {
            this.h.add(emVar);
        }
    }

    @DexIgnore
    public void a(String str, boolean z) {
        synchronized (this.i) {
            this.e.remove(str);
            tl.a().a(j, String.format("%s %s executed; reschedule = %s", new Object[]{getClass().getSimpleName(), str, Boolean.valueOf(z)}), new Throwable[0]);
            for (em a2 : this.h) {
                a2.a(str, z);
            }
        }
    }
}
