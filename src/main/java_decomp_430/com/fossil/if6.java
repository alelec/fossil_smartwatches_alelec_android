package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class if6 implements xe6<Object> {
    @DexIgnore
    public static /* final */ if6 a; // = new if6();

    @DexIgnore
    public af6 getContext() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public String toString() {
        return "This continuation is already complete";
    }
}
