package com.fossil;

import android.content.Context;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class di3 extends q1 {
    @DexIgnore
    public di3(Context context) {
        super(context);
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        t1 t1Var = (t1) a(i, i2, i3, charSequence);
        fi3 fi3 = new fi3(e(), this, t1Var);
        t1Var.a((c2) fi3);
        return fi3;
    }
}
