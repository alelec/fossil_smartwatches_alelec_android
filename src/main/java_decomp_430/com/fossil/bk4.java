package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Pair;
import com.portfolio.platform.PortfolioApp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bk4 {
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> a; // = new k();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> b; // = new v();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> c; // = new b0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> d; // = new e0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> e; // = new f0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> f; // = new g0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> g; // = new h0();
    @DexIgnore
    @SuppressLint({"ConstantLocale"})
    public static ThreadLocal<SimpleDateFormat> h; // = new d();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> i; // = new e();
    @DexIgnore
    public static ThreadLocal<DateTimeFormatter> j; // = new f();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> k; // = new g();
    @DexIgnore
    public static ThreadLocal<DateTimeFormatter> l; // = new h();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> m; // = new i();
    @DexIgnore
    public static TimeZone n; // = TimeZone.getDefault();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.getDefault());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSS", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("EEEE, MMMM dd", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMMM, yyyy", Locale.getDefault());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter initialValue() {
            return DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("hh:mm aa", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter initialValue() {
            return ISODateTimeFormat.dateTime().withLocale(Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return simpleDateFormat;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class k extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class l extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyyMMdd", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class m extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class n extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSS", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class o extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("EEEE, MMMM dd", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class p extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMMM, yyyy", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class q extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("hh:mm aa", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class r extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class s extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class t extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class u extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class v extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyyMMdd", Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class w extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter initialValue() {
            return DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class x extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return simpleDateFormat;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class y extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter initialValue() {
            return ISODateTimeFormat.dateTime().withLocale(Locale.US);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class z extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        }
    }

    /*
    static {
        new c0();
        new d0();
        new a();
        new b();
        new c();
    }
    */

    @DexIgnore
    public static DateTime a(DateTimeZone dateTimeZone, String str) {
        return l.get().withZone(dateTimeZone).parseDateTime(str);
    }

    @DexIgnore
    public static DateTime b(String str) {
        return j.get().parseDateTime(str);
    }

    @DexIgnore
    public static DateTime c(String str) {
        return l.get().withOffsetParsed().parseDateTime(str);
    }

    @DexIgnore
    public static boolean d(Date date, Date date2) {
        if (date == null || date2 == null) {
            return false;
        }
        return i(date).equals(i(date2));
    }

    @DexIgnore
    public static String e(Date date) {
        a.get().setTimeZone(n);
        return a.get().format(date);
    }

    @DexIgnore
    public static String f(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date);
    }

    @DexIgnore
    public static String g(Date date) {
        f.set(new SimpleDateFormat("hh:mm aa", Locale.getDefault()));
        return f.get().format(date);
    }

    @DexIgnore
    public static String h(Date date) {
        d.set(new SimpleDateFormat("MMMM yyyy", Locale.getDefault()));
        d.get().setTimeZone(n);
        return d.get().format(date);
    }

    @DexIgnore
    public static String i(Date date) {
        b.get().setTimeZone(n);
        return b.get().format(date);
    }

    @DexIgnore
    public static Date j(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        a(instance);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar k(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return b(instance);
    }

    @DexIgnore
    public static Calendar l(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        return c(instance);
    }

    @DexIgnore
    public static Date m(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(5, 1);
        return instance.getTime();
    }

    @DexIgnore
    public static Date n(Date date) {
        return b(date, 1);
    }

    @DexIgnore
    public static Date o(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        d(instance);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar p(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return e(instance);
    }

    @DexIgnore
    public static Calendar q(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        return f(instance);
    }

    @DexIgnore
    public static Pair<Date, Date> r(Date date) {
        return new Pair<>(q(date).getTime(), l(date).getTime());
    }

    @DexIgnore
    public static Boolean s(Date date) {
        if (date == null) {
            return false;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i2 = instance.get(1);
        int i3 = instance.get(2);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(new Date());
        int i4 = instance2.get(1);
        int i5 = instance2.get(2);
        if (i2 == i4 && i3 == i5) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static Boolean t(Date date) {
        return Boolean.valueOf(i(date).equals(i(new Date())));
    }

    @DexIgnore
    public static String u(Date date) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(date);
    }

    @DexIgnore
    public static String a(DateTimeZone dateTimeZone, DateTime dateTime) {
        return l.get().withZone(DateTimeZone.getDefault()).print(dateTime);
    }

    @DexIgnore
    public static String b(Date date) {
        i.get().setTimeZone(n);
        return i.get().format(date);
    }

    @DexIgnore
    public static pc6<Integer, Integer, Integer> c(int i2) {
        return new pc6<>(Integer.valueOf(i2 / DateTimeConstants.SECONDS_PER_HOUR), Integer.valueOf((i2 % DateTimeConstants.SECONDS_PER_HOUR) / 60), Integer.valueOf(i2 % 60));
    }

    @DexIgnore
    public static String a(DateTime dateTime) {
        return l.get().withOffsetParsed().print(dateTime);
    }

    @DexIgnore
    public static Date e(String str) throws ParseException {
        return a.get().parse(str);
    }

    @DexIgnore
    public static Calendar f(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.add(5, -(calendar2.get(7) - 1));
        d(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static Date a(String str) {
        try {
            return b.get().parse(str);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static Calendar b(Calendar calendar, TimeZone timeZone) {
        calendar.setTimeZone(timeZone);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    @DexIgnore
    public static String d(Date date) {
        h.get().setTimeZone(n);
        return h.get().format(date);
    }

    @DexIgnore
    public static Calendar e(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.set(5, 1);
        d(calendar2);
        return calendar2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public static String a(Date date) {
        return DateFormat.getMediumDateFormat(PortfolioApp.T.getApplicationContext()).format(date).toString();
    }

    @DexIgnore
    public static Calendar c(long j2) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j2);
        instance.setTimeZone(n);
        return instance;
    }

    @DexIgnore
    public static Calendar d(Calendar calendar) {
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    @DexIgnore
    public static boolean a(long j2, long j3) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTimeInMillis(j2);
        Calendar instance2 = Calendar.getInstance(Locale.US);
        instance2.setTimeInMillis(j3);
        if (instance.get(1) == instance2.get(1) && instance.get(2) == instance2.get(2)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static Date b(Date date, int i2) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        instance.add(5, -i2);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar c(Long l2) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l2.longValue());
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance;
    }

    @DexIgnore
    public static Date d(Date date, TimeZone timeZone) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        b(instance, timeZone);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar a(Calendar calendar) {
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        calendar.set(14, 999);
        return calendar;
    }

    @DexIgnore
    public static boolean b(Date date, Date date2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.getTime();
        d(instance);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        instance2.getTime();
        d(instance2);
        return instance.after(instance2);
    }

    @DexIgnore
    public static Date d(String str) {
        return ISODateTimeFormat.dateTime().parseDateTime(str).toDate();
    }

    @DexIgnore
    public static Date c(Date date, TimeZone timeZone) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        a(instance, timeZone);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar a(Calendar calendar, TimeZone timeZone) {
        calendar.setTimeZone(timeZone);
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        calendar.set(14, 999);
        return calendar;
    }

    @DexIgnore
    public static boolean c(Date date, Date date2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.getTime();
        d(instance);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        instance2.getTime();
        d(instance2);
        return (instance2 instanceof Calendar) && instance.compareTo(instance2) >= 0;
    }

    @DexIgnore
    public static Date a(int i2, String str) {
        try {
            DateTimeZone forOffsetMillis = DateTimeZone.forOffsetMillis(i2 * 1000);
            DateTime parseDateTime = ISODateTimeFormat.dateTime().withZone(forOffsetMillis).parseDateTime(str);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            simpleDateFormat.setTimeZone(forOffsetMillis.toTimeZone());
            return c.get().parse(simpleDateFormat.format(Long.valueOf(parseDateTime.getMillis())));
        } catch (Exception unused) {
            Calendar instance = Calendar.getInstance();
            instance.set(1990, 0, 1);
            return instance.getTime();
        }
    }

    @DexIgnore
    public static String b(DateTime dateTime) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(new Date(dateTime.getMillis()));
    }

    @DexIgnore
    public static Calendar b(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.set(5, calendar2.getActualMaximum(5));
        a(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static Calendar c(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.add(5, -(calendar2.get(7) - 7));
        a(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static synchronized Date b(Date date, TimeZone timeZone) {
        Date parse;
        synchronized (bk4.class) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
            String format = simpleDateFormat.format(date);
            simpleDateFormat.setTimeZone(timeZone);
            try {
                parse = simpleDateFormat.parse(format);
            } catch (ParseException e2) {
                e2.printStackTrace();
                return new Date();
            }
        }
        return parse;
    }

    @DexIgnore
    public static TimeZone a(int i2) {
        TimeZone timeZone;
        TimeZone timeZone2 = TimeZone.getDefault();
        if (i2 == timeZone2.getRawOffset() / 1000) {
            return timeZone2;
        }
        String[] availableIDs = TimeZone.getAvailableIDs(i2 * 1000);
        if (availableIDs != null && availableIDs.length > 0) {
            for (String str : availableIDs) {
                if (!TextUtils.isEmpty(str) && (timeZone = TimeZone.getTimeZone(str)) != null) {
                    return timeZone;
                }
            }
        }
        return TimeZone.getDefault();
    }

    @DexIgnore
    public static String c(Date date) {
        c.get().setTimeZone(n);
        return c.get().format(date);
    }

    @DexIgnore
    public static String b(long j2) {
        return e.get().format(new Date(j2));
    }

    @DexIgnore
    public static int b(Long l2) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTimeInMillis(l2.longValue());
        return instance.get(11);
    }

    @DexIgnore
    public static int a(String str, boolean z2) {
        return a(str, new Date(), z2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v19, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v21, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v23, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public static String b(int i2) {
        switch (i2) {
            case 0:
                return jm4.a((Context) PortfolioApp.T, 2131886621);
            case 1:
                return jm4.a((Context) PortfolioApp.T, 2131886620);
            case 2:
                return jm4.a((Context) PortfolioApp.T, 2131886624);
            case 3:
                return jm4.a((Context) PortfolioApp.T, 2131886617);
            case 4:
                return jm4.a((Context) PortfolioApp.T, 2131886625);
            case 5:
                return jm4.a((Context) PortfolioApp.T, 2131886623);
            case 6:
                return jm4.a((Context) PortfolioApp.T, 2131886622);
            case 7:
                return jm4.a((Context) PortfolioApp.T, 2131886618);
            case 8:
                return jm4.a((Context) PortfolioApp.T, 2131886628);
            case 9:
                return jm4.a((Context) PortfolioApp.T, 2131886627);
            case 10:
                return jm4.a((Context) PortfolioApp.T, 2131886626);
            case 11:
                return jm4.a((Context) PortfolioApp.T, 2131886619);
            default:
                return "";
        }
    }

    @DexIgnore
    public static int a(String str, Date date, boolean z2) {
        if (TextUtils.isEmpty(str)) {
            return TimeZone.getDefault().getRawOffset() / 1000;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (!timeZone.inDaylightTime(date) || !z2) {
            return timeZone.getRawOffset() / 1000;
        }
        return (timeZone.getRawOffset() + timeZone.getDSTSavings()) / 1000;
    }

    @DexIgnore
    public static String a(Date date, TimeZone timeZone) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(date);
    }

    @DexIgnore
    public static String a(long j2, int i2) {
        e.get().setTimeZone(a(i2));
        return e.get().format(new Date(j2));
    }

    @DexIgnore
    public static void b() {
        a = new j();
        b = new l();
        c = new m();
        new n();
        new o();
        d = new p();
        e = new q();
        g = new r();
        new s();
        new t();
        k = new u();
        j = new w();
        m = new x();
        l = new y();
        h = new z();
        i = new a0();
        n = TimeZone.getDefault();
    }

    @DexIgnore
    public static String a(long j2) {
        return g.get().format(new Date(j2));
    }

    @DexIgnore
    public static Pair<Date, Date> a(Date date, Date date2) {
        Pair<Date, Date> r2 = r(date);
        Date date3 = (Date) r2.first;
        Date date4 = (Date) r2.second;
        if (!b(date2, date3)) {
            date2 = date3;
        }
        if (b(date4, new Date())) {
            date4 = new Date();
        }
        return new Pair<>(date2, date4);
    }

    @DexIgnore
    public static int a(Long l2) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTimeInMillis(l2.longValue());
        return instance.get(7);
    }

    @DexIgnore
    public static boolean a(Long l2, Long l3) {
        return o(new Date(l2.longValue())).getTime() == o(new Date(l3.longValue())).getTime();
    }

    @DexIgnore
    public static Calendar a(int i2, Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.add(1, i2 / 12);
        calendar2.add(2, i2 % 12);
        calendar2.set(5, 1);
        return calendar2;
    }

    @DexIgnore
    public static int a() {
        return a(TimeZone.getDefault());
    }

    @DexIgnore
    public static int a(TimeZone timeZone) {
        return timeZone.getRawOffset() / 1000;
    }

    @DexIgnore
    public static DateTime a(Date date, int i2) {
        return new DateTime((Object) date).withZone(DateTimeZone.forOffsetMillis(i2 * 1000));
    }
}
