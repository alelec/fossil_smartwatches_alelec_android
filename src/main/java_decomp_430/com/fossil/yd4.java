package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yd4 extends xd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public long I;

    /*
    static {
        K.put(2131362676, 1);
        K.put(2131362561, 2);
        K.put(2131362820, 3);
        K.put(2131363218, 4);
        K.put(2131363128, 5);
        K.put(2131362979, 6);
        K.put(2131362516, 7);
        K.put(2131362185, 8);
        K.put(2131362554, 9);
        K.put(2131362521, 10);
        K.put(2131362193, 11);
        K.put(2131363150, 12);
        K.put(2131363149, 13);
        K.put(2131361919, 14);
        K.put(2131361921, 15);
        K.put(2131363212, 16);
        K.put(2131362589, 17);
        K.put(2131362594, 18);
        K.put(2131362537, 19);
        K.put(2131362642, 20);
        K.put(2131362641, 21);
        K.put(2131363157, 22);
        K.put(2131363175, 23);
        K.put(2131361934, 24);
    }
    */

    @DexIgnore
    public yd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 25, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public yd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[14], objArr[15], objArr[24], objArr[8], objArr[11], objArr[7], objArr[10], objArr[19], objArr[9], objArr[2], objArr[17], objArr[18], objArr[21], objArr[20], objArr[1], objArr[3], objArr[0], objArr[6], objArr[5], objArr[13], objArr[12], objArr[22], objArr[23], objArr[16], objArr[4]);
        this.I = -1;
        this.D.setTag((Object) null);
        a(view);
        f();
    }
}
