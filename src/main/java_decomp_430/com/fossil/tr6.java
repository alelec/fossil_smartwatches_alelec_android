package com.fossil;

import com.fossil.sr6;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tr6 {
    @DexIgnore
    public /* final */ aq6 a;
    @DexIgnore
    public sr6.a b;
    @DexIgnore
    public ar6 c;
    @DexIgnore
    public /* final */ iq6 d;
    @DexIgnore
    public /* final */ dq6 e;
    @DexIgnore
    public /* final */ pq6 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ sr6 h;
    @DexIgnore
    public int i;
    @DexIgnore
    public pr6 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public wr6 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends WeakReference<tr6> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public a(tr6 tr6, Object obj) {
            super(tr6);
            this.a = obj;
        }
    }

    @DexIgnore
    public tr6(iq6 iq6, aq6 aq6, dq6 dq6, pq6 pq6, Object obj) {
        this.d = iq6;
        this.a = aq6;
        this.e = dq6;
        this.f = pq6;
        this.h = new sr6(aq6, i(), dq6, pq6);
        this.g = obj;
    }

    @DexIgnore
    public wr6 a(OkHttpClient okHttpClient, Interceptor.Chain chain, boolean z) {
        try {
            wr6 a2 = a(chain.d(), chain.a(), chain.b(), okHttpClient.q(), okHttpClient.A(), z).a(okHttpClient, chain, this);
            synchronized (this.d) {
                this.n = a2;
            }
            return a2;
        } catch (IOException e2) {
            throw new rr6(e2);
        }
    }

    @DexIgnore
    public wr6 b() {
        wr6 wr6;
        synchronized (this.d) {
            wr6 = this.n;
        }
        return wr6;
    }

    @DexIgnore
    public synchronized pr6 c() {
        return this.j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r0 = r1.b;
     */
    @DexIgnore
    public boolean d() {
        sr6.a aVar;
        return this.c != null || (aVar != null && aVar.b()) || this.h.a();
    }

    @DexIgnore
    public void e() {
        pr6 pr6;
        Socket a2;
        synchronized (this.d) {
            pr6 = this.j;
            a2 = a(true, false, false);
            if (this.j != null) {
                pr6 = null;
            }
        }
        fr6.a(a2);
        if (pr6 != null) {
            this.f.b(this.e, (hq6) pr6);
        }
    }

    @DexIgnore
    public void f() {
        pr6 pr6;
        Socket a2;
        synchronized (this.d) {
            pr6 = this.j;
            a2 = a(false, true, false);
            if (this.j != null) {
                pr6 = null;
            }
        }
        fr6.a(a2);
        if (pr6 != null) {
            dr6.a.a(this.e, (IOException) null);
            this.f.b(this.e, (hq6) pr6);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final Socket g() {
        pr6 pr6 = this.j;
        if (pr6 == null || !pr6.k) {
            return null;
        }
        return a(false, false, true);
    }

    @DexIgnore
    public ar6 h() {
        return this.c;
    }

    @DexIgnore
    public final qr6 i() {
        return dr6.a.a(this.d);
    }

    @DexIgnore
    public String toString() {
        pr6 c2 = c();
        return c2 != null ? c2.toString() : this.a.toString();
    }

    @DexIgnore
    public Socket b(pr6 pr6) {
        if (this.n == null && this.j.n.size() == 1) {
            Socket a2 = a(true, false, false);
            this.j = pr6;
            pr6.n.add(this.j.n.get(0));
            return a2;
        }
        throw new IllegalStateException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0.a(r9) != false) goto L_0x0018;
     */
    @DexIgnore
    public final pr6 a(int i2, int i3, int i4, int i5, boolean z, boolean z2) throws IOException {
        while (true) {
            pr6 a2 = a(i2, i3, i4, i5, z);
            synchronized (this.d) {
                if (a2.l == 0) {
                    return a2;
                }
            }
            e();
        }
        while (true) {
        }
    }

    @DexIgnore
    public final pr6 a(int i2, int i3, int i4, int i5, boolean z) throws IOException {
        Socket g2;
        Socket socket;
        pr6 pr6;
        pr6 pr62;
        ar6 ar6;
        pr6 pr63;
        boolean z2;
        boolean z3;
        sr6.a aVar;
        synchronized (this.d) {
            if (this.l) {
                throw new IllegalStateException("released");
            } else if (this.n != null) {
                throw new IllegalStateException("codec != null");
            } else if (!this.m) {
                pr6 pr64 = this.j;
                g2 = g();
                socket = null;
                if (this.j != null) {
                    pr62 = this.j;
                    pr6 = null;
                } else {
                    pr6 = pr64;
                    pr62 = null;
                }
                if (!this.k) {
                    pr6 = null;
                }
                if (pr62 == null) {
                    dr6.a.a(this.d, this.a, this, (ar6) null);
                    if (this.j != null) {
                        pr63 = this.j;
                        ar6 = null;
                        z2 = true;
                    } else {
                        ar6 = this.c;
                        pr63 = pr62;
                    }
                } else {
                    pr63 = pr62;
                    ar6 = null;
                }
                z2 = false;
            } else {
                throw new IOException("Canceled");
            }
        }
        fr6.a(g2);
        if (pr6 != null) {
            this.f.b(this.e, (hq6) pr6);
        }
        if (z2) {
            this.f.a(this.e, (hq6) pr63);
        }
        if (pr63 != null) {
            return pr63;
        }
        if (ar6 != null || ((aVar = this.b) != null && aVar.b())) {
            z3 = false;
        } else {
            this.b = this.h.c();
            z3 = true;
        }
        synchronized (this.d) {
            if (!this.m) {
                if (z3) {
                    List<ar6> a2 = this.b.a();
                    int size = a2.size();
                    int i6 = 0;
                    while (true) {
                        if (i6 >= size) {
                            break;
                        }
                        ar6 ar62 = a2.get(i6);
                        dr6.a.a(this.d, this.a, this, ar62);
                        if (this.j != null) {
                            pr63 = this.j;
                            this.c = ar62;
                            z2 = true;
                            break;
                        }
                        i6++;
                    }
                }
                if (!z2) {
                    if (ar6 == null) {
                        ar6 = this.b.c();
                    }
                    this.c = ar6;
                    this.i = 0;
                    pr63 = new pr6(this.d, ar6);
                    a(pr63, false);
                }
            } else {
                throw new IOException("Canceled");
            }
        }
        if (z2) {
            this.f.a(this.e, (hq6) pr63);
            return pr63;
        }
        pr63.a(i2, i3, i4, i5, z, this.e, this.f);
        i().a(pr63.f());
        synchronized (this.d) {
            this.k = true;
            dr6.a.b(this.d, pr63);
            if (pr63.e()) {
                socket = dr6.a.a(this.d, this.a, this);
                pr63 = this.j;
            }
        }
        fr6.a(socket);
        this.f.a(this.e, (hq6) pr63);
        return pr63;
    }

    @DexIgnore
    public void a(boolean z, wr6 wr6, long j2, IOException iOException) {
        pr6 pr6;
        Socket a2;
        boolean z2;
        this.f.b(this.e, j2);
        synchronized (this.d) {
            if (wr6 != null) {
                if (wr6 == this.n) {
                    if (!z) {
                        this.j.l++;
                    }
                    pr6 = this.j;
                    a2 = a(z, false, true);
                    if (this.j != null) {
                        pr6 = null;
                    }
                    z2 = this.l;
                }
            }
            throw new IllegalStateException("expected " + this.n + " but was " + wr6);
        }
        fr6.a(a2);
        if (pr6 != null) {
            this.f.b(this.e, (hq6) pr6);
        }
        if (iOException != null) {
            this.f.a(this.e, dr6.a.a(this.e, iOException));
        } else if (z2) {
            dr6.a.a(this.e, (IOException) null);
            this.f.a(this.e);
        }
    }

    @DexIgnore
    public final Socket a(boolean z, boolean z2, boolean z3) {
        Socket socket;
        if (z3) {
            this.n = null;
        }
        if (z2) {
            this.l = true;
        }
        pr6 pr6 = this.j;
        if (pr6 != null) {
            if (z) {
                pr6.k = true;
            }
            if (this.n == null && (this.l || this.j.k)) {
                a(this.j);
                if (this.j.n.isEmpty()) {
                    this.j.o = System.nanoTime();
                    if (dr6.a.a(this.d, this.j)) {
                        socket = this.j.g();
                        this.j = null;
                        return socket;
                    }
                }
                socket = null;
                this.j = null;
                return socket;
            }
        }
        return null;
    }

    @DexIgnore
    public void a() {
        wr6 wr6;
        pr6 pr6;
        synchronized (this.d) {
            this.m = true;
            wr6 = this.n;
            pr6 = this.j;
        }
        if (wr6 != null) {
            wr6.cancel();
        } else if (pr6 != null) {
            pr6.b();
        }
    }

    @DexIgnore
    public void a(IOException iOException) {
        boolean z;
        pr6 pr6;
        Socket a2;
        synchronized (this.d) {
            if (iOException instanceof ts6) {
                hs6 hs6 = ((ts6) iOException).errorCode;
                if (hs6 == hs6.REFUSED_STREAM) {
                    this.i++;
                    if (this.i > 1) {
                        this.c = null;
                    }
                    z = false;
                    pr6 = this.j;
                    a2 = a(z, false, true);
                    if (this.j != null || !this.k) {
                        pr6 = null;
                    }
                } else {
                    if (hs6 != hs6.CANCEL) {
                        this.c = null;
                    }
                    z = false;
                    pr6 = this.j;
                    a2 = a(z, false, true);
                    pr6 = null;
                }
            } else {
                if (this.j != null && (!this.j.e() || (iOException instanceof gs6))) {
                    if (this.j.l == 0) {
                        if (!(this.c == null || iOException == null)) {
                            this.h.a(this.c, iOException);
                        }
                        this.c = null;
                    }
                }
                z = false;
                pr6 = this.j;
                a2 = a(z, false, true);
                pr6 = null;
            }
            z = true;
            pr6 = this.j;
            a2 = a(z, false, true);
            pr6 = null;
        }
        fr6.a(a2);
        if (pr6 != null) {
            this.f.b(this.e, (hq6) pr6);
        }
    }

    @DexIgnore
    public void a(pr6 pr6, boolean z) {
        if (this.j == null) {
            this.j = pr6;
            this.k = z;
            pr6.n.add(new a(this, this.g));
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public final void a(pr6 pr6) {
        int size = pr6.n.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (pr6.n.get(i2).get() == this) {
                pr6.n.remove(i2);
                return;
            }
        }
        throw new IllegalStateException();
    }
}
