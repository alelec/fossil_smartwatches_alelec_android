package com.fossil;

import com.fossil.zm2;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class um2<T extends zm2<T>> {
    @DexIgnore
    public abstract int a(Map.Entry<?, ?> entry);

    @DexIgnore
    public abstract xm2<T> a(Object obj);

    @DexIgnore
    public abstract Object a(sm2 sm2, ro2 ro2, int i);

    @DexIgnore
    public abstract void a(tq2 tq2, Map.Entry<?, ?> entry) throws IOException;

    @DexIgnore
    public abstract boolean a(ro2 ro2);

    @DexIgnore
    public abstract xm2<T> b(Object obj);

    @DexIgnore
    public abstract void c(Object obj);
}
