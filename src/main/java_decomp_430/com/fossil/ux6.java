package com.fossil;

import java.lang.annotation.Annotation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux6 implements tx6 {
    @DexIgnore
    public static /* final */ tx6 a; // = new ux6();

    @DexIgnore
    public static Annotation[] a(Annotation[] annotationArr) {
        if (vx6.a(annotationArr, (Class<? extends Annotation>) tx6.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[(annotationArr.length + 1)];
        annotationArr2[0] = a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    @DexIgnore
    public Class<? extends Annotation> annotationType() {
        return tx6.class;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof tx6;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "@" + tx6.class.getName() + "()";
    }
}
