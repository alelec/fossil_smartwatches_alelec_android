package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn4 extends BroadcastReceiver {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        MFLogger.d("TimeTickReceiver", "onReceive - action=" + action);
        if (!TextUtils.isEmpty(action) && wg6.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
            Calendar instance = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            wg6.a((Object) instance, "calendar");
            String format = simpleDateFormat.format(instance.getTime());
            an4 an4 = new an4(context);
            String A = an4.A();
            MFLogger.d("TimeTickReceiver", "onReceive - day= " + format + ", widgetsDateChanged= " + A);
            if (!xj6.b(A, format, false, 2, (Object) null) && context != null) {
                MFLogger.d("TimeTickReceiver", "onReceive - need to resetAllContentWidgetsUI");
                an4.x(format);
            }
        }
    }
}
