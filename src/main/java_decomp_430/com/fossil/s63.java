package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s63 extends t63 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public s63(x53 x53) {
        super(x53);
        this.a.a(this);
    }

    @DexIgnore
    public void m() {
    }

    @DexIgnore
    public final void n() {
        if (!r()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void o() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!q()) {
            this.a.k();
            this.b = true;
        }
    }

    @DexIgnore
    public final void p() {
        if (!this.b) {
            m();
            this.a.k();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean q();

    @DexIgnore
    public final boolean r() {
        return this.b;
    }
}
