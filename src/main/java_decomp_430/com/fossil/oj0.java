package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oj0 {
    @DexIgnore
    public /* synthetic */ oj0(qg6 qg6) {
    }

    @DexIgnore
    public final il0 a(ch0 ch0) {
        switch (vh0.a[ch0.b.ordinal()]) {
            case 1:
                return il0.SUCCESS;
            case 2:
                return il0.INTERRUPTED;
            case 3:
                return il0.REQUEST_TIMEOUT;
            case 4:
                return il0.CONNECTION_DROPPED;
            case 5:
                return il0.BLUETOOTH_OFF;
            case 6:
                return il0.HID_PROXY_NOT_CONNECTED;
            case 7:
                return il0.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
            case 8:
                return il0.HID_INPUT_DEVICE_DISABLED;
            case 9:
                return il0.HID_UNKNOWN_ERROR;
            case 10:
                return il0.REQUEST_UNSUPPORTED;
            case 11:
                int i = ch0.c.b;
                if (i == ao0.UNSUPPORTED_FILE_HANDLE.a) {
                    return il0.UNSUPPORTED_FILE_HANDLE;
                }
                if (i == ao0.REQUEST_NOT_SUPPORTED.a) {
                    return il0.REQUEST_UNSUPPORTED;
                }
                return il0.COMMAND_ERROR;
            default:
                return il0.COMMAND_ERROR;
        }
    }
}
