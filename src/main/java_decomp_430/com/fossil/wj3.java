package com.fossil;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wj3 extends tj3 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new b();
    @DexIgnore
    public /* final */ TextInputLayout.g f; // = new c(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            wj3 wj3 = wj3.this;
            wj3.c.setChecked(!wj3.c());
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements TextInputLayout.f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(true);
            wj3 wj3 = wj3.this;
            wj3.c.setChecked(!wj3.c());
            editText.removeTextChangedListener(wj3.this.d);
            editText.addTextChangedListener(wj3.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements TextInputLayout.g {
        @DexIgnore
        public c(wj3 wj3) {
        }

        @DexIgnore
        public void a(TextInputLayout textInputLayout, int i) {
            EditText editText = textInputLayout.getEditText();
            if (editText != null && i == 1) {
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            EditText editText = wj3.this.a.getEditText();
            if (editText != null) {
                int selectionEnd = editText.getSelectionEnd();
                if (wj3.this.c()) {
                    editText.setTransformationMethod((TransformationMethod) null);
                } else {
                    editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                if (selectionEnd >= 0) {
                    editText.setSelection(selectionEnd);
                }
            }
        }
    }

    @DexIgnore
    public wj3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public final boolean c() {
        EditText editText = this.a.getEditText();
        return editText != null && (editText.getTransformationMethod() instanceof PasswordTransformationMethod);
    }

    @DexIgnore
    public void a() {
        this.a.setEndIconDrawable(u0.c(this.b, qf3.design_password_eye));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(vf3.password_toggle_content_description));
        this.a.setEndIconOnClickListener(new d());
        this.a.a(this.e);
        this.a.a(this.f);
    }
}
