package com.fossil;

import com.fossil.fitness.WorkoutType;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fi4 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    CYCLING("cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final fi4 a(String str) {
            fi4 fi4;
            wg6.b(str, ServerSetting.VALUE);
            fi4[] values = fi4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    fi4 = null;
                    break;
                }
                fi4 = values[i];
                String mValue = fi4.getMValue();
                String lowerCase = str.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (wg6.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return fi4 != null ? fi4 : fi4.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final fi4 a(int i) {
            if (i == WorkoutType.UNKNOWN.ordinal()) {
                return fi4.UNKNOWN;
            }
            if (i == WorkoutType.RUNNING.ordinal()) {
                return fi4.RUNNING;
            }
            if (i == WorkoutType.CYCLING.ordinal()) {
                return fi4.CYCLING;
            }
            if (i == WorkoutType.TREADMILL.ordinal()) {
                return fi4.TREADMILL;
            }
            if (i == WorkoutType.ELLIPTICAL.ordinal()) {
                return fi4.ELLIPTICAL;
            }
            if (i == WorkoutType.WEIGHTS.ordinal()) {
                return fi4.WEIGHTS;
            }
            if (i == WorkoutType.WORKOUT.ordinal()) {
                return fi4.WORKOUT;
            }
            return fi4.UNKNOWN;
        }

        @DexIgnore
        public final lc6<Integer, Integer> a(fi4 fi4) {
            if (fi4 == null) {
                fi4 = fi4.UNKNOWN;
            }
            if (fi4 != null) {
                switch (ei4.a[fi4.ordinal()]) {
                    case 1:
                        return new lc6<>(2131231228, 2131886405);
                    case 2:
                        return new lc6<>(2131231225, 2131886402);
                    case 3:
                        return new lc6<>(2131231229, 2131886406);
                    case 4:
                        return new lc6<>(2131231226, 2131886403);
                    case 5:
                        return new lc6<>(2131231230, 2131886407);
                    case 6:
                        return new lc6<>(2131231227, 2131886408);
                    case 7:
                        return new lc6<>(2131231227, 2131886408);
                }
            }
            throw new kc6();
        }
    }

    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public fi4(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        wg6.b(str, "<set-?>");
        this.mValue = str;
    }
}
