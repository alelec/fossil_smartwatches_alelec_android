package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p00 {
    @DexIgnore
    public Class<?> a;
    @DexIgnore
    public Class<?> b;
    @DexIgnore
    public Class<?> c;

    @DexIgnore
    public p00() {
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        this.a = cls;
        this.b = cls2;
        this.c = cls3;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || p00.class != obj.getClass()) {
            return false;
        }
        p00 p00 = (p00) obj;
        return this.a.equals(p00.a) && this.b.equals(p00.b) && r00.b((Object) this.c, (Object) p00.c);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((this.a.hashCode() * 31) + this.b.hashCode()) * 31;
        Class<?> cls = this.c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    @DexIgnore
    public String toString() {
        return "MultiClassKey{first=" + this.a + ", second=" + this.b + '}';
    }

    @DexIgnore
    public p00(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        a(cls, cls2, cls3);
    }
}
