package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qc1 extends xg6 implements hg6<l50, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qc1(ArrayList arrayList) {
        super(1);
        this.a = arrayList;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        boolean z;
        Object obj2;
        ic0 b;
        boolean z2;
        l50 l50 = (l50) obj;
        Iterator it = this.a.iterator();
        while (true) {
            z = false;
            if (!it.hasNext()) {
                obj2 = null;
                break;
            }
            obj2 = it.next();
            x50 x50 = (x50) obj2;
            if (x50.getPositionConfig().getDistanceFromCenter() == l50.getPositionConfig().getDistanceFromCenter() && x50.getPositionConfig().getAngle() == l50.getPositionConfig().getAngle()) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                break;
            }
        }
        x50 x502 = (x50) obj2;
        if (!(x502 == null || (b = x502.b()) == null)) {
            z = b.b();
        }
        return Boolean.valueOf(z);
    }
}
