package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dd3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qc3 a;
    @DexIgnore
    public /* final */ /* synthetic */ cd3 b;

    @DexIgnore
    public dd3(cd3 cd3, qc3 qc3) {
        this.b = cd3;
        this.a = qc3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onComplete(this.a);
            }
        }
    }
}
