package com.fossil;

import android.content.Intent;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w22 extends g12 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ Fragment b;
    @DexIgnore
    public /* final */ /* synthetic */ int c;

    @DexIgnore
    public w22(Intent intent, Fragment fragment, int i) {
        this.a = intent;
        this.b = fragment;
        this.c = i;
    }

    @DexIgnore
    public final void a() {
        Intent intent = this.a;
        if (intent != null) {
            this.b.startActivityForResult(intent, this.c);
        }
    }
}
