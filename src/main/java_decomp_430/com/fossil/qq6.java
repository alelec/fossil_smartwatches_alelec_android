package com.fossil;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qq6 extends RequestBody {
    @DexIgnore
    public static /* final */ uq6 c; // = uq6.a("application/x-www-form-urlencoded");
    @DexIgnore
    public /* final */ List<String> a;
    @DexIgnore
    public /* final */ List<String> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ Charset c;

        @DexIgnore
        public a() {
            this((Charset) null);
        }

        @DexIgnore
        public a a(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(tq6.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                this.b.add(tq6.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a b(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("name == null");
            } else if (str2 != null) {
                this.a.add(tq6.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                this.b.add(tq6.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true, this.c));
                return this;
            } else {
                throw new NullPointerException("value == null");
            }
        }

        @DexIgnore
        public a(Charset charset) {
            this.a = new ArrayList();
            this.b = new ArrayList();
            this.c = charset;
        }

        @DexIgnore
        public qq6 a() {
            return new qq6(this.a, this.b);
        }
    }

    @DexIgnore
    public qq6(List<String> list, List<String> list2) {
        this.a = fr6.a(list);
        this.b = fr6.a(list2);
    }

    @DexIgnore
    public long a() {
        return a((kt6) null, true);
    }

    @DexIgnore
    public uq6 b() {
        return c;
    }

    @DexIgnore
    public void a(kt6 kt6) throws IOException {
        a(kt6, false);
    }

    @DexIgnore
    public final long a(kt6 kt6, boolean z) {
        jt6 jt6;
        if (z) {
            jt6 = new jt6();
        } else {
            jt6 = kt6.a();
        }
        int size = this.a.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                jt6.writeByte(38);
            }
            jt6.a(this.a.get(i));
            jt6.writeByte(61);
            jt6.a(this.b.get(i));
        }
        if (!z) {
            return 0;
        }
        long p = jt6.p();
        jt6.k();
        return p;
    }
}
