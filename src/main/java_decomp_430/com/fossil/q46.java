package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ o46 d;

    @DexIgnore
    public q46(o46 o46, List list, boolean z, boolean z2) {
        this.d = o46;
        this.a = list;
        this.b = z;
        this.c = z2;
    }

    @DexIgnore
    public void run() {
        this.d.a((List<y46>) this.a, this.b);
        if (this.c) {
            this.a.clear();
        }
    }
}
