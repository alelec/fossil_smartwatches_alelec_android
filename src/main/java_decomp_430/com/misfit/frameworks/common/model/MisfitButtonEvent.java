package com.misfit.frameworks.common.model;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MisfitButtonEvent implements Parcelable {
    SINGLE_PRESS(19),
    DOUBLE_PRESS(20),
    TRIPLE_PRESS(21),
    LONG_PRESS(12),
    DOUBLE_PRESS_AND_HOLD(22),
    UNKNOWN(0);
    
    @DexIgnore
    public static /* final */ Parcelable.Creator<MisfitButtonEvent> CREATOR; // = null;
    @DexIgnore
    public int id;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<MisfitButtonEvent> {
        @DexIgnore
        public MisfitButtonEvent createFromParcel(Parcel parcel) {
            return MisfitButtonEvent.getValue(parcel.readInt());
        }

        @DexIgnore
        public MisfitButtonEvent[] newArray(int i) {
            return new MisfitButtonEvent[i];
        }
    }

    /*
    static {
        CREATOR = new Anon1();
    }
    */

    @DexIgnore
    public MisfitButtonEvent(int i) {
        this.id = i;
    }

    @DexIgnore
    public static MisfitButtonEvent getValue(int i) {
        for (MisfitButtonEvent misfitButtonEvent : values()) {
            if (misfitButtonEvent.equalsTo(i)) {
                return misfitButtonEvent;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equalsTo(int i) {
        return this.id == i;
    }

    @DexIgnore
    public int getID() {
        return this.id;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
    }
}
