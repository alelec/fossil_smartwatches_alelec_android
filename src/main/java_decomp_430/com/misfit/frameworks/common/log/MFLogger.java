package com.misfit.frameworks.common.log;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFLogger {
    @DexIgnore
    public static /* final */ String TAG; // = "MFLogger";
    @DexIgnore
    public static MFFileHelper fileHelper;
    @DexIgnore
    public static boolean isDebuggable;
    @DexIgnore
    public static boolean isInitialized;
    @DexIgnore
    public static String tagPrefix;

    @DexIgnore
    public static void d(String str, String str2) {
        if (isDebuggable && isInitialized) {
            Log.d(tagPrefix + str, str2);
            MFFileHelper mFFileHelper = fileHelper;
            mFFileHelper.d(tagPrefix + str, str2);
        }
    }

    @DexIgnore
    public static void e(String str, String str2) {
        if (isDebuggable && isInitialized) {
            Log.e(tagPrefix + str, str2);
            MFFileHelper mFFileHelper = fileHelper;
            mFFileHelper.e(tagPrefix + str, str2);
        }
    }

    @DexIgnore
    public static List<File> exportLogFiles() {
        return fileHelper.exportLogs();
    }

    @DexIgnore
    public static void i(String str, String str2) {
        if (isDebuggable && isInitialized) {
            Log.i(tagPrefix + str, str2);
            MFFileHelper mFFileHelper = fileHelper;
            mFFileHelper.i(tagPrefix + str, str2);
        }
    }

    @DexIgnore
    public static void initialize(Context context) {
        initialize(context, (String) null);
    }

    @DexIgnore
    public static void v(String str, String str2) {
        if (isDebuggable && isInitialized) {
            Log.v(tagPrefix + str, str2);
            MFFileHelper mFFileHelper = fileHelper;
            mFFileHelper.d(tagPrefix + str, str2);
        }
    }

    @DexIgnore
    public static void initialize(Context context, String str) {
        String str2;
        fileHelper = MFFileHelper.getInstance();
        boolean z = true;
        isInitialized = true;
        if (TextUtils.isEmpty(str)) {
            str2 = "";
        } else {
            str2 = str + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        tagPrefix = str2;
        if ((context.getApplicationInfo().flags & 2) == 0) {
            z = false;
        }
        isDebuggable = z;
        Log.d(TAG, "Inside " + TAG + ".initialize - debuggable=" + isDebuggable);
        fileHelper.init(context);
    }
}
