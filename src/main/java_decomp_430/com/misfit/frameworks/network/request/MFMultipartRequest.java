package com.misfit.frameworks.network.request;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.kv6;
import com.fossil.mv6;
import com.fossil.ov6;
import com.fossil.pv6;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.HTTPMethod;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.configuration.MFHeader;
import com.misfit.frameworks.network.manager.MFNetwork;
import com.misfit.frameworks.network.responses.MFResponse;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MFMultipartRequest extends MFBaseRequest {
    @DexIgnore
    public static /* final */ String TAG; // = "MFMultipartRequest";
    @DexIgnore
    public kv6 multipartEntity;

    @DexIgnore
    public MFMultipartRequest(Context context) {
        super(context);
    }

    @DexIgnore
    private void appendFormDataToEntity(String str, kv6 kv6) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                kv6.a(next, (mv6) new pv6(jSONObject.get(next).toString()));
            }
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.d(str2, "Exception when appenFormDataToEntity " + e);
        }
    }

    @DexIgnore
    public void buildHeader(MFHeader mFHeader) {
        HashMap<String, String> headerMap;
        if (mFHeader != null && (headerMap = this.configuration.getHeader().getHeaderMap()) != null) {
            for (String next : headerMap.keySet()) {
                String str = headerMap.get(next);
                if (!TextUtils.isEmpty(str)) {
                    this.httpURLConnection.setRequestProperty(next, str);
                }
            }
        }
    }

    @DexIgnore
    public void buildRequest() {
        this.configuration = initConfiguration();
        this.jsonData = initJsonData();
        this.method = initHttpMethod();
        this.apiMethod = initApiMethod();
        this.buttonApiResponse = initResponse();
        JSONObject initUploadFileUrl = initUploadFileUrl();
        Uri parse = Uri.parse(this.configuration.getBaseServerUrl() + this.apiMethod);
        kv6 kv6 = new kv6();
        try {
            if (this.jsonData != null) {
                if (this.jsonData instanceof JSONObject) {
                    JSONObject jSONObject = (JSONObject) this.jsonData;
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        appendFormDataToEntity(jSONObject.get(keys.next()).toString(), kv6);
                    }
                } else {
                    throw new Exception("For MultipartRequest, jsonData must be an object");
                }
            }
            if (initUploadFileUrl != null) {
                Iterator<String> keys2 = initUploadFileUrl.keys();
                while (keys2.hasNext()) {
                    String next = keys2.next();
                    kv6.a(next, (mv6) new ov6(new File(initUploadFileUrl.get(next).toString()), "image/jpeg"));
                }
            }
            this.url = new URL(parse.toString());
            this.httpURLConnection = (HttpURLConnection) this.url.openConnection();
            this.httpURLConnection.setDoOutput(true);
            this.httpURLConnection.setRequestMethod("POST");
            buildHeader(this.configuration.getHeader());
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error when build request multipart " + e);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x010b A[Catch:{ Exception -> 0x01a2, all -> 0x01a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0117 A[Catch:{ Exception -> 0x01a2, all -> 0x01a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x016f A[Catch:{ Exception -> 0x01a2, all -> 0x01a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0199  */
    public MFResponse execute() {
        BufferedInputStream bufferedInputStream;
        String readStream;
        HttpURLConnection httpURLConnection;
        InputStream inputStream = null;
        try {
            buildRequest();
            this.buttonApiResponse.setRequestId(this.requestId);
            if (this.httpURLConnection == null) {
                MFLogger.d(TAG, "execute - httpUriRequest == null");
                this.buttonApiResponse.setHttpReturnCode(MFNetworkReturnCode.REQUEST_NOT_FOUND);
                MFResponse mFResponse = this.buttonApiResponse;
                HttpURLConnection httpURLConnection2 = this.httpURLConnection;
                if (httpURLConnection2 != null) {
                    httpURLConnection2.disconnect();
                }
                return mFResponse;
            }
            if (MFNetwork.isDebug()) {
                for (Map.Entry entry : this.httpURLConnection.getRequestProperties().entrySet()) {
                    List list = (List) entry.getValue();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Header value--");
                    sb.append((String) entry.getKey());
                    sb.append(":");
                    sb.append((list == null || list.isEmpty()) ? "" : (String) list.get(0));
                    MFLogger.d(str, sb.toString());
                }
                String str2 = TAG;
                MFLogger.d(str2, "Inside MFBaseRequest.doInBackground - sending REQUEST: command=, requestId=" + this.requestId + "\nurl=" + this.httpURLConnection.getURL().toString() + "\njsonData =");
                if (this.jsonData != null) {
                    String str3 = TAG;
                    MFLogger.d(str3, "Inside MFBaseRequest.doInBackground - jsondata : " + this.jsonData.toString());
                }
            }
            this.httpURLConnection.setUseCaches(false);
            if (this.method != HTTPMethod.POST) {
                if (this.method != HTTPMethod.PATCH) {
                    this.httpURLConnection.connect();
                    if (this.httpURLConnection.getResponseCode() != 200) {
                        bufferedInputStream = new BufferedInputStream(this.httpURLConnection.getInputStream());
                    } else {
                        bufferedInputStream = new BufferedInputStream(this.httpURLConnection.getErrorStream());
                    }
                    BufferedInputStream bufferedInputStream2 = bufferedInputStream;
                    readStream = readStream(bufferedInputStream2);
                    String str4 = TAG;
                    MFLogger.d(str4, "Inside MFBaseRequest.Worker.doInBackground requestId " + this.buttonApiResponse.getRequestId() + "- RESPONSE {httpStatus=" + this.httpURLConnection.getResponseCode() + ", jsonData=" + readStream + "}");
                    this.buttonApiResponse.setHttpReturnCode(this.httpURLConnection.getResponseCode());
                    if (!TextUtils.isEmpty(readStream)) {
                        Object nextValue = new JSONTokener(readStream).nextValue();
                        if (nextValue instanceof JSONObject) {
                            this.buttonApiResponse.parse(new JSONObject(readStream));
                        } else if (nextValue instanceof JSONArray) {
                            this.buttonApiResponse.parse(new JSONArray(readStream));
                        }
                    }
                    httpURLConnection = this.httpURLConnection;
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    bufferedInputStream2.close();
                    return this.buttonApiResponse;
                }
            }
            DataOutputStream dataOutputStream = new DataOutputStream(this.httpURLConnection.getOutputStream());
            this.multipartEntity.writeTo(dataOutputStream);
            dataOutputStream.flush();
            dataOutputStream.close();
            if (this.httpURLConnection.getResponseCode() != 200) {
            }
            BufferedInputStream bufferedInputStream22 = bufferedInputStream;
            readStream = readStream(bufferedInputStream22);
            String str42 = TAG;
            MFLogger.d(str42, "Inside MFBaseRequest.Worker.doInBackground requestId " + this.buttonApiResponse.getRequestId() + "- RESPONSE {httpStatus=" + this.httpURLConnection.getResponseCode() + ", jsonData=" + readStream + "}");
            this.buttonApiResponse.setHttpReturnCode(this.httpURLConnection.getResponseCode());
            if (!TextUtils.isEmpty(readStream)) {
            }
            httpURLConnection = this.httpURLConnection;
            if (httpURLConnection != null) {
            }
            try {
                bufferedInputStream22.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.buttonApiResponse;
        } catch (Exception e2) {
            String str5 = TAG;
            MFLogger.e(str5, "Error inside MFBaseRequest.Worker.doInBackground - e=" + e2);
            this.exception = e2;
            HttpURLConnection httpURLConnection3 = this.httpURLConnection;
            if (httpURLConnection3 != null) {
                httpURLConnection3.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Throwable th) {
            HttpURLConnection httpURLConnection4 = this.httpURLConnection;
            if (httpURLConnection4 != null) {
                httpURLConnection4.disconnect();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            throw th;
        }
    }

    @DexIgnore
    public abstract JSONObject initUploadFileUrl();
}
