package com.misfit.frameworks.network.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MFServer {
    LINK,
    SHINE,
    HOME,
    CUCUMBER
}
