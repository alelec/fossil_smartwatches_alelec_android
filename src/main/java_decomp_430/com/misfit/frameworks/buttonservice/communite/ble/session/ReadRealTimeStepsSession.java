package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.nh6;
import com.fossil.o60;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadRealTimeStepsSession extends EnableMaintainingSession {
    @DexIgnore
    public long mRealTimeSteps; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadRealTimeStepsState extends BleStateAbs {
        @DexIgnore
        public zb0<HashMap<s60, r60>> task;

        @DexIgnore
        public ReadRealTimeStepsState() {
            super(ReadRealTimeStepsSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<s60, r60> hashMap) {
            ReadRealTimeStepsSession readRealTimeStepsSession = ReadRealTimeStepsSession.this;
            nh6 nh6 = nh6.a;
            Object[] objArr = {hashMap.get(s60.TIME), hashMap.get(s60.BATTERY), hashMap.get(s60.BIOMETRIC_PROFILE), hashMap.get(s60.DAILY_STEP), hashMap.get(s60.DAILY_STEP_GOAL), hashMap.get(s60.DAILY_CALORIE), hashMap.get(s60.DAILY_CALORIE_GOAL), hashMap.get(s60.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(s60.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(s60.DAILY_DISTANCE), hashMap.get(s60.INACTIVE_NUDGE), hashMap.get(s60.VIBE_STRENGTH), hashMap.get(s60.DO_NOT_DISTURB_SCHEDULE)};
            String format = String.format("Get configuration  " + ReadRealTimeStepsSession.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            readRealTimeStepsSession.log(format);
        }

        @DexIgnore
        public boolean onEnter() {
            ReadRealTimeStepsSession.this.log("Read Real Time Steps");
            this.task = ReadRealTimeStepsSession.this.getBleAdapter().getDeviceConfig(ReadRealTimeStepsSession.this.getLogSession(), this);
            if (this.task == null) {
                ReadRealTimeStepsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            ReadRealTimeStepsSession.this.stop(FailureCode.FAILED_TO_GET_REAL_TIME_STEP);
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
            wg6.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(s60.DAILY_STEP)) {
                ReadRealTimeStepsSession readRealTimeStepsSession = ReadRealTimeStepsSession.this;
                o60 o60 = hashMap.get(s60.DAILY_STEP);
                if (o60 != null) {
                    readRealTimeStepsSession.mRealTimeSteps = o60.getStep();
                    ReadRealTimeStepsSession readRealTimeStepsSession2 = ReadRealTimeStepsSession.this;
                    readRealTimeStepsSession2.log("Read Real Time Steps Success, value=" + ReadRealTimeStepsSession.this.mRealTimeSteps);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                }
            } else {
                ReadRealTimeStepsSession.this.log("Read Real Time Steps Success, but no value.");
            }
            ReadRealTimeStepsSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<HashMap<s60, r60>> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadRealTimeStepsSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putLong(Constants.DAILY_STEPS, this.mRealTimeSteps);
    }

    @DexIgnore
    public BleSession copyObject() {
        ReadRealTimeStepsSession readRealTimeStepsSession = new ReadRealTimeStepsSession(getBleAdapter(), getBleSessionCallback());
        readRealTimeStepsSession.setDevice(getDevice());
        return readRealTimeStepsSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE;
        String name = ReadRealTimeStepsState.class.getName();
        wg6.a((Object) name, "ReadRealTimeStepsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
