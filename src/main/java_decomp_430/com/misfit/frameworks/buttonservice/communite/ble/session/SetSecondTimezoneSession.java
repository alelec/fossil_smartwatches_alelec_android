package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.SettingsUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetSecondTimezoneSession extends QuickResponseSession {
    @DexIgnore
    public String mOldSecondTimezoneId;
    @DexIgnore
    public String mSecondTimezoneId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetAutoSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        public DoneSetAutoSecondTimezoneState() {
            super(SetSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetSecondTimezoneSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetSecondTimezoneState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetSecondTimezoneState() {
            super(SetSecondTimezoneSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareConfigData() {
            int timezoneRawOffsetById = ConversionUtils.INSTANCE.getTimezoneRawOffsetById(SetSecondTimezoneSession.this.mSecondTimezoneId);
            b70 b70 = new b70();
            short s = (short) timezoneRawOffsetById;
            if (SettingsUtils.INSTANCE.isSecondTimezoneInRange(s)) {
                b70.a(s);
            } else {
                SetSecondTimezoneSession setSecondTimezoneSession = SetSecondTimezoneSession.this;
                setSecondTimezoneSession.log("Set Device Config: Timezone is out of range: " + timezoneRawOffsetById);
            }
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetSecondTimezoneSession.this.getBleAdapter().setDeviceConfig(SetSecondTimezoneSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                SetSecondTimezoneSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SetSecondTimezoneSession.this.stop(FailureCode.FAILED_TO_SET_SECOND_TIMEZONE);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoSecondTimezone(SetSecondTimezoneSession.this.getBleAdapter().getContext(), SetSecondTimezoneSession.this.mSecondTimezoneId);
            DevicePreferenceUtils.removeSettingFlag(SetSecondTimezoneSession.this.getBleAdapter().getContext(), DeviceSettings.SECOND_TIMEZONE);
            SetSecondTimezoneSession setSecondTimezoneSession = SetSecondTimezoneSession.this;
            setSecondTimezoneSession.enterStateAsync(setSecondTimezoneSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetSecondTimezoneSession(String str, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_SECOND_TIMEZONE, bleAdapterImpl, bleSessionCallback);
        wg6.b(str, "mSecondTimezoneId");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mSecondTimezoneId = str;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_SECOND_TIMEZONE) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetSecondTimezoneSession setSecondTimezoneSession = new SetSecondTimezoneSession(this.mSecondTimezoneId, getBleAdapter(), getBleSessionCallback());
        setSecondTimezoneSession.setDevice(getDevice());
        return setSecondTimezoneSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE);
    }

    @DexIgnore
    public void initSettings() {
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(getContext());
        wg6.a((Object) autoSecondTimezoneId, "DevicePreferenceUtils.ge\u2026SecondTimezoneId(context)");
        this.mOldSecondTimezoneId = autoSecondTimezoneId;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_SECOND_TIMEZONE_STATE;
        String name = SetSecondTimezoneState.class.getName();
        wg6.a((Object) name, "SetSecondTimezoneState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetAutoSecondTimezoneState.class.getName();
        wg6.a((Object) name2, "DoneSetAutoSecondTimezoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wg6.b(objArr, "params");
        initSettings();
        String str = this.mOldSecondTimezoneId;
        if (str == null) {
            wg6.d("mOldSecondTimezoneId");
            throw null;
        } else if (!wg6.a((Object) str, (Object) this.mSecondTimezoneId)) {
            return super.onStart(Arrays.copyOf(objArr, objArr.length));
        } else {
            log("The second timezone ids are the same. No need to set again");
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            return true;
        }
    }
}
