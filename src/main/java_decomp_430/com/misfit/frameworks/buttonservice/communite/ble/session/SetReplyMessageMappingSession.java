package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.wg6;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetReplyMessageMappingSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ ReplyMessageMappingGroup mReplyMessageMappingList;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetReplyMessageMappingState extends BleStateAbs {
        @DexIgnore
        public yb0<cd6> task;

        @DexIgnore
        public SetReplyMessageMappingState() {
            super(SetReplyMessageMappingSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetReplyMessageMappingSession.this.getBleAdapter().setReplyMessageMapping(SetReplyMessageMappingSession.this.getLogSession(), SetReplyMessageMappingSession.this.mReplyMessageMappingList, this);
            if (this.task == null) {
                SetReplyMessageMappingSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetReplyMessageMappingError(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetReplyMessageMappingSession.this.getContext(), SetReplyMessageMappingSession.this.getSerial())) {
                SetReplyMessageMappingSession.this.log("Reach the limit retry. Stop.");
                SetReplyMessageMappingSession.this.stop(FailureCode.FAILED_TO_REPLY_MESSAGE_MAPPING);
            }
        }

        @DexIgnore
        public void onSetReplyMessageMappingSuccess() {
            stopTimeout();
            SetReplyMessageMappingSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            yb0<cd6> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetReplyMessageMappingSession(ReplyMessageMappingGroup replyMessageMappingGroup, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_REPLY_MESSAGE_MAPPING, bleAdapterImpl, bleSessionCallback);
        wg6.b(replyMessageMappingGroup, "mReplyMessageMappingList");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mReplyMessageMappingList = replyMessageMappingGroup;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_REPLY_MESSAGE_MAPPING) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetReplyMessageMappingSession setReplyMessageMappingSession = new SetReplyMessageMappingSession(this.mReplyMessageMappingList, getBleAdapter(), getBleSessionCallback());
        setReplyMessageMappingSession.setDevice(getDevice());
        return setReplyMessageMappingSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE;
        String name = SetReplyMessageMappingState.class.getName();
        wg6.a((Object) name, "SetReplyMessageMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
