package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.nh6;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isUIAction;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public zb0<r40> task;

        @DexIgnore
        public FetchDeviceInfoState() {
            super(ConnectDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = ConnectDeviceSession.this.getBleAdapter().fetchDeviceInfo(ConnectDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                ConnectDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onFetchDeviceInfoFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            q40 deviceObj = ConnectDeviceSession.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != q40.c.CONNECTED) {
                ConnectDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(ConnectDeviceSession.this.getBleAdapter().getContext(), ConnectDeviceSession.this.getSerial())) {
                ConnectDeviceSession.this.log("Reach the limit retry. Stop.");
                ConnectDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        public void onFetchDeviceInfoSuccess(r40 r40) {
            wg6.b(r40, "deviceInformation");
            stopTimeout();
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            connectDeviceSession.log("onFetchDeviceInfoSuccess fwVersion " + r40.getFirmwareVersion());
            FLogger.INSTANCE.updateActiveDeviceInfo(new ActiveDeviceInfo(r40.getSerialNumber(), r40.getModelNumber(), r40.getFirmwareVersion()));
            ConnectDeviceSession connectDeviceSession2 = ConnectDeviceSession.this;
            connectDeviceSession2.enterStateAsync(connectDeviceSession2.createConcreteState(BleSessionAbs.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<r40> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public zb0<HashMap<s60, r60>> task;

        @DexIgnore
        public GetDeviceConfigState() {
            super(ConnectDeviceSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<s60, r60> hashMap) {
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            nh6 nh6 = nh6.a;
            Object[] objArr = {hashMap.get(s60.TIME), hashMap.get(s60.BATTERY), hashMap.get(s60.BIOMETRIC_PROFILE), hashMap.get(s60.DAILY_STEP), hashMap.get(s60.DAILY_STEP_GOAL), hashMap.get(s60.DAILY_CALORIE), hashMap.get(s60.DAILY_CALORIE_GOAL), hashMap.get(s60.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(s60.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(s60.DAILY_DISTANCE), hashMap.get(s60.INACTIVE_NUDGE), hashMap.get(s60.VIBE_STRENGTH), hashMap.get(s60.DO_NOT_DISTURB_SCHEDULE)};
            String format = String.format("Get configuration  " + ConnectDeviceSession.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            connectDeviceSession.log(format);
        }

        @DexIgnore
        public boolean onEnter() {
            this.task = ConnectDeviceSession.this.getBleAdapter().getDeviceConfig(ConnectDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
                connectDeviceSession.enterStateAsync(connectDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            ConnectDeviceSession.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            connectDeviceSession.enterStateAsync(connectDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
            wg6.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            connectDeviceSession.enterStateAsync(connectDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<HashMap<s60, r60>> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        public /* final */ /* synthetic */ ConnectDeviceSession this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public TransferSettingsSubFlow(ConnectDeviceSession connectDeviceSession) {
            super(connectDeviceSession.getTAG(), connectDeviceSession, connectDeviceSession.getMfLog(), connectDeviceSession.getLogSession(), false, connectDeviceSession.getSerial(), connectDeviceSession.getBleAdapter(), (UserProfile) null, connectDeviceSession.multiAlarmSettings, connectDeviceSession.complicationAppMappingSettings, connectDeviceSession.watchAppMappingSettings, connectDeviceSession.backgroundConfig, connectDeviceSession.notificationFilterSettings, connectDeviceSession.localizationData, connectDeviceSession.microAppMappings, connectDeviceSession.secondTimezoneOffset, connectDeviceSession.getBleSessionCallback());
            this.this$0 = connectDeviceSession;
        }

        @DexIgnore
        public void onStop(int i) {
            this.this$0.stop(i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectDeviceSession(boolean z, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(!z ? SessionType.CONNECT : SessionType.UI, !z ? CommunicateMode.RECONNECT : CommunicateMode.FORCE_CONNECT, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.isUIAction = z;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public BleSession copyObject() {
        ConnectDeviceSession connectDeviceSession = new ConnectDeviceSession(this.isUIAction, getBleAdapter(), getBleSessionCallback());
        connectDeviceSession.setDevice(getDevice());
        return connectDeviceSession;
    }

    @DexIgnore
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        wg6.a((Object) name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        wg6.a((Object) name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name3 = TransferSettingsSubFlow.class.getName();
        wg6.a((Object) name3, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }

    @DexIgnore
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        wg6.b(str, "serial");
        wg6.b(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }
}
