package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.fitness.FitnessData;
import com.fossil.h60;
import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2 extends xg6 implements hg6<FitnessData[], cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ h60 $biometricProfile$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, h60 h60, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$biometricProfile$inlined = h60;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((FitnessData[]) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(FitnessData[] fitnessDataArr) {
        wg6.b(fitnessDataArr, "it");
        this.this$0.log(this.$logSession$inlined, "Read Data Files Success");
        this.$callback$inlined.onReadDataFilesSuccess(fitnessDataArr);
    }
}
