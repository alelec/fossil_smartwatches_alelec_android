package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$sendCustomCommand$Anon1 extends xg6 implements hg6<cd6, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ CustomRequest $command;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$sendCustomCommand$Anon1(BleAdapterImpl bleAdapterImpl, CustomRequest customRequest) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$command = customRequest;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((cd6) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(cd6 cd6) {
        wg6.b(cd6, "it");
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = FLogger.Session.OTHER;
        bleAdapterImpl.log(session, "Send Custom Command Success: " + this.$command);
    }
}
