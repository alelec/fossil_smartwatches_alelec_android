package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.bc0;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.wg6;
import com.fossil.xj6;
import com.fossil.yb0;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseOTASubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ byte[] firmwareBytes;
    @DexIgnore
    public /* final */ FirmwareData firmwareData;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public zb0<r40> mFetchDeviceInfoTask;

        @DexIgnore
        public FetchDeviceInfoState() {
            super(BaseOTASubFlow.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (xj6.a(BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion())) {
                this.mFetchDeviceInfoTask = BaseOTASubFlow.this.getBleAdapter().fetchDeviceInfo(BaseOTASubFlow.this.getLogSession(), this);
                if (this.mFetchDeviceInfoTask == null) {
                    BaseOTASubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
            baseOTASubFlow.enterSubStateAsync(baseOTASubFlow.createConcreteState(SubFlow.SessionState.OTA_STATE));
            return true;
        }

        @DexIgnore
        public void onFetchDeviceInfoFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            q40 deviceObj = BaseOTASubFlow.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != q40.c.CONNECTED) {
                BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(BaseOTASubFlow.this.getBleAdapter().getContext(), BaseOTASubFlow.this.getSerial())) {
                BaseOTASubFlow.this.getBleSession().log("Reach the limit retry. Stop.");
                BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        public void onFetchDeviceInfoSuccess(r40 r40) {
            wg6.b(r40, "deviceInformation");
            stopTimeout();
            BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
            baseOTASubFlow.enterSubStateAsync(baseOTASubFlow.createConcreteState(SubFlow.SessionState.OTA_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<r40> zb0 = this.mFetchDeviceInfoTask;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTAState extends BleStateAbs {
        @DexIgnore
        public yb0<String> mOTATask;
        @DexIgnore
        public float previousProgress;

        @DexIgnore
        public OTAState() {
            super(BaseOTASubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        public void onDataTransferCompleted() {
            if (isExist()) {
                this.mOTATask = null;
                stopTimeout();
                BaseOTASubFlow.this.stopSubFlow(0);
            }
        }

        @DexIgnore
        public void onDataTransferFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            if (isExist()) {
                this.mOTATask = null;
                stopTimeout();
                if (!retry(BaseOTASubFlow.this.getBleAdapter().getContext(), BaseOTASubFlow.this.getSerial())) {
                    BaseOTASubFlow.this.log("Reach the limit retry. Stop.");
                    BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_OTA);
                }
            }
        }

        @DexIgnore
        public void onDataTransferProgressChange(float f) {
            if (isExist()) {
                float f2 = f * ((float) 100);
                if (f2 - this.previousProgress >= ((float) 1) || f2 >= 100.0f) {
                    this.previousProgress = f2;
                    setTimeout(f2 >= 100.0f ? 60000 : 30000);
                    startTimeout();
                }
                BleCommunicator.CommunicationResultCallback communicationResultCallback = BaseOTASubFlow.this.getCommunicationResultCallback();
                if (communicationResultCallback != null) {
                    communicationResultCallback.onOtaProgressUpdated(BaseOTASubFlow.this.getSerial(), f2);
                }
            }
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (xj6.b(BaseOTASubFlow.this.getFirmwareData().getFirmwareVersion(), BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion(), true)) {
                BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
                baseOTASubFlow.log("Do OTA: Old fw and New fw are same. " + BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion());
                BaseOTASubFlow.this.stopSubFlow(0);
            } else {
                this.mOTATask = BaseOTASubFlow.this.getBleAdapter().doOTA(BaseOTASubFlow.this.getLogSession(), BaseOTASubFlow.this.getFirmwareBytes(), this);
                if (this.mOTATask == null) {
                    BaseOTASubFlow.this.stopSubFlow(10000);
                } else {
                    startTimeout();
                    this.previousProgress = 0.0f;
                }
            }
            return true;
        }

        @DexIgnore
        public void onExit() {
            super.onExit();
            yb0<String> yb0 = this.mOTATask;
            if (yb0 != null) {
                yb0.e();
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            yb0<String> yb0 = this.mOTATask;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseOTASubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, FirmwareData firmwareData2, byte[] bArr, BleSession.BleSessionCallback bleSessionCallback2, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        wg6.b(str, "tagName");
        wg6.b(bleSession, "bleSession");
        wg6.b(session, "logSession");
        wg6.b(str2, "serial");
        wg6.b(bleAdapterImpl, "bleAdapterV2");
        wg6.b(firmwareData2, "firmwareData");
        wg6.b(bArr, "firmwareBytes");
        this.mflog = mFLog;
        this.firmwareData = firmwareData2;
        this.firmwareBytes = bArr;
        this.bleSessionCallback = bleSessionCallback2;
        this.communicationResultCallback = communicationResultCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public final byte[] getFirmwareBytes() {
        return this.firmwareBytes;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        wg6.a((Object) name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.OTA_STATE;
        String name2 = OTAState.class.getName();
        wg6.a((Object) name2, "OTAState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.FETCH_DEVICE_INFO_STATE));
        return true;
    }
}
