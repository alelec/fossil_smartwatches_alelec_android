package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalibrationDeviceSession extends EnableMaintainingSession implements ICalibrationSession {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ApplyHandState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public ApplyHandState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        public void onApplyHandPositionFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            CalibrationDeviceSession.this.stop(FailureCode.FAILED_TO_APPLY_HAND_POSITION);
        }

        @DexIgnore
        public void onApplyHandPositionSuccess() {
            stopTimeout();
            BleSession.BleSessionCallback access$getBleSessionCallback$p = CalibrationDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p != null) {
                Bundle bundle = Bundle.EMPTY;
                wg6.a((Object) bundle, "Bundle.EMPTY");
                access$getBleSessionCallback$p.onBleStateResult(0, bundle);
            }
            CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
            calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState(BleSessionAbs.SessionState.RELEASE_HAND_CONTROL_STATE));
        }

        @DexIgnore
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.APPLY_HAND_POSITION);
            this.task = CalibrationDeviceSession.this.getBleAdapter().calibrationApplyHandPosition(CalibrationDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                CalibrationDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class MoveHandState extends BleStateAbs {
        @DexIgnore
        public boolean isSending;
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public MoveHandState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final boolean isSending() {
            return this.isSending;
        }

        @DexIgnore
        public final boolean moveHand(HandCalibrationObj handCalibrationObj) {
            wg6.b(handCalibrationObj, "handCalibrationObj");
            this.task = CalibrationDeviceSession.this.getBleAdapter().calibrationMoveHand(CalibrationDeviceSession.this.getLogSession(), handCalibrationObj, this);
            if (this.task == null) {
                CalibrationDeviceSession.this.stop(10000);
            } else {
                this.isSending = true;
                startTimeout();
            }
            return true;
        }

        @DexIgnore
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.MOVE_HAND);
            return true;
        }

        @DexIgnore
        public void onMoveHandFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            this.isSending = false;
            CalibrationDeviceSession.this.addFailureCode(FailureCode.FAILED_TO_MOVE_HAND);
        }

        @DexIgnore
        public void onMoveHandSuccess() {
            stopTimeout();
            this.isSending = false;
            BleSession.BleSessionCallback access$getBleSessionCallback$p = CalibrationDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p != null) {
                Bundle bundle = Bundle.EMPTY;
                wg6.a((Object) bundle, "Bundle.EMPTY");
                access$getBleSessionCallback$p.onBleStateResult(0, bundle);
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }

        @DexIgnore
        public final void setSending(boolean z) {
            this.isSending = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReleaseHandControlState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public ReleaseHandControlState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.EXIT_CALIBRATION);
            this.task = CalibrationDeviceSession.this.getBleAdapter().calibrationReleaseHandControl(CalibrationDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                CalibrationDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onReleaseHandControlFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            CalibrationDeviceSession.this.stop(FailureCode.FAILED_TO_RELEASE_HAND_CONTROL);
        }

        @DexIgnore
        public void onReleaseHandControlSuccess() {
            stopTimeout();
            CalibrationDeviceSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class RequestHandControlState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public RequestHandControlState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.ENTER_CALIBRATION);
            this.task = CalibrationDeviceSession.this.getBleAdapter().calibrationRequestHandControl(CalibrationDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                CalibrationDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onRequestHandControlFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            CalibrationDeviceSession.this.stop(FailureCode.FAILED_TO_REQUEST_HAND_CONTROL);
        }

        @DexIgnore
        public void onRequestHandControlSuccess() {
            stopTimeout();
            BleSession.BleSessionCallback access$getBleSessionCallback$p = CalibrationDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p != null) {
                Bundle bundle = Bundle.EMPTY;
                wg6.a((Object) bundle, "Bundle.EMPTY");
                access$getBleSessionCallback$p.onBleStateResult(0, bundle);
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ResetHandsState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public ResetHandsState() {
            super(CalibrationDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            CalibrationDeviceSession.this.setCommunicateMode(CommunicateMode.RESET_HAND);
            this.task = CalibrationDeviceSession.this.getBleAdapter().calibrationResetHandToZeroDegree(CalibrationDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
                calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState(BleSessionAbs.SessionState.MOVE_HAND_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onResetHandsFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            CalibrationDeviceSession.this.addFailureCode(FailureCode.FAILED_TO_RESET_HAND_CONTROL);
            CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
            calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState(BleSessionAbs.SessionState.MOVE_HAND_STATE));
        }

        @DexIgnore
        public void onResetHandsSuccess() {
            stopTimeout();
            BleSession.BleSessionCallback access$getBleSessionCallback$p = CalibrationDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p != null) {
                Bundle bundle = Bundle.EMPTY;
                wg6.a((Object) bundle, "Bundle.EMPTY");
                access$getBleSessionCallback$p.onBleStateResult(0, bundle);
            }
            CalibrationDeviceSession calibrationDeviceSession = CalibrationDeviceSession.this;
            calibrationDeviceSession.enterStateAsync(calibrationDeviceSession.createConcreteState(BleSessionAbs.SessionState.MOVE_HAND_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CalibrationDeviceSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.ENTER_CALIBRATION, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        CalibrationDeviceSession calibrationDeviceSession = new CalibrationDeviceSession(getBleAdapter(), getBleSessionCallback());
        calibrationDeviceSession.setDevice(getDevice());
        return calibrationDeviceSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.REQUEST_HAND_CONTROL_STATE);
    }

    @DexIgnore
    public boolean handleApplyHandsPosition() {
        enterStateAsync(createConcreteState(BleSessionAbs.SessionState.APPLY_HAND_STATE));
        return true;
    }

    @DexIgnore
    public boolean handleMoveHandRequest(HandCalibrationObj handCalibrationObj) {
        wg6.b(handCalibrationObj, "handCalibrationObj");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".handleMoveHandRequest() - currentState=" + getCurrentState());
        if (BleState.Companion.isNull(getCurrentState())) {
            onStart(new Object[0]);
            return true;
        } else if (getCurrentState() instanceof MoveHandState) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                MoveHandState moveHandState = (MoveHandState) currentState;
                if (moveHandState.isSending()) {
                    return true;
                }
                moveHandState.moveHand(handCalibrationObj);
                return true;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.CalibrationDeviceSession.MoveHandState");
        } else {
            FLogger.INSTANCE.getLocal().e(getTAG(), ".handleMoveHandRequest() - currentState is not move hand state");
            return true;
        }
    }

    @DexIgnore
    public boolean handleReleaseHandControl() {
        return enterStateAsync(createConcreteState(BleSessionAbs.SessionState.RELEASE_HAND_CONTROL_STATE));
    }

    @DexIgnore
    public boolean handleResetHandsPosition() {
        enterStateAsync(createConcreteState(BleSessionAbs.SessionState.RESET_HANDS_STATE));
        return true;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.REQUEST_HAND_CONTROL_STATE;
        String name = RequestHandControlState.class.getName();
        wg6.a((Object) name, "RequestHandControlState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.RESET_HANDS_STATE;
        String name2 = ResetHandsState.class.getName();
        wg6.a((Object) name2, "ResetHandsState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.MOVE_HAND_STATE;
        String name3 = MoveHandState.class.getName();
        wg6.a((Object) name3, "MoveHandState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.APPLY_HAND_STATE;
        String name4 = ApplyHandState.class.getName();
        wg6.a((Object) name4, "ApplyHandState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.RELEASE_HAND_CONTROL_STATE;
        String name5 = ReleaseHandControlState.class.getName();
        wg6.a((Object) name5, "ReleaseHandControlState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }
}
