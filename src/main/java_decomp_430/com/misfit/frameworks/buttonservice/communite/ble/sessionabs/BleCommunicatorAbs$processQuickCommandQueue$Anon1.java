package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs$processQuickCommandQueue$1", f = "BleCommunicatorAbs.kt", l = {}, m = "invokeSuspend")
public final class BleCommunicatorAbs$processQuickCommandQueue$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleCommunicatorAbs$processQuickCommandQueue$Anon1(BleCommunicatorAbs bleCommunicatorAbs, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        BleCommunicatorAbs$processQuickCommandQueue$Anon1 bleCommunicatorAbs$processQuickCommandQueue$Anon1 = new BleCommunicatorAbs$processQuickCommandQueue$Anon1(this.this$0, xe6);
        bleCommunicatorAbs$processQuickCommandQueue$Anon1.p$ = (il6) obj;
        return bleCommunicatorAbs$processQuickCommandQueue$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BleCommunicatorAbs$processQuickCommandQueue$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.processQuickCommandQueue();
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
