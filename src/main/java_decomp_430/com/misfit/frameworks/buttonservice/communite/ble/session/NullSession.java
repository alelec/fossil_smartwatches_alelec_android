package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.content.Context;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NullSession extends BleSessionAbs implements BleSession.INullSession {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NullSession(Context context) {
        super(CommunicateMode.IDLE, new BleAdapterImpl(context, "", ""), (BleSession.BleSessionCallback) null);
        wg6.b(context, "context");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        FLogger.INSTANCE.getLocal().e(getTAG(), ".buildExtraInfoReturned(), NullSessionV2 does nothing.");
    }

    @DexIgnore
    public BleSession copyObject() {
        return new NullSession(getContext());
    }

    @DexIgnore
    public void initStateMap() {
        FLogger.INSTANCE.getLocal().e(getTAG(), ".initStateMap(), NullSessionV2 does nothing.");
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wg6.b(objArr, "params");
        FLogger.INSTANCE.getLocal().e(getTAG(), ".onStart(), NullSessionV2 does nothing.");
        return false;
    }
}
