package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.wg6;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetNotificationFiltersConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ AppNotificationFilterSettings mNotificationFilterSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilters extends BleStateAbs {
        @DexIgnore
        public yb0<cd6> task;

        @DexIgnore
        public SetNotificationFilters() {
            super(SetNotificationFiltersConfigSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetNotificationFiltersConfigSession.this.getBleAdapter().setNotificationFilters(SetNotificationFiltersConfigSession.this.getLogSession(), SetNotificationFiltersConfigSession.this.mNotificationFilterSettings.getNotificationFilters(), this);
            if (this.task == null) {
                SetNotificationFiltersConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetNotificationFilterFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetNotificationFiltersConfigSession.this.getContext(), SetNotificationFiltersConfigSession.this.getSerial())) {
                SetNotificationFiltersConfigSession.this.log("Reach the limit retry. Stop.");
                SetNotificationFiltersConfigSession.this.stop(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            }
        }

        @DexIgnore
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoNotificationFiltersConfig(SetNotificationFiltersConfigSession.this.getBleAdapter().getContext(), SetNotificationFiltersConfigSession.this.getBleAdapter().getSerial(), SetNotificationFiltersConfigSession.this.mNotificationFilterSettings);
            SetNotificationFiltersConfigSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            yb0<cd6> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetNotificationFiltersConfigSession(AppNotificationFilterSettings appNotificationFilterSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_NOTIFICATION_FILTERS, bleAdapterImpl, bleSessionCallback);
        wg6.b(appNotificationFilterSettings, "mNotificationFilterSettings");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNotificationFilterSettings = appNotificationFilterSettings;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetNotificationFiltersConfigSession setNotificationFiltersConfigSession = new SetNotificationFiltersConfigSession(this.mNotificationFilterSettings, getBleAdapter(), getBleSessionCallback());
        setNotificationFiltersConfigSession.setDevice(getDevice());
        return setNotificationFiltersConfigSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name = SetNotificationFilters.class.getName();
        wg6.a((Object) name, "SetNotificationFilters::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
