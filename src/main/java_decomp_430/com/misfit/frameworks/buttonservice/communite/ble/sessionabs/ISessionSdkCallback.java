package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.bc0;
import com.fossil.c90;
import com.fossil.fitness.FitnessData;
import com.fossil.l40;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.r60;
import com.fossil.s60;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ISessionSdkCallback {
    @DexIgnore
    void onApplyHandPositionFailed(bc0 bc0);

    @DexIgnore
    void onApplyHandPositionSuccess();

    @DexIgnore
    void onAuthenticateDeviceFail(bc0 bc0);

    @DexIgnore
    void onAuthenticateDeviceSuccess(byte[] bArr);

    @DexIgnore
    void onAuthorizeDeviceFailed();

    @DexIgnore
    void onAuthorizeDeviceSuccess();

    @DexIgnore
    void onConfigureMicroAppFail(bc0 bc0);

    @DexIgnore
    void onConfigureMicroAppSuccess();

    @DexIgnore
    void onDataTransferCompleted();

    @DexIgnore
    void onDataTransferFailed(bc0 bc0);

    @DexIgnore
    void onDataTransferProgressChange(float f);

    @DexIgnore
    void onDeviceFound(q40 q40, int i);

    @DexIgnore
    void onEraseDataFilesFailed(bc0 bc0);

    @DexIgnore
    void onEraseDataFilesSuccess();

    @DexIgnore
    void onEraseHWLogFailed(bc0 bc0);

    @DexIgnore
    void onEraseHWLogSuccess();

    @DexIgnore
    void onExchangeSecretKeyFail(bc0 bc0);

    @DexIgnore
    void onExchangeSecretKeySuccess(byte[] bArr);

    @DexIgnore
    void onFetchDeviceInfoFailed(bc0 bc0);

    @DexIgnore
    void onFetchDeviceInfoSuccess(r40 r40);

    @DexIgnore
    void onGetDeviceConfigFailed(bc0 bc0);

    @DexIgnore
    void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap);

    @DexIgnore
    void onGetWatchParamsFail();

    @DexIgnore
    void onMoveHandFailed(bc0 bc0);

    @DexIgnore
    void onMoveHandSuccess();

    @DexIgnore
    void onNextSession();

    @DexIgnore
    void onNotifyNotificationEventFailed(bc0 bc0);

    @DexIgnore
    void onNotifyNotificationEventSuccess();

    @DexIgnore
    void onPlayAnimationFail(bc0 bc0);

    @DexIgnore
    void onPlayAnimationSuccess();

    @DexIgnore
    void onPlayDeviceAnimation(boolean z, bc0 bc0);

    @DexIgnore
    void onReadCurrentWorkoutSessionFailed(bc0 bc0);

    @DexIgnore
    void onReadCurrentWorkoutSessionSuccess(c90 c90);

    @DexIgnore
    void onReadDataFilesFailed(bc0 bc0);

    @DexIgnore
    void onReadDataFilesProgressChanged(float f);

    @DexIgnore
    void onReadDataFilesSuccess(FitnessData[] fitnessDataArr);

    @DexIgnore
    void onReadHWLogFailed(bc0 bc0);

    @DexIgnore
    void onReadHWLogProgressChanged(float f);

    @DexIgnore
    void onReadHWLogSuccess();

    @DexIgnore
    void onReadRssiFailed(bc0 bc0);

    @DexIgnore
    void onReadRssiSuccess(int i);

    @DexIgnore
    void onReleaseHandControlFailed(bc0 bc0);

    @DexIgnore
    void onReleaseHandControlSuccess();

    @DexIgnore
    void onRequestHandControlFailed(bc0 bc0);

    @DexIgnore
    void onRequestHandControlSuccess();

    @DexIgnore
    void onResetHandsFailed(bc0 bc0);

    @DexIgnore
    void onResetHandsSuccess();

    @DexIgnore
    void onScanFail(l40 l40);

    @DexIgnore
    void onSendMicroAppDataFail(bc0 bc0);

    @DexIgnore
    void onSendMicroAppDataSuccess();

    @DexIgnore
    void onSetAlarmFailed(bc0 bc0);

    @DexIgnore
    void onSetAlarmSuccess();

    @DexIgnore
    void onSetBackgroundImageFailed(bc0 bc0);

    @DexIgnore
    void onSetBackgroundImageSuccess();

    @DexIgnore
    void onSetComplicationFailed(bc0 bc0);

    @DexIgnore
    void onSetComplicationSuccess();

    @DexIgnore
    void onSetDeviceConfigFailed(bc0 bc0);

    @DexIgnore
    void onSetDeviceConfigSuccess();

    @DexIgnore
    void onSetFrontLightFailed(bc0 bc0);

    @DexIgnore
    void onSetFrontLightSuccess();

    @DexIgnore
    void onSetLocalizationDataFail(bc0 bc0);

    @DexIgnore
    void onSetLocalizationDataSuccess();

    @DexIgnore
    void onSetNotificationFilterFailed(bc0 bc0);

    @DexIgnore
    void onSetNotificationFilterProgressChanged(float f);

    @DexIgnore
    void onSetNotificationFilterSuccess();

    @DexIgnore
    void onSetReplyMessageMappingError(bc0 bc0);

    @DexIgnore
    void onSetReplyMessageMappingSuccess();

    @DexIgnore
    void onSetWatchAppFailed(bc0 bc0);

    @DexIgnore
    void onSetWatchAppSuccess();

    @DexIgnore
    void onSetWatchParamsFail(bc0 bc0);

    @DexIgnore
    void onSetWatchParamsSuccess();

    @DexIgnore
    void onStopCurrentWorkoutSessionFailed(bc0 bc0);

    @DexIgnore
    void onStopCurrentWorkoutSessionSuccess();

    @DexIgnore
    void onVerifySecretKeyFail(bc0 bc0);

    @DexIgnore
    void onVerifySecretKeySuccess(boolean z);
}
