package com.misfit.frameworks.buttonservice.extensions;

import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmExtensionKt$sortMultiAlarmSettingList$comparator$Anon1<T> implements Comparator<AlarmSetting> {
    @DexIgnore
    public static /* final */ AlarmExtensionKt$sortMultiAlarmSettingList$comparator$Anon1 INSTANCE; // = new AlarmExtensionKt$sortMultiAlarmSettingList$comparator$Anon1();

    @DexIgnore
    public final int compare(AlarmSetting alarmSetting, AlarmSetting alarmSetting2) {
        int minute;
        int minute2;
        int i = -1;
        if (alarmSetting == null || alarmSetting2 == null) {
            int i2 = alarmSetting == null ? -1 : 1;
            if (alarmSetting2 != null) {
                i = 1;
            }
            return i2 - i;
        }
        if (alarmSetting.getAlarmDaysAsInt() - alarmSetting2.getAlarmDaysAsInt() != 0) {
            minute = alarmSetting.getAlarmDaysAsInt();
            minute2 = alarmSetting2.getAlarmDaysAsInt();
        } else if (alarmSetting.getHour() - alarmSetting2.getHour() != 0) {
            minute = alarmSetting.getHour();
            minute2 = alarmSetting2.getHour();
        } else if (alarmSetting.getMinute() - alarmSetting2.getMinute() != 0) {
            minute = alarmSetting.getMinute();
            minute2 = alarmSetting2.getMinute();
        } else {
            int i3 = alarmSetting.isRepeat() ? 1 : -1;
            if (alarmSetting2.isRepeat()) {
                i = 1;
            }
            return i3 - i;
        }
        return minute - minute2;
    }
}
