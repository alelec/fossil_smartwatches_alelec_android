package com.misfit.frameworks.buttonservice.ble;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.k40;
import com.fossil.l40;
import com.fossil.o40;
import com.fossil.q40;
import com.fossil.qg6;
import com.fossil.s40;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ScanService implements k40.b {
    @DexIgnore
    public static /* final */ long BLE_SCAN_TIMEOUT; // = 120000;
    @DexIgnore
    public static /* final */ int CONNECT_TIMEOUT; // = 10000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_BOND_RSSI_MARK; // = -999999;
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_RSSI_MARK; // = 0;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public Handler autoStopHandler;
    @DexIgnore
    public /* final */ Runnable autoStopTask; // = new ScanService$autoStopTask$Anon1(this);
    @DexIgnore
    public Callback callback;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public boolean isScanning; // = false;
    @DexIgnore
    public MFLog mfLog;
    @DexIgnore
    public long startScanTimestamp;
    @DexIgnore
    public /* final */ long tagTime;

    @DexIgnore
    public interface Callback {
        @DexIgnore
        void onDeviceFound(q40 q40, int i);

        @DexIgnore
        void onScanFail(l40 l40);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ScanService.class.getSimpleName();
        wg6.a((Object) simpleName, "ScanService::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ScanService(Context context2, Callback callback2, long j) {
        wg6.b(context2, "context");
        this.callback = callback2;
        this.tagTime = j;
        Context applicationContext = context2.getApplicationContext();
        wg6.a((Object) applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    @DexIgnore
    private final String getCollectionTagByActiveLog() {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            String l = Long.toString(this.tagTime);
            wg6.a((Object) l, "java.lang.Long.toString(tagTime)");
            return l;
        } else if (mFLog != null) {
            return String.valueOf(mFLog.getStartTimeEpoch());
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    private final void log(String str) {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            return;
        }
        if (mFLog != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            MFLog mFLog2 = this.mfLog;
            if (mFLog2 != null) {
                sb.append(mFLog2.getSerial());
                sb.append(" - Scanning] ");
                sb.append(str);
                mFLog.log(sb.toString());
                return;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    private final void startAutoStopTimer() {
        stopAutoStopTimer();
        this.autoStopHandler = new Handler(Looper.getMainLooper());
        Handler handler = this.autoStopHandler;
        if (handler != null) {
            handler.postDelayed(this.autoStopTask, BLE_SCAN_TIMEOUT);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    private final void stopAutoStopTimer() {
        Handler handler = this.autoStopHandler;
        if (handler != null) {
            handler.removeCallbacks(this.autoStopTask);
        }
    }

    @DexIgnore
    public final q40 buildDeviceBySerial(String str, String str2) {
        wg6.b(str, "serial");
        wg6.b(str2, "macAddress");
        try {
            return k40.l.a(str, str2);
        } catch (Exception e) {
            log("BuildDeviceBySerrial, error:" + e);
            return null;
        }
    }

    @DexIgnore
    public void onDeviceFound(q40 q40, int i) {
        wg6.b(q40, "device");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".onDeviceFound, serialNumber=" + q40.l().getSerialNumber() + ", fastPairIdHex=" + q40.l().getFastPairIdInHexString() + ", hashcode=" + hashCode());
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onDeviceFound, callback is null");
        } else if (callback2 != null) {
            callback2.onDeviceFound(q40, i);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void onStartScanFailed(l40 l40) {
        wg6.b(l40, "scanError");
        stopScan();
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onStartScanFail, callback is null");
        } else if (callback2 != null) {
            callback2.onScanFail(l40);
        } else {
            wg6.a();
            throw null;
        }
        this.callback = null;
    }

    @DexIgnore
    public final void setActiveDeviceLog(String str) {
        wg6.b(str, "serial");
        this.mfLog = MFLogManager.getInstance(this.context).getActiveLog(str);
    }

    @DexIgnore
    public final synchronized void startScan() {
        FLogger.INSTANCE.getLocal().d(TAG, ".startScan");
        this.isScanning = true;
        k40.l.a(new o40().setDeviceTypes(new s40[]{s40.DIANA, s40.MINI, s40.SLIM, s40.SE1, s40.WEAR_OS}), this);
        log("Called start scan api v2.");
    }

    @DexIgnore
    public final synchronized void startScanWithAutoStopTimer() {
        FLogger.INSTANCE.getLocal().d(TAG, ".startScanWithAutoStopTimer()");
        startAutoStopTimer();
        this.isScanning = true;
        k40.l.a(new o40().setDeviceTypes(new s40[]{s40.DIANA, s40.MINI, s40.SLIM, s40.SE1, s40.WEAR_OS}), this);
        this.startScanTimestamp = System.currentTimeMillis();
    }

    @DexIgnore
    public final synchronized void stopScan() {
        FLogger.INSTANCE.getLocal().d(TAG, ".stopScan");
        this.startScanTimestamp = -1;
        this.isScanning = false;
        stopAutoStopTimer();
        k40.l.b(this);
        log("Called stop scan api v2.");
    }
}
