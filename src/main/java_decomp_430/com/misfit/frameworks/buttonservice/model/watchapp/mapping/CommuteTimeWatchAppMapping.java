package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.de0;
import com.fossil.h80;
import com.fossil.o80;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public List<String> destinations;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(List<String> list) {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME());
        wg6.b(list, "destinations");
        this.destinations = list;
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public o80 toSDKSetting() {
        List<String> list = this.destinations;
        if (list != null) {
            Object[] array = list.toArray(new String[0]);
            if (array != null) {
                return new h80(new de0((String[]) array));
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        wg6.d("destinations");
        throw null;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        List<String> list = this.destinations;
        if (list != null) {
            parcel.writeStringList(list);
        } else {
            wg6.d("destinations");
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        this.destinations = arrayList;
    }
}
