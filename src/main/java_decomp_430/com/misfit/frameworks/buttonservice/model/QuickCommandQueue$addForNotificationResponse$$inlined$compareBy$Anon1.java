package com.misfit.frameworks.buttonservice.model;

import com.fossil.ue6;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickCommandQueue$addForNotificationResponse$$inlined$compareBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    public final int compare(T t, T t2) {
        return ue6.a(Integer.valueOf(((NotificationBaseObj) t).getLifeCountDownObject().getCount()), Integer.valueOf(((NotificationBaseObj) t2).getLifeCountDownObject().getCount()));
    }
}
