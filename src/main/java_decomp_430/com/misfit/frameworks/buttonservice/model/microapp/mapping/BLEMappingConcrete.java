package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLEMappingConcrete extends BLEMapping {
    @DexIgnore
    public Gesture getGesture() {
        return null;
    }

    @DexIgnore
    public String getHash() {
        return this.mType + "";
    }

    @DexIgnore
    public boolean isNeedHID() {
        return false;
    }

    @DexIgnore
    public boolean isNeedStreaming() {
        return false;
    }
}
