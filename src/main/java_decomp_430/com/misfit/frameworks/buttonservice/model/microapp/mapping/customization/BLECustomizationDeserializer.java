package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.util.Log;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.lu3;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLECustomizationDeserializer implements hu3<BLECustomization> {
    @DexIgnore
    public BLECustomization deserialize(JsonElement jsonElement, Type type, gu3 gu3) throws lu3 {
        Log.d(BLECustomizationDeserializer.class.getName(), jsonElement.toString());
        if (jsonElement.d().a("type").b() != 1) {
            return new BLENonCustomization();
        }
        return (BLECustomization) new Gson().a(jsonElement, BLEGoalTrackingCustomization.class);
    }
}
