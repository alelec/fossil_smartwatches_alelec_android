package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.vu3;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.BaseEnumType;
import com.j256.ormlite.support.DatabaseResults;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.enums.Gesture;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "mapping")
public class Mapping implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_ACTION; // = "action";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_FAMILY; // = "deviceFamily";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_ID; // = "deviceId";
    @DexIgnore
    public static /* final */ String COLUMN_EXTRA_INFO; // = "extraInfo";
    @DexIgnore
    public static /* final */ String COLUMN_GESTURE; // = "gesture";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<Mapping> CREATOR; // = new Anon1();
    @DexIgnore
    public boolean isServiceCommand;
    @DexIgnore
    @vu3("action")
    @DatabaseField(columnName = "action")
    public int mAction;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public String mDeviceFamily;
    @DexIgnore
    @vu3("deviceId")
    @DatabaseField(columnName = "deviceId")
    public String mDeviceId;
    @DexIgnore
    @vu3("extraInfo")
    @DatabaseField(columnName = "extraInfo")
    public String mExtraInfo;
    @DexIgnore
    @vu3("gesture")
    @DatabaseField(columnName = "gesture", persisterClass = GestureDataType.class)
    public Gesture mGesture;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String mId;
    @DexIgnore
    @vu3("objectId")
    @DatabaseField(columnName = "objectId")
    public String mObjectId;
    @DexIgnore
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public String mUpdatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Mapping> {
        @DexIgnore
        public Mapping createFromParcel(Parcel parcel) {
            return new Mapping(parcel);
        }

        @DexIgnore
        public Mapping[] newArray(int i) {
            return new Mapping[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class GestureDataType extends BaseEnumType {
        @DexIgnore
        public static /* final */ GestureDataType singleTon; // = new GestureDataType();

        @DexIgnore
        public GestureDataType() {
            super(SqlType.INTEGER);
        }

        @DexIgnore
        public static GestureDataType getSingleton() {
            return singleTon;
        }

        @DexIgnore
        public Class<?> getPrimaryClass() {
            return Integer.TYPE;
        }

        @DexIgnore
        public Object javaToSqlArg(FieldType fieldType, Object obj) {
            return Integer.valueOf(((Gesture) obj).getValue());
        }

        @DexIgnore
        public Object makeConfigObject(FieldType fieldType) throws SQLException {
            HashMap hashMap = new HashMap();
            Gesture[] gestureArr = (Gesture[]) fieldType.getType().getEnumConstants();
            if (gestureArr != null) {
                for (Gesture gesture : gestureArr) {
                    hashMap.put(Integer.valueOf(gesture.getValue()), gesture);
                }
                return hashMap;
            }
            throw new SQLException("Field " + fieldType + " improperly configured as type " + this);
        }

        @DexIgnore
        public Object parseDefaultString(FieldType fieldType, String str) {
            return Integer.valueOf(Integer.parseInt(str));
        }

        @DexIgnore
        public Object resultToSqlArg(FieldType fieldType, DatabaseResults databaseResults, int i) throws SQLException {
            return Integer.valueOf(databaseResults.getInt(i));
        }

        @DexIgnore
        public Object sqlArgToJava(FieldType fieldType, Object obj, int i) throws SQLException {
            Enum enumR;
            if (fieldType == null) {
                return obj;
            }
            Integer num = (Integer) obj;
            Map map = (Map) fieldType.getDataTypeConfigObj();
            if (map == null) {
                enumR = null;
            } else {
                enumR = (Enum) map.get(num);
            }
            return BaseEnumType.enumVal(fieldType, num, enumR, fieldType.getUnknownEnumVal());
        }
    }

    @DexIgnore
    public Mapping() {
        this.mExtraInfo = "";
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof Mapping)) {
            Mapping mapping = (Mapping) obj;
            String deviceFamily = mapping.getDeviceFamily();
            String deviceFamily2 = getDeviceFamily();
            boolean z = (TextUtils.isEmpty(deviceFamily) && TextUtils.isEmpty(deviceFamily2)) || (deviceFamily != null && deviceFamily.equalsIgnoreCase(deviceFamily2));
            if (mapping.getAction() == getAction() && mapping.getGesture() == getGesture() && z) {
                if (mapping.getAction() != 505) {
                    return true;
                }
                if (mapping.getExtraInfo() != null) {
                    return mapping.getExtraInfo().equalsIgnoreCase(getExtraInfo());
                }
                if (getExtraInfo() == null) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    @DexIgnore
    public int getAction() {
        return this.mAction;
    }

    @DexIgnore
    public String getDeviceFamily() {
        return this.mDeviceFamily;
    }

    @DexIgnore
    public String getDeviceId() {
        return this.mDeviceId;
    }

    @DexIgnore
    public String getExtraInfo() {
        return this.mExtraInfo;
    }

    @DexIgnore
    public Gesture getGesture() {
        return this.mGesture;
    }

    @DexIgnore
    public String getId() {
        return this.mDeviceId.concat(this.mGesture.toString());
    }

    @DexIgnore
    public String getObjectId() {
        return this.mObjectId;
    }

    @DexIgnore
    public String getUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public boolean isServiceCommand() {
        return this.isServiceCommand;
    }

    @DexIgnore
    public void setAction(int i) {
        this.mAction = i;
    }

    @DexIgnore
    public void setDeviceFamily(String str) {
        this.mDeviceFamily = str;
    }

    @DexIgnore
    public void setDeviceId(String str) {
        this.mDeviceId = str;
        this.mDeviceFamily = DeviceIdentityUtils.getDeviceFamily(str).name();
    }

    @DexIgnore
    public void setExtraInfo(String str) {
        this.mExtraInfo = str;
    }

    @DexIgnore
    public void setGesture(Gesture gesture) {
        this.mGesture = gesture;
    }

    @DexIgnore
    public void setId(String str) {
        this.mId = str;
    }

    @DexIgnore
    public void setIsServiceCommand(boolean z) {
        this.isServiceCommand = z;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.mObjectId = str;
    }

    @DexIgnore
    public void setUpdatedAt(String str) {
        this.mUpdatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "Serial=" + getDeviceId() + ", deviceFamily=" + getDeviceFamily() + ", action=" + this.mAction + ", gesture=" + this.mGesture + ", extraInfo=" + this.mExtraInfo + "\n";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mDeviceId);
        parcel.writeInt(this.mGesture.getValue());
        parcel.writeInt(this.mAction);
        parcel.writeString(this.mExtraInfo);
        parcel.writeString(this.mUpdatedAt);
        parcel.writeString(this.mObjectId);
        parcel.writeString(this.mId);
        parcel.writeString(this.mDeviceFamily);
        parcel.writeByte(this.isServiceCommand ? (byte) 1 : 0);
    }

    @DexIgnore
    public Mapping clone() {
        Mapping mapping = new Mapping();
        mapping.mDeviceId = this.mDeviceId;
        mapping.mGesture = this.mGesture;
        mapping.mAction = this.mAction;
        mapping.mExtraInfo = this.mExtraInfo;
        mapping.mUpdatedAt = this.mUpdatedAt;
        mapping.mObjectId = this.mObjectId;
        mapping.mDeviceFamily = this.mDeviceFamily;
        mapping.isServiceCommand = this.isServiceCommand;
        mapping.mId = this.mId;
        return mapping;
    }

    @DexIgnore
    public Mapping(Gesture gesture, int i) {
        this.mGesture = gesture;
        this.mAction = i;
    }

    @DexIgnore
    public Mapping(Gesture gesture, int i, String str) {
        this(gesture, i);
        this.mExtraInfo = str;
    }

    @DexIgnore
    public Mapping(Parcel parcel) {
        this.mDeviceId = parcel.readString();
        this.mGesture = Gesture.fromInt(parcel.readInt());
        this.mAction = parcel.readInt();
        this.mExtraInfo = parcel.readString();
        this.mUpdatedAt = parcel.readString();
        this.mObjectId = parcel.readString();
        this.mId = parcel.readString();
        this.mDeviceFamily = parcel.readString();
        this.isServiceCommand = parcel.readByte() != 0;
    }
}
