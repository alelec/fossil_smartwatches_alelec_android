package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ae0;
import com.fossil.be0;
import com.fossil.dc0;
import com.fossil.l50;
import com.fossil.m50;
import com.fossil.qg6;
import com.fossil.tu6;
import com.fossil.wg6;
import com.fossil.xj6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BackgroundConfig implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "BackgroundConfig";
    @DexIgnore
    public BackgroundImgData bottomComplicationBackground;
    @DexIgnore
    public BackgroundImgData leftComplicationBackground;
    @DexIgnore
    public BackgroundImgData mainBackground;
    @DexIgnore
    public BackgroundImgData rightComplicationBackground;
    @DexIgnore
    public /* final */ long timestamp;
    @DexIgnore
    public BackgroundImgData topComplicationBackground;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundConfig> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public BackgroundConfig createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new BackgroundConfig(parcel, (qg6) null);
        }

        @DexIgnore
        public BackgroundConfig[] newArray(int i) {
            return new BackgroundConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundConfig(Parcel parcel, qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    private final String getHash() {
        return this.mainBackground.getHash() + ':' + this.topComplicationBackground.getHash() + ':' + this.rightComplicationBackground.getHash() + ':' + this.bottomComplicationBackground.getHash() + ':' + this.leftComplicationBackground.getHash();
    }

    @DexIgnore
    private final l50 getMainBackgroundImage(String str) {
        String imgData = this.mainBackground.getImgData();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "imageData = " + imgData);
        if (imgData.length() == 0) {
            return new l50(this.mainBackground.getImgName(), new byte[0], (dc0) null, 4, (qg6) null);
        }
        if (!yj6.a((CharSequence) imgData, (CharSequence) str, false, 2, (Object) null) || !yj6.a((CharSequence) imgData, (CharSequence) Constants.PHOTO_BINARY_NAME_SUFFIX, true)) {
            return this.mainBackground.toSDKBackgroundImage();
        }
        try {
            byte[] b = tu6.b(new File(this.mainBackground.getImgData()));
            String imgName = this.mainBackground.getImgName();
            wg6.a((Object) b, "data");
            return new l50(imgName, b, (dc0) null, 4, (qg6) null);
        } catch (Exception e) {
            e.printStackTrace();
            return this.mainBackground.toSDKBackgroundImage();
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BackgroundConfig)) {
            return false;
        }
        return xj6.b(getHash(), ((BackgroundConfig) obj).getHash(), true);
    }

    @DexIgnore
    public final long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final ae0 toSDKBackgroundImageConfig(String str) {
        l50 l50;
        wg6.b(str, "directory");
        if (wg6.a((Object) this.mainBackground.getImgName(), (Object) Constants.MAIN_BACKGROUND_NAME)) {
            l50 = getMainBackgroundImage(str);
        } else {
            l50 = this.mainBackground.toSDKBackgroundImage();
        }
        return new ae0(new be0[]{new m50(l50, this.topComplicationBackground.toSDKBackgroundImage(), this.rightComplicationBackground.toSDKBackgroundImage(), this.bottomComplicationBackground.toSDKBackgroundImage(), this.leftComplicationBackground.toSDKBackgroundImage())});
    }

    @DexIgnore
    public String toString() {
        return "{timestamp: " + this.timestamp + ", " + "mainBackground: " + this.mainBackground + ", " + "topComplicationBackground: " + this.topComplicationBackground + ", " + "rightComplicationBackground: " + this.rightComplicationBackground + ", bottomComplicationBackground: " + this.bottomComplicationBackground + ", " + "leftComplicationBackground: " + this.leftComplicationBackground + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeLong(this.timestamp);
        this.mainBackground.writeToParcel(parcel, i);
        this.topComplicationBackground.writeToParcel(parcel, i);
        this.rightComplicationBackground.writeToParcel(parcel, i);
        this.bottomComplicationBackground.writeToParcel(parcel, i);
        this.leftComplicationBackground.writeToParcel(parcel, i);
    }

    @DexIgnore
    public BackgroundConfig(long j, BackgroundImgData backgroundImgData, BackgroundImgData backgroundImgData2, BackgroundImgData backgroundImgData3, BackgroundImgData backgroundImgData4, BackgroundImgData backgroundImgData5) {
        wg6.b(backgroundImgData, "mainBackground");
        wg6.b(backgroundImgData2, "topComplicationBackground");
        wg6.b(backgroundImgData3, "rightComplicationBackground");
        wg6.b(backgroundImgData4, "bottomComplicationBackground");
        wg6.b(backgroundImgData5, "leftComplicationBackground");
        this.timestamp = j;
        this.mainBackground = backgroundImgData;
        this.topComplicationBackground = backgroundImgData2;
        this.rightComplicationBackground = backgroundImgData3;
        this.bottomComplicationBackground = backgroundImgData4;
        this.leftComplicationBackground = backgroundImgData5;
    }

    @DexIgnore
    public BackgroundConfig(Parcel parcel) {
        this.timestamp = parcel.readLong();
        this.mainBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.topComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.rightComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.bottomComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
        this.leftComplicationBackground = BackgroundImgData.CREATOR.createFromParcel(parcel);
    }
}
