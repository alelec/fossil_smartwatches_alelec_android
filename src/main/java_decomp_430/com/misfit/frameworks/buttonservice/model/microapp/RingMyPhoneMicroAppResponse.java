package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.aa0;
import com.fossil.e90;
import com.fossil.tc0;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.fossil.xc0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingMyPhoneMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public RingMyPhoneMicroAppResponse() {
        super(e90.RING_MY_PHONE_MICRO_APP);
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof aa0) || w40 == null) {
            return null;
        }
        return new xc0((aa0) x90, w40);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
    }
}
