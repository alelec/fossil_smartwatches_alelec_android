package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.e90;
import com.fossil.qg6;
import com.fossil.tc0;
import com.fossil.td0;
import com.fossil.ud0;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MicroAppInstruction implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<MicroAppInstruction> CREATOR; // = new MicroAppInstruction$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public td0 declarationID;
    @DexIgnore
    public ud0 variantID;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Parcelable.Creator<MicroAppInstruction> getCREATOR() {
            return MicroAppInstruction.CREATOR;
        }

        @DexIgnore
        public final void setCREATOR(Parcelable.Creator<MicroAppInstruction> creator) {
            MicroAppInstruction.CREATOR = creator;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum MicroAppID {
        UAPP_HID_MEDIA_CONTROL_MUSIC("music-control", true, false),
        UAPP_HID_MEDIA_VOL_UP_ID("music-volumn-up", true, false),
        UAPP_HID_MEDIA_VOL_DOWN_ID("music-volumn-down", true, false),
        UAPP_ACTIVITY_TAGGING_ID(Constants.ACTIVITY, false, false),
        UAPP_GOAL_TRACKING_ID("goal-tracking", false, false),
        UAPP_DATE_ID(HardwareLog.COLUMN_DATE, false, false),
        UAPP_TIME2_ID("second-time-zone", false, false),
        UAPP_ALERT_ID("alert", false, false),
        UAPP_ALARM_ID(Alarm.TABLE_NAME, false, false),
        UAPP_PROGRESS_ID(Constants.ACTIVITY, false, false),
        UAPP_WEATHER_STANDARD("weather", false, true),
        UAPP_COMMUTE_TIME("commute-time", false, true),
        UAPP_TOGGLE_MODE("sequence", false, false),
        UAPP_RING_PHONE("ring-my-phone", false, true),
        UAPP_SELFIE(Constants.SELFIE, true, false),
        UAPP_STOPWATCH("stopwatch", false, false),
        UAPP_UNKNOWN("unknown", false, false);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ boolean isNeedHID;
        @DexIgnore
        public /* final */ boolean isNeedStreaming;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MicroAppID getMicroAppId(String str) {
                wg6.b(str, ServerSetting.VALUE);
                MicroAppID[] values = MicroAppID.values();
                int length = values.length;
                for (int i = 0; i < length; i++) {
                    if (wg6.a((Object) values[i].getValue(), (Object) str)) {
                        return values[i];
                    }
                }
                return MicroAppID.UAPP_TOGGLE_MODE;
            }

            @DexIgnore
            public final MicroAppID getMicroAppIdFromDeviceEventId(int i) {
                if (i == e90.RING_MY_PHONE_MICRO_APP.ordinal()) {
                    return MicroAppID.UAPP_RING_PHONE;
                }
                if (i == e90.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == e90.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) {
                    return MicroAppID.UAPP_COMMUTE_TIME;
                }
                return MicroAppID.UAPP_UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public MicroAppID(String str, boolean z, boolean z2) {
            this.value = str;
            this.isNeedHID = z;
            this.isNeedStreaming = z2;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final boolean isNeedHID() {
            return this.isNeedHID;
        }

        @DexIgnore
        public final boolean isNeedStreaming() {
            return this.isNeedStreaming;
        }
    }

    @DexIgnore
    public MicroAppInstruction(td0 td0, ud0 ud0) {
        wg6.b(td0, "declarationID");
        wg6.b(ud0, "variantID");
        String name = MicroAppInstruction.class.getName();
        wg6.a((Object) name, "javaClass.name");
        this.className = name;
        this.declarationID = td0;
        this.variantID = ud0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final td0 getDeclarationID() {
        return this.declarationID;
    }

    @DexIgnore
    public abstract tc0 getMicroAppData();

    @DexIgnore
    public final ud0 getVariantID() {
        return this.variantID;
    }

    @DexIgnore
    public final void setDeclarationID(td0 td0) {
        this.declarationID = td0;
    }

    @DexIgnore
    public final void setVariantID(ud0 ud0) {
        this.variantID = ud0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.className);
        td0 td0 = this.declarationID;
        if (td0 != null) {
            parcel.writeInt(td0.ordinal());
            ud0 ud0 = this.variantID;
            if (ud0 != null) {
                parcel.writeInt(ud0.ordinal());
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public MicroAppInstruction(Parcel parcel) {
        wg6.b(parcel, "in");
        String name = MicroAppInstruction.class.getName();
        wg6.a((Object) name, "javaClass.name");
        this.className = name;
        this.declarationID = td0.values()[parcel.readInt()];
        this.variantID = ud0.values()[parcel.readInt()];
    }
}
