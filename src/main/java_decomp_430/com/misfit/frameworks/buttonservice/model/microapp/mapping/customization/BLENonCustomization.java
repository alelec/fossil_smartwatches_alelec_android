package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.pd0;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BLENonCustomization extends BLECustomization {
    @DexIgnore
    public BLENonCustomization() {
        super(0);
    }

    @DexIgnore
    public pd0 getCustomizationFrame() {
        return null;
    }

    @DexIgnore
    public String getHash() {
        return getType() + ":non";
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLENonCustomization(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "in");
    }
}
