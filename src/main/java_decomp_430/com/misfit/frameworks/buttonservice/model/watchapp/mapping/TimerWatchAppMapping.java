package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimerWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public TimerWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getTIMER());
    }

    @DexIgnore
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    public o80 toSDKSetting() {
        return new n80();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimerWatchAppMapping(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
    }
}
