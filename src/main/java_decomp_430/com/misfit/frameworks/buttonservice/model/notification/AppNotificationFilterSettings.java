package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotificationFilterSettings implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public List<AppNotificationFilter> mNotificationFilters;
    @DexIgnore
    public /* final */ long timeStamp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilterSettings> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public final long compareTimeStamp(AppNotificationFilterSettings appNotificationFilterSettings, AppNotificationFilterSettings appNotificationFilterSettings2) {
            long j = 0;
            long timeStamp = appNotificationFilterSettings != null ? appNotificationFilterSettings.getTimeStamp() : 0;
            if (appNotificationFilterSettings2 != null) {
                j = appNotificationFilterSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(AppNotificationFilterSettings appNotificationFilterSettings, AppNotificationFilterSettings appNotificationFilterSettings2) {
            List list;
            List list2;
            if (appNotificationFilterSettings == null || (list = appNotificationFilterSettings.getNotificationFilters()) == null) {
                list = new ArrayList();
            }
            if (appNotificationFilterSettings2 == null || (list2 = appNotificationFilterSettings2.getNotificationFilters()) == null) {
                list2 = new ArrayList();
            }
            return list.size() == list2.size() && list.containsAll(list2);
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public AppNotificationFilterSettings createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new AppNotificationFilterSettings(parcel);
        }

        @DexIgnore
        public AppNotificationFilterSettings[] newArray(int i) {
            return new AppNotificationFilterSettings[i];
        }
    }

    @DexIgnore
    public AppNotificationFilterSettings(List<AppNotificationFilter> list, long j) {
        wg6.b(list, "notificationFilters");
        this.mNotificationFilters = list;
        this.timeStamp = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final List<AppNotificationFilter> getNotificationFilters() {
        return this.mNotificationFilters;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeTypedList(this.mNotificationFilters);
    }

    @DexIgnore
    public AppNotificationFilterSettings(Parcel parcel) {
        wg6.b(parcel, "parcel");
        this.timeStamp = parcel.readLong();
        this.mNotificationFilters = new ArrayList();
        parcel.readTypedList(this.mNotificationFilters, AppNotificationFilter.CREATOR);
    }
}
