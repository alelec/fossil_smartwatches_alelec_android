package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.af6;
import com.fossil.fh;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.oh;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class LogDatabase extends oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "LogDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public abstract LogDao getLogDao();

    @DexIgnore
    public void init(fh fhVar) {
        wg6.b(fhVar, "configuration");
        LogDatabase.super.init(fhVar);
        try {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new LogDatabase$init$Anon1(this, (xe6) null), 3, (Object) null);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when init database " + e);
        }
    }
}
