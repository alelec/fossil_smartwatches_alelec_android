package com.misfit.frameworks.buttonservice.log;

import com.fossil.mj6;
import com.fossil.wg6;
import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$exportLogs$Anon1 implements FileFilter {
    @DexIgnore
    public static /* final */ BufferLogWriter$exportLogs$Anon1 INSTANCE; // = new BufferLogWriter$exportLogs$Anon1();

    @DexIgnore
    public final boolean accept(File file) {
        wg6.a((Object) file, "subFile");
        if (file.isFile()) {
            String name = file.getName();
            wg6.a((Object) name, "subFile.name");
            if (new mj6(BufferLogWriter.ALL_BUFFER_FILE_NAME_REGEX_PATTERN).matches(name)) {
                return true;
            }
        }
        return false;
    }
}
