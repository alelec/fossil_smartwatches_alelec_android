package com.misfit.frameworks.buttonservice.log;

import com.fossil.qg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Failure<T> extends RepoResponse<T> {
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String errorItems;
    @DexIgnore
    public /* final */ ServerError serverError;
    @DexIgnore
    public /* final */ Throwable throwable;

    @DexIgnore
    public Failure(int i, ServerError serverError2, Throwable th, String str) {
        super((qg6) null);
        this.code = i;
        this.serverError = serverError2;
        this.throwable = th;
        this.errorItems = str;
    }

    @DexIgnore
    public static /* synthetic */ Failure copy$default(Failure failure, int i, ServerError serverError2, Throwable th, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = failure.code;
        }
        if ((i2 & 2) != 0) {
            serverError2 = failure.serverError;
        }
        if ((i2 & 4) != 0) {
            th = failure.throwable;
        }
        if ((i2 & 8) != 0) {
            str = failure.errorItems;
        }
        return failure.copy(i, serverError2, th, str);
    }

    @DexIgnore
    public final int component1() {
        return this.code;
    }

    @DexIgnore
    public final ServerError component2() {
        return this.serverError;
    }

    @DexIgnore
    public final Throwable component3() {
        return this.throwable;
    }

    @DexIgnore
    public final String component4() {
        return this.errorItems;
    }

    @DexIgnore
    public final Failure<T> copy(int i, ServerError serverError2, Throwable th, String str) {
        return new Failure<>(i, serverError2, th, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Failure)) {
            return false;
        }
        Failure failure = (Failure) obj;
        return this.code == failure.code && wg6.a((Object) this.serverError, (Object) failure.serverError) && wg6.a((Object) this.throwable, (Object) failure.throwable) && wg6.a((Object) this.errorItems, (Object) failure.errorItems);
    }

    @DexIgnore
    public final int getCode() {
        return this.code;
    }

    @DexIgnore
    public final String getErrorItems() {
        return this.errorItems;
    }

    @DexIgnore
    public final ServerError getServerError() {
        return this.serverError;
    }

    @DexIgnore
    public final Throwable getThrowable() {
        return this.throwable;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.code * 31;
        ServerError serverError2 = this.serverError;
        int i2 = 0;
        int hashCode = (i + (serverError2 != null ? serverError2.hashCode() : 0)) * 31;
        Throwable th = this.throwable;
        int hashCode2 = (hashCode + (th != null ? th.hashCode() : 0)) * 31;
        String str = this.errorItems;
        if (str != null) {
            i2 = str.hashCode();
        }
        return hashCode2 + i2;
    }

    @DexIgnore
    public String toString() {
        return "Failure(code=" + this.code + ", serverError=" + this.serverError + ", throwable=" + this.throwable + ", errorItems=" + this.errorItems + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Failure(int i, ServerError serverError2, Throwable th, String str, int i2, qg6 qg6) {
        this(i, serverError2, (i2 & 4) != 0 ? null : th, (i2 & 8) != 0 ? null : str);
    }
}
