package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ej6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rc6;
import com.fossil.ro3;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xp6;
import com.fossil.yf6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$1", f = "BufferLogWriter.kt", l = {186, 79, 98, 103, 108}, m = "invokeSuspend")
public final class BufferLogWriter$pollEventQueue$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$pollEventQueue$Anon1(BufferLogWriter bufferLogWriter, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bufferLogWriter;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        BufferLogWriter$pollEventQueue$Anon1 bufferLogWriter$pollEventQueue$Anon1 = new BufferLogWriter$pollEventQueue$Anon1(this.this$0, xe6);
        bufferLogWriter$pollEventQueue$Anon1.p$ = (il6) obj;
        return bufferLogWriter$pollEventQueue$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((BufferLogWriter$pollEventQueue$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v36, resolved type: com.fossil.xp6} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0215 A[Catch:{ Exception -> 0x0038 }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x021a A[Catch:{ Exception -> 0x0038 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x019a A[Catch:{ all -> 0x01e9, all -> 0x01ec }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01e1 A[SYNTHETIC, Splitter:B:91:0x01e1] */
    public final Object invokeSuspend(Object obj) {
        xp6 xp6;
        File file;
        il6 il6;
        hh6 hh6;
        LogEvent logEvent;
        boolean z;
        BufferLogWriter bufferLogWriter;
        il6 il62;
        File file2;
        LogEvent logEvent2;
        Throwable th;
        String str;
        il6 il63;
        xp6 xp62;
        File file3;
        hh6 hh62;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il64 = this.p$;
            xp6 access$getMBufferLogMutex$p = this.this$0.mBufferLogMutex;
            this.L$0 = il64;
            this.L$1 = access$getMBufferLogMutex$p;
            this.label = 1;
            if (access$getMBufferLogMutex$p.a((Object) null, this) == a) {
                return a;
            }
            il63 = il64;
            xp6 = access$getMBufferLogMutex$p;
        } else if (i == 1) {
            xp6 = (xp6) this.L$1;
            nc6.a(obj);
            il63 = (il6) this.L$0;
        } else if (i == 2) {
            List list = (List) this.L$4;
            hh62 = (hh6) this.L$3;
            file3 = (File) this.L$2;
            xp62 = (xp6) this.L$1;
            il63 = (il6) this.L$0;
            try {
                nc6.a(obj);
                hh62.element = 0;
                file2 = file3;
                hh6 = hh62;
                xp6 = xp62;
                il62 = il63;
                logEvent2 = (LogEvent) this.this$0.logEventQueue.poll();
                z = logEvent2 instanceof FlushLogEvent;
                if (logEvent2 != null && !(logEvent2 instanceof FinishLogEvent) && !z) {
                    FileOutputStream fileOutputStream = new FileOutputStream(file2, true);
                    try {
                        str = logEvent2.toString() + "\n";
                        Charset charset = ej6.a;
                        if (str != null) {
                            byte[] bytes = str.getBytes(charset);
                            wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                            fileOutputStream.write(bytes);
                            fileOutputStream.flush();
                            int i2 = hh6.element;
                            hh6.element = i2 + 1;
                            hf6.a(i2);
                            yf6.a(fileOutputStream, (Throwable) null);
                            if (logEvent2.getLogLevel() == FLogger.LogLevel.SUMMARY) {
                                dl6 a2 = zl6.a();
                                BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2 bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2 = new BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2(logEvent2, (xe6) null, this);
                                this.L$0 = il62;
                                this.L$1 = xp6;
                                this.L$2 = file2;
                                this.L$3 = hh6;
                                this.L$4 = logEvent2;
                                this.Z$0 = z;
                                this.L$5 = fileOutputStream;
                                this.label = 3;
                                if (gk6.a(a2, bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2, this) == a) {
                                    return a;
                                }
                                logEvent = logEvent2;
                                file = file2;
                                il6 = il62;
                                bufferLogWriter = this.this$0;
                                this.L$0 = il6;
                                this.L$1 = xp6;
                                this.L$2 = file;
                                this.L$3 = hh6;
                                this.L$4 = logEvent;
                                this.label = 4;
                                if (bufferLogWriter.renameToFullBufferFile(file, z, this) == a) {
                                }
                                if (logEvent instanceof FinishLogEvent) {
                                }
                                cd6 cd6 = cd6.a;
                                xp6.a((Object) null);
                                return cd6.a;
                            }
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        yf6.a(fileOutputStream, th);
                        throw th3;
                    }
                }
                file = file2;
                il6 = il62;
                logEvent = logEvent2;
                bufferLogWriter = this.this$0;
                this.L$0 = il6;
                this.L$1 = xp6;
                this.L$2 = file;
                this.L$3 = hh6;
                this.L$4 = logEvent;
                this.label = 4;
                if (bufferLogWriter.renameToFullBufferFile(file, z, this) == a) {
                }
                if (logEvent instanceof FinishLogEvent) {
                }
            } catch (Exception e) {
                e = e;
                xp6 = xp62;
            } catch (Throwable th4) {
                th = th4;
                xp6 = xp62;
                xp6.a((Object) null);
                throw th;
            }
            cd6 cd62 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } else if (i == 3) {
            FileOutputStream fileOutputStream2 = (FileOutputStream) this.L$5;
            boolean z2 = this.Z$0;
            logEvent = (LogEvent) this.L$4;
            hh6 = (hh6) this.L$3;
            file2 = (File) this.L$2;
            xp6 xp63 = (xp6) this.L$1;
            il62 = (il6) this.L$0;
            try {
                nc6.a(obj);
                z = z2;
                xp6 = xp63;
                file = file2;
                il6 = il62;
                if (hh6.element >= this.this$0.threshold || z) {
                    bufferLogWriter = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = xp6;
                    this.L$2 = file;
                    this.L$3 = hh6;
                    this.L$4 = logEvent;
                    this.label = 4;
                    if (bufferLogWriter.renameToFullBufferFile(file, z, this) == a) {
                        return a;
                    }
                }
                if (logEvent instanceof FinishLogEvent) {
                }
            } catch (Exception e2) {
                e = e2;
                xp6 = xp63;
                System.out.print(e.toString());
                cd6 cd622 = cd6.a;
                xp6.a((Object) null);
                return cd6.a;
            } catch (Throwable th5) {
                th = th5;
                xp6 = xp63;
                xp6.a((Object) null);
                throw th;
            }
            cd6 cd6222 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } else if (i == 4) {
            LogEvent logEvent3 = (LogEvent) this.L$4;
            hh6 hh63 = (hh6) this.L$3;
            File file4 = (File) this.L$2;
            xp6 xp64 = (xp6) this.L$1;
            il6 = (il6) this.L$0;
            try {
                nc6.a(obj);
                file = file4;
                logEvent = logEvent3;
                xp6 = xp64;
                hh6 = hh63;
                if (logEvent instanceof FinishLogEvent) {
                    dl6 a3 = zl6.a();
                    BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 = new BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2((xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = xp6;
                    this.L$2 = file;
                    this.L$3 = hh6;
                    this.L$4 = logEvent;
                    this.label = 5;
                    if (gk6.a(a3, bufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2, this) == a) {
                        return a;
                    }
                }
            } catch (Exception e3) {
                e = e3;
                xp6 = xp64;
                System.out.print(e.toString());
                cd6 cd62222 = cd6.a;
                xp6.a((Object) null);
                return cd6.a;
            } catch (Throwable th6) {
                th = th6;
                xp6 = xp64;
                xp6.a((Object) null);
                throw th;
            }
            cd6 cd622222 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } else if (i == 5) {
            LogEvent logEvent4 = (LogEvent) this.L$4;
            hh6 hh64 = (hh6) this.L$3;
            File file5 = (File) this.L$2;
            xp6 = this.L$1;
            il6 il65 = (il6) this.L$0;
            nc6.a(obj);
            cd6 cd6222222 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        try {
            if (!xj6.a(this.this$0.logFilePath)) {
                File file6 = new File(this.this$0.logFilePath);
                hh6 hh65 = new hh6();
                File parentFile = file6.getParentFile();
                Boolean a4 = parentFile != null ? hf6.a(parentFile.exists()) : null;
                if (a4 != null) {
                    if (!a4.booleanValue()) {
                        File parentFile2 = file6.getParentFile();
                        if (parentFile2 != null) {
                            parentFile2.mkdirs();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    try {
                        if (!file6.exists()) {
                            file6.createNewFile();
                            hh65.element = 0;
                        } else {
                            List b = ro3.b(file6, Charset.defaultCharset());
                            wg6.a((Object) b, "logLines");
                            hh65.element = b.size();
                            if (hh65.element >= this.this$0.threshold) {
                                BufferLogWriter bufferLogWriter2 = this.this$0;
                                this.L$0 = il63;
                                this.L$1 = xp6;
                                this.L$2 = file6;
                                this.L$3 = hh65;
                                this.L$4 = b;
                                this.label = 2;
                                if (bufferLogWriter2.renameToFullBufferFile(file6, false, this) == a) {
                                    return a;
                                }
                                file3 = file6;
                                xp62 = xp6;
                                hh62 = hh65;
                                hh62.element = 0;
                                file2 = file3;
                                hh6 = hh62;
                                xp6 = xp62;
                                il62 = il63;
                                logEvent2 = (LogEvent) this.this$0.logEventQueue.poll();
                                z = logEvent2 instanceof FlushLogEvent;
                                FileOutputStream fileOutputStream3 = new FileOutputStream(file2, true);
                                str = logEvent2.toString() + "\n";
                                Charset charset2 = ej6.a;
                                if (str != null) {
                                }
                            }
                        }
                        file2 = file6;
                        il62 = il63;
                        hh6 = hh65;
                        logEvent2 = (LogEvent) this.this$0.logEventQueue.poll();
                        z = logEvent2 instanceof FlushLogEvent;
                        FileOutputStream fileOutputStream32 = new FileOutputStream(file2, true);
                        str = logEvent2.toString() + "\n";
                        Charset charset22 = ej6.a;
                        if (str != null) {
                        }
                    } catch (Exception e4) {
                        e = e4;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            cd6 cd62222222 = cd6.a;
            xp6.a((Object) null);
            return cd6.a;
        } catch (Throwable th7) {
            th = th7;
            xp6.a((Object) null);
            throw th;
        }
    }
}
