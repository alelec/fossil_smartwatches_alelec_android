package com.misfit.frameworks.buttonservice.log.model;

import com.fossil.vu3;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoveDeviceLog {
    @DexIgnore
    @vu3("current_device")
    public String currentDevice;

    @DexIgnore
    public RemoveDeviceLog(String str) {
        wg6.b(str, "currentDevice");
        this.currentDevice = str;
    }

    @DexIgnore
    public final String getCurrentDevice() {
        return this.currentDevice;
    }

    @DexIgnore
    public final void setCurrentDevice(String str) {
        wg6.b(str, "<set-?>");
        this.currentDevice = str;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }
}
