package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.logger.Logger;
import com.j256.ormlite.logger.LoggerFactory;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.SQLException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    @DexIgnore
    public /* final */ Logger LOG; // = LoggerFactory.getLogger((Class<?>) DatabaseHelper.class);
    @DexIgnore
    public /* final */ String TAG; // = DatabaseHelper.class.getCanonicalName();
    @DexIgnore
    public Map<Integer, UpgradeCommand> availableUpgrades;
    @DexIgnore
    public String dbName;
    @DexIgnore
    public Class<?>[] entities;

    @DexIgnore
    public DatabaseHelper(Context context, String str, int i, Class<?>[] clsArr, Map<Integer, UpgradeCommand> map) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, i);
        this.dbName = str;
        this.entities = clsArr;
        this.availableUpgrades = map;
    }

    @DexIgnore
    private void copyFile(File file, File file2) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        FileChannel channel = fileInputStream.getChannel();
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        FileChannel channel2 = fileOutputStream.getChannel();
        try {
            channel.transferTo(0, channel.size(), channel2);
        } finally {
            if (channel != null) {
                channel.close();
            }
            fileInputStream.close();
            if (channel2 != null) {
                channel2.close();
            }
            fileOutputStream.close();
        }
    }

    @DexIgnore
    private void createAllTables() throws SQLException {
        createTables(this.entities);
    }

    @DexIgnore
    private void dropAllTables() throws SQLException {
        dropTables(this.entities);
    }

    @DexIgnore
    public void close() {
        DatabaseHelper.super.close();
    }

    @DexIgnore
    public void createTable(Class<?> cls) throws SQLException {
        TableUtils.createTable(getConnectionSource(), cls);
    }

    @DexIgnore
    public void createTables(Class<?>[] clsArr) throws SQLException {
        for (Class<?> createTable : clsArr) {
            TableUtils.createTable(getConnectionSource(), createTable);
        }
    }

    @DexIgnore
    public void dropTable(Class<?> cls) throws SQLException {
        dropTable(getConnectionSource(), cls);
    }

    @DexIgnore
    public void dropTables(Class<?>[] clsArr) throws SQLException {
        for (Class<?> dropTable : clsArr) {
            dropTable(getConnectionSource(), dropTable);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.database.sqlite.SQLiteOpenHelper, com.misfit.frameworks.buttonservice.db.DatabaseHelper] */
    public void exportDB(Context context) {
        FLogger.INSTANCE.getLocal().d(this.TAG, "exporting DB to file");
        File databasePath = context.getDatabasePath(getDatabaseName());
        File file = new File(Environment.getExternalStorageDirectory(), "nrml");
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(file, databasePath.getName());
        try {
            file2.createNewFile();
            copyFile(databasePath, file2);
        } catch (IOException e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, "Unable to Export DB: " + e.getMessage());
        }
    }

    @DexIgnore
    public void onCreate(SQLiteDatabase sQLiteDatabase, ConnectionSource connectionSource) {
        try {
            createAllTables();
        } catch (SQLException e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, "Can't create database - e=" + e);
            throw new RuntimeException(e);
        }
    }

    @DexIgnore
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.i(str, "onUpgrade, oldVersion=" + i + " newVersion=" + i2);
        try {
            upgrade(sQLiteDatabase, i, i2);
            FLogger.INSTANCE.getLocal().i(this.TAG, "successful upgrade!");
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.e(str2, "Can't migrate databases, bootstrap database, data will be lost - e=" + e);
            try {
                dropAllTables();
                onCreate(sQLiteDatabase, connectionSource);
            } catch (SQLException e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = this.TAG;
                local3.e(str3, "Can't drop databases - e=" + e2);
                throw new RuntimeException(e);
            }
        }
    }

    @DexIgnore
    public void upgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (this.availableUpgrades != null) {
            while (true) {
                i++;
                if (i <= i2) {
                    UpgradeCommand upgradeCommand = this.availableUpgrades.get(Integer.valueOf(i));
                    if (upgradeCommand != null) {
                        sQLiteDatabase.beginTransaction();
                        try {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str = this.TAG;
                            local.d(str, "Upgrading database to: " + i);
                            upgradeCommand.execute(sQLiteDatabase);
                            sQLiteDatabase.setTransactionSuccessful();
                        } finally {
                            sQLiteDatabase.endTransaction();
                        }
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new RuntimeException("No upgrade commands provided");
        }
    }

    @DexIgnore
    private void dropTable(ConnectionSource connectionSource, Class<?> cls) {
        try {
            TableUtils.dropTable(connectionSource, cls, true);
        } catch (SQLException unused) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, "Could not drop table: " + cls.getSimpleName());
        }
    }
}
