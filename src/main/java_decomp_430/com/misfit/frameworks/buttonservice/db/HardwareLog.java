package com.misfit.frameworks.buttonservice.db;

import com.j256.ormlite.field.DatabaseField;
import java.io.Serializable;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HardwareLog implements Serializable {
    @DexIgnore
    public static /* final */ String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_LOG; // = "log";
    @DexIgnore
    public static /* final */ String COLUMN_READ; // = "read";
    @DexIgnore
    public static /* final */ String COLUMN_SERIAL; // = "serial";
    @DexIgnore
    @DatabaseField(columnName = "date")
    public Date date;
    @DexIgnore
    @DatabaseField(columnName = "id", generatedId = true)
    public long id;
    @DexIgnore
    @DatabaseField(columnName = "log")
    public String log;
    @DexIgnore
    @DatabaseField(columnName = "read")
    public boolean read;
    @DexIgnore
    @DatabaseField(columnName = "serial")
    public String serial;

    @DexIgnore
    public Date getDate() {
        return this.date;
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public String getLog() {
        return this.log;
    }

    @DexIgnore
    public String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public boolean isRead() {
        return this.read;
    }

    @DexIgnore
    public void setDate(Date date2) {
        this.date = date2;
    }

    @DexIgnore
    public void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public void setLog(String str) {
        this.log = str;
    }

    @DexIgnore
    public void setRead(boolean z) {
        this.read = z;
    }

    @DexIgnore
    public void setSerial(String str) {
        this.serial = str;
    }
}
