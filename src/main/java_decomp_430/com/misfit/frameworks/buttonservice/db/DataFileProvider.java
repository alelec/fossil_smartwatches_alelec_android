package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataFileProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ String DB_NAME; // = "data_file.db";
    @DexIgnore
    public static DataFileProvider sInstance;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE datafile ADD COLUMN syncTime BIGINT");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
        }
    }

    @DexIgnore
    public DataFileProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataFile, String> getDataFileDao() throws SQLException {
        return this.databaseHelper.getDao(DataFile.class);
    }

    @DexIgnore
    public static synchronized DataFileProvider getInstance(Context context) {
        DataFileProvider dataFileProvider;
        synchronized (DataFileProvider.class) {
            if (sInstance == null) {
                sInstance = new DataFileProvider(context, DB_NAME);
            }
            dataFileProvider = sInstance;
        }
        return dataFileProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.i(str, "Inside " + this.TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.e(str2, "Error inside " + this.TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public int deleteDataFiles(String str) {
        try {
            DeleteBuilder<DataFile, String> deleteBuilder = getDataFileDao().deleteBuilder();
            deleteBuilder.where().eq("serial", str);
            return deleteBuilder.delete();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Error inside " + this.TAG + ".deleteDataFile - e=" + e);
            return 0;
        }
    }

    @DexIgnore
    public List<DataFile> getAllDataFiles(long j, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            String str2 = "SELECT key FROM dataFile WHERE serial = '" + str + "' AND " + DataFile.COLUMN_SYNC_TIME + " <= " + j;
            GenericRawResults<String[]> queryRaw = getDataFileDao().queryRaw(str2, new String[0]);
            FLogger.INSTANCE.getLocal().d(this.TAG, str2);
            ArrayList arrayList2 = new ArrayList();
            for (String[] next : queryRaw) {
                if (next.length > 0) {
                    arrayList2.add(next[0]);
                }
            }
            queryRaw.close();
            if (arrayList2.size() > 0) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    arrayList.add(getDataFile((String) it.next()));
                }
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e(this.TAG, "Error inside " + this.TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [android.database.sqlite.SQLiteOpenHelper, com.misfit.frameworks.buttonservice.db.DatabaseHelper] */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x012a, code lost:
        if (r3 != null) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x012c, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x015a, code lost:
        if (r3 != null) goto L_0x012c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x015d, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0160  */
    public DataFile getDataFile(String str, String str2) {
        DataFile dataFile;
        long j;
        String str3;
        int i;
        String str4 = str;
        SQLiteDatabase readableDatabase = this.databaseHelper.getReadableDatabase();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT length(dataFile), serial, syncTime FROM dataFile WHERE `key` = ?");
        sb.append(str2 == null ? "" : " AND serial = ?");
        Cursor rawQuery = readableDatabase.rawQuery(sb.toString(), str2 == null ? new String[]{str4} : new String[]{str4, str2});
        try {
            QueryBuilder<DataFile, String> queryBuilder = getDataFileDao().queryBuilder();
            queryBuilder.where().eq("key", str4);
            if (rawQuery == null || rawQuery.getCount() <= 0) {
                str3 = "";
                i = 0;
                j = 0;
            } else {
                rawQuery.moveToFirst();
                i = rawQuery.getInt(0);
                str3 = rawQuery.getString(1);
                j = rawQuery.getLong(2);
                rawQuery.close();
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str5 = this.TAG;
            local.d(str5, "Inside .getDataFile - fileData len=" + i);
            if (i > 1000000) {
                StringBuffer stringBuffer = new StringBuffer();
                Cursor cursor = rawQuery;
                long j2 = 0;
                while (j2 < ((long) i)) {
                    long j3 = j2 + 1;
                    try {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("SELECT substr(dataFile, ?, ? ) FROM dataFile WHERE `key` = ?");
                        sb2.append(str2 == null ? "" : " AND serial = ?");
                        cursor = readableDatabase.rawQuery(sb2.toString(), str2 == null ? new String[]{Long.toString(j3), Long.toString(1000000), str4} : new String[]{Long.toString(j3), Long.toString(1000000), str4, str2});
                        if (cursor != null && cursor.getCount() > 0) {
                            cursor.moveToFirst();
                            String string = cursor.getString(0);
                            stringBuffer.append(string);
                            j2 += (long) string.length();
                        }
                    } catch (Exception e) {
                        e = e;
                        rawQuery = cursor;
                        dataFile = null;
                        try {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str6 = this.TAG;
                            local2.e(str6, "Error inside " + this.TAG + ".getDataFile - e=" + e);
                        } catch (Throwable th) {
                            th = th;
                            if (rawQuery != null) {
                                rawQuery.close();
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        rawQuery = cursor;
                        if (rawQuery != null) {
                        }
                        throw th;
                    }
                }
                rawQuery = cursor;
                dataFile = new DataFile(str, stringBuffer.toString(), str3, j);
            } else {
                dataFile = getDataFileDao().queryForFirst(queryBuilder.prepare());
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Exception e2) {
                    e = e2;
                }
            }
        } catch (Exception e3) {
            e = e3;
            dataFile = null;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str62 = this.TAG;
            local22.e(str62, "Error inside " + this.TAG + ".getDataFile - e=" + e);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{DataFile.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public void saveDataFile(DataFile dataFile) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                local2.d(str2, "Inside " + this.TAG + ".saveDataFile - Save dataFile=" + dataFile);
                getDataFileDao().create(dataFile);
                return;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local3.e(str3, "Error inside " + this.TAG + ".saveDataFile -e=Data file existed");
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = this.TAG;
            local4.e(str4, "Error inside " + this.TAG + ".saveDataFile - e=" + e);
        }
    }

    @DexIgnore
    public DataFile getDataFile(String str) {
        return getDataFile(str, (String) null);
    }
}
