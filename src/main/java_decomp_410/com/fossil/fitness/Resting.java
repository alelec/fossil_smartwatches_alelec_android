package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Resting implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Resting> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mStartTime;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ byte mValue;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Resting> {
        @DexIgnore
        public Resting createFromParcel(Parcel parcel) {
            return new Resting(parcel);
        }

        @DexIgnore
        public Resting[] newArray(int i) {
            return new Resting[i];
        }
    }

    @DexIgnore
    public Resting(int i, int i2, byte b) {
        this.mStartTime = i;
        this.mTimezoneOffsetInSecond = i2;
        this.mValue = b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Resting)) {
            return false;
        }
        Resting resting = (Resting) obj;
        if (this.mStartTime == resting.mStartTime && this.mTimezoneOffsetInSecond == resting.mTimezoneOffsetInSecond && this.mValue == resting.mValue) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public byte getValue() {
        return this.mValue;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mStartTime) * 31) + this.mTimezoneOffsetInSecond) * 31) + this.mValue;
    }

    @DexIgnore
    public String toString() {
        return "Resting{mStartTime=" + this.mStartTime + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mValue=" + this.mValue + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeByte(this.mValue);
    }

    @DexIgnore
    public Resting(Parcel parcel) {
        this.mStartTime = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mValue = parcel.readByte();
    }
}
