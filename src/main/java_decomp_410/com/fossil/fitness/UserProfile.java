package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class UserProfile implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<UserProfile> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ short mAge;
    @DexIgnore
    public /* final */ Gender mGender;
    @DexIgnore
    public /* final */ float mHeightInMeter;
    @DexIgnore
    public /* final */ float mWeightInKg;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<UserProfile> {
        @DexIgnore
        public UserProfile createFromParcel(Parcel parcel) {
            return new UserProfile(parcel);
        }

        @DexIgnore
        public UserProfile[] newArray(int i) {
            return new UserProfile[i];
        }
    }

    @DexIgnore
    public UserProfile(short s, Gender gender, float f, float f2) {
        this.mAge = s;
        this.mGender = gender;
        this.mHeightInMeter = f;
        this.mWeightInKg = f2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof UserProfile)) {
            return false;
        }
        UserProfile userProfile = (UserProfile) obj;
        if (this.mAge == userProfile.mAge && this.mGender == userProfile.mGender && this.mHeightInMeter == userProfile.mHeightInMeter && this.mWeightInKg == userProfile.mWeightInKg) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public short getAge() {
        return this.mAge;
    }

    @DexIgnore
    public Gender getGender() {
        return this.mGender;
    }

    @DexIgnore
    public float getHeightInMeter() {
        return this.mHeightInMeter;
    }

    @DexIgnore
    public float getWeightInKg() {
        return this.mWeightInKg;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((527 + this.mAge) * 31) + this.mGender.hashCode()) * 31) + Float.floatToIntBits(this.mHeightInMeter)) * 31) + Float.floatToIntBits(this.mWeightInKg);
    }

    @DexIgnore
    public String toString() {
        return "UserProfile{mAge=" + this.mAge + ",mGender=" + this.mGender + ",mHeightInMeter=" + this.mHeightInMeter + ",mWeightInKg=" + this.mWeightInKg + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mAge);
        parcel.writeInt(this.mGender.ordinal());
        parcel.writeFloat(this.mHeightInMeter);
        parcel.writeFloat(this.mWeightInKg);
    }

    @DexIgnore
    public UserProfile(Parcel parcel) {
        this.mAge = (short) parcel.readInt();
        this.mGender = Gender.values()[parcel.readInt()];
        this.mHeightInMeter = parcel.readFloat();
        this.mWeightInKg = parcel.readFloat();
    }
}
