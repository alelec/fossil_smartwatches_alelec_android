package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FitnessData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<FitnessData> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ ActiveMinute mActiveMinute;
    @DexIgnore
    public /* final */ Calorie mCalorie;
    @DexIgnore
    public /* final */ Distance mDistance;
    @DexIgnore
    public /* final */ int mEndTime;
    @DexIgnore
    public /* final */ ArrayList<GoalTracking> mGoals;
    @DexIgnore
    public /* final */ HeartRate mHeartrate;
    @DexIgnore
    public /* final */ ArrayList<Resting> mResting;
    @DexIgnore
    public /* final */ ArrayList<SleepSession> mSleeps;
    @DexIgnore
    public /* final */ int mStartTime;
    @DexIgnore
    public /* final */ Step mStep;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ ArrayList<WorkoutSession> mWorkouts;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<FitnessData> {
        @DexIgnore
        public FitnessData createFromParcel(Parcel parcel) {
            return new FitnessData(parcel);
        }

        @DexIgnore
        public FitnessData[] newArray(int i) {
            return new FitnessData[i];
        }
    }

    @DexIgnore
    public FitnessData(int i, int i2, int i3, Step step, Calorie calorie, Distance distance, ArrayList<SleepSession> arrayList, ArrayList<WorkoutSession> arrayList2, ActiveMinute activeMinute, ArrayList<Resting> arrayList3, HeartRate heartRate, ArrayList<GoalTracking> arrayList4) {
        this.mStartTime = i;
        this.mEndTime = i2;
        this.mTimezoneOffsetInSecond = i3;
        this.mStep = step;
        this.mCalorie = calorie;
        this.mDistance = distance;
        this.mSleeps = arrayList;
        this.mWorkouts = arrayList2;
        this.mActiveMinute = activeMinute;
        this.mResting = arrayList3;
        this.mHeartrate = heartRate;
        this.mGoals = arrayList4;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof FitnessData)) {
            return false;
        }
        FitnessData fitnessData = (FitnessData) obj;
        if (this.mStartTime != fitnessData.mStartTime || this.mEndTime != fitnessData.mEndTime || this.mTimezoneOffsetInSecond != fitnessData.mTimezoneOffsetInSecond || !this.mStep.equals(fitnessData.mStep) || !this.mCalorie.equals(fitnessData.mCalorie) || !this.mDistance.equals(fitnessData.mDistance) || !this.mSleeps.equals(fitnessData.mSleeps) || !this.mWorkouts.equals(fitnessData.mWorkouts)) {
            return false;
        }
        if (!(this.mActiveMinute == null && fitnessData.mActiveMinute == null)) {
            ActiveMinute activeMinute = this.mActiveMinute;
            if (activeMinute == null || !activeMinute.equals(fitnessData.mActiveMinute)) {
                return false;
            }
        }
        if (!this.mResting.equals(fitnessData.mResting)) {
            return false;
        }
        if (!(this.mHeartrate == null && fitnessData.mHeartrate == null)) {
            HeartRate heartRate = this.mHeartrate;
            if (heartRate == null || !heartRate.equals(fitnessData.mHeartrate)) {
                return false;
            }
        }
        if (this.mGoals.equals(fitnessData.mGoals)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public ActiveMinute getActiveMinute() {
        return this.mActiveMinute;
    }

    @DexIgnore
    public Calorie getCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public Distance getDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public int getEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public ArrayList<GoalTracking> getGoals() {
        return this.mGoals;
    }

    @DexIgnore
    public HeartRate getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public ArrayList<Resting> getResting() {
        return this.mResting;
    }

    @DexIgnore
    public ArrayList<SleepSession> getSleeps() {
        return this.mSleeps;
    }

    @DexIgnore
    public int getStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public Step getStep() {
        return this.mStep;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public ArrayList<WorkoutSession> getWorkouts() {
        return this.mWorkouts;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((((((527 + this.mStartTime) * 31) + this.mEndTime) * 31) + this.mTimezoneOffsetInSecond) * 31) + this.mStep.hashCode()) * 31) + this.mCalorie.hashCode()) * 31) + this.mDistance.hashCode()) * 31) + this.mSleeps.hashCode()) * 31) + this.mWorkouts.hashCode()) * 31;
        ActiveMinute activeMinute = this.mActiveMinute;
        int i = 0;
        int hashCode2 = (((hashCode + (activeMinute == null ? 0 : activeMinute.hashCode())) * 31) + this.mResting.hashCode()) * 31;
        HeartRate heartRate = this.mHeartrate;
        if (heartRate != null) {
            i = heartRate.hashCode();
        }
        return ((hashCode2 + i) * 31) + this.mGoals.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "FitnessData{mStartTime=" + this.mStartTime + ",mEndTime=" + this.mEndTime + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mStep=" + this.mStep + ",mCalorie=" + this.mCalorie + ",mDistance=" + this.mDistance + ",mSleeps=" + this.mSleeps + ",mWorkouts=" + this.mWorkouts + ",mActiveMinute=" + this.mActiveMinute + ",mResting=" + this.mResting + ",mHeartrate=" + this.mHeartrate + ",mGoals=" + this.mGoals + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mEndTime);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        this.mStep.writeToParcel(parcel, i);
        this.mCalorie.writeToParcel(parcel, i);
        this.mDistance.writeToParcel(parcel, i);
        parcel.writeList(this.mSleeps);
        parcel.writeList(this.mWorkouts);
        if (this.mActiveMinute != null) {
            parcel.writeByte((byte) 1);
            this.mActiveMinute.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mResting);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            this.mHeartrate.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mGoals);
    }

    @DexIgnore
    public FitnessData(Parcel parcel) {
        this.mStartTime = parcel.readInt();
        this.mEndTime = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mStep = new Step(parcel);
        this.mCalorie = new Calorie(parcel);
        this.mDistance = new Distance(parcel);
        this.mSleeps = new ArrayList<>();
        parcel.readList(this.mSleeps, FitnessData.class.getClassLoader());
        this.mWorkouts = new ArrayList<>();
        parcel.readList(this.mWorkouts, FitnessData.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mActiveMinute = null;
        } else {
            this.mActiveMinute = new ActiveMinute(parcel);
        }
        this.mResting = new ArrayList<>();
        parcel.readList(this.mResting, FitnessData.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = new HeartRate(parcel);
        }
        this.mGoals = new ArrayList<>();
        parcel.readList(this.mGoals, FitnessData.class.getClassLoader());
    }
}
