package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum InformationCode {
    UNKNOWN,
    SOFTWARE_RESET,
    HARDWARE_RESET,
    TIME_CHANGED,
    FIRST_MINUTE,
    OUT_OF_BATTERY
}
