package com.fossil.wearables.fsl.fitness;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Fitness {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Values {
        @DexIgnore
        public double calories; // = 0.0d;
        @DexIgnore
        public double distance; // = 0.0d;
        @DexIgnore
        public double steps; // = 0.0d;
    }

    @DexIgnore
    public static final Values addSamples(List<SampleDay> list) {
        Values values = new Values();
        for (SampleDay next : list) {
            values.steps += next.getSteps();
            values.calories += next.getCalories();
            values.distance += next.getDistance();
        }
        return values;
    }
}
