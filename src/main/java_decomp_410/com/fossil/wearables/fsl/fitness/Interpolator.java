package com.fossil.wearables.fsl.fitness;

import android.util.Log;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.ReadableInstant;
import org.joda.time.chrono.GregorianChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class Interpolator {
    @DexIgnore
    public static /* final */ String TAG; // = "Interpolator";

    @DexIgnore
    public static int calculateIntensityLevel(long j) {
        if (j < 70) {
            return 0;
        }
        return j < 140 ? 1 : 2;
    }

    @DexIgnore
    public static SampleDay createSample(DateTime dateTime, double d, double d2, double d3, List<Integer> list) {
        TimeZone timeZone = dateTime.getZone().toTimeZone();
        return new SampleDay(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), timeZone.getID(), timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0, d, d2, d3, list);
    }

    @DexIgnore
    public static DateTime getCursorEndDateTime(DateTime dateTime) {
        return new DateTime((Object) dateTime, dateTime.getChronology()).withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
    }

    @DexIgnore
    public static DateTime getCursorEndHourTime(DateTime dateTime) {
        return new DateTime((Object) dateTime, dateTime.getChronology()).withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
    }

    @DexIgnore
    public static List<SampleDay> interpolateDays(Date date, Date date2, TimeZone timeZone, double d, double d2, double d3) {
        int i;
        DateTime dateTime;
        DateTime dateTime2;
        int i2;
        int i3;
        GregorianChronology instance = GregorianChronology.getInstance(DateTimeZone.forTimeZone(timeZone));
        DateTime dateTime3 = new DateTime((Object) date, (Chronology) instance);
        DateTime dateTime4 = new DateTime((Object) date2, (Chronology) instance);
        ArrayList arrayList = new ArrayList();
        int i4 = 4;
        MathContext mathContext = new MathContext(4, RoundingMode.HALF_UP);
        if (dateTime3.isBefore((ReadableInstant) dateTime4)) {
            double millis = (double) new Duration((ReadableInstant) dateTime3, (ReadableInstant) dateTime4).getMillis();
            BigDecimal bigDecimal = new BigDecimal(d / millis);
            BigDecimal bigDecimal2 = new BigDecimal(d2 / millis);
            BigDecimal bigDecimal3 = new BigDecimal(d3 / millis);
            DateTime dateTime5 = dateTime3;
            while (dateTime4.isAfter((ReadableInstant) dateTime5)) {
                DateTime cursorEndDateTime = getCursorEndDateTime(dateTime5);
                if (cursorEndDateTime.isAfter((ReadableInstant) dateTime4)) {
                    cursorEndDateTime = dateTime4;
                }
                BigDecimal bigDecimal4 = new BigDecimal((double) new Period((ReadableInstant) dateTime5, (ReadableInstant) cursorEndDateTime, PeriodType.millis()).getMillis());
                double doubleValue = bigDecimal.multiply(bigDecimal4, mathContext).setScale(2, i4).doubleValue();
                double doubleValue2 = bigDecimal2.multiply(bigDecimal4, mathContext).setScale(2, i4).doubleValue();
                double doubleValue3 = bigDecimal3.multiply(bigDecimal4, mathContext).setScale(2, i4).doubleValue();
                DateTime dateTime6 = dateTime5;
                int calculateIntensityLevel = calculateIntensityLevel(((long) doubleValue) / new Duration((ReadableInstant) dateTime3, (ReadableInstant) dateTime4).getStandardMinutes());
                if (calculateIntensityLevel == 0) {
                    dateTime2 = dateTime3;
                    dateTime = dateTime4;
                    i3 = (int) (((double) 0) + doubleValue);
                    i2 = 0;
                } else if (calculateIntensityLevel != 2) {
                    dateTime2 = dateTime3;
                    dateTime = dateTime4;
                    i2 = (int) (((double) 0) + doubleValue);
                    i3 = 0;
                } else {
                    dateTime2 = dateTime3;
                    dateTime = dateTime4;
                    i = (int) (((double) 0) + doubleValue);
                    i3 = 0;
                    i2 = 0;
                    arrayList.add(createSample(dateTime6, doubleValue, doubleValue2, doubleValue3, Arrays.asList(new Integer[]{Integer.valueOf(i3), Integer.valueOf(i2), Integer.valueOf(i)})));
                    dateTime5 = cursorEndDateTime.plusMillis(1);
                    dateTime3 = dateTime2;
                    dateTime4 = dateTime;
                    i4 = 4;
                }
                i = 0;
                arrayList.add(createSample(dateTime6, doubleValue, doubleValue2, doubleValue3, Arrays.asList(new Integer[]{Integer.valueOf(i3), Integer.valueOf(i2), Integer.valueOf(i)})));
                dateTime5 = cursorEndDateTime.plusMillis(1);
                dateTime3 = dateTime2;
                dateTime4 = dateTime;
                i4 = 4;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static List<SampleRaw> interpolateSampleHalfHour(Date date, Date date2, TimeZone timeZone, String str, FitnessSourceType fitnessSourceType, FitnessMovementType fitnessMovementType, double d, double d2, double d3) {
        DateTime dateTime;
        ArrayList arrayList = new ArrayList();
        GregorianChronology instance = GregorianChronology.getInstance(DateTimeZone.forTimeZone(timeZone));
        DateTime dateTime2 = new DateTime((Object) date, (Chronology) instance);
        DateTime dateTime3 = new DateTime((Object) date2, (Chronology) instance);
        MathContext mathContext = new MathContext(4, RoundingMode.HALF_UP);
        if (dateTime2.isBefore((ReadableInstant) dateTime3)) {
            double millis = (double) new Duration((ReadableInstant) dateTime2, (ReadableInstant) dateTime3).getMillis();
            BigDecimal bigDecimal = new BigDecimal(d / millis);
            BigDecimal bigDecimal2 = new BigDecimal(d2 / millis);
            BigDecimal bigDecimal3 = new BigDecimal(d3 / millis);
            for (DateTime dateTime4 = new DateTime((Object) dateTime2, dateTime2.getChronology()); dateTime3.isAfter((ReadableInstant) dateTime4); dateTime4 = dateTime.plusMillis(1)) {
                DateTime dateTime5 = new DateTime((Object) dateTime4, dateTime4.getChronology());
                if (dateTime4.getMinuteOfHour() < 30) {
                    dateTime = dateTime5.withMinuteOfHour(29).withSecondOfMinute(59).withMillisOfSecond(999);
                } else {
                    dateTime = dateTime5.withMinuteOfHour(59).withSecondOfMinute(59).withMillisOfSecond(999);
                }
                if (dateTime.isAfter((ReadableInstant) dateTime3)) {
                    dateTime = dateTime3;
                }
                BigDecimal bigDecimal4 = new BigDecimal((double) new Period((ReadableInstant) dateTime4, (ReadableInstant) dateTime, PeriodType.millis()).getMillis());
                try {
                    arrayList.add(new SampleRaw(dateTime4.toDate(), dateTime.toDate(), timeZone.getID(), str, fitnessSourceType, fitnessMovementType, bigDecimal.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue(), bigDecimal2.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue(), bigDecimal3.multiply(bigDecimal4, mathContext).setScale(2, 4).doubleValue()));
                } catch (Exception e) {
                    String str2 = TAG;
                    Log.e(str2, "Error inside " + TAG + ".interpolateHours - e=" + e);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static DateTime resetHour(DateTime dateTime) {
        return new DateTime((Object) dateTime, dateTime.getChronology()).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0);
    }
}
