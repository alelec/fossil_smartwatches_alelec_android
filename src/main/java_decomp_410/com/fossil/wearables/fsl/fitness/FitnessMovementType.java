package com.fossil.wearables.fsl.fitness;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FitnessMovementType {
    WALKING("walking"),
    RUNNING("running"),
    BIKING("biking");
    
    @DexIgnore
    public static /* final */ Map<String, FitnessMovementType> map; // = null;
    @DexIgnore
    public /* final */ String name;

    /*
    static {
        int i;
        map = new HashMap();
        for (FitnessMovementType fitnessMovementType : values()) {
            map.put(fitnessMovementType.name, fitnessMovementType);
        }
    }
    */

    @DexIgnore
    FitnessMovementType(String str) {
        this.name = str;
    }

    @DexIgnore
    public static FitnessMovementType valueFor(String str) {
        return map.get(str);
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }
}
