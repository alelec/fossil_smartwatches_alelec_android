package com.fossil.wearables.fsl.fitness;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "activitySettings")
public class ActivitySettings {
    @DexIgnore
    public static /* final */ String COLUMN_CURRENT_ACTIVE_TIME_GOAL; // = "currentActiveTimeGoal";
    @DexIgnore
    public static /* final */ String COLUMN_CURRENT_CALORIES_GOAL; // = "currentCaloriesGoal";
    @DexIgnore
    public static /* final */ String COLUMN_CURRENT_STEP_GOAL; // = "currentStepGoal";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "activitySettings";
    @DexIgnore
    @DatabaseField(columnName = "currentActiveTimeGoal")
    public int currentActiveTimeGoal;
    @DexIgnore
    @DatabaseField(columnName = "currentCaloriesGoal")
    public int currentCaloriesGoal;
    @DexIgnore
    @DatabaseField(columnName = "currentStepGoal")
    public int currentStepGoal;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id; // = UUID.randomUUID().toString();

    @DexIgnore
    public ActivitySettings() {
    }

    @DexIgnore
    public int getCurrentActiveTimeGoal() {
        return this.currentActiveTimeGoal;
    }

    @DexIgnore
    public int getCurrentCaloriesGoal() {
        return this.currentCaloriesGoal;
    }

    @DexIgnore
    public int getCurrentStepGoal() {
        return this.currentStepGoal;
    }

    @DexIgnore
    public void setCurrentActiveTimeGoal(int i) {
        this.currentActiveTimeGoal = i;
    }

    @DexIgnore
    public void setCurrentCaloriesGoal(int i) {
        this.currentCaloriesGoal = i;
    }

    @DexIgnore
    public void setCurrentStepGoal(int i) {
        this.currentStepGoal = i;
    }

    @DexIgnore
    public ActivitySettings(int i, int i2, int i3) {
        this.currentStepGoal = i;
        this.currentCaloriesGoal = i2;
        this.currentActiveTimeGoal = i3;
    }
}
