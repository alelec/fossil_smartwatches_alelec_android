package com.fossil.wearables.fsl.countdown;

import android.content.Context;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class CountDownProviderImpl extends BaseDbProvider implements CountDownProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "countdown.db";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public CountDownProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<CountDown, Long> getCountDownDao() throws SQLException {
        return this.databaseHelper.getDao(CountDown.class);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x000c */
    public void clearData() {
        Class<CountDown> cls = CountDown.class;
        TableUtils.clearTable(this.databaseHelper.getConnectionSource(), cls);
        try {
            TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), cls);
        } catch (Exception unused) {
        }
        Log.e(this.TAG, "Failed to clear data");
    }

    @DexIgnore
    public void deleteCountDown(String str) {
        try {
            DeleteBuilder<CountDown, Long> deleteBuilder = getCountDownDao().deleteBuilder();
            deleteBuilder.where().eq("uri", str);
            getCountDownDao().delete(deleteBuilder.prepare());
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".deleteCountDown - e=" + e);
        }
    }

    @DexIgnore
    public void dropThenCreateTables() {
        try {
            this.databaseHelper.dropTables(getDbEntities());
            TableUtils.createTableIfNotExists(this.databaseHelper.getConnectionSource(), CountDown.class);
        } catch (Exception unused) {
            Log.e(this.TAG, "Failed to drop tables");
        }
    }

    @DexIgnore
    public CountDown getActiveCountDown() {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(CountDownStatus.ACTIVE.getValue()));
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getActiveCountDown - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<CountDown> getAllCountDown() {
        try {
            return getCountDownDao().queryForAll();
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDown(String str, long j) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("name", str).and().eq(CountDown.COLUMN_ENDED_AT, Long.valueOf(j));
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getCountDown - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDownByClientId(String str) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("uri", str);
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getCountDownByClientId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDownById(long j) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("id", Long.valueOf(j));
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getCountDownByClientId - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public CountDown getCountDownByServerId(String str) {
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("serverId", str);
            return getCountDownDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".getCountDown - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<CountDown> getCountDownsByStatus(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.orderBy("updatedAt", false);
            queryBuilder.orderBy("createdAt", false);
            queryBuilder.where().eq("status", Integer.valueOf(i));
            List<CountDown> query = getCountDownDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getPastCountDownsPaging - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{CountDown.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public List<CountDown> getPastCountDownsPaging(long j, long j2) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<CountDown, Long> queryBuilder = getCountDownDao().queryBuilder();
            queryBuilder.where().eq("status", Integer.valueOf(CountDownStatus.COMPLETED.getValue())).or().eq("status", Integer.valueOf(CountDownStatus.INACTIVE.getValue()));
            queryBuilder.orderBy("updatedAt", false);
            queryBuilder.orderBy("createdAt", false);
            queryBuilder.offset(Long.valueOf(j));
            queryBuilder.limit(Long.valueOf(j2));
            List<CountDown> query = getCountDownDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".getPastCountDownsPaging - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public void saveCountDown(CountDown countDown) {
        try {
            CountDown countDownByClientId = getCountDownByClientId(countDown.getUri());
            if (countDownByClientId != null) {
                countDown.setUpdatedAt(System.currentTimeMillis());
                countDown.setId(countDownByClientId.getId());
            } else {
                countDown.setCreatedAt(System.currentTimeMillis());
            }
            getCountDownDao().createOrUpdate(countDown);
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".saveCountDown - e=" + e);
        }
    }

    @DexIgnore
    public void setCountDownStatus(String str, CountDownStatus countDownStatus) {
        try {
            CountDown countDownByClientId = getCountDownByClientId(str);
            if (countDownByClientId != null) {
                countDownByClientId.setStatus(countDownStatus);
                saveCountDown(countDownByClientId);
            }
        } catch (Exception e) {
            String str2 = this.TAG;
            Log.e(str2, "Error inside " + this.TAG + ".setCountDownStatus - e=" + e);
        }
    }

    @DexIgnore
    public void deleteCountDown(long j) {
        try {
            DeleteBuilder<CountDown, Long> deleteBuilder = getCountDownDao().deleteBuilder();
            deleteBuilder.where().eq("id", Long.valueOf(j));
            getCountDownDao().delete(deleteBuilder.prepare());
        } catch (Exception e) {
            String str = this.TAG;
            Log.e(str, "Error inside " + this.TAG + ".deleteCountDown - e=" + e);
        }
    }
}
