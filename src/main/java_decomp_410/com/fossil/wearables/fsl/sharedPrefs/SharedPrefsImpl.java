package com.fossil.wearables.fsl.sharedPrefs;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class SharedPrefsImpl implements SharedPrefs {
    @DexIgnore
    public /* final */ String PREF_FILE;

    @DexIgnore
    public SharedPrefsImpl(String str) {
        this.PREF_FILE = "prefs_" + str;
    }

    @DexIgnore
    public boolean getSharedPreferenceBoolean(Context context, String str, boolean z) {
        return context.getSharedPreferences(this.PREF_FILE, 0).getBoolean(str, z);
    }

    @DexIgnore
    public int getSharedPreferenceInt(Context context, String str, int i) {
        return context.getSharedPreferences(this.PREF_FILE, 0).getInt(str, i);
    }

    @DexIgnore
    public String getSharedPreferenceString(Context context, String str, String str2) {
        return context.getSharedPreferences(this.PREF_FILE, 0).getString(str, str2);
    }

    @DexIgnore
    public void setSharedPreferenceBoolean(Context context, String str, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences(this.PREF_FILE, 0).edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    @DexIgnore
    public void setSharedPreferenceInt(Context context, String str, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences(this.PREF_FILE, 0).edit();
        edit.putInt(str, i);
        edit.commit();
    }

    @DexIgnore
    public void setSharedPreferenceString(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences(this.PREF_FILE, 0).edit();
        edit.putString(str, str2);
        edit.commit();
    }
}
