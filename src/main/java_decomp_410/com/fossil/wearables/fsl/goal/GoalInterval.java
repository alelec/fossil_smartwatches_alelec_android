package com.fossil.wearables.fsl.goal;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class GoalInterval extends BaseModel {
    @DexIgnore
    @DatabaseField(columnName = "goal_id", foreign = true, foreignAutoRefresh = true)
    public Goal goal;
    @DexIgnore
    @DatabaseField
    public int index;
    @DexIgnore
    @DatabaseField
    public int value;

    @DexIgnore
    public Goal getGoal() {
        return this.goal;
    }

    @DexIgnore
    public int getIndex() {
        return this.index;
    }

    @DexIgnore
    public double getProgress() {
        return ((double) this.value) / ((double) this.goal.getTargetValue());
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }

    @DexIgnore
    public void setGoal(Goal goal2) {
        this.goal = goal2;
    }

    @DexIgnore
    public void setIndex(int i) {
        this.index = i;
    }

    @DexIgnore
    public void setValue(int i) {
        if (i > this.goal.getTargetValue()) {
            i = this.goal.getTargetValue();
        }
        this.value = i;
    }
}
