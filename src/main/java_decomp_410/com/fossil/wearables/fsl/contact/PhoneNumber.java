package com.fossil.wearables.fsl.contact;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class PhoneNumber extends BaseModel {
    @DexIgnore
    @DatabaseField(columnName = "phone_number_id", foreign = true, foreignAutoRefresh = true)
    public Contact contact;
    @DexIgnore
    @DatabaseField
    public String number;

    @DexIgnore
    public Contact getContact() {
        return this.contact;
    }

    @DexIgnore
    public String getNumber() {
        return this.number;
    }

    @DexIgnore
    public void setContact(Contact contact2) {
        this.contact = contact2;
    }

    @DexIgnore
    public void setNumber(String str) {
        this.number = str;
    }
}
