package com.fossil.wearables.fsl.contact;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ContactProvider extends BaseProvider {
    @DexIgnore
    void clearAllTables();

    @DexIgnore
    List<ContactGroup> getAllContactGroups();

    @DexIgnore
    List<ContactGroup> getAllContactGroups(int i);

    @DexIgnore
    Contact getContact(int i);

    @DexIgnore
    ContactGroup getContactGroup(int i);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingEmail(String str);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingEmail(String str, int i);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingIncomingCall(String str);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingSms(String str);

    @DexIgnore
    List<ContactGroup> getContactGroupsMatchingSms(String str, int i);

    @DexIgnore
    void removeAllContactGroups();

    @DexIgnore
    void removeContact(Contact contact);

    @DexIgnore
    void removeContactGroup(ContactGroup contactGroup);

    @DexIgnore
    void removeEmailAddress(EmailAddress emailAddress);

    @DexIgnore
    void removePhoneNumber(PhoneNumber phoneNumber);

    @DexIgnore
    void saveContact(Contact contact);

    @DexIgnore
    void saveContactGroup(ContactGroup contactGroup);

    @DexIgnore
    void saveEmailAddress(EmailAddress emailAddress);

    @DexIgnore
    void savePhoneNumber(PhoneNumber phoneNumber);

    @DexIgnore
    void syncContacts();
}
