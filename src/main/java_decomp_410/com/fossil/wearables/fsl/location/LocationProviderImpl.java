package com.fossil.wearables.fsl.location;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class LocationProviderImpl extends BaseDbProvider implements LocationProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "location.db";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wearables.fsl.location.LocationProviderImpl$Anon1$Anon1")
        /* renamed from: com.fossil.wearables.fsl.location.LocationProviderImpl$Anon1$Anon1  reason: collision with other inner class name */
        public class C0042Anon1 implements UpgradeCommand {
            @DexIgnore
            public C0042Anon1() {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("CREATE TABLE devicelocation (deviceSerial VARCHAR PRIMARY KEY, longitude DOUBLE, latitude DOUBLE, timestamp BIGINT)");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new C0042Anon1());
        }
    }

    @DexIgnore
    public LocationProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DeviceLocation, String> getDeviceLocationDao() throws SQLException {
        return this.databaseHelper.getDao(DeviceLocation.class);
    }

    @DexIgnore
    private Dao<Location, Integer> getLocationDao() throws SQLException {
        return this.databaseHelper.getDao(Location.class);
    }

    @DexIgnore
    private Dao<LocationGroup, Integer> getLocationGroupDao() throws SQLException {
        return this.databaseHelper.getDao(LocationGroup.class);
    }

    @DexIgnore
    public List<DeviceLocation> getAllDevicesLocation() {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DeviceLocation, String> queryBuilder = getDeviceLocationDao().queryBuilder();
            queryBuilder.orderBy("timestamp", true);
            List<DeviceLocation> query = getDeviceLocationDao().query(queryBuilder.prepare());
            if (query != null && query.size() > 0) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    public List<LocationGroup> getAllLocationGroups() {
        ArrayList arrayList = new ArrayList();
        try {
            return getLocationGroupDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{LocationGroup.class, Location.class, DeviceLocation.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public DeviceLocation getDeviceLocation(String str) {
        try {
            QueryBuilder<DeviceLocation, String> queryBuilder = getDeviceLocationDao().queryBuilder();
            queryBuilder.where().eq(DeviceLocation.COLUMN_DEVICE_SERIAL, str);
            queryBuilder.orderBy("timestamp", false);
            List<DeviceLocation> query = getDeviceLocationDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0) {
                return null;
            }
            return query.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public Location getLocation(int i) {
        try {
            return getLocationDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public LocationGroup getLocationGroup(int i) {
        try {
            return getLocationGroupDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public List<Location> getLocations(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<Location, Integer> queryBuilder = getLocationDao().queryBuilder();
            queryBuilder.where().eq("location_group_id", Integer.valueOf(i));
            return getLocationDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public void removeDeviceLocation(DeviceLocation deviceLocation) {
        try {
            getDeviceLocationDao().delete(deviceLocation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public void removeLocation(Location location) {
        if (location != null) {
            try {
                getLocationDao().delete(location);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void removeLocationGroup(LocationGroup locationGroup) {
        if (locationGroup != null) {
            try {
                for (Location removeLocation : locationGroup.getLocations()) {
                    removeLocation(removeLocation);
                }
                getLocationGroupDao().delete(locationGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveDeviceLocation(DeviceLocation deviceLocation) {
        if (deviceLocation != null) {
            try {
                DeviceLocation deviceLocation2 = getDeviceLocation(deviceLocation.getDeviceSerial());
                if (deviceLocation2 != null) {
                    deviceLocation2.setLatitude(deviceLocation.getLatitude());
                    deviceLocation2.setLongitude(deviceLocation.getLongitude());
                    deviceLocation2.setTimeStamp(deviceLocation.getTimeStamp());
                    getDeviceLocationDao().update(deviceLocation2);
                    return;
                }
                getDeviceLocationDao().create(deviceLocation);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveLocation(Location location) {
        if (location != null) {
            try {
                QueryBuilder<Location, Integer> queryBuilder = getLocationDao().queryBuilder();
                queryBuilder.where().le("latitude", Double.valueOf(location.getLatitude())).and().ge("longitude", Double.valueOf(location.getLongitude()));
                List<Location> query = getLocationDao().query(queryBuilder.prepare());
                if (query != null && query.size() > 0) {
                    Location location2 = query.get(0);
                    if (location2 != null) {
                        location.setDbRowId(location2.getDbRowId());
                    }
                }
                getLocationDao().createOrUpdate(location);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveLocationGroup(LocationGroup locationGroup) {
        if (locationGroup != null) {
            try {
                LocationGroup queryForSameId = getLocationGroupDao().queryForSameId(locationGroup);
                if (queryForSameId != null) {
                    locationGroup.setDbRowId(queryForSameId.getDbRowId());
                }
                getLocationGroupDao().createOrUpdate(locationGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void removeDeviceLocation(String str) {
        try {
            QueryBuilder<DeviceLocation, String> queryBuilder = getDeviceLocationDao().queryBuilder();
            queryBuilder.where().eq(DeviceLocation.COLUMN_DEVICE_SERIAL, str);
            List<DeviceLocation> query = getDeviceLocationDao().query(queryBuilder.prepare());
            if (query != null && query.size() > 0) {
                for (DeviceLocation removeDeviceLocation : query) {
                    removeDeviceLocation(removeDeviceLocation);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    public List<Location> getLocations(int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<Location, Integer> queryBuilder = getLocationDao().queryBuilder();
            queryBuilder.where().eq("verified", Boolean.valueOf(z)).and().eq("location_group_id", Integer.valueOf(i));
            return getLocationDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public List<Location> getLocations(List<Location> list, boolean z) {
        ArrayList arrayList = new ArrayList();
        for (Location next : list) {
            if (next.isVerified() == z) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
