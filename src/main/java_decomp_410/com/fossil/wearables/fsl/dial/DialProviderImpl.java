package com.fossil.wearables.fsl.dial;

import android.content.Context;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class DialProviderImpl extends BaseDbProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "dial.db";
    @DexIgnore
    public static DialProviderImpl sDialProvider;
    @DexIgnore
    public /* final */ String TAG; // = DialProviderImpl.class.getSimpleName();

    @DexIgnore
    public DialProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public static DialProviderImpl getInstance(Context context) {
        if (sDialProvider == null) {
            sDialProvider = new DialProviderImpl(context, DB_NAME);
        }
        return sDialProvider;
    }

    @DexIgnore
    public void create(SavedDial savedDial) {
        if (savedDial != null) {
            try {
                this.databaseHelper.getDao(SavedDial.class).create(savedDial);
                Dao dao = this.databaseHelper.getDao(ConfigItem.class);
                if (savedDial.getConfigItems() != null) {
                    for (ConfigItem create : savedDial.getConfigItems()) {
                        dao.create(create);
                    }
                } else if (savedDial.initialConfigItems != null) {
                    Iterator<ConfigItem> it = savedDial.initialConfigItems.iterator();
                    while (it.hasNext()) {
                        dao.create(it.next());
                    }
                }
                String str = this.TAG;
                Log.d(str, "create " + savedDial.getDisplayName());
            } catch (SQLException e) {
                String str2 = this.TAG;
                Log.e(str2, "create '" + savedDial.getDisplayName() + "' failed.\n" + e.getLocalizedMessage());
            }
        }
    }

    @DexIgnore
    public void delete(int i) {
        try {
            Dao dao = this.databaseHelper.getDao(SavedDial.class);
            SavedDial savedDial = (SavedDial) dao.queryForId(Integer.valueOf(i));
            dao.delete(savedDial);
            String str = this.TAG;
            Log.d(str, "delete " + savedDial.getDisplayName());
        } catch (SQLException e) {
            String str2 = this.TAG;
            Log.e(str2, "delete '" + i + "' failed.\n" + e.getLocalizedMessage());
        }
    }

    @DexIgnore
    public List<SavedDial> getAllSavedDials() {
        List<SavedDial> list = null;
        try {
            list = this.databaseHelper.getDao(SavedDial.class).queryForAll();
            String str = this.TAG;
            Log.d(str, "getAllSavedDials() = " + list.size());
            return list;
        } catch (SQLException e) {
            String str2 = this.TAG;
            Log.e(str2, "getAllSavedDials failed.\n" + e.getLocalizedMessage());
            return list;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{SavedDial.class, ConfigItem.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 3;
    }

    @DexIgnore
    public SavedDial getSavedDial(int i) {
        throw null;
        // SavedDial savedDial;
        // SQLException e;
        // try {
        //     savedDial = (SavedDial) this.databaseHelper.getDao(SavedDial.class).queryForId(Integer.valueOf(i));
        //     try {
        //         Log.d(this.TAG, "getSavedDial(" + i + ") = " + savedDial.getDisplayName());
        //     } catch (SQLException e2) {
        //         e = e2;
        //     }
        // } catch (SQLException e3) {
        //     SQLException sQLException = e3;
        //     savedDial = null;
        //     e = sQLException;
        //     Log.e(this.TAG, "getSavedDial '" + i + "' failed.\n" + e.getLocalizedMessage());
        //     return savedDial;
        // }
        // return savedDial;
    }

    @DexIgnore
    public void update(SavedDial savedDial) {
        if (savedDial != null) {
            try {
                Dao dao = this.databaseHelper.getDao(SavedDial.class);
                SavedDial savedDial2 = (SavedDial) dao.queryForSameId(savedDial);
                if (savedDial2 != null) {
                    savedDial.setDbRowId(savedDial2.getDbRowId());
                }
                dao.update(savedDial);
                Dao dao2 = this.databaseHelper.getDao(ConfigItem.class);
                for (ConfigItem configItem : savedDial.getConfigItems()) {
                    ConfigItem configItem2 = (ConfigItem) dao2.queryForSameId(configItem);
                    if (configItem2 != null) {
                        configItem.setDbRowId(configItem2.getDbRowId());
                    }
                    dao2.update(configItem);
                }
                String str = this.TAG;
                Log.d(str, "updated " + savedDial.getDisplayName());
            } catch (SQLException e) {
                String str2 = this.TAG;
                Log.e(str2, "update '" + savedDial.getDisplayName() + "' failed.\n" + e.getLocalizedMessage());
            }
        }
    }

    @DexIgnore
    public void updateNoConfig(SavedDial savedDial) {
        if (savedDial != null) {
            try {
                Dao dao = this.databaseHelper.getDao(SavedDial.class);
                SavedDial savedDial2 = (SavedDial) dao.queryForSameId(savedDial);
                if (savedDial2 != null) {
                    savedDial.setDbRowId(savedDial2.getDbRowId());
                }
                dao.update(savedDial);
                String str = this.TAG;
                Log.d(str, "updated (no config) " + savedDial.getDisplayName());
            } catch (SQLException e) {
                String str2 = this.TAG;
                Log.e(str2, "update '" + savedDial.getDisplayName() + "' failed.\n" + e.getLocalizedMessage());
            }
        }
    }
}
