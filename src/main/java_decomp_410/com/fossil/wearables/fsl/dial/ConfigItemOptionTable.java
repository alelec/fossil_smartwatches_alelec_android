package com.fossil.wearables.fsl.dial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ConfigItemOptionTable {
    @DexIgnore
    public Map<String, List<ValueItem>> optionsTable;

    @DexIgnore
    public List<ValueItem> getOptions(String str) {
        Map<String, List<ValueItem>> map = this.optionsTable;
        if (map == null || map.size() == 0) {
            return null;
        }
        return this.optionsTable.get(str);
    }

    @DexIgnore
    public void putBooleanOptions(String str, int i, int i2) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ValueItem(ConfigItem.VALUE_ON, i));
        arrayList.add(new ValueItem(ConfigItem.VALUE_OFF, i2));
        putOptions(str, arrayList);
    }

    @DexIgnore
    public void putOptions(String str, List<ValueItem> list) {
        if (this.optionsTable == null) {
            this.optionsTable = new HashMap();
        }
        this.optionsTable.put(str, list);
    }
}
