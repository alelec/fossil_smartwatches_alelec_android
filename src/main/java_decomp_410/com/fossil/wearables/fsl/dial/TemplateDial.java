package com.fossil.wearables.fsl.dial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class TemplateDial implements DialListItem {
    @DexIgnore
    public ConfigItemOptionTable configItemOptionTable;
    @DexIgnore
    public String mDisplayName;
    @DexIgnore
    public boolean mIsCustomizable;
    @DexIgnore
    public int mResId;
    @DexIgnore
    public String mUniqueId;
    @DexIgnore
    public Class mWatchFaceClass;

    @DexIgnore
    public TemplateDial() {
    }

    @DexIgnore
    public int getConfigItemDisplayResId(String str) {
        return 0;
    }

    @DexIgnore
    public ConfigItemOptionTable getConfigItemOptionTable() {
        return this.configItemOptionTable;
    }

    @DexIgnore
    public int getDbRowId() {
        return -1;
    }

    @DexIgnore
    public String getDisplayName() {
        return this.mDisplayName;
    }

    @DexIgnore
    public int getResId() {
        return this.mResId;
    }

    @DexIgnore
    public Bitmap getThumbnailBitmap() {
        return null;
    }

    @DexIgnore
    public Drawable getThumbnailDrawable(Context context) {
        return context.getResources().getDrawable(this.mResId);
    }

    @DexIgnore
    public String getUniqueId() {
        if (this.mUniqueId == null) {
            this.mUniqueId = this.mDisplayName.replace(' ', '_');
            String simpleName = TemplateDial.class.getSimpleName();
            Log.d(simpleName, "No unique id -> use default rule: " + this.mUniqueId);
        }
        return this.mUniqueId;
    }

    @DexIgnore
    public Class getWatchFaceClass() {
        return this.mWatchFaceClass;
    }

    @DexIgnore
    public boolean isCustomizable() {
        return this.mIsCustomizable;
    }

    @DexIgnore
    public void livePreview(Object obj, ConfigItem configItem, Context context) {
    }

    @DexIgnore
    public void setInitialConfigItemsTo(SavedDial savedDial) {
    }

    @DexIgnore
    public String toString() {
        return this.mDisplayName;
    }

    @DexIgnore
    public TemplateDial(String str, boolean z, int i, Class cls) {
        this.mDisplayName = str;
        this.mIsCustomizable = z;
        this.mResId = i;
        this.mWatchFaceClass = cls;
    }

    @DexIgnore
    public TemplateDial(String str, int i, Class cls, ConfigItemOptionTable configItemOptionTable2, String str2) {
        this.mDisplayName = str;
        this.mIsCustomizable = true;
        this.mResId = i;
        this.mWatchFaceClass = cls;
        this.configItemOptionTable = configItemOptionTable2;
        this.mUniqueId = str2;
    }
}
