package com.fossil.wearables.fsl.sleep;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.fossil.wearables.fsl.utils.TimeUtils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.common.log.MFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class MFSleepSessionProviderImp extends BaseDbProvider implements MFSleepSessionProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "sleep.db";
    @DexIgnore
    public static /* final */ int DEF_SLEEP_GOAL; // = 480;
    @DexIgnore
    public static /* final */ String TAG; // = "MFSleepSessionProviderImp";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp$Anon1$Anon1")
        /* renamed from: com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp$Anon1$Anon1  reason: collision with other inner class name */
        public class C0043Anon1 implements UpgradeCommand {
            @DexIgnore
            public C0043Anon1() {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                MFLogger.d(MFSleepSessionProviderImp.TAG, " ---- UPGRADE DB SLEEP");
                sQLiteDatabase.execSQL("ALTER TABLE sleep_session ADD COLUMN day VARCHAR");
                MFLogger.d(MFSleepSessionProviderImp.TAG, " ---- UPGRADE DB SLEEP SUCCESS");
                sQLiteDatabase.execSQL("UPDATE sleep_session SET day = (select strftime('%Y-%m-%d', datetime((" + MFSleepSession.COLUMN_EDITED_END_TIME + " + " + "timezoneOffset" + ") , 'unixepoch')) from sleep_session b where sleep_session.id = b. id)");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon2 implements UpgradeCommand {
            @DexIgnore
            public Anon2() {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE sleep_session ADD COLUMN pinType INTEGER DEFAULT 0");
                sQLiteDatabase.execSQL("ALTER TABLE sleep_date ADD COLUMN pinType INTEGER DEFAULT 0");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new C0043Anon1());
            put(3, new Anon2());
        }
    }

    @DexIgnore
    public MFSleepSessionProviderImp(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private MFSleepDay createMFSleepDayFromMFSleepSession(MFSleepSession mFSleepSession) {
        String str = TAG;
        Log.i(str, "Create SleepDay - date=" + TimeUtils.getStringTimeYYYYMMDD(mFSleepSession.getDate()));
        return new MFSleepDay(TimeUtils.getStringTimeYYYYMMDD(mFSleepSession.getDate()), TimeUtils.getTimezoneOffset(), getLastSleepGoal(), mFSleepSession.getEditedSleepMinutes(), mFSleepSession.getEditedSleepStateDistInMinute(), mFSleepSession.getUpdatedAt());
    }

    @DexIgnore
    private MFSleepDay editSleepDayWhenAddSleepSession(MFSleepDay mFSleepDay, MFSleepSession mFSleepSession) {
        String str = TAG;
        Log.i(str, "editSleepDayWhenAddSleepSession - date=" + TimeUtils.getStringTimeYYYYMMDD(mFSleepSession.getDate()));
        SleepDistribution sleepDistributionByString = SleepDistribution.getSleepDistributionByString(mFSleepDay.getSleepStateDistInMinute());
        SleepDistribution sleepDistributionByString2 = SleepDistribution.getSleepDistributionByString(mFSleepSession.getEditedSleepStateDistInMinute());
        if (sleepDistributionByString != null && sleepDistributionByString2 != null) {
            sleepDistributionByString.setAwake(sleepDistributionByString.getAwake() + sleepDistributionByString2.getAwake());
            sleepDistributionByString.setLight(sleepDistributionByString.getLight() + sleepDistributionByString2.getLight());
            sleepDistributionByString.setDeep(sleepDistributionByString.getDeep() + sleepDistributionByString2.getDeep());
        } else if (sleepDistributionByString2 != null) {
            sleepDistributionByString = new SleepDistribution(sleepDistributionByString2.getAwake(), sleepDistributionByString2.getLight(), sleepDistributionByString2.getDeep());
        }
        mFSleepDay.setSleepMinutes(mFSleepDay.getSleepMinutes() + mFSleepSession.getEditedSleepMinutes());
        if (sleepDistributionByString != null) {
            mFSleepDay.setSleepStateDistInMinute(sleepDistributionByString.getSleepDistribution());
        }
        mFSleepDay.setUpdatedAt(new DateTime(Calendar.getInstance().getTimeInMillis()));
        return mFSleepDay;
    }

    @DexIgnore
    private MFSleepDay editSleepDayWhenDeleteSleepSession(MFSleepDay mFSleepDay, MFSleepSession mFSleepSession) {
        SleepDistribution sleepDistributionByString = SleepDistribution.getSleepDistributionByString(mFSleepDay.getSleepStateDistInMinute());
        SleepDistribution sleepDistributionByString2 = SleepDistribution.getSleepDistributionByString(mFSleepSession.getEditedSleepStateDistInMinute());
        if (sleepDistributionByString2 == null) {
            return mFSleepDay;
        }
        sleepDistributionByString.setAwake(sleepDistributionByString.getAwake() - sleepDistributionByString2.getAwake());
        sleepDistributionByString.setLight(sleepDistributionByString.getLight() - sleepDistributionByString2.getLight());
        sleepDistributionByString.setDeep(sleepDistributionByString.getDeep() - sleepDistributionByString2.getDeep());
        mFSleepDay.setSleepMinutes(mFSleepDay.getSleepMinutes() - mFSleepSession.getEditedSleepMinutes());
        mFSleepDay.setSleepStateDistInMinute(sleepDistributionByString.getSleepDistribution());
        mFSleepDay.setUpdatedAt(new DateTime(Calendar.getInstance().getTimeInMillis()));
        return mFSleepDay;
    }

    @DexIgnore
    private MFSleepDay editSleepDayWhenEditSleepSession(MFSleepDay mFSleepDay, MFSleepSession mFSleepSession, MFSleepSession mFSleepSession2) {
        return editSleepDayWhenAddSleepSession(editSleepDayWhenDeleteSleepSession(mFSleepDay, mFSleepSession), mFSleepSession2);
    }

    @DexIgnore
    private Dao<MFSleepDay, Integer> getSleepDao() throws SQLException {
        return this.databaseHelper.getDao(MFSleepDay.class);
    }

    @DexIgnore
    private Dao<MFSleepGoal, Integer> getSleepGoalDao() throws SQLException {
        return this.databaseHelper.getDao(MFSleepGoal.class);
    }

    @DexIgnore
    private Dao<MFSleepSession, Integer> getSleepSessionDao() throws SQLException {
        return this.databaseHelper.getDao(MFSleepSession.class);
    }

    @DexIgnore
    public boolean addJustSleepSession(MFSleepSession mFSleepSession) {
        if (mFSleepSession != null) {
            try {
                QueryBuilder<MFSleepSession, Integer> queryBuilder = getSleepSessionDao().queryBuilder();
                queryBuilder.where().eq(MFSleepSession.COLUMN_REAL_END_TIME, Integer.valueOf(mFSleepSession.getRealEndTime()));
                List<MFSleepSession> query = getSleepSessionDao().query(queryBuilder.prepare());
                if (query != null && !query.isEmpty()) {
                    MFSleepSession mFSleepSession2 = query.get(0);
                    if (mFSleepSession2 != null) {
                        mFSleepSession.setDbRowId(mFSleepSession2.getDbRowId());
                    }
                }
                getSleepSessionDao().createOrUpdate(mFSleepSession);
                return true;
            } catch (Exception e) {
                String str = TAG;
                MFLogger.e(str, "Error inside " + TAG + ".addSleepSession - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    public boolean addSleepDay(MFSleepDay mFSleepDay) {
        String str = TAG;
        MFLogger.d(str, "addSleepDay - sleep=" + mFSleepDay);
        if (mFSleepDay != null) {
            try {
                QueryBuilder<MFSleepDay, Integer> queryBuilder = getSleepDao().queryBuilder();
                queryBuilder.where().eq("date", mFSleepDay.getDate());
                List<MFSleepDay> query = getSleepDao().query(queryBuilder.prepare());
                if (query != null && query.size() > 0) {
                    MFSleepDay mFSleepDay2 = query.get(0);
                    if (mFSleepDay2 != null) {
                        mFSleepDay.setDbRowId(mFSleepDay2.getDbRowId());
                    }
                }
                getSleepDao().createOrUpdate(mFSleepDay);
                return true;
            } catch (Exception e) {
                String str2 = TAG;
                MFLogger.e(str2, "addSleepDay - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    public boolean addSleepSession(MFSleepSession mFSleepSession) {
        String str = TAG;
        MFLogger.d(str, "addSleepSession - session=" + mFSleepSession);
        if (mFSleepSession != null) {
            try {
                QueryBuilder<MFSleepSession, Integer> queryBuilder = getSleepSessionDao().queryBuilder();
                queryBuilder.where().eq(MFSleepSession.COLUMN_REAL_END_TIME, Integer.valueOf(mFSleepSession.getRealEndTime()));
                List<MFSleepSession> query = getSleepSessionDao().query(queryBuilder.prepare());
                MFSleepSession mFSleepSession2 = null;
                if (query != null && query.size() > 0) {
                    mFSleepSession2 = query.get(0);
                    if (mFSleepSession2 != null) {
                        mFSleepSession.setDbRowId(mFSleepSession2.getDbRowId());
                    }
                }
                MFSleepDay sleepDay = getSleepDay(TimeUtils.getStringTimeYYYYMMDD(mFSleepSession.getDate()));
                if (sleepDay == null) {
                    if (!addSleepDay(createMFSleepDayFromMFSleepSession(mFSleepSession))) {
                        return false;
                    }
                } else if (mFSleepSession2 == null && !addSleepDay(editSleepDayWhenAddSleepSession(sleepDay, mFSleepSession))) {
                    return false;
                }
                getSleepSessionDao().createOrUpdate(mFSleepSession);
                return true;
            } catch (Exception e) {
                String str2 = TAG;
                MFLogger.e(str2, "addSleepSession - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    public void deleteAllSleepDays() {
        MFLogger.d(TAG, "deleteAllSleepSummaries");
        try {
            TableUtils.clearTable(getSleepDao().getConnectionSource(), MFSleepDay.class);
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error inside " + TAG + ".deleteAllSleepSummaries - e=" + e);
        }
    }

    @DexIgnore
    public void deleteAllSleepSessions() {
        MFLogger.d(TAG, "deleteAllSleepSessions");
        try {
            TableUtils.clearTable(getSleepSessionDao().getConnectionSource(), MFSleepSession.class);
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error inside " + TAG + ".deleteAllSleepSessions - e=" + e);
        }
    }

    @DexIgnore
    public boolean deleteSleepSession(long j) {
        String str = TAG;
        MFLogger.d(str, "deleteSleepSession - realEndTime=" + j);
        MFSleepSession sleepSession = getSleepSession(j);
        MFSleepDay sleepDay = getSleepDay(TimeUtils.getStringTimeYYYYMMDD(sleepSession.getDate()));
        if (!(sleepSession == null || sleepDay == null)) {
            MFSleepDay editSleepDayWhenDeleteSleepSession = editSleepDayWhenDeleteSleepSession(sleepDay, sleepSession);
            if (editSleepDayWhenDeleteSleepSession == null || !addSleepDay(editSleepDayWhenDeleteSleepSession)) {
                return false;
            }
            try {
                DeleteBuilder<MFSleepSession, Integer> deleteBuilder = getSleepSessionDao().deleteBuilder();
                deleteBuilder.where().eq(MFSleepSession.COLUMN_REAL_END_TIME, Long.valueOf(j));
                deleteBuilder.delete();
                return true;
            } catch (SQLException e) {
                String str2 = TAG;
                MFLogger.e(str2, "deleteSleepSession - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    public boolean editSleepSession(MFSleepSession mFSleepSession) {
        String str = TAG;
        MFLogger.d(str, "editSleepSession - session=" + mFSleepSession);
        if (mFSleepSession != null) {
            try {
                MFSleepDay sleepDay = getSleepDay(TimeUtils.getStringTimeYYYYMMDD(mFSleepSession.getDate()));
                MFSleepSession sleepSession = getSleepSession((long) mFSleepSession.getRealEndTime());
                if (sleepDay == null || !addSleepDay(editSleepDayWhenEditSleepSession(sleepDay, sleepSession, mFSleepSession))) {
                    return false;
                }
                if (sleepSession != null) {
                    mFSleepSession.setDbRowId(sleepSession.getDbRowId());
                }
                getSleepSessionDao().createOrUpdate(mFSleepSession);
                return true;
            } catch (Exception e) {
                String str2 = TAG;
                MFLogger.e(str2, "editSleepSession - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    public MFSleepGoal getDailySleepGoal(String str) {
        String str2 = TAG;
        MFLogger.d(str2, "getDailySleepGoal - date=" + str);
        try {
            QueryBuilder<MFSleepGoal, Integer> queryBuilder = getSleepGoalDao().queryBuilder();
            queryBuilder.where().le("date", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            String str3 = TAG;
            MFLogger.e(str3, "getDailySleepGoal - e=" + e);
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{MFSleepSession.class, MFSleepDay.class, MFSleepGoal.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 3;
    }

    @DexIgnore
    public int getLastSleepGoal() {
        MFLogger.d(TAG, "getLastSleepGoal");
        try {
            QueryBuilder<MFSleepGoal, Integer> queryBuilder = getSleepGoalDao().queryBuilder();
            queryBuilder.orderBy("date", false);
            MFSleepGoal queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst == null) {
                return 480;
            }
            return queryForFirst.getMinute();
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "getLastSleepGoal - e=" + e);
            return 480;
        }
    }

    @DexIgnore
    public int getLastSleepGoalFromDate(String str) {
        try {
            QueryBuilder<MFSleepGoal, Integer> queryBuilder = getSleepGoalDao().queryBuilder();
            queryBuilder.where().not().gt("date", str);
            queryBuilder.orderBy("date", false);
            MFSleepGoal queryForFirst = queryBuilder.queryForFirst();
            if (queryForFirst == null) {
                return 480;
            }
            return queryForFirst.getMinute();
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.e(str2, "Error inside " + TAG + ".getLastSleepGoal() - e=" + e);
            return 480;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getPendingSleepSessions() {
        MFLogger.d(TAG, "getPendingSleepSessions");
        try {
            QueryBuilder<MFSleepSession, Integer> queryBuilder = getSleepSessionDao().queryBuilder();
            queryBuilder.where().ne("pinType", 0);
            return queryBuilder.query();
        } catch (SQLException e) {
            String str = TAG;
            MFLogger.e(str, "getPendingSleepSessions - e=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    public MFSleepDay getSleepDay(String str) {
        String str2 = TAG;
        MFLogger.d(str2, "getSleepDay - dateTime=" + str);
        try {
            QueryBuilder<MFSleepDay, Integer> queryBuilder = getSleepDao().queryBuilder();
            queryBuilder.where().eq("date", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            String str3 = TAG;
            MFLogger.e(str3, "getSleepDay - dateTime=" + str + ", e=" + e);
            return null;
        }
    }

    @DexIgnore
    public List<MFSleepDay> getSleepDayInRange(String str, String str2) {
        List<MFSleepDay> arrayList = new ArrayList<>();
        try {
            QueryBuilder<MFSleepDay, Integer> queryBuilder = getSleepDao().queryBuilder();
            queryBuilder.where().between("date", str, str2);
            queryBuilder.orderBy("date", true);
            arrayList = queryBuilder.query();
        } catch (Exception e) {
            String str3 = TAG;
            MFLogger.e(str3, "Error inside " + TAG + ".getLastSleepGoal() - e=" + e);
        }
        return arrayList != null ? arrayList : new ArrayList();
    }

    @DexIgnore
    public List<MFSleepDay> getSleepDays(long j, long j2) {
        String str = TAG;
        MFLogger.d(str, "getSleepDays - fromDate=" + j + ", toDate=" + j2);
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<MFSleepDay, Integer> queryBuilder = getSleepDao().queryBuilder();
            queryBuilder.orderBy("date", true);
            Where<MFSleepDay, Integer> where = queryBuilder.where();
            where.between("date", Long.valueOf(j), Long.valueOf(j2));
            List<MFSleepDay> query = where.query();
            if (query == null || query.size() <= 0) {
                return arrayList;
            }
            return query;
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.e(str2, "getSleepDays - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public MFSleepSession getSleepSession(long j) {
        String str = TAG;
        MFLogger.d(str, "getSleepSession - realEndTime=" + j);
        try {
            QueryBuilder<MFSleepSession, Integer> queryBuilder = getSleepSessionDao().queryBuilder();
            queryBuilder.where().eq(MFSleepSession.COLUMN_REAL_END_TIME, Long.valueOf(j));
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            String str2 = TAG;
            Log.e(str2, "getSleepSession - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public int getSleepSessionNumberFromSleepDay(long j) {
        String str = TAG;
        MFLogger.d(str, "getSleepSessionNumberFromSleepDay - time=" + j);
        List<MFSleepSession> sleepSessions = getSleepSessions(TimeUtils.getStartOfDay(new Date(j)).getTime(), TimeUtils.getEndOfDay(new Date(j)).getTime());
        if (sleepSessions == null || sleepSessions.size() <= 0) {
            return 0;
        }
        return sleepSessions.size();
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(long j, long j2) {
        String str = TAG;
        MFLogger.d(str, "getSleepSessions - fromDate=" + j + ", toDate=" + j2);
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<MFSleepSession, Integer> queryBuilder = getSleepSessionDao().queryBuilder();
            queryBuilder.orderBy(MFSleepSession.COLUMN_EDITED_START_TIME, true);
            Where<MFSleepSession, Integer> where = queryBuilder.where();
            where.between("date", Long.valueOf(j), Long.valueOf(j2));
            List<MFSleepSession> query = where.query();
            if (query == null || query.size() <= 0) {
                return arrayList;
            }
            return query;
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.e(str2, "getSleepSessions - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public int getTodaySleepGoal() {
        MFLogger.d(TAG, "getTodaySleepGoal");
        MFSleepGoal dailySleepGoal = getDailySleepGoal(TimeUtils.getStringCurrentTimeYYYYMMDD());
        if (dailySleepGoal == null) {
            return 480;
        }
        return dailySleepGoal.getMinute();
    }

    @DexIgnore
    public MFSleepGoal updateDailySleepGoal(MFSleepGoal mFSleepGoal) {
        String str = TAG;
        MFLogger.d(str, "updateDailySleepGoal - sleepGoal=" + mFSleepGoal);
        try {
            QueryBuilder<MFSleepGoal, Integer> queryBuilder = getSleepGoalDao().queryBuilder();
            queryBuilder.where().eq("date", mFSleepGoal.getDate()).and().eq("timezoneOffset", Integer.valueOf(TimeUtils.getTimezoneOffset()));
            List<MFSleepGoal> query = getSleepGoalDao().query(queryBuilder.prepare());
            if (query != null && query.size() > 0) {
                MFSleepGoal mFSleepGoal2 = query.get(0);
                if (mFSleepGoal2 != null) {
                    mFSleepGoal.setDbRowId(mFSleepGoal2.getDbRowId());
                    mFSleepGoal.setDate(mFSleepGoal2.getDate());
                }
            }
            getSleepGoalDao().createOrUpdate(mFSleepGoal);
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.e(str2, "updateDailySleepGoal - dateTime=" + mFSleepGoal.getDate() + ", e=" + e);
        }
        return mFSleepGoal;
    }

    @DexIgnore
    public void updateSleepDay(MFSleepDay mFSleepDay) {
        String str = TAG;
        MFLogger.d(str, "updateSleepDay - sleepDay=" + mFSleepDay);
        try {
            getSleepDao().update(mFSleepDay);
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.d(str2, "updateSleepDay - e=" + e);
        }
    }

    @DexIgnore
    public void updateSleepSessionPinType(MFSleepSession mFSleepSession, int i) {
        try {
            MFSleepSession sleepSession = getSleepSession((long) mFSleepSession.getRealEndTime());
            if (sleepSession != null) {
                sleepSession.setPinType(i);
                getSleepSessionDao().update(sleepSession);
            }
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "updateSleepSessionPinType - e=" + e);
        }
    }

    @DexIgnore
    public boolean deleteSleepSession(MFSleepSession mFSleepSession) {
        String str = TAG;
        MFLogger.d(str, "deleteSleepSession - objectId=" + mFSleepSession.getObjectId());
        MFSleepDay sleepDay = getSleepDay(TimeUtils.getStringTimeYYYYMMDD(mFSleepSession.getDate()));
        if (sleepDay == null) {
            return false;
        }
        MFSleepDay editSleepDayWhenDeleteSleepSession = editSleepDayWhenDeleteSleepSession(sleepDay, mFSleepSession);
        if (editSleepDayWhenDeleteSleepSession == null || !addSleepDay(editSleepDayWhenDeleteSleepSession)) {
            return false;
        }
        try {
            DeleteBuilder<MFSleepSession, Integer> deleteBuilder = getSleepSessionDao().deleteBuilder();
            deleteBuilder.where().eq("date", Long.valueOf(mFSleepSession.getDate()));
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            String str2 = TAG;
            MFLogger.e(str2, "deleteSleepSession - e=" + e);
            return false;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(long j) {
        String str = TAG;
        MFLogger.d(str, "getSleepSessions - time=" + j);
        ArrayList arrayList = new ArrayList();
        Date date = new Date(j);
        long time = TimeUtils.getStartOfDay(date).getTime();
        long time2 = TimeUtils.getEndOfDay(date).getTime();
        try {
            QueryBuilder<MFSleepSession, Integer> queryBuilder = getSleepSessionDao().queryBuilder();
            queryBuilder.orderBy(MFSleepSession.COLUMN_EDITED_START_TIME, true);
            Where<MFSleepSession, Integer> where = queryBuilder.where();
            where.between("date", Long.valueOf(time), Long.valueOf(time2));
            List<MFSleepSession> query = where.query();
            if (query == null || query.size() <= 0) {
                return arrayList;
            }
            return query;
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.e(str2, "getSleepSessions - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public List<MFSleepSession> getSleepSessions(String str) {
        String str2 = TAG;
        MFLogger.d(str2, "getSleepSessions - day=" + str);
        List arrayList = new ArrayList();
        try {
            QueryBuilder queryBuilder = getSleepSessionDao().queryBuilder();
            queryBuilder.orderBy(MFSleepSession.COLUMN_EDITED_START_TIME, true);
            Where where = queryBuilder.where();
            where.eq("day", str);
            List query = where.query();
            if (query != null && query.size() > 0) {
                arrayList = query;
            }
        } catch (Exception e) {
            String str3 = TAG;
            MFLogger.e(str3, "getSleepSessions - e=" + e);
        }
        String str4 = TAG;
        MFLogger.d(str4, "getSleepSessions - sleepSessions.size=" + arrayList.size());
        return arrayList;
    }
}
