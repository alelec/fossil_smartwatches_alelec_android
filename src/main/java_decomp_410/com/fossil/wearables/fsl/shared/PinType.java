package com.fossil.wearables.fsl.shared;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class PinType {
    @DexIgnore
    public static /* final */ int TYPE_PIN_ADD; // = 1;
    @DexIgnore
    public static /* final */ int TYPE_PIN_DELETE; // = 3;
    @DexIgnore
    public static /* final */ int TYPE_PIN_EDIT; // = 2;
    @DexIgnore
    public static /* final */ int TYPE_PIN_SYNCED; // = 0;
}
