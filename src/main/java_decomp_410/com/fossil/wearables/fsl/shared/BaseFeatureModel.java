package com.fossil.wearables.fsl.shared;

import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class BaseFeatureModel extends BaseModel {
    @DexIgnore
    public static /* final */ String COLUMN_COLOR; // = "color";
    @DexIgnore
    public static /* final */ String COLUMN_ENABLED; // = "enabled";
    @DexIgnore
    public static /* final */ String COLUMN_HAPTIC; // = "haptic";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_TIMESTAMP; // = "timestamp";
    @DexIgnore
    @DatabaseField
    public String color;
    @DexIgnore
    @DatabaseField
    public boolean enabled; // = true;
    @DexIgnore
    @DatabaseField
    public String haptic;
    @DexIgnore
    @DatabaseField
    public int hour;
    @DexIgnore
    @DatabaseField
    public boolean isVibrationOnly;
    @DexIgnore
    @DatabaseField
    public String name;
    @DexIgnore
    @DatabaseField
    public long timestamp;

    @DexIgnore
    public String getColor() {
        return this.color;
    }

    @DexIgnore
    public String getHaptic() {
        return this.haptic;
    }

    @DexIgnore
    public int getHour() {
        return this.hour;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public long getTimestamp() {
        return this.timestamp;
    }

    @DexIgnore
    public boolean isEnabled() {
        return this.enabled;
    }

    @DexIgnore
    public boolean isVibrationOnly() {
        return this.isVibrationOnly;
    }

    @DexIgnore
    public void setColor(String str) {
        this.color = str;
    }

    @DexIgnore
    public void setEnabled(boolean z) {
        this.enabled = z;
    }

    @DexIgnore
    public void setHaptic(String str) {
        this.haptic = str;
    }

    @DexIgnore
    public void setHour(int i) {
        this.hour = i;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setTimestamp(long j) {
        this.timestamp = j;
    }

    @DexIgnore
    public void setVibrationOnly(boolean z) {
        this.isVibrationOnly = z;
    }
}
