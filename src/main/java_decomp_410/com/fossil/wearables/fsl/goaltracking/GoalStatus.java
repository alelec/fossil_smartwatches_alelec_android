package com.fossil.wearables.fsl.goaltracking;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum GoalStatus {
    ACTIVE(1),
    INACTIVE(0),
    REMOVED(-1);
    
    @DexIgnore
    public int value;

    @DexIgnore
    GoalStatus(int i) {
        this.value = i;
    }

    @DexIgnore
    public static GoalStatus fromInt(int i) {
        for (GoalStatus goalStatus : values()) {
            if (goalStatus.getValue() == i) {
                return goalStatus;
            }
        }
        return ACTIVE;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
