package com.fossil.blesdk.utils;

import android.annotation.SuppressLint;
import com.fossil.blesdk.obfuscated.kd4;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class EncryptionAES128 {
    @DexIgnore
    public static /* final */ EncryptionAES128 a; // = new EncryptionAES128();

    @DexIgnore
    public enum Operation {
        ENCRYPT(1),
        DECRYPT(2);
        
        @DexIgnore
        public /* final */ int cipherOptMode;

        @DexIgnore
        Operation(int i) {
            this.cipherOptMode = i;
        }

        @DexIgnore
        public final int getCipherOptMode$blesdk_productionRelease() {
            return this.cipherOptMode;
        }
    }

    @DexIgnore
    public enum Transformation {
        CBC_PKCS5_PADDING("AES/CBC/PKCS5PADDING"),
        CBC_NO_PADDING("AES/CBC/NoPadding"),
        CTR_NO_PADDING("AES/CTR/NoPadding");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        Transformation(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue$blesdk_productionRelease() {
            return this.value;
        }
    }

    @DexIgnore
    @SuppressLint({"TrulyRandom"})
    public final SecretKey a() throws NoSuchAlgorithmException {
        SecureRandom secureRandom = new SecureRandom();
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        instance.init(256, secureRandom);
        SecretKey generateKey = instance.generateKey();
        kd4.a((Object) generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final byte[] b(Transformation transformation, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        kd4.b(transformation, "transformation");
        kd4.b(bArr, "rawKey");
        kd4.b(bArr2, "iv");
        kd4.b(bArr3, "data");
        return a(Operation.ENCRYPT, transformation, bArr, bArr2, bArr3);
    }

    @DexIgnore
    public final byte[] a(Operation operation, Transformation transformation, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        kd4.b(operation, "operation");
        kd4.b(transformation, "transformation");
        kd4.b(bArr, "rawKey");
        kd4.b(bArr2, "iv");
        kd4.b(bArr3, "data");
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        Cipher instance = Cipher.getInstance(transformation.getValue$blesdk_productionRelease());
        instance.init(operation.getCipherOptMode$blesdk_productionRelease(), secretKeySpec, new IvParameterSpec(bArr2));
        byte[] doFinal = instance.doFinal(bArr3);
        kd4.a((Object) doFinal, "cipher.doFinal(data)");
        return doFinal;
    }

    @DexIgnore
    public final byte[] a(Transformation transformation, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        kd4.b(transformation, "transformation");
        kd4.b(bArr, "rawKey");
        kd4.b(bArr2, "iv");
        kd4.b(bArr3, "data");
        return a(Operation.DECRYPT, transformation, bArr, bArr2, bArr3);
    }
}
