package com.fossil.blesdk.device;

import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.x90;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$getHardwareLog$Anon1 extends Lambda implements xc4<qa4, qa4> {
    @DexIgnore
    public static /* final */ DeviceImplementation$getHardwareLog$Anon1 INSTANCE; // = new DeviceImplementation$getHardwareLog$Anon1();

    @DexIgnore
    public DeviceImplementation$getHardwareLog$Anon1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((qa4) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(qa4 qa4) {
        kd4.b(qa4, "it");
        x90.a(da0.l, 0, 1, (Object) null);
    }
}
