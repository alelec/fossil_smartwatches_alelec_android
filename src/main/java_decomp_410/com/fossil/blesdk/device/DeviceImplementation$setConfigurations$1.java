package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$setConfigurations$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.data.config.DeviceConfigItem, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.DeviceImplementation$setConfigurations$1 INSTANCE; // = new com.fossil.blesdk.device.DeviceImplementation$setConfigurations$1();

    @DexIgnore
    public DeviceImplementation$setConfigurations$1() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(com.fossil.blesdk.device.data.config.DeviceConfigItem deviceConfigItem) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceConfigItem, "it");
        return deviceConfigItem.toJSONString(2);
    }
}
