package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$streamingEventListener$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.asyncevent.AsyncEvent, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$streamingEventListener$1(com.fossil.blesdk.device.DeviceImplementation deviceImplementation) {
        super(1);
        this.this$0 = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.asyncevent.AsyncEvent) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.asyncevent.AsyncEvent asyncEvent) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(asyncEvent, "asyncEvent");
        this.this$0.f2351k.mo7209a(this.this$0, asyncEvent);
    }
}
