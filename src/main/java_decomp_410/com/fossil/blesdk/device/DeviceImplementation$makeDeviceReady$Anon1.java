package com.fossil.blesdk.device;

import com.fossil.blesdk.device.Device;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$makeDeviceReady$Anon1 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$makeDeviceReady$Anon1(DeviceImplementation deviceImplementation) {
        super(1);
        this.this$Anon0 = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "it");
        if (this.this$Anon0.getState() != Device.State.CONNECTED) {
            this.this$Anon0.h = true;
            this.this$Anon0.a(Device.State.CONNECTING);
        }
    }
}
