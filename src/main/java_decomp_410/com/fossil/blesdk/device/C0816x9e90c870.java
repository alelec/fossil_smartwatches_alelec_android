package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1 */
public final class C0816x9e90c870 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a */
    public static final class C0817a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0816x9e90c870 f2437e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.Object f2438f;

        @DexIgnore
        public C0817a(com.fossil.blesdk.device.C0816x9e90c870 deviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1, java.lang.Object obj) {
            this.f2437e = deviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2438f = obj;
        }

        @DexIgnore
        public final void run() {
            this.f2437e.$progressTask.mo16948c(this.f2438f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b */
    public static final class C0818b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0816x9e90c870 f2439e;

        @DexIgnore
        public C0818b(com.fossil.blesdk.device.C0816x9e90c870 deviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1) {
            this.f2439e = deviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
        }

        @DexIgnore
        public final void run() {
            this.f2439e.$progressTask.mo16946b(new com.fossil.blesdk.device.FeatureError(com.fossil.blesdk.device.FeatureErrorCode.UNKNOWN_ERROR, (com.fossil.blesdk.device.logic.phase.Phase.Result) null, 2, (com.fossil.blesdk.obfuscated.fd4) null));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c */
    public static final class C0819c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0816x9e90c870 f2440e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2441f;

        @DexIgnore
        public C0819c(com.fossil.blesdk.device.C0816x9e90c870 deviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1, com.fossil.blesdk.device.logic.phase.Phase phase) {
            this.f2440e = deviceImplementation$readRssi$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2441f = phase;
        }

        @DexIgnore
        public final void run() {
            this.f2440e.$progressTask.mo16946b(com.fossil.blesdk.device.FeatureError.Companion.mo6343a(this.f2441f.mo7565k(), com.fossil.blesdk.obfuscated.rb4.m27313a((kotlin.Pair<? extends K, ? extends V>[]) new kotlin.Pair[]{com.fossil.blesdk.obfuscated.oa4.m26076a(com.fossil.blesdk.device.FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED, java.lang.Boolean.valueOf(this.f2440e.this$0.mo5950q()))})));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0816x9e90c870(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(1);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        if (phase.mo7565k().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS) {
            java.lang.Object i = phase.mo7345i();
            if (i instanceof java.lang.Integer) {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0816x9e90c870.C0817a(this, i));
            } else {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0816x9e90c870.C0818b(this));
            }
        } else {
            this.this$0.f2346f.post(new com.fossil.blesdk.device.C0816x9e90c870.C0819c(this, phase));
            this.this$0.mo5919a(phase.mo7565k());
        }
    }
}
