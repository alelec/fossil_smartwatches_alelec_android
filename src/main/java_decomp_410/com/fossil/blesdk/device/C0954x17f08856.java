package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$155 */
public final class C0954x17f08856 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.phase.Phase, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$155$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$155$a */
    public static final class C0955a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0954x17f08856 f2583e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2584f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ float f2585g;

        @DexIgnore
        public C0955a(com.fossil.blesdk.device.C0954x17f08856 deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$155, com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
            this.f2583e = deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$155;
            this.f2584f = phase;
            this.f2585g = f;
        }

        @DexIgnore
        public final void run() {
            com.fossil.blesdk.device.DeviceImplementation deviceImplementation = this.f2583e.this$0;
            com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
            java.lang.String logName$blesdk_productionRelease = this.f2584f.mo7562g().getLogName$blesdk_productionRelease();
            com.fossil.blesdk.device.DeviceImplementation.m2997a(deviceImplementation, logLevel, logName$blesdk_productionRelease, "Progress: " + this.f2585g, false, 8, (java.lang.Object) null);
            this.f2583e.$progressTask.mo16943a(this.f2585g);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0954x17f08856(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(2);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executingPhase");
        this.this$0.f2346f.post(new com.fossil.blesdk.device.C0954x17f08856.C0955a(this, phase, f));
    }
}
