package com.fossil.blesdk.device.event;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceEventManager$processDeviceEvent$2 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.obfuscated.qa4, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation $device;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.asyncevent.AsyncEvent $event;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceEventManager$processDeviceEvent$2(com.fossil.blesdk.device.asyncevent.AsyncEvent asyncEvent, com.fossil.blesdk.device.DeviceImplementation deviceImplementation) {
        super(1);
        this.$event = asyncEvent;
        this.$device = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.obfuscated.qa4) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.obfuscated.qa4 qa4) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(qa4, "it");
        for (com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame backgroundSyncFrame : ((com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent) this.$event).getBackgroundSyncFrames()) {
            com.fossil.blesdk.device.data.file.FileType a = com.fossil.blesdk.device.data.file.FileType.Companion.mo6970a(backgroundSyncFrame.getFileHandle());
            if (a != null) {
                switch (com.fossil.blesdk.obfuscated.b30.f3656a[a.ordinal()]) {
                    case 1:
                        if (backgroundSyncFrame.getAction() != com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction.GET) {
                            break;
                        } else {
                            com.fossil.blesdk.device.DeviceImplementation.m3000b(this.$device, false, true, (com.fossil.blesdk.device.data.enumerate.Priority) null, 4, (java.lang.Object) null);
                            break;
                        }
                    case 2:
                        this.$device.mo5918a((com.fossil.blesdk.device.event.DeviceEvent) new com.fossil.blesdk.device.event.notification.AlarmSyncNotification(this.$event.getEventSequence$blesdk_productionRelease(), backgroundSyncFrame.getAction()));
                        break;
                    case 3:
                        if (backgroundSyncFrame.getAction() != com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction.GET) {
                            break;
                        } else {
                            this.$device.mo5959y();
                            break;
                        }
                    case 4:
                        this.$device.mo5918a((com.fossil.blesdk.device.event.DeviceEvent) new com.fossil.blesdk.device.event.notification.NotificationFilterSyncNotification(this.$event.getEventSequence$blesdk_productionRelease(), backgroundSyncFrame.getAction()));
                        break;
                    case 5:
                        this.$device.mo5918a((com.fossil.blesdk.device.event.DeviceEvent) new com.fossil.blesdk.device.event.notification.DeviceConfigSyncNotification(this.$event.getEventSequence$blesdk_productionRelease(), backgroundSyncFrame.getAction()));
                        break;
                    case 6:
                        if (backgroundSyncFrame.getAction() != com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncAction.GET) {
                            break;
                        } else {
                            com.fossil.blesdk.device.DeviceImplementation.m2991a(this.$device, false, true, (com.fossil.blesdk.device.data.enumerate.Priority) null, 4, (java.lang.Object) null);
                            break;
                        }
                }
            }
        }
    }
}
