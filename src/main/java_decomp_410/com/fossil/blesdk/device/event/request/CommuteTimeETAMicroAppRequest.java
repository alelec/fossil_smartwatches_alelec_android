package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.microapp.response.MicroAppEvent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeETAMicroAppRequest extends MicroAppRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeETAMicroAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeETAMicroAppRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeETAMicroAppRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public CommuteTimeETAMicroAppRequest[] newArray(int i) {
            return new CommuteTimeETAMicroAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeETAMicroAppRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppRequest(byte b, MicroAppEvent microAppEvent) {
        super(DeviceEventId.COMMUTE_TIME_ETA_MICRO_APP, b, microAppEvent);
        kd4.b(microAppEvent, "commuteTimeETAMicroAppEvent");
    }

    @DexIgnore
    public CommuteTimeETAMicroAppRequest(Parcel parcel) {
        super(parcel);
    }
}
