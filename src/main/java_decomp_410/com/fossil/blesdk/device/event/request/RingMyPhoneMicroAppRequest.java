package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.microapp.response.MicroAppEvent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingMyPhoneMicroAppRequest extends MicroAppRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<RingMyPhoneMicroAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public RingMyPhoneMicroAppRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new RingMyPhoneMicroAppRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public RingMyPhoneMicroAppRequest[] newArray(int i) {
            return new RingMyPhoneMicroAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ RingMyPhoneMicroAppRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingMyPhoneMicroAppRequest(byte b, MicroAppEvent microAppEvent) {
        super(DeviceEventId.RING_MY_PHONE_MICRO_APP, b, microAppEvent);
        kd4.b(microAppEvent, "ringMyPhoneMicroAppEvent");
    }

    @DexIgnore
    public RingMyPhoneMicroAppRequest(Parcel parcel) {
        super(parcel);
    }
}
