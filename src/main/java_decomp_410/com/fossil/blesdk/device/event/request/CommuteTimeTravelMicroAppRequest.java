package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.microapp.response.MicroAppEvent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeTravelMicroAppRequest extends MicroAppRequest {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ MicroAppEvent commuteTimeTravelMicroAppEvent;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeTravelMicroAppRequest> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeTravelMicroAppRequest createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeTravelMicroAppRequest(parcel, (fd4) null);
        }

        @DexIgnore
        public CommuteTimeTravelMicroAppRequest[] newArray(int i) {
            return new CommuteTimeTravelMicroAppRequest[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeTravelMicroAppRequest(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final MicroAppEvent getCommuteTimeTravelMicroAppEvent() {
        return this.commuteTimeTravelMicroAppEvent;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppRequest(byte b, MicroAppEvent microAppEvent) {
        super(DeviceEventId.COMMUTE_TIME_TRAVEL_MICRO_APP, b, microAppEvent);
        kd4.b(microAppEvent, "commuteTimeTravelMicroAppEvent");
        this.commuteTimeTravelMicroAppEvent = microAppEvent;
    }

    @DexIgnore
    public CommuteTimeTravelMicroAppRequest(Parcel parcel) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(MicroAppEvent.class.getClassLoader());
        if (readParcelable != null) {
            this.commuteTimeTravelMicroAppEvent = (MicroAppEvent) readParcelable;
        } else {
            kd4.a();
            throw null;
        }
    }
}
