package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceRequest extends DeviceEvent {
    @DexIgnore
    public /* final */ int requestId;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRequest(DeviceEventId deviceEventId, byte b, int i) {
        super(deviceEventId, b);
        kd4.b(deviceEventId, "deviceEventId");
        this.requestId = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.requestId == ((DeviceRequest) obj).requestId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.DeviceRequest");
    }

    @DexIgnore
    public final int getRequestId$blesdk_productionRelease() {
        return this.requestId;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.requestId;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(super.toJSONObject(), JSONKey.REQUEST_ID, Integer.valueOf(this.requestId));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.requestId);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRequest(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        this.requestId = parcel.readInt();
    }
}
