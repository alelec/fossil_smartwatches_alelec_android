package com.fossil.blesdk.device.event;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DeviceEvent extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public /* final */ DeviceEventId deviceEventId;
    @DexIgnore
    public /* final */ byte eventSequence;

    @DexIgnore
    public DeviceEvent(DeviceEventId deviceEventId2, byte b) {
        kd4.b(deviceEventId2, "deviceEventId");
        this.deviceEventId = deviceEventId2;
        this.eventSequence = b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DeviceEvent deviceEvent = (DeviceEvent) obj;
            return this.deviceEventId == deviceEvent.deviceEventId && this.eventSequence == deviceEvent.eventSequence;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.DeviceEvent");
    }

    @DexIgnore
    public final DeviceEventId getDeviceEventId() {
        return this.deviceEventId;
    }

    @DexIgnore
    public final byte getEventSequence$blesdk_productionRelease() {
        return this.eventSequence;
    }

    @DexIgnore
    public int hashCode() {
        return (this.deviceEventId.hashCode() * 31) + this.eventSequence;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.EVENT_ID, this.deviceEventId.getLogName$blesdk_productionRelease()), JSONKey.SEQUENCE, Byte.valueOf(this.eventSequence));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.deviceEventId.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.eventSequence);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DeviceEvent(Parcel parcel) {
        throw null;
        // this(DeviceEventId.valueOf(r0), parcel.readByte());
        // kd4.b(parcel, "parcel");
        // String readString = parcel.readString();
        // if (readString != null) {
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }
}
