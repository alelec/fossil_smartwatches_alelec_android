package com.fossil.blesdk.device.event.request;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.event.DeviceEventId;
import com.fossil.blesdk.model.microapp.response.MicroAppEvent;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppRequest extends DeviceRequest {
    @DexIgnore
    public /* final */ MicroAppEvent microAppEvent;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRequest(DeviceEventId deviceEventId, byte b, MicroAppEvent microAppEvent2) {
        super(deviceEventId, b, n90.b(microAppEvent2.getRequestId()));
        kd4.b(deviceEventId, "deviceEventId");
        kd4.b(microAppEvent2, "microAppEvent");
        this.microAppEvent = microAppEvent2;
    }

    @DexIgnore
    public static /* synthetic */ void microAppEvent$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.microAppEvent, (Object) ((MicroAppRequest) obj).microAppEvent) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
    }

    @DexIgnore
    public final MicroAppEvent getMicroAppEvent$blesdk_productionRelease() {
        return this.microAppEvent;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.microAppEvent.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        throw null;
        // JSONObject jSONObject = super.toJSONObject();
        // try {
        //     wa0.a(jSONObject, JSONKey.MICRO_APP_EVENT, this.microAppEvent);
        // } catch (JSONException e) {
        //     da0.l.a(e);
        // }
        // return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.microAppEvent, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRequest(Parcel parcel) {
        super(parcel);
        kd4.b(parcel, "parcel");
        Parcelable readParcelable = parcel.readParcelable(MicroAppEvent.class.getClassLoader());
        if (readParcelable != null) {
            this.microAppEvent = (MicroAppEvent) readParcelable;
        } else {
            kd4.a();
            throw null;
        }
    }
}
