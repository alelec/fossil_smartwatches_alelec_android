package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.phase.Phase, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements java.lang.Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111 e;
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f;
        @DexIgnore
        public /* final */ /* synthetic */ float g;

        @DexIgnore
        public a(com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111 deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111, com.fossil.blesdk.device.logic.phase.Phase phase, float f2) {
            this.e = deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111;
            this.f = phase;
            this.g = f2;
        }

        @DexIgnore
        public final void run() {
            com.fossil.blesdk.device.DeviceImplementation deviceImplementation = this.e.this$0;
            com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
            java.lang.String logName$blesdk_productionRelease = this.f.g().getLogName$blesdk_productionRelease();
            com.fossil.blesdk.device.DeviceImplementation.a(deviceImplementation, logLevel, logName$blesdk_productionRelease, "Progress: " + this.g, false, 8, (java.lang.Object) null);
            this.e.$progressTask.a(this.g);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(2);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
        com.fossil.blesdk.obfuscated.kd4.b(phase, "executingPhase");
        this.this$0.f.post(new com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$111.a(this, phase, f));
    }
}
