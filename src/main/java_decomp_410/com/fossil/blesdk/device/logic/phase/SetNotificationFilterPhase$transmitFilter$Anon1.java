package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetNotificationFilterPhase$transmitFilter$Anon1 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ SetNotificationFilterPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetNotificationFilterPhase$transmitFilter$Anon1(SetNotificationFilterPhase setNotificationFilterPhase) {
        super(1);
        this.this$Anon0 = setNotificationFilterPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "it");
        SetNotificationFilterPhase setNotificationFilterPhase = this.this$Anon0;
        setNotificationFilterPhase.a(Phase.Result.copy$default(setNotificationFilterPhase.k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
    }
}
