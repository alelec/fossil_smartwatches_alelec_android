package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.q60;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DisconnectPhase extends Phase {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DisconnectPhase(Peripheral peripheral, Phase.a aVar) {
        super(peripheral, aVar, PhaseId.DISCONNECT, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new q60(j()), (xc4) DisconnectPhase$disconnect$Anon1.INSTANCE, (xc4) DisconnectPhase$disconnect$Anon2.INSTANCE, (yc4) null, (xc4) new DisconnectPhase$disconnect$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public void t() {
        A();
    }
}
