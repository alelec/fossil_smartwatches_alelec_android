package com.fossil.blesdk.device.logic.request.code;

import com.fossil.blesdk.obfuscated.kd4;
import java.nio.ByteBuffer;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DeviceConfigOperationCode {
    GET_CONNECTION_PARAMETERS(DeviceConfigOperationId.GET, DeviceConfigParameterId.CURRENT_CONNECTION_PARAMETERS, (DeviceConfigOperationId) null, (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 60, (byte[]) null),
    REQUEST_CONNECTION_PRIORITY(DeviceConfigOperationId.SET, DeviceConfigParameterId.CONNECTION_PARAMETERS_REQUEST, (DeviceConfigOperationId) null, (DeviceConfigParameterId) null, DeviceConfigParameterId.CURRENT_CONNECTION_PARAMETERS, (DeviceConfigOperationId) null, 44, (byte[]) null),
    PLAY_ANIMATION(DeviceConfigOperationId.SET, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, DiagnosticFunctionParametersData.PAIR_ANIMATION.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    GET_OPTIMAL_PAYLOAD(DeviceConfigOperationId.GET, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, DiagnosticFunctionParametersData.OPTIMAL_PAYLOAD.getData(), (DeviceConfigParameterId) null, (byte[]) null, DiagnosticFunctionParametersData.OPTIMAL_PAYLOAD.getData(), 24, (byte[]) null),
    REQUEST_HANDS(DeviceConfigOperationId.SET, DeviceConfigParameterId.CALIBRATION_SETTING, HandConfigs.REQUEST_HANDS.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    RELEASE_HANDS(DeviceConfigOperationId.SET, DeviceConfigParameterId.CALIBRATION_SETTING, HandConfigs.RELEASE_HANDS.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    MOVE_HANDS(DeviceConfigOperationId.SET, DeviceConfigParameterId.CALIBRATION_SETTING, HandConfigs.MOVE_HANDS.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    SET_CALIBRATION_POSITION(DeviceConfigOperationId.SET, DeviceConfigParameterId.HARDWARE_TEST, HandConfigs.SET_CALIBRATION_POSITION.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    GET_CURRENT_WORKOUT_SESSION(DeviceConfigOperationId.GET, DeviceConfigParameterId.WORKOUT_SESSION_PRIMARY_ID, WorkoutSessionParameterData.WORKOUT_SESSION_CONTROL.getData(), (DeviceConfigParameterId) null, (byte[]) null, WorkoutSessionParameterData.WORKOUT_SESSION_CONTROL.getData(), 24, (byte[]) null),
    STOP_CURRENT_WORKOUT_SESSION(DeviceConfigOperationId.SET, DeviceConfigParameterId.WORKOUT_SESSION_PRIMARY_ID, WorkoutSessionParameterData.WORKOUT_SESSION_CONTROL.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    GET_HEARTBEAT_STATISTIC(DeviceConfigOperationId.GET, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, DiagnosticFunctionParametersData.HEARTBEAT_STATISTIC.getData(), DeviceConfigOperationId.RESPONSE, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, DiagnosticFunctionParametersData.HEARTBEAT_STATISTIC.getData()),
    GET_HEARTBEAT_INTERVAL(DeviceConfigOperationId.GET, DeviceConfigParameterId.STREAMING_CONFIG, StreamingConfigParametersData.HEARTBEAT_INTERVAL.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    SET_HEARTBEAT_INTERVAL(DeviceConfigOperationId.SET, DeviceConfigParameterId.STREAMING_CONFIG, StreamingConfigParametersData.HEARTBEAT_INTERVAL.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    REQUEST_DISCOVER_SERVICE(DeviceConfigOperationId.SET, DeviceConfigParameterId.ADVANCE_BLE_REQUEST, AdvanceBleParameterData.REQUEST_DISCOVER_SERVICE.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    CLEAN_UP_DEVICE(DeviceConfigOperationId.SET, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, AdvanceBleParameterData.CLEAN_UP_DEVICE.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null),
    LEGACY_OTA_ENTER(DeviceConfigOperationId.SET, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, OtaLegacyConfigParametersData.LEGACY_OTA_ENTER.getData(), (DeviceConfigParameterId) null, (byte[]) null, OtaLegacyConfigParametersData.LEGACY_OTA_ENTER_RESPONSE.getData(), 24, (byte[]) null),
    LEGACY_OTA_RESET(DeviceConfigOperationId.SET, DeviceConfigParameterId.DIAGNOSTIC_FUNCTIONS, OtaLegacyConfigParametersData.LEGACY_OTA_RESET.getData(), (DeviceConfigParameterId) null, (byte[]) null, (DeviceConfigOperationId) null, 56, (byte[]) null);
    
    @DexIgnore
    public /* final */ DeviceConfigOperationId operationId;
    @DexIgnore
    public /* final */ byte[] parameterData;
    @DexIgnore
    public /* final */ DeviceConfigParameterId parameterId;
    @DexIgnore
    public /* final */ DeviceConfigOperationId responseOperationId;
    @DexIgnore
    public /* final */ byte[] responseParameterData;
    @DexIgnore
    public /* final */ DeviceConfigParameterId responseParameterId;

    @DexIgnore
    public enum AdvanceBleParameterData {
        REQUEST_DISCOVER_SERVICE(new byte[]{1}),
        CLEAN_UP_DEVICE(new byte[]{35, 0, 1, 0, 0, 0});
        
        @DexIgnore
        public /* final */ byte[] data;

        @DexIgnore
        AdvanceBleParameterData(byte[] bArr) {
            this.data = bArr;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.data;
        }
    }

    @DexIgnore
    public enum DeviceConfigOperationId {
        GET((byte) 1),
        SET((byte) 2),
        RESPONSE((byte) 3);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        DeviceConfigOperationId(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId() {
            return this.id;
        }
    }

    @DexIgnore
    public enum DeviceConfigParameterId {
        WORKOUT_SESSION_PRIMARY_ID((byte) 3),
        CONNECTION_PARAMETERS_REQUEST((byte) 9),
        CURRENT_CONNECTION_PARAMETERS((byte) 10),
        STREAMING_CONFIG((byte) 12),
        CALIBRATION_SETTING(DateTimeFieldType.SECOND_OF_MINUTE),
        ADVANCE_BLE_REQUEST(DateTimeFieldType.MILLIS_OF_SECOND),
        DIAGNOSTIC_FUNCTIONS((byte) 241),
        HARDWARE_TEST((byte) 242);
        
        @DexIgnore
        public /* final */ byte id;

        @DexIgnore
        DeviceConfigParameterId(byte b) {
            this.id = b;
        }

        @DexIgnore
        public final byte getId() {
            return this.id;
        }
    }

    @DexIgnore
    public enum DiagnosticFunctionParametersData {
        PAIR_ANIMATION(new byte[]{5}),
        OPTIMAL_PAYLOAD(new byte[]{40}),
        HEARTBEAT_STATISTIC(new byte[]{41});
        
        @DexIgnore
        public /* final */ byte[] data;

        @DexIgnore
        DiagnosticFunctionParametersData(byte[] bArr) {
            this.data = bArr;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.data;
        }
    }

    @DexIgnore
    public enum HandConfigs {
        REQUEST_HANDS(new byte[]{1}),
        RELEASE_HANDS(new byte[]{2}),
        MOVE_HANDS(new byte[]{3}),
        SET_CALIBRATION_POSITION(new byte[]{DateTimeFieldType.HOUR_OF_HALFDAY});
        
        @DexIgnore
        public /* final */ byte[] data;

        @DexIgnore
        HandConfigs(byte[] bArr) {
            this.data = bArr;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.data;
        }
    }

    @DexIgnore
    public enum OtaLegacyConfigParametersData {
        LEGACY_OTA_ENTER(new byte[]{8}),
        LEGACY_OTA_ENTER_RESPONSE(new byte[]{9}),
        LEGACY_OTA_RESET(new byte[]{10});
        
        @DexIgnore
        public /* final */ byte[] data;

        @DexIgnore
        OtaLegacyConfigParametersData(byte[] bArr) {
            this.data = bArr;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.data;
        }
    }

    @DexIgnore
    public enum StreamingConfigParametersData {
        HEARTBEAT_INTERVAL(new byte[]{(byte) 240});
        
        @DexIgnore
        public /* final */ byte[] data;

        @DexIgnore
        StreamingConfigParametersData(byte[] bArr) {
            this.data = bArr;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.data;
        }
    }

    @DexIgnore
    public enum WorkoutSessionParameterData {
        WORKOUT_SESSION_CONTROL(new byte[]{2});
        
        @DexIgnore
        public /* final */ byte[] data;

        @DexIgnore
        WorkoutSessionParameterData(byte[] bArr) {
            this.data = bArr;
        }

        @DexIgnore
        public final byte[] getData() {
            return this.data;
        }
    }

    @DexIgnore
    DeviceConfigOperationCode(DeviceConfigOperationId deviceConfigOperationId, DeviceConfigParameterId deviceConfigParameterId, byte[] bArr, DeviceConfigOperationId deviceConfigOperationId2, DeviceConfigParameterId deviceConfigParameterId2, byte[] bArr2) {
        this.operationId = deviceConfigOperationId;
        this.parameterId = deviceConfigParameterId;
        this.parameterData = bArr;
        this.responseOperationId = deviceConfigOperationId2;
        this.responseParameterId = deviceConfigParameterId2;
        this.responseParameterData = bArr2;
    }

    @DexIgnore
    public final byte[] getOperationCode() {
        byte[] array = ByteBuffer.allocate(this.parameterData.length + 2).put(this.operationId.getId()).put(this.parameterId.getId()).put(this.parameterData).array();
        kd4.a((Object) array, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] getResponseOperationCode() {
        byte[] array = ByteBuffer.allocate(this.responseParameterData.length + 2).put(this.responseOperationId.getId()).put(this.responseParameterId.getId()).put(this.responseParameterData).array();
        kd4.a((Object) array, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        return array;
    }
}
