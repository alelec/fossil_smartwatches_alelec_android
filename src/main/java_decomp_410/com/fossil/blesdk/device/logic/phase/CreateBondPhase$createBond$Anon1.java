package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n60;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CreateBondPhase$createBond$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ CreateBondPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CreateBondPhase$createBond$Anon1(CreateBondPhase createBondPhase) {
        super(1);
        this.this$Anon0 = createBondPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        this.this$Anon0.z = ((n60) request).B();
    }
}
