package com.fossil.blesdk.device.logic.data.connectionparameter;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectionParameters extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ int interval;
    @DexIgnore
    public /* final */ int latency;
    @DexIgnore
    public /* final */ int timeout;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ConnectionParameters> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ConnectionParameters createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ConnectionParameters(parcel, (fd4) null);
        }

        @DexIgnore
        public ConnectionParameters[] newArray(int i) {
            return new ConnectionParameters[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ConnectionParameters(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getInterval() {
        return this.interval;
    }

    @DexIgnore
    public final int getLatency() {
        return this.latency;
    }

    @DexIgnore
    public final int getTimeout() {
        return this.timeout;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.INTERVAL, Integer.valueOf(this.interval)), JSONKey.LATENCY, Integer.valueOf(this.latency)), JSONKey.TIMEOUT, Integer.valueOf(this.timeout));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.interval);
        }
        if (parcel != null) {
            parcel.writeInt(this.latency);
        }
        if (parcel != null) {
            parcel.writeInt(this.timeout);
        }
    }

    @DexIgnore
    public ConnectionParameters(int i, int i2, int i3) {
        this.interval = i;
        this.latency = i2;
        this.timeout = i3;
    }

    @DexIgnore
    public ConnectionParameters(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
    }
}
