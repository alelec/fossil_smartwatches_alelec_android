package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrySetConnectionParamsPhase$trySetConnectionParameters$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ TrySetConnectionParamsPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TrySetConnectionParamsPhase$trySetConnectionParameters$Anon2(TrySetConnectionParamsPhase trySetConnectionParamsPhase) {
        super(1);
        this.this$Anon0 = trySetConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "it");
        TrySetConnectionParamsPhase trySetConnectionParamsPhase = this.this$Anon0;
        trySetConnectionParamsPhase.z = trySetConnectionParamsPhase.z + 1;
        this.this$Anon0.B();
    }
}
