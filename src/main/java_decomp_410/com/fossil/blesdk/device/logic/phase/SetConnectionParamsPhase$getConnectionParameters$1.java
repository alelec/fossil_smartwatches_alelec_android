package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetConnectionParamsPhase$getConnectionParameters$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.SetConnectionParamsPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetConnectionParamsPhase$getConnectionParameters$1(com.fossil.blesdk.device.logic.phase.SetConnectionParamsPhase setConnectionParamsPhase) {
        super(1);
        this.this$0 = setConnectionParamsPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        this.this$0.f3027z = ((com.fossil.blesdk.obfuscated.u70) request).mo16680I();
        com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters B = this.this$0.mo7626B();
        if (B == null) {
            com.fossil.blesdk.device.logic.phase.SetConnectionParamsPhase setConnectionParamsPhase = this.this$0;
            setConnectionParamsPhase.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(setConnectionParamsPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.FLOW_BROKEN, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
        } else if (com.fossil.blesdk.obfuscated.a50.f3362a.mo8540a(B, this.this$0.f3026B)) {
            com.fossil.blesdk.device.logic.phase.SetConnectionParamsPhase setConnectionParamsPhase2 = this.this$0;
            setConnectionParamsPhase2.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(setConnectionParamsPhase2.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
        } else {
            this.this$0.mo7627C();
        }
    }
}
