package com.fossil.blesdk.device.logic.request;

import android.os.Handler;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.log.debuglog.LogLevel;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.a70;
import com.fossil.blesdk.obfuscated.b20;
import com.fossil.blesdk.obfuscated.b70;
import com.fossil.blesdk.obfuscated.c20;
import com.fossil.blesdk.obfuscated.d20;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ea0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb0;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import com.fossil.blesdk.obfuscated.n10;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o10;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.misfit.frameworks.buttonservice.model.Mapping;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Request {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public long e;
    @DexIgnore
    public SdkLogEntry f;
    @DexIgnore
    public /* final */ ArrayList<ResponseInfo> g;
    @DexIgnore
    public long h;
    @DexIgnore
    public b i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public BluetoothCommand k;
    @DexIgnore
    public CopyOnWriteArrayList<xc4<Request, qa4>> l;
    @DexIgnore
    public CopyOnWriteArrayList<xc4<Request, qa4>> m;
    @DexIgnore
    public CopyOnWriteArrayList<xc4<Request, qa4>> n;
    @DexIgnore
    public CopyOnWriteArrayList<yc4<Request, Float, qa4>> o;
    @DexIgnore
    public /* final */ wc4<qa4> p;
    @DexIgnore
    public /* final */ Peripheral.a q;
    @DexIgnore
    public /* final */ Peripheral.b r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public Result v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public /* final */ RequestId x;
    @DexIgnore
    public /* final */ Peripheral y;
    @DexIgnore
    public /* final */ int z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public boolean e;
        @DexIgnore
        public /* final */ wc4<qa4> f;

        @DexIgnore
        public b(Request request, wc4<qa4> wc4) {
            kd4.b(wc4, "onTimeOut");
            this.f = wc4;
        }

        @DexIgnore
        public final void a() {
            this.e = true;
        }

        @DexIgnore
        public void run() {
            if (!this.e) {
                this.f.invoke();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Peripheral.a {
        @DexIgnore
        public /* final */ /* synthetic */ Request a;

        @DexIgnore
        public c(Request request) {
            this.a = request;
        }

        @DexIgnore
        public void a(BluetoothCommand bluetoothCommand) {
            kd4.b(bluetoothCommand, Constants.COMMAND);
            if (kd4.a((Object) this.a.k, (Object) bluetoothCommand)) {
                this.a.k = null;
                this.a.c(bluetoothCommand);
            }
        }

        @DexIgnore
        public void b(BluetoothCommand bluetoothCommand) {
            kd4.b(bluetoothCommand, Constants.COMMAND);
            if (kd4.a((Object) this.a.k, (Object) bluetoothCommand)) {
                this.a.k = null;
                this.a.d(bluetoothCommand);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Peripheral.b {
        @DexIgnore
        public /* final */ /* synthetic */ Request a;

        @DexIgnore
        public d(Request request) {
            this.a = request;
        }

        @DexIgnore
        public void a(b20 b20) {
            kd4.b(b20, "notification");
            if (b20 instanceof c20) {
                c20 c20 = (c20) b20;
                if (this.a.a(c20) > 0) {
                    Request request = this.a;
                    request.a(request.a(c20));
                    Request request2 = this.a;
                    request2.a(request2.d());
                    this.a.a(new ResponseInfo(0, c20.a(), c20.b(), wa0.a(new JSONObject(), JSONKey.PROPOSED_TIMEOUT, Long.valueOf(this.a.m())), 1, (fd4) null));
                    return;
                }
            }
            this.a.a(b20);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public Request(RequestId requestId, Peripheral peripheral, int i2) {
        RequestId requestId2 = requestId;
        Peripheral peripheral2 = peripheral;
        kd4.b(requestId2, "id");
        kd4.b(peripheral2, "peripheral");
        this.x = requestId2;
        this.y = peripheral2;
        this.z = i2;
        this.a = this.x.getLogName$blesdk_productionRelease();
        String uuid = UUID.randomUUID().toString();
        kd4.a((Object) uuid, "UUID.randomUUID().toString()");
        this.b = uuid;
        this.c = "";
        this.d = "";
        this.e = System.currentTimeMillis();
        System.currentTimeMillis();
        this.f = new SdkLogEntry(this.x.getLogName$blesdk_productionRelease(), EventType.REQUEST, this.y.k(), this.c, this.d, false, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(new JSONObject(), JSONKey.REQUEST_UUID, this.b), 448, (fd4) null);
        this.g = new ArrayList<>();
        this.h = 3000;
        this.j = hb0.a.a();
        this.l = new CopyOnWriteArrayList<>();
        this.m = new CopyOnWriteArrayList<>();
        this.n = new CopyOnWriteArrayList<>();
        this.o = new CopyOnWriteArrayList<>();
        this.p = new Request$actionOnRequestTimeout$Anon1(this);
        this.q = new c(this);
        this.r = new d(this);
        this.v = new Result(this.x, Result.ResultCode.NOT_START, (BluetoothCommand.Result) null, (o70) null, 12, (fd4) null);
        this.w = true;
    }

    @DexIgnore
    public long a(c20 c20) {
        kd4.b(c20, "notification");
        return 0;
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
    }

    @DexIgnore
    public void b(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "value");
        this.d = str;
        SdkLogEntry sdkLogEntry = this.f;
        if (sdkLogEntry != null) {
            sdkLogEntry.setPhaseUuid(this.d);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r1 != null) goto L_0x0024;
     */
    @DexIgnore
    public final void c() {
        JSONObject jSONObject;
        if (this.t || this.u) {
            b();
            return;
        }
        this.k = h();
        BluetoothCommand bluetoothCommand = this.k;
        if (bluetoothCommand != null) {
            if (bluetoothCommand != null) {
                SdkLogEntry sdkLogEntry = this.f;
                if (sdkLogEntry != null) {
                    jSONObject = sdkLogEntry.getExtraData();
                }
                jSONObject = new JSONObject();
                JSONObject a2 = m90.a(m90.a(jSONObject, bluetoothCommand.a(false)), b(bluetoothCommand));
                SdkLogEntry sdkLogEntry2 = this.f;
                if (sdkLogEntry2 != null) {
                    sdkLogEntry2.setExtraData(a2);
                }
            }
            Peripheral peripheral = this.y;
            BluetoothCommand bluetoothCommand2 = this.k;
            if (bluetoothCommand2 != null) {
                peripheral.a(bluetoothCommand2);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            s();
        }
    }

    @DexIgnore
    public final wc4<qa4> d() {
        return this.p;
    }

    @DexIgnore
    public final Handler e() {
        return this.j;
    }

    @DexIgnore
    public final RequestId f() {
        return this.x;
    }

    @DexIgnore
    public boolean g() {
        return this.s;
    }

    @DexIgnore
    public abstract BluetoothCommand h();

    @DexIgnore
    public final Peripheral i() {
        return this.y;
    }

    @DexIgnore
    public final String j() {
        return this.c;
    }

    @DexIgnore
    public final String k() {
        return this.d;
    }

    @DexIgnore
    public final SdkLogEntry l() {
        return this.f;
    }

    @DexIgnore
    public long m() {
        return this.h;
    }

    @DexIgnore
    public final Result n() {
        return this.v;
    }

    @DexIgnore
    public final int o() {
        return this.z;
    }

    @DexIgnore
    public boolean p() {
        return this.w;
    }

    @DexIgnore
    public final String q() {
        return this.a;
    }

    @DexIgnore
    public final boolean r() {
        return this.t;
    }

    @DexIgnore
    public abstract void s();

    @DexIgnore
    public JSONObject t() {
        return new JSONObject();
    }

    @DexIgnore
    public JSONObject u() {
        return new JSONObject();
    }

    @DexIgnore
    public final void v() {
        if (!this.t) {
            Peripheral peripheral = this.y;
            LogLevel logLevel = LogLevel.DEBUG;
            String str = this.a;
            Peripheral.a(peripheral, logLevel, str, "Request started: " + t().toString(2), false, 8, (Object) null);
            long currentTimeMillis = System.currentTimeMillis();
            SdkLogEntry sdkLogEntry = this.f;
            if (sdkLogEntry != null) {
                sdkLogEntry.setTimeStamp(currentTimeMillis);
            }
            SdkLogEntry sdkLogEntry2 = this.f;
            if (sdkLogEntry2 != null) {
                sdkLogEntry2.setExtraData(m90.a(sdkLogEntry2.getExtraData(), wa0.a(wa0.a(wa0.a(t(), JSONKey.CREATED_AT, Double.valueOf(n90.a(this.e))), JSONKey.STARTED_AT, Double.valueOf(n90.a(currentTimeMillis))), JSONKey.REQUEST_TIMEOUT_IN_MS, Long.valueOf(m()))));
            }
            a(this.p);
            this.y.a(this.q);
            this.y.a(this.r);
            c();
        }
    }

    @DexIgnore
    public final void w() {
        a(Result.ResultCode.INTERRUPTED);
    }

    @DexIgnore
    public final void x() {
        b bVar = this.i;
        if (bVar != null) {
            this.j.removeCallbacks(bVar);
        }
        b bVar2 = this.i;
        if (bVar2 != null) {
            bVar2.a();
        }
        this.i = null;
    }

    @DexIgnore
    public final void y() {
        SdkLogEntry sdkLogEntry = this.f;
        if (sdkLogEntry != null) {
            wa0.a(sdkLogEntry.getExtraData(), JSONKey.COMPLETED_AT, Double.valueOf(n90.a(System.currentTimeMillis())));
            if (p()) {
                da0.l.b(sdkLogEntry);
            }
            this.f = null;
        }
    }

    @DexIgnore
    public final void z() {
        if (p()) {
            da0.l.b(a());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ResponseInfo extends JSONAbleObject {
        @DexIgnore
        public GattCharacteristic.CharacteristicId characteristicId;
        @DexIgnore
        public /* final */ JSONObject extraInfo;
        @DexIgnore
        public byte[] rawData;
        @DexIgnore
        public /* final */ long timeStamp;

        @DexIgnore
        public ResponseInfo() {
            this(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, (JSONObject) null, 15, (fd4) null);
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ ResponseInfo(long j, GattCharacteristic.CharacteristicId characteristicId2, byte[] bArr, JSONObject jSONObject, int i, fd4 fd4) {
            this((i & 1) != 0 ? System.currentTimeMillis() : j, (i & 2) != 0 ? null : characteristicId2, (i & 4) != 0 ? new byte[0] : bArr, (i & 8) != 0 ? new JSONObject() : jSONObject);
        }

        @DexIgnore
        public final GattCharacteristic.CharacteristicId getCharacteristicId() {
            return this.characteristicId;
        }

        @DexIgnore
        public final JSONObject getExtraInfo() {
            return this.extraInfo;
        }

        @DexIgnore
        public final byte[] getRawData() {
            return this.rawData;
        }

        @DexIgnore
        public final long getTimeStamp() {
            return this.timeStamp;
        }

        @DexIgnore
        public final void setCharacteristicId(GattCharacteristic.CharacteristicId characteristicId2) {
            this.characteristicId = characteristicId2;
        }

        @DexIgnore
        public final void setRawData(byte[] bArr) {
            kd4.b(bArr, "<set-?>");
            this.rawData = bArr;
        }

        @DexIgnore
        public JSONObject toJSONObject() {
            JSONObject a = wa0.a(new JSONObject(), JSONKey.TIMESTAMP, Double.valueOf(n90.a(this.timeStamp)));
            JSONKey jSONKey = JSONKey.CHANNEL_ID;
            GattCharacteristic.CharacteristicId characteristicId2 = this.characteristicId;
            return m90.a(wa0.a(wa0.a(a, jSONKey, characteristicId2 != null ? characteristicId2.getLogName$blesdk_productionRelease() : null), JSONKey.RAW_DATA, k90.a(this.rawData, (String) null, 1, (Object) null)), this.extraInfo);
        }

        @DexIgnore
        public ResponseInfo(long j, GattCharacteristic.CharacteristicId characteristicId2, byte[] bArr, JSONObject jSONObject) {
            kd4.b(bArr, "rawData");
            kd4.b(jSONObject, Mapping.COLUMN_EXTRA_INFO);
            this.timeStamp = j;
            this.characteristicId = characteristicId2;
            this.rawData = bArr;
            this.extraInfo = jSONObject;
        }
    }

    @DexIgnore
    public void d(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.v = Result.copy$default(this.v, (RequestId) null, Result.Companion.a(bluetoothCommand.e()).getResultCode(), bluetoothCommand.e(), (o70) null, 9, (Object) null);
        SdkLogEntry sdkLogEntry = this.f;
        if (sdkLogEntry != null) {
            sdkLogEntry.setSuccess(true);
        }
        SdkLogEntry sdkLogEntry2 = this.f;
        if (sdkLogEntry2 != null) {
            JSONObject extraData = sdkLogEntry2.getExtraData();
            if (extraData != null) {
                wa0.a(extraData, JSONKey.MESSAGE, Result.ResultCode.SUCCESS.getLogName$blesdk_productionRelease());
            }
        }
        if (this.v.getResultCode() == Result.ResultCode.SUCCESS) {
            a(bluetoothCommand);
        }
    }

    @DexIgnore
    public final void b(Result result) {
        kd4.b(result, "<set-?>");
        this.v = result;
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "value");
        this.c = str;
        SdkLogEntry sdkLogEntry = this.f;
        if (sdkLogEntry != null) {
            sdkLogEntry.setPhaseName(this.c);
        }
    }

    @DexIgnore
    public void b(boolean z2) {
        this.w = z2;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result extends JSONAbleObject {
        @DexIgnore
        public static /* final */ a Companion; // = new a((fd4) null);
        @DexIgnore
        public /* final */ BluetoothCommand.Result commandResult;
        @DexIgnore
        public /* final */ RequestId requestId;
        @DexIgnore
        public /* final */ o70 responseStatus;
        @DexIgnore
        public /* final */ ResultCode resultCode;

        @DexIgnore
        public enum ResultCode {
            SUCCESS(0),
            NOT_START(1),
            COMMAND_ERROR(2),
            RESPONSE_ERROR(3),
            TIMEOUT(4),
            EOF_TIME_OUT(5),
            CONNECTION_DROPPED(6),
            MISS_PACKAGE(7),
            INVALID_DATA_LENGTH(8),
            RECEIVED_DATA_CRC_MISS_MATCH(9),
            INVALID_RESPONSE_LENGTH(10),
            INVALID_RESPONSE_DATA(11),
            REQUEST_UNSUPPORTED(12),
            UNSUPPORTED_FILE_HANDLE(13),
            BLUETOOTH_OFF(14),
            WRONG_AUTHENTICATION_KEY_TYPE(15),
            HID_PROXY_NOT_CONNECTED(256),
            HID_FAIL_TO_INVOKE_PRIVATE_METHOD(257),
            HID_INPUT_DEVICE_DISABLED(258),
            HID_UNKNOWN_ERROR(511),
            INTERRUPTED(254),
            UNKNOWN_ERROR(255);
            
            @DexIgnore
            public static /* final */ a Companion; // = null;
            @DexIgnore
            public /* final */ int id;
            @DexIgnore
            public /* final */ String logName;

            @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a {
                @DexIgnore
                public a() {
                }

                @DexIgnore
                public final ResultCode a(BluetoothCommand.Result result) {
                    kd4.b(result, "commandResult");
                    switch (a70.a[result.getResultCode().ordinal()]) {
                        case 1:
                            return ResultCode.SUCCESS;
                        case 2:
                            return ResultCode.INTERRUPTED;
                        case 3:
                            return ResultCode.TIMEOUT;
                        case 4:
                            return ResultCode.CONNECTION_DROPPED;
                        case 5:
                            return ResultCode.BLUETOOTH_OFF;
                        case 6:
                            return ResultCode.HID_PROXY_NOT_CONNECTED;
                        case 7:
                            return ResultCode.HID_FAIL_TO_INVOKE_PRIVATE_METHOD;
                        case 8:
                            return ResultCode.HID_INPUT_DEVICE_DISABLED;
                        case 9:
                            return ResultCode.HID_UNKNOWN_ERROR;
                        case 10:
                            int bluetoothGattStatus = result.getGattResult().getBluetoothGattStatus();
                            if (bluetoothGattStatus == 1) {
                                return ResultCode.UNSUPPORTED_FILE_HANDLE;
                            }
                            if (bluetoothGattStatus != 6) {
                                return ResultCode.COMMAND_ERROR;
                            }
                            return ResultCode.REQUEST_UNSUPPORTED;
                        default:
                            return ResultCode.COMMAND_ERROR;
                    }
                }

                @DexIgnore
                public /* synthetic */ a(fd4 fd4) {
                    this();
                }
            }

            /*
            static {
                Companion = new a((fd4) null);
            }
            */

            @DexIgnore
            ResultCode(int i) {
                this.id = i;
                String name = name();
                Locale locale = Locale.US;
                kd4.a((Object) locale, "Locale.US");
                if (name != null) {
                    String lowerCase = name.toLowerCase(locale);
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    this.logName = lowerCase;
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }

            @DexIgnore
            public final int getId() {
                return this.id;
            }

            @DexIgnore
            public final String getLogName$blesdk_productionRelease() {
                return this.logName;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final Result a(BluetoothCommand.Result result) {
                kd4.b(result, "commandResult");
                return new Result((RequestId) null, ResultCode.Companion.a(result), result, (o70) null, 9, (fd4) null);
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }

            @DexIgnore
            public final Result a(o70 o70) {
                ResultCode resultCode;
                kd4.b(o70, "responseStatus");
                if (!o70.isSuccessCode()) {
                    resultCode = ResultCode.RESPONSE_ERROR;
                } else {
                    resultCode = ResultCode.SUCCESS;
                }
                return new Result((RequestId) null, resultCode, (BluetoothCommand.Result) null, o70, 5, (fd4) null);
            }
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(RequestId requestId2, ResultCode resultCode2, BluetoothCommand.Result result, o70 o70, int i, fd4 fd4) {
            this((i & 1) != 0 ? RequestId.UNKNOWN : requestId2, resultCode2, (i & 4) != 0 ? new BluetoothCommand.Result((BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.SUCCESS, (GattOperationResult.GattResult) null, 5, (fd4) null) : result, (i & 8) != 0 ? null : o70);
        }

        @DexIgnore
        public static /* synthetic */ Result copy$default(Result result, RequestId requestId2, ResultCode resultCode2, BluetoothCommand.Result result2, o70 o70, int i, Object obj) {
            if ((i & 1) != 0) {
                requestId2 = result.requestId;
            }
            if ((i & 2) != 0) {
                resultCode2 = result.resultCode;
            }
            if ((i & 4) != 0) {
                result2 = result.commandResult;
            }
            if ((i & 8) != 0) {
                o70 = result.responseStatus;
            }
            return result.copy(requestId2, resultCode2, result2, o70);
        }

        @DexIgnore
        public final RequestId component1() {
            return this.requestId;
        }

        @DexIgnore
        public final ResultCode component2() {
            return this.resultCode;
        }

        @DexIgnore
        public final BluetoothCommand.Result component3() {
            return this.commandResult;
        }

        @DexIgnore
        public final o70 component4() {
            return this.responseStatus;
        }

        @DexIgnore
        public final Result copy(RequestId requestId2, ResultCode resultCode2, BluetoothCommand.Result result, o70 o70) {
            kd4.b(requestId2, "requestId");
            kd4.b(resultCode2, "resultCode");
            kd4.b(result, "commandResult");
            return new Result(requestId2, resultCode2, result, o70);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return kd4.a((Object) this.requestId, (Object) result.requestId) && kd4.a((Object) this.resultCode, (Object) result.resultCode) && kd4.a((Object) this.commandResult, (Object) result.commandResult) && kd4.a((Object) this.responseStatus, (Object) result.responseStatus);
        }

        @DexIgnore
        public final BluetoothCommand.Result getCommandResult() {
            return this.commandResult;
        }

        @DexIgnore
        public final RequestId getRequestId() {
            return this.requestId;
        }

        @DexIgnore
        public final o70 getResponseStatus() {
            return this.responseStatus;
        }

        @DexIgnore
        public final ResultCode getResultCode() {
            return this.resultCode;
        }

        @DexIgnore
        public int hashCode() {
            RequestId requestId2 = this.requestId;
            int i = 0;
            int hashCode = (requestId2 != null ? requestId2.hashCode() : 0) * 31;
            ResultCode resultCode2 = this.resultCode;
            int hashCode2 = (hashCode + (resultCode2 != null ? resultCode2.hashCode() : 0)) * 31;
            BluetoothCommand.Result result = this.commandResult;
            int hashCode3 = (hashCode2 + (result != null ? result.hashCode() : 0)) * 31;
            o70 o70 = this.responseStatus;
            if (o70 != null) {
                i = o70.hashCode();
            }
            return hashCode3 + i;
        }

        @DexIgnore
        public JSONObject toJSONObject() {
            JSONObject jSONObject = new JSONObject();
            try {
                wa0.a(wa0.a(jSONObject, JSONKey.REQUEST_ID, this.requestId.getLogName$blesdk_productionRelease()), JSONKey.RESULT_CODE, this.resultCode.getLogName$blesdk_productionRelease());
                o70 o70 = this.responseStatus;
                if (o70 != null && !o70.isSuccessCode()) {
                    wa0.a(jSONObject, JSONKey.RESPONSE_STATUS, this.responseStatus.getLogName());
                }
                if (this.commandResult.getResultCode() != BluetoothCommand.Result.ResultCode.SUCCESS) {
                    wa0.a(jSONObject, JSONKey.COMMAND_RESULT, this.commandResult.toJSONObject());
                }
            } catch (JSONException e) {
                da0.l.a(e);
            }
            return jSONObject;
        }

        @DexIgnore
        public String toString() {
            return "Result(requestId=" + this.requestId + ", resultCode=" + this.resultCode + ", commandResult=" + this.commandResult + ", responseStatus=" + this.responseStatus + ")";
        }

        @DexIgnore
        public Result(RequestId requestId2, ResultCode resultCode2, BluetoothCommand.Result result, o70 o70) {
            kd4.b(requestId2, "requestId");
            kd4.b(resultCode2, "resultCode");
            kd4.b(result, "commandResult");
            this.requestId = requestId2;
            this.resultCode = resultCode2;
            this.commandResult = result;
            this.responseStatus = o70;
        }
    }

    @DexIgnore
    public final JSONObject b(BluetoothCommand bluetoothCommand) {
        byte[] bArr;
        JSONObject jSONObject = new JSONObject();
        boolean z2 = this instanceof TransferDataRequest;
        boolean z3 = false;
        if (z2) {
            bArr = ((TransferDataRequest) this).A().b();
        } else if (bluetoothCommand instanceof n10) {
            bArr = ((n10) bluetoothCommand).j();
        } else {
            bArr = bluetoothCommand instanceof o10 ? ((o10) bluetoothCommand).j() : new byte[0];
        }
        if (bArr.length == 0) {
            z3 = true;
        }
        if (!z3) {
            int length = bArr.length;
            wa0.a(wa0.a(wa0.a(jSONObject, JSONKey.RAW_DATA, (length > 500 || (z2 && kd4.a((Object) this.c, (Object) PhaseId.SEND_APP_NOTIFICATION.getLogName$blesdk_productionRelease()))) ? "" : k90.a(bArr, (String) null, 1, (Object) null)), JSONKey.RAW_DATA_LENGTH, Integer.valueOf(length)), JSONKey.RAW_DATA_CRC, Long.valueOf(Crc32Calculator.a.a(bArr, Crc32Calculator.CrcType.CRC32)));
        }
        return jSONObject;
    }

    @DexIgnore
    public void a(long j2) {
        this.h = j2;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.s = z2;
    }

    @DexIgnore
    public final void a(float f2) {
        for (yc4 invoke : this.o) {
            invoke.invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public final void a(b20 b20) {
        if (b20 instanceof d20) {
            a((d20) b20);
        } else if (b20 instanceof c20) {
            b((c20) b20);
        }
    }

    @DexIgnore
    public final void c(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.v = Result.copy$default(this.v, (RequestId) null, Result.Companion.a(bluetoothCommand.e()).getResultCode(), bluetoothCommand.e(), (o70) null, 9, (Object) null);
        SdkLogEntry sdkLogEntry = this.f;
        if (sdkLogEntry != null) {
            sdkLogEntry.setSuccess(false);
        }
        SdkLogEntry sdkLogEntry2 = this.f;
        if (sdkLogEntry2 != null) {
            JSONObject extraData = sdkLogEntry2.getExtraData();
            if (extraData != null) {
                JSONObject a2 = wa0.a(extraData, JSONKey.MESSAGE, this.v.getResultCode().getLogName$blesdk_productionRelease());
                if (a2 != null) {
                    wa0.a(a2, JSONKey.ERROR_DETAIL, bluetoothCommand.e().toJSONObject());
                }
            }
        }
        y();
        b();
    }

    @DexIgnore
    public void a(d20 d20) {
        kd4.b(d20, "connectionStateChangedNotification");
        if (d20.a() == Peripheral.State.CONNECTED) {
            return;
        }
        if (d20.b() == -1) {
            a(Result.ResultCode.BLUETOOTH_OFF);
        } else {
            a(Result.ResultCode.CONNECTION_DROPPED);
        }
    }

    @DexIgnore
    public final void a(ResponseInfo responseInfo) {
        kd4.b(responseInfo, "responseInfo");
        this.g.add(responseInfo);
    }

    @DexIgnore
    public final Request c(xc4<? super Request, qa4> xc4) {
        kd4.b(xc4, "actionOnRequestSuccess");
        if (!this.t) {
            this.l.add(xc4);
        } else if (this.v.getResultCode() == Result.ResultCode.SUCCESS) {
            xc4.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final SdkLogEntry a() {
        Object[] array = this.g.toArray(new ResponseInfo[0]);
        if (array != null) {
            ResponseInfo[] responseInfoArr = (ResponseInfo[]) array;
            JSONObject a2 = wa0.a(wa0.a(new JSONObject(), JSONKey.MESSAGE, this.v.getResultCode().getLogName$blesdk_productionRelease()), JSONKey.REQUEST_UUID, this.b);
            if (this.v.getResultCode() == Result.ResultCode.SUCCESS) {
                m90.a(a2, u(), false, 2, (Object) null);
            } else {
                wa0.a(a2, JSONKey.ERROR_DETAIL, this.v.toJSONObject());
            }
            if (!(responseInfoArr.length == 0)) {
                JSONArray jSONArray = new JSONArray();
                for (ResponseInfo jSONObject : responseInfoArr) {
                    jSONArray.put(jSONObject.toJSONObject());
                }
                wa0.a(a2, JSONKey.RESPONSES, jSONArray);
            }
            return new SdkLogEntry(this.x.getLogName$blesdk_productionRelease(), EventType.RESPONSE, this.y.k(), this.c, this.d, Result.ResultCode.SUCCESS == this.v.getResultCode(), (String) null, (DeviceInformation) null, (ea0) null, a2, 448, (fd4) null);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void b() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            x();
            this.y.b(this.q);
            this.y.b(this.r);
            y();
            z();
            if (Result.ResultCode.SUCCESS == this.v.getResultCode()) {
                Peripheral peripheral = this.y;
                LogLevel logLevel = LogLevel.DEBUG;
                String str = this.a;
                Peripheral.a(peripheral, logLevel, str, "Request success: " + m90.a(this.v.toJSONObject(), u()).toString(2), false, 8, (Object) null);
                for (xc4 invoke : this.l) {
                    invoke.invoke(this);
                }
            } else {
                Peripheral peripheral2 = this.y;
                LogLevel logLevel2 = LogLevel.ERROR;
                String str2 = this.a;
                Peripheral.a(peripheral2, logLevel2, str2, "Request error: " + m90.a(this.v.toJSONObject(), u()).toString(2), false, 8, (Object) null);
                for (xc4 invoke2 : this.m) {
                    invoke2.invoke(this);
                }
            }
            for (xc4 invoke3 : this.n) {
                invoke3.invoke(this);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Request(RequestId requestId, Peripheral peripheral, int i2, int i3, fd4 fd4) {
        this(requestId, peripheral, (i3 & 4) != 0 ? 3 : i2);
    }

    @DexIgnore
    public final void a(Result result) {
        kd4.b(result, "requestResult");
        this.v = result;
        b();
    }

    @DexIgnore
    public final void a(wc4<qa4> wc4) {
        kd4.b(wc4, "actionOnTimeout");
        x();
        if (m() > 0) {
            this.i = new b(this, wc4);
            Peripheral peripheral = this.y;
            LogLevel logLevel = LogLevel.DEBUG;
            String str = this.a;
            Peripheral.a(peripheral, logLevel, str, "Start timeout: " + m(), false, 8, (Object) null);
            b bVar = this.i;
            if (bVar != null) {
                this.j.postDelayed(bVar, m());
            }
        }
    }

    @DexIgnore
    public final Request b(xc4<? super Request, qa4> xc4) {
        kd4.b(xc4, "actionOnRequestError");
        if (!this.t) {
            this.m.add(xc4);
        } else if (this.v.getResultCode() != Result.ResultCode.SUCCESS) {
            xc4.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final void a(Result.ResultCode resultCode) {
        BluetoothCommand.Result.ResultCode resultCode2;
        kd4.b(resultCode, "resultCode");
        if (!this.t && !this.u) {
            Peripheral peripheral = this.y;
            LogLevel logLevel = LogLevel.DEBUG;
            String str = this.a;
            Peripheral.a(peripheral, logLevel, str, "Request stopWithResultCode: " + resultCode, false, 8, (Object) null);
            this.u = true;
            this.v = Result.copy$default(this.v, (RequestId) null, resultCode, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null);
            BluetoothCommand bluetoothCommand = this.k;
            if (bluetoothCommand == null || bluetoothCommand.g()) {
                b();
                return;
            }
            int i2 = b70.a[resultCode.ordinal()];
            if (i2 == 1) {
                resultCode2 = BluetoothCommand.Result.ResultCode.INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT;
            } else if (i2 == 2) {
                resultCode2 = BluetoothCommand.Result.ResultCode.CONNECTION_DROPPED;
            } else if (i2 != 3) {
                resultCode2 = BluetoothCommand.Result.ResultCode.INTERRUPTED;
            } else {
                resultCode2 = BluetoothCommand.Result.ResultCode.BLUETOOTH_OFF;
            }
            this.y.a(this.k, resultCode2);
        }
    }

    @DexIgnore
    public final Request a(xc4<? super Request, qa4> xc4) {
        kd4.b(xc4, "actionOnRequestDone");
        if (!this.t) {
            this.n.add(xc4);
        } else {
            xc4.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public final Request a(yc4<? super Request, ? super Float, qa4> yc4) {
        kd4.b(yc4, "actionOnRequestProgressChanged");
        if (!this.t) {
            this.o.add(yc4);
        }
        return this;
    }
}
