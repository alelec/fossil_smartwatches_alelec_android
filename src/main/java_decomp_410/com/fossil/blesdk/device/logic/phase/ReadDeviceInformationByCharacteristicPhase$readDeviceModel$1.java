package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadDeviceInformationByCharacteristicPhase$readDeviceModel$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.ReadDeviceInformationByCharacteristicPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadDeviceInformationByCharacteristicPhase$readDeviceModel$1(com.fossil.blesdk.device.logic.phase.ReadDeviceInformationByCharacteristicPhase readDeviceInformationByCharacteristicPhase) {
        super(1);
        this.this$0 = readDeviceInformationByCharacteristicPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.device.logic.request.Request request2 = request;
        com.fossil.blesdk.obfuscated.kd4.m24411b(request2, "executedRequest");
        com.fossil.blesdk.device.logic.phase.ReadDeviceInformationByCharacteristicPhase readDeviceInformationByCharacteristicPhase = this.this$0;
        readDeviceInformationByCharacteristicPhase.f3020z = com.fossil.blesdk.device.DeviceInformation.copy$default(readDeviceInformationByCharacteristicPhase.mo7605A(), (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, (java.lang.String) null, ((com.fossil.blesdk.obfuscated.v60) request2).mo16989B(), (java.lang.String) null, (com.fossil.blesdk.device.data.Version) null, (com.fossil.blesdk.device.data.Version) null, (com.fossil.blesdk.device.data.Version) null, (java.util.LinkedHashMap) null, (java.util.LinkedHashMap) null, (com.fossil.blesdk.device.DeviceInformation.BondRequirement) null, (com.fossil.blesdk.device.data.config.DeviceConfigKey[]) null, (com.fossil.blesdk.device.data.Version) null, (java.lang.String) null, (com.fossil.blesdk.device.data.Version) null, 131039, (java.lang.Object) null);
        com.fossil.blesdk.device.logic.phase.ReadDeviceInformationByCharacteristicPhase readDeviceInformationByCharacteristicPhase2 = this.this$0;
        readDeviceInformationByCharacteristicPhase2.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(readDeviceInformationByCharacteristicPhase2.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
    }
}
