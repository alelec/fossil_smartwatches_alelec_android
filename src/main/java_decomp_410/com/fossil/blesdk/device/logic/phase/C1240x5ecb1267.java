package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredCharacteristicsString$1 */
public final class C1240x5ecb1267 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.C1240x5ecb1267 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.C1240x5ecb1267();

    @DexIgnore
    public C1240x5ecb1267() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId characteristicId) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(characteristicId, "characteristicId");
        return characteristicId.name();
    }
}
