package com.fossil.blesdk.device.logic.request;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Request$actionOnRequestTimeout$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.request.Request this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Request$actionOnRequestTimeout$1(com.fossil.blesdk.device.logic.request.Request request) {
        super(0);
        this.this$0 = request;
    }

    @DexIgnore
    public final void invoke() {
        com.fossil.blesdk.device.core.Peripheral.m3116a(this.this$0.mo7739i(), com.fossil.blesdk.log.debuglog.LogLevel.DEBUG, this.this$0.mo7747q(), "Request timeout", false, 8, (java.lang.Object) null);
        this.this$0.mo7716a(com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.TIMEOUT);
    }
}
