package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class StreamingPhase$startStreaming$streamingRequest$Anon1 extends Lambda implements xc4<AsyncEvent, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ StreamingPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public StreamingPhase$startStreaming$streamingRequest$Anon1(StreamingPhase streamingPhase) {
        super(1);
        this.this$Anon0 = streamingPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((AsyncEvent) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(AsyncEvent asyncEvent) {
        kd4.b(asyncEvent, "asyncEvent");
        xc4 a = this.this$Anon0.z;
        if (a != null) {
            qa4 qa4 = (qa4) a.invoke(asyncEvent);
        }
    }
}
