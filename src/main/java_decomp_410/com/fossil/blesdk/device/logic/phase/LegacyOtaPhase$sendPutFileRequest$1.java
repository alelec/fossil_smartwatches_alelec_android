package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$sendPutFileRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacyOtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$sendPutFileRequest$1(com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase) {
        super(1);
        this.this$0 = legacyOtaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        com.fossil.blesdk.obfuscated.x80 x80 = (com.fossil.blesdk.obfuscated.x80) request;
        this.this$0.mo7417a(com.fossil.blesdk.obfuscated.ya4.m30970a(this.this$0.f2949K, (int) x80.mo17622K(), (int) java.lang.Math.min(this.this$0.f2939A, x80.mo17622K() + x80.mo17621J())), x80.mo17622K());
    }
}
