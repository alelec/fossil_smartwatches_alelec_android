package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.background.BackgroundImage;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.g60;
import com.fossil.blesdk.obfuscated.i60;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o20;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SetBackgroundImagePhase extends Phase {
    @DexIgnore
    public static /* final */ a A; // = new a((fd4) null);
    @DexIgnore
    public /* final */ BackgroundImageConfig z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final byte[] a(BackgroundImage[] backgroundImageArr, short s, Version version) {
            kd4.b(backgroundImageArr, "backgroundImages");
            kd4.b(version, "version");
            Object[] array = za4.c((T[]) backgroundImageArr).toArray(new BackgroundImage[0]);
            if (array != null) {
                BackgroundImage[] backgroundImageArr2 = (BackgroundImage[]) array;
                if (backgroundImageArr2.length == 0) {
                    return new byte[0];
                }
                return o20.c.a(s, version, backgroundImageArr2);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ SetBackgroundImagePhase(Peripheral peripheral, Phase.a aVar, BackgroundImageConfig backgroundImageConfig, String str, int i, fd4 fd4) {
        this(peripheral, aVar, backgroundImageConfig, str);
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final void A() {
        BackgroundImageConfig backgroundImageConfig = this.z;
        Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.ASSET.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = ua0.y.g();
        }
        backgroundImageConfig.setFileVersion$blesdk_productionRelease(version);
        Phase.a((Phase) this, (Phase) new i60(j(), e(), PhaseId.PUT_BACKGROUND_IMAGE_CONFIG, this.z.getSettingAssignmentJSON$blesdk_productionRelease(), false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 48, (fd4) null), (xc4) new SetBackgroundImagePhase$putBackgroundImageConfig$Anon1(this), (xc4) new SetBackgroundImagePhase$putBackgroundImageConfig$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void B() {
        boolean z2 = false;
        Object[] array = this.z.getBackgroundImageList$blesdk_productionRelease().toArray(new BackgroundImage[0]);
        if (array != null) {
            BackgroundImage[] backgroundImageArr = (BackgroundImage[]) array;
            if (backgroundImageArr.length == 0) {
                z2 = true;
            }
            if (!z2) {
                try {
                    a aVar = A;
                    Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.ASSET.getFileHandleMask$blesdk_productionRelease()));
                    if (version == null) {
                        version = ua0.y.g();
                    }
                    byte[] a2 = aVar.a(backgroundImageArr, 1792, version);
                    Phase.a((Phase) this, (Phase) new g60(j(), e(), PhaseId.PUT_BACKGROUND_IMAGE_DATA, true, 1792, a2, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 64, (fd4) null), (xc4) new SetBackgroundImagePhase$putBackgroundImageData$Anon1(this), (xc4) new SetBackgroundImagePhase$putBackgroundImageData$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
                } catch (FileFormatException e) {
                    da0.l.a(e);
                    a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.INCOMPATIBLE_FIRMWARE, (Request.Result) null, 5, (Object) null));
                }
            } else {
                A();
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public void t() {
        B();
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.BACKGROUND_IMAGE_CONFIG, this.z.toJSONObject());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetBackgroundImagePhase(Peripheral peripheral, Phase.a aVar, BackgroundImageConfig backgroundImageConfig, String str) {
        super(peripheral, aVar, PhaseId.SET_BACKGROUND_IMAGE, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(backgroundImageConfig, "backgroundImageConfig");
        kd4.b(str, "phaseUuid");
        this.z = backgroundImageConfig;
    }
}
