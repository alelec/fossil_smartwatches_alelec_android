package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.d70;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SendAsyncEventAckPhase extends Phase {
    @DexIgnore
    public /* final */ AsyncEvent A;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.ASYNC}));

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SendAsyncEventAckPhase(Peripheral peripheral, Phase.a aVar, AsyncEvent asyncEvent) {
        super(peripheral, aVar, PhaseId.SEND_ASYNC_EVENT_ACK, (String) null, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(asyncEvent, "asyncEvent");
        this.A = asyncEvent;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        Phase.a((Phase) this, (Request) new d70(j(), this.A), (xc4) SendAsyncEventAckPhase$onStart$Anon1.INSTANCE, (xc4) SendAsyncEventAckPhase$onStart$Anon2.INSTANCE, (yc4) null, (xc4) new SendAsyncEventAckPhase$onStart$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.DEVICE_EVENT, this.A.toJSONObject());
    }
}
