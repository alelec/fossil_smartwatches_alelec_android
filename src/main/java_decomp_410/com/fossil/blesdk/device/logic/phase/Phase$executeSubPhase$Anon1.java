package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeSubPhase$Anon1 extends Lambda implements yc4<Phase, Float, qa4> {
    @DexIgnore
    public static /* final */ Phase$executeSubPhase$Anon1 INSTANCE; // = new Phase$executeSubPhase$Anon1();

    @DexIgnore
    public Phase$executeSubPhase$Anon1() {
        super(2);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Phase) obj, ((Number) obj2).floatValue());
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase, float f) {
        kd4.b(phase, "<anonymous parameter 0>");
    }
}
