package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ConnectHIDPhase$connectHID$Anon3 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ConnectHIDPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectHIDPhase$connectHID$Anon3(ConnectHIDPhase connectHIDPhase) {
        super(1);
        this.this$Anon0 = connectHIDPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        ConnectHIDPhase connectHIDPhase = this.this$Anon0;
        connectHIDPhase.c(Phase.Result.copy$default(connectHIDPhase.k(), (PhaseId) null, Phase.Result.ResultCode.Companion.a(request.n()), request.n(), 1, (Object) null));
    }
}
