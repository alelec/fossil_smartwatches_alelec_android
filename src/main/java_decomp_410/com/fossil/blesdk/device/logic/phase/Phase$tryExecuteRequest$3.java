package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$tryExecuteRequest$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.wc4 $nextAction;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3$1")
    /* renamed from: com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3$1 */
    public static final class C12521 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.request.Request $originalRequest;
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C12521(com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3 phase$tryExecuteRequest$3, com.fossil.blesdk.device.logic.request.Request request) {
            super(1);
            this.this$0 = phase$tryExecuteRequest$3;
            this.$originalRequest = request;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.device.logic.request.Request) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
            com.fossil.blesdk.device.logic.request.Request request2 = this.$originalRequest;
            if ((request2 instanceof com.fossil.blesdk.obfuscated.l80) || request2.mo7744n().getResultCode() == com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.INTERRUPTED) {
                this.this$0.this$0.mo7547a(this.$originalRequest.mo7744n());
            } else {
                this.this$0.$nextAction.invoke();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3$2")
    /* renamed from: com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3$2 */
    public static final class C12532 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C12532(com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3 phase$tryExecuteRequest$3) {
            super(1);
            this.this$0 = phase$tryExecuteRequest$3;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
            invoke((com.fossil.blesdk.device.logic.request.Request) obj);
            return com.fossil.blesdk.obfuscated.qa4.f17909a;
        }

        @DexIgnore
        public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
            com.fossil.blesdk.obfuscated.kd4.m24411b(request, "abortRequest");
            if (this.this$0.this$0.mo7565k().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.INTERRUPTED) {
                com.fossil.blesdk.device.logic.phase.Phase phase = this.this$0.this$0;
                phase.mo7545a(phase.mo7565k());
                return;
            }
            this.this$0.this$0.mo7547a(request.mo7744n());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$tryExecuteRequest$3(com.fossil.blesdk.device.logic.phase.Phase phase, com.fossil.blesdk.obfuscated.wc4 wc4) {
        super(1);
        this.this$0 = phase;
        this.$nextAction = wc4;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        java.lang.Short sh;
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "originalRequest");
        com.fossil.blesdk.device.logic.phase.Phase phase = this.this$0;
        phase.f3001m = phase.f3001m + 1;
        com.fossil.blesdk.device.logic.phase.Phase phase2 = this.this$0;
        phase2.mo7554b(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(phase2.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.Companion.mo7587a(request.mo7744n()), request.mo7744n(), 1, (java.lang.Object) null));
        if (request.mo7744n().getResultCode() == com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.CONNECTION_DROPPED || request.mo7744n().getResultCode() == com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.BLUETOOTH_OFF || request.mo7744n().getCommandResult().getGattResult().getResultCode() == com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.ResultCode.GATT_NULL) {
            this.this$0.mo7547a(request.mo7744n());
            return;
        }
        if (request instanceof com.fossil.blesdk.obfuscated.e80) {
            sh = java.lang.Short.valueOf(((com.fossil.blesdk.obfuscated.e80) request).mo10350I());
        } else {
            sh = request instanceof com.fossil.blesdk.device.logic.request.TransferDataRequest ? java.lang.Short.valueOf(((com.fossil.blesdk.device.logic.request.TransferDataRequest) request).mo7785D()) : null;
        }
        if (sh != null) {
            com.fossil.blesdk.device.logic.phase.Phase phase3 = this.this$0;
            com.fossil.blesdk.obfuscated.c80 c80 = new com.fossil.blesdk.obfuscated.c80(sh.shortValue(), this.this$0.mo7564j(), 0, 4, (com.fossil.blesdk.obfuscated.fd4) null);
            com.fossil.blesdk.device.logic.phase.Phase.m3629a(phase3, (com.fossil.blesdk.device.logic.request.Request) c80, (com.fossil.blesdk.obfuscated.xc4) new com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3.C12521(this, request), (com.fossil.blesdk.obfuscated.xc4) new com.fossil.blesdk.device.logic.phase.Phase$tryExecuteRequest$3.C12532(this), (com.fossil.blesdk.obfuscated.yc4) null, (com.fossil.blesdk.obfuscated.xc4) null, (com.fossil.blesdk.obfuscated.xc4) null, 56, (java.lang.Object) null);
            return;
        }
        this.$nextAction.invoke();
    }
}
