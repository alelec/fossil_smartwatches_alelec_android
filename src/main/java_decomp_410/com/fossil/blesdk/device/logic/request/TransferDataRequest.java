package com.fossil.blesdk.device.logic.request;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.code.FileControlOperationCode;
import com.fossil.blesdk.device.logic.request.code.FileControlStatusCode;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.obfuscated.c20;
import com.fossil.blesdk.obfuscated.d90;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j60;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n10;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o70;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wc4;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.common.constants.Constants;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TransferDataRequest extends Request {
    @DexIgnore
    public long A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C; // = 15000;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ wc4<qa4> E;
    @DexIgnore
    public /* final */ short F;
    @DexIgnore
    public /* final */ j60 G;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransferDataRequest(short s, j60 j60, Peripheral peripheral) {
        super(RequestId.TRANSFER_DATA, peripheral, 0, 4, (fd4) null);
        kd4.b(j60, "dataTransferProtocol");
        kd4.b(peripheral, "peripheral");
        this.F = s;
        this.G = j60;
        this.E = new TransferDataRequest$actionOnEOFTimeout$Anon1(this, peripheral);
    }

    @DexIgnore
    public final j60 A() {
        return this.G;
    }

    @DexIgnore
    public final long B() {
        return this.B;
    }

    @DexIgnore
    public final long C() {
        return this.A;
    }

    @DexIgnore
    public final short D() {
        return this.F;
    }

    @DexIgnore
    public final void E() {
        float min = Math.min((((float) this.G.g()) * 1.0f) / ((float) this.G.i()), 1.0f);
        if (Math.abs(this.D - min) > 0.001f || this.G.g() >= this.G.i()) {
            this.D = min;
            a(this.D);
        }
    }

    @DexIgnore
    public void a(long j) {
        this.C = j;
    }

    @DexIgnore
    public void b(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
        if (c20.a() == GattCharacteristic.CharacteristicId.FTC && c20.b().length >= 12) {
            ByteBuffer order = ByteBuffer.wrap(c20.b()).order(ByteOrder.LITTLE_ENDIAN);
            byte b = order.get(0);
            if (this.F != order.getShort(1)) {
                return;
            }
            if (FileControlOperationCode.EOF_REACH.responseCode() == b) {
                x();
                Request.Result a2 = Request.Result.Companion.a((o70) FileControlStatusCode.Companion.a(order.get(3)));
                b(Request.Result.copy$default(n(), (RequestId) null, a2.getResultCode(), (BluetoothCommand.Result) null, a2.getResponseStatus(), 5, (Object) null));
                this.A = n90.b(order.getInt(4));
                this.B = n90.b(order.getInt(8));
                a(new Request.ResponseInfo(0, c20.a(), c20.b(), wa0.a(wa0.a(new JSONObject(), JSONKey.WRITTEN_SIZE, Long.valueOf(this.A)), JSONKey.WRITTEN_DATA_CRC, Long.valueOf(this.B)), 1, (fd4) null));
                a(n());
                return;
            }
            a(new Request.ResponseInfo(0, c20.a(), c20.b(), (JSONObject) null, 9, (fd4) null));
            a(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.RESPONSE_ERROR, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
    }

    @DexIgnore
    public void d(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.Companion.a(bluetoothCommand.e()).getResultCode(), bluetoothCommand.e(), (o70) null, 9, (Object) null));
        SdkLogEntry l = l();
        if (l != null) {
            l.setSuccess(true);
        }
        SdkLogEntry l2 = l();
        if (l2 != null) {
            JSONObject extraData = l2.getExtraData();
            if (extraData != null) {
                wa0.a(extraData, JSONKey.MESSAGE, Request.Result.ResultCode.SUCCESS.getLogName$blesdk_productionRelease());
            }
        }
        if (n().getResultCode() == Request.Result.ResultCode.SUCCESS) {
            a(bluetoothCommand);
        }
        a(d());
        SdkLogEntry l3 = l();
        if (l3 != null) {
            JSONObject extraData2 = l3.getExtraData();
            if (extraData2 != null) {
                wa0.a(extraData2, JSONKey.TRANSFERRED_DATA_SIZE, Integer.valueOf(this.G.g()));
            }
        }
        E();
        c();
    }

    @DexIgnore
    public BluetoothCommand h() {
        if (!this.G.j()) {
            return null;
        }
        byte[] e = this.G.e();
        if (g()) {
            e = d90.b.b(i().k(), this.G.a(), e);
        }
        return new n10(this.G.a(), e, i().h());
    }

    @DexIgnore
    public long m() {
        return this.C;
    }

    @DexIgnore
    public void s() {
        a(30000);
        a(this.E);
        y();
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.FILE_HANDLE, n90.a(this.F));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.WRITTEN_SIZE, Long.valueOf(this.A)), JSONKey.WRITTEN_DATA_CRC, Long.valueOf(this.B));
    }
}
