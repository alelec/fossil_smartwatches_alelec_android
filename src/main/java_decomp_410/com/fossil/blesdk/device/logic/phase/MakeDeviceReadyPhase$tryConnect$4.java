package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$tryConnect$4 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request.Result, java.lang.Boolean> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$4 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$4();

    @DexIgnore
    public MakeDeviceReadyPhase$tryConnect$4() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return java.lang.Boolean.valueOf(invoke((com.fossil.blesdk.device.logic.request.Request.Result) obj));
    }

    @DexIgnore
    public final boolean invoke(com.fossil.blesdk.device.logic.request.Request.Result result) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(result, "requestResult");
        return result.getResultCode() == com.fossil.blesdk.device.logic.request.Request.Result.ResultCode.BLUETOOTH_OFF;
    }
}
