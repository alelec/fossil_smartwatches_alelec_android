package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$requestDiscoverService$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$requestDiscoverService$3(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase = this.this$0;
        makeDeviceReadyPhase.mo7483c(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(makeDeviceReadyPhase.mo7565k(), (com.fossil.blesdk.device.logic.phase.PhaseId) null, com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS, (com.fossil.blesdk.device.logic.request.Request.Result) null, 5, (java.lang.Object) null));
    }
}
