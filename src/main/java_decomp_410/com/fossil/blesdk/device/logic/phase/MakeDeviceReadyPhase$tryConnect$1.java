package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$tryConnect$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$1$a")
    /* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$1$a */
    public static final class C1243a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$1 f2982e;

        @DexIgnore
        public C1243a(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$1 makeDeviceReadyPhase$tryConnect$1) {
            this.f2982e = makeDeviceReadyPhase$tryConnect$1;
        }

        @DexIgnore
        public final void run() {
            this.f2982e.this$0.mo7468E();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MakeDeviceReadyPhase$tryConnect$1(com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase makeDeviceReadyPhase) {
        super(1);
        this.this$0 = makeDeviceReadyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        this.this$0.mo7561f().postDelayed(new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$tryConnect$1.C1243a(this), 1600);
    }
}
