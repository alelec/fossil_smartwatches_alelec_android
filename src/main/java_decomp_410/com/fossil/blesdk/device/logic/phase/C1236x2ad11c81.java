package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.logic.phase.FetchDeviceInformationPhase$readDeviceInformationFromCharacteristics$2 */
public final class C1236x2ad11c81 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.FetchDeviceInformationPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C1236x2ad11c81(com.fossil.blesdk.device.logic.phase.FetchDeviceInformationPhase fetchDeviceInformationPhase) {
        super(1);
        this.this$0 = fetchDeviceInformationPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        this.this$0.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(phase.mo7565k(), this.this$0.mo7565k().getPhaseId(), (com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode) null, (com.fossil.blesdk.device.logic.request.Request.Result) null, 6, (java.lang.Object) null));
    }
}
