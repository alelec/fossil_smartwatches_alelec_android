package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncFlowPhase$runGetHWLogPhase$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.phase.Phase, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.SyncFlowPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncFlowPhase$runGetHWLogPhase$3(com.fossil.blesdk.device.logic.phase.SyncFlowPhase syncFlowPhase) {
        super(2);
        this.this$0 = syncFlowPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "<anonymous parameter 0>");
        if (f == 1.0f) {
            this.this$0.mo7541a(f);
            return;
        }
        com.fossil.blesdk.device.logic.phase.SyncFlowPhase syncFlowPhase = this.this$0;
        syncFlowPhase.mo7541a(syncFlowPhase.f3044F + (f * this.this$0.f3045G));
    }
}
