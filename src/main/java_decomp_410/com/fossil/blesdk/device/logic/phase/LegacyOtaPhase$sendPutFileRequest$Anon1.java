package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.x80;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.ya4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$sendPutFileRequest$Anon1 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ LegacyOtaPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$sendPutFileRequest$Anon1(LegacyOtaPhase legacyOtaPhase) {
        super(1);
        this.this$Anon0 = legacyOtaPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "executedRequest");
        x80 x80 = (x80) request;
        this.this$Anon0.a(ya4.a(this.this$Anon0.K, (int) x80.K(), (int) Math.min(this.this$Anon0.A, x80.K() + x80.J())), x80.K());
    }
}
