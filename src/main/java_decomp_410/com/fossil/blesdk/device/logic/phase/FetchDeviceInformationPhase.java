package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.s50;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y60;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class FetchDeviceInformationPhase extends Phase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> A;
    @DexIgnore
    public DeviceInformation z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ FetchDeviceInformationPhase(Peripheral peripheral, Phase.a aVar, String str, int i, fd4 fd4) {
        this(peripheral, aVar, str);
        if ((i & 4) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Phase) new s50(j(), e(), 0, l(), 4, (fd4) null), (xc4) new FetchDeviceInformationPhase$readDeviceInformationFile$Anon1(this), (xc4) new FetchDeviceInformationPhase$readDeviceInformationFile$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Phase) new ReadDeviceInformationByCharacteristicPhase(j(), e(), l()), (xc4) new FetchDeviceInformationPhase$readDeviceInformationFromCharacteristics$Anon1(this), (xc4) new FetchDeviceInformationPhase$readDeviceInformationFromCharacteristics$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new y60(j()), (xc4) new FetchDeviceInformationPhase$readSoftwareRevision$Anon1(this), (xc4) new FetchDeviceInformationPhase$readSoftwareRevision$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.A;
    }

    @DexIgnore
    public void t() {
        C();
    }

    @DexIgnore
    public JSONObject x() {
        return wa0.a(super.x(), JSONKey.DEVICE_INFO, this.z.toJSONObject());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public FetchDeviceInformationPhase(Peripheral peripheral, Phase.a aVar, String str) {
        super(r1, r2, PhaseId.FETCH_DEVICE_INFORMATION, r3);
        Peripheral peripheral2 = peripheral;
        Phase.a aVar2 = aVar;
        String str2 = str;
        kd4.b(peripheral2, "peripheral");
        kd4.b(aVar2, "delegate");
        kd4.b(str2, "phaseUuid");
        this.z = new DeviceInformation(peripheral.i(), peripheral.k(), "", "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (fd4) null);
        this.A = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));
    }

    @DexIgnore
    public DeviceInformation i() {
        return this.z;
    }
}
