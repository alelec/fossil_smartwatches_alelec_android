package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ExchangeSecretKeyPhase$authenticateWithSecretKey$$inlined$let$lambda$Anon1 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ ExchangeSecretKeyPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ExchangeSecretKeyPhase$authenticateWithSecretKey$$inlined$let$lambda$Anon1(ExchangeSecretKeyPhase exchangeSecretKeyPhase) {
        super(1);
        this.this$Anon0 = exchangeSecretKeyPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "executedPhase");
        this.this$Anon0.a(Phase.Result.copy$default(phase.k(), this.this$Anon0.k().getPhaseId(), (Phase.Result.ResultCode) null, (Request.Result) null, 6, (Object) null));
    }
}
