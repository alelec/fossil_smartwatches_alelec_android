package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredServiceUuidString$1 */
public final class C1241xb62a54bf extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<java.util.UUID, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.C1241xb62a54bf INSTANCE; // = new com.fossil.blesdk.device.logic.phase.C1241xb62a54bf();

    @DexIgnore
    public C1241xb62a54bf() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(java.util.UUID uuid) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(uuid, "it");
        java.lang.String uuid2 = uuid.toString();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid2, "it.toString()");
        return uuid2;
    }
}
