package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeSubPhase$4 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.xc4 $actionOnError;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.xc4 $isTerminatedPhaseError;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Phase$executeSubPhase$4(com.fossil.blesdk.device.logic.phase.Phase phase, com.fossil.blesdk.obfuscated.xc4 xc4, com.fossil.blesdk.obfuscated.xc4 xc42) {
        super(1);
        this.this$0 = phase;
        this.$isTerminatedPhaseError = xc4;
        this.$actionOnError = xc42;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        if (((java.lang.Boolean) this.$isTerminatedPhaseError.invoke(phase.mo7565k())).booleanValue()) {
            this.this$0.mo7545a(com.fossil.blesdk.device.logic.phase.Phase.Result.copy$default(phase.mo7565k(), this.this$0.mo7562g(), (com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode) null, (com.fossil.blesdk.device.logic.request.Request.Result) null, 6, (java.lang.Object) null));
        } else {
            this.$actionOnError.invoke(phase);
        }
    }
}
