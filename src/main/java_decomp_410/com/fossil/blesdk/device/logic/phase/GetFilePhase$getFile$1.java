package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GetFilePhase$getFile$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.wc4<com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.GetFilePhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetFilePhase$getFile$1(com.fossil.blesdk.device.logic.phase.GetFilePhase getFilePhase) {
        super(0);
        this.this$0 = getFilePhase;
    }

    @DexIgnore
    public final void invoke() {
        com.fossil.blesdk.database.entity.DeviceFile c = this.this$0.mo7386H();
        if (c != null) {
            com.fossil.blesdk.device.data.file.FileHandle fileHandle = new com.fossil.blesdk.device.data.file.FileHandle(c.getFileHandle$blesdk_productionRelease());
            com.fossil.blesdk.database.entity.DeviceFile a = this.this$0.mo7394a(fileHandle.getFileType$blesdk_productionRelease(), fileHandle.getFileIndex$blesdk_productionRelease());
            if (a == null) {
                a = this.this$0.mo7386H();
                if (a == null) {
                    com.fossil.blesdk.obfuscated.kd4.m24405a();
                    throw null;
                }
            }
            this.this$0.mo7397b(a);
            return;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }
}
