package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.yc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncPhase$onStart$Anon3 extends Lambda implements yc4<Phase, Float, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ SyncPhase this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncPhase$onStart$Anon3(SyncPhase syncPhase) {
        super(2);
        this.this$Anon0 = syncPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        invoke((Phase) obj, ((Number) obj2).floatValue());
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase, float f) {
        kd4.b(phase, "<anonymous parameter 0>");
        this.this$Anon0.a(f);
    }
}
