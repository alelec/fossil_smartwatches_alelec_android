package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.g60;
import com.fossil.blesdk.obfuscated.i80;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l80;
import com.fossil.blesdk.obfuscated.m50;
import com.fossil.blesdk.obfuscated.oa4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.rb4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.obfuscated.z40;
import com.fossil.blesdk.obfuscated.z60;
import com.fossil.blesdk.setting.JSONKey;
import java.util.HashMap;
import java.util.LinkedHashMap;
import kotlin.Pair;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OtaPhase extends g60 {
    @DexIgnore
    public long Q;
    @DexIgnore
    public long R;
    @DexIgnore
    public String S;
    @DexIgnore
    public String T;
    @DexIgnore
    public DeviceInformation U;
    @DexIgnore
    public int V;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public OtaPhase(Peripheral peripheral, Phase.a aVar, byte[] bArr) {
        super(peripheral, aVar, PhaseId.OTA, false, z40.b.b(peripheral.k(), FileType.OTA), r6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        byte[] bArr2 = bArr;
        kd4.b(bArr2, "fileData");
        String firmwareVersion = aVar.getDeviceInformation().getFirmwareVersion();
        this.S = firmwareVersion.length() == 0 ? "unknown" : firmwareVersion;
        this.T = "unknown";
        this.U = new DeviceInformation(peripheral.i(), peripheral.k(), "", "", "", (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (DeviceInformation.BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131040, (fd4) null);
    }

    @DexIgnore
    public l80 H() {
        return new i80(A(), j());
    }

    @DexIgnore
    public void I() {
        if (y00.f.a()) {
            Q();
        } else {
            P();
        }
    }

    @DexIgnore
    public final DeviceInformation O() {
        return this.U;
    }

    @DexIgnore
    public final void P() {
        int i = this.V;
        if (i < 2) {
            this.V = i + 1;
            HashMap a2 = rb4.a((Pair<? extends K, ? extends V>[]) new Pair[]{oa4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.AUTO_CONNECT, Boolean.valueOf(y00.f.a(true))), oa4.a(MakeDeviceReadyPhase.MakeDeviceReadyOption.CONNECTION_TIME_OUT, 55000L)});
            if (this.Q == 0) {
                this.Q = System.currentTimeMillis();
            }
            Phase.a((Phase) this, (Phase) new MakeDeviceReadyPhase(j(), e(), a2, l()), (xc4) new OtaPhase$makeDeviceReady$Anon1(this), (xc4) new OtaPhase$makeDeviceReady$Anon2(this), (yc4) null, (xc4) null, (xc4) OtaPhase$makeDeviceReady$Anon3.INSTANCE, 24, (Object) null);
            return;
        }
        a(k());
    }

    @DexIgnore
    public final void Q() {
        Phase.a((Phase) this, (Request) new z60(j()), (xc4) OtaPhase$removeBond$Anon1.INSTANCE, (xc4) OtaPhase$removeBond$Anon2.INSTANCE, (yc4) null, (xc4) new OtaPhase$removeBond$Anon3(this), (xc4) OtaPhase$removeBond$Anon4.INSTANCE, 8, (Object) null);
    }

    @DexIgnore
    public final void R() {
        Phase.a((Phase) this, (Phase) new LegacyOtaPhase(j(), e(), N(), false, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, l(), 48, (fd4) null), (xc4) new OtaPhase$runOTALegacyFlow$Anon1(this), (xc4) new OtaPhase$runOTALegacyFlow$Anon2(this), (yc4) new OtaPhase$runOTALegacyFlow$Anon3(this), (xc4) null, (xc4) null, 48, (Object) null);
    }

    @DexIgnore
    public void t() {
        this.S = e().getDeviceInformation().getFirmwareVersion();
        if (y00.f.a()) {
            j().b(true);
            a((xc4<? super Phase, qa4>) new OtaPhase$onStart$Anon1(this));
        }
        if (y00.f.b(e().getDeviceInformation())) {
            R();
        } else {
            super.t();
        }
    }

    @DexIgnore
    public JSONObject x() {
        return wa0.a(wa0.a(wa0.a(super.x(), JSONKey.RECONNECT_DURATION_IN_MS, Long.valueOf(Math.max(this.R - this.Q, 0))), JSONKey.OLD_FIRMWARE, this.S), JSONKey.NEW_FIRMWARE, this.T);
    }

    @DexIgnore
    public String i() {
        return this.U.getFirmwareVersion();
    }

    @DexIgnore
    public void a(Peripheral.State state) {
        kd4.b(state, "newState");
        if (m50.a[state.ordinal()] == 1) {
            Request d = d();
            if (d == null || d.r()) {
                Phase q = q();
                if (q == null || q.s()) {
                    a(Phase.Result.ResultCode.CONNECTION_DROPPED);
                }
            }
        }
    }
}
