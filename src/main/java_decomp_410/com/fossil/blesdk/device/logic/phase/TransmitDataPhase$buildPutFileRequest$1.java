package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TransmitDataPhase$buildPutFileRequest$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.TransmitDataPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransmitDataPhase$buildPutFileRequest$1(com.fossil.blesdk.device.logic.phase.TransmitDataPhase transmitDataPhase) {
        super(1);
        this.this$0 = transmitDataPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "it");
        this.this$0.f3065I = ((com.fossil.blesdk.obfuscated.j80) request).mo12264J();
        com.fossil.blesdk.obfuscated.c90.f4020b.mo9434a(this.this$0.mo7564j().mo6457k()).mo9109c(this.this$0.f3065I);
        this.this$0.mo7549a(com.fossil.blesdk.device.logic.request.RequestId.TRANSFER_DATA, com.fossil.blesdk.device.logic.request.RequestId.GET_FILE_SIZE_WRITTEN);
    }
}
