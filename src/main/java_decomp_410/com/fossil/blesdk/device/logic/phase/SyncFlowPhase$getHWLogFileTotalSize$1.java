package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class SyncFlowPhase$getHWLogFileTotalSize$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.SyncFlowPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncFlowPhase$getHWLogFileTotalSize$1(com.fossil.blesdk.device.logic.phase.SyncFlowPhase syncFlowPhase) {
        super(1);
        this.this$0 = syncFlowPhase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        com.fossil.blesdk.device.logic.phase.SyncFlowPhase syncFlowPhase = this.this$0;
        int i = 0;
        for (com.fossil.blesdk.database.entity.DeviceFile fileLength : ((com.fossil.blesdk.obfuscated.h80) request).mo11605N()) {
            i += (int) fileLength.getFileLength();
        }
        syncFlowPhase.f3042D = com.fossil.blesdk.obfuscated.n90.m11117b(i);
    }
}
