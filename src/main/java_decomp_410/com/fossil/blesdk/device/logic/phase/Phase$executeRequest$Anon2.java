package com.fossil.blesdk.device.logic.phase;

import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Phase$executeRequest$Anon2 extends Lambda implements xc4<Request, qa4> {
    @DexIgnore
    public static /* final */ Phase$executeRequest$Anon2 INSTANCE; // = new Phase$executeRequest$Anon2();

    @DexIgnore
    public Phase$executeRequest$Anon2() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Request) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Request request) {
        kd4.b(request, "<anonymous parameter 0>");
    }
}
