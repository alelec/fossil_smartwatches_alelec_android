package com.fossil.blesdk.device.logic.phase;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParametersSet;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j00;
import com.fossil.blesdk.obfuscated.j90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m80;
import com.fossil.blesdk.obfuscated.n80;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o80;
import com.fossil.blesdk.obfuscated.s80;
import com.fossil.blesdk.obfuscated.u80;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.yc4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class LegacySyncPhase extends Phase {
    @DexIgnore
    public short A;
    @DexIgnore
    public long B;
    @DexIgnore
    public int C;
    @DexIgnore
    public /* final */ ArrayList<DeviceFile> D;
    @DexIgnore
    public /* final */ ConnectionParametersSet E;
    @DexIgnore
    public /* final */ short F;
    @DexIgnore
    public float G;
    @DexIgnore
    public /* final */ BiometricProfile H;
    @DexIgnore
    public /* final */ boolean I;
    @DexIgnore
    public /* final */ boolean J;
    @DexIgnore
    public /* final */ float K;
    @DexIgnore
    public /* final */ ArrayList<ResourceType> z;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ LegacySyncPhase(Peripheral peripheral, Phase.a aVar, BiometricProfile biometricProfile, boolean z2, boolean z3, float f, String str, int i, fd4 fd4) {
        this(peripheral, aVar, biometricProfile, z2, z3, r7, r8);
        String str2;
        float f2 = (i & 32) != 0 ? 0.001f : f;
        if ((i & 64) != 0) {
            String uuid = UUID.randomUUID().toString();
            kd4.a((Object) uuid, "UUID.randomUUID().toString()");
            str2 = uuid;
        } else {
            str2 = str;
        }
    }

    @DexIgnore
    public final void A() {
        Phase.a((Phase) this, (Request) new m80((short) (this.F + this.C), j(), 0, 4, (fd4) null), (xc4) LegacySyncPhase$abortFile$Anon1.INSTANCE, (xc4) LegacySyncPhase$abortFile$Anon2.INSTANCE, (yc4) null, (xc4) new LegacySyncPhase$abortFile$Anon3(this), (xc4) null, 40, (Object) null);
    }

    @DexIgnore
    public final void B() {
        Phase.a((Phase) this, (Request) new n80(this.F, j(), 0, 4, (fd4) null), (xc4) new LegacySyncPhase$closeCurrentActivityFileRequest$Anon1(this), (xc4) new LegacySyncPhase$closeCurrentActivityFileRequest$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void C() {
        Phase.a((Phase) this, (Request) new o80(this.F, j(), 0, 4, (fd4) null), (xc4) new LegacySyncPhase$eraseActivityFileRequest$Anon1(this), (xc4) new LegacySyncPhase$eraseActivityFileRequest$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void D() {
        short s = (short) (this.F + this.C);
        Phase.a((Phase) this, (Request) new s80(s, this.B, j(), 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 24, (fd4) null), (xc4) new LegacySyncPhase$getActivityFileRequest$Anon1(this, s), (xc4) new LegacySyncPhase$getActivityFileRequest$Anon2(this), (yc4) new LegacySyncPhase$getActivityFileRequest$Anon3(this), (xc4) null, (xc4) null, 48, (Object) null);
    }

    @DexIgnore
    public final ArrayList<DeviceFile> E() {
        return this.D;
    }

    @DexIgnore
    public final void F() {
        Phase.a((Phase) this, (Request) new u80(this.F, j(), 0, 4, (fd4) null), (xc4) new LegacySyncPhase$getListFileRequest$Anon1(this), (xc4) new LegacySyncPhase$getListFileRequest$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public final void G() {
        this.C++;
        if (this.C < this.A) {
            D();
        } else if (this.J) {
            a(Phase.Result.copy$default(k(), (PhaseId) null, Phase.Result.ResultCode.SUCCESS, (Request.Result) null, 5, (Object) null));
        } else {
            C();
        }
    }

    @DexIgnore
    public final void H() {
        Phase.a((Phase) this, (Phase) new SetConnectionParamsPhase(j(), e(), this.E, l()), (xc4) new LegacySyncPhase$seConnectionParams$Anon1(this), (xc4) new LegacySyncPhase$seConnectionParams$Anon2(this), (yc4) null, (xc4) null, (xc4) null, 56, (Object) null);
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.z;
    }

    @DexIgnore
    public void t() {
        H();
    }

    @DexIgnore
    public JSONObject u() {
        JSONObject put = super.u().put(DeviceConfigKey.BIOMETRIC_PROFILE.getLogName$blesdk_productionRelease(), this.H.valueDescription());
        kd4.a((Object) put, "super.optionDescription(\u2026ofile.valueDescription())");
        return wa0.a(wa0.a(wa0.a(put, JSONKey.SKIP_READ_ACTIVITY_FILES, Boolean.valueOf(this.I)), JSONKey.SKIP_ERASE_ACTIVITY_FILES, Boolean.valueOf(this.J)), JSONKey.FILE_HANDLE, n90.a(this.F));
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.FILES;
        Object[] array = this.D.toArray(new DeviceFile[0]);
        if (array != null) {
            return wa0.a(x, jSONKey, j00.a((JSONAbleObject[]) array));
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacySyncPhase(Peripheral peripheral, Phase.a aVar, BiometricProfile biometricProfile, boolean z2, boolean z3, float f, String str) {
        super(peripheral, aVar, PhaseId.LEGACY_SYNC, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(biometricProfile, "biometricProfile");
        kd4.b(str, "phaseUuid");
        this.H = biometricProfile;
        this.I = z2;
        this.J = z3;
        this.K = f;
        this.z = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.FILE_CONFIG, ResourceType.TRANSFER_DATA}));
        this.D = new ArrayList<>();
        this.E = ua0.y.n();
        this.F = 256;
    }

    @DexIgnore
    public ArrayList<DeviceFile> i() {
        return this.D;
    }
}
