package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class LegacyOtaPhase$verifySegment$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.request.Request, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ long $expectedSegmentCRC;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.LegacyOtaPhase this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LegacyOtaPhase$verifySegment$1(com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase, long j) {
        super(1);
        this.this$0 = legacyOtaPhase;
        this.$expectedSegmentCRC = j;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.request.Request) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.request.Request request) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(request, "executedRequest");
        if (this.$expectedSegmentCRC == ((com.fossil.blesdk.obfuscated.z80) request).mo18446J()) {
            com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase = this.this$0;
            legacyOtaPhase.f2944F = legacyOtaPhase.f2943E;
            if (this.this$0.f2942D != this.this$0.f2944F) {
                this.this$0.mo7409B();
            } else if (this.this$0.f2944F == this.this$0.f2939A) {
                this.this$0.mo7415H();
            } else {
                this.this$0.mo7413F();
            }
        } else {
            com.fossil.blesdk.device.logic.phase.LegacyOtaPhase legacyOtaPhase2 = this.this$0;
            legacyOtaPhase2.f2943E = java.lang.Math.max(0, legacyOtaPhase2.f2943E - 6144);
            if (this.this$0.f2943E == 0) {
                this.this$0.mo7413F();
            } else {
                this.this$0.mo7416I();
            }
        }
    }
}
