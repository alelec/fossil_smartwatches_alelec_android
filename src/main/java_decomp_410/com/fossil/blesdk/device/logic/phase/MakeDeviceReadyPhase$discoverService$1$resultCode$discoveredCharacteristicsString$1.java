package com.fossil.blesdk.device.logic.phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredCharacteristicsString$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredCharacteristicsString$1 INSTANCE; // = new com.fossil.blesdk.device.logic.phase.MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredCharacteristicsString$1();

    @DexIgnore
    public MakeDeviceReadyPhase$discoverService$1$resultCode$discoveredCharacteristicsString$1() {
        super(1);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        return invoke((com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId) obj);
    }

    @DexIgnore
    public final java.lang.String invoke(com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId characteristicId) {
        com.fossil.blesdk.obfuscated.kd4.b(characteristicId, "characteristicId");
        return characteristicId.name();
    }
}
