package com.fossil.blesdk.device;

import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$setNotificationFilter$Anon1 extends Lambda implements xc4<NotificationFilter, String> {
    @DexIgnore
    public static /* final */ DeviceImplementation$setNotificationFilter$Anon1 INSTANCE; // = new DeviceImplementation$setNotificationFilter$Anon1();

    @DexIgnore
    public DeviceImplementation$setNotificationFilter$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(NotificationFilter notificationFilter) {
        kd4.b(notificationFilter, "it");
        return notificationFilter.toJSONString(2);
    }
}
