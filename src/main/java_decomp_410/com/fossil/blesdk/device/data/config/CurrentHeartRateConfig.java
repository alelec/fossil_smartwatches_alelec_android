package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CurrentHeartRateConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ short currentHeartRate;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CurrentHeartRateConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CurrentHeartRateConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 1) {
                return new CurrentHeartRateConfig(n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).get(0)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", " + "require: 1");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CurrentHeartRateConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CurrentHeartRateConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public CurrentHeartRateConfig[] newArray(int i) {
            return new CurrentHeartRateConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CurrentHeartRateConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) CurrentHeartRateConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.currentHeartRate == ((CurrentHeartRateConfig) obj).currentHeartRate;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CurrentHeartRateConfig");
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.currentHeartRate;
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) this.currentHeartRate).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.currentHeartRate;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.currentHeartRate);
        }
    }

    @DexIgnore
    public CurrentHeartRateConfig(short s) {
        super(DeviceConfigKey.CURRENT_HEART_RATE);
        this.currentHeartRate = s;
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            wa0.a(jSONObject, JSONKey.CURRENT_HEART_RATE, Short.valueOf(this.currentHeartRate));
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public CurrentHeartRateConfig(Parcel parcel) {
        super(parcel);
        this.currentHeartRate = n90.b(parcel.readByte());
    }
}
