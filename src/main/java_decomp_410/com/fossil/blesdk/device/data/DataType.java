package com.fossil.blesdk.device.data;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DataType {
    ACTIVITY_FILE(FileType.ACTIVITY_FILE.getFileHandleMask$blesdk_productionRelease()),
    HARDWARE_LOG(FileType.HARDWARE_LOG.getFileHandleMask$blesdk_productionRelease());
    
    @DexIgnore
    public /* final */ short fileHandleMask;
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    DataType(short s) {
        this.fileHandleMask = s;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final short getFileHandleMask$blesdk_productionRelease() {
        return this.fileHandleMask;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
