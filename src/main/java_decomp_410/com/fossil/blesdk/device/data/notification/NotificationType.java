package com.fossil.blesdk.device.data.notification;

import com.fossil.blesdk.obfuscated.kd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum NotificationType {
    UNSUPPORTED((byte) 0),
    INCOMING_CALL((byte) 1),
    TEXT((byte) 2),
    NOTIFICATION((byte) 3),
    EMAIL((byte) 4),
    CALENDAR((byte) 5),
    MISSED_CALL((byte) 6),
    REMOVED((byte) 7);
    
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    NotificationType(byte b) {
        this.id = b;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
