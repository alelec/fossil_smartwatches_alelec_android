package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.enumerate.CaloriesUnit;
import com.fossil.blesdk.device.data.enumerate.DateFormat;
import com.fossil.blesdk.device.data.enumerate.DistanceUnit;
import com.fossil.blesdk.device.data.enumerate.TemperatureUnit;
import com.fossil.blesdk.device.data.enumerate.TimeFormat;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import com.portfolio.platform.data.model.MFUser;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DisplayUnitConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ int CALORIES_BIT_INDEX; // = 1;
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ int DATA_LENGTH; // = 4;
    @DexIgnore
    public static /* final */ int DATE_BIT_INDEX; // = 4;
    @DexIgnore
    public static /* final */ int DISTANCE_BIT_INDEX; // = 2;
    @DexIgnore
    public static /* final */ int TEMPERATURE_BIT_INDEX; // = 0;
    @DexIgnore
    public static /* final */ int TIME_BIT_INDEX; // = 3;
    @DexIgnore
    public /* final */ CaloriesUnit caloriesUnit;
    @DexIgnore
    public /* final */ DateFormat dateFormat;
    @DexIgnore
    public /* final */ DistanceUnit distanceUnit;
    @DexIgnore
    public /* final */ TemperatureUnit temperatureUnit;
    @DexIgnore
    public /* final */ TimeFormat timeFormat;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DisplayUnitConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DisplayUnitConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 4) {
                int i = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0);
                TemperatureUnit a = TemperatureUnit.Companion.a((i >> 0) & 1);
                CaloriesUnit a2 = CaloriesUnit.Companion.a((i >> 1) & 1);
                DistanceUnit a3 = DistanceUnit.Companion.a((i >> 2) & 1);
                TimeFormat a4 = TimeFormat.Companion.a((i >> 3) & 1);
                DateFormat a5 = DateFormat.Companion.a((i >> 4) & 1);
                if (a != null && a2 != null && a3 != null && a4 != null && a5 != null) {
                    return new DisplayUnitConfig(a, a2, a3, a4, a5);
                }
                throw new IllegalArgumentException("Invalid data properties");
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 4");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DisplayUnitConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DisplayUnitConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public DisplayUnitConfig[] newArray(int i) {
            return new DisplayUnitConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DisplayUnitConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DisplayUnitConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DisplayUnitConfig displayUnitConfig = (DisplayUnitConfig) obj;
            return this.temperatureUnit == displayUnitConfig.temperatureUnit && this.caloriesUnit == displayUnitConfig.caloriesUnit && this.distanceUnit == displayUnitConfig.distanceUnit && this.timeFormat == displayUnitConfig.timeFormat && this.dateFormat == displayUnitConfig.dateFormat;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DisplayUnitConfig");
    }

    @DexIgnore
    public final CaloriesUnit getCaloriesUnit() {
        return this.caloriesUnit;
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((this.dateFormat.getValue$blesdk_productionRelease() << 4) | (this.temperatureUnit.getValue$blesdk_productionRelease() << 0) | 0 | (this.caloriesUnit.getValue$blesdk_productionRelease() << 1) | (this.distanceUnit.getValue$blesdk_productionRelease() << 2) | (this.timeFormat.getValue$blesdk_productionRelease() << 3)).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final DateFormat getDateFormat() {
        return this.dateFormat;
    }

    @DexIgnore
    public final DistanceUnit getDistanceUnit() {
        return this.distanceUnit;
    }

    @DexIgnore
    public final TemperatureUnit getTemperatureUnit() {
        return this.temperatureUnit;
    }

    @DexIgnore
    public final TimeFormat getTimeFormat() {
        return this.timeFormat;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.temperatureUnit.hashCode() * 31) + this.caloriesUnit.hashCode()) * 31) + this.distanceUnit.hashCode()) * 31) + this.timeFormat.hashCode()) * 31) + this.dateFormat.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.temperatureUnit.name());
        }
        if (parcel != null) {
            parcel.writeString(this.caloriesUnit.name());
        }
        if (parcel != null) {
            parcel.writeString(this.distanceUnit.name());
        }
        if (parcel != null) {
            parcel.writeString(this.timeFormat.name());
        }
        if (parcel != null) {
            parcel.writeString(this.dateFormat.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DisplayUnitConfig(TemperatureUnit temperatureUnit2, CaloriesUnit caloriesUnit2, DistanceUnit distanceUnit2, TimeFormat timeFormat2, DateFormat dateFormat2) {
        super(DeviceConfigKey.DISPLAY_UNIT);
        kd4.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        kd4.b(caloriesUnit2, "caloriesUnit");
        kd4.b(distanceUnit2, MFUser.DISTANCE_UNIT);
        kd4.b(timeFormat2, "timeFormat");
        kd4.b(dateFormat2, "dateFormat");
        this.temperatureUnit = temperatureUnit2;
        this.caloriesUnit = caloriesUnit2;
        this.distanceUnit = distanceUnit2;
        this.timeFormat = timeFormat2;
        this.dateFormat = dateFormat2;
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(jSONObject, JSONKey.TEMPERATURE, this.temperatureUnit.getLogName$blesdk_productionRelease()), JSONKey.CALORIES, this.caloriesUnit.getLogName$blesdk_productionRelease()), JSONKey.DISTANCE, this.distanceUnit.getLogName$blesdk_productionRelease()), JSONKey.TIME, this.timeFormat.getLogName$blesdk_productionRelease()), JSONKey.DATE, this.dateFormat.getLogName$blesdk_productionRelease());
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public DisplayUnitConfig(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.temperatureUnit = TemperatureUnit.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                this.caloriesUnit = CaloriesUnit.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    this.distanceUnit = DistanceUnit.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        this.timeFormat = TimeFormat.valueOf(readString4);
                        String readString5 = parcel.readString();
                        if (readString5 != null) {
                            this.dateFormat = DateFormat.valueOf(readString5);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
