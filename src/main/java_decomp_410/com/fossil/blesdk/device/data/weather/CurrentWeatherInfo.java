package com.fossil.blesdk.device.data.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.WeatherCondition;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CurrentWeatherInfo extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ int chanceOfRain;
    @DexIgnore
    public /* final */ float currentTemperature;
    @DexIgnore
    public /* final */ WeatherCondition currentWeatherCondition;
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CurrentWeatherInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CurrentWeatherInfo createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CurrentWeatherInfo(parcel, (fd4) null);
        }

        @DexIgnore
        public CurrentWeatherInfo[] newArray(int i) {
            return new CurrentWeatherInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CurrentWeatherInfo(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void chanceOfRain$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void currentTemperature$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void currentWeatherCondition$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void highTemperature$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void lowTemperature$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) CurrentWeatherInfo.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            CurrentWeatherInfo currentWeatherInfo = (CurrentWeatherInfo) obj;
            return this.currentTemperature == currentWeatherInfo.currentTemperature && this.highTemperature == currentWeatherInfo.highTemperature && this.lowTemperature == currentWeatherInfo.lowTemperature && this.chanceOfRain == currentWeatherInfo.chanceOfRain && this.currentWeatherCondition == currentWeatherInfo.currentWeatherCondition;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CurrentWeatherInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.chanceOfRain;
    }

    @DexIgnore
    public final float getCurrentTemperature() {
        return this.currentTemperature;
    }

    @DexIgnore
    public final WeatherCondition getCurrentWeatherCondition() {
        return this.currentWeatherCondition;
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.highTemperature;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.lowTemperature;
    }

    @DexIgnore
    public final JSONObject getSettingJSONData$blesdk_productionRelease() throws JSONException {
        JSONObject put = new JSONObject().put("temp", Float.valueOf(this.currentTemperature)).put("high", Float.valueOf(this.highTemperature)).put("low", Float.valueOf(this.lowTemperature)).put("rain", this.chanceOfRain).put("cond_id", this.currentWeatherCondition.getId$blesdk_productionRelease());
        kd4.a((Object) put, "JSONObject()\n           \u2026rrentWeatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((Float.valueOf(this.currentTemperature).hashCode() * 31) + Float.valueOf(this.highTemperature).hashCode()) * 31) + Float.valueOf(this.lowTemperature).hashCode()) * 31) + this.chanceOfRain) * 31) + this.currentWeatherCondition.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.CURRENT_TEMPERATURE, Float.valueOf(this.currentTemperature)), JSONKey.HIGH_TEMPERATURE, Float.valueOf(this.highTemperature)), JSONKey.LOW_TEMPERATURE, Float.valueOf(this.lowTemperature)), JSONKey.CHANCE_OF_RAIN, Integer.valueOf(this.chanceOfRain)), JSONKey.WEATHER_CONDITION, this.currentWeatherCondition.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeFloat(this.currentTemperature);
        }
        if (parcel != null) {
            parcel.writeFloat(this.highTemperature);
        }
        if (parcel != null) {
            parcel.writeFloat(this.lowTemperature);
        }
        if (parcel != null) {
            parcel.writeInt(this.chanceOfRain);
        }
        if (parcel != null) {
            parcel.writeString(this.currentWeatherCondition.name());
        }
    }

    @DexIgnore
    public CurrentWeatherInfo(float f, float f2, float f3, int i, WeatherCondition weatherCondition) {
        kd4.b(weatherCondition, "currentWeatherCondition");
        this.currentTemperature = n90.a(f, 2);
        this.highTemperature = n90.a(f2, 2);
        this.lowTemperature = n90.a(f3, 2);
        this.chanceOfRain = i;
        this.currentWeatherCondition = weatherCondition;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CurrentWeatherInfo(Parcel parcel) {
        throw null;
/*        this(r1, r2, r3, r4, WeatherCondition.valueOf(r7));
        float readFloat = parcel.readFloat();
        float readFloat2 = parcel.readFloat();
        float readFloat3 = parcel.readFloat();
        int readInt = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            return;
        }
        kd4.a();
        throw null;
*/    }
}
