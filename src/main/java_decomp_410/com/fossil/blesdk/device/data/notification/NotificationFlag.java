package com.fossil.blesdk.device.data.notification;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.ArrayList;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum NotificationFlag {
    SILENT((byte) 1),
    IMPORTANT((byte) 2),
    PRE_EXISTING((byte) 4),
    ALLOW_USER_ACTION((byte) 8);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ byte id;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final NotificationFlag[] a(String[] strArr) {
            NotificationFlag notificationFlag;
            kd4.b(strArr, "stringArray");
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                NotificationFlag[] values = NotificationFlag.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        notificationFlag = null;
                        break;
                    }
                    notificationFlag = values[i];
                    if (kd4.a((Object) notificationFlag.getLogName$blesdk_productionRelease(), (Object) str) || kd4.a((Object) notificationFlag.name(), (Object) str)) {
                        break;
                    }
                    i++;
                }
                if (notificationFlag != null) {
                    arrayList.add(notificationFlag);
                }
            }
            Object[] array = arrayList.toArray(new NotificationFlag[0]);
            if (array != null) {
                return (NotificationFlag[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    NotificationFlag(byte b) {
        this.id = b;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte getId$blesdk_productionRelease() {
        return this.id;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
