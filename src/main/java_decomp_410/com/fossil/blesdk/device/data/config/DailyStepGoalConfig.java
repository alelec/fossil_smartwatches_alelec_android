package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailyStepGoalConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_STEP; // = 0;
    @DexIgnore
    public static /* final */ long e; // = n90.a(jd4.a);
    @DexIgnore
    public /* final */ long step;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DailyStepGoalConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DailyStepGoalConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 4) {
                return new DailyStepGoalConfig(n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 4");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DailyStepGoalConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DailyStepGoalConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public DailyStepGoalConfig[] newArray(int i) {
            return new DailyStepGoalConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DailyStepGoalConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DailyStepGoalConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.step == ((DailyStepGoalConfig) obj).step;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.step).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final long getStep() {
        return this.step;
    }

    @DexIgnore
    public int hashCode() {
        return Long.valueOf(this.step).hashCode();
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        long j = e;
        long j2 = this.step;
        if (!(0 <= j2 && j >= j2)) {
            throw new IllegalArgumentException("step(" + this.step + ") is out of range " + "[0, " + e + "].");
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.step);
        }
    }

    @DexIgnore
    public DailyStepGoalConfig(long j) throws IllegalArgumentException {
        super(DeviceConfigKey.DAILY_STEP_GOAL);
        this.step = j;
        j();
    }

    @DexIgnore
    public Long valueDescription() {
        return Long.valueOf(this.step);
    }

    @DexIgnore
    public DailyStepGoalConfig(Parcel parcel) {
        super(parcel);
        this.step = parcel.readLong();
        j();
    }
}
