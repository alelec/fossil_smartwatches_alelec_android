package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartRateModeConfig extends DeviceConfigItem implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ HeartRateMode heartRateMode;

    @DexIgnore
    public enum HeartRateMode {
        CONTINUOUS((byte) 0),
        LOW_POWER((byte) 1),
        DISABLE((byte) 2);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ byte value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final HeartRateMode a(byte b) throws IllegalArgumentException {
                HeartRateMode heartRateMode;
                HeartRateMode[] values = HeartRateMode.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        heartRateMode = null;
                        break;
                    }
                    heartRateMode = values[i];
                    if (heartRateMode.getValue$blesdk_productionRelease() == b) {
                        break;
                    }
                    i++;
                }
                if (heartRateMode != null) {
                    return heartRateMode;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        HeartRateMode(byte b) {
            this.value = b;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final byte getValue$blesdk_productionRelease() {
            return this.value;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<HeartRateModeConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HeartRateModeConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 1) {
                return new HeartRateModeConfig(HeartRateMode.Companion.a(bArr[0]));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 1");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public HeartRateModeConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new HeartRateModeConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public HeartRateModeConfig[] newArray(int i) {
            return new HeartRateModeConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ HeartRateModeConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) HeartRateModeConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.heartRateMode == ((HeartRateModeConfig) obj).heartRateMode;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.heartRateMode.getValue$blesdk_productionRelease()).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final HeartRateMode getHeartRateMode() {
        return this.heartRateMode;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.heartRateMode.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.heartRateMode.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateModeConfig(HeartRateMode heartRateMode2) {
        super(DeviceConfigKey.HEART_RATE_MODE);
        kd4.b(heartRateMode2, "heartRateMode");
        this.heartRateMode = heartRateMode2;
    }

    @DexIgnore
    public String valueDescription() {
        return this.heartRateMode.getLogName$blesdk_productionRelease();
    }

    @DexIgnore
    public HeartRateModeConfig(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.heartRateMode = HeartRateMode.valueOf(readString);
        } else {
            kd4.a();
            throw null;
        }
    }
}
