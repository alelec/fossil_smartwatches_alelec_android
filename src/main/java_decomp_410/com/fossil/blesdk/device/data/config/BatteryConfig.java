package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BatteryConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_PERCENTAGE; // = 100;
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_VOLTAGE; // = 4400;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_PERCENTAGE; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_VOLTAGE; // = 0;
    @DexIgnore
    public /* final */ byte percentage;
    @DexIgnore
    public /* final */ short voltage;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BatteryConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final BatteryConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 3) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new BatteryConfig(order.getShort(0), order.get(2));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 3");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BatteryConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BatteryConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public BatteryConfig[] newArray(int i) {
            return new BatteryConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BatteryConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) BatteryConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BatteryConfig batteryConfig = (BatteryConfig) obj;
            return this.voltage == batteryConfig.voltage && this.percentage == batteryConfig.percentage;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).putShort(this.voltage).put(this.percentage).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final byte getPercentage() {
        return this.percentage;
    }

    @DexIgnore
    public final short getVoltage() {
        return this.voltage;
    }

    @DexIgnore
    public int hashCode() {
        return (this.voltage * 31) + this.percentage;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        short s = this.voltage;
        boolean z = true;
        if (s >= 0 && 4400 >= s) {
            byte b = this.percentage;
            if (b < 0 || 100 < b) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException("percentage(" + this.percentage + ") is out of range " + "[0, 100].");
            }
            return;
        }
        throw new IllegalArgumentException("voltage(" + this.voltage + ") is out of range " + "[0, 4400].");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(n90.b(this.voltage));
        }
        if (parcel != null) {
            parcel.writeByte(this.percentage);
        }
    }

    @DexIgnore
    public BatteryConfig(short s, byte b) throws IllegalArgumentException {
        super(DeviceConfigKey.BATTERY);
        this.voltage = s;
        this.percentage = b;
        j();
    }

    @DexIgnore
    public JSONObject valueDescription() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("voltage", Short.valueOf(this.voltage));
            jSONObject.put("percentage", Byte.valueOf(this.percentage));
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public BatteryConfig(Parcel parcel) {
        super(parcel);
        this.voltage = (short) parcel.readInt();
        this.percentage = parcel.readByte();
        j();
    }
}
