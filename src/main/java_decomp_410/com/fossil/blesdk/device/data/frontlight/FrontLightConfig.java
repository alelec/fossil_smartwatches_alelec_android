package com.fossil.blesdk.device.data.frontlight;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FrontLightConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ boolean isEnable;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<FrontLightConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public FrontLightConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new FrontLightConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public FrontLightConfig[] newArray(int i) {
            return new FrontLightConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ FrontLightConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void isEnable$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) FrontLightConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.isEnable == ((FrontLightConfig) obj).isEnable;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.frontlight.FrontLightConfig");
    }

    @DexIgnore
    public final JSONObject getFrontLightSettingsJSON$blesdk_productionRelease() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("set", i());
            jSONObject.put("push", jSONObject2);
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int hashCode() {
        return Boolean.valueOf(this.isEnable).hashCode();
    }

    @DexIgnore
    public final JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("is_front_light_enabled", this.isEnable);
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final boolean isEnable$blesdk_productionRelease() {
        return this.isEnable;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return i();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.isEnable ? 1 : 0);
        }
    }

    @DexIgnore
    public FrontLightConfig(boolean z) {
        this.isEnable = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public FrontLightConfig(Parcel parcel) {
        this(parcel.readInt() != 0);
    }
}
