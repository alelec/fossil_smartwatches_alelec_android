package com.fossil.blesdk.device.data.alarm;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RepeatedAlarm extends Alarm {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<RepeatedAlarm> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public RepeatedAlarm createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new RepeatedAlarm(parcel, (fd4) null);
        }

        @DexIgnore
        public RepeatedAlarm[] newArray(int i) {
            return new RepeatedAlarm[i];
        }
    }

    @DexIgnore
    public /* synthetic */ RepeatedAlarm(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (!(!getDaysOfWeek$blesdk_productionRelease().isEmpty())) {
            throw new IllegalArgumentException("Day Of Week must not be empty.");
        }
    }

    @DexIgnore
    public final Set<Weekday> getDaysOfWeek() {
        return getDaysOfWeek$blesdk_productionRelease();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RepeatedAlarm(byte b, byte b2, Set<Weekday> set) {
        super(b, b2, set, true, false, true);
        kd4.b(set, "daysOfWeek");
        i();
    }

    @DexIgnore
    public RepeatedAlarm(Parcel parcel) {
        super(parcel);
        i();
    }
}
