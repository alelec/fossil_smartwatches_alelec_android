package com.fossil.blesdk.device.data.background;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig;
import com.fossil.blesdk.model.preset.DevicePresetItem;
import com.fossil.blesdk.obfuscated.cb4;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundImageConfig extends DevicePresetItem {
    @DexIgnore
    @Keep
    public static /* final */ int BACKGROUND_Z_INDEX; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int BOTTOM_BACKGROUND_ANGLE; // = 180;
    @DexIgnore
    @Keep
    public static /* final */ int COMPLICATION_Z_INDEX; // = 1;
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int LEFT_BACKGROUND_ANGLE; // = 270;
    @DexIgnore
    @Keep
    public static /* final */ int MAIN_BACKGROUND_ANGLE; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int RIGHT_BACKGROUND_ANGLE; // = 90;
    @DexIgnore
    @Keep
    public static /* final */ int TOP_BACKGROUND_ANGLE; // = 0;
    @DexIgnore
    public byte[] backgroundConfigData;
    @DexIgnore
    public /* final */ ArrayList<BackgroundImage> backgroundImageList;
    @DexIgnore
    public /* final */ BackgroundImage bottomBackground;
    @DexIgnore
    public /* final */ BackgroundImage leftBackground;
    @DexIgnore
    public /* final */ BackgroundImage mainBackground;
    @DexIgnore
    public /* final */ BackgroundImage rightBackground;
    @DexIgnore
    public /* final */ BackgroundImage topBackground;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BackgroundImageConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final boolean a(BackgroundImage backgroundImage, BackgroundImage backgroundImage2) {
            kd4.b(backgroundImage, "first");
            kd4.b(backgroundImage2, "second");
            return kd4.a((Object) backgroundImage.getFileName(), (Object) backgroundImage2.getFileName()) && !Arrays.equals(backgroundImage.getFileData(), backgroundImage2.getFileData());
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BackgroundImageConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BackgroundImageConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public BackgroundImageConfig[] newArray(int i) {
            return new BackgroundImageConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundImageConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void bottomBackground$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void leftBackground$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void mainBackground$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void rightBackground$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void topBackground$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) BackgroundImageConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BackgroundImageConfig backgroundImageConfig = (BackgroundImageConfig) obj;
            return !(kd4.a((Object) this.mainBackground, (Object) backgroundImageConfig.mainBackground) ^ true) && !(kd4.a((Object) this.topBackground, (Object) backgroundImageConfig.topBackground) ^ true) && !(kd4.a((Object) this.rightBackground, (Object) backgroundImageConfig.rightBackground) ^ true) && !(kd4.a((Object) this.bottomBackground, (Object) backgroundImageConfig.bottomBackground) ^ true) && !(kd4.a((Object) this.leftBackground, (Object) backgroundImageConfig.leftBackground) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.background.BackgroundImageConfig");
    }

    @DexIgnore
    public JSONObject getAssignmentJSON$blesdk_productionRelease() {
        throw null;
        // JSONObject put = new JSONObject().put("watchFace._.config.backgrounds", new JSONArray().put(this.mainBackground.getAssignmentJSON$blesdk_productionRelease()).put(this.topBackground.getAssignmentJSON$blesdk_productionRelease()).put(this.rightBackground.getAssignmentJSON$blesdk_productionRelease()).put(this.bottomBackground.getAssignmentJSON$blesdk_productionRelease()).put(this.leftBackground.getAssignmentJSON$blesdk_productionRelease()));
        // kd4.a((Object) put, "JSONObject().put(UIScrip\u2026oundsAssignmentJsonArray)");
        // return put;
    }

    @DexIgnore
    public final byte[] getBackgroundConfigData$blesdk_productionRelease() {
        return this.backgroundConfigData;
    }

    @DexIgnore
    public final ArrayList<BackgroundImage> getBackgroundImageList$blesdk_productionRelease() {
        return this.backgroundImageList;
    }

    @DexIgnore
    public final BackgroundImage getBottomBackground() {
        return this.bottomBackground;
    }

    @DexIgnore
    public final BackgroundImage getLeftBackground() {
        return this.leftBackground;
    }

    @DexIgnore
    public final BackgroundImage getMainBackground() {
        return this.mainBackground;
    }

    @DexIgnore
    public final BackgroundImage getRightBackground() {
        return this.rightBackground;
    }

    @DexIgnore
    public final BackgroundImage getTopBackground() {
        return this.topBackground;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.mainBackground.hashCode() * 31) + this.topBackground.hashCode()) * 31) + this.rightBackground.hashCode()) * 31) + this.bottomBackground.hashCode()) * 31) + this.leftBackground.hashCode();
    }

    @DexIgnore
    public final void i() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        throw null;
        // for (BackgroundImage entryData$blesdk_productionRelease : this.backgroundImageList) {
        //     byteArrayOutputStream.write(entryData$blesdk_productionRelease.getEntryData$blesdk_productionRelease());
        // }
        // byte[] byteArray = byteArrayOutputStream.toByteArray();
        // kd4.a((Object) byteArray, "entryData.toByteArray()");
        // this.backgroundConfigData = byteArray;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        List c = kb4.c(cb4.a((Object[]) new BackgroundImage[]{this.mainBackground, this.topBackground, this.rightBackground, this.bottomBackground, this.leftBackground}));
        int size = c.size() - 1;
        for (int i = 0; i < size; i++) {
            int size2 = c.size();
            int i2 = i;
            while (i2 < size2) {
                if (!CREATOR.a((BackgroundImage) c.get(i), (BackgroundImage) c.get(i2))) {
                    i2++;
                } else {
                    throw new IllegalArgumentException("Background images " + ((BackgroundImage) c.get(i)).getFileName() + " and  " + ((BackgroundImage) c.get(i2)).getFileName() + ' ' + "have same names but different data.");
                }
            }
        }
        ArrayList<BackgroundImage> arrayList = this.backgroundImageList;
        ArrayList arrayList2 = new ArrayList();
        for (Object next : c) {
            if (!((BackgroundImage) next).isEmptyFile$blesdk_productionRelease()) {
                arrayList2.add(next);
            }
        }
        arrayList.addAll(arrayList2);
    }

    @DexIgnore
    public final void setBackgroundConfigData$blesdk_productionRelease(byte[] bArr) {
        kd4.b(bArr, "<set-?>");
        this.backgroundConfigData = bArr;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        throw null;
        // JSONObject jSONObject = new JSONObject();
        // try {
        //     wa0.a(jSONObject, JSONKey.MAIN, this.mainBackground.toJSONObject());
        //     wa0.a(jSONObject, JSONKey.TOP_COMPLICATION, this.topBackground.toJSONObject());
        //     wa0.a(jSONObject, JSONKey.RIGHT_COMPLICATION, this.rightBackground.toJSONObject());
        //     wa0.a(jSONObject, JSONKey.BOTTOM_COMPLICATION, this.bottomBackground.toJSONObject());
        //     wa0.a(jSONObject, JSONKey.LEFT_COMPLICATION, this.leftBackground.toJSONObject());
        // } catch (JSONException e) {
        //     da0.l.a(e);
        // }
        // return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.mainBackground, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.topBackground, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.rightBackground, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.bottomBackground, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.leftBackground, i);
        }
    }

    @DexIgnore
    public BackgroundImageConfig(BackgroundImage backgroundImage, BackgroundImage backgroundImage2, BackgroundImage backgroundImage3, BackgroundImage backgroundImage4, BackgroundImage backgroundImage5) throws IllegalArgumentException {
        kd4.b(backgroundImage, "mainBackground");
        kd4.b(backgroundImage2, "topBackground");
        kd4.b(backgroundImage3, "rightBackground");
        kd4.b(backgroundImage4, "bottomBackground");
        kd4.b(backgroundImage5, "leftBackground");
        this.backgroundImageList = new ArrayList<>();
        this.backgroundConfigData = new byte[0];
        this.mainBackground = backgroundImage;
        this.topBackground = backgroundImage2;
        this.rightBackground = backgroundImage3;
        this.bottomBackground = backgroundImage4;
        this.leftBackground = backgroundImage5;
        this.mainBackground.setPositionConfig$blesdk_productionRelease(new BackgroundPositionConfig(0, 0, 0));
        this.topBackground.setPositionConfig$blesdk_productionRelease(new BackgroundPositionConfig(0, 62, 1));
        this.rightBackground.setPositionConfig$blesdk_productionRelease(new BackgroundPositionConfig(90, 62, 1));
        this.bottomBackground.setPositionConfig$blesdk_productionRelease(new BackgroundPositionConfig(BOTTOM_BACKGROUND_ANGLE, 62, 1));
        this.leftBackground.setPositionConfig$blesdk_productionRelease(new BackgroundPositionConfig(LEFT_BACKGROUND_ANGLE, 62, 1));
        j();
        i();
    }

    @DexIgnore
    public BackgroundImageConfig(Parcel parcel) {
        super(parcel);
        this.backgroundImageList = new ArrayList<>();
        this.backgroundConfigData = new byte[0];
        Parcelable readParcelable = parcel.readParcelable(BackgroundImage.class.getClassLoader());
        if (readParcelable != null) {
            this.mainBackground = (BackgroundImage) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(BackgroundImage.class.getClassLoader());
            if (readParcelable2 != null) {
                this.topBackground = (BackgroundImage) readParcelable2;
                Parcelable readParcelable3 = parcel.readParcelable(BackgroundImage.class.getClassLoader());
                if (readParcelable3 != null) {
                    this.rightBackground = (BackgroundImage) readParcelable3;
                    Parcelable readParcelable4 = parcel.readParcelable(BackgroundImage.class.getClassLoader());
                    if (readParcelable4 != null) {
                        this.bottomBackground = (BackgroundImage) readParcelable4;
                        Parcelable readParcelable5 = parcel.readParcelable(BackgroundImage.class.getClassLoader());
                        if (readParcelable5 != null) {
                            this.leftBackground = (BackgroundImage) readParcelable5;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
