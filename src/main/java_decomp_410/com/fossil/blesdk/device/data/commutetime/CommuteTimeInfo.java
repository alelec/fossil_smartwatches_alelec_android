package com.fossil.blesdk.device.data.commutetime;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.facebook.share.internal.ShareConstants;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeInfo extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_TRAFFIC_LENGTH; // = 10;
    @DexIgnore
    public /* final */ int commuteTimeInMinute;
    @DexIgnore
    public /* final */ String destination;
    @DexIgnore
    public /* final */ String traffic;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeInfo createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeInfo(parcel, (fd4) null);
        }

        @DexIgnore
        public CommuteTimeInfo[] newArray(int i) {
            return new CommuteTimeInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeInfo(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void commuteTimeInMinute$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void destination$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void traffic$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getCommuteTimeInMinute() {
        return this.commuteTimeInMinute;
    }

    @DexIgnore
    public final String getDestination() {
        return this.destination;
    }

    @DexIgnore
    public final String getTraffic() {
        return this.traffic;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        if (!(this.traffic.length() <= 10)) {
            throw new IllegalArgumentException(("traffic(" + this.traffic + ") length must be less than or equal to 10").toString());
        }
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.DESTINATION, this.destination), JSONKey.COMMUTE_TIME_IN_MINUTE, Integer.valueOf(this.commuteTimeInMinute)), JSONKey.TRAFFIC, this.traffic);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CommuteTimeInfo(Parcel parcel) {
        throw null;
        // this(r0, r2, r4);
        // String readString = parcel.readString();
        // if (readString != null) {
        //     int readInt = parcel.readInt();
        //     String readString2 = parcel.readString();
        //     if (readString2 != null) {
        //     } else {
        //         kd4.a();
        //         throw null;
        //     }
        // } else {
        //     kd4.a();
        //     throw null;
        // }
    }

    @DexIgnore
    public CommuteTimeInfo(String str, int i, String str2) throws IllegalArgumentException {
        kd4.b(str, ShareConstants.DESTINATION);
        kd4.b(str2, "traffic");
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
        i();
    }
}
