package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class FileHandle extends JSONAbleObject {
    @DexIgnore
    public static /* final */ a Companion; // = new a((fd4) null);
    @DexIgnore
    public /* final */ short fileHandle;
    @DexIgnore
    public /* final */ byte fileIndex;
    @DexIgnore
    public /* final */ byte fileType;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a(short s) {
            if (s == 256) {
                return "legacy_activity_file";
            }
            if (s == 23131) {
                return "legacy_ota";
            }
            switch (s) {
                case 1792:
                    return "asset_background_file";
                case 1793:
                    return "asset_notification_icon_file";
                case 1794:
                    return "asset_localization_file";
                default:
                    FileType a = FileType.Companion.a(s);
                    if (a != null) {
                        String logName$blesdk_productionRelease = a.getLogName$blesdk_productionRelease();
                        if (logName$blesdk_productionRelease != null) {
                            return logName$blesdk_productionRelease;
                        }
                    }
                    return "unknown";
            }
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public FileHandle(short s) {
        this.fileHandle = s;
        ByteBuffer putShort = ByteBuffer.allocate(2).putShort(this.fileHandle);
        kd4.a((Object) putShort, "ByteBuffer.allocate(2).putShort(fileHandle)");
        this.fileType = putShort.get(0);
        this.fileIndex = putShort.get(1);
    }

    @DexIgnore
    public final short getFileHandle$blesdk_productionRelease() {
        return this.fileHandle;
    }

    @DexIgnore
    public final byte getFileIndex$blesdk_productionRelease() {
        return this.fileIndex;
    }

    @DexIgnore
    public final byte getFileType$blesdk_productionRelease() {
        return this.fileType;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.FILE_TYPE, n90.a(this.fileType)), JSONKey.FILE_INDEX, n90.a(this.fileIndex)), JSONKey.FILE_HANDLE, n90.a(this.fileHandle)), JSONKey.FILE_HANDLE_DESCRIPTION, Companion.a(this.fileHandle));
    }

    @DexIgnore
    public FileHandle(byte b, byte b2) {
        this(ByteBuffer.allocate(2).put(b).put(b2).getShort(0));
    }
}
