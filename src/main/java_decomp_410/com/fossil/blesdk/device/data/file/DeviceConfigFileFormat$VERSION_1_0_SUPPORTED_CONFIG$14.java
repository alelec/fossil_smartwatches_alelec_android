package com.fossil.blesdk.device.data.file;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$14 extends kotlin.jvm.internal.FunctionReference implements com.fossil.blesdk.obfuscated.xc4<byte[], com.fossil.blesdk.device.data.config.HeartRateModeConfig> {
    @DexIgnore
    public DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$14(com.fossil.blesdk.device.data.config.HeartRateModeConfig.C1153a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final java.lang.String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final com.fossil.blesdk.obfuscated.je4 getOwner() {
        return com.fossil.blesdk.obfuscated.md4.m25263a(com.fossil.blesdk.device.data.config.HeartRateModeConfig.C1153a.class);
    }

    @DexIgnore
    public final java.lang.String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/HeartRateModeConfig;";
    }

    @DexIgnore
    public final com.fossil.blesdk.device.data.config.HeartRateModeConfig invoke(byte[] bArr) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bArr, "p1");
        return ((com.fossil.blesdk.device.data.config.HeartRateModeConfig.C1153a) this.receiver).mo6849a(bArr);
    }
}
