package com.fossil.blesdk.device.data.alarm;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.LinkedHashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class OneShotAlarm extends Alarm implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ Weekday dayOfWeek;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<OneShotAlarm> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final LinkedHashSet<Weekday> a(Weekday weekday) {
            LinkedHashSet<Weekday> linkedHashSet = new LinkedHashSet<>();
            if (weekday == null) {
                hb4.a(linkedHashSet, (Object[]) Weekday.values());
            } else {
                linkedHashSet.add(weekday);
            }
            return linkedHashSet;
        }

        @DexIgnore
        public OneShotAlarm createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new OneShotAlarm(parcel, (fd4) null);
        }

        @DexIgnore
        public OneShotAlarm[] newArray(int i) {
            return new OneShotAlarm[i];
        }
    }

    @DexIgnore
    public /* synthetic */ OneShotAlarm(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final Weekday getDayOfWeek() {
        return this.dayOfWeek;
    }

    @DexIgnore
    public OneShotAlarm(byte b, byte b2, Weekday weekday) throws IllegalArgumentException {
        super(b, b2, CREATOR.a(weekday), false, false, true);
        this.dayOfWeek = weekday;
    }

    @DexIgnore
    public OneShotAlarm(Parcel parcel) {
        super(parcel);
        this.dayOfWeek = (getDaysOfWeek$blesdk_productionRelease().isEmpty() || getDaysOfWeek$blesdk_productionRelease().size() == Weekday.values().length) ? null : (Weekday) kb4.d(getDaysOfWeek$blesdk_productionRelease()).get(0);
    }
}
