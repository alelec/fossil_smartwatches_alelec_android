package com.fossil.blesdk.device.data.notification;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationHandMovingConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ int DATA_LENGTH; // = 8;
    @DexIgnore
    @Keep
    public static /* final */ short HAND_DEGREE_DEVICE_DEFAULT_POSITION; // = -1;
    @DexIgnore
    @Keep
    public static /* final */ short HAND_DEGREE_NO_MOVE; // = -2;
    @DexIgnore
    @Keep
    public static /* final */ short MAXIMUM_DEGREE; // = 359;
    @DexIgnore
    @Keep
    public static /* final */ int MAXIMUM_DURATION_IN_MS; // = 60000;
    @DexIgnore
    @Keep
    public static /* final */ short MINIMUM_DEGREE; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_DURATION_IN_MS; // = 1000;
    @DexIgnore
    public /* final */ int durationInMs;
    @DexIgnore
    public /* final */ short hourDegree;
    @DexIgnore
    public /* final */ short minuteDegree;
    @DexIgnore
    public /* final */ short subeyeDegree;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<NotificationHandMovingConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public NotificationHandMovingConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new NotificationHandMovingConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public NotificationHandMovingConfig[] newArray(int i) {
            return new NotificationHandMovingConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ NotificationHandMovingConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) NotificationHandMovingConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationHandMovingConfig notificationHandMovingConfig = (NotificationHandMovingConfig) obj;
            return this.hourDegree == notificationHandMovingConfig.hourDegree && this.minuteDegree == notificationHandMovingConfig.minuteDegree && this.subeyeDegree == notificationHandMovingConfig.subeyeDegree && this.durationInMs == notificationHandMovingConfig.durationInMs;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig");
    }

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort(this.hourDegree).putShort(this.minuteDegree).putShort(this.subeyeDegree).putShort((short) this.durationInMs).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final int getDurationInMs() {
        return this.durationInMs;
    }

    @DexIgnore
    public final short getHourDegree() {
        return this.hourDegree;
    }

    @DexIgnore
    public final short getMinuteDegree() {
        return this.minuteDegree;
    }

    @DexIgnore
    public final short getSubeyeDegree() {
        return this.subeyeDegree;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.hourDegree * 31) + this.minuteDegree) * 31) + this.subeyeDegree) * 31) + this.durationInMs;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        short s = this.hourDegree;
        boolean z = false;
        if (s == -2 || s == -1 || (s >= 0 && 359 >= s)) {
            short s2 = this.minuteDegree;
            if (s2 == -2 || s2 == -1 || (s2 >= 0 && 359 >= s2)) {
                short s3 = this.subeyeDegree;
                if (s3 == -2 || s3 == -1 || (s3 >= 0 && 359 >= s3)) {
                    int i = this.durationInMs;
                    if (1000 <= i && 60000 >= i) {
                        z = true;
                    }
                    if (!z) {
                        throw new IllegalArgumentException("durationInMs (" + this.durationInMs + ") is out of range " + "[1000, 60000].");
                    }
                    return;
                }
                throw new IllegalArgumentException("subeyeDegree (" + this.subeyeDegree + ") must be equal to " + "-2 or -1 or in range" + "[0, 359].");
            }
            throw new IllegalArgumentException("minuteDegree (" + this.minuteDegree + ") must be equal to " + "-2 or -1 or in range" + "[0, 359].");
        }
        throw new IllegalArgumentException("hourDegree (" + this.hourDegree + ") must be equal to " + "-2 or -1 or in range" + "[0, 359].");
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.HOUR_DEGREE, Short.valueOf(this.hourDegree)), JSONKey.MINUTE_DEGREE, Short.valueOf(this.minuteDegree)), JSONKey.SUBEYE_DEGREE, Short.valueOf(this.subeyeDegree)), JSONKey.DURATION_IN_MS, Integer.valueOf(this.durationInMs));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.hourDegree);
        }
        if (parcel != null) {
            parcel.writeInt(this.minuteDegree);
        }
        if (parcel != null) {
            parcel.writeInt(this.subeyeDegree);
        }
        if (parcel != null) {
            parcel.writeInt(this.durationInMs);
        }
    }

    @DexIgnore
    public NotificationHandMovingConfig(short s, short s2, short s3, int i) throws IllegalArgumentException {
        this.hourDegree = s;
        this.minuteDegree = s2;
        this.subeyeDegree = s3;
        this.durationInMs = i;
        i();
    }

    @DexIgnore
    public NotificationHandMovingConfig(Parcel parcel) {
        this((short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt(), parcel.readInt());
    }
}
