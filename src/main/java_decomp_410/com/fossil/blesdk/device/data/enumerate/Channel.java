package com.fossil.blesdk.device.data.enumerate;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum Channel {
    DC(GattCharacteristic.CharacteristicId.DC),
    FTC(GattCharacteristic.CharacteristicId.FTC),
    FTD(GattCharacteristic.CharacteristicId.FTD),
    AUT(GattCharacteristic.CharacteristicId.AUTHENTICATION),
    ASYNC(GattCharacteristic.CharacteristicId.ASYNC),
    FTD_1(GattCharacteristic.CharacteristicId.FTD_1);
    
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId gattCharacteristicId;

    @DexIgnore
    Channel(GattCharacteristic.CharacteristicId characteristicId) {
        this.gattCharacteristicId = characteristicId;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId getGattCharacteristicId$blesdk_productionRelease() {
        return this.gattCharacteristicId;
    }
}
