package com.fossil.blesdk.device.data.music;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Arrays;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TrackInfo extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_ALBUM_NAME_LENGTH; // = 50;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_APP_NAME_LENGTH; // = 50;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_ARTIST_NAME_LENGTH; // = 50;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_TITLE_NAME_LENGTH; // = 50;
    @DexIgnore
    public /* final */ String albumName;
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String artistName;
    @DexIgnore
    public /* final */ String trackTitle;
    @DexIgnore
    public /* final */ byte volume;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<TrackInfo> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public TrackInfo createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new TrackInfo(parcel, (fd4) null);
        }

        @DexIgnore
        public TrackInfo[] newArray(int i) {
            return new TrackInfo[i];
        }
    }

    @DexIgnore
    public /* synthetic */ TrackInfo(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) TrackInfo.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            TrackInfo trackInfo = (TrackInfo) obj;
            return !(kd4.a((Object) this.appName, (Object) trackInfo.appName) ^ true) && this.volume == trackInfo.volume && !(kd4.a((Object) this.trackTitle, (Object) trackInfo.trackTitle) ^ true) && !(kd4.a((Object) this.artistName, (Object) trackInfo.artistName) ^ true) && !(kd4.a((Object) this.albumName, (Object) trackInfo.albumName) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.TrackInfo");
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.albumName;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.artistName;
    }

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        ByteBuffer allocate = ByteBuffer.allocate(6);
        kd4.a((Object) allocate, "ByteBuffer.allocate(HEADER_LENGTH.toInt())");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        String str = this.appName;
        Charset f = ua0.y.f();
        if (str != null) {
            byte[] bytes = str.getBytes(f);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            byte min = (byte) Math.min(bytes.length + 1, 50);
            String str2 = this.trackTitle;
            Charset f2 = ua0.y.f();
            if (str2 != null) {
                byte[] bytes2 = str2.getBytes(f2);
                kd4.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                byte min2 = (byte) Math.min(bytes2.length + 1, 50);
                String str3 = this.artistName;
                Charset f3 = ua0.y.f();
                if (str3 != null) {
                    byte[] bytes3 = str3.getBytes(f3);
                    kd4.a((Object) bytes3, "(this as java.lang.String).getBytes(charset)");
                    byte min3 = (byte) Math.min(bytes3.length + 1, 50);
                    String str4 = this.albumName;
                    Charset f4 = ua0.y.f();
                    if (str4 != null) {
                        byte[] bytes4 = str4.getBytes(f4);
                        kd4.a((Object) bytes4, "(this as java.lang.String).getBytes(charset)");
                        byte min4 = (byte) Math.min(bytes4.length + 1, 50);
                        short s = (short) (min + 7 + min2 + min3 + min4);
                        allocate.putShort(s);
                        allocate.put(min);
                        allocate.put(min2);
                        allocate.put(min3);
                        allocate.put(min4);
                        ByteBuffer allocate2 = ByteBuffer.allocate(s);
                        kd4.a((Object) allocate2, "ByteBuffer.allocate(totalLen.toInt())");
                        allocate2.order(ByteOrder.LITTLE_ENDIAN);
                        allocate2.put(allocate.array());
                        ByteBuffer allocate3 = ByteBuffer.allocate(s - 6);
                        kd4.a((Object) allocate3, "ByteBuffer.allocate(totalLen - HEADER_LENGTH)");
                        allocate3.order(ByteOrder.LITTLE_ENDIAN);
                        allocate3.put(this.volume);
                        String str5 = this.appName;
                        Charset f5 = ua0.y.f();
                        if (str5 != null) {
                            byte[] bytes5 = str5.getBytes(f5);
                            kd4.a((Object) bytes5, "(this as java.lang.String).getBytes(charset)");
                            allocate3.put(Arrays.copyOfRange(bytes5, 0, min - 1)).put((byte) 0);
                            String str6 = this.trackTitle;
                            Charset f6 = ua0.y.f();
                            if (str6 != null) {
                                byte[] bytes6 = str6.getBytes(f6);
                                kd4.a((Object) bytes6, "(this as java.lang.String).getBytes(charset)");
                                allocate3.put(Arrays.copyOfRange(bytes6, 0, min2 - 1)).put((byte) 0);
                                String str7 = this.artistName;
                                Charset f7 = ua0.y.f();
                                if (str7 != null) {
                                    byte[] bytes7 = str7.getBytes(f7);
                                    kd4.a((Object) bytes7, "(this as java.lang.String).getBytes(charset)");
                                    allocate3.put(Arrays.copyOfRange(bytes7, 0, min3 - 1)).put((byte) 0);
                                    String str8 = this.albumName;
                                    Charset f8 = ua0.y.f();
                                    if (str8 != null) {
                                        byte[] bytes8 = str8.getBytes(f8);
                                        kd4.a((Object) bytes8, "(this as java.lang.String).getBytes(charset)");
                                        allocate3.put(Arrays.copyOfRange(bytes8, 0, min4 - 1)).put((byte) 0);
                                        allocate2.put(allocate3.array());
                                        byte[] array = allocate2.array();
                                        kd4.a((Object) array, "trackInfoData.array()");
                                        return array;
                                    }
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                            }
                            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                        }
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.trackTitle;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.volume;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.appName.hashCode() * 31) + this.volume) * 31) + this.trackTitle.hashCode()) * 31) + this.artistName.hashCode()) * 31) + this.albumName.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.APP_NAME, this.appName), JSONKey.VOLUME, Byte.valueOf(this.volume)), JSONKey.TITLE, this.trackTitle), JSONKey.ARTIST, this.artistName), JSONKey.ALBUM, this.albumName);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.appName);
        }
        if (parcel != null) {
            parcel.writeByte(this.volume);
        }
        if (parcel != null) {
            parcel.writeString(this.trackTitle);
        }
        if (parcel != null) {
            parcel.writeString(this.artistName);
        }
        if (parcel != null) {
            parcel.writeString(this.albumName);
        }
    }

    @DexIgnore
    public TrackInfo(String str, byte b, String str2, String str3, String str4) {
        kd4.b(str, "appName");
        kd4.b(str2, "trackTitle");
        kd4.b(str3, "artistName");
        kd4.b(str4, "albumName");
        this.appName = str;
        this.volume = b;
        this.trackTitle = str2;
        this.artistName = str3;
        this.albumName = str4;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public TrackInfo(Parcel parcel) {
        throw null;
/*        this(r1, r2, r3, r4, r5);
        String readString = parcel.readString();
        if (readString != null) {
            byte readByte = parcel.readByte();
            String readString2 = parcel.readString();
            if (readString2 != null) {
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
*/    }
}
