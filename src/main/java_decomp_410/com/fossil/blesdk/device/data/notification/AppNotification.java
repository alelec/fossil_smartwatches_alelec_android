package com.fossil.blesdk.device.data.notification;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.notification.NotificationFlag;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.y20;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.utils.Crc32Calculator;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;
import java.util.Arrays;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AppNotification extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_MESSAGE_LENGTH_IN_BYTE; // = 249;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_SENDER_LENGTH_IN_BYTE; // = 97;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_TITLE_LENGTH_IN_BYTE; // = 99;
    @DexIgnore
    public /* final */ long appBundleCrc;
    @DexIgnore
    public /* final */ NotificationFlag[] flags;
    @DexIgnore
    public /* final */ String message;
    @DexIgnore
    public /* final */ String sender;
    @DexIgnore
    public /* final */ String title;
    @DexIgnore
    public /* final */ NotificationType type;
    @DexIgnore
    public /* final */ int uid;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<AppNotification> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AppNotification createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new AppNotification(parcel, (fd4) null);
        }

        @DexIgnore
        public AppNotification[] newArray(int i) {
            return new AppNotification[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotification(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) AppNotification.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            AppNotification appNotification = (AppNotification) obj;
            return this.type == appNotification.type && this.uid == appNotification.uid && this.appBundleCrc == appNotification.appBundleCrc && !(kd4.a((Object) this.title, (Object) appNotification.title) ^ true) && !(kd4.a((Object) this.sender, (Object) appNotification.sender) ^ true) && !(kd4.a((Object) this.message, (Object) appNotification.message) ^ true) && Arrays.equals(this.flags, appNotification.flags);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotification");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.appBundleCrc;
    }

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        ByteBuffer allocate = ByteBuffer.allocate(10);
        kd4.a((Object) allocate, "ByteBuffer.allocate(10)");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        byte b = (byte) 10;
        byte id$blesdk_productionRelease = this.type.getId$blesdk_productionRelease();
        byte i = i();
        byte b2 = (byte) 4;
        String a2 = o90.a(this.title);
        Charset f = ua0.y.f();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(f);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            String a3 = o90.a(this.sender);
            Charset f2 = ua0.y.f();
            if (a3 != null) {
                byte[] bytes2 = a3.getBytes(f2);
                kd4.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                String a4 = o90.a(this.message);
                Charset f3 = ua0.y.f();
                if (a4 != null) {
                    byte[] bytes3 = a4.getBytes(f3);
                    kd4.a((Object) bytes3, "(this as java.lang.String).getBytes(charset)");
                    short length = (short) (b + b2 + b2 + bytes.length + bytes2.length + bytes3.length);
                    allocate.putShort(length);
                    allocate.put(b);
                    allocate.put(id$blesdk_productionRelease);
                    allocate.put(i);
                    allocate.put(b2);
                    allocate.put(b2);
                    allocate.put((byte) bytes.length);
                    allocate.put((byte) bytes2.length);
                    allocate.put((byte) bytes3.length);
                    ByteBuffer allocate2 = ByteBuffer.allocate(length);
                    kd4.a((Object) allocate2, "ByteBuffer.allocate(totalLen.toInt())");
                    allocate2.order(ByteOrder.LITTLE_ENDIAN);
                    allocate2.put(allocate.array());
                    ByteBuffer allocate3 = ByteBuffer.allocate(length - b);
                    kd4.a((Object) allocate3, "ByteBuffer.allocate(totalLen - headerLen)");
                    allocate3.order(ByteOrder.LITTLE_ENDIAN);
                    allocate3.putInt(this.uid);
                    allocate3.putInt((int) this.appBundleCrc);
                    allocate3.put(bytes);
                    allocate3.put(bytes2);
                    allocate3.put(bytes3);
                    allocate2.put(allocate3.array());
                    byte[] array = allocate2.array();
                    kd4.a((Object) array, "notificationData.array()");
                    return array;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final NotificationFlag[] getFlags() {
        return this.flags;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public final NotificationType getType() {
        return this.type;
    }

    @DexIgnore
    public final int getUid() {
        return this.uid;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((this.type.hashCode() * 31) + this.uid) * 31) + Long.valueOf(this.appBundleCrc).hashCode()) * 31) + this.title.hashCode()) * 31) + this.sender.hashCode()) * 31) + this.message.hashCode()) * 31) + Arrays.hashCode(this.flags);
    }

    @DexIgnore
    public final byte i() {
        byte b = 0;
        for (NotificationFlag id$blesdk_productionRelease : this.flags) {
            b = (byte) (b | id$blesdk_productionRelease.getId$blesdk_productionRelease());
        }
        return b;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        String str = this.message;
        Charset f = ua0.y.f();
        if (str != null) {
            byte[] bytes = str.getBytes(f);
            kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.TYPE, this.type.getLogName$blesdk_productionRelease()), JSONKey.UID, Integer.valueOf(this.uid)), JSONKey.APP_BUNDLE_CRC, Long.valueOf(this.appBundleCrc)), JSONKey.TITLE, this.title), JSONKey.SENDER, this.sender), JSONKey.MESSAGE_LENGTH, Integer.valueOf(this.message.length())), JSONKey.MESSAGE_CRC, Long.valueOf(Crc32Calculator.a.a(bytes, Crc32Calculator.CrcType.CRC32))), JSONKey.FLAGS, y20.a(this.flags));
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeSerializable(this.type);
        parcel.writeInt(this.uid);
        parcel.writeLong(this.appBundleCrc);
        parcel.writeString(this.title);
        parcel.writeString(this.sender);
        parcel.writeString(this.message);
        parcel.writeStringArray(y20.b(this.flags));
    }

    @DexIgnore
    public AppNotification(NotificationType notificationType, int i, long j, String str, String str2, String str3, NotificationFlag[] notificationFlagArr) {
        throw null;
        // kd4.b(notificationType, "type");
        // kd4.b(str, "title");
        // kd4.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
        // kd4.b(str3, "message");
        // kd4.b(notificationFlagArr, "flags");
        // this.type = notificationType;
        // this.uid = i;
        // this.appBundleCrc = j;
        // this.title = o90.a(str, 99, (Charset) null, (CodingErrorAction) null, 6, (Object) null);
        // this.sender = o90.a(str2, 97, (Charset) null, (CodingErrorAction) null, 6, (Object) null);
        // this.message = o90.a(str3, MAX_MESSAGE_LENGTH_IN_BYTE, (Charset) null, (CodingErrorAction) null, 6, (Object) null);
        // this.flags = notificationFlagArr;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public AppNotification(Parcel parcel) {
        throw null;
/*        this(r2, r3, r4, r6, r7, r8, r9);
        Serializable readSerializable = parcel.readSerializable();
        if (readSerializable != null) {
            NotificationType notificationType = (NotificationType) readSerializable;
            int readInt = parcel.readInt();
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        NotificationFlag.a aVar = NotificationFlag.Companion;
                        String[] createStringArray = parcel.createStringArray();
                        if (createStringArray != null) {
                            NotificationFlag[] a2 = aVar.a(createStringArray);
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationType");
*/    }
}
