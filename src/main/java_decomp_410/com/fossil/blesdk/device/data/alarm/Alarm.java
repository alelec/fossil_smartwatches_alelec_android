package com.fossil.blesdk.device.data.alarm;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j20;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.setting.JSONKey;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class Alarm extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ int LENGTH_DATA_PER_ITEM; // = 3;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_HOUR; // = 23;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_MINUTE; // = 59;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_HOUR; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_MINUTE; // = 0;
    @DexIgnore
    public /* final */ boolean allowSnooze;
    @DexIgnore
    public /* final */ Set<Weekday> daysOfWeek;
    @DexIgnore
    public /* final */ byte hour;
    @DexIgnore
    public /* final */ boolean isEnable;
    @DexIgnore
    public /* final */ boolean isWeeklyRepeated;
    @DexIgnore
    public /* final */ byte minute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<Alarm> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final Alarm a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 3) {
                boolean z = false;
                byte b = (byte) 128;
                byte b2 = (byte) (bArr[0] & b);
                Set<Weekday> a = Weekday.Companion.a(bArr[0]);
                if (((byte) (b & bArr[1])) != 0) {
                    z = true;
                }
                byte b3 = (byte) (bArr[1] & 64);
                byte b4 = (byte) (bArr[1] & 63);
                byte b5 = (byte) (bArr[2] & 31);
                if (z) {
                    return new RepeatedAlarm(b5, b4, a);
                }
                if (a.isEmpty() || a.size() == Weekday.values().length) {
                    return new OneShotAlarm(b5, b4, (Weekday) null);
                }
                return new OneShotAlarm(b5, b4, (Weekday) kb4.d(a).get(0));
            }
            throw new IllegalArgumentException("Size(" + bArr.length + " not equal to 3.");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public Alarm createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            parcel.readByte();
            parcel.readByte();
            parcel.createStringArray();
            boolean z = parcel.readInt() != 0;
            parcel.setDataPosition(0);
            if (z) {
                return RepeatedAlarm.CREATOR.createFromParcel(parcel);
            }
            return OneShotAlarm.CREATOR.createFromParcel(parcel);
        }

        @DexIgnore
        public Alarm[] newArray(int i) {
            return new Alarm[i];
        }
    }

    @DexIgnore
    public Alarm(byte b, byte b2, Set<Weekday> set, boolean z, boolean z2, boolean z3) throws IllegalArgumentException {
        kd4.b(set, "daysOfWeek");
        this.hour = b;
        this.minute = b2;
        this.daysOfWeek = set;
        this.isWeeklyRepeated = z;
        this.allowSnooze = z2;
        this.isEnable = z3;
        i();
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Alarm alarm = (Alarm) obj;
            return this.hour == alarm.hour && this.minute == alarm.minute && j20.a((Set<? extends Weekday>) this.daysOfWeek) == j20.a((Set<? extends Weekday>) alarm.daysOfWeek) && this.isWeeklyRepeated == alarm.isWeeklyRepeated && this.allowSnooze == alarm.allowSnooze && this.isEnable == alarm.isEnable;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.Alarm");
    }

    @DexIgnore
    public final boolean getAllowSnooze$blesdk_productionRelease() {
        return this.allowSnooze;
    }

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        byte[] bArr = new byte[3];
        byte b = this.isWeeklyRepeated ? (byte) 128 : (byte) 0;
        byte b2 = this.allowSnooze ? (byte) 64 : (byte) 0;
        bArr[0] = (byte) ((this.isEnable ? (byte) 128 : (byte) 0) | j20.a((Set<? extends Weekday>) this.daysOfWeek));
        bArr[1] = (byte) (((byte) (b | b2)) | this.minute);
        bArr[2] = this.hour;
        return bArr;
    }

    @DexIgnore
    public final Set<Weekday> getDaysOfWeek$blesdk_productionRelease() {
        return this.daysOfWeek;
    }

    @DexIgnore
    public final byte getHour() {
        return this.hour;
    }

    @DexIgnore
    public final byte getMinute() {
        return this.minute;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((this.hour * 31) + this.minute) * 31) + this.daysOfWeek.hashCode()) * 31) + Boolean.valueOf(this.isWeeklyRepeated).hashCode()) * 31) + Boolean.valueOf(this.allowSnooze).hashCode()) * 31) + Boolean.valueOf(this.isEnable).hashCode();
    }

    @DexIgnore
    public void i() throws IllegalArgumentException {
        byte b = this.hour;
        boolean z = true;
        if (b >= 0 && 23 >= b) {
            byte b2 = this.minute;
            if (b2 < 0 || 59 < b2) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException("minute(" + this.minute + ") is out of range " + "[0, 59].");
            }
            return;
        }
        throw new IllegalArgumentException("hour(" + this.hour + ") is out of range " + "[0, 23].");
    }

    @DexIgnore
    public final boolean isEnable$blesdk_productionRelease() {
        return this.isEnable;
    }

    @DexIgnore
    public final boolean isWeeklyRepeated$blesdk_productionRelease() {
        return this.isWeeklyRepeated;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.HOUR, Byte.valueOf(this.hour)), JSONKey.MINUTE, Byte.valueOf(this.minute)), JSONKey.DAYS, j20.b(this.daysOfWeek)), JSONKey.REPEAT, Boolean.valueOf(this.isWeeklyRepeated)), JSONKey.ENABLE, Boolean.valueOf(this.isEnable)), JSONKey.ALLOW_SNOOZE, Boolean.valueOf(this.allowSnooze));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.hour);
        }
        if (parcel != null) {
            parcel.writeByte(this.minute);
        }
        if (parcel != null) {
            Object[] array = this.daysOfWeek.toArray(new Weekday[0]);
            if (array != null) {
                parcel.writeStringArray(j20.a((Weekday[]) array));
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        if (parcel != null) {
            parcel.writeInt(this.isWeeklyRepeated ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.allowSnooze ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.isEnable ? 1 : 0);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public Alarm(Parcel parcel) {
        throw null;
/*        this(r2, r3, r4, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0);
        kd4.b(parcel, "parcel");
        byte readByte = parcel.readByte();
        byte readByte2 = parcel.readByte();
        Weekday.a aVar = Weekday.Companion;
        String[] createStringArray = parcel.createStringArray();
        if (createStringArray != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet(za4.e(aVar.a(createStringArray)));
            i();
            return;
        }
        kd4.a();
        throw null;
*/    }
}
