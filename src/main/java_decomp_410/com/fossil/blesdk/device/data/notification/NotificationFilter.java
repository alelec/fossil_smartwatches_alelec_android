package com.fossil.blesdk.device.data.notification;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.notification.filter.NotificationIconConfig;
import com.fossil.blesdk.obfuscated.bd4;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.o90;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;
import kotlin.TypeCastException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class NotificationFilter extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ int HEADER_ENTRY_LENGTH; // = 2;
    @DexIgnore
    public static /* final */ byte HEADER_NOTIFICATION_FILTER_LENGTH; // = 2;
    @DexIgnore
    @Keep
    public static /* final */ short MAX_PRIORITY; // = n90.a(bd4.a);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_SENDER_NAME_LENGTH_IN_BYTE; // = 97;
    @DexIgnore
    @Keep
    public static /* final */ short MIN_PRIORITY; // = n90.b(bd4.a);
    @DexIgnore
    public /* final */ long appBundleCrc;
    @DexIgnore
    public /* final */ byte groupId;
    @DexIgnore
    public NotificationHandMovingConfig handMovingConfig;
    @DexIgnore
    public NotificationIconConfig iconConfig;
    @DexIgnore
    public short priority;
    @DexIgnore
    public String sender;
    @DexIgnore
    public NotificationVibePattern vibePattern;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<NotificationFilter> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public NotificationFilter createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new NotificationFilter(parcel, (fd4) null);
        }

        @DexIgnore
        public NotificationFilter[] newArray(int i) {
            return new NotificationFilter[i];
        }
    }

    @DexIgnore
    public /* synthetic */ NotificationFilter(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public final byte[] a(byte b, byte[] bArr) {
        if (bArr.length + 2 > 100) {
            return new byte[0];
        }
        byte[] array = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN).put(b).put((byte) bArr.length).put(bArr).array();
        kd4.a((Object) array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] buildNotificationFilterData$blesdk_productionRelease() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte id$blesdk_productionRelease = NotificationEntryId.APP_BUNDLE_CRC32.getId$blesdk_productionRelease();
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.appBundleCrc).array();
        kd4.a((Object) array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        byteArrayOutputStream.write(a(id$blesdk_productionRelease, array));
        byteArrayOutputStream.write(a(NotificationEntryId.GROUP_ID.getId$blesdk_productionRelease(), new byte[]{this.groupId}));
        if (this.sender.length() > 0) {
            byte id$blesdk_productionRelease2 = NotificationEntryId.SENDER_NAME.getId$blesdk_productionRelease();
            String a2 = o90.a(this.sender);
            Charset f = ua0.y.f();
            if (a2 != null) {
                byte[] bytes = a2.getBytes(f);
                kd4.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                byteArrayOutputStream.write(a(id$blesdk_productionRelease2, bytes));
            } else {
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        if (this.priority != ((short) -1)) {
            byteArrayOutputStream.write(a(NotificationEntryId.PRIORITY.getId$blesdk_productionRelease(), new byte[]{(byte) this.priority}));
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
        if (notificationHandMovingConfig != null) {
            byteArrayOutputStream.write(a(NotificationEntryId.HAND_MOVING.getId$blesdk_productionRelease(), notificationHandMovingConfig.getData$blesdk_productionRelease()));
        }
        NotificationVibePattern notificationVibePattern = this.vibePattern;
        if (notificationVibePattern != null) {
            byteArrayOutputStream.write(a(NotificationEntryId.VIBE.getId$blesdk_productionRelease(), new byte[]{notificationVibePattern.getId$blesdk_productionRelease()}));
        }
        NotificationIconConfig notificationIconConfig = this.iconConfig;
        if (notificationIconConfig != null) {
            byteArrayOutputStream.write(a(NotificationEntryId.ICON_IMAGE.getId$blesdk_productionRelease(), notificationIconConfig.getNotificationFilterIconConfigData$blesdk_productionRelease()));
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byte[] array2 = ByteBuffer.allocate(byteArray.length + 2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) byteArray.length).put(byteArray).array();
        kd4.a((Object) array2, "ByteBuffer\n             \u2026\n                .array()");
        return array2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) NotificationFilter.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationFilter notificationFilter = (NotificationFilter) obj;
            return this.appBundleCrc == notificationFilter.appBundleCrc && this.groupId == notificationFilter.groupId && !(kd4.a((Object) this.sender, (Object) notificationFilter.sender) ^ true) && this.priority == notificationFilter.priority && !(kd4.a((Object) this.handMovingConfig, (Object) notificationFilter.handMovingConfig) ^ true) && this.vibePattern == notificationFilter.vibePattern && !(kd4.a((Object) this.iconConfig, (Object) notificationFilter.iconConfig) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.appBundleCrc;
    }

    @DexIgnore
    public final byte getGroupId() {
        return this.groupId;
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.handMovingConfig;
    }

    @DexIgnore
    public final NotificationIconConfig getIconConfig() {
        return this.iconConfig;
    }

    @DexIgnore
    public final short getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.vibePattern;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((Long.valueOf(this.appBundleCrc).hashCode() * 31) + this.groupId) * 31) + this.sender.hashCode()) * 31) + this.priority;
        NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
        if (notificationHandMovingConfig != null) {
            hashCode = (hashCode * 31) + notificationHandMovingConfig.hashCode();
        }
        NotificationVibePattern notificationVibePattern = this.vibePattern;
        if (notificationVibePattern != null) {
            hashCode = (hashCode * 31) + notificationVibePattern.hashCode();
        }
        NotificationIconConfig notificationIconConfig = this.iconConfig;
        return notificationIconConfig != null ? (hashCode * 31) + notificationIconConfig.hashCode() : hashCode;
    }

    @DexIgnore
    public final NotificationFilter setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        kd4.b(notificationHandMovingConfig, "handMovingConfig");
        this.handMovingConfig = notificationHandMovingConfig;
        return this;
    }

    @DexIgnore
    public final NotificationFilter setIconConfig(NotificationIconConfig notificationIconConfig) {
        kd4.b(notificationIconConfig, "iconConfig");
        this.iconConfig = notificationIconConfig;
        return this;
    }

    @DexIgnore
    /* renamed from: setPriority  reason: collision with other method in class */
    public final void m0setPriority(short s) {
        short b = n90.b(bd4.a);
        short a2 = n90.a(bd4.a);
        if (b > s || a2 < s) {
            s = -1;
        }
        this.priority = s;
    }

    @DexIgnore
    /* renamed from: setSender  reason: collision with other method in class */
    public final void m1setSender(String str) {
        throw null;
        // this.sender = o90.a(str, 97, (Charset) null, (CodingErrorAction) null, 6, (Object) null);
    }

    @DexIgnore
    public final NotificationFilter setVibePatternConfig(NotificationVibePattern notificationVibePattern) {
        kd4.b(notificationVibePattern, "vibePattern");
        this.vibePattern = notificationVibePattern;
        return this;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        try {
            wa0.a(jSONObject, JSONKey.APP_BUNDLE_CRC, Long.valueOf(this.appBundleCrc));
            wa0.a(jSONObject, JSONKey.GROUP_ID, Byte.valueOf(this.groupId));
            wa0.a(jSONObject, JSONKey.SENDER, this.sender);
            wa0.a(jSONObject, JSONKey.PRIORITY, Short.valueOf(this.priority));
            JSONKey jSONKey = JSONKey.HAND_MOVING_CONFIG;
            NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
            JSONObject jSONObject2 = null;
            wa0.a(jSONObject, jSONKey, notificationHandMovingConfig != null ? notificationHandMovingConfig.toJSONObject() : null);
            JSONKey jSONKey2 = JSONKey.VIBE_PATTERN;
            NotificationVibePattern notificationVibePattern = this.vibePattern;
            wa0.a(jSONObject, jSONKey2, notificationVibePattern != null ? Byte.valueOf(notificationVibePattern.getId$blesdk_productionRelease()) : null);
            JSONKey jSONKey3 = JSONKey.ICON_CONFIG;
            NotificationIconConfig notificationIconConfig = this.iconConfig;
            if (notificationIconConfig != null) {
                jSONObject2 = notificationIconConfig.toJSONObject();
            }
            wa0.a(jSONObject, jSONKey3, jSONObject2);
        } catch (JSONException e) {
            da0.l.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.appBundleCrc);
        }
        if (parcel != null) {
            parcel.writeByte(this.groupId);
        }
        if (parcel != null) {
            parcel.writeString(this.sender);
        }
        if (parcel != null) {
            parcel.writeInt(this.priority);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.handMovingConfig, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.vibePattern);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.iconConfig, i);
        }
    }

    @DexIgnore
    public NotificationFilter(long j, byte b) {
        this.appBundleCrc = j;
        this.groupId = b;
        this.sender = "";
        this.priority = 255;
    }

    @DexIgnore
    public final NotificationFilter setPriority(short s) {
        setPriority(s);
        return this;
    }

    @DexIgnore
    public final NotificationFilter setSender(String str) {
        kd4.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
        setSender(str);
        return this;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public NotificationFilter(long j, byte b, NotificationHandMovingConfig notificationHandMovingConfig, NotificationVibePattern notificationVibePattern) {
        this(j, b);
        kd4.b(notificationHandMovingConfig, "handMovingConfig");
        kd4.b(notificationVibePattern, "vibePattern");
        this.handMovingConfig = notificationHandMovingConfig;
        this.vibePattern = notificationVibePattern;
    }

    @DexIgnore
    public NotificationFilter(Parcel parcel) {
        this(parcel.readLong(), parcel.readByte());
        String readString = parcel.readString();
        setSender(readString == null ? "" : readString);
        setPriority((short) parcel.readInt());
        this.handMovingConfig = (NotificationHandMovingConfig) parcel.readParcelable(NotificationHandMovingConfig.class.getClassLoader());
        this.vibePattern = (NotificationVibePattern) parcel.readSerializable();
        this.iconConfig = (NotificationIconConfig) parcel.readParcelable(NotificationIconConfig.class.getClassLoader());
    }
}
