package com.fossil.blesdk.device.data.config;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum DeviceConfigKey {
    BIOMETRIC_PROFILE(1, "biometric_profile"),
    DAILY_STEP(2, "daily_step"),
    DAILY_STEP_GOAL(3, "daily_step_goal"),
    DAILY_CALORIE(4, "daily_calories"),
    DAILY_CALORIE_GOAL(5, "daily_calories_goal"),
    DAILY_TOTAL_ACTIVE_MINUTE(6, "daily_active_minutes"),
    DAILY_ACTIVE_MINUTE_GOAL(7, "daily_active_minute_goal"),
    DAILY_DISTANCE(8, "daily_distance"),
    INACTIVE_NUDGE(9, "inactive_nudge"),
    VIBE_STRENGTH(10, "vibe_strength_level"),
    DO_NOT_DISTURB_SCHEDULE(11, "dnd_schedule"),
    TIME(12, LogBuilder.KEY_TIME),
    BATTERY(13, Constants.BATTERY),
    HEART_RATE_MODE(14, "heart_rate_mode"),
    DAILY_SLEEP(15, "daily_sleep"),
    DISPLAY_UNIT(16, "display_unit"),
    SECOND_TIMEZONE_OFFSET(17, "second_timezone_offset"),
    CURRENT_HEART_RATE(18, "current_heart_rate");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public static /* final */ int ENTRY_ID_LENGTH; // = 2;
    @DexIgnore
    public /* final */ short entryId;
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    DeviceConfigKey(int i, String daily_calories_goal) {
        throw null;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DeviceConfigKey a(short s) {
            for (DeviceConfigKey deviceConfigKey : DeviceConfigKey.values()) {
                if (deviceConfigKey.getEntryId$blesdk_productionRelease() == s) {
                    return deviceConfigKey;
                }
            }
            return null;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final DeviceConfigKey a(String str) {
            kd4.b(str, "logName");
            for (DeviceConfigKey deviceConfigKey : DeviceConfigKey.values()) {
                if (kd4.a((Object) str, (Object) deviceConfigKey.getLogName$blesdk_productionRelease())) {
                    return deviceConfigKey;
                }
            }
            return null;
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    DeviceConfigKey(short s, String str) {
        this.entryId = s;
        this.logName = str;
    }

    @DexIgnore
    public final short getEntryId$blesdk_productionRelease() {
        return this.entryId;
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
