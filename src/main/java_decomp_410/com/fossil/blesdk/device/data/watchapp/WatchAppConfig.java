package com.fossil.blesdk.device.data.watchapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.preset.DevicePresetItem;
import com.fossil.blesdk.model.watchapp.config.ButtonEvent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.hb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.m90;
import java.util.HashSet;
import java.util.Iterator;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WatchAppConfig extends DevicePresetItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public static /* final */ ButtonEvent e; // = ButtonEvent.TOP_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ ButtonEvent f; // = ButtonEvent.MIDDLE_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ ButtonEvent g; // = ButtonEvent.BOTTOM_SHORT_PRESS_RELEASE;
    @DexIgnore
    public /* final */ HashSet<WatchApp> watchAppAssignment;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WatchAppConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WatchAppConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WatchAppConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public WatchAppConfig[] newArray(int i) {
            return new WatchAppConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WatchAppConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void bottomButton$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void middleButton$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void topButton$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) WatchAppConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.watchAppAssignment, (Object) ((WatchAppConfig) obj).watchAppAssignment) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchAppConfig");
    }

    @DexIgnore
    public JSONObject getAssignmentJSON$blesdk_productionRelease() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (WatchApp watchApp : this.watchAppAssignment) {
            jSONArray.put(watchApp.toJSONObject());
            m90.a(jSONObject, watchApp.getDataConfigJSONObject$blesdk_productionRelease(), false, 2, (Object) null);
        }
        jSONObject.put("master._.config.buttons", jSONArray);
        return jSONObject;
    }

    @DexIgnore
    public final WatchApp getBottomButton() {
        T t;
        boolean z;
        Iterator<T> it = this.watchAppAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((WatchApp) t).getButtonEvent() == g) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (WatchApp) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final WatchApp getMiddleButton() {
        T t;
        boolean z;
        Iterator<T> it = this.watchAppAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((WatchApp) t).getButtonEvent() == f) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (WatchApp) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final WatchApp getTopButton() {
        T t;
        boolean z;
        Iterator<T> it = this.watchAppAssignment.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (((WatchApp) t).getButtonEvent() == e) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        if (t != null) {
            return (WatchApp) t;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.watchAppAssignment.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() {
        return getAssignmentJSON$blesdk_productionRelease();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.watchAppAssignment.toArray(new WatchApp[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public WatchAppConfig(WatchApp[] watchAppArr) {
        kd4.b(watchAppArr, "watchAppAssignment");
        this.watchAppAssignment = new HashSet<>();
        hb4.a(this.watchAppAssignment, (T[]) watchAppArr);
    }

    @DexIgnore
    public WatchAppConfig(WatchApp watchApp, WatchApp watchApp2, WatchApp watchApp3) {
        kd4.b(watchApp, "topButton");
        kd4.b(watchApp2, "middleButton");
        kd4.b(watchApp3, "bottomButton");
        this.watchAppAssignment = new HashSet<>();
        watchApp.setButtonEvent$blesdk_productionRelease(e);
        watchApp2.setButtonEvent$blesdk_productionRelease(f);
        watchApp3.setButtonEvent$blesdk_productionRelease(g);
        this.watchAppAssignment.add(watchApp);
        this.watchAppAssignment.add(watchApp2);
        this.watchAppAssignment.add(watchApp3);
    }

    @DexIgnore
    public WatchAppConfig(Parcel parcel) {
        super(parcel);
        this.watchAppAssignment = new HashSet<>();
        HashSet<WatchApp> hashSet = this.watchAppAssignment;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(WatchApp.class.getClassLoader());
        if (readParcelableArray != null) {
            hb4.a(hashSet, (T[]) (WatchApp[]) readParcelableArray);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchApp>");
    }
}
