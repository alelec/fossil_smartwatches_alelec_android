package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.od4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailyActiveMinuteGoalConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_MINUTE; // = 0;
    @DexIgnore
    public static /* final */ int e; // = n90.a(od4.a);
    @DexIgnore
    public /* final */ int minute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DailyActiveMinuteGoalConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DailyActiveMinuteGoalConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 2) {
                return new DailyActiveMinuteGoalConfig(n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 2");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DailyActiveMinuteGoalConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DailyActiveMinuteGoalConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public DailyActiveMinuteGoalConfig[] newArray(int i) {
            return new DailyActiveMinuteGoalConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DailyActiveMinuteGoalConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DailyActiveMinuteGoalConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.minute == ((DailyActiveMinuteGoalConfig) obj).minute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.minute).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public int hashCode() {
        return this.minute;
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        int i = e;
        int i2 = this.minute;
        if (!(i2 >= 0 && i >= i2)) {
            throw new IllegalArgumentException("minute(" + this.minute + ") is out of range " + "[0, " + e + "].");
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.minute);
        }
    }

    @DexIgnore
    public DailyActiveMinuteGoalConfig(int i) throws IllegalArgumentException {
        super(DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL);
        this.minute = i;
        j();
    }

    @DexIgnore
    public Integer valueDescription() {
        return Integer.valueOf(this.minute);
    }

    @DexIgnore
    public DailyActiveMinuteGoalConfig(Parcel parcel) {
        super(parcel);
        this.minute = parcel.readInt();
        j();
    }
}
