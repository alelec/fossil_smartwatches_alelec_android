package com.fossil.blesdk.device.data.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.enumerate.WeatherCondition;
import com.fossil.blesdk.device.data.enumerate.Weekday;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class WeatherDayForecast extends JSONAbleObject implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ float highTemperature;
    @DexIgnore
    public /* final */ float lowTemperature;
    @DexIgnore
    public /* final */ WeatherCondition weatherCondition;
    @DexIgnore
    public /* final */ Weekday weekday;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<WeatherDayForecast> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public WeatherDayForecast createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new WeatherDayForecast(parcel, (fd4) null);
        }

        @DexIgnore
        public WeatherDayForecast[] newArray(int i) {
            return new WeatherDayForecast[i];
        }
    }

    @DexIgnore
    public /* synthetic */ WeatherDayForecast(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void highTemperature$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void lowTemperature$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void weatherCondition$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) WeatherDayForecast.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            WeatherDayForecast weatherDayForecast = (WeatherDayForecast) obj;
            return this.weekday == weatherDayForecast.weekday && this.highTemperature == weatherDayForecast.highTemperature && this.lowTemperature == weatherDayForecast.lowTemperature && this.weatherCondition == weatherDayForecast.weatherCondition;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherDayForecast");
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.highTemperature;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.lowTemperature;
    }

    @DexIgnore
    public final JSONObject getSettingJSONData$blesdk_productionRelease() throws JSONException {
        JSONObject put = new JSONObject().put("day", this.weekday.getUiScriptName$blesdk_productionRelease()).put("high", Float.valueOf(this.highTemperature)).put("low", Float.valueOf(this.lowTemperature)).put("cond_id", this.weatherCondition.getId$blesdk_productionRelease());
        kd4.a((Object) put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public final WeatherCondition getWeatherCondition() {
        return this.weatherCondition;
    }

    @DexIgnore
    public final Weekday getWeekday() {
        return this.weekday;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.weekday.hashCode() * 31) + Float.valueOf(this.highTemperature).hashCode()) * 31) + Float.valueOf(this.lowTemperature).hashCode()) * 31) + this.weatherCondition.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.WEEKDAY, this.weekday.getLogName$blesdk_productionRelease()), JSONKey.HIGH_TEMPERATURE, Float.valueOf(this.highTemperature)), JSONKey.LOW_TEMPERATURE, Float.valueOf(this.lowTemperature)), JSONKey.WEATHER_CONDITION, this.weatherCondition.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.weekday.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.highTemperature);
        }
        if (parcel != null) {
            parcel.writeFloat(this.lowTemperature);
        }
        if (parcel != null) {
            parcel.writeString(this.weatherCondition.name());
        }
    }

    @DexIgnore
    public WeatherDayForecast(Weekday weekday2, float f, float f2, WeatherCondition weatherCondition2) {
        kd4.b(weekday2, "weekday");
        kd4.b(weatherCondition2, "weatherCondition");
        this.weekday = weekday2;
        this.highTemperature = n90.a(f, 2);
        this.lowTemperature = n90.a(f2, 2);
        this.weatherCondition = weatherCondition2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public WeatherDayForecast(Parcel parcel) {
        throw null;
/*        this(r0, r2, r3, WeatherCondition.valueOf(r5));
        String readString = parcel.readString();
        if (readString != null) {
            Weekday valueOf = Weekday.valueOf(readString);
            float readFloat = parcel.readFloat();
            float readFloat2 = parcel.readFloat();
            String readString2 = parcel.readString();
            if (readString2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
