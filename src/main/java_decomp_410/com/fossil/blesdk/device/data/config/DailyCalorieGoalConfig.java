package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.jd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DailyCalorieGoalConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MINIMUM_CALORIE; // = 0;
    @DexIgnore
    public static /* final */ long e; // = n90.a(jd4.a);
    @DexIgnore
    public /* final */ long calorie;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DailyCalorieGoalConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final DailyCalorieGoalConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 4) {
                return new DailyCalorieGoalConfig(n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0)));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 4");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DailyCalorieGoalConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new DailyCalorieGoalConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public DailyCalorieGoalConfig[] newArray(int i) {
            return new DailyCalorieGoalConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ DailyCalorieGoalConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DailyCalorieGoalConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.calorie == ((DailyCalorieGoalConfig) obj).calorie;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig");
    }

    @DexIgnore
    public final long getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.calorie).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return Long.valueOf(this.calorie).hashCode();
    }

    @DexIgnore
    public final void j() throws IllegalArgumentException {
        long j = e;
        long j2 = this.calorie;
        if (!(0 <= j2 && j >= j2)) {
            throw new IllegalArgumentException("calorie(" + this.calorie + ") is out of range " + "[0, " + e + "].");
        }
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.calorie);
        }
    }

    @DexIgnore
    public DailyCalorieGoalConfig(long j) throws IllegalArgumentException {
        super(DeviceConfigKey.DAILY_CALORIE_GOAL);
        this.calorie = j;
        j();
    }

    @DexIgnore
    public Long valueDescription() {
        return Long.valueOf(this.calorie);
    }

    @DexIgnore
    public DailyCalorieGoalConfig(Parcel parcel) {
        super(parcel);
        this.calorie = parcel.readLong();
        j();
    }
}
