package com.fossil.blesdk.device.data.config;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class VibeStrengthConfig extends DeviceConfigItem {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ VibeStrengthLevel vibeStrengthLevel;

    @DexIgnore
    public enum VibeStrengthLevel {
        OFF((byte) 0),
        LOW((byte) 50),
        MEDIUM((byte) 75),
        HIGH((byte) 100);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;
        @DexIgnore
        public /* final */ byte value;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final VibeStrengthLevel a(byte b) {
                boolean z = false;
                if (true == (b <= 25)) {
                    return VibeStrengthLevel.OFF;
                }
                if (true == (b <= 50)) {
                    return VibeStrengthLevel.LOW;
                }
                if (b <= 75) {
                    z = true;
                }
                if (true == z) {
                    return VibeStrengthLevel.MEDIUM;
                }
                return VibeStrengthLevel.HIGH;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        VibeStrengthLevel(byte b) {
            this.value = b;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }

        @DexIgnore
        public final byte getValue$blesdk_productionRelease() {
            return this.value;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<VibeStrengthConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final VibeStrengthConfig a(byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "rawData");
            if (bArr.length == 1) {
                return new VibeStrengthConfig(VibeStrengthLevel.Companion.a(bArr[0]));
            }
            throw new IllegalArgumentException("Invalid data size: " + bArr.length + ", require: 1");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public VibeStrengthConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new VibeStrengthConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public VibeStrengthConfig[] newArray(int i) {
            return new VibeStrengthConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ VibeStrengthConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) VibeStrengthConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.vibeStrengthLevel == ((VibeStrengthConfig) obj).vibeStrengthLevel;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
    }

    @DexIgnore
    public byte[] getDataContent() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.vibeStrengthLevel.getValue$blesdk_productionRelease()).array();
        kd4.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final VibeStrengthLevel getVibeStrengthLevel() {
        return this.vibeStrengthLevel;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.vibeStrengthLevel.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.vibeStrengthLevel.name());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VibeStrengthConfig(VibeStrengthLevel vibeStrengthLevel2) {
        super(DeviceConfigKey.VIBE_STRENGTH);
        kd4.b(vibeStrengthLevel2, "vibeStrengthLevel");
        this.vibeStrengthLevel = vibeStrengthLevel2;
    }

    @DexIgnore
    public String valueDescription() {
        return this.vibeStrengthLevel.getLogName$blesdk_productionRelease();
    }

    @DexIgnore
    public VibeStrengthConfig(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.vibeStrengthLevel = VibeStrengthLevel.valueOf(readString);
        } else {
            kd4.a();
            throw null;
        }
    }
}
