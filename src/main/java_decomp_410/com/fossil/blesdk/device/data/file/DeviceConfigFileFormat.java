package com.fossil.blesdk.device.data.file;

import com.fossil.blesdk.device.data.config.BatteryConfig;
import com.fossil.blesdk.device.data.config.BiometricProfile;
import com.fossil.blesdk.device.data.config.CurrentHeartRateConfig;
import com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig;
import com.fossil.blesdk.device.data.config.DailyCalorieConfig;
import com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig;
import com.fossil.blesdk.device.data.config.DailyDistanceConfig;
import com.fossil.blesdk.device.data.config.DailySleepConfig;
import com.fossil.blesdk.device.data.config.DailyStepConfig;
import com.fossil.blesdk.device.data.config.DailyStepGoalConfig;
import com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.config.DisplayUnitConfig;
import com.fossil.blesdk.device.data.config.DoNotDisturbScheduleConfig;
import com.fossil.blesdk.device.data.config.HeartRateModeConfig;
import com.fossil.blesdk.device.data.config.InactiveNudgeConfig;
import com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig;
import com.fossil.blesdk.device.data.config.TimeConfig;
import com.fossil.blesdk.device.data.config.VibeStrengthConfig;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.k20;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.l20;
import com.fossil.blesdk.obfuscated.q20;
import com.fossil.blesdk.obfuscated.r20;
import com.fossil.blesdk.obfuscated.s20;
import com.fossil.blesdk.obfuscated.t20;
import com.fossil.blesdk.obfuscated.u20;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.ya4;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceConfigFileFormat extends s20<DeviceConfigItem[], HashMap<DeviceConfigKey, DeviceConfigItem>> {
    @DexIgnore
    public static /* final */ a[] a; // = {new a(DeviceConfigKey.BIOMETRIC_PROFILE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon1(BiometricProfile.CREATOR)), new a(DeviceConfigKey.DAILY_STEP, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon2(DailyStepConfig.CREATOR)), new a(DeviceConfigKey.DAILY_STEP_GOAL, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon3(DailyStepGoalConfig.CREATOR)), new a(DeviceConfigKey.DAILY_CALORIE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon4(DailyCalorieConfig.CREATOR)), new a(DeviceConfigKey.DAILY_CALORIE_GOAL, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon5(DailyCalorieGoalConfig.CREATOR)), new a(DeviceConfigKey.DAILY_TOTAL_ACTIVE_MINUTE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon6(DailyTotalActiveMinuteConfig.CREATOR)), new a(DeviceConfigKey.DAILY_ACTIVE_MINUTE_GOAL, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon7(DailyActiveMinuteGoalConfig.CREATOR)), new a(DeviceConfigKey.DAILY_DISTANCE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon8(DailyDistanceConfig.CREATOR)), new a(DeviceConfigKey.INACTIVE_NUDGE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon9(InactiveNudgeConfig.CREATOR)), new a(DeviceConfigKey.VIBE_STRENGTH, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon10(VibeStrengthConfig.CREATOR)), new a(DeviceConfigKey.DO_NOT_DISTURB_SCHEDULE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon11(DoNotDisturbScheduleConfig.CREATOR)), new a(DeviceConfigKey.TIME, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon12(TimeConfig.CREATOR)), new a(DeviceConfigKey.BATTERY, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon13(BatteryConfig.CREATOR)), new a(DeviceConfigKey.HEART_RATE_MODE, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon14(HeartRateModeConfig.CREATOR)), new a(DeviceConfigKey.DAILY_SLEEP, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon15(DailySleepConfig.CREATOR)), new a(DeviceConfigKey.DISPLAY_UNIT, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon16(DisplayUnitConfig.CREATOR)), new a(DeviceConfigKey.SECOND_TIMEZONE_OFFSET, new DeviceConfigFileFormat$VERSION_1_0_SUPPORTED_CONFIG$Anon17(SecondTimezoneOffsetConfig.CREATOR))};
    @DexIgnore
    public static /* final */ a[] b; // = ((a[]) ya4.a((T[]) a, new a(DeviceConfigKey.CURRENT_HEART_RATE, new DeviceConfigFileFormat$VERSION_2_0_SUPPORTED_CONFIG$Anon1(CurrentHeartRateConfig.CREATOR))));
    @DexIgnore
    public static /* final */ k20<DeviceConfigItem[]>[] c; // = {new b(), new c()};
    @DexIgnore
    public static /* final */ l20<HashMap<DeviceConfigKey, DeviceConfigItem>>[] d; // = {new d(FileType.DEVICE_CONFIG), new e(FileType.DEVICE_CONFIG)};
    @DexIgnore
    public static /* final */ DeviceConfigFileFormat e; // = new DeviceConfigFileFormat();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ DeviceConfigKey a;
        @DexIgnore
        public /* final */ xc4<byte[], DeviceConfigItem> b;

        @DexIgnore
        public a(DeviceConfigKey deviceConfigKey, xc4<? super byte[], ? extends DeviceConfigItem> xc4) {
            throw null;
            // kd4.b(deviceConfigKey, "key");
            // kd4.b(xc4, "transformer");
            // this.a = deviceConfigKey;
            // this.b = xc4;
        }

        @DexIgnore
        public final DeviceConfigKey a() {
            return this.a;
        }

        @DexIgnore
        public final xc4<byte[], DeviceConfigItem> b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends q20<DeviceConfigItem[]> {
        @DexIgnore
        public byte[] a(DeviceConfigItem[] deviceConfigItemArr) {
            kd4.b(deviceConfigItemArr, "entries");
            return DeviceConfigFileFormat.e.a(deviceConfigItemArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends r20<DeviceConfigItem[]> {
        @DexIgnore
        public byte[] a(DeviceConfigItem[] deviceConfigItemArr) {
            kd4.b(deviceConfigItemArr, "entries");
            return DeviceConfigFileFormat.e.a(deviceConfigItemArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends t20<HashMap<DeviceConfigKey, DeviceConfigItem>> {
        @DexIgnore
        public d(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public HashMap<DeviceConfigKey, DeviceConfigItem> b(byte[] bArr) {
            kd4.b(bArr, "data");
            DeviceConfigFileFormat deviceConfigFileFormat = DeviceConfigFileFormat.e;
            return deviceConfigFileFormat.a(bArr, DeviceConfigFileFormat.a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends u20<HashMap<DeviceConfigKey, DeviceConfigItem>> {
        @DexIgnore
        public e(FileType fileType) {
            super(fileType);
        }

        @DexIgnore
        public HashMap<DeviceConfigKey, DeviceConfigItem> b(byte[] bArr) {
            kd4.b(bArr, "data");
            DeviceConfigFileFormat deviceConfigFileFormat = DeviceConfigFileFormat.e;
            return deviceConfigFileFormat.a(bArr, DeviceConfigFileFormat.b);
        }
    }

    @DexIgnore
    public l20<HashMap<DeviceConfigKey, DeviceConfigItem>>[] b() {
        return d;
    }

    @DexIgnore
    public k20<DeviceConfigItem[]>[] a() {
        return c;
    }

    @DexIgnore
    public final byte[] a(DeviceConfigItem[] deviceConfigItemArr) {
        throw null;
        // ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // for (DeviceConfigItem dataConfig$blesdk_productionRelease : deviceConfigItemArr) {
        //     byteArrayOutputStream.write(dataConfig$blesdk_productionRelease.getDataConfig$blesdk_productionRelease());
        // }
        // byte[] byteArray = byteArrayOutputStream.toByteArray();
        // kd4.a((Object) byteArray, "entryData.toByteArray()");
        // return byteArray;
    }

    @DexIgnore
    public final HashMap<DeviceConfigKey, DeviceConfigItem> a(byte[] bArr, a[] aVarArr) {
        DeviceConfigItem deviceConfigItem;
        a aVar;
        int length = (bArr.length - 12) - 4;
        byte[] a2 = ya4.a(bArr, 12, length + 12);
        ByteBuffer order = ByteBuffer.wrap(a2).order(ByteOrder.LITTLE_ENDIAN);
        HashMap<DeviceConfigKey, DeviceConfigItem> hashMap = new HashMap<>();
        int i = 0;
        while (true) {
            int i2 = i + 3;
            if (i2 > length) {
                return hashMap;
            }
            short s = order.getShort(i);
            byte b2 = order.get(i + 2);
            DeviceConfigKey a3 = DeviceConfigKey.Companion.a(s);
            try {
                byte[] a4 = ya4.a(a2, i2, i2 + b2);
                int length2 = aVarArr.length;
                int i3 = 0;
                while (true) {
                    deviceConfigItem = null;
                    if (i3 >= length2) {
                        aVar = null;
                        break;
                    }
                    aVar = aVarArr[i3];
                    if (aVar.a() == a3) {
                        break;
                    }
                    i3++;
                }
                if (aVar != null) {
                    deviceConfigItem = aVar.b().invoke(a4);
                }
                if (!(a3 == null || deviceConfigItem == null)) {
                    hashMap.put(a3, deviceConfigItem);
                }
            } catch (Exception e2) {
                da0.l.a(e2);
            }
            i += b2 + 3;
        }
    }
}
