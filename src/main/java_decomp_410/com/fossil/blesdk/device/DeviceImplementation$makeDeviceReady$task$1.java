package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$makeDeviceReady$task$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase.Result, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ kotlin.jvm.internal.Ref$ObjectRef $phaseController;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$makeDeviceReady$task$1(kotlin.jvm.internal.Ref$ObjectRef ref$ObjectRef) {
        super(1);
        this.$phaseController = ref$ObjectRef;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase.Result) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase.Result result) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(result, "it");
        com.fossil.blesdk.device.logic.phase.Phase phase = (com.fossil.blesdk.device.logic.phase.Phase) this.$phaseController.element;
        if (phase != null) {
            phase.mo7544a(com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.INTERRUPTED);
        }
    }
}
