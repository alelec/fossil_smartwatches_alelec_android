package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.backgroundsync.BackgroundSyncFrame;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.ee4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BackgroundSyncEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ int BACKGROUND_SYNC_HEADER_LENGTH; // = 3;
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ BackgroundSyncFrame[] backgroundSyncFrames;
    @DexIgnore
    public /* final */ byte[] eventData;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BackgroundSyncEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x005e A[LOOP:1: B:11:0x004d->B:13:0x005e, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0060 A[EDGE_INSN: B:30:0x0060->B:14:0x0060 ?: BREAK  , SYNTHETIC] */
        public final BackgroundSyncEvent a(byte b, byte[] bArr) throws IllegalArgumentException {
            kd4.b(bArr, "eventData");
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (i < bArr.length) {
                short b2 = n90.b(order.get(i));
                int i2 = i + 1;
                int i3 = i2 + b2;
                if (i3 <= bArr.length) {
                    byte[] a = ya4.a(bArr, i2, i3);
                    if (a.length % 3 == 0) {
                        wd4 a2 = ee4.a((wd4) ee4.d(0, a.length), 3);
                        int a3 = a2.a();
                        int b3 = a2.b();
                        int c = a2.c();
                        if (c >= 0) {
                            if (a3 > b3) {
                            }
                            while (true) {
                                arrayList.add(BackgroundSyncFrame.CREATOR.a(ya4.a(a, a3, a3 + 3)));
                                if (a3 != b3) {
                                    break;
                                }
                                a3 += c;
                            }
                        } else {
                            if (a3 < b3) {
                            }
                            while (true) {
                                arrayList.add(BackgroundSyncFrame.CREATOR.a(ya4.a(a, a3, a3 + 3)));
                                if (a3 != b3) {
                                }
                                a3 += c;
                            }
                        }
                        i += b2 + 1;
                    } else {
                        throw new IllegalArgumentException("Chain size (" + a.length + ") is not " + "divide to 3.");
                    }
                } else {
                    throw new IllegalArgumentException("Invalid Chain length " + '(' + i3 + "), " + "remain " + bArr.length + '.');
                }
            }
            Object[] array = arrayList.toArray(new BackgroundSyncFrame[0]);
            if (array != null) {
                return new BackgroundSyncEvent(b, (BackgroundSyncFrame[]) array, bArr);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BackgroundSyncEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BackgroundSyncEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public BackgroundSyncEvent[] newArray(int i) {
            return new BackgroundSyncEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ BackgroundSyncEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) BackgroundSyncEvent.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            BackgroundSyncEvent backgroundSyncEvent = (BackgroundSyncEvent) obj;
            return getEventSequence$blesdk_productionRelease() == backgroundSyncEvent.getEventSequence$blesdk_productionRelease() && Arrays.equals(this.backgroundSyncFrames, backgroundSyncEvent.backgroundSyncFrames) && Arrays.equals(this.eventData, backgroundSyncEvent.eventData);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent");
    }

    @DexIgnore
    public final BackgroundSyncFrame[] getBackgroundSyncFrames() {
        return this.backgroundSyncFrames;
    }

    @DexIgnore
    public final byte[] getEventData() {
        return this.eventData;
    }

    @DexIgnore
    public byte[] getEventResponseData() {
        return this.eventData;
    }

    @DexIgnore
    public int hashCode() {
        return (((getEventSequence$blesdk_productionRelease() * 31) + this.backgroundSyncFrames.hashCode()) * 31) + Arrays.hashCode(this.eventData);
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (BackgroundSyncFrame jSONObject : this.backgroundSyncFrames) {
            jSONArray.put(jSONObject.toJSONObject());
        }
        return wa0.a(wa0.a(super.toJSONObject(), JSONKey.FRAMES, jSONArray), JSONKey.TOTAL_FRAMES, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(getEventSequence$blesdk_productionRelease());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.backgroundSyncFrames, i);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.eventData);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BackgroundSyncEvent(byte b, BackgroundSyncFrame[] backgroundSyncFrameArr, byte[] bArr) {
        super(AsyncEventType.BACKGROUND_SYNC_EVENT, b);
        kd4.b(backgroundSyncFrameArr, "backgroundSyncFrames");
        kd4.b(bArr, "eventData");
        this.backgroundSyncFrames = backgroundSyncFrameArr;
        this.eventData = bArr;
    }

    @DexIgnore
    public BackgroundSyncEvent(Parcel parcel) {
        super(parcel);
        Object[] createTypedArray = parcel.createTypedArray(BackgroundSyncFrame.CREATOR);
        if (createTypedArray != null) {
            this.backgroundSyncFrames = (BackgroundSyncFrame[]) createTypedArray;
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                this.eventData = createByteArray;
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
