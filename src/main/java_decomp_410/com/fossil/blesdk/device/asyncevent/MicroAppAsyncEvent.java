package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.model.microapp.response.MicroAppEvent;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class MicroAppAsyncEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ MicroAppEvent microAppEvent;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<MicroAppAsyncEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final MicroAppAsyncEvent a(byte b, byte[] bArr) {
            kd4.b(bArr, "eventData");
            return new MicroAppAsyncEvent(b, new MicroAppEvent(bArr));
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public MicroAppAsyncEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new MicroAppAsyncEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public MicroAppAsyncEvent[] newArray(int i) {
            return new MicroAppAsyncEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ MicroAppAsyncEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) MicroAppAsyncEvent.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            MicroAppAsyncEvent microAppAsyncEvent = (MicroAppAsyncEvent) obj;
            return getEventType$blesdk_productionRelease() == microAppAsyncEvent.getEventType$blesdk_productionRelease() && getEventSequence$blesdk_productionRelease() == microAppAsyncEvent.getEventSequence$blesdk_productionRelease() && !(kd4.a((Object) this.microAppEvent, (Object) microAppAsyncEvent.microAppEvent) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.MicroAppAsyncEvent");
    }

    @DexIgnore
    public final MicroAppEvent getMicroAppEvent$blesdk_productionRelease() {
        return this.microAppEvent;
    }

    @DexIgnore
    public int hashCode() {
        return (((getEventType$blesdk_productionRelease().hashCode() * 31) + getEventSequence$blesdk_productionRelease()) * 31) + this.microAppEvent.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(super.toJSONObject(), JSONKey.MICRO_APP_EVENT, this.microAppEvent.toJSONObject());
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.microAppEvent, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppAsyncEvent(byte b, MicroAppEvent microAppEvent2) {
        super(AsyncEventType.MICRO_APP_EVENT, b);
        kd4.b(microAppEvent2, "microAppEvent");
        this.microAppEvent = microAppEvent2;
    }

    @DexIgnore
    public MicroAppAsyncEvent(Parcel parcel) {
        super(parcel);
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray != null) {
            this.microAppEvent = new MicroAppEvent(createByteArray);
        } else {
            kd4.a();
            throw null;
        }
    }
}
