package com.fossil.blesdk.device.asyncevent;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.heartbeat.HeartbeatAppId;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HeartbeatEvent extends AsyncEvent {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<HeartbeatEvent> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public HeartbeatEvent createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new HeartbeatEvent(parcel, (fd4) null);
        }

        @DexIgnore
        public HeartbeatEvent[] newArray(int i) {
            return new HeartbeatEvent[i];
        }
    }

    @DexIgnore
    public /* synthetic */ HeartbeatEvent(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public byte[] getEventResponseData() {
        return new byte[]{HeartbeatAppId.FOSSIL.getId$blesdk_productionRelease()};
    }

    @DexIgnore
    public HeartbeatEvent(byte b) {
        super(AsyncEventType.HEARTBEAT_EVENT, b);
    }

    @DexIgnore
    public HeartbeatEvent(Parcel parcel) {
        super(parcel);
    }
}
