package com.fossil.blesdk.device;

import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import com.fossil.blesdk.obfuscated.y00;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon58 extends Lambda implements xc4<Phase, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceImplementation this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$Anon58(DeviceImplementation deviceImplementation, Phase phase) {
        super(1);
        this.this$Anon0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Phase) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Phase phase) {
        kd4.b(phase, "preparingPhase");
        if (!y00.f.a(this.this$Anon0.getDeviceInformation(), this.$phase$inlined)) {
            phase.a(Phase.Result.ResultCode.REQUEST_UNSUPPORTED);
        }
    }
}
