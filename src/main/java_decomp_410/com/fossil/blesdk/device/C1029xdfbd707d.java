package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$79 */
public final class C1029xdfbd707d extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.yc4<com.fossil.blesdk.device.logic.phase.Phase, java.lang.Float, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$79$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$79$a */
    public static final class C1030a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C1029xdfbd707d f2640e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2641f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ float f2642g;

        @DexIgnore
        public C1030a(com.fossil.blesdk.device.C1029xdfbd707d deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$79, com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
            this.f2640e = deviceImplementation$validateAndRunPhase$$inlined$synchronized$lambda$79;
            this.f2641f = phase;
            this.f2642g = f;
        }

        @DexIgnore
        public final void run() {
            com.fossil.blesdk.device.DeviceImplementation deviceImplementation = this.f2640e.this$0;
            com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
            java.lang.String logName$blesdk_productionRelease = this.f2641f.mo7562g().getLogName$blesdk_productionRelease();
            com.fossil.blesdk.device.DeviceImplementation.m2997a(deviceImplementation, logLevel, logName$blesdk_productionRelease, "Progress: " + this.f2642g, false, 8, (java.lang.Object) null);
            this.f2640e.$progressTask.mo16943a(this.f2642g);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C1029xdfbd707d(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(2);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj, java.lang.Object obj2) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj, ((java.lang.Number) obj2).floatValue());
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase, float f) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executingPhase");
        this.this$0.f2346f.post(new com.fossil.blesdk.device.C1029xdfbd707d.C1030a(this, phase, f));
    }
}
