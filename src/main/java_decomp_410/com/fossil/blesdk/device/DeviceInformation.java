package com.fossil.blesdk.device;

import android.bluetooth.BluetoothAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.VersionInformation;
import com.fossil.blesdk.device.data.WatchParametersFileVersion;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.FileHandle;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.ee4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.i20;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.r00;
import com.fossil.blesdk.obfuscated.s00;
import com.fossil.blesdk.obfuscated.ua0;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.obfuscated.wd4;
import com.fossil.blesdk.obfuscated.ya4;
import com.fossil.blesdk.obfuscated.za4;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceInformation extends JSONAbleObject {
    @DexIgnore
    public static /* final */ a Companion; // = new a((fd4) null);
    @DexIgnore
    public /* final */ BondRequirement bondRequired;
    @DexIgnore
    public /* final */ Version bootloaderVersion;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, Version> currentFilesVersion;
    @DexIgnore
    public /* final */ Version deviceSecurityVersion;
    @DexIgnore
    public DeviceType deviceType;
    @DexIgnore
    public /* final */ String firmwareVersion;
    @DexIgnore
    public /* final */ Version fontVersion;
    @DexIgnore
    public /* final */ String hardwareRevision;
    @DexIgnore
    public /* final */ String heartRateSerialNumber;
    @DexIgnore
    public /* final */ String localeString;
    @DexIgnore
    public /* final */ String macAddress;
    @DexIgnore
    public /* final */ Version microAppVersion;
    @DexIgnore
    public /* final */ String modelNumber;
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ String serialNumber;
    @DexIgnore
    public /* final */ DeviceConfigKey[] supportedDeviceConfigKeys;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, Version> supportedFilesVersion;
    @DexIgnore
    public /* final */ Version watchAppVersion;

    @DexIgnore
    public enum BondRequirement {
        NO_REQUIRE((byte) 0),
        REQUIRE((byte) 1);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ byte id;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final BondRequirement a(byte b) {
                for (BondRequirement bondRequirement : BondRequirement.values()) {
                    if (bondRequirement.getId$blesdk_productionRelease() == b) {
                        return bondRequirement;
                    }
                }
                return null;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        BondRequirement(byte b) {
            this.id = b;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final byte getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    public enum DeviceInfoItemKey {
        SERIAL_NUMBER(1),
        HARDWARE_REVISION(2),
        FIRMWARE_VERSION(3),
        MODEL_NUMBER(4),
        HEART_RATE_SERIAL_NUMBER(5),
        BOOTLOADER_VERSION(6),
        WATCH_APP_VERSION(7),
        FONT_VERSION(8),
        LUTS_VERSION(9),
        SUPPORTED_FILES_VERSION(10),
        BOND_REQUIREMENT(11),
        SUPPORTED_DEVICE_CONFIGS(12),
        DEVICE_SECURITY_VERSION(14),
        SOCKET_INFO(15),
        LOCALE(16),
        MICRO_APP_SYSTEM_VERSION(17),
        LOCALE_VERSION(18),
        CURRENT_FILES_VERSION(19),
        UNKNOWN((short) 65535);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ short id;
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        DeviceInfoItemKey(int i) {
            throw null;
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final DeviceInfoItemKey a(short s) {
                DeviceInfoItemKey deviceInfoItemKey;
                DeviceInfoItemKey[] values = DeviceInfoItemKey.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        deviceInfoItemKey = null;
                        break;
                    }
                    deviceInfoItemKey = values[i];
                    if (deviceInfoItemKey.getId$blesdk_productionRelease() == s) {
                        break;
                    }
                    i++;
                }
                return deviceInfoItemKey != null ? deviceInfoItemKey : DeviceInfoItemKey.UNKNOWN;
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        DeviceInfoItemKey(short s) {
            this.id = s;
            String name = name();
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            if (name != null) {
                String lowerCase = name.toLowerCase(locale);
                kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                this.logName = lowerCase;
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final short getId$blesdk_productionRelease() {
            return this.id;
        }

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final boolean a(String str) {
            kd4.b(str, "serialNumber");
            if (str.length() != 10) {
                return false;
            }
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isLetterOrDigit(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        /* JADX WARNING: Can't fix incorrect switch cases order */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x03b7, code lost:
            r4 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x03b8, code lost:
            r3 = r26;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x040a, code lost:
            r1 = r1 + (r24 + 3);
         */
        @DexIgnore
        public final DeviceInformation a(byte[] bArr) throws IllegalArgumentException {
            throw null;
            // DeviceInformation deviceInformation;
            // byte[] bArr2 = bArr;
            // kd4.b(bArr2, "data");
            // DeviceInformation deviceInformation2 = new DeviceInformation("", "", "", (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131064, (fd4) null);
            // ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            // DeviceInformation deviceInformation3 = deviceInformation2;
            // int i = 0;
            // while (i <= bArr2.length - 3) {
            //     short s = order.getShort(i);
            //     byte b = order.get(i + 2);
            //     DeviceInfoItemKey a = DeviceInfoItemKey.Companion.a(s);
            //     int i2 = i + 3;
            //     int i3 = i2 + b;
            //     if (bArr2.length >= i3) {
            //         byte[] a2 = ya4.a(bArr2, i2, i3);
            //         switch (r00.a[a.ordinal()]) {
            //             case 1:
            //                 Charset forName = Charset.forName("US-ASCII");
            //                 kd4.a((Object) forName, "Charset.forName(\"US-ASCII\")");
            //                 String str = new String(a2, forName);
            //                 if (a(str)) {
            //                     deviceInformation3 = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, str, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131067, (Object) null);
            //                     break;
            //                 } else {
            //                     throw new IllegalArgumentException("Invalid Serial Number: " + str);
            //                 }
            //             case 2:
            //                 Charset forName2 = Charset.forName("US-ASCII");
            //                 kd4.a((Object) forName2, "Charset.forName(\"US-ASCII\")");
            //                 String str2 = r3;
            //                 String str3 = new String(a2, forName2);
            //                 deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, str2, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131063, (Object) null);
            //             case 3:
            //                 Charset forName3 = Charset.forName("US-ASCII");
            //                 kd4.a((Object) forName3, "Charset.forName(\"US-ASCII\")");
            //                 String str4 = r3;
            //                 String str5 = new String(a2, forName3);
            //                 deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, str4, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131055, (Object) null);
            //             case 4:
            //                 Charset forName4 = Charset.forName("US-ASCII");
            //                 kd4.a((Object) forName4, "Charset.forName(\"US-ASCII\")");
            //                 String str6 = r3;
            //                 String str7 = new String(a2, forName4);
            //                 deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, str6, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131039, (Object) null);
            //             case 5:
            //                 Charset forName5 = Charset.forName("US-ASCII");
            //                 kd4.a((Object) forName5, "Charset.forName(\"US-ASCII\")");
            //                 String str8 = r3;
            //                 String str9 = new String(a2, forName5);
            //                 deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, str8, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 131007, (Object) null);
            //             case 6:
            //                 Version a3 = Version.CREATOR.a(a2);
            //                 if (a3 != null) {
            //                     deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, a3, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 130943, (Object) null);
            //                 }
            //                 break;
            //             case 7:
            //                 Version a4 = Version.CREATOR.a(a2);
            //                 if (a4 != null) {
            //                     deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, a4, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 130815, (Object) null);
            //                 }
            //                 break;
            //             case 8:
            //                 Version a5 = Version.CREATOR.a(a2);
            //                 if (a5 != null) {
            //                     deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, a5, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 130559, (Object) null);
            //                 }
            //                 break;
            //             case 9:
            //                 Version a6 = Version.CREATOR.a(a2);
            //                 if (a6 != null) {
            //                     deviceInformation3.getCurrentFilesVersion$blesdk_productionRelease().put(Short.valueOf(FileType.LUTS_FILE.getFileHandleMask$blesdk_productionRelease()), a6);
            //                     break;
            //                 }
            //                 break;
            //             case 10:
            //                 byte[] bArr3 = a2;
            //                 wd4 a7 = ee4.a((wd4) za4.a(bArr3), 3);
            //                 int a8 = a7.a();
            //                 int b2 = a7.b();
            //                 int c = a7.c();
            //                 if (c < 0 ? a8 >= b2 : a8 <= b2) {
            //                     while (true) {
            //                         FileType a9 = FileType.Companion.a(bArr3[a8]);
            //                         if (a9 != null) {
            //                             deviceInformation3.getSupportedFilesVersion$blesdk_productionRelease().put(Short.valueOf(a9.getFileHandleMask$blesdk_productionRelease()), new Version(bArr3[a8 + 1], bArr3[a8 + 2]));
            //                             qa4 qa4 = qa4.a;
            //                         }
            //                         if (a8 == b2) {
            //                             break;
            //                         } else {
            //                             a8 += c;
            //                         }
            //                     }
            //                 }
            //             case 11:
            //                 byte[] bArr4 = a2;
            //                 wd4 a10 = ee4.a((wd4) za4.a(bArr4), 4);
            //                 int a11 = a10.a();
            //                 int b3 = a10.b();
            //                 int c2 = a10.c();
            //                 if (c2 < 0 ? a11 >= b3 : a11 <= b3) {
            //                     while (true) {
            //                         ByteBuffer order2 = ByteBuffer.wrap(bArr4).order(ByteOrder.LITTLE_ENDIAN);
            //                         deviceInformation3.getCurrentFilesVersion$blesdk_productionRelease().put(Short.valueOf(order2.getShort(a11)), new Version(order2.get(a11 + 2), order2.get(a11 + 3)));
            //                         if (a11 == b3) {
            //                             break;
            //                         } else {
            //                             a11 += c2;
            //                         }
            //                     }
            //                 }
            //             case 12:
            //                 byte[] bArr5 = a2;
            //                 if (!(bArr5.length == 0)) {
            //                     BondRequirement a12 = BondRequirement.Companion.a(bArr5[0]);
            //                     if (a12 != null) {
            //                         deviceInformation3 = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, a12, (DeviceConfigKey[]) null, (Version) null, (String) null, (Version) null, 126975, (Object) null);
            //                         qa4 qa42 = qa4.a;
            //                         break;
            //                     }
            //                 }
            //                 break;
            //             case 13:
            //                 byte[] bArr6 = a2;
            //                 ByteBuffer order3 = ByteBuffer.wrap(bArr6).order(ByteOrder.LITTLE_ENDIAN);
            //                 ArrayList arrayList = new ArrayList();
            //                 wd4 a13 = ee4.a((wd4) za4.a(bArr6), 2);
            //                 int a14 = a13.a();
            //                 int b4 = a13.b();
            //                 int c3 = a13.c();
            //                 if (c3 < 0 ? a14 >= b4 : a14 <= b4) {
            //                     while (true) {
            //                         DeviceConfigKey a15 = DeviceConfigKey.Companion.a(order3.getShort(a14));
            //                         if (a15 != null) {
            //                             Boolean.valueOf(arrayList.add(a15));
            //                         }
            //                         if (a14 != b4) {
            //                             a14 += c3;
            //                         }
            //                     }
            //                 }
            //                 Object[] array = arrayList.toArray(new DeviceConfigKey[0]);
            //                 if (array != null) {
            //                     deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) array, (Version) null, (String) null, (Version) null, 122879, (Object) null);
            //                     break;
            //                 } else {
            //                     throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            //                 }
            //             case 14:
            //                 Version a16 = Version.CREATOR.a(a2);
            //                 if (a16 != null) {
            //                     deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, a16, (String) null, (Version) null, 114687, (Object) null);
            //                     break;
            //                 }
            //                 break;
            //             case 16:
            //                 Charset forName6 = Charset.forName("US-ASCII");
            //                 kd4.a((Object) forName6, "Charset.forName(\"US-ASCII\")");
            //                 String str10 = r3;
            //                 String str11 = new String(a2, forName6);
            //                 deviceInformation = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, str10, (Version) null, 98303, (Object) null);
            //                 break;
            //             case 17:
            //                 Version a17 = Version.CREATOR.a(a2);
            //                 if (a17 != null) {
            //                     deviceInformation3.getCurrentFilesVersion$blesdk_productionRelease().put((short) 1794, a17);
            //                 }
            //                 break;
            //             case 18:
            //                 Version a18 = Version.CREATOR.a(a2);
            //                 if (a18 != null) {
            //                     deviceInformation3 = DeviceInformation.copy$default(deviceInformation3, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Version) null, (Version) null, (Version) null, (LinkedHashMap) null, (LinkedHashMap) null, (BondRequirement) null, (DeviceConfigKey[]) null, (Version) null, (String) null, a18, 65535, (Object) null);
            //                 }
            //                 break;
            //         }
            //     } else {
            //         return deviceInformation3;
            //     }
            // }
            // return deviceInformation3;
        }

        @DexIgnore
        public final DeviceInformation a(JSONObject jSONObject) {
            JSONObject jSONObject2 = jSONObject;
            kd4.b(jSONObject2, "jsonObject");
            String optString = jSONObject2.optString(JSONKey.NAME.getLogName$blesdk_productionRelease(), "");
            String optString2 = jSONObject2.optString(JSONKey.MAC_ADDRESS.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString, "name");
            if ((optString.length() == 0) || !BluetoothAdapter.checkBluetoothAddress(optString2)) {
                return null;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            JSONArray optJSONArray = jSONObject2.optJSONArray(JSONKey.SUPPORTED_FILES_VERSION.getLogName$blesdk_productionRelease());
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            Short valueOf = Short.valueOf(Short.parseShort(optJSONObject.get(JSONKey.FILE_HANDLE.getLogName$blesdk_productionRelease()).toString()));
                            Version.a aVar = Version.CREATOR;
                            String optString3 = optJSONObject.optString(JSONKey.VERSION.getLogName$blesdk_productionRelease());
                            kd4.a((Object) optString3, "fileVersionJSON.optString(JSONKey.VERSION.logName)");
                            linkedHashMap.put(valueOf, aVar.a(optString3));
                        }
                    } catch (JSONException e) {
                        da0.l.a(e);
                    }
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            JSONArray optJSONArray2 = jSONObject2.optJSONArray(JSONKey.CURRENT_FILES_VERSION.getLogName$blesdk_productionRelease());
            if (optJSONArray2 != null) {
                int length2 = optJSONArray2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    try {
                        JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject2 != null) {
                            Short valueOf2 = Short.valueOf(Short.parseShort(optJSONObject2.get(JSONKey.FILE_HANDLE.getLogName$blesdk_productionRelease()).toString()));
                            Version.a aVar2 = Version.CREATOR;
                            String optString4 = optJSONObject2.optString(JSONKey.VERSION.getLogName$blesdk_productionRelease());
                            kd4.a((Object) optString4, "fileVersionJSON.optString(JSONKey.VERSION.logName)");
                            linkedHashMap2.put(valueOf2, aVar2.a(optString4));
                        }
                    } catch (JSONException e2) {
                        da0.l.a(e2);
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray3 = jSONObject2.optJSONArray(JSONKey.SUPPORTED_DEVICE_CONFIG.getLogName$blesdk_productionRelease());
            if (optJSONArray3 != null) {
                int length3 = optJSONArray3.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    try {
                        DeviceConfigKey a = DeviceConfigKey.Companion.a(optJSONArray3.get(i3).toString());
                        if (a != null) {
                            arrayList.add(a);
                        }
                    } catch (JSONException e3) {
                        da0.l.a(e3);
                    }
                }
            }
            kd4.a((Object) optString2, "macAddress");
            String optString5 = jSONObject2.optString(JSONKey.SERIAL_NUMBER.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString5, "jsonObject.optString(JSO\u2026ERIAL_NUMBER.logName, \"\")");
            String optString6 = jSONObject2.optString(JSONKey.HARDWARE_REVISION.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString6, "jsonObject.optString(JSO\u2026ARE_REVISION.logName, \"\")");
            String optString7 = jSONObject2.optString(JSONKey.FIRMWARE_VERSION.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString7, "jsonObject.optString(JSO\u2026WARE_VERSION.logName, \"\")");
            String optString8 = jSONObject2.optString(JSONKey.MODEL_NUMBER.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString8, "jsonObject.optString(JSO\u2026MODEL_NUMBER.logName, \"\")");
            String optString9 = jSONObject2.optString(JSONKey.HEART_RATE_SERIAL_NUMBER.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString9, "jsonObject.optString(JSO\u2026ERIAL_NUMBER.logName, \"\")");
            Version.a aVar3 = Version.CREATOR;
            String optString10 = jSONObject2.optString(JSONKey.BOOT_LOADER_VERSION.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString10, "jsonObject\n             \u2026ADER_VERSION.logName, \"\")");
            Version a2 = aVar3.a(optString10);
            Version.a aVar4 = Version.CREATOR;
            String optString11 = jSONObject2.optString(JSONKey.WATCH_APP_VERSION.getLogName$blesdk_productionRelease(), "");
            kd4.a((Object) optString11, "jsonObject\n             \u2026_APP_VERSION.logName, \"\")");
            Version a3 = aVar4.a(optString11);
            Version.a aVar5 = Version.CREATOR;
            LinkedHashMap linkedHashMap3 = linkedHashMap2;
            String optString12 = jSONObject2.optString(JSONKey.FONT_VERSION.getLogName$blesdk_productionRelease(), "");
            LinkedHashMap linkedHashMap4 = linkedHashMap;
            kd4.a((Object) optString12, "jsonObject\n             \u2026FONT_VERSION.logName, \"\")");
            Version a4 = aVar5.a(optString12);
            BondRequirement a5 = BondRequirement.Companion.a((byte) jSONObject2.optInt(JSONKey.BOND_REQUIRED.getLogName$blesdk_productionRelease(), 0));
            if (a5 == null) {
                a5 = BondRequirement.NO_REQUIRE;
            }
            BondRequirement bondRequirement = a5;
            Object[] array = arrayList.toArray(new DeviceConfigKey[0]);
            if (array != null) {
                Version.a aVar6 = Version.CREATOR;
                String optString13 = jSONObject2.optString(JSONKey.DEVICE_SECURITY_VERSION.getLogName$blesdk_productionRelease(), "");
                kd4.a((Object) optString13, "jsonObject\n             \u2026RITY_VERSION.logName, \"\")");
                Version a6 = aVar6.a(optString13);
                String optString14 = jSONObject2.optString(JSONKey.LOCALE.getLogName$blesdk_productionRelease(), "en_US");
                String str = optString14;
                kd4.a((Object) optString14, "jsonObject.optString(JSO\u2026nt.DEFAULT_LOCALE_STRING)");
                Version.a aVar7 = Version.CREATOR;
                String optString15 = jSONObject2.optString(JSONKey.MICRO_APP_VERSION.getLogName$blesdk_productionRelease(), "");
                kd4.a((Object) optString15, "jsonObject\n             \u2026_APP_VERSION.logName, \"\")");
                return new DeviceInformation(optString, optString2, optString5, optString6, optString7, optString8, optString9, a2, a3, a4, linkedHashMap4, linkedHashMap3, bondRequirement, (DeviceConfigKey[]) array, a6, str, aVar7.a(optString15));
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final DeviceInformation a(DeviceInformation deviceInformation, DeviceInformation deviceInformation2) {
            String str;
            String str2;
            String str3;
            String str4;
            String str5;
            String str6;
            String str7;
            Version version;
            Version version2;
            Version version3;
            LinkedHashMap<Short, Version> linkedHashMap;
            LinkedHashMap<Short, Version> linkedHashMap2;
            DeviceConfigKey[] deviceConfigKeyArr;
            Version version4;
            String str8;
            Version version5;
            kd4.b(deviceInformation, "first");
            kd4.b(deviceInformation2, "second");
            if (deviceInformation2.getName().length() > 0) {
                str = deviceInformation2.getName();
            } else {
                str = deviceInformation.getName();
            }
            String str9 = str;
            if (deviceInformation2.getMacAddress().length() > 0) {
                str2 = deviceInformation2.getMacAddress();
            } else {
                str2 = deviceInformation.getMacAddress();
            }
            String str10 = str2;
            if (a(deviceInformation2.getSerialNumber())) {
                str3 = deviceInformation2.getSerialNumber();
            } else {
                str3 = deviceInformation.getSerialNumber();
            }
            String str11 = str3;
            if (deviceInformation2.getHardwareRevision().length() > 0) {
                str4 = deviceInformation2.getHardwareRevision();
            } else {
                str4 = deviceInformation.getHardwareRevision();
            }
            String str12 = str4;
            if (deviceInformation2.getFirmwareVersion().length() > 0) {
                str5 = deviceInformation2.getFirmwareVersion();
            } else {
                str5 = deviceInformation.getFirmwareVersion();
            }
            String str13 = str5;
            if (deviceInformation2.getModelNumber().length() > 0) {
                str6 = deviceInformation2.getModelNumber();
            } else {
                str6 = deviceInformation.getModelNumber();
            }
            String str14 = str6;
            if (deviceInformation2.getHeartRateSerialNumber$blesdk_productionRelease().length() > 0) {
                str7 = deviceInformation2.getHeartRateSerialNumber$blesdk_productionRelease();
            } else {
                str7 = deviceInformation.getHeartRateSerialNumber$blesdk_productionRelease();
            }
            String str15 = str7;
            if (!kd4.a((Object) deviceInformation2.getBootloaderVersion$blesdk_productionRelease(), (Object) new Version((byte) 0, (byte) 0))) {
                version = deviceInformation2.getBootloaderVersion$blesdk_productionRelease();
            } else {
                version = deviceInformation.getBootloaderVersion$blesdk_productionRelease();
            }
            Version version6 = version;
            if (!kd4.a((Object) deviceInformation2.getWatchAppVersion$blesdk_productionRelease(), (Object) new Version((byte) 0, (byte) 0))) {
                version2 = deviceInformation2.getWatchAppVersion$blesdk_productionRelease();
            } else {
                version2 = deviceInformation.getWatchAppVersion$blesdk_productionRelease();
            }
            Version version7 = version2;
            if (!kd4.a((Object) deviceInformation2.getFontVersion$blesdk_productionRelease(), (Object) new Version((byte) 0, (byte) 0))) {
                version3 = deviceInformation2.getFontVersion$blesdk_productionRelease();
            } else {
                version3 = deviceInformation.getFontVersion$blesdk_productionRelease();
            }
            if (!deviceInformation2.getSupportedFilesVersion$blesdk_productionRelease().isEmpty()) {
                linkedHashMap = deviceInformation2.getSupportedFilesVersion$blesdk_productionRelease();
            } else {
                linkedHashMap = deviceInformation.getSupportedFilesVersion$blesdk_productionRelease();
            }
            LinkedHashMap<Short, Version> linkedHashMap3 = linkedHashMap;
            if (!deviceInformation2.getCurrentFilesVersion$blesdk_productionRelease().isEmpty()) {
                linkedHashMap2 = deviceInformation2.getCurrentFilesVersion$blesdk_productionRelease();
            } else {
                linkedHashMap2 = deviceInformation.getCurrentFilesVersion$blesdk_productionRelease();
            }
            LinkedHashMap<Short, Version> linkedHashMap4 = linkedHashMap2;
            BondRequirement bondRequired$blesdk_productionRelease = deviceInformation2.getBondRequired$blesdk_productionRelease();
            if (!(deviceInformation2.getSupportedDeviceConfigKeys$blesdk_productionRelease().length == 0)) {
                deviceConfigKeyArr = deviceInformation2.getSupportedDeviceConfigKeys$blesdk_productionRelease();
            } else {
                deviceConfigKeyArr = deviceInformation.getSupportedDeviceConfigKeys$blesdk_productionRelease();
            }
            DeviceConfigKey[] deviceConfigKeyArr2 = deviceConfigKeyArr;
            if (!kd4.a((Object) deviceInformation2.getDeviceSecurityVersion$blesdk_productionRelease(), (Object) new Version((byte) 0, (byte) 0))) {
                version4 = deviceInformation2.getDeviceSecurityVersion$blesdk_productionRelease();
            } else {
                version4 = deviceInformation.getDeviceSecurityVersion$blesdk_productionRelease();
            }
            Version version8 = version4;
            if (!kd4.a((Object) deviceInformation2.getLocaleString(), (Object) "en_US")) {
                str8 = deviceInformation2.getLocaleString();
            } else {
                str8 = deviceInformation.getLocaleString();
            }
            if (!kd4.a((Object) deviceInformation2.getMicroAppVersion(), (Object) new Version((byte) 0, (byte) 0))) {
                version5 = deviceInformation2.getMicroAppVersion();
            } else {
                version5 = deviceInformation.getMicroAppVersion();
            }
            return new DeviceInformation(str9, str10, str11, str12, str13, str14, str15, version6, version7, version3, linkedHashMap3, linkedHashMap4, bondRequired$blesdk_productionRelease, deviceConfigKeyArr2, version8, str8, version5);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ DeviceInformation(String str, String str2, String str3, String str4, String str5, String str6, String str7, Version version, Version version2, Version version3, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, BondRequirement bondRequirement, DeviceConfigKey[] deviceConfigKeyArr, Version version4, String str8, Version version5, int i, fd4 fd4) {
        throw null;
        // this(str, str2, str3, (r0 & 8) != 0 ? "" : str4, (r0 & 16) != 0 ? "" : str5, (r0 & 32) != 0 ? "" : str6, (r0 & 64) != 0 ? "" : str7, (r0 & 128) != 0 ? new Version((byte) 0, (byte) 0) : version, (r0 & 256) != 0 ? new Version((byte) 0, (byte) 0) : version2, (r0 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? new Version((byte) 0, (byte) 0) : version3, (r0 & 1024) != 0 ? new LinkedHashMap() : linkedHashMap, (r0 & 2048) != 0 ? new LinkedHashMap() : linkedHashMap2, (r0 & 4096) != 0 ? BondRequirement.NO_REQUIRE : bondRequirement, (r0 & 8192) != 0 ? new DeviceConfigKey[0] : deviceConfigKeyArr, (r0 & RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE) != 0 ? new Version((byte) 0, (byte) 0) : version4, (32768 & r0) != 0 ? "en_US" : str8, (r0 & 65536) != 0 ? new Version((byte) 0, (byte) 0) : version5);
        // int i2 = i;
    }

    @DexIgnore
    public static /* synthetic */ DeviceInformation copy$default(DeviceInformation deviceInformation, String str, String str2, String str3, String str4, String str5, String str6, String str7, Version version, Version version2, Version version3, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, BondRequirement bondRequirement, DeviceConfigKey[] deviceConfigKeyArr, Version version4, String str8, Version version5, int i, Object obj) {
        DeviceInformation deviceInformation2 = deviceInformation;
        int i2 = i;
        return deviceInformation.copy((i2 & 1) != 0 ? deviceInformation2.name : str, (i2 & 2) != 0 ? deviceInformation2.macAddress : str2, (i2 & 4) != 0 ? deviceInformation2.serialNumber : str3, (i2 & 8) != 0 ? deviceInformation2.hardwareRevision : str4, (i2 & 16) != 0 ? deviceInformation2.firmwareVersion : str5, (i2 & 32) != 0 ? deviceInformation2.modelNumber : str6, (i2 & 64) != 0 ? deviceInformation2.heartRateSerialNumber : str7, (i2 & 128) != 0 ? deviceInformation2.bootloaderVersion : version, (i2 & 256) != 0 ? deviceInformation2.watchAppVersion : version2, (i2 & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0 ? deviceInformation2.fontVersion : version3, (i2 & 1024) != 0 ? deviceInformation2.supportedFilesVersion : linkedHashMap, (i2 & 2048) != 0 ? deviceInformation2.currentFilesVersion : linkedHashMap2, (i2 & 4096) != 0 ? deviceInformation2.bondRequired : bondRequirement, (i2 & 8192) != 0 ? deviceInformation2.supportedDeviceConfigKeys : deviceConfigKeyArr, (i2 & RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE) != 0 ? deviceInformation2.deviceSecurityVersion : version4, (i2 & 32768) != 0 ? deviceInformation2.localeString : str8, (i2 & 65536) != 0 ? deviceInformation2.microAppVersion : version5);
    }

    @DexIgnore
    public static /* synthetic */ void localeVersion$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void watchParametersFileVersion$annotations() {
    }

    @DexIgnore
    public final JSONArray a(HashMap<Short, Version> hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry next : hashMap.entrySet()) {
            jSONArray.put(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.FILE_HANDLE, n90.a(((Number) next.getKey()).shortValue())), JSONKey.FILE_HANDLE_DESCRIPTION, FileHandle.Companion.a(((Number) next.getKey()).shortValue())), JSONKey.VERSION, ((Version) next.getValue()).toString()));
        }
        return jSONArray;
    }

    @DexIgnore
    public final String component1() {
        return this.name;
    }

    @DexIgnore
    public final Version component10$blesdk_productionRelease() {
        return this.fontVersion;
    }

    @DexIgnore
    public final LinkedHashMap<Short, Version> component11$blesdk_productionRelease() {
        return this.supportedFilesVersion;
    }

    @DexIgnore
    public final LinkedHashMap<Short, Version> component12$blesdk_productionRelease() {
        return this.currentFilesVersion;
    }

    @DexIgnore
    public final BondRequirement component13$blesdk_productionRelease() {
        return this.bondRequired;
    }

    @DexIgnore
    public final DeviceConfigKey[] component14$blesdk_productionRelease() {
        return this.supportedDeviceConfigKeys;
    }

    @DexIgnore
    public final Version component15$blesdk_productionRelease() {
        return this.deviceSecurityVersion;
    }

    @DexIgnore
    public final String component16() {
        return this.localeString;
    }

    @DexIgnore
    public final Version component17() {
        return this.microAppVersion;
    }

    @DexIgnore
    public final String component2() {
        return this.macAddress;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component4() {
        return this.hardwareRevision;
    }

    @DexIgnore
    public final String component5() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final String component6() {
        return this.modelNumber;
    }

    @DexIgnore
    public final String component7$blesdk_productionRelease() {
        return this.heartRateSerialNumber;
    }

    @DexIgnore
    public final Version component8$blesdk_productionRelease() {
        return this.bootloaderVersion;
    }

    @DexIgnore
    public final Version component9$blesdk_productionRelease() {
        return this.watchAppVersion;
    }

    @DexIgnore
    public final DeviceInformation copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, Version version, Version version2, Version version3, LinkedHashMap<Short, Version> linkedHashMap, LinkedHashMap<Short, Version> linkedHashMap2, BondRequirement bondRequirement, DeviceConfigKey[] deviceConfigKeyArr, Version version4, String str8, Version version5) {
        String str9 = str;
        kd4.b(str9, "name");
        kd4.b(str2, "macAddress");
        kd4.b(str3, "serialNumber");
        kd4.b(str4, "hardwareRevision");
        kd4.b(str5, "firmwareVersion");
        kd4.b(str6, "modelNumber");
        kd4.b(str7, "heartRateSerialNumber");
        kd4.b(version, "bootloaderVersion");
        kd4.b(version2, "watchAppVersion");
        kd4.b(version3, "fontVersion");
        kd4.b(linkedHashMap, "supportedFilesVersion");
        kd4.b(linkedHashMap2, "currentFilesVersion");
        kd4.b(bondRequirement, "bondRequired");
        kd4.b(deviceConfigKeyArr, "supportedDeviceConfigKeys");
        kd4.b(version4, "deviceSecurityVersion");
        kd4.b(str8, "localeString");
        kd4.b(version5, "microAppVersion");
        return new DeviceInformation(str9, str2, str3, str4, str5, str6, str7, version, version2, version3, linkedHashMap, linkedHashMap2, bondRequirement, deviceConfigKeyArr, version4, str8, version5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) DeviceInformation.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            DeviceInformation deviceInformation = (DeviceInformation) obj;
            return !(kd4.a((Object) this.name, (Object) deviceInformation.name) ^ true) && !(kd4.a((Object) this.macAddress, (Object) deviceInformation.macAddress) ^ true) && !(kd4.a((Object) this.serialNumber, (Object) deviceInformation.serialNumber) ^ true) && !(kd4.a((Object) this.hardwareRevision, (Object) deviceInformation.hardwareRevision) ^ true) && !(kd4.a((Object) this.firmwareVersion, (Object) deviceInformation.firmwareVersion) ^ true) && !(kd4.a((Object) this.modelNumber, (Object) deviceInformation.modelNumber) ^ true) && !(kd4.a((Object) this.heartRateSerialNumber, (Object) deviceInformation.heartRateSerialNumber) ^ true) && !(kd4.a((Object) this.bootloaderVersion, (Object) deviceInformation.bootloaderVersion) ^ true) && !(kd4.a((Object) this.watchAppVersion, (Object) deviceInformation.watchAppVersion) ^ true) && !(kd4.a((Object) this.fontVersion, (Object) deviceInformation.fontVersion) ^ true) && !(kd4.a((Object) this.supportedFilesVersion, (Object) deviceInformation.supportedFilesVersion) ^ true) && !(kd4.a((Object) this.currentFilesVersion, (Object) deviceInformation.currentFilesVersion) ^ true) && this.bondRequired == deviceInformation.bondRequired && Arrays.equals(this.supportedDeviceConfigKeys, deviceInformation.supportedDeviceConfigKeys) && !(kd4.a((Object) this.deviceSecurityVersion, (Object) deviceInformation.deviceSecurityVersion) ^ true) && !(kd4.a((Object) this.localeString, (Object) deviceInformation.localeString) ^ true) && !(kd4.a((Object) this.microAppVersion, (Object) deviceInformation.microAppVersion) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.DeviceInformation");
    }

    @DexIgnore
    public final BondRequirement getBondRequired$blesdk_productionRelease() {
        return this.bondRequired;
    }

    @DexIgnore
    public final Version getBootloaderVersion$blesdk_productionRelease() {
        return this.bootloaderVersion;
    }

    @DexIgnore
    public final LinkedHashMap<Short, Version> getCurrentFilesVersion$blesdk_productionRelease() {
        return this.currentFilesVersion;
    }

    @DexIgnore
    public final Version getDeviceSecurityVersion$blesdk_productionRelease() {
        return this.deviceSecurityVersion;
    }

    @DexIgnore
    public final DeviceType getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final Version getFontVersion$blesdk_productionRelease() {
        return this.fontVersion;
    }

    @DexIgnore
    public final String getHardwareRevision() {
        return this.hardwareRevision;
    }

    @DexIgnore
    public final String getHeartRateSerialNumber$blesdk_productionRelease() {
        return this.heartRateSerialNumber;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.localeString;
    }

    @DexIgnore
    public final Version getLocaleVersion() {
        Version version = this.currentFilesVersion.get((short) 1794);
        return version != null ? version : ua0.y.g();
    }

    @DexIgnore
    public final VersionInformation getLocaleVersions() {
        Version version = this.currentFilesVersion.get((short) 1794);
        if (version == null) {
            version = ua0.y.g();
        }
        Version version2 = this.supportedFilesVersion.get(Short.valueOf(FileType.ASSET.getFileHandleMask$blesdk_productionRelease()));
        if (version2 == null) {
            version2 = ua0.y.g();
        }
        return new VersionInformation(version, version2);
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public final Version getMicroAppVersion() {
        return this.microAppVersion;
    }

    @DexIgnore
    public final String getModelNumber() {
        return this.modelNumber;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final DeviceConfigKey[] getSupportedDeviceConfigKeys$blesdk_productionRelease() {
        return this.supportedDeviceConfigKeys;
    }

    @DexIgnore
    public final LinkedHashMap<Short, Version> getSupportedFilesVersion$blesdk_productionRelease() {
        return this.supportedFilesVersion;
    }

    @DexIgnore
    public final Version getWatchAppVersion$blesdk_productionRelease() {
        return this.watchAppVersion;
    }

    @DexIgnore
    public final VersionInformation getWatchParameterVersions() {
        Version version = this.currentFilesVersion.get(Short.valueOf(FileType.WATCH_PARAMETERS_FILE.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = ua0.y.g();
        }
        Version version2 = this.supportedFilesVersion.get(Short.valueOf(FileType.WATCH_PARAMETERS_FILE.getFileHandleMask$blesdk_productionRelease()));
        if (version2 == null) {
            version2 = ua0.y.g();
        }
        return new VersionInformation(version, version2);
    }

    @DexIgnore
    public final WatchParametersFileVersion getWatchParametersFileVersion() {
        Version version = this.currentFilesVersion.get(Short.valueOf(FileType.WATCH_PARAMETERS_FILE.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = ua0.y.g();
        }
        Version version2 = this.supportedFilesVersion.get(Short.valueOf(FileType.WATCH_PARAMETERS_FILE.getFileHandleMask$blesdk_productionRelease()));
        if (version2 == null) {
            version2 = ua0.y.g();
        }
        return new WatchParametersFileVersion(version, version2);
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((((((((((((((((((((((((this.name.hashCode() * 31) + this.macAddress.hashCode()) * 31) + this.serialNumber.hashCode()) * 31) + this.hardwareRevision.hashCode()) * 31) + this.firmwareVersion.hashCode()) * 31) + this.modelNumber.hashCode()) * 31) + this.heartRateSerialNumber.hashCode()) * 31) + this.bootloaderVersion.hashCode()) * 31) + this.watchAppVersion.hashCode()) * 31) + this.fontVersion.hashCode()) * 31) + this.supportedFilesVersion.hashCode()) * 31) + this.currentFilesVersion.hashCode()) * 31) + this.bondRequired.hashCode()) * 31) + Arrays.hashCode(this.supportedDeviceConfigKeys)) * 31) + this.deviceSecurityVersion.hashCode()) * 31) + this.localeString.hashCode()) * 31) + this.microAppVersion.hashCode();
    }

    @DexIgnore
    public final JSONArray toJSONArray$blesdk_productionRelease(HashMap<FileType, Version> hashMap) {
        kd4.b(hashMap, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry next : hashMap.entrySet()) {
            jSONArray.put(wa0.a(wa0.a(new JSONObject(), JSONKey.FILE_TYPE, ((FileType) next.getKey()).getLogName$blesdk_productionRelease()), JSONKey.VERSION, ((Version) next.getValue()).toString()));
        }
        return jSONArray;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        throw null;
        // JSONObject a2 = wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.NAME, this.name), JSONKey.DEVICE_TYPE, this.deviceType.getLogName$blesdk_productionRelease()), JSONKey.MAC_ADDRESS, this.macAddress), JSONKey.SERIAL_NUMBER, this.serialNumber), JSONKey.HARDWARE_REVISION, this.hardwareRevision), JSONKey.FIRMWARE_VERSION, this.firmwareVersion), JSONKey.MODEL_NUMBER, this.modelNumber), JSONKey.HEART_RATE_SERIAL_NUMBER, this.heartRateSerialNumber), JSONKey.BOOT_LOADER_VERSION, this.bootloaderVersion.getShortDescription()), JSONKey.WATCH_APP_VERSION, this.watchAppVersion.getShortDescription()), JSONKey.FONT_VERSION, this.fontVersion.getShortDescription()), JSONKey.SUPPORTED_FILES_VERSION, a(this.supportedFilesVersion)), JSONKey.CURRENT_FILES_VERSION, a(this.currentFilesVersion));
        // JSONKey jSONKey = JSONKey.BOND_REQUIRED;
        // int i = s00.a[this.bondRequired.ordinal()];
        // boolean z = true;
        // if (i == 1) {
        //     z = false;
        // } else if (i != 2) {
        //     throw new NoWhenBranchMatchedException();
        // }
        // return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(a2, jSONKey, Boolean.valueOf(z)), JSONKey.SUPPORTED_DEVICE_CONFIG, i20.a(this.supportedDeviceConfigKeys)), JSONKey.DEVICE_SECURITY_VERSION, this.deviceSecurityVersion.getShortDescription()), JSONKey.LOCALE, this.localeString), JSONKey.MICRO_APP_VERSION, this.microAppVersion.getShortDescription());
    }

    @DexIgnore
    public String toString() {
        return "DeviceInformation(name=" + this.name + ", macAddress=" + this.macAddress + ", serialNumber=" + this.serialNumber + ", hardwareRevision=" + this.hardwareRevision + ", firmwareVersion=" + this.firmwareVersion + ", modelNumber=" + this.modelNumber + ", heartRateSerialNumber=" + this.heartRateSerialNumber + ", bootloaderVersion=" + this.bootloaderVersion + ", watchAppVersion=" + this.watchAppVersion + ", fontVersion=" + this.fontVersion + ", supportedFilesVersion=" + this.supportedFilesVersion + ", currentFilesVersion=" + this.currentFilesVersion + ", bondRequired=" + this.bondRequired + ", supportedDeviceConfigKeys=" + Arrays.toString(this.supportedDeviceConfigKeys) + ", deviceSecurityVersion=" + this.deviceSecurityVersion + ", localeString=" + this.localeString + ", microAppVersion=" + this.microAppVersion + ")";
    }

    @DexIgnore
    public DeviceInformation(String str, String str2, String str3, String str4, String str5, String str6, String str7, Version version, Version version2, Version version3, LinkedHashMap<Short, Version> linkedHashMap, LinkedHashMap<Short, Version> linkedHashMap2, BondRequirement bondRequirement, DeviceConfigKey[] deviceConfigKeyArr, Version version4, String str8, Version version5) {
        String str9 = str;
        String str10 = str2;
        String str11 = str3;
        String str12 = str4;
        String str13 = str5;
        String str14 = str6;
        String str15 = str7;
        Version version6 = version;
        Version version7 = version2;
        Version version8 = version3;
        LinkedHashMap<Short, Version> linkedHashMap3 = linkedHashMap;
        LinkedHashMap<Short, Version> linkedHashMap4 = linkedHashMap2;
        BondRequirement bondRequirement2 = bondRequirement;
        DeviceConfigKey[] deviceConfigKeyArr2 = deviceConfigKeyArr;
        String str16 = str8;
        kd4.b(str9, "name");
        kd4.b(str10, "macAddress");
        kd4.b(str11, "serialNumber");
        kd4.b(str12, "hardwareRevision");
        kd4.b(str13, "firmwareVersion");
        kd4.b(str14, "modelNumber");
        kd4.b(str15, "heartRateSerialNumber");
        kd4.b(version6, "bootloaderVersion");
        kd4.b(version7, "watchAppVersion");
        kd4.b(version8, "fontVersion");
        kd4.b(linkedHashMap3, "supportedFilesVersion");
        kd4.b(linkedHashMap4, "currentFilesVersion");
        kd4.b(bondRequirement2, "bondRequired");
        kd4.b(deviceConfigKeyArr2, "supportedDeviceConfigKeys");
        kd4.b(version4, "deviceSecurityVersion");
        kd4.b(str8, "localeString");
        kd4.b(version5, "microAppVersion");
        this.name = str9;
        this.macAddress = str10;
        this.serialNumber = str11;
        this.hardwareRevision = str12;
        this.firmwareVersion = str13;
        this.modelNumber = str14;
        this.heartRateSerialNumber = str15;
        this.bootloaderVersion = version6;
        this.watchAppVersion = version7;
        this.fontVersion = version8;
        this.supportedFilesVersion = linkedHashMap3;
        this.currentFilesVersion = linkedHashMap4;
        this.bondRequired = bondRequirement2;
        this.supportedDeviceConfigKeys = deviceConfigKeyArr2;
        this.deviceSecurityVersion = version4;
        this.localeString = str8;
        this.microAppVersion = version5;
        this.deviceType = DeviceType.Companion.a(this.modelNumber);
        if (this.deviceType == DeviceType.UNKNOWN) {
            this.deviceType = DeviceType.Companion.b(this.serialNumber);
        }
    }
}
