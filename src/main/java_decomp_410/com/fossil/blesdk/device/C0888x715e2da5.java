package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1 */
public final class C0888x715e2da5 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1$a */
    public static final class C0889a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0888x715e2da5 f2527e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.Object f2528f;

        @DexIgnore
        public C0889a(com.fossil.blesdk.device.C0888x715e2da5 deviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1, java.lang.Object obj) {
            this.f2527e = deviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2528f = obj;
        }

        @DexIgnore
        public final void run() {
            this.f2527e.$progressTask.mo16948c(this.f2528f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1$b */
    public static final class C0890b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0888x715e2da5 f2529e;

        @DexIgnore
        public C0890b(com.fossil.blesdk.device.C0888x715e2da5 deviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1) {
            this.f2529e = deviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
        }

        @DexIgnore
        public final void run() {
            this.f2529e.$progressTask.mo16946b(new com.fossil.blesdk.device.FeatureError(com.fossil.blesdk.device.FeatureErrorCode.UNKNOWN_ERROR, (com.fossil.blesdk.device.logic.phase.Phase.Result) null, 2, (com.fossil.blesdk.obfuscated.fd4) null));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c")
    /* renamed from: com.fossil.blesdk.device.DeviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1$c */
    public static final class C0891c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.C0888x715e2da5 f2530e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f2531f;

        @DexIgnore
        public C0891c(com.fossil.blesdk.device.C0888x715e2da5 deviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1, com.fossil.blesdk.device.logic.phase.Phase phase) {
            this.f2530e = deviceImplementation$startAuthentication$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f2531f = phase;
        }

        @DexIgnore
        public final void run() {
            this.f2530e.$progressTask.mo16946b(com.fossil.blesdk.device.FeatureError.Companion.mo6343a(this.f2531f.mo7565k(), com.fossil.blesdk.obfuscated.rb4.m27313a((kotlin.Pair<? extends K, ? extends V>[]) new kotlin.Pair[]{com.fossil.blesdk.obfuscated.oa4.m26076a(com.fossil.blesdk.device.FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED, java.lang.Boolean.valueOf(this.f2530e.this$0.mo5950q()))})));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C0888x715e2da5(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(1);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(phase, "executedPhase");
        if (phase.mo7565k().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS) {
            java.lang.Object i = phase.mo7345i();
            if (i instanceof byte[]) {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0888x715e2da5.C0889a(this, i));
            } else {
                this.this$0.f2346f.post(new com.fossil.blesdk.device.C0888x715e2da5.C0890b(this));
            }
        } else {
            this.this$0.f2346f.post(new com.fossil.blesdk.device.C0888x715e2da5.C0891c(this, phase));
            this.this$0.mo5919a(phase.mo7565k());
        }
    }
}
