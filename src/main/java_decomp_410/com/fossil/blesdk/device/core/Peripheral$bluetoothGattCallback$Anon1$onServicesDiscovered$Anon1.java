package com.fossil.blesdk.device.core;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import com.fossil.blesdk.obfuscated.kb4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral$bluetoothGattCallback$Anon1$onServicesDiscovered$Anon1 extends Lambda implements xc4<BluetoothGattService, String> {
    @DexIgnore
    public static /* final */ Peripheral$bluetoothGattCallback$Anon1$onServicesDiscovered$Anon1 INSTANCE; // = new Peripheral$bluetoothGattCallback$Anon1$onServicesDiscovered$Anon1();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Lambda implements xc4<BluetoothGattCharacteristic, String> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public Anon1() {
            super(1);
        }

        @DexIgnore
        public final String invoke(BluetoothGattCharacteristic bluetoothGattCharacteristic) {
            StringBuilder sb = new StringBuilder();
            sb.append("    ");
            kd4.a((Object) bluetoothGattCharacteristic, "characteristic");
            sb.append(bluetoothGattCharacteristic.getUuid().toString());
            return sb.toString();
        }
    }

    @DexIgnore
    public Peripheral$bluetoothGattCallback$Anon1$onServicesDiscovered$Anon1() {
        super(1);
    }

    @DexIgnore
    public final String invoke(BluetoothGattService bluetoothGattService) {
        StringBuilder sb = new StringBuilder();
        kd4.a((Object) bluetoothGattService, Constants.SERVICE);
        sb.append(bluetoothGattService.getUuid().toString());
        sb.append("\n");
        List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
        kd4.a((Object) characteristics, "service.characteristics");
        sb.append(kb4.a(characteristics, "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, Anon1.INSTANCE, 30, (Object) null));
        return sb.toString();
    }
}
