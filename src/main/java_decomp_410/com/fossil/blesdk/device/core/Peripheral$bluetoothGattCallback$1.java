package com.fossil.blesdk.device.core;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Peripheral$bluetoothGattCallback$1 extends android.bluetooth.BluetoothGattCallback {

    @DexIgnore
    /* renamed from: a */
    public /* final */ /* synthetic */ com.fossil.blesdk.device.core.Peripheral f2772a;

    @DexIgnore
    public Peripheral$bluetoothGattCallback$1(com.fossil.blesdk.device.core.Peripheral peripheral) {
        this.f2772a = peripheral;
    }

    @DexIgnore
    public void onCharacteristicChanged(android.bluetooth.BluetoothGatt bluetoothGatt, android.bluetooth.BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        com.fossil.blesdk.device.core.Peripheral peripheral = this.f2772a;
        com.fossil.blesdk.device.core.gatt.GattCharacteristic.C1106a aVar = com.fossil.blesdk.device.core.gatt.GattCharacteristic.f2787b;
        if (bluetoothGattCharacteristic != null) {
            java.util.UUID uuid = bluetoothGattCharacteristic.getUuid();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "characteristic!!.uuid");
            com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId a = aVar.mo6570a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            peripheral.mo6412a(a, value);
            return;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicRead(android.bluetooth.BluetoothGatt bluetoothGatt, android.bluetooth.BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        com.fossil.blesdk.device.core.Peripheral peripheral = this.f2772a;
        com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult a = com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.Companion.mo6585a(i);
        com.fossil.blesdk.device.core.gatt.GattCharacteristic.C1106a aVar = com.fossil.blesdk.device.core.gatt.GattCharacteristic.f2787b;
        if (bluetoothGattCharacteristic != null) {
            java.util.UUID uuid = bluetoothGattCharacteristic.getUuid();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "characteristic!!.uuid");
            com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId a2 = aVar.mo6570a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            peripheral.mo6418a(a, a2, value);
            return;
        }
        com.fossil.blesdk.obfuscated.kd4.m24405a();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicWrite(android.bluetooth.BluetoothGatt bluetoothGatt, android.bluetooth.BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGatt, "gatt");
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGattCharacteristic, "characteristic");
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        com.fossil.blesdk.device.core.Peripheral peripheral = this.f2772a;
        com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult a = com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.Companion.mo6585a(i);
        com.fossil.blesdk.device.core.gatt.GattCharacteristic.C1106a aVar = com.fossil.blesdk.device.core.gatt.GattCharacteristic.f2787b;
        java.util.UUID uuid = bluetoothGattCharacteristic.getUuid();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) uuid, "characteristic.uuid");
        com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId a2 = aVar.mo6570a(uuid);
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value == null) {
            value = new byte[0];
        }
        peripheral.mo6442b(a, a2, value);
    }

    @DexIgnore
    public void onConnectionStateChange(android.bluetooth.BluetoothGatt bluetoothGatt, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGatt, "gatt");
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        com.fossil.blesdk.device.core.Peripheral peripheral = this.f2772a;
        com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
        com.fossil.blesdk.device.core.Peripheral.m3116a(peripheral, logLevel, "Peripheral", "BluetoothGattCallback.onConnectionStateChange: status (" + i3 + "), " + "state (" + i4 + ").", false, 8, (java.lang.Object) null);
        com.fossil.blesdk.obfuscated.da0 da0 = com.fossil.blesdk.obfuscated.da0.f4334l;
        com.fossil.blesdk.log.sdklog.SdkLogEntry sdkLogEntry = r4;
        com.fossil.blesdk.log.sdklog.SdkLogEntry sdkLogEntry2 = new com.fossil.blesdk.log.sdklog.SdkLogEntry("connection_state_changed", com.fossil.blesdk.log.sdklog.EventType.CENTRAL_EVENT, this.f2772a.mo6457k(), "", "", i3 == 0, (java.lang.String) null, (com.fossil.blesdk.device.DeviceInformation) null, (com.fossil.blesdk.obfuscated.ea0) null, com.fossil.blesdk.obfuscated.wa0.m15640a(com.fossil.blesdk.obfuscated.wa0.m15640a(new org.json.JSONObject(), com.fossil.blesdk.setting.JSONKey.STATUS, java.lang.Integer.valueOf(i)), com.fossil.blesdk.setting.JSONKey.NEW_STATE, this.f2772a.mo6444c(i4).getLogName$blesdk_productionRelease()), 448, (com.fossil.blesdk.obfuscated.fd4) null);
        da0.mo8649b(sdkLogEntry);
        this.f2772a.mo6404a(i4, i3);
    }

    @DexIgnore
    public void onDescriptorWrite(android.bluetooth.BluetoothGatt bluetoothGatt, android.bluetooth.BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGatt, "gatt");
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGattDescriptor, "descriptor");
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        com.fossil.blesdk.device.core.Peripheral peripheral = this.f2772a;
        com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("onDescriptorWrite: ");
        android.bluetooth.BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) characteristic, "descriptor\n                    .characteristic");
        sb.append(characteristic.getUuid());
        com.fossil.blesdk.device.core.Peripheral.m3116a(peripheral, logLevel, "Peripheral", sb.toString(), false, 8, (java.lang.Object) null);
        android.bluetooth.BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        java.util.UUID uuid = characteristic2 != null ? characteristic2.getUuid() : null;
        java.util.UUID uuid2 = bluetoothGattDescriptor.getUuid();
        byte[] value = bluetoothGattDescriptor.getValue();
        if (value == null) {
            value = new byte[0];
        }
        if (uuid == null || uuid2 == null) {
            com.fossil.blesdk.device.core.Peripheral peripheral2 = this.f2772a;
            com.fossil.blesdk.log.debuglog.LogLevel logLevel2 = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
            com.fossil.blesdk.device.core.Peripheral.m3116a(peripheral2, logLevel2, "Peripheral", "onDescriptorWrite: ERROR, " + "characteristicUuid=" + uuid + ", " + "descriptorUuid=" + uuid2 + '.', false, 8, (java.lang.Object) null);
            return;
        }
        this.f2772a.mo6417a(com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.Companion.mo6585a(i), com.fossil.blesdk.device.core.gatt.GattCharacteristic.f2787b.mo6570a(uuid), com.fossil.blesdk.device.core.gatt.GattDescriptor.f2795d.mo6575a(uuid2), value);
    }

    @DexIgnore
    public void onMtuChanged(android.bluetooth.BluetoothGatt bluetoothGatt, int i, int i2) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGatt, "gatt");
        super.onMtuChanged(bluetoothGatt, i, i2);
        if (i2 == 0) {
            this.f2772a.f2678m = i - 3;
        }
        this.f2772a.mo6438b(com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.Companion.mo6585a(i2), i);
    }

    @DexIgnore
    public void onReadRemoteRssi(android.bluetooth.BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        this.f2772a.mo6413a(com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.Companion.mo6585a(i2), i);
    }

    @DexIgnore
    public void onServicesDiscovered(android.bluetooth.BluetoothGatt bluetoothGatt, int i) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(bluetoothGatt, "gatt");
        super.onServicesDiscovered(bluetoothGatt, i);
        java.util.List<android.bluetooth.BluetoothGattService> services = bluetoothGatt.getServices();
        com.fossil.blesdk.device.core.Peripheral peripheral = this.f2772a;
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) services, "services");
        peripheral.mo6421a((java.util.List<? extends android.bluetooth.BluetoothGattService>) services);
        com.fossil.blesdk.device.core.Peripheral peripheral2 = this.f2772a;
        com.fossil.blesdk.device.core.Peripheral peripheral3 = peripheral2;
        com.fossil.blesdk.log.debuglog.LogLevel logLevel = com.fossil.blesdk.log.debuglog.LogLevel.DEBUG;
        com.fossil.blesdk.device.core.Peripheral.m3116a(peripheral3, logLevel, "onServicesDiscovered", com.fossil.blesdk.obfuscated.kb4.m24365a(services, "\n", (java.lang.CharSequence) null, (java.lang.CharSequence) null, 0, (java.lang.CharSequence) null, com.fossil.blesdk.device.core.Peripheral$bluetoothGattCallback$1$onServicesDiscovered$1.INSTANCE, 30, (java.lang.Object) null), false, 8, (java.lang.Object) null);
        com.fossil.blesdk.device.core.Peripheral peripheral4 = this.f2772a;
        com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult a = com.fossil.blesdk.device.core.gatt.operation.GattOperationResult.GattResult.Companion.mo6585a(i);
        java.util.ArrayList arrayList = new java.util.ArrayList(com.fossil.blesdk.obfuscated.db4.m20831a(services, 10));
        for (android.bluetooth.BluetoothGattService bluetoothGattService : services) {
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) bluetoothGattService, "it");
            arrayList.add(bluetoothGattService.getUuid());
        }
        java.lang.Object[] array = arrayList.toArray(new java.util.UUID[0]);
        if (array != null) {
            java.util.UUID[] uuidArr = (java.util.UUID[]) array;
            java.util.Set keySet = this.f2772a.f2677l.keySet();
            com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) keySet, "gattCharacteristicMap.keys");
            java.lang.Object[] array2 = keySet.toArray(new com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId[0]);
            if (array2 != null) {
                peripheral4.mo6419a(a, uuidArr, (com.fossil.blesdk.device.core.gatt.GattCharacteristic.CharacteristicId[]) array2);
                return;
            }
            throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new kotlin.TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
