package com.fossil.blesdk.device.core.command;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum BluetoothCommandId {
    CLOSE,
    CONNECT,
    DISCONNECT,
    DISCOVER_SERVICE,
    READ_CHARACTERISTIC,
    READ_DESCRIPTOR,
    REQUEST_MTU,
    SUBSCRIBE_CHARACTERISTIC,
    WRITE_CHARACTERISTIC,
    WRITE_DESCRIPTOR,
    READ_RSSI,
    CREATE_BOND,
    REMOVE_BOND,
    CONNECT_HID,
    DISCONNECT_HID,
    UNKNOWN;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
