package com.fossil.blesdk.device;

import android.os.Parcelable;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.event.DeviceEvent;
import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.o00;
import com.fossil.blesdk.obfuscated.p00;
import kotlin.NoWhenBranchMatchedException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface Device extends Parcelable {

    @DexIgnore
    public enum BondState {
        BOND_NONE,
        BONDING,
        BONDED;
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final BondState a(Peripheral.BondState bondState) {
                throw null;
                // kd4.b(bondState, "peripheralBondState");
                // int i = o00.a[bondState.ordinal()];
                // if (i == 1) {
                //     return BondState.BOND_NONE;
                // }
                // if (i == 2) {
                //     return BondState.BONDING;
                // }
                // if (i == 3) {
                //     return BondState.BONDED;
                // }
                // throw new NoWhenBranchMatchedException();
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }

            @DexIgnore
            public final BondState a(int i) {
                if (i == 11) {
                    return BondState.BONDING;
                }
                if (i != 12) {
                    return BondState.BOND_NONE;
                }
                return BondState.BONDED;
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    public enum HIDState {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING;
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public /* final */ String logName;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public final HIDState a(Peripheral.HIDState hIDState) {
                throw null;
                // kd4.b(hIDState, "peripheralState");
                // int i = p00.a[hIDState.ordinal()];
                // if (i == 1) {
                //     return HIDState.DISCONNECTED;
                // }
                // if (i == 2) {
                //     return HIDState.CONNECTING;
                // }
                // if (i == 3) {
                //     return HIDState.CONNECTED;
                // }
                // if (i == 4) {
                //     return HIDState.DISCONNECTING;
                // }
                // throw new NoWhenBranchMatchedException();
            }

            @DexIgnore
            public /* synthetic */ a(fd4 fd4) {
                this();
            }
        }

        /*
        static {
            Companion = new a((fd4) null);
        }
        */

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    public enum State {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        UPGRADING_FIRMWARE,
        DISCONNECTING;
        
        @DexIgnore
        public /* final */ String logName;

        @DexIgnore
        public final String getLogName$blesdk_productionRelease() {
            return this.logName;
        }
    }

    @DexIgnore
    public interface a {
        @DexIgnore
        void onDeviceStateChanged(Device device, State state, State state2);

        @DexIgnore
        void onEventReceived(Device device, DeviceEvent deviceEvent);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Device device, Error error);
    }

    @DexIgnore
    <T> T a(Class<T> cls);

    @DexIgnore
    void a(a aVar);

    @DexIgnore
    BondState getBondState();

    @DexIgnore
    DeviceInformation getDeviceInformation();

    @DexIgnore
    State getState();

    @DexIgnore
    boolean isActive();
}
