package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$setNotificationFilter$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.data.notification.NotificationFilter, java.lang.String> {
    @DexIgnore
    public static /* final */ com.fossil.blesdk.device.DeviceImplementation$setNotificationFilter$1 INSTANCE; // = new com.fossil.blesdk.device.DeviceImplementation$setNotificationFilter$1();

    @DexIgnore
    public DeviceImplementation$setNotificationFilter$1() {
        super(1);
    }

    @DexIgnore
    public final java.lang.String invoke(com.fossil.blesdk.device.data.notification.NotificationFilter notificationFilter) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(notificationFilter, "it");
        return notificationFilter.toJSONString(2);
    }
}
