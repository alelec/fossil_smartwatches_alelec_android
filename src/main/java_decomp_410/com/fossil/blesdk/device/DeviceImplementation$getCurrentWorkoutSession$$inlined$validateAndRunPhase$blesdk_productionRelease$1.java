package com.fossil.blesdk.device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase $phase$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.f90 $progressTask;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation this$0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements java.lang.Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 e;
        @DexIgnore
        public /* final */ /* synthetic */ java.lang.Object f;

        @DexIgnore
        public a(com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 deviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1, java.lang.Object obj) {
            this.e = deviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f = obj;
        }

        @DexIgnore
        public final void run() {
            this.e.$progressTask.c(this.f);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements java.lang.Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 e;

        @DexIgnore
        public b(com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 deviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1) {
            this.e = deviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
        }

        @DexIgnore
        public final void run() {
            this.e.$progressTask.b(new com.fossil.blesdk.device.FeatureError(com.fossil.blesdk.device.FeatureErrorCode.UNKNOWN_ERROR, (com.fossil.blesdk.device.logic.phase.Phase.Result) null, 2, (com.fossil.blesdk.obfuscated.fd4) null));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements java.lang.Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 e;
        @DexIgnore
        public /* final */ /* synthetic */ com.fossil.blesdk.device.logic.phase.Phase f;

        @DexIgnore
        public c(com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1 deviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1, com.fossil.blesdk.device.logic.phase.Phase phase) {
            this.e = deviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1;
            this.f = phase;
        }

        @DexIgnore
        public final void run() {
            this.e.$progressTask.b(com.fossil.blesdk.device.FeatureError.Companion.a(this.f.k(), com.fossil.blesdk.obfuscated.rb4.a((kotlin.Pair<? extends K, ? extends V>[]) new kotlin.Pair[]{com.fossil.blesdk.obfuscated.oa4.a(com.fossil.blesdk.device.FeatureError.PhaseResultToFeatureErrorConversionOption.HAS_SERVICE_CHANGED, java.lang.Boolean.valueOf(this.e.this$0.q()))})));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1(com.fossil.blesdk.obfuscated.f90 f90, com.fossil.blesdk.device.DeviceImplementation deviceImplementation, com.fossil.blesdk.device.logic.phase.Phase phase) {
        super(1);
        this.$progressTask = f90;
        this.this$0 = deviceImplementation;
        this.$phase$inlined = phase;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase) obj);
        return com.fossil.blesdk.obfuscated.qa4.a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase phase) {
        com.fossil.blesdk.obfuscated.kd4.b(phase, "executedPhase");
        if (phase.k().getResultCode() == com.fossil.blesdk.device.logic.phase.Phase.Result.ResultCode.SUCCESS) {
            java.lang.Object i = phase.i();
            if (i instanceof com.fossil.blesdk.device.data.workoutsession.WorkoutSession) {
                this.this$0.f.post(new com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1.a(this, i));
            } else {
                this.this$0.f.post(new com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1.b(this));
            }
        } else {
            this.this$0.f.post(new com.fossil.blesdk.device.DeviceImplementation$getCurrentWorkoutSession$$inlined$validateAndRunPhase$blesdk_productionRelease$1.c(this, phase));
            this.this$0.a(phase.k());
        }
    }
}
