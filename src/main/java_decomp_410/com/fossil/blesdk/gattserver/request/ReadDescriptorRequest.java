package com.fossil.blesdk.gattserver.request;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ReadDescriptorRequest extends ClientRequest {
    @DexIgnore
    public /* final */ BluetoothGattDescriptor descriptor;
    @DexIgnore
    public /* final */ int offset;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadDescriptorRequest(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattDescriptor bluetoothGattDescriptor) {
        super(bluetoothDevice, i);
        kd4.b(bluetoothDevice, "device");
        kd4.b(bluetoothGattDescriptor, "descriptor");
        this.offset = i2;
        this.descriptor = bluetoothGattDescriptor;
    }

    @DexIgnore
    public final BluetoothGattDescriptor getDescriptor$blesdk_productionRelease() {
        return this.descriptor;
    }

    @DexIgnore
    public final int getOffset$blesdk_productionRelease() {
        return this.offset;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        String str;
        JSONObject a = wa0.a(super.toJSONObject(), JSONKey.OFFSET, Integer.valueOf(this.offset));
        JSONKey jSONKey = JSONKey.CHARACTERISTIC;
        BluetoothGattCharacteristic characteristic = this.descriptor.getCharacteristic();
        if (characteristic != null) {
            UUID uuid = characteristic.getUuid();
            if (uuid != null) {
                str = uuid.toString();
                return wa0.a(wa0.a(a, jSONKey, str), JSONKey.DESCRIPTOR, this.descriptor.getUuid().toString());
            }
        }
        str = null;
        return wa0.a(wa0.a(a, jSONKey, str), JSONKey.DESCRIPTOR, this.descriptor.getUuid().toString());
    }
}
