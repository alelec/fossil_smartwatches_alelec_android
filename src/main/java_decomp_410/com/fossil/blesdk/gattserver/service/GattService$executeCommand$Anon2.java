package com.fossil.blesdk.gattserver.service;

import com.fossil.blesdk.gattserver.command.Command;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.xc4;
import kotlin.jvm.internal.Lambda;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattService$executeCommand$Anon2 extends Lambda implements xc4<Command, qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ GattService this$Anon0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GattService$executeCommand$Anon2(GattService gattService) {
        super(1);
        this.this$Anon0 = gattService;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((Command) obj);
        return qa4.a;
    }

    @DexIgnore
    public final void invoke(Command command) {
        kd4.b(command, "executedCommand");
        this.this$Anon0.c(command);
    }
}
