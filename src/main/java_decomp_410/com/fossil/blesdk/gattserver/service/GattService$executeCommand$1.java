package com.fossil.blesdk.gattserver.service;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GattService$executeCommand$1 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.gattserver.command.Command, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.gattserver.service.GattService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GattService$executeCommand$1(com.fossil.blesdk.gattserver.service.GattService gattService) {
        super(1);
        this.this$0 = gattService;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.gattserver.command.Command) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.gattserver.command.Command command) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(command, "executedCommand");
        this.this$0.mo7953d(command);
    }
}
