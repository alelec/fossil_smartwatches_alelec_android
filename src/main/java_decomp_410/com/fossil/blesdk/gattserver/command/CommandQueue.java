package com.fossil.blesdk.gattserver.command;

import com.fossil.blesdk.gattserver.GattServer;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.qa4;
import com.fossil.blesdk.obfuscated.t90;
import com.fossil.blesdk.obfuscated.xc4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommandQueue {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public /* final */ LinkedList<Command> a; // = new LinkedList<>();
    @DexIgnore
    public Command b;
    @DexIgnore
    public /* final */ GattServer c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = CommandQueue.class.getSimpleName();
        kd4.a((Object) simpleName, "CommandQueue::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CommandQueue(GattServer gattServer) {
        kd4.b(gattServer, "gattServer");
        this.c = gattServer;
    }

    @DexIgnore
    public final void b(Command command) {
        this.b = command;
        Command command2 = this.b;
        if (command2 != null) {
            command2.a((xc4<? super Command, qa4>) new CommandQueue$execute$Anon1(this));
            if (command2 != null) {
                command2.b(this.c);
            }
        }
    }

    @DexIgnore
    public final int c(Command command) {
        synchronized (this.a) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                if (command.e() < this.a.get(i).e()) {
                    return i;
                }
            }
            int size2 = this.a.size();
            return size2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0038, code lost:
        r4.a.add(r5);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public final void a(Command command) {
        kd4.b(command, Constants.COMMAND);
        t90 t90 = t90.c;
        String str = d;
        t90.a(str, "enqueue: " + command.d().getLogName$blesdk_productionRelease());
        synchronized (this.a) {
            this.a.add(c(command), command);
            qa4 qa4 = qa4.a;
        }
        if (this.b == null) {
            Command poll = this.a.poll();
            if (poll != null) {
                b(poll);
            }
        }
    }

    @DexIgnore
    public final void a() {
        Command poll = this.a.poll();
        if (poll != null) {
            b(poll);
        } else {
            this.b = null;
        }
    }
}
