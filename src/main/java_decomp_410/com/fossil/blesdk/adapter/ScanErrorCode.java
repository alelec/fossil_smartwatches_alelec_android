package com.fossil.blesdk.adapter;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.h90;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum ScanErrorCode implements h90 {
    BLUETOOTH_OFF(0),
    LOCATION_PERMISSION_NOT_GRANTED(1),
    LOCATION_SERVICE_NOT_ENABLED(2),
    REGISTRATION_FAILED(250),
    SYSTEM_INTERNAL_ERROR(251),
    UNSUPPORTED(252),
    OUT_OF_HARDWARE_RESOURCE(253),
    SCANNING_TOO_FREQUENTLY(254),
    UNKNOWN_ERROR(255);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String logName;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ScanErrorCode a(int i) {
            ScanErrorCode scanErrorCode;
            ScanErrorCode[] values = ScanErrorCode.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    scanErrorCode = null;
                    break;
                }
                scanErrorCode = values[i2];
                if (scanErrorCode.getCode() == i) {
                    break;
                }
                i2++;
            }
            return scanErrorCode != null ? scanErrorCode : ScanErrorCode.UNKNOWN_ERROR;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    ScanErrorCode(int i) {
        this.code = i;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public int getCode() {
        return this.code;
    }

    @DexIgnore
    public String getLogName() {
        return this.logName;
    }
}
