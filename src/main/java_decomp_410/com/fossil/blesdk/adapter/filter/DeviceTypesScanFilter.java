package com.fossil.blesdk.adapter.filter;

import com.fossil.blesdk.device.DeviceImplementation;
import com.fossil.blesdk.device.DeviceType;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.t00;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class DeviceTypesScanFilter extends AbstractScanFilter {
    @DexIgnore
    public /* final */ DeviceType[] deviceTypes;

    @DexIgnore
    public DeviceTypesScanFilter(DeviceType[] deviceTypeArr) {
        kd4.b(deviceTypeArr, "deviceTypes");
        this.deviceTypes = deviceTypeArr;
    }

    @DexIgnore
    public boolean matches$blesdk_productionRelease(DeviceImplementation deviceImplementation) {
        kd4.b(deviceImplementation, "device");
        if (this.deviceTypes.length == 0) {
            return true;
        }
        for (DeviceType deviceType : this.deviceTypes) {
            if (deviceType == deviceImplementation.getDeviceInformation().getDeviceType()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(new JSONObject(), JSONKey.FILTER_TYPE, "device_type"), JSONKey.DEVICE_TYPES, t00.a(this.deviceTypes));
    }
}
