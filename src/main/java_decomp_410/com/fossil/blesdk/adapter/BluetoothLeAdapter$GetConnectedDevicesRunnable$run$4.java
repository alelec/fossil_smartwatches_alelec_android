package com.fossil.blesdk.adapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter$GetConnectedDevicesRunnable$run$4 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.logic.phase.Phase.Result, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ android.bluetooth.BluetoothDevice $bluetoothDevice;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BluetoothLeAdapter$GetConnectedDevicesRunnable$run$4(android.bluetooth.BluetoothDevice bluetoothDevice) {
        super(1);
        this.$bluetoothDevice = bluetoothDevice;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.logic.phase.Phase.Result) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.logic.phase.Phase.Result result) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(result, "it");
        com.fossil.blesdk.adapter.BluetoothLeAdapter.f2303d.remove(this.$bluetoothDevice.getAddress());
    }
}
