package com.fossil.blesdk.adapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BluetoothLeAdapter$GetConnectedDevicesRunnable$run$3 extends kotlin.jvm.internal.Lambda implements com.fossil.blesdk.obfuscated.xc4<com.fossil.blesdk.device.DeviceInformation, com.fossil.blesdk.obfuscated.qa4> {
    @DexIgnore
    public /* final */ /* synthetic */ android.bluetooth.BluetoothDevice $bluetoothDevice;
    @DexIgnore
    public /* final */ /* synthetic */ com.fossil.blesdk.device.DeviceImplementation $connectedDevice;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BluetoothLeAdapter$GetConnectedDevicesRunnable$run$3(android.bluetooth.BluetoothDevice bluetoothDevice, com.fossil.blesdk.device.DeviceImplementation deviceImplementation) {
        super(1);
        this.$bluetoothDevice = bluetoothDevice;
        this.$connectedDevice = deviceImplementation;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ java.lang.Object invoke(java.lang.Object obj) {
        invoke((com.fossil.blesdk.device.DeviceInformation) obj);
        return com.fossil.blesdk.obfuscated.qa4.f17909a;
    }

    @DexIgnore
    public final void invoke(com.fossil.blesdk.device.DeviceInformation deviceInformation) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(deviceInformation, "it");
        com.fossil.blesdk.device.DeviceImplementation.C0760b bVar = com.fossil.blesdk.device.DeviceImplementation.C0760b.f2366d;
        java.lang.String serialNumber = deviceInformation.getSerialNumber();
        java.lang.String address = this.$bluetoothDevice.getAddress();
        com.fossil.blesdk.obfuscated.kd4.m24407a((java.lang.Object) address, "bluetoothDevice.address");
        bVar.mo5966a(serialNumber, address);
        com.fossil.blesdk.adapter.BluetoothLeAdapter.f2303d.remove(this.$bluetoothDevice.getAddress());
        com.fossil.blesdk.adapter.BluetoothLeAdapter.f2311l.mo5764c(this.$connectedDevice);
    }
}
