package com.fossil.blesdk.adapter;

import com.fossil.blesdk.error.Error;
import com.fossil.blesdk.obfuscated.kd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ScanError extends Error {
    @DexIgnore
    public /* final */ ScanErrorCode errorCode;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ScanError(ScanErrorCode scanErrorCode) {
        super(scanErrorCode);
        kd4.b(scanErrorCode, "errorCode");
        this.errorCode = scanErrorCode;
    }

    @DexIgnore
    public ScanErrorCode getErrorCode() {
        return this.errorCode;
    }
}
