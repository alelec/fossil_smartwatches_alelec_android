package com.fossil.blesdk.log.generic;

import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class LogEntry extends JSONAbleObject {
    @DexIgnore
    public /* final */ String entryId;
    @DexIgnore
    public long lineNumber;
    @DexIgnore
    public long timeStamp; // = System.currentTimeMillis();

    @DexIgnore
    public LogEntry(String str) {
        kd4.b(str, "entryId");
        this.entryId = str;
    }

    @DexIgnore
    public final String getEntryId() {
        return this.entryId;
    }

    @DexIgnore
    public final long getLineNumber() {
        return this.lineNumber;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final void setLineNumber(long j) {
        this.lineNumber = j;
    }

    @DexIgnore
    public final void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject put = new JSONObject().put("timestamp", n90.a(this.timeStamp)).put("line_number", this.lineNumber);
        kd4.a((Object) put, "JSONObject().put(LogEntr\u2026.LINE_NUMBER, lineNumber)");
        return put;
    }
}
