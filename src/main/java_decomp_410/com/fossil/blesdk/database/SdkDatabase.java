package com.fossil.blesdk.database;

import android.content.Context;
import android.database.Cursor;
import androidx.room.RoomDatabase;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.gg;
import com.fossil.blesdk.obfuscated.k00;
import com.fossil.blesdk.obfuscated.k90;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.rf;
import com.fossil.blesdk.obfuscated.va0;
import com.fossil.blesdk.obfuscated.yf;
import com.fossil.blesdk.utils.Crc32Calculator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class SdkDatabase extends RoomDatabase {
    @DexIgnore
    public static /* final */ String a; // = ("com.fossil.blesdk.database." + va0.f.e());
    @DexIgnore
    public static SdkDatabase b;
    @DexIgnore
    public static /* final */ yf c; // = new a(1, 2);
    @DexIgnore
    public static /* final */ yf d; // = new b(2, 3);
    @DexIgnore
    public static /* final */ c e; // = new c((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends yf {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(gg ggVar) {
            kd4.b(ggVar, "database");
            ggVar.b("DROP TABLE ActivityFile");
            ggVar.b("CREATE TABLE DeviceFile(deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, PRIMARY KEY(deviceMacAddress, fileType, fileIndex))");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yf {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        public void migrate(gg ggVar) {
            throw null;
        //     gg ggVar2 = ggVar;
        //     kd4.b(ggVar2, "database");
        //     ggVar2.b("CREATE TABLE DeviceFile_New(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, isCompleted INTEGER NOT NULL)");
        //     Cursor d = ggVar2.d("SELECT * from DeviceFile");
        //     kd4.a((Object) d, "database.query(\"SELECT * from DeviceFile\")");
        //     int columnIndex = d.getColumnIndex("deviceMacAddress");
        //     int columnIndex2 = d.getColumnIndex("fileType");
        //     int columnIndex3 = d.getColumnIndex("fileIndex");
        //     int columnIndex4 = d.getColumnIndex("rawData");
        //     int columnIndex5 = d.getColumnIndex("fileLength");
        //     int columnIndex6 = d.getColumnIndex("fileCrc");
        //     int columnIndex7 = d.getColumnIndex("createdTimeStamp");
        //     while (d.moveToNext()) {
        //         String string = d.getString(columnIndex);
        //         kd4.a((Object) string, "oldDataCursor.getString(\u2026iceMacAddressColumnIndex)");
        //         byte b = (byte) d.getShort(columnIndex2);
        //         byte b2 = (byte) d.getShort(columnIndex3);
        //         byte[] blob = d.getBlob(columnIndex4);
        //         kd4.a((Object) blob, "oldDataCursor.getBlob(rawDataColumnIndex)");
        //         long j = d.getLong(columnIndex5);
        //         int i = columnIndex;
        //         int i2 = columnIndex2;
        //         long j2 = d.getLong(columnIndex6);
        //         int i3 = columnIndex3;
        //         int i4 = columnIndex4;
        //         long j3 = d.getLong(columnIndex7);
        //         Cursor cursor = d;
        //         int i5 = columnIndex5;
        //         int i6 = j2 == Crc32Calculator.a.a(blob, Crc32Calculator.CrcType.CRC32) ? 1 : 0;
        //         ggVar2.b("Insert into DeviceFile_New(deviceMacAddress, fileType, fileIndex, rawData, fileLength, fileCrc, createdTimeStamp, isCompleted) values ('" + string + "', " + b + ", " + b2 + ", X'" + k90.a(blob, (String) null, 1, (Object) null) + "', " + j + ", " + j2 + ", " + j3 + ", " + i6 + ')');
        //         columnIndex = i;
        //         columnIndex2 = i2;
        //         columnIndex3 = i3;
        //         columnIndex4 = i4;
        //         d = cursor;
        //         columnIndex5 = i5;
        //         columnIndex6 = columnIndex6;
        //     }
        //     ggVar2.b("DROP TABLE DeviceFile");
        //     ggVar2.b("ALTER TABLE DeviceFile_New RENAME TO DeviceFile");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final SdkDatabase a() {
            if (SdkDatabase.b == null) {
                Context a = va0.f.a();
                if (a != null) {
                    RoomDatabase.a<SdkDatabase> a2 = rf.a(a, SdkDatabase.class, SdkDatabase.a);
                    a2.a(SdkDatabase.e.b(), SdkDatabase.e.c());
                    a2.a();
                    SdkDatabase.b = a2.b();
                }
            }
            return SdkDatabase.b;
        }

        @DexIgnore
        public final yf b() {
            return SdkDatabase.c;
        }

        @DexIgnore
        public final yf c() {
            return SdkDatabase.d;
        }

        @DexIgnore
        public /* synthetic */ c(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public abstract k00 a();
}
