package com.fossil.blesdk.model.enumerate;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum RingPhoneState {
    ON,
    OFF;
    
    @DexIgnore
    public /* final */ String logName;

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }
}
