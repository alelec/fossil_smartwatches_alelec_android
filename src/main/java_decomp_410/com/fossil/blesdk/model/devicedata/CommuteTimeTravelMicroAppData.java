package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.event.request.CommuteTimeTravelMicroAppRequest;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.MicroAppRequest;
import com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.v20;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class CommuteTimeTravelMicroAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ Version microAppVersion;
    @DexIgnore
    public /* final */ boolean shipHandsToTwelve;
    @DexIgnore
    public /* final */ int travelTimeInMinute;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<CommuteTimeTravelMicroAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public CommuteTimeTravelMicroAppData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new CommuteTimeTravelMicroAppData(parcel, (fd4) null);
        }

        @DexIgnore
        public CommuteTimeTravelMicroAppData[] newArray(int i) {
            return new CommuteTimeTravelMicroAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ CommuteTimeTravelMicroAppData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void microAppVersion$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void shipHandsToTwelve$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void travelTimeInMinute$annotations() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) CommuteTimeTravelMicroAppData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            CommuteTimeTravelMicroAppData commuteTimeTravelMicroAppData = (CommuteTimeTravelMicroAppData) obj;
            return !(kd4.a((Object) this.microAppVersion, (Object) commuteTimeTravelMicroAppData.microAppVersion) ^ true) && this.shipHandsToTwelve == commuteTimeTravelMicroAppData.shipHandsToTwelve && this.travelTimeInMinute == commuteTimeTravelMicroAppData.travelTimeInMinute;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeTravelMicroAppData");
    }

    @DexIgnore
    public final Version getMicroAppVersion() {
        return this.microAppVersion;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // try {
        //     v20 v20 = v20.c;
        //     DeviceRequest deviceRequest = getDeviceRequest();
        //     if (deviceRequest != null) {
        //         return v20.a(s, version, new MicroAppCommuteTimeResponse(((MicroAppRequest) deviceRequest).getMicroAppEvent$blesdk_productionRelease(), new Version(this.microAppVersion.getMajor(), this.microAppVersion.getMinor()), this.shipHandsToTwelve, this.travelTimeInMinute).getData());
        //     }
        //     throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        // } catch (FileFormatException e) {
        //     da0.l.a(e);
        //     return new byte[0];
        // }
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return wa0.a(wa0.a(wa0.a(super.getResponseJSONLog(), JSONKey.MICRO_APP_VERSION, this.microAppVersion.toString()), JSONKey.SHIP_HANDS_TO_TWELVE, Boolean.valueOf(this.shipHandsToTwelve)), JSONKey.TRAVEL_TIME_IN_MINUTE, Integer.valueOf(this.travelTimeInMinute));
    }

    @DexIgnore
    public final boolean getShipHandsToTwelve() {
        return this.shipHandsToTwelve;
    }

    @DexIgnore
    public final int getTravelTimeInMinute() {
        return this.travelTimeInMinute;
    }

    @DexIgnore
    public int hashCode() {
        return (((((super.hashCode() * 31) + this.microAppVersion.hashCode()) * 31) + Boolean.valueOf(this.shipHandsToTwelve).hashCode()) * 31) + Integer.valueOf(this.travelTimeInMinute).hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.microAppVersion, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.shipHandsToTwelve ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.travelTimeInMinute);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppData(CommuteTimeTravelMicroAppRequest commuteTimeTravelMicroAppRequest, Version version, int i) {
        super(commuteTimeTravelMicroAppRequest, (String) null);
        kd4.b(commuteTimeTravelMicroAppRequest, "commuteTimeTravelMicroAppRequest");
        kd4.b(version, "microAppVersion");
        this.shipHandsToTwelve = true;
        this.travelTimeInMinute = i;
        this.microAppVersion = version;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CommuteTimeTravelMicroAppData(Parcel parcel) {
        super(null, null);
        throw null;
/*        this(r0, (Version) r2, parcel.readInt());
        Parcelable readParcelable = parcel.readParcelable(CommuteTimeTravelMicroAppRequest.class.getClassLoader());
        if (readParcelable != null) {
            CommuteTimeTravelMicroAppRequest commuteTimeTravelMicroAppRequest = (CommuteTimeTravelMicroAppRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
