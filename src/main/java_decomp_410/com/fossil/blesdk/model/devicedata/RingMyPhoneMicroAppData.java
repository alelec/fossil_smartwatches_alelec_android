package com.fossil.blesdk.model.devicedata;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;
import com.fossil.blesdk.device.event.request.DeviceRequest;
import com.fossil.blesdk.device.event.request.MicroAppRequest;
import com.fossil.blesdk.device.event.request.RingMyPhoneMicroAppRequest;
import com.fossil.blesdk.model.microapp.response.MicroAppRingPhoneResponse;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.v20;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class RingMyPhoneMicroAppData extends DeviceData {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ Version microAppVersion;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<RingMyPhoneMicroAppData> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public RingMyPhoneMicroAppData createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new RingMyPhoneMicroAppData(parcel, (fd4) null);
        }

        @DexIgnore
        public RingMyPhoneMicroAppData[] newArray(int i) {
            return new RingMyPhoneMicroAppData[i];
        }
    }

    @DexIgnore
    public /* synthetic */ RingMyPhoneMicroAppData(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) RingMyPhoneMicroAppData.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            return !(kd4.a((Object) this.microAppVersion, (Object) ((RingMyPhoneMicroAppData) obj).microAppVersion) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.RingMyPhoneMicroAppData");
    }

    @DexIgnore
    public final Version getMicroAppVersion() {
        return this.microAppVersion;
    }

    @DexIgnore
    public byte[] getResponseData$blesdk_productionRelease(short s, Version version) {
        throw null;
        // kd4.b(version, "version");
        // try {
        //     v20 v20 = v20.c;
        //     DeviceRequest deviceRequest = getDeviceRequest();
        //     if (deviceRequest != null) {
        //         return v20.a(s, version, new MicroAppRingPhoneResponse(((MicroAppRequest) deviceRequest).getMicroAppEvent$blesdk_productionRelease(), new Version(this.microAppVersion.getMajor(), this.microAppVersion.getMinor())).getData());
        //     }
        //     throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        // } catch (FileFormatException e) {
        //     da0.l.a(e);
        //     return new byte[0];
        // }
    }

    @DexIgnore
    public JSONObject getResponseJSONLog() {
        return wa0.a(super.getResponseJSONLog(), JSONKey.MICRO_APP_VERSION, this.microAppVersion.toString());
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.microAppVersion.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.microAppVersion, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingMyPhoneMicroAppData(RingMyPhoneMicroAppRequest ringMyPhoneMicroAppRequest, Version version) {
        super(ringMyPhoneMicroAppRequest, (String) null);
        kd4.b(ringMyPhoneMicroAppRequest, "ringPhoneRequest");
        kd4.b(version, "microAppVersion");
        this.microAppVersion = version;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public RingMyPhoneMicroAppData(Parcel parcel) {
        super(null, null);
        throw null;
/*        this(r0, (Version) r4);
        Parcelable readParcelable = parcel.readParcelable(RingMyPhoneMicroAppRequest.class.getClassLoader());
        if (readParcelable != null) {
            RingMyPhoneMicroAppRequest ringMyPhoneMicroAppRequest = (RingMyPhoneMicroAppRequest) readParcelable;
            Parcelable readParcelable2 = parcel.readParcelable(Version.class.getClassLoader());
            if (readParcelable2 != null) {
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
*/    }
}
