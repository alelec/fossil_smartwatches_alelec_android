package com.fossil.blesdk.model.buddychallenge;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.j00;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.io.Serializable;
import java.util.ArrayList;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class BuddyChallenge extends JSONAbleObject implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ String challengeId;
    @DexIgnore
    public /* final */ int durationInMinute;
    @DexIgnore
    public /* final */ ArrayList<Player> players;
    @DexIgnore
    public /* final */ int remainingInMinute;
    @DexIgnore
    public /* final */ BuddyChallengeState state;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<BuddyChallenge> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public BuddyChallenge createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new BuddyChallenge(parcel);
        }

        @DexIgnore
        public BuddyChallenge[] newArray(int i) {
            return new BuddyChallenge[i];
        }
    }

    @DexIgnore
    public BuddyChallenge(String str, ArrayList<Player> arrayList, BuddyChallengeState buddyChallengeState, int i, int i2) {
        kd4.b(str, "challengeId");
        kd4.b(arrayList, "players");
        kd4.b(buddyChallengeState, "state");
        this.challengeId = str;
        this.players = arrayList;
        this.state = buddyChallengeState;
        this.durationInMinute = i;
        this.remainingInMinute = i2;
    }

    @DexIgnore
    public static /* synthetic */ BuddyChallenge copy$default(BuddyChallenge buddyChallenge, String str, ArrayList<Player> arrayList, BuddyChallengeState buddyChallengeState, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            str = buddyChallenge.challengeId;
        }
        if ((i3 & 2) != 0) {
            arrayList = buddyChallenge.players;
        }
        ArrayList<Player> arrayList2 = arrayList;
        if ((i3 & 4) != 0) {
            buddyChallengeState = buddyChallenge.state;
        }
        BuddyChallengeState buddyChallengeState2 = buddyChallengeState;
        if ((i3 & 8) != 0) {
            i = buddyChallenge.durationInMinute;
        }
        int i4 = i;
        if ((i3 & 16) != 0) {
            i2 = buddyChallenge.remainingInMinute;
        }
        return buddyChallenge.copy(str, arrayList2, buddyChallengeState2, i4, i2);
    }

    @DexIgnore
    public final String component1() {
        return this.challengeId;
    }

    @DexIgnore
    public final ArrayList<Player> component2() {
        return this.players;
    }

    @DexIgnore
    public final BuddyChallengeState component3() {
        return this.state;
    }

    @DexIgnore
    public final int component4() {
        return this.durationInMinute;
    }

    @DexIgnore
    public final int component5() {
        return this.remainingInMinute;
    }

    @DexIgnore
    public final BuddyChallenge copy(String str, ArrayList<Player> arrayList, BuddyChallengeState buddyChallengeState, int i, int i2) {
        kd4.b(str, "challengeId");
        kd4.b(arrayList, "players");
        kd4.b(buddyChallengeState, "state");
        return new BuddyChallenge(str, arrayList, buddyChallengeState, i, i2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof BuddyChallenge) {
                BuddyChallenge buddyChallenge = (BuddyChallenge) obj;
                if (kd4.a((Object) this.challengeId, (Object) buddyChallenge.challengeId) && kd4.a((Object) this.players, (Object) buddyChallenge.players) && kd4.a((Object) this.state, (Object) buddyChallenge.state)) {
                    if (this.durationInMinute == buddyChallenge.durationInMinute) {
                        if (this.remainingInMinute == buddyChallenge.remainingInMinute) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.challengeId;
    }

    @DexIgnore
    public final int getDurationInMinute() {
        return this.durationInMinute;
    }

    @DexIgnore
    public final ArrayList<Player> getPlayers() {
        return this.players;
    }

    @DexIgnore
    public final int getRemainingInMinute() {
        return this.remainingInMinute;
    }

    @DexIgnore
    public final BuddyChallengeState getState() {
        return this.state;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.challengeId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        ArrayList<Player> arrayList = this.players;
        int hashCode2 = (hashCode + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        BuddyChallengeState buddyChallengeState = this.state;
        if (buddyChallengeState != null) {
            i = buddyChallengeState.hashCode();
        }
        return ((((hashCode2 + i) * 31) + this.durationInMinute) * 31) + this.remainingInMinute;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject a2 = wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.CHALLENGE_ID, this.challengeId), JSONKey.PLAYER_NUM, Integer.valueOf(this.players.size())), JSONKey.SESSION_STATE, this.state.getLogName()), JSONKey.SESSION_DURATION, Integer.valueOf(this.durationInMinute)), JSONKey.SESSION_REMAIN, Integer.valueOf(this.remainingInMinute));
        ArrayList arrayList = new ArrayList(this.players);
        while (arrayList.size() < 4) {
            arrayList.add(new Player("", 0, 0));
        }
        JSONKey jSONKey = JSONKey.USER;
        Object[] array = arrayList.toArray(new Player[0]);
        if (array != null) {
            wa0.a(a2, jSONKey, j00.a((JSONAbleObject[]) array));
            return a2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public String toString() {
        return "BuddyChallenge(challengeId=" + this.challengeId + ", players=" + this.players + ", state=" + this.state + ", durationInMinute=" + this.durationInMinute + ", remainingInMinute=" + this.remainingInMinute + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeString(this.challengeId);
        parcel.writeTypedList(this.players);
        parcel.writeInt(this.state.ordinal());
        parcel.writeInt(this.durationInMinute);
        parcel.writeInt(this.remainingInMinute);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public BuddyChallenge(Parcel parcel) {
        throw null;
/*        this(r2, r3, BuddyChallengeState.values()[parcel.readInt()], parcel.readInt(), parcel.readInt());
        kd4.b(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
            ArrayList createTypedArrayList = parcel.createTypedArrayList(Player.CREATOR);
            if (createTypedArrayList != null) {
                return;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
*/    }
}
