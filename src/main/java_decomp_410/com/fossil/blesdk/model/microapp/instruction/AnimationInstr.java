package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.animation.HandAnimation;
import com.fossil.blesdk.model.microapp.enumerate.AnimationType;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class AnimationInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ AnimationType animationType;
    @DexIgnore
    public HandAnimation[] animations;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<AnimationInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public AnimationInstr createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new AnimationInstr(parcel, (fd4) null);
        }

        @DexIgnore
        public AnimationInstr[] newArray(int i) {
            return new AnimationInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AnimationInstr(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) AnimationInstr.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.animations, ((AnimationInstr) obj).animations);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.AnimationInstr");
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        ByteBuffer order = ByteBuffer.allocate((this.animations.length * 3) + 2).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.animationType.getId$blesdk_productionRelease());
        byte b = (byte) 0;
        order.put(b);
        for (HandAnimation handAnimation : this.animations) {
            b = (byte) (b | handAnimation.getMicroAppHandId().getValue());
            order.put(handAnimation.getData());
        }
        order.put(1, b);
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int hashCode() {
        return this.animations.hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (HandAnimation jSONObject : this.animations) {
            jSONArray.put(jSONObject.toJSONObject());
        }
        return wa0.a(super.toJSONObject(), JSONKey.ANIMATIONS, jSONArray);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.animations, i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AnimationInstr(HandAnimation[] handAnimationArr) {
        super(InstructionId.ANIMATION);
        kd4.b(handAnimationArr, "animations");
        this.animations = new HandAnimation[0];
        this.animationType = AnimationType.SIMPLE_MOVEMENT;
        this.animations = handAnimationArr;
    }

    @DexIgnore
    public AnimationInstr(Parcel parcel) {
        super(parcel);
        this.animations = new HandAnimation[0];
        this.animationType = AnimationType.SIMPLE_MOVEMENT;
        Object[] readArray = parcel.readArray(HandAnimation.class.getClassLoader());
        if (readArray != null) {
            this.animations = (HandAnimation[]) readArray;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.microapp.animation.HandAnimation>");
    }
}
