package com.fossil.blesdk.model.microapp.enumerate;

import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public enum FileType {
    CONFIGURATION_FILE((byte) 0),
    ELEMENT_CONFIGURATION_FILE((byte) 1),
    LAUNCH_FILE((byte) 2),
    LAUNCH_ELEMENT_FILE((byte) 3),
    DECLARATION_FILE((byte) 4),
    DECLARATION_ELEMENT_FILE((byte) 5),
    CUSTOMIZATION_FILE((byte) 6),
    CUSTOMIZATION_ELEMENT_FAME((byte) 7),
    REMOTE_ACTIVITY((byte) 8);
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String logName;
    @DexIgnore
    public /* final */ byte value;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        Companion = new a((fd4) null);
    }
    */

    @DexIgnore
    FileType(byte b) {
        this.value = b;
        String name = name();
        Locale locale = Locale.US;
        kd4.a((Object) locale, "Locale.US");
        if (name != null) {
            String lowerCase = name.toLowerCase(locale);
            kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            this.logName = lowerCase;
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String getLogName$blesdk_productionRelease() {
        return this.logName;
    }

    @DexIgnore
    public final byte getValue() {
        return this.value;
    }
}
