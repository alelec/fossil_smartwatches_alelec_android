package com.fossil.blesdk.model.microapp.customization;

import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.ha0;
import com.fossil.blesdk.obfuscated.kd4;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class MicroAppCustomization extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public ha0 microAppHandle;

    @DexIgnore
    public final byte[] getData$blesdk_productionRelease() {
        ha0 ha0 = this.microAppHandle;
        if (ha0 != null) {
            int length = ha0.a().length + 3 + parameters$blesdk_productionRelease().length;
            ByteBuffer allocate = ByteBuffer.allocate(length);
            kd4.a((Object) allocate, "ByteBuffer.allocate(size)");
            allocate.order(ByteOrder.LITTLE_ENDIAN);
            ha0 ha02 = this.microAppHandle;
            if (ha02 != null) {
                allocate.put(ha02.a());
                allocate.putShort((short) length);
                allocate.put((byte) numberOfCustomizationType$blesdk_productionRelease());
                allocate.put(parameters$blesdk_productionRelease());
                byte[] array = allocate.array();
                kd4.a((Object) array, "byteBuffer.array()");
                return array;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final ha0 getMicroAppHandle$blesdk_productionRelease() {
        return this.microAppHandle;
    }

    @DexIgnore
    public abstract int numberOfCustomizationType$blesdk_productionRelease();

    @DexIgnore
    public abstract byte[] parameters$blesdk_productionRelease();

    @DexIgnore
    public final void setMicroAppHandle$blesdk_productionRelease(ha0 ha0) {
        this.microAppHandle = ha0;
    }
}
