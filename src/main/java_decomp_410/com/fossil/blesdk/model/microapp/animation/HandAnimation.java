package com.fossil.blesdk.model.microapp.animation;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.model.microapp.enumerate.Direction;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppHandId;
import com.fossil.blesdk.model.microapp.enumerate.MovingType;
import com.fossil.blesdk.model.microapp.enumerate.Speed;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HandAnimation extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ Direction direction;
    @DexIgnore
    public /* final */ MicroAppHandId microAppHandId;
    @DexIgnore
    public /* final */ MovingType movingType;
    @DexIgnore
    public /* final */ short rotation;
    @DexIgnore
    public /* final */ Speed speed;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<HandAnimation> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public HandAnimation createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new HandAnimation(parcel, (fd4) null);
        }

        @DexIgnore
        public HandAnimation[] newArray(int i) {
            return new HandAnimation[i];
        }
    }

    @DexIgnore
    public /* synthetic */ HandAnimation(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void direction$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void microAppHandId$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void movingType$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void rotation$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void speed$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) HandAnimation.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            HandAnimation handAnimation = (HandAnimation) obj;
            return this.microAppHandId == handAnimation.microAppHandId && this.direction == handAnimation.direction && this.movingType == handAnimation.movingType && this.speed == handAnimation.speed && this.rotation == handAnimation.rotation;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.animation.HandAnimation");
    }

    @DexIgnore
    public final byte[] getData() {
        ByteBuffer order = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put((byte) (((byte) (((byte) (((byte) (this.direction.getValue() << 6)) | ((byte) (this.movingType.getValue() << 5)))) | 0)) | this.speed.getValue()));
        order.putShort(this.rotation);
        byte[] array = order.array();
        kd4.a((Object) array, "dataBuffer.array()");
        return array;
    }

    @DexIgnore
    public final Direction getDirection() {
        return this.direction;
    }

    @DexIgnore
    public final MicroAppHandId getMicroAppHandId() {
        return this.microAppHandId;
    }

    @DexIgnore
    public final MovingType getMovingType() {
        return this.movingType;
    }

    @DexIgnore
    public final short getRotation() {
        return this.rotation;
    }

    @DexIgnore
    public final Speed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.microAppHandId.hashCode() * 31) + this.direction.hashCode()) * 31) + this.movingType.hashCode()) * 31) + this.speed.hashCode()) * 31) + this.rotation;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(wa0.a(wa0.a(wa0.a(new JSONObject(), JSONKey.HAND_ID, this.microAppHandId.getLogName$blesdk_productionRelease()), JSONKey.DIRECTION, this.direction.getLogName$blesdk_productionRelease()), JSONKey.MOVING_TYPE, this.movingType.getLogName$blesdk_productionRelease()), JSONKey.SPEED, this.speed.getLogName$blesdk_productionRelease()), JSONKey.ROTATION, Short.valueOf(this.rotation));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.microAppHandId.name());
        }
        if (parcel != null) {
            parcel.writeString(this.direction.name());
        }
        if (parcel != null) {
            parcel.writeString(this.movingType.name());
        }
        if (parcel != null) {
            parcel.writeString(this.speed.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.rotation);
        }
    }

    @DexIgnore
    public HandAnimation(MicroAppHandId microAppHandId2, Direction direction2, MovingType movingType2, Speed speed2, short s) {
        kd4.b(microAppHandId2, "microAppHandId");
        kd4.b(direction2, "direction");
        kd4.b(movingType2, "movingType");
        kd4.b(speed2, PlaceManager.PARAM_SPEED);
        this.microAppHandId = microAppHandId2;
        this.direction = direction2;
        this.movingType = movingType2;
        this.speed = speed2;
        this.rotation = s;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public HandAnimation(Parcel parcel) {
        throw null;
/*        this(r3, r4, r5, Speed.valueOf(r0), (short) parcel.readInt());
        String readString = parcel.readString();
        if (readString != null) {
            MicroAppHandId valueOf = MicroAppHandId.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                Direction valueOf2 = Direction.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    MovingType valueOf3 = MovingType.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
*/    }
}
