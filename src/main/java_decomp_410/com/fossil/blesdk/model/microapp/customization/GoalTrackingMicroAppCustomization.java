package com.fossil.blesdk.model.microapp.customization;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppCustomizationType;
import com.fossil.blesdk.obfuscated.bd4;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.n90;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class GoalTrackingMicroAppCustomization extends MicroAppCustomization {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ short MINIMUM_GOAL_ID; // = 0;
    @DexIgnore
    public static /* final */ short e; // = n90.a(bd4.a);
    @DexIgnore
    public /* final */ short goalId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<GoalTrackingMicroAppCustomization> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public GoalTrackingMicroAppCustomization createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new GoalTrackingMicroAppCustomization(parcel, (fd4) null);
        }

        @DexIgnore
        public GoalTrackingMicroAppCustomization[] newArray(int i) {
            return new GoalTrackingMicroAppCustomization[i];
        }
    }

    @DexIgnore
    public /* synthetic */ GoalTrackingMicroAppCustomization(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final short getGoalId() {
        return this.goalId;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        short s = e;
        short s2 = this.goalId;
        if (!(s2 >= 0 && s >= s2)) {
            throw new IllegalArgumentException("goalId(" + this.goalId + ") is out of range " + "[0, " + e + "].");
        }
    }

    @DexIgnore
    public int numberOfCustomizationType$blesdk_productionRelease() {
        return 1;
    }

    @DexIgnore
    public byte[] parameters$blesdk_productionRelease() {
        ByteBuffer allocate = ByteBuffer.allocate(3);
        kd4.a((Object) allocate, "ByteBuffer.allocate(GOAL\u2026ACKING_TYPE_FRAME_LENGTH)");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put(MicroAppCustomizationType.GOAL_TRACKING.getValue());
        allocate.put((byte) 1);
        allocate.put((byte) this.goalId);
        byte[] array = allocate.array();
        kd4.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(new JSONObject(), JSONKey.GOAL_ID, Short.valueOf(this.goalId));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        kd4.b(parcel, "parcel");
        parcel.writeByte((byte) this.goalId);
    }

    @DexIgnore
    public GoalTrackingMicroAppCustomization(short s) throws IllegalArgumentException {
        this.goalId = s;
        i();
    }

    @DexIgnore
    public GoalTrackingMicroAppCustomization(Parcel parcel) {
        this(n90.b(parcel.readByte()));
    }
}
