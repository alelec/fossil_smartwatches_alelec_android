package com.fossil.blesdk.model.microapp.instruction;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.model.microapp.enumerate.HIDCode;
import com.fossil.blesdk.model.microapp.enumerate.InstructionId;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.wa0;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class HIDInstr extends Instruction {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ HIDCode code;
    @DexIgnore
    public /* final */ boolean immediateRelease;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<HIDInstr> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public HIDInstr createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new HIDInstr(parcel, (fd4) null);
        }

        @DexIgnore
        public HIDInstr[] newArray(int i) {
            return new HIDInstr[i];
        }
    }

    @DexIgnore
    public /* synthetic */ HIDInstr(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) HIDInstr.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            HIDInstr hIDInstr = (HIDInstr) obj;
            return this.code == hIDInstr.code && this.immediateRelease == hIDInstr.immediateRelease;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.HIDInstr");
    }

    @DexIgnore
    public byte[] getParameters$blesdk_productionRelease() {
        throw null;
        // ByteBuffer order = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        // kd4.a((Object) order, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        // order.putShort(this.code.getValue());
        // order.put((this.code.getType().getValue() << 7) | this.immediateRelease ? (byte) 1 : 0);
        // byte[] array = order.array();
        // kd4.a((Object) array, "byteBuffer.array()");
        // return array;
    }

    @DexIgnore
    public int hashCode() {
        return (this.code.hashCode() * 31) + Boolean.valueOf(this.immediateRelease).hashCode();
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        return wa0.a(wa0.a(super.toJSONObject(), JSONKey.HID_CODE, this.code.getLogName$blesdk_productionRelease()), JSONKey.IMMEDIATE_RELEASE, Boolean.valueOf(this.immediateRelease));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.code.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.immediateRelease ? 1 : 0);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HIDInstr(HIDCode hIDCode, boolean z) {
        super(InstructionId.HID);
        kd4.b(hIDCode, "code");
        this.code = hIDCode;
        this.immediateRelease = z;
    }

    @DexIgnore
    public HIDInstr(Parcel parcel) {
        super(parcel);
        this.code = HIDCode.values()[parcel.readInt()];
        this.immediateRelease = parcel.readInt() != 0;
    }
}
