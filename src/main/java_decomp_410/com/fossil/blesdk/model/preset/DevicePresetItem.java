package com.fossil.blesdk.model.preset;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.background.BackgroundImageConfig;
import com.fossil.blesdk.device.data.complication.ComplicationConfig;
import com.fossil.blesdk.device.data.watchapp.WatchAppConfig;
import com.fossil.blesdk.obfuscated.da0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import com.fossil.blesdk.obfuscated.ua0;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class DevicePresetItem extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public Version fileVersion; // = ua0.y.g();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<DevicePresetItem> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public DevicePresetItem createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            String readString = parcel.readString();
            parcel.setDataPosition(0);
            if (kd4.a((Object) readString, (Object) ComplicationConfig.class.getCanonicalName())) {
                return ComplicationConfig.CREATOR.createFromParcel(parcel);
            }
            if (kd4.a((Object) readString, (Object) WatchAppConfig.class.getCanonicalName())) {
                return WatchAppConfig.CREATOR.createFromParcel(parcel);
            }
            if (kd4.a((Object) readString, (Object) BackgroundImageConfig.class.getCanonicalName())) {
                return BackgroundImageConfig.CREATOR.createFromParcel(parcel);
            }
            throw new IllegalArgumentException("Invalid parcel!");
        }

        @DexIgnore
        public DevicePresetItem[] newArray(int i) {
            return new DevicePresetItem[i];
        }
    }

    @DexIgnore
    public DevicePresetItem() {
    }

    @DexIgnore
    public abstract JSONObject getAssignmentJSON$blesdk_productionRelease();

    @DexIgnore
    public final Version getFileVersion$blesdk_productionRelease() {
        return this.fileVersion;
    }

    @DexIgnore
    public final JSONObject getSettingAssignmentJSON$blesdk_productionRelease() {
        try {
            JSONObject put = new JSONObject().put("push", new JSONObject().put("set", getAssignmentJSON$blesdk_productionRelease()));
            kd4.a((Object) put, "JSONObject().put(UIScrip\u2026ET, getAssignmentJSON()))");
            return put;
        } catch (JSONException e) {
            da0.l.a(e);
            return new JSONObject();
        }
    }

    @DexIgnore
    public final void setFileVersion$blesdk_productionRelease(Version version) {
        kd4.b(version, "<set-?>");
        this.fileVersion = version;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(getClass().getCanonicalName());
        }
    }

    @DexIgnore
    public DevicePresetItem(Parcel parcel) {
        kd4.b(parcel, "parcel");
        parcel.readString();
    }
}
