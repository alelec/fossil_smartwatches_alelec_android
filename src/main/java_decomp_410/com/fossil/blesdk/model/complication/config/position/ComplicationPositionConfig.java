package com.fossil.blesdk.model.complication.config.position;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Keep;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ComplicationPositionConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    @Keep
    public static /* final */ int MAX_ANGLE; // = 359;
    @DexIgnore
    @Keep
    public static /* final */ int MAX_DISTANCE_FROM_CENTER; // = 120;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_ANGLE; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int MIN_DISTANCE_FROM_CENTER; // = 0;
    @DexIgnore
    @Keep
    public static /* final */ int RECOMMENDED_DISTANCE_FROM_CENTER; // = 62;
    @DexIgnore
    public /* final */ int angle;
    @DexIgnore
    public /* final */ int distanceFromCenter;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationPositionConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ComplicationPositionConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new ComplicationPositionConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public ComplicationPositionConfig[] newArray(int i) {
            return new ComplicationPositionConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationPositionConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void angle$annotations() {
    }

    @DexIgnore
    public static /* synthetic */ void distanceFromCenter$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) ComplicationPositionConfig.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ComplicationPositionConfig complicationPositionConfig = (ComplicationPositionConfig) obj;
            return this.angle == complicationPositionConfig.angle && this.distanceFromCenter == complicationPositionConfig.distanceFromCenter;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.position.ComplicationPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.angle;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.distanceFromCenter;
    }

    @DexIgnore
    public int hashCode() {
        return (this.angle * 31) + this.distanceFromCenter;
    }

    @DexIgnore
    public final void i() throws IllegalArgumentException {
        int i = this.angle;
        boolean z = true;
        if (i >= 0 && 359 >= i) {
            int i2 = this.distanceFromCenter;
            if (i2 < 0 || 120 < i2) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException("distanceFromCenter(" + this.distanceFromCenter + ") is out of " + "range [0, 120].");
            }
            return;
        }
        throw new IllegalArgumentException("angle(" + this.angle + ") is out of range " + "[0, 359].");
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject put = new JSONObject().put("angle", this.angle).put("distance", this.distanceFromCenter);
        kd4.a((Object) put, "JSONObject()\n           \u2026ANCE, distanceFromCenter)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.angle);
        }
        if (parcel != null) {
            parcel.writeInt(this.distanceFromCenter);
        }
    }

    @DexIgnore
    public ComplicationPositionConfig(int i, int i2) throws IllegalArgumentException {
        this.angle = i;
        this.distanceFromCenter = i2;
        i();
    }

    @DexIgnore
    public ComplicationPositionConfig(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt());
    }
}
