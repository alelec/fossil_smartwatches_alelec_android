package com.fossil.blesdk.model.complication.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.TypeCastException;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class TimeZoneTwoComplicationDataConfig extends ComplicationDataConfig {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ String location;
    @DexIgnore
    public /* final */ int utcOffsetInMinutes;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<TimeZoneTwoComplicationDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public TimeZoneTwoComplicationDataConfig createFromParcel(Parcel parcel) {
            kd4.b(parcel, "parcel");
            return new TimeZoneTwoComplicationDataConfig(parcel, (fd4) null);
        }

        @DexIgnore
        public TimeZoneTwoComplicationDataConfig[] newArray(int i) {
            return new TimeZoneTwoComplicationDataConfig[i];
        }
    }

    @DexIgnore
    public /* synthetic */ TimeZoneTwoComplicationDataConfig(Parcel parcel, fd4 fd4) {
        this(parcel);
    }

    @DexIgnore
    public static /* synthetic */ void utcOffsetInMinutes$annotations() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!kd4.a((Object) TimeZoneTwoComplicationDataConfig.class, (Object) obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            TimeZoneTwoComplicationDataConfig timeZoneTwoComplicationDataConfig = (TimeZoneTwoComplicationDataConfig) obj;
            return !(kd4.a((Object) this.location, (Object) timeZoneTwoComplicationDataConfig.location) ^ true) && this.utcOffsetInMinutes == timeZoneTwoComplicationDataConfig.utcOffsetInMinutes;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    public final String getLocation() {
        return this.location;
    }

    @DexIgnore
    public final int getUtcOffsetInMinutes() {
        return this.utcOffsetInMinutes;
    }

    @DexIgnore
    public int hashCode() {
        return (((super.hashCode() * 31) + this.location.hashCode()) * 31) + this.utcOffsetInMinutes;
    }

    @DexIgnore
    public JSONObject toJSONObject() throws JSONException {
        JSONObject put = new JSONObject().put("loc", this.location).put("utc", this.utcOffsetInMinutes);
        kd4.a((Object) put, "JSONObject()\n           \u2026.UTC, utcOffsetInMinutes)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.location);
        }
        if (parcel != null) {
            parcel.writeInt(this.utcOffsetInMinutes);
        }
    }

    @DexIgnore
    public TimeZoneTwoComplicationDataConfig(Parcel parcel) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.location = readString;
            this.utcOffsetInMinutes = parcel.readInt();
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TimeZoneTwoComplicationDataConfig(String str, int i) {
        super(ComplicationDataConfigId.SECOND_TIMEZONE);
        kd4.b(str, PlaceFields.LOCATION);
        this.location = str;
        this.utcOffsetInMinutes = i;
    }
}
