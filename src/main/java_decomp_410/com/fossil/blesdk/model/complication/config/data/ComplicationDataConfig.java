package com.fossil.blesdk.model.complication.config.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.contract.JSONAbleObject;
import com.fossil.blesdk.obfuscated.fa0;
import com.fossil.blesdk.obfuscated.fd4;
import com.fossil.blesdk.obfuscated.kd4;
import kotlin.NoWhenBranchMatchedException;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ComplicationDataConfig extends JSONAbleObject implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((fd4) null);
    @DexIgnore
    public /* final */ ComplicationDataConfigId dataConfigId;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ComplicationDataConfig> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public ComplicationDataConfig createFromParcel(Parcel parcel) {
            throw null;
            // kd4.b(parcel, "parcel");
            // ComplicationDataConfigId complicationDataConfigId = ComplicationDataConfigId.values()[parcel.readInt()];
            // parcel.setDataPosition(0);
            // int i = fa0.a[complicationDataConfigId.ordinal()];
            // if (i == 1) {
            //     return ComplicationEmptyDataConfig.CREATOR.createFromParcel(parcel);
            // }
            // if (i == 2) {
            //     return TimeZoneTwoComplicationDataConfig.CREATOR.createFromParcel(parcel);
            // }
            // throw new NoWhenBranchMatchedException();
        }

        @DexIgnore
        public ComplicationDataConfig[] newArray(int i) {
            return new ComplicationDataConfig[i];
        }
    }

    @DexIgnore
    public ComplicationDataConfig(ComplicationDataConfigId complicationDataConfigId) {
        kd4.b(complicationDataConfigId, "dataConfigId");
        this.dataConfigId = complicationDataConfigId;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!kd4.a((Object) getClass(), (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.dataConfigId == ((ComplicationDataConfig) obj).dataConfigId;
        }
        throw new TypeCastException("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.ComplicationDataConfig");
    }

    @DexIgnore
    public int hashCode() {
        return this.dataConfigId.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.dataConfigId.ordinal());
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ComplicationDataConfig(Parcel parcel) {
        this(ComplicationDataConfigId.values()[parcel.readInt()]);
        kd4.b(parcel, "parcel");
    }
}
