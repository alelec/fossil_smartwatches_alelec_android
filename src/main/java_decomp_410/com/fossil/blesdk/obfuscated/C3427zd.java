package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zd */
public class C3427zd<A, B> extends com.fossil.blesdk.obfuscated.C3026ud<B> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3026ud<A> f11536a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> f11537b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zd$a */
    public class C3428a extends com.fossil.blesdk.obfuscated.C3026ud.C3028b<A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3026ud.C3028b f11538a;

        @DexIgnore
        public C3428a(com.fossil.blesdk.obfuscated.C3026ud.C3028b bVar) {
            this.f11538a = bVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16753a(java.util.List<A> list, int i, int i2) {
            this.f11538a.mo16753a(com.fossil.blesdk.obfuscated.C2307ld.convert(com.fossil.blesdk.obfuscated.C3427zd.this.f11537b, list), i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.zd$b */
    public class C3429b extends com.fossil.blesdk.obfuscated.C3026ud.C3031e<A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3026ud.C3031e f11540a;

        @DexIgnore
        public C3429b(com.fossil.blesdk.obfuscated.C3026ud.C3031e eVar) {
            this.f11540a = eVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16754a(java.util.List<A> list) {
            this.f11540a.mo16754a(com.fossil.blesdk.obfuscated.C2307ld.convert(com.fossil.blesdk.obfuscated.C3427zd.this.f11537b, list));
        }
    }

    @DexIgnore
    public C3427zd(com.fossil.blesdk.obfuscated.C3026ud<A> udVar, com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> m3Var) {
        this.f11536a = udVar;
        this.f11537b = m3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.f11536a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.f11536a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.f11536a.isInvalid();
    }

    @DexIgnore
    public void loadInitial(com.fossil.blesdk.obfuscated.C3026ud.C3030d dVar, com.fossil.blesdk.obfuscated.C3026ud.C3028b<B> bVar) {
        this.f11536a.loadInitial(dVar, new com.fossil.blesdk.obfuscated.C3427zd.C3428a(bVar));
    }

    @DexIgnore
    public void loadRange(com.fossil.blesdk.obfuscated.C3026ud.C3033g gVar, com.fossil.blesdk.obfuscated.C3026ud.C3031e<B> eVar) {
        this.f11536a.loadRange(gVar, new com.fossil.blesdk.obfuscated.C3427zd.C3429b(eVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.f11536a.removeInvalidatedCallback(cVar);
    }
}
