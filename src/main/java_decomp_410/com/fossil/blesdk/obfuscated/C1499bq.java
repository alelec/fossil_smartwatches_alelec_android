package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bq */
public class C1499bq implements com.fossil.blesdk.obfuscated.C2428mp, com.fossil.blesdk.obfuscated.C2902so.C2903a<java.lang.Object> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2428mp.C2429a f3864e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2499np<?> f3865f;

    @DexIgnore
    /* renamed from: g */
    public int f3866g;

    @DexIgnore
    /* renamed from: h */
    public int f3867h; // = -1;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2143jo f3868i;

    @DexIgnore
    /* renamed from: j */
    public java.util.List<com.fossil.blesdk.obfuscated.C2912sr<java.io.File, ?>> f3869j;

    @DexIgnore
    /* renamed from: k */
    public int f3870k;

    @DexIgnore
    /* renamed from: l */
    public volatile com.fossil.blesdk.obfuscated.C2912sr.C2913a<?> f3871l;

    @DexIgnore
    /* renamed from: m */
    public java.io.File f3872m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C1573cq f3873n;

    @DexIgnore
    public C1499bq(com.fossil.blesdk.obfuscated.C2499np<?> npVar, com.fossil.blesdk.obfuscated.C2428mp.C2429a aVar) {
        this.f3865f = npVar;
        this.f3864e = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9253a() {
        java.util.List<com.fossil.blesdk.obfuscated.C2143jo> c = this.f3865f.mo14061c();
        boolean z = false;
        if (c.isEmpty()) {
            return false;
        }
        java.util.List<java.lang.Class<?>> k = this.f3865f.mo14070k();
        if (!k.isEmpty()) {
            while (true) {
                if (this.f3869j == null || !mo9254b()) {
                    this.f3867h++;
                    if (this.f3867h >= k.size()) {
                        this.f3866g++;
                        if (this.f3866g >= c.size()) {
                            return false;
                        }
                        this.f3867h = 0;
                    }
                    com.fossil.blesdk.obfuscated.C2143jo joVar = c.get(this.f3866g);
                    java.lang.Class cls = k.get(this.f3867h);
                    com.fossil.blesdk.obfuscated.C1573cq cqVar = new com.fossil.blesdk.obfuscated.C1573cq(this.f3865f.mo14058b(), joVar, this.f3865f.mo14071l(), this.f3865f.mo14073n(), this.f3865f.mo14065f(), this.f3865f.mo14059b(cls), cls, this.f3865f.mo14068i());
                    this.f3873n = cqVar;
                    this.f3872m = this.f3865f.mo14063d().mo16535a(this.f3873n);
                    java.io.File file = this.f3872m;
                    if (file != null) {
                        this.f3868i = joVar;
                        this.f3869j = this.f3865f.mo14054a(file);
                        this.f3870k = 0;
                    }
                } else {
                    this.f3871l = null;
                    while (!z && mo9254b()) {
                        java.util.List<com.fossil.blesdk.obfuscated.C2912sr<java.io.File, ?>> list = this.f3869j;
                        int i = this.f3870k;
                        this.f3870k = i + 1;
                        this.f3871l = list.get(i).mo8911a(this.f3872m, this.f3865f.mo14073n(), this.f3865f.mo14065f(), this.f3865f.mo14068i());
                        if (this.f3871l != null && this.f3865f.mo14062c(this.f3871l.f9451c.getDataClass())) {
                            this.f3871l.f9451c.mo8870a(this.f3865f.mo14069j(), this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
        } else if (java.io.File.class.equals(this.f3865f.mo14072m())) {
            return false;
        } else {
            throw new java.lang.IllegalStateException("Failed to find any load path from " + this.f3865f.mo14067h() + " to " + this.f3865f.mo14072m());
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo9254b() {
        return this.f3870k < this.f3869j.size();
    }

    @DexIgnore
    public void cancel() {
        com.fossil.blesdk.obfuscated.C2912sr.C2913a<?> aVar = this.f3871l;
        if (aVar != null) {
            aVar.f9451c.cancel();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9252a(java.lang.Object obj) {
        this.f3864e.mo3952a(this.f3868i, obj, this.f3871l.f9451c, com.bumptech.glide.load.DataSource.RESOURCE_DISK_CACHE, this.f3873n);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9251a(java.lang.Exception exc) {
        this.f3864e.mo3951a(this.f3873n, exc, this.f3871l.f9451c, com.bumptech.glide.load.DataSource.RESOURCE_DISK_CACHE);
    }
}
