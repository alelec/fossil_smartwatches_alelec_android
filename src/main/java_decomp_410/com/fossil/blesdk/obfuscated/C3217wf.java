package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wf */
public abstract class C3217wf {
    @DexIgnore
    public /* final */ androidx.room.RoomDatabase mDatabase;
    @DexIgnore
    public /* final */ java.util.concurrent.atomic.AtomicBoolean mLock; // = new java.util.concurrent.atomic.AtomicBoolean(false);
    @DexIgnore
    public volatile com.fossil.blesdk.obfuscated.C2221kg mStmt;

    @DexIgnore
    public C3217wf(androidx.room.RoomDatabase roomDatabase) {
        this.mDatabase = roomDatabase;
    }

    @DexIgnore
    private com.fossil.blesdk.obfuscated.C2221kg createNewStatement() {
        return this.mDatabase.compileStatement(createQuery());
    }

    @DexIgnore
    private com.fossil.blesdk.obfuscated.C2221kg getStmt(boolean z) {
        if (!z) {
            return createNewStatement();
        }
        if (this.mStmt == null) {
            this.mStmt = createNewStatement();
        }
        return this.mStmt;
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2221kg acquire() {
        assertNotMainThread();
        return getStmt(this.mLock.compareAndSet(false, true));
    }

    @DexIgnore
    public void assertNotMainThread() {
        this.mDatabase.assertNotMainThread();
    }

    @DexIgnore
    public abstract java.lang.String createQuery();

    @DexIgnore
    public void release(com.fossil.blesdk.obfuscated.C2221kg kgVar) {
        if (kgVar == this.mStmt) {
            this.mLock.set(false);
        }
    }
}
