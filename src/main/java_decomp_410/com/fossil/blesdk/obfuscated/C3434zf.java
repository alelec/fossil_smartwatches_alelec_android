package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zf */
public abstract class C3434zf<T> extends com.fossil.blesdk.obfuscated.C3026ud<T> {
    @DexIgnore
    public /* final */ java.lang.String mCountQuery;
    @DexIgnore
    public /* final */ androidx.room.RoomDatabase mDb;
    @DexIgnore
    public /* final */ boolean mInTransaction;
    @DexIgnore
    public /* final */ java.lang.String mLimitOffsetQuery;
    @DexIgnore
    public /* final */ com.fossil.blesdk.obfuscated.C2647pf.C2650c mObserver;
    @DexIgnore
    public /* final */ com.fossil.blesdk.obfuscated.C3035uf mSourceQuery;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zf$a */
    public class C3435a extends com.fossil.blesdk.obfuscated.C2647pf.C2650c {
        @DexIgnore
        public C3435a(java.lang.String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public void onInvalidated(java.util.Set<java.lang.String> set) {
            com.fossil.blesdk.obfuscated.C3434zf.this.invalidate();
        }
    }

    @DexIgnore
    public C3434zf(androidx.room.RoomDatabase roomDatabase, com.fossil.blesdk.obfuscated.C2125jg jgVar, boolean z, java.lang.String... strArr) {
        this(roomDatabase, com.fossil.blesdk.obfuscated.C3035uf.m14728a(jgVar), z, strArr);
    }

    @DexIgnore
    private com.fossil.blesdk.obfuscated.C3035uf getSQLiteQuery(int i, int i2) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b(this.mLimitOffsetQuery, this.mSourceQuery.mo10918a() + 2);
        b.mo16765a(this.mSourceQuery);
        b.mo11934b(b.mo10918a() - 1, (long) i2);
        b.mo11934b(b.mo10918a(), (long) i);
        return b;
    }

    @DexIgnore
    public abstract java.util.List<T> convertRows(android.database.Cursor cursor);

    @DexIgnore
    public int countItems() {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b(this.mCountQuery, this.mSourceQuery.mo10918a());
        b.mo16765a(this.mSourceQuery);
        android.database.Cursor query = this.mDb.query(b);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            query.close();
            b.mo16767c();
            return 0;
        } finally {
            query.close();
            b.mo16767c();
        }
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().mo14689c();
        return super.isInvalid();
    }

    @DexIgnore
    public void loadInitial(com.fossil.blesdk.obfuscated.C3026ud.C3030d dVar, com.fossil.blesdk.obfuscated.C3026ud.C3028b<T> bVar) {
        com.fossil.blesdk.obfuscated.C3035uf ufVar;
        java.util.List list;
        int i;
        java.util.List emptyList = java.util.Collections.emptyList();
        this.mDb.beginTransaction();
        android.database.Cursor cursor = null;
        try {
            int countItems = countItems();
            if (countItems != 0) {
                i = com.fossil.blesdk.obfuscated.C3026ud.computeInitialLoadPosition(dVar, countItems);
                ufVar = getSQLiteQuery(i, com.fossil.blesdk.obfuscated.C3026ud.computeInitialLoadSize(dVar, i, countItems));
                try {
                    cursor = this.mDb.query(ufVar);
                    list = convertRows(cursor);
                    this.mDb.setTransactionSuccessful();
                } catch (Throwable th) {
                    th = th;
                }
            } else {
                list = emptyList;
                ufVar = null;
                i = 0;
            }
            if (cursor != null) {
                cursor.close();
            }
            this.mDb.endTransaction();
            if (ufVar != null) {
                ufVar.mo16767c();
            }
            bVar.mo16753a(list, i, countItems);
        } catch (Throwable th2) {
            th = th2;
            ufVar = null;
            if (cursor != null) {
                cursor.close();
            }
            this.mDb.endTransaction();
            if (ufVar != null) {
                ufVar.mo16767c();
            }
            throw th;
        }
    }

    @DexIgnore
    public void loadRange(com.fossil.blesdk.obfuscated.C3026ud.C3033g gVar, com.fossil.blesdk.obfuscated.C3026ud.C3031e<T> eVar) {
        eVar.mo16754a(loadRange(gVar.f9941a, gVar.f9942b));
    }

    @DexIgnore
    public C3434zf(androidx.room.RoomDatabase roomDatabase, com.fossil.blesdk.obfuscated.C3035uf ufVar, boolean z, java.lang.String... strArr) {
        this.mDb = roomDatabase;
        this.mSourceQuery = ufVar;
        this.mInTransaction = z;
        this.mCountQuery = "SELECT COUNT(*) FROM ( " + this.mSourceQuery.mo10920b() + " )";
        this.mLimitOffsetQuery = "SELECT * FROM ( " + this.mSourceQuery.mo10920b() + " ) LIMIT ? OFFSET ?";
        this.mObserver = new com.fossil.blesdk.obfuscated.C3434zf.C3435a(strArr);
        roomDatabase.getInvalidationTracker().mo14687b(this.mObserver);
    }

    @DexIgnore
    public java.util.List<T> loadRange(int i, int i2) {
        com.fossil.blesdk.obfuscated.C3035uf sQLiteQuery = getSQLiteQuery(i, i2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            android.database.Cursor cursor = null;
            try {
                cursor = this.mDb.query(sQLiteQuery);
                java.util.List<T> convertRows = convertRows(cursor);
                this.mDb.setTransactionSuccessful();
                return convertRows;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                this.mDb.endTransaction();
                sQLiteQuery.mo16767c();
            }
        } else {
            android.database.Cursor query = this.mDb.query(sQLiteQuery);
            try {
                return convertRows(query);
            } finally {
                query.close();
                sQLiteQuery.mo16767c();
            }
        }
    }
}
