package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.blesdk.obfuscated.a23;
import com.fossil.blesdk.obfuscated.ov2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vu2 extends as2 implements d23, View.OnClickListener, a23.d, ws3.g, ks2 {
    @DexIgnore
    public static /* final */ a s; // = new a((fd4) null);
    @DexIgnore
    public c23 k;
    @DexIgnore
    public ConstraintLayout l;
    @DexIgnore
    public RecyclerViewPager m;
    @DexIgnore
    public int n;
    @DexIgnore
    public CustomPageIndicator o;
    @DexIgnore
    public a23 p;
    @DexIgnore
    public tr3<ad2> q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final vu2 a() {
            return new vu2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ vu2 a;

        @DexIgnore
        public b(vu2 vu2) {
            this.a = vu2;
        }

        @DexIgnore
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            kd4.b(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            View childAt = recyclerView.getChildAt(0);
            kd4.a((Object) childAt, "recyclerView.getChildAt(0)");
            int measuredWidth = childAt.getMeasuredWidth();
            int computeHorizontalScrollOffset = recyclerView.computeHorizontalScrollOffset();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "initUI - offset=" + computeHorizontalScrollOffset);
            if (computeHorizontalScrollOffset % measuredWidth == 0) {
                int i3 = computeHorizontalScrollOffset / measuredWidth;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDianaCustomizeFragment", "initUI - position=" + i3);
                if (i3 != -1) {
                    this.a.p(i3);
                    this.a.T0().a(i3);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ov2.b {
        @DexIgnore
        public /* final */ /* synthetic */ vu2 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(vu2 vu2, String str) {
            this.a = vu2;
            this.b = str;
        }

        @DexIgnore
        public void a(String str) {
            kd4.b(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.T0().a(str, this.b);
            }
        }

        @DexIgnore
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore
    public void N(boolean z) {
        if (z) {
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        vl2 Q02 = Q0();
        if (Q02 != null) {
            Q02.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HomeDianaCustomizeFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final c23 T0() {
        c23 c23 = this.k;
        if (c23 != null) {
            return c23;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2, String str, int i) {
        List<? extends f8<View, String>> list3 = list;
        String str2 = str;
        kd4.b(list, "views");
        List<? extends f8<CustomizeWidget, String>> list4 = list2;
        kd4.b(list2, "customizeWidgetViews");
        kd4.b(str2, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetComplicationClick preset=");
        sb.append(f13 != null ? f13.a() : null);
        sb.append(" complicationPos=");
        sb.append(str2);
        sb.append(" position=");
        sb.append(i);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (f13 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                String c2 = f13.c();
                ArrayList arrayList = new ArrayList(list);
                c23 c23 = this.k;
                if (c23 != null) {
                    aVar.a(activity, c2, arrayList, list2, c23.i(), 1, str, ViewHierarchy.DIMENSION_TOP_KEY);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void c(String str, String str2) {
        kd4.b(str, "presetName");
        kd4.b(str2, "presetId");
        if (getChildFragmentManager().a("RenamePresetDialogFragment") == null) {
            ov2 a2 = ov2.k.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    public void d(int i) {
        if (isActive()) {
            a23 a23 = this.p;
            if (a23 == null) {
                kd4.d("mAdapterDiana");
                throw null;
            } else if (a23.getItemCount() > i) {
                RecyclerViewPager recyclerViewPager = this.m;
                if (recyclerViewPager != null) {
                    recyclerViewPager.i(i);
                } else {
                    kd4.d("rvCustomize");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void e(int i) {
        this.n = i;
        a23 a23 = this.p;
        if (a23 == null) {
            kd4.d("mAdapterDiana");
            throw null;
        } else if (a23.getItemCount() > i) {
            RecyclerViewPager recyclerViewPager = this.m;
            if (recyclerViewPager != null) {
                recyclerViewPager.i(i);
            } else {
                kd4.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public int getItemCount() {
        RecyclerViewPager recyclerViewPager = this.m;
        if (recyclerViewPager != null) {
            RecyclerView.g adapter = recyclerViewPager.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        kd4.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public void j() {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_CreateNew_NewPreset_CTA__Set));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(getChildFragmentManager(), "");
    }

    @DexIgnore
    public void l() {
        String string = getString(R.string.DesignPatterns_SetComplication_SettingComplication_Text__ApplyingToWatch);
        kd4.a((Object) string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        S(string);
    }

    @DexIgnore
    public void m() {
        a();
    }

    @DexIgnore
    public void onClick(View view) {
        kd4.b(view, "v");
        if (view.getId() == R.id.ftv_pair_watch) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                kd4.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, 2, (Object) null);
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        ad2 ad2 = (ad2) qa.a(layoutInflater, R.layout.fragment_home_diana_customize, viewGroup, false, O0());
        kd4.a((Object) ad2, "binding");
        a(ad2);
        this.q = new tr3<>(this, ad2);
        tr3<ad2> tr3 = this.q;
        if (tr3 != null) {
            ad2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        c23 c23 = this.k;
        if (c23 != null) {
            if (c23 != null) {
                c23.g();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        c23 c23 = this.k;
        if (c23 != null) {
            if (c23 != null) {
                c23.f();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("customize_view");
    }

    @DexIgnore
    public final void p(int i) {
        this.n = i;
    }

    @DexIgnore
    public void v() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showCreateNewSuccessfully");
        CustomPageIndicator customPageIndicator = this.o;
        if (customPageIndicator != null) {
            customPageIndicator.invalidate();
        } else {
            kd4.d("indicator");
            throw null;
        }
    }

    @DexIgnore
    public void y0() {
        c23 c23 = this.k;
        if (c23 != null) {
            c23.j();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void z0() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onAddPresetClick");
        c23 c23 = this.k;
        if (c23 != null) {
            c23.h();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void a(ad2 ad2) {
        this.p = new a23(new ArrayList(), this);
        ConstraintLayout constraintLayout = ad2.q;
        kd4.a((Object) constraintLayout, "binding.clNoDevice");
        this.l = constraintLayout;
        ad2.u.setImageResource(R.drawable.customization_no_device);
        ad2.s.setOnClickListener(this);
        RecyclerViewPager recyclerViewPager = ad2.v;
        kd4.a((Object) recyclerViewPager, "binding.rvPreset");
        this.m = recyclerViewPager;
        RecyclerViewPager recyclerViewPager2 = this.m;
        if (recyclerViewPager2 != null) {
            recyclerViewPager2.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
            RecyclerViewPager recyclerViewPager3 = this.m;
            if (recyclerViewPager3 != null) {
                a23 a23 = this.p;
                if (a23 != null) {
                    recyclerViewPager3.setAdapter(a23);
                    RecyclerViewPager recyclerViewPager4 = this.m;
                    if (recyclerViewPager4 != null) {
                        recyclerViewPager4.a((RecyclerView.q) new b(this));
                        RecyclerViewPager recyclerViewPager5 = this.m;
                        if (recyclerViewPager5 != null) {
                            recyclerViewPager5.i(this.n);
                            CustomPageIndicator customPageIndicator = ad2.r;
                            kd4.a((Object) customPageIndicator, "binding.cpiPreset");
                            this.o = customPageIndicator;
                            ArrayList arrayList = new ArrayList();
                            arrayList.add(new CustomPageIndicator.a(2, R.drawable.current));
                            arrayList.add(new CustomPageIndicator.a(1, 0, 2, (fd4) null));
                            arrayList.add(new CustomPageIndicator.a(0, R.drawable.add));
                            CustomPageIndicator customPageIndicator2 = this.o;
                            if (customPageIndicator2 != null) {
                                RecyclerViewPager recyclerViewPager6 = this.m;
                                if (recyclerViewPager6 != null) {
                                    customPageIndicator2.a((RecyclerView) recyclerViewPager6, this.n, (List<CustomPageIndicator.a>) arrayList);
                                    ad2.t.setOnClickListener(this);
                                    return;
                                }
                                kd4.d("rvCustomize");
                                throw null;
                            }
                            kd4.d("indicator");
                            throw null;
                        }
                        kd4.d("rvCustomize");
                        throw null;
                    }
                    kd4.d("rvCustomize");
                    throw null;
                }
                kd4.d("mAdapterDiana");
                throw null;
            }
            kd4.d("rvCustomize");
            throw null;
        }
        kd4.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public void b(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2) {
        kd4.b(list, "views");
        kd4.b(list2, "customizeWidgetViews");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onEditThemeClick preset=");
        sb.append(f13 != null ? f13.a() : null);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        c23 c23 = this.k;
        if (c23 != null) {
            c23.a(f13, list, list2);
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void d(boolean z) {
        if (z) {
            tr3<ad2> tr3 = this.q;
            if (tr3 != null) {
                ad2 a2 = tr3.a();
                if (a2 != null) {
                    TextView textView = a2.w;
                    kd4.a((Object) textView, "tvTapIconToCustomize");
                    textView.setVisibility(0);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        tr3<ad2> tr32 = this.q;
        if (tr32 != null) {
            ad2 a3 = tr32.a();
            if (a3 != null) {
                TextView textView2 = a3.w;
                kd4.a((Object) textView2, "tvTapIconToCustomize");
                textView2.setVisibility(4);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(boolean z, String str, String str2, String str3) {
        kd4.b(str, "currentPresetName");
        kd4.b(str2, "nextPresetName");
        kd4.b(str3, "nextPresetId");
        isActive();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String string = activity.getString(R.string.Customization_Delete_Confirm_Text__DeletingAPresetIsPermanentAnd);
            kd4.a((Object) string, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
            if (z) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    String string2 = activity2.getString(R.string.Customization_Delete_CurrentPreset_Text__DeletingAPresetIsPermanentAnd);
                    kd4.a((Object) string2, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
                    pd4 pd4 = pd4.a;
                    Object[] objArr = {qf4.d(str2)};
                    string = String.format(string2, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) string, "java.lang.String.format(format, *args)");
                } else {
                    kd4.a();
                    throw null;
                }
            }
            Bundle bundle = new Bundle();
            bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
            pd4 pd42 = pd4.a;
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_CurrentPreset_Title__DeletePresetName);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
            Object[] objArr2 = {str};
            String format = String.format(a2, Arrays.copyOf(objArr2, objArr2.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            fVar.a((int) R.id.tv_title, format);
            fVar.a((int) R.id.tv_description, string);
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_Confirm_CTA__Delete));
            fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_Confirm_CTA__Cancel));
            fVar.a((int) R.id.tv_ok);
            fVar.a((int) R.id.tv_cancel);
            fVar.a(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void c(List<f13> list) {
        kd4.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showPresets - data=" + list.size());
        a23 a23 = this.p;
        if (a23 != null) {
            a23.a(list);
            RecyclerViewPager recyclerViewPager = this.m;
            if (recyclerViewPager != null) {
                recyclerViewPager.i(this.n);
            } else {
                kd4.d("rvCustomize");
                throw null;
            }
        } else {
            kd4.d("mAdapterDiana");
            throw null;
        }
    }

    @DexIgnore
    public void c(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showDeleteSuccessfully - position=" + i);
        this.n = i;
        a23 a23 = this.p;
        if (a23 != null) {
            if (a23.getItemCount() > i) {
                RecyclerViewPager recyclerViewPager = this.m;
                if (recyclerViewPager != null) {
                    recyclerViewPager.i(i);
                } else {
                    kd4.d("rvCustomize");
                    throw null;
                }
            }
            CustomPageIndicator customPageIndicator = this.o;
            if (customPageIndicator != null) {
                customPageIndicator.invalidate();
            } else {
                kd4.d("indicator");
                throw null;
            }
        } else {
            kd4.d("mAdapterDiana");
            throw null;
        }
    }

    @DexIgnore
    public void a(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2) {
        kd4.b(list, "views");
        kd4.b(list2, "customizeWidgetViews");
        if (f13 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                String c2 = f13.c();
                ArrayList arrayList = new ArrayList(list);
                c23 c23 = this.k;
                if (c23 != null) {
                    aVar.a(activity, c2, arrayList, list2, c23.i(), 0, ViewHierarchy.DIMENSION_TOP_KEY, ViewHierarchy.DIMENSION_TOP_KEY);
                    return;
                }
                kd4.d("mPresenter");
                throw null;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2, String str, int i) {
        List<? extends f8<View, String>> list3 = list;
        String str2 = str;
        kd4.b(list, "views");
        List<? extends f8<CustomizeWidget, String>> list4 = list2;
        kd4.b(list2, "customizeWidgetViews");
        kd4.b(str2, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(f13 != null ? f13.a() : null);
        sb.append(" watchAppPos=");
        sb.append(str2);
        sb.append(" position=");
        sb.append(i);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (f13 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                String c2 = f13.c();
                ArrayList arrayList = new ArrayList(list);
                c23 c23 = this.k;
                if (c23 != null) {
                    aVar.a(activity, c2, arrayList, list2, c23.i(), 2, ViewHierarchy.DIMENSION_TOP_KEY, str);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        String str2;
        kd4.b(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i == R.id.tv_ok) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                kd4.a((Object) str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
            } else {
                str2 = "";
            }
            c23 c23 = this.k;
            if (c23 != null) {
                c23.a(str2);
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (z) {
            ConstraintLayout constraintLayout = this.l;
            if (constraintLayout != null) {
                constraintLayout.setVisibility(0);
            } else {
                kd4.d("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout2 = this.l;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(8);
            } else {
                kd4.d("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(c23 c23) {
        kd4.b(c23, "presenter");
        this.k = c23;
    }
}
