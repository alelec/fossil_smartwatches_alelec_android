package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ra */
public class C2794ra extends com.fossil.blesdk.obfuscated.C2469na {

    @DexIgnore
    /* renamed from: a */
    public java.util.Set<java.lang.Class<? extends com.fossil.blesdk.obfuscated.C2469na>> f8889a; // = new java.util.HashSet();

    @DexIgnore
    /* renamed from: b */
    public java.util.List<com.fossil.blesdk.obfuscated.C2469na> f8890b; // = new java.util.concurrent.CopyOnWriteArrayList();

    @DexIgnore
    /* renamed from: c */
    public java.util.List<java.lang.String> f8891c; // = new java.util.concurrent.CopyOnWriteArrayList();

    @DexIgnore
    /* renamed from: a */
    public void mo15468a(com.fossil.blesdk.obfuscated.C2469na naVar) {
        if (this.f8889a.add(naVar.getClass())) {
            this.f8890b.add(naVar);
            for (com.fossil.blesdk.obfuscated.C2469na a : naVar.mo13887a()) {
                mo15468a(a);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo15469b() {
        boolean z = false;
        for (java.lang.String next : this.f8891c) {
            try {
                java.lang.Class<?> cls = java.lang.Class.forName(next);
                if (com.fossil.blesdk.obfuscated.C2469na.class.isAssignableFrom(cls)) {
                    mo15468a((com.fossil.blesdk.obfuscated.C2469na) cls.newInstance());
                    this.f8891c.remove(next);
                    z = true;
                }
            } catch (java.lang.ClassNotFoundException unused) {
            } catch (java.lang.IllegalAccessException e) {
                android.util.Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e);
            } catch (java.lang.InstantiationException e2) {
                android.util.Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + next, e2);
            }
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.databinding.ViewDataBinding mo13885a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View view, int i) {
        for (com.fossil.blesdk.obfuscated.C2469na a : this.f8890b) {
            androidx.databinding.ViewDataBinding a2 = a.mo13885a(paVar, view, i);
            if (a2 != null) {
                return a2;
            }
        }
        if (mo15469b()) {
            return mo13885a(paVar, view, i);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.databinding.ViewDataBinding mo13886a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View[] viewArr, int i) {
        for (com.fossil.blesdk.obfuscated.C2469na a : this.f8890b) {
            androidx.databinding.ViewDataBinding a2 = a.mo13886a(paVar, viewArr, i);
            if (a2 != null) {
                return a2;
            }
        }
        if (mo15469b()) {
            return mo13886a(paVar, viewArr, i);
        }
        return null;
    }
}
