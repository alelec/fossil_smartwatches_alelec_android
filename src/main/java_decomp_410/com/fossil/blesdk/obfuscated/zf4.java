package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class zf4 {
    @DexIgnore
    public static /* synthetic */ Object a(CoroutineContext coroutineContext, yc4 yc4, int i, Object obj) throws InterruptedException {
        if ((i & 1) != 0) {
            coroutineContext = EmptyCoroutineContext.INSTANCE;
        }
        return yf4.a(coroutineContext, yc4);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003b, code lost:
        if (r1 != null) goto L_0x0044;
     */
    @DexIgnore
    public static final <T> T a(CoroutineContext coroutineContext, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4) throws InterruptedException {
        CoroutineContext coroutineContext2;
        sh4 sh4;
        kd4.b(coroutineContext, "context");
        kd4.b(yc4, "block");
        Thread currentThread = Thread.currentThread();
        zb4 zb4 = (zb4) coroutineContext.get(zb4.b);
        if (zb4 == null) {
            sh4 = bj4.b.b();
            coroutineContext2 = tg4.a(yh4.e, coroutineContext.plus(sh4));
        } else {
            if (!(zb4 instanceof sh4)) {
                zb4 = null;
            }
            sh4 = (sh4) zb4;
            if (sh4 != null) {
                if (!sh4.H()) {
                    sh4 = null;
                }
            }
            sh4 = bj4.b.a();
            coroutineContext2 = tg4.a(yh4.e, coroutineContext);
        }
        kd4.a((Object) currentThread, "currentThread");
        wf4 wf4 = new wf4(coroutineContext2, currentThread, sh4);
        wf4.a(CoroutineStart.DEFAULT, wf4, yc4);
        return wf4.m();
    }
}
