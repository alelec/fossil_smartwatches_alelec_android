package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w53 implements Factory<WatchAppSearchPresenter> {
    @DexIgnore
    public static WatchAppSearchPresenter a(s53 s53, WatchAppRepository watchAppRepository, en2 en2) {
        return new WatchAppSearchPresenter(s53, watchAppRepository, en2);
    }
}
