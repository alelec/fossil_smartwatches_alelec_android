package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zn3 implements Factory<ArrayList<Integer>> {
    @DexIgnore
    public static ArrayList<Integer> a(yn3 yn3) {
        ArrayList<Integer> a = yn3.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
