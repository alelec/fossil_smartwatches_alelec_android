package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bk4 {
    @DexIgnore
    public static final Throwable a(uf4<?> uf4, Throwable th) {
        kd4.b(uf4, "$this$tryRecover");
        kd4.b(th, "exception");
        if (!(uf4 instanceof ak4)) {
            uf4 = null;
        }
        ak4 ak4 = (ak4) uf4;
        if (ak4 != null) {
            yb4<T> yb4 = ak4.h;
            if (yb4 != null) {
                return ck4.a(th, (yb4<?>) yb4);
            }
        }
        return th;
    }
}
