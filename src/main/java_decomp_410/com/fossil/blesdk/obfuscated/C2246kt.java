package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kt */
public class C2246kt implements com.fossil.blesdk.obfuscated.C1438aq<byte[]> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ byte[] f7006e;

    @DexIgnore
    public C2246kt(byte[] bArr) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(bArr);
        this.f7006e = bArr;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8887a() {
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return this.f7006e.length;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<byte[]> mo8889c() {
        return byte[].class;
    }

    @DexIgnore
    public byte[] get() {
        return this.f7006e;
    }
}
