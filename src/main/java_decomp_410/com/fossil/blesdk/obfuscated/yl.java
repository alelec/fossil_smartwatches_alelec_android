package com.fossil.blesdk.obfuscated;

import androidx.work.impl.utils.futures.AbstractFuture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yl<V> extends AbstractFuture<V> {
    @DexIgnore
    public static <V> yl<V> e() {
        return new yl<>();
    }

    @DexIgnore
    public boolean a(Throwable th) {
        return super.a(th);
    }

    @DexIgnore
    public boolean b(V v) {
        return super.b(v);
    }

    @DexIgnore
    public boolean a(zv1<? extends V> zv1) {
        return super.a(zv1);
    }
}
