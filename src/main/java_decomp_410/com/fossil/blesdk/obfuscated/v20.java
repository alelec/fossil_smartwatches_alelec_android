package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v20 extends s20<byte[], byte[]> {
    @DexIgnore
    public static /* final */ k20<byte[]>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ l20<byte[]>[] b; // = new l20[0];
    @DexIgnore
    public static /* final */ v20 c; // = new v20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends q20<byte[]> {
        @DexIgnore
        public /* bridge */ /* synthetic */ byte[] a(Object obj) {
            byte[] bArr = (byte[]) obj;
            a(bArr);
            return bArr;
        }

        @DexIgnore
        public byte[] a(byte[] bArr) {
            kd4.b(bArr, "entries");
            byte[] unused = v20.c.b(bArr);
            return bArr;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends r20<byte[]> {
        @DexIgnore
        public /* bridge */ /* synthetic */ byte[] a(Object obj) {
            byte[] bArr = (byte[]) obj;
            a(bArr);
            return bArr;
        }

        @DexIgnore
        public byte[] a(byte[] bArr) {
            kd4.b(bArr, "entries");
            byte[] unused = v20.c.b(bArr);
            return bArr;
        }
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) {
        return bArr;
    }

    @DexIgnore
    public l20<byte[]>[] b() {
        return b;
    }

    @DexIgnore
    public k20<byte[]>[] a() {
        return a;
    }
}
