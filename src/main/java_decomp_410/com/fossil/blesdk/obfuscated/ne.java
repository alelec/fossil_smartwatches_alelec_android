package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ne implements Runnable {
    @DexIgnore
    public static /* final */ ThreadLocal<ne> i; // = new ThreadLocal<>();
    @DexIgnore
    public static Comparator<c> j; // = new a();
    @DexIgnore
    public ArrayList<RecyclerView> e; // = new ArrayList<>();
    @DexIgnore
    public long f;
    @DexIgnore
    public long g;
    @DexIgnore
    public ArrayList<c> h; // = new ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<c> {
        @DexIgnore
        /* renamed from: a */
        public int compare(c cVar, c cVar2) {
            if ((cVar.d == null) == (cVar2.d == null)) {
                boolean z = cVar.a;
                if (z == cVar2.a) {
                    int i = cVar2.b - cVar.b;
                    if (i != 0) {
                        return i;
                    }
                    int i2 = cVar.c - cVar2.c;
                    if (i2 != 0) {
                        return i2;
                    }
                    return 0;
                } else if (z) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (cVar.d == null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public RecyclerView d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a() {
            this.a = false;
            this.b = 0;
            this.c = 0;
            this.d = null;
            this.e = 0;
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView) {
        this.e.add(recyclerView);
    }

    @DexIgnore
    public void b(RecyclerView recyclerView) {
        this.e.remove(recyclerView);
    }

    @DexIgnore
    public void run() {
        try {
            s7.a("RV Prefetch");
            if (!this.e.isEmpty()) {
                int size = this.e.size();
                long j2 = 0;
                for (int i2 = 0; i2 < size; i2++) {
                    RecyclerView recyclerView = this.e.get(i2);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j2 = Math.max(recyclerView.getDrawingTime(), j2);
                    }
                }
                if (j2 != 0) {
                    b(TimeUnit.MILLISECONDS.toNanos(j2) + this.g);
                    this.f = 0;
                    s7.a();
                }
            }
        } finally {
            this.f = 0;
            s7.a();
        }
    }

    @DexIgnore
    public void a(RecyclerView recyclerView, int i2, int i3) {
        if (recyclerView.isAttachedToWindow() && this.f == 0) {
            this.f = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.k0.b(i2, i3);
    }

    @DexIgnore
    public void b(long j2) {
        a();
        a(j2);
    }

    @DexIgnore
    public final void a() {
        c cVar;
        int size = this.e.size();
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            RecyclerView recyclerView = this.e.get(i3);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.k0.a(recyclerView, false);
                i2 += recyclerView.k0.d;
            }
        }
        this.h.ensureCapacity(i2);
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            RecyclerView recyclerView2 = this.e.get(i5);
            if (recyclerView2.getWindowVisibility() == 0) {
                b bVar = recyclerView2.k0;
                int abs = Math.abs(bVar.a) + Math.abs(bVar.b);
                int i6 = i4;
                for (int i7 = 0; i7 < bVar.d * 2; i7 += 2) {
                    if (i6 >= this.h.size()) {
                        cVar = new c();
                        this.h.add(cVar);
                    } else {
                        cVar = this.h.get(i6);
                    }
                    int i8 = bVar.c[i7 + 1];
                    cVar.a = i8 <= abs;
                    cVar.b = abs;
                    cVar.c = i8;
                    cVar.d = recyclerView2;
                    cVar.e = bVar.c[i7];
                    i6++;
                }
                i4 = i6;
            }
        }
        Collections.sort(this.h, j);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements RecyclerView.m.c {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int[] c;
        @DexIgnore
        public int d;

        @DexIgnore
        public void a(RecyclerView recyclerView, boolean z) {
            this.d = 0;
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            RecyclerView.m mVar = recyclerView.q;
            if (recyclerView.p != null && mVar != null && mVar.w()) {
                if (z) {
                    if (!recyclerView.h.c()) {
                        mVar.a(recyclerView.p.getItemCount(), (RecyclerView.m.c) this);
                    }
                } else if (!recyclerView.p()) {
                    mVar.a(this.a, this.b, recyclerView.l0, (RecyclerView.m.c) this);
                }
                int i = this.d;
                if (i > mVar.m) {
                    mVar.m = i;
                    mVar.n = z;
                    recyclerView.f.j();
                }
            }
        }

        @DexIgnore
        public void b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public void a(int i, int i2) {
            if (i < 0) {
                throw new IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 >= 0) {
                int i3 = this.d * 2;
                int[] iArr = this.c;
                if (iArr == null) {
                    this.c = new int[4];
                    Arrays.fill(this.c, -1);
                } else if (i3 >= iArr.length) {
                    this.c = new int[(i3 * 2)];
                    System.arraycopy(iArr, 0, this.c, 0, iArr.length);
                }
                int[] iArr2 = this.c;
                iArr2[i3] = i;
                iArr2[i3 + 1] = i2;
                this.d++;
            } else {
                throw new IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        @DexIgnore
        public boolean a(int i) {
            if (this.c != null) {
                int i2 = this.d * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.c[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }

        @DexIgnore
        public void a() {
            int[] iArr = this.c;
            if (iArr != null) {
                Arrays.fill(iArr, -1);
            }
            this.d = 0;
        }
    }

    @DexIgnore
    public static boolean a(RecyclerView recyclerView, int i2) {
        int b2 = recyclerView.i.b();
        for (int i3 = 0; i3 < b2; i3++) {
            RecyclerView.ViewHolder n = RecyclerView.n(recyclerView.i.e(i3));
            if (n.mPosition == i2 && !n.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final RecyclerView.ViewHolder a(RecyclerView recyclerView, int i2, long j2) {
        if (a(recyclerView, i2)) {
            return null;
        }
        RecyclerView.Recycler recycler = recyclerView.f;
        try {
            recyclerView.z();
            RecyclerView.ViewHolder a2 = recycler.a(i2, false, j2);
            if (a2 != null) {
                if (!a2.isBound() || a2.isInvalid()) {
                    recycler.a(a2, false);
                } else {
                    recycler.b(a2.itemView);
                }
            }
            return a2;
        } finally {
            recyclerView.a(false);
        }
    }

    @DexIgnore
    public final void a(RecyclerView recyclerView, long j2) {
        if (recyclerView != null) {
            if (recyclerView.H && recyclerView.i.b() != 0) {
                recyclerView.G();
            }
            b bVar = recyclerView.k0;
            bVar.a(recyclerView, true);
            if (bVar.d != 0) {
                try {
                    s7.a("RV Nested Prefetch");
                    recyclerView.l0.a(recyclerView.p);
                    for (int i2 = 0; i2 < bVar.d * 2; i2 += 2) {
                        a(recyclerView, bVar.c[i2], j2);
                    }
                } finally {
                    s7.a();
                }
            }
        }
    }

    @DexIgnore
    public final void a(c cVar, long j2) {
        RecyclerView.ViewHolder a2 = a(cVar.d, cVar.e, cVar.a ? ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD : j2);
        if (a2 != null && a2.mNestedRecyclerView != null && a2.isBound() && !a2.isInvalid()) {
            a((RecyclerView) a2.mNestedRecyclerView.get(), j2);
        }
    }

    @DexIgnore
    public final void a(long j2) {
        int i2 = 0;
        while (i2 < this.h.size()) {
            c cVar = this.h.get(i2);
            if (cVar.d != null) {
                a(cVar, j2);
                cVar.a();
                i2++;
            } else {
                return;
            }
        }
    }
}
