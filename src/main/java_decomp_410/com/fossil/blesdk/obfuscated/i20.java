package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i20 {
    @DexIgnore
    public static final JSONArray a(DeviceConfigKey[] deviceConfigKeyArr) {
        kd4.b(deviceConfigKeyArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (DeviceConfigKey logName$blesdk_productionRelease : deviceConfigKeyArr) {
            jSONArray.put(logName$blesdk_productionRelease.getLogName$blesdk_productionRelease());
        }
        return jSONArray;
    }
}
