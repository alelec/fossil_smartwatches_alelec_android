package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rm extends Thread {
    @DexIgnore
    public /* final */ BlockingQueue<Request<?>> e;
    @DexIgnore
    public /* final */ qm f;
    @DexIgnore
    public /* final */ lm g;
    @DexIgnore
    public /* final */ vm h;
    @DexIgnore
    public volatile boolean i; // = false;

    @DexIgnore
    public rm(BlockingQueue<Request<?>> blockingQueue, qm qmVar, lm lmVar, vm vmVar) {
        this.e = blockingQueue;
        this.f = qmVar;
        this.g = lmVar;
        this.h = vmVar;
    }

    @DexIgnore
    @TargetApi(14)
    public final void a(Request<?> request) {
        if (Build.VERSION.SDK_INT >= 14) {
            TrafficStats.setThreadStatsTag(request.getTrafficStatsTag());
        }
    }

    @DexIgnore
    public void b() {
        this.i = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                a();
            } catch (InterruptedException unused) {
                if (this.i) {
                    Thread.currentThread().interrupt();
                    return;
                }
                xm.c("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }

    @DexIgnore
    public final void a() throws InterruptedException {
        b(this.e.take());
    }

    @DexIgnore
    public void b(Request<?> request) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            request.addMarker("network-queue-take");
            if (request.isCanceled()) {
                request.finish("network-discard-cancelled");
                request.notifyListenerResponseNotUsable();
                return;
            }
            a(request);
            sm a = this.f.a(request);
            request.addMarker("network-http-complete");
            if (!a.e || !request.hasHadResponseDelivered()) {
                um<?> parseNetworkResponse = request.parseNetworkResponse(a);
                request.addMarker("network-parse-complete");
                if (request.shouldCache() && parseNetworkResponse.b != null) {
                    this.g.a(request.getCacheKey(), parseNetworkResponse.b);
                    request.addMarker("network-cache-written");
                }
                request.markDelivered();
                this.h.a(request, parseNetworkResponse);
                request.notifyListenerResponseReceived(parseNetworkResponse);
                return;
            }
            request.finish("not-modified");
            request.notifyListenerResponseNotUsable();
        } catch (VolleyError e2) {
            e2.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            a(request, e2);
            request.notifyListenerResponseNotUsable();
        } catch (Exception e3) {
            xm.a(e3, "Unhandled exception %s", e3.toString());
            VolleyError volleyError = new VolleyError((Throwable) e3);
            volleyError.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.h.a(request, volleyError);
            request.notifyListenerResponseNotUsable();
        }
    }

    @DexIgnore
    public final void a(Request<?> request, VolleyError volleyError) {
        this.h.a(request, request.parseNetworkError(volleyError));
    }
}
