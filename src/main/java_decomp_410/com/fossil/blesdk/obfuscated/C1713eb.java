package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.eb */
public abstract class C1713eb {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eb$a")
    /* renamed from: com.fossil.blesdk.obfuscated.eb$a */
    public class C1714a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ int f4752e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.util.ArrayList f4753f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ java.util.ArrayList f4754g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ java.util.ArrayList f4755h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ java.util.ArrayList f4756i;

        @DexIgnore
        public C1714a(com.fossil.blesdk.obfuscated.C1713eb ebVar, int i, java.util.ArrayList arrayList, java.util.ArrayList arrayList2, java.util.ArrayList arrayList3, java.util.ArrayList arrayList4) {
            this.f4752e = i;
            this.f4753f = arrayList;
            this.f4754g = arrayList2;
            this.f4755h = arrayList3;
            this.f4756i = arrayList4;
        }

        @DexIgnore
        public void run() {
            for (int i = 0; i < this.f4752e; i++) {
                com.fossil.blesdk.obfuscated.C1776f9.m6818a((android.view.View) this.f4753f.get(i), (java.lang.String) this.f4754g.get(i));
                com.fossil.blesdk.obfuscated.C1776f9.m6818a((android.view.View) this.f4755h.get(i), (java.lang.String) this.f4756i.get(i));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eb$b")
    /* renamed from: com.fossil.blesdk.obfuscated.eb$b */
    public class C1715b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.ArrayList f4757e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.util.Map f4758f;

        @DexIgnore
        public C1715b(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.util.ArrayList arrayList, java.util.Map map) {
            this.f4757e = arrayList;
            this.f4758f = map;
        }

        @DexIgnore
        public void run() {
            int size = this.f4757e.size();
            for (int i = 0; i < size; i++) {
                android.view.View view = (android.view.View) this.f4757e.get(i);
                java.lang.String q = com.fossil.blesdk.obfuscated.C1776f9.m6851q(view);
                if (q != null) {
                    com.fossil.blesdk.obfuscated.C1776f9.m6818a(view, com.fossil.blesdk.obfuscated.C1713eb.m6361a((java.util.Map<java.lang.String, java.lang.String>) this.f4758f, q));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eb$c")
    /* renamed from: com.fossil.blesdk.obfuscated.eb$c */
    public class C1716c implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.ArrayList f4759e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.util.Map f4760f;

        @DexIgnore
        public C1716c(com.fossil.blesdk.obfuscated.C1713eb ebVar, java.util.ArrayList arrayList, java.util.Map map) {
            this.f4759e = arrayList;
            this.f4760f = map;
        }

        @DexIgnore
        public void run() {
            int size = this.f4759e.size();
            for (int i = 0; i < size; i++) {
                android.view.View view = (android.view.View) this.f4759e.get(i);
                com.fossil.blesdk.obfuscated.C1776f9.m6818a(view, (java.lang.String) this.f4760f.get(com.fossil.blesdk.obfuscated.C1776f9.m6851q(view)));
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract java.lang.Object mo9859a(java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3);

    @DexIgnore
    /* renamed from: a */
    public void mo10405a(android.view.View view, android.graphics.Rect rect) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        rect.set(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight());
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9860a(android.view.ViewGroup viewGroup, java.lang.Object obj);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9861a(java.lang.Object obj, android.graphics.Rect rect);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9862a(java.lang.Object obj, android.view.View view);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9863a(java.lang.Object obj, android.view.View view, java.util.ArrayList<android.view.View> arrayList);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9864a(java.lang.Object obj, java.lang.Object obj2, java.util.ArrayList<android.view.View> arrayList, java.lang.Object obj3, java.util.ArrayList<android.view.View> arrayList2, java.lang.Object obj4, java.util.ArrayList<android.view.View> arrayList3);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9865a(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9866a(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo9867a(java.lang.Object obj);

    @DexIgnore
    /* renamed from: b */
    public abstract java.lang.Object mo9868b(java.lang.Object obj);

    @DexIgnore
    /* renamed from: b */
    public abstract java.lang.Object mo9869b(java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3);

    @DexIgnore
    /* renamed from: b */
    public abstract void mo9870b(java.lang.Object obj, android.view.View view);

    @DexIgnore
    /* renamed from: b */
    public abstract void mo9871b(java.lang.Object obj, android.view.View view, java.util.ArrayList<android.view.View> arrayList);

    @DexIgnore
    /* renamed from: b */
    public abstract void mo9872b(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2);

    @DexIgnore
    /* renamed from: c */
    public abstract java.lang.Object mo9873c(java.lang.Object obj);

    @DexIgnore
    /* renamed from: c */
    public abstract void mo9874c(java.lang.Object obj, android.view.View view);

    @DexIgnore
    /* renamed from: a */
    public java.util.ArrayList<java.lang.String> mo10404a(java.util.ArrayList<android.view.View> arrayList) {
        java.util.ArrayList<java.lang.String> arrayList2 = new java.util.ArrayList<>();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            android.view.View view = arrayList.get(i);
            arrayList2.add(com.fossil.blesdk.obfuscated.C1776f9.m6851q(view));
            com.fossil.blesdk.obfuscated.C1776f9.m6818a(view, (java.lang.String) null);
        }
        return arrayList2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10406a(android.view.View view, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2, java.util.ArrayList<java.lang.String> arrayList3, java.util.Map<java.lang.String, java.lang.String> map) {
        int size = arrayList2.size();
        java.util.ArrayList arrayList4 = new java.util.ArrayList();
        for (int i = 0; i < size; i++) {
            android.view.View view2 = arrayList.get(i);
            java.lang.String q = com.fossil.blesdk.obfuscated.C1776f9.m6851q(view2);
            arrayList4.add(q);
            if (q != null) {
                com.fossil.blesdk.obfuscated.C1776f9.m6818a(view2, (java.lang.String) null);
                java.lang.String str = map.get(q);
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    } else if (str.equals(arrayList3.get(i2))) {
                        com.fossil.blesdk.obfuscated.C1776f9.m6818a(arrayList2.get(i2), q);
                        break;
                    } else {
                        i2++;
                    }
                }
            }
        }
        com.fossil.blesdk.obfuscated.C1713eb.C1714a aVar = new com.fossil.blesdk.obfuscated.C1713eb.C1714a(this, size, arrayList2, arrayList3, arrayList, arrayList4);
        com.fossil.blesdk.obfuscated.C1865gb.m7335a(view, aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10409a(java.util.ArrayList<android.view.View> arrayList, android.view.View view) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof android.view.ViewGroup) {
            android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
            if (com.fossil.blesdk.obfuscated.C1930h9.m7829a(viewGroup)) {
                arrayList.add(viewGroup);
                return;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                mo10409a(arrayList, viewGroup.getChildAt(i));
            }
            return;
        }
        arrayList.add(view);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10410a(java.util.Map<java.lang.String, android.view.View> map, android.view.View view) {
        if (view.getVisibility() == 0) {
            java.lang.String q = com.fossil.blesdk.obfuscated.C1776f9.m6851q(view);
            if (q != null) {
                map.put(q, view);
            }
            if (view instanceof android.view.ViewGroup) {
                android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    mo10410a(map, viewGroup.getChildAt(i));
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10407a(android.view.View view, java.util.ArrayList<android.view.View> arrayList, java.util.Map<java.lang.String, java.lang.String> map) {
        com.fossil.blesdk.obfuscated.C1865gb.m7335a(view, new com.fossil.blesdk.obfuscated.C1713eb.C1715b(this, arrayList, map));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10408a(android.view.ViewGroup viewGroup, java.util.ArrayList<android.view.View> arrayList, java.util.Map<java.lang.String, java.lang.String> map) {
        com.fossil.blesdk.obfuscated.C1865gb.m7335a(viewGroup, new com.fossil.blesdk.obfuscated.C1713eb.C1716c(this, arrayList, map));
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6362a(java.util.List<android.view.View> list, android.view.View view) {
        int size = list.size();
        if (!m6364a(list, view, size)) {
            list.add(view);
            for (int i = size; i < list.size(); i++) {
                android.view.View view2 = list.get(i);
                if (view2 instanceof android.view.ViewGroup) {
                    android.view.ViewGroup viewGroup = (android.view.ViewGroup) view2;
                    int childCount = viewGroup.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        android.view.View childAt = viewGroup.getChildAt(i2);
                        if (!m6364a(list, childAt, size)) {
                            list.add(childAt);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6364a(java.util.List<android.view.View> list, android.view.View view, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (list.get(i2) == view) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6363a(java.util.List list) {
        return list == null || list.isEmpty();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m6361a(java.util.Map<java.lang.String, java.lang.String> map, java.lang.String str) {
        for (java.util.Map.Entry next : map.entrySet()) {
            if (str.equals(next.getValue())) {
                return (java.lang.String) next.getKey();
            }
        }
        return null;
    }
}
