package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzuv;
import com.google.android.gms.internal.measurement.zzxp;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mb1 {
    @DexIgnore
    public static /* final */ ob1 a;

    /*
    static {
        ob1 ob1;
        if (!(kb1.b() && kb1.c()) || n71.a()) {
            ob1 = new pb1();
        } else {
            ob1 = new qb1();
        }
        a = ob1;
    }
    */

    @DexIgnore
    public static int a(int i) {
        if (i > -12) {
            return -1;
        }
        return i;
    }

    @DexIgnore
    public static int a(int i, int i2) {
        if (i > -12 || i2 > -65) {
            return -1;
        }
        return i ^ (i2 << 8);
    }

    @DexIgnore
    public static int a(int i, int i2, int i3) {
        if (i > -12 || i2 > -65 || i3 > -65) {
            return -1;
        }
        return (i ^ (i2 << 8)) ^ (i3 << 16);
    }

    @DexIgnore
    public static boolean a(byte[] bArr) {
        return a.a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public static int b(byte[] bArr, int i, int i2) {
        byte b = bArr[i - 1];
        int i3 = i2 - i;
        if (i3 == 0) {
            return a((int) b);
        }
        if (i3 == 1) {
            return a((int) b, (int) bArr[i]);
        }
        if (i3 == 2) {
            return a((int) b, (int) bArr[i], (int) bArr[i + 1]);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static String c(byte[] bArr, int i, int i2) throws zzuv {
        return a.b(bArr, i, i2);
    }

    @DexIgnore
    public static boolean a(byte[] bArr, int i, int i2) {
        return a.a(bArr, i, i2);
    }

    @DexIgnore
    public static int a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        int i2 = 0;
        while (i2 < length && charSequence.charAt(i2) < 128) {
            i2++;
        }
        int i3 = length;
        while (true) {
            if (i2 >= length) {
                break;
            }
            char charAt = charSequence.charAt(i2);
            if (charAt < 2048) {
                i3 += (127 - charAt) >>> 31;
                i2++;
            } else {
                int length2 = charSequence.length();
                while (i2 < length2) {
                    char charAt2 = charSequence.charAt(i2);
                    if (charAt2 < 2048) {
                        i += (127 - charAt2) >>> 31;
                    } else {
                        i += 2;
                        if (55296 <= charAt2 && charAt2 <= 57343) {
                            if (Character.codePointAt(charSequence, i2) >= 65536) {
                                i2++;
                            } else {
                                throw new zzxp(i2, length2);
                            }
                        }
                    }
                    i2++;
                }
                i3 += i;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("UTF-8 length does not fit in int: ");
        sb.append(((long) i3) + 4294967296L);
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public static int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        return a.a(charSequence, bArr, i, i2);
    }

    @DexIgnore
    public static void a(CharSequence charSequence, ByteBuffer byteBuffer) {
        ob1 ob1 = a;
        if (byteBuffer.hasArray()) {
            int arrayOffset = byteBuffer.arrayOffset();
            byteBuffer.position(a(charSequence, byteBuffer.array(), byteBuffer.position() + arrayOffset, byteBuffer.remaining()) - arrayOffset);
        } else if (byteBuffer.isDirect()) {
            ob1.a(charSequence, byteBuffer);
        } else {
            ob1.b(charSequence, byteBuffer);
        }
    }
}
