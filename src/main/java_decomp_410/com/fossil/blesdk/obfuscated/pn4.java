package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jn4;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import okhttp3.internal.http2.ErrorCode;
import okhttp3.internal.http2.StreamResetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pn4 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ nn4 d;
    @DexIgnore
    public /* final */ Deque<yl4> e; // = new ArrayDeque();
    @DexIgnore
    public jn4.a f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ b h;
    @DexIgnore
    public /* final */ a i;
    @DexIgnore
    public /* final */ c j; // = new c();
    @DexIgnore
    public /* final */ c k; // = new c();
    @DexIgnore
    public ErrorCode l; // = null;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements yo4 {
        @DexIgnore
        public /* final */ jo4 e; // = new jo4();
        @DexIgnore
        public /* final */ jo4 f; // = new jo4();
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public b(long j2) {
            this.g = j2;
        }

        @DexIgnore
        public final void a(long j2) {
            pn4.this.d.h(j2);
        }

        @DexIgnore
        public long b(jo4 jo4, long j2) throws IOException {
            ErrorCode errorCode;
            long j3;
            jn4.a aVar;
            yl4 yl4;
            long j4 = j2;
            if (j4 >= 0) {
                while (true) {
                    synchronized (pn4.this) {
                        pn4.this.j.g();
                        try {
                            errorCode = pn4.this.l != null ? pn4.this.l : null;
                            if (!this.h) {
                                if (pn4.this.e.isEmpty() || pn4.this.f == null) {
                                    if (this.f.B() > 0) {
                                        j3 = this.f.b(jo4, Math.min(j4, this.f.B()));
                                        pn4.this.a += j3;
                                        if (errorCode == null && pn4.this.a >= ((long) (pn4.this.d.r.c() / 2))) {
                                            pn4.this.d.c(pn4.this.c, pn4.this.a);
                                            pn4.this.a = 0;
                                        }
                                    } else {
                                        jo4 jo42 = jo4;
                                        if (this.i || errorCode != null) {
                                            j3 = -1;
                                        } else {
                                            pn4.this.k();
                                        }
                                    }
                                    yl4 = null;
                                    aVar = null;
                                } else {
                                    yl4 = (yl4) pn4.this.e.removeFirst();
                                    aVar = pn4.this.f;
                                    jo4 jo43 = jo4;
                                    j3 = -1;
                                }
                                pn4.this.j.k();
                                if (yl4 != null && aVar != null) {
                                    aVar.a(yl4);
                                }
                            } else {
                                throw new IOException("stream closed");
                            }
                        } finally {
                            pn4.this.j.k();
                        }
                    }
                }
                if (j3 != -1) {
                    a(j3);
                    return j3;
                } else if (errorCode == null) {
                    return -1;
                } else {
                    throw new StreamResetException(errorCode);
                }
            } else {
                throw new IllegalArgumentException("byteCount < 0: " + j4);
            }
        }

        @DexIgnore
        public void close() throws IOException {
            long B;
            ArrayList<yl4> arrayList;
            jn4.a aVar;
            synchronized (pn4.this) {
                this.h = true;
                B = this.f.B();
                this.f.w();
                arrayList = null;
                if (pn4.this.e.isEmpty() || pn4.this.f == null) {
                    aVar = null;
                } else {
                    arrayList = new ArrayList<>(pn4.this.e);
                    pn4.this.e.clear();
                    aVar = pn4.this.f;
                }
                pn4.this.notifyAll();
            }
            if (B > 0) {
                a(B);
            }
            pn4.this.a();
            if (aVar != null) {
                for (yl4 a : arrayList) {
                    aVar.a(a);
                }
            }
        }

        @DexIgnore
        public void a(lo4 lo4, long j2) throws IOException {
            boolean z;
            boolean z2;
            boolean z3;
            while (j2 > 0) {
                synchronized (pn4.this) {
                    z = this.i;
                    z2 = true;
                    z3 = this.f.B() + j2 > this.g;
                }
                if (z3) {
                    lo4.skip(j2);
                    pn4.this.c(ErrorCode.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    lo4.skip(j2);
                    return;
                } else {
                    long b = lo4.b(this.e, j2);
                    if (b != -1) {
                        j2 -= b;
                        synchronized (pn4.this) {
                            if (this.f.B() != 0) {
                                z2 = false;
                            }
                            this.f.a((yo4) this.e);
                            if (z2) {
                                pn4.this.notifyAll();
                            }
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        @DexIgnore
        public zo4 b() {
            return pn4.this.j;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ho4 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        public void i() {
            pn4.this.c(ErrorCode.CANCEL);
        }

        @DexIgnore
        public void k() throws IOException {
            if (h()) {
                throw b((IOException) null);
            }
        }
    }

    @DexIgnore
    public pn4(int i2, nn4 nn4, boolean z, boolean z2, yl4 yl4) {
        if (nn4 != null) {
            this.c = i2;
            this.d = nn4;
            this.b = (long) nn4.s.c();
            this.h = new b((long) nn4.r.c());
            this.i = new a();
            this.h.i = z2;
            this.i.g = z;
            if (yl4 != null) {
                this.e.add(yl4);
            }
            if (f() && yl4 != null) {
                throw new IllegalStateException("locally-initiated streams shouldn't have headers yet");
            } else if (!f() && yl4 == null) {
                throw new IllegalStateException("remotely-initiated streams should have headers");
            }
        } else {
            throw new NullPointerException("connection == null");
        }
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public xo4 d() {
        synchronized (this) {
            if (!this.g) {
                if (!f()) {
                    throw new IllegalStateException("reply before requesting the sink");
                }
            }
        }
        return this.i;
    }

    @DexIgnore
    public yo4 e() {
        return this.h;
    }

    @DexIgnore
    public boolean f() {
        if (this.d.e == ((this.c & 1) == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public synchronized boolean g() {
        if (this.l != null) {
            return false;
        }
        if ((this.h.i || this.h.h) && ((this.i.g || this.i.f) && this.g)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public zo4 h() {
        return this.j;
    }

    @DexIgnore
    public void i() {
        boolean g2;
        synchronized (this) {
            this.h.i = true;
            g2 = g();
            notifyAll();
        }
        if (!g2) {
            this.d.d(this.c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        r2.j.k();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        throw r0;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public synchronized yl4 j() throws IOException {
        this.j.g();
        while (this.e.isEmpty() && this.l == null) {
            k();
        }
        this.j.k();
        if (!this.e.isEmpty()) {
        } else {
            throw new StreamResetException(this.l);
        }
        return this.e.removeFirst();
    }

    @DexIgnore
    public void k() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    @DexIgnore
    public zo4 l() {
        return this.k;
    }

    @DexIgnore
    public void a(ErrorCode errorCode) throws IOException {
        if (b(errorCode)) {
            this.d.b(this.c, errorCode);
        }
    }

    @DexIgnore
    public final boolean b(ErrorCode errorCode) {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            if (this.h.i && this.i.g) {
                return false;
            }
            this.l = errorCode;
            notifyAll();
            this.d.d(this.c);
            return true;
        }
    }

    @DexIgnore
    public void c(ErrorCode errorCode) {
        if (b(errorCode)) {
            this.d.c(this.c, errorCode);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements xo4 {
        @DexIgnore
        public /* final */ jo4 e; // = new jo4();
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            this.e.a(jo4, j);
            while (this.e.B() >= 16384) {
                a(false);
            }
        }

        @DexIgnore
        public zo4 b() {
            return pn4.this.k;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
            if (r8.e.B() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
            if (r8.e.B() <= 0) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
            r0 = r8.h;
            r0.d.a(r0.c, true, (com.fossil.blesdk.obfuscated.jo4) null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x003a, code lost:
            r2 = r8.h;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x003c, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r8.f = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003f, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0040, code lost:
            r8.h.d.flush();
            r8.h.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r8.h.i.g != false) goto L_0x003a;
         */
        @DexIgnore
        public void close() throws IOException {
            synchronized (pn4.this) {
                if (this.f) {
                }
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            synchronized (pn4.this) {
                pn4.this.b();
            }
            while (this.e.B() > 0) {
                a(false);
                pn4.this.d.flush();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void a(boolean z) throws IOException {
            long min;
            synchronized (pn4.this) {
                pn4.this.k.g();
                while (pn4.this.b <= 0 && !this.g && !this.f && pn4.this.l == null) {
                    try {
                        pn4.this.k();
                    } catch (Throwable th) {
                        pn4.this.k.k();
                        throw th;
                    }
                }
                pn4.this.k.k();
                pn4.this.b();
                min = Math.min(pn4.this.b, this.e.B());
                pn4.this.b -= min;
            }
            pn4.this.k.g();
            try {
                pn4.this.d.a(pn4.this.c, z && min == this.e.B(), this.e, min);
            } finally {
                pn4.this.k.k();
            }
        }
    }

    @DexIgnore
    public void a(List<jn4> list) {
        boolean g2;
        synchronized (this) {
            this.g = true;
            this.e.add(jm4.b(list));
            g2 = g();
            notifyAll();
        }
        if (!g2) {
            this.d.d(this.c);
        }
    }

    @DexIgnore
    public synchronized void d(ErrorCode errorCode) {
        if (this.l == null) {
            this.l = errorCode;
            notifyAll();
        }
    }

    @DexIgnore
    public void a(lo4 lo4, int i2) throws IOException {
        this.h.a(lo4, (long) i2);
    }

    @DexIgnore
    public void b() throws IOException {
        a aVar = this.i;
        if (aVar.f) {
            throw new IOException("stream closed");
        } else if (!aVar.g) {
            ErrorCode errorCode = this.l;
            if (errorCode != null) {
                throw new StreamResetException(errorCode);
            }
        } else {
            throw new IOException("stream finished");
        }
    }

    @DexIgnore
    public void a() throws IOException {
        boolean z;
        boolean g2;
        synchronized (this) {
            z = !this.h.i && this.h.h && (this.i.g || this.i.f);
            g2 = g();
        }
        if (z) {
            a(ErrorCode.CANCEL);
        } else if (!g2) {
            this.d.d(this.c);
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }
}
