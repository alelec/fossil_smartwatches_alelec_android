package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x11 implements Parcelable.Creator<w11> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ArrayList<DataType> arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            if (SafeParcelReader.a(a) != 1) {
                SafeParcelReader.v(parcel, a);
            } else {
                arrayList = SafeParcelReader.c(parcel, a, DataType.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new w11(arrayList);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new w11[i];
    }
}
