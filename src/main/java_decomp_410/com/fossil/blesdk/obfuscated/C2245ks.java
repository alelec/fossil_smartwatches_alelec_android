package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ks */
public final class C2245ks<T> implements com.fossil.blesdk.obfuscated.C2581oo<T> {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2581oo<?> f6956b; // = new com.fossil.blesdk.obfuscated.C2245ks();

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C2245ks<T> m9758a() {
        return (com.fossil.blesdk.obfuscated.C2245ks) f6956b;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<T> mo12877a(android.content.Context context, com.fossil.blesdk.obfuscated.C1438aq<T> aqVar, int i, int i2) {
        return aqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
    }
}
