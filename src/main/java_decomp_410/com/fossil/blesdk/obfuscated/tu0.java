package com.fossil.blesdk.obfuscated;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tu0 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("UTF-8");
    @DexIgnore
    public static /* final */ byte[] b;

    /*
    static {
        Charset.forName("ISO-8859-1");
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        byte[] bArr2 = b;
        yt0.a(bArr2, 0, bArr2.length, false);
    }
    */

    @DexIgnore
    public static int a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        for (int i5 = i2; i5 < i2 + i3; i5++) {
            i4 = (i4 * 31) + bArr[i5];
        }
        return i4;
    }

    @DexIgnore
    public static int a(long j) {
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public static int a(boolean z) {
        return z ? 1231 : 1237;
    }

    @DexIgnore
    public static int a(byte[] bArr) {
        int length = bArr.length;
        int a2 = a(length, bArr, 0, length);
        if (a2 == 0) {
            return 1;
        }
        return a2;
    }

    @DexIgnore
    public static <T> T a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public static Object a(Object obj, Object obj2) {
        tv0 c = ((sv0) obj).c();
        c.a((sv0) obj2);
        return c.v();
    }

    @DexIgnore
    public static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public static boolean a(sv0 sv0) {
        return false;
    }

    @DexIgnore
    public static boolean b(byte[] bArr) {
        return jx0.a(bArr);
    }

    @DexIgnore
    public static String c(byte[] bArr) {
        return new String(bArr, a);
    }
}
