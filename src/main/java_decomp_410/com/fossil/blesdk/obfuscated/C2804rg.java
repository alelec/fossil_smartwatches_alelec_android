package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rg */
public class C2804rg extends android.graphics.drawable.Drawable implements android.graphics.drawable.Animatable {

    @DexIgnore
    /* renamed from: k */
    public static /* final */ android.view.animation.Interpolator f8937k; // = new android.view.animation.LinearInterpolator();

    @DexIgnore
    /* renamed from: l */
    public static /* final */ android.view.animation.Interpolator f8938l; // = new com.fossil.blesdk.obfuscated.C2009ib();

    @DexIgnore
    /* renamed from: m */
    public static /* final */ int[] f8939m; // = {-16777216};

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2804rg.C2807c f8940e; // = new com.fossil.blesdk.obfuscated.C2804rg.C2807c();

    @DexIgnore
    /* renamed from: f */
    public float f8941f;

    @DexIgnore
    /* renamed from: g */
    public android.content.res.Resources f8942g;

    @DexIgnore
    /* renamed from: h */
    public android.animation.Animator f8943h;

    @DexIgnore
    /* renamed from: i */
    public float f8944i;

    @DexIgnore
    /* renamed from: j */
    public boolean f8945j;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.rg$a */
    public class C2805a implements android.animation.ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2804rg.C2807c f8946a;

        @DexIgnore
        public C2805a(com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar) {
            this.f8946a = cVar;
        }

        @DexIgnore
        public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
            float floatValue = ((java.lang.Float) valueAnimator.getAnimatedValue()).floatValue();
            com.fossil.blesdk.obfuscated.C2804rg.this.mo15514b(floatValue, this.f8946a);
            com.fossil.blesdk.obfuscated.C2804rg.this.mo15509a(floatValue, this.f8946a, false);
            com.fossil.blesdk.obfuscated.C2804rg.this.invalidateSelf();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rg$b")
    /* renamed from: com.fossil.blesdk.obfuscated.rg$b */
    public class C2806b implements android.animation.Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2804rg.C2807c f8948a;

        @DexIgnore
        public C2806b(com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar) {
            this.f8948a = cVar;
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
        }

        @DexIgnore
        public void onAnimationRepeat(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2804rg.this.mo15509a(1.0f, this.f8948a, true);
            this.f8948a.mo15556l();
            this.f8948a.mo15554j();
            com.fossil.blesdk.obfuscated.C2804rg rgVar = com.fossil.blesdk.obfuscated.C2804rg.this;
            if (rgVar.f8945j) {
                rgVar.f8945j = false;
                animator.cancel();
                animator.setDuration(1332);
                animator.start();
                this.f8948a.mo15537a(false);
                return;
            }
            rgVar.f8944i += 1.0f;
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
            com.fossil.blesdk.obfuscated.C2804rg.this.f8944i = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rg$c")
    /* renamed from: com.fossil.blesdk.obfuscated.rg$c */
    public static class C2807c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.RectF f8950a; // = new android.graphics.RectF();

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.graphics.Paint f8951b; // = new android.graphics.Paint();

        @DexIgnore
        /* renamed from: c */
        public /* final */ android.graphics.Paint f8952c; // = new android.graphics.Paint();

        @DexIgnore
        /* renamed from: d */
        public /* final */ android.graphics.Paint f8953d; // = new android.graphics.Paint();

        @DexIgnore
        /* renamed from: e */
        public float f8954e; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: f */
        public float f8955f; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: g */
        public float f8956g; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

        @DexIgnore
        /* renamed from: h */
        public float f8957h; // = 5.0f;

        @DexIgnore
        /* renamed from: i */
        public int[] f8958i;

        @DexIgnore
        /* renamed from: j */
        public int f8959j;

        @DexIgnore
        /* renamed from: k */
        public float f8960k;

        @DexIgnore
        /* renamed from: l */
        public float f8961l;

        @DexIgnore
        /* renamed from: m */
        public float f8962m;

        @DexIgnore
        /* renamed from: n */
        public boolean f8963n;

        @DexIgnore
        /* renamed from: o */
        public android.graphics.Path f8964o;

        @DexIgnore
        /* renamed from: p */
        public float f8965p; // = 1.0f;

        @DexIgnore
        /* renamed from: q */
        public float f8966q;

        @DexIgnore
        /* renamed from: r */
        public int f8967r;

        @DexIgnore
        /* renamed from: s */
        public int f8968s;

        @DexIgnore
        /* renamed from: t */
        public int f8969t; // = 255;

        @DexIgnore
        /* renamed from: u */
        public int f8970u;

        @DexIgnore
        public C2807c() {
            this.f8951b.setStrokeCap(android.graphics.Paint.Cap.SQUARE);
            this.f8951b.setAntiAlias(true);
            this.f8951b.setStyle(android.graphics.Paint.Style.STROKE);
            this.f8952c.setStyle(android.graphics.Paint.Style.FILL);
            this.f8952c.setAntiAlias(true);
            this.f8953d.setColor(0);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15532a(float f, float f2) {
            this.f8967r = (int) f;
            this.f8968s = (int) f2;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo15541b(int i) {
            this.f8970u = i;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo15544c(int i) {
            this.f8959j = i;
            this.f8970u = this.f8958i[this.f8959j];
        }

        @DexIgnore
        /* renamed from: d */
        public int mo15545d() {
            return (this.f8959j + 1) % this.f8958i.length;
        }

        @DexIgnore
        /* renamed from: e */
        public void mo15548e(float f) {
            this.f8954e = f;
        }

        @DexIgnore
        /* renamed from: f */
        public void mo15550f(float f) {
            this.f8957h = f;
            this.f8951b.setStrokeWidth(f);
        }

        @DexIgnore
        /* renamed from: g */
        public float mo15551g() {
            return this.f8961l;
        }

        @DexIgnore
        /* renamed from: h */
        public float mo15552h() {
            return this.f8962m;
        }

        @DexIgnore
        /* renamed from: i */
        public float mo15553i() {
            return this.f8960k;
        }

        @DexIgnore
        /* renamed from: j */
        public void mo15554j() {
            mo15544c(mo15545d());
        }

        @DexIgnore
        /* renamed from: k */
        public void mo15555k() {
            this.f8960k = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f8961l = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.f8962m = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            mo15548e(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            mo15543c((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            mo15546d(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        /* renamed from: l */
        public void mo15556l() {
            this.f8960k = this.f8954e;
            this.f8961l = this.f8955f;
            this.f8962m = this.f8956g;
        }

        @DexIgnore
        /* renamed from: b */
        public float mo15539b() {
            return this.f8955f;
        }

        @DexIgnore
        /* renamed from: d */
        public void mo15546d(float f) {
            this.f8956g = f;
        }

        @DexIgnore
        /* renamed from: e */
        public float mo15547e() {
            return this.f8954e;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15535a(android.graphics.Canvas canvas, android.graphics.Rect rect) {
            android.graphics.RectF rectF = this.f8950a;
            float f = this.f8966q;
            float f2 = (this.f8957h / 2.0f) + f;
            if (f <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f2 = (((float) java.lang.Math.min(rect.width(), rect.height())) / 2.0f) - java.lang.Math.max((((float) this.f8967r) * this.f8965p) / 2.0f, this.f8957h / 2.0f);
            }
            rectF.set(((float) rect.centerX()) - f2, ((float) rect.centerY()) - f2, ((float) rect.centerX()) + f2, ((float) rect.centerY()) + f2);
            float f3 = this.f8954e;
            float f4 = this.f8956g;
            float f5 = (f3 + f4) * 360.0f;
            float f6 = ((this.f8955f + f4) * 360.0f) - f5;
            this.f8951b.setColor(this.f8970u);
            this.f8951b.setAlpha(this.f8969t);
            float f7 = this.f8957h / 2.0f;
            rectF.inset(f7, f7);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.f8953d);
            float f8 = -f7;
            rectF.inset(f8, f8);
            canvas.drawArc(rectF, f5, f6, false, this.f8951b);
            mo15534a(canvas, f5, f6, rectF);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo15540b(float f) {
            this.f8966q = f;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo15542c() {
            return this.f8958i[mo15545d()];
        }

        @DexIgnore
        /* renamed from: f */
        public int mo15549f() {
            return this.f8958i[this.f8959j];
        }

        @DexIgnore
        /* renamed from: c */
        public void mo15543c(float f) {
            this.f8955f = f;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15534a(android.graphics.Canvas canvas, float f, float f2, android.graphics.RectF rectF) {
            if (this.f8963n) {
                android.graphics.Path path = this.f8964o;
                if (path == null) {
                    this.f8964o = new android.graphics.Path();
                    this.f8964o.setFillType(android.graphics.Path.FillType.EVEN_ODD);
                } else {
                    path.reset();
                }
                this.f8964o.moveTo(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.f8964o.lineTo(((float) this.f8967r) * this.f8965p, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                android.graphics.Path path2 = this.f8964o;
                float f3 = this.f8965p;
                path2.lineTo((((float) this.f8967r) * f3) / 2.0f, ((float) this.f8968s) * f3);
                this.f8964o.offset(((java.lang.Math.min(rectF.width(), rectF.height()) / 2.0f) + rectF.centerX()) - ((((float) this.f8967r) * this.f8965p) / 2.0f), rectF.centerY() + (this.f8957h / 2.0f));
                this.f8964o.close();
                this.f8952c.setColor(this.f8970u);
                this.f8952c.setAlpha(this.f8969t);
                canvas.save();
                canvas.rotate(f + f2, rectF.centerX(), rectF.centerY());
                canvas.drawPath(this.f8964o, this.f8952c);
                canvas.restore();
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15538a(int[] iArr) {
            this.f8958i = iArr;
            mo15544c(0);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15536a(android.graphics.ColorFilter colorFilter) {
            this.f8951b.setColorFilter(colorFilter);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15533a(int i) {
            this.f8969t = i;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo15530a() {
            return this.f8969t;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15537a(boolean z) {
            if (this.f8963n != z) {
                this.f8963n = z;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo15531a(float f) {
            if (f != this.f8965p) {
                this.f8965p = f;
            }
        }
    }

    @DexIgnore
    public C2804rg(android.content.Context context) {
        com.fossil.blesdk.obfuscated.C2109j8.m8871a(context);
        this.f8942g = context.getResources();
        this.f8940e.mo15538a(f8939m);
        mo15516d(2.5f);
        mo15504a();
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo15503a(float f, int i, int i2) {
        int i3 = (i >> 24) & 255;
        int i4 = (i >> 16) & 255;
        int i5 = (i >> 8) & 255;
        int i6 = i & 255;
        return ((i3 + ((int) (((float) (((i2 >> 24) & 255) - i3)) * f))) << 24) | ((i4 + ((int) (((float) (((i2 >> 16) & 255) - i4)) * f))) << 16) | ((i5 + ((int) (((float) (((i2 >> 8) & 255) - i5)) * f))) << 8) | (i6 + ((int) (f * ((float) ((i2 & 255) - i6)))));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15507a(float f, float f2, float f3, float f4) {
        com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar = this.f8940e;
        float f5 = this.f8942g.getDisplayMetrics().density;
        cVar.mo15550f(f2 * f5);
        cVar.mo15540b(f * f5);
        cVar.mo15544c(0);
        cVar.mo15532a(f3 * f5, f4 * f5);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15513b(float f) {
        this.f8940e.mo15546d(f);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo15515c(float f) {
        this.f8941f = f;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo15516d(float f) {
        this.f8940e.mo15550f(f);
        invalidateSelf();
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        android.graphics.Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.f8941f, bounds.exactCenterX(), bounds.exactCenterY());
        this.f8940e.mo15535a(canvas, bounds);
        canvas.restore();
    }

    @DexIgnore
    public int getAlpha() {
        return this.f8940e.mo15530a();
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public boolean isRunning() {
        return this.f8943h.isRunning();
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.f8940e.mo15533a(i);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        this.f8940e.mo15536a(colorFilter);
        invalidateSelf();
    }

    @DexIgnore
    public void start() {
        this.f8943h.cancel();
        this.f8940e.mo15556l();
        if (this.f8940e.mo15539b() != this.f8940e.mo15547e()) {
            this.f8945j = true;
            this.f8943h.setDuration(666);
            this.f8943h.start();
            return;
        }
        this.f8940e.mo15544c(0);
        this.f8940e.mo15555k();
        this.f8943h.setDuration(1332);
        this.f8943h.start();
    }

    @DexIgnore
    public void stop() {
        this.f8943h.cancel();
        mo15515c(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.f8940e.mo15537a(false);
        this.f8940e.mo15544c(0);
        this.f8940e.mo15555k();
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15514b(float f, com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar) {
        if (f > 0.75f) {
            cVar.mo15541b(mo15503a((f - 0.75f) / 0.25f, cVar.mo15549f(), cVar.mo15542c()));
        } else {
            cVar.mo15541b(cVar.mo15549f());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15510a(int i) {
        if (i == 0) {
            mo15507a(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            mo15507a(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15511a(boolean z) {
        this.f8940e.mo15537a(z);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15505a(float f) {
        this.f8940e.mo15531a(f);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15506a(float f, float f2) {
        this.f8940e.mo15548e(f);
        this.f8940e.mo15543c(f2);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15512a(int... iArr) {
        this.f8940e.mo15538a(iArr);
        this.f8940e.mo15544c(0);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15508a(float f, com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar) {
        mo15514b(f, cVar);
        cVar.mo15548e(cVar.mo15553i() + (((cVar.mo15551g() - 0.01f) - cVar.mo15553i()) * f));
        cVar.mo15543c(cVar.mo15551g());
        cVar.mo15546d(cVar.mo15552h() + ((((float) (java.lang.Math.floor((double) (cVar.mo15552h() / 0.8f)) + 1.0d)) - cVar.mo15552h()) * f));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15509a(float f, com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar, boolean z) {
        float f2;
        float f3;
        if (this.f8945j) {
            mo15508a(f, cVar);
        } else if (f != 1.0f || z) {
            float h = cVar.mo15552h();
            if (f < 0.5f) {
                float i = cVar.mo15553i();
                float f4 = i;
                f2 = (f8938l.getInterpolation(f / 0.5f) * 0.79f) + 0.01f + i;
                f3 = f4;
            } else {
                f2 = cVar.mo15553i() + 0.79f;
                f3 = f2 - (((1.0f - f8938l.getInterpolation((f - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            cVar.mo15548e(f3);
            cVar.mo15543c(f2);
            cVar.mo15546d(h + (0.20999998f * f));
            mo15515c((f + this.f8944i) * 216.0f);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15504a() {
        com.fossil.blesdk.obfuscated.C2804rg.C2807c cVar = this.f8940e;
        android.animation.ValueAnimator ofFloat = android.animation.ValueAnimator.ofFloat(new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
        ofFloat.addUpdateListener(new com.fossil.blesdk.obfuscated.C2804rg.C2805a(cVar));
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(f8937k);
        ofFloat.addListener(new com.fossil.blesdk.obfuscated.C2804rg.C2806b(cVar));
        this.f8943h = ofFloat;
    }
}
