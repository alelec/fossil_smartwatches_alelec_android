package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p72 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<DianaPresetWatchAppSetting>> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<DianaPresetWatchAppSetting>> {
    }

    @DexIgnore
    public final String a(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        kd4.b(arrayList, "configurationList");
        if (arrayList.isEmpty()) {
            return "";
        }
        return new Gson().a((Object) arrayList, new a().getType());
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> a(String str) {
        kd4.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        Object a2 = new Gson().a(str, new b().getType());
        kd4.a(a2, "Gson().fromJson(data, type)");
        return (ArrayList) a2;
    }
}
