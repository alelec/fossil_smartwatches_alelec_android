package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.gr4;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.gson.AlarmDeserializer;
import com.portfolio.platform.gson.DianaPresetDeserializer;
import com.portfolio.platform.gson.DianaRecommendPresetDeserializer;
import com.portfolio.platform.gson.HybridPresetDeserializer;
import com.portfolio.platform.gson.HybridRecommendPresetDeserializer;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.joda.time.DateTime;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ap2 {
    @DexIgnore
    public static /* final */ Retrofit.b a;
    @DexIgnore
    public static Retrofit b;
    @DexIgnore
    public static Cache c;
    @DexIgnore
    public static /* final */ at3 d;
    @DexIgnore
    public static /* final */ OkHttpClient.b e;
    @DexIgnore
    public static String f;
    @DexIgnore
    public static /* final */ ap2 g; // = new ap2();

    /*
    static {
        Retrofit.b bVar = new Retrofit.b();
        rz1 rz1 = new rz1();
        rz1.b(new jk2());
        rz1.a((oz1) new jk2());
        rz1.a(Date.class, new GsonConvertDate());
        rz1.a(DianaPreset.class, new DianaPresetDeserializer());
        rz1.a(DianaRecommendPreset.class, new DianaRecommendPresetDeserializer());
        rz1.a(HybridPreset.class, new HybridPresetDeserializer());
        rz1.a(HybridRecommendPreset.class, new HybridRecommendPresetDeserializer());
        rz1.a(DateTime.class, new GsonConvertDateTime());
        rz1.a(Alarm.class, new AlarmDeserializer());
        bVar.a((gr4.a) GsonConverterFactory.a(rz1.a()));
        a = bVar;
        at3 at3 = new at3();
        at3.a(kd4.a((Object) "release", (Object) "release") ? HttpLoggingInterceptor.Level.BASIC : HttpLoggingInterceptor.Level.BODY);
        d = at3;
        OkHttpClient.b bVar2 = new OkHttpClient.b();
        bVar2.a((Interceptor) d);
        e = bVar2;
    }
    */

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "baseUrl");
        if (!TextUtils.equals(f, str)) {
            f = str;
            Retrofit.b bVar = a;
            String str2 = f;
            if (str2 != null) {
                bVar.a(str2);
                b = a.a();
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(hl4 hl4) {
        kd4.b(hl4, "authenticator");
        e.a(hl4);
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final void a(File file) {
        kd4.b(file, "cacheDir");
        if (c == null) {
            File file2 = new File(file.getAbsolutePath(), "cacheResponse");
            if (!file2.exists()) {
                file2.mkdir();
            }
            c = new Cache(file2, 10485760);
        }
        e.a(c);
    }

    @DexIgnore
    public final void a() {
        Cache cache = c;
        if (cache != null) {
            cache.y();
        }
    }

    @DexIgnore
    public final void a(Interceptor interceptor) {
        e.b(5, TimeUnit.SECONDS);
        e.a(5, TimeUnit.SECONDS);
        e.c(5, TimeUnit.SECONDS);
        e.b().clear();
        e.a((Interceptor) d);
        if (interceptor != null && !e.b().contains(interceptor)) {
            e.a(interceptor);
        }
        a.a(e.a());
        b = a.a();
    }

    @DexIgnore
    public final <S> S a(Class<S> cls) {
        kd4.b(cls, "serviceClass");
        Retrofit retrofit3 = b;
        if (retrofit3 != null) {
            return retrofit3.a(cls);
        }
        kd4.a();
        throw null;
    }
}
