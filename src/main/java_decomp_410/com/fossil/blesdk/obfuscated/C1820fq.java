package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fq */
public interface C1820fq<T> {
    @DexIgnore
    /* renamed from: a */
    int mo11010a(T t);

    @DexIgnore
    /* renamed from: a */
    java.lang.String mo11011a();

    @DexIgnore
    /* renamed from: b */
    int mo11012b();

    @DexIgnore
    T newArray(int i);
}
