package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class es3 {
    @DexIgnore
    public static /* final */ qs3 a; // = new qs3("GLOBAL");

    @DexIgnore
    public static void a() {
        a.b();
    }

    @DexIgnore
    public static sg b() {
        return a;
    }

    @DexIgnore
    public static void c() {
        a.c();
    }
}
