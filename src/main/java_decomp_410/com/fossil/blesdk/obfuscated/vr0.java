package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vr0 extends IInterface {
    @DexIgnore
    boolean c(boolean z) throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    boolean zzc() throws RemoteException;
}
