package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hk3 implements Factory<BaseActivity> {
    @DexIgnore
    public static BaseActivity a(gk3 gk3) {
        BaseActivity a = gk3.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
