package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f13 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<h13> c;
    @DexIgnore
    public ArrayList<h13> d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public WatchFaceWrapper f;

    @DexIgnore
    public f13(String str, String str2, ArrayList<h13> arrayList, ArrayList<h13> arrayList2, boolean z, WatchFaceWrapper watchFaceWrapper) {
        kd4.b(str, "mPresetId");
        kd4.b(str2, "mPresetName");
        kd4.b(arrayList, "mComplications");
        kd4.b(arrayList2, "mWatchApps");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = arrayList2;
        this.e = z;
        this.f = watchFaceWrapper;
    }

    @DexIgnore
    public final ArrayList<h13> a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<h13> e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof f13) {
                f13 f13 = (f13) obj;
                if (kd4.a((Object) this.a, (Object) f13.a) && kd4.a((Object) this.b, (Object) f13.b) && kd4.a((Object) this.c, (Object) f13.c) && kd4.a((Object) this.d, (Object) f13.d)) {
                    if (!(this.e == f13.e) || !kd4.a((Object) this.f, (Object) f13.f)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final WatchFaceWrapper f() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ArrayList<h13> arrayList = this.c;
        int hashCode3 = (hashCode2 + (arrayList != null ? arrayList.hashCode() : 0)) * 31;
        ArrayList<h13> arrayList2 = this.d;
        int hashCode4 = (hashCode3 + (arrayList2 != null ? arrayList2.hashCode() : 0)) * 31;
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i2 = (hashCode4 + (z ? 1 : 0)) * 31;
        WatchFaceWrapper watchFaceWrapper = this.f;
        if (watchFaceWrapper != null) {
            i = watchFaceWrapper.hashCode();
        }
        return i2 + i;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetConfigWrapper(mPresetId=" + this.a + ", mPresetName=" + this.b + ", mComplications=" + this.c + ", mWatchApps=" + this.d + ", mIsActive=" + this.e + ", mWatchFaceWrapper=" + this.f + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ f13(String str, String str2, ArrayList arrayList, ArrayList arrayList2, boolean z, WatchFaceWrapper watchFaceWrapper, int i, fd4 fd4) {
        this(str, str2, arrayList, arrayList2, z, (i & 32) != 0 ? null : watchFaceWrapper);
    }
}
