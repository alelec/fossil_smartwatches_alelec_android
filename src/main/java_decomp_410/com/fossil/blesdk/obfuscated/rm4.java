package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rm4 implements Interceptor {
    @DexIgnore
    public /* final */ OkHttpClient a;

    @DexIgnore
    public rm4(OkHttpClient okHttpClient) {
        this.a = okHttpClient;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        dn4 dn4 = (dn4) chain;
        dm4 n = dn4.n();
        wm4 h = dn4.h();
        return dn4.a(n, h, h.a(this.a, chain, !n.e().equals("GET")), h.c());
    }
}
