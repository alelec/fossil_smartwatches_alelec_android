package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dh0 {
    @DexIgnore
    public /* final */ ig0 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ fe0<?> c;

    @DexIgnore
    public dh0(ig0 ig0, int i, fe0<?> fe0) {
        this.a = ig0;
        this.b = i;
        this.c = fe0;
    }
}
