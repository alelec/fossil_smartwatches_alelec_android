package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j4<E> implements Cloneable {
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long[] f;
    @DexIgnore
    public Object[] g;
    @DexIgnore
    public int h;

    @DexIgnore
    public j4() {
        this(10);
    }

    @DexIgnore
    public void a(long j) {
        int a = i4.a(this.f, this.h, j);
        if (a >= 0) {
            Object[] objArr = this.g;
            Object obj = objArr[a];
            Object obj2 = i;
            if (obj != obj2) {
                objArr[a] = obj2;
                this.e = true;
            }
        }
    }

    @DexIgnore
    public E b(long j) {
        return b(j, (Object) null);
    }

    @DexIgnore
    public void c(long j, E e2) {
        int a = i4.a(this.f, this.h, j);
        if (a >= 0) {
            this.g[a] = e2;
            return;
        }
        int i2 = ~a;
        if (i2 < this.h) {
            Object[] objArr = this.g;
            if (objArr[i2] == i) {
                this.f[i2] = j;
                objArr[i2] = e2;
                return;
            }
        }
        if (this.e && this.h >= this.f.length) {
            b();
            i2 = ~i4.a(this.f, this.h, j);
        }
        int i3 = this.h;
        if (i3 >= this.f.length) {
            int c = i4.c(i3 + 1);
            long[] jArr = new long[c];
            Object[] objArr2 = new Object[c];
            long[] jArr2 = this.f;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.g;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.f = jArr;
            this.g = objArr2;
        }
        int i4 = this.h;
        if (i4 - i2 != 0) {
            long[] jArr3 = this.f;
            int i5 = i2 + 1;
            System.arraycopy(jArr3, i2, jArr3, i5, i4 - i2);
            Object[] objArr4 = this.g;
            System.arraycopy(objArr4, i2, objArr4, i5, this.h - i2);
        }
        this.f[i2] = j;
        this.g[i2] = e2;
        this.h++;
    }

    @DexIgnore
    public String toString() {
        if (c() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.h * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.h; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            sb.append(a(i2));
            sb.append('=');
            Object c = c(i2);
            if (c != this) {
                sb.append(c);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public j4(int i2) {
        this.e = false;
        if (i2 == 0) {
            this.f = i4.b;
            this.g = i4.c;
        } else {
            int c = i4.c(i2);
            this.f = new long[c];
            this.g = new Object[c];
        }
        this.h = 0;
    }

    @DexIgnore
    public E b(long j, E e2) {
        int a = i4.a(this.f, this.h, j);
        if (a >= 0) {
            E[] eArr = this.g;
            if (eArr[a] != i) {
                return eArr[a];
            }
        }
        return e2;
    }

    @DexIgnore
    public j4<E> clone() {
        try {
            j4<E> j4Var = (j4) super.clone();
            j4Var.f = (long[]) this.f.clone();
            j4Var.g = (Object[]) this.g.clone();
            return j4Var;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public long a(int i2) {
        if (this.e) {
            b();
        }
        return this.f[i2];
    }

    @DexIgnore
    public void b(int i2) {
        Object[] objArr = this.g;
        Object obj = objArr[i2];
        Object obj2 = i;
        if (obj != obj2) {
            objArr[i2] = obj2;
            this.e = true;
        }
    }

    @DexIgnore
    public void a() {
        int i2 = this.h;
        Object[] objArr = this.g;
        for (int i3 = 0; i3 < i2; i3++) {
            objArr[i3] = null;
        }
        this.h = 0;
        this.e = false;
    }

    @DexIgnore
    public final void b() {
        int i2 = this.h;
        long[] jArr = this.f;
        Object[] objArr = this.g;
        int i3 = 0;
        for (int i4 = 0; i4 < i2; i4++) {
            Object obj = objArr[i4];
            if (obj != i) {
                if (i4 != i3) {
                    jArr[i3] = jArr[i4];
                    objArr[i3] = obj;
                    objArr[i4] = null;
                }
                i3++;
            }
        }
        this.e = false;
        this.h = i3;
    }

    @DexIgnore
    public void a(long j, E e2) {
        int i2 = this.h;
        if (i2 == 0 || j > this.f[i2 - 1]) {
            if (this.e && this.h >= this.f.length) {
                b();
            }
            int i3 = this.h;
            if (i3 >= this.f.length) {
                int c = i4.c(i3 + 1);
                long[] jArr = new long[c];
                Object[] objArr = new Object[c];
                long[] jArr2 = this.f;
                System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                Object[] objArr2 = this.g;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.f = jArr;
                this.g = objArr;
            }
            this.f[i3] = j;
            this.g[i3] = e2;
            this.h = i3 + 1;
            return;
        }
        c(j, e2);
    }

    @DexIgnore
    public int c() {
        if (this.e) {
            b();
        }
        return this.h;
    }

    @DexIgnore
    public E c(int i2) {
        if (this.e) {
            b();
        }
        return this.g[i2];
    }

    @DexIgnore
    public int c(long j) {
        if (this.e) {
            b();
        }
        return i4.a(this.f, this.h, j);
    }
}
