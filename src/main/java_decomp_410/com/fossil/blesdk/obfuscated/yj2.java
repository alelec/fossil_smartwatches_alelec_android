package com.fossil.blesdk.obfuscated;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yj2 implements sr<zj2, InputStream> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements so<InputStream> {
        @DexIgnore
        public volatile boolean e;
        @DexIgnore
        public /* final */ zj2 f;

        @DexIgnore
        public a(yj2 yj2, zj2 zj2) {
            kd4.b(zj2, "mAppIconModel");
            this.f = zj2;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super InputStream> aVar) {
            kd4.b(priority, "priority");
            kd4.b(aVar, Constants.CALLBACK);
            try {
                Drawable applicationIcon = PortfolioApp.W.c().getPackageManager().getApplicationIcon(this.f.a().getIdentifier());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap a = vr3.a(applicationIcon);
                if (a != null) {
                    a.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                }
                aVar.a(this.e ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            } catch (PackageManager.NameNotFoundException e2) {
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
            this.e = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tr<zj2, InputStream> {
        @DexIgnore
        public yj2 a(wr wrVar) {
            kd4.b(wrVar, "multiFactory");
            return new yj2();
        }
    }

    @DexIgnore
    public boolean a(zj2 zj2) {
        kd4.b(zj2, "appIconModel");
        return true;
    }

    @DexIgnore
    public sr.a<InputStream> a(zj2 zj2, int i, int i2, lo loVar) {
        kd4.b(zj2, "appIconModel");
        kd4.b(loVar, "options");
        return new sr.a<>(zj2, new a(this, zj2));
    }
}
