package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class m74 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore
    public m74(String str, String str2, String str3, String str4, String str5, boolean z, k74 k74) {
        this.a = str2;
        this.b = str3;
        this.c = str4;
        this.d = str5;
        this.e = z;
    }
}
