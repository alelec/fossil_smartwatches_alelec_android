package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hg */
public interface C1945hg {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hg$a */
    public static abstract class C1946a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f5733a;

        @DexIgnore
        public C1946a(int i) {
            this.f5733a = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11638a(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        }

        @DexIgnore
        /* renamed from: a */
        public abstract void mo11639a(com.fossil.blesdk.obfuscated.C1874gg ggVar, int i, int i2);

        @DexIgnore
        /* renamed from: a */
        public final void mo11640a(java.lang.String str) {
            if (!str.equalsIgnoreCase(":memory:") && str.trim().length() != 0) {
                android.util.Log.w("SupportSQLite", "deleting the database file: " + str);
                try {
                    if (android.os.Build.VERSION.SDK_INT >= 16) {
                        android.database.sqlite.SQLiteDatabase.deleteDatabase(new java.io.File(str));
                        return;
                    }
                    try {
                        if (!new java.io.File(str).delete()) {
                            android.util.Log.e("SupportSQLite", "Could not delete the database file " + str);
                        }
                    } catch (java.lang.Exception e) {
                        android.util.Log.e("SupportSQLite", "error while deleting corrupted database file", e);
                    }
                } catch (java.lang.Exception e2) {
                    android.util.Log.w("SupportSQLite", "delete failed: ", e2);
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0034, code lost:
            if (r0 != null) goto L_0x0036;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
            r3 = r0.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
            if (r3.hasNext() != false) goto L_0x0040;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
            mo11640a((java.lang.String) r3.next().second);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x004e, code lost:
            mo11640a(r3.mo11238w());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
            throw r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x002e, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0030 */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0071  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x002e A[ExcHandler: all (r1v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r0 
  PHI: (r0v10 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) = (r0v3 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v4 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>), (r0v4 java.util.List<android.util.Pair<java.lang.String, java.lang.String>>) binds: [B:5:0x0029, B:8:0x0030, B:9:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:5:0x0029] */
        @DexIgnore
        /* renamed from: b */
        public void mo11641b(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
            android.util.Log.e("SupportSQLite", "Corruption reported by sqlite on database: " + ggVar.mo11238w());
            if (!ggVar.isOpen()) {
                mo11640a(ggVar.mo11238w());
                return;
            }
            java.util.List<android.util.Pair<java.lang.String, java.lang.String>> list = null;
            try {
                list = ggVar.mo11235t();
                ggVar.close();
            } catch (java.io.IOException unused) {
            } catch (Throwable th) {
            }
            if (list == null) {
                for (android.util.Pair<java.lang.String, java.lang.String> pair : list) {
                    mo11640a((java.lang.String) pair.second);
                }
                return;
            }
            mo11640a(ggVar.mo11238w());
        }

        @DexIgnore
        /* renamed from: b */
        public abstract void mo11642b(com.fossil.blesdk.obfuscated.C1874gg ggVar, int i, int i2);

        @DexIgnore
        /* renamed from: c */
        public abstract void mo11643c(com.fossil.blesdk.obfuscated.C1874gg ggVar);

        @DexIgnore
        /* renamed from: d */
        public void mo11644d(com.fossil.blesdk.obfuscated.C1874gg ggVar) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hg$b")
    /* renamed from: com.fossil.blesdk.obfuscated.hg$b */
    public static class C1947b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Context f5734a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f5735b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C1945hg.C1946a f5736c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hg$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.hg$b$a */
        public static class C1948a {

            @DexIgnore
            /* renamed from: a */
            public android.content.Context f5737a;

            @DexIgnore
            /* renamed from: b */
            public java.lang.String f5738b;

            @DexIgnore
            /* renamed from: c */
            public com.fossil.blesdk.obfuscated.C1945hg.C1946a f5739c;

            @DexIgnore
            public C1948a(android.content.Context context) {
                this.f5737a = context;
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C1945hg.C1947b mo11647a() {
                com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar = this.f5739c;
                if (aVar != null) {
                    android.content.Context context = this.f5737a;
                    if (context != null) {
                        return new com.fossil.blesdk.obfuscated.C1945hg.C1947b(context, this.f5738b, aVar);
                    }
                    throw new java.lang.IllegalArgumentException("Must set a non-null context to create the configuration.");
                }
                throw new java.lang.IllegalArgumentException("Must set a callback to create the configuration.");
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C1945hg.C1947b.C1948a mo11646a(java.lang.String str) {
                this.f5738b = str;
                return this;
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C1945hg.C1947b.C1948a mo11645a(com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar) {
                this.f5739c = aVar;
                return this;
            }
        }

        @DexIgnore
        public C1947b(android.content.Context context, java.lang.String str, com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar) {
            this.f5734a = context;
            this.f5735b = str;
            this.f5736c = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C1945hg.C1947b.C1948a m7875a(android.content.Context context) {
            return new com.fossil.blesdk.obfuscated.C1945hg.C1947b.C1948a(context);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.hg$c */
    public interface C1949c {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C1945hg mo11648a(com.fossil.blesdk.obfuscated.C1945hg.C1947b bVar);
    }

    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C1874gg mo11634a();

    @DexIgnore
    /* renamed from: a */
    void mo11635a(boolean z);

    @DexIgnore
    void close();

    @DexIgnore
    java.lang.String getDatabaseName();
}
