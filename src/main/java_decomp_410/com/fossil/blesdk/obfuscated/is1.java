package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class is1 extends Drawable {
    @DexIgnore
    public /* final */ Paint a; // = new Paint(1);
    @DexIgnore
    public /* final */ Rect b; // = new Rect();
    @DexIgnore
    public /* final */ RectF c; // = new RectF();
    @DexIgnore
    public /* final */ b d; // = new b();
    @DexIgnore
    public float e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l; // = true;
    @DexIgnore
    public float m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Drawable.ConstantState {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return is1.this;
        }
    }

    @DexIgnore
    public is1() {
        this.a.setStyle(Paint.Style.STROKE);
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        this.f = i2;
        this.g = i3;
        this.h = i4;
        this.i = i5;
    }

    @DexIgnore
    public final void b(float f2) {
        if (f2 != this.m) {
            this.m = f2;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (this.l) {
            this.a.setShader(a());
            this.l = false;
        }
        float strokeWidth = this.a.getStrokeWidth() / 2.0f;
        RectF rectF = this.c;
        copyBounds(this.b);
        rectF.set(this.b);
        rectF.left += strokeWidth;
        rectF.top += strokeWidth;
        rectF.right -= strokeWidth;
        rectF.bottom -= strokeWidth;
        canvas.save();
        canvas.rotate(this.m, rectF.centerX(), rectF.centerY());
        canvas.drawOval(rectF, this.a);
        canvas.restore();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.d;
    }

    @DexIgnore
    public int getOpacity() {
        return this.e > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? -3 : -2;
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        int round = Math.round(this.e);
        rect.set(round, round, round, round);
        return true;
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList = this.j;
        return (colorStateList != null && colorStateList.isStateful()) || super.isStateful();
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.l = true;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        ColorStateList colorStateList = this.j;
        if (colorStateList != null) {
            int colorForState = colorStateList.getColorForState(iArr, this.k);
            if (colorForState != this.k) {
                this.l = true;
                this.k = colorForState;
            }
        }
        if (this.l) {
            invalidateSelf();
        }
        return this.l;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.a.setAlpha(i2);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
        invalidateSelf();
    }

    @DexIgnore
    public void a(float f2) {
        if (this.e != f2) {
            this.e = f2;
            this.a.setStrokeWidth(f2 * 1.3333f);
            this.l = true;
            invalidateSelf();
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            this.k = colorStateList.getColorForState(getState(), this.k);
        }
        this.j = colorStateList;
        this.l = true;
        invalidateSelf();
    }

    @DexIgnore
    public final Shader a() {
        Rect rect = this.b;
        copyBounds(rect);
        float height = this.e / ((float) rect.height());
        return new LinearGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) rect.top, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) rect.bottom, new int[]{t6.b(this.f, this.k), t6.b(this.g, this.k), t6.b(t6.c(this.g, 0), this.k), t6.b(t6.c(this.i, 0), this.k), t6.b(this.i, this.k), t6.b(this.h, this.k)}, new float[]{0.0f, height, 0.5f, 0.5f, 1.0f - height, 1.0f}, Shader.TileMode.CLAMP);
    }
}
