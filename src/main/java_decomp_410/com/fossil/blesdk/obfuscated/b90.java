package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import java.util.Arrays;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b90 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public byte[] b; // = new byte[8];
    @DexIgnore
    public byte[] c; // = new byte[8];
    @DexIgnore
    public /* final */ Hashtable<GattCharacteristic.CharacteristicId, e90> d; // = new Hashtable<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public final void a(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    public final byte[] b() {
        return this.a;
    }

    @DexIgnore
    public final void c(GattCharacteristic.CharacteristicId characteristicId) {
        kd4.b(characteristicId, "characteristicId");
        b(characteristicId).a(this.b, this.c);
    }

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = this.a;
        if (bArr == null) {
            return null;
        }
        if (bArr.length <= 16) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, 16);
        kd4.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public final e90 b(GattCharacteristic.CharacteristicId characteristicId) {
        e90 e90 = this.d.get(characteristicId);
        if (e90 == null) {
            e90 = new e90(characteristicId, this.b, this.c, 0, 0);
        }
        this.d.put(characteristicId, e90);
        return e90;
    }

    @DexIgnore
    public final void a(byte[] bArr, byte[] bArr2) {
        kd4.b(bArr, "phoneRandomNumber");
        kd4.b(bArr2, "deviceRandomNumber");
        if (bArr.length == 8 && bArr2.length == 8) {
            this.b = bArr;
            this.c = bArr2;
        }
    }

    @DexIgnore
    public final void a(GattCharacteristic.CharacteristicId characteristicId, int i) {
        kd4.b(characteristicId, "characteristicId");
        e90 e90 = this.d.get(characteristicId);
        if (e90 != null) {
            e90.a(i);
        }
    }

    @DexIgnore
    public final byte[] a(GattCharacteristic.CharacteristicId characteristicId) {
        kd4.b(characteristicId, "characteristicId");
        return b(characteristicId).a();
    }
}
