package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.setting.SharedPreferenceFileName;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y90 extends aa0 {
    @DexIgnore
    public static long k; // = 100;
    @DexIgnore
    public static /* final */ y90 l; // = new y90();

    @DexIgnore
    public y90() {
        super("raw_hardware_log", 204800, "raw_hardware_log", "raw_hardware_log", new na0("", "", ""), 1800, new ba0(), SharedPreferenceFileName.HARDWARE_LOG_REFERENCE, false);
    }

    @DexIgnore
    public long b() {
        return k;
    }
}
