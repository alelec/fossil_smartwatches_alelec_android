package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yw */
public class C3394yw extends com.fossil.blesdk.obfuscated.v44<java.lang.Void> implements com.fossil.blesdk.obfuscated.w44 {

    @DexIgnore
    /* renamed from: k */
    public /* final */ com.fossil.blesdk.obfuscated.C3069uy f11421k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.util.Collection<? extends com.fossil.blesdk.obfuscated.v44> f11422l;

    @DexIgnore
    public C3394yw() {
        this(new com.fossil.blesdk.obfuscated.C1518bx(), new com.fossil.blesdk.obfuscated.C2259ky(), new com.fossil.blesdk.obfuscated.C3069uy());
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17131a(int i, java.lang.String str, java.lang.String str2) {
        m17132v();
        m17133w().f11421k.mo16924b(i, str, str2);
    }

    @DexIgnore
    /* renamed from: v */
    public static void m17132v() {
        if (m17133w() == null) {
            throw new java.lang.IllegalStateException("Crashlytics must be initialized by calling Fabric.with(Context) prior to calling Crashlytics.getInstance()");
        }
    }

    @DexIgnore
    /* renamed from: w */
    public static com.fossil.blesdk.obfuscated.C3394yw m17133w() {
        return (com.fossil.blesdk.obfuscated.C3394yw) com.fossil.blesdk.obfuscated.q44.m26795a(com.fossil.blesdk.obfuscated.C3394yw.class);
    }

    @DexIgnore
    /* renamed from: i */
    public java.util.Collection<? extends com.fossil.blesdk.obfuscated.v44> mo18356i() {
        return this.f11422l;
    }

    @DexIgnore
    /* renamed from: k */
    public java.lang.Void m17136k() {
        return null;
    }

    @DexIgnore
    /* renamed from: p */
    public java.lang.String mo9329p() {
        return "com.crashlytics.sdk.android:crashlytics";
    }

    @DexIgnore
    /* renamed from: r */
    public java.lang.String mo9330r() {
        return "2.10.1.34";
    }

    @DexIgnore
    public C3394yw(com.fossil.blesdk.obfuscated.C1518bx bxVar, com.fossil.blesdk.obfuscated.C2259ky kyVar, com.fossil.blesdk.obfuscated.C3069uy uyVar) {
        this.f11421k = uyVar;
        this.f11422l = java.util.Collections.unmodifiableCollection(java.util.Arrays.asList(new com.fossil.blesdk.obfuscated.v44[]{bxVar, kyVar, uyVar}));
    }
}
