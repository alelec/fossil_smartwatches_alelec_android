package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class c1<T> extends d1<T> {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public Map<i7, MenuItem> c;
    @DexIgnore
    public Map<j7, SubMenu> d;

    @DexIgnore
    public c1(Context context, T t) {
        super(t);
        this.b = context;
    }

    @DexIgnore
    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof i7)) {
            return menuItem;
        }
        i7 i7Var = (i7) menuItem;
        if (this.c == null) {
            this.c = new g4();
        }
        MenuItem menuItem2 = this.c.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItem a = r1.a(this.b, i7Var);
        this.c.put(i7Var, a);
        return a;
    }

    @DexIgnore
    public final void b() {
        Map<i7, MenuItem> map = this.c;
        if (map != null) {
            map.clear();
        }
        Map<j7, SubMenu> map2 = this.d;
        if (map2 != null) {
            map2.clear();
        }
    }

    @DexIgnore
    public final void b(int i) {
        Map<i7, MenuItem> map = this.c;
        if (map != null) {
            Iterator<i7> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof j7)) {
            return subMenu;
        }
        j7 j7Var = (j7) subMenu;
        if (this.d == null) {
            this.d = new g4();
        }
        SubMenu subMenu2 = this.d.get(j7Var);
        if (subMenu2 != null) {
            return subMenu2;
        }
        SubMenu a = r1.a(this.b, j7Var);
        this.d.put(j7Var, a);
        return a;
    }

    @DexIgnore
    public final void a(int i) {
        Map<i7, MenuItem> map = this.c;
        if (map != null) {
            Iterator<i7> it = map.keySet().iterator();
            while (it.hasNext()) {
                if (i == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }
}
