package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y9 */
public class C3349y9 extends com.fossil.blesdk.obfuscated.C2951t9 {

    @DexIgnore
    /* renamed from: w */
    public /* final */ android.widget.ListView f11213w;

    @DexIgnore
    public C3349y9(android.widget.ListView listView) {
        super(listView);
        this.f11213w = listView;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16305a(int i, int i2) {
        com.fossil.blesdk.obfuscated.C3419z9.m17300b(this.f11213w, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo16306a(int i) {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038 A[RETURN] */
    /* renamed from: b */
    public boolean mo16309b(int i) {
        android.widget.ListView listView = this.f11213w;
        int count = listView.getCount();
        if (count == 0) {
            return false;
        }
        int childCount = listView.getChildCount();
        int firstVisiblePosition = listView.getFirstVisiblePosition();
        int i2 = firstVisiblePosition + childCount;
        if (i > 0) {
            if (i2 < count || listView.getChildAt(childCount - 1).getBottom() > listView.getHeight()) {
                return true;
            }
            return false;
        } else if (i >= 0) {
            return false;
        } else {
            if (firstVisiblePosition <= 0 && listView.getChildAt(0).getTop() >= 0) {
                return false;
            }
        }
        return true;
    }
}
