package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zg2 extends yg2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.iv_workout, 1);
        x.put(R.id.ftv_workout_title, 2);
        x.put(R.id.ftv_workout_value, 3);
        x.put(R.id.ftv_workout_time, 4);
    }
    */

    @DexIgnore
    public zg2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 5, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public zg2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[4], objArr[2], objArr[3], objArr[1]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
