package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.os.Bundle;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rj1 extends pk1 {
    @DexIgnore
    public qj1 c;
    @DexIgnore
    public volatile qj1 d;
    @DexIgnore
    public qj1 e;
    @DexIgnore
    public /* final */ Map<Activity, qj1> f; // = new g4();
    @DexIgnore
    public String g;

    @DexIgnore
    public rj1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final qj1 A() {
        v();
        e();
        return this.c;
    }

    @DexIgnore
    public final qj1 B() {
        f();
        return this.d;
    }

    @DexIgnore
    public final void a(Activity activity, String str, String str2) {
        if (this.d == null) {
            d().v().a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f.get(activity) == null) {
            d().v().a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = a(activity.getClass().getCanonicalName());
            }
            boolean equals = this.d.b.equals(str2);
            boolean e2 = nl1.e(this.d.a, str);
            if (equals && e2) {
                d().x().a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                d().v().a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                d().A().a("Setting current screen to name, class", str == null ? "null" : str, str2);
                qj1 qj1 = new qj1(str, str2, j().s());
                this.f.put(activity, qj1);
                a(activity, qj1, true);
            } else {
                d().v().a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    @DexIgnore
    public final void b(Activity activity) {
        qj1 d2 = d(activity);
        this.e = this.d;
        this.d = null;
        a().a((Runnable) new uj1(this, d2));
    }

    @DexIgnore
    public final void c(Activity activity) {
        a(activity, d(activity), false);
        ag1 n = n();
        n.a().a((Runnable) new cj1(n, n.c().c()));
    }

    @DexIgnore
    public final qj1 d(Activity activity) {
        bk0.a(activity);
        qj1 qj1 = this.f.get(activity);
        if (qj1 != null) {
            return qj1;
        }
        qj1 qj12 = new qj1((String) null, a(activity.getClass().getCanonicalName()), j().s());
        this.f.put(activity, qj12);
        return qj12;
    }

    @DexIgnore
    public final boolean x() {
        return false;
    }

    @DexIgnore
    public final void b(Activity activity, Bundle bundle) {
        if (bundle != null) {
            qj1 qj1 = this.f.get(activity);
            if (qj1 != null) {
                Bundle bundle2 = new Bundle();
                bundle2.putLong("id", qj1.c);
                bundle2.putString("name", qj1.a);
                bundle2.putString("referrer_name", qj1.b);
                bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
            }
        }
    }

    @DexIgnore
    public final void a(Activity activity, qj1 qj1, boolean z) {
        qj1 qj12 = this.d == null ? this.e : this.d;
        if (qj1.b == null) {
            qj1 = new qj1(qj1.a, a(activity.getClass().getCanonicalName()), qj1.c);
        }
        this.e = this.d;
        this.d = qj1;
        a().a((Runnable) new sj1(this, z, qj12, qj1));
    }

    @DexIgnore
    public final void a(qj1 qj1, boolean z) {
        n().a(c().c());
        if (t().a(qj1.d, z)) {
            qj1.d = false;
        }
    }

    @DexIgnore
    public static void a(qj1 qj1, Bundle bundle, boolean z) {
        if (bundle != null && qj1 != null && (!bundle.containsKey("_sc") || z)) {
            String str = qj1.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", qj1.b);
            bundle.putLong("_si", qj1.c);
        } else if (bundle != null && qj1 == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    @DexIgnore
    public final void a(String str, qj1 qj1) {
        e();
        synchronized (this) {
            if (this.g == null || this.g.equals(str) || qj1 != null) {
                this.g = str;
            }
        }
    }

    @DexIgnore
    public static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    @DexIgnore
    public final void a(Activity activity, Bundle bundle) {
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle("com.google.app_measurement.screen_service");
            if (bundle2 != null) {
                this.f.put(activity, new qj1(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
            }
        }
    }

    @DexIgnore
    public final void a(Activity activity) {
        this.f.remove(activity);
    }
}
