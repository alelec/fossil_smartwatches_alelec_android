package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ev0 {
    @DexIgnore
    public static /* final */ ev0 a; // = new gv0();
    @DexIgnore
    public static /* final */ ev0 b; // = new hv0();

    @DexIgnore
    public ev0() {
    }

    @DexIgnore
    public static ev0 a() {
        return a;
    }

    @DexIgnore
    public static ev0 b() {
        return b;
    }

    @DexIgnore
    public abstract void a(Object obj, long j);

    @DexIgnore
    public abstract <L> void a(Object obj, Object obj2, long j);
}
