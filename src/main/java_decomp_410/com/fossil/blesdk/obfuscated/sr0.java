package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sr0 implements IInterface {
    @DexIgnore
    public /* final */ IBinder e;
    @DexIgnore
    public /* final */ String f;

    @DexIgnore
    public sr0(IBinder iBinder, String str) {
        this.e = iBinder;
        this.f = str;
    }

    @DexIgnore
    public final Parcel a(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.e.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e2) {
            throw e2;
        } finally {
            parcel.recycle();
        }
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.e;
    }

    @DexIgnore
    public final Parcel o() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f);
        return obtain;
    }
}
