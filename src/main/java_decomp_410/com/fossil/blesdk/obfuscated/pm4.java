package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pm4 extends no4 {
    @DexIgnore
    public boolean f;

    @DexIgnore
    public pm4(xo4 xo4) {
        super(xo4);
    }

    @DexIgnore
    public void a(jo4 jo4, long j) throws IOException {
        if (this.f) {
            jo4.skip(j);
            return;
        }
        try {
            super.a(jo4, j);
        } catch (IOException e) {
            this.f = true;
            a(e);
        }
    }

    @DexIgnore
    public void a(IOException iOException) {
        throw null;
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.f) {
            try {
                super.close();
            } catch (IOException e) {
                this.f = true;
                a(e);
            }
        }
    }

    @DexIgnore
    public void flush() throws IOException {
        if (!this.f) {
            try {
                super.flush();
            } catch (IOException e) {
                this.f = true;
                a(e);
            }
        }
    }
}
