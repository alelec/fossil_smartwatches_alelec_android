package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Marker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class x14 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public DisplayMetrics c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public String i;
    @DexIgnore
    public String j;
    @DexIgnore
    public String k;
    @DexIgnore
    public int l;
    @DexIgnore
    public String m;
    @DexIgnore
    public String n;
    @DexIgnore
    public Context o;
    @DexIgnore
    public String p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;

    @DexIgnore
    public x14(Context context) {
        this.b = "2.0.3";
        this.d = Build.VERSION.SDK_INT;
        this.e = Build.MODEL;
        this.f = Build.MANUFACTURER;
        this.g = Locale.getDefault().getLanguage();
        this.l = 0;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.o = context.getApplicationContext();
        this.c = e24.g(this.o);
        this.a = e24.m(this.o);
        this.h = h04.d(this.o);
        this.i = e24.l(this.o);
        this.j = TimeZone.getDefault().getID();
        this.l = e24.r(this.o);
        this.k = e24.s(this.o);
        this.m = this.o.getPackageName();
        if (this.d >= 14) {
            this.p = e24.y(this.o);
        }
        this.q = e24.x(this.o).toString();
        this.r = e24.w(this.o);
        this.s = e24.d();
        this.n = e24.b(this.o);
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        String str2;
        if (thread == null) {
            if (this.c != null) {
                jSONObject.put("sr", this.c.widthPixels + Marker.ANY_MARKER + this.c.heightPixels);
                jSONObject.put("dpi", this.c.xdpi + Marker.ANY_MARKER + this.c.ydpi);
            }
            if (t04.a(this.o).e()) {
                JSONObject jSONObject2 = new JSONObject();
                j24.a(jSONObject2, "bs", j24.d(this.o));
                j24.a(jSONObject2, "ss", j24.e(this.o));
                if (jSONObject2.length() > 0) {
                    j24.a(jSONObject, "wf", jSONObject2.toString());
                }
            }
            JSONArray a2 = j24.a(this.o, 10);
            if (a2 != null && a2.length() > 0) {
                j24.a(jSONObject, "wflist", a2.toString());
            }
            str = this.p;
            str2 = "sen";
        } else {
            j24.a(jSONObject, "thn", thread.getName());
            j24.a(jSONObject, "qq", h04.f(this.o));
            j24.a(jSONObject, "cui", h04.c(this.o));
            if (e24.c(this.r) && this.r.split(ZendeskConfig.SLASH).length == 2) {
                j24.a(jSONObject, "fram", this.r.split(ZendeskConfig.SLASH)[0]);
            }
            if (e24.c(this.s) && this.s.split(ZendeskConfig.SLASH).length == 2) {
                j24.a(jSONObject, "from", this.s.split(ZendeskConfig.SLASH)[0]);
            }
            if (g14.b(this.o).a(this.o) != null) {
                jSONObject.put("ui", g14.b(this.o).a(this.o).b());
            }
            str = h04.e(this.o);
            str2 = "mid";
        }
        j24.a(jSONObject, str2, str);
        j24.a(jSONObject, "pcn", e24.t(this.o));
        j24.a(jSONObject, "osn", Build.VERSION.RELEASE);
        j24.a(jSONObject, "av", this.a);
        j24.a(jSONObject, "ch", this.h);
        j24.a(jSONObject, "mf", this.f);
        j24.a(jSONObject, "sv", this.b);
        j24.a(jSONObject, "osd", Build.DISPLAY);
        j24.a(jSONObject, "prod", Build.PRODUCT);
        j24.a(jSONObject, "tags", Build.TAGS);
        j24.a(jSONObject, "id", Build.ID);
        j24.a(jSONObject, "fng", Build.FINGERPRINT);
        j24.a(jSONObject, "lch", this.n);
        j24.a(jSONObject, "ov", Integer.toString(this.d));
        jSONObject.put("os", 1);
        j24.a(jSONObject, "op", this.i);
        j24.a(jSONObject, "lg", this.g);
        j24.a(jSONObject, "md", this.e);
        j24.a(jSONObject, "tz", this.j);
        int i2 = this.l;
        if (i2 != 0) {
            jSONObject.put("jb", i2);
        }
        j24.a(jSONObject, "sd", this.k);
        j24.a(jSONObject, "apn", this.m);
        j24.a(jSONObject, "cpu", this.q);
        j24.a(jSONObject, "abi", Build.CPU_ABI);
        j24.a(jSONObject, "abi2", Build.CPU_ABI2);
        j24.a(jSONObject, "ram", this.r);
        j24.a(jSONObject, "rom", this.s);
    }
}
