package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilNotificationImageView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pt2 extends RecyclerView.g<c> {
    @DexIgnore
    public List<ContactGroup> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ rv c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(ContactGroup contactGroup);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ uh2 a;
        @DexIgnore
        public /* final */ /* synthetic */ pt2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b = this.e.b.b;
                if (b != null) {
                    b.a();
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public b(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    b b = this.e.b.b;
                    if (b != null) {
                        b.a((ContactGroup) this.e.b.a.get(adapterPosition));
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pt2$c$c")
        /* renamed from: com.fossil.blesdk.obfuscated.pt2$c$c  reason: collision with other inner class name */
        public static final class C0100c implements qv<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ c e;
            @DexIgnore
            public /* final */ /* synthetic */ ContactGroup f;

            @DexIgnore
            public C0100c(c cVar, ContactGroup contactGroup) {
                this.e = cVar;
                this.f = contactGroup;
            }

            @DexIgnore
            public boolean a(GlideException glideException, Object obj, bw<Drawable> bwVar, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onLoadFailed");
                Contact contact = this.f.getContacts().get(0);
                kd4.a((Object) contact, "contactGroup.contacts[0]");
                contact.setPhotoThumbUri((String) null);
                return false;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, bw<Drawable> bwVar, DataSource dataSource, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onResourceReady");
                this.e.a.t.setImageDrawable(drawable);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pt2 pt2, uh2 uh2) {
            super(uh2.d());
            kd4.b(uh2, "binding");
            this.b = pt2;
            this.a = uh2;
            this.a.q.setOnClickListener(new a(this));
            this.a.s.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void a(ContactGroup contactGroup) {
            Uri uri;
            kd4.b(contactGroup, "contactGroup");
            FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "set favorite contact data");
            if (contactGroup.getContacts().size() > 0) {
                Contact contact = contactGroup.getContacts().get(0);
                kd4.a((Object) contact, "contactGroup.contacts[0]");
                if (!TextUtils.isEmpty(contact.getPhotoThumbUri())) {
                    Contact contact2 = contactGroup.getContacts().get(0);
                    kd4.a((Object) contact2, "contactGroup.contacts[0]");
                    uri = Uri.parse(contact2.getPhotoThumbUri());
                } else {
                    uri = null;
                }
                FossilNotificationImageView fossilNotificationImageView = this.a.t;
                kd4.a((Object) fossilNotificationImageView, "binding.ivFavoriteContactIcon");
                fk2 a2 = ck2.a(fossilNotificationImageView.getContext());
                Contact contact3 = contactGroup.getContacts().get(0);
                kd4.a((Object) contact3, "contactGroup.contacts[0]");
                ek2<Drawable> a3 = a2.a((Object) new bk2(uri, contact3.getDisplayName())).a(true).a(pp.a).a((lv<?>) this.b.c);
                FossilNotificationImageView fossilNotificationImageView2 = this.a.t;
                kd4.a((Object) fossilNotificationImageView2, "binding.ivFavoriteContactIcon");
                fk2 a4 = ck2.a(fossilNotificationImageView2.getContext());
                Contact contact4 = contactGroup.getContacts().get(0);
                kd4.a((Object) contact4, "contactGroup.contacts[0]");
                ek2<Drawable> b2 = a3.a((wn<Drawable>) a4.a((Object) new bk2((Uri) null, contact4.getDisplayName())).a((lv<?>) this.b.c)).b(new C0100c(this, contactGroup));
                FossilNotificationImageView fossilNotificationImageView3 = this.a.t;
                kd4.a((Object) fossilNotificationImageView3, "binding.ivFavoriteContactIcon");
                b2.a((ImageView) fossilNotificationImageView3.getFossilCircleImageView());
                Contact contact5 = contactGroup.getContacts().get(0);
                kd4.a((Object) contact5, "contactGroup.contacts[0]");
                if (TextUtils.isEmpty(contact5.getPhotoThumbUri())) {
                    this.a.t.a();
                    FossilNotificationImageView fossilNotificationImageView4 = this.a.t;
                    kd4.a((Object) fossilNotificationImageView4, "binding.ivFavoriteContactIcon");
                    fossilNotificationImageView4.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.oval_solid_light_grey));
                } else {
                    this.a.t.b();
                    this.a.t.setBackgroundResource(R.color.transparent);
                }
                FlexibleTextView flexibleTextView = this.a.r;
                kd4.a((Object) flexibleTextView, "binding.ftvFavoriteContactName");
                Contact contact6 = contactGroup.getContacts().get(0);
                kd4.a((Object) contact6, "contactGroup.contacts[0]");
                flexibleTextView.setText(contact6.getDisplayName());
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public pt2() {
        lv a2 = new rv().a((oo<Bitmap>) new nk2());
        kd4.a((Object) a2, "RequestOptions().transform(CircleTransform())");
        this.c = (rv) a2;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        kd4.b(cVar, "holder");
        cVar.a(this.a.get(i));
    }

    @DexIgnore
    public final List<ContactGroup> b() {
        return this.a;
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        uh2 a2 = uh2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        kd4.a((Object) a2, "ItemFavoriteContactNotif\u2026.context), parent, false)");
        return new c(this, a2);
    }

    @DexIgnore
    public final void a(List<ContactGroup> list) {
        kd4.b(list, "listContactGroup");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0061, code lost:
        if (r3 == r5.getContactId()) goto L_0x0065;
     */
    @DexIgnore
    public final void a(ContactGroup contactGroup) {
        kd4.b(contactGroup, "contactGroup");
        Iterator<ContactGroup> it = this.a.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            }
            ContactGroup next = it.next();
            List<Contact> contacts = next.getContacts();
            kd4.a((Object) contacts, "it.contacts");
            boolean z = true;
            if (!contacts.isEmpty()) {
                List<Contact> contacts2 = contactGroup.getContacts();
                kd4.a((Object) contacts2, "contactGroup.contacts");
                if (!contacts2.isEmpty()) {
                    Contact contact = next.getContacts().get(0);
                    kd4.a((Object) contact, "it.contacts[0]");
                    int contactId = contact.getContactId();
                    Contact contact2 = contactGroup.getContacts().get(0);
                    kd4.a((Object) contact2, "contactGroup.contacts[0]");
                }
            }
            z = false;
            if (z) {
                break;
            }
            i++;
        }
        if (i != -1) {
            this.a.remove(i);
            notifyItemRemoved(i);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.b = bVar;
    }
}
