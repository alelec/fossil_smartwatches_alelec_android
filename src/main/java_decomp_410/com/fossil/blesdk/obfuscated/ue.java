package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ue {
    @DexIgnore
    void a(int i, int i2);

    @DexIgnore
    void a(int i, int i2, Object obj);

    @DexIgnore
    void b(int i, int i2);

    @DexIgnore
    void c(int i, int i2);
}
