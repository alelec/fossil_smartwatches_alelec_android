package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e4 */
public class C1702e4 extends android.graphics.drawable.Drawable {

    @DexIgnore
    /* renamed from: a */
    public float f4688a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.graphics.Paint f4689b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.graphics.RectF f4690c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.graphics.Rect f4691d;

    @DexIgnore
    /* renamed from: e */
    public float f4692e;

    @DexIgnore
    /* renamed from: f */
    public boolean f4693f; // = false;

    @DexIgnore
    /* renamed from: g */
    public boolean f4694g; // = true;

    @DexIgnore
    /* renamed from: h */
    public android.content.res.ColorStateList f4695h;

    @DexIgnore
    /* renamed from: i */
    public android.graphics.PorterDuffColorFilter f4696i;

    @DexIgnore
    /* renamed from: j */
    public android.content.res.ColorStateList f4697j;

    @DexIgnore
    /* renamed from: k */
    public android.graphics.PorterDuff.Mode f4698k; // = android.graphics.PorterDuff.Mode.SRC_IN;

    @DexIgnore
    public C1702e4(android.content.res.ColorStateList colorStateList, float f) {
        this.f4688a = f;
        this.f4689b = new android.graphics.Paint(5);
        mo10308a(colorStateList);
        this.f4690c = new android.graphics.RectF();
        this.f4691d = new android.graphics.Rect();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10308a(android.content.res.ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = android.content.res.ColorStateList.valueOf(0);
        }
        this.f4695h = colorStateList;
        this.f4689b.setColor(this.f4695h.getColorForState(getState(), this.f4695h.getDefaultColor()));
    }

    @DexIgnore
    /* renamed from: b */
    public float mo10310b() {
        return this.f4692e;
    }

    @DexIgnore
    /* renamed from: c */
    public float mo10312c() {
        return this.f4688a;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        boolean z;
        android.graphics.Paint paint = this.f4689b;
        if (this.f4696i == null || paint.getColorFilter() != null) {
            z = false;
        } else {
            paint.setColorFilter(this.f4696i);
            z = true;
        }
        android.graphics.RectF rectF = this.f4690c;
        float f = this.f4688a;
        canvas.drawRoundRect(rectF, f, f, paint);
        if (z) {
            paint.setColorFilter((android.graphics.ColorFilter) null);
        }
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public void getOutline(android.graphics.Outline outline) {
        outline.setRoundRect(this.f4691d, this.f4688a);
    }

    @DexIgnore
    public boolean isStateful() {
        android.content.res.ColorStateList colorStateList = this.f4697j;
        if (colorStateList == null || !colorStateList.isStateful()) {
            android.content.res.ColorStateList colorStateList2 = this.f4695h;
            if ((colorStateList2 == null || !colorStateList2.isStateful()) && !super.isStateful()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        super.onBoundsChange(rect);
        mo10309a(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        android.content.res.ColorStateList colorStateList = this.f4695h;
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        boolean z = colorForState != this.f4689b.getColor();
        if (z) {
            this.f4689b.setColor(colorForState);
        }
        android.content.res.ColorStateList colorStateList2 = this.f4697j;
        if (colorStateList2 != null) {
            android.graphics.PorterDuff.Mode mode = this.f4698k;
            if (mode != null) {
                this.f4696i = mo10305a(colorStateList2, mode);
                return true;
            }
        }
        return z;
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.f4689b.setAlpha(i);
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        this.f4689b.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        this.f4697j = colorStateList;
        this.f4696i = mo10305a(this.f4697j, this.f4698k);
        invalidateSelf();
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        this.f4698k = mode;
        this.f4696i = mo10305a(this.f4697j, this.f4698k);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10311b(android.content.res.ColorStateList colorStateList) {
        mo10308a(colorStateList);
        invalidateSelf();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10307a(float f, boolean z, boolean z2) {
        if (f != this.f4692e || this.f4693f != z || this.f4694g != z2) {
            this.f4692e = f;
            this.f4693f = z;
            this.f4694g = z2;
            mo10309a((android.graphics.Rect) null);
            invalidateSelf();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10309a(android.graphics.Rect rect) {
        if (rect == null) {
            rect = getBounds();
        }
        this.f4690c.set((float) rect.left, (float) rect.top, (float) rect.right, (float) rect.bottom);
        this.f4691d.set(rect);
        if (this.f4693f) {
            float b = com.fossil.blesdk.obfuscated.C1768f4.m6709b(this.f4692e, this.f4688a, this.f4694g);
            this.f4691d.inset((int) java.lang.Math.ceil((double) com.fossil.blesdk.obfuscated.C1768f4.m6708a(this.f4692e, this.f4688a, this.f4694g)), (int) java.lang.Math.ceil((double) b));
            this.f4690c.set(this.f4691d);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10306a(float f) {
        if (f != this.f4688a) {
            this.f4688a = f;
            mo10309a((android.graphics.Rect) null);
            invalidateSelf();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.content.res.ColorStateList mo10304a() {
        return this.f4695h;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.PorterDuffColorFilter mo10305a(android.content.res.ColorStateList colorStateList, android.graphics.PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new android.graphics.PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }
}
