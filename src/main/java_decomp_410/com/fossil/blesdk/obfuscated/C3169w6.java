package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w6 */
public class C3169w6 extends com.fossil.blesdk.obfuscated.C1394a7 {
    @DexIgnore
    /* renamed from: a */
    public final java.io.File mo17303a(android.os.ParcelFileDescriptor parcelFileDescriptor) {
        try {
            java.lang.String readlink = android.system.Os.readlink("/proc/self/fd/" + parcelFileDescriptor.getFd());
            if (android.system.OsConstants.S_ISREG(android.system.Os.stat(readlink).st_mode)) {
                return new java.io.File(readlink);
            }
        } catch (android.system.ErrnoException unused) {
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0047, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x004c, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r4.addSuppressed(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0050, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0053, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0054, code lost:
        if (r5 != null) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x005e, code lost:
        throw r6;
     */
    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8577a(android.content.Context context, android.os.CancellationSignal cancellationSignal, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, int i) {
        if (fVarArr.length < 1) {
            return null;
        }
        com.fossil.blesdk.obfuscated.C3012u7.C3018f a = mo8581a(fVarArr, i);
        try {
            android.os.ParcelFileDescriptor openFileDescriptor = context.getContentResolver().openFileDescriptor(a.mo16677c(), "r", cancellationSignal);
            java.io.File a2 = mo17303a(openFileDescriptor);
            if (a2 != null) {
                if (a2.canRead()) {
                    android.graphics.Typeface createFromFile = android.graphics.Typeface.createFromFile(a2);
                    if (openFileDescriptor != null) {
                        openFileDescriptor.close();
                    }
                    return createFromFile;
                }
            }
            java.io.FileInputStream fileInputStream = new java.io.FileInputStream(openFileDescriptor.getFileDescriptor());
            android.graphics.Typeface a3 = super.mo8579a(context, (java.io.InputStream) fileInputStream);
            fileInputStream.close();
            if (openFileDescriptor != null) {
                openFileDescriptor.close();
            }
            return a3;
        } catch (java.io.IOException unused) {
            return null;
        } catch (Throwable th) {
            r4.addSuppressed(th);
        }
    }
}
