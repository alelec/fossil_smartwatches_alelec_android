package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rj0 {
    @DexIgnore
    public rj0(String str, String str2) {
        bk0.a(str, (Object) "log tag cannot be null");
        bk0.a(str.length() <= 23, "tag \"%s\" is longer than the %d character maximum", str, 23);
        if (str2 == null || str2.length() <= 0) {
        }
    }

    @DexIgnore
    public rj0(String str) {
        this(str, (String) null);
    }
}
