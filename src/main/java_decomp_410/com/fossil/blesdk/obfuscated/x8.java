package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewParent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class x8 {
    @DexIgnore
    public ViewParent a;
    @DexIgnore
    public ViewParent b;
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int[] e;

    @DexIgnore
    public x8(View view) {
        this.c = view;
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.d) {
            f9.E(this.c);
        }
        this.d = z;
    }

    @DexIgnore
    public boolean b() {
        return this.d;
    }

    @DexIgnore
    public boolean c(int i) {
        return a(i, 0);
    }

    @DexIgnore
    public void d(int i) {
        ViewParent a2 = a(i);
        if (a2 != null) {
            i9.a(a2, this.c, i);
            a(i, (ViewParent) null);
        }
    }

    @DexIgnore
    public boolean b(int i) {
        return a(i) != null;
    }

    @DexIgnore
    public void c() {
        d(0);
    }

    @DexIgnore
    public boolean a() {
        return b(0);
    }

    @DexIgnore
    public boolean a(int i, int i2) {
        if (b(i2)) {
            return true;
        }
        if (!b()) {
            return false;
        }
        View view = this.c;
        for (ViewParent parent = this.c.getParent(); parent != null; parent = parent.getParent()) {
            if (i9.b(parent, view, this.c, i, i2)) {
                a(i2, parent);
                i9.a(parent, view, this.c, i, i2);
                return true;
            }
            if (parent instanceof View) {
                view = (View) parent;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(int i, int i2, int i3, int i4, int[] iArr) {
        return a(i, i2, i3, i4, iArr, 0);
    }

    @DexIgnore
    public boolean a(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        int i6;
        int i7;
        int[] iArr2 = iArr;
        if (b()) {
            ViewParent a2 = a(i5);
            if (a2 == null) {
                return false;
            }
            if (i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
                if (iArr2 != null) {
                    this.c.getLocationInWindow(iArr2);
                    i7 = iArr2[0];
                    i6 = iArr2[1];
                } else {
                    i7 = 0;
                    i6 = 0;
                }
                i9.a(a2, this.c, i, i2, i3, i4, i5);
                if (iArr2 != null) {
                    this.c.getLocationInWindow(iArr2);
                    iArr2[0] = iArr2[0] - i7;
                    iArr2[1] = iArr2[1] - i6;
                }
                return true;
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(int i, int i2, int[] iArr, int[] iArr2) {
        return a(i, i2, iArr, iArr2, 0);
    }

    @DexIgnore
    public boolean a(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        int i4;
        int i5;
        if (b()) {
            ViewParent a2 = a(i3);
            if (a2 == null) {
                return false;
            }
            if (i != 0 || i2 != 0) {
                if (iArr2 != null) {
                    this.c.getLocationInWindow(iArr2);
                    i5 = iArr2[0];
                    i4 = iArr2[1];
                } else {
                    i5 = 0;
                    i4 = 0;
                }
                if (iArr == null) {
                    if (this.e == null) {
                        this.e = new int[2];
                    }
                    iArr = this.e;
                }
                iArr[0] = 0;
                iArr[1] = 0;
                i9.a(a2, this.c, i, i2, iArr, i3);
                if (iArr2 != null) {
                    this.c.getLocationInWindow(iArr2);
                    iArr2[0] = iArr2[0] - i5;
                    iArr2[1] = iArr2[1] - i4;
                }
                if (iArr[0] == 0 && iArr[1] == 0) {
                    return false;
                }
                return true;
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(float f, float f2, boolean z) {
        if (b()) {
            ViewParent a2 = a(0);
            if (a2 != null) {
                return i9.a(a2, this.c, f, f2, z);
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(float f, float f2) {
        if (b()) {
            ViewParent a2 = a(0);
            if (a2 != null) {
                return i9.a(a2, this.c, f, f2);
            }
        }
        return false;
    }

    @DexIgnore
    public final ViewParent a(int i) {
        if (i == 0) {
            return this.a;
        }
        if (i != 1) {
            return null;
        }
        return this.b;
    }

    @DexIgnore
    public final void a(int i, ViewParent viewParent) {
        if (i == 0) {
            this.a = viewParent;
        } else if (i == 1) {
            this.b = viewParent;
        }
    }
}
