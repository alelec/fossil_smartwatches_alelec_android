package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface je1 extends IInterface {
    @DexIgnore
    g51 a(kf1 kf1) throws RemoteException;

    @DexIgnore
    void a(sn0 sn0) throws RemoteException;

    @DexIgnore
    void b(sn0 sn0) throws RemoteException;

    @DexIgnore
    void clear() throws RemoteException;

    @DexIgnore
    oe1 m() throws RemoteException;
}
