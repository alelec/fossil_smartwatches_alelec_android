package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.logic.phase.Phase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class u00 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Phase.Result.ResultCode.values().length];

    /*
    static {
        a[Phase.Result.ResultCode.INTERRUPTED.ordinal()] = 1;
        a[Phase.Result.ResultCode.CONNECTION_DROPPED.ordinal()] = 2;
        a[Phase.Result.ResultCode.INCOMPATIBLE_FIRMWARE.ordinal()] = 3;
        a[Phase.Result.ResultCode.REQUEST_ERROR.ordinal()] = 4;
        a[Phase.Result.ResultCode.LACK_OF_SERVICE.ordinal()] = 5;
        a[Phase.Result.ResultCode.LACK_OF_CHARACTERISTIC.ordinal()] = 6;
        a[Phase.Result.ResultCode.INVALID_SERIAL_NUMBER.ordinal()] = 7;
        a[Phase.Result.ResultCode.DATA_TRANSFER_RETRY_REACH_THRESHOLD.ordinal()] = 8;
        a[Phase.Result.ResultCode.EXCHANGED_VALUE_NOT_SATISFIED.ordinal()] = 9;
        a[Phase.Result.ResultCode.INVALID_FILE_LENGTH.ordinal()] = 10;
        a[Phase.Result.ResultCode.MISMATCH_VERSION.ordinal()] = 11;
        a[Phase.Result.ResultCode.INVALID_FILE_CRC.ordinal()] = 12;
        a[Phase.Result.ResultCode.INVALID_RESPONSE.ordinal()] = 13;
        a[Phase.Result.ResultCode.INVALID_DATA_LENGTH.ordinal()] = 14;
        a[Phase.Result.ResultCode.INCORRECT_FILE_DATA.ordinal()] = 15;
        a[Phase.Result.ResultCode.NOT_ENOUGH_FILE_TO_PROCESS.ordinal()] = 16;
        a[Phase.Result.ResultCode.WAITING_FOR_EXECUTION_TIMEOUT.ordinal()] = 17;
        a[Phase.Result.ResultCode.REQUEST_UNSUPPORTED.ordinal()] = 18;
        a[Phase.Result.ResultCode.UNSUPPORTED_FILE_HANDLE.ordinal()] = 19;
        a[Phase.Result.ResultCode.BLUETOOTH_OFF.ordinal()] = 20;
        a[Phase.Result.ResultCode.AUTHENTICATION_FAILED.ordinal()] = 21;
        a[Phase.Result.ResultCode.WRONG_RANDOM_NUMBER.ordinal()] = 22;
        a[Phase.Result.ResultCode.SECRET_KEY_IS_REQUIRED.ordinal()] = 23;
        a[Phase.Result.ResultCode.INVALID_PARAMETER.ordinal()] = 24;
        a[Phase.Result.ResultCode.HID_INPUT_DEVICE_DISABLED.ordinal()] = 25;
        a[Phase.Result.ResultCode.NOT_ALLOW_TO_START.ordinal()] = 26;
        a[Phase.Result.ResultCode.DATABASE_ERROR.ordinal()] = 27;
        a[Phase.Result.ResultCode.FLOW_BROKEN.ordinal()] = 28;
        a[Phase.Result.ResultCode.SUCCESS.ordinal()] = 29;
        a[Phase.Result.ResultCode.NOT_START.ordinal()] = 30;
    }
    */
}
