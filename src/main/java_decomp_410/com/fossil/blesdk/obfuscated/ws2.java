package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ws2 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<c> a;
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ int c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(c cVar);

        @DexIgnore
        void b(c cVar);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public List<String> d;
        @DexIgnore
        public String e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public c() {
            this(0, (String) null, (String) null, (List) null, (String) null, false, 63, (fd4) null);
        }

        @DexIgnore
        public c(int i, String str, String str2, List<String> list, String str3, boolean z) {
            kd4.b(str, "shortDescription");
            kd4.b(str2, "longDescription");
            kd4.b(list, "permsSet");
            this.a = i;
            this.b = str;
            this.c = str2;
            this.d = list;
            this.e = str3;
            this.f = z;
        }

        @DexIgnore
        public final String a() {
            return this.e;
        }

        @DexIgnore
        public final boolean b() {
            return this.f;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.a;
        }

        @DexIgnore
        public final List<String> e() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if ((this.a == cVar.a) && kd4.a((Object) this.b, (Object) cVar.b) && kd4.a((Object) this.c, (Object) cVar.c) && kd4.a((Object) this.d, (Object) cVar.d) && kd4.a((Object) this.e, (Object) cVar.e)) {
                        if (this.f == cVar.f) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final String f() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a * 31;
            String str = this.b;
            int i2 = 0;
            int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.c;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            List<String> list = this.d;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            String str3 = this.e;
            if (str3 != null) {
                i2 = str3.hashCode();
            }
            int i3 = (hashCode3 + i2) * 31;
            boolean z = this.f;
            if (z) {
                z = true;
            }
            return i3 + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "PermissionModel(permsId=" + this.a + ", shortDescription=" + this.b + ", longDescription=" + this.c + ", permsSet=" + this.d + ", extraLink=" + this.e + ", granted=" + this.f + ")";
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ c(int i, String str, String str2, List list, String str3, boolean z, int i2, fd4 fd4) {
            this(r13, (i2 & 2) != 0 ? r1 : str, (i2 & 4) == 0 ? str2 : r1, (i2 & 8) != 0 ? new ArrayList() : list, (i2 & 16) != 0 ? null : str3, (i2 & 32) != 0 ? false : z);
            int i3 = (i2 & 1) != 0 ? 0 : i;
            String str4 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.f = z;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ mi2 a;
        @DexIgnore
        public /* final */ /* synthetic */ ws2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d e;

            @DexIgnore
            public a(d dVar) {
                this.e = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.E;
                    ImageButton imageButton = this.e.a.s;
                    kd4.a((Object) imageButton, "binding.ibInfo");
                    Context context = imageButton.getContext();
                    kd4.a((Object) context, "binding.ibInfo.context");
                    List b = this.e.b.a;
                    if (b != null) {
                        String a = ((c) b.get(adapterPosition)).a();
                        if (a != null) {
                            aVar.a(context, "", a);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d e;

            @DexIgnore
            public b(d dVar) {
                this.e = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.e.b.a;
                    if (b != null) {
                        c cVar = (c) b.get(adapterPosition);
                        if (bn2.d.c(cVar.d())) {
                            b a = this.e.b.b;
                            if (a != null) {
                                a.a(cVar);
                                return;
                            }
                            return;
                        }
                        b a2 = this.e.b.b;
                        if (a2 != null) {
                            a2.b(cVar);
                            return;
                        }
                        return;
                    }
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ws2 ws2, mi2 mi2) {
            super(mi2.d());
            kd4.b(mi2, "binding");
            this.b = ws2;
            this.a = mi2;
            this.a.s.setOnClickListener(new a(this));
            this.a.q.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void a(c cVar) {
            kd4.b(cVar, "permissionModel");
            FlexibleTextView flexibleTextView = this.a.r;
            kd4.a((Object) flexibleTextView, "binding.ftvDescription");
            flexibleTextView.setText(cVar.f());
            FlexibleButton flexibleButton = this.a.q;
            kd4.a((Object) flexibleButton, "binding.fbGrantPermission");
            flexibleButton.setEnabled(!cVar.b());
            ImageView imageView = this.a.t;
            kd4.a((Object) imageView, "binding.ivCheck");
            int i = 0;
            imageView.setVisibility(cVar.b() ? 0 : 4);
            ImageButton imageButton = this.a.s;
            kd4.a((Object) imageButton, "binding.ibInfo");
            if (cVar.a() == null) {
                i = 4;
            }
            imageButton.setVisibility(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ii2 a;
        @DexIgnore
        public /* final */ /* synthetic */ ws2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e e;

            @DexIgnore
            public a(e eVar) {
                this.e = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.E;
                    ImageButton imageButton = this.e.a.s;
                    kd4.a((Object) imageButton, "binding.ibInfo");
                    Context context = imageButton.getContext();
                    kd4.a((Object) context, "binding.ibInfo.context");
                    List b = this.e.b.a;
                    if (b != null) {
                        String a = ((c) b.get(adapterPosition)).a();
                        if (a != null) {
                            aVar.a(context, "", a);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e e;

            @DexIgnore
            public b(e eVar) {
                this.e = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.e.b.a;
                    if (b != null) {
                        c cVar = (c) b.get(adapterPosition);
                        if (bn2.d.c(cVar.d())) {
                            b a = this.e.b.b;
                            if (a != null) {
                                a.a(cVar);
                                return;
                            }
                            return;
                        }
                        b a2 = this.e.b.b;
                        if (a2 != null) {
                            a2.b(cVar);
                            return;
                        }
                        return;
                    }
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ws2 ws2, ii2 ii2) {
            super(ii2.d());
            kd4.b(ii2, "binding");
            this.b = ws2;
            this.a = ii2;
            this.a.s.setOnClickListener(new a(this));
            this.a.q.setOnClickListener(new b(this));
        }

        @DexIgnore
        public final void a(c cVar) {
            kd4.b(cVar, "permissionModel");
            FlexibleTextView flexibleTextView = this.a.r;
            kd4.a((Object) flexibleTextView, "binding.ftvDescription");
            flexibleTextView.setText(cVar.f());
            FlexibleButton flexibleButton = this.a.q;
            kd4.a((Object) flexibleButton, "binding.fbGrantPermission");
            flexibleButton.setEnabled(!cVar.b());
            ImageView imageView = this.a.t;
            kd4.a((Object) imageView, "binding.ivCheck");
            int i = 0;
            imageView.setVisibility(cVar.b() ? 0 : 4);
            ImageButton imageButton = this.a.s;
            kd4.a((Object) imageButton, "binding.ibInfo");
            if (cVar.a() == null) {
                i = 4;
            }
            imageButton.setVisibility(i);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ws2(int i) {
        this.c = i;
    }

    @DexIgnore
    public int getItemCount() {
        List<c> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        kd4.b(viewHolder, "holder");
        if (viewHolder instanceof e) {
            e eVar = (e) viewHolder;
            List<c> list = this.a;
            if (list != null) {
                eVar.a(list.get(i));
            } else {
                kd4.a();
                throw null;
            }
        } else {
            d dVar = (d) viewHolder;
            List<c> list2 = this.a;
            if (list2 != null) {
                dVar.a(list2.get(i));
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        if (this.c != 1) {
            mi2 a2 = mi2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
            kd4.a((Object) a2, "ItemSinglePermissionBind\u2026.context), parent, false)");
            return new d(this, a2);
        }
        ii2 a3 = ii2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        kd4.a((Object) a3, "ItemPermissionBinding.in\u2026.context), parent, false)");
        return new e(this, a3);
    }

    @DexIgnore
    public final void a(List<c> list) {
        kd4.b(list, "permissionList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.b = bVar;
    }
}
