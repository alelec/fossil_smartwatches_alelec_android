package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wa0 {
    @DexIgnore
    public static final JSONObject a(JSONObject jSONObject, JSONKey jSONKey, Object obj) {
        kd4.b(jSONObject, "$this$put");
        kd4.b(jSONKey, "key");
        JSONObject put = jSONObject.put(jSONKey.getLogName$blesdk_productionRelease(), obj);
        kd4.a((Object) put, "this.put(key.logName, value)");
        return put;
    }
}
