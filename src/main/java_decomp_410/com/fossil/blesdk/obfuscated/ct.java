package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ct implements mo<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ os a; // = new os();

    @DexIgnore
    public boolean a(InputStream inputStream, lo loVar) throws IOException {
        return true;
    }

    @DexIgnore
    public aq<Bitmap> a(InputStream inputStream, int i, int i2, lo loVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(kw.a(inputStream)), i, i2, loVar);
    }
}
