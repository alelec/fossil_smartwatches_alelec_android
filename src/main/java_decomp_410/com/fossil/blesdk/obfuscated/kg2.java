package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kg2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kg2(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, FlexibleTextView flexibleTextView2, CardView cardView, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, ImageView imageView2, View view2, View view3, View view4, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, FlexibleTextView flexibleTextView16) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = flexibleTextView2;
        this.t = flexibleButton;
        this.u = flexibleButton2;
        this.v = flexibleButton3;
        this.w = imageView2;
        this.x = flexibleTextView3;
        this.y = flexibleTextView5;
        this.z = flexibleTextView6;
        this.A = flexibleTextView7;
        this.B = flexibleTextView9;
        this.C = flexibleTextView11;
        this.D = flexibleTextView12;
        this.E = flexibleTextView14;
        this.F = flexibleTextView16;
    }
}
