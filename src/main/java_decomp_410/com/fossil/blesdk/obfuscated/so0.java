package com.fossil.blesdk.obfuscated;

import android.util.SparseArray;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class so0 implements yb0, de0.d.b {
    @DexIgnore
    public /* final */ SparseArray<List<DataType>> e;
    @DexIgnore
    public /* final */ Set<Scope> f;
    @DexIgnore
    public /* final */ GoogleSignInAccount g;

    @DexIgnore
    public so0(SparseArray<List<DataType>> sparseArray, GoogleSignInAccount googleSignInAccount) {
        this.e = sparseArray;
        this.g = googleSignInAccount;
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < sparseArray.size(); i++) {
            int keyAt = sparseArray.keyAt(i);
            for (DataType dataType : sparseArray.valueAt(i)) {
                if (keyAt == 0 && dataType.J() != null) {
                    arrayList.add(new Scope(dataType.J()));
                } else if (keyAt == 1 && dataType.K() != null) {
                    arrayList.add(new Scope(dataType.K()));
                }
            }
        }
        this.f = oq0.a(arrayList);
    }

    @DexIgnore
    public static a c() {
        return new a();
    }

    @DexIgnore
    public GoogleSignInAccount a() {
        return this.g;
    }

    @DexIgnore
    public List<Scope> b() {
        return new ArrayList(this.f);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && so0.class == obj.getClass()) {
            so0 so0 = (so0) obj;
            return zj0.a(this.e, so0.e) && zj0.a(this.g, so0.g);
        }
    }

    @DexIgnore
    public int hashCode() {
        return zj0.a(this.e, this.g);
    }

    @DexIgnore
    public static a a(GoogleSignInAccount googleSignInAccount) {
        if (googleSignInAccount == null) {
            return new a();
        }
        a aVar = new a();
        a unused = aVar.a(googleSignInAccount);
        return aVar;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ SparseArray<List<DataType>> a;
        @DexIgnore
        public GoogleSignInAccount b;

        @DexIgnore
        public a() {
            this.a = new SparseArray<>();
        }

        @DexIgnore
        public final a a(DataType dataType, int i) {
            boolean z = true;
            if (!(i == 0 || i == 1)) {
                z = false;
            }
            bk0.a(z, (Object) "valid access types are FitnessOptions.ACCESS_READ or FitnessOptions.ACCESS_WRITE");
            List list = this.a.get(i);
            if (list == null) {
                list = new ArrayList();
                this.a.put(i, list);
            }
            list.add(dataType);
            return this;
        }

        @DexIgnore
        public final a a(GoogleSignInAccount googleSignInAccount) {
            this.b = googleSignInAccount;
            return this;
        }

        @DexIgnore
        public final so0 a() {
            return new so0(this.a, this.b);
        }
    }
}
