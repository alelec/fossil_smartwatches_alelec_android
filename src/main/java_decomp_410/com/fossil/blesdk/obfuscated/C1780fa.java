package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fa */
public class C1780fa extends android.widget.Filter {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1780fa.C1781a f5116a;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.fa$a */
    public interface C1781a {
        @DexIgnore
        /* renamed from: a */
        android.database.Cursor mo10374a();

        @DexIgnore
        /* renamed from: a */
        android.database.Cursor mo10813a(java.lang.CharSequence charSequence);

        @DexIgnore
        /* renamed from: a */
        void mo10377a(android.database.Cursor cursor);

        @DexIgnore
        /* renamed from: b */
        java.lang.CharSequence mo10380b(android.database.Cursor cursor);
    }

    @DexIgnore
    public C1780fa(com.fossil.blesdk.obfuscated.C1780fa.C1781a aVar) {
        this.f5116a = aVar;
    }

    @DexIgnore
    public java.lang.CharSequence convertResultToString(java.lang.Object obj) {
        return this.f5116a.mo10380b((android.database.Cursor) obj);
    }

    @DexIgnore
    public android.widget.Filter.FilterResults performFiltering(java.lang.CharSequence charSequence) {
        android.database.Cursor a = this.f5116a.mo10813a(charSequence);
        android.widget.Filter.FilterResults filterResults = new android.widget.Filter.FilterResults();
        if (a != null) {
            filterResults.count = a.getCount();
            filterResults.values = a;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(java.lang.CharSequence charSequence, android.widget.Filter.FilterResults filterResults) {
        android.database.Cursor a = this.f5116a.mo10374a();
        java.lang.Object obj = filterResults.values;
        if (obj != null && obj != a) {
            this.f5116a.mo10377a((android.database.Cursor) obj);
        }
    }
}
