package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.codeless.CodelessMatcher;
import io.fabric.sdk.android.InitializationException;
import io.fabric.sdk.android.services.concurrency.Priority;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class u44<Result> extends c64<Void, Void, Result> {
    @DexIgnore
    public /* final */ v44<Result> s;

    @DexIgnore
    public u44(v44<Result> v44) {
        this.s = v44;
    }

    @DexIgnore
    public void c(Result result) {
        this.s.a(result);
        this.s.h.a((Exception) new InitializationException(this.s.p() + " Initialization was cancelled"));
    }

    @DexIgnore
    public void d(Result result) {
        this.s.b(result);
        this.s.h.a(result);
    }

    @DexIgnore
    public void f() {
        super.f();
        y54 a = a("onPreExecute");
        try {
            boolean u = this.s.u();
            a.c();
            if (u) {
                return;
            }
        } catch (UnmetDependencyException e) {
            throw e;
        } catch (Exception e2) {
            q44.g().e("Fabric", "Failure onPreExecute()", e2);
            a.c();
        } catch (Throwable th) {
            a.c();
            b(true);
            throw th;
        }
        b(true);
    }

    @DexIgnore
    public Priority getPriority() {
        return Priority.HIGH;
    }

    @DexIgnore
    public Result a(Void... voidArr) {
        y54 a = a("doInBackground");
        Result k = !e() ? this.s.k() : null;
        a.c();
        return k;
    }

    @DexIgnore
    public final y54 a(String str) {
        y54 y54 = new y54(this.s.p() + CodelessMatcher.CURRENT_CLASS_NAME + str, "KitInitialization");
        y54.b();
        return y54;
    }
}
