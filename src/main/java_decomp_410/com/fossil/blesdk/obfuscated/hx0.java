package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import libcore.io.Memory;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hx0 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(hx0.class.getName());
    @DexIgnore
    public static /* final */ Unsafe b; // = d();
    @DexIgnore
    public static /* final */ Class<?> c; // = nt0.b();
    @DexIgnore
    public static /* final */ boolean d; // = c(Long.TYPE);
    @DexIgnore
    public static /* final */ boolean e; // = c(Integer.TYPE);
    @DexIgnore
    public static /* final */ d f;
    @DexIgnore
    public static /* final */ boolean g; // = f();
    @DexIgnore
    public static /* final */ boolean h; // = e();
    @DexIgnore
    public static /* final */ long i; // = ((long) a((Class<?>) byte[].class));
    @DexIgnore
    public static /* final */ long j; // = b(g());
    @DexIgnore
    public static /* final */ boolean k; // = (ByteOrder.nativeOrder() != ByteOrder.BIG_ENDIAN);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends d {
        @DexIgnore
        public a(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        public final void a(long j, byte b) {
            Memory.pokeByte((int) (j & -1), b);
        }

        @DexIgnore
        public final void a(Object obj, long j, byte b) {
            if (hx0.k) {
                hx0.a(obj, j, b);
            } else {
                hx0.b(obj, j, b);
            }
        }

        @DexIgnore
        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }

        @DexIgnore
        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        public final void a(Object obj, long j, boolean z) {
            if (hx0.k) {
                hx0.b(obj, j, z);
            } else {
                hx0.c(obj, j, z);
            }
        }

        @DexIgnore
        public final void a(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray((int) (j2 & -1), bArr, (int) j, (int) j3);
        }

        @DexIgnore
        public final boolean c(Object obj, long j) {
            return hx0.k ? hx0.i(obj, j) : hx0.j(obj, j);
        }

        @DexIgnore
        public final float d(Object obj, long j) {
            return Float.intBitsToFloat(a(obj, j));
        }

        @DexIgnore
        public final double e(Object obj, long j) {
            return Double.longBitsToDouble(b(obj, j));
        }

        @DexIgnore
        public final byte f(Object obj, long j) {
            return hx0.k ? hx0.g(obj, j) : hx0.h(obj, j);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends d {
        @DexIgnore
        public b(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        public final void a(long j, byte b) {
            Memory.pokeByte(j, b);
        }

        @DexIgnore
        public final void a(Object obj, long j, byte b) {
            if (hx0.k) {
                hx0.a(obj, j, b);
            } else {
                hx0.b(obj, j, b);
            }
        }

        @DexIgnore
        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }

        @DexIgnore
        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        public final void a(Object obj, long j, boolean z) {
            if (hx0.k) {
                hx0.b(obj, j, z);
            } else {
                hx0.c(obj, j, z);
            }
        }

        @DexIgnore
        public final void a(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray(j2, bArr, (int) j, (int) j3);
        }

        @DexIgnore
        public final boolean c(Object obj, long j) {
            return hx0.k ? hx0.i(obj, j) : hx0.j(obj, j);
        }

        @DexIgnore
        public final float d(Object obj, long j) {
            return Float.intBitsToFloat(a(obj, j));
        }

        @DexIgnore
        public final double e(Object obj, long j) {
            return Double.longBitsToDouble(b(obj, j));
        }

        @DexIgnore
        public final byte f(Object obj, long j) {
            return hx0.k ? hx0.g(obj, j) : hx0.h(obj, j);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends d {
        @DexIgnore
        public c(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        public final void a(long j, byte b) {
            this.a.putByte(j, b);
        }

        @DexIgnore
        public final void a(Object obj, long j, byte b) {
            this.a.putByte(obj, j, b);
        }

        @DexIgnore
        public final void a(Object obj, long j, double d) {
            this.a.putDouble(obj, j, d);
        }

        @DexIgnore
        public final void a(Object obj, long j, float f) {
            this.a.putFloat(obj, j, f);
        }

        @DexIgnore
        public final void a(Object obj, long j, boolean z) {
            this.a.putBoolean(obj, j, z);
        }

        @DexIgnore
        public final void a(byte[] bArr, long j, long j2, long j3) {
            this.a.copyMemory(bArr, hx0.i + j, (Object) null, j2, j3);
        }

        @DexIgnore
        public final boolean c(Object obj, long j) {
            return this.a.getBoolean(obj, j);
        }

        @DexIgnore
        public final float d(Object obj, long j) {
            return this.a.getFloat(obj, j);
        }

        @DexIgnore
        public final double e(Object obj, long j) {
            return this.a.getDouble(obj, j);
        }

        @DexIgnore
        public final byte f(Object obj, long j) {
            return this.a.getByte(obj, j);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d {
        @DexIgnore
        public Unsafe a;

        @DexIgnore
        public d(Unsafe unsafe) {
            this.a = unsafe;
        }

        @DexIgnore
        public final int a(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        @DexIgnore
        public final long a(Field field) {
            return this.a.objectFieldOffset(field);
        }

        @DexIgnore
        public abstract void a(long j, byte b);

        @DexIgnore
        public abstract void a(Object obj, long j, byte b);

        @DexIgnore
        public abstract void a(Object obj, long j, double d);

        @DexIgnore
        public abstract void a(Object obj, long j, float f);

        @DexIgnore
        public final void a(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        @DexIgnore
        public final void a(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }

        @DexIgnore
        public abstract void a(Object obj, long j, boolean z);

        @DexIgnore
        public abstract void a(byte[] bArr, long j, long j2, long j3);

        @DexIgnore
        public final long b(Object obj, long j) {
            return this.a.getLong(obj, j);
        }

        @DexIgnore
        public abstract boolean c(Object obj, long j);

        @DexIgnore
        public abstract float d(Object obj, long j);

        @DexIgnore
        public abstract double e(Object obj, long j);

        @DexIgnore
        public abstract byte f(Object obj, long j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00c6  */
    /*
    static {
        d dVar;
        Field a2;
        Class<Object[]> cls = Object[].class;
        Class<double[]> cls2 = double[].class;
        Class<float[]> cls3 = float[].class;
        Class<long[]> cls4 = long[].class;
        Class<int[]> cls5 = int[].class;
        Class<boolean[]> cls6 = boolean[].class;
        if (b != null) {
            if (!nt0.a()) {
                dVar = new c(b);
            } else if (d) {
                dVar = new b(b);
            } else if (e) {
                dVar = new a(b);
            }
            f = dVar;
            a((Class<?>) cls6);
            b((Class<?>) cls6);
            a((Class<?>) cls5);
            b((Class<?>) cls5);
            a((Class<?>) cls4);
            b((Class<?>) cls4);
            a((Class<?>) cls3);
            b((Class<?>) cls3);
            a((Class<?>) cls2);
            b((Class<?>) cls2);
            a((Class<?>) cls);
            b((Class<?>) cls);
            a2 = a((Class<?>) String.class, "value");
            if (a2 == null || a2.getType() != char[].class) {
                a2 = null;
            }
            b(a2);
        }
        dVar = null;
        f = dVar;
        a((Class<?>) cls6);
        b((Class<?>) cls6);
        a((Class<?>) cls5);
        b((Class<?>) cls5);
        a((Class<?>) cls4);
        b((Class<?>) cls4);
        a((Class<?>) cls3);
        b((Class<?>) cls3);
        a((Class<?>) cls2);
        b((Class<?>) cls2);
        a((Class<?>) cls);
        b((Class<?>) cls);
        a2 = a((Class<?>) String.class, "value");
        a2 = null;
        b(a2);
    }
    */

    @DexIgnore
    public static byte a(byte[] bArr, long j2) {
        return f.f(bArr, i + j2);
    }

    @DexIgnore
    public static int a(Class<?> cls) {
        if (h) {
            return f.a.arrayBaseOffset(cls);
        }
        return -1;
    }

    @DexIgnore
    public static int a(Object obj, long j2) {
        return f.a(obj, j2);
    }

    @DexIgnore
    public static long a(Field field) {
        return f.a(field);
    }

    @DexIgnore
    public static long a(ByteBuffer byteBuffer) {
        return f.b(byteBuffer, j);
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static void a(long j2, byte b2) {
        f.a(j2, b2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int a2 = a(obj, j3);
        int i2 = ((~((int) j2)) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a2 & (~(255 << i2))));
    }

    @DexIgnore
    public static void a(Object obj, long j2, double d2) {
        f.a(obj, j2, d2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, float f2) {
        f.a(obj, j2, f2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, int i2) {
        f.a(obj, j2, i2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, long j3) {
        f.a(obj, j2, j3);
    }

    @DexIgnore
    public static void a(Object obj, long j2, Object obj2) {
        f.a.putObject(obj, j2, obj2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, boolean z) {
        f.a(obj, j2, z);
    }

    @DexIgnore
    public static void a(byte[] bArr, long j2, byte b2) {
        f.a((Object) bArr, i + j2, b2);
    }

    @DexIgnore
    public static void a(byte[] bArr, long j2, long j3, long j4) {
        f.a(bArr, j2, j3, j4);
    }

    @DexIgnore
    public static int b(Class<?> cls) {
        if (h) {
            return f.a.arrayIndexScale(cls);
        }
        return -1;
    }

    @DexIgnore
    public static long b(Object obj, long j2) {
        return f.b(obj, j2);
    }

    @DexIgnore
    public static long b(Field field) {
        if (field == null) {
            return -1;
        }
        d dVar = f;
        if (dVar == null) {
            return -1;
        }
        return dVar.a(field);
    }

    @DexIgnore
    public static void b(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = (((int) j2) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a(obj, j3) & (~(255 << i2))));
    }

    @DexIgnore
    public static void b(Object obj, long j2, boolean z) {
        a(obj, j2, z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static boolean b() {
        return h;
    }

    @DexIgnore
    public static void c(Object obj, long j2, boolean z) {
        b(obj, j2, z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static boolean c() {
        return g;
    }

    @DexIgnore
    public static boolean c(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!nt0.a()) {
            return false;
        }
        try {
            Class<?> cls3 = c;
            cls3.getMethod("peekLong", new Class[]{cls, Boolean.TYPE});
            cls3.getMethod("pokeLong", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls3.getMethod("pokeInt", new Class[]{cls, Integer.TYPE, Boolean.TYPE});
            cls3.getMethod("peekInt", new Class[]{cls, Boolean.TYPE});
            cls3.getMethod("pokeByte", new Class[]{cls, Byte.TYPE});
            cls3.getMethod("peekByte", new Class[]{cls});
            cls3.getMethod("pokeByteArray", new Class[]{cls, cls2, Integer.TYPE, Integer.TYPE});
            cls3.getMethod("peekByteArray", new Class[]{cls, cls2, Integer.TYPE, Integer.TYPE});
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public static boolean c(Object obj, long j2) {
        return f.c(obj, j2);
    }

    @DexIgnore
    public static float d(Object obj, long j2) {
        return f.d(obj, j2);
    }

    @DexIgnore
    public static Unsafe d() {
        try {
            return (Unsafe) AccessController.doPrivileged(new ix0());
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static double e(Object obj, long j2) {
        return f.e(obj, j2);
    }

    @DexIgnore
    public static boolean e() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls2.getMethod("arrayBaseOffset", new Class[]{Class.class});
            cls2.getMethod("arrayIndexScale", new Class[]{Class.class});
            cls2.getMethod("getInt", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putInt", new Class[]{cls, Long.TYPE, Integer.TYPE});
            cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putLong", new Class[]{cls, Long.TYPE, Long.TYPE});
            cls2.getMethod("getObject", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putObject", new Class[]{cls, Long.TYPE, cls});
            if (nt0.a()) {
                return true;
            }
            cls2.getMethod("getByte", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putByte", new Class[]{cls, Long.TYPE, Byte.TYPE});
            cls2.getMethod("getBoolean", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putBoolean", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls2.getMethod("getFloat", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putFloat", new Class[]{cls, Long.TYPE, Float.TYPE});
            cls2.getMethod("getDouble", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putDouble", new Class[]{cls, Long.TYPE, Double.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static Object f(Object obj, long j2) {
        return f.a.getObject(obj, j2);
    }

    @DexIgnore
    public static boolean f() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
            if (g() == null) {
                return false;
            }
            if (nt0.a()) {
                return true;
            }
            cls2.getMethod("getByte", new Class[]{Long.TYPE});
            cls2.getMethod("putByte", new Class[]{Long.TYPE, Byte.TYPE});
            cls2.getMethod("getInt", new Class[]{Long.TYPE});
            cls2.getMethod("putInt", new Class[]{Long.TYPE, Integer.TYPE});
            cls2.getMethod("getLong", new Class[]{Long.TYPE});
            cls2.getMethod("putLong", new Class[]{Long.TYPE, Long.TYPE});
            cls2.getMethod("copyMemory", new Class[]{Long.TYPE, Long.TYPE, Long.TYPE});
            cls2.getMethod("copyMemory", new Class[]{cls, Long.TYPE, cls, Long.TYPE, Long.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static byte g(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) (((~j2) & 3) << 3)));
    }

    @DexIgnore
    public static Field g() {
        if (nt0.a()) {
            Field a2 = a((Class<?>) Buffer.class, "effectiveDirectAddress");
            if (a2 != null) {
                return a2;
            }
        }
        Field a3 = a((Class<?>) Buffer.class, "address");
        if (a3 == null || a3.getType() != Long.TYPE) {
            return null;
        }
        return a3;
    }

    @DexIgnore
    public static byte h(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) ((j2 & 3) << 3)));
    }

    @DexIgnore
    public static boolean i(Object obj, long j2) {
        return g(obj, j2) != 0;
    }

    @DexIgnore
    public static boolean j(Object obj, long j2) {
        return h(obj, j2) != 0;
    }
}
