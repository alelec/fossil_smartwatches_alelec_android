package com.fossil.blesdk.obfuscated;

import com.facebook.internal.Utility;
import java.security.MessageDigest;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xy3 {
    @DexIgnore
    public static final String a(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
            instance.update(bArr);
            char[] cArr2 = new char[(r1 * 2)];
            int i = 0;
            for (byte b : instance.digest()) {
                int i2 = i + 1;
                cArr2[i] = cArr[(b >>> 4) & 15];
                i = i2 + 1;
                cArr2[i2] = cArr[b & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
            }
            return new String(cArr2);
        } catch (Exception unused) {
            return null;
        }
    }
}
