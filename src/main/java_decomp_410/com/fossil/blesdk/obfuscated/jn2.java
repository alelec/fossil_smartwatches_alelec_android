package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jn2 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public GoogleSignInOptions a;
    @DexIgnore
    public xb0 b;
    @DexIgnore
    public /* final */ String c; // = "23412525";
    @DexIgnore
    public WeakReference<BaseActivity> d;
    @DexIgnore
    public ln2 e;
    @DexIgnore
    public int f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements rn1<Void> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onComplete(wn1<Void> wn1) {
            kd4.b(wn1, "it");
            FLogger.INSTANCE.getLocal().e(jn2.g, "Log out google account completely");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements sn1 {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public final void onFailure(Exception exc) {
            kd4.b(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = jn2.g;
            local.e(d, "Could not log out google account, error = " + exc.getLocalizedMessage());
            exc.printStackTrace();
        }
    }

    /*
    static {
        new a((fd4) null);
        String canonicalName = jn2.class.getCanonicalName();
        if (canonicalName != null) {
            kd4.a((Object) canonicalName, "MFLoginGoogleManager::class.java.canonicalName!!");
            g = canonicalName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001f, code lost:
        if (r0 != null) goto L_0x0024;
     */
    @DexIgnore
    public final void a(WeakReference<BaseActivity> weakReference, ln2 ln2) {
        String str;
        kd4.b(weakReference, Constants.ACTIVITY);
        kd4.b(ln2, Constants.CALLBACK);
        Access a2 = new SoLibraryLoader().a((Context) PortfolioApp.W.c());
        if (a2 != null) {
            str = a2.getA();
        }
        str = this.c;
        if (str != null) {
            String obj = StringsKt__StringsKt.d(str).toString();
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(GoogleSignInOptions.s);
            aVar.a(obj);
            aVar.b(obj);
            aVar.b();
            aVar.d();
            GoogleSignInOptions a3 = aVar.a();
            kd4.a((Object) a3, "GoogleSignInOptions.Buil\u2026\n                .build()");
            this.a = a3;
            Context applicationContext = PortfolioApp.W.c().getApplicationContext();
            GoogleSignInOptions googleSignInOptions = this.a;
            if (googleSignInOptions != null) {
                this.b = vb0.a(applicationContext, googleSignInOptions);
                this.e = ln2;
                this.d = weakReference;
                b();
                return;
            }
            kd4.d("gso");
            throw null;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final void b() {
        a((WeakReference<Activity>) this.d);
        a();
    }

    @DexIgnore
    public final void c() {
        a((WeakReference<Activity>) this.d);
    }

    @DexIgnore
    public final boolean a(int i, int i2, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "Inside .onActivityResult requestCode=" + i + ", resultCode=" + i2);
        if (i != 922) {
            return true;
        }
        if (intent != null) {
            zb0 a2 = qb0.f.a(intent);
            a(a2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = g;
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .onActivityResult googleSignInResult=");
            kd4.a((Object) a2, Constants.RESULT);
            sb.append(a2.G());
            local2.d(str2, sb.toString());
            return true;
        }
        a((zb0) null);
        return true;
    }

    @DexIgnore
    public final void a(zb0 zb0) {
        int i = 600;
        if (zb0 == null) {
            c();
            ln2 ln2 = this.e;
            if (ln2 != null) {
                ln2.a(600, (ud0) null, "");
            }
        } else if (zb0.b()) {
            GoogleSignInAccount a2 = zb0.a();
            if (a2 == null) {
                ln2 ln22 = this.e;
                if (ln22 != null) {
                    ln22.a(600, (ud0) null, "");
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                FLogger.INSTANCE.getLocal().d(g, "Step 1: Login using google success");
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String J = a2.J();
                if (J == null) {
                    J = "";
                }
                signUpSocialAuth.setEmail(J);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = g;
                local.d(str, "Google user email is " + a2.J());
                String L = a2.L();
                if (L == null) {
                    L = "";
                }
                signUpSocialAuth.setFirstName(L);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "Google user first name is " + a2.L());
                String K = a2.K();
                if (K == null) {
                    K = "";
                }
                signUpSocialAuth.setLastName(K);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = g;
                local3.d(str3, "Google user last name is " + a2.K());
                String O = a2.O();
                if (O == null) {
                    O = "";
                }
                signUpSocialAuth.setToken(O);
                signUpSocialAuth.setClientId(AppHelper.f.a(""));
                signUpSocialAuth.setService("google");
                ln2 ln23 = this.e;
                if (ln23 != null) {
                    ln23.a(signUpSocialAuth);
                }
                this.f = 0;
                c();
            }
        } else {
            Status G = zb0.G();
            kd4.a((Object) G, "result.status");
            int I = G.I();
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = g;
            local4.e(str4, "login result code from google: " + I);
            if (I == 12502) {
                int i2 = this.f;
                if (i2 < 2) {
                    this.f = i2 + 1;
                    a();
                    return;
                }
                this.f = 0;
                FLogger.INSTANCE.getLocal().e(g, "why the login session always end up here, bug from Google!!");
                c();
                ln2 ln24 = this.e;
                if (ln24 != null) {
                    ln24.a(600, (ud0) null, "");
                    return;
                }
                return;
            }
            if (I == 12501) {
                i = 2;
            }
            c();
            ln2 ln25 = this.e;
            if (ln25 != null) {
                ln25.a(i, (ud0) null, "");
            }
        }
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().e(g, "connectNewAccount");
        WeakReference<BaseActivity> weakReference = this.d;
        if (weakReference != null) {
            if (weakReference == null) {
                kd4.a();
                throw null;
            } else if (weakReference.get() != null) {
                xb0 xb0 = this.b;
                if (xb0 != null) {
                    if (xb0 != null) {
                        Intent i = xb0.i();
                        kd4.a((Object) i, "googleSignInClient!!.signInIntent");
                        WeakReference<BaseActivity> weakReference2 = this.d;
                        if (weakReference2 != null) {
                            BaseActivity baseActivity = (BaseActivity) weakReference2.get();
                            if (baseActivity != null) {
                                baseActivity.startActivityForResult(i, 922);
                                return;
                            }
                            return;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
            }
        }
        ln2 ln2 = this.e;
        if (ln2 != null) {
            ln2.a(600, (ud0) null, "");
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        if ((weakReference != null ? (Activity) weakReference.get() : null) != null && vb0.a((Context) weakReference.get()) != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "inside .logOut(), googleSignInClient=" + this.b);
            xb0 xb0 = this.b;
            if (xb0 == null) {
                return;
            }
            if (xb0 != null) {
                wn1<Void> j = xb0.j();
                if (j != null) {
                    j.a((rn1<Void>) b.a);
                }
                if (j != null) {
                    j.a((sn1) c.a);
                    return;
                }
                return;
            }
            kd4.a();
            throw null;
        }
    }
}
