package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class en2 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences b;
    @DexIgnore
    public /* final */ SharedPreferences c;
    @DexIgnore
    public /* final */ SharedPreferences d;
    @DexIgnore
    public /* final */ SharedPreferences e;
    @DexIgnore
    public /* final */ SharedPreferences f;
    @DexIgnore
    public /* final */ SharedPreferences g;
    @DexIgnore
    public /* final */ SharedPreferences h;
    @DexIgnore
    public /* final */ SharedPreferences i;
    @DexIgnore
    public /* final */ SharedPreferences j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TypeToken<List<String>> {
        @DexIgnore
        public a(en2 en2) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TypeToken<List<String>> {
        @DexIgnore
        public b(en2 en2) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends TypeToken<List<String>> {
        @DexIgnore
        public c(en2 en2) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends TypeToken<List<String>> {
        @DexIgnore
        public d(en2 en2) {
        }
    }

    @DexIgnore
    public en2(Context context) {
        this.a = context.getSharedPreferences("PREFERENCE_DEVICE", 0);
        this.b = context.getSharedPreferences("PREFERENCE_AUTH", 0);
        this.c = context.getSharedPreferences("PREFERENCE_ONBOARDING", 0);
        this.d = context.getSharedPreferences("PREFERENCE_MIGRATE", 0);
        this.e = context.getSharedPreferences("PREFERENCE_BUDDY_CHALLENGE", 0);
        this.f = context.getSharedPreferences("PREFERENCE_CONNECTED_APPS", 0);
        this.j = context.getSharedPreferences("project_fossil_wearables_fossil", 0);
        this.g = context.getSharedPreferences("PREFERENCE_APPS", 0);
        this.h = context.getSharedPreferences("PREFERENCE_USER", 0);
        this.i = context.getSharedPreferences("PREFERENCE_FIREBASE", 0);
    }

    @DexIgnore
    public boolean A() {
        return a(this.h, "KEY_ALL_APPS_TOGGLE_ENABLE", true);
    }

    @DexIgnore
    public boolean B() {
        return a(this.a, "AutoSyncEnable", true);
    }

    @DexIgnore
    public boolean C() {
        return a(this.a, "consider_bundle_firmware_as_latest", false);
    }

    @DexIgnore
    public boolean D() {
        return a(this.h, "KEY_DND_MODE_HYBRID_ENABLE", false);
    }

    @DexIgnore
    public boolean E() {
        return a(this.h, "KEY_DND_SCHEDULED_ENABLE", false);
    }

    @DexIgnore
    public boolean F() {
        return this.a.getBoolean("KEY_IS_DEVICE_LOCATE_ENABLE", false);
    }

    @DexIgnore
    public boolean G() {
        return this.c.getBoolean("KEY_DEVICE_LOCATE_NEED_WARNING", true);
    }

    @DexIgnore
    public boolean H() {
        return a(this.h, "KEY_IS_GOAL_TRACKING_SETTING_COMPLETED", false);
    }

    @DexIgnore
    public boolean I() {
        return a(this.a, "HWLogSyncEnable", true);
    }

    @DexIgnore
    public boolean J() {
        return a(this.a, "KEY_DISPLAY_DEVICE_INFO", false);
    }

    @DexIgnore
    public boolean K() {
        return a(this.h, "KEY_SHOWING_RATING_PROMPT_CURRENT_VERSION", true);
    }

    @DexIgnore
    public boolean L() {
        return a(this.g, "KEY_NEED_TO_UPDATE_BLE_WHEN_UPGRADE_LEGACY", false);
    }

    @DexIgnore
    public boolean M() {
        return a(this.h, "KEY_NEW_SOLUTION_FOR_SMS_ENABLE", true);
    }

    @DexIgnore
    public boolean N() {
        return a(this.g, "KEY_IS_REGISTER_CONTACT_OBSERVER", false);
    }

    @DexIgnore
    public boolean O() {
        return a(this.h, "KEY_REMINDERS_BEDTIME_REMINDER_ENABLE", false);
    }

    @DexIgnore
    public boolean P() {
        return a(this.h, "KEY_REMINDERS_INACTIVITY_NUDGE_ENABLE", false);
    }

    @DexIgnore
    public boolean Q() {
        return a(this.h, "KEY_REMINDERS_KEEP_GOING_ENABLE", false);
    }

    @DexIgnore
    public boolean R() {
        return a(this.h, "KEY_REMINDERS_SLEEP_SUMMARY_ENABLE", false);
    }

    @DexIgnore
    public boolean S() {
        return a(this.a, "showAllDevices", false);
    }

    @DexIgnore
    public boolean T() {
        return a(this.a, "skipOta", false);
    }

    @DexIgnore
    public boolean U() {
        return a(this.a, "KEY_SYNC_SESSION_RUNNING", false);
    }

    @DexIgnore
    public void V() {
        this.a.edit().clear().apply();
        this.b.edit().clear().apply();
        this.c.edit().clear().apply();
        this.e.edit().clear().apply();
        this.h.edit().clear().apply();
        W();
    }

    @DexIgnore
    public final void W() {
        boolean K = K();
        this.h.edit().clear().apply();
        r(K);
    }

    @DexIgnore
    public final int a(SharedPreferences sharedPreferences, String str, int i2) {
        return sharedPreferences.getInt(str, i2);
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, int i2) {
        sharedPreferences.edit().putInt(str, i2).apply();
    }

    @DexIgnore
    public void c(boolean z) {
        b(this.a, "consider_bundle_firmware_as_latest", z);
    }

    @DexIgnore
    public HeartRateMode d(String str) {
        return HeartRateMode.Companion.fromValue(a(this.a, "HeartRateMode", HeartRateMode.NONE.getValue()));
    }

    @DexIgnore
    public long e(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "lastReminderSyncTimeSuccess" + str, 0);
    }

    @DexIgnore
    public String f() {
        return a(this.i, "KEY_FIREBASE_TOKEN", "");
    }

    @DexIgnore
    public void g(boolean z) {
        b(this.a, "FrontLightEnable", z);
    }

    @DexIgnore
    public void h(boolean z) {
        b(this.f, "KEY_GOOGLE_FIT_USER_ENABLE", z);
    }

    @DexIgnore
    public void i(boolean z) {
        b(this.a, "HWLogSyncEnable", z);
    }

    @DexIgnore
    public int j(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "PREFS_VIBRATION_STRENGTH" + str, 25);
    }

    @DexIgnore
    public boolean k(String str) {
        return a(this.a, "FULL_SYNC_DEVICE", "").startsWith(str);
    }

    @DexIgnore
    public boolean l(String str) {
        return a(this.a, "FrontLightEnable", false);
    }

    @DexIgnore
    public String m() {
        return this.d.getString("KEY_USER_LAST_APP_VERSION_NAME_Q", "");
    }

    @DexIgnore
    public void n(String str) {
        SharedPreferences sharedPreferences = this.a;
        a(sharedPreferences, "tempDeviceFirmwareInfo_" + str);
    }

    @DexIgnore
    public void o(String str) {
        b(this.a, "active_device_address", str);
    }

    @DexIgnore
    public void p(String str) {
        b(this.a, "installation_id_android", str);
    }

    @DexIgnore
    public void q(String str) {
        b(this.d, "KEY_USER_LAST_APP_VERSION_NAME_Q", str);
    }

    @DexIgnore
    public void r(String str) {
        b(this.i, "KEY_FIREBASE_TOKEN", str);
    }

    @DexIgnore
    public String s() {
        return a(this.j, "KEY_LOW_BATTERY_DEVICE", "");
    }

    @DexIgnore
    public String t() {
        return this.j.getString("active_device_address", "");
    }

    @DexIgnore
    public void u(String str) {
        b(this.a, "KEY_LOW_BATTERY_DEVICE", str);
    }

    @DexIgnore
    public String v() {
        return a(this.a, "realTimeStepTimeStamp_", "");
    }

    @DexIgnore
    public void w(String str) {
        b(this.b, "USER_ACCESS_TOKEN", str);
    }

    @DexIgnore
    public void x(boolean z) {
        b(this.a, "showAllDevices", z);
    }

    @DexIgnore
    public void y(boolean z) {
        b(this.a, "skipOta", z);
    }

    @DexIgnore
    public String z() {
        return a(this.g, "KEY_WIDGETS_DATE_CHANGED", "");
    }

    @DexIgnore
    public final long a(SharedPreferences sharedPreferences, String str, long j2) {
        return sharedPreferences.getLong(str, j2);
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, long j2) {
        sharedPreferences.edit().putLong(str, j2).apply();
    }

    @DexIgnore
    public void c(long j2, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "lastSyncTimeSuccess" + str, j2);
    }

    @DexIgnore
    public void d(int i2) {
        b(this.a, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", i2);
    }

    @DexIgnore
    public int e() {
        return a(this.h, "KEY_CURRENT_SYNC_SUCCESSULLY_SESSIONS", 0);
    }

    @DexIgnore
    public long f(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "lastSyncTime" + str, 0);
    }

    @DexIgnore
    public long g(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "lastSyncTimeSuccess" + str, 0);
    }

    @DexIgnore
    public boolean h() {
        return a(this.h, "KEY_COMMUTE_TIME_TUTORIAL", false);
    }

    @DexIgnore
    public String i(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "tempDeviceFirmwareInfo_" + str, "");
    }

    @DexIgnore
    public int j() {
        return a(this.g, "KEY_USING_SWITCH_URL_SETTING", 0);
    }

    @DexIgnore
    public void l(boolean z) {
        b(this.a, "KEY_NEED_TO_SHOW_LEGACY_POPUP", z);
    }

    @DexIgnore
    public boolean m(String str) {
        SharedPreferences sharedPreferences = this.g;
        return a(sharedPreferences, "KEY_MIGRATION_COMPLETE" + str, false);
    }

    @DexIgnore
    public void n(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsRegisteredContactObserver - isSet=" + z);
        b(this.g, "KEY_IS_REGISTER_CONTACT_OBSERVER", z);
    }

    @DexIgnore
    public int o() {
        return a(this.a, "lastNotificationID", 0);
    }

    @DexIgnore
    public void p(boolean z) {
        b(this.a, "KEY_DISPLAY_DEVICE_INFO", z);
    }

    @DexIgnore
    public List<String> q() {
        String a2 = a(this.h, "KEY_MICRO_APP_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new d(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getMicroAppSearchedIdsRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public boolean r() {
        return this.j.getBoolean("KEY_ENABLE_FIND_MY_DEVICE", false);
    }

    @DexIgnore
    public void s(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setKeyWeatherSettings - weatherSettings=" + str);
        b(this.a, "KEY_WEATHER_SETTINGS", str);
    }

    @DexIgnore
    public void t(boolean z) {
        b(this.h, "KEY_REMINDERS_BEDTIME_REMINDER_ENABLE", z);
    }

    @DexIgnore
    public int u() {
        return a(this.j, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", 25);
    }

    @DexIgnore
    public void v(String str) {
        b(this.a, "realTimeStepTimeStamp_", str);
    }

    @DexIgnore
    public int w() {
        return a(this.a, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", 25);
    }

    @DexIgnore
    public String x() {
        return a(this.b, "USER_ACCESS_TOKEN", "");
    }

    @DexIgnore
    public List<String> y() {
        String a2 = a(this.h, "KEY_WATCH_APP_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new c(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getWatchAppSearchedIdsRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public final void a(SharedPreferences sharedPreferences, String str) {
        sharedPreferences.edit().remove(str).apply();
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, String str2) {
        sharedPreferences.edit().putString(str, str2).apply();
    }

    @DexIgnore
    public void c(int i2) {
        b(this.a, "lastNotificationID", i2);
    }

    @DexIgnore
    public List<String> d() {
        String a2 = a(this.h, "KEY_COMPLICATION_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new a(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getComplicationSearchedIdsRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public void e(boolean z) {
        b(this.h, "KEY_DND_SCHEDULED_ENABLE", z);
    }

    @DexIgnore
    public void f(boolean z) {
        b(this.a, "KEY_IS_DEVICE_LOCATE_ENABLE", z);
    }

    @DexIgnore
    public boolean g() {
        return a(this.f, "KEY_GOOGLE_FIT_USER_ENABLE", false);
    }

    @DexIgnore
    public String h(String str) {
        SharedPreferences sharedPreferences = this.g;
        return a(sharedPreferences, "localization_" + str, "");
    }

    @DexIgnore
    public boolean i() {
        return a(this.h, "KEY_NOTIFICATION_ACCESS_SHOULD_INFORM", true);
    }

    @DexIgnore
    public void j(boolean z) {
        b(this.h, "KEY_IS_GOAL_TRACKING_SETTING_COMPLETED", z);
    }

    @DexIgnore
    public void k(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsNeedRemoveRedundantContact - isNeed=" + z);
        b(this.g, "KEY_NEED_REMOVE_REDUNDANT_CONTACT", z);
    }

    @DexIgnore
    public String l() {
        String a2 = a(this.a, "KEY_WEATHER_SETTINGS", "");
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        return new Gson().a((Object) new WeatherSettings(), (Type) WeatherSettings.class);
    }

    @DexIgnore
    public void m(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsNeedToUpdateBLEWhenUpgradeLegacy - isNeedToUpdate=" + z);
        b(this.g, "KEY_NEED_TO_UPDATE_BLE_WHEN_UPGRADE_LEGACY", z);
    }

    @DexIgnore
    public void o(boolean z) {
        b(this.h, "KEY_COMMUTE_TIME_TUTORIAL", z);
    }

    @DexIgnore
    public String p() {
        return a(this.g, "KEY_LOCALE_VERSION", "");
    }

    @DexIgnore
    public void r(boolean z) {
        b(this.h, "KEY_SHOWING_RATING_PROMPT_CURRENT_VERSION", z);
    }

    @DexIgnore
    public void t(String str) {
        b(this.g, "KEY_LOCALE_VERSION", str);
    }

    @DexIgnore
    public void u(boolean z) {
        b(this.h, "KEY_REMINDERS_INACTIVITY_NUDGE_ENABLE", z);
    }

    @DexIgnore
    public void v(boolean z) {
        b(this.h, "KEY_REMINDERS_KEEP_GOING_ENABLE", z);
    }

    @DexIgnore
    public void w(boolean z) {
        b(this.h, "KEY_REMINDERS_SLEEP_SUMMARY_ENABLE", z);
    }

    @DexIgnore
    public void x(String str) {
        b(this.g, "KEY_WIDGETS_DATE_CHANGED", str);
    }

    @DexIgnore
    public final String a(SharedPreferences sharedPreferences, String str, String str2) {
        return sharedPreferences.getString(str, str2);
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, boolean z) {
        sharedPreferences.edit().putBoolean(str, z).apply();
    }

    @DexIgnore
    public String c(String str) {
        SharedPreferences sharedPreferences = this.h;
        return a(sharedPreferences, "encryptedDataInKeyStore" + str, "");
    }

    @DexIgnore
    public long n() {
        long a2 = a(this.h, "KEY_LAST_NO_CRASH_EVENT_TIMESTAMP", -1);
        if (a2 != -1) {
            return a2;
        }
        long currentTimeMillis = System.currentTimeMillis();
        c(currentTimeMillis);
        return currentTimeMillis;
    }

    @DexIgnore
    public void s(boolean z) {
        b(this.h, "KEY_NEW_SOLUTION_FOR_SMS_ENABLE", z);
    }

    @DexIgnore
    public final boolean a(SharedPreferences sharedPreferences, String str, boolean z) {
        return sharedPreferences.getBoolean(str, z);
    }

    @DexIgnore
    public String b() {
        return a(this.a, "active_device_address", "");
    }

    @DexIgnore
    public void c(String str, String str2) {
        SharedPreferences sharedPreferences = this.h;
        b(sharedPreferences, "encryptedDataInKeyStore" + str, str2);
    }

    @DexIgnore
    public String k() {
        return a(this.a, "KEY_WEARING_POSITION", "unspecified");
    }

    @DexIgnore
    public Firmware a(String str) {
        SharedPreferences sharedPreferences = this.a;
        try {
            return (Firmware) new Gson().a(a(sharedPreferences, "bundle_latest_firmware_data" + str, ""), Firmware.class);
        } catch (JsonSyntaxException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SharedPreferencesManager", ".getBundleLatestFirmwareModel() failed, ex=" + e2);
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SharedPreferencesManager", ".getBundleLatestFirmwareModel() failed, ex=" + e3);
        }
        return null;
    }

    @DexIgnore
    public void b(boolean z) {
        b(this.a, "AutoSyncEnable", z);
    }

    @DexIgnore
    public void c(long j2) {
        b(this.h, "KEY_LAST_NO_CRASH_EVENT_TIMESTAMP", j2);
    }

    @DexIgnore
    public void b(long j2, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "lastSyncTime" + str, j2);
    }

    @DexIgnore
    public List<String> c() {
        String a2 = a(this.h, "KEY_ADDRESS_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new b(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getAddressSearchedRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public void b(long j2) {
        b(this.a, "lastHWSyncTime", j2);
    }

    @DexIgnore
    public void b(String str, boolean z) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "KEY_PAIR_COMPLETE_DEVICE" + str, z);
    }

    @DexIgnore
    public void a(Firmware firmware, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "bundle_latest_firmware_data" + str, new Gson().a((Object) firmware));
    }

    @DexIgnore
    public String b(String str) {
        SharedPreferences sharedPreferences = this.h;
        return a(sharedPreferences, "cipherIv" + str, "");
    }

    @DexIgnore
    public void q(boolean z) {
        b(this.h, "KEY_NOTIFICATION_ACCESS_SHOULD_INFORM", z);
    }

    @DexIgnore
    public void a(HeartRateMode heartRateMode) {
        b(this.a, "HeartRateMode", heartRateMode.getValue());
    }

    @DexIgnore
    public void b(String str, String str2) {
        SharedPreferences sharedPreferences = this.h;
        b(sharedPreferences, "cipherIv" + str, str2);
    }

    @DexIgnore
    public void d(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_WATCH_APP_SEARCHED_RECENT", new Gson().a((Object) list));
        }
    }

    @DexIgnore
    public void a(long j2, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "lastReminderSyncTimeSuccess" + str, j2);
    }

    @DexIgnore
    public void b(int i2) {
        b(this.h, "KEY_CURRENT_SYNC_SUCCESSULLY_SESSIONS", i2);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "calibrationState_" + str + dn2.p.a().c(), z);
    }

    @DexIgnore
    public void b(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_COMPLICATION_SEARCHED_RECENT", new Gson().a((Object) list));
        }
    }

    @DexIgnore
    public void d(boolean z) {
        b(this.h, "KEY_DND_MODE_HYBRID_ENABLE", z);
    }

    @DexIgnore
    public void d(String str, String str2) {
        SharedPreferences sharedPreferences = this.g;
        b(sharedPreferences, "localization_" + str2, str);
    }

    @DexIgnore
    public void c(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_MICRO_APP_SEARCHED_RECENT", new Gson().a((Object) list));
        }
    }

    @DexIgnore
    public void a(String str, long j2, boolean z) {
        String str2;
        SharedPreferences sharedPreferences = this.a;
        if (z) {
            str2 = str + j2;
        } else {
            str2 = "";
        }
        b(sharedPreferences, "FULL_SYNC_DEVICE", str2);
    }

    @DexIgnore
    public long a() {
        return a(this.b, "ACCESS_TOKEN_TIME", 0);
    }

    @DexIgnore
    public void a(long j2) {
        b(this.b, "ACCESS_TOKEN_TIME", j2);
    }

    @DexIgnore
    public void a(String str, String str2) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "tempDeviceFirmwareInfo_" + str, str2);
    }

    @DexIgnore
    public void a(boolean z, String str) {
        SharedPreferences sharedPreferences = this.g;
        b(sharedPreferences, "KEY_MIGRATION_COMPLETE" + str, z);
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setAppDataVersion - appDataVersion=" + i2);
        b(this.g, "KEY_USER_LAST_DATA_VERSION", i2);
    }

    @DexIgnore
    public void a(Boolean bool) {
        b(this.a, "KEY_SYNC_SESSION_RUNNING", bool.booleanValue());
    }

    @DexIgnore
    public void a(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_ADDRESS_SEARCHED_RECENT", new Gson().a((Object) list));
        }
    }

    @DexIgnore
    public void a(String str, long j2) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "DELAY_OTA_BY_USER" + str, j2);
    }

    @DexIgnore
    public void a(boolean z) {
        b(this.h, "KEY_ALL_APPS_TOGGLE_ENABLE", z);
    }
}
