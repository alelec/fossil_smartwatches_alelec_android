package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jp4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ kp4 b;
    @DexIgnore
    public /* final */ qp4 c;

    @DexIgnore
    public jp4(String str, qp4 qp4) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (qp4 != null) {
            this.a = str;
            this.c = qp4;
            this.b = new kp4();
            a(qp4);
            b(qp4);
            c(qp4);
        } else {
            throw new IllegalArgumentException("Body may not be null");
        }
    }

    @DexIgnore
    public qp4 a() {
        return this.c;
    }

    @DexIgnore
    public kp4 b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (str != null) {
            this.b.a(new np4(str, str2));
            return;
        }
        throw new IllegalArgumentException("Field name may not be null");
    }

    @DexIgnore
    public void b(qp4 qp4) {
        StringBuilder sb = new StringBuilder();
        sb.append(qp4.c());
        if (qp4.b() != null) {
            sb.append("; charset=");
            sb.append(qp4.b());
        }
        a(GraphRequest.CONTENT_TYPE_HEADER, sb.toString());
    }

    @DexIgnore
    public void c(qp4 qp4) {
        a("Content-Transfer-Encoding", qp4.a());
    }

    @DexIgnore
    public void a(qp4 qp4) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(c());
        sb.append("\"");
        if (qp4.d() != null) {
            sb.append("; filename=\"");
            sb.append(qp4.d());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }
}
