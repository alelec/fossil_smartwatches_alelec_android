package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.zj0;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fq0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<fq0> CREATOR; // = new gq0();
    @DexIgnore
    public /* final */ DataSet e;
    @DexIgnore
    public /* final */ g11 f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore
    public fq0(DataSet dataSet, IBinder iBinder, boolean z) {
        this.e = dataSet;
        this.f = h11.a(iBinder);
        this.g = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof fq0) && zj0.a(this.e, ((fq0) obj).e);
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(this.e);
    }

    @DexIgnore
    public final String toString() {
        zj0.a a = zj0.a((Object) this);
        a.a("dataSet", this.e);
        return a.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) this.e, i, false);
        g11 g11 = this.f;
        kk0.a(parcel, 2, g11 == null ? null : g11.asBinder(), false);
        kk0.a(parcel, 4, this.g);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public fq0(DataSet dataSet, g11 g11, boolean z) {
        this.e = dataSet;
        this.f = g11;
        this.g = z;
    }
}
