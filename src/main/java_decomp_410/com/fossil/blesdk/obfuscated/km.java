package com.fossil.blesdk.obfuscated;

import bolts.UnobservedTaskException;
import com.fossil.blesdk.obfuscated.im;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class km {
    @DexIgnore
    public im<?> a;

    @DexIgnore
    public km(im<?> imVar) {
        this.a = imVar;
    }

    @DexIgnore
    public void a() {
        this.a = null;
    }

    @DexIgnore
    public void finalize() throws Throwable {
        try {
            im<?> imVar = this.a;
            if (imVar != null) {
                im.g j = im.j();
                if (j != null) {
                    j.a(imVar, new UnobservedTaskException(imVar.a()));
                }
            }
        } finally {
            super.finalize();
        }
    }
}
