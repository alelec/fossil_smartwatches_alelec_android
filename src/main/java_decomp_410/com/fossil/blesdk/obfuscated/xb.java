package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@Deprecated
public interface xb extends LifecycleOwner {
    @DexIgnore
    LifecycleRegistry getLifecycle();
}
