package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mg */
public class C2409mg implements com.fossil.blesdk.obfuscated.C1945hg {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2409mg.C2410a f7489a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.mg$a */
    public static class C2410a extends android.database.sqlite.SQLiteOpenHelper {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C2324lg[] f7490e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C1945hg.C1946a f7491f;

        @DexIgnore
        /* renamed from: g */
        public boolean f7492g;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mg$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.mg$a$a */
        public class C2411a implements android.database.DatabaseErrorHandler {

            @DexIgnore
            /* renamed from: a */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1945hg.C1946a f7493a;

            @DexIgnore
            /* renamed from: b */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2324lg[] f7494b;

            @DexIgnore
            public C2411a(com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar, com.fossil.blesdk.obfuscated.C2324lg[] lgVarArr) {
                this.f7493a = aVar;
                this.f7494b = lgVarArr;
            }

            @DexIgnore
            public void onCorruption(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
                this.f7493a.mo11641b(com.fossil.blesdk.obfuscated.C2409mg.C2410a.m10729a(this.f7494b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public C2410a(android.content.Context context, java.lang.String str, com.fossil.blesdk.obfuscated.C2324lg[] lgVarArr, com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar) {
            super(context, str, (android.database.sqlite.SQLiteDatabase.CursorFactory) null, aVar.f5733a, new com.fossil.blesdk.obfuscated.C2409mg.C2410a.C2411a(aVar, lgVarArr));
            this.f7491f = aVar;
            this.f7490e = lgVarArr;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2324lg mo13614a(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            return m10729a(this.f7490e, sQLiteDatabase);
        }

        @DexIgnore
        public synchronized void close() {
            super.close();
            this.f7490e[0] = null;
        }

        @DexIgnore
        public void onConfigure(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            this.f7491f.mo11638a((com.fossil.blesdk.obfuscated.C1874gg) mo13614a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            this.f7491f.mo11643c(mo13614a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(android.database.sqlite.SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.f7492g = true;
            this.f7491f.mo11639a(mo13614a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            if (!this.f7492g) {
                this.f7491f.mo11644d(mo13614a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(android.database.sqlite.SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.f7492g = true;
            this.f7491f.mo11642b(mo13614a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        /* renamed from: y */
        public synchronized com.fossil.blesdk.obfuscated.C1874gg mo13621y() {
            this.f7492g = false;
            android.database.sqlite.SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.f7492g) {
                close();
                return mo13621y();
            }
            return mo13614a(writableDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2324lg m10729a(com.fossil.blesdk.obfuscated.C2324lg[] lgVarArr, android.database.sqlite.SQLiteDatabase sQLiteDatabase) {
            com.fossil.blesdk.obfuscated.C2324lg lgVar = lgVarArr[0];
            if (lgVar == null || !lgVar.mo13288a(sQLiteDatabase)) {
                lgVarArr[0] = new com.fossil.blesdk.obfuscated.C2324lg(sQLiteDatabase);
            }
            return lgVarArr[0];
        }
    }

    @DexIgnore
    public C2409mg(android.content.Context context, java.lang.String str, com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar) {
        this.f7489a = mo13613a(context, str, aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2409mg.C2410a mo13613a(android.content.Context context, java.lang.String str, com.fossil.blesdk.obfuscated.C1945hg.C1946a aVar) {
        return new com.fossil.blesdk.obfuscated.C2409mg.C2410a(context, str, new com.fossil.blesdk.obfuscated.C2324lg[1], aVar);
    }

    @DexIgnore
    public void close() {
        this.f7489a.close();
    }

    @DexIgnore
    public java.lang.String getDatabaseName() {
        return this.f7489a.getDatabaseName();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11635a(boolean z) {
        this.f7489a.setWriteAheadLoggingEnabled(z);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1874gg mo11634a() {
        return this.f7489a.mo13621y();
    }
}
