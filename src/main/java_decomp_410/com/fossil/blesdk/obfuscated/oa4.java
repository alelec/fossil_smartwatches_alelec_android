package com.fossil.blesdk.obfuscated;

import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oa4 {
    @DexIgnore
    public static final <A, B> Pair<A, B> a(A a, B b) {
        return new Pair<>(a, b);
    }
}
