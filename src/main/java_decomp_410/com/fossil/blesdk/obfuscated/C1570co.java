package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.co */
public interface C1570co {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.co$a */
    public interface C1571a {
        @DexIgnore
        /* renamed from: a */
        android.graphics.Bitmap mo9599a(int i, int i2, android.graphics.Bitmap.Config config);

        @DexIgnore
        /* renamed from: a */
        void mo9600a(android.graphics.Bitmap bitmap);

        @DexIgnore
        /* renamed from: a */
        void mo9601a(byte[] bArr);

        @DexIgnore
        /* renamed from: a */
        void mo9602a(int[] iArr);

        @DexIgnore
        /* renamed from: a */
        int[] mo9603a(int i);

        @DexIgnore
        /* renamed from: b */
        byte[] mo9604b(int i);
    }

    @DexIgnore
    /* renamed from: a */
    android.graphics.Bitmap mo9589a();

    @DexIgnore
    /* renamed from: a */
    void mo9590a(android.graphics.Bitmap.Config config);

    @DexIgnore
    /* renamed from: b */
    void mo9591b();

    @DexIgnore
    /* renamed from: c */
    int mo9592c();

    @DexIgnore
    void clear();

    @DexIgnore
    /* renamed from: d */
    int mo9594d();

    @DexIgnore
    /* renamed from: e */
    java.nio.ByteBuffer mo9595e();

    @DexIgnore
    /* renamed from: f */
    void mo9596f();

    @DexIgnore
    /* renamed from: g */
    int mo9597g();

    @DexIgnore
    /* renamed from: h */
    int mo9598h();
}
