package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lh2 extends kh2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = new ViewDataBinding.j(5);
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        w.a(0, new String[]{"item_calories_day"}, new int[]{1}, new int[]{R.layout.item_calories_day});
        x.put(R.id.cl_weekly, 2);
        x.put(R.id.ftv_weekly, 3);
        x.put(R.id.ftv_weekly_value, 4);
    }
    */

    @DexIgnore
    public lh2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 5, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
        ViewDataBinding.d(this.r);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.r.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 2;
        }
        this.r.f();
        g();
    }

    @DexIgnore
    public lh2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 1, objArr[2], objArr[1], objArr[3], objArr[4]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
