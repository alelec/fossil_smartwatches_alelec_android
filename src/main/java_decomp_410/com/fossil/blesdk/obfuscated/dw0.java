package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dw0 extends fe0<Object> implements od0 {
    @DexIgnore
    public dw0(Context context) {
        super(context, md0.o, null, (df0) new re0());
    }

    @DexIgnore
    public static od0 a(Context context) {
        return new dw0(context);
    }

    @DexIgnore
    public final he0<Status> a(rd0 rd0) {
        iy0 iy0 = new iy0(rd0, a());
        a(iy0);
        return iy0;
    }
}
