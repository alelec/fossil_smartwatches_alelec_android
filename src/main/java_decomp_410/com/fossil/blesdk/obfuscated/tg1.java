package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.measurement.AppMeasurement;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tg1 extends ui1 {
    @DexIgnore
    public char c; // = 0;
    @DexIgnore
    public long d; // = -1;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ vg1 f; // = new vg1(this, 6, false, false);
    @DexIgnore
    public /* final */ vg1 g; // = new vg1(this, 6, true, false);
    @DexIgnore
    public /* final */ vg1 h; // = new vg1(this, 6, false, true);
    @DexIgnore
    public /* final */ vg1 i; // = new vg1(this, 5, false, false);
    @DexIgnore
    public /* final */ vg1 j; // = new vg1(this, 5, true, false);
    @DexIgnore
    public /* final */ vg1 k; // = new vg1(this, 5, false, true);
    @DexIgnore
    public /* final */ vg1 l; // = new vg1(this, 4, false, false);
    @DexIgnore
    public /* final */ vg1 m; // = new vg1(this, 3, false, false);
    @DexIgnore
    public /* final */ vg1 n; // = new vg1(this, 2, false, false);

    @DexIgnore
    public tg1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public static Object a(String str) {
        if (str == null) {
            return null;
        }
        return new wg1(str);
    }

    @DexIgnore
    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return str;
        }
        return str.substring(0, lastIndexOf);
    }

    @DexIgnore
    public final vg1 A() {
        return this.n;
    }

    @DexIgnore
    public final String B() {
        String str;
        synchronized (this) {
            if (this.e == null) {
                if (this.a.C() != null) {
                    this.e = this.a.C();
                } else {
                    this.e = xl1.s();
                }
            }
            str = this.e;
        }
        return str;
    }

    @DexIgnore
    public final String C() {
        Pair<String, Long> b = k().d.b();
        if (b == null || b == fh1.w) {
            return null;
        }
        String valueOf = String.valueOf(b.second);
        String str = (String) b.first;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length());
        sb.append(valueOf);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public final boolean p() {
        return false;
    }

    @DexIgnore
    public final vg1 s() {
        return this.f;
    }

    @DexIgnore
    public final vg1 t() {
        return this.g;
    }

    @DexIgnore
    public final vg1 u() {
        return this.h;
    }

    @DexIgnore
    public final vg1 v() {
        return this.i;
    }

    @DexIgnore
    public final vg1 w() {
        return this.j;
    }

    @DexIgnore
    public final vg1 x() {
        return this.k;
    }

    @DexIgnore
    public final vg1 y() {
        return this.l;
    }

    @DexIgnore
    public final vg1 z() {
        return this.m;
    }

    @DexIgnore
    public final void a(int i2, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && a(i2)) {
            a(i2, a(false, str, obj, obj2, obj3));
        }
        if (!z2 && i2 >= 5) {
            bk0.a(str);
            th1 x = this.a.x();
            if (x == null) {
                a(6, "Scheduler not set. Not logging error/warn");
            } else if (!x.m()) {
                a(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                if (i2 < 0) {
                    i2 = 0;
                }
                x.a((Runnable) new ug1(this, i2 >= 9 ? 8 : i2, str, obj, obj2, obj3));
            }
        }
    }

    @DexIgnore
    public final boolean a(int i2) {
        return Log.isLoggable(B(), i2);
    }

    @DexIgnore
    public final void a(int i2, String str) {
        Log.println(i2, B(), str);
    }

    @DexIgnore
    public static String a(boolean z, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String a = a(z, obj);
        String a2 = a(z, obj2);
        String a3 = a(z, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(a)) {
            sb.append(str2);
            sb.append(a);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a2)) {
            sb.append(str2);
            sb.append(a2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a3)) {
            sb.append(str2);
            sb.append(a3);
        }
        return sb.toString();
    }

    @DexIgnore
    public static String a(boolean z, Object obj) {
        String str = "";
        if (obj == null) {
            return str;
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf((long) ((Integer) obj).intValue());
        }
        int i2 = 0;
        if (obj instanceof Long) {
            if (!z) {
                return String.valueOf(obj);
            }
            Long l2 = (Long) obj;
            if (Math.abs(l2.longValue()) < 100) {
                return String.valueOf(obj);
            }
            if (String.valueOf(obj).charAt(0) == '-') {
                str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            }
            String valueOf = String.valueOf(Math.abs(l2.longValue()));
            long round = Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1)));
            long round2 = Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(str.length() + 43 + str.length());
            sb.append(str);
            sb.append(round);
            sb.append("...");
            sb.append(str);
            sb.append(round2);
            return sb.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (obj instanceof Throwable) {
                Throwable th = (Throwable) obj;
                StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
                String b = b(AppMeasurement.class.getCanonicalName());
                String b2 = b(xh1.class.getCanonicalName());
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i2];
                    if (!stackTraceElement.isNativeMethod()) {
                        String className = stackTraceElement.getClassName();
                        if (className != null) {
                            String b3 = b(className);
                            if (b3.equals(b) || b3.equals(b2)) {
                                sb2.append(": ");
                                sb2.append(stackTraceElement);
                            }
                        } else {
                            continue;
                        }
                    }
                    i2++;
                }
                return sb2.toString();
            } else if (obj instanceof wg1) {
                return ((wg1) obj).a;
            } else {
                if (z) {
                    return ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                }
                return String.valueOf(obj);
            }
        }
    }
}
