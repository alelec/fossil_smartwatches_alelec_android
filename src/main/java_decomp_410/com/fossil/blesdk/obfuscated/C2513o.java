package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.o */
public class C2513o {
    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m11512a(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getExtras();
    }
}
