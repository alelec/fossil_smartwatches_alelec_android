package com.fossil.blesdk.obfuscated;

import com.android.volley.ParseError;
import com.fossil.blesdk.obfuscated.um;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kn extends ln<JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kn(int i, String str, JSONObject jSONObject, um.b<JSONObject> bVar, um.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    @DexIgnore
    public um<JSONObject> parseNetworkResponse(sm smVar) {
        try {
            return um.a(new JSONObject(new String(smVar.b, en.a(smVar.c, ln.PROTOCOL_CHARSET))), en.a(smVar));
        } catch (UnsupportedEncodingException e) {
            return um.a(new ParseError((Throwable) e));
        } catch (JSONException e2) {
            return um.a(new ParseError((Throwable) e2));
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public kn(String str, JSONObject jSONObject, um.b<JSONObject> bVar, um.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }
}
