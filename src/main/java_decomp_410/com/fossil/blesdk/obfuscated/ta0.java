package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothGattDescriptor;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ta0 {
    @DexIgnore
    public /* final */ Hashtable<BluetoothGattDescriptor, byte[]> a; // = new Hashtable<>();

    @DexIgnore
    public final Hashtable<BluetoothGattDescriptor, byte[]> a() {
        return this.a;
    }
}
