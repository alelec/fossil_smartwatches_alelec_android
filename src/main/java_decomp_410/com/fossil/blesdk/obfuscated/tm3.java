package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tm3 {
    @DexIgnore
    public sm3 a;

    @DexIgnore
    public tm3(sm3 sm3) {
        kd4.b(sm3, "mPairingInstructionsView");
        this.a = sm3;
    }

    @DexIgnore
    public final sm3 a() {
        return this.a;
    }
}
