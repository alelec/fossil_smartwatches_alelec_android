package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class re2 extends qe2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.iv_close, 1);
        y.put(R.id.progressBar, 2);
        y.put(R.id.ftv_title, 3);
        y.put(R.id.iv_device_image, 4);
        y.put(R.id.ftv_desc, 5);
        y.put(R.id.br_top, 6);
        y.put(R.id.rl_wear_os_group, 7);
        y.put(R.id.ftv_wear_os, 8);
        y.put(R.id.iv_help, 9);
        y.put(R.id.fb_skip, 10);
        y.put(R.id.fb_find_watch, 11);
    }
    */

    @DexIgnore
    public re2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 12, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public re2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[6], objArr[11], objArr[10], objArr[5], objArr[3], objArr[8], objArr[1], objArr[4], objArr[9], objArr[2], objArr[7]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
