package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentHostCallback;
import androidx.fragment.app.FragmentManagerImpl;
import androidx.fragment.app.FragmentManagerNonConfig;
import androidx.lifecycle.ViewModelStore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ab implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ab> CREATOR; // = new a();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Bundle m;
    @DexIgnore
    public /* final */ boolean n;
    @DexIgnore
    public Bundle o;
    @DexIgnore
    public Fragment p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ab> {
        @DexIgnore
        public ab createFromParcel(Parcel parcel) {
            return new ab(parcel);
        }

        @DexIgnore
        public ab[] newArray(int i) {
            return new ab[i];
        }
    }

    @DexIgnore
    public ab(Fragment fragment) {
        this.e = fragment.getClass().getName();
        this.f = fragment.mIndex;
        this.g = fragment.mFromLayout;
        this.h = fragment.mFragmentId;
        this.i = fragment.mContainerId;
        this.j = fragment.mTag;
        this.k = fragment.mRetainInstance;
        this.l = fragment.mDetached;
        this.m = fragment.mArguments;
        this.n = fragment.mHidden;
    }

    @DexIgnore
    public Fragment a(FragmentHostCallback fragmentHostCallback, ya yaVar, Fragment fragment, FragmentManagerNonConfig fragmentManagerNonConfig, ViewModelStore viewModelStore) {
        if (this.p == null) {
            Context c = fragmentHostCallback.c();
            Bundle bundle = this.m;
            if (bundle != null) {
                bundle.setClassLoader(c.getClassLoader());
            }
            if (yaVar != null) {
                this.p = yaVar.a(c, this.e, this.m);
            } else {
                this.p = Fragment.instantiate(c, this.e, this.m);
            }
            Bundle bundle2 = this.o;
            if (bundle2 != null) {
                bundle2.setClassLoader(c.getClassLoader());
                this.p.mSavedFragmentState = this.o;
            }
            this.p.setIndex(this.f, fragment);
            Fragment fragment2 = this.p;
            fragment2.mFromLayout = this.g;
            fragment2.mRestored = true;
            fragment2.mFragmentId = this.h;
            fragment2.mContainerId = this.i;
            fragment2.mTag = this.j;
            fragment2.mRetainInstance = this.k;
            fragment2.mDetached = this.l;
            fragment2.mHidden = this.n;
            fragment2.mFragmentManager = fragmentHostCallback.e;
            if (FragmentManagerImpl.I) {
                Log.v("FragmentManager", "Instantiated fragment " + this.p);
            }
        }
        Fragment fragment3 = this.p;
        fragment3.mChildNonConfig = fragmentManagerNonConfig;
        fragment3.mViewModelStore = viewModelStore;
        return fragment3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g ? 1 : 0);
        parcel.writeInt(this.h);
        parcel.writeInt(this.i);
        parcel.writeString(this.j);
        parcel.writeInt(this.k ? 1 : 0);
        parcel.writeInt(this.l ? 1 : 0);
        parcel.writeBundle(this.m);
        parcel.writeInt(this.n ? 1 : 0);
        parcel.writeBundle(this.o);
    }

    @DexIgnore
    public ab(Parcel parcel) {
        this.e = parcel.readString();
        this.f = parcel.readInt();
        boolean z = true;
        this.g = parcel.readInt() != 0;
        this.h = parcel.readInt();
        this.i = parcel.readInt();
        this.j = parcel.readString();
        this.k = parcel.readInt() != 0;
        this.l = parcel.readInt() != 0;
        this.m = parcel.readBundle();
        this.n = parcel.readInt() == 0 ? false : z;
        this.o = parcel.readBundle();
    }
}
