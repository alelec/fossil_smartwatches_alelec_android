package com.fossil.blesdk.obfuscated;

import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ll4 {
    @DexIgnore
    public static /* final */ ll4 c; // = new a().a();
    @DexIgnore
    public /* final */ Set<b> a;
    @DexIgnore
    public /* final */ eo4 b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<b> a; // = new ArrayList();

        @DexIgnore
        public ll4 a() {
            return new ll4(new LinkedHashSet(this.a), (eo4) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ ByteString d;

        @DexIgnore
        public boolean a(String str) {
            if (!this.a.startsWith("*.")) {
                return str.equals(this.b);
            }
            int indexOf = str.indexOf(46);
            if ((str.length() - indexOf) - 1 == this.b.length()) {
                String str2 = this.b;
                if (str.regionMatches(false, indexOf + 1, str2, 0, str2.length())) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof b) {
                b bVar = (b) obj;
                return this.a.equals(bVar.a) && this.c.equals(bVar.c) && this.d.equals(bVar.d);
            }
        }

        @DexIgnore
        public int hashCode() {
            return ((((527 + this.a.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.c + this.d.base64();
        }
    }

    @DexIgnore
    public ll4(Set<b> set, eo4 eo4) {
        this.a = set;
        this.b = eo4;
    }

    @DexIgnore
    public static ByteString b(X509Certificate x509Certificate) {
        return ByteString.of(x509Certificate.getPublicKey().getEncoded()).sha256();
    }

    @DexIgnore
    public void a(String str, List<Certificate> list) throws SSLPeerUnverifiedException {
        List<b> a2 = a(str);
        if (!a2.isEmpty()) {
            eo4 eo4 = this.b;
            if (eo4 != null) {
                list = eo4.a(list, str);
            }
            int size = list.size();
            for (int i = 0; i < size; i++) {
                X509Certificate x509Certificate = (X509Certificate) list.get(i);
                int size2 = a2.size();
                ByteString byteString = null;
                ByteString byteString2 = null;
                for (int i2 = 0; i2 < size2; i2++) {
                    b bVar = a2.get(i2);
                    if (bVar.c.equals("sha256/")) {
                        if (byteString == null) {
                            byteString = b(x509Certificate);
                        }
                        if (bVar.d.equals(byteString)) {
                            return;
                        }
                    } else if (bVar.c.equals("sha1/")) {
                        if (byteString2 == null) {
                            byteString2 = a(x509Certificate);
                        }
                        if (bVar.d.equals(byteString2)) {
                            return;
                        }
                    } else {
                        throw new AssertionError("unsupported hashAlgorithm: " + bVar.c);
                    }
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Certificate pinning failure!");
            sb.append("\n  Peer certificate chain:");
            int size3 = list.size();
            for (int i3 = 0; i3 < size3; i3++) {
                X509Certificate x509Certificate2 = (X509Certificate) list.get(i3);
                sb.append("\n    ");
                sb.append(a((Certificate) x509Certificate2));
                sb.append(": ");
                sb.append(x509Certificate2.getSubjectDN().getName());
            }
            sb.append("\n  Pinned certificates for ");
            sb.append(str);
            sb.append(":");
            int size4 = a2.size();
            for (int i4 = 0; i4 < size4; i4++) {
                sb.append("\n    ");
                sb.append(a2.get(i4));
            }
            throw new SSLPeerUnverifiedException(sb.toString());
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ll4) {
            ll4 ll4 = (ll4) obj;
            if (!jm4.a((Object) this.b, (Object) ll4.b) || !this.a.equals(ll4.a)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        eo4 eo4 = this.b;
        return ((eo4 != null ? eo4.hashCode() : 0) * 31) + this.a.hashCode();
    }

    @DexIgnore
    public List<b> a(String str) {
        List<b> emptyList = Collections.emptyList();
        for (b next : this.a) {
            if (next.a(str)) {
                if (emptyList.isEmpty()) {
                    emptyList = new ArrayList<>();
                }
                emptyList.add(next);
            }
        }
        return emptyList;
    }

    @DexIgnore
    public ll4 a(eo4 eo4) {
        if (jm4.a((Object) this.b, (Object) eo4)) {
            return this;
        }
        return new ll4(this.a, eo4);
    }

    @DexIgnore
    public static String a(Certificate certificate) {
        if (certificate instanceof X509Certificate) {
            return "sha256/" + b((X509Certificate) certificate).base64();
        }
        throw new IllegalArgumentException("Certificate pinning requires X509 certificates");
    }

    @DexIgnore
    public static ByteString a(X509Certificate x509Certificate) {
        return ByteString.of(x509Certificate.getPublicKey().getEncoded()).sha1();
    }
}
