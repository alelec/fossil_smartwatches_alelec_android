package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ja */
public class C2114ja {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ja$a */
    public interface C2115a<T> {
        @DexIgnore
        /* renamed from: a */
        void mo11879a(T t, android.graphics.Rect rect);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ja$b */
    public interface C2116b<T, V> {
        @DexIgnore
        /* renamed from: a */
        int mo11881a(T t);

        @DexIgnore
        /* renamed from: a */
        V mo11883a(T t, int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ja$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ja$c */
    public static class C2117c<T> implements java.util.Comparator<T> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.graphics.Rect f6388e; // = new android.graphics.Rect();

        @DexIgnore
        /* renamed from: f */
        public /* final */ android.graphics.Rect f6389f; // = new android.graphics.Rect();

        @DexIgnore
        /* renamed from: g */
        public /* final */ boolean f6390g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ com.fossil.blesdk.obfuscated.C2114ja.C2115a<T> f6391h;

        @DexIgnore
        public C2117c(boolean z, com.fossil.blesdk.obfuscated.C2114ja.C2115a<T> aVar) {
            this.f6390g = z;
            this.f6391h = aVar;
        }

        @DexIgnore
        public int compare(T t, T t2) {
            android.graphics.Rect rect = this.f6388e;
            android.graphics.Rect rect2 = this.f6389f;
            this.f6391h.mo11879a(t, rect);
            this.f6391h.mo11879a(t2, rect2);
            int i = rect.top;
            int i2 = rect2.top;
            if (i < i2) {
                return -1;
            }
            if (i > i2) {
                return 1;
            }
            int i3 = rect.left;
            int i4 = rect2.left;
            if (i3 < i4) {
                if (this.f6390g) {
                    return 1;
                }
                return -1;
            } else if (i3 <= i4) {
                int i5 = rect.bottom;
                int i6 = rect2.bottom;
                if (i5 < i6) {
                    return -1;
                }
                if (i5 > i6) {
                    return 1;
                }
                int i7 = rect.right;
                int i8 = rect2.right;
                if (i7 < i8) {
                    if (this.f6390g) {
                        return 1;
                    }
                    return -1;
                } else if (i7 <= i8) {
                    return 0;
                } else {
                    if (this.f6390g) {
                        return -1;
                    }
                    return 1;
                }
            } else if (this.f6390g) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m8903a(int i, int i2) {
        return (i * 13 * i) + (i2 * i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static <L, T> T m8904a(L l, com.fossil.blesdk.obfuscated.C2114ja.C2116b<L, T> bVar, com.fossil.blesdk.obfuscated.C2114ja.C2115a<T> aVar, T t, int i, boolean z, boolean z2) {
        int a = bVar.mo11881a(l);
        java.util.ArrayList arrayList = new java.util.ArrayList(a);
        for (int i2 = 0; i2 < a; i2++) {
            arrayList.add(bVar.mo11883a(l, i2));
        }
        java.util.Collections.sort(arrayList, new com.fossil.blesdk.obfuscated.C2114ja.C2117c(z, aVar));
        if (i == 1) {
            return m8910b(t, arrayList, z2);
        }
        if (i == 2) {
            return m8906a(t, arrayList, z2);
        }
        throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD}.");
    }

    @DexIgnore
    /* renamed from: b */
    public static <T> T m8910b(T t, java.util.ArrayList<T> arrayList, boolean z) {
        int i;
        int size = arrayList.size();
        if (t == null) {
            i = size;
        } else {
            i = arrayList.indexOf(t);
        }
        int i2 = i - 1;
        if (i2 >= 0) {
            return arrayList.get(i2);
        }
        if (!z || size <= 0) {
            return null;
        }
        return arrayList.get(size - 1);
    }

    @DexIgnore
    /* renamed from: c */
    public static int m8913c(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        return java.lang.Math.max(0, m8914d(i, rect, rect2));
    }

    @DexIgnore
    /* renamed from: d */
    public static int m8914d(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        int i2;
        int i3;
        if (i == 17) {
            i2 = rect.left;
            i3 = rect2.right;
        } else if (i == 33) {
            i2 = rect.top;
            i3 = rect2.bottom;
        } else if (i == 66) {
            i2 = rect2.left;
            i3 = rect.right;
        } else if (i == 130) {
            i2 = rect2.top;
            i3 = rect.bottom;
        } else {
            throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return i2 - i3;
    }

    @DexIgnore
    /* renamed from: e */
    public static int m8915e(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        return java.lang.Math.max(1, m8916f(i, rect, rect2));
    }

    @DexIgnore
    /* renamed from: f */
    public static int m8916f(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        int i2;
        int i3;
        if (i == 17) {
            i2 = rect.left;
            i3 = rect2.left;
        } else if (i == 33) {
            i2 = rect.top;
            i3 = rect2.top;
        } else if (i == 66) {
            i2 = rect2.right;
            i3 = rect.right;
        } else if (i == 130) {
            i2 = rect2.bottom;
            i3 = rect.bottom;
        } else {
            throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return i2 - i3;
    }

    @DexIgnore
    /* renamed from: g */
    public static int m8917g(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    if (i != 130) {
                        throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                }
            }
            return java.lang.Math.abs((rect.left + (rect.width() / 2)) - (rect2.left + (rect2.width() / 2)));
        }
        return java.lang.Math.abs((rect.top + (rect.height() / 2)) - (rect2.top + (rect2.height() / 2)));
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m8912b(int i, android.graphics.Rect rect, android.graphics.Rect rect2, android.graphics.Rect rect3) {
        if (!m8909a(rect, rect2, i)) {
            return false;
        }
        if (!m8909a(rect, rect3, i) || m8908a(i, rect, rect2, rect3)) {
            return true;
        }
        if (!m8908a(i, rect, rect3, rect2) && m8903a(m8913c(i, rect, rect2), m8917g(i, rect, rect2)) < m8903a(m8913c(i, rect, rect3), m8917g(i, rect, rect3))) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> T m8906a(T t, java.util.ArrayList<T> arrayList, boolean z) {
        int i;
        int size = arrayList.size();
        if (t == null) {
            i = -1;
        } else {
            i = arrayList.lastIndexOf(t);
        }
        int i2 = i + 1;
        if (i2 < size) {
            return arrayList.get(i2);
        }
        if (!z || size <= 0) {
            return null;
        }
        return arrayList.get(0);
    }

    @DexIgnore
    /* renamed from: a */
    public static <L, T> T m8905a(L l, com.fossil.blesdk.obfuscated.C2114ja.C2116b<L, T> bVar, com.fossil.blesdk.obfuscated.C2114ja.C2115a<T> aVar, T t, android.graphics.Rect rect, int i) {
        android.graphics.Rect rect2 = new android.graphics.Rect(rect);
        if (i == 17) {
            rect2.offset(rect.width() + 1, 0);
        } else if (i == 33) {
            rect2.offset(0, rect.height() + 1);
        } else if (i == 66) {
            rect2.offset(-(rect.width() + 1), 0);
        } else if (i == 130) {
            rect2.offset(0, -(rect.height() + 1));
        } else {
            throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        T t2 = null;
        int a = bVar.mo11881a(l);
        android.graphics.Rect rect3 = new android.graphics.Rect();
        for (int i2 = 0; i2 < a; i2++) {
            T a2 = bVar.mo11883a(l, i2);
            if (a2 != t) {
                aVar.mo11879a(a2, rect3);
                if (m8912b(i, rect, rect3, rect2)) {
                    rect2.set(rect3);
                    t2 = a2;
                }
            }
        }
        return t2;
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m8911b(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    if (i == 130) {
                        return rect.bottom <= rect2.top;
                    }
                    throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                } else if (rect.right <= rect2.left) {
                    return true;
                } else {
                    return false;
                }
            } else if (rect.top >= rect2.bottom) {
                return true;
            } else {
                return false;
            }
        } else if (rect.left >= rect2.right) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m8908a(int i, android.graphics.Rect rect, android.graphics.Rect rect2, android.graphics.Rect rect3) {
        boolean a = m8907a(i, rect, rect2);
        if (m8907a(i, rect, rect3) || !a) {
            return false;
        }
        if (m8911b(i, rect, rect3) && i != 17 && i != 66 && m8913c(i, rect, rect2) >= m8915e(i, rect, rect3)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m8909a(android.graphics.Rect rect, android.graphics.Rect rect2, int i) {
        if (i == 17) {
            int i2 = rect.right;
            int i3 = rect2.right;
            if ((i2 > i3 || rect.left >= i3) && rect.left > rect2.left) {
                return true;
            }
            return false;
        } else if (i == 33) {
            int i4 = rect.bottom;
            int i5 = rect2.bottom;
            if ((i4 > i5 || rect.top >= i5) && rect.top > rect2.top) {
                return true;
            }
            return false;
        } else if (i == 66) {
            int i6 = rect.left;
            int i7 = rect2.left;
            if ((i6 < i7 || rect.right <= i7) && rect.right < rect2.right) {
                return true;
            }
            return false;
        } else if (i == 130) {
            int i8 = rect.top;
            int i9 = rect2.top;
            return (i8 < i9 || rect.bottom <= i9) && rect.bottom < rect2.bottom;
        } else {
            throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m8907a(int i, android.graphics.Rect rect, android.graphics.Rect rect2) {
        if (i != 17) {
            if (i != 33) {
                if (i != 66) {
                    if (i != 130) {
                        throw new java.lang.IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
                    }
                }
            }
            if (rect2.right < rect.left || rect2.left > rect.right) {
                return false;
            }
            return true;
        }
        if (rect2.bottom < rect.top || rect2.top > rect.bottom) {
            return false;
        }
        return true;
    }
}
