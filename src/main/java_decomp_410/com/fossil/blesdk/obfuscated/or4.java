package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.obfuscated.bm4;
import com.fossil.blesdk.obfuscated.dm4;
import com.fossil.blesdk.obfuscated.wl4;
import com.fossil.blesdk.obfuscated.yl4;
import com.fossil.blesdk.obfuscated.zl4;
import java.io.IOException;
import java.util.regex.Pattern;
import okhttp3.RequestBody;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class or4 {
    @DexIgnore
    public static /* final */ char[] l; // = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(.*/)?(\\.|%2e|%2E){1,2}(/.*)?");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ zl4 b;
    @DexIgnore
    public String c;
    @DexIgnore
    public zl4.a d;
    @DexIgnore
    public /* final */ dm4.a e; // = new dm4.a();
    @DexIgnore
    public /* final */ yl4.a f;
    @DexIgnore
    public am4 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public bm4.a i;
    @DexIgnore
    public wl4.a j;
    @DexIgnore
    public RequestBody k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RequestBody {
        @DexIgnore
        public /* final */ RequestBody a;
        @DexIgnore
        public /* final */ am4 b;

        @DexIgnore
        public a(RequestBody requestBody, am4 am4) {
            this.a = requestBody;
            this.b = am4;
        }

        @DexIgnore
        public long a() throws IOException {
            return this.a.a();
        }

        @DexIgnore
        public am4 b() {
            return this.b;
        }

        @DexIgnore
        public void a(ko4 ko4) throws IOException {
            this.a.a(ko4);
        }
    }

    @DexIgnore
    public or4(String str, zl4 zl4, String str2, yl4 yl4, am4 am4, boolean z, boolean z2, boolean z3) {
        this.a = str;
        this.b = zl4;
        this.c = str2;
        this.g = am4;
        this.h = z;
        if (yl4 != null) {
            this.f = yl4.a();
        } else {
            this.f = new yl4.a();
        }
        if (z2) {
            this.j = new wl4.a();
        } else if (z3) {
            this.i = new bm4.a();
            this.i.a(bm4.f);
        }
    }

    @DexIgnore
    public void a(Object obj) {
        this.c = obj.toString();
    }

    @DexIgnore
    public void b(String str, String str2, boolean z) {
        if (this.c != null) {
            String a2 = a(str2, z);
            String str3 = this.c;
            String replace = str3.replace("{" + str + "}", a2);
            if (!m.matcher(replace).matches()) {
                this.c = replace;
                return;
            }
            throw new IllegalArgumentException("@Path parameters shouldn't perform path traversal ('.' or '..'): " + str2);
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void c(String str, String str2, boolean z) {
        String str3 = this.c;
        if (str3 != null) {
            this.d = this.b.a(str3);
            if (this.d != null) {
                this.c = null;
            } else {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        if (z) {
            this.d.a(str, str2);
        } else {
            this.d.b(str, str2);
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (GraphRequest.CONTENT_TYPE_HEADER.equalsIgnoreCase(str)) {
            try {
                this.g = am4.a(str2);
            } catch (IllegalArgumentException e2) {
                throw new IllegalArgumentException("Malformed content type: " + str2, e2);
            }
        } else {
            this.f.a(str, str2);
        }
    }

    @DexIgnore
    public static String a(String str, boolean z) {
        int length = str.length();
        int i2 = 0;
        while (i2 < length) {
            int codePointAt = str.codePointAt(i2);
            if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                jo4 jo4 = new jo4();
                jo4.a(str, 0, i2);
                a(jo4, str, i2, length, z);
                return jo4.z();
            }
            i2 += Character.charCount(codePointAt);
        }
        return str;
    }

    @DexIgnore
    public static void a(jo4 jo4, String str, int i2, int i3, boolean z) {
        jo4 jo42 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt < 32 || codePointAt >= 127 || " \"<>^`{}|\\?#".indexOf(codePointAt) != -1 || (!z && (codePointAt == 47 || codePointAt == 37))) {
                    if (jo42 == null) {
                        jo42 = new jo4();
                    }
                    jo42.c(codePointAt);
                    while (!jo42.g()) {
                        byte readByte = jo42.readByte() & FileType.MASKED_INDEX;
                        jo4.writeByte(37);
                        jo4.writeByte((int) l[(readByte >> 4) & 15]);
                        jo4.writeByte((int) l[readByte & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY]);
                    }
                } else {
                    jo4.c(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        if (z) {
            this.j.b(str, str2);
        } else {
            this.j.a(str, str2);
        }
    }

    @DexIgnore
    public void a(yl4 yl4, RequestBody requestBody) {
        this.i.a(yl4, requestBody);
    }

    @DexIgnore
    public void a(bm4.b bVar) {
        this.i.a(bVar);
    }

    @DexIgnore
    public void a(RequestBody requestBody) {
        this.k = requestBody;
    }

    @DexIgnore
    public dm4.a a() {
        zl4 zl4;
        zl4.a aVar = this.d;
        if (aVar != null) {
            zl4 = aVar.a();
        } else {
            zl4 = this.b.b(this.c);
            if (zl4 == null) {
                throw new IllegalArgumentException("Malformed URL. Base: " + this.b + ", Relative: " + this.c);
            }
        }
        a aVar2 = this.k;
        if (aVar2 == null) {
            wl4.a aVar3 = this.j;
            if (aVar3 != null) {
                aVar2 = aVar3.a();
            } else {
                bm4.a aVar4 = this.i;
                if (aVar4 != null) {
                    aVar2 = aVar4.a();
                } else if (this.h) {
                    aVar2 = RequestBody.a((am4) null, new byte[0]);
                }
            }
        }
        am4 am4 = this.g;
        if (am4 != null) {
            if (aVar2 != null) {
                aVar2 = new a(aVar2, am4);
            } else {
                this.f.a(GraphRequest.CONTENT_TYPE_HEADER, am4.toString());
            }
        }
        dm4.a aVar5 = this.e;
        aVar5.a(zl4);
        aVar5.a(this.f.a());
        aVar5.a(this.a, aVar2);
        return aVar5;
    }
}
