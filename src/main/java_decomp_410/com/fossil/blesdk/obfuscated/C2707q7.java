package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q7 */
public final class C2707q7 {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.Locale[] f8553b; // = new java.util.Locale[0];

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Locale[] f8554a;

    /*
    static {
        new com.fossil.blesdk.obfuscated.C2707q7(new java.util.Locale[0]);
        new java.util.Locale("en", "XA");
        new java.util.Locale("ar", "XB");
        com.fossil.blesdk.obfuscated.C2533o7.m11628a("en-Latn");
    }
    */

    @DexIgnore
    public C2707q7(java.util.Locale... localeArr) {
        if (localeArr.length == 0) {
            this.f8554a = f8553b;
            return;
        }
        java.util.Locale[] localeArr2 = new java.util.Locale[localeArr.length];
        java.util.HashSet hashSet = new java.util.HashSet();
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        int i = 0;
        while (i < localeArr.length) {
            java.util.Locale locale = localeArr[i];
            if (locale == null) {
                throw new java.lang.NullPointerException("list[" + i + "] is null");
            } else if (!hashSet.contains(locale)) {
                java.util.Locale locale2 = (java.util.Locale) locale.clone();
                localeArr2[i] = locale2;
                sb.append(com.fossil.blesdk.obfuscated.C2533o7.m11627a(locale2));
                if (i < localeArr.length - 1) {
                    sb.append(',');
                }
                hashSet.add(locale2);
                i++;
            } else {
                throw new java.lang.IllegalArgumentException("list[" + i + "] is a repetition");
            }
        }
        this.f8554a = localeArr2;
        sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Locale mo15039a(int i) {
        if (i >= 0) {
            java.util.Locale[] localeArr = this.f8554a;
            if (i < localeArr.length) {
                return localeArr[i];
            }
        }
        return null;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2707q7)) {
            return false;
        }
        java.util.Locale[] localeArr = ((com.fossil.blesdk.obfuscated.C2707q7) obj).f8554a;
        if (this.f8554a.length != localeArr.length) {
            return false;
        }
        int i = 0;
        while (true) {
            java.util.Locale[] localeArr2 = this.f8554a;
            if (i >= localeArr2.length) {
                return true;
            }
            if (!localeArr2[i].equals(localeArr[i])) {
                return false;
            }
            i++;
        }
    }

    @DexIgnore
    public int hashCode() {
        int i = 1;
        int i2 = 0;
        while (true) {
            java.util.Locale[] localeArr = this.f8554a;
            if (i2 >= localeArr.length) {
                return i;
            }
            i = (i * 31) + localeArr[i2].hashCode();
            i2++;
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("[");
        int i = 0;
        while (true) {
            java.util.Locale[] localeArr = this.f8554a;
            if (i < localeArr.length) {
                sb.append(localeArr[i]);
                if (i < this.f8554a.length - 1) {
                    sb.append(',');
                }
                i++;
            } else {
                sb.append("]");
                return sb.toString();
            }
        }
    }
}
