package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v31 extends l31 implements u31 {
    @DexIgnore
    public v31(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    @DexIgnore
    public final void a(k41 k41) throws RemoteException {
        Parcel o = o();
        q41.a(o, (Parcelable) k41);
        b(59, o);
    }

    @DexIgnore
    public final void a(tc1 tc1, w31 w31, String str) throws RemoteException {
        Parcel o = o();
        q41.a(o, (Parcelable) tc1);
        q41.a(o, (IInterface) w31);
        o.writeString(str);
        b(63, o);
    }

    @DexIgnore
    public final void a(v41 v41) throws RemoteException {
        Parcel o = o();
        q41.a(o, (Parcelable) v41);
        b(75, o);
    }

    @DexIgnore
    public final Location b(String str) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        Parcel a = a(21, o);
        Location location = (Location) q41.a(a, Location.CREATOR);
        a.recycle();
        return location;
    }

    @DexIgnore
    public final void e(boolean z) throws RemoteException {
        Parcel o = o();
        q41.a(o, z);
        b(12, o);
    }
}
