package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.SharedElementCallback;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.view.View;
import androidx.core.app.SharedElementCallback;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v5 extends k6 {
    @DexIgnore
    public static c c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ String[] e;
        @DexIgnore
        public /* final */ /* synthetic */ Activity f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;

        @DexIgnore
        public a(String[] strArr, Activity activity, int i) {
            this.e = strArr;
            this.f = activity;
            this.g = i;
        }

        @DexIgnore
        public void run() {
            int[] iArr = new int[this.e.length];
            PackageManager packageManager = this.f.getPackageManager();
            String packageName = this.f.getPackageName();
            int length = this.e.length;
            for (int i = 0; i < length; i++) {
                iArr[i] = packageManager.checkPermission(this.e[i], packageName);
            }
            ((b) this.f).onRequestPermissionsResult(this.g, this.e, iArr);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        boolean a(Activity activity, int i, int i2, Intent intent);

        @DexIgnore
        boolean a(Activity activity, String[] strArr, int i);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void validateRequestPermissionsRequestCode(int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends SharedElementCallback {
        @DexIgnore
        public /* final */ androidx.core.app.SharedElementCallback a;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements SharedElementCallback.a {
            @DexIgnore
            public /* final */ /* synthetic */ SharedElementCallback.OnSharedElementsReadyListener a;

            @DexIgnore
            public a(e eVar, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
                this.a = onSharedElementsReadyListener;
            }

            @DexIgnore
            public void a() {
                this.a.onSharedElementsReady();
            }
        }

        @DexIgnore
        public e(androidx.core.app.SharedElementCallback sharedElementCallback) {
            this.a = sharedElementCallback;
        }

        @DexIgnore
        public Parcelable onCaptureSharedElementSnapshot(View view, Matrix matrix, RectF rectF) {
            return this.a.a(view, matrix, rectF);
        }

        @DexIgnore
        public View onCreateSnapshotView(Context context, Parcelable parcelable) {
            return this.a.a(context, parcelable);
        }

        @DexIgnore
        public void onMapSharedElements(List<String> list, Map<String, View> map) {
            this.a.a(list, map);
        }

        @DexIgnore
        public void onRejectSharedElements(List<View> list) {
            this.a.a(list);
        }

        @DexIgnore
        public void onSharedElementEnd(List<String> list, List<View> list2, List<View> list3) {
            this.a.a(list, list2, list3);
        }

        @DexIgnore
        public void onSharedElementStart(List<String> list, List<View> list2, List<View> list3) {
            this.a.b(list, list2, list3);
        }

        @DexIgnore
        public void onSharedElementsArrived(List<String> list, List<View> list2, SharedElementCallback.OnSharedElementsReadyListener onSharedElementsReadyListener) {
            this.a.a(list, list2, (SharedElementCallback.a) new a(this, onSharedElementsReadyListener));
        }
    }

    @DexIgnore
    public static c a() {
        return c;
    }

    @DexIgnore
    public static void b(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    public static void c(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.postponeEnterTransition();
        }
    }

    @DexIgnore
    public static void d(Activity activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.startPostponedEnterTransition();
        }
    }

    @DexIgnore
    public static void a(Activity activity, Intent intent, int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startActivityForResult(intent, i, bundle);
        } else {
            activity.startActivityForResult(intent, i);
        }
    }

    @DexIgnore
    public static void b(Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setExitSharedElementCallback(sharedElementCallback != null ? new e(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    public static void a(Activity activity, IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4, Bundle bundle) throws IntentSender.SendIntentException {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4, bundle);
        } else {
            activity.startIntentSenderForResult(intentSender, i, intent, i2, i3, i4);
        }
    }

    @DexIgnore
    public static void a(Activity activity) {
        if (Build.VERSION.SDK_INT >= 16) {
            activity.finishAffinity();
        } else {
            activity.finish();
        }
    }

    @DexIgnore
    public static void a(Activity activity, androidx.core.app.SharedElementCallback sharedElementCallback) {
        if (Build.VERSION.SDK_INT >= 21) {
            activity.setEnterSharedElementCallback(sharedElementCallback != null ? new e(sharedElementCallback) : null);
        }
    }

    @DexIgnore
    public static void a(Activity activity, String[] strArr, int i) {
        c cVar = c;
        if (cVar != null && cVar.a(activity, strArr, i)) {
            return;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity instanceof d) {
                ((d) activity).validateRequestPermissionsRequestCode(i);
            }
            activity.requestPermissions(strArr, i);
        } else if (activity instanceof b) {
            new Handler(Looper.getMainLooper()).post(new a(strArr, activity, i));
        }
    }

    @DexIgnore
    public static boolean a(Activity activity, String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return activity.shouldShowRequestPermissionRationale(str);
        }
        return false;
    }
}
