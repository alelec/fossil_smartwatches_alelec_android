package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v3 {
    @DexIgnore
    public static /* final */ int cardview_dark_background; // = 2131099718;
    @DexIgnore
    public static /* final */ int cardview_light_background; // = 2131099719;
    @DexIgnore
    public static /* final */ int cardview_shadow_end_color; // = 2131099720;
    @DexIgnore
    public static /* final */ int cardview_shadow_start_color; // = 2131099721;
}
