package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ex */
public class C1752ex {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f4963a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f4964b;

    @DexIgnore
    /* renamed from: c */
    public boolean f4965c;

    @DexIgnore
    public C1752ex(int i, int i2, boolean z) {
        this.f4963a = i;
        this.f4964b = i2;
        this.f4965c = z;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo10634a(java.lang.String str) {
        int length = str.length();
        int i = this.f4964b;
        if (length <= i) {
            return str;
        }
        mo10635a((java.lang.RuntimeException) new java.lang.IllegalArgumentException(java.lang.String.format(java.util.Locale.US, "String is too long, truncating to %d characters", new java.lang.Object[]{java.lang.Integer.valueOf(i)})));
        return str.substring(0, this.f4964b);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10636a(java.lang.Object obj, java.lang.String str) {
        if (obj != null) {
            return false;
        }
        mo10635a((java.lang.RuntimeException) new java.lang.NullPointerException(str + " must not be null"));
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10637a(java.util.Map<java.lang.String, java.lang.Object> map, java.lang.String str) {
        if (map.size() < this.f4963a || map.containsKey(str)) {
            return false;
        }
        mo10635a((java.lang.RuntimeException) new java.lang.IllegalArgumentException(java.lang.String.format(java.util.Locale.US, "Limit of %d attributes reached, skipping attribute", new java.lang.Object[]{java.lang.Integer.valueOf(this.f4963a)})));
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10635a(java.lang.RuntimeException runtimeException) {
        if (!this.f4965c) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("Answers", "Invalid user input detected", runtimeException);
            return;
        }
        throw runtimeException;
    }
}
