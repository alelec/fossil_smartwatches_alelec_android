package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h6 */
public final class C1926h6 {
    @DexIgnore
    /* renamed from: a */
    public static android.app.RemoteInput[] m7802a(com.fossil.blesdk.obfuscated.C1926h6[] h6VarArr) {
        if (h6VarArr == null) {
            return null;
        }
        android.app.RemoteInput[] remoteInputArr = new android.app.RemoteInput[h6VarArr.length];
        if (h6VarArr.length <= 0) {
            return remoteInputArr;
        }
        m7801a(h6VarArr[0]);
        throw null;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo11598a() {
        throw null;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.app.RemoteInput m7801a(com.fossil.blesdk.obfuscated.C1926h6 h6Var) {
        h6Var.mo11598a();
        throw null;
    }
}
