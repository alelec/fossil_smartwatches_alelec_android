package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;
import java.util.concurrent.ScheduledExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fx implements r64 {
    @DexIgnore
    public /* final */ v44 a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ gx c;
    @DexIgnore
    public /* final */ ey d;
    @DexIgnore
    public /* final */ z64 e;
    @DexIgnore
    public /* final */ rx f;
    @DexIgnore
    public /* final */ ScheduledExecutorService g;
    @DexIgnore
    public by h; // = new nx();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ j74 e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore
        public a(j74 j74, String str) {
            this.e = j74;
            this.f = str;
        }

        @DexIgnore
        public void run() {
            try {
                fx.this.h.a(this.e, this.f);
            } catch (Exception e2) {
                q44.g().e("Answers", "Failed to set analytics settings data", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            try {
                by byVar = fx.this.h;
                fx.this.h = new nx();
                byVar.d();
            } catch (Exception e2) {
                q44.g().e("Answers", "Failed to disable events", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                fx.this.h.a();
            } catch (Exception e2) {
                q44.g().e("Answers", "Failed to send events files", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            try {
                cy a = fx.this.d.a();
                yx a2 = fx.this.c.a();
                a2.a((r64) fx.this);
                fx.this.h = new ox(fx.this.a, fx.this.b, fx.this.g, a2, fx.this.e, a, fx.this.f);
            } catch (Exception e2) {
                q44.g().e("Answers", "Failed to enable events", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            try {
                fx.this.h.b();
            } catch (Exception e2) {
                q44.g().e("Answers", "Failed to flush events", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ SessionEvent.b e;
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;

        @DexIgnore
        public f(SessionEvent.b bVar, boolean z) {
            this.e = bVar;
            this.f = z;
        }

        @DexIgnore
        public void run() {
            try {
                fx.this.h.a(this.e);
                if (this.f) {
                    fx.this.h.b();
                }
            } catch (Exception e2) {
                q44.g().e("Answers", "Failed to process event", e2);
            }
        }
    }

    @DexIgnore
    public fx(v44 v44, Context context, gx gxVar, ey eyVar, z64 z64, ScheduledExecutorService scheduledExecutorService, rx rxVar) {
        this.a = v44;
        this.b = context;
        this.c = gxVar;
        this.d = eyVar;
        this.e = z64;
        this.g = scheduledExecutorService;
        this.f = rxVar;
    }

    @DexIgnore
    public void a(SessionEvent.b bVar) {
        a(bVar, false, false);
    }

    @DexIgnore
    public void b(SessionEvent.b bVar) {
        a(bVar, false, true);
    }

    @DexIgnore
    public void c(SessionEvent.b bVar) {
        a(bVar, true, false);
    }

    @DexIgnore
    public void a(j74 j74, String str) {
        a((Runnable) new a(j74, str));
    }

    @DexIgnore
    public void b() {
        a((Runnable) new d());
    }

    @DexIgnore
    public void c() {
        a((Runnable) new e());
    }

    @DexIgnore
    public void a() {
        a((Runnable) new b());
    }

    @DexIgnore
    public final void b(Runnable runnable) {
        try {
            this.g.submit(runnable).get();
        } catch (Exception e2) {
            q44.g().e("Answers", "Failed to run events task", e2);
        }
    }

    @DexIgnore
    public void a(String str) {
        a((Runnable) new c());
    }

    @DexIgnore
    public void a(SessionEvent.b bVar, boolean z, boolean z2) {
        f fVar = new f(bVar, z2);
        if (z) {
            b((Runnable) fVar);
        } else {
            a((Runnable) fVar);
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        try {
            this.g.submit(runnable);
        } catch (Exception e2) {
            q44.g().e("Answers", "Failed to submit events task", e2);
        }
    }
}
