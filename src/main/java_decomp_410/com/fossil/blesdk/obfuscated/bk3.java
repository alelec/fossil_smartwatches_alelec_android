package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bk3 implements MembersInjector<LoginActivity> {
    @DexIgnore
    public static void a(LoginActivity loginActivity, in2 in2) {
        loginActivity.B = in2;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, jn2 jn2) {
        loginActivity.C = jn2;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, kn2 kn2) {
        loginActivity.D = kn2;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, MFLoginWechatManager mFLoginWechatManager) {
        loginActivity.E = mFLoginWechatManager;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, LoginPresenter loginPresenter) {
        loginActivity.F = loginPresenter;
    }
}
