package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fg2 extends eg2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public long D;

    /*
    static {
        F.put(R.id.progressBar, 1);
        F.put(R.id.cl_updating_fw, 2);
        F.put(R.id.ftv_update, 3);
        F.put(R.id.progress_update, 4);
        F.put(R.id.ftv_countdown_time, 5);
        F.put(R.id.ftv_update_warning, 6);
        F.put(R.id.rvp_tutorial, 7);
        F.put(R.id.indicator, 8);
        F.put(R.id.fb_continue, 9);
        F.put(R.id.cl_update_fw_fail, 10);
        F.put(R.id.ftv_title_fail, 11);
        F.put(R.id.iv_failed, 12);
        F.put(R.id.ftv_failed_desc, 13);
        F.put(R.id.ftv_help, 14);
        F.put(R.id.fb_try_again, 15);
    }
    */

    @DexIgnore
    public fg2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 16, E, F));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public fg2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[10], objArr[2], objArr[9], objArr[15], objArr[5], objArr[13], objArr[14], objArr[11], objArr[3], objArr[6], objArr[8], objArr[12], objArr[1], objArr[4], objArr[7]);
        this.D = -1;
        this.C = objArr[0];
        this.C.setTag((Object) null);
        a(view);
        f();
    }
}
