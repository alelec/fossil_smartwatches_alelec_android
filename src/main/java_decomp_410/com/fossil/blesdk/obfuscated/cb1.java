package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cb1 implements db1 {
    @DexIgnore
    public /* final */ /* synthetic */ zzte a;

    @DexIgnore
    public cb1(zzte zzte) {
        this.a = zzte;
    }

    @DexIgnore
    public final byte a(int i) {
        return this.a.zzam(i);
    }

    @DexIgnore
    public final int size() {
        return this.a.size();
    }
}
