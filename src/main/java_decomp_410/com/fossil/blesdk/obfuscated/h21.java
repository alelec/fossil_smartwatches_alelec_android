package com.fossil.blesdk.obfuscated;

import com.facebook.places.PlaceManager;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h21 {
    @DexIgnore
    public static /* final */ double c; // = (10.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ double d; // = (1000.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ double e; // = (2000.0d / ((double) TimeUnit.HOURS.toNanos(1)));
    @DexIgnore
    public static /* final */ double f; // = (100.0d / ((double) TimeUnit.SECONDS.toNanos(1)));
    @DexIgnore
    public static /* final */ Set<String> g; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{PlaceManager.PARAM_ALTITUDE, "duration", "food_item", "meal_type", "repetitions", "resistance", "resistance_type", "debug_session", "google.android.fitness.SessionV2"})));
    @DexIgnore
    public static /* final */ h21 h; // = new h21();
    @DexIgnore
    public /* final */ Map<String, Map<String, j21>> a;
    @DexIgnore
    public /* final */ Map<String, j21> b;

    @DexIgnore
    public h21() {
        HashMap hashMap = new HashMap();
        hashMap.put("latitude", new j21(-90.0d, 90.0d));
        hashMap.put("longitude", new j21(-180.0d, 180.0d));
        hashMap.put(PlaceManager.PARAM_ACCURACY, new j21(0.0d, 10000.0d));
        hashMap.put("bpm", new j21(0.0d, 1000.0d));
        hashMap.put(PlaceManager.PARAM_ALTITUDE, new j21(-100000.0d, 100000.0d));
        hashMap.put("percentage", new j21(0.0d, 100.0d));
        hashMap.put("confidence", new j21(0.0d, 100.0d));
        hashMap.put("duration", new j21(0.0d, 9.223372036854776E18d));
        hashMap.put("height", new j21(0.0d, 3.0d));
        hashMap.put(Constants.PROFILE_KEY_UNITS_WEIGHT, new j21(0.0d, 1000.0d));
        hashMap.put(PlaceManager.PARAM_SPEED, new j21(0.0d, 11000.0d));
        this.b = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("com.google.step_count.delta", a("steps", new j21(0.0d, c)));
        hashMap2.put("com.google.calories.consumed", a("calories", new j21(0.0d, d)));
        hashMap2.put("com.google.calories.expended", a("calories", new j21(0.0d, e)));
        hashMap2.put("com.google.distance.delta", a("distance", new j21(0.0d, f)));
        this.a = Collections.unmodifiableMap(hashMap2);
    }

    @DexIgnore
    public static <K, V> Map<K, V> a(K k, V v) {
        HashMap hashMap = new HashMap();
        hashMap.put(k, v);
        return hashMap;
    }

    @DexIgnore
    public final j21 a(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public final j21 a(String str, String str2) {
        Map map = this.a.get(str);
        if (map != null) {
            return (j21) map.get(str2);
        }
        return null;
    }

    @DexIgnore
    public static h21 a() {
        return h;
    }
}
