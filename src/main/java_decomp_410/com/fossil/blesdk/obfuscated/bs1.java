package com.fossil.blesdk.obfuscated;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import com.fossil.blesdk.obfuscated.as1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface bs1 extends as1.a {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements TypeEvaluator<e> {
        @DexIgnore
        public static /* final */ TypeEvaluator<e> b; // = new b();
        @DexIgnore
        public /* final */ e a; // = new e();

        @DexIgnore
        /* renamed from: a */
        public e evaluate(float f, e eVar, e eVar2) {
            this.a.a(vs1.b(eVar.a, eVar2.a, f), vs1.b(eVar.b, eVar2.b, f), vs1.b(eVar.c, eVar2.c, f));
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends Property<bs1, e> {
        @DexIgnore
        public static /* final */ Property<bs1, e> a; // = new c("circularReveal");

        @DexIgnore
        public c(String str) {
            super(e.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public e get(bs1 bs1) {
            return bs1.getRevealInfo();
        }

        @DexIgnore
        /* renamed from: a */
        public void set(bs1 bs1, e eVar) {
            bs1.setRevealInfo(eVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends Property<bs1, Integer> {
        @DexIgnore
        public static /* final */ Property<bs1, Integer> a; // = new d("circularRevealScrimColor");

        @DexIgnore
        public d(String str) {
            super(Integer.class, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Integer get(bs1 bs1) {
            return Integer.valueOf(bs1.getCircularRevealScrimColor());
        }

        @DexIgnore
        /* renamed from: a */
        public void set(bs1 bs1, Integer num) {
            bs1.setCircularRevealScrimColor(num.intValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public void a(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public e(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public void a(e eVar) {
            a(eVar.a, eVar.b, eVar.c);
        }

        @DexIgnore
        public boolean a() {
            return this.c == Float.MAX_VALUE;
        }

        @DexIgnore
        public e(e eVar) {
            this(eVar.a, eVar.b, eVar.c);
        }
    }

    @DexIgnore
    void a();

    @DexIgnore
    void b();

    @DexIgnore
    int getCircularRevealScrimColor();

    @DexIgnore
    e getRevealInfo();

    @DexIgnore
    void setCircularRevealOverlayDrawable(Drawable drawable);

    @DexIgnore
    void setCircularRevealScrimColor(int i);

    @DexIgnore
    void setRevealInfo(e eVar);
}
