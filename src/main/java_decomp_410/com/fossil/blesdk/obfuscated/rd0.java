package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.md0;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rd0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<rd0> CREATOR; // = new sd0();
    @DexIgnore
    public uy0 e;
    @DexIgnore
    public byte[] f;
    @DexIgnore
    public int[] g;
    @DexIgnore
    public String[] h;
    @DexIgnore
    public int[] i;
    @DexIgnore
    public byte[][] j;
    @DexIgnore
    public hm1[] k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public /* final */ jy0 m;
    @DexIgnore
    public /* final */ md0.c n;
    @DexIgnore
    public /* final */ md0.c o;

    @DexIgnore
    public rd0(uy0 uy0, jy0 jy0, md0.c cVar, md0.c cVar2, int[] iArr, String[] strArr, int[] iArr2, byte[][] bArr, hm1[] hm1Arr, boolean z) {
        this.e = uy0;
        this.m = jy0;
        this.n = cVar;
        this.o = null;
        this.g = iArr;
        this.h = null;
        this.i = iArr2;
        this.j = null;
        this.k = null;
        this.l = z;
    }

    @DexIgnore
    public rd0(uy0 uy0, byte[] bArr, int[] iArr, String[] strArr, int[] iArr2, byte[][] bArr2, boolean z, hm1[] hm1Arr) {
        this.e = uy0;
        this.f = bArr;
        this.g = iArr;
        this.h = strArr;
        this.m = null;
        this.n = null;
        this.o = null;
        this.i = iArr2;
        this.j = bArr2;
        this.k = hm1Arr;
        this.l = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof rd0) {
            rd0 rd0 = (rd0) obj;
            return zj0.a(this.e, rd0.e) && Arrays.equals(this.f, rd0.f) && Arrays.equals(this.g, rd0.g) && Arrays.equals(this.h, rd0.h) && zj0.a(this.m, rd0.m) && zj0.a(this.n, rd0.n) && zj0.a(this.o, rd0.o) && Arrays.equals(this.i, rd0.i) && Arrays.deepEquals(this.j, rd0.j) && Arrays.equals(this.k, rd0.k) && this.l == rd0.l;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(this.e, this.f, this.g, this.h, this.m, this.n, this.o, this.i, this.j, this.k, Boolean.valueOf(this.l));
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder("LogEventParcelable[");
        sb.append(this.e);
        sb.append(", LogEventBytes: ");
        byte[] bArr = this.f;
        sb.append(bArr == null ? null : new String(bArr));
        sb.append(", TestCodes: ");
        sb.append(Arrays.toString(this.g));
        sb.append(", MendelPackages: ");
        sb.append(Arrays.toString(this.h));
        sb.append(", LogEvent: ");
        sb.append(this.m);
        sb.append(", ExtensionProducer: ");
        sb.append(this.n);
        sb.append(", VeProducer: ");
        sb.append(this.o);
        sb.append(", ExperimentIDs: ");
        sb.append(Arrays.toString(this.i));
        sb.append(", ExperimentTokens: ");
        sb.append(Arrays.toString(this.j));
        sb.append(", ExperimentTokensParcelables: ");
        sb.append(Arrays.toString(this.k));
        sb.append(", AddPhenotypeExperimentTokens: ");
        sb.append(this.l);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 2, (Parcelable) this.e, i2, false);
        kk0.a(parcel, 3, this.f, false);
        kk0.a(parcel, 4, this.g, false);
        kk0.a(parcel, 5, this.h, false);
        kk0.a(parcel, 6, this.i, false);
        kk0.a(parcel, 7, this.j, false);
        kk0.a(parcel, 8, this.l);
        kk0.a(parcel, 9, (T[]) this.k, i2, false);
        kk0.a(parcel, a);
    }
}
