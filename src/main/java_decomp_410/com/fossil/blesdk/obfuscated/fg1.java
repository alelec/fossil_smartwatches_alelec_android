package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fg1 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> e; // = this.f.e.keySet().iterator();
    @DexIgnore
    public /* final */ /* synthetic */ eg1 f;

    @DexIgnore
    public fg1(eg1 eg1) {
        this.f = eg1;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.e.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
