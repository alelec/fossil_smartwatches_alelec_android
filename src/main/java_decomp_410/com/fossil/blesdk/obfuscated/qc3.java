package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qc3 extends zr2 implements pc3 {
    @DexIgnore
    public tr3<mc2> j;
    @DexIgnore
    public oc3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewHeartRateCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ qc3 a;

        @DexIgnore
        public b(qc3 qc3) {
            this.a = qc3;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            kd4.b(calendar, "calendar");
            oc3 a2 = qc3.a(this.a);
            Date time = calendar.getTime();
            kd4.a((Object) time, "calendar.time");
            a2.a(time);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewHeartRateCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ qc3 a;

        @DexIgnore
        public c(qc3 qc3) {
            this.a = qc3;
        }

        @DexIgnore
        public void a(int i, Calendar calendar) {
            kd4.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
                Date time = calendar.getTime();
                kd4.a((Object) time, "it.time");
                kd4.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ oc3 a(qc3 qc3) {
        oc3 oc3 = qc3.k;
        if (oc3 != null) {
            return oc3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HeartRateOverviewMonthFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onCreateView");
        mc2 mc2 = (mc2) qa.a(layoutInflater, R.layout.fragment_heartrate_overview_month, viewGroup, false, O0());
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = mc2.q;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        recyclerViewHeartRateCalendar.setEndDate(instance);
        mc2.q.setOnCalendarMonthChanged(new b(this));
        mc2.q.setOnCalendarItemClickListener(new c(this));
        this.j = new tr3<>(this, mc2);
        tr3<mc2> tr3 = this.j;
        if (tr3 != null) {
            mc2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onResume");
        oc3 oc3 = this.k;
        if (oc3 != null) {
            oc3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onStop");
        oc3 oc3 = this.k;
        if (oc3 != null) {
            oc3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View p(int i) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        View view = (View) this.l.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.l.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    @DexIgnore
    public void a(oc3 oc3) {
        kd4.b(oc3, "presenter");
        this.k = oc3;
    }

    @DexIgnore
    public void a(TreeMap<Long, Integer> treeMap) {
        kd4.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        tr3<mc2> tr3 = this.j;
        if (tr3 != null) {
            mc2 a2 = tr3.a();
            if (a2 != null) {
                RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = a2.q;
                if (recyclerViewHeartRateCalendar != null) {
                    recyclerViewHeartRateCalendar.setData(treeMap);
                }
            }
            ((RecyclerViewHeartRateCalendar) p(g62.calendarMonth)).setEnableButtonNextAndPrevMonth(true);
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        kd4.b(date, "selectDate");
        kd4.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        tr3<mc2> tr3 = this.j;
        if (tr3 != null) {
            mc2 a2 = tr3.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                kd4.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                kd4.a((Object) instance2, "startCalendar");
                instance2.setTime(rk2.n(date2));
                kd4.a((Object) instance3, "endCalendar");
                instance3.setTime(rk2.i(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
