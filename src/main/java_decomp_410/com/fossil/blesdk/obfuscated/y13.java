package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y13 implements Factory<DianaCustomizeEditPresenter> {
    @DexIgnore
    public static DianaCustomizeEditPresenter a(t13 t13, UserRepository userRepository, int i, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, wm2 wm2, en2 en2) {
        return new DianaCustomizeEditPresenter(t13, userRepository, i, setDianaPresetToWatchUseCase, wm2, en2);
    }
}
