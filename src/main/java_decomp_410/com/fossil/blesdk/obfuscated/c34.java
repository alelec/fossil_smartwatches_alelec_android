package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import com.zendesk.belvedere.BelvedereSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class c34 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<c34> CREATOR; // = new a();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ Intent f;
    @DexIgnore
    public /* final */ BelvedereSource g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<c34> {
        @DexIgnore
        public c34 createFromParcel(Parcel parcel) {
            return new c34(parcel, (a) null);
        }

        @DexIgnore
        public c34[] newArray(int i) {
            return new c34[i];
        }
    }

    @DexIgnore
    public /* synthetic */ c34(Parcel parcel, a aVar) {
        this(parcel);
    }

    @DexIgnore
    public BelvedereSource a() {
        return this.g;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.e);
        parcel.writeParcelable(this.f, i);
        parcel.writeSerializable(this.g);
    }

    @DexIgnore
    public c34(Intent intent, int i, BelvedereSource belvedereSource) {
        this.f = intent;
        this.e = i;
        this.g = belvedereSource;
    }

    @DexIgnore
    public void a(Activity activity) {
        activity.startActivityForResult(this.f, this.e);
    }

    @DexIgnore
    public void a(Fragment fragment) {
        fragment.startActivityForResult(this.f, this.e);
    }

    @DexIgnore
    public c34(Parcel parcel) {
        this.e = parcel.readInt();
        this.f = (Intent) parcel.readParcelable(c34.class.getClassLoader());
        this.g = (BelvedereSource) parcel.readSerializable();
    }
}
