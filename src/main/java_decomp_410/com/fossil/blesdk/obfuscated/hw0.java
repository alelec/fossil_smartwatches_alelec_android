package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.internal.clearcut.zzcb;
import java.lang.reflect.Field;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hw0 {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public Field C;
    @DexIgnore
    public Object D;
    @DexIgnore
    public Object E;
    @DexIgnore
    public Object F;
    @DexIgnore
    public /* final */ iw0 a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public Class<?> c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int[] n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q; // = Integer.MAX_VALUE;
    @DexIgnore
    public int r; // = Integer.MIN_VALUE;
    @DexIgnore
    public int s; // = 0;
    @DexIgnore
    public int t; // = 0;
    @DexIgnore
    public int u; // = 0;
    @DexIgnore
    public int v; // = 0;
    @DexIgnore
    public int w; // = 0;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore
    public hw0(Class<?> cls, String str, Object[] objArr) {
        this.c = cls;
        this.a = new iw0(str);
        this.b = objArr;
        this.d = this.a.b();
        this.e = this.a.b();
        int[] iArr = null;
        if (this.e == 0) {
            this.f = 0;
            this.g = 0;
            this.h = 0;
            this.i = 0;
            this.j = 0;
            this.l = 0;
            this.k = 0;
            this.m = 0;
            this.n = null;
            return;
        }
        this.f = this.a.b();
        this.g = this.a.b();
        this.h = this.a.b();
        this.i = this.a.b();
        this.l = this.a.b();
        this.k = this.a.b();
        this.j = this.a.b();
        this.m = this.a.b();
        int b2 = this.a.b();
        this.n = b2 != 0 ? new int[b2] : iArr;
        this.o = (this.f << 1) + this.g;
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00d0, code lost:
        if (e() != false) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x015e, code lost:
        if (r1 != false) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0165, code lost:
        if (e() != false) goto L_0x00d2;
     */
    @DexIgnore
    public final boolean a() {
        int i2;
        Object type;
        boolean z2 = false;
        if (!this.a.a()) {
            return false;
        }
        this.x = this.a.b();
        this.y = this.a.b();
        this.z = this.y & 255;
        int i3 = this.x;
        if (i3 < this.q) {
            this.q = i3;
        }
        int i4 = this.x;
        if (i4 > this.r) {
            this.r = i4;
        }
        if (this.z == zzcb.MAP.id()) {
            this.s++;
        } else if (this.z >= zzcb.DOUBLE_LIST.id() && this.z <= zzcb.GROUP_LIST.id()) {
            this.t++;
        }
        this.w++;
        if (lw0.a(this.q, this.x, this.w)) {
            this.v = this.x + 1;
            i2 = this.v - this.q;
        } else {
            i2 = this.u + 1;
        }
        this.u = i2;
        if ((this.y & 1024) != 0) {
            int[] iArr = this.n;
            int i5 = this.p;
            this.p = i5 + 1;
            iArr[i5] = this.x;
        }
        this.D = null;
        this.E = null;
        this.F = null;
        if (f()) {
            this.A = this.a.b();
            if (!(this.z == zzcb.MESSAGE.id() + 51 || this.z == zzcb.GROUP.id() + 51)) {
                if (this.z == zzcb.ENUM.id() + 51) {
                }
                return true;
            }
            type = b();
            this.D = type;
            return true;
        }
        this.C = a(this.c, (String) b());
        if (j()) {
            this.B = this.a.b();
        }
        if (this.z == zzcb.MESSAGE.id() || this.z == zzcb.GROUP.id()) {
            type = this.C.getType();
            this.D = type;
            return true;
        }
        if (!(this.z == zzcb.MESSAGE_LIST.id() || this.z == zzcb.GROUP_LIST.id())) {
            if (this.z != zzcb.ENUM.id() && this.z != zzcb.ENUM_LIST.id() && this.z != zzcb.ENUM_LIST_PACKED.id()) {
                if (this.z == zzcb.MAP.id()) {
                    this.F = b();
                    if ((this.y & 2048) != 0) {
                        z2 = true;
                    }
                }
                return true;
            }
        }
        type = b();
        this.D = type;
        return true;
        this.E = b();
        return true;
    }

    @DexIgnore
    public final Object b() {
        Object[] objArr = this.b;
        int i2 = this.o;
        this.o = i2 + 1;
        return objArr[i2];
    }

    @DexIgnore
    public final int c() {
        return this.x;
    }

    @DexIgnore
    public final int d() {
        return this.z;
    }

    @DexIgnore
    public final boolean e() {
        return (this.d & 1) == 1;
    }

    @DexIgnore
    public final boolean f() {
        return this.z > zzcb.MAP.id();
    }

    @DexIgnore
    public final Field g() {
        int i2 = this.A << 1;
        Object obj = this.b[i2];
        if (obj instanceof Field) {
            return (Field) obj;
        }
        Field a2 = a(this.c, (String) obj);
        this.b[i2] = a2;
        return a2;
    }

    @DexIgnore
    public final Field h() {
        int i2 = (this.A << 1) + 1;
        Object obj = this.b[i2];
        if (obj instanceof Field) {
            return (Field) obj;
        }
        Field a2 = a(this.c, (String) obj);
        this.b[i2] = a2;
        return a2;
    }

    @DexIgnore
    public final Field i() {
        return this.C;
    }

    @DexIgnore
    public final boolean j() {
        return e() && this.z <= zzcb.GROUP.id();
    }

    @DexIgnore
    public final Field k() {
        int i2 = (this.f << 1) + (this.B / 32);
        Object obj = this.b[i2];
        if (obj instanceof Field) {
            return (Field) obj;
        }
        Field a2 = a(this.c, (String) obj);
        this.b[i2] = a2;
        return a2;
    }

    @DexIgnore
    public final int l() {
        return this.B % 32;
    }

    @DexIgnore
    public final boolean m() {
        return (this.y & 256) != 0;
    }

    @DexIgnore
    public final boolean n() {
        return (this.y & RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) != 0;
    }

    @DexIgnore
    public final Object o() {
        return this.D;
    }

    @DexIgnore
    public final Object p() {
        return this.E;
    }

    @DexIgnore
    public final Object q() {
        return this.F;
    }
}
