package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t41 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<t41> CREATOR; // = new u41();
    @DexIgnore
    public static /* final */ List<jj0> h; // = Collections.emptyList();
    @DexIgnore
    public static /* final */ kd1 i; // = new kd1();
    @DexIgnore
    public kd1 e;
    @DexIgnore
    public List<jj0> f;
    @DexIgnore
    public String g;

    @DexIgnore
    public t41(kd1 kd1, List<jj0> list, String str) {
        this.e = kd1;
        this.f = list;
        this.g = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof t41)) {
            return false;
        }
        t41 t41 = (t41) obj;
        return zj0.a(this.e, t41.e) && zj0.a(this.f, t41.f) && zj0.a(this.g, t41.g);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) this.e, i2, false);
        kk0.b(parcel, 2, this.f, false);
        kk0.a(parcel, 3, this.g, false);
        kk0.a(parcel, a);
    }
}
