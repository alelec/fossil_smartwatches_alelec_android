package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class a5 extends ConstraintWidget {
    @DexIgnore
    public float k0; // = -1.0f;
    @DexIgnore
    public int l0; // = -1;
    @DexIgnore
    public int m0; // = -1;
    @DexIgnore
    public ConstraintAnchor n0; // = this.t;
    @DexIgnore
    public int o0;
    @DexIgnore
    public boolean p0;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[ConstraintAnchor.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[ConstraintAnchor.Type.LEFT.ordinal()] = 1;
            a[ConstraintAnchor.Type.RIGHT.ordinal()] = 2;
            a[ConstraintAnchor.Type.TOP.ordinal()] = 3;
            a[ConstraintAnchor.Type.BOTTOM.ordinal()] = 4;
            a[ConstraintAnchor.Type.BASELINE.ordinal()] = 5;
            a[ConstraintAnchor.Type.CENTER.ordinal()] = 6;
            a[ConstraintAnchor.Type.CENTER_X.ordinal()] = 7;
            a[ConstraintAnchor.Type.CENTER_Y.ordinal()] = 8;
            a[ConstraintAnchor.Type.NONE.ordinal()] = 9;
        }
        */
    }

    @DexIgnore
    public a5() {
        this.o0 = 0;
        this.p0 = false;
        new d5();
        this.B.clear();
        this.B.add(this.n0);
        int length = this.A.length;
        for (int i = 0; i < length; i++) {
            this.A[i] = this.n0;
        }
    }

    @DexIgnore
    public int K() {
        return this.o0;
    }

    @DexIgnore
    public ConstraintAnchor a(ConstraintAnchor.Type type) {
        switch (a.a[type.ordinal()]) {
            case 1:
            case 2:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 3:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new AssertionError(type.name());
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public ArrayList<ConstraintAnchor> c() {
        return this.B;
    }

    @DexIgnore
    public void e(float f) {
        if (f > -1.0f) {
            this.k0 = f;
            this.l0 = -1;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void u(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = i;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void v(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = -1;
            this.m0 = i;
        }
    }

    @DexIgnore
    public void w(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            this.B.clear();
            if (this.o0 == 1) {
                this.n0 = this.s;
            } else {
                this.n0 = this.t;
            }
            this.B.add(this.n0);
            int length = this.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.A[i2] = this.n0;
            }
        }
    }

    @DexIgnore
    public void c(q4 q4Var) {
        if (l() != null) {
            int b = q4Var.b((Object) this.n0);
            if (this.o0 == 1) {
                s(b);
                t(0);
                h(l().j());
                p(0);
                return;
            }
            s(0);
            t(b);
            p(l().t());
            h(0);
        }
    }

    @DexIgnore
    public void a(int i) {
        ConstraintWidget l = l();
        if (l != null) {
            if (K() == 1) {
                this.t.d().a(1, l.t.d(), 0);
                this.v.d().a(1, l.t.d(), 0);
                if (this.l0 != -1) {
                    this.s.d().a(1, l.s.d(), this.l0);
                    this.u.d().a(1, l.s.d(), this.l0);
                } else if (this.m0 != -1) {
                    this.s.d().a(1, l.u.d(), -this.m0);
                    this.u.d().a(1, l.u.d(), -this.m0);
                } else if (this.k0 != -1.0f && l.k() == ConstraintWidget.DimensionBehaviour.FIXED) {
                    int i2 = (int) (((float) l.E) * this.k0);
                    this.s.d().a(1, l.s.d(), i2);
                    this.u.d().a(1, l.s.d(), i2);
                }
            } else {
                this.s.d().a(1, l.s.d(), 0);
                this.u.d().a(1, l.s.d(), 0);
                if (this.l0 != -1) {
                    this.t.d().a(1, l.t.d(), this.l0);
                    this.v.d().a(1, l.t.d(), this.l0);
                } else if (this.m0 != -1) {
                    this.t.d().a(1, l.v.d(), -this.m0);
                    this.v.d().a(1, l.v.d(), -this.m0);
                } else if (this.k0 != -1.0f && l.r() == ConstraintWidget.DimensionBehaviour.FIXED) {
                    int i3 = (int) (((float) l.F) * this.k0);
                    this.t.d().a(1, l.t.d(), i3);
                    this.v.d().a(1, l.t.d(), i3);
                }
            }
        }
    }

    @DexIgnore
    public void a(q4 q4Var) {
        y4 y4Var = (y4) l();
        if (y4Var != null) {
            ConstraintAnchor a2 = y4Var.a(ConstraintAnchor.Type.LEFT);
            ConstraintAnchor a3 = y4Var.a(ConstraintAnchor.Type.RIGHT);
            ConstraintWidget constraintWidget = this.D;
            boolean z = constraintWidget != null && constraintWidget.C[0] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            if (this.o0 == 0) {
                a2 = y4Var.a(ConstraintAnchor.Type.TOP);
                a3 = y4Var.a(ConstraintAnchor.Type.BOTTOM);
                ConstraintWidget constraintWidget2 = this.D;
                z = constraintWidget2 != null && constraintWidget2.C[1] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            }
            if (this.l0 != -1) {
                SolverVariable a4 = q4Var.a((Object) this.n0);
                q4Var.a(a4, q4Var.a((Object) a2), this.l0, 6);
                if (z) {
                    q4Var.b(q4Var.a((Object) a3), a4, 0, 5);
                }
            } else if (this.m0 != -1) {
                SolverVariable a5 = q4Var.a((Object) this.n0);
                SolverVariable a6 = q4Var.a((Object) a3);
                q4Var.a(a5, a6, -this.m0, 6);
                if (z) {
                    q4Var.b(a5, q4Var.a((Object) a2), 0, 5);
                    q4Var.b(a6, a5, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                q4Var.a(q4.a(q4Var, q4Var.a((Object) this.n0), q4Var.a((Object) a2), q4Var.a((Object) a3), this.k0, this.p0));
            }
        }
    }
}
