package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xw {
    @DexIgnore
    public static xw b() {
        return new b();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(boolean z);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends xw {
        @DexIgnore
        public volatile boolean a;

        @DexIgnore
        public b() {
            super();
        }

        @DexIgnore
        public void a() {
            if (this.a) {
                throw new IllegalStateException("Already released");
            }
        }

        @DexIgnore
        public void a(boolean z) {
            this.a = z;
        }
    }

    @DexIgnore
    public xw() {
    }
}
