package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ay3 extends qx3<ImageView> {
    @DexIgnore
    public ux3 m;

    @DexIgnore
    public ay3(Picasso picasso, ImageView imageView, gy3 gy3, int i, int i2, int i3, Drawable drawable, String str, Object obj, ux3 ux3, boolean z) {
        super(picasso, imageView, gy3, i, i2, i3, drawable, str, obj, z);
        this.m = ux3;
    }

    @DexIgnore
    public void a(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
        if (bitmap != null) {
            ImageView imageView = (ImageView) this.c.get();
            if (imageView != null) {
                Picasso picasso = this.a;
                Bitmap bitmap2 = bitmap;
                Picasso.LoadedFrom loadedFrom2 = loadedFrom;
                ey3.a(imageView, picasso.e, bitmap2, loadedFrom2, this.d, picasso.m);
                ux3 ux3 = this.m;
                if (ux3 != null) {
                    ux3.onSuccess();
                    return;
                }
                return;
            }
            return;
        }
        throw new AssertionError(String.format("Attempted to complete action with no result!\n%s", new Object[]{this}));
    }

    @DexIgnore
    public void b() {
        ImageView imageView = (ImageView) this.c.get();
        if (imageView != null) {
            int i = this.g;
            if (i != 0) {
                imageView.setImageResource(i);
            } else {
                Drawable drawable = this.h;
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                }
            }
            ux3 ux3 = this.m;
            if (ux3 != null) {
                ux3.onError();
            }
        }
    }

    @DexIgnore
    public void a() {
        super.a();
        if (this.m != null) {
            this.m = null;
        }
    }
}
