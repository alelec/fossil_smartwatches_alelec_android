package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ep */
public final class C1740ep {
    @DexIgnore
    /* renamed from: a */
    public static boolean m6535a(int i, int i2) {
        return i != Integer.MIN_VALUE && i2 != Integer.MIN_VALUE && i <= 512 && i2 <= 384;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6536a(android.net.Uri uri) {
        return m6537b(uri) && !m6539d(uri);
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m6537b(android.net.Uri uri) {
        return uri != null && "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    @DexIgnore
    /* renamed from: c */
    public static boolean m6538c(android.net.Uri uri) {
        return m6537b(uri) && m6539d(uri);
    }

    @DexIgnore
    /* renamed from: d */
    public static boolean m6539d(android.net.Uri uri) {
        return uri.getPathSegments().contains("video");
    }
}
