package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.wd4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ee4 extends de4 {
    @DexIgnore
    public static final int a(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    @DexIgnore
    public static final long a(long j, long j2) {
        return j < j2 ? j2 : j;
    }

    @DexIgnore
    public static final wd4 a(wd4 wd4, int i) {
        kd4.b(wd4, "$this$step");
        de4.a(i > 0, Integer.valueOf(i));
        wd4.a aVar = wd4.h;
        int a = wd4.a();
        int b = wd4.b();
        if (wd4.c() <= 0) {
            i = -i;
        }
        return aVar.a(a, b, i);
    }

    @DexIgnore
    public static final int b(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    @DexIgnore
    public static final long b(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    @DexIgnore
    public static final wd4 c(int i, int i2) {
        return wd4.h.a(i, i2, -1);
    }

    @DexIgnore
    public static final yd4 d(int i, int i2) {
        if (i2 <= Integer.MIN_VALUE) {
            return yd4.j.a();
        }
        return new yd4(i, i2 - 1);
    }

    @DexIgnore
    public static final be4 a(long j, int i) {
        return new be4(j, ((long) i) - 1);
    }

    @DexIgnore
    public static final int a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
        } else if (i < i2) {
            return i2;
        } else {
            return i > i3 ? i3 : i;
        }
    }
}
