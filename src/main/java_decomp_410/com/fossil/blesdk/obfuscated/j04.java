package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.Thread;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j04 {
    @DexIgnore
    public static y14 a;
    @DexIgnore
    public static volatile Map<String, Properties> b; // = new ConcurrentHashMap();
    @DexIgnore
    public static volatile Map<Integer, Integer> c; // = new ConcurrentHashMap(10);
    @DexIgnore
    public static volatile long d; // = 0;
    @DexIgnore
    public static volatile long e; // = 0;
    @DexIgnore
    public static volatile long f; // = 0;
    @DexIgnore
    public static String g; // = "";
    @DexIgnore
    public static volatile int h; // = 0;
    @DexIgnore
    public static volatile String i; // = "";
    @DexIgnore
    public static volatile String j; // = "";
    @DexIgnore
    public static Map<String, Long> k; // = new ConcurrentHashMap();
    @DexIgnore
    public static Map<String, Long> l; // = new ConcurrentHashMap();
    @DexIgnore
    public static t14 m; // = e24.b();
    @DexIgnore
    public static Thread.UncaughtExceptionHandler n; // = null;
    @DexIgnore
    public static volatile boolean o; // = true;
    @DexIgnore
    public static volatile int p; // = 0;
    @DexIgnore
    public static volatile long q; // = 0;
    @DexIgnore
    public static Context r; // = null;
    @DexIgnore
    public static volatile long s; // = 0;

    /*
    static {
        new ConcurrentHashMap();
    }
    */

    @DexIgnore
    public static int a(Context context, boolean z, k04 k04) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = z && currentTimeMillis - e >= ((long) h04.m());
        e = currentTimeMillis;
        if (f == 0) {
            f = e24.c();
        }
        if (currentTimeMillis >= f) {
            f = e24.c();
            if (g14.b(context).a(context).d() != 1) {
                g14.b(context).a(context).a(1);
            }
            h04.b(0);
            p = 0;
            g = e24.a(0);
            z2 = true;
        }
        String str = g;
        if (e24.a(k04)) {
            str = k04.a() + g;
        }
        if (!l.containsKey(str)) {
            z2 = true;
        }
        if (z2) {
            if (e24.a(k04)) {
                a(context, k04);
            } else if (h04.c() < h04.f()) {
                e24.A(context);
                a(context, (k04) null);
            } else {
                m.c("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
            l.put(str, 1L);
        }
        if (o) {
            h(context);
            o = false;
        }
        return h;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        return;
     */
    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (j04.class) {
            if (context != null) {
                if (a == null) {
                    if (b(context)) {
                        Context applicationContext = context.getApplicationContext();
                        r = applicationContext;
                        a = new y14();
                        g = e24.a(0);
                        d = System.currentTimeMillis() + h04.x;
                        a.a(new t24(applicationContext));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(Context context, int i2) {
        t14 t14;
        String str;
        if (h04.s()) {
            if (h04.q()) {
                t14 t142 = m;
                t142.e("commitEvents, maxNumber=" + i2);
            }
            Context g2 = g(context);
            if (g2 == null) {
                t14 = m;
                str = "The Context of StatService.commitEvents() can not be null!";
            } else if (i2 < -1 || i2 == 0) {
                t14 = m;
                str = "The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.";
            } else if (t04.a(r).f() && c(g2) != null) {
                a.a(new u04(g2, i2));
                return;
            } else {
                return;
            }
            t14.d(str);
        }
    }

    @DexIgnore
    public static void a(Context context, k04 k04) {
        if (c(context) != null) {
            if (h04.q()) {
                m.a((Object) "start new session.");
            }
            if (k04 == null || h == 0) {
                h = e24.a();
            }
            h04.a(0);
            h04.b();
            new c14(new s04(context, h, b(), k04)).a();
        }
    }

    @DexIgnore
    public static void a(Context context, String str, k04 k04) {
        if (h04.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new y24(str2, g2, k04));
            }
        }
    }

    @DexIgnore
    public static void a(Context context, String str, Properties properties, k04 k04) {
        t14 t14;
        String str2;
        if (h04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                t14 = m;
                str2 = "The Context of StatService.trackCustomEvent() can not be null!";
            } else if (a(str)) {
                t14 = m;
                str2 = "The event_id of StatService.trackCustomEvent() can not be null or empty.";
            } else {
                m04 m04 = new m04(str, (String[]) null, properties);
                if (c(g2) != null) {
                    a.a(new x24(g2, k04, m04));
                    return;
                }
                return;
            }
            t14.d(str2);
        }
    }

    @DexIgnore
    public static void a(Context context, Throwable th) {
        if (h04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.reportSdkSelfException() can not be null!");
            } else if (c(g2) != null) {
                a.a(new v24(g2, th));
            }
        }
    }

    @DexIgnore
    public static boolean a() {
        if (p < 2) {
            return false;
        }
        q = System.currentTimeMillis();
        return true;
    }

    @DexIgnore
    public static boolean a(Context context, String str, String str2, k04 k04) {
        try {
            if (!h04.s()) {
                m.d("MTA StatService is disable.");
                return false;
            }
            if (h04.q()) {
                m.a((Object) "MTA SDK version, current: " + "2.0.3" + " ,required: " + str2);
            }
            if (context != null) {
                if (str2 != null) {
                    if (e24.b("2.0.3") < e24.b(str2)) {
                        m.d(("MTA SDK version conflicted, current: " + "2.0.3" + ",required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                        h04.b(false);
                        return false;
                    }
                    String d2 = h04.d(context);
                    if (d2 == null || d2.length() == 0) {
                        h04.b(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    }
                    if (str != null) {
                        h04.b(context, str);
                    }
                    if (c(context) == null) {
                        return true;
                    }
                    a.a(new z04(context, k04));
                    return true;
                }
            }
            m.d("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
            h04.b(false);
            return false;
        } catch (Throwable th) {
            m.a(th);
            return false;
        }
    }

    @DexIgnore
    public static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    @DexIgnore
    public static JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (h04.c.d != 0) {
                jSONObject2.put("v", h04.c.d);
            }
            jSONObject.put(Integer.toString(h04.c.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (h04.b.d != 0) {
                jSONObject3.put("v", h04.b.d);
            }
            jSONObject.put(Integer.toString(h04.b.a), jSONObject3);
        } catch (JSONException e2) {
            m.a((Throwable) e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public static void b(Context context, k04 k04) {
        if (h04.s() && c(context) != null) {
            a.a(new u24(context, k04));
        }
    }

    @DexIgnore
    public static void b(Context context, String str, k04 k04) {
        if (h04.s()) {
            Context g2 = g(context);
            if (g2 == null || str == null || str.length() == 0) {
                m.d("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            String str2 = new String(str);
            if (c(g2) != null) {
                a.a(new x04(g2, str2, k04));
            }
        }
    }

    @DexIgnore
    public static boolean b(Context context) {
        boolean z;
        long a2 = i24.a(context, h04.n, 0);
        long b2 = e24.b("2.0.3");
        boolean z2 = false;
        if (b2 <= a2) {
            t14 t14 = m;
            t14.d("MTA is disable for current version:" + b2 + ",wakeup version:" + a2);
            z = false;
        } else {
            z = true;
        }
        long a3 = i24.a(context, h04.o, 0);
        if (a3 > System.currentTimeMillis()) {
            t14 t142 = m;
            t142.d("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + a3);
        } else {
            z2 = z;
        }
        h04.b(z2);
        return z2;
    }

    @DexIgnore
    public static y14 c(Context context) {
        if (a == null) {
            synchronized (j04.class) {
                if (a == null) {
                    try {
                        a(context);
                    } catch (Throwable th) {
                        m.b(th);
                        h04.b(false);
                    }
                }
            }
        }
        return a;
    }

    @DexIgnore
    public static void c() {
        p = 0;
        q = 0;
    }

    @DexIgnore
    public static void c(Context context, k04 k04) {
        if (h04.s() && c(context) != null) {
            a.a(new y04(context, k04));
        }
    }

    @DexIgnore
    public static Properties d(String str) {
        return b.get(str);
    }

    @DexIgnore
    public static void d() {
        p++;
        q = System.currentTimeMillis();
        f(r);
    }

    @DexIgnore
    public static void d(Context context) {
        if (h04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                q24.b(g2).a((o04) new p04(g2), (p24) new w24());
            } catch (Throwable th) {
                m.a(th);
            }
        }
    }

    @DexIgnore
    public static void e(Context context) {
        s = System.currentTimeMillis() + ((long) (h04.l() * 60000));
        i24.b(context, "last_period_ts", s);
        a(context, -1);
    }

    @DexIgnore
    public static void f(Context context) {
        if (h04.s() && h04.J > 0) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else {
                g14.b(g2).b();
            }
        }
    }

    @DexIgnore
    public static Context g(Context context) {
        return context != null ? context : r;
    }

    @DexIgnore
    public static void h(Context context) {
        if (h04.s()) {
            Context g2 = g(context);
            if (g2 == null) {
                m.d("The Context of StatService.testSpeed() can not be null!");
            } else if (c(g2) != null) {
                a.a(new v04(g2));
            }
        }
    }
}
