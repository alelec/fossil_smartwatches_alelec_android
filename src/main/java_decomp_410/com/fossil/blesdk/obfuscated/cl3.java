package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cl3 implements MembersInjector<OnboardingHeightWeightActivity> {
    @DexIgnore
    public static void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity, OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
        onboardingHeightWeightActivity.B = onboardingHeightWeightPresenter;
    }
}
