package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cx0 extends ax0<bx0, bx0> {
    @DexIgnore
    public static void a(Object obj, bx0 bx0) {
        ((ru0) obj).zzjp = bx0;
    }

    @DexIgnore
    public final /* synthetic */ Object a() {
        return bx0.e();
    }

    @DexIgnore
    public final void a(Object obj) {
        ((ru0) obj).zzjp.c();
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, int i, long j) {
        ((bx0) obj).a(i << 3, (Object) Long.valueOf(j));
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, ox0 ox0) throws IOException {
        ((bx0) obj).b(ox0);
    }

    @DexIgnore
    public final /* synthetic */ void a(Object obj, Object obj2) {
        a(obj, (bx0) obj2);
    }

    @DexIgnore
    public final /* synthetic */ int b(Object obj) {
        return ((bx0) obj).a();
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, ox0 ox0) throws IOException {
        ((bx0) obj).a(ox0);
    }

    @DexIgnore
    public final /* synthetic */ void b(Object obj, Object obj2) {
        a(obj, (bx0) obj2);
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj) {
        return ((ru0) obj).zzjp;
    }

    @DexIgnore
    public final /* synthetic */ Object c(Object obj, Object obj2) {
        bx0 bx0 = (bx0) obj;
        bx0 bx02 = (bx0) obj2;
        return bx02.equals(bx0.d()) ? bx0 : bx0.a(bx0, bx02);
    }

    @DexIgnore
    public final /* synthetic */ int d(Object obj) {
        return ((bx0) obj).b();
    }
}
