package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bf2 extends af2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public long F;

    /*
    static {
        H.put(R.id.customToolbar, 1);
        H.put(R.id.iv_back, 2);
        H.put(R.id.avatar, 3);
        H.put(R.id.iv_user_avatar_edit, 4);
        H.put(R.id.input_first_name, 5);
        H.put(R.id.et_first_name, 6);
        H.put(R.id.input_last_name, 7);
        H.put(R.id.et_last_name, 8);
        H.put(R.id.tv_email, 9);
        H.put(R.id.tv_dob, 10);
        H.put(R.id.ftv_gender, 11);
        H.put(R.id.fb_male, 12);
        H.put(R.id.fb_female, 13);
        H.put(R.id.fb_other, 14);
        H.put(R.id.ftv_height, 15);
        H.put(R.id.ftv_height_unit, 16);
        H.put(R.id.rvp_height, 17);
        H.put(R.id.ftv_weight, 18);
        H.put(R.id.ftv_weight_unit, 19);
        H.put(R.id.rvp_weight, 20);
        H.put(R.id.save, 21);
    }
    */

    @DexIgnore
    public bf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 22, G, H));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public bf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[3], objArr[1], objArr[6], objArr[8], objArr[13], objArr[12], objArr[14], objArr[11], objArr[15], objArr[16], objArr[18], objArr[19], objArr[5], objArr[7], objArr[2], objArr[4], objArr[17], objArr[20], objArr[21], objArr[10], objArr[9]);
        this.F = -1;
        this.E = objArr[0];
        this.E.setTag((Object) null);
        a(view);
        f();
    }
}
