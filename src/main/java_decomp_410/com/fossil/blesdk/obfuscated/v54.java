package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FileLruCache;
import com.fossil.blesdk.device.data.file.FileType;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v54 implements Closeable {
    @DexIgnore
    public static /* final */ Logger k; // = Logger.getLogger(v54.class.getName());
    @DexIgnore
    public /* final */ RandomAccessFile e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public b h;
    @DexIgnore
    public b i;
    @DexIgnore
    public /* final */ byte[] j; // = new byte[16];

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements d {
        @DexIgnore
        public boolean a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder b;

        @DexIgnore
        public a(v54 v54, StringBuilder sb) {
            this.b = sb;
        }

        @DexIgnore
        public void a(InputStream inputStream, int i) throws IOException {
            if (this.a) {
                this.a = false;
            } else {
                this.b.append(", ");
            }
            this.b.append(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static /* final */ b c; // = new b(0, 0);
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public String toString() {
            return b.class.getSimpleName() + "[position = " + this.a + ", length = " + this.b + "]";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends InputStream {
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public /* synthetic */ c(v54 v54, b bVar, a aVar) {
            this(bVar);
        }

        @DexIgnore
        public int read(byte[] bArr, int i, int i2) throws IOException {
            Object unused = v54.b(bArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
            if ((i | i2) < 0 || i2 > bArr.length - i) {
                throw new ArrayIndexOutOfBoundsException();
            }
            int i3 = this.f;
            if (i3 <= 0) {
                return -1;
            }
            if (i2 > i3) {
                i2 = i3;
            }
            v54.this.a(this.e, bArr, i, i2);
            this.e = v54.this.e(this.e + i2);
            this.f -= i2;
            return i2;
        }

        @DexIgnore
        public c(b bVar) {
            this.e = v54.this.e(bVar.a + 4);
            this.f = bVar.b;
        }

        @DexIgnore
        public int read() throws IOException {
            if (this.f == 0) {
                return -1;
            }
            v54.this.e.seek((long) this.e);
            int read = v54.this.e.read();
            this.e = v54.this.e(this.e + 1);
            this.f--;
            return read;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(InputStream inputStream, int i) throws IOException;
    }

    @DexIgnore
    public v54(File file) throws IOException {
        if (!file.exists()) {
            a(file);
        }
        this.e = b(file);
        A();
    }

    @DexIgnore
    public static void b(byte[] bArr, int i2, int i3) {
        bArr[i2] = (byte) (i3 >> 24);
        bArr[i2 + 1] = (byte) (i3 >> 16);
        bArr[i2 + 2] = (byte) (i3 >> 8);
        bArr[i2 + 3] = (byte) i3;
    }

    @DexIgnore
    public final void A() throws IOException {
        this.e.seek(0);
        this.e.readFully(this.j);
        this.f = a(this.j, 0);
        if (((long) this.f) <= this.e.length()) {
            this.g = a(this.j, 4);
            int a2 = a(this.j, 8);
            int a3 = a(this.j, 12);
            this.h = c(a2);
            this.i = c(a3);
            return;
        }
        throw new IOException("File is truncated. Expected length: " + this.f + ", Actual length: " + this.e.length());
    }

    @DexIgnore
    public final int B() {
        return this.f - D();
    }

    @DexIgnore
    public synchronized void C() throws IOException {
        if (z()) {
            throw new NoSuchElementException();
        } else if (this.g == 1) {
            y();
        } else {
            int e2 = e(this.h.a + 4 + this.h.b);
            a(e2, this.j, 0, 4);
            int a2 = a(this.j, 0);
            a(this.f, this.g - 1, e2, this.i.a);
            this.g--;
            this.h = new b(e2, a2);
        }
    }

    @DexIgnore
    public int D() {
        if (this.g == 0) {
            return 16;
        }
        b bVar = this.i;
        int i2 = bVar.a;
        int i3 = this.h.a;
        if (i2 >= i3) {
            return (i2 - i3) + 4 + bVar.b + 16;
        }
        return (((i2 + 4) + bVar.b) + this.f) - i3;
    }

    @DexIgnore
    public final b c(int i2) throws IOException {
        if (i2 == 0) {
            return b.c;
        }
        this.e.seek((long) i2);
        return new b(i2, this.e.readInt());
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public final void d(int i2) throws IOException {
        this.e.setLength((long) i2);
        this.e.getChannel().force(true);
    }

    @DexIgnore
    public final int e(int i2) {
        int i3 = this.f;
        return i2 < i3 ? i2 : (i2 + 16) - i3;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(v54.class.getSimpleName());
        sb.append('[');
        sb.append("fileLength=");
        sb.append(this.f);
        sb.append(", size=");
        sb.append(this.g);
        sb.append(", first=");
        sb.append(this.h);
        sb.append(", last=");
        sb.append(this.i);
        sb.append(", element lengths=[");
        try {
            a((d) new a(this, sb));
        } catch (IOException e2) {
            k.log(Level.WARNING, "read error", e2);
        }
        sb.append("]]");
        return sb.toString();
    }

    @DexIgnore
    public synchronized void y() throws IOException {
        a(4096, 0, 0, 0);
        this.g = 0;
        this.h = b.c;
        this.i = b.c;
        if (this.f > 4096) {
            d(4096);
        }
        this.f = 4096;
    }

    @DexIgnore
    public synchronized boolean z() {
        return this.g == 0;
    }

    @DexIgnore
    public static void a(byte[] bArr, int... iArr) {
        int i2 = 0;
        for (int b2 : iArr) {
            b(bArr, i2, b2);
            i2 += 4;
        }
    }

    @DexIgnore
    public static RandomAccessFile b(File file) throws FileNotFoundException {
        return new RandomAccessFile(file, "rwd");
    }

    @DexIgnore
    public final void b(int i2, byte[] bArr, int i3, int i4) throws IOException {
        int e2 = e(i2);
        int i5 = e2 + i4;
        int i6 = this.f;
        if (i5 <= i6) {
            this.e.seek((long) e2);
            this.e.write(bArr, i3, i4);
            return;
        }
        int i7 = i6 - e2;
        this.e.seek((long) e2);
        this.e.write(bArr, i3, i7);
        this.e.seek(16);
        this.e.write(bArr, i3 + i7, i4 - i7);
    }

    @DexIgnore
    public static int a(byte[] bArr, int i2) {
        return ((bArr[i2] & FileType.MASKED_INDEX) << 24) + ((bArr[i2 + 1] & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY) + ((bArr[i2 + 2] & FileType.MASKED_INDEX) << 8) + (bArr[i2 + 3] & FileType.MASKED_INDEX);
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) throws IOException {
        a(this.j, i2, i3, i4, i5);
        this.e.seek(0);
        this.e.write(this.j);
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void a(File file) throws IOException {
        File file2 = new File(file.getPath() + ".tmp");
        RandomAccessFile b2 = b(file2);
        try {
            b2.setLength(4096);
            b2.seek(0);
            byte[] bArr = new byte[16];
            a(bArr, 4096, 0, 0, 0);
            b2.write(bArr);
            b2.close();
            if (!file2.renameTo(file)) {
                throw new IOException("Rename failed!");
            }
        } catch (Throwable th) {
            b2.close();
            throw th;
        }
    }

    @DexIgnore
    public final void b(int i2) throws IOException {
        int i3 = i2 + 4;
        int B = B();
        if (B < i3) {
            int i4 = this.f;
            do {
                B += i4;
                i4 <<= 1;
            } while (B < i3);
            d(i4);
            b bVar = this.i;
            int e2 = e(bVar.a + 4 + bVar.b);
            if (e2 < this.h.a) {
                FileChannel channel = this.e.getChannel();
                channel.position((long) this.f);
                long j2 = (long) (e2 - 4);
                if (channel.transferTo(16, j2, channel) != j2) {
                    throw new AssertionError("Copied insufficient number of bytes!");
                }
            }
            int i5 = this.i.a;
            int i6 = this.h.a;
            if (i5 < i6) {
                int i7 = (this.f + i5) - 16;
                a(i4, this.g, i6, i7);
                this.i = new b(i7, this.i.b);
            } else {
                a(i4, this.g, i6, i5);
            }
            this.f = i4;
        }
    }

    @DexIgnore
    public final void a(int i2, byte[] bArr, int i3, int i4) throws IOException {
        int e2 = e(i2);
        int i5 = e2 + i4;
        int i6 = this.f;
        if (i5 <= i6) {
            this.e.seek((long) e2);
            this.e.readFully(bArr, i3, i4);
            return;
        }
        int i7 = i6 - e2;
        this.e.seek((long) e2);
        this.e.readFully(bArr, i3, i7);
        this.e.seek(16);
        this.e.readFully(bArr, i3 + i7, i4 - i7);
    }

    @DexIgnore
    public static <T> T b(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    @DexIgnore
    public void a(byte[] bArr) throws IOException {
        a(bArr, 0, bArr.length);
    }

    @DexIgnore
    public synchronized void a(byte[] bArr, int i2, int i3) throws IOException {
        int i4;
        b(bArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        if ((i2 | i3) < 0 || i3 > bArr.length - i2) {
            throw new IndexOutOfBoundsException();
        }
        b(i3);
        boolean z = z();
        if (z) {
            i4 = 16;
        } else {
            i4 = e(this.i.a + 4 + this.i.b);
        }
        b bVar = new b(i4, i3);
        b(this.j, 0, i3);
        b(bVar.a, this.j, 0, 4);
        b(bVar.a + 4, bArr, i2, i3);
        a(this.f, this.g + 1, z ? bVar.a : this.h.a, bVar.a);
        this.i = bVar;
        this.g++;
        if (z) {
            this.h = this.i;
        }
    }

    @DexIgnore
    public synchronized void a(d dVar) throws IOException {
        int i2 = this.h.a;
        for (int i3 = 0; i3 < this.g; i3++) {
            b c2 = c(i2);
            dVar.a(new c(this, c2, (a) null), c2.b);
            i2 = e(c2.a + 4 + c2.b);
        }
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        return (D() + 4) + i2 <= i3;
    }
}
