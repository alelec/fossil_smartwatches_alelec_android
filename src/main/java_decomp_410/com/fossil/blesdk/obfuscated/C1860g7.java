package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g7 */
public class C1860g7 extends com.fossil.blesdk.obfuscated.C1772f7 {

    @DexIgnore
    /* renamed from: l */
    public static java.lang.reflect.Method f5393l;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.g7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.g7$a */
    public static class C1861a extends com.fossil.blesdk.obfuscated.C1772f7.C1773a {
        @DexIgnore
        public C1861a(com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar, android.content.res.Resources resources) {
            super(aVar, resources);
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            return new com.fossil.blesdk.obfuscated.C1860g7(this, resources);
        }
    }

    @DexIgnore
    public C1860g7(android.graphics.drawable.Drawable drawable) {
        super(drawable);
        mo11151d();
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo10746b() {
        if (android.os.Build.VERSION.SDK_INT != 21) {
            return false;
        }
        android.graphics.drawable.Drawable drawable = this.f5082j;
        if ((drawable instanceof android.graphics.drawable.GradientDrawable) || (drawable instanceof android.graphics.drawable.DrawableContainer) || (drawable instanceof android.graphics.drawable.InsetDrawable) || (drawable instanceof android.graphics.drawable.RippleDrawable)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1772f7.C1773a mo10747c() {
        return new com.fossil.blesdk.obfuscated.C1860g7.C1861a(this.f5080h, (android.content.res.Resources) null);
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo11151d() {
        if (f5393l == null) {
            try {
                f5393l = android.graphics.drawable.Drawable.class.getDeclaredMethod("isProjected", new java.lang.Class[0]);
            } catch (java.lang.Exception e) {
                android.util.Log.w("WrappedDrawableApi21", "Failed to retrieve Drawable#isProjected() method", e);
            }
        }
    }

    @DexIgnore
    public android.graphics.Rect getDirtyBounds() {
        return this.f5082j.getDirtyBounds();
    }

    @DexIgnore
    public void getOutline(android.graphics.Outline outline) {
        this.f5082j.getOutline(outline);
    }

    @DexIgnore
    public boolean isProjected() {
        android.graphics.drawable.Drawable drawable = this.f5082j;
        if (drawable != null) {
            java.lang.reflect.Method method = f5393l;
            if (method != null) {
                try {
                    return ((java.lang.Boolean) method.invoke(drawable, new java.lang.Object[0])).booleanValue();
                } catch (java.lang.Exception e) {
                    android.util.Log.w("WrappedDrawableApi21", "Error calling Drawable#isProjected() method", e);
                }
            }
        }
        return false;
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        this.f5082j.setHotspot(f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.f5082j.setHotspotBounds(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        if (!super.setState(iArr)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void setTint(int i) {
        if (mo10746b()) {
            super.setTint(i);
        } else {
            this.f5082j.setTint(i);
        }
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        if (mo10746b()) {
            super.setTintList(colorStateList);
        } else {
            this.f5082j.setTintList(colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        if (mo10746b()) {
            super.setTintMode(mode);
        } else {
            this.f5082j.setTintMode(mode);
        }
    }

    @DexIgnore
    public C1860g7(com.fossil.blesdk.obfuscated.C1772f7.C1773a aVar, android.content.res.Resources resources) {
        super(aVar, resources);
        mo11151d();
    }
}
