package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ma1 {
    @DexIgnore
    int a();

    @DexIgnore
    @Deprecated
    <T> T a(na1<T> na1, i81 i81) throws IOException;

    @DexIgnore
    void a(List<Boolean> list) throws IOException;

    @DexIgnore
    @Deprecated
    <T> void a(List<T> list, na1<T> na1, i81 i81) throws IOException;

    @DexIgnore
    int b() throws IOException;

    @DexIgnore
    <T> T b(na1<T> na1, i81 i81) throws IOException;

    @DexIgnore
    void b(List<Integer> list) throws IOException;

    @DexIgnore
    <T> void b(List<T> list, na1<T> na1, i81 i81) throws IOException;

    @DexIgnore
    long c() throws IOException;

    @DexIgnore
    void c(List<Long> list) throws IOException;

    @DexIgnore
    zzte d() throws IOException;

    @DexIgnore
    void d(List<Integer> list) throws IOException;

    @DexIgnore
    String e() throws IOException;

    @DexIgnore
    void e(List<Float> list) throws IOException;

    @DexIgnore
    void f(List<zzte> list) throws IOException;

    @DexIgnore
    boolean f() throws IOException;

    @DexIgnore
    int g() throws IOException;

    @DexIgnore
    void g(List<Double> list) throws IOException;

    @DexIgnore
    int h() throws IOException;

    @DexIgnore
    void h(List<String> list) throws IOException;

    @DexIgnore
    long i() throws IOException;

    @DexIgnore
    void i(List<Long> list) throws IOException;

    @DexIgnore
    int j() throws IOException;

    @DexIgnore
    void j(List<Integer> list) throws IOException;

    @DexIgnore
    long k() throws IOException;

    @DexIgnore
    void k(List<Long> list) throws IOException;

    @DexIgnore
    String l() throws IOException;

    @DexIgnore
    void l(List<Integer> list) throws IOException;

    @DexIgnore
    long m() throws IOException;

    @DexIgnore
    void m(List<String> list) throws IOException;

    @DexIgnore
    void n(List<Long> list) throws IOException;

    @DexIgnore
    boolean n() throws IOException;

    @DexIgnore
    int o() throws IOException;

    @DexIgnore
    void o(List<Long> list) throws IOException;

    @DexIgnore
    int p() throws IOException;

    @DexIgnore
    void p(List<Integer> list) throws IOException;

    @DexIgnore
    int q() throws IOException;

    @DexIgnore
    void q(List<Integer> list) throws IOException;

    @DexIgnore
    long r() throws IOException;

    @DexIgnore
    double readDouble() throws IOException;

    @DexIgnore
    float readFloat() throws IOException;
}
