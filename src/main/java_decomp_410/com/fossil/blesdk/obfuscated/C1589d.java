package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d */
public interface C1589d extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.d$a")
    /* renamed from: com.fossil.blesdk.obfuscated.d$a */
    public static abstract class C1590a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C1589d {
        @DexIgnore
        public C1590a() {
            attachInterface(this, "android.support.v4.app.INotificationSideChannel");
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            if (i == 1) {
                parcel.enforceInterface("android.support.v4.app.INotificationSideChannel");
                mo9725a(parcel.readString(), parcel.readInt(), parcel.readString(), parcel.readInt() != 0 ? (android.app.Notification) android.app.Notification.CREATOR.createFromParcel(parcel) : null);
                return true;
            } else if (i == 2) {
                parcel.enforceInterface("android.support.v4.app.INotificationSideChannel");
                mo9724a(parcel.readString(), parcel.readInt(), parcel.readString());
                return true;
            } else if (i == 3) {
                parcel.enforceInterface("android.support.v4.app.INotificationSideChannel");
                mo9723a(parcel.readString());
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.v4.app.INotificationSideChannel");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo9723a(java.lang.String str) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo9724a(java.lang.String str, int i, java.lang.String str2) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo9725a(java.lang.String str, int i, java.lang.String str2, android.app.Notification notification) throws android.os.RemoteException;
}
