package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.np */
public final class C2499np<Transcode> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2912sr.C2913a<?>> f7835a; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2143jo> f7836b; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2977tn f7837c;

    @DexIgnore
    /* renamed from: d */
    public java.lang.Object f7838d;

    @DexIgnore
    /* renamed from: e */
    public int f7839e;

    @DexIgnore
    /* renamed from: f */
    public int f7840f;

    @DexIgnore
    /* renamed from: g */
    public java.lang.Class<?> f7841g;

    @DexIgnore
    /* renamed from: h */
    public com.bumptech.glide.load.engine.DecodeJob.C0381e f7842h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2337lo f7843i;

    @DexIgnore
    /* renamed from: j */
    public java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> f7844j;

    @DexIgnore
    /* renamed from: k */
    public java.lang.Class<Transcode> f7845k;

    @DexIgnore
    /* renamed from: l */
    public boolean f7846l;

    @DexIgnore
    /* renamed from: m */
    public boolean f7847m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C2143jo f7848n;

    @DexIgnore
    /* renamed from: o */
    public com.bumptech.glide.Priority f7849o;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C2663pp f7850p;

    @DexIgnore
    /* renamed from: q */
    public boolean f7851q;

    @DexIgnore
    /* renamed from: r */
    public boolean f7852r;

    @DexIgnore
    /* renamed from: a */
    public <R> void mo14056a(com.fossil.blesdk.obfuscated.C2977tn tnVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2143jo joVar, int i, int i2, com.fossil.blesdk.obfuscated.C2663pp ppVar, java.lang.Class<?> cls, java.lang.Class<R> cls2, com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2337lo loVar, java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>> map, boolean z, boolean z2, com.bumptech.glide.load.engine.DecodeJob.C0381e eVar) {
        this.f7837c = tnVar;
        this.f7838d = obj;
        this.f7848n = joVar;
        this.f7839e = i;
        this.f7840f = i2;
        this.f7850p = ppVar;
        this.f7841g = cls;
        this.f7842h = eVar;
        this.f7845k = cls2;
        this.f7849o = priority;
        this.f7843i = loVar;
        this.f7844j = map;
        this.f7851q = z;
        this.f7852r = z2;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1885gq mo14058b() {
        return this.f7837c.mo16511a();
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo14062c(java.lang.Class<?> cls) {
        return mo14053a(cls) != null;
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2981tq mo14063d() {
        return this.f7842h.mo3981a();
    }

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2663pp mo14064e() {
        return this.f7850p;
    }

    @DexIgnore
    /* renamed from: f */
    public int mo14065f() {
        return this.f7840f;
    }

    @DexIgnore
    /* renamed from: g */
    public java.util.List<com.fossil.blesdk.obfuscated.C2912sr.C2913a<?>> mo14066g() {
        if (!this.f7846l) {
            this.f7846l = true;
            this.f7835a.clear();
            java.util.List a = this.f7837c.mo16517f().mo3931a(this.f7838d);
            int size = a.size();
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C2912sr.C2913a a2 = ((com.fossil.blesdk.obfuscated.C2912sr) a.get(i)).mo8911a(this.f7838d, this.f7839e, this.f7840f, this.f7843i);
                if (a2 != null) {
                    this.f7835a.add(a2);
                }
            }
        }
        return this.f7835a;
    }

    @DexIgnore
    /* renamed from: h */
    public java.lang.Class<?> mo14067h() {
        return this.f7838d.getClass();
    }

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2337lo mo14068i() {
        return this.f7843i;
    }

    @DexIgnore
    /* renamed from: j */
    public com.bumptech.glide.Priority mo14069j() {
        return this.f7849o;
    }

    @DexIgnore
    /* renamed from: k */
    public java.util.List<java.lang.Class<?>> mo14070k() {
        return this.f7837c.mo16517f().mo3936c(this.f7838d.getClass(), this.f7841g, this.f7845k);
    }

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C2143jo mo14071l() {
        return this.f7848n;
    }

    @DexIgnore
    /* renamed from: m */
    public java.lang.Class<?> mo14072m() {
        return this.f7845k;
    }

    @DexIgnore
    /* renamed from: n */
    public int mo14073n() {
        return this.f7839e;
    }

    @DexIgnore
    /* renamed from: o */
    public boolean mo14074o() {
        return this.f7852r;
    }

    @DexIgnore
    /* renamed from: b */
    public <Z> com.fossil.blesdk.obfuscated.C2581oo<Z> mo14059b(java.lang.Class<Z> cls) {
        com.fossil.blesdk.obfuscated.C2581oo<Z> ooVar = this.f7844j.get(cls);
        if (ooVar == null) {
            java.util.Iterator<java.util.Map.Entry<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2581oo<?>>> it = this.f7844j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                java.util.Map.Entry next = it.next();
                if (((java.lang.Class) next.getKey()).isAssignableFrom(cls)) {
                    ooVar = (com.fossil.blesdk.obfuscated.C2581oo) next.getValue();
                    break;
                }
            }
        }
        if (ooVar != null) {
            return ooVar;
        }
        if (!this.f7844j.isEmpty() || !this.f7851q) {
            return com.fossil.blesdk.obfuscated.C2245ks.m9758a();
        }
        throw new java.lang.IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.List<com.fossil.blesdk.obfuscated.C2143jo> mo14061c() {
        if (!this.f7847m) {
            this.f7847m = true;
            this.f7836b.clear();
            java.util.List<com.fossil.blesdk.obfuscated.C2912sr.C2913a<?>> g = mo14066g();
            int size = g.size();
            for (int i = 0; i < size; i++) {
                com.fossil.blesdk.obfuscated.C2912sr.C2913a aVar = g.get(i);
                if (!this.f7836b.contains(aVar.f9449a)) {
                    this.f7836b.add(aVar.f9449a);
                }
                for (int i2 = 0; i2 < aVar.f9450b.size(); i2++) {
                    if (!this.f7836b.contains(aVar.f9450b.get(i2))) {
                        this.f7836b.add(aVar.f9450b.get(i2));
                    }
                }
            }
        }
        return this.f7836b;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo14060b(com.fossil.blesdk.obfuscated.C1438aq<?> aqVar) {
        return this.f7837c.mo16517f().mo3934b(aqVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14055a() {
        this.f7837c = null;
        this.f7838d = null;
        this.f7848n = null;
        this.f7841g = null;
        this.f7845k = null;
        this.f7843i = null;
        this.f7849o = null;
        this.f7844j = null;
        this.f7850p = null;
        this.f7835a.clear();
        this.f7846l = false;
        this.f7836b.clear();
        this.f7847m = false;
    }

    @DexIgnore
    /* renamed from: a */
    public <Data> com.fossil.blesdk.obfuscated.C3376yp<Data, ?, Transcode> mo14053a(java.lang.Class<Data> cls) {
        return this.f7837c.mo16517f().mo3933b(cls, this.f7841g, this.f7845k);
    }

    @DexIgnore
    /* renamed from: a */
    public <Z> com.fossil.blesdk.obfuscated.C2498no<Z> mo14052a(com.fossil.blesdk.obfuscated.C1438aq<Z> aqVar) {
        return this.f7837c.mo16517f().mo3928a(aqVar);
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C2912sr<java.io.File, ?>> mo14054a(java.io.File file) throws com.bumptech.glide.Registry.NoModelLoaderAvailableException {
        return this.f7837c.mo16517f().mo3931a(file);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14057a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        java.util.List<com.fossil.blesdk.obfuscated.C2912sr.C2913a<?>> g = mo14066g();
        int size = g.size();
        for (int i = 0; i < size; i++) {
            if (g.get(i).f9449a.equals(joVar)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public <X> com.fossil.blesdk.obfuscated.C1963ho<X> mo14051a(X x) throws com.bumptech.glide.Registry.NoSourceEncoderAvailableException {
        return this.f7837c.mo16517f().mo3935c(x);
    }
}
