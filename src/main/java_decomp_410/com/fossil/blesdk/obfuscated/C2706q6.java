package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q6 */
public final class C2706q6 {
    @DexIgnore
    /* renamed from: a */
    public static int m12621a(int i) {
        if (i <= 4) {
            return 8;
        }
        return i * 2;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> T[] m12623a(T[] tArr, int i, T t) {
        if (i + 1 > tArr.length) {
            T[] tArr2 = (java.lang.Object[]) java.lang.reflect.Array.newInstance(tArr.getClass().getComponentType(), m12621a(i));
            java.lang.System.arraycopy(tArr, 0, tArr2, 0, i);
            tArr = tArr2;
        }
        tArr[i] = t;
        return tArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static int[] m12622a(int[] iArr, int i, int i2) {
        if (i + 1 > iArr.length) {
            int[] iArr2 = new int[m12621a(i)];
            java.lang.System.arraycopy(iArr, 0, iArr2, 0, i);
            iArr = iArr2;
        }
        iArr[i] = i2;
        return iArr;
    }
}
