package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wn2 extends BaseProvider {
    @DexIgnore
    void a(String str, int i);

    @DexIgnore
    boolean a(MicroAppSetting microAppSetting);

    @DexIgnore
    MicroAppSetting c(String str);

    @DexIgnore
    boolean c();

    @DexIgnore
    List<MicroAppSetting> d();

    @DexIgnore
    List<MicroAppSetting> getPendingMicroAppSettings();
}
