package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class a71<T> {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static Context g;
    @DexIgnore
    public static /* final */ AtomicInteger h; // = new AtomicInteger();
    @DexIgnore
    public /* final */ g71 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ T c;
    @DexIgnore
    public volatile int d;
    @DexIgnore
    public volatile T e;

    @DexIgnore
    public a71(g71 g71, String str, T t) {
        this.d = -1;
        if (g71.a != null) {
            this.a = g71;
            this.b = str;
            this.c = t;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    @DexIgnore
    public static void a(Context context) {
        synchronized (f) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (g != context) {
                synchronized (o61.class) {
                    o61.f.clear();
                }
                synchronized (h71.class) {
                    h71.f.clear();
                }
                synchronized (w61.class) {
                    w61.b = null;
                }
                h.incrementAndGet();
                g = context;
            }
        }
    }

    @DexIgnore
    public static void f() {
        h.incrementAndGet();
    }

    @DexIgnore
    public abstract T a(Object obj);

    @DexIgnore
    public final T b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return a(this.a.c);
    }

    @DexIgnore
    public final T d() {
        t61 t61;
        String str = (String) w61.a(g).a("gms:phenotype:phenotype_flag:debug_bypass_phenotype");
        if (!(str != null && l61.c.matcher(str).matches())) {
            if (this.a.a != null) {
                t61 = o61.a(g.getContentResolver(), this.a.a);
            } else {
                t61 = h71.a(g, (String) null);
            }
            if (t61 != null) {
                Object a2 = t61.a(c());
                if (a2 != null) {
                    return a(a2);
                }
            }
        } else {
            String valueOf = String.valueOf(c());
            Log.w("PhenotypeFlag", valueOf.length() != 0 ? "Bypass reading Phenotype values for flag: ".concat(valueOf) : new String("Bypass reading Phenotype values for flag: "));
        }
        return null;
    }

    @DexIgnore
    public final T e() {
        Object a2 = w61.a(g).a(a(this.a.b));
        if (a2 != null) {
            return a(a2);
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ a71(g71 g71, String str, Object obj, b71 b71) {
        this(g71, str, obj);
    }

    @DexIgnore
    public final String a(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    @DexIgnore
    public final T a() {
        int i = h.get();
        if (this.d < i) {
            synchronized (this) {
                if (this.d < i) {
                    if (g != null) {
                        T d2 = d();
                        if (d2 == null) {
                            d2 = e();
                            if (d2 == null) {
                                d2 = this.c;
                            }
                        }
                        this.e = d2;
                        this.d = i;
                    } else {
                        throw new IllegalStateException("Must call PhenotypeFlag.init() first");
                    }
                }
            }
        }
        return this.e;
    }

    @DexIgnore
    public static a71<Long> a(g71 g71, String str, long j) {
        return new b71(g71, str, Long.valueOf(j));
    }

    @DexIgnore
    public static a71<Integer> a(g71 g71, String str, int i) {
        return new c71(g71, str, Integer.valueOf(i));
    }

    @DexIgnore
    public static a71<Boolean> a(g71 g71, String str, boolean z) {
        return new d71(g71, str, Boolean.valueOf(z));
    }

    @DexIgnore
    public static a71<Double> a(g71 g71, String str, double d2) {
        return new e71(g71, str, Double.valueOf(d2));
    }

    @DexIgnore
    public static a71<String> a(g71 g71, String str, String str2) {
        return new f71(g71, str, str2);
    }
}
