package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xq2 implements Factory<SwitchActiveDeviceUseCase> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<vj2> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<en2> e;

    @DexIgnore
    public xq2(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<vj2> provider3, Provider<PortfolioApp> provider4, Provider<en2> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static xq2 a(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<vj2> provider3, Provider<PortfolioApp> provider4, Provider<en2> provider5) {
        return new xq2(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static SwitchActiveDeviceUseCase b(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<vj2> provider3, Provider<PortfolioApp> provider4, Provider<en2> provider5) {
        return new SwitchActiveDeviceUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get());
    }

    @DexIgnore
    public SwitchActiveDeviceUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e);
    }
}
