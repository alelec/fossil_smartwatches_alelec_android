package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ve */
public class C3106ve {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3106ve.C3107a f10231a;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ve$a */
    public interface C3107a {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.C1795fe.C1797b mo10853a(int i, int i2, int i3, java.lang.Object obj);

        @DexIgnore
        /* renamed from: a */
        void mo10855a(com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar);
    }

    @DexIgnore
    public C3106ve(com.fossil.blesdk.obfuscated.C3106ve.C3107a aVar) {
        this.f10231a = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17036a(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list, int i, int i2) {
        com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar = list.get(i);
        com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar2 = list.get(i2);
        int i3 = bVar2.f5146a;
        if (i3 == 1) {
            mo17037a(list, i, bVar, i2, bVar2);
        } else if (i3 == 2) {
            mo17039b(list, i, bVar, i2, bVar2);
        } else if (i3 == 4) {
            mo17040c(list, i, bVar, i2, bVar2);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17038b(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list) {
        while (true) {
            int a = mo17035a(list);
            if (a != -1) {
                mo17036a(list, a, a + 1);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0027  */
    /* renamed from: c */
    public void mo17040c(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list, int i, com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar, int i2, com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar2) {
        com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar3;
        int i3;
        int i4;
        int i5 = bVar.f5149d;
        int i6 = bVar2.f5147b;
        com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar4 = null;
        if (i5 < i6) {
            bVar2.f5147b = i6 - 1;
        } else {
            int i7 = bVar2.f5149d;
            if (i5 < i6 + i7) {
                bVar2.f5149d = i7 - 1;
                bVar3 = this.f10231a.mo10853a(4, bVar.f5147b, 1, bVar2.f5148c);
                i3 = bVar.f5147b;
                i4 = bVar2.f5147b;
                if (i3 > i4) {
                    bVar2.f5147b = i4 + 1;
                } else {
                    int i8 = bVar2.f5149d;
                    if (i3 < i4 + i8) {
                        int i9 = (i4 + i8) - i3;
                        bVar4 = this.f10231a.mo10853a(4, i3 + 1, i9, bVar2.f5148c);
                        bVar2.f5149d -= i9;
                    }
                }
                list.set(i2, bVar);
                if (bVar2.f5149d <= 0) {
                    list.set(i, bVar2);
                } else {
                    list.remove(i);
                    this.f10231a.mo10855a(bVar2);
                }
                if (bVar3 != null) {
                    list.add(i, bVar3);
                }
                if (bVar4 == null) {
                    list.add(i, bVar4);
                    return;
                }
                return;
            }
        }
        bVar3 = null;
        i3 = bVar.f5147b;
        i4 = bVar2.f5147b;
        if (i3 > i4) {
        }
        list.set(i2, bVar);
        if (bVar2.f5149d <= 0) {
        }
        if (bVar3 != null) {
        }
        if (bVar4 == null) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0077  */
    /* renamed from: b */
    public void mo17039b(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list, int i, com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar, int i2, com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar2) {
        boolean z;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = bVar.f5147b;
        int i8 = bVar.f5149d;
        boolean z2 = false;
        if (i7 < i8) {
            if (bVar2.f5147b == i7 && bVar2.f5149d == i8 - i7) {
                z = false;
            } else {
                z = false;
                i3 = bVar.f5149d;
                i4 = bVar2.f5147b;
                if (i3 < i4) {
                    bVar2.f5147b = i4 - 1;
                } else {
                    int i9 = bVar2.f5149d;
                    if (i3 < i4 + i9) {
                        bVar2.f5149d = i9 - 1;
                        bVar.f5146a = 2;
                        bVar.f5149d = 1;
                        if (bVar2.f5149d == 0) {
                            list.remove(i2);
                            this.f10231a.mo10855a(bVar2);
                            return;
                        }
                        return;
                    }
                }
                i5 = bVar.f5147b;
                i6 = bVar2.f5147b;
                com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar3 = null;
                if (i5 <= i6) {
                    bVar2.f5147b = i6 + 1;
                } else {
                    int i10 = bVar2.f5149d;
                    if (i5 < i6 + i10) {
                        bVar3 = this.f10231a.mo10853a(2, i5 + 1, (i6 + i10) - i5, (java.lang.Object) null);
                        bVar2.f5149d = bVar.f5147b - bVar2.f5147b;
                    }
                }
                if (z2) {
                    list.set(i, bVar2);
                    list.remove(i2);
                    this.f10231a.mo10855a(bVar);
                    return;
                }
                if (z) {
                    if (bVar3 != null) {
                        int i11 = bVar.f5147b;
                        if (i11 > bVar3.f5147b) {
                            bVar.f5147b = i11 - bVar3.f5149d;
                        }
                        int i12 = bVar.f5149d;
                        if (i12 > bVar3.f5147b) {
                            bVar.f5149d = i12 - bVar3.f5149d;
                        }
                    }
                    int i13 = bVar.f5147b;
                    if (i13 > bVar2.f5147b) {
                        bVar.f5147b = i13 - bVar2.f5149d;
                    }
                    int i14 = bVar.f5149d;
                    if (i14 > bVar2.f5147b) {
                        bVar.f5149d = i14 - bVar2.f5149d;
                    }
                } else {
                    if (bVar3 != null) {
                        int i15 = bVar.f5147b;
                        if (i15 >= bVar3.f5147b) {
                            bVar.f5147b = i15 - bVar3.f5149d;
                        }
                        int i16 = bVar.f5149d;
                        if (i16 >= bVar3.f5147b) {
                            bVar.f5149d = i16 - bVar3.f5149d;
                        }
                    }
                    int i17 = bVar.f5147b;
                    if (i17 >= bVar2.f5147b) {
                        bVar.f5147b = i17 - bVar2.f5149d;
                    }
                    int i18 = bVar.f5149d;
                    if (i18 >= bVar2.f5147b) {
                        bVar.f5149d = i18 - bVar2.f5149d;
                    }
                }
                list.set(i, bVar2);
                if (bVar.f5147b != bVar.f5149d) {
                    list.set(i2, bVar);
                } else {
                    list.remove(i2);
                }
                if (bVar3 != null) {
                    list.add(i, bVar3);
                    return;
                }
                return;
            }
        } else if (bVar2.f5147b == i8 + 1 && bVar2.f5149d == i7 - i8) {
            z = true;
        } else {
            z = true;
            i3 = bVar.f5149d;
            i4 = bVar2.f5147b;
            if (i3 < i4) {
            }
            i5 = bVar.f5147b;
            i6 = bVar2.f5147b;
            com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar32 = null;
            if (i5 <= i6) {
            }
            if (z2) {
            }
        }
        z2 = true;
        i3 = bVar.f5149d;
        i4 = bVar2.f5147b;
        if (i3 < i4) {
        }
        i5 = bVar.f5147b;
        i6 = bVar2.f5147b;
        com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar322 = null;
        if (i5 <= i6) {
        }
        if (z2) {
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17037a(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list, int i, com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar, int i2, com.fossil.blesdk.obfuscated.C1795fe.C1797b bVar2) {
        int i3 = bVar.f5149d < bVar2.f5147b ? -1 : 0;
        if (bVar.f5147b < bVar2.f5147b) {
            i3++;
        }
        int i4 = bVar2.f5147b;
        int i5 = bVar.f5147b;
        if (i4 <= i5) {
            bVar.f5147b = i5 + bVar2.f5149d;
        }
        int i6 = bVar2.f5147b;
        int i7 = bVar.f5149d;
        if (i6 <= i7) {
            bVar.f5149d = i7 + bVar2.f5149d;
        }
        bVar2.f5147b += i3;
        list.set(i, bVar2);
        list.set(i2, bVar);
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo17035a(java.util.List<com.fossil.blesdk.obfuscated.C1795fe.C1797b> list) {
        boolean z = false;
        for (int size = list.size() - 1; size >= 0; size--) {
            if (list.get(size).f5146a != 8) {
                z = true;
            } else if (z) {
                return size;
            }
        }
        return -1;
    }
}
