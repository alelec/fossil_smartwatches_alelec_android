package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface so<T> {

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(Exception exc);

        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    void a();

    @DexIgnore
    void a(Priority priority, a<? super T> aVar);

    @DexIgnore
    DataSource b();

    @DexIgnore
    void cancel();

    @DexIgnore
    Class<T> getDataClass();
}
