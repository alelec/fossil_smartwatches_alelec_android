package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ge0;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gg0 implements ge0.b {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference e;
    @DexIgnore
    public /* final */ /* synthetic */ ef0 f;
    @DexIgnore
    public /* final */ /* synthetic */ eg0 g;

    @DexIgnore
    public gg0(eg0 eg0, AtomicReference atomicReference, ef0 ef0) {
        this.g = eg0;
        this.e = atomicReference;
        this.f = ef0;
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.g.a((ge0) this.e.get(), this.f, true);
    }

    @DexIgnore
    public final void f(int i) {
    }
}
