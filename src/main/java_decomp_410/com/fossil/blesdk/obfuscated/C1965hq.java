package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hq */
public class C1965hq implements com.fossil.blesdk.obfuscated.C2762qq {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1965hq.C1967b f5821a; // = new com.fossil.blesdk.obfuscated.C1965hq.C1967b();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2430mq<com.fossil.blesdk.obfuscated.C1965hq.C1966a, android.graphics.Bitmap> f5822b; // = new com.fossil.blesdk.obfuscated.C2430mq<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.hq$a */
    public static class C1966a implements com.fossil.blesdk.obfuscated.C2825rq {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C1965hq.C1967b f5823a;

        @DexIgnore
        /* renamed from: b */
        public int f5824b;

        @DexIgnore
        /* renamed from: c */
        public int f5825c;

        @DexIgnore
        /* renamed from: d */
        public android.graphics.Bitmap.Config f5826d;

        @DexIgnore
        public C1966a(com.fossil.blesdk.obfuscated.C1965hq.C1967b bVar) {
            this.f5823a = bVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11714a(int i, int i2, android.graphics.Bitmap.Config config) {
            this.f5824b = i;
            this.f5825c = i2;
            this.f5826d = config;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.fossil.blesdk.obfuscated.C1965hq.C1966a)) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1965hq.C1966a aVar = (com.fossil.blesdk.obfuscated.C1965hq.C1966a) obj;
            if (this.f5824b == aVar.f5824b && this.f5825c == aVar.f5825c && this.f5826d == aVar.f5826d) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            int i = ((this.f5824b * 31) + this.f5825c) * 31;
            android.graphics.Bitmap.Config config = this.f5826d;
            return i + (config != null ? config.hashCode() : 0);
        }

        @DexIgnore
        public java.lang.String toString() {
            return com.fossil.blesdk.obfuscated.C1965hq.m7937c(this.f5824b, this.f5825c, this.f5826d);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11713a() {
            this.f5823a.mo12068a(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.hq$b")
    /* renamed from: com.fossil.blesdk.obfuscated.hq$b */
    public static class C1967b extends com.fossil.blesdk.obfuscated.C2056iq<com.fossil.blesdk.obfuscated.C1965hq.C1966a> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1965hq.C1966a mo11719a(int i, int i2, android.graphics.Bitmap.Config config) {
            com.fossil.blesdk.obfuscated.C1965hq.C1966a aVar = (com.fossil.blesdk.obfuscated.C1965hq.C1966a) mo12069b();
            aVar.mo11714a(i, i2, config);
            return aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1965hq.C1966a m7949a() {
            return new com.fossil.blesdk.obfuscated.C1965hq.C1966a(this);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static java.lang.String m7938d(android.graphics.Bitmap bitmap) {
        return m7937c(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11708a(android.graphics.Bitmap bitmap) {
        this.f5822b.mo13695a(this.f5821a.mo11719a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo11710b(int i, int i2, android.graphics.Bitmap.Config config) {
        return m7937c(i, i2, config);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo11711c(android.graphics.Bitmap bitmap) {
        return m7938d(bitmap);
    }

    @DexIgnore
    public java.lang.String toString() {
        return "AttributeStrategy:\n  " + this.f5822b;
    }

    @DexIgnore
    /* renamed from: c */
    public static java.lang.String m7937c(int i, int i2, android.graphics.Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo11709b(android.graphics.Bitmap bitmap) {
        return com.fossil.blesdk.obfuscated.C3066uw.m14922a(bitmap);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo11707a(int i, int i2, android.graphics.Bitmap.Config config) {
        return this.f5822b.mo13693a(this.f5821a.mo11719a(i, i2, config));
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo11706a() {
        return this.f5822b.mo13692a();
    }
}
