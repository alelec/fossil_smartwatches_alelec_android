package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r82 extends q82 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout H;
    @DexIgnore
    public long I;

    /*
    static {
        K.put(R.id.cl_top, 1);
        K.put(R.id.iv_back, 2);
        K.put(R.id.tv_title, 3);
        K.put(R.id.cl_overview_day, 4);
        K.put(R.id.iv_back_date, 5);
        K.put(R.id.ftv_day_of_week, 6);
        K.put(R.id.ftv_day_of_month, 7);
        K.put(R.id.line, 8);
        K.put(R.id.ftv_daily_value, 9);
        K.put(R.id.ftv_daily_unit, 10);
        K.put(R.id.ftv_est, 11);
        K.put(R.id.iv_next_date, 12);
        K.put(R.id.appBarLayout, 13);
        K.put(R.id.cl_pb_goal, 14);
        K.put(R.id.pb_goal, 15);
        K.put(R.id.ftv_progress_value, 16);
        K.put(R.id.ftv_goal_value, 17);
        K.put(R.id.dayChart, 18);
        K.put(R.id.llWorkout, 19);
        K.put(R.id.v_elevation, 20);
        K.put(R.id.ftv_no_workout_recorded, 21);
        K.put(R.id.rvWorkout, 22);
    }
    */

    @DexIgnore
    public r82(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 23, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexIgnore
    public r82(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[13], objArr[4], objArr[14], objArr[1], objArr[18], objArr[10], objArr[9], objArr[7], objArr[6], objArr[11], objArr[17], objArr[21], objArr[16], objArr[2], objArr[5], objArr[12], objArr[8], objArr[19], objArr[15], objArr[22], objArr[3], objArr[20]);
        this.I = -1;
        this.H = objArr[0];
        this.H.setTag((Object) null);
        a(view);
        f();
    }
}
