package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.dr4;
import com.fossil.blesdk.obfuscated.gr4;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nr4 {
    @DexIgnore
    public static /* final */ nr4 a; // = d();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends nr4 {
        @DexIgnore
        public boolean a(Method method) {
            return method.isDefault();
        }

        @DexIgnore
        public List<? extends gr4.a> b() {
            return Collections.singletonList(lr4.a);
        }

        @DexIgnore
        public int c() {
            return 1;
        }

        @DexIgnore
        public Object a(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
            Constructor<MethodHandles.Lookup> declaredConstructor = MethodHandles.Lookup.class.getDeclaredConstructor(new Class[]{Class.class, Integer.TYPE});
            declaredConstructor.setAccessible(true);
            return declaredConstructor.newInstance(new Object[]{cls, -1}).unreflectSpecial(method, cls).bindTo(obj).invokeWithArguments(objArr);
        }

        @DexIgnore
        public List<? extends dr4.a> a(Executor executor) {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(fr4.a);
            arrayList.add(new hr4(executor));
            return Collections.unmodifiableList(arrayList);
        }
    }

    @DexIgnore
    public static nr4 d() {
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                return new a();
            }
        } catch (ClassNotFoundException unused) {
        }
        try {
            Class.forName("java.util.Optional");
            return new b();
        } catch (ClassNotFoundException unused2) {
            return new nr4();
        }
    }

    @DexIgnore
    public static nr4 e() {
        return a;
    }

    @DexIgnore
    public List<? extends dr4.a> a(Executor executor) {
        return Collections.singletonList(new hr4(executor));
    }

    @DexIgnore
    public Executor a() {
        return null;
    }

    @DexIgnore
    public boolean a(Method method) {
        return false;
    }

    @DexIgnore
    public List<? extends gr4.a> b() {
        return Collections.emptyList();
    }

    @DexIgnore
    public int c() {
        return 0;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends nr4 {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nr4$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.nr4$a$a  reason: collision with other inner class name */
        public static class C0097a implements Executor {
            @DexIgnore
            public /* final */ Handler e; // = new Handler(Looper.getMainLooper());

            @DexIgnore
            public void execute(Runnable runnable) {
                this.e.post(runnable);
            }
        }

        @DexIgnore
        public boolean a(Method method) {
            if (Build.VERSION.SDK_INT < 24) {
                return false;
            }
            return method.isDefault();
        }

        @DexIgnore
        public List<? extends gr4.a> b() {
            if (Build.VERSION.SDK_INT >= 24) {
                return Collections.singletonList(lr4.a);
            }
            return Collections.emptyList();
        }

        @DexIgnore
        public int c() {
            return Build.VERSION.SDK_INT >= 24 ? 1 : 0;
        }

        @DexIgnore
        public Executor a() {
            return new C0097a();
        }

        @DexIgnore
        public List<? extends dr4.a> a(Executor executor) {
            if (executor != null) {
                hr4 hr4 = new hr4(executor);
                if (Build.VERSION.SDK_INT < 24) {
                    return Collections.singletonList(hr4);
                }
                return Arrays.asList(new dr4.a[]{fr4.a, hr4});
            }
            throw new AssertionError();
        }
    }

    @DexIgnore
    public Object a(Method method, Class<?> cls, Object obj, Object... objArr) throws Throwable {
        throw new UnsupportedOperationException();
    }
}
