package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wa */
public final class C3177wa implements android.os.Parcelable {
    @DexIgnore
    public static /* final */ android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C3177wa> CREATOR; // = new com.fossil.blesdk.obfuscated.C3177wa.C3178a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ int[] f10505e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f10506f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ int f10507g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.lang.String f10508h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ int f10509i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ int f10510j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ java.lang.CharSequence f10511k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ int f10512l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ java.lang.CharSequence f10513m;

    @DexIgnore
    /* renamed from: n */
    public /* final */ java.util.ArrayList<java.lang.String> f10514n;

    @DexIgnore
    /* renamed from: o */
    public /* final */ java.util.ArrayList<java.lang.String> f10515o;

    @DexIgnore
    /* renamed from: p */
    public /* final */ boolean f10516p;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wa$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wa$a */
    public static class C3178a implements android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C3177wa> {
        @DexIgnore
        public com.fossil.blesdk.obfuscated.C3177wa createFromParcel(android.os.Parcel parcel) {
            return new com.fossil.blesdk.obfuscated.C3177wa(parcel);
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C3177wa[] newArray(int i) {
            return new com.fossil.blesdk.obfuscated.C3177wa[i];
        }
    }

    @DexIgnore
    public C3177wa(com.fossil.blesdk.obfuscated.C3101va vaVar) {
        int size = vaVar.f10191b.size();
        this.f10505e = new int[(size * 6)];
        if (vaVar.f10198i) {
            int i = 0;
            for (int i2 = 0; i2 < size; i2++) {
                com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = vaVar.f10191b.get(i2);
                int[] iArr = this.f10505e;
                int i3 = i + 1;
                iArr[i] = aVar.f10211a;
                int i4 = i3 + 1;
                androidx.fragment.app.Fragment fragment = aVar.f10212b;
                iArr[i3] = fragment != null ? fragment.mIndex : -1;
                int[] iArr2 = this.f10505e;
                int i5 = i4 + 1;
                iArr2[i4] = aVar.f10213c;
                int i6 = i5 + 1;
                iArr2[i5] = aVar.f10214d;
                int i7 = i6 + 1;
                iArr2[i6] = aVar.f10215e;
                i = i7 + 1;
                iArr2[i7] = aVar.f10216f;
            }
            this.f10506f = vaVar.f10196g;
            this.f10507g = vaVar.f10197h;
            this.f10508h = vaVar.f10200k;
            this.f10509i = vaVar.f10202m;
            this.f10510j = vaVar.f10203n;
            this.f10511k = vaVar.f10204o;
            this.f10512l = vaVar.f10205p;
            this.f10513m = vaVar.f10206q;
            this.f10514n = vaVar.f10207r;
            this.f10515o = vaVar.f10208s;
            this.f10516p = vaVar.f10209t;
            return;
        }
        throw new java.lang.IllegalStateException("Not on back stack");
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3101va mo17337a(androidx.fragment.app.FragmentManagerImpl fragmentManagerImpl) {
        com.fossil.blesdk.obfuscated.C3101va vaVar = new com.fossil.blesdk.obfuscated.C3101va(fragmentManagerImpl);
        int i = 0;
        int i2 = 0;
        while (i < this.f10505e.length) {
            com.fossil.blesdk.obfuscated.C3101va.C3102a aVar = new com.fossil.blesdk.obfuscated.C3101va.C3102a();
            int i3 = i + 1;
            aVar.f10211a = this.f10505e[i];
            if (androidx.fragment.app.FragmentManagerImpl.f1046I) {
                android.util.Log.v("FragmentManager", "Instantiate " + vaVar + " op #" + i2 + " base fragment #" + this.f10505e[i3]);
            }
            int i4 = i3 + 1;
            int i5 = this.f10505e[i3];
            if (i5 >= 0) {
                aVar.f10212b = fragmentManagerImpl.f1062i.get(i5);
            } else {
                aVar.f10212b = null;
            }
            int[] iArr = this.f10505e;
            int i6 = i4 + 1;
            aVar.f10213c = iArr[i4];
            int i7 = i6 + 1;
            aVar.f10214d = iArr[i6];
            int i8 = i7 + 1;
            aVar.f10215e = iArr[i7];
            aVar.f10216f = iArr[i8];
            vaVar.f10192c = aVar.f10213c;
            vaVar.f10193d = aVar.f10214d;
            vaVar.f10194e = aVar.f10215e;
            vaVar.f10195f = aVar.f10216f;
            vaVar.mo17005a(aVar);
            i2++;
            i = i8 + 1;
        }
        vaVar.f10196g = this.f10506f;
        vaVar.f10197h = this.f10507g;
        vaVar.f10200k = this.f10508h;
        vaVar.f10202m = this.f10509i;
        vaVar.f10198i = true;
        vaVar.f10203n = this.f10510j;
        vaVar.f10204o = this.f10511k;
        vaVar.f10205p = this.f10512l;
        vaVar.f10206q = this.f10513m;
        vaVar.f10207r = this.f10514n;
        vaVar.f10208s = this.f10515o;
        vaVar.f10209t = this.f10516p;
        vaVar.mo17002a(1);
        return vaVar;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(android.os.Parcel parcel, int i) {
        parcel.writeIntArray(this.f10505e);
        parcel.writeInt(this.f10506f);
        parcel.writeInt(this.f10507g);
        parcel.writeString(this.f10508h);
        parcel.writeInt(this.f10509i);
        parcel.writeInt(this.f10510j);
        android.text.TextUtils.writeToParcel(this.f10511k, parcel, 0);
        parcel.writeInt(this.f10512l);
        android.text.TextUtils.writeToParcel(this.f10513m, parcel, 0);
        parcel.writeStringList(this.f10514n);
        parcel.writeStringList(this.f10515o);
        parcel.writeInt(this.f10516p ? 1 : 0);
    }

    @DexIgnore
    public C3177wa(android.os.Parcel parcel) {
        this.f10505e = parcel.createIntArray();
        this.f10506f = parcel.readInt();
        this.f10507g = parcel.readInt();
        this.f10508h = parcel.readString();
        this.f10509i = parcel.readInt();
        this.f10510j = parcel.readInt();
        this.f10511k = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f10512l = parcel.readInt();
        this.f10513m = (java.lang.CharSequence) android.text.TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f10514n = parcel.createStringArrayList();
        this.f10515o = parcel.createStringArrayList();
        this.f10516p = parcel.readInt() != 0;
    }
}
