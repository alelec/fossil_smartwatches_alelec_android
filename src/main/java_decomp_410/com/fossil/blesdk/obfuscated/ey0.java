package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru0;
import com.google.android.gms.internal.clearcut.zzco;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ey0 extends ru0<ey0, a> implements uv0 {
    @DexIgnore
    public static volatile cw0<ey0> zzbg;
    @DexIgnore
    public static /* final */ ey0 zzbir; // = new ey0();
    @DexIgnore
    public wu0<b> zzbiq; // = ru0.h();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ru0.a<ey0, a> implements uv0 {
        @DexIgnore
        public a() {
            super(ey0.zzbir);
        }

        @DexIgnore
        public /* synthetic */ a(fy0 fy0) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ru0<b, a> implements uv0 {
        @DexIgnore
        public static volatile cw0<b> zzbg;
        @DexIgnore
        public static /* final */ b zzbiv; // = new b();
        @DexIgnore
        public int zzbb;
        @DexIgnore
        public String zzbis; // = "";
        @DexIgnore
        public long zzbit;
        @DexIgnore
        public long zzbiu;
        @DexIgnore
        public int zzya;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ru0.a<b, a> implements uv0 {
            @DexIgnore
            public a() {
                super(b.zzbiv);
            }

            @DexIgnore
            public /* synthetic */ a(fy0 fy0) {
                this();
            }

            @DexIgnore
            public final a a(long j) {
                g();
                ((b) this.f).a(j);
                return this;
            }

            @DexIgnore
            public final a a(String str) {
                g();
                ((b) this.f).a(str);
                return this;
            }

            @DexIgnore
            public final a b(long j) {
                g();
                ((b) this.f).b(j);
                return this;
            }
        }

        /*
        static {
            ru0.a(b.class, zzbiv);
        }
        */

        @DexIgnore
        public static a n() {
            return (a) zzbiv.a(ru0.e.e, (Object) null, (Object) null);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v14, types: [com.fossil.blesdk.obfuscated.cw0<com.fossil.blesdk.obfuscated.ey0$b>, com.fossil.blesdk.obfuscated.ru0$b] */
        public final Object a(int i, Object obj, Object obj2) {
            cw0<b> cw0;
            switch (fy0.a[i - 1]) {
                case 1:
                    return new b();
                case 2:
                    return new a((fy0) null);
                case 3:
                    return ru0.a((sv0) zzbiv, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0005\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0002\u0003", new Object[]{"zzbb", "zzya", "zzbis", "zzbit", "zzbiu"});
                case 4:
                    return zzbiv;
                case 5:
                    cw0<b> cw02 = zzbg;
                    cw0<b> cw03 = cw02;
                    if (cw02 == null) {
                        synchronized (b.class) {
                            cw0<b> cw04 = zzbg;
                            cw0 = cw04;
                            if (cw04 == null) {
                                Object bVar = new ru0.b(zzbiv);
                                zzbg = bVar;
                                cw0 = bVar;
                            }
                        }
                        cw03 = cw0;
                    }
                    return cw03;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        @DexIgnore
        public final void a(long j) {
            this.zzbb |= 4;
            this.zzbit = j;
        }

        @DexIgnore
        public final void a(String str) {
            if (str != null) {
                this.zzbb |= 2;
                this.zzbis = str;
                return;
            }
            throw new NullPointerException();
        }

        @DexIgnore
        public final void b(long j) {
            this.zzbb |= 8;
            this.zzbiu = j;
        }

        @DexIgnore
        public final int i() {
            return this.zzya;
        }

        @DexIgnore
        public final boolean j() {
            return (this.zzbb & 1) == 1;
        }

        @DexIgnore
        public final String k() {
            return this.zzbis;
        }

        @DexIgnore
        public final long l() {
            return this.zzbit;
        }

        @DexIgnore
        public final long m() {
            return this.zzbiu;
        }
    }

    /*
    static {
        ru0.a(ey0.class, zzbir);
    }
    */

    @DexIgnore
    public static ey0 a(byte[] bArr) throws zzco {
        return (ey0) ru0.b(zzbir, bArr);
    }

    @DexIgnore
    public static ey0 j() {
        return zzbir;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v14, types: [com.fossil.blesdk.obfuscated.cw0<com.fossil.blesdk.obfuscated.ey0>, com.fossil.blesdk.obfuscated.ru0$b] */
    public final Object a(int i, Object obj, Object obj2) {
        cw0<ey0> cw0;
        switch (fy0.a[i - 1]) {
            case 1:
                return new ey0();
            case 2:
                return new a((fy0) null);
            case 3:
                return ru0.a((sv0) zzbir, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0002\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzbiq", b.class});
            case 4:
                return zzbir;
            case 5:
                cw0<ey0> cw02 = zzbg;
                cw0<ey0> cw03 = cw02;
                if (cw02 == null) {
                    synchronized (ey0.class) {
                        cw0<ey0> cw04 = zzbg;
                        cw0 = cw04;
                        if (cw04 == null) {
                            Object bVar = new ru0.b(zzbir);
                            zzbg = bVar;
                            cw0 = bVar;
                        }
                    }
                    cw03 = cw0;
                }
                return cw03;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final List<b> i() {
        return this.zzbiq;
    }
}
