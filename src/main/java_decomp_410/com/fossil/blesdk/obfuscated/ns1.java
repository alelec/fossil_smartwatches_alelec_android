package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ns1 extends h1 {
    @DexIgnore
    public ns1(Context context) {
        super(context);
    }

    @DexIgnore
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        k1 k1Var = (k1) a(i, i2, i3, charSequence);
        ps1 ps1 = new ps1(e(), this, k1Var);
        k1Var.a((v1) ps1);
        return ps1;
    }
}
