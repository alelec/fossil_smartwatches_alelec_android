package com.fossil.blesdk.obfuscated;

import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class it0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore
    public it0(Uri uri) {
        this((String) null, uri, "", "", false, false);
    }

    @DexIgnore
    public it0(String str, Uri uri, String str2, String str3, boolean z, boolean z2) {
        this.a = str;
        this.b = uri;
        this.c = str2;
        this.d = str3;
        this.e = z;
        this.f = z2;
    }

    @DexIgnore
    public final it0 a(String str) {
        boolean z = this.e;
        if (!z) {
            return new it0(this.a, this.b, str, this.d, z, this.f);
        }
        throw new IllegalStateException("Cannot set GServices prefix and skip GServices");
    }

    @DexIgnore
    public final <T> ys0<T> a(String str, T t, ht0<T> ht0) {
        return ys0.a(this, str, t, ht0);
    }

    @DexIgnore
    public final ys0<String> a(String str, String str2) {
        return ys0.a(this, str, (String) null);
    }

    @DexIgnore
    public final ys0<Boolean> a(String str, boolean z) {
        return ys0.a(this, str, false);
    }

    @DexIgnore
    public final it0 b(String str) {
        return new it0(this.a, this.b, this.c, str, this.e, this.f);
    }
}
