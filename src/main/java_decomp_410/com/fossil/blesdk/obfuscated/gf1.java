package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gf1 extends a51 implements le1 {
    @DexIgnore
    public gf1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IMapViewDelegate");
    }

    @DexIgnore
    public final void a(Bundle bundle) throws RemoteException {
        Parcel o = o();
        c51.a(o, (Parcelable) bundle);
        Parcel a = a(7, o);
        if (a.readInt() != 0) {
            bundle.readFromParcel(a);
        }
        a.recycle();
    }

    @DexIgnore
    public final void b(Bundle bundle) throws RemoteException {
        Parcel o = o();
        c51.a(o, (Parcelable) bundle);
        b(2, o);
    }

    @DexIgnore
    public final void c() throws RemoteException {
        b(13, o());
    }

    @DexIgnore
    public final void d() throws RemoteException {
        b(3, o());
    }

    @DexIgnore
    public final sn0 getView() throws RemoteException {
        Parcel a = a(8, o());
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void onLowMemory() throws RemoteException {
        b(6, o());
    }

    @DexIgnore
    public final void onPause() throws RemoteException {
        b(4, o());
    }

    @DexIgnore
    public final void b() throws RemoteException {
        b(5, o());
    }

    @DexIgnore
    public final void a(se1 se1) throws RemoteException {
        Parcel o = o();
        c51.a(o, (IInterface) se1);
        b(9, o);
    }

    @DexIgnore
    public final void a() throws RemoteException {
        b(12, o());
    }
}
