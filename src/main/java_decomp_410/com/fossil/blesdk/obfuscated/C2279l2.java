package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l2 */
public class C2279l2 extends android.widget.ListView {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.graphics.Rect f7095e; // = new android.graphics.Rect();

    @DexIgnore
    /* renamed from: f */
    public int f7096f; // = 0;

    @DexIgnore
    /* renamed from: g */
    public int f7097g; // = 0;

    @DexIgnore
    /* renamed from: h */
    public int f7098h; // = 0;

    @DexIgnore
    /* renamed from: i */
    public int f7099i; // = 0;

    @DexIgnore
    /* renamed from: j */
    public int f7100j;

    @DexIgnore
    /* renamed from: k */
    public java.lang.reflect.Field f7101k;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C2279l2.C2280a f7102l;

    @DexIgnore
    /* renamed from: m */
    public boolean f7103m;

    @DexIgnore
    /* renamed from: n */
    public boolean f7104n;

    @DexIgnore
    /* renamed from: o */
    public boolean f7105o;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C2110j9 f7106p;

    @DexIgnore
    /* renamed from: q */
    public com.fossil.blesdk.obfuscated.C3349y9 f7107q;

    @DexIgnore
    /* renamed from: r */
    public com.fossil.blesdk.obfuscated.C2279l2.C2281b f7108r;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l2$a")
    /* renamed from: com.fossil.blesdk.obfuscated.l2$a */
    public static class C2280a extends com.fossil.blesdk.obfuscated.C2615p0 {

        @DexIgnore
        /* renamed from: f */
        public boolean f7109f; // = true;

        @DexIgnore
        public C2280a(android.graphics.drawable.Drawable drawable) {
            super(drawable);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13104a(boolean z) {
            this.f7109f = z;
        }

        @DexIgnore
        public void draw(android.graphics.Canvas canvas) {
            if (this.f7109f) {
                super.draw(canvas);
            }
        }

        @DexIgnore
        public void setHotspot(float f, float f2) {
            if (this.f7109f) {
                super.setHotspot(f, f2);
            }
        }

        @DexIgnore
        public void setHotspotBounds(int i, int i2, int i3, int i4) {
            if (this.f7109f) {
                super.setHotspotBounds(i, i2, i3, i4);
            }
        }

        @DexIgnore
        public boolean setState(int[] iArr) {
            if (this.f7109f) {
                return super.setState(iArr);
            }
            return false;
        }

        @DexIgnore
        public boolean setVisible(boolean z, boolean z2) {
            if (this.f7109f) {
                return super.setVisible(z, z2);
            }
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l2$b")
    /* renamed from: com.fossil.blesdk.obfuscated.l2$b */
    public class C2281b implements java.lang.Runnable {
        @DexIgnore
        public C2281b() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13109a() {
            com.fossil.blesdk.obfuscated.C2279l2 l2Var = com.fossil.blesdk.obfuscated.C2279l2.this;
            l2Var.f7108r = null;
            l2Var.removeCallbacks(this);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo13110b() {
            com.fossil.blesdk.obfuscated.C2279l2.this.post(this);
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2279l2 l2Var = com.fossil.blesdk.obfuscated.C2279l2.this;
            l2Var.f7108r = null;
            l2Var.drawableStateChanged();
        }
    }

    @DexIgnore
    public C2279l2(android.content.Context context, boolean z) {
        super(context, (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.dropDownListViewStyle);
        this.f7104n = z;
        setCacheColorHint(0);
        try {
            this.f7101k = android.widget.AbsListView.class.getDeclaredField("mIsChildViewEnabled");
            this.f7101k.setAccessible(true);
        } catch (java.lang.NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    private void setSelectorEnabled(boolean z) {
        com.fossil.blesdk.obfuscated.C2279l2.C2280a aVar = this.f7102l;
        if (aVar != null) {
            aVar.mo13104a(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int mo13082a(int i, int i2, int i3, int i4, int i5) {
        int i6;
        int listPaddingTop = getListPaddingTop();
        int listPaddingBottom = getListPaddingBottom();
        getListPaddingLeft();
        getListPaddingRight();
        int dividerHeight = getDividerHeight();
        android.graphics.drawable.Drawable divider = getDivider();
        android.widget.ListAdapter adapter = getAdapter();
        if (adapter == null) {
            return listPaddingTop + listPaddingBottom;
        }
        int i7 = listPaddingTop + listPaddingBottom;
        if (dividerHeight <= 0 || divider == null) {
            dividerHeight = 0;
        }
        int count = adapter.getCount();
        int i8 = i7;
        android.view.View view = null;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        while (i9 < count) {
            int itemViewType = adapter.getItemViewType(i9);
            if (itemViewType != i10) {
                view = null;
                i10 = itemViewType;
            }
            view = adapter.getView(i9, view, this);
            android.view.ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = generateDefaultLayoutParams();
                view.setLayoutParams(layoutParams);
            }
            int i12 = layoutParams.height;
            if (i12 > 0) {
                i6 = android.view.View.MeasureSpec.makeMeasureSpec(i12, 1073741824);
            } else {
                i6 = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
            }
            view.measure(i, i6);
            view.forceLayout();
            if (i9 > 0) {
                i8 += dividerHeight;
            }
            i8 += view.getMeasuredHeight();
            if (i8 >= i4) {
                return (i5 < 0 || i9 <= i5 || i11 <= 0 || i8 == i4) ? i4 : i11;
            }
            if (i5 >= 0 && i9 >= i5) {
                i11 = i8;
            }
            i9++;
        }
        return i8;
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo13090b(int i, android.view.View view) {
        android.graphics.drawable.Drawable selector = getSelector();
        boolean z = true;
        boolean z2 = (selector == null || i == -1) ? false : true;
        if (z2) {
            selector.setVisible(false, false);
        }
        mo13084a(i, view);
        if (z2) {
            android.graphics.Rect rect = this.f7095e;
            float exactCenterX = rect.exactCenterX();
            float exactCenterY = rect.exactCenterY();
            if (getVisibility() != 0) {
                z = false;
            }
            selector.setVisible(z, false);
            com.fossil.blesdk.obfuscated.C1538c7.m5297a(selector, exactCenterX, exactCenterY);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo13092c() {
        android.graphics.drawable.Drawable selector = getSelector();
        if (selector != null && mo13091b() && isPressed()) {
            selector.setState(getDrawableState());
        }
    }

    @DexIgnore
    public void dispatchDraw(android.graphics.Canvas canvas) {
        mo13086a(canvas);
        super.dispatchDraw(canvas);
    }

    @DexIgnore
    public void drawableStateChanged() {
        if (this.f7108r == null) {
            super.drawableStateChanged();
            setSelectorEnabled(true);
            mo13092c();
        }
    }

    @DexIgnore
    public boolean hasFocus() {
        return this.f7104n || super.hasFocus();
    }

    @DexIgnore
    public boolean hasWindowFocus() {
        return this.f7104n || super.hasWindowFocus();
    }

    @DexIgnore
    public boolean isFocused() {
        return this.f7104n || super.isFocused();
    }

    @DexIgnore
    public boolean isInTouchMode() {
        return (this.f7104n && this.f7103m) || super.isInTouchMode();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.f7108r = null;
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public boolean onHoverEvent(android.view.MotionEvent motionEvent) {
        if (android.os.Build.VERSION.SDK_INT < 26) {
            return super.onHoverEvent(motionEvent);
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 10 && this.f7108r == null) {
            this.f7108r = new com.fossil.blesdk.obfuscated.C2279l2.C2281b();
            this.f7108r.mo13110b();
        }
        boolean onHoverEvent = super.onHoverEvent(motionEvent);
        if (actionMasked == 9 || actionMasked == 7) {
            int pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
            if (!(pointToPosition == -1 || pointToPosition == getSelectedItemPosition())) {
                android.view.View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                if (childAt.isEnabled()) {
                    setSelectionFromTop(pointToPosition, childAt.getTop() - getTop());
                }
                mo13092c();
            }
        } else {
            setSelection(-1);
        }
        return onHoverEvent;
    }

    @DexIgnore
    public boolean onTouchEvent(android.view.MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.f7100j = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY());
        }
        com.fossil.blesdk.obfuscated.C2279l2.C2281b bVar = this.f7108r;
        if (bVar != null) {
            bVar.mo13109a();
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setListSelectionHidden(boolean z) {
        this.f7103m = z;
    }

    @DexIgnore
    public void setSelector(android.graphics.drawable.Drawable drawable) {
        this.f7102l = drawable != null ? new com.fossil.blesdk.obfuscated.C2279l2.C2280a(drawable) : null;
        super.setSelector(this.f7102l);
        android.graphics.Rect rect = new android.graphics.Rect();
        if (drawable != null) {
            drawable.getPadding(rect);
        }
        this.f7096f = rect.left;
        this.f7097g = rect.top;
        this.f7098h = rect.right;
        this.f7099i = rect.bottom;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo13091b() {
        return this.f7105o;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000c, code lost:
        if (r0 != 3) goto L_0x000e;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0065  */
    /* renamed from: a */
    public boolean mo13089a(android.view.MotionEvent motionEvent, int i) {
        boolean z;
        boolean z2;
        int findPointerIndex;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 1) {
            z2 = false;
            findPointerIndex = motionEvent.findPointerIndex(i);
            if (findPointerIndex >= 0) {
            }
            z = false;
            z2 = false;
            mo13083a();
            if (!z2) {
            }
            return z2;
        } else if (actionMasked == 2) {
            z2 = true;
            findPointerIndex = motionEvent.findPointerIndex(i);
            if (findPointerIndex >= 0) {
                int x = (int) motionEvent.getX(findPointerIndex);
                int y = (int) motionEvent.getY(findPointerIndex);
                int pointToPosition = pointToPosition(x, y);
                if (pointToPosition == -1) {
                    z = true;
                    if (!z2 || z) {
                        mo13083a();
                    }
                    if (!z2) {
                        if (this.f7107q == null) {
                            this.f7107q = new com.fossil.blesdk.obfuscated.C3349y9(this);
                        }
                        this.f7107q.mo16303a(true);
                        this.f7107q.onTouch(this, motionEvent);
                    } else {
                        com.fossil.blesdk.obfuscated.C3349y9 y9Var = this.f7107q;
                        if (y9Var != null) {
                            y9Var.mo16303a(false);
                        }
                    }
                    return z2;
                }
                android.view.View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                mo13088a(childAt, pointToPosition, (float) x, (float) y);
                if (actionMasked == 1) {
                    mo13087a(childAt, pointToPosition);
                }
            }
            z = false;
            z2 = false;
            mo13083a();
            if (!z2) {
            }
            return z2;
        }
        z = false;
        z2 = true;
        mo13083a();
        if (!z2) {
        }
        return z2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13087a(android.view.View view, int i) {
        performItemClick(view, i, getItemIdAtPosition(i));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13086a(android.graphics.Canvas canvas) {
        if (!this.f7095e.isEmpty()) {
            android.graphics.drawable.Drawable selector = getSelector();
            if (selector != null) {
                selector.setBounds(this.f7095e);
                selector.draw(canvas);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13085a(int i, android.view.View view, float f, float f2) {
        mo13090b(i, view);
        android.graphics.drawable.Drawable selector = getSelector();
        if (selector != null && i != -1) {
            com.fossil.blesdk.obfuscated.C1538c7.m5297a(selector, f, f2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13084a(int i, android.view.View view) {
        android.graphics.Rect rect = this.f7095e;
        rect.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        rect.left -= this.f7096f;
        rect.top -= this.f7097g;
        rect.right += this.f7098h;
        rect.bottom += this.f7099i;
        try {
            boolean z = this.f7101k.getBoolean(this);
            if (view.isEnabled() != z) {
                this.f7101k.set(this, java.lang.Boolean.valueOf(!z));
                if (i != -1) {
                    refreshDrawableState();
                }
            }
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13083a() {
        this.f7105o = false;
        setPressed(false);
        drawableStateChanged();
        android.view.View childAt = getChildAt(this.f7100j - getFirstVisiblePosition());
        if (childAt != null) {
            childAt.setPressed(false);
        }
        com.fossil.blesdk.obfuscated.C2110j9 j9Var = this.f7106p;
        if (j9Var != null) {
            j9Var.mo12280a();
            this.f7106p = null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13088a(android.view.View view, int i, float f, float f2) {
        this.f7105o = true;
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            drawableHotspotChanged(f, f2);
        }
        if (!isPressed()) {
            setPressed(true);
        }
        layoutChildren();
        int i2 = this.f7100j;
        if (i2 != -1) {
            android.view.View childAt = getChildAt(i2 - getFirstVisiblePosition());
            if (!(childAt == null || childAt == view || !childAt.isPressed())) {
                childAt.setPressed(false);
            }
        }
        this.f7100j = i;
        float left = f - ((float) view.getLeft());
        float top = f2 - ((float) view.getTop());
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.drawableHotspotChanged(left, top);
        }
        if (!view.isPressed()) {
            view.setPressed(true);
        }
        mo13085a(i, view, f, f2);
        setSelectorEnabled(false);
        refreshDrawableState();
    }
}
