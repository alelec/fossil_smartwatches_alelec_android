package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gy1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ Intent f;
    @DexIgnore
    public /* final */ /* synthetic */ ey1 g;

    @DexIgnore
    public gy1(ey1 ey1, Intent intent, Intent intent2) {
        this.g = ey1;
        this.e = intent;
        this.f = intent2;
    }

    @DexIgnore
    public final void run() {
        this.g.d(this.e);
        this.g.a(this.f);
    }
}
