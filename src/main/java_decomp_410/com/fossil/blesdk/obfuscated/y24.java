package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ Context f;
    @DexIgnore
    public /* final */ /* synthetic */ k04 g;

    @DexIgnore
    public y24(String str, Context context, k04 k04) {
        this.e = str;
        this.f = context;
        this.g = k04;
    }

    @DexIgnore
    public final void run() {
        try {
            synchronized (j04.k) {
                if (j04.k.size() >= h04.h()) {
                    t14 f2 = j04.m;
                    f2.d("The number of page events exceeds the maximum value " + Integer.toString(h04.h()));
                    return;
                }
                String unused = j04.i = this.e;
                if (j04.k.containsKey(j04.i)) {
                    t14 f3 = j04.m;
                    f3.c("Duplicate PageID : " + j04.i + ", onResume() repeated?");
                    return;
                }
                j04.k.put(j04.i, Long.valueOf(System.currentTimeMillis()));
                j04.a(this.f, true, this.g);
            }
        } catch (Throwable th) {
            j04.m.a(th);
            j04.a(this.f, th);
        }
    }
}
