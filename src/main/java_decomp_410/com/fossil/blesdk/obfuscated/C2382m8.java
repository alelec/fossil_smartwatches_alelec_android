package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m8 */
public abstract class C2382m8 {
    @DexIgnore
    public static /* final */ java.lang.String TAG; // = "ActionProvider(support)";
    @DexIgnore
    public /* final */ android.content.Context mContext;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2382m8.C2383a mSubUiVisibilityListener;
    @DexIgnore
    public com.fossil.blesdk.obfuscated.C2382m8.C2384b mVisibilityListener;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.m8$a */
    public interface C2383a {
        @DexIgnore
        /* renamed from: b */
        void mo13539b(boolean z);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.m8$b */
    public interface C2384b {
        @DexIgnore
        void onActionProviderVisibilityChanged(boolean z);
    }

    @DexIgnore
    public C2382m8(android.content.Context context) {
        this.mContext = context;
    }

    @DexIgnore
    public android.content.Context getContext() {
        return this.mContext;
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return false;
    }

    @DexIgnore
    public boolean isVisible() {
        return true;
    }

    @DexIgnore
    public abstract android.view.View onCreateActionView();

    @DexIgnore
    public android.view.View onCreateActionView(android.view.MenuItem menuItem) {
        return onCreateActionView();
    }

    @DexIgnore
    public boolean onPerformDefaultAction() {
        return false;
    }

    @DexIgnore
    public void onPrepareSubMenu(android.view.SubMenu subMenu) {
    }

    @DexIgnore
    public boolean overridesItemVisibility() {
        return false;
    }

    @DexIgnore
    public void refreshVisibility() {
        if (this.mVisibilityListener != null && overridesItemVisibility()) {
            this.mVisibilityListener.onActionProviderVisibilityChanged(isVisible());
        }
    }

    @DexIgnore
    public void reset() {
        this.mVisibilityListener = null;
        this.mSubUiVisibilityListener = null;
    }

    @DexIgnore
    public void setSubUiVisibilityListener(com.fossil.blesdk.obfuscated.C2382m8.C2383a aVar) {
        this.mSubUiVisibilityListener = aVar;
    }

    @DexIgnore
    public void setVisibilityListener(com.fossil.blesdk.obfuscated.C2382m8.C2384b bVar) {
        if (!(this.mVisibilityListener == null || bVar == null)) {
            android.util.Log.w(TAG, "setVisibilityListener: Setting a new ActionProvider.VisibilityListener when one is already set. Are you reusing this " + getClass().getSimpleName() + " instance while it is still in use somewhere else?");
        }
        this.mVisibilityListener = bVar;
    }

    @DexIgnore
    public void subUiVisibilityChanged(boolean z) {
        com.fossil.blesdk.obfuscated.C2382m8.C2383a aVar = this.mSubUiVisibilityListener;
        if (aVar != null) {
            aVar.mo13539b(z);
        }
    }
}
