package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.rn0;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wn0 implements vn0<T> {
    @DexIgnore
    public /* final */ /* synthetic */ rn0 a;

    @DexIgnore
    public wn0(rn0 rn0) {
        this.a = rn0;
    }

    @DexIgnore
    public final void a(T t) {
        tn0 unused = this.a.a = t;
        Iterator it = this.a.c.iterator();
        while (it.hasNext()) {
            ((rn0.a) it.next()).a(this.a.a);
        }
        this.a.c.clear();
        Bundle unused2 = this.a.b = null;
    }
}
