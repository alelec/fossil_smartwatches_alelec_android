package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gv */
public final class C1907gv {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.bumptech.glide.load.ImageHeaderParser> f5570a; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: a */
    public synchronized java.util.List<com.bumptech.glide.load.ImageHeaderParser> mo11404a() {
        return this.f5570a;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo11405a(com.bumptech.glide.load.ImageHeaderParser imageHeaderParser) {
        this.f5570a.add(imageHeaderParser);
    }
}
