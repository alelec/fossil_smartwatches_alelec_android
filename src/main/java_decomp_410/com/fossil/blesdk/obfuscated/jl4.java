package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface jl4 extends Cloneable {

    @DexIgnore
    public interface a {
        @DexIgnore
        jl4 a(dm4 dm4);
    }

    @DexIgnore
    void a(kl4 kl4);

    @DexIgnore
    void cancel();

    @DexIgnore
    dm4 n();

    @DexIgnore
    boolean o();

    @DexIgnore
    Response r() throws IOException;
}
