package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class iz3 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public void a(Bundle bundle) {
        this.a = bundle.getInt("_wxapi_baseresp_errcode");
        bundle.getString("_wxapi_baseresp_errstr");
        bundle.getString("_wxapi_baseresp_transaction");
        this.b = bundle.getString("_wxapi_baseresp_openId");
    }
}
