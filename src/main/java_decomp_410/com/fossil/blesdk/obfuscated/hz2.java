package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.cz2;
import com.fossil.blesdk.obfuscated.vs2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hz2 extends as2 implements gz2, ws3.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public fz2 k;
    @DexIgnore
    public tr3<ud2> l;
    @DexIgnore
    public vs2 m;
    @DexIgnore
    public cz2 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return hz2.p;
        }

        @DexIgnore
        public final hz2 b() {
            return new hz2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements cz2.b {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 a;

        @DexIgnore
        public c(hz2 hz2) {
            this.a = hz2;
        }

        @DexIgnore
        public void a(int i, boolean z, boolean z2) {
            hz2.b(this.a).a(i, z, z2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 e;

        @DexIgnore
        public d(hz2 hz2) {
            this.e = hz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 e;

        @DexIgnore
        public e(hz2 hz2) {
            this.e = hz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 e;

        @DexIgnore
        public f(hz2 hz2) {
            this.e = hz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hz2.b(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 e;

        @DexIgnore
        public g(hz2 hz2) {
            this.e = hz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hz2.b(this.e).j();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 e;

        @DexIgnore
        public h(hz2 hz2) {
            this.e = hz2;
        }

        @DexIgnore
        public final void onClick(View view) {
            hz2.b(this.e).l();
        }
    }

    /*
    static {
        String simpleName = hz2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationContactsAndA\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ fz2 b(hz2 hz2) {
        fz2 fz2 = hz2.k;
        if (fz2 != null) {
            return fz2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        T0();
        return true;
    }

    @DexIgnore
    public void T0() {
        tr3<ud2> tr3 = this.l;
        if (tr3 == null) {
            kd4.d("mBinding");
            throw null;
        } else if (tr3.a() != null) {
            fz2 fz2 = this.k;
            if (fz2 == null) {
                kd4.d("mPresenter");
                throw null;
            } else if (!fz2.i()) {
                close();
            } else {
                ds3 ds3 = ds3.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                ds3.b(childFragmentManager);
            }
        }
    }

    @DexIgnore
    public final void U0() {
        vs2 vs2 = this.m;
        if (vs2 == null) {
            kd4.d("mAdapter");
            throw null;
        } else if (vs2.b()) {
            fz2 fz2 = this.k;
            if (fz2 != null) {
                fz2.m();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(childFragmentManager);
        }
    }

    @DexIgnore
    public void c(int i, ArrayList<ContactWrapper> arrayList) {
        kd4.b(arrayList, "contactWrappersSelected");
        NotificationHybridContactActivity.C.a(this, i, arrayList);
    }

    @DexIgnore
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public void f(List<Object> list) {
        kd4.b(list, "contactAndAppData");
        vs2 vs2 = this.m;
        if (vs2 != null) {
            vs2.a((List<? extends Object>) list);
            if (!isActive()) {
                return;
            }
            if (list.isEmpty()) {
                tr3<ud2> tr3 = this.l;
                if (tr3 != null) {
                    ud2 a2 = tr3.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.q;
                        kd4.a((Object) flexibleTextView, "it.ftvAssignSection");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.x;
                        kd4.a((Object) recyclerView, "it.rvAssign");
                        recyclerView.setVisibility(8);
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            tr3<ud2> tr32 = this.l;
            if (tr32 != null) {
                ud2 a3 = tr32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.q;
                    kd4.a((Object) flexibleTextView2, "it.ftvAssignSection");
                    flexibleTextView2.setVisibility(0);
                    RecyclerView recyclerView2 = a3.x;
                    kd4.a((Object) recyclerView2, "it.rvAssign");
                    recyclerView2.setVisibility(0);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public void h(int i) {
        tr3<ud2> tr3 = this.l;
        if (tr3 != null) {
            ud2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.r;
                if (flexibleTextView != null) {
                    pd4 pd4 = pd4.a;
                    String a3 = sm2.a(getContext(), (int) R.string.AlertsHybrid_AssignNotifications_Select_Title__AssignToNumber);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026ct_Title__AssignToNumber)");
                    Object[] objArr = {Integer.valueOf(i)};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j() {
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
                kd4.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i != 4567) {
            if (i != 5678) {
                if (i == 6789 && i2 == -1 && intent != null) {
                    Serializable serializableExtra = intent.getSerializableExtra("CONTACT_DATA");
                    if (serializableExtra != null) {
                        ArrayList arrayList = (ArrayList) serializableExtra;
                        fz2 fz2 = this.k;
                        if (fz2 != null) {
                            fz2.b(arrayList);
                        } else {
                            kd4.d("mPresenter");
                            throw null;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                    }
                }
            } else if (i2 == -1 && intent != null) {
                Serializable serializableExtra2 = intent.getSerializableExtra("CONTACT_DATA");
                if (serializableExtra2 != null) {
                    ArrayList arrayList2 = (ArrayList) serializableExtra2;
                    fz2 fz22 = this.k;
                    if (fz22 != null) {
                        fz22.a((ArrayList<ContactWrapper>) arrayList2);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> /* = java.util.ArrayList<com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper> */");
                }
            }
        } else if (i2 == -1 && intent != null) {
            ArrayList<String> stringArrayListExtra = intent.getStringArrayListExtra("APP_DATA");
            fz2 fz23 = this.k;
            if (fz23 != null) {
                kd4.a((Object) stringArrayListExtra, "stringAppsSelected");
                fz23.c(stringArrayListExtra);
                return;
            }
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ud2 ud2 = (ud2) qa.a(layoutInflater, R.layout.fragment_notification_contacts_and_apps_assigned, viewGroup, false, O0());
        this.n = (cz2) getChildFragmentManager().a(cz2.w.a());
        if (this.n == null) {
            this.n = cz2.w.b();
        }
        cz2 cz2 = this.n;
        if (cz2 != null) {
            cz2.a(new c(this));
        }
        FlexibleTextView flexibleTextView = ud2.q;
        kd4.a((Object) flexibleTextView, "binding.ftvAssignSection");
        flexibleTextView.setVisibility(8);
        RecyclerView recyclerView = ud2.x;
        kd4.a((Object) recyclerView, "binding.rvAssign");
        recyclerView.setVisibility(8);
        ImageView imageView = ud2.t;
        kd4.a((Object) imageView, "binding.ivDone");
        Context context = getContext();
        if (context != null) {
            imageView.setBackground(k6.c(context, R.drawable.button_radious_color_grey));
            ud2.s.setOnClickListener(new d(this));
            ud2.t.setOnClickListener(new e(this));
            ud2.w.setOnClickListener(new f(this));
            ud2.v.setOnClickListener(new g(this));
            ud2.u.setOnClickListener(new h(this));
            vs2 vs2 = new vs2();
            vs2.a((vs2.c) new b(this));
            this.m = vs2;
            RecyclerView recyclerView2 = ud2.x;
            recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext()));
            recyclerView2.setHasFixedSize(true);
            vs2 vs22 = this.m;
            if (vs22 != null) {
                recyclerView2.setAdapter(vs22);
                this.l = new tr3<>(this, ud2);
                kd4.a((Object) ud2, "binding");
                return ud2.d();
            }
            kd4.d("mAdapter");
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        fz2 fz2 = this.k;
        if (fz2 != null) {
            fz2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        fz2 fz2 = this.k;
        if (fz2 != null) {
            fz2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void p(boolean z) {
        if (isActive()) {
            tr3<ud2> tr3 = this.l;
            if (tr3 != null) {
                ud2 a2 = tr3.a();
                if (a2 != null) {
                    ImageView imageView = a2.t;
                    if (imageView != null) {
                        kd4.a((Object) imageView, "doneButton");
                        imageView.setEnabled(z);
                        imageView.setClickable(z);
                        if (z) {
                            Context context = getContext();
                            if (context != null) {
                                imageView.setBackground(k6.c(context, R.color.primaryColor));
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            Context context2 = getContext();
                            if (context2 != null) {
                                imageView.setBackground(k6.c(context2, R.color.disabledButton));
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void b(int i, ArrayList<String> arrayList) {
        kd4.b(arrayList, "stringAppsSelected");
        NotificationHybridAppActivity.C.a(this, i, arrayList);
    }

    @DexIgnore
    public void a(fz2 fz2) {
        kd4.b(fz2, "presenter");
        this.k = fz2;
    }

    @DexIgnore
    public void a(int i, ArrayList<ContactWrapper> arrayList) {
        kd4.b(arrayList, "contactWrappersSelected");
        NotificationHybridEveryoneActivity.C.a(this, i, arrayList);
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            return;
        }
        if (i == R.id.tv_cancel) {
            close();
        } else if (i == R.id.tv_ok) {
            U0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements vs2.c {
        @DexIgnore
        public /* final */ /* synthetic */ hz2 a;

        @DexIgnore
        public b(hz2 hz2) {
            this.a = hz2;
        }

        @DexIgnore
        public void a(ContactWrapper contactWrapper) {
            kd4.b(contactWrapper, "contactWrapper");
            cz2 a2 = this.a.n;
            if (a2 != null) {
                Contact contact = contactWrapper.getContact();
                Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                if (valueOf != null) {
                    int intValue = valueOf.intValue();
                    Contact contact2 = contactWrapper.getContact();
                    Boolean valueOf2 = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                    if (valueOf2 != null) {
                        boolean booleanValue = valueOf2.booleanValue();
                        Contact contact3 = contactWrapper.getContact();
                        Boolean valueOf3 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                        if (valueOf3 != null) {
                            a2.a(intValue, booleanValue, valueOf3.booleanValue());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            cz2 a3 = this.a.n;
            if (a3 != null) {
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                a3.show(childFragmentManager, cz2.w.a());
            }
        }

        @DexIgnore
        public void a() {
            hz2.b(this.a).h();
        }
    }
}
