package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ym */
public class C3370ym extends com.fossil.blesdk.obfuscated.C3445zm {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1882gn f11291a;

    @DexIgnore
    public C3370ym(com.fossil.blesdk.obfuscated.C1882gn gnVar) {
        this.f11291a = gnVar;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1815fn mo11693b(com.android.volley.Request<?> request, java.util.Map<java.lang.String, java.lang.String> map) throws java.io.IOException, com.android.volley.AuthFailureError {
        try {
            org.apache.http.HttpResponse a = this.f11291a.mo10080a(request, map);
            int statusCode = a.getStatusLine().getStatusCode();
            org.apache.http.Header[] allHeaders = a.getAllHeaders();
            java.util.ArrayList arrayList = new java.util.ArrayList(allHeaders.length);
            for (org.apache.http.Header header : allHeaders) {
                arrayList.add(new com.fossil.blesdk.obfuscated.C2660pm(header.getName(), header.getValue()));
            }
            if (a.getEntity() == null) {
                return new com.fossil.blesdk.obfuscated.C1815fn(statusCode, arrayList);
            }
            long contentLength = a.getEntity().getContentLength();
            if (((long) ((int) contentLength)) == contentLength) {
                return new com.fossil.blesdk.obfuscated.C1815fn(statusCode, arrayList, (int) a.getEntity().getContentLength(), a.getEntity().getContent());
            }
            throw new java.io.IOException("Response too large: " + contentLength);
        } catch (org.apache.http.conn.ConnectTimeoutException e) {
            throw new java.net.SocketTimeoutException(e.getMessage());
        }
    }
}
