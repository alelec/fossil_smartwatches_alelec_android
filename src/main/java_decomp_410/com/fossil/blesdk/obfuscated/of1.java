package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class of1 implements Parcelable.Creator<LatLngBounds> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        LatLng latLng = null;
        LatLng latLng2 = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                latLng = (LatLng) SafeParcelReader.a(parcel, a, LatLng.CREATOR);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                latLng2 = (LatLng) SafeParcelReader.a(parcel, a, LatLng.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new LatLngBounds(latLng, latLng2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LatLngBounds[i];
    }
}
