package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fg implements jg {
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ Object[] f;

    @DexIgnore
    public fg(String str, Object[] objArr) {
        this.e = str;
        this.f = objArr;
    }

    @DexIgnore
    public void a(ig igVar) {
        a(igVar, this.f);
    }

    @DexIgnore
    public String b() {
        return this.e;
    }

    @DexIgnore
    public int a() {
        Object[] objArr = this.f;
        if (objArr == null) {
            return 0;
        }
        return objArr.length;
    }

    @DexIgnore
    public static void a(ig igVar, Object[] objArr) {
        if (objArr != null) {
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                Object obj = objArr[i];
                i++;
                a(igVar, i, obj);
            }
        }
    }

    @DexIgnore
    public fg(String str) {
        this(str, (Object[]) null);
    }

    @DexIgnore
    public static void a(ig igVar, int i, Object obj) {
        if (obj == null) {
            igVar.a(i);
        } else if (obj instanceof byte[]) {
            igVar.a(i, (byte[]) obj);
        } else if (obj instanceof Float) {
            igVar.a(i, (double) ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            igVar.a(i, ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            igVar.b(i, ((Long) obj).longValue());
        } else if (obj instanceof Integer) {
            igVar.b(i, (long) ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            igVar.b(i, (long) ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            igVar.b(i, (long) ((Byte) obj).byteValue());
        } else if (obj instanceof String) {
            igVar.a(i, (String) obj);
        } else if (obj instanceof Boolean) {
            igVar.b(i, ((Boolean) obj).booleanValue() ? 1 : 0);
        } else {
            throw new IllegalArgumentException("Cannot bind " + obj + " at index " + i + " Supported types: null, byte[], float, double, long, int, short, byte," + " string");
        }
    }
}
