package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zn1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements b {
        @DexIgnore
        public /* final */ CountDownLatch a;

        @DexIgnore
        public a() {
            this.a = new CountDownLatch(1);
        }

        @DexIgnore
        public final void a() throws InterruptedException {
            this.a.await();
        }

        @DexIgnore
        public final void onCanceled() {
            this.a.countDown();
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            this.a.countDown();
        }

        @DexIgnore
        public final void onSuccess(Object obj) {
            this.a.countDown();
        }

        @DexIgnore
        public final boolean a(long j, TimeUnit timeUnit) throws InterruptedException {
            return this.a.await(j, timeUnit);
        }

        @DexIgnore
        public /* synthetic */ a(vo1 vo1) {
            this();
        }
    }

    @DexIgnore
    public interface b extends qn1, sn1, tn1<Object> {
    }

    @DexIgnore
    public static <TResult> wn1<TResult> a(TResult tresult) {
        uo1 uo1 = new uo1();
        uo1.a(tresult);
        return uo1;
    }

    @DexIgnore
    public static <TResult> TResult b(wn1<TResult> wn1) throws ExecutionException {
        if (wn1.e()) {
            return wn1.b();
        }
        if (wn1.c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(wn1.a());
    }

    @DexIgnore
    public static <TResult> wn1<TResult> a(Exception exc) {
        uo1 uo1 = new uo1();
        uo1.a(exc);
        return uo1;
    }

    @DexIgnore
    public static <TResult> TResult a(wn1<TResult> wn1) throws ExecutionException, InterruptedException {
        bk0.a();
        bk0.a(wn1, (Object) "Task must not be null");
        if (wn1.d()) {
            return b(wn1);
        }
        a aVar = new a((vo1) null);
        a(wn1, aVar);
        aVar.a();
        return b(wn1);
    }

    @DexIgnore
    public static <TResult> TResult a(wn1<TResult> wn1, long j, TimeUnit timeUnit) throws ExecutionException, InterruptedException, TimeoutException {
        bk0.a();
        bk0.a(wn1, (Object) "Task must not be null");
        bk0.a(timeUnit, (Object) "TimeUnit must not be null");
        if (wn1.d()) {
            return b(wn1);
        }
        a aVar = new a((vo1) null);
        a(wn1, aVar);
        if (aVar.a(j, timeUnit)) {
            return b(wn1);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    @DexIgnore
    public static void a(wn1<?> wn1, b bVar) {
        wn1.a(yn1.b, (tn1<? super Object>) bVar);
        wn1.a(yn1.b, (sn1) bVar);
        wn1.a(yn1.b, (qn1) bVar);
    }
}
