package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yk0 extends ks0 implements xk0 {
    @DexIgnore
    public yk0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    @DexIgnore
    public final void a(vk0 vk0) throws RemoteException {
        Parcel o = o();
        ms0.a(o, (IInterface) vk0);
        c(1, o);
    }
}
