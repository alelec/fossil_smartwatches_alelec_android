package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l1 */
public class C2274l1 extends com.fossil.blesdk.obfuscated.C1525c1<com.fossil.blesdk.obfuscated.C2001i7> implements android.view.MenuItem {

    @DexIgnore
    /* renamed from: e */
    public java.lang.reflect.Method f7087e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l1$a")
    /* renamed from: com.fossil.blesdk.obfuscated.l1$a */
    public class C2275a extends com.fossil.blesdk.obfuscated.C2382m8 {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.view.ActionProvider f7088a;

        @DexIgnore
        public C2275a(android.content.Context context, android.view.ActionProvider actionProvider) {
            super(context);
            this.f7088a = actionProvider;
        }

        @DexIgnore
        public boolean hasSubMenu() {
            return this.f7088a.hasSubMenu();
        }

        @DexIgnore
        public android.view.View onCreateActionView() {
            return this.f7088a.onCreateActionView();
        }

        @DexIgnore
        public boolean onPerformDefaultAction() {
            return this.f7088a.onPerformDefaultAction();
        }

        @DexIgnore
        public void onPrepareSubMenu(android.view.SubMenu subMenu) {
            this.f7088a.onPrepareSubMenu(com.fossil.blesdk.obfuscated.C2274l1.this.mo9377a(subMenu));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l1$b")
    /* renamed from: com.fossil.blesdk.obfuscated.l1$b */
    public static class C2276b extends android.widget.FrameLayout implements com.fossil.blesdk.obfuscated.C2935t0 {

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.view.CollapsibleActionView f7090e;

        @DexIgnore
        public C2276b(android.view.View view) {
            super(view.getContext());
            this.f7090e = (android.view.CollapsibleActionView) view;
            addView(view);
        }

        @DexIgnore
        /* renamed from: a */
        public android.view.View mo13075a() {
            return (android.view.View) this.f7090e;
        }

        @DexIgnore
        public void onActionViewCollapsed() {
            this.f7090e.onActionViewCollapsed();
        }

        @DexIgnore
        public void onActionViewExpanded() {
            this.f7090e.onActionViewExpanded();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l1$c")
    /* renamed from: com.fossil.blesdk.obfuscated.l1$c */
    public class C2277c extends com.fossil.blesdk.obfuscated.C1592d1<android.view.MenuItem.OnActionExpandListener> implements android.view.MenuItem.OnActionExpandListener {
        @DexIgnore
        public C2277c(android.view.MenuItem.OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        @DexIgnore
        public boolean onMenuItemActionCollapse(android.view.MenuItem menuItem) {
            return ((android.view.MenuItem.OnActionExpandListener) this.f4235a).onMenuItemActionCollapse(com.fossil.blesdk.obfuscated.C2274l1.this.mo9376a(menuItem));
        }

        @DexIgnore
        public boolean onMenuItemActionExpand(android.view.MenuItem menuItem) {
            return ((android.view.MenuItem.OnActionExpandListener) this.f4235a).onMenuItemActionExpand(com.fossil.blesdk.obfuscated.C2274l1.this.mo9376a(menuItem));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l1$d")
    /* renamed from: com.fossil.blesdk.obfuscated.l1$d */
    public class C2278d extends com.fossil.blesdk.obfuscated.C1592d1<android.view.MenuItem.OnMenuItemClickListener> implements android.view.MenuItem.OnMenuItemClickListener {
        @DexIgnore
        public C2278d(android.view.MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        @DexIgnore
        public boolean onMenuItemClick(android.view.MenuItem menuItem) {
            return ((android.view.MenuItem.OnMenuItemClickListener) this.f4235a).onMenuItemClick(com.fossil.blesdk.obfuscated.C2274l1.this.mo9376a(menuItem));
        }
    }

    @DexIgnore
    public C2274l1(android.content.Context context, com.fossil.blesdk.obfuscated.C2001i7 i7Var) {
        super(context, i7Var);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13019a(boolean z) {
        try {
            if (this.f7087e == null) {
                this.f7087e = ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getClass().getDeclaredMethod("setExclusiveCheckable", new java.lang.Class[]{java.lang.Boolean.TYPE});
            }
            this.f7087e.invoke(this.f4235a, new java.lang.Object[]{java.lang.Boolean.valueOf(z)});
        } catch (java.lang.Exception e) {
            android.util.Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }

    @DexIgnore
    public boolean collapseActionView() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).collapseActionView();
    }

    @DexIgnore
    public boolean expandActionView() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).expandActionView();
    }

    @DexIgnore
    public android.view.ActionProvider getActionProvider() {
        com.fossil.blesdk.obfuscated.C2382m8 a = ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).mo8358a();
        if (a instanceof com.fossil.blesdk.obfuscated.C2274l1.C2275a) {
            return ((com.fossil.blesdk.obfuscated.C2274l1.C2275a) a).f7088a;
        }
        return null;
    }

    @DexIgnore
    public android.view.View getActionView() {
        android.view.View actionView = ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getActionView();
        return actionView instanceof com.fossil.blesdk.obfuscated.C2274l1.C2276b ? ((com.fossil.blesdk.obfuscated.C2274l1.C2276b) actionView).mo13075a() : actionView;
    }

    @DexIgnore
    public int getAlphabeticModifiers() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getAlphabeticModifiers();
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getAlphabeticShortcut();
    }

    @DexIgnore
    public java.lang.CharSequence getContentDescription() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getContentDescription();
    }

    @DexIgnore
    public int getGroupId() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getGroupId();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getIcon() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getIcon();
    }

    @DexIgnore
    public android.content.res.ColorStateList getIconTintList() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getIconTintList();
    }

    @DexIgnore
    public android.graphics.PorterDuff.Mode getIconTintMode() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getIconTintMode();
    }

    @DexIgnore
    public android.content.Intent getIntent() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getIntent();
    }

    @DexIgnore
    public int getItemId() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getItemId();
    }

    @DexIgnore
    public android.view.ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getMenuInfo();
    }

    @DexIgnore
    public int getNumericModifiers() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getNumericModifiers();
    }

    @DexIgnore
    public char getNumericShortcut() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getNumericShortcut();
    }

    @DexIgnore
    public int getOrder() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getOrder();
    }

    @DexIgnore
    public android.view.SubMenu getSubMenu() {
        return mo9377a(((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getSubMenu());
    }

    @DexIgnore
    public java.lang.CharSequence getTitle() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getTitle();
    }

    @DexIgnore
    public java.lang.CharSequence getTitleCondensed() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getTitleCondensed();
    }

    @DexIgnore
    public java.lang.CharSequence getTooltipText() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getTooltipText();
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).hasSubMenu();
    }

    @DexIgnore
    public boolean isActionViewExpanded() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).isActionViewExpanded();
    }

    @DexIgnore
    public boolean isCheckable() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).isCheckable();
    }

    @DexIgnore
    public boolean isChecked() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).isChecked();
    }

    @DexIgnore
    public boolean isEnabled() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).isEnabled();
    }

    @DexIgnore
    public boolean isVisible() {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).isVisible();
    }

    @DexIgnore
    public android.view.MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).mo8357a(actionProvider != null ? mo13018a(actionProvider) : null);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setActionView(android.view.View view) {
        if (view instanceof android.view.CollapsibleActionView) {
            view = new com.fossil.blesdk.obfuscated.C2274l1.C2276b(view);
        }
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setActionView(view);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setAlphabeticShortcut(char c) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setAlphabeticShortcut(c);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setCheckable(boolean z) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setCheckable(z);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setChecked(boolean z) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setChecked(z);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setContentDescription(java.lang.CharSequence charSequence) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setContentDescription(charSequence);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setEnabled(boolean z) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setEnabled(z);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIcon(android.graphics.drawable.Drawable drawable) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setIcon(drawable);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIconTintList(android.content.res.ColorStateList colorStateList) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setIconTintList(colorStateList);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIconTintMode(android.graphics.PorterDuff.Mode mode) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setIconTintMode(mode);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIntent(android.content.Intent intent) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setIntent(intent);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setNumericShortcut(char c) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setNumericShortcut(c);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setOnActionExpandListener(android.view.MenuItem.OnActionExpandListener onActionExpandListener) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setOnActionExpandListener(onActionExpandListener != null ? new com.fossil.blesdk.obfuscated.C2274l1.C2277c(onActionExpandListener) : null);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setOnMenuItemClickListener(android.view.MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setOnMenuItemClickListener(onMenuItemClickListener != null ? new com.fossil.blesdk.obfuscated.C2274l1.C2278d(onMenuItemClickListener) : null);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setShortcut(char c, char c2) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setShortcut(c, c2);
        return this;
    }

    @DexIgnore
    public void setShowAsAction(int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setShowAsAction(i);
    }

    @DexIgnore
    public android.view.MenuItem setShowAsActionFlags(int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setShowAsActionFlags(i);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitle(java.lang.CharSequence charSequence) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setTitle(charSequence);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitleCondensed(java.lang.CharSequence charSequence) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setTitleCondensed(charSequence);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTooltipText(java.lang.CharSequence charSequence) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setTooltipText(charSequence);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setVisible(boolean z) {
        return ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setVisible(z);
    }

    @DexIgnore
    public android.view.MenuItem setAlphabeticShortcut(char c, int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setAlphabeticShortcut(c, i);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setIcon(int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setIcon(i);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setNumericShortcut(char c, int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setNumericShortcut(c, i);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setShortcut(char c, char c2, int i, int i2) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setShortcut(c, c2, i, i2);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setTitle(int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setTitle(i);
        return this;
    }

    @DexIgnore
    public android.view.MenuItem setActionView(int i) {
        ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setActionView(i);
        android.view.View actionView = ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).getActionView();
        if (actionView instanceof android.view.CollapsibleActionView) {
            ((com.fossil.blesdk.obfuscated.C2001i7) this.f4235a).setActionView((android.view.View) new com.fossil.blesdk.obfuscated.C2274l1.C2276b(actionView));
        }
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2274l1.C2275a mo13018a(android.view.ActionProvider actionProvider) {
        return new com.fossil.blesdk.obfuscated.C2274l1.C2275a(this.f3952b, actionProvider);
    }
}
