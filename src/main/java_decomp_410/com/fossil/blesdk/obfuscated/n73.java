package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n73 {
    @DexIgnore
    public /* final */ z83 a;
    @DexIgnore
    public /* final */ z73 b;
    @DexIgnore
    public /* final */ z93 c;
    @DexIgnore
    public /* final */ zb3 d;
    @DexIgnore
    public /* final */ zc3 e;
    @DexIgnore
    public /* final */ za3 f;

    @DexIgnore
    public n73(z83 z83, z73 z73, z93 z93, zb3 zb3, zc3 zc3, za3 za3) {
        kd4.b(z83, "mActivityView");
        kd4.b(z73, "mActiveTimeView");
        kd4.b(z93, "mCaloriesView");
        kd4.b(zb3, "mHeartrateView");
        kd4.b(zc3, "mSleepView");
        kd4.b(za3, "mGoalTrackingView");
        this.a = z83;
        this.b = z73;
        this.c = z93;
        this.d = zb3;
        this.e = zc3;
        this.f = za3;
    }

    @DexIgnore
    public final z73 a() {
        return this.b;
    }

    @DexIgnore
    public final z83 b() {
        return this.a;
    }

    @DexIgnore
    public final z93 c() {
        return this.c;
    }

    @DexIgnore
    public final za3 d() {
        return this.f;
    }

    @DexIgnore
    public final zb3 e() {
        return this.d;
    }

    @DexIgnore
    public final zc3 f() {
        return this.e;
    }
}
