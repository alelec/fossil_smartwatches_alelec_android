package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sp1 extends bj0 implements cp1 {
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public sp1(DataHolder dataHolder, int i, int i2) {
        super(dataHolder, i);
        this.d = i2;
    }

    @DexIgnore
    public final ep1 a() {
        return new up1(this.a, this.b, this.d);
    }

    @DexIgnore
    public final int b() {
        return b("event_type");
    }

    @DexIgnore
    public final String toString() {
        String str = b() == 1 ? "changed" : b() == 2 ? "deleted" : "unknown";
        String valueOf = String.valueOf(a());
        StringBuilder sb = new StringBuilder(str.length() + 32 + String.valueOf(valueOf).length());
        sb.append("DataEventRef{ type=");
        sb.append(str);
        sb.append(", dataitem=");
        sb.append(valueOf);
        sb.append(" }");
        return sb.toString();
    }
}
