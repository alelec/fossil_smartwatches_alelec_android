package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class wy1 implements Runnable {
    @DexIgnore
    public /* final */ vy1 e;
    @DexIgnore
    public /* final */ Bundle f;
    @DexIgnore
    public /* final */ xn1 g;

    @DexIgnore
    public wy1(vy1 vy1, Bundle bundle, xn1 xn1) {
        this.e = vy1;
        this.f = bundle;
        this.g = xn1;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
