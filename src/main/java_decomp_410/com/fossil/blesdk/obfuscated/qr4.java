package com.fossil.blesdk.obfuscated;

import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qr4<T> {
    @DexIgnore
    public /* final */ Response a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ em4 c;

    @DexIgnore
    public qr4(Response response, T t, em4 em4) {
        this.a = response;
        this.b = t;
        this.c = em4;
    }

    @DexIgnore
    public static <T> qr4<T> a(T t, Response response) {
        ur4.a(response, "rawResponse == null");
        if (response.E()) {
            return new qr4<>(response, t, (em4) null);
        }
        throw new IllegalArgumentException("rawResponse must be successful response");
    }

    @DexIgnore
    public int b() {
        return this.a.B();
    }

    @DexIgnore
    public em4 c() {
        return this.c;
    }

    @DexIgnore
    public boolean d() {
        return this.a.E();
    }

    @DexIgnore
    public String e() {
        return this.a.F();
    }

    @DexIgnore
    public Response f() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public static <T> qr4<T> a(em4 em4, Response response) {
        ur4.a(em4, "body == null");
        ur4.a(response, "rawResponse == null");
        if (!response.E()) {
            return new qr4<>(response, (Object) null, em4);
        }
        throw new IllegalArgumentException("rawResponse should not be successful response");
    }

    @DexIgnore
    public T a() {
        return this.b;
    }
}
