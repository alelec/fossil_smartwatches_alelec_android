package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fx2 {
    @DexIgnore
    void a(dw2 dw2);

    @DexIgnore
    void a(ix2 ix2);

    @DexIgnore
    void a(nw2 nw2);
}
