package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z2 */
public class C3410z2 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f11478a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.res.TypedArray f11479b;

    @DexIgnore
    /* renamed from: c */
    public android.util.TypedValue f11480c;

    @DexIgnore
    public C3410z2(android.content.Context context, android.content.res.TypedArray typedArray) {
        this.f11478a = context;
        this.f11479b = typedArray;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3410z2 m17204a(android.content.Context context, android.util.AttributeSet attributeSet, int[] iArr) {
        return new com.fossil.blesdk.obfuscated.C3410z2(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    @DexIgnore
    /* renamed from: b */
    public android.graphics.drawable.Drawable mo18422b(int i) {
        if (this.f11479b.hasValue(i)) {
            int resourceId = this.f11479b.getResourceId(i, 0);
            if (resourceId != 0) {
                return com.fossil.blesdk.obfuscated.C2364m0.m10497c(this.f11478a, resourceId);
            }
        }
        return this.f11479b.getDrawable(i);
    }

    @DexIgnore
    /* renamed from: c */
    public android.graphics.drawable.Drawable mo18424c(int i) {
        if (!this.f11479b.hasValue(i)) {
            return null;
        }
        int resourceId = this.f11479b.getResourceId(i, 0);
        if (resourceId != 0) {
            return com.fossil.blesdk.obfuscated.C1526c2.m5217a().mo9385a(this.f11478a, resourceId, true);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.String mo18426d(int i) {
        return this.f11479b.getString(i);
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.CharSequence mo18428e(int i) {
        return this.f11479b.getText(i);
    }

    @DexIgnore
    /* renamed from: f */
    public int mo18429f(int i, int i2) {
        return this.f11479b.getLayoutDimension(i, i2);
    }

    @DexIgnore
    /* renamed from: g */
    public int mo18431g(int i, int i2) {
        return this.f11479b.getResourceId(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3410z2 m17205a(android.content.Context context, android.util.AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new com.fossil.blesdk.obfuscated.C3410z2(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    @DexIgnore
    /* renamed from: d */
    public int mo18425d(int i, int i2) {
        return this.f11479b.getInt(i, i2);
    }

    @DexIgnore
    /* renamed from: e */
    public int mo18427e(int i, int i2) {
        return this.f11479b.getInteger(i, i2);
    }

    @DexIgnore
    /* renamed from: f */
    public java.lang.CharSequence[] mo18430f(int i) {
        return this.f11479b.getTextArray(i);
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo18432g(int i) {
        return this.f11479b.hasValue(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3410z2 m17203a(android.content.Context context, int i, int[] iArr) {
        return new com.fossil.blesdk.obfuscated.C3410z2(context, context.obtainStyledAttributes(i, iArr));
    }

    @DexIgnore
    /* renamed from: c */
    public int mo18423c(int i, int i2) {
        return this.f11479b.getDimensionPixelSize(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo18417a(int i, int i2, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar) {
        int resourceId = this.f11479b.getResourceId(i, 0);
        if (resourceId == 0) {
            return null;
        }
        if (this.f11480c == null) {
            this.f11480c = new android.util.TypedValue();
        }
        return com.fossil.blesdk.obfuscated.C2785r6.m13072a(this.f11478a, resourceId, this.f11480c, i2, aVar);
    }

    @DexIgnore
    /* renamed from: b */
    public float mo18420b(int i, float f) {
        return this.f11479b.getFloat(i, f);
    }

    @DexIgnore
    /* renamed from: b */
    public int mo18421b(int i, int i2) {
        return this.f11479b.getDimensionPixelOffset(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo18419a(int i, boolean z) {
        return this.f11479b.getBoolean(i, z);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo18415a(int i, int i2) {
        return this.f11479b.getColor(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public android.content.res.ColorStateList mo18416a(int i) {
        if (this.f11479b.hasValue(i)) {
            int resourceId = this.f11479b.getResourceId(i, 0);
            if (resourceId != 0) {
                android.content.res.ColorStateList b = com.fossil.blesdk.obfuscated.C2364m0.m10496b(this.f11478a, resourceId);
                if (b != null) {
                    return b;
                }
            }
        }
        return this.f11479b.getColorStateList(i);
    }

    @DexIgnore
    /* renamed from: a */
    public float mo18414a(int i, float f) {
        return this.f11479b.getDimension(i, f);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18418a() {
        this.f11479b.recycle();
    }
}
