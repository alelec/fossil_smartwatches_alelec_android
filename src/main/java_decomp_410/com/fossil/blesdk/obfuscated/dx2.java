package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.MutableLiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dx2 extends ic {
    @DexIgnore
    public MutableLiveData<a> c; // = new MutableLiveData<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public a(int i, boolean z) {
            this.a = i;
            this.b = z;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (this.a == aVar.a) {
                        if (this.b == aVar.b) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            return i + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "NotificationSetting(type=" + this.a + ", isCall=" + this.b + ")";
        }
    }

    @DexIgnore
    public final MutableLiveData<a> c() {
        return this.c;
    }
}
