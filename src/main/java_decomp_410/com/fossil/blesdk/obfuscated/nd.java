package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.qd;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nd<Key, Value> {
    @DexIgnore
    public Key a;
    @DexIgnore
    public qd.f b;
    @DexIgnore
    public ld.b<Key, Value> c;
    @DexIgnore
    public qd.c d;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public Executor e; // = h3.b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends pb<qd<Value>> {
        @DexIgnore
        public qd<Value> g;
        @DexIgnore
        public ld<Key, Value> h;
        @DexIgnore
        public /* final */ ld.c i; // = new C0024a();
        @DexIgnore
        public /* final */ /* synthetic */ Object j;
        @DexIgnore
        public /* final */ /* synthetic */ ld.b k;
        @DexIgnore
        public /* final */ /* synthetic */ qd.f l;
        @DexIgnore
        public /* final */ /* synthetic */ Executor m;
        @DexIgnore
        public /* final */ /* synthetic */ Executor n;
        @DexIgnore
        public /* final */ /* synthetic */ qd.c o;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nd$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.nd$a$a  reason: collision with other inner class name */
        public class C0024a implements ld.c {
            @DexIgnore
            public C0024a() {
            }

            @DexIgnore
            public void a() {
                a.this.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Executor executor, Object obj, ld.b bVar, qd.f fVar, Executor executor2, Executor executor3, qd.c cVar) {
            super(executor);
            this.j = obj;
            this.k = bVar;
            this.l = fVar;
            this.m = executor2;
            this.n = executor3;
            this.o = cVar;
        }

        @DexIgnore
        public qd<Value> a() {
            Object obj = this.j;
            qd<Value> qdVar = this.g;
            if (qdVar != null) {
                obj = qdVar.e();
            }
            do {
                ld<Key, Value> ldVar = this.h;
                if (ldVar != null) {
                    ldVar.removeInvalidatedCallback(this.i);
                }
                this.h = this.k.create();
                this.h.addInvalidatedCallback(this.i);
                qd.d dVar = new qd.d(this.h, this.l);
                dVar.b(this.m);
                dVar.a(this.n);
                dVar.a(this.o);
                dVar.a(obj);
                this.g = dVar.a();
            } while (this.g.h());
            return this.g;
        }
    }

    @DexIgnore
    public nd(ld.b<Key, Value> bVar, qd.f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("PagedList.Config must be provided");
        } else if (bVar != null) {
            this.c = bVar;
            this.b = fVar;
        } else {
            throw new IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public LiveData<qd<Value>> a() {
        return a(this.a, this.b, this.d, this.c, h3.d(), this.e);
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public static <Key, Value> LiveData<qd<Value>> a(Key key, qd.f fVar, qd.c cVar, ld.b<Key, Value> bVar, Executor executor, Executor executor2) {
        return new a(executor2, key, bVar, fVar, executor, executor2, cVar).b();
    }
}
