package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.widget.FilterQueryProvider;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface l03 extends v52<k03> {
    @DexIgnore
    void H();

    @DexIgnore
    void a(Cursor cursor);

    @DexIgnore
    void a(ArrayList<ContactWrapper> arrayList);

    @DexIgnore
    void a(List<ContactWrapper> list, FilterQueryProvider filterQueryProvider, int i);

    @DexIgnore
    void x0();
}
