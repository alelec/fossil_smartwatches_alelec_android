package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.hv3;
import java.io.IOException;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nw3 implements qu3 {
    @DexIgnore
    public static /* final */ qu3 a; // = new nw3();

    @DexIgnore
    public hv3 a(Proxy proxy, jv3 jv3) throws IOException {
        List<vu3> d = jv3.d();
        hv3 l = jv3.l();
        dv3 d2 = l.d();
        int size = d.size();
        for (int i = 0; i < size; i++) {
            vu3 vu3 = d.get(i);
            if ("Basic".equalsIgnoreCase(vu3.b())) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) proxy.address();
                PasswordAuthentication requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(inetSocketAddress.getHostName(), a(proxy, d2), inetSocketAddress.getPort(), d2.j(), vu3.a(), vu3.b(), d2.l(), Authenticator.RequestorType.PROXY);
                if (requestPasswordAuthentication != null) {
                    String a2 = zu3.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()));
                    hv3.b g = l.g();
                    g.b("Proxy-Authorization", a2);
                    return g.a();
                }
            }
        }
        return null;
    }

    @DexIgnore
    public hv3 b(Proxy proxy, jv3 jv3) throws IOException {
        List<vu3> d = jv3.d();
        hv3 l = jv3.l();
        dv3 d2 = l.d();
        int size = d.size();
        for (int i = 0; i < size; i++) {
            vu3 vu3 = d.get(i);
            if (!"Basic".equalsIgnoreCase(vu3.b())) {
                Proxy proxy2 = proxy;
            } else {
                PasswordAuthentication requestPasswordAuthentication = Authenticator.requestPasswordAuthentication(d2.f(), a(proxy, d2), d2.h(), d2.j(), vu3.a(), vu3.b(), d2.l(), Authenticator.RequestorType.SERVER);
                if (requestPasswordAuthentication != null) {
                    String a2 = zu3.a(requestPasswordAuthentication.getUserName(), new String(requestPasswordAuthentication.getPassword()));
                    hv3.b g = l.g();
                    g.b("Authorization", a2);
                    return g.a();
                }
            }
        }
        return null;
    }

    @DexIgnore
    public final InetAddress a(Proxy proxy, dv3 dv3) throws IOException {
        if (proxy == null || proxy.type() == Proxy.Type.DIRECT) {
            return InetAddress.getByName(dv3.f());
        }
        return ((InetSocketAddress) proxy.address()).getAddress();
    }
}
