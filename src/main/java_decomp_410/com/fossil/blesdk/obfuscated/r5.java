package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r5 {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968743;
    @DexIgnore
    public static /* final */ int font; // = 2130969182;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969187;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969188;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969189;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969190;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969191;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969192;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969193;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969194;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969195;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969834;
}
