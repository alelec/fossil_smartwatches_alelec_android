package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l22 implements m22 {
    @DexIgnore
    public int a() {
        return 4;
    }

    @DexIgnore
    public void a(n22 n22) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!n22.i()) {
                break;
            }
            a(n22.c(), sb);
            n22.f++;
            if (sb.length() >= 4) {
                n22.a(a((CharSequence) sb, 0));
                sb.delete(0, 4);
                if (p22.a(n22.d(), n22.f, a()) != a()) {
                    n22.b(0);
                    break;
                }
            }
        }
        sb.append(31);
        a(n22, (CharSequence) sb);
    }

    @DexIgnore
    public static void a(n22 n22, CharSequence charSequence) {
        try {
            int length = charSequence.length();
            if (length != 0) {
                boolean z = true;
                if (length == 1) {
                    n22.l();
                    int a = n22.g().a() - n22.a();
                    if (n22.f() == 0 && a <= 2) {
                        n22.b(0);
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String a2 = a(charSequence, 0);
                    if (!(!n22.i()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        n22.c(n22.a() + i);
                        if (n22.g().a() - n22.a() >= 3) {
                            n22.c(n22.a() + a2.length());
                            z = false;
                        }
                    }
                    if (z) {
                        n22.k();
                        n22.f -= i;
                    } else {
                        n22.a(a2);
                    }
                    n22.b(0);
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            n22.b(0);
        }
    }

    @DexIgnore
    public static void a(char c, StringBuilder sb) {
        if (c >= ' ' && c <= '?') {
            sb.append(c);
        } else if (c < '@' || c > '^') {
            p22.a(c);
            throw null;
        } else {
            sb.append((char) (c - '@'));
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int length = charSequence.length() - i;
        if (length != 0) {
            char charAt = charSequence.charAt(i);
            char c = 0;
            char charAt2 = length >= 2 ? charSequence.charAt(i + 1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(i + 2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(i + 3);
            }
            int i2 = (charAt << 18) + (charAt2 << 12) + (charAt3 << 6) + c;
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }
}
