package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ne */
public final class C2482ne implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.lang.ThreadLocal<com.fossil.blesdk.obfuscated.C2482ne> f7735i; // = new java.lang.ThreadLocal<>();

    @DexIgnore
    /* renamed from: j */
    public static java.util.Comparator<com.fossil.blesdk.obfuscated.C2482ne.C2485c> f7736j; // = new com.fossil.blesdk.obfuscated.C2482ne.C2483a();

    @DexIgnore
    /* renamed from: e */
    public java.util.ArrayList<androidx.recyclerview.widget.RecyclerView> f7737e; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: f */
    public long f7738f;

    @DexIgnore
    /* renamed from: g */
    public long f7739g;

    @DexIgnore
    /* renamed from: h */
    public java.util.ArrayList<com.fossil.blesdk.obfuscated.C2482ne.C2485c> f7740h; // = new java.util.ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ne$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ne$a */
    public static class C2483a implements java.util.Comparator<com.fossil.blesdk.obfuscated.C2482ne.C2485c> {
        @DexIgnore
        /* renamed from: a */
        public int compare(com.fossil.blesdk.obfuscated.C2482ne.C2485c cVar, com.fossil.blesdk.obfuscated.C2482ne.C2485c cVar2) {
            if ((cVar.f7748d == null) == (cVar2.f7748d == null)) {
                boolean z = cVar.f7745a;
                if (z == cVar2.f7745a) {
                    int i = cVar2.f7746b - cVar.f7746b;
                    if (i != 0) {
                        return i;
                    }
                    int i2 = cVar.f7747c - cVar2.f7747c;
                    if (i2 != 0) {
                        return i2;
                    }
                    return 0;
                } else if (z) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (cVar.f7748d == null) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ne$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ne$b */
    public static class C2484b implements androidx.recyclerview.widget.RecyclerView.C0236m.C0239c {

        @DexIgnore
        /* renamed from: a */
        public int f7741a;

        @DexIgnore
        /* renamed from: b */
        public int f7742b;

        @DexIgnore
        /* renamed from: c */
        public int[] f7743c;

        @DexIgnore
        /* renamed from: d */
        public int f7744d;

        @DexIgnore
        /* renamed from: a */
        public void mo13943a(androidx.recyclerview.widget.RecyclerView recyclerView, boolean z) {
            this.f7744d = 0;
            int[] iArr = this.f7743c;
            if (iArr != null) {
                java.util.Arrays.fill(iArr, -1);
            }
            androidx.recyclerview.widget.RecyclerView.C0236m mVar = recyclerView.f1324q;
            if (recyclerView.f1322p != null && mVar != null && mVar.mo2975w()) {
                if (z) {
                    if (!recyclerView.f1306h.mo10866c()) {
                        mVar.mo2415a(recyclerView.f1322p.getItemCount(), (androidx.recyclerview.widget.RecyclerView.C0236m.C0239c) this);
                    }
                } else if (!recyclerView.mo2643p()) {
                    mVar.mo2413a(this.f7741a, this.f7742b, recyclerView.f1315l0, (androidx.recyclerview.widget.RecyclerView.C0236m.C0239c) this);
                }
                int i = this.f7744d;
                if (i > mVar.f1394m) {
                    mVar.f1394m = i;
                    mVar.f1395n = z;
                    recyclerView.f1302f.mo2736j();
                }
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo13945b(int i, int i2) {
            this.f7741a = i;
            this.f7742b = i2;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo2983a(int i, int i2) {
            if (i < 0) {
                throw new java.lang.IllegalArgumentException("Layout positions must be non-negative");
            } else if (i2 >= 0) {
                int i3 = this.f7744d * 2;
                int[] iArr = this.f7743c;
                if (iArr == null) {
                    this.f7743c = new int[4];
                    java.util.Arrays.fill(this.f7743c, -1);
                } else if (i3 >= iArr.length) {
                    this.f7743c = new int[(i3 * 2)];
                    java.lang.System.arraycopy(iArr, 0, this.f7743c, 0, iArr.length);
                }
                int[] iArr2 = this.f7743c;
                iArr2[i3] = i;
                iArr2[i3 + 1] = i2;
                this.f7744d++;
            } else {
                throw new java.lang.IllegalArgumentException("Pixel distance must be non-negative");
            }
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo13944a(int i) {
            if (this.f7743c != null) {
                int i2 = this.f7744d * 2;
                for (int i3 = 0; i3 < i2; i3 += 2) {
                    if (this.f7743c[i3] == i) {
                        return true;
                    }
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13942a() {
            int[] iArr = this.f7743c;
            if (iArr != null) {
                java.util.Arrays.fill(iArr, -1);
            }
            this.f7744d = 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ne$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ne$c */
    public static class C2485c {

        @DexIgnore
        /* renamed from: a */
        public boolean f7745a;

        @DexIgnore
        /* renamed from: b */
        public int f7746b;

        @DexIgnore
        /* renamed from: c */
        public int f7747c;

        @DexIgnore
        /* renamed from: d */
        public androidx.recyclerview.widget.RecyclerView f7748d;

        @DexIgnore
        /* renamed from: e */
        public int f7749e;

        @DexIgnore
        /* renamed from: a */
        public void mo13946a() {
            this.f7745a = false;
            this.f7746b = 0;
            this.f7747c = 0;
            this.f7748d = null;
            this.f7749e = 0;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13933a(androidx.recyclerview.widget.RecyclerView recyclerView) {
        this.f7737e.add(recyclerView);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo13938b(androidx.recyclerview.widget.RecyclerView recyclerView) {
        this.f7737e.remove(recyclerView);
    }

    @DexIgnore
    public void run() {
        try {
            com.fossil.blesdk.obfuscated.C2864s7.m13552a("RV Prefetch");
            if (!this.f7737e.isEmpty()) {
                int size = this.f7737e.size();
                long j = 0;
                for (int i = 0; i < size; i++) {
                    androidx.recyclerview.widget.RecyclerView recyclerView = this.f7737e.get(i);
                    if (recyclerView.getWindowVisibility() == 0) {
                        j = java.lang.Math.max(recyclerView.getDrawingTime(), j);
                    }
                }
                if (j != 0) {
                    mo13937b(java.util.concurrent.TimeUnit.MILLISECONDS.toNanos(j) + this.f7739g);
                    this.f7738f = 0;
                    com.fossil.blesdk.obfuscated.C2864s7.m13551a();
                }
            }
        } finally {
            this.f7738f = 0;
            com.fossil.blesdk.obfuscated.C2864s7.m13551a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13934a(androidx.recyclerview.widget.RecyclerView recyclerView, int i, int i2) {
        if (recyclerView.isAttachedToWindow() && this.f7738f == 0) {
            this.f7738f = recyclerView.getNanoTime();
            recyclerView.post(this);
        }
        recyclerView.f1313k0.mo13945b(i, i2);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo13937b(long j) {
        mo13931a();
        mo13932a(j);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13931a() {
        com.fossil.blesdk.obfuscated.C2482ne.C2485c cVar;
        int size = this.f7737e.size();
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            androidx.recyclerview.widget.RecyclerView recyclerView = this.f7737e.get(i2);
            if (recyclerView.getWindowVisibility() == 0) {
                recyclerView.f1313k0.mo13943a(recyclerView, false);
                i += recyclerView.f1313k0.f7744d;
            }
        }
        this.f7740h.ensureCapacity(i);
        int i3 = 0;
        for (int i4 = 0; i4 < size; i4++) {
            androidx.recyclerview.widget.RecyclerView recyclerView2 = this.f7737e.get(i4);
            if (recyclerView2.getWindowVisibility() == 0) {
                com.fossil.blesdk.obfuscated.C2482ne.C2484b bVar = recyclerView2.f1313k0;
                int abs = java.lang.Math.abs(bVar.f7741a) + java.lang.Math.abs(bVar.f7742b);
                int i5 = i3;
                for (int i6 = 0; i6 < bVar.f7744d * 2; i6 += 2) {
                    if (i5 >= this.f7740h.size()) {
                        cVar = new com.fossil.blesdk.obfuscated.C2482ne.C2485c();
                        this.f7740h.add(cVar);
                    } else {
                        cVar = this.f7740h.get(i5);
                    }
                    int i7 = bVar.f7743c[i6 + 1];
                    cVar.f7745a = i7 <= abs;
                    cVar.f7746b = abs;
                    cVar.f7747c = i7;
                    cVar.f7748d = recyclerView2;
                    cVar.f7749e = bVar.f7743c[i6];
                    i5++;
                }
                i3 = i5;
            }
        }
        java.util.Collections.sort(this.f7740h, f7736j);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m11202a(androidx.recyclerview.widget.RecyclerView recyclerView, int i) {
        int b = recyclerView.f1308i.mo12315b();
        for (int i2 = 0; i2 < b; i2++) {
            androidx.recyclerview.widget.RecyclerView.ViewHolder n = androidx.recyclerview.widget.RecyclerView.m1418n(recyclerView.f1308i.mo12323e(i2));
            if (n.mPosition == i && !n.isInvalid()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.recyclerview.widget.RecyclerView.ViewHolder mo13930a(androidx.recyclerview.widget.RecyclerView recyclerView, int i, long j) {
        if (m11202a(recyclerView, i)) {
            return null;
        }
        androidx.recyclerview.widget.RecyclerView.Recycler recycler = recyclerView.f1302f;
        try {
            recyclerView.mo2680z();
            androidx.recyclerview.widget.RecyclerView.ViewHolder a = recycler.mo2700a(i, false, j);
            if (a != null) {
                if (!a.isBound() || a.isInvalid()) {
                    recycler.mo2708a(a, false);
                } else {
                    recycler.mo2717b(a.itemView);
                }
            }
            return a;
        } finally {
            recyclerView.mo2528a(false);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13935a(androidx.recyclerview.widget.RecyclerView recyclerView, long j) {
        if (recyclerView != null) {
            if (recyclerView.f1280H && recyclerView.f1308i.mo12315b() != 0) {
                recyclerView.mo2490G();
            }
            com.fossil.blesdk.obfuscated.C2482ne.C2484b bVar = recyclerView.f1313k0;
            bVar.mo13943a(recyclerView, true);
            if (bVar.f7744d != 0) {
                try {
                    com.fossil.blesdk.obfuscated.C2864s7.m13552a("RV Nested Prefetch");
                    recyclerView.f1315l0.mo2739a(recyclerView.f1322p);
                    for (int i = 0; i < bVar.f7744d * 2; i += 2) {
                        mo13930a(recyclerView, bVar.f7743c[i], j);
                    }
                } finally {
                    com.fossil.blesdk.obfuscated.C2864s7.m13551a();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13936a(com.fossil.blesdk.obfuscated.C2482ne.C2485c cVar, long j) {
        androidx.recyclerview.widget.RecyclerView.ViewHolder a = mo13930a(cVar.f7748d, cVar.f7749e, cVar.f7745a ? com.misfit.frameworks.buttonservice.ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD : j);
        if (a != null && a.mNestedRecyclerView != null && a.isBound() && !a.isInvalid()) {
            mo13935a((androidx.recyclerview.widget.RecyclerView) a.mNestedRecyclerView.get(), j);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13932a(long j) {
        int i = 0;
        while (i < this.f7740h.size()) {
            com.fossil.blesdk.obfuscated.C2482ne.C2485c cVar = this.f7740h.get(i);
            if (cVar.f7748d != null) {
                mo13936a(cVar, j);
                cVar.mo13946a();
                i++;
            } else {
                return;
            }
        }
    }
}
