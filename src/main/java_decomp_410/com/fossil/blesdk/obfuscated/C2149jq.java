package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jq */
public interface C2149jq {
    @DexIgnore
    /* renamed from: a */
    android.graphics.Bitmap mo12443a(int i, int i2, android.graphics.Bitmap.Config config);

    @DexIgnore
    /* renamed from: a */
    void mo12444a();

    @DexIgnore
    /* renamed from: a */
    void mo12445a(int i);

    @DexIgnore
    /* renamed from: a */
    void mo12446a(android.graphics.Bitmap bitmap);

    @DexIgnore
    /* renamed from: b */
    android.graphics.Bitmap mo12447b(int i, int i2, android.graphics.Bitmap.Config config);
}
