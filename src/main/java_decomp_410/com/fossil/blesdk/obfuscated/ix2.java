package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ix2 extends xs3 implements hx2 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a((fd4) null);
    @DexIgnore
    public /* final */ pa m; // = new v62(this);
    @DexIgnore
    public tr3<ge2> n;
    @DexIgnore
    public gx2 o;
    @DexIgnore
    public j42 p;
    @DexIgnore
    public dx2 q;
    @DexIgnore
    public HashMap r;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ix2.s;
        }

        @DexIgnore
        public final ix2 b() {
            return new ix2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ix2 e;

        @DexIgnore
        public b(ix2 ix2) {
            this.e = ix2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ix2.a(this.e).a(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ix2 e;

        @DexIgnore
        public c(ix2 ix2) {
            this.e = ix2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ix2.a(this.e).a(1);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ix2 e;

        @DexIgnore
        public d(ix2 ix2) {
            this.e = ix2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ix2.a(this.e).a(2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ix2 e;

        @DexIgnore
        public e(ix2 ix2) {
            this.e = ix2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.dismissAllowingStateLoss();
        }
    }

    /*
    static {
        String simpleName = ix2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationSettingsType\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gx2 a(ix2 ix2) {
        gx2 gx2 = ix2.o;
        if (gx2 != null) {
            return gx2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void P0() {
        tr3<ge2> tr3 = this.n;
        if (tr3 != null) {
            ge2 a2 = tr3.a();
            if (a2 != null) {
                ImageView imageView = a2.v;
                kd4.a((Object) imageView, "it.ivEveryoneCheck");
                imageView.setVisibility(4);
                ImageView imageView2 = a2.w;
                kd4.a((Object) imageView2, "it.ivFavoriteContactsCheck");
                imageView2.setVisibility(4);
                ImageView imageView3 = a2.x;
                kd4.a((Object) imageView3, "it.ivNoOneCheck");
                imageView3.setVisibility(4);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d(String str) {
        kd4.b(str, "title");
        tr3<ge2> tr3 = this.n;
        if (tr3 != null) {
            ge2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void m(int i) {
        P0();
        tr3<ge2> tr3 = this.n;
        if (tr3 != null) {
            ge2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (i == 0) {
                ImageView imageView = a2.v;
                kd4.a((Object) imageView, "it.ivEveryoneCheck");
                imageView.setVisibility(0);
            } else if (i == 1) {
                ImageView imageView2 = a2.w;
                kd4.a((Object) imageView2, "it.ivFavoriteContactsCheck");
                imageView2.setVisibility(0);
            } else if (i == 2) {
                ImageView imageView3 = a2.x;
                kd4.a((Object) imageView3, "it.ivNoOneCheck");
                imageView3.setVisibility(0);
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.W.c().g().a(new kx2(this)).a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity = (NotificationCallsAndMessagesActivity) activity;
            j42 j42 = this.p;
            if (j42 != null) {
                ic a2 = lc.a((FragmentActivity) notificationCallsAndMessagesActivity, (kc.b) j42).a(dx2.class);
                kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ingViewModel::class.java)");
                this.q = (dx2) a2;
                gx2 gx2 = this.o;
                if (gx2 != null) {
                    dx2 dx2 = this.q;
                    if (dx2 != null) {
                        gx2.a(dx2);
                    } else {
                        kd4.d("mNotificationSettingViewModel");
                        throw null;
                    }
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else {
                kd4.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity");
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ge2 ge2 = (ge2) qa.a(layoutInflater, R.layout.fragment_notification_settings_type, viewGroup, false, this.m);
        ge2.q.setOnClickListener(new b(this));
        ge2.r.setOnClickListener(new c(this));
        ge2.s.setOnClickListener(new d(this));
        ge2.u.setOnClickListener(new e(this));
        this.n = new tr3<>(this, ge2);
        kd4.a((Object) ge2, "binding");
        return ge2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        gx2 gx2 = this.o;
        if (gx2 != null) {
            gx2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        gx2 gx2 = this.o;
        if (gx2 != null) {
            gx2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(gx2 gx2) {
        kd4.b(gx2, "presenter");
        this.o = gx2;
    }
}
