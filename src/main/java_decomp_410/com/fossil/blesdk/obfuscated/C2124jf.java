package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jf */
public class C2124jf {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1945hg.C1949c f6410a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.Context f6411b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f6412c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ androidx.room.RoomDatabase.C0274c f6413d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<androidx.room.RoomDatabase.C0273b> f6414e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ boolean f6415f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ androidx.room.RoomDatabase.JournalMode f6416g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ java.util.concurrent.Executor f6417h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ java.util.concurrent.Executor f6418i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ boolean f6419j;

    @DexIgnore
    /* renamed from: k */
    public /* final */ boolean f6420k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ boolean f6421l;

    @DexIgnore
    /* renamed from: m */
    public /* final */ java.util.Set<java.lang.Integer> f6422m;

    @DexIgnore
    public C2124jf(android.content.Context context, java.lang.String str, com.fossil.blesdk.obfuscated.C1945hg.C1949c cVar, androidx.room.RoomDatabase.C0274c cVar2, java.util.List<androidx.room.RoomDatabase.C0273b> list, boolean z, androidx.room.RoomDatabase.JournalMode journalMode, java.util.concurrent.Executor executor, java.util.concurrent.Executor executor2, boolean z2, boolean z3, boolean z4, java.util.Set<java.lang.Integer> set) {
        this.f6410a = cVar;
        this.f6411b = context;
        this.f6412c = str;
        this.f6413d = cVar2;
        this.f6414e = list;
        this.f6415f = z;
        this.f6416g = journalMode;
        this.f6417h = executor;
        this.f6418i = executor2;
        this.f6419j = z2;
        this.f6420k = z3;
        this.f6421l = z4;
        this.f6422m = set;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12340a(int i, int i2) {
        if ((!(i > i2) || !this.f6421l) && this.f6420k) {
            java.util.Set<java.lang.Integer> set = this.f6422m;
            if (set == null || !set.contains(java.lang.Integer.valueOf(i))) {
                return true;
            }
        }
        return false;
    }
}
