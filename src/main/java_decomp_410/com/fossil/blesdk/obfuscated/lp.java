package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.tq;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lp<DataType> implements tq.b {
    @DexIgnore
    public /* final */ ho<DataType> a;
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ lo c;

    @DexIgnore
    public lp(ho<DataType> hoVar, DataType datatype, lo loVar) {
        this.a = hoVar;
        this.b = datatype;
        this.c = loVar;
    }

    @DexIgnore
    public boolean a(File file) {
        return this.a.a(this.b, file, this.c);
    }
}
