package com.fossil.blesdk.obfuscated;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface tq {

    @DexIgnore
    public interface a {
        @DexIgnore
        tq build();
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(File file);
    }

    @DexIgnore
    File a(jo joVar);

    @DexIgnore
    void a(jo joVar, b bVar);
}
