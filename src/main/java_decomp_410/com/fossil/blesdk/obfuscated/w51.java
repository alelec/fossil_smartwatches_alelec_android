package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w51 extends vb1<w51> {
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Boolean e; // = null;
    @DexIgnore
    public String[] f; // = dc1.c;

    @DexIgnore
    public w51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            ub1.a(2, str);
        }
        Boolean bool = this.e;
        if (bool != null) {
            ub1.a(3, bool.booleanValue());
        }
        String[] strArr = this.f;
        if (strArr != null && strArr.length > 0) {
            int i = 0;
            while (true) {
                String[] strArr2 = this.f;
                if (i >= strArr2.length) {
                    break;
                }
                String str2 = strArr2[i];
                if (str2 != null) {
                    ub1.a(4, str2);
                }
                i++;
            }
        }
        super.a(ub1);
    }

    @DexIgnore
    public final w51 b(tb1 tb1) throws IOException {
        int e2;
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                try {
                    e2 = tb1.e();
                    if (e2 < 0 || e2 > 6) {
                        StringBuilder sb = new StringBuilder(41);
                        sb.append(e2);
                        sb.append(" is not a valid enum MatchType");
                    } else {
                        this.c = Integer.valueOf(e2);
                    }
                } catch (IllegalArgumentException unused) {
                    tb1.f(tb1.a());
                    a(tb1, c2);
                }
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 24) {
                this.e = Boolean.valueOf(tb1.d());
            } else if (c2 == 34) {
                int a = dc1.a(tb1, 34);
                String[] strArr = this.f;
                int length = strArr == null ? 0 : strArr.length;
                String[] strArr2 = new String[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.f, 0, strArr2, 0, length);
                }
                while (length < strArr2.length - 1) {
                    strArr2[length] = tb1.b();
                    tb1.c();
                    length++;
                }
                strArr2[length] = tb1.b();
                this.f = strArr2;
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
        StringBuilder sb2 = new StringBuilder(41);
        sb2.append(e2);
        sb2.append(" is not a valid enum MatchType");
        throw new IllegalArgumentException(sb2.toString());
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof w51)) {
            return false;
        }
        w51 w51 = (w51) obj;
        Integer num = this.c;
        if (num == null) {
            if (w51.c != null) {
                return false;
            }
        } else if (!num.equals(w51.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (w51.d != null) {
                return false;
            }
        } else if (!str.equals(w51.d)) {
            return false;
        }
        Boolean bool = this.e;
        if (bool == null) {
            if (w51.e != null) {
                return false;
            }
        } else if (!bool.equals(w51.e)) {
            return false;
        }
        if (!zb1.a((Object[]) this.f, (Object[]) w51.f)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(w51.b);
        }
        xb1 xb12 = w51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (w51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i = 0;
        int intValue = (hashCode + (num == null ? 0 : num.intValue())) * 31;
        String str = this.d;
        int hashCode2 = (intValue + (str == null ? 0 : str.hashCode())) * 31;
        Boolean bool = this.e;
        int hashCode3 = (((hashCode2 + (bool == null ? 0 : bool.hashCode())) * 31) + zb1.a((Object[]) this.f)) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i = this.b.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            a += ub1.b(2, str);
        }
        Boolean bool = this.e;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(3) + 1;
        }
        String[] strArr = this.f;
        if (strArr == null || strArr.length <= 0) {
            return a;
        }
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            String[] strArr2 = this.f;
            if (i >= strArr2.length) {
                return a + i2 + (i3 * 1);
            }
            String str2 = strArr2[i];
            if (str2 != null) {
                i3++;
                i2 += ub1.a(str2);
            }
            i++;
        }
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        b(tb1);
        return this;
    }
}
