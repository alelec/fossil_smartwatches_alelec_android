package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ze0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class od1 extends hf0<f41, qc1> {
    @DexIgnore
    public /* final */ /* synthetic */ oc1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public od1(oc1 oc1, ze0.a aVar) {
        super(aVar);
        this.b = oc1;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar, xn1 xn1) throws RemoteException {
        try {
            ((f41) bVar).b(a(), this.b.a((xn1<Boolean>) xn1));
        } catch (RuntimeException e) {
            xn1.b((Exception) e);
        }
    }
}
