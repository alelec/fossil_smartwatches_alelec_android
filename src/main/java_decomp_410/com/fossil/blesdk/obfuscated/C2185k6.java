package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k6 */
public class C2185k6 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.Object f6714a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public static android.util.TypedValue f6715b;

    @DexIgnore
    /* renamed from: a */
    public static boolean m9353a(android.content.Context context, android.content.Intent[] intentArr, android.os.Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            context.startActivities(intentArr, bundle);
            return true;
        }
        context.startActivities(intentArr);
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.io.File[] m9357b(android.content.Context context, java.lang.String str) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return context.getExternalFilesDirs(str);
        }
        return new java.io.File[]{context.getExternalFilesDir(str)};
    }

    @DexIgnore
    /* renamed from: c */
    public static android.graphics.drawable.Drawable m9358c(android.content.Context context, int i) {
        int i2;
        int i3 = android.os.Build.VERSION.SDK_INT;
        if (i3 >= 21) {
            return context.getDrawable(i);
        }
        if (i3 >= 16) {
            return context.getResources().getDrawable(i);
        }
        synchronized (f6714a) {
            if (f6715b == null) {
                f6715b = new android.util.TypedValue();
            }
            context.getResources().getValue(i, f6715b, true);
            i2 = f6715b.resourceId;
        }
        return context.getResources().getDrawable(i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m9352a(android.content.Context context, android.content.Intent intent, android.os.Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            context.startActivity(intent, bundle);
        } else {
            context.startActivity(intent);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.res.ColorStateList m9355b(android.content.Context context, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        return context.getResources().getColorStateList(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.io.File[] m9354a(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return context.getExternalCacheDirs();
        }
        return new java.io.File[]{context.getExternalCacheDir()};
    }

    @DexIgnore
    /* renamed from: b */
    public static java.io.File m9356b(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return context.getNoBackupFilesDir();
        }
        return m9350a(new java.io.File(context.getApplicationInfo().dataDir, "no_backup"));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m9348a(android.content.Context context, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return context.getColor(i);
        }
        return context.getResources().getColor(i);
    }

    @DexIgnore
    /* renamed from: c */
    public static boolean m9359c(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return context.isDeviceProtectedStorage();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m9349a(android.content.Context context, java.lang.String str) {
        if (str != null) {
            return context.checkPermission(str, android.os.Process.myPid(), android.os.Process.myUid());
        }
        throw new java.lang.IllegalArgumentException("permission is null");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0035, code lost:
        return r4;
     */
    @DexIgnore
    /* renamed from: a */
    public static synchronized java.io.File m9350a(java.io.File file) {
        synchronized (com.fossil.blesdk.obfuscated.C2185k6.class) {
            if (!file.exists() && !file.mkdirs()) {
                if (file.exists()) {
                    return file;
                }
                android.util.Log.w("ContextCompat", "Unable to create files subdir " + file.getPath());
                return null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m9351a(android.content.Context context, android.content.Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }
}
