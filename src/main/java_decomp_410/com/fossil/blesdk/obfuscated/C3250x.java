package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x */
public final class C3250x {
    @DexIgnore
    public static /* final */ int abc_action_bar_title_item; // = 2131558400;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_container; // = 2131558401;
    @DexIgnore
    public static /* final */ int abc_action_menu_item_layout; // = 2131558402;
    @DexIgnore
    public static /* final */ int abc_action_menu_layout; // = 2131558403;
    @DexIgnore
    public static /* final */ int abc_action_mode_bar; // = 2131558404;
    @DexIgnore
    public static /* final */ int abc_action_mode_close_item_material; // = 2131558405;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view; // = 2131558406;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_list_item; // = 2131558407;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_bar_material; // = 2131558408;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_material; // = 2131558409;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_title_material; // = 2131558410;
    @DexIgnore
    public static /* final */ int abc_cascading_menu_item_layout; // = 2131558411;
    @DexIgnore
    public static /* final */ int abc_dialog_title_material; // = 2131558412;
    @DexIgnore
    public static /* final */ int abc_expanded_menu_layout; // = 2131558413;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_checkbox; // = 2131558414;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_icon; // = 2131558415;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_layout; // = 2131558416;
    @DexIgnore
    public static /* final */ int abc_list_menu_item_radio; // = 2131558417;
    @DexIgnore
    public static /* final */ int abc_popup_menu_header_item_layout; // = 2131558418;
    @DexIgnore
    public static /* final */ int abc_popup_menu_item_layout; // = 2131558419;
    @DexIgnore
    public static /* final */ int abc_screen_content_include; // = 2131558420;
    @DexIgnore
    public static /* final */ int abc_screen_simple; // = 2131558421;
    @DexIgnore
    public static /* final */ int abc_screen_simple_overlay_action_mode; // = 2131558422;
    @DexIgnore
    public static /* final */ int abc_screen_toolbar; // = 2131558423;
    @DexIgnore
    public static /* final */ int abc_search_dropdown_item_icons_2line; // = 2131558424;
    @DexIgnore
    public static /* final */ int abc_search_view; // = 2131558425;
    @DexIgnore
    public static /* final */ int abc_select_dialog_material; // = 2131558426;
    @DexIgnore
    public static /* final */ int abc_tooltip; // = 2131558427;
    @DexIgnore
    public static /* final */ int notification_action; // = 2131558670;
    @DexIgnore
    public static /* final */ int notification_action_tombstone; // = 2131558671;
    @DexIgnore
    public static /* final */ int notification_template_custom_big; // = 2131558679;
    @DexIgnore
    public static /* final */ int notification_template_icon_group; // = 2131558680;
    @DexIgnore
    public static /* final */ int notification_template_part_chronometer; // = 2131558684;
    @DexIgnore
    public static /* final */ int notification_template_part_time; // = 2131558685;
    @DexIgnore
    public static /* final */ int select_dialog_item_material; // = 2131558708;
    @DexIgnore
    public static /* final */ int select_dialog_multichoice_material; // = 2131558709;
    @DexIgnore
    public static /* final */ int select_dialog_singlechoice_material; // = 2131558710;
    @DexIgnore
    public static /* final */ int support_simple_spinner_dropdown_item; // = 2131558712;
}
