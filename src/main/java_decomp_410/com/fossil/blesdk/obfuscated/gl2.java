package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gl2 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = cb4.a((T[]) new String[]{MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue()});
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = cb4.a((T[]) new String[]{MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue()});
    @DexIgnore
    public static /* final */ gl2 c; // = new gl2();

    @DexIgnore
    public final String a(String str) {
        kd4.b(str, "microAppId");
        if (kd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_SecondTimezoneSelectCity_Title__ChooseACity);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
            return a2;
        } else if (kd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DetailsCommuteTime_CTA__SetDestination);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        } else if (kd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.getValue())) {
            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_DianaProfile_List__SetGoals);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026naProfile_List__SetGoals)");
            return a4;
        } else if (!kd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return "";
        } else {
            String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_HybridRingPhoneSelectRingtone_Title__RingPhone);
            kd4.a((Object) a5, "LanguageHelper.getString\u2026ingtone_Title__RingPhone)");
            return a5;
        }
    }

    @DexIgnore
    public final List<String> b(String str) {
        kd4.b(str, "microAppId");
        if (kd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            int i = Build.VERSION.SDK_INT;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppHelper", "android.os.Build.VERSION.SDK_INT=" + i);
            if (i >= 29) {
                return cb4.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
            }
            return cb4.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
        } else if (kd4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return cb4.a((T[]) new String[]{InAppPermission.NOTIFICATION_ACCESS});
        } else {
            return new ArrayList();
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        kd4.b(str, "microAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        kd4.b(str, "microAppId");
        return a.contains(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        kd4.b(str, "microAppId");
        List<String> b2 = b(str);
        String[] a2 = ns3.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppHelper", "isPermissionGrantedForMicroApp " + str + " granted=" + a2 + " required=" + b2);
        for (String b3 : b2) {
            if (!za4.b((T[]) a2, b3)) {
                return false;
            }
        }
        return true;
    }
}
