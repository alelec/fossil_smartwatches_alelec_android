package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yx1 extends gz0 {
    @DexIgnore
    public /* final */ /* synthetic */ xx1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yx1(xx1 xx1, Looper looper) {
        super(looper);
        this.a = xx1;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        this.a.a(message);
    }
}
