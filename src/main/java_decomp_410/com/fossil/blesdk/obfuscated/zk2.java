package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.so0;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zk2 {
    @DexIgnore
    public static /* final */ String c; // = "zk2";
    @DexIgnore
    public static ge0 d;
    @DexIgnore
    public static d e;
    @DexIgnore
    public en2 a;
    @DexIgnore
    public /* final */ Executor b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ge0.c {
        @DexIgnore
        public a(zk2 zk2) {
        }

        @DexIgnore
        public void a(ud0 ud0) {
            d dVar = zk2.e;
            if (dVar != null) {
                dVar.a(ud0);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ge0.b {
        @DexIgnore
        public b(zk2 zk2) {
        }

        @DexIgnore
        public void e(Bundle bundle) {
            d dVar = zk2.e;
            if (dVar != null) {
                dVar.a();
            }
        }

        @DexIgnore
        public void f(int i) {
            if (i == 2) {
                FLogger.INSTANCE.getLocal().d(zk2.c, "Connection lost.  Cause: Network Lost.");
            } else if (i == 1) {
                FLogger.INSTANCE.getLocal().d(zk2.c, "Connection lost.  Reason: Service Disconnected");
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            he0<Status> a = ro0.e.a(zk2.d);
            d dVar = zk2.e;
            if (dVar != null) {
                dVar.a(a);
            }
            zk2.this.a.h(false);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(he0<Status> he0);

        @DexIgnore
        void a(ud0 ud0);
    }

    @DexIgnore
    public zk2(Context context, Executor executor, en2 en2) {
        if (d == null) {
            ge0.a aVar = new ge0.a(context);
            aVar.a((de0<? extends de0.d.C0010d>) ro0.b, new Scope[0]);
            aVar.a((de0<? extends de0.d.C0010d>) ro0.a, new Scope[0]);
            aVar.a((de0<? extends de0.d.C0010d>) ro0.d, new Scope[0]);
            aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.body.write", "https://www.googleapis.com/auth/fitness.location.write"});
            aVar.a((ge0.b) new b(this));
            aVar.a((ge0.c) new a(this));
            d = aVar.a();
        }
        this.a = en2;
        if (f()) {
            a();
        }
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        d.c();
        this.a.h(true);
    }

    @DexIgnore
    public void b() {
        d.d();
        this.a.h(false);
    }

    @DexIgnore
    public final so0 c() {
        so0.a c2 = so0.c();
        c2.a(DataType.p, 1);
        return c2.a();
    }

    @DexIgnore
    public boolean d() {
        return vb0.a(vb0.a((Context) PortfolioApp.R), (yb0) c());
    }

    @DexIgnore
    public boolean e() {
        return d.g() && d();
    }

    @DexIgnore
    public boolean f() {
        return this.a.g();
    }

    @DexIgnore
    public void g() {
        this.b.execute(new c());
    }

    @DexIgnore
    public void h() {
        try {
            b();
            vb0.a((Context) PortfolioApp.R, new GoogleSignInOptions.a(GoogleSignInOptions.s).a()).j();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void i() {
        this.a.h(false);
    }

    @DexIgnore
    public void a(d dVar) {
        e = dVar;
    }

    @DexIgnore
    public void a(Fragment fragment) {
        vb0.a(fragment, 1, vb0.a((Context) PortfolioApp.R), (yb0) c());
    }
}
