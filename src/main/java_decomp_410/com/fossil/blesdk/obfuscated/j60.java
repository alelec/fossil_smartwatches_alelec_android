package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import java.nio.ByteBuffer;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j60 {
    @DexIgnore
    public short a; // = -1;
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ byte[] d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId f;

    @DexIgnore
    public j60(byte[] bArr, int i, GattCharacteristic.CharacteristicId characteristicId) {
        kd4.b(bArr, "data");
        kd4.b(characteristicId, "characteristicId");
        this.d = bArr;
        this.e = i;
        this.f = characteristicId;
        ByteBuffer wrap = ByteBuffer.wrap(this.d);
        kd4.a((Object) wrap, "ByteBuffer.wrap(data)");
        this.b = wrap;
        byte[] bArr2 = this.d;
        this.c = bArr2.length;
        ByteBuffer wrap2 = ByteBuffer.wrap(bArr2);
        kd4.a((Object) wrap2, "ByteBuffer.wrap(data)");
        this.b = wrap2;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId a() {
        return this.f;
    }

    @DexIgnore
    public final byte[] b() {
        return this.d;
    }

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public final int d() {
        return this.e - f();
    }

    @DexIgnore
    public final byte[] e() {
        int remaining = this.b.remaining();
        if (remaining <= 0) {
            return new byte[0];
        }
        this.a = (short) (this.a + 1);
        byte[] c2 = c();
        int min = Math.min(remaining + c2.length, this.e);
        byte[] copyOfRange = Arrays.copyOfRange(c2, 0, min);
        this.b.get(copyOfRange, c2.length, min - c2.length);
        kd4.a((Object) copyOfRange, "bytes");
        return copyOfRange;
    }

    @DexIgnore
    public abstract int f();

    @DexIgnore
    public final int g() {
        return Math.min((this.a + 1) * d(), this.c);
    }

    @DexIgnore
    public final short h() {
        return this.a;
    }

    @DexIgnore
    public final int i() {
        return this.c;
    }

    @DexIgnore
    public final boolean j() {
        return this.b.remaining() > 0;
    }
}
