package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qg1 extends SQLiteOpenHelper {
    @DexIgnore
    public /* final */ /* synthetic */ pg1 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qg1(pg1 pg1, Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        this.e = pg1;
    }

    @DexIgnore
    public final SQLiteDatabase getWritableDatabase() throws SQLiteException {
        try {
            return super.getWritableDatabase();
        } catch (SQLiteDatabaseLockedException e2) {
            throw e2;
        } catch (SQLiteException unused) {
            this.e.d().s().a("Opening the local database failed, dropping and recreating it");
            if (!this.e.getContext().getDatabasePath("google_app_measurement_local.db").delete()) {
                this.e.d().s().a("Failed to delete corrupted local db file", "google_app_measurement_local.db");
            }
            try {
                return super.getWritableDatabase();
            } catch (SQLiteException e3) {
                this.e.d().s().a("Failed to open local database. Events will bypass local storage", e3);
                return null;
            }
        }
    }

    @DexIgnore
    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        em1.a(this.e.d(), sQLiteDatabase);
    }

    @DexIgnore
    public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [java.lang.String[]] */
    /* JADX WARNING: type inference failed for: r0v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    public final void onOpen(SQLiteDatabase sQLiteDatabase) {
        if (Build.VERSION.SDK_INT < 15) {
            Object r0 = 0;
            try {
                Cursor rawQuery = sQLiteDatabase.rawQuery("PRAGMA journal_mode=memory", r0);
                rawQuery.moveToFirst();
                r0 = rawQuery;
            } finally {
                if (r0 != 0) {
                    r0.close();
                }
            }
        }
        em1.a(this.e.d(), sQLiteDatabase, GraphRequest.DEBUG_MESSAGES_KEY, "create table if not exists messages ( type INTEGER NOT NULL, entry BLOB NOT NULL)", "type,entry", (String[]) null);
    }

    @DexIgnore
    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
