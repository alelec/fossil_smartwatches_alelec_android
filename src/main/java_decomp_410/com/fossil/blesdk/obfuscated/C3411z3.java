package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z3 */
public class C3411z3 extends com.fossil.blesdk.obfuscated.C1461b4 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.z3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.z3$a */
    public class C3412a implements com.fossil.blesdk.obfuscated.C1768f4.C1769a {
        @DexIgnore
        public C3412a(com.fossil.blesdk.obfuscated.C3411z3 z3Var) {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9043a(android.graphics.Canvas canvas, android.graphics.RectF rectF, float f, android.graphics.Paint paint) {
            canvas.drawRoundRect(rectF, f, f, paint);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8511a() {
        com.fossil.blesdk.obfuscated.C1768f4.f5010r = new com.fossil.blesdk.obfuscated.C3411z3.C3412a(this);
    }
}
