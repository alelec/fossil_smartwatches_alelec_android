package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ug1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ int e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ Object h;
    @DexIgnore
    public /* final */ /* synthetic */ Object i;
    @DexIgnore
    public /* final */ /* synthetic */ tg1 j;

    @DexIgnore
    public ug1(tg1 tg1, int i2, String str, Object obj, Object obj2, Object obj3) {
        this.j = tg1;
        this.e = i2;
        this.f = str;
        this.g = obj;
        this.h = obj2;
        this.i = obj3;
    }

    @DexIgnore
    public final void run() {
        fh1 t = this.j.a.t();
        if (!t.m()) {
            this.j.a(6, "Persisted config not initialized. Not logging error/warn");
            return;
        }
        if (this.j.c == 0) {
            if (this.j.l().m()) {
                tg1 tg1 = this.j;
                tg1.b();
                char unused = tg1.c = 'C';
            } else {
                tg1 tg12 = this.j;
                tg12.b();
                char unused2 = tg12.c = 'c';
            }
        }
        if (this.j.d < 0) {
            tg1 tg13 = this.j;
            long unused3 = tg13.d = tg13.l().n();
        }
        char charAt = "01VDIWEA?".charAt(this.e);
        char a = this.j.c;
        long b = this.j.d;
        String a2 = tg1.a(true, this.f, this.g, this.h, this.i);
        StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 24);
        sb.append("2");
        sb.append(charAt);
        sb.append(a);
        sb.append(b);
        sb.append(":");
        sb.append(a2);
        String sb2 = sb.toString();
        if (sb2.length() > 1024) {
            sb2 = this.f.substring(0, 1024);
        }
        t.d.a(sb2, 1);
    }
}
