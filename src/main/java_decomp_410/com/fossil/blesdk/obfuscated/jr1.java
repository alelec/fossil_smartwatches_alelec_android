package com.fossil.blesdk.obfuscated;

import android.animation.TypeEvaluator;
import android.graphics.Matrix;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jr1 implements TypeEvaluator<Matrix> {
    @DexIgnore
    public /* final */ float[] a; // = new float[9];
    @DexIgnore
    public /* final */ float[] b; // = new float[9];
    @DexIgnore
    public /* final */ Matrix c; // = new Matrix();

    @DexIgnore
    /* renamed from: a */
    public Matrix evaluate(float f, Matrix matrix, Matrix matrix2) {
        matrix.getValues(this.a);
        matrix2.getValues(this.b);
        for (int i = 0; i < 9; i++) {
            float[] fArr = this.b;
            float f2 = fArr[i];
            float[] fArr2 = this.a;
            fArr[i] = fArr2[i] + ((f2 - fArr2[i]) * f);
        }
        this.c.setValues(this.b);
        return this.c;
    }
}
