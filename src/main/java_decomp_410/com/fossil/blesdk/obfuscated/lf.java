package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lf<T> extends wf {
    @DexIgnore
    public lf(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    @DexIgnore
    public abstract void bind(kg kgVar, T t);

    @DexIgnore
    public final void insert(T t) {
        kg acquire = acquire();
        try {
            bind(acquire, t);
            acquire.o();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long insertAndReturnId(T t) {
        kg acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.o();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(Collection<T> collection) {
        kg acquire = acquire();
        try {
            long[] jArr = new long[collection.size()];
            int i = 0;
            for (T bind : collection) {
                bind(acquire, bind);
                jArr[i] = acquire.o();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final Long[] insertAndReturnIdsArrayBox(Collection<T> collection) {
        kg acquire = acquire();
        try {
            Long[] lArr = new Long[collection.size()];
            int i = 0;
            for (T bind : collection) {
                bind(acquire, bind);
                lArr[i] = Long.valueOf(acquire.o());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final List<Long> insertAndReturnIdsList(T[] tArr) {
        kg acquire = acquire();
        try {
            ArrayList arrayList = new ArrayList(tArr.length);
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                arrayList.add(i, Long.valueOf(acquire.o()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(T[] tArr) {
        kg acquire = acquire();
        try {
            for (T bind : tArr) {
                bind(acquire, bind);
                acquire.o();
            }
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(T[] tArr) {
        kg acquire = acquire();
        try {
            long[] jArr = new long[tArr.length];
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                jArr[i] = acquire.o();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final Long[] insertAndReturnIdsArrayBox(T[] tArr) {
        kg acquire = acquire();
        try {
            Long[] lArr = new Long[tArr.length];
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                lArr[i] = Long.valueOf(acquire.o());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final List<Long> insertAndReturnIdsList(Collection<T> collection) {
        kg acquire = acquire();
        try {
            ArrayList arrayList = new ArrayList(collection.size());
            int i = 0;
            for (T bind : collection) {
                bind(acquire, bind);
                arrayList.add(i, Long.valueOf(acquire.o()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(Iterable<T> iterable) {
        kg acquire = acquire();
        try {
            for (T bind : iterable) {
                bind(acquire, bind);
                acquire.o();
            }
        } finally {
            release(acquire);
        }
    }
}
