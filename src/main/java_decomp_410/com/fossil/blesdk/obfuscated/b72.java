package com.fossil.blesdk.obfuscated;

import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b72 {
    @DexIgnore
    public final long a(DateTime dateTime) {
        if (dateTime != null) {
            return dateTime.getMillis();
        }
        return 0;
    }

    @DexIgnore
    public final DateTime a(long j) {
        return new DateTime(j);
    }
}
