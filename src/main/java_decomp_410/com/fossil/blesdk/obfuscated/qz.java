package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.Report;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qz implements Report {
    @DexIgnore
    public /* final */ File a;

    @DexIgnore
    public qz(File file) {
        this.a = file;
    }

    @DexIgnore
    public Map<String, String> a() {
        return null;
    }

    @DexIgnore
    public String b() {
        return this.a.getName();
    }

    @DexIgnore
    public File c() {
        return null;
    }

    @DexIgnore
    public File[] d() {
        return this.a.listFiles();
    }

    @DexIgnore
    public String e() {
        return null;
    }

    @DexIgnore
    public Report.Type getType() {
        return Report.Type.NATIVE;
    }

    @DexIgnore
    public void remove() {
        for (File file : d()) {
            q44.g().d("CrashlyticsCore", "Removing native report file at " + file.getPath());
            file.delete();
        }
        q44.g().d("CrashlyticsCore", "Removing native report directory at " + this.a);
        this.a.delete();
    }
}
