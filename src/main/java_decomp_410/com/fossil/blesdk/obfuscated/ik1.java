package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ik1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ kg1 e;
    @DexIgnore
    public /* final */ /* synthetic */ hk1 f;

    @DexIgnore
    public ik1(hk1 hk1, kg1 kg1) {
        this.f = hk1;
        this.e = kg1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f) {
            boolean unused = this.f.a = false;
            if (!this.f.c.B()) {
                this.f.c.d().A().a("Connected to service");
                this.f.c.a(this.e);
            }
        }
    }
}
