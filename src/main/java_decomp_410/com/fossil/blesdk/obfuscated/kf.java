package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kf<T> extends wf {
    @DexIgnore
    public kf(RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    @DexIgnore
    public abstract void bind(kg kgVar, T t);

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public final int handle(T t) {
        kg acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.n();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(Iterable<T> iterable) {
        kg acquire = acquire();
        int i = 0;
        try {
            for (T bind : iterable) {
                bind(acquire, bind);
                i += acquire.n();
            }
            return i;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(T[] tArr) {
        kg acquire = acquire();
        try {
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                i += acquire.n();
            }
            return i;
        } finally {
            release(acquire);
        }
    }
}
