package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.ms2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y43 extends zr2 {
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel j;
    @DexIgnore
    public j42 k;
    @DexIgnore
    public ms2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final y43 a(String str) {
            kd4.b(str, MicroAppSetting.SETTING);
            y43 y43 = new y43();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_SETTING, str);
            y43.setArguments(bundle);
            return y43;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ms2.b {
        @DexIgnore
        public /* final */ /* synthetic */ y43 a;

        @DexIgnore
        public b(y43 y43) {
            this.a = y43;
        }

        @DexIgnore
        public void a(AddressWrapper addressWrapper) {
            kd4.b(addressWrapper, "address");
            this.a.a(addressWrapper);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends eu3 {
        @DexIgnore
        public /* final */ /* synthetic */ y43 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(int i, int i2, y43 y43) {
            super(i, i2);
            this.f = y43;
        }

        @DexIgnore
        public void b(RecyclerView.ViewHolder viewHolder, int i) {
            kd4.b(viewHolder, "viewHolder");
            int adapterPosition = viewHolder.getAdapterPosition();
            ms2 b = this.f.l;
            y43.c(this.f).b(b != null ? b.a(adapterPosition) : null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y43 e;

        @DexIgnore
        public d(y43 y43) {
            this.e = y43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y43 e;

        @DexIgnore
        public e(y43 y43) {
            this.e = y43;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements cc<CommuteTimeWatchAppSettingsViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ y43 a;

        @DexIgnore
        public f(y43 y43) {
            this.a = y43;
        }

        @DexIgnore
        public final void a(CommuteTimeWatchAppSettingsViewModel.b bVar) {
            List<AddressWrapper> a2 = bVar.a();
            if (a2 != null) {
                this.a.x(a2);
            }
        }
    }

    /*
    static {
        kd4.a((Object) y43.class.getSimpleName(), "CommuteTimeWatchAppSetti\u2026nt::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeWatchAppSettingsViewModel c(y43 y43) {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = y43.j;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            return commuteTimeWatchAppSettingsViewModel;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        r(true);
        return true;
    }

    @DexIgnore
    public final void T0() {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        if (commuteTimeWatchAppSettingsViewModel == null) {
            kd4.d("mViewModel");
            throw null;
        } else if (commuteTimeWatchAppSettingsViewModel.f()) {
            a(new AddressWrapper());
        } else {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.o(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null && i == 113) {
            AddressWrapper addressWrapper = (AddressWrapper) intent.getParcelableExtra("KEY_SELECTED_ADDRESS");
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                commuteTimeWatchAppSettingsViewModel.a(addressWrapper);
            } else {
                kd4.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.W.c().g().a(new f53()).a(this);
        j42 j42 = this.k;
        String str = null;
        if (j42 != null) {
            ic a2 = lc.a((Fragment) this, (kc.b) j42).a(CommuteTimeWatchAppSettingsViewModel.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ngsViewModel::class.java)");
            this.j = (CommuteTimeWatchAppSettingsViewModel) a2;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    str = arguments.getString(Constants.USER_SETTING);
                }
                commuteTimeWatchAppSettingsViewModel.b(str);
                return;
            }
            kd4.d("mViewModel");
            throw null;
        }
        kd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ia2 ia2 = (ia2) qa.a(layoutInflater, R.layout.fragment_commute_time_watch_app_settings, viewGroup, false, O0());
        ia2.r.setOnClickListener(new d(this));
        ia2.q.setOnClickListener(new e(this));
        ms2 ms2 = new ms2();
        ms2.a((ms2.b) new b(this));
        this.l = ms2;
        RecyclerView recyclerView = ia2.s;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.l);
        new oe(new c(0, 4, this)).a(recyclerView);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.e().a(getViewLifecycleOwner(), new f(this));
            new tr3(this, ia2);
            kd4.a((Object) ia2, "binding");
            return ia2.d();
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.g();
        } else {
            kd4.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void r(boolean z) {
        if (z) {
            Intent intent = new Intent();
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                intent.putExtra("COMMUTE_TIME_WATCH_APP_SETTING", commuteTimeWatchAppSettingsViewModel.d());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                kd4.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public final void x(List<AddressWrapper> list) {
        ms2 ms2 = this.l;
        if (ms2 != null) {
            ms2.a((List<AddressWrapper>) kb4.d(list));
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY_SELECTED_ADDRESS", addressWrapper);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.j;
        ArrayList<String> arrayList = null;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            CommuteTimeWatchAppSetting c2 = commuteTimeWatchAppSettingsViewModel.c();
            if (c2 != null) {
                arrayList = c2.getListAddressNameExceptOf(addressWrapper);
            }
            bundle.putStringArrayList("KEY_LIST_ADDRESS", arrayList);
            CommuteTimeSettingsDetailActivity.B.a(this, bundle, 113);
            return;
        }
        kd4.d("mViewModel");
        throw null;
    }
}
