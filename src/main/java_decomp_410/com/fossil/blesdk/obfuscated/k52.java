package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k52 implements Factory<dn2> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public k52(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static k52 a(n42 n42) {
        return new k52(n42);
    }

    @DexIgnore
    public static dn2 b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static dn2 c(n42 n42) {
        dn2 l = n42.l();
        n44.a(l, "Cannot return null from a non-@Nullable @Provides method");
        return l;
    }

    @DexIgnore
    public dn2 get() {
        return b(this.a);
    }
}
