package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.en */
public class C1738en {
    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2334lm.C2335a m6518a(com.fossil.blesdk.obfuscated.C2897sm smVar) {
        boolean z;
        long j;
        long j2;
        long j3;
        long j4;
        com.fossil.blesdk.obfuscated.C2897sm smVar2 = smVar;
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        java.util.Map<java.lang.String, java.lang.String> map = smVar2.f9392c;
        java.lang.String str = map.get(com.fossil.wearables.fsl.dial.ConfigItem.KEY_DATE);
        long a = str != null ? m6517a(str) : 0;
        java.lang.String str2 = map.get(com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
        int i = 0;
        if (str2 != null) {
            java.lang.String[] split = str2.split(",", 0);
            j2 = 0;
            int i2 = 0;
            j = 0;
            while (i < split.length) {
                java.lang.String trim = split[i].trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j2 = java.lang.Long.parseLong(trim.substring(8));
                    } catch (java.lang.Exception unused) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    j = java.lang.Long.parseLong(trim.substring(23));
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    i2 = 1;
                }
                i++;
            }
            i = i2;
            z = true;
        } else {
            j2 = 0;
            j = 0;
            z = false;
        }
        java.lang.String str3 = map.get("Expires");
        long a2 = str3 != null ? m6517a(str3) : 0;
        java.lang.String str4 = map.get("Last-Modified");
        long a3 = str4 != null ? m6517a(str4) : 0;
        java.lang.String str5 = map.get("ETag");
        if (z) {
            j4 = currentTimeMillis + (j2 * 1000);
            if (i == 0) {
                java.lang.Long.signum(j);
                j3 = (j * 1000) + j4;
                com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar = new com.fossil.blesdk.obfuscated.C2334lm.C2335a();
                aVar.f7239a = smVar2.f9391b;
                aVar.f7240b = str5;
                aVar.f7244f = j4;
                aVar.f7243e = j3;
                aVar.f7241c = a;
                aVar.f7242d = a3;
                aVar.f7245g = map;
                aVar.f7246h = smVar2.f9393d;
                return aVar;
            }
        } else if (a <= 0 || a2 < a) {
            j4 = 0;
        } else {
            j3 = (a2 - a) + currentTimeMillis;
            j4 = j3;
            com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar2 = new com.fossil.blesdk.obfuscated.C2334lm.C2335a();
            aVar2.f7239a = smVar2.f9391b;
            aVar2.f7240b = str5;
            aVar2.f7244f = j4;
            aVar2.f7243e = j3;
            aVar2.f7241c = a;
            aVar2.f7242d = a3;
            aVar2.f7245g = map;
            aVar2.f7246h = smVar2.f9393d;
            return aVar2;
        }
        j3 = j4;
        com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar22 = new com.fossil.blesdk.obfuscated.C2334lm.C2335a();
        aVar22.f7239a = smVar2.f9391b;
        aVar22.f7240b = str5;
        aVar22.f7244f = j4;
        aVar22.f7243e = j3;
        aVar22.f7241c = a;
        aVar22.f7242d = a3;
        aVar22.f7245g = map;
        aVar22.f7246h = smVar2.f9393d;
        return aVar22;
    }

    @DexIgnore
    /* renamed from: a */
    public static long m6517a(java.lang.String str) {
        try {
            return m6521a().parse(str).getTime();
        } catch (java.text.ParseException e) {
            com.fossil.blesdk.obfuscated.C3296xm.m16420a(e, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m6519a(long j) {
        return m6521a().format(new java.util.Date(j));
    }

    @DexIgnore
    /* renamed from: a */
    public static java.text.SimpleDateFormat m6521a() {
        java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", java.util.Locale.US);
        simpleDateFormat.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m6520a(java.util.Map<java.lang.String, java.lang.String> map, java.lang.String str) {
        java.lang.String str2 = map.get(com.facebook.GraphRequest.CONTENT_TYPE_HEADER);
        if (str2 != null) {
            java.lang.String[] split = str2.split(";", 0);
            for (int i = 1; i < split.length; i++) {
                java.lang.String[] split2 = split[i].trim().split(com.j256.ormlite.stmt.query.SimpleComparison.EQUAL_TO_OPERATION, 0);
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return str;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.Map<java.lang.String, java.lang.String> m6523a(java.util.List<com.fossil.blesdk.obfuscated.C2660pm> list) {
        java.util.TreeMap treeMap = new java.util.TreeMap(java.lang.String.CASE_INSENSITIVE_ORDER);
        for (com.fossil.blesdk.obfuscated.C2660pm next : list) {
            treeMap.put(next.mo14804a(), next.mo14805b());
        }
        return treeMap;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<com.fossil.blesdk.obfuscated.C2660pm> m6522a(java.util.Map<java.lang.String, java.lang.String> map) {
        java.util.ArrayList arrayList = new java.util.ArrayList(map.size());
        for (java.util.Map.Entry next : map.entrySet()) {
            arrayList.add(new com.fossil.blesdk.obfuscated.C2660pm((java.lang.String) next.getKey(), (java.lang.String) next.getValue()));
        }
        return arrayList;
    }
}
