package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.text.Regex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qs2 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public List<Pair<Complication, String>> a;
    @DexIgnore
    public List<Pair<Complication, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public e d;
    @DexIgnore
    public d e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(qs2 qs2, View view) {
            super(view);
            kd4.b(view, "itemView");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends a {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(qs2 qs2, View view) {
            super(qs2, view);
            kd4.b(view, "itemView");
            View findViewById = view.findViewById(R.id.tv_recent);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
            } else {
                kd4.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(Complication complication);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends a {
        @DexIgnore
        public /* final */ CustomizeWidget a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public Complication d;
        @DexIgnore
        public /* final */ /* synthetic */ qs2 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ f e;

            @DexIgnore
            public a(f fVar) {
                this.e = fVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Complication a = this.e.a();
                if (a != null) {
                    e a2 = this.e.e.d;
                    if (a2 != null) {
                        a2.a(a);
                    } else {
                        FLogger.INSTANCE.getLocal().d(qs2.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(qs2.f, "itemClick(), complication tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qs2 qs2, View view) {
            super(qs2, view);
            kd4.b(view, "itemView");
            this.e = qs2;
            View findViewById = view.findViewById(R.id.wc_icon);
            kd4.a((Object) findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(R.id.tv_name);
            kd4.a((Object) findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.tv_assigned_to);
            if (findViewById3 != null) {
                this.c = (FlexibleTextView) findViewById3;
                view.setOnClickListener(new a(this));
                return;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public final Complication a() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.a;
        }

        @DexIgnore
        public final void a(Complication complication) {
            this.d = complication;
        }
    }

    /*
    static {
        new b((fd4) null);
        String name = qs2.class.getName();
        kd4.a((Object) name, "ComplicationSearchAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    public int getItemCount() {
        List<Pair<Complication, String>> list = this.a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    public int getItemViewType(int i) {
        return (this.a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final void a(List<Pair<Complication, String>> list) {
        kd4.b(list, "value");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void b(List<Pair<Complication, String>> list) {
        this.a = list;
        List<Pair<Complication, String>> list2 = this.a;
        if (list2 != null && list2.isEmpty()) {
            d dVar = this.e;
            if (dVar != null) {
                dVar.a(this.c);
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_complication_search_header, viewGroup, false);
            kd4.a((Object) inflate, "view");
            return new c(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_complication_search, viewGroup, false);
        kd4.a((Object) inflate2, "view");
        return new f(this, inflate2);
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        if (!(aVar instanceof c)) {
            f fVar = (f) aVar;
            List<Pair<Complication, String>> list = this.a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty((CharSequence) this.b.get(i2).getSecond())) {
                        fVar.b().setVisibility(0);
                        fVar.b().setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Text__Assigned));
                    } else {
                        fVar.b().setVisibility(8);
                    }
                    fVar.d().b(((Complication) this.b.get(i2).getFirst()).getComplicationId());
                    fVar.c().setText(sm2.a(PortfolioApp.W.c(), ((Complication) this.b.get(i2).getFirst()).getNameKey(), ((Complication) this.b.get(i2).getFirst()).getName()));
                    fVar.a((Complication) this.b.get(i2).getFirst());
                    return;
                }
                fVar.a((Complication) null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty((CharSequence) list.get(i).getSecond())) {
                    fVar.b().setVisibility(0);
                    fVar.b().setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_Elements_Text__Assigned));
                } else {
                    fVar.b().setVisibility(8);
                }
                fVar.d().b(((Complication) list.get(i).getFirst()).getComplicationId());
                String a2 = sm2.a(PortfolioApp.W.c(), ((Complication) list.get(i).getFirst()).getNameKey(), ((Complication) list.get(i).getFirst()).getName());
                FlexibleTextView c2 = fVar.c();
                kd4.a((Object) a2, "name");
                c2.setText(a(a2, this.c));
                fVar.a((Complication) list.get(i).getFirst());
            } else {
                fVar.a((Complication) null);
            }
        } else if (this.b.isEmpty()) {
            ((c) aVar).a().setVisibility(4);
        } else {
            ((c) aVar).a().setVisibility(0);
        }
    }

    @DexIgnore
    public final SpannableString a(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                Regex regex = new Regex(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    re4<hf4> findAll$default = Regex.findAll$default(regex, lowerCase, 0, 2, (Object) null);
                    SpannableString spannableString = new SpannableString(str);
                    for (hf4 hf4 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), hf4.a().e().intValue(), hf4.a().e().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    public final void a(e eVar) {
        kd4.b(eVar, "listener");
        this.d = eVar;
    }

    @DexIgnore
    public final void a(d dVar) {
        kd4.b(dVar, "listener");
        this.e = dVar;
    }
}
