package com.fossil.blesdk.obfuscated;

import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sw1 {
    @DexIgnore
    public /* final */ iw1<?> a;
    @DexIgnore
    public /* final */ Set<sw1> b; // = new HashSet();
    @DexIgnore
    public /* final */ Set<sw1> c; // = new HashSet();

    @DexIgnore
    public sw1(iw1<?> iw1) {
        this.a = iw1;
    }

    @DexIgnore
    public final void a(sw1 sw1) {
        this.b.add(sw1);
    }

    @DexIgnore
    public final void b(sw1 sw1) {
        this.c.add(sw1);
    }

    @DexIgnore
    public final void c(sw1 sw1) {
        this.c.remove(sw1);
    }

    @DexIgnore
    public final boolean d() {
        return this.b.isEmpty();
    }

    @DexIgnore
    public final Set<sw1> a() {
        return this.b;
    }

    @DexIgnore
    public final iw1<?> b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c.isEmpty();
    }
}
