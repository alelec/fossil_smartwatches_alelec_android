package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.h4 */
public final class C1922h4<E> implements java.util.Collection<E>, java.util.Set<E> {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ int[] f5667i; // = new int[0];

    @DexIgnore
    /* renamed from: j */
    public static /* final */ java.lang.Object[] f5668j; // = new java.lang.Object[0];

    @DexIgnore
    /* renamed from: k */
    public static java.lang.Object[] f5669k;

    @DexIgnore
    /* renamed from: l */
    public static int f5670l;

    @DexIgnore
    /* renamed from: m */
    public static java.lang.Object[] f5671m;

    @DexIgnore
    /* renamed from: n */
    public static int f5672n;

    @DexIgnore
    /* renamed from: e */
    public int[] f5673e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.Object[] f5674f;

    @DexIgnore
    /* renamed from: g */
    public int f5675g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2289l4<E, E> f5676h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.h4$a")
    /* renamed from: com.fossil.blesdk.obfuscated.h4$a */
    public class C1923a extends com.fossil.blesdk.obfuscated.C2289l4<E, E> {
        @DexIgnore
        public C1923a() {
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Object mo11126a(int i, int i2) {
            return com.fossil.blesdk.obfuscated.C1922h4.this.f5674f[i];
        }

        @DexIgnore
        /* renamed from: b */
        public int mo11131b(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C1922h4.this.indexOf(obj);
        }

        @DexIgnore
        /* renamed from: c */
        public int mo11133c() {
            return com.fossil.blesdk.obfuscated.C1922h4.this.f5675g;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo11125a(java.lang.Object obj) {
            return com.fossil.blesdk.obfuscated.C1922h4.this.indexOf(obj);
        }

        @DexIgnore
        /* renamed from: b */
        public java.util.Map<E, E> mo11132b() {
            throw new java.lang.UnsupportedOperationException("not a map");
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11130a(E e, E e2) {
            com.fossil.blesdk.obfuscated.C1922h4.this.add(e);
        }

        @DexIgnore
        /* renamed from: a */
        public E mo11127a(int i, E e) {
            throw new java.lang.UnsupportedOperationException("not a map");
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11129a(int i) {
            com.fossil.blesdk.obfuscated.C1922h4.this.mo11580g(i);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11128a() {
            com.fossil.blesdk.obfuscated.C1922h4.this.clear();
        }
    }

    @DexIgnore
    public C1922h4() {
        this(0);
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo11569a(java.lang.Object obj, int i) {
        int i2 = this.f5675g;
        if (i2 == 0) {
            return -1;
        }
        int a = com.fossil.blesdk.obfuscated.C1997i4.m8223a(this.f5673e, i2, i);
        if (a < 0 || obj.equals(this.f5674f[a])) {
            return a;
        }
        int i3 = a + 1;
        while (i3 < i2 && this.f5673e[i3] == i) {
            if (obj.equals(this.f5674f[i3])) {
                return i3;
            }
            i3++;
        }
        int i4 = a - 1;
        while (i4 >= 0 && this.f5673e[i4] == i) {
            if (obj.equals(this.f5674f[i4])) {
                return i4;
            }
            i4--;
        }
        return ~i3;
    }

    @DexIgnore
    public boolean add(E e) {
        int i;
        int i2;
        if (e == null) {
            i2 = mo11574b();
            i = 0;
        } else {
            int hashCode = e.hashCode();
            i = hashCode;
            i2 = mo11569a(e, hashCode);
        }
        if (i2 >= 0) {
            return false;
        }
        int i3 = ~i2;
        int i4 = this.f5675g;
        if (i4 >= this.f5673e.length) {
            int i5 = 4;
            if (i4 >= 8) {
                i5 = (i4 >> 1) + i4;
            } else if (i4 >= 4) {
                i5 = 8;
            }
            int[] iArr = this.f5673e;
            java.lang.Object[] objArr = this.f5674f;
            mo11571a(i5);
            int[] iArr2 = this.f5673e;
            if (iArr2.length > 0) {
                java.lang.System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                java.lang.System.arraycopy(objArr, 0, this.f5674f, 0, objArr.length);
            }
            m7777a(iArr, objArr, this.f5675g);
        }
        int i6 = this.f5675g;
        if (i3 < i6) {
            int[] iArr3 = this.f5673e;
            int i7 = i3 + 1;
            java.lang.System.arraycopy(iArr3, i3, iArr3, i7, i6 - i3);
            java.lang.Object[] objArr2 = this.f5674f;
            java.lang.System.arraycopy(objArr2, i3, objArr2, i7, this.f5675g - i3);
        }
        this.f5673e[i3] = i;
        this.f5674f[i3] = e;
        this.f5675g++;
        return true;
    }

    @DexIgnore
    public boolean addAll(java.util.Collection<? extends E> collection) {
        mo11579f(this.f5675g + collection.size());
        boolean z = false;
        for (java.lang.Object add : collection) {
            z |= add(add);
        }
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo11574b() {
        int i = this.f5675g;
        if (i == 0) {
            return -1;
        }
        int a = com.fossil.blesdk.obfuscated.C1997i4.m8223a(this.f5673e, i, 0);
        if (a < 0 || this.f5674f[a] == null) {
            return a;
        }
        int i2 = a + 1;
        while (i2 < i && this.f5673e[i2] == 0) {
            if (this.f5674f[i2] == null) {
                return i2;
            }
            i2++;
        }
        int i3 = a - 1;
        while (i3 >= 0 && this.f5673e[i3] == 0) {
            if (this.f5674f[i3] == null) {
                return i3;
            }
            i3--;
        }
        return ~i2;
    }

    @DexIgnore
    public void clear() {
        int i = this.f5675g;
        if (i != 0) {
            m7777a(this.f5673e, this.f5674f, i);
            this.f5673e = f5667i;
            this.f5674f = f5668j;
            this.f5675g = 0;
        }
    }

    @DexIgnore
    public boolean contains(java.lang.Object obj) {
        return indexOf(obj) >= 0;
    }

    @DexIgnore
    public boolean containsAll(java.util.Collection<?> collection) {
        for (java.lang.Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof java.util.Set) {
            java.util.Set set = (java.util.Set) obj;
            if (size() != set.size()) {
                return false;
            }
            int i = 0;
            while (i < this.f5675g) {
                try {
                    if (!set.contains(mo11581h(i))) {
                        return false;
                    }
                    i++;
                } catch (java.lang.ClassCastException | java.lang.NullPointerException unused) {
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: f */
    public void mo11579f(int i) {
        int[] iArr = this.f5673e;
        if (iArr.length < i) {
            java.lang.Object[] objArr = this.f5674f;
            mo11571a(i);
            int i2 = this.f5675g;
            if (i2 > 0) {
                java.lang.System.arraycopy(iArr, 0, this.f5673e, 0, i2);
                java.lang.System.arraycopy(objArr, 0, this.f5674f, 0, this.f5675g);
            }
            m7777a(iArr, objArr, this.f5675g);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public E mo11580g(int i) {
        E[] eArr = this.f5674f;
        E e = eArr[i];
        int i2 = this.f5675g;
        if (i2 <= 1) {
            m7777a(this.f5673e, eArr, i2);
            this.f5673e = f5667i;
            this.f5674f = f5668j;
            this.f5675g = 0;
        } else {
            int[] iArr = this.f5673e;
            int i3 = 8;
            if (iArr.length <= 8 || i2 >= iArr.length / 3) {
                this.f5675g--;
                int i4 = this.f5675g;
                if (i < i4) {
                    int[] iArr2 = this.f5673e;
                    int i5 = i + 1;
                    java.lang.System.arraycopy(iArr2, i5, iArr2, i, i4 - i);
                    java.lang.Object[] objArr = this.f5674f;
                    java.lang.System.arraycopy(objArr, i5, objArr, i, this.f5675g - i);
                }
                this.f5674f[this.f5675g] = null;
            } else {
                if (i2 > 8) {
                    i3 = i2 + (i2 >> 1);
                }
                int[] iArr3 = this.f5673e;
                java.lang.Object[] objArr2 = this.f5674f;
                mo11571a(i3);
                this.f5675g--;
                if (i > 0) {
                    java.lang.System.arraycopy(iArr3, 0, this.f5673e, 0, i);
                    java.lang.System.arraycopy(objArr2, 0, this.f5674f, 0, i);
                }
                int i6 = this.f5675g;
                if (i < i6) {
                    int i7 = i + 1;
                    java.lang.System.arraycopy(iArr3, i7, this.f5673e, i, i6 - i);
                    java.lang.System.arraycopy(objArr2, i7, this.f5674f, i, this.f5675g - i);
                }
            }
        }
        return e;
    }

    @DexIgnore
    /* renamed from: h */
    public E mo11581h(int i) {
        return this.f5674f[i];
    }

    @DexIgnore
    public int hashCode() {
        int[] iArr = this.f5673e;
        int i = this.f5675g;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            i2 += iArr[i3];
        }
        return i2;
    }

    @DexIgnore
    public int indexOf(java.lang.Object obj) {
        return obj == null ? mo11574b() : mo11569a(obj, obj.hashCode());
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.f5675g <= 0;
    }

    @DexIgnore
    public java.util.Iterator<E> iterator() {
        return mo11570a().mo13146e().iterator();
    }

    @DexIgnore
    public boolean remove(java.lang.Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf < 0) {
            return false;
        }
        mo11580g(indexOf);
        return true;
    }

    @DexIgnore
    public boolean removeAll(java.util.Collection<?> collection) {
        boolean z = false;
        for (java.lang.Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    @DexIgnore
    public boolean retainAll(java.util.Collection<?> collection) {
        boolean z = false;
        for (int i = this.f5675g - 1; i >= 0; i--) {
            if (!collection.contains(this.f5674f[i])) {
                mo11580g(i);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public int size() {
        return this.f5675g;
    }

    @DexIgnore
    public java.lang.Object[] toArray() {
        int i = this.f5675g;
        java.lang.Object[] objArr = new java.lang.Object[i];
        java.lang.System.arraycopy(this.f5674f, 0, objArr, 0, i);
        return objArr;
    }

    @DexIgnore
    public java.lang.String toString() {
        if (isEmpty()) {
            return "{}";
        }
        java.lang.StringBuilder sb = new java.lang.StringBuilder(this.f5675g * 14);
        sb.append('{');
        for (int i = 0; i < this.f5675g; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            java.lang.Object h = mo11581h(i);
            if (h != this) {
                sb.append(h);
            } else {
                sb.append("(this Set)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public C1922h4(int i) {
        if (i == 0) {
            this.f5673e = f5667i;
            this.f5674f = f5668j;
        } else {
            mo11571a(i);
        }
        this.f5675g = 0;
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        if (tArr.length < this.f5675g) {
            tArr = (java.lang.Object[]) java.lang.reflect.Array.newInstance(tArr.getClass().getComponentType(), this.f5675g);
        }
        java.lang.System.arraycopy(this.f5674f, 0, tArr, 0, this.f5675g);
        int length = tArr.length;
        int i = this.f5675g;
        if (length > i) {
            tArr[i] = null;
        }
        return tArr;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11571a(int i) {
        if (i == 8) {
            synchronized (com.fossil.blesdk.obfuscated.C1922h4.class) {
                if (f5671m != null) {
                    java.lang.Object[] objArr = f5671m;
                    this.f5674f = objArr;
                    f5671m = (java.lang.Object[]) objArr[0];
                    this.f5673e = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    f5672n--;
                    return;
                }
            }
        } else if (i == 4) {
            synchronized (com.fossil.blesdk.obfuscated.C1922h4.class) {
                if (f5669k != null) {
                    java.lang.Object[] objArr2 = f5669k;
                    this.f5674f = objArr2;
                    f5669k = (java.lang.Object[]) objArr2[0];
                    this.f5673e = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f5670l--;
                    return;
                }
            }
        }
        this.f5673e = new int[i];
        this.f5674f = new java.lang.Object[i];
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7777a(int[] iArr, java.lang.Object[] objArr, int i) {
        if (iArr.length == 8) {
            synchronized (com.fossil.blesdk.obfuscated.C1922h4.class) {
                if (f5672n < 10) {
                    objArr[0] = f5671m;
                    objArr[1] = iArr;
                    for (int i2 = i - 1; i2 >= 2; i2--) {
                        objArr[i2] = null;
                    }
                    f5671m = objArr;
                    f5672n++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (com.fossil.blesdk.obfuscated.C1922h4.class) {
                if (f5670l < 10) {
                    objArr[0] = f5669k;
                    objArr[1] = iArr;
                    for (int i3 = i - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    f5669k = objArr;
                    f5670l++;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2289l4<E, E> mo11570a() {
        if (this.f5676h == null) {
            this.f5676h = new com.fossil.blesdk.obfuscated.C1922h4.C1923a();
        }
        return this.f5676h;
    }
}
