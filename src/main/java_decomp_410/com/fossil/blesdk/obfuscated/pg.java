package com.fossil.blesdk.obfuscated;

import android.database.sqlite.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pg extends og implements kg {
    @DexIgnore
    public /* final */ SQLiteStatement f;

    @DexIgnore
    public pg(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.f = sQLiteStatement;
    }

    @DexIgnore
    public int n() {
        return this.f.executeUpdateDelete();
    }

    @DexIgnore
    public long o() {
        return this.f.executeInsert();
    }
}
