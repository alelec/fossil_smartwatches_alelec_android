package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.re */
public class C2802re {

    @DexIgnore
    /* renamed from: a */
    public boolean f8927a; // = true;

    @DexIgnore
    /* renamed from: b */
    public int f8928b;

    @DexIgnore
    /* renamed from: c */
    public int f8929c;

    @DexIgnore
    /* renamed from: d */
    public int f8930d;

    @DexIgnore
    /* renamed from: e */
    public int f8931e;

    @DexIgnore
    /* renamed from: f */
    public int f8932f; // = 0;

    @DexIgnore
    /* renamed from: g */
    public int f8933g; // = 0;

    @DexIgnore
    /* renamed from: h */
    public boolean f8934h;

    @DexIgnore
    /* renamed from: i */
    public boolean f8935i;

    @DexIgnore
    /* renamed from: a */
    public boolean mo15499a(androidx.recyclerview.widget.RecyclerView.State state) {
        int i = this.f8929c;
        return i >= 0 && i < state.mo2737a();
    }

    @DexIgnore
    public java.lang.String toString() {
        return "LayoutState{mAvailable=" + this.f8928b + ", mCurrentPosition=" + this.f8929c + ", mItemDirection=" + this.f8930d + ", mLayoutDirection=" + this.f8931e + ", mStartLine=" + this.f8932f + ", mEndLine=" + this.f8933g + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View mo15498a(androidx.recyclerview.widget.RecyclerView.Recycler recycler) {
        android.view.View d = recycler.mo2724d(this.f8929c);
        this.f8929c += this.f8930d;
        return d;
    }
}
