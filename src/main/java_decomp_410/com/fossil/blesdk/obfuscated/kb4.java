package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;
import kotlin.TypeCastException;
import kotlin.collections.SlidingWindowKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kb4 extends jb4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements re4<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable a;

        @DexIgnore
        public a(Iterable iterable) {
            this.a = iterable;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, T t) {
        kd4.b(iterable, "$this$contains");
        if (iterable instanceof Collection) {
            return ((Collection) iterable).contains(t);
        }
        return b(iterable, t) >= 0;
    }

    @DexIgnore
    public static final <T> int b(Iterable<? extends T> iterable, T t) {
        kd4.b(iterable, "$this$indexOf");
        if (iterable instanceof List) {
            return ((List) iterable).indexOf(t);
        }
        int i = 0;
        for (Object next : iterable) {
            if (i < 0) {
                cb4.c();
                throw null;
            } else if (kd4.a((Object) t, (Object) next)) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> List<T> c(Iterable<? extends T> iterable, int i) {
        kd4.b(iterable, "$this$take");
        int i2 = 0;
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return cb4.a();
        } else {
            if (iterable instanceof Collection) {
                if (i >= ((Collection) iterable).size()) {
                    return k(iterable);
                }
                if (i == 1) {
                    return bb4.a(d(iterable));
                }
            }
            ArrayList arrayList = new ArrayList(i);
            for (Object next : iterable) {
                int i3 = i2 + 1;
                if (i2 == i) {
                    break;
                }
                arrayList.add(next);
                i2 = i3;
            }
            return cb4.b(arrayList);
        }
    }

    @DexIgnore
    public static final <T> T d(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$first");
        if (iterable instanceof List) {
            return d((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            return it.next();
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T> T e(List<? extends T> list) {
        kd4.b(list, "$this$firstOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @DexIgnore
    public static final <T> T f(List<? extends T> list) {
        kd4.b(list, "$this$last");
        if (!list.isEmpty()) {
            return list.get(cb4.a(list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    @DexIgnore
    public static final <T> T g(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$single");
        if (iterable instanceof List) {
            return g((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T next = it.next();
            if (!it.hasNext()) {
                return next;
            }
            throw new IllegalArgumentException("Collection has more than one element.");
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> List<T> h(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$sorted");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return k(iterable);
            }
            Object[] array = collection.toArray(new Comparable[0]);
            if (array == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                Comparable[] comparableArr = (Comparable[]) array;
                if (comparableArr != null) {
                    ya4.b(comparableArr);
                    return ya4.a((T[]) comparableArr);
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> l = l(iterable);
            gb4.c(l);
            return l;
        }
    }

    @DexIgnore
    public static final int i(Iterable<Integer> iterable) {
        kd4.b(iterable, "$this$sum");
        int i = 0;
        for (Integer intValue : iterable) {
            i += intValue.intValue();
        }
        return i;
    }

    @DexIgnore
    public static final <T> HashSet<T> j(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$toHashSet");
        HashSet<T> hashSet = new HashSet<>(rb4.a(db4.a(iterable, 12)));
        a(iterable, hashSet);
        return hashSet;
    }

    @DexIgnore
    public static final <T> List<T> k(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$toList");
        if (!(iterable instanceof Collection)) {
            return cb4.b(l(iterable));
        }
        Collection collection = (Collection) iterable;
        int size = collection.size();
        if (size == 0) {
            return cb4.a();
        }
        if (size != 1) {
            return d(collection);
        }
        return bb4.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
    }

    @DexIgnore
    public static final <T> List<T> l(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$toMutableList");
        if (iterable instanceof Collection) {
            return d((Collection) iterable);
        }
        ArrayList arrayList = new ArrayList();
        a(iterable, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T> Set<T> m(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$toMutableSet");
        if (iterable instanceof Collection) {
            return new LinkedHashSet((Collection) iterable);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        a(iterable, linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> Set<T> n(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$toSet");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return vb4.a();
            }
            if (size != 1) {
                LinkedHashSet linkedHashSet = new LinkedHashSet(rb4.a(collection.size()));
                a(iterable, linkedHashSet);
                return linkedHashSet;
            }
            return ub4.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
        }
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        a(iterable, linkedHashSet2);
        return vb4.a(linkedHashSet2);
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T e(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$max");
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        T t = (Comparable) it.next();
        while (it.hasNext()) {
            T t2 = (Comparable) it.next();
            if (t.compareTo(t2) < 0) {
                t = t2;
            }
        }
        return t;
    }

    @DexIgnore
    public static final <T> T a(List<? extends T> list, int i) {
        kd4.b(list, "$this$getOrNull");
        if (i < 0 || i > cb4.a(list)) {
            return null;
        }
        return list.get(i);
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> T f(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$min");
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        T t = (Comparable) it.next();
        while (it.hasNext()) {
            T t2 = (Comparable) it.next();
            if (t.compareTo(t2) > 0) {
                t = t2;
            }
        }
        return t;
    }

    @DexIgnore
    public static final <T> int a(List<? extends T> list, T t) {
        kd4.b(list, "$this$indexOf");
        return list.indexOf(t);
    }

    @DexIgnore
    public static final <T> Set<T> b(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        kd4.b(iterable, "$this$intersect");
        kd4.b(iterable2, FacebookRequestErrorClassification.KEY_OTHER);
        Set<T> m = m(iterable);
        hb4.b(m, iterable2);
        return m;
    }

    @DexIgnore
    public static final <T> List<T> a(Iterable<? extends T> iterable, Comparator<? super T> comparator) {
        kd4.b(iterable, "$this$sortedWith");
        kd4.b(comparator, "comparator");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return k(iterable);
            }
            Object[] array = collection.toArray(new Object[0]);
            if (array == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                ya4.a((T[]) array, comparator);
                return ya4.a((T[]) array);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> l = l(iterable);
            gb4.a(l, comparator);
            return l;
        }
    }

    @DexIgnore
    public static final <T> T d(List<? extends T> list) {
        kd4.b(list, "$this$first");
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    @DexIgnore
    public static final <T> List<List<T>> b(Iterable<? extends T> iterable, int i) {
        kd4.b(iterable, "$this$chunked");
        return a(iterable, i, i, true);
    }

    @DexIgnore
    public static final <T> re4<T> b(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$asSequence");
        return new a(iterable);
    }

    @DexIgnore
    public static final <T> T g(List<? extends T> list) {
        kd4.b(list, "$this$single");
        int size = list.size();
        if (size == 0) {
            throw new NoSuchElementException("List is empty.");
        } else if (size == 1) {
            return list.get(0);
        } else {
            throw new IllegalArgumentException("List has more than one element.");
        }
    }

    @DexIgnore
    public static final <T> List<T> d(Collection<? extends T> collection) {
        kd4.b(collection, "$this$toMutableList");
        return new ArrayList(collection);
    }

    @DexIgnore
    public static final int[] c(Collection<Integer> collection) {
        kd4.b(collection, "$this$toIntArray");
        int[] iArr = new int[collection.size()];
        int i = 0;
        for (Integer intValue : collection) {
            iArr[i] = intValue.intValue();
            i++;
        }
        return iArr;
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C a(Iterable<? extends T> iterable, C c) {
        kd4.b(iterable, "$this$toCollection");
        kd4.b(c, ShareConstants.DESTINATION);
        for (Object add : iterable) {
            c.add(add);
        }
        return c;
    }

    @DexIgnore
    public static final <T> List<T> c(Iterable<? extends T> iterable) {
        kd4.b(iterable, "$this$distinct");
        return k(m(iterable));
    }

    @DexIgnore
    public static final <T> List<T> a(Collection<? extends T> collection, T t) {
        kd4.b(collection, "$this$plus");
        ArrayList arrayList = new ArrayList(collection.size() + 1);
        arrayList.addAll(collection);
        arrayList.add(t);
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> c(Collection<? extends T> collection, Iterable<? extends T> iterable) {
        kd4.b(collection, "$this$plus");
        kd4.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            Collection collection2 = (Collection) iterable;
            ArrayList arrayList = new ArrayList(collection.size() + collection2.size());
            arrayList.addAll(collection);
            arrayList.addAll(collection2);
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList(collection);
        hb4.a(arrayList2, iterable);
        return arrayList2;
    }

    @DexIgnore
    public static final <T> List<List<T>> a(Iterable<? extends T> iterable, int i, int i2, boolean z) {
        kd4.b(iterable, "$this$windowed");
        SlidingWindowKt.a(i, i2);
        if (!(iterable instanceof RandomAccess) || !(iterable instanceof List)) {
            ArrayList arrayList = new ArrayList();
            Iterator<List<T>> a2 = SlidingWindowKt.a(iterable.iterator(), i, i2, z, false);
            while (a2.hasNext()) {
                arrayList.add(a2.next());
            }
            return arrayList;
        }
        List list = (List) iterable;
        int size = list.size();
        ArrayList arrayList2 = new ArrayList(((size + i2) - 1) / i2);
        int i3 = 0;
        while (i3 < size) {
            int b = ee4.b(i, size - i3);
            if (b < i && !z) {
                break;
            }
            ArrayList arrayList3 = new ArrayList(b);
            for (int i4 = 0; i4 < b; i4++) {
                arrayList3.add(list.get(i4 + i3));
            }
            arrayList2.add(arrayList3);
            i3 += i2;
        }
        return arrayList2;
    }

    @DexIgnore
    public static final <T, A extends Appendable> A a(Iterable<? extends T> iterable, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, xc4<? super T, ? extends CharSequence> xc4) {
        kd4.b(iterable, "$this$joinTo");
        kd4.b(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        kd4.b(charSequence, "separator");
        kd4.b(charSequence2, "prefix");
        kd4.b(charSequence3, "postfix");
        kd4.b(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object next : iterable) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            nf4.a(a2, next, xc4);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static /* synthetic */ String a(Iterable iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, xc4 xc4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence7 = charSequence4;
        if ((i2 & 32) != 0) {
            xc4 = null;
        }
        return a(iterable, charSequence, charSequence6, charSequence5, i3, charSequence7, xc4);
    }

    @DexIgnore
    public static final <T> String a(Iterable<? extends T> iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, xc4<? super T, ? extends CharSequence> xc4) {
        kd4.b(iterable, "$this$joinToString");
        kd4.b(charSequence, "separator");
        kd4.b(charSequence2, "prefix");
        kd4.b(charSequence3, "postfix");
        kd4.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(iterable, sb, charSequence, charSequence2, charSequence3, i, charSequence4, xc4);
        String sb2 = sb.toString();
        kd4.a((Object) sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }
}
