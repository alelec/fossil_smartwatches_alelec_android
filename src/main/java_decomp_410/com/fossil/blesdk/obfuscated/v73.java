package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface v73 extends bs2<u73> {
    @DexIgnore
    void C();

    @DexIgnore
    void S();

    @DexIgnore
    void U();

    @DexIgnore
    void V();

    @DexIgnore
    void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void a(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z);

    @DexIgnore
    void a(String str, String str2);

    @DexIgnore
    void a(Date date);

    @DexIgnore
    void a(boolean z, boolean z2, boolean z3);

    @DexIgnore
    void b(int i);

    @DexIgnore
    void b(boolean z);

    @DexIgnore
    void j(boolean z);

    @DexIgnore
    void o(boolean z);

    @DexIgnore
    void z();
}
