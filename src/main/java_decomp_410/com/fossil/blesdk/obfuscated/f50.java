package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.entity.DeviceFile;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.GetFilePhase;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f50 extends GetFilePhase {
    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ f50(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, HashMap hashMap, String str, int i, fd4 fd4) {
        this(peripheral, aVar, phaseId, r4, str);
        HashMap hashMap2 = (i & 8) != 0 ? new HashMap() : hashMap;
        if ((i & 16) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public JSONObject x() {
        JSONArray jSONArray = new JSONArray();
        Iterator<DeviceFile> it = J().iterator();
        while (it.hasNext()) {
            jSONArray.put(it.next().toJSONObject(false));
        }
        JSONObject put = super.x().put(GetFilePhase.GetFileOption.SKIP_ERASE.getLogName$blesdk_productionRelease(), L());
        kd4.a((Object) put, "super.resultDescription(\u2026ERASE.logName, skipErase)");
        return wa0.a(put, JSONKey.FILES, jSONArray);
    }

    @DexIgnore
    public byte[][] i() {
        ArrayList<DeviceFile> J = J();
        ArrayList arrayList = new ArrayList(db4.a(J, 10));
        for (DeviceFile rawData : J) {
            arrayList.add(rawData.getRawData());
        }
        Object[] array = arrayList.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public f50(Peripheral peripheral, Phase.a aVar, PhaseId phaseId, HashMap<GetFilePhase.GetFileOption, Object> hashMap, String str) {
        super(peripheral, aVar, phaseId, z40.b.a(peripheral.k(), FileType.DATA_COLLECTION_FILE), hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r8, 32, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(phaseId, "phaseId");
        kd4.b(hashMap, "options");
        String str2 = str;
        kd4.b(str2, "phaseUuid");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ f50(Peripheral peripheral, Phase.a aVar, HashMap hashMap, String str, int i, fd4 fd4) {
        this(peripheral, aVar, hashMap, str);
        hashMap = (i & 4) != 0 ? new HashMap() : hashMap;
        if ((i & 8) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public f50(Peripheral peripheral, Phase.a aVar, HashMap<GetFilePhase.GetFileOption, Object> hashMap, String str) {
        this(peripheral, aVar, PhaseId.GET_DATA_COLLECTION_FILE, hashMap, str);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(hashMap, "options");
        kd4.b(str, "phaseUuid");
    }
}
