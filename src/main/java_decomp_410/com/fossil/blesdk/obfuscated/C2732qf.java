package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qf */
public class C2732qf {

    @DexIgnore
    /* renamed from: a */
    public android.content.Context f8642a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f8643b;

    @DexIgnore
    /* renamed from: c */
    public int f8644c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2647pf f8645d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2647pf.C2650c f8646e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2486nf f8647f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.util.concurrent.Executor f8648g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2406mf f8649h; // = new com.fossil.blesdk.obfuscated.C2732qf.C2733a();

    @DexIgnore
    /* renamed from: i */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f8650i; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: j */
    public /* final */ android.content.ServiceConnection f8651j; // = new com.fossil.blesdk.obfuscated.C2732qf.C2735b();

    @DexIgnore
    /* renamed from: k */
    public /* final */ java.lang.Runnable f8652k; // = new com.fossil.blesdk.obfuscated.C2732qf.C2736c();

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.lang.Runnable f8653l; // = new com.fossil.blesdk.obfuscated.C2732qf.C2737d();

    @DexIgnore
    /* renamed from: m */
    public /* final */ java.lang.Runnable f8654m; // = new com.fossil.blesdk.obfuscated.C2732qf.C2738e();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$a")
    /* renamed from: com.fossil.blesdk.obfuscated.qf$a */
    public class C2733a extends com.fossil.blesdk.obfuscated.C2406mf.C2407a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qf$a$a */
        public class C2734a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ java.lang.String[] f8656e;

            @DexIgnore
            public C2734a(java.lang.String[] strArr) {
                this.f8656e = strArr;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C2732qf.this.f8645d.mo14682a(this.f8656e);
            }
        }

        @DexIgnore
        public C2733a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13602a(java.lang.String[] strArr) {
            com.fossil.blesdk.obfuscated.C2732qf.this.f8648g.execute(new com.fossil.blesdk.obfuscated.C2732qf.C2733a.C2734a(strArr));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$b")
    /* renamed from: com.fossil.blesdk.obfuscated.qf$b */
    public class C2735b implements android.content.ServiceConnection {
        @DexIgnore
        public C2735b() {
        }

        @DexIgnore
        public void onServiceConnected(android.content.ComponentName componentName, android.os.IBinder iBinder) {
            com.fossil.blesdk.obfuscated.C2732qf.this.f8647f = com.fossil.blesdk.obfuscated.C2486nf.C2487a.m11229a(iBinder);
            com.fossil.blesdk.obfuscated.C2732qf qfVar = com.fossil.blesdk.obfuscated.C2732qf.this;
            qfVar.f8648g.execute(qfVar.f8652k);
        }

        @DexIgnore
        public void onServiceDisconnected(android.content.ComponentName componentName) {
            com.fossil.blesdk.obfuscated.C2732qf qfVar = com.fossil.blesdk.obfuscated.C2732qf.this;
            qfVar.f8648g.execute(qfVar.f8653l);
            com.fossil.blesdk.obfuscated.C2732qf qfVar2 = com.fossil.blesdk.obfuscated.C2732qf.this;
            qfVar2.f8647f = null;
            qfVar2.f8642a = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$c")
    /* renamed from: com.fossil.blesdk.obfuscated.qf$c */
    public class C2736c implements java.lang.Runnable {
        @DexIgnore
        public C2736c() {
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C2486nf nfVar = com.fossil.blesdk.obfuscated.C2732qf.this.f8647f;
                if (nfVar != null) {
                    com.fossil.blesdk.obfuscated.C2732qf.this.f8644c = nfVar.mo3258a(com.fossil.blesdk.obfuscated.C2732qf.this.f8649h, com.fossil.blesdk.obfuscated.C2732qf.this.f8643b);
                    com.fossil.blesdk.obfuscated.C2732qf.this.f8645d.mo14681a(com.fossil.blesdk.obfuscated.C2732qf.this.f8646e);
                }
            } catch (android.os.RemoteException e) {
                android.util.Log.w("ROOM", "Cannot register multi-instance invalidation callback", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$d")
    /* renamed from: com.fossil.blesdk.obfuscated.qf$d */
    public class C2737d implements java.lang.Runnable {
        @DexIgnore
        public C2737d() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2732qf qfVar = com.fossil.blesdk.obfuscated.C2732qf.this;
            qfVar.f8645d.mo14690c(qfVar.f8646e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$e")
    /* renamed from: com.fossil.blesdk.obfuscated.qf$e */
    public class C2738e implements java.lang.Runnable {
        @DexIgnore
        public C2738e() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2732qf qfVar = com.fossil.blesdk.obfuscated.C2732qf.this;
            qfVar.f8645d.mo14690c(qfVar.f8646e);
            try {
                com.fossil.blesdk.obfuscated.C2486nf nfVar = com.fossil.blesdk.obfuscated.C2732qf.this.f8647f;
                if (nfVar != null) {
                    nfVar.mo3260a(com.fossil.blesdk.obfuscated.C2732qf.this.f8649h, com.fossil.blesdk.obfuscated.C2732qf.this.f8644c);
                }
            } catch (android.os.RemoteException e) {
                android.util.Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e);
            }
            com.fossil.blesdk.obfuscated.C2732qf qfVar2 = com.fossil.blesdk.obfuscated.C2732qf.this;
            android.content.Context context = qfVar2.f8642a;
            if (context != null) {
                context.unbindService(qfVar2.f8651j);
                com.fossil.blesdk.obfuscated.C2732qf.this.f8642a = null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$f")
    /* renamed from: com.fossil.blesdk.obfuscated.qf$f */
    public class C2739f extends com.fossil.blesdk.obfuscated.C2647pf.C2650c {
        @DexIgnore
        public C2739f(java.lang.String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        public void onInvalidated(java.util.Set<java.lang.String> set) {
            if (!com.fossil.blesdk.obfuscated.C2732qf.this.f8650i.get()) {
                try {
                    com.fossil.blesdk.obfuscated.C2732qf.this.f8647f.mo3259a(com.fossil.blesdk.obfuscated.C2732qf.this.f8644c, (java.lang.String[]) set.toArray(new java.lang.String[0]));
                } catch (android.os.RemoteException e) {
                    android.util.Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public C2732qf(android.content.Context context, java.lang.String str, com.fossil.blesdk.obfuscated.C2647pf pfVar, java.util.concurrent.Executor executor) {
        this.f8642a = context.getApplicationContext();
        this.f8643b = str;
        this.f8645d = pfVar;
        this.f8648g = executor;
        this.f8646e = new com.fossil.blesdk.obfuscated.C2732qf.C2739f(pfVar.f8353b);
        this.f8642a.bindService(new android.content.Intent(this.f8642a, androidx.room.MultiInstanceInvalidationService.class), this.f8651j, 1);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15214a() {
        if (this.f8650i.compareAndSet(false, true)) {
            this.f8648g.execute(this.f8654m);
        }
    }
}
