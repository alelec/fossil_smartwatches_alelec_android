package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sc1 {
    @DexIgnore
    public static /* final */ de0.g<f41> a; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0.a<f41, Object> b; // = new cd1();
    @DexIgnore
    public static /* final */ de0<Object> c; // = new de0<>("LocationServices.API", b, a);
    @DexIgnore
    @Deprecated
    public static /* final */ nc1 d; // = new x41();
    @DexIgnore
    @Deprecated
    public static /* final */ xc1 e; // = new n41();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<R extends me0> extends te0<R, f41> {
        @DexIgnore
        public a(ge0 ge0) {
            super(sc1.c, ge0);
        }
    }

    /*
    static {
        new q31();
    }
    */

    @DexIgnore
    public static oc1 a(Context context) {
        return new oc1(context);
    }

    @DexIgnore
    public static yc1 b(Context context) {
        return new yc1(context);
    }
}
