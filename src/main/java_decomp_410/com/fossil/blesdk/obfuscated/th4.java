package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.blesdk.obfuscated.hh4;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class th4 extends uh4 implements hh4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater h;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater i;
    @DexIgnore
    public volatile Object _delayed; // = null;
    @DexIgnore
    public volatile Object _queue; // = null;
    @DexIgnore
    public volatile boolean isCompleted;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends c {
        @DexIgnore
        public /* final */ dg4<qa4> h;
        @DexIgnore
        public /* final */ /* synthetic */ th4 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(th4 th4, long j, dg4<? super qa4> dg4) {
            super(j);
            kd4.b(dg4, "cont");
            this.i = th4;
            this.h = dg4;
        }

        @DexIgnore
        public void run() {
            this.h.a((ug4) this.i, qa4.a);
        }

        @DexIgnore
        public String toString() {
            return super.toString() + this.h.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c {
        @DexIgnore
        public /* final */ Runnable h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(long j, Runnable runnable) {
            super(j);
            kd4.b(runnable, "block");
            this.h = runnable;
        }

        @DexIgnore
        public void run() {
            this.h.run();
        }

        @DexIgnore
        public String toString() {
            return super.toString() + this.h.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends hk4<c> {
        @DexIgnore
        public long b;

        @DexIgnore
        public d(long j) {
            this.b = j;
        }
    }

    /*
    static {
        Class<Object> cls = Object.class;
        Class<th4> cls2 = th4.class;
        h = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_queue");
        i = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_delayed");
    }
    */

    @DexIgnore
    public long C() {
        if (super.C() == 0) {
            return 0;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof uj4) {
                if (!((uj4) obj).c()) {
                    return 0;
                }
            } else if (obj == wh4.b) {
                return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
            } else {
                return 0;
            }
        }
        d dVar = (d) this._delayed;
        if (dVar != null) {
            c cVar = (c) dVar.d();
            if (cVar != null) {
                long j = cVar.g;
                cj4 a2 = dj4.a();
                return ee4.a(j - (a2 != null ? a2.a() : System.nanoTime()), 0);
            }
        }
        return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0055  */
    public long F() {
        Runnable L;
        ik4 ik4;
        if (G()) {
            return C();
        }
        d dVar = (d) this._delayed;
        if (dVar == null || dVar.c()) {
            L = L();
            if (L != null) {
                L.run();
            }
            return C();
        }
        cj4 a2 = dj4.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        do {
            synchronized (dVar) {
                ik4 a4 = dVar.a();
                ik4 = null;
                if (a4 != null) {
                    c cVar = (c) a4;
                    if (cVar.a(a3) ? b(cVar) : false) {
                        ik4 = dVar.a(0);
                    }
                }
            }
        } while (((c) ik4) != null);
        L = L();
        if (L != null) {
        }
        return C();
    }

    @DexIgnore
    public final void K() {
        if (!ch4.a() || this.isCompleted) {
            while (true) {
                Object obj = this._queue;
                if (obj == null) {
                    if (h.compareAndSet(this, (Object) null, wh4.b)) {
                        return;
                    }
                } else if (obj instanceof uj4) {
                    ((uj4) obj).a();
                    return;
                } else if (obj != wh4.b) {
                    uj4 uj4 = new uj4(8, true);
                    if (obj != null) {
                        uj4.a((Runnable) obj);
                        if (h.compareAndSet(this, obj, uj4)) {
                            return;
                        }
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Runnable L() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof uj4) {
                if (obj != null) {
                    uj4 uj4 = (uj4) obj;
                    Object f = uj4.f();
                    if (f != uj4.g) {
                        return (Runnable) f;
                    }
                    h.compareAndSet(this, obj, uj4.e());
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == wh4.b) {
                return null;
            } else {
                if (h.compareAndSet(this, obj, (Object) null)) {
                    if (obj != null) {
                        return (Runnable) obj;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public boolean M() {
        if (!E()) {
            return false;
        }
        d dVar = (d) this._delayed;
        if (dVar != null && !dVar.c()) {
            return false;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof uj4) {
                return ((uj4) obj).c();
            }
            if (obj == wh4.b) {
                return true;
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void N() {
        cj4 a2 = dj4.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        while (true) {
            d dVar = (d) this._delayed;
            if (dVar != null) {
                c cVar = (c) dVar.f();
                if (cVar != null) {
                    a(a3, cVar);
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void O() {
        this._queue = null;
        this._delayed = null;
    }

    @DexIgnore
    public final oh4 b(long j, Runnable runnable) {
        kd4.b(runnable, "block");
        long a2 = wh4.a(j);
        if (a2 >= 4611686018427387903L) {
            return ri4.e;
        }
        cj4 a3 = dj4.a();
        long a4 = a3 != null ? a3.a() : System.nanoTime();
        b bVar = new b(a2 + a4, runnable);
        b(a4, (c) bVar);
        return bVar;
    }

    @DexIgnore
    public final int c(long j, c cVar) {
        if (this.isCompleted) {
            return 1;
        }
        d dVar = (d) this._delayed;
        if (dVar == null) {
            i.compareAndSet(this, (Object) null, new d(j));
            Object obj = this._delayed;
            if (obj != null) {
                dVar = (d) obj;
            } else {
                kd4.a();
                throw null;
            }
        }
        return cVar.a(j, dVar, this);
    }

    @DexIgnore
    public void shutdown() {
        bj4.b.c();
        this.isCompleted = true;
        K();
        do {
        } while (F() <= 0);
        N();
    }

    @DexIgnore
    public oh4 a(long j, Runnable runnable) {
        kd4.b(runnable, "block");
        return hh4.a.a(this, j, runnable);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c implements Runnable, Comparable<c>, oh4, ik4 {
        @DexIgnore
        public Object e;
        @DexIgnore
        public int f; // = -1;
        @DexIgnore
        public long g;

        @DexIgnore
        public c(long j) {
            this.g = j;
        }

        @DexIgnore
        public void a(hk4<?> hk4) {
            if (this.e != wh4.a) {
                this.e = hk4;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @DexIgnore
        public final synchronized void dispose() {
            Object obj = this.e;
            if (obj != wh4.a) {
                if (!(obj instanceof d)) {
                    obj = null;
                }
                d dVar = (d) obj;
                if (dVar != null) {
                    dVar.b(this);
                }
                this.e = wh4.a;
            }
        }

        @DexIgnore
        public hk4<?> i() {
            Object obj = this.e;
            if (!(obj instanceof hk4)) {
                obj = null;
            }
            return (hk4) obj;
        }

        @DexIgnore
        public int j() {
            return this.f;
        }

        @DexIgnore
        public String toString() {
            return "Delayed[nanos=" + this.g + ']';
        }

        @DexIgnore
        public void a(int i) {
            this.f = i;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            kd4.b(cVar, FacebookRequestErrorClassification.KEY_OTHER);
            int i = ((this.g - cVar.g) > 0 ? 1 : ((this.g - cVar.g) == 0 ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @DexIgnore
        public final boolean a(long j) {
            return j - this.g >= 0;
        }

        @DexIgnore
        public final synchronized int a(long j, d dVar, th4 th4) {
            kd4.b(dVar, "delayed");
            kd4.b(th4, "eventLoop");
            if (this.e == wh4.a) {
                return 2;
            }
            synchronized (dVar) {
                c cVar = (c) dVar.a();
                if (th4.isCompleted) {
                    return 1;
                }
                if (cVar == null) {
                    dVar.b = j;
                } else {
                    long j2 = cVar.g;
                    if (j2 - j < 0) {
                        j = j2;
                    }
                    if (j - dVar.b > 0) {
                        dVar.b = j;
                    }
                }
                if (this.g - dVar.b < 0) {
                    this.g = dVar.b;
                }
                dVar.a(this);
                return 0;
            }
        }
    }

    @DexIgnore
    public void a(long j, dg4<? super qa4> dg4) {
        kd4.b(dg4, "continuation");
        long a2 = wh4.a(j);
        if (a2 < 4611686018427387903L) {
            cj4 a3 = dj4.a();
            long a4 = a3 != null ? a3.a() : System.nanoTime();
            a aVar = new a(this, a2 + a4, dg4);
            fg4.a((dg4<?>) dg4, (oh4) aVar);
            b(a4, (c) aVar);
        }
    }

    @DexIgnore
    public final void b(long j, c cVar) {
        kd4.b(cVar, "delayedTask");
        int c2 = c(j, cVar);
        if (c2 != 0) {
            if (c2 == 1) {
                a(j, cVar);
            } else if (c2 != 2) {
                throw new IllegalStateException("unexpected result".toString());
            }
        } else if (a(cVar)) {
            J();
        }
    }

    @DexIgnore
    public final void a(CoroutineContext coroutineContext, Runnable runnable) {
        kd4.b(coroutineContext, "context");
        kd4.b(runnable, "block");
        a(runnable);
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        kd4.b(runnable, "task");
        if (b(runnable)) {
            J();
        } else {
            eh4.k.a(runnable);
        }
    }

    @DexIgnore
    public final boolean b(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (this.isCompleted) {
                return false;
            }
            if (obj == null) {
                if (h.compareAndSet(this, (Object) null, runnable)) {
                    return true;
                }
            } else if (obj instanceof uj4) {
                if (obj != null) {
                    uj4 uj4 = (uj4) obj;
                    int a2 = uj4.a(runnable);
                    if (a2 == 0) {
                        return true;
                    }
                    if (a2 == 1) {
                        h.compareAndSet(this, obj, uj4.e());
                    } else if (a2 == 2) {
                        return false;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == wh4.b) {
                return false;
            } else {
                uj4 uj42 = new uj4(8, true);
                if (obj != null) {
                    uj42.a((Runnable) obj);
                    uj42.a(runnable);
                    if (h.compareAndSet(this, obj, uj42)) {
                        return true;
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(c cVar) {
        d dVar = (d) this._delayed;
        return (dVar != null ? (c) dVar.d() : null) == cVar;
    }
}
