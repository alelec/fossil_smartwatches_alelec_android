package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zh */
public class C3437zh implements com.fossil.blesdk.obfuscated.C1418ai {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.ViewOverlay f11553a;

    @DexIgnore
    public C3437zh(android.view.View view) {
        this.f11553a = view.getOverlay();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8741a(android.graphics.drawable.Drawable drawable) {
        this.f11553a.add(drawable);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8742b(android.graphics.drawable.Drawable drawable) {
        this.f11553a.remove(drawable);
    }
}
