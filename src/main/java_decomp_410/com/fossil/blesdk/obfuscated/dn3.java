package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.model.ShineDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class dn3 extends u52 {
    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(ShineDevice shineDevice);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(ShineDevice shineDevice);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public abstract ShineDevice h();

    @DexIgnore
    public abstract boolean i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();
}
