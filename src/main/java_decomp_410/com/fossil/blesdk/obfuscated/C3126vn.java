package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vn */
public final class C3126vn {
    @DexIgnore
    public static /* final */ int action_container; // = 2131361824;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361826;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361827;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361833;
    @DexIgnore
    public static /* final */ int actions; // = 2131361834;
    @DexIgnore
    public static /* final */ int async; // = 2131361854;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361872;
    @DexIgnore
    public static /* final */ int bottom; // = 2131361873;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131361945;
    @DexIgnore
    public static /* final */ int end; // = 2131362079;
    @DexIgnore
    public static /* final */ int forever; // = 2131362141;
    @DexIgnore
    public static /* final */ int glide_custom_view_target_tag; // = 2131362324;
    @DexIgnore
    public static /* final */ int icon; // = 2131362359;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362360;
    @DexIgnore
    public static /* final */ int info; // = 2131362371;
    @DexIgnore
    public static /* final */ int italic; // = 2131362382;
    @DexIgnore
    public static /* final */ int left; // = 2131362479;
    @DexIgnore
    public static /* final */ int line1; // = 2131362482;
    @DexIgnore
    public static /* final */ int line3; // = 2131362483;
    @DexIgnore
    public static /* final */ int none; // = 2131362539;
    @DexIgnore
    public static /* final */ int normal; // = 2131362540;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362541;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362542;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362543;
    @DexIgnore
    public static /* final */ int right; // = 2131362617;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362618;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362619;
    @DexIgnore
    public static /* final */ int start; // = 2131362737;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131362759;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131362760;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131362761;
    @DexIgnore
    public static /* final */ int text; // = 2131362765;
    @DexIgnore
    public static /* final */ int text2; // = 2131362766;
    @DexIgnore
    public static /* final */ int time; // = 2131362777;
    @DexIgnore
    public static /* final */ int title; // = 2131362779;
    @DexIgnore
    public static /* final */ int top; // = 2131362787;
}
