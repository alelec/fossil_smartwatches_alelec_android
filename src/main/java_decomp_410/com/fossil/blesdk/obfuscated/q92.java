package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageButton;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class q92 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ AppCompatImageView r;
    @DexIgnore
    public /* final */ RTLImageButton s;
    @DexIgnore
    public /* final */ RTLImageButton t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ DashBar x;

    @DexIgnore
    public q92(Object obj, View view, int i, RTLImageView rTLImageView, AppCompatImageView appCompatImageView, RTLImageButton rTLImageButton, RTLImageButton rTLImageButton2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, FlexibleTextView flexibleTextView, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView2, DashBar dashBar) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = appCompatImageView;
        this.s = rTLImageButton;
        this.t = rTLImageButton2;
        this.u = flexibleTextView;
        this.v = flexibleButton;
        this.w = flexibleTextView2;
        this.x = dashBar;
    }
}
