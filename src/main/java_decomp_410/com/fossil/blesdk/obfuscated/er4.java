package com.fossil.blesdk.obfuscated;

import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface er4<T> {
    @DexIgnore
    void onFailure(Call<T> call, Throwable th);

    @DexIgnore
    void onResponse(Call<T> call, qr4<T> qr4);
}
