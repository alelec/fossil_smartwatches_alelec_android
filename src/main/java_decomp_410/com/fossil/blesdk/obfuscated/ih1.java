package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ih1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ /* synthetic */ fh1 e;

    @DexIgnore
    public ih1(fh1 fh1, String str, long j) {
        this.e = fh1;
        bk0.b(str);
        this.a = str;
        this.b = j;
    }

    @DexIgnore
    public final long a() {
        if (!this.c) {
            this.c = true;
            this.d = this.e.s().getLong(this.a, this.b);
        }
        return this.d;
    }

    @DexIgnore
    public final void a(long j) {
        SharedPreferences.Editor edit = this.e.s().edit();
        edit.putLong(this.a, j);
        edit.apply();
        this.d = j;
    }
}
