package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import android.view.PointerIcon;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c9 {
    @DexIgnore
    public Object a;

    @DexIgnore
    public c9(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public static c9 a(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 24) {
            return new c9(PointerIcon.getSystemIcon(context, i));
        }
        return new c9((Object) null);
    }
}
