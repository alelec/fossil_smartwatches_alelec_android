package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.NotificationFlag;
import java.util.ArrayList;
import kotlin.TypeCastException;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y20 {
    @DexIgnore
    public static final JSONArray a(NotificationFlag[] notificationFlagArr) {
        kd4.b(notificationFlagArr, "$this$toJSONArray");
        JSONArray jSONArray = new JSONArray();
        for (NotificationFlag logName$blesdk_productionRelease : notificationFlagArr) {
            jSONArray.put(logName$blesdk_productionRelease.getLogName$blesdk_productionRelease());
        }
        return jSONArray;
    }

    @DexIgnore
    public static final String[] b(NotificationFlag[] notificationFlagArr) {
        kd4.b(notificationFlagArr, "$this$toStringArray");
        ArrayList arrayList = new ArrayList(notificationFlagArr.length);
        for (NotificationFlag name : notificationFlagArr) {
            arrayList.add(name.name());
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return (String[]) array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
