package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ha */
public abstract class C1931ha implements android.os.Parcelable {
    @DexIgnore
    public static /* final */ android.os.Parcelable.Creator<com.fossil.blesdk.obfuscated.C1931ha> CREATOR; // = new com.fossil.blesdk.obfuscated.C1931ha.C1933b();

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C1931ha f5706f; // = new com.fossil.blesdk.obfuscated.C1931ha.C1932a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.os.Parcelable f5707e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ha$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ha$a */
    public static class C1932a extends com.fossil.blesdk.obfuscated.C1931ha {
        @DexIgnore
        public C1932a() {
            super((com.fossil.blesdk.obfuscated.C1931ha.C1932a) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ha$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ha$b */
    public static class C1933b implements android.os.Parcelable.ClassLoaderCreator<com.fossil.blesdk.obfuscated.C1931ha> {
        @DexIgnore
        public com.fossil.blesdk.obfuscated.C1931ha[] newArray(int i) {
            return new com.fossil.blesdk.obfuscated.C1931ha[i];
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C1931ha createFromParcel(android.os.Parcel parcel, java.lang.ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return com.fossil.blesdk.obfuscated.C1931ha.f5706f;
            }
            throw new java.lang.IllegalStateException("superState must be null");
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C1931ha createFromParcel(android.os.Parcel parcel) {
            return createFromParcel(parcel, (java.lang.ClassLoader) null);
        }
    }

    @DexIgnore
    public /* synthetic */ C1931ha(com.fossil.blesdk.obfuscated.C1931ha.C1932a aVar) {
        this();
    }

    @DexIgnore
    /* renamed from: a */
    public final android.os.Parcelable mo11607a() {
        return this.f5707e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(android.os.Parcel parcel, int i) {
        parcel.writeParcelable(this.f5707e, i);
    }

    @DexIgnore
    public C1931ha() {
        this.f5707e = null;
    }

    @DexIgnore
    public C1931ha(android.os.Parcelable parcelable) {
        if (parcelable != null) {
            this.f5707e = parcelable == f5706f ? null : parcelable;
            return;
        }
        throw new java.lang.IllegalArgumentException("superState must not be null");
    }

    @DexIgnore
    public C1931ha(android.os.Parcel parcel, java.lang.ClassLoader classLoader) {
        this.f5707e = parcel.readParcelable(classLoader) == null ? f5706f : parcel.readParcelable(classLoader);
    }
}
