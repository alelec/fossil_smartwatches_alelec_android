package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class se0 implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    @DexIgnore
    public static /* final */ se0 i; // = new se0();
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ ArrayList<a> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h; // = false;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public static void a(Application application) {
        synchronized (i) {
            if (!i.h) {
                application.registerActivityLifecycleCallbacks(i);
                application.registerComponentCallbacks(i);
                i.h = true;
            }
        }
    }

    @DexIgnore
    public static se0 b() {
        return i;
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.e.compareAndSet(true, false);
        this.f.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.e.compareAndSet(true, false);
        this.f.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }

    @DexIgnore
    public final void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public final void onLowMemory() {
    }

    @DexIgnore
    public final void onTrimMemory(int i2) {
        if (i2 == 20 && this.e.compareAndSet(false, true)) {
            this.f.set(true);
            a(true);
        }
    }

    @DexIgnore
    @TargetApi(16)
    public final boolean b(boolean z) {
        if (!this.f.get()) {
            if (!pm0.c()) {
                return z;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.f.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.e.set(true);
            }
        }
        return a();
    }

    @DexIgnore
    public final boolean a() {
        return this.e.get();
    }

    @DexIgnore
    public final void a(a aVar) {
        synchronized (i) {
            this.g.add(aVar);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        synchronized (i) {
            ArrayList<a> arrayList = this.g;
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                a aVar = arrayList.get(i2);
                i2++;
                aVar.a(z);
            }
        }
    }
}
