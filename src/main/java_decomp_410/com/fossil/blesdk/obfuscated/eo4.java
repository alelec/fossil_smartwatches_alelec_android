package com.fossil.blesdk.obfuscated;

import java.security.cert.Certificate;
import java.util.List;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.X509TrustManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eo4 {
    @DexIgnore
    public static eo4 a(X509TrustManager x509TrustManager) {
        return ao4.d().a(x509TrustManager);
    }

    @DexIgnore
    public abstract List<Certificate> a(List<Certificate> list, String str) throws SSLPeerUnverifiedException;
}
