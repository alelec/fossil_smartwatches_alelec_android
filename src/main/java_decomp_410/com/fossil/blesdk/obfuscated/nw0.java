package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nw0 extends mw0<FieldDescriptorType, Object> {
    @DexIgnore
    public nw0(int i) {
        super(i, (nw0) null);
    }

    @DexIgnore
    public final void g() {
        if (!a()) {
            for (int i = 0; i < b(); i++) {
                Map.Entry a = a(i);
                if (((nu0) a.getKey()).c()) {
                    a.setValue(Collections.unmodifiableList((List) a.getValue()));
                }
            }
            for (Map.Entry entry : c()) {
                if (((nu0) entry.getKey()).c()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.g();
    }
}
