package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ax0<T, B> {
    @DexIgnore
    public abstract B a();

    @DexIgnore
    public abstract void a(Object obj);

    @DexIgnore
    public abstract void a(B b, int i, long j);

    @DexIgnore
    public abstract void a(T t, ox0 ox0) throws IOException;

    @DexIgnore
    public abstract void a(Object obj, T t);

    @DexIgnore
    public abstract int b(T t);

    @DexIgnore
    public abstract void b(T t, ox0 ox0) throws IOException;

    @DexIgnore
    public abstract void b(Object obj, B b);

    @DexIgnore
    public abstract T c(Object obj);

    @DexIgnore
    public abstract T c(T t, T t2);

    @DexIgnore
    public abstract int d(T t);
}
