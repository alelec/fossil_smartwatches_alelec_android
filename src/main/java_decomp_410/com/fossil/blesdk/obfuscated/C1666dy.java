package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dy */
public class C1666dy implements com.fossil.blesdk.obfuscated.o64<com.crashlytics.android.answers.SessionEvent> {
    @DexIgnore
    /* renamed from: b */
    public byte[] mo10154a(com.crashlytics.android.answers.SessionEvent sessionEvent) throws java.io.IOException {
        return mo10153a(sessionEvent).toString().getBytes("UTF-8");
    }

    @DexIgnore
    @android.annotation.TargetApi(9)
    /* renamed from: a */
    public org.json.JSONObject mo10153a(com.crashlytics.android.answers.SessionEvent sessionEvent) throws java.io.IOException {
        try {
            org.json.JSONObject jSONObject = new org.json.JSONObject();
            com.fossil.blesdk.obfuscated.C1587cy cyVar = sessionEvent.f2164a;
            jSONObject.put("appBundleId", cyVar.f4214a);
            jSONObject.put("executionId", cyVar.f4215b);
            jSONObject.put("installationId", cyVar.f4216c);
            jSONObject.put("limitAdTrackingEnabled", cyVar.f4217d);
            jSONObject.put("betaDeviceToken", cyVar.f4218e);
            jSONObject.put("buildId", cyVar.f4219f);
            jSONObject.put("osVersion", cyVar.f4220g);
            jSONObject.put("deviceModel", cyVar.f4221h);
            jSONObject.put("appVersionCode", cyVar.f4222i);
            jSONObject.put("appVersionName", cyVar.f4223j);
            jSONObject.put("timestamp", sessionEvent.f2165b);
            jSONObject.put("type", sessionEvent.f2166c.toString());
            if (sessionEvent.f2167d != null) {
                jSONObject.put("details", new org.json.JSONObject(sessionEvent.f2167d));
            }
            jSONObject.put("customType", sessionEvent.f2168e);
            if (sessionEvent.f2169f != null) {
                jSONObject.put("customAttributes", new org.json.JSONObject(sessionEvent.f2169f));
            }
            jSONObject.put("predefinedType", sessionEvent.f2170g);
            if (sessionEvent.f2171h != null) {
                jSONObject.put("predefinedAttributes", new org.json.JSONObject(sessionEvent.f2171h));
            }
            return jSONObject;
        } catch (org.json.JSONException e) {
            if (android.os.Build.VERSION.SDK_INT >= 9) {
                throw new java.io.IOException(e.getMessage(), e);
            }
            throw new java.io.IOException(e.getMessage());
        }
    }
}
