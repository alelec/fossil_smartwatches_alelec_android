package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.rk */
public class C2812rk extends com.fossil.blesdk.obfuscated.C2971tk<java.lang.Boolean> {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.lang.String f9008i; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("BatteryChrgTracker");

    @DexIgnore
    public C2812rk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(context, zlVar);
    }

    @DexIgnore
    /* renamed from: d */
    public android.content.IntentFilter mo15616d() {
        android.content.IntentFilter intentFilter = new android.content.IntentFilter();
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            intentFilter.addAction("android.os.action.CHARGING");
            intentFilter.addAction("android.os.action.DISCHARGING");
        } else {
            intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
            intentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        }
        return intentFilter;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Boolean m13249a() {
        android.content.Intent registerReceiver = this.f10017b.registerReceiver((android.content.BroadcastReceiver) null, new android.content.IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            return java.lang.Boolean.valueOf(mo15615a(registerReceiver));
        }
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f9008i, "getInitialState - null intent received", new java.lang.Throwable[0]);
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15614a(android.content.Context context, android.content.Intent intent) {
        java.lang.String action = intent.getAction();
        if (action != null) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9008i, java.lang.String.format("Received %s", new java.lang.Object[]{action}), new java.lang.Throwable[0]);
            char c = 65535;
            switch (action.hashCode()) {
                case -1886648615:
                    if (action.equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                        c = 3;
                        break;
                    }
                    break;
                case -54942926:
                    if (action.equals("android.os.action.DISCHARGING")) {
                        c = 1;
                        break;
                    }
                    break;
                case 948344062:
                    if (action.equals("android.os.action.CHARGING")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1019184907:
                    if (action.equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                mo16804a(true);
            } else if (c == 1) {
                mo16804a(false);
            } else if (c == 2) {
                mo16804a(true);
            } else if (c == 3) {
                mo16804a(false);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo15615a(android.content.Intent intent) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            int intExtra = intent.getIntExtra("status", -1);
            if (intExtra == 2 || intExtra == 5) {
                return true;
            }
        } else if (intent.getIntExtra("plugged", 0) != 0) {
            return true;
        }
        return false;
    }
}
