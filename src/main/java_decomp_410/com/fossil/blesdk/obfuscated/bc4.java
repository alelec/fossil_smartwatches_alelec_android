package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.intrinsics.CoroutineSingletons;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bc4<T> implements yb4<T>, fc4 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater<bc4<?>, Object> g; // = AtomicReferenceFieldUpdater.newUpdater(bc4.class, Object.class, "e");
    @DexIgnore
    public volatile Object e;
    @DexIgnore
    public /* final */ yb4<T> f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public bc4(yb4<? super T> yb4, Object obj) {
        kd4.b(yb4, "delegate");
        this.f = yb4;
        this.e = obj;
    }

    @DexIgnore
    public final Object a() {
        Object obj = this.e;
        CoroutineSingletons coroutineSingletons = CoroutineSingletons.UNDECIDED;
        if (obj == coroutineSingletons) {
            if (g.compareAndSet(this, coroutineSingletons, cc4.a())) {
                return cc4.a();
            }
            obj = this.e;
        }
        if (obj == CoroutineSingletons.RESUMED) {
            return cc4.a();
        }
        if (!(obj instanceof Result.Failure)) {
            return obj;
        }
        throw ((Result.Failure) obj).exception;
    }

    @DexIgnore
    public fc4 getCallerFrame() {
        yb4<T> yb4 = this.f;
        if (!(yb4 instanceof fc4)) {
            yb4 = null;
        }
        return (fc4) yb4;
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return this.f.getContext();
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.e;
            CoroutineSingletons coroutineSingletons = CoroutineSingletons.UNDECIDED;
            if (obj2 == coroutineSingletons) {
                if (g.compareAndSet(this, coroutineSingletons, obj)) {
                    return;
                }
            } else if (obj2 != cc4.a()) {
                throw new IllegalStateException("Already resumed");
            } else if (g.compareAndSet(this, cc4.a(), CoroutineSingletons.RESUMED)) {
                this.f.resumeWith(obj);
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return "SafeContinuation for " + this.f;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public bc4(yb4<? super T> yb4) {
        this(yb4, CoroutineSingletons.UNDECIDED);
        kd4.b(yb4, "delegate");
    }
}
