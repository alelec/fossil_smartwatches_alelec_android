package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.fi4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class gi4<J extends fi4> extends ki4<J> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gi4(J j) {
        super(j);
        kd4.b(j, "job");
    }
}
