package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.le4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface me4<T, R> extends le4<R>, xc4<T, R> {

    @DexIgnore
    public interface a<T, R> extends le4.b<R>, xc4<T, R> {
    }

    @DexIgnore
    R get(T t);

    @DexIgnore
    Object getDelegate(T t);

    @DexIgnore
    a<T, R> getGetter();
}
