package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.u8 */
public final class C3020u8 {
    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public static int m14622a(android.view.MotionEvent motionEvent) {
        return motionEvent.getActionMasked();
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m14623a(android.view.MotionEvent motionEvent, int i) {
        return (motionEvent.getSource() & i) == i;
    }
}
