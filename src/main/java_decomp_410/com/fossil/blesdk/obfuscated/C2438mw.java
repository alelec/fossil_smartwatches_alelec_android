package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.mw */
public final class C2438mw extends java.io.FilterInputStream {

    @DexIgnore
    /* renamed from: e */
    public /* final */ long f7588e;

    @DexIgnore
    /* renamed from: f */
    public int f7589f;

    @DexIgnore
    public C2438mw(java.io.InputStream inputStream, long j) {
        super(inputStream);
        this.f7588e = j;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.io.InputStream m10869a(java.io.InputStream inputStream, long j) {
        return new com.fossil.blesdk.obfuscated.C2438mw(inputStream, j);
    }

    @DexIgnore
    public synchronized int available() throws java.io.IOException {
        return (int) java.lang.Math.max(this.f7588e - ((long) this.f7589f), (long) this.in.available());
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo13722b(int i) throws java.io.IOException {
        if (i >= 0) {
            this.f7589f += i;
        } else if (this.f7588e - ((long) this.f7589f) > 0) {
            throw new java.io.IOException("Failed to read all expected data, expected: " + this.f7588e + ", but read: " + this.f7589f);
        }
        return i;
    }

    @DexIgnore
    public synchronized int read() throws java.io.IOException {
        int read;
        read = super.read();
        mo13722b(read >= 0 ? 1 : -1);
        return read;
    }

    @DexIgnore
    public int read(byte[] bArr) throws java.io.IOException {
        return read(bArr, 0, bArr.length);
    }

    @DexIgnore
    public synchronized int read(byte[] bArr, int i, int i2) throws java.io.IOException {
        int read;
        read = super.read(bArr, i, i2);
        mo13722b(read);
        return read;
    }
}
