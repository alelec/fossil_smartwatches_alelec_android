package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f73 implements Factory<SearchMicroAppPresenter> {
    @DexIgnore
    public static SearchMicroAppPresenter a(b73 b73, MicroAppRepository microAppRepository, en2 en2, PortfolioApp portfolioApp) {
        return new SearchMicroAppPresenter(b73, microAppRepository, en2, portfolioApp);
    }
}
