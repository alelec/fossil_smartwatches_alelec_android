package com.fossil.blesdk.obfuscated;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.measurement.AppMeasurement;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xh1 implements vi1 {
    @DexIgnore
    public static volatile xh1 G;
    @DexIgnore
    public volatile Boolean A;
    @DexIgnore
    public Boolean B;
    @DexIgnore
    public Boolean C;
    @DexIgnore
    public int D;
    @DexIgnore
    public AtomicInteger E; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ long F;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ ul1 f;
    @DexIgnore
    public /* final */ xl1 g;
    @DexIgnore
    public /* final */ fh1 h;
    @DexIgnore
    public /* final */ tg1 i;
    @DexIgnore
    public /* final */ th1 j;
    @DexIgnore
    public /* final */ tk1 k;
    @DexIgnore
    public /* final */ AppMeasurement l;
    @DexIgnore
    public /* final */ nl1 m;
    @DexIgnore
    public /* final */ rg1 n;
    @DexIgnore
    public /* final */ hm0 o;
    @DexIgnore
    public /* final */ rj1 p;
    @DexIgnore
    public /* final */ dj1 q;
    @DexIgnore
    public /* final */ ag1 r;
    @DexIgnore
    public pg1 s;
    @DexIgnore
    public vj1 t;
    @DexIgnore
    public bg1 u;
    @DexIgnore
    public ng1 v;
    @DexIgnore
    public lh1 w;
    @DexIgnore
    public boolean x; // = false;
    @DexIgnore
    public Boolean y;
    @DexIgnore
    public long z;

    @DexIgnore
    public xh1(bj1 bj1) {
        bk0.a(bj1);
        this.f = new ul1(bj1.a);
        jg1.a(this.f);
        this.a = bj1.a;
        this.b = bj1.b;
        this.c = bj1.c;
        this.d = bj1.d;
        this.e = bj1.e;
        this.A = bj1.f;
        og1 og1 = bj1.g;
        if (og1 != null) {
            Bundle bundle = og1.g;
            if (bundle != null) {
                Object obj = bundle.get("measurementEnabled");
                if (obj instanceof Boolean) {
                    this.B = (Boolean) obj;
                }
                Object obj2 = og1.g.get("measurementDeactivated");
                if (obj2 instanceof Boolean) {
                    this.C = (Boolean) obj2;
                }
            }
        }
        a71.a(this.a);
        this.o = km0.d();
        this.F = this.o.b();
        this.g = new xl1(this);
        fh1 fh1 = new fh1(this);
        fh1.r();
        this.h = fh1;
        tg1 tg1 = new tg1(this);
        tg1.r();
        this.i = tg1;
        nl1 nl1 = new nl1(this);
        nl1.r();
        this.m = nl1;
        rg1 rg1 = new rg1(this);
        rg1.r();
        this.n = rg1;
        this.r = new ag1(this);
        rj1 rj1 = new rj1(this);
        rj1.z();
        this.p = rj1;
        dj1 dj1 = new dj1(this);
        dj1.z();
        this.q = dj1;
        this.l = new AppMeasurement(this);
        tk1 tk1 = new tk1(this);
        tk1.z();
        this.k = tk1;
        th1 th1 = new th1(this);
        th1.r();
        this.j = th1;
        if (this.a.getApplicationContext() instanceof Application) {
            dj1 k2 = k();
            if (k2.getContext().getApplicationContext() instanceof Application) {
                Application application = (Application) k2.getContext().getApplicationContext();
                if (k2.c == null) {
                    k2.c = new nj1(k2, (ej1) null);
                }
                application.unregisterActivityLifecycleCallbacks(k2.c);
                application.registerActivityLifecycleCallbacks(k2.c);
                k2.d().A().a("Registered activity lifecycle callback");
            }
        } else {
            d().v().a("Application context is not an Application");
        }
        this.j.a((Runnable) new yh1(this, bj1));
    }

    @DexIgnore
    public final String A() {
        return this.b;
    }

    @DexIgnore
    public final String B() {
        return this.c;
    }

    @DexIgnore
    public final String C() {
        return this.d;
    }

    @DexIgnore
    public final boolean D() {
        return this.e;
    }

    @DexIgnore
    public final boolean E() {
        return this.A != null && this.A.booleanValue();
    }

    @DexIgnore
    public final long F() {
        Long valueOf = Long.valueOf(t().j.a());
        if (valueOf.longValue() == 0) {
            return this.F;
        }
        return Math.min(this.F, valueOf.longValue());
    }

    @DexIgnore
    public final void G() {
        this.E.incrementAndGet();
    }

    @DexIgnore
    public final boolean H() {
        g();
        a().e();
        Boolean bool = this.y;
        if (bool == null || this.z == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.o.c() - this.z) > 1000)) {
            this.z = this.o.c();
            boolean z2 = true;
            this.y = Boolean.valueOf(s().d("android.permission.INTERNET") && s().d("android.permission.ACCESS_NETWORK_STATE") && (bn0.b(this.a).a() || this.g.r() || (oh1.a(this.a) && nl1.a(this.a, false))));
            if (this.y.booleanValue()) {
                if (!s().d(l().A(), l().C()) && TextUtils.isEmpty(l().C())) {
                    z2 = false;
                }
                this.y = Boolean.valueOf(z2);
            }
        }
        return this.y.booleanValue();
    }

    @DexIgnore
    public final void a(bj1 bj1) {
        String str;
        vg1 vg1;
        a().e();
        xl1.s();
        bg1 bg1 = new bg1(this);
        bg1.r();
        this.u = bg1;
        ng1 ng1 = new ng1(this);
        ng1.z();
        this.v = ng1;
        pg1 pg1 = new pg1(this);
        pg1.z();
        this.s = pg1;
        vj1 vj1 = new vj1(this);
        vj1.z();
        this.t = vj1;
        this.m.o();
        this.h.o();
        this.w = new lh1(this);
        this.v.w();
        d().y().a("App measurement is starting up, version", Long.valueOf(this.g.n()));
        d().y().a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String B2 = ng1.B();
        if (TextUtils.isEmpty(this.b)) {
            if (s().c(B2)) {
                vg1 = d().y();
                str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                vg1 = d().y();
                String valueOf = String.valueOf(B2);
                str = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
            }
            vg1.a(str);
        }
        d().z().a("Debug-level message logging enabled");
        if (this.D != this.E.get()) {
            d().s().a("Not all components initialized", Integer.valueOf(this.D), Integer.valueOf(this.E.get()));
        }
        this.x = true;
    }

    @DexIgnore
    public final ul1 b() {
        return this.f;
    }

    @DexIgnore
    public final hm0 c() {
        return this.o;
    }

    @DexIgnore
    public final tg1 d() {
        b((ui1) this.i);
        return this.i;
    }

    @DexIgnore
    public final boolean e() {
        boolean z2;
        a().e();
        g();
        if (this.g.a(jg1.n0)) {
            if (this.g.o()) {
                return false;
            }
            Boolean bool = this.C;
            if (bool != null && bool.booleanValue()) {
                return false;
            }
            Boolean x2 = t().x();
            if (x2 != null) {
                return x2.booleanValue();
            }
            Boolean p2 = this.g.p();
            if (p2 != null) {
                return p2.booleanValue();
            }
            Boolean bool2 = this.B;
            if (bool2 != null) {
                return bool2.booleanValue();
            }
            if (we0.b()) {
                return false;
            }
            if (!this.g.a(jg1.k0) || this.A == null) {
                return true;
            }
            return this.A.booleanValue();
        } else if (this.g.o()) {
            return false;
        } else {
            Boolean p3 = this.g.p();
            if (p3 != null) {
                z2 = p3.booleanValue();
            } else {
                z2 = !we0.b();
                if (z2 && this.A != null && jg1.k0.a().booleanValue()) {
                    z2 = this.A.booleanValue();
                }
            }
            return t().c(z2);
        }
    }

    @DexIgnore
    public final void f() {
        a().e();
        if (t().e.a() == 0) {
            t().e.a(this.o.b());
        }
        if (Long.valueOf(t().j.a()).longValue() == 0) {
            d().A().a("Persisting first open", Long.valueOf(this.F));
            t().j.a(this.F);
        }
        if (H()) {
            if (!TextUtils.isEmpty(l().A()) || !TextUtils.isEmpty(l().C())) {
                s();
                if (nl1.a(l().A(), t().t(), l().C(), t().u())) {
                    d().y().a("Rechecking which service to use due to a GMP App Id change");
                    t().w();
                    o().B();
                    this.t.A();
                    this.t.E();
                    t().j.a(this.F);
                    t().l.a((String) null);
                }
                t().c(l().A());
                t().d(l().C());
                if (this.g.q(l().B())) {
                    this.k.b(this.F);
                }
            }
            k().a(t().l.a());
            if (!TextUtils.isEmpty(l().A()) || !TextUtils.isEmpty(l().C())) {
                boolean e2 = e();
                if (!t().A() && !this.g.o()) {
                    t().d(!e2);
                }
                if (!this.g.i(l().B()) || e2) {
                    k().F();
                }
                m().a((AtomicReference<String>) new AtomicReference());
            }
        } else if (e()) {
            if (!s().d("android.permission.INTERNET")) {
                d().s().a("App is missing INTERNET permission");
            }
            if (!s().d("android.permission.ACCESS_NETWORK_STATE")) {
                d().s().a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!bn0.b(this.a).a() && !this.g.r()) {
                if (!oh1.a(this.a)) {
                    d().s().a("AppMeasurementReceiver not registered/enabled");
                }
                if (!nl1.a(this.a, false)) {
                    d().s().a("AppMeasurementService not registered/enabled");
                }
            }
            d().s().a("Uploading is not possible. App measurement disabled");
        }
    }

    @DexIgnore
    public final void g() {
        if (!this.x) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    @DexIgnore
    public final Context getContext() {
        return this.a;
    }

    @DexIgnore
    public final void h() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    @DexIgnore
    public final void i() {
    }

    @DexIgnore
    public final ag1 j() {
        ag1 ag1 = this.r;
        if (ag1 != null) {
            return ag1;
        }
        throw new IllegalStateException("Component not created");
    }

    @DexIgnore
    public final dj1 k() {
        b((pk1) this.q);
        return this.q;
    }

    @DexIgnore
    public final ng1 l() {
        b((pk1) this.v);
        return this.v;
    }

    @DexIgnore
    public final vj1 m() {
        b((pk1) this.t);
        return this.t;
    }

    @DexIgnore
    public final rj1 n() {
        b((pk1) this.p);
        return this.p;
    }

    @DexIgnore
    public final pg1 o() {
        b((pk1) this.s);
        return this.s;
    }

    @DexIgnore
    public final tk1 p() {
        b((pk1) this.k);
        return this.k;
    }

    @DexIgnore
    public final bg1 q() {
        b((ui1) this.u);
        return this.u;
    }

    @DexIgnore
    public final rg1 r() {
        a((ti1) this.n);
        return this.n;
    }

    @DexIgnore
    public final nl1 s() {
        a((ti1) this.m);
        return this.m;
    }

    @DexIgnore
    public final fh1 t() {
        a((ti1) this.h);
        return this.h;
    }

    @DexIgnore
    public final xl1 u() {
        return this.g;
    }

    @DexIgnore
    public final tg1 v() {
        tg1 tg1 = this.i;
        if (tg1 == null || !tg1.m()) {
            return null;
        }
        return this.i;
    }

    @DexIgnore
    public final lh1 w() {
        return this.w;
    }

    @DexIgnore
    public final th1 x() {
        return this.j;
    }

    @DexIgnore
    public final AppMeasurement y() {
        return this.l;
    }

    @DexIgnore
    public final boolean z() {
        return TextUtils.isEmpty(this.b);
    }

    @DexIgnore
    public static void b(ui1 ui1) {
        if (ui1 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!ui1.m()) {
            String valueOf = String.valueOf(ui1.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public static void b(pk1 pk1) {
        if (pk1 == null) {
            throw new IllegalStateException("Component not created");
        } else if (!pk1.u()) {
            String valueOf = String.valueOf(pk1.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final th1 a() {
        b((ui1) this.j);
        return this.j;
    }

    @DexIgnore
    public static xh1 a(Context context, og1 og1) {
        if (og1 != null && (og1.e == null || og1.f == null)) {
            og1 = new og1(og1.a, og1.b, og1.c, og1.d, (String) null, (String) null, og1.g);
        }
        bk0.a(context);
        bk0.a(context.getApplicationContext());
        if (G == null) {
            synchronized (xh1.class) {
                if (G == null) {
                    G = new xh1(new bj1(context, og1));
                }
            }
        } else if (og1 != null) {
            Bundle bundle = og1.g;
            if (bundle != null && bundle.containsKey("dataCollectionDefaultEnabled")) {
                G.a(og1.g.getBoolean("dataCollectionDefaultEnabled"));
            }
        }
        return G;
    }

    @DexIgnore
    public static void a(ti1 ti1) {
        if (ti1 == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.A = Boolean.valueOf(z2);
    }

    @DexIgnore
    public final void a(ui1 ui1) {
        this.D++;
    }

    @DexIgnore
    public final void a(pk1 pk1) {
        this.D++;
    }
}
