package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class we2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ TabLayout r;
    @DexIgnore
    public /* final */ TabLayout s;
    @DexIgnore
    public /* final */ TabLayout t;
    @DexIgnore
    public /* final */ TabLayout u;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public we2(Object obj, View view, int i, RTLImageView rTLImageView, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, TabLayout tabLayout, TabLayout tabLayout2, TabLayout tabLayout3, TabLayout tabLayout4, FlexibleTextView flexibleTextView5, ConstraintLayout constraintLayout5) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = tabLayout;
        this.s = tabLayout2;
        this.t = tabLayout3;
        this.u = tabLayout4;
    }
}
