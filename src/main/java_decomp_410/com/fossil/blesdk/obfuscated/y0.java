package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y0 {
    @DexIgnore
    public /* final */ ArrayList<j9> a; // = new ArrayList<>();
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public Interpolator c;
    @DexIgnore
    public k9 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ l9 f; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends l9 {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a() {
            this.b = 0;
            this.a = false;
            y0.this.b();
        }

        @DexIgnore
        public void b(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == y0.this.a.size()) {
                k9 k9Var = y0.this.d;
                if (k9Var != null) {
                    k9Var.b((View) null);
                }
                a();
            }
        }

        @DexIgnore
        public void c(View view) {
            if (!this.a) {
                this.a = true;
                k9 k9Var = y0.this.d;
                if (k9Var != null) {
                    k9Var.c((View) null);
                }
            }
        }
    }

    @DexIgnore
    public y0 a(j9 j9Var) {
        if (!this.e) {
            this.a.add(j9Var);
        }
        return this;
    }

    @DexIgnore
    public void b() {
        this.e = false;
    }

    @DexIgnore
    public void c() {
        if (!this.e) {
            Iterator<j9> it = this.a.iterator();
            while (it.hasNext()) {
                j9 next = it.next();
                long j = this.b;
                if (j >= 0) {
                    next.a(j);
                }
                Interpolator interpolator = this.c;
                if (interpolator != null) {
                    next.a(interpolator);
                }
                if (this.d != null) {
                    next.a((k9) this.f);
                }
                next.c();
            }
            this.e = true;
        }
    }

    @DexIgnore
    public y0 a(j9 j9Var, j9 j9Var2) {
        this.a.add(j9Var);
        j9Var2.b(j9Var.b());
        this.a.add(j9Var2);
        return this;
    }

    @DexIgnore
    public void a() {
        if (this.e) {
            Iterator<j9> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            this.e = false;
        }
    }

    @DexIgnore
    public y0 a(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    @DexIgnore
    public y0 a(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    @DexIgnore
    public y0 a(k9 k9Var) {
        if (!this.e) {
            this.d = k9Var;
        }
        return this;
    }
}
