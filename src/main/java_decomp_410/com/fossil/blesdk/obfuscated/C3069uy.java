package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@com.fossil.blesdk.obfuscated.b64({com.fossil.blesdk.obfuscated.C3396yy.class})
/* renamed from: com.fossil.blesdk.obfuscated.uy */
public class C3069uy extends com.fossil.blesdk.obfuscated.v44<java.lang.Void> {

    @DexIgnore
    /* renamed from: k */
    public /* final */ long f10084k;

    @DexIgnore
    /* renamed from: l */
    public /* final */ java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.String> f10085l;

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C3154vy f10086m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C3154vy f10087n;

    @DexIgnore
    /* renamed from: o */
    public com.fossil.blesdk.obfuscated.C3248wy f10088o;

    @DexIgnore
    /* renamed from: p */
    public com.crashlytics.android.core.CrashlyticsController f10089p;

    @DexIgnore
    /* renamed from: q */
    public java.lang.String f10090q;

    @DexIgnore
    /* renamed from: r */
    public java.lang.String f10091r;

    @DexIgnore
    /* renamed from: s */
    public java.lang.String f10092s;

    @DexIgnore
    /* renamed from: t */
    public float f10093t;

    @DexIgnore
    /* renamed from: u */
    public boolean f10094u;

    @DexIgnore
    /* renamed from: v */
    public /* final */ com.fossil.blesdk.obfuscated.C2847rz f10095v;

    @DexIgnore
    /* renamed from: w */
    public com.fossil.blesdk.obfuscated.z64 f10096w;

    @DexIgnore
    /* renamed from: x */
    public com.fossil.blesdk.obfuscated.C2994ty f10097x;

    @DexIgnore
    /* renamed from: y */
    public com.fossil.blesdk.obfuscated.C3396yy f10098y;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uy$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uy$a */
    public class C3070a extends com.fossil.blesdk.obfuscated.d64<java.lang.Void> {
        @DexIgnore
        public C3070a() {
        }

        @DexIgnore
        public p011io.fabric.sdk.android.services.concurrency.Priority getPriority() {
            return p011io.fabric.sdk.android.services.concurrency.Priority.IMMEDIATE;
        }

        @DexIgnore
        public java.lang.Void call() throws java.lang.Exception {
            return com.fossil.blesdk.obfuscated.C3069uy.this.mo9328k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uy$b")
    /* renamed from: com.fossil.blesdk.obfuscated.uy$b */
    public class C3071b implements java.util.concurrent.Callable<java.lang.Void> {
        @DexIgnore
        public C3071b() {
        }

        @DexIgnore
        public java.lang.Void call() throws java.lang.Exception {
            com.fossil.blesdk.obfuscated.C3069uy.this.f10086m.mo17229a();
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Initialization marker file created.");
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uy$c")
    /* renamed from: com.fossil.blesdk.obfuscated.uy$c */
    public class C3072c implements java.util.concurrent.Callable<java.lang.Boolean> {
        @DexIgnore
        public C3072c() {
        }

        @DexIgnore
        public java.lang.Boolean call() throws java.lang.Exception {
            try {
                boolean d = com.fossil.blesdk.obfuscated.C3069uy.this.f10086m.mo17232d();
                com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
                g.mo30060d("CrashlyticsCore", "Initialization marker file removed: " + d);
                return java.lang.Boolean.valueOf(d);
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Problem encountered deleting Crashlytics initialization marker.", e);
                return false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uy$d")
    /* renamed from: com.fossil.blesdk.obfuscated.uy$d */
    public static final class C3073d implements java.util.concurrent.Callable<java.lang.Boolean> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.fossil.blesdk.obfuscated.C3154vy f10102e;

        @DexIgnore
        public C3073d(com.fossil.blesdk.obfuscated.C3154vy vyVar) {
            this.f10102e = vyVar;
        }

        @DexIgnore
        public java.lang.Boolean call() throws java.lang.Exception {
            if (!this.f10102e.mo17231c()) {
                return java.lang.Boolean.FALSE;
            }
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Found previous crash marker.");
            this.f10102e.mo17232d();
            return java.lang.Boolean.TRUE;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uy$e")
    /* renamed from: com.fossil.blesdk.obfuscated.uy$e */
    public static final class C3074e implements com.fossil.blesdk.obfuscated.C3248wy {
        @DexIgnore
        public C3074e() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16937a() {
        }

        @DexIgnore
        public /* synthetic */ C3074e(com.fossil.blesdk.obfuscated.C3069uy.C3070a aVar) {
            this();
        }
    }

    @DexIgnore
    public C3069uy() {
        this(1.0f, (com.fossil.blesdk.obfuscated.C3248wy) null, (com.fossil.blesdk.obfuscated.C2847rz) null, false);
    }

    @DexIgnore
    /* renamed from: G */
    public static com.fossil.blesdk.obfuscated.C3069uy m14951G() {
        return (com.fossil.blesdk.obfuscated.C3069uy) com.fossil.blesdk.obfuscated.q44.m26795a(com.fossil.blesdk.obfuscated.C3069uy.class);
    }

    @DexIgnore
    /* renamed from: d */
    public static boolean m14955d(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3069uy G = m14951G();
        if (G != null && G.f10089p != null) {
            return true;
        }
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30063e("CrashlyticsCore", "Crashlytics must be initialized by calling Fabric.with(Context) " + str, (java.lang.Throwable) null);
        return false;
    }

    @DexIgnore
    /* renamed from: e */
    public static java.lang.String m14956e(java.lang.String str) {
        if (str == null) {
            return str;
        }
        java.lang.String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    @DexIgnore
    /* renamed from: A */
    public com.fossil.blesdk.obfuscated.C3329xy mo16914A() {
        com.fossil.blesdk.obfuscated.C3396yy yyVar = this.f10098y;
        if (yyVar != null) {
            return yyVar.mo18367a();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: B */
    public java.lang.String mo16915B() {
        if (mo31736o().mo44543a()) {
            return this.f10091r;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: C */
    public java.lang.String mo16916C() {
        if (mo31736o().mo44543a()) {
            return this.f10090q;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: D */
    public java.lang.String mo16917D() {
        if (mo31736o().mo44543a()) {
            return this.f10092s;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: E */
    public void mo16918E() {
        this.f10097x.mo16607a(new com.fossil.blesdk.obfuscated.C3069uy.C3072c());
    }

    @DexIgnore
    /* renamed from: F */
    public void mo16919F() {
        this.f10097x.mo16608b(new com.fossil.blesdk.obfuscated.C3069uy.C3071b());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16924b(int i, java.lang.String str, java.lang.String str2) {
        mo16920a(i, str, str2);
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30055a(i, "" + str, "" + str2, true);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16926c(java.lang.String str) {
        if (!this.f10094u && m14955d("prior to setting user data.")) {
            this.f10090q = m14956e(str);
            this.f10089p.mo4114a(this.f10090q, this.f10092s, this.f10091r);
        }
    }

    @DexIgnore
    /* renamed from: p */
    public java.lang.String mo9329p() {
        return "com.crashlytics.sdk.android.crashlytics-core";
    }

    @DexIgnore
    /* renamed from: r */
    public java.lang.String mo9330r() {
        return "2.7.0.33";
    }

    @DexIgnore
    /* renamed from: u */
    public boolean mo9331u() {
        return mo16923a(super.mo31733l());
    }

    @DexIgnore
    /* renamed from: v */
    public final void mo16927v() {
        if (java.lang.Boolean.TRUE.equals((java.lang.Boolean) this.f10097x.mo16608b(new com.fossil.blesdk.obfuscated.C3069uy.C3073d(this.f10087n)))) {
            try {
                this.f10088o.mo16937a();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Exception thrown by CrashlyticsListener while notifying of previous crash.", e);
            }
        }
    }

    @DexIgnore
    /* renamed from: w */
    public void mo16928w() {
        this.f10087n.mo17229a();
    }

    @DexIgnore
    /* renamed from: x */
    public boolean mo16929x() {
        return this.f10086m.mo17231c();
    }

    @DexIgnore
    /* renamed from: y */
    public final void mo16930y() {
        com.fossil.blesdk.obfuscated.C3069uy.C3070a aVar = new com.fossil.blesdk.obfuscated.C3069uy.C3070a();
        for (com.fossil.blesdk.obfuscated.i64 a : mo31734m()) {
            aVar.mo25642a(a);
        }
        java.util.concurrent.Future submit = mo31735n().mo30316b().submit(aVar);
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, java.util.concurrent.TimeUnit.SECONDS);
        } catch (java.lang.InterruptedException e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Crashlytics was interrupted during initialization.", e);
        } catch (java.util.concurrent.ExecutionException e2) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Problem encountered during Crashlytics initialization.", e2);
        } catch (java.util.concurrent.TimeoutException e3) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Crashlytics timed out during initialization.", e3);
        }
    }

    @DexIgnore
    /* renamed from: z */
    public java.util.Map<java.lang.String, java.lang.String> mo16931z() {
        return java.util.Collections.unmodifiableMap(this.f10085l);
    }

    @DexIgnore
    public C3069uy(float f, com.fossil.blesdk.obfuscated.C3248wy wyVar, com.fossil.blesdk.obfuscated.C2847rz rzVar, boolean z) {
        this(f, wyVar, rzVar, z, com.fossil.blesdk.obfuscated.q54.m26831a("Crashlytics Exception Handler"));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo16923a(android.content.Context context) {
        android.content.Context context2 = context;
        if (!com.fossil.blesdk.obfuscated.o54.m26050a(context).mo29818a()) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Crashlytics is disabled, because data collection is disabled by Firebase.");
            this.f10094u = true;
        }
        if (this.f10094u) {
            return false;
        }
        java.lang.String d = new com.fossil.blesdk.obfuscated.k54().mo28770d(context2);
        if (d == null) {
            return false;
        }
        java.lang.String n = p011io.fabric.sdk.android.services.common.CommonUtils.m36898n(context);
        if (m14953a(n, p011io.fabric.sdk.android.services.common.CommonUtils.m36877a(context2, "com.crashlytics.RequireBuildId", true))) {
            try {
                com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
                g.mo30064i("CrashlyticsCore", "Initializing Crashlytics Core " + mo9330r());
                com.fossil.blesdk.obfuscated.f74 f74 = new com.fossil.blesdk.obfuscated.f74(this);
                this.f10087n = new com.fossil.blesdk.obfuscated.C3154vy("crash_marker", f74);
                this.f10086m = new com.fossil.blesdk.obfuscated.C3154vy("initialization_marker", f74);
                com.fossil.blesdk.obfuscated.C2933sz a = com.fossil.blesdk.obfuscated.C2933sz.m13966a(new com.fossil.blesdk.obfuscated.h74(mo31733l(), "com.crashlytics.android.core.CrashlyticsCore"), this);
                com.fossil.blesdk.obfuscated.C3474zy zyVar = this.f10095v != null ? new com.fossil.blesdk.obfuscated.C3474zy(this.f10095v) : null;
                this.f10096w = new com.fossil.blesdk.obfuscated.y64(com.fossil.blesdk.obfuscated.q44.m26805g());
                this.f10096w.mo32794a(zyVar);
                p011io.fabric.sdk.android.services.common.IdManager o = mo31736o();
                com.fossil.blesdk.obfuscated.C2359ly a2 = com.fossil.blesdk.obfuscated.C2359ly.m10475a(context2, o, d, n);
                com.crashlytics.android.core.CrashlyticsController crashlyticsController = r1;
                com.crashlytics.android.core.CrashlyticsController crashlyticsController2 = new com.crashlytics.android.core.CrashlyticsController(this, this.f10097x, this.f10096w, o, a, f74, a2, new com.fossil.blesdk.obfuscated.C3397yz(context2, new com.fossil.blesdk.obfuscated.C2260kz(context2, a2.f7362d)), new com.fossil.blesdk.obfuscated.C1667dz(this), com.fossil.blesdk.obfuscated.C2258kx.m9879b(context));
                this.f10089p = crashlyticsController;
                boolean x = mo16929x();
                mo16927v();
                this.f10089p.mo4116a(java.lang.Thread.getDefaultUncaughtExceptionHandler(), new com.fossil.blesdk.obfuscated.t54().mo31152e(context2));
                if (!x || !p011io.fabric.sdk.android.services.common.CommonUtils.m36881b(context)) {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Exception handling initialization successful");
                    return true;
                }
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Crashlytics did not finish previous background initialization. Initializing synchronously.");
                mo16930y();
                return false;
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Crashlytics was not started due to an exception during initialization", e);
                this.f10089p = null;
                return false;
            }
        } else {
            throw new p011io.fabric.sdk.android.services.concurrency.UnmetDependencyException("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    @DexIgnore
    /* renamed from: k */
    public java.lang.Void m14971k() {
        mo16919F();
        this.f10089p.mo4095a();
        try {
            this.f10089p.mo4158p();
            com.fossil.blesdk.obfuscated.b84 a = com.fossil.blesdk.obfuscated.z74.m31276d().mo33017a();
            if (a == null) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30066w("CrashlyticsCore", "Received null settings, skipping report submission!");
                mo16918E();
                return null;
            }
            this.f10089p.mo4104a(a);
            if (!a.f13601d.f19364b) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Collection of crash reports disabled in Crashlytics settings.");
                mo16918E();
                return null;
            } else if (!com.fossil.blesdk.obfuscated.o54.m26050a(mo31733l()).mo29818a()) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Automatic collection of crash reports disabled by Firebase settings.");
                mo16918E();
                return null;
            } else {
                com.fossil.blesdk.obfuscated.C3329xy A = mo16914A();
                if (A != null && !this.f10089p.mo4124a(A)) {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Could not finalize previous NDK sessions.");
                }
                if (!this.f10089p.mo4136b(a.f13599b)) {
                    com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Could not finalize previous sessions.");
                }
                this.f10089p.mo4096a(this.f10093t, a);
                mo16918E();
                return null;
            }
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Crashlytics encountered a problem during asynchronous initialization.", e);
        } catch (Throwable th) {
            mo16918E();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16925b(java.lang.String str) {
        if (!this.f10094u && m14955d("prior to setting user data.")) {
            this.f10091r = m14956e(str);
            this.f10089p.mo4114a(this.f10090q, this.f10092s, this.f10091r);
        }
    }

    @DexIgnore
    public C3069uy(float f, com.fossil.blesdk.obfuscated.C3248wy wyVar, com.fossil.blesdk.obfuscated.C2847rz rzVar, boolean z, java.util.concurrent.ExecutorService executorService) {
        this.f10090q = null;
        this.f10091r = null;
        this.f10092s = null;
        this.f10093t = f;
        this.f10088o = wyVar == null ? new com.fossil.blesdk.obfuscated.C3069uy.C3074e((com.fossil.blesdk.obfuscated.C3069uy.C3070a) null) : wyVar;
        this.f10095v = rzVar;
        this.f10094u = z;
        this.f10097x = new com.fossil.blesdk.obfuscated.C2994ty(executorService);
        this.f10085l = new java.util.concurrent.ConcurrentHashMap<>();
        this.f10084k = java.lang.System.currentTimeMillis();
    }

    @DexIgnore
    /* renamed from: c */
    public static java.lang.String m14954c(int i, java.lang.String str, java.lang.String str2) {
        return p011io.fabric.sdk.android.services.common.CommonUtils.m36862a(i) + com.zendesk.sdk.network.impl.ZendeskConfig.SLASH + str + " " + str2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16921a(java.lang.String str) {
        mo16920a(3, "CrashlyticsCore", str);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16920a(int i, java.lang.String str, java.lang.String str2) {
        if (!this.f10094u && m14955d("prior to logging messages.")) {
            this.f10089p.mo4099a(java.lang.System.currentTimeMillis() - this.f10084k, m14954c(i, str, str2));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16922a(java.lang.String str, java.lang.String str2) {
        java.lang.String str3;
        if (this.f10094u || !m14955d("prior to setting keys.")) {
            return;
        }
        if (str == null) {
            android.content.Context l = mo31733l();
            if (l == null || !p011io.fabric.sdk.android.services.common.CommonUtils.m36894j(l)) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Attempting to set custom attribute with null key, ignoring.", (java.lang.Throwable) null);
                return;
            }
            throw new java.lang.IllegalArgumentException("Custom attribute key must not be null.");
        }
        java.lang.String e = m14956e(str);
        if (this.f10085l.size() < 64 || this.f10085l.containsKey(e)) {
            if (str2 == null) {
                str3 = "";
            } else {
                str3 = m14956e(str2);
            }
            this.f10085l.put(e, str3);
            this.f10089p.mo4118a((java.util.Map<java.lang.String, java.lang.String>) this.f10085l);
            return;
        }
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Exceeded maximum number of custom attributes (64)");
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m14953a(java.lang.String str, boolean z) {
        if (!z) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Configured not to require a build ID.");
            return true;
        } else if (!p011io.fabric.sdk.android.services.common.CommonUtils.m36882b(str)) {
            return true;
        } else {
            android.util.Log.e("CrashlyticsCore", com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME);
            android.util.Log.e("CrashlyticsCore", ".     |  | ");
            android.util.Log.e("CrashlyticsCore", ".     |  |");
            android.util.Log.e("CrashlyticsCore", ".     |  |");
            android.util.Log.e("CrashlyticsCore", ".   \\ |  | /");
            android.util.Log.e("CrashlyticsCore", ".    \\    /");
            android.util.Log.e("CrashlyticsCore", ".     \\  /");
            android.util.Log.e("CrashlyticsCore", ".      \\/");
            android.util.Log.e("CrashlyticsCore", com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME);
            android.util.Log.e("CrashlyticsCore", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            android.util.Log.e("CrashlyticsCore", com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME);
            android.util.Log.e("CrashlyticsCore", ".      /\\");
            android.util.Log.e("CrashlyticsCore", ".     /  \\");
            android.util.Log.e("CrashlyticsCore", ".    /    \\");
            android.util.Log.e("CrashlyticsCore", ".   / |  | \\");
            android.util.Log.e("CrashlyticsCore", ".     |  |");
            android.util.Log.e("CrashlyticsCore", ".     |  |");
            android.util.Log.e("CrashlyticsCore", ".     |  |");
            android.util.Log.e("CrashlyticsCore", com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME);
            return false;
        }
    }
}
