package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class r80 extends q80 {
    @DexIgnore
    public byte[] L; // = new byte[0];
    @DexIgnore
    public byte M; // = -1;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public GattCharacteristic.CharacteristicId P; // = GattCharacteristic.CharacteristicId.FTD;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r80(LegacyFileControlOperationCode legacyFileControlOperationCode, short s, RequestId requestId, Peripheral peripheral, int i) {
        super(legacyFileControlOperationCode, s, requestId, peripheral, i);
        kd4.b(legacyFileControlOperationCode, "fileOperation");
        kd4.b(requestId, "requestId");
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public void J() {
    }

    @DexIgnore
    public final JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject jSONObject = new JSONObject();
        if (!this.N) {
            this.N = true;
            c(n().getResultCode() != Request.Result.ResultCode.SUCCESS);
            byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) 8).array();
            kd4.a((Object) array, "ByteBuffer.allocate(1)\n \u2026                 .array()");
            b(array);
        } else {
            c(true);
        }
        return jSONObject;
    }

    @DexIgnore
    public abstract void c(byte[] bArr);

    @DexIgnore
    public final boolean c(c20 c20) {
        kd4.b(c20, "characteristicChangeNotification");
        return c20.a() == this.P;
    }

    @DexIgnore
    public final boolean d(byte b) {
        return ((byte) (b & Byte.MIN_VALUE)) != ((byte) 0);
    }

    @DexIgnore
    public final void f(c20 c20) {
        kd4.b(c20, "characteristicChangedNotification");
        if (!this.N) {
            super.f(c20);
            return;
        }
        byte[] b = c20.b();
        c(true);
        a(new Request.ResponseInfo(0, c20.a(), b, new JSONObject(), 1, (fd4) null));
    }

    @DexIgnore
    public final void g(c20 c20) {
        byte[] bArr;
        kd4.b(c20, "characteristicChangedNotification");
        if (g()) {
            bArr = d90.b.a(i().k(), this.P, c20.b());
        } else {
            bArr = c20.b();
        }
        int c = c(bArr[0]);
        int c2 = c((byte) (this.M + 1));
        if (c == c2) {
            a(d());
            this.M = (byte) c2;
            this.L = k90.a(this.L, ya4.a(bArr, 1, bArr.length));
            this.O = d(bArr[0]);
            if (this.O) {
                c(this.L);
                J();
                return;
            }
            return;
        }
        b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.MISS_PACKAGE, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        c(true);
    }

    @DexIgnore
    public final int c(byte b) {
        return n90.b(n90.b((byte) (b & 63)));
    }
}
