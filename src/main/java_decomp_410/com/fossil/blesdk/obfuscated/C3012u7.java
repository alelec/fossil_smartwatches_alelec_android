package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.u7 */
public class C3012u7 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2183k4<java.lang.String, android.graphics.Typeface> f9867a; // = new com.fossil.blesdk.obfuscated.C2183k4<>(16);

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C3092v7 f9868b; // = new com.fossil.blesdk.obfuscated.C3092v7("fonts", 10, 10000);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.lang.Object f9869c; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: d */
    public static /* final */ androidx.collection.SimpleArrayMap<java.lang.String, java.util.ArrayList<com.fossil.blesdk.obfuscated.C3092v7.C3097d<com.fossil.blesdk.obfuscated.C3012u7.C3019g>>> f9870d; // = new androidx.collection.SimpleArrayMap<>();

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.util.Comparator<byte[]> f9871e; // = new com.fossil.blesdk.obfuscated.C3012u7.C3016d();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$a */
    public static class C3013a implements java.util.concurrent.Callable<com.fossil.blesdk.obfuscated.C3012u7.C3019g> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ android.content.Context f9872e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2944t7 f9873f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ int f9874g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ java.lang.String f9875h;

        @DexIgnore
        public C3013a(android.content.Context context, com.fossil.blesdk.obfuscated.C2944t7 t7Var, int i, java.lang.String str) {
            this.f9872e = context;
            this.f9873f = t7Var;
            this.f9874g = i;
            this.f9875h = str;
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C3012u7.C3019g call() throws java.lang.Exception {
            com.fossil.blesdk.obfuscated.C3012u7.C3019g a = com.fossil.blesdk.obfuscated.C3012u7.m14600a(this.f9872e, this.f9873f, this.f9874g);
            android.graphics.Typeface typeface = a.f9886a;
            if (typeface != null) {
                com.fossil.blesdk.obfuscated.C3012u7.f9867a.mo12623a(this.f9875h, typeface);
            }
            return a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$b")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$b */
    public static class C3014b implements com.fossil.blesdk.obfuscated.C3092v7.C3097d<com.fossil.blesdk.obfuscated.C3012u7.C3019g> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2785r6.C2786a f9876a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ android.os.Handler f9877b;

        @DexIgnore
        public C3014b(com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar, android.os.Handler handler) {
            this.f9876a = aVar;
            this.f9877b = handler;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16669a(com.fossil.blesdk.obfuscated.C3012u7.C3019g gVar) {
            if (gVar == null) {
                this.f9876a.mo15442a(1, this.f9877b);
                return;
            }
            int i = gVar.f9887b;
            if (i == 0) {
                this.f9876a.mo15443a(gVar.f9886a, this.f9877b);
            } else {
                this.f9876a.mo15442a(i, this.f9877b);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$c")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$c */
    public static class C3015c implements com.fossil.blesdk.obfuscated.C3092v7.C3097d<com.fossil.blesdk.obfuscated.C3012u7.C3019g> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ java.lang.String f9878a;

        @DexIgnore
        public C3015c(java.lang.String str) {
            this.f9878a = str;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
            if (r0 >= r1.size()) goto L_0x002c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
            ((com.fossil.blesdk.obfuscated.C3092v7.C3097d) r1.get(r0)).mo16669a(r5);
            r0 = r0 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
            r0 = 0;
         */
        @DexIgnore
        /* renamed from: a */
        public void mo16669a(com.fossil.blesdk.obfuscated.C3012u7.C3019g gVar) {
            synchronized (com.fossil.blesdk.obfuscated.C3012u7.f9869c) {
                java.util.ArrayList arrayList = com.fossil.blesdk.obfuscated.C3012u7.f9870d.get(this.f9878a);
                if (arrayList != null) {
                    com.fossil.blesdk.obfuscated.C3012u7.f9870d.remove(this.f9878a);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$d")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$d */
    public static class C3016d implements java.util.Comparator<byte[]> {
        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v4, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: byte} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            int i;
            int i2;
            if (bArr.length != bArr2.length) {
                i2 = bArr.length;
                i = bArr2.length;
            } else {
                int i3 = 0;
                while (i3 < bArr.length) {
                    if (bArr[i3] != bArr2[i3]) {
                        i2 = bArr[i3];
                        i = bArr2[i3];
                    } else {
                        i3++;
                    }
                }
                return 0;
            }
            return i2 - i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$e")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$e */
    public static class C3017e {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int f9879a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C3012u7.C3018f[] f9880b;

        @DexIgnore
        public C3017e(int i, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr) {
            this.f9879a = i;
            this.f9880b = fVarArr;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3012u7.C3018f[] mo16673a() {
            return this.f9880b;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo16674b() {
            return this.f9879a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$f")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$f */
    public static class C3018f {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.net.Uri f9881a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f9882b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f9883c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ boolean f9884d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f9885e;

        @DexIgnore
        public C3018f(android.net.Uri uri, int i, int i2, boolean z, int i3) {
            com.fossil.blesdk.obfuscated.C2109j8.m8871a(uri);
            this.f9881a = uri;
            this.f9882b = i;
            this.f9883c = i2;
            this.f9884d = z;
            this.f9885e = i3;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo16675a() {
            return this.f9885e;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo16676b() {
            return this.f9882b;
        }

        @DexIgnore
        /* renamed from: c */
        public android.net.Uri mo16677c() {
            return this.f9881a;
        }

        @DexIgnore
        /* renamed from: d */
        public int mo16678d() {
            return this.f9883c;
        }

        @DexIgnore
        /* renamed from: e */
        public boolean mo16679e() {
            return this.f9884d;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.u7$g")
    /* renamed from: com.fossil.blesdk.obfuscated.u7$g */
    public static final class C3019g {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.graphics.Typeface f9886a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f9887b;

        @DexIgnore
        public C3019g(android.graphics.Typeface typeface, int i) {
            this.f9886a = typeface;
            this.f9887b = i;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3012u7.C3019g m14600a(android.content.Context context, com.fossil.blesdk.obfuscated.C2944t7 t7Var, int i) {
        try {
            com.fossil.blesdk.obfuscated.C3012u7.C3017e a = m14599a(context, (android.os.CancellationSignal) null, t7Var);
            int i2 = -3;
            if (a.mo16674b() == 0) {
                android.graphics.Typeface a2 = com.fossil.blesdk.obfuscated.C3091v6.m15082a(context, (android.os.CancellationSignal) null, a.mo16673a(), i);
                if (a2 != null) {
                    i2 = 0;
                }
                return new com.fossil.blesdk.obfuscated.C3012u7.C3019g(a2, i2);
            }
            if (a.mo16674b() == 1) {
                i2 = -2;
            }
            return new com.fossil.blesdk.obfuscated.C3012u7.C3019g((android.graphics.Typeface) null, i2);
        } catch (android.content.pm.PackageManager.NameNotFoundException unused) {
            return new com.fossil.blesdk.obfuscated.C3012u7.C3019g((android.graphics.Typeface) null, -1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0078, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0089, code lost:
        f9868b.mo16993a(r1, new com.fossil.blesdk.obfuscated.C3012u7.C3015c(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0093, code lost:
        return null;
     */
    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m14598a(android.content.Context context, com.fossil.blesdk.obfuscated.C2944t7 t7Var, com.fossil.blesdk.obfuscated.C2785r6.C2786a aVar, android.os.Handler handler, boolean z, int i, int i2) {
        com.fossil.blesdk.obfuscated.C3012u7.C3014b bVar;
        java.lang.String str = t7Var.mo16276c() + com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
        android.graphics.Typeface b = f9867a.mo12627b(str);
        if (b != null) {
            if (aVar != null) {
                aVar.mo751a(b);
            }
            return b;
        } else if (!z || i != -1) {
            com.fossil.blesdk.obfuscated.C3012u7.C3013a aVar2 = new com.fossil.blesdk.obfuscated.C3012u7.C3013a(context, t7Var, i2, str);
            if (z) {
                try {
                    return ((com.fossil.blesdk.obfuscated.C3012u7.C3019g) f9868b.mo16990a(aVar2, i)).f9886a;
                } catch (java.lang.InterruptedException unused) {
                    return null;
                }
            } else {
                if (aVar == null) {
                    bVar = null;
                } else {
                    bVar = new com.fossil.blesdk.obfuscated.C3012u7.C3014b(aVar, handler);
                }
                synchronized (f9869c) {
                    if (f9870d.containsKey(str)) {
                        if (bVar != null) {
                            f9870d.get(str).add(bVar);
                        }
                    } else if (bVar != null) {
                        java.util.ArrayList arrayList = new java.util.ArrayList();
                        arrayList.add(bVar);
                        f9870d.put(str, arrayList);
                    }
                }
            }
        } else {
            com.fossil.blesdk.obfuscated.C3012u7.C3019g a = m14600a(context, t7Var, i2);
            if (aVar != null) {
                int i3 = a.f9887b;
                if (i3 == 0) {
                    aVar.mo15443a(a.f9886a, handler);
                } else {
                    aVar.mo15442a(i3, handler);
                }
            }
            return a.f9886a;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.Map<android.net.Uri, java.nio.ByteBuffer> m14603a(android.content.Context context, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, android.os.CancellationSignal cancellationSignal) {
        java.util.HashMap hashMap = new java.util.HashMap();
        for (com.fossil.blesdk.obfuscated.C3012u7.C3018f fVar : fVarArr) {
            if (fVar.mo16675a() == 0) {
                android.net.Uri c = fVar.mo16677c();
                if (!hashMap.containsKey(c)) {
                    hashMap.put(c, com.fossil.blesdk.obfuscated.C1466b7.m4803a(context, cancellationSignal, c));
                }
            }
        }
        return java.util.Collections.unmodifiableMap(hashMap);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3012u7.C3017e m14599a(android.content.Context context, android.os.CancellationSignal cancellationSignal, com.fossil.blesdk.obfuscated.C2944t7 t7Var) throws android.content.pm.PackageManager.NameNotFoundException {
        android.content.pm.ProviderInfo a = m14597a(context.getPackageManager(), t7Var, context.getResources());
        if (a == null) {
            return new com.fossil.blesdk.obfuscated.C3012u7.C3017e(1, (com.fossil.blesdk.obfuscated.C3012u7.C3018f[]) null);
        }
        return new com.fossil.blesdk.obfuscated.C3012u7.C3017e(0, m14605a(context, t7Var, a.authority, cancellationSignal));
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.pm.ProviderInfo m14597a(android.content.pm.PackageManager packageManager, com.fossil.blesdk.obfuscated.C2944t7 t7Var, android.content.res.Resources resources) throws android.content.pm.PackageManager.NameNotFoundException {
        java.lang.String d = t7Var.mo16277d();
        android.content.pm.ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(d, 0);
        if (resolveContentProvider == null) {
            throw new android.content.pm.PackageManager.NameNotFoundException("No package found for authority: " + d);
        } else if (resolveContentProvider.packageName.equals(t7Var.mo16278e())) {
            java.util.List<byte[]> a = m14602a(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            java.util.Collections.sort(a, f9871e);
            java.util.List<java.util.List<byte[]>> a2 = m14601a(t7Var, resources);
            for (int i = 0; i < a2.size(); i++) {
                java.util.ArrayList arrayList = new java.util.ArrayList(a2.get(i));
                java.util.Collections.sort(arrayList, f9871e);
                if (m14604a(a, (java.util.List<byte[]>) arrayList)) {
                    return resolveContentProvider;
                }
            }
            return null;
        } else {
            throw new android.content.pm.PackageManager.NameNotFoundException("Found content provider " + d + ", but package was not " + t7Var.mo16278e());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<java.util.List<byte[]>> m14601a(com.fossil.blesdk.obfuscated.C2944t7 t7Var, android.content.res.Resources resources) {
        if (t7Var.mo16274a() != null) {
            return t7Var.mo16274a();
        }
        return com.fossil.blesdk.obfuscated.C2528o6.m11603a(resources, t7Var.mo16275b());
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m14604a(java.util.List<byte[]> list, java.util.List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!java.util.Arrays.equals(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<byte[]> m14602a(android.content.pm.Signature[] signatureArr) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (android.content.pm.Signature byteArray : signatureArr) {
            arrayList.add(byteArray.toByteArray());
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3012u7.C3018f[] m14605a(android.content.Context context, com.fossil.blesdk.obfuscated.C2944t7 t7Var, java.lang.String str, android.os.CancellationSignal cancellationSignal) {
        android.net.Uri uri;
        android.database.Cursor query;
        java.lang.String str2 = str;
        java.util.ArrayList arrayList = new java.util.ArrayList();
        android.net.Uri build = new android.net.Uri.Builder().scheme("content").authority(str2).build();
        android.net.Uri build2 = new android.net.Uri.Builder().scheme("content").authority(str2).appendPath("file").build();
        android.database.Cursor cursor = null;
        try {
            if (android.os.Build.VERSION.SDK_INT > 16) {
                query = context.getContentResolver().query(build, new java.lang.String[]{com.j256.ormlite.field.FieldType.FOREIGN_ID_FIELD_SUFFIX, "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new java.lang.String[]{t7Var.mo16279f()}, (java.lang.String) null, cancellationSignal);
            } else {
                query = context.getContentResolver().query(build, new java.lang.String[]{com.j256.ormlite.field.FieldType.FOREIGN_ID_FIELD_SUFFIX, "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new java.lang.String[]{t7Var.mo16279f()}, (java.lang.String) null);
            }
            if (cursor != null && cursor.getCount() > 0) {
                int columnIndex = cursor.getColumnIndex("result_code");
                java.util.ArrayList arrayList2 = new java.util.ArrayList();
                int columnIndex2 = cursor.getColumnIndex(com.j256.ormlite.field.FieldType.FOREIGN_ID_FIELD_SUFFIX);
                int columnIndex3 = cursor.getColumnIndex("file_id");
                int columnIndex4 = cursor.getColumnIndex("font_ttc_index");
                int columnIndex5 = cursor.getColumnIndex("font_weight");
                int columnIndex6 = cursor.getColumnIndex("font_italic");
                while (cursor.moveToNext()) {
                    int i = columnIndex != -1 ? cursor.getInt(columnIndex) : 0;
                    int i2 = columnIndex4 != -1 ? cursor.getInt(columnIndex4) : 0;
                    if (columnIndex3 == -1) {
                        uri = android.content.ContentUris.withAppendedId(build, cursor.getLong(columnIndex2));
                    } else {
                        uri = android.content.ContentUris.withAppendedId(build2, cursor.getLong(columnIndex3));
                    }
                    com.fossil.blesdk.obfuscated.C3012u7.C3018f fVar = new com.fossil.blesdk.obfuscated.C3012u7.C3018f(uri, i2, columnIndex5 != -1 ? cursor.getInt(columnIndex5) : com.misfit.frameworks.common.constants.MFNetworkReturnCode.BAD_REQUEST, columnIndex6 != -1 && cursor.getInt(columnIndex6) == 1, i);
                    arrayList2.add(fVar);
                }
                arrayList = arrayList2;
            }
            return (com.fossil.blesdk.obfuscated.C3012u7.C3018f[]) arrayList.toArray(new com.fossil.blesdk.obfuscated.C3012u7.C3018f[0]);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
