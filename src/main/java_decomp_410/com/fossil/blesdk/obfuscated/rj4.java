package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rj4 {
    @DexIgnore
    public static /* final */ Object a; // = new dk4("CONDITION_FALSE");

    /*
    static {
        new dk4("ALREADY_REMOVED");
        new dk4("LIST_EMPTY");
        new dk4("REMOVE_PREPARED");
    }
    */

    @DexIgnore
    public static final Object a() {
        return a;
    }

    @DexIgnore
    public static final sj4 a(Object obj) {
        kd4.b(obj, "$this$unwrap");
        zj4 zj4 = (zj4) (!(obj instanceof zj4) ? null : obj);
        if (zj4 != null) {
            sj4 sj4 = zj4.a;
            if (sj4 != null) {
                return sj4;
            }
        }
        return (sj4) obj;
    }
}
