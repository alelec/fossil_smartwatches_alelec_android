package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.uirenew.customview.ExpandableTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zj3 extends FrameLayout implements View.OnClickListener {
    @DexIgnore
    public a e;
    @DexIgnore
    public ExpandableTextView f;
    @DexIgnore
    public InAppNotification g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void b(InAppNotification inAppNotification);
    }

    @DexIgnore
    public void a() {
        ((ViewGroup) ((Activity) getContext()).findViewById(16908290)).removeView(this);
        a aVar = this.e;
        if (aVar != null) {
            aVar.b(this.g);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.content) {
            this.f.e();
        } else if (id == R.id.dismiss_btn || id == R.id.view_btn) {
            a();
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
