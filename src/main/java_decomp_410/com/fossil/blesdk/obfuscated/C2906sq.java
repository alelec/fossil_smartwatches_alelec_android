package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sq */
public class C2906sq implements com.fossil.blesdk.obfuscated.C2762qq {

    @DexIgnore
    /* renamed from: d */
    public static /* final */ android.graphics.Bitmap.Config[] f9422d;

    @DexIgnore
    /* renamed from: e */
    public static /* final */ android.graphics.Bitmap.Config[] f9423e; // = f9422d;

    @DexIgnore
    /* renamed from: f */
    public static /* final */ android.graphics.Bitmap.Config[] f9424f; // = {android.graphics.Bitmap.Config.RGB_565};

    @DexIgnore
    /* renamed from: g */
    public static /* final */ android.graphics.Bitmap.Config[] f9425g; // = {android.graphics.Bitmap.Config.ARGB_4444};

    @DexIgnore
    /* renamed from: h */
    public static /* final */ android.graphics.Bitmap.Config[] f9426h; // = {android.graphics.Bitmap.Config.ALPHA_8};

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2906sq.C2909c f9427a; // = new com.fossil.blesdk.obfuscated.C2906sq.C2909c();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2430mq<com.fossil.blesdk.obfuscated.C2906sq.C2908b, android.graphics.Bitmap> f9428b; // = new com.fossil.blesdk.obfuscated.C2430mq<>();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Map<android.graphics.Bitmap.Config, java.util.NavigableMap<java.lang.Integer, java.lang.Integer>> f9429c; // = new java.util.HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.sq$a */
    public static /* synthetic */ class C2907a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f9430a; // = new int[android.graphics.Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            f9430a[android.graphics.Bitmap.Config.ARGB_8888.ordinal()] = 1;
            f9430a[android.graphics.Bitmap.Config.RGB_565.ordinal()] = 2;
            f9430a[android.graphics.Bitmap.Config.ARGB_4444.ordinal()] = 3;
            try {
                f9430a[android.graphics.Bitmap.Config.ALPHA_8.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sq$b")
    /* renamed from: com.fossil.blesdk.obfuscated.sq$b */
    public static final class C2908b implements com.fossil.blesdk.obfuscated.C2825rq {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2906sq.C2909c f9431a;

        @DexIgnore
        /* renamed from: b */
        public int f9432b;

        @DexIgnore
        /* renamed from: c */
        public android.graphics.Bitmap.Config f9433c;

        @DexIgnore
        public C2908b(com.fossil.blesdk.obfuscated.C2906sq.C2909c cVar) {
            this.f9431a = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo16136a(int i, android.graphics.Bitmap.Config config) {
            this.f9432b = i;
            this.f9433c = config;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (!(obj instanceof com.fossil.blesdk.obfuscated.C2906sq.C2908b)) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2906sq.C2908b bVar = (com.fossil.blesdk.obfuscated.C2906sq.C2908b) obj;
            if (this.f9432b != bVar.f9432b || !com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f9433c, (java.lang.Object) bVar.f9433c)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.f9432b * 31;
            android.graphics.Bitmap.Config config = this.f9433c;
            return i + (config != null ? config.hashCode() : 0);
        }

        @DexIgnore
        public java.lang.String toString() {
            return com.fossil.blesdk.obfuscated.C2906sq.m13826b(this.f9432b, this.f9433c);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11713a() {
            this.f9431a.mo12068a(this);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sq$c")
    /* renamed from: com.fossil.blesdk.obfuscated.sq$c */
    public static class C2909c extends com.fossil.blesdk.obfuscated.C2056iq<com.fossil.blesdk.obfuscated.C2906sq.C2908b> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2906sq.C2908b mo16140a(int i, android.graphics.Bitmap.Config config) {
            com.fossil.blesdk.obfuscated.C2906sq.C2908b bVar = (com.fossil.blesdk.obfuscated.C2906sq.C2908b) mo12069b();
            bVar.mo16136a(i, config);
            return bVar;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2906sq.C2908b m13840a() {
            return new com.fossil.blesdk.obfuscated.C2906sq.C2908b(this);
        }
    }

    /*
    static {
        android.graphics.Bitmap.Config[] configArr = {android.graphics.Bitmap.Config.ARGB_8888, null};
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            configArr = (android.graphics.Bitmap.Config[]) java.util.Arrays.copyOf(configArr, configArr.length + 1);
            configArr[configArr.length - 1] = android.graphics.Bitmap.Config.RGBA_F16;
        }
        f9422d = configArr;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public void mo11708a(android.graphics.Bitmap bitmap) {
        com.fossil.blesdk.obfuscated.C2906sq.C2908b a = this.f9427a.mo16140a(com.fossil.blesdk.obfuscated.C3066uw.m14922a(bitmap), bitmap.getConfig());
        this.f9428b.mo13695a(a, bitmap);
        java.util.NavigableMap<java.lang.Integer, java.lang.Integer> a2 = mo16133a(bitmap.getConfig());
        java.lang.Integer num = (java.lang.Integer) a2.get(java.lang.Integer.valueOf(a.f9432b));
        java.lang.Integer valueOf = java.lang.Integer.valueOf(a.f9432b);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        a2.put(valueOf, java.lang.Integer.valueOf(i));
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo11710b(int i, int i2, android.graphics.Bitmap.Config config) {
        return m13826b(com.fossil.blesdk.obfuscated.C3066uw.m14920a(i, i2, config), config);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo11711c(android.graphics.Bitmap bitmap) {
        return m13826b(com.fossil.blesdk.obfuscated.C3066uw.m14922a(bitmap), bitmap.getConfig());
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("SizeConfigStrategy{groupedMap=");
        sb.append(this.f9428b);
        sb.append(", sortedSizes=(");
        for (java.util.Map.Entry next : this.f9429c.entrySet()) {
            sb.append(next.getKey());
            sb.append('[');
            sb.append(next.getValue());
            sb.append("], ");
        }
        if (!this.f9429c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")}");
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: b */
    public int mo11709b(android.graphics.Bitmap bitmap) {
        return com.fossil.blesdk.obfuscated.C3066uw.m14922a(bitmap);
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.String m13826b(int i, android.graphics.Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    @DexIgnore
    /* renamed from: b */
    public static android.graphics.Bitmap.Config[] m13827b(android.graphics.Bitmap.Config config) {
        if (android.os.Build.VERSION.SDK_INT >= 26 && android.graphics.Bitmap.Config.RGBA_F16.equals(config)) {
            return f9423e;
        }
        int i = com.fossil.blesdk.obfuscated.C2906sq.C2907a.f9430a[config.ordinal()];
        if (i == 1) {
            return f9422d;
        }
        if (i == 2) {
            return f9424f;
        }
        if (i == 3) {
            return f9425g;
        }
        if (i == 4) {
            return f9426h;
        }
        return new android.graphics.Bitmap.Config[]{config};
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo11707a(int i, int i2, android.graphics.Bitmap.Config config) {
        com.fossil.blesdk.obfuscated.C2906sq.C2908b a = mo16132a(com.fossil.blesdk.obfuscated.C3066uw.m14920a(i, i2, config), config);
        android.graphics.Bitmap a2 = this.f9428b.mo13693a(a);
        if (a2 != null) {
            mo16134a(java.lang.Integer.valueOf(a.f9432b), a2);
            a2.reconfigure(i, i2, config);
        }
        return a2;
    }

    @DexIgnore
    /* renamed from: a */
    public final com.fossil.blesdk.obfuscated.C2906sq.C2908b mo16132a(int i, android.graphics.Bitmap.Config config) {
        com.fossil.blesdk.obfuscated.C2906sq.C2908b a = this.f9427a.mo16140a(i, config);
        android.graphics.Bitmap.Config[] b = m13827b(config);
        int length = b.length;
        int i2 = 0;
        while (i2 < length) {
            android.graphics.Bitmap.Config config2 = b[i2];
            java.lang.Integer ceilingKey = mo16133a(config2).ceilingKey(java.lang.Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i2++;
            } else {
                if (ceilingKey.intValue() == i) {
                    if (config2 == null) {
                        if (config == null) {
                            return a;
                        }
                    } else if (config2.equals(config)) {
                        return a;
                    }
                }
                this.f9427a.mo12068a(a);
                return this.f9427a.mo16140a(ceilingKey.intValue(), config2);
            }
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Bitmap mo11706a() {
        android.graphics.Bitmap a = this.f9428b.mo13692a();
        if (a != null) {
            mo16134a(java.lang.Integer.valueOf(com.fossil.blesdk.obfuscated.C3066uw.m14922a(a)), a);
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16134a(java.lang.Integer num, android.graphics.Bitmap bitmap) {
        java.util.NavigableMap<java.lang.Integer, java.lang.Integer> a = mo16133a(bitmap.getConfig());
        java.lang.Integer num2 = (java.lang.Integer) a.get(num);
        if (num2 == null) {
            throw new java.lang.NullPointerException("Tried to decrement empty size, size: " + num + ", removed: " + mo11711c(bitmap) + ", this: " + this);
        } else if (num2.intValue() == 1) {
            a.remove(num);
        } else {
            a.put(num, java.lang.Integer.valueOf(num2.intValue() - 1));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.util.NavigableMap<java.lang.Integer, java.lang.Integer> mo16133a(android.graphics.Bitmap.Config config) {
        java.util.NavigableMap<java.lang.Integer, java.lang.Integer> navigableMap = this.f9429c.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        java.util.TreeMap treeMap = new java.util.TreeMap();
        this.f9429c.put(config, treeMap);
        return treeMap;
    }
}
