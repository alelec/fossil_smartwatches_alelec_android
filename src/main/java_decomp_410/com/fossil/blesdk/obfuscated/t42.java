package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t42 implements Factory<PortfolioApp> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public t42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static t42 a(n42 n42) {
        return new t42(n42);
    }

    @DexIgnore
    public static PortfolioApp b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static PortfolioApp c(n42 n42) {
        PortfolioApp b = n42.b();
        n44.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    @DexIgnore
    public PortfolioApp get() {
        return b(this.a);
    }
}
