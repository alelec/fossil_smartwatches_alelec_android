package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import com.fossil.blesdk.obfuscated.ze0;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class af0 {
    @DexIgnore
    public /* final */ Set<ze0<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public final void a() {
        for (ze0<?> a2 : this.a) {
            a2.a();
        }
        this.a.clear();
    }

    @DexIgnore
    public static <L> ze0<L> a(L l, Looper looper, String str) {
        bk0.a(l, (Object) "Listener must not be null");
        bk0.a(looper, (Object) "Looper must not be null");
        bk0.a(str, (Object) "Listener type must not be null");
        return new ze0<>(looper, l, str);
    }

    @DexIgnore
    public static <L> ze0.a<L> a(L l, String str) {
        bk0.a(l, (Object) "Listener must not be null");
        bk0.a(str, (Object) "Listener type must not be null");
        bk0.a(str, (Object) "Listener type must not be empty");
        return new ze0.a<>(l, str);
    }
}
