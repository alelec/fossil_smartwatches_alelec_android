package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nu */
public final class C2504nu implements com.fossil.blesdk.obfuscated.C2344lu {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f7868e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2344lu.C2345a f7869f;

    @DexIgnore
    /* renamed from: g */
    public boolean f7870g;

    @DexIgnore
    /* renamed from: h */
    public boolean f7871h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ android.content.BroadcastReceiver f7872i; // = new com.fossil.blesdk.obfuscated.C2504nu.C2505a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nu$a")
    /* renamed from: com.fossil.blesdk.obfuscated.nu$a */
    public class C2505a extends android.content.BroadcastReceiver {
        @DexIgnore
        public C2505a() {
        }

        @DexIgnore
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            com.fossil.blesdk.obfuscated.C2504nu nuVar = com.fossil.blesdk.obfuscated.C2504nu.this;
            boolean z = nuVar.f7870g;
            nuVar.f7870g = nuVar.mo14095a(context);
            if (z != com.fossil.blesdk.obfuscated.C2504nu.this.f7870g) {
                if (android.util.Log.isLoggable("ConnectivityMonitor", 3)) {
                    android.util.Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + com.fossil.blesdk.obfuscated.C2504nu.this.f7870g);
                }
                com.fossil.blesdk.obfuscated.C2504nu nuVar2 = com.fossil.blesdk.obfuscated.C2504nu.this;
                nuVar2.f7869f.mo13389a(nuVar2.f7870g);
            }
        }
    }

    @DexIgnore
    public C2504nu(android.content.Context context, com.fossil.blesdk.obfuscated.C2344lu.C2345a aVar) {
        this.f7868e = context.getApplicationContext();
        this.f7869f = aVar;
    }

    @DexIgnore
    @android.annotation.SuppressLint({"MissingPermission"})
    /* renamed from: a */
    public boolean mo14095a(android.content.Context context) {
        android.net.ConnectivityManager connectivityManager = (android.net.ConnectivityManager) context.getSystemService("connectivity");
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(connectivityManager);
        try {
            android.net.NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (java.lang.RuntimeException e) {
            if (android.util.Log.isLoggable("ConnectivityMonitor", 5)) {
                android.util.Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e);
            }
            return true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14096b() {
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14097c() {
        mo14099e();
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo14098d() {
        if (!this.f7871h) {
            this.f7870g = mo14095a(this.f7868e);
            try {
                this.f7868e.registerReceiver(this.f7872i, new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.f7871h = true;
            } catch (java.lang.SecurityException e) {
                if (android.util.Log.isLoggable("ConnectivityMonitor", 5)) {
                    android.util.Log.w("ConnectivityMonitor", "Failed to register", e);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo14099e() {
        if (this.f7871h) {
            this.f7868e.unregisterReceiver(this.f7872i);
            this.f7871h = false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14094a() {
        mo14098d();
    }
}
