package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kn */
public class C2231kn extends com.fossil.blesdk.obfuscated.C2336ln<org.json.JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public C2231kn(int i, java.lang.String str, org.json.JSONObject jSONObject, com.fossil.blesdk.obfuscated.C3047um.C3049b<org.json.JSONObject> bVar, com.fossil.blesdk.obfuscated.C3047um.C3048a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    @DexIgnore
    public com.fossil.blesdk.obfuscated.C3047um<org.json.JSONObject> parseNetworkResponse(com.fossil.blesdk.obfuscated.C2897sm smVar) {
        try {
            return com.fossil.blesdk.obfuscated.C3047um.m14806a(new org.json.JSONObject(new java.lang.String(smVar.f9391b, com.fossil.blesdk.obfuscated.C1738en.m6520a(smVar.f9392c, com.fossil.blesdk.obfuscated.C2336ln.PROTOCOL_CHARSET))), com.fossil.blesdk.obfuscated.C1738en.m6518a(smVar));
        } catch (java.io.UnsupportedEncodingException e) {
            return com.fossil.blesdk.obfuscated.C3047um.m14805a(new com.android.volley.ParseError((java.lang.Throwable) e));
        } catch (org.json.JSONException e2) {
            return com.fossil.blesdk.obfuscated.C3047um.m14805a(new com.android.volley.ParseError((java.lang.Throwable) e2));
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public C2231kn(java.lang.String str, org.json.JSONObject jSONObject, com.fossil.blesdk.obfuscated.C3047um.C3049b<org.json.JSONObject> bVar, com.fossil.blesdk.obfuscated.C3047um.C3048a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }
}
