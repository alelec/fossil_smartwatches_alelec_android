package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.fossil.blesdk.obfuscated.qj0;
import com.fossil.blesdk.obfuscated.vj0;
import com.google.android.gms.common.api.Scope;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ij0<T extends IInterface> {
    @DexIgnore
    public static /* final */ wd0[] A; // = new wd0[0];
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public int d;
    @DexIgnore
    public long e;
    @DexIgnore
    public tl0 f;
    @DexIgnore
    public /* final */ Context g;
    @DexIgnore
    public /* final */ qj0 h;
    @DexIgnore
    public /* final */ yd0 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ Object k;
    @DexIgnore
    public /* final */ Object l;
    @DexIgnore
    public xj0 m;
    @DexIgnore
    public c n;
    @DexIgnore
    public T o;
    @DexIgnore
    public /* final */ ArrayList<h<?>> p;
    @DexIgnore
    public j q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ a s;
    @DexIgnore
    public /* final */ b t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ String v;
    @DexIgnore
    public ud0 w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public volatile nl0 y;
    @DexIgnore
    public AtomicInteger z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void e(Bundle bundle);

        @DexIgnore
        void f(int i);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(ud0 ud0);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(ud0 ud0);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements c {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void a(ud0 ud0) {
            if (ud0.L()) {
                ij0 ij0 = ij0.this;
                ij0.a((tj0) null, ij0.w());
            } else if (ij0.this.t != null) {
                ij0.this.t.a(ud0);
            }
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class f extends h<Boolean> {
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Bundle e;

        @DexIgnore
        public f(int i, Bundle bundle) {
            super(true);
            this.d = i;
            this.e = bundle;
        }

        @DexIgnore
        public abstract void a(ud0 ud0);

        @DexIgnore
        /* JADX WARNING: type inference failed for: r5v11, types: [android.os.Parcelable] */
        /* JADX WARNING: Multi-variable type inference failed */
        public final /* synthetic */ void a(Object obj) {
            PendingIntent pendingIntent = null;
            if (((Boolean) obj) == null) {
                ij0.this.b(1, null);
                return;
            }
            int i = this.d;
            if (i != 0) {
                if (i != 10) {
                    ij0.this.b(1, null);
                    Bundle bundle = this.e;
                    if (bundle != null) {
                        pendingIntent = bundle.getParcelable("pendingIntent");
                    }
                    a(new ud0(this.d, pendingIntent));
                    return;
                }
                ij0.this.b(1, null);
                throw new IllegalStateException(String.format("A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. ", new Object[]{getClass().getSimpleName(), ij0.this.z(), ij0.this.y()}));
            } else if (!e()) {
                ij0.this.b(1, null);
                a(new ud0(8, (PendingIntent) null));
            }
        }

        @DexIgnore
        public final void c() {
        }

        @DexIgnore
        public abstract boolean e();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g extends bz0 {
        @DexIgnore
        public g(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public static void a(Message message) {
            h hVar = (h) message.obj;
            hVar.c();
            hVar.b();
        }

        @DexIgnore
        public static boolean b(Message message) {
            int i = message.what;
            return i == 2 || i == 1 || i == 7;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: android.app.PendingIntent} */
        /* JADX WARNING: Multi-variable type inference failed */
        public final void handleMessage(Message message) {
            ud0 ud0;
            ud0 ud02;
            if (ij0.this.z.get() == message.arg1) {
                int i = message.what;
                if ((i == 1 || i == 7 || ((i == 4 && !ij0.this.q()) || message.what == 5)) && !ij0.this.e()) {
                    a(message);
                    return;
                }
                int i2 = message.what;
                PendingIntent pendingIntent = null;
                if (i2 == 4) {
                    ud0 unused = ij0.this.w = new ud0(message.arg2);
                    if (!ij0.this.E() || ij0.this.x) {
                        if (ij0.this.w != null) {
                            ud02 = ij0.this.w;
                        } else {
                            ud02 = new ud0(8);
                        }
                        ij0.this.n.a(ud02);
                        ij0.this.a(ud02);
                        return;
                    }
                    ij0.this.b(3, null);
                } else if (i2 == 5) {
                    if (ij0.this.w != null) {
                        ud0 = ij0.this.w;
                    } else {
                        ud0 = new ud0(8);
                    }
                    ij0.this.n.a(ud0);
                    ij0.this.a(ud0);
                } else if (i2 == 3) {
                    Object obj = message.obj;
                    if (obj instanceof PendingIntent) {
                        pendingIntent = obj;
                    }
                    ud0 ud03 = new ud0(message.arg2, pendingIntent);
                    ij0.this.n.a(ud03);
                    ij0.this.a(ud03);
                } else if (i2 == 6) {
                    ij0.this.b(5, null);
                    if (ij0.this.s != null) {
                        ij0.this.s.f(message.arg2);
                    }
                    ij0.this.a(message.arg2);
                    boolean unused2 = ij0.this.a(5, 1, null);
                } else if (i2 == 2 && !ij0.this.c()) {
                    a(message);
                } else if (b(message)) {
                    ((h) message.obj).d();
                } else {
                    int i3 = message.what;
                    StringBuilder sb = new StringBuilder(45);
                    sb.append("Don't know how to handle message: ");
                    sb.append(i3);
                    Log.wtf("GmsClient", sb.toString(), new Exception());
                }
            } else if (b(message)) {
                a(message);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class h<TListener> {
        @DexIgnore
        public TListener a;
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore
        public h(TListener tlistener) {
            this.a = tlistener;
        }

        @DexIgnore
        public final void a() {
            synchronized (this) {
                this.a = null;
            }
        }

        @DexIgnore
        public abstract void a(TListener tlistener);

        @DexIgnore
        public final void b() {
            a();
            synchronized (ij0.this.p) {
                ij0.this.p.remove(this);
            }
        }

        @DexIgnore
        public abstract void c();

        @DexIgnore
        public final void d() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.a;
                if (this.b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
            }
            if (tlistener != null) {
                try {
                    a(tlistener);
                } catch (RuntimeException e) {
                    c();
                    throw e;
                }
            } else {
                c();
            }
            synchronized (this) {
                this.b = true;
            }
            b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements ServiceConnection {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public j(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            xj0 xj0;
            if (iBinder == null) {
                ij0.this.c(16);
                return;
            }
            synchronized (ij0.this.l) {
                ij0 ij0 = ij0.this;
                if (iBinder == null) {
                    xj0 = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof xj0)) {
                        xj0 = new wj0(iBinder);
                    } else {
                        xj0 = (xj0) queryLocalInterface;
                    }
                }
                xj0 unused = ij0.m = xj0;
            }
            ij0.this.a(0, (Bundle) null, this.a);
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (ij0.this.l) {
                xj0 unused = ij0.this.m = null;
            }
            Handler handler = ij0.this.j;
            handler.sendMessage(handler.obtainMessage(6, this.a, 1));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k extends f {
        @DexIgnore
        public /* final */ IBinder g;

        @DexIgnore
        public k(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.g = iBinder;
        }

        @DexIgnore
        public final void a(ud0 ud0) {
            if (ij0.this.t != null) {
                ij0.this.t.a(ud0);
            }
            ij0.this.a(ud0);
        }

        @DexIgnore
        public final boolean e() {
            try {
                String interfaceDescriptor = this.g.getInterfaceDescriptor();
                if (!ij0.this.y().equals(interfaceDescriptor)) {
                    String y = ij0.this.y();
                    StringBuilder sb = new StringBuilder(String.valueOf(y).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(y);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                IInterface a = ij0.this.a(this.g);
                if (a == null || (!ij0.this.a(2, 4, a) && !ij0.this.a(3, 4, a))) {
                    return false;
                }
                ud0 unused = ij0.this.w = null;
                Bundle n = ij0.this.n();
                if (ij0.this.s == null) {
                    return true;
                }
                ij0.this.s.e(n);
                return true;
            } catch (RemoteException unused2) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l extends f {
        @DexIgnore
        public l(int i, Bundle bundle) {
            super(i, (Bundle) null);
        }

        @DexIgnore
        public final void a(ud0 ud0) {
            if (!ij0.this.q() || !ij0.this.E()) {
                ij0.this.n.a(ud0);
                ij0.this.a(ud0);
                return;
            }
            ij0.this.c(16);
        }

        @DexIgnore
        public final boolean e() {
            ij0.this.n.a(ud0.i);
            return true;
        }
    }

    /*
    static {
        new String[]{"service_esmobile", "service_googleme"};
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ij0(Context context, Looper looper, int i2, a aVar, b bVar, String str) {
        this(context, looper, r3, r4, i2, aVar, bVar, str);
        qj0 a2 = qj0.a(context);
        yd0 a3 = yd0.a();
        bk0.a(aVar);
        bk0.a(bVar);
    }

    @DexIgnore
    public String A() {
        return "com.google.android.gms";
    }

    @DexIgnore
    public boolean B() {
        return false;
    }

    @DexIgnore
    public final String C() {
        String str = this.v;
        return str == null ? this.g.getClass().getName() : str;
    }

    @DexIgnore
    public final boolean D() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 3;
        }
        return z2;
    }

    @DexIgnore
    public final boolean E() {
        if (this.x || TextUtils.isEmpty(y()) || TextUtils.isEmpty(v())) {
            return false;
        }
        try {
            Class.forName(y());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public abstract T a(IBinder iBinder);

    @DexIgnore
    public void a(int i2, T t2) {
    }

    @DexIgnore
    public final void a(nl0 nl0) {
        this.y = nl0;
    }

    @DexIgnore
    public final void b(int i2, T t2) {
        tl0 tl0;
        bk0.a((i2 == 4) == (t2 != null));
        synchronized (this.k) {
            this.r = i2;
            this.o = t2;
            a(i2, t2);
            if (i2 != 1) {
                if (i2 == 2 || i2 == 3) {
                    if (!(this.q == null || this.f == null)) {
                        String c2 = this.f.c();
                        String a2 = this.f.a();
                        StringBuilder sb = new StringBuilder(String.valueOf(c2).length() + 70 + String.valueOf(a2).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(c2);
                        sb.append(" on ");
                        sb.append(a2);
                        Log.e("GmsClient", sb.toString());
                        this.h.a(this.f.c(), this.f.a(), this.f.b(), this.q, C());
                        this.z.incrementAndGet();
                    }
                    this.q = new j(this.z.get());
                    if (this.r != 3 || v() == null) {
                        tl0 = new tl0(A(), z(), false, 129);
                    } else {
                        tl0 = new tl0(t().getPackageName(), v(), true, 129);
                    }
                    this.f = tl0;
                    if (!this.h.a(new qj0.a(this.f.c(), this.f.a(), this.f.b()), this.q, C())) {
                        String c3 = this.f.c();
                        String a3 = this.f.a();
                        StringBuilder sb2 = new StringBuilder(String.valueOf(c3).length() + 34 + String.valueOf(a3).length());
                        sb2.append("unable to connect to service: ");
                        sb2.append(c3);
                        sb2.append(" on ");
                        sb2.append(a3);
                        Log.e("GmsClient", sb2.toString());
                        a(16, (Bundle) null, this.z.get());
                    }
                } else if (i2 == 4) {
                    a(t2);
                }
            } else if (this.q != null) {
                this.h.a(this.f.c(), this.f.a(), this.f.b(), this.q, C());
                this.q = null;
            }
        }
    }

    @DexIgnore
    public boolean c() {
        boolean z2;
        synchronized (this.k) {
            z2 = this.r == 4;
        }
        return z2;
    }

    @DexIgnore
    public boolean d() {
        return false;
    }

    @DexIgnore
    public boolean e() {
        boolean z2;
        synchronized (this.k) {
            if (this.r != 2) {
                if (this.r != 3) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    @DexIgnore
    public String f() {
        if (c()) {
            tl0 tl0 = this.f;
            if (tl0 != null) {
                return tl0.a();
            }
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    @DexIgnore
    public boolean h() {
        return true;
    }

    @DexIgnore
    public int i() {
        return yd0.a;
    }

    @DexIgnore
    public final wd0[] j() {
        nl0 nl0 = this.y;
        if (nl0 == null) {
            return null;
        }
        return nl0.f;
    }

    @DexIgnore
    public Intent k() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    @DexIgnore
    public boolean l() {
        return false;
    }

    @DexIgnore
    public IBinder m() {
        synchronized (this.l) {
            if (this.m == null) {
                return null;
            }
            IBinder asBinder = this.m.asBinder();
            return asBinder;
        }
    }

    @DexIgnore
    public Bundle n() {
        return null;
    }

    @DexIgnore
    public void o() {
        int a2 = this.i.a(this.g, i());
        if (a2 != 0) {
            b(1, (IInterface) null);
            a((c) new d(), a2, (PendingIntent) null);
            return;
        }
        a((c) new d());
    }

    @DexIgnore
    public final void p() {
        if (!c()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    @DexIgnore
    public boolean q() {
        return false;
    }

    @DexIgnore
    public Account r() {
        return null;
    }

    @DexIgnore
    public wd0[] s() {
        return A;
    }

    @DexIgnore
    public final Context t() {
        return this.g;
    }

    @DexIgnore
    public Bundle u() {
        return new Bundle();
    }

    @DexIgnore
    public String v() {
        return null;
    }

    @DexIgnore
    public Set<Scope> w() {
        return Collections.EMPTY_SET;
    }

    @DexIgnore
    public final T x() throws DeadObjectException {
        T t2;
        synchronized (this.k) {
            if (this.r != 5) {
                p();
                bk0.b(this.o != null, "Client is connected but service is null");
                t2 = this.o;
            } else {
                throw new DeadObjectException();
            }
        }
        return t2;
    }

    @DexIgnore
    public abstract String y();

    @DexIgnore
    public abstract String z();

    @DexIgnore
    public void a(T t2) {
        this.c = System.currentTimeMillis();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends vj0.a {
        @DexIgnore
        public ij0 e;
        @DexIgnore
        public /* final */ int f;

        @DexIgnore
        public i(ij0 ij0, int i) {
            this.e = ij0;
            this.f = i;
        }

        @DexIgnore
        public final void a(int i, IBinder iBinder, Bundle bundle) {
            bk0.a(this.e, (Object) "onPostInitComplete can be called only once per call to getRemoteService");
            this.e.a(i, iBinder, bundle, this.f);
            this.e = null;
        }

        @DexIgnore
        public final void b(int i, Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        @DexIgnore
        public final void a(int i, IBinder iBinder, nl0 nl0) {
            bk0.a(this.e, (Object) "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            bk0.a(nl0);
            this.e.a(nl0);
            a(i, iBinder, nl0.e);
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.a = i2;
        this.b = System.currentTimeMillis();
    }

    @DexIgnore
    public final void c(int i2) {
        int i3;
        if (D()) {
            i3 = 5;
            this.x = true;
        } else {
            i3 = 4;
        }
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(i3, this.z.get(), 16));
    }

    @DexIgnore
    public void a(ud0 ud0) {
        this.d = ud0.H();
        this.e = System.currentTimeMillis();
    }

    @DexIgnore
    public ij0(Context context, Looper looper, qj0 qj0, yd0 yd0, int i2, a aVar, b bVar, String str) {
        this.k = new Object();
        this.l = new Object();
        this.p = new ArrayList<>();
        this.r = 1;
        this.w = null;
        this.x = false;
        this.y = null;
        this.z = new AtomicInteger(0);
        bk0.a(context, (Object) "Context must not be null");
        this.g = context;
        bk0.a(looper, (Object) "Looper must not be null");
        Looper looper2 = looper;
        bk0.a(qj0, (Object) "Supervisor must not be null");
        this.h = qj0;
        bk0.a(yd0, (Object) "API availability must not be null");
        this.i = yd0;
        this.j = new g(looper);
        this.u = i2;
        this.s = aVar;
        this.t = bVar;
        this.v = str;
    }

    @DexIgnore
    public final boolean a(int i2, int i3, T t2) {
        synchronized (this.k) {
            if (this.r != i2) {
                return false;
            }
            b(i3, t2);
            return true;
        }
    }

    @DexIgnore
    public void a(c cVar) {
        bk0.a(cVar, (Object) "Connection progress callbacks cannot be null.");
        this.n = cVar;
        b(2, (IInterface) null);
    }

    @DexIgnore
    public void a() {
        this.z.incrementAndGet();
        synchronized (this.p) {
            int size = this.p.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.p.get(i2).a();
            }
            this.p.clear();
        }
        synchronized (this.l) {
            this.m = null;
        }
        b(1, (IInterface) null);
    }

    @DexIgnore
    public void a(c cVar, int i2, PendingIntent pendingIntent) {
        bk0.a(cVar, (Object) "Connection progress callbacks cannot be null.");
        this.n = cVar;
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(3, this.z.get(), i2, pendingIntent));
    }

    @DexIgnore
    public void a(int i2, IBinder iBinder, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(1, i3, -1, new k(i2, iBinder, bundle)));
    }

    @DexIgnore
    public final void a(int i2, Bundle bundle, int i3) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(7, i3, -1, new l(i2, (Bundle) null)));
    }

    @DexIgnore
    public void a(tj0 tj0, Set<Scope> set) {
        Bundle u2 = u();
        nj0 nj0 = new nj0(this.u);
        nj0.h = this.g.getPackageName();
        nj0.k = u2;
        if (set != null) {
            nj0.j = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (l()) {
            nj0.l = r() != null ? r() : new Account("<<default account>>", "com.google");
            if (tj0 != null) {
                nj0.i = tj0.asBinder();
            }
        } else if (B()) {
            nj0.l = r();
        }
        nj0.m = A;
        nj0.n = s();
        try {
            synchronized (this.l) {
                if (this.m != null) {
                    this.m.a(new i(this, this.z.get()), nj0);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e2);
            b(1);
        } catch (SecurityException e3) {
            throw e3;
        } catch (RemoteException | RuntimeException e4) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e4);
            a(8, (IBinder) null, (Bundle) null, this.z.get());
        }
    }

    @DexIgnore
    public void b(int i2) {
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(6, this.z.get(), i2));
    }

    @DexIgnore
    public void a(e eVar) {
        eVar.a();
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i2;
        T t2;
        xj0 xj0;
        synchronized (this.k) {
            i2 = this.r;
            t2 = this.o;
        }
        synchronized (this.l) {
            xj0 = this.m;
        }
        printWriter.append(str).append("mConnectState=");
        if (i2 == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i2 == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i2 == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i2 == 4) {
            printWriter.print("CONNECTED");
        } else if (i2 != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append(" mService=");
        if (t2 == null) {
            printWriter.append("null");
        } else {
            printWriter.append(y()).append("@").append(Integer.toHexString(System.identityHashCode(t2.asBinder())));
        }
        printWriter.append(" mServiceBroker=");
        if (xj0 == null) {
            printWriter.println("null");
        } else {
            printWriter.append("IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(xj0.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.c > 0) {
            PrintWriter append = printWriter.append(str).append("lastConnectedTime=");
            long j2 = this.c;
            String format = simpleDateFormat.format(new Date(j2));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j2);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.b > 0) {
            printWriter.append(str).append("lastSuspendedCause=");
            int i3 = this.a;
            if (i3 == 1) {
                printWriter.append("CAUSE_SERVICE_DISCONNECTED");
            } else if (i3 != 2) {
                printWriter.append(String.valueOf(i3));
            } else {
                printWriter.append("CAUSE_NETWORK_LOST");
            }
            PrintWriter append2 = printWriter.append(" lastSuspendedTime=");
            long j3 = this.b;
            String format2 = simpleDateFormat.format(new Date(j3));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j3);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.e > 0) {
            printWriter.append(str).append("lastFailedStatus=").append(ee0.getStatusCodeString(this.d));
            PrintWriter append3 = printWriter.append(" lastFailedTime=");
            long j4 = this.e;
            String format3 = simpleDateFormat.format(new Date(j4));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j4);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }
}
