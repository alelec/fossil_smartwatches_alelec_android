package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class p10 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[GattCharacteristic.CharacteristicId.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[GattCharacteristic.CharacteristicId.values().length];

    /*
    static {
        a[GattCharacteristic.CharacteristicId.MODEL_NUMBER.ordinal()] = 1;
        a[GattCharacteristic.CharacteristicId.SERIAL_NUMBER.ordinal()] = 2;
        a[GattCharacteristic.CharacteristicId.FIRMWARE_VERSION.ordinal()] = 3;
        a[GattCharacteristic.CharacteristicId.SOFTWARE_REVISION.ordinal()] = 4;
        a[GattCharacteristic.CharacteristicId.DC.ordinal()] = 5;
        a[GattCharacteristic.CharacteristicId.FTC.ordinal()] = 6;
        a[GattCharacteristic.CharacteristicId.FTD.ordinal()] = 7;
        a[GattCharacteristic.CharacteristicId.FTD_1.ordinal()] = 8;
        a[GattCharacteristic.CharacteristicId.AUTHENTICATION.ordinal()] = 9;
        a[GattCharacteristic.CharacteristicId.ASYNC.ordinal()] = 10;
        b[GattCharacteristic.CharacteristicId.MODEL_NUMBER.ordinal()] = 1;
        b[GattCharacteristic.CharacteristicId.SERIAL_NUMBER.ordinal()] = 2;
        b[GattCharacteristic.CharacteristicId.FIRMWARE_VERSION.ordinal()] = 3;
        b[GattCharacteristic.CharacteristicId.SOFTWARE_REVISION.ordinal()] = 4;
        b[GattCharacteristic.CharacteristicId.DC.ordinal()] = 5;
        b[GattCharacteristic.CharacteristicId.FTC.ordinal()] = 6;
        b[GattCharacteristic.CharacteristicId.ASYNC.ordinal()] = 7;
        b[GattCharacteristic.CharacteristicId.FTD.ordinal()] = 8;
        b[GattCharacteristic.CharacteristicId.FTD_1.ordinal()] = 9;
        b[GattCharacteristic.CharacteristicId.AUTHENTICATION.ordinal()] = 10;
    }
    */
}
