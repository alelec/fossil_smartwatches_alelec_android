package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gw implements jo {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ jo c;

    @DexIgnore
    public gw(int i, jo joVar) {
        this.b = i;
        this.c = joVar;
    }

    @DexIgnore
    public static jo a(Context context) {
        return new gw(context.getResources().getConfiguration().uiMode & 48, hw.b(context));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof gw)) {
            return false;
        }
        gw gwVar = (gw) obj;
        if (this.b != gwVar.b || !this.c.equals(gwVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return uw.a((Object) this.c, this.b);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        this.c.a(messageDigest);
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.b).array());
    }
}
