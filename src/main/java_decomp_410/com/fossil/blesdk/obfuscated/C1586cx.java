package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cx */
public class C1586cx {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1752ex f4212a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.String, java.lang.Object> f4213b; // = new java.util.concurrent.ConcurrentHashMap();

    @DexIgnore
    public C1586cx(com.fossil.blesdk.obfuscated.C1752ex exVar) {
        this.f4212a = exVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9706a(java.lang.String str, java.lang.String str2) {
        if (!this.f4212a.mo10636a((java.lang.Object) str, "key") && !this.f4212a.mo10636a((java.lang.Object) str2, "value")) {
            mo9705a(this.f4212a.mo10634a(str), (java.lang.Object) this.f4212a.mo10634a(str2));
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        return new org.json.JSONObject(this.f4213b).toString();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9704a(java.lang.String str, java.lang.Number number) {
        if (!this.f4212a.mo10636a((java.lang.Object) str, "key") && !this.f4212a.mo10636a((java.lang.Object) number, "value")) {
            mo9705a(this.f4212a.mo10634a(str), (java.lang.Object) number);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9705a(java.lang.String str, java.lang.Object obj) {
        if (!this.f4212a.mo10637a(this.f4213b, str)) {
            this.f4213b.put(str, obj);
        }
    }
}
