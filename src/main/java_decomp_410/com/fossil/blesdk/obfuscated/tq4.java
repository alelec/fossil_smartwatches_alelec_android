package com.fossil.blesdk.obfuscated;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import com.fossil.blesdk.obfuscated.c0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tq4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String[] f;

    @DexIgnore
    public tq4(String str, String str2, String str3, int i, int i2, String[] strArr) {
        this.a = str;
        this.b = str2;
        this.e = str3;
        this.c = i;
        this.d = i2;
        this.f = strArr;
    }

    @DexIgnore
    public Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("positiveButton", this.a);
        bundle.putString("negativeButton", this.b);
        bundle.putString("rationaleMsg", this.e);
        bundle.putInt("theme", this.c);
        bundle.putInt("requestCode", this.d);
        bundle.putStringArray("permissions", this.f);
        return bundle;
    }

    @DexIgnore
    public c0 b(Context context, DialogInterface.OnClickListener onClickListener) {
        c0.a aVar;
        int i = this.c;
        if (i > 0) {
            aVar = new c0.a(context, i);
        } else {
            aVar = new c0.a(context);
        }
        aVar.a(false);
        aVar.b(this.a, onClickListener);
        aVar.a((CharSequence) this.b, onClickListener);
        aVar.a((CharSequence) this.e);
        return aVar.a();
    }

    @DexIgnore
    public tq4(Bundle bundle) {
        this.a = bundle.getString("positiveButton");
        this.b = bundle.getString("negativeButton");
        this.e = bundle.getString("rationaleMsg");
        this.c = bundle.getInt("theme");
        this.d = bundle.getInt("requestCode");
        this.f = bundle.getStringArray("permissions");
    }

    @DexIgnore
    public AlertDialog a(Context context, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder;
        int i = this.c;
        if (i > 0) {
            builder = new AlertDialog.Builder(context, i);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        return builder.setCancelable(false).setPositiveButton(this.a, onClickListener).setNegativeButton(this.b, onClickListener).setMessage(this.e).create();
    }
}
