package com.fossil.blesdk.obfuscated;

import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface pu1<K, V> {
    @DexIgnore
    Map<K, Collection<V>> asMap();

    @DexIgnore
    void clear();

    @DexIgnore
    boolean containsEntry(Object obj, Object obj2);

    @DexIgnore
    boolean containsKey(Object obj);

    @DexIgnore
    Collection<Map.Entry<K, V>> entries();

    @DexIgnore
    Collection<V> get(K k);

    @DexIgnore
    boolean isEmpty();

    @DexIgnore
    Set<K> keySet();

    @DexIgnore
    boolean put(K k, V v);

    @DexIgnore
    boolean putAll(K k, Iterable<? extends V> iterable);

    @DexIgnore
    boolean remove(Object obj, Object obj2);

    @DexIgnore
    int size();
}
