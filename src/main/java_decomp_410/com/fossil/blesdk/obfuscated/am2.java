package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class am2 implements Cloneable {
    @DexIgnore
    public int e;
    @DexIgnore
    public byte[] f;

    @DexIgnore
    public am2(int i, byte[] bArr) {
        if (bArr != null) {
            this.e = i;
            this.f = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.f, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    @DexIgnore
    public void a(byte[] bArr) {
        if (bArr != null) {
            this.f = new byte[bArr.length];
            System.arraycopy(bArr, 0, this.f, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    @DexIgnore
    public byte[] b() {
        byte[] bArr = this.f;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        byte[] bArr = this.f;
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        try {
            return new am2(this.e, bArr2);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "failed to clone an EncodedStringValue: " + this);
            e2.printStackTrace();
            throw new CloneNotSupportedException(e2.getMessage());
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        return new java.lang.String(r3.f);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        return new java.lang.String(r3.f, "iso-8859-1");
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0018 */
    public String a() {
        int i = this.e;
        if (i == 0) {
            return new String(this.f);
        }
        return new String(this.f, yl2.a(i));
    }

    @DexIgnore
    public am2(byte[] bArr) {
        this(106, bArr);
    }

    @DexIgnore
    public am2(String str) {
        try {
            this.f = str.getBytes(ln.PROTOCOL_CHARSET);
            this.e = 106;
        } catch (UnsupportedEncodingException e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("EncodedStringValue", "Default encoding must be supported=" + e2);
        }
    }
}
