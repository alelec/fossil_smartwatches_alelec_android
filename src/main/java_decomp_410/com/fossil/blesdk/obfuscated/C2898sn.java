package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sn */
public final class C2898sn {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C3371yn<?, ?>> f9395a; // = new com.fossil.blesdk.obfuscated.C1855g4();

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2755qp f9396b;

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C2149jq f9397c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1885gq f9398d;

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1440ar f9399e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1650dr f9400f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1650dr f9401g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2981tq.C2982a f9402h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C1500br f9403i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C2436mu f9404j;

    @DexIgnore
    /* renamed from: k */
    public int f9405k; // = 4;

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C2815rn.C2816a f9406l; // = new com.fossil.blesdk.obfuscated.C2898sn.C2899a(this);

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C3062uu.C3064b f9407m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C1650dr f9408n;

    @DexIgnore
    /* renamed from: o */
    public boolean f9409o;

    @DexIgnore
    /* renamed from: p */
    public java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> f9410p;

    @DexIgnore
    /* renamed from: q */
    public boolean f9411q;

    @DexIgnore
    /* renamed from: r */
    public boolean f9412r;

    @DexIgnore
    /* renamed from: s */
    public int f9413s; // = 700;

    @DexIgnore
    /* renamed from: t */
    public int f9414t; // = 128;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.sn$a")
    /* renamed from: com.fossil.blesdk.obfuscated.sn$a */
    public class C2899a implements com.fossil.blesdk.obfuscated.C2815rn.C2816a {
        @DexIgnore
        public C2899a(com.fossil.blesdk.obfuscated.C2898sn snVar) {
        }

        @DexIgnore
        public com.fossil.blesdk.obfuscated.C2842rv build() {
            return new com.fossil.blesdk.obfuscated.C2842rv();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16121a(com.fossil.blesdk.obfuscated.C3062uu.C3064b bVar) {
        this.f9407m = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2815rn mo16120a(android.content.Context context) {
        android.content.Context context2 = context;
        if (this.f9400f == null) {
            this.f9400f = com.fossil.blesdk.obfuscated.C1650dr.m6031d();
        }
        if (this.f9401g == null) {
            this.f9401g = com.fossil.blesdk.obfuscated.C1650dr.m6030c();
        }
        if (this.f9408n == null) {
            this.f9408n = com.fossil.blesdk.obfuscated.C1650dr.m6028b();
        }
        if (this.f9403i == null) {
            this.f9403i = new com.fossil.blesdk.obfuscated.C1500br.C1501a(context2).mo9264a();
        }
        if (this.f9404j == null) {
            this.f9404j = new com.fossil.blesdk.obfuscated.C2603ou();
        }
        if (this.f9397c == null) {
            int b = this.f9403i.mo9262b();
            if (b > 0) {
                this.f9397c = new com.fossil.blesdk.obfuscated.C2669pq((long) b);
            } else {
                this.f9397c = new com.fossil.blesdk.obfuscated.C2236kq();
            }
        }
        if (this.f9398d == null) {
            this.f9398d = new com.fossil.blesdk.obfuscated.C2584oq(this.f9403i.mo9260a());
        }
        if (this.f9399e == null) {
            this.f9399e = new com.fossil.blesdk.obfuscated.C3459zq((long) this.f9403i.mo9263c());
        }
        if (this.f9402h == null) {
            this.f9402h = new com.fossil.blesdk.obfuscated.C3377yq(context2);
        }
        if (this.f9396b == null) {
            com.fossil.blesdk.obfuscated.C2755qp qpVar = new com.fossil.blesdk.obfuscated.C2755qp(this.f9399e, this.f9402h, this.f9401g, this.f9400f, com.fossil.blesdk.obfuscated.C1650dr.m6032e(), this.f9408n, this.f9409o);
            this.f9396b = qpVar;
        }
        java.util.List<com.fossil.blesdk.obfuscated.C2771qv<java.lang.Object>> list = this.f9410p;
        if (list == null) {
            this.f9410p = java.util.Collections.emptyList();
        } else {
            this.f9410p = java.util.Collections.unmodifiableList(list);
        }
        android.content.Context context3 = context;
        com.fossil.blesdk.obfuscated.C2815rn rnVar = new com.fossil.blesdk.obfuscated.C2815rn(context3, this.f9396b, this.f9399e, this.f9397c, this.f9398d, new com.fossil.blesdk.obfuscated.C3062uu(this.f9407m), this.f9404j, this.f9405k, this.f9406l, this.f9395a, this.f9410p, this.f9411q, this.f9412r, this.f9413s, this.f9414t);
        return rnVar;
    }
}
