package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uw0 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> g;
    @DexIgnore
    public /* final */ /* synthetic */ mw0 h;

    @DexIgnore
    public uw0(mw0 mw0) {
        this.h = mw0;
        this.e = -1;
    }

    @DexIgnore
    public /* synthetic */ uw0(mw0 mw0, nw0 nw0) {
        this(mw0);
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> a() {
        if (this.g == null) {
            this.g = this.h.g.entrySet().iterator();
        }
        return this.g;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e + 1 < this.h.f.size() || (!this.h.g.isEmpty() && a().hasNext());
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        this.f = true;
        int i = this.e + 1;
        this.e = i;
        return (Map.Entry) (i < this.h.f.size() ? this.h.f.get(this.e) : a().next());
    }

    @DexIgnore
    public final void remove() {
        if (this.f) {
            this.f = false;
            this.h.e();
            if (this.e < this.h.f.size()) {
                mw0 mw0 = this.h;
                int i = this.e;
                this.e = i - 1;
                Object unused = mw0.b(i);
                return;
            }
            a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }
}
