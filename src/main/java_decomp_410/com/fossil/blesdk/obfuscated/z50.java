package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.config.DeviceConfigItem;
import com.fossil.blesdk.device.data.config.DeviceConfigKey;
import com.fossil.blesdk.device.data.file.DeviceConfigFileFormat;
import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.TransmitDataPhase;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import kotlin.TypeCastException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z50 extends TransmitDataPhase {
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public DeviceConfigKey[] Q;
    @DexIgnore
    public /* final */ DeviceConfigItem[] R;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ z50(Peripheral peripheral, Phase.a aVar, DeviceConfigItem[] deviceConfigItemArr, short s, String str, int i, fd4 fd4) {
        this(peripheral, aVar, deviceConfigItemArr, r4, str);
        short b = (i & 8) != 0 ? z40.b.b(peripheral.k(), FileType.DEVICE_CONFIG) : s;
        if ((i & 16) != 0) {
            str = UUID.randomUUID().toString();
            kd4.a((Object) str, "UUID.randomUUID().toString()");
        }
    }

    @DexIgnore
    public byte[] F() {
        DeviceConfigFileFormat deviceConfigFileFormat = DeviceConfigFileFormat.e;
        short A = A();
        Version version = e().getDeviceInformation().getSupportedFilesVersion$blesdk_productionRelease().get(Short.valueOf(FileType.DEVICE_CONFIG.getFileHandleMask$blesdk_productionRelease()));
        if (version == null) {
            version = ua0.y.g();
        }
        return deviceConfigFileFormat.a(A, version, this.R);
    }

    @DexIgnore
    public void I() {
        String str;
        DeviceConfigItem[] deviceConfigItemArr = this.R;
        ArrayList arrayList = new ArrayList(deviceConfigItemArr.length);
        for (DeviceConfigItem key : deviceConfigItemArr) {
            arrayList.add(key.getKey());
        }
        Object[] array = arrayList.toArray(new DeviceConfigKey[0]);
        if (array != null) {
            this.Q = (DeviceConfigKey[]) array;
            t90 t90 = t90.c;
            String r = r();
            StringBuilder sb = new StringBuilder();
            sb.append("affectedConfigs: ");
            DeviceConfigKey[] deviceConfigKeyArr = this.Q;
            if (deviceConfigKeyArr != null) {
                str = Arrays.toString(deviceConfigKeyArr);
                kd4.a((Object) str, "java.util.Arrays.toString(this)");
            } else {
                str = null;
            }
            sb.append(str);
            t90.a(r, sb.toString());
            super.I();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public boolean c() {
        return this.P;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.CONFIGS, j00.a(this.R));
    }

    @DexIgnore
    public JSONObject x() {
        JSONObject x = super.x();
        JSONKey jSONKey = JSONKey.AFFECTED_CONFIGS;
        DeviceConfigKey[] deviceConfigKeyArr = this.Q;
        return wa0.a(x, jSONKey, deviceConfigKeyArr != null ? i20.a(deviceConfigKeyArr) : null);
    }

    @DexIgnore
    public DeviceConfigKey[] i() {
        DeviceConfigKey[] deviceConfigKeyArr = this.Q;
        return deviceConfigKeyArr != null ? deviceConfigKeyArr : new DeviceConfigKey[0];
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public z50(Peripheral peripheral, Phase.a aVar, DeviceConfigItem[] deviceConfigItemArr, short s, String str) {
        super(peripheral, aVar, PhaseId.SET_DEVICE_CONFIGS, true, s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, r7, 32, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(deviceConfigItemArr, "configs");
        String str2 = str;
        kd4.b(str2, "phaseUuid");
        this.R = deviceConfigItemArr;
        this.P = true;
    }
}
