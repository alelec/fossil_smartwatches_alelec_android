package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ie */
public class C2016ie implements com.fossil.blesdk.obfuscated.C3034ue {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3034ue f6020a;

    @DexIgnore
    /* renamed from: b */
    public int f6021b; // = 0;

    @DexIgnore
    /* renamed from: c */
    public int f6022c; // = -1;

    @DexIgnore
    /* renamed from: d */
    public int f6023d; // = -1;

    @DexIgnore
    /* renamed from: e */
    public java.lang.Object f6024e; // = null;

    @DexIgnore
    public C2016ie(com.fossil.blesdk.obfuscated.C3034ue ueVar) {
        this.f6020a = ueVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11922a() {
        int i = this.f6021b;
        if (i != 0) {
            if (i == 1) {
                this.f6020a.mo11201b(this.f6022c, this.f6023d);
            } else if (i == 2) {
                this.f6020a.mo11202c(this.f6022c, this.f6023d);
            } else if (i == 3) {
                this.f6020a.mo11200a(this.f6022c, this.f6023d, this.f6024e);
            }
            this.f6024e = null;
            this.f6021b = 0;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11201b(int i, int i2) {
        if (this.f6021b == 1) {
            int i3 = this.f6022c;
            if (i >= i3) {
                int i4 = this.f6023d;
                if (i <= i3 + i4) {
                    this.f6023d = i4 + i2;
                    this.f6022c = java.lang.Math.min(i, i3);
                    return;
                }
            }
        }
        mo11922a();
        this.f6022c = i;
        this.f6023d = i2;
        this.f6021b = 1;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11202c(int i, int i2) {
        if (this.f6021b == 2) {
            int i3 = this.f6022c;
            if (i3 >= i && i3 <= i + i2) {
                this.f6023d += i2;
                this.f6022c = i;
                return;
            }
        }
        mo11922a();
        this.f6022c = i;
        this.f6023d = i2;
        this.f6021b = 2;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11199a(int i, int i2) {
        mo11922a();
        this.f6020a.mo11199a(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11200a(int i, int i2, java.lang.Object obj) {
        if (this.f6021b == 3) {
            int i3 = this.f6022c;
            int i4 = this.f6023d;
            if (i <= i3 + i4) {
                int i5 = i + i2;
                if (i5 >= i3 && this.f6024e == obj) {
                    this.f6022c = java.lang.Math.min(i, i3);
                    this.f6023d = java.lang.Math.max(i4 + i3, i5) - this.f6022c;
                    return;
                }
            }
        }
        mo11922a();
        this.f6022c = i;
        this.f6023d = i2;
        this.f6024e = obj;
        this.f6021b = 3;
    }
}
