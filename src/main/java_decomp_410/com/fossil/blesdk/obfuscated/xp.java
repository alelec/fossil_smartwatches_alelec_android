package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xp {
    @DexIgnore
    public /* final */ Map<jo, rp<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<jo, rp<?>> b; // = new HashMap();

    @DexIgnore
    public rp<?> a(jo joVar, boolean z) {
        return a(z).get(joVar);
    }

    @DexIgnore
    public void b(jo joVar, rp<?> rpVar) {
        Map<jo, rp<?>> a2 = a(rpVar.g());
        if (rpVar.equals(a2.get(joVar))) {
            a2.remove(joVar);
        }
    }

    @DexIgnore
    public void a(jo joVar, rp<?> rpVar) {
        a(rpVar.g()).put(joVar, rpVar);
    }

    @DexIgnore
    public final Map<jo, rp<?>> a(boolean z) {
        return z ? this.b : this.a;
    }
}
