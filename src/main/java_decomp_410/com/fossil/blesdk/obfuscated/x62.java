package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x62 {
    @DexIgnore
    public /* final */ Gson a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public x62() {
        rz1 rz1 = new rz1();
        rz1.a(Date.class, new GsonConverterShortDate());
        Gson a2 = rz1.a();
        kd4.a((Object) a2, "GsonBuilder().registerTy\u2026rterShortDate()).create()");
        this.a = a2;
    }

    @DexIgnore
    public final String a(ActivityStatistic.ActivityDailyBest activityDailyBest) {
        if (activityDailyBest == null) {
            return null;
        }
        try {
            return this.a.a((Object) activityDailyBest);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("fromActivityDailyBest - e=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ActivityStatistic.CaloriesBestDay b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (ActivityStatistic.CaloriesBestDay) this.a.a(str, ActivityStatistic.CaloriesBestDay.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toCaloriesBestDay - e=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ActivityStatistic.ActivityDailyBest a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (ActivityStatistic.ActivityDailyBest) this.a.a(str, ActivityStatistic.ActivityDailyBest.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toActivityDailyBest - e=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(ActivityStatistic.CaloriesBestDay caloriesBestDay) {
        if (caloriesBestDay == null) {
            return null;
        }
        try {
            return this.a.a((Object) caloriesBestDay);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("fromCaloriesBestDay - e=");
            e.printStackTrace();
            sb.append(qa4.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }
}
