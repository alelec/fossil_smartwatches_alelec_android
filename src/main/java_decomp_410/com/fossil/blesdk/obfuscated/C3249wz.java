package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wz */
public class C3249wz implements com.fossil.blesdk.obfuscated.c00 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ int f10756a;

    @DexIgnore
    public C3249wz(int i) {
        this.f10756a = i;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.StackTraceElement[] mo9374a(java.lang.StackTraceElement[] stackTraceElementArr) {
        java.lang.StackTraceElement[] a = m16077a(stackTraceElementArr, this.f10756a);
        return a.length < stackTraceElementArr.length ? a : stackTraceElementArr;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.StackTraceElement[] m16077a(java.lang.StackTraceElement[] stackTraceElementArr, int i) {
        int i2;
        java.util.HashMap hashMap = new java.util.HashMap();
        java.lang.StackTraceElement[] stackTraceElementArr2 = new java.lang.StackTraceElement[stackTraceElementArr.length];
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i3 < stackTraceElementArr.length) {
            java.lang.StackTraceElement stackTraceElement = stackTraceElementArr[i3];
            java.lang.Integer num = (java.lang.Integer) hashMap.get(stackTraceElement);
            if (num == null || !m16076a(stackTraceElementArr, num.intValue(), i3)) {
                stackTraceElementArr2[i4] = stackTraceElementArr[i3];
                i4++;
                i2 = i3;
                i5 = 1;
            } else {
                int intValue = i3 - num.intValue();
                if (i5 < i) {
                    java.lang.System.arraycopy(stackTraceElementArr, i3, stackTraceElementArr2, i4, intValue);
                    i4 += intValue;
                    i5++;
                }
                i2 = (intValue - 1) + i3;
            }
            hashMap.put(stackTraceElement, java.lang.Integer.valueOf(i3));
            i3 = i2 + 1;
        }
        java.lang.StackTraceElement[] stackTraceElementArr3 = new java.lang.StackTraceElement[i4];
        java.lang.System.arraycopy(stackTraceElementArr2, 0, stackTraceElementArr3, 0, stackTraceElementArr3.length);
        return stackTraceElementArr3;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m16076a(java.lang.StackTraceElement[] stackTraceElementArr, int i, int i2) {
        int i3 = i2 - i;
        if (i2 + i3 > stackTraceElementArr.length) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!stackTraceElementArr[i + i4].equals(stackTraceElementArr[i2 + i4])) {
                return false;
            }
        }
        return true;
    }
}
