package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kj0 {
    @DexIgnore
    public /* final */ Account a;
    @DexIgnore
    public /* final */ Set<Scope> b;
    @DexIgnore
    public /* final */ Set<Scope> c;
    @DexIgnore
    public /* final */ Map<de0<?>, b> d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ vm1 g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public Integer i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public Account a;
        @DexIgnore
        public h4<Scope> b;
        @DexIgnore
        public Map<de0<?>, b> c;
        @DexIgnore
        public int d; // = 0;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public vm1 h; // = vm1.m;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public final a a(Account account) {
            this.a = account;
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            this.f = str;
            return this;
        }

        @DexIgnore
        public final a a(Collection<Scope> collection) {
            if (this.b == null) {
                this.b = new h4<>();
            }
            this.b.addAll(collection);
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public final kj0 a() {
            return new kj0(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Set<Scope> a;

        @DexIgnore
        public b(Set<Scope> set) {
            bk0.a(set);
            this.a = Collections.unmodifiableSet(set);
        }
    }

    @DexIgnore
    public kj0(Account account, Set<Scope> set, Map<de0<?>, b> map, int i2, View view, String str, String str2, vm1 vm1, boolean z) {
        this.a = account;
        this.b = set == null ? Collections.EMPTY_SET : Collections.unmodifiableSet(set);
        this.d = map == null ? Collections.EMPTY_MAP : map;
        this.e = str;
        this.f = str2;
        this.g = vm1;
        this.h = z;
        HashSet hashSet = new HashSet(this.b);
        for (b bVar : this.d.values()) {
            hashSet.addAll(bVar.a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public final Account a() {
        return this.a;
    }

    @DexIgnore
    @Deprecated
    public final String b() {
        Account account = this.a;
        if (account != null) {
            return account.name;
        }
        return null;
    }

    @DexIgnore
    public final Account c() {
        Account account = this.a;
        if (account != null) {
            return account;
        }
        return new Account("<<default account>>", "com.google");
    }

    @DexIgnore
    public final Set<Scope> d() {
        return this.c;
    }

    @DexIgnore
    public final Integer e() {
        return this.i;
    }

    @DexIgnore
    public final Map<de0<?>, b> f() {
        return this.d;
    }

    @DexIgnore
    public final String g() {
        return this.f;
    }

    @DexIgnore
    public final String h() {
        return this.e;
    }

    @DexIgnore
    public final Set<Scope> i() {
        return this.b;
    }

    @DexIgnore
    public final vm1 j() {
        return this.g;
    }

    @DexIgnore
    public final boolean k() {
        return this.h;
    }

    @DexIgnore
    public final void a(Integer num) {
        this.i = num;
    }

    @DexIgnore
    public final Set<Scope> a(de0<?> de0) {
        b bVar = this.d.get(de0);
        if (bVar == null || bVar.a.isEmpty()) {
            return this.b;
        }
        HashSet hashSet = new HashSet(this.b);
        hashSet.addAll(bVar.a);
        return hashSet;
    }
}
