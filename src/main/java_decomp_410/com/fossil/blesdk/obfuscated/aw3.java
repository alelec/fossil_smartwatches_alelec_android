package com.fossil.blesdk.obfuscated;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.yv3;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.squareup.okhttp.Protocol;
import com.squareup.okhttp.internal.framed.ErrorCode;
import com.squareup.okhttp.internal.framed.HeadersMode;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class aw3 implements Closeable {
    @DexIgnore
    public static /* final */ ExecutorService A; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), wv3.a("OkHttp FramedConnection", true));
    @DexIgnore
    public /* final */ Protocol e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ gw3 g;
    @DexIgnore
    public /* final */ Map<Integer, bw3> h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public long m;
    @DexIgnore
    public /* final */ ExecutorService n;
    @DexIgnore
    public Map<Integer, iw3> o;
    @DexIgnore
    public /* final */ jw3 p;
    @DexIgnore
    public long q;
    @DexIgnore
    public long r;
    @DexIgnore
    public /* final */ kw3 s;
    @DexIgnore
    public /* final */ kw3 t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public /* final */ mw3 v;
    @DexIgnore
    public /* final */ Socket w;
    @DexIgnore
    public /* final */ zv3 x;
    @DexIgnore
    public /* final */ i y;
    @DexIgnore
    public /* final */ Set<Integer> z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            try {
                aw3.this.c(this.f, this.g);
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, Object[] objArr, int i, long j) {
            super(str, objArr);
            this.f = i;
            this.g = j;
        }

        @DexIgnore
        public void b() {
            try {
                aw3.this.x.a(this.f, this.g);
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ boolean f;
        @DexIgnore
        public /* final */ /* synthetic */ int g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;
        @DexIgnore
        public /* final */ /* synthetic */ iw3 i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str, Object[] objArr, boolean z, int i2, int i3, iw3 iw3) {
            super(str, objArr);
            this.f = z;
            this.g = i2;
            this.h = i3;
            this.i = iw3;
        }

        @DexIgnore
        public void b() {
            try {
                aw3.this.a(this.f, this.g, this.h, this.i);
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(String str, Object[] objArr, int i, List list) {
            super(str, objArr);
            this.f = i;
            this.g = list;
        }

        @DexIgnore
        public void b() {
            if (aw3.this.p.a(this.f, (List<cw3>) this.g)) {
                try {
                    aw3.this.x.a(this.f, ErrorCode.CANCEL);
                    synchronized (aw3.this) {
                        aw3.this.z.remove(Integer.valueOf(this.f));
                    }
                } catch (IOException unused) {
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ List g;
        @DexIgnore
        public /* final */ /* synthetic */ boolean h;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, Object[] objArr, int i2, List list, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = list;
            this.h = z;
        }

        @DexIgnore
        public void b() {
            boolean a = aw3.this.p.a(this.f, this.g, this.h);
            if (a) {
                try {
                    aw3.this.x.a(this.f, ErrorCode.CANCEL);
                } catch (IOException unused) {
                    return;
                }
            }
            if (a || this.h) {
                synchronized (aw3.this) {
                    aw3.this.z.remove(Integer.valueOf(this.f));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ jo4 g;
        @DexIgnore
        public /* final */ /* synthetic */ int h;
        @DexIgnore
        public /* final */ /* synthetic */ boolean i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str, Object[] objArr, int i2, jo4 jo4, int i3, boolean z) {
            super(str, objArr);
            this.f = i2;
            this.g = jo4;
            this.h = i3;
            this.i = z;
        }

        @DexIgnore
        public void b() {
            try {
                boolean a = aw3.this.p.a(this.f, this.g, this.h, this.i);
                if (a) {
                    aw3.this.x.a(this.f, ErrorCode.CANCEL);
                }
                if (a || this.i) {
                    synchronized (aw3.this) {
                        aw3.this.z.remove(Integer.valueOf(this.f));
                    }
                }
            } catch (IOException unused) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends rv3 {
        @DexIgnore
        public /* final */ /* synthetic */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ ErrorCode g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str, Object[] objArr, int i, ErrorCode errorCode) {
            super(str, objArr);
            this.f = i;
            this.g = errorCode;
        }

        @DexIgnore
        public void b() {
            aw3.this.p.a(this.f, this.g);
            synchronized (aw3.this) {
                aw3.this.z.remove(Integer.valueOf(this.f));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public String a;
        @DexIgnore
        public Socket b;
        @DexIgnore
        public gw3 c; // = gw3.a;
        @DexIgnore
        public Protocol d; // = Protocol.SPDY_3;
        @DexIgnore
        public jw3 e; // = jw3.a;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public h(String str, boolean z, Socket socket) throws IOException {
            this.a = str;
            this.f = z;
            this.b = socket;
        }

        @DexIgnore
        public h a(Protocol protocol) {
            this.d = protocol;
            return this;
        }

        @DexIgnore
        public aw3 a() throws IOException {
            return new aw3(this, (a) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends rv3 implements yv3.a {
        @DexIgnore
        public yv3 f;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends rv3 {
            @DexIgnore
            public /* final */ /* synthetic */ bw3 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(String str, Object[] objArr, bw3 bw3) {
                super(str, objArr);
                this.f = bw3;
            }

            @DexIgnore
            public void b() {
                try {
                    aw3.this.g.a(this.f);
                } catch (IOException e) {
                    Logger logger = pv3.a;
                    Level level = Level.INFO;
                    logger.log(level, "StreamHandler failure for " + aw3.this.i, e);
                    try {
                        this.f.a(ErrorCode.PROTOCOL_ERROR);
                    } catch (IOException unused) {
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends rv3 {
            @DexIgnore
            public /* final */ /* synthetic */ kw3 f;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(String str, Object[] objArr, kw3 kw3) {
                super(str, objArr);
                this.f = kw3;
            }

            @DexIgnore
            public void b() {
                try {
                    aw3.this.x.a(this.f);
                } catch (IOException unused) {
                }
            }
        }

        @DexIgnore
        public /* synthetic */ i(aw3 aw3, a aVar) {
            this();
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(int i, int i2, int i3, boolean z) {
        }

        @DexIgnore
        public void a(boolean z, int i, lo4 lo4, int i2) throws IOException {
            if (aw3.this.c(i)) {
                aw3.this.a(i, lo4, i2, z);
                return;
            }
            bw3 b2 = aw3.this.b(i);
            if (b2 == null) {
                aw3.this.d(i, ErrorCode.INVALID_STREAM);
                lo4.skip((long) i2);
                return;
            }
            b2.a(lo4, i2);
            if (z) {
                b2.j();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            r1 = com.squareup.okhttp.internal.framed.ErrorCode.PROTOCOL_ERROR;
            r0 = com.squareup.okhttp.internal.framed.ErrorCode.PROTOCOL_ERROR;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r2 = r5.g;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x004a, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x004b, code lost:
            r4 = r2;
            r2 = r1;
            r1 = r4;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x003b */
        public void b() {
            aw3 aw3;
            ErrorCode errorCode;
            ErrorCode errorCode2 = ErrorCode.INTERNAL_ERROR;
            try {
                this.f = aw3.this.v.a(so4.a(so4.b(aw3.this.w)), aw3.this.f);
                if (!aw3.this.f) {
                    this.f.q();
                }
                while (this.f.a(this)) {
                }
                errorCode = ErrorCode.NO_ERROR;
                errorCode2 = ErrorCode.CANCEL;
                try {
                    aw3 = aw3.this;
                } catch (IOException unused) {
                }
            } catch (IOException unused2) {
                errorCode = errorCode2;
            } catch (Throwable th) {
                Throwable th2 = th;
                ErrorCode errorCode3 = errorCode2;
                try {
                    aw3.this.a(errorCode3, errorCode2);
                } catch (IOException unused3) {
                }
                wv3.a((Closeable) this.f);
                throw th2;
            }
            aw3.a(errorCode, errorCode2);
            wv3.a((Closeable) this.f);
        }

        @DexIgnore
        public i() {
            super("OkHttp %s", aw3.this.i);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x008f, code lost:
            if (r14.failIfStreamPresent() == false) goto L_0x009c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0091, code lost:
            r0.c(com.squareup.okhttp.internal.framed.ErrorCode.PROTOCOL_ERROR);
            r8.g.e(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x009b, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x009c, code lost:
            r0.a(r13, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x009f, code lost:
            if (r10 == false) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a1, code lost:
            r0.j();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
            return;
         */
        @DexIgnore
        public void a(boolean z, boolean z2, int i, int i2, List<cw3> list, HeadersMode headersMode) {
            if (aw3.this.c(i)) {
                aw3.this.a(i, list, z2);
                return;
            }
            synchronized (aw3.this) {
                if (!aw3.this.l) {
                    bw3 b2 = aw3.this.b(i);
                    if (b2 == null) {
                        if (headersMode.failIfStreamAbsent()) {
                            aw3.this.d(i, ErrorCode.INVALID_STREAM);
                        } else if (i > aw3.this.j) {
                            if (i % 2 != aw3.this.k % 2) {
                                bw3 bw3 = new bw3(i, aw3.this, z, z2, list);
                                int unused = aw3.this.j = i;
                                aw3.this.h.put(Integer.valueOf(i), bw3);
                                aw3.A.execute(new a("OkHttp %s stream %d", new Object[]{aw3.this.i, Integer.valueOf(i)}, bw3));
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode) {
            if (aw3.this.c(i)) {
                aw3.this.b(i, errorCode);
                return;
            }
            bw3 e = aw3.this.e(i);
            if (e != null) {
                e.d(errorCode);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v14, types: [java.lang.Object[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        public void a(boolean z, kw3 kw3) {
            bw3[] bw3Arr;
            long j;
            synchronized (aw3.this) {
                int c = aw3.this.t.c(65536);
                if (z) {
                    aw3.this.t.a();
                }
                aw3.this.t.a(kw3);
                if (aw3.this.z() == Protocol.HTTP_2) {
                    a(kw3);
                }
                int c2 = aw3.this.t.c(65536);
                bw3Arr = null;
                if (c2 == -1 || c2 == c) {
                    j = 0;
                } else {
                    j = (long) (c2 - c);
                    if (!aw3.this.u) {
                        aw3.this.h(j);
                        boolean unused = aw3.this.u = true;
                    }
                    if (!aw3.this.h.isEmpty()) {
                        bw3Arr = aw3.this.h.values().toArray(new bw3[aw3.this.h.size()]);
                    }
                }
            }
            if (bw3Arr != null && j != 0) {
                for (bw3 bw3 : bw3Arr) {
                    synchronized (bw3) {
                        bw3.a(j);
                    }
                }
            }
        }

        @DexIgnore
        public final void a(kw3 kw3) {
            aw3.A.execute(new b("OkHttp %s ACK Settings", new Object[]{aw3.this.i}, kw3));
        }

        @DexIgnore
        public void a(boolean z, int i, int i2) {
            if (z) {
                iw3 c = aw3.this.d(i);
                if (c != null) {
                    c.b();
                    return;
                }
                return;
            }
            aw3.this.b(true, i, i2, (iw3) null);
        }

        @DexIgnore
        public void a(int i, ErrorCode errorCode, ByteString byteString) {
            bw3[] bw3Arr;
            byteString.size();
            synchronized (aw3.this) {
                bw3Arr = (bw3[]) aw3.this.h.values().toArray(new bw3[aw3.this.h.size()]);
                boolean unused = aw3.this.l = true;
            }
            for (bw3 bw3 : bw3Arr) {
                if (bw3.c() > i && bw3.g()) {
                    bw3.d(ErrorCode.REFUSED_STREAM);
                    aw3.this.e(bw3.c());
                }
            }
        }

        @DexIgnore
        public void a(int i, long j) {
            if (i == 0) {
                synchronized (aw3.this) {
                    aw3.this.r += j;
                    aw3.this.notifyAll();
                }
                return;
            }
            bw3 b2 = aw3.this.b(i);
            if (b2 != null) {
                synchronized (b2) {
                    b2.a(j);
                }
            }
        }

        @DexIgnore
        public void a(int i, int i2, List<cw3> list) {
            aw3.this.a(i2, list);
        }
    }

    /*
    static {
        Class<aw3> cls = aw3.class;
    }
    */

    @DexIgnore
    public /* synthetic */ aw3(h hVar, a aVar) throws IOException {
        this(hVar);
    }

    @DexIgnore
    public synchronized boolean A() {
        return this.m != ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
    }

    @DexIgnore
    public void B() throws IOException {
        this.x.p();
        this.x.b(this.s);
        int c2 = this.s.c(65536);
        if (c2 != 65536) {
            this.x.a(0, (long) (c2 - 65536));
        }
    }

    @DexIgnore
    public void close() throws IOException {
        a(ErrorCode.NO_ERROR, ErrorCode.CANCEL);
    }

    @DexIgnore
    public void flush() throws IOException {
        this.x.flush();
    }

    @DexIgnore
    public synchronized long y() {
        return this.m;
    }

    @DexIgnore
    public Protocol z() {
        return this.e;
    }

    @DexIgnore
    public aw3(h hVar) throws IOException {
        this.h = new HashMap();
        this.m = System.nanoTime();
        this.q = 0;
        this.s = new kw3();
        this.t = new kw3();
        this.u = false;
        this.z = new LinkedHashSet();
        this.e = hVar.d;
        this.p = hVar.e;
        this.f = hVar.f;
        this.g = hVar.c;
        this.k = hVar.f ? 1 : 2;
        if (hVar.f && this.e == Protocol.HTTP_2) {
            this.k += 2;
        }
        boolean c2 = hVar.f;
        if (hVar.f) {
            this.s.a(7, 0, 16777216);
        }
        this.i = hVar.a;
        Protocol protocol = this.e;
        if (protocol == Protocol.HTTP_2) {
            this.v = new ew3();
            this.n = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), wv3.a(String.format("OkHttp %s Push Observer", new Object[]{this.i}), true));
            this.t.a(7, 0, 65535);
            this.t.a(5, 0, RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        } else if (protocol == Protocol.SPDY_3) {
            this.v = new lw3();
            this.n = null;
        } else {
            throw new AssertionError(protocol);
        }
        this.r = (long) this.t.c(65536);
        this.w = hVar.b;
        this.x = this.v.a(so4.a(so4.a(hVar.b)), this.f);
        this.y = new i(this, (a) null);
        new Thread(this.y).start();
    }

    @DexIgnore
    public void d(int i2, ErrorCode errorCode) {
        A.submit(new a("OkHttp %s stream %d", new Object[]{this.i, Integer.valueOf(i2)}, i2, errorCode));
    }

    @DexIgnore
    public synchronized bw3 e(int i2) {
        bw3 remove;
        remove = this.h.remove(Integer.valueOf(i2));
        if (remove != null && this.h.isEmpty()) {
            a(true);
        }
        notifyAll();
        return remove;
    }

    @DexIgnore
    public void h(long j2) {
        this.r += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    @DexIgnore
    public void c(int i2, ErrorCode errorCode) throws IOException {
        this.x.a(i2, errorCode);
    }

    @DexIgnore
    public final synchronized iw3 d(int i2) {
        return this.o != null ? this.o.remove(Integer.valueOf(i2)) : null;
    }

    @DexIgnore
    public void c(int i2, long j2) {
        A.execute(new b("OkHttp Window Update %s stream %d", new Object[]{this.i, Integer.valueOf(i2)}, i2, j2));
    }

    @DexIgnore
    public synchronized bw3 b(int i2) {
        return this.h.get(Integer.valueOf(i2));
    }

    @DexIgnore
    public final boolean c(int i2) {
        return this.e == Protocol.HTTP_2 && i2 != 0 && (i2 & 1) == 0;
    }

    @DexIgnore
    public final void b(boolean z2, int i2, int i3, iw3 iw3) {
        A.execute(new c("OkHttp %s ping %08x%08x", new Object[]{this.i, Integer.valueOf(i2), Integer.valueOf(i3)}, z2, i2, i3, iw3));
    }

    @DexIgnore
    public final void b(int i2, ErrorCode errorCode) {
        this.n.execute(new g("OkHttp %s Push Reset[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, errorCode));
    }

    @DexIgnore
    public final synchronized void a(boolean z2) {
        long j2;
        if (z2) {
            try {
                j2 = System.nanoTime();
            } catch (Throwable th) {
                throw th;
            }
        } else {
            j2 = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        this.m = j2;
    }

    @DexIgnore
    public bw3 a(List<cw3> list, boolean z2, boolean z3) throws IOException {
        return a(0, list, z2, z3);
    }

    @DexIgnore
    public final bw3 a(int i2, List<cw3> list, boolean z2, boolean z3) throws IOException {
        int i3;
        bw3 bw3;
        boolean z4 = !z2;
        boolean z5 = !z3;
        synchronized (this.x) {
            synchronized (this) {
                if (!this.l) {
                    i3 = this.k;
                    this.k += 2;
                    bw3 = new bw3(i3, this, z4, z5, list);
                    if (bw3.h()) {
                        this.h.put(Integer.valueOf(i3), bw3);
                        a(false);
                    }
                } else {
                    throw new IOException("shutdown");
                }
            }
            if (i2 == 0) {
                this.x.a(z4, z5, i3, i2, list);
            } else if (!this.f) {
                this.x.a(i2, i3, list);
            } else {
                throw new IllegalArgumentException("client streams shouldn't have associated stream IDs");
            }
        }
        if (!z2) {
            this.x.flush();
        }
        return bw3;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = java.lang.Math.min((int) java.lang.Math.min(r12, r8.r), r8.x.r());
        r6 = (long) r3;
        r8.r -= r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        throw new java.io.InterruptedIOException();
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x005a */
    public void a(int i2, boolean z2, jo4 jo4, long j2) throws IOException {
        int min;
        long j3;
        if (j2 == 0) {
            this.x.a(z2, i2, jo4, 0);
            return;
        }
        while (j2 > 0) {
            synchronized (this) {
                while (true) {
                    if (this.r > 0) {
                        break;
                    } else if (this.h.containsKey(Integer.valueOf(i2))) {
                        wait();
                    } else {
                        throw new IOException("stream closed");
                    }
                }
            }
            j2 -= j3;
            this.x.a(z2 && j2 == 0, i2, jo4, min);
        }
    }

    @DexIgnore
    public final void a(boolean z2, int i2, int i3, iw3 iw3) throws IOException {
        synchronized (this.x) {
            if (iw3 != null) {
                iw3.c();
            }
            this.x.a(z2, i2, i3);
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode) throws IOException {
        synchronized (this.x) {
            synchronized (this) {
                if (!this.l) {
                    this.l = true;
                    int i2 = this.j;
                    this.x.a(i2, errorCode, wv3.a);
                }
            }
        }
    }

    @DexIgnore
    public final void a(ErrorCode errorCode, ErrorCode errorCode2) throws IOException {
        int i2;
        bw3[] bw3Arr;
        iw3[] iw3Arr = null;
        try {
            a(errorCode);
            e = null;
        } catch (IOException e2) {
            e = e2;
        }
        synchronized (this) {
            if (!this.h.isEmpty()) {
                bw3Arr = (bw3[]) this.h.values().toArray(new bw3[this.h.size()]);
                this.h.clear();
                a(false);
            } else {
                bw3Arr = null;
            }
            if (this.o != null) {
                this.o = null;
                iw3Arr = (iw3[]) this.o.values().toArray(new iw3[this.o.size()]);
            }
        }
        if (bw3Arr != null) {
            IOException iOException = e;
            for (bw3 a2 : bw3Arr) {
                try {
                    a2.a(errorCode2);
                } catch (IOException e3) {
                    if (iOException != null) {
                        iOException = e3;
                    }
                }
            }
            e = iOException;
        }
        if (iw3Arr != null) {
            for (iw3 a3 : iw3Arr) {
                a3.a();
            }
        }
        try {
            this.x.close();
        } catch (IOException e4) {
            if (e == null) {
                e = e4;
            }
        }
        try {
            this.w.close();
        } catch (IOException e5) {
            e = e5;
        }
        if (e != null) {
            throw e;
        }
    }

    @DexIgnore
    public final void a(int i2, List<cw3> list) {
        synchronized (this) {
            if (this.z.contains(Integer.valueOf(i2))) {
                d(i2, ErrorCode.PROTOCOL_ERROR);
                return;
            }
            this.z.add(Integer.valueOf(i2));
            this.n.execute(new d("OkHttp %s Push Request[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, list));
        }
    }

    @DexIgnore
    public final void a(int i2, List<cw3> list, boolean z2) {
        this.n.execute(new e("OkHttp %s Push Headers[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, list, z2));
    }

    @DexIgnore
    public final void a(int i2, lo4 lo4, int i3, boolean z2) throws IOException {
        jo4 jo4 = new jo4();
        long j2 = (long) i3;
        lo4.g(j2);
        lo4.b(jo4, j2);
        if (jo4.B() == j2) {
            this.n.execute(new f("OkHttp %s Push Data[%s]", new Object[]{this.i, Integer.valueOf(i2)}, i2, jo4, i3, z2));
            return;
        }
        throw new IOException(jo4.B() + " != " + i3);
    }
}
