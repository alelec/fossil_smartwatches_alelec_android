package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@android.annotation.SuppressLint({"InlinedApi"})
/* renamed from: com.fossil.blesdk.obfuscated.kh */
public class C2222kh {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ int[] f6854a; // = {16843073, 16843160, 16843746, 16843855};

    @DexIgnore
    /* renamed from: b */
    public static /* final */ int[] f6855b; // = {16843983};

    @DexIgnore
    /* renamed from: c */
    public static /* final */ int[] f6856c; // = {16843900};

    @DexIgnore
    /* renamed from: d */
    public static /* final */ int[] f6857d; // = {16843745};

    @DexIgnore
    /* renamed from: e */
    public static /* final */ int[] f6858e; // = {16843964, 16843965};

    @DexIgnore
    /* renamed from: f */
    public static /* final */ int[] f6859f; // = {16843824};

    @DexIgnore
    /* renamed from: g */
    public static /* final */ int[] f6860g; // = {16843744};

    @DexIgnore
    /* renamed from: h */
    public static /* final */ int[] f6861h; // = {16843901, 16843902, 16843903};

    @DexIgnore
    /* renamed from: i */
    public static /* final */ int[] f6862i; // = {16843978};
}
