package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.bm4;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mr4<T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mr4<Iterable<T>> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(or4 or4, Iterable<T> iterable) throws IOException {
            if (iterable != null) {
                for (T a2 : iterable) {
                    mr4.this.a(or4, a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends mr4<Object> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(or4 or4, Object obj) throws IOException {
            if (obj != null) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    mr4.this.a(or4, Array.get(obj, i));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> extends mr4<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ gr4<T, RequestBody> c;

        @DexIgnore
        public c(Method method, int i, gr4<T, RequestBody> gr4) {
            this.a = method;
            this.b = i;
            this.c = gr4;
        }

        @DexIgnore
        public void a(or4 or4, T t) {
            if (t != null) {
                try {
                    or4.a(this.c.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw ur4.a(method, e, i, "Unable to convert " + t + " to RequestBody", new Object[0]);
                }
            } else {
                throw ur4.a(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> extends mr4<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ gr4<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public d(String str, gr4<T, String> gr4, boolean z) {
            ur4.a(str, "name == null");
            this.a = str;
            this.b = gr4;
            this.c = z;
        }

        @DexIgnore
        public void a(or4 or4, T t) throws IOException {
            if (t != null) {
                String a2 = this.b.a(t);
                if (a2 != null) {
                    or4.a(this.a, a2, this.c);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> extends mr4<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ gr4<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public e(Method method, int i, gr4<T, String> gr4, boolean z) {
            this.a = method;
            this.b = i;
            this.c = gr4;
            this.d = z;
        }

        @DexIgnore
        public void a(or4 or4, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                or4.a(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw ur4.a(method, i, "Field map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw ur4.a(method2, i2, "Field map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw ur4.a(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw ur4.a(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> extends mr4<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ gr4<T, String> b;

        @DexIgnore
        public f(String str, gr4<T, String> gr4) {
            ur4.a(str, "name == null");
            this.a = str;
            this.b = gr4;
        }

        @DexIgnore
        public void a(or4 or4, T t) throws IOException {
            if (t != null) {
                String a2 = this.b.a(t);
                if (a2 != null) {
                    or4.a(this.a, a2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> extends mr4<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ yl4 c;
        @DexIgnore
        public /* final */ gr4<T, RequestBody> d;

        @DexIgnore
        public g(Method method, int i, yl4 yl4, gr4<T, RequestBody> gr4) {
            this.a = method;
            this.b = i;
            this.c = yl4;
            this.d = gr4;
        }

        @DexIgnore
        public void a(or4 or4, T t) {
            if (t != null) {
                try {
                    or4.a(this.c, this.d.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw ur4.a(method, i, "Unable to convert " + t + " to RequestBody", e);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> extends mr4<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ gr4<T, RequestBody> c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public h(Method method, int i, gr4<T, RequestBody> gr4, String str) {
            this.a = method;
            this.b = i;
            this.c = gr4;
            this.d = str;
        }

        @DexIgnore
        public void a(or4 or4, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            or4.a(yl4.a("Content-Disposition", "form-data; name=\"" + str + "\"", "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw ur4.a(method, i, "Part map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw ur4.a(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw ur4.a(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> extends mr4<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ gr4<T, String> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public i(Method method, int i, String str, gr4<T, String> gr4, boolean z) {
            this.a = method;
            this.b = i;
            ur4.a(str, "name == null");
            this.c = str;
            this.d = gr4;
            this.e = z;
        }

        @DexIgnore
        public void a(or4 or4, T t) throws IOException {
            if (t != null) {
                or4.b(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.a;
            int i = this.b;
            throw ur4.a(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> extends mr4<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ gr4<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public j(String str, gr4<T, String> gr4, boolean z) {
            ur4.a(str, "name == null");
            this.a = str;
            this.b = gr4;
            this.c = z;
        }

        @DexIgnore
        public void a(or4 or4, T t) throws IOException {
            if (t != null) {
                String a2 = this.b.a(t);
                if (a2 != null) {
                    or4.c(this.a, a2, this.c);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> extends mr4<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ gr4<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public k(Method method, int i, gr4<T, String> gr4, boolean z) {
            this.a = method;
            this.b = i;
            this.c = gr4;
            this.d = z;
        }

        @DexIgnore
        public void a(or4 or4, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry next : map.entrySet()) {
                    String str = (String) next.getKey();
                    if (str != null) {
                        Object value = next.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                or4.c(str, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw ur4.a(method, i, "Query map value '" + value + "' converted to null by " + this.c.getClass().getName() + " for key '" + str + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw ur4.a(method2, i2, "Query map contained null value for key '" + str + "'.", new Object[0]);
                        }
                    } else {
                        throw ur4.a(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw ur4.a(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> extends mr4<T> {
        @DexIgnore
        public /* final */ gr4<T, String> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public l(gr4<T, String> gr4, boolean z) {
            this.a = gr4;
            this.b = z;
        }

        @DexIgnore
        public void a(or4 or4, T t) throws IOException {
            if (t != null) {
                or4.c(this.a.a(t), (String) null, this.b);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends mr4<bm4.b> {
        @DexIgnore
        public static /* final */ m a; // = new m();

        @DexIgnore
        public void a(or4 or4, bm4.b bVar) {
            if (bVar != null) {
                or4.a(bVar);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends mr4<Object> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public n(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        public void a(or4 or4, Object obj) {
            if (obj != null) {
                or4.a(obj);
                return;
            }
            throw ur4.a(this.a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    @DexIgnore
    public final mr4<Object> a() {
        return new b();
    }

    @DexIgnore
    public abstract void a(or4 or4, T t) throws IOException;

    @DexIgnore
    public final mr4<Iterable<T>> b() {
        return new a();
    }
}
