package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.h1;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l0 extends ActionBar implements ActionBarOverlayLayout.d {
    @DexIgnore
    public static /* final */ Interpolator B; // = new AccelerateInterpolator();
    @DexIgnore
    public static /* final */ Interpolator C; // = new DecelerateInterpolator();
    @DexIgnore
    public /* final */ m9 A; // = new c();
    @DexIgnore
    public Context a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public ActionBarOverlayLayout c;
    @DexIgnore
    public ActionBarContainer d;
    @DexIgnore
    public j2 e;
    @DexIgnore
    public ActionBarContextView f;
    @DexIgnore
    public View g;
    @DexIgnore
    public s2 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public d j;
    @DexIgnore
    public ActionMode k;
    @DexIgnore
    public ActionMode.Callback l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public ArrayList<ActionBar.a> n; // = new ArrayList<>();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public int p; // = 0;
    @DexIgnore
    public boolean q; // = true;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u; // = true;
    @DexIgnore
    public y0 v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public /* final */ k9 y; // = new a();
    @DexIgnore
    public /* final */ k9 z; // = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends l9 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void b(View view) {
            l0 l0Var = l0.this;
            if (l0Var.q) {
                View view2 = l0Var.g;
                if (view2 != null) {
                    view2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    l0.this.d.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
            }
            l0.this.d.setVisibility(8);
            l0.this.d.setTransitioning(false);
            l0 l0Var2 = l0.this;
            l0Var2.v = null;
            l0Var2.l();
            ActionBarOverlayLayout actionBarOverlayLayout = l0.this.c;
            if (actionBarOverlayLayout != null) {
                f9.D(actionBarOverlayLayout);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends l9 {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void b(View view) {
            l0 l0Var = l0.this;
            l0Var.v = null;
            l0Var.d.requestLayout();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements m9 {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(View view) {
            ((View) l0.this.d.getParent()).invalidate();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ActionMode implements h1.a {
        @DexIgnore
        public /* final */ Context g;
        @DexIgnore
        public /* final */ h1 h;
        @DexIgnore
        public ActionMode.Callback i;
        @DexIgnore
        public WeakReference<View> j;

        @DexIgnore
        public d(Context context, ActionMode.Callback callback) {
            this.g = context;
            this.i = callback;
            h1 h1Var = new h1(context);
            h1Var.c(1);
            this.h = h1Var;
            this.h.a((h1.a) this);
        }

        @DexIgnore
        public void a() {
            l0 l0Var = l0.this;
            if (l0Var.j == this) {
                if (!l0.a(l0Var.r, l0Var.s, false)) {
                    l0 l0Var2 = l0.this;
                    l0Var2.k = this;
                    l0Var2.l = this.i;
                } else {
                    this.i.a(this);
                }
                this.i = null;
                l0.this.f(false);
                l0.this.f.a();
                l0.this.e.k().sendAccessibilityEvent(32);
                l0 l0Var3 = l0.this;
                l0Var3.c.setHideOnContentScrollEnabled(l0Var3.x);
                l0.this.j = null;
            }
        }

        @DexIgnore
        public void b(CharSequence charSequence) {
            l0.this.f.setTitle(charSequence);
        }

        @DexIgnore
        public Menu c() {
            return this.h;
        }

        @DexIgnore
        public MenuInflater d() {
            return new x0(this.g);
        }

        @DexIgnore
        public CharSequence e() {
            return l0.this.f.getSubtitle();
        }

        @DexIgnore
        public CharSequence g() {
            return l0.this.f.getTitle();
        }

        @DexIgnore
        public void i() {
            if (l0.this.j == this) {
                this.h.s();
                try {
                    this.i.b(this, this.h);
                } finally {
                    this.h.r();
                }
            }
        }

        @DexIgnore
        public boolean j() {
            return l0.this.f.c();
        }

        @DexIgnore
        public boolean k() {
            this.h.s();
            try {
                return this.i.a((ActionMode) this, (Menu) this.h);
            } finally {
                this.h.r();
            }
        }

        @DexIgnore
        public void b(int i2) {
            b((CharSequence) l0.this.a.getResources().getString(i2));
        }

        @DexIgnore
        public View b() {
            WeakReference<View> weakReference = this.j;
            if (weakReference != null) {
                return (View) weakReference.get();
            }
            return null;
        }

        @DexIgnore
        public void a(View view) {
            l0.this.f.setCustomView(view);
            this.j = new WeakReference<>(view);
        }

        @DexIgnore
        public void a(CharSequence charSequence) {
            l0.this.f.setSubtitle(charSequence);
        }

        @DexIgnore
        public void a(int i2) {
            a((CharSequence) l0.this.a.getResources().getString(i2));
        }

        @DexIgnore
        public void a(boolean z) {
            super.a(z);
            l0.this.f.setTitleOptional(z);
        }

        @DexIgnore
        public boolean a(h1 h1Var, MenuItem menuItem) {
            ActionMode.Callback callback = this.i;
            if (callback != null) {
                return callback.a((ActionMode) this, menuItem);
            }
            return false;
        }

        @DexIgnore
        public void a(h1 h1Var) {
            if (this.i != null) {
                i();
                l0.this.f.e();
            }
        }
    }

    @DexIgnore
    public l0(Activity activity, boolean z2) {
        new ArrayList();
        View decorView = activity.getWindow().getDecorView();
        b(decorView);
        if (!z2) {
            this.g = decorView.findViewById(16908290);
        }
    }

    @DexIgnore
    public static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    @DexIgnore
    public final j2 a(View view) {
        if (view instanceof j2) {
            return (j2) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public final void b(View view) {
        this.c = (ActionBarOverlayLayout) view.findViewById(w.decor_content_parent);
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.e = a(view.findViewById(w.action_bar));
        this.f = (ActionBarContextView) view.findViewById(w.action_context_bar);
        this.d = (ActionBarContainer) view.findViewById(w.action_bar_container);
        j2 j2Var = this.e;
        if (j2Var == null || this.f == null || this.d == null) {
            throw new IllegalStateException(l0.class.getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.a = j2Var.getContext();
        boolean z2 = (this.e.l() & 4) != 0;
        if (z2) {
            this.i = true;
        }
        s0 a2 = s0.a(this.a);
        k(a2.a() || z2);
        i(a2.f());
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes((AttributeSet) null, a0.ActionBar, r.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(a0.ActionBar_hideOnContentScroll, false)) {
            j(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(a0.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public void c() {
        if (!this.s) {
            this.s = true;
            l(true);
        }
    }

    @DexIgnore
    public void d(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    @DexIgnore
    public void e(boolean z2) {
        this.w = z2;
        if (!z2) {
            y0 y0Var = this.v;
            if (y0Var != null) {
                y0Var.a();
            }
        }
    }

    @DexIgnore
    public void f(boolean z2) {
        j9 j9Var;
        j9 j9Var2;
        if (z2) {
            p();
        } else {
            n();
        }
        if (o()) {
            if (z2) {
                j9Var = this.e.a(4, 100);
                j9Var2 = this.f.a(0, 200);
            } else {
                j9Var2 = this.e.a(0, 200);
                j9Var = this.f.a(8, 100);
            }
            y0 y0Var = new y0();
            y0Var.a(j9Var, j9Var2);
            y0Var.c();
        } else if (z2) {
            this.e.setVisibility(4);
            this.f.setVisibility(0);
        } else {
            this.e.setVisibility(0);
            this.f.setVisibility(8);
        }
    }

    @DexIgnore
    public int g() {
        return this.e.l();
    }

    @DexIgnore
    public void h(boolean z2) {
        y0 y0Var = this.v;
        if (y0Var != null) {
            y0Var.a();
        }
        this.d.setVisibility(0);
        if (this.p != 0 || (!this.w && !z2)) {
            this.d.setAlpha(1.0f);
            this.d.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            if (this.q) {
                View view = this.g;
                if (view != null) {
                    view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
            }
            this.z.b((View) null);
        } else {
            this.d.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f2 = (float) (-this.d.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.d.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.d.setTranslationY(f2);
            y0 y0Var2 = new y0();
            j9 a2 = f9.a(this.d);
            a2.b((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a2.a(this.A);
            y0Var2.a(a2);
            if (this.q) {
                View view2 = this.g;
                if (view2 != null) {
                    view2.setTranslationY(f2);
                    j9 a3 = f9.a(this.g);
                    a3.b((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    y0Var2.a(a3);
                }
            }
            y0Var2.a(C);
            y0Var2.a(250);
            y0Var2.a(this.z);
            this.v = y0Var2;
            y0Var2.c();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.c;
        if (actionBarOverlayLayout != null) {
            f9.D(actionBarOverlayLayout);
        }
    }

    @DexIgnore
    public final void i(boolean z2) {
        this.o = z2;
        if (!this.o) {
            this.e.a((s2) null);
            this.d.setTabContainer(this.h);
        } else {
            this.d.setTabContainer((s2) null);
            this.e.a(this.h);
        }
        boolean z3 = true;
        boolean z4 = m() == 2;
        s2 s2Var = this.h;
        if (s2Var != null) {
            if (z4) {
                s2Var.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.c;
                if (actionBarOverlayLayout != null) {
                    f9.D(actionBarOverlayLayout);
                }
            } else {
                s2Var.setVisibility(8);
            }
        }
        this.e.b(!this.o && z4);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.c;
        if (this.o || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z3);
    }

    @DexIgnore
    public void j(boolean z2) {
        if (!z2 || this.c.j()) {
            this.x = z2;
            this.c.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    @DexIgnore
    public void k(boolean z2) {
        this.e.a(z2);
    }

    @DexIgnore
    public void l() {
        ActionMode.Callback callback = this.l;
        if (callback != null) {
            callback.a(this.k);
            this.k = null;
            this.l = null;
        }
    }

    @DexIgnore
    public int m() {
        return this.e.j();
    }

    @DexIgnore
    public final void n() {
        if (this.t) {
            this.t = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    @DexIgnore
    public final boolean o() {
        return f9.z(this.d);
    }

    @DexIgnore
    public final void p() {
        if (!this.t) {
            this.t = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.c;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    @DexIgnore
    public void d() {
        y0 y0Var = this.v;
        if (y0Var != null) {
            y0Var.a();
            this.v = null;
        }
    }

    @DexIgnore
    public void g(boolean z2) {
        y0 y0Var = this.v;
        if (y0Var != null) {
            y0Var.a();
        }
        if (this.p != 0 || (!this.w && !z2)) {
            this.y.b((View) null);
            return;
        }
        this.d.setAlpha(1.0f);
        this.d.setTransitioning(true);
        y0 y0Var2 = new y0();
        float f2 = (float) (-this.d.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.d.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        j9 a2 = f9.a(this.d);
        a2.b(f2);
        a2.a(this.A);
        y0Var2.a(a2);
        if (this.q) {
            View view = this.g;
            if (view != null) {
                j9 a3 = f9.a(view);
                a3.b(f2);
                y0Var2.a(a3);
            }
        }
        y0Var2.a(B);
        y0Var2.a(250);
        y0Var2.a(this.y);
        this.v = y0Var2;
        y0Var2.c();
    }

    @DexIgnore
    public void c(boolean z2) {
        if (!this.i) {
            d(z2);
        }
    }

    @DexIgnore
    public final void l(boolean z2) {
        if (a(this.r, this.s, this.t)) {
            if (!this.u) {
                this.u = true;
                h(z2);
            }
        } else if (this.u) {
            this.u = false;
            g(z2);
        }
    }

    @DexIgnore
    public void a(float f2) {
        f9.b((View) this.d, f2);
    }

    @DexIgnore
    public void a(Configuration configuration) {
        i(s0.a(this.a).f());
    }

    @DexIgnore
    public void a(int i2) {
        this.p = i2;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.e.setTitle(charSequence);
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int l2 = this.e.l();
        if ((i3 & 4) != 0) {
            this.i = true;
        }
        this.e.a((i2 & i3) | ((~i3) & l2));
    }

    @DexIgnore
    public l0(Dialog dialog) {
        new ArrayList();
        b(dialog.getWindow().getDecorView());
    }

    @DexIgnore
    public ActionMode a(ActionMode.Callback callback) {
        d dVar = this.j;
        if (dVar != null) {
            dVar.a();
        }
        this.c.setHideOnContentScrollEnabled(false);
        this.f.d();
        d dVar2 = new d(this.f.getContext(), callback);
        if (!dVar2.k()) {
            return null;
        }
        this.j = dVar2;
        dVar2.i();
        this.f.a(dVar2);
        f(true);
        this.f.sendAccessibilityEvent(32);
        return dVar2;
    }

    @DexIgnore
    public boolean f() {
        j2 j2Var = this.e;
        if (j2Var == null || !j2Var.h()) {
            return false;
        }
        this.e.collapseActionView();
        return true;
    }

    @DexIgnore
    public void b(boolean z2) {
        if (z2 != this.m) {
            this.m = z2;
            int size = this.n.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.n.get(i2).a(z2);
            }
        }
    }

    @DexIgnore
    public void a(boolean z2) {
        this.q = z2;
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.e.setWindowTitle(charSequence);
    }

    @DexIgnore
    public void a() {
        if (this.s) {
            this.s = false;
            l(true);
        }
    }

    @DexIgnore
    public boolean a(int i2, KeyEvent keyEvent) {
        d dVar = this.j;
        if (dVar == null) {
            return false;
        }
        Menu c2 = dVar.c();
        if (c2 == null) {
            return false;
        }
        boolean z2 = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z2 = false;
        }
        c2.setQwertyMode(z2);
        return c2.performShortcut(i2, keyEvent, 0);
    }

    @DexIgnore
    public Context h() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.a.getTheme().resolveAttribute(r.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.b = new ContextThemeWrapper(this.a, i2);
            } else {
                this.b = this.a;
            }
        }
        return this.b;
    }
}
