package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.Context;
import androidx.work.ListenableWorker;
import com.fossil.blesdk.obfuscated.m44;
import com.google.common.collect.ImmutableMap;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.CloudImageHelper_MembersInjector;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper_MembersInjector;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.LocationSource_Factory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository_Factory;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.ActivitiesRepository_Factory;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.AlarmsRepository_Factory;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.CategoryRepository_Factory;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository_Factory;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.ComplicationRepository_Factory;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceDatabase;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DeviceRepository_Factory;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaPresetRepository_Factory;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.FitnessDataRepository_Factory;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository_Factory;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository_Factory;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository_Factory;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.MicroAppRepository_Factory;
import com.portfolio.platform.data.source.NotificationsDataSource;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.NotificationsRepositoryModule;
import com.portfolio.platform.data.source.NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory;
import com.portfolio.platform.data.source.NotificationsRepository_Factory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideActivitySampleDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCategoryDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDeviceDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideDianaPresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessDataDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideFitnessHelperFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHeartRateDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidePresetDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSampleRawDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSkuDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSleepDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideSleepDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvideWorkoutDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesAlarmDaoFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory;
import com.portfolio.platform.data.source.PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory;
import com.portfolio.platform.data.source.RepositoriesModule;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideComplicationRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideUserRemoteDataSourceFactory;
import com.portfolio.platform.data.source.RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.data.source.SkuDao;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository_Factory;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository_Factory;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository_Factory;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository_Factory;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule;
import com.portfolio.platform.data.source.UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory;
import com.portfolio.platform.data.source.UserDataSource;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserRepository_Factory;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository_Factory;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchAppRepository_Factory;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository_Factory;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository_Factory;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository_Factory;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository_Factory;
import com.portfolio.platform.data.source.loader.NotificationsLoader;
import com.portfolio.platform.data.source.loader.NotificationsLoader_Factory;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDao;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.ComplicationDao;
import com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.local.diana.WatchAppDao;
import com.portfolio.platform.data.source.local.diana.WatchAppLastSettingDao;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository_Factory;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource_Factory;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.remote.WatchAppRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource_Factory;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LightAndHapticsManager;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.migration.MigrationHelper;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AlarmReceiver;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;
import com.portfolio.platform.receiver.BootReceiver;
import com.portfolio.platform.receiver.LocaleChangedReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import com.portfolio.platform.service.fcm.FossilFirebaseInstanceIDService;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import com.portfolio.platform.service.notification.HybridMessageNotificationComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase;
import com.portfolio.platform.ui.user.usecase.SignUpSocialUseCase;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.alarm.AlarmPresenter;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsPresenter;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.HomePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.InactivityNudgeTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemeFragment;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditPresenter;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter;
import com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightActivity;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.uirenew.splash.SplashPresenter;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.GetWeather;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.util.UserUtils;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.portfolio.platform.workers.TimeChangeReceiver;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x52 implements l42 {
    @DexIgnore
    public Provider<AlarmsRemoteDataSource> A;
    @DexIgnore
    public Provider<WorkoutDao> A0;
    @DexIgnore
    public ComplicationLastSettingRepository_Factory A1;
    @DexIgnore
    public Provider<AlarmsRepository> B;
    @DexIgnore
    public Provider<WorkoutSessionRepository> B0;
    @DexIgnore
    public WatchAppLastSettingRepository_Factory B1;
    @DexIgnore
    public Provider<AlarmHelper> C;
    @DexIgnore
    public Provider<RemindersSettingsDatabase> C0;
    @DexIgnore
    public b23 C1;
    @DexIgnore
    public Provider<FitnessDatabase> D;
    @DexIgnore
    public Provider<ThirdPartyDatabase> D0;
    @DexIgnore
    public f63 D1;
    @DexIgnore
    public Provider<ActivitySummaryDao> E;
    @DexIgnore
    public Provider<AnalyticsHelper> E0;
    @DexIgnore
    public pr2 E1;
    @DexIgnore
    public Provider<FitnessDataDao> F;
    @DexIgnore
    public Provider<CategoryDatabase> F0;
    @DexIgnore
    public or2 F1;
    @DexIgnore
    public Provider<xk2> G;
    @DexIgnore
    public Provider<CategoryDao> G0;
    @DexIgnore
    public rh3 G1;
    @DexIgnore
    public Provider<SummariesRepository> H;
    @DexIgnore
    public CategoryRemoteDataSource_Factory H0;
    @DexIgnore
    public wq2 H1;
    @DexIgnore
    public Provider<SleepDatabase> I;
    @DexIgnore
    public CategoryRepository_Factory I0;
    @DexIgnore
    public br2 I1;
    @DexIgnore
    public Provider<SleepDao> J;
    @DexIgnore
    public WatchAppRepository_Factory J0;
    @DexIgnore
    public xq2 J1;
    @DexIgnore
    public Provider<SleepSummariesRepository> K;
    @DexIgnore
    public ComplicationRepository_Factory K0;
    @DexIgnore
    public xp3 K1;
    @DexIgnore
    public Provider<GuestApiService> L;
    @DexIgnore
    public Provider<WatchFaceDao> L0;
    @DexIgnore
    public g53 L1;
    @DexIgnore
    public Provider<h42> M;
    @DexIgnore
    public WatchFaceRemoteDataSource_Factory M0;
    @DexIgnore
    public d53 M1;
    @DexIgnore
    public Provider<NotificationsDataSource> N;
    @DexIgnore
    public WatchFaceRepository_Factory N0;
    @DexIgnore
    public Provider<Map<Class<? extends ic>, Provider<ic>>> N1;
    @DexIgnore
    public Provider<NotificationsRepository> O;
    @DexIgnore
    public wy2 O0;
    @DexIgnore
    public Provider<j42> O1;
    @DexIgnore
    public Provider<DeviceDatabase> P;
    @DexIgnore
    public WatchLocalizationRepository_Factory P0;
    @DexIgnore
    public Provider<NotificationSettingsDao> P1;
    @DexIgnore
    public Provider<DeviceDao> Q;
    @DexIgnore
    public qn3 Q0;
    @DexIgnore
    public Provider<b62> Q1;
    @DexIgnore
    public Provider<SkuDao> R;
    @DexIgnore
    public rn3 R0;
    @DexIgnore
    public Provider<MigrationHelper> R1;
    @DexIgnore
    public DeviceRemoteDataSource_Factory S;
    @DexIgnore
    public rq2 S0;
    @DexIgnore
    public Provider<ServerSettingDataSource> S1;
    @DexIgnore
    public Provider<DeviceRepository> T;
    @DexIgnore
    public lr3 T0;
    @DexIgnore
    public Provider<ServerSettingDataSource> T1;
    @DexIgnore
    public Provider<ContentResolver> U;
    @DexIgnore
    public qr3 U0;
    @DexIgnore
    public Provider<UserUtils> U1;
    @DexIgnore
    public Provider<j62> V;
    @DexIgnore
    public dr2 V0;
    @DexIgnore
    public Provider<HybridCustomizeDatabase> W;
    @DexIgnore
    public qq2 W0;
    @DexIgnore
    public Provider<HybridPresetDao> X;
    @DexIgnore
    public wj2 X0;
    @DexIgnore
    public HybridPresetRemoteDataSource_Factory Y;
    @DexIgnore
    public Provider<ApplicationEventListener> Y0;
    @DexIgnore
    public Provider<HybridPresetRepository> Z;
    @DexIgnore
    public Provider<ShakeFeedbackService> Z0;
    @DexIgnore
    public n42 a;
    @DexIgnore
    public Provider<SampleRawDao> a0;
    @DexIgnore
    public FitnessDataRepository_Factory a1;
    @DexIgnore
    public Provider<Context> b;
    @DexIgnore
    public Provider<ActivitySampleDao> b0;
    @DexIgnore
    public a52 b1;
    @DexIgnore
    public Provider<en2> c;
    @DexIgnore
    public Provider<ActivitiesRepository> c0;
    @DexIgnore
    public Provider<ThirdPartyRepository> c1;
    @DexIgnore
    public Provider<PortfolioApp> d;
    @DexIgnore
    public Provider<ShortcutApiService> d0;
    @DexIgnore
    public nu3 d1;
    @DexIgnore
    public Provider<DNDSettingsDatabase> e;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> e0;
    @DexIgnore
    public Provider<jo2> e1;
    @DexIgnore
    public Provider<NotificationSettingsDatabase> f;
    @DexIgnore
    public Provider<MicroAppSettingDataSource> f0;
    @DexIgnore
    public Provider<SmsMmsReceiver> f1;
    @DexIgnore
    public Provider<DianaNotificationComponent> g;
    @DexIgnore
    public Provider<MicroAppSettingRepository> g0;
    @DexIgnore
    public pr3 g1;
    @DexIgnore
    public Provider<DianaCustomizeDatabase> h;
    @DexIgnore
    public Provider<SleepSessionsRepository> h0;
    @DexIgnore
    public Provider<LinkStreamingManager> h1;
    @DexIgnore
    public Provider<DianaPresetDao> i;
    @DexIgnore
    public Provider<GoalTrackingDatabase> i0;
    @DexIgnore
    public Provider<WatchParamHelper> i1;
    @DexIgnore
    public Provider<dn2> j;
    @DexIgnore
    public Provider<GoalTrackingDao> j0;
    @DexIgnore
    public Provider<HybridMessageNotificationComponent> j1;
    @DexIgnore
    public Provider<AuthApiGuestService> k;
    @DexIgnore
    public Provider<GoalTrackingRepository> k0;
    @DexIgnore
    public Provider<in2> k1;
    @DexIgnore
    public Provider<xo2> l;
    @DexIgnore
    public Provider<jn2> l0;
    @DexIgnore
    public Provider<kn2> l1;
    @DexIgnore
    public Provider<bp2> m;
    @DexIgnore
    public Provider<WatchAppDao> m0;
    @DexIgnore
    public Provider<MFLoginWechatManager> m1;
    @DexIgnore
    public Provider<ApiServiceV2> n;
    @DexIgnore
    public Provider<WatchAppRemoteDataSource> n0;
    @DexIgnore
    public MicroAppLastSettingRepository_Factory n1;
    @DexIgnore
    public Provider<DianaPresetRemoteDataSource> o;
    @DexIgnore
    public Provider<ComplicationDao> o0;
    @DexIgnore
    public Provider<MigrationManager> o1;
    @DexIgnore
    public Provider<DianaPresetRepository> p;
    @DexIgnore
    public Provider<ComplicationRemoteDataSource> p0;
    @DexIgnore
    public Provider<FirmwareFileRepository> p1;
    @DexIgnore
    public Provider<CustomizeRealDataDatabase> q;
    @DexIgnore
    public Provider<MicroAppDao> q0;
    @DexIgnore
    public Provider<lr2> q1;
    @DexIgnore
    public Provider<CustomizeRealDataDao> r;
    @DexIgnore
    public MicroAppRemoteDataSource_Factory r0;
    @DexIgnore
    public Provider<LocationSource> r1;
    @DexIgnore
    public Provider<CustomizeRealDataRepository> s;
    @DexIgnore
    public Provider<MicroAppRepository> s0;
    @DexIgnore
    public Provider<InAppNotificationDatabase> s1;
    @DexIgnore
    public Provider<AuthApiUserService> t;
    @DexIgnore
    public Provider<MicroAppLastSettingDao> t0;
    @DexIgnore
    public Provider<InAppNotificationDao> t1;
    @DexIgnore
    public Provider<UserDataSource> u;
    @DexIgnore
    public Provider<ComplicationLastSettingDao> u0;
    @DexIgnore
    public Provider<InAppNotificationRepository> u1;
    @DexIgnore
    public Provider<UserDataSource> v;
    @DexIgnore
    public Provider<WatchAppLastSettingDao> v0;
    @DexIgnore
    public Provider<ak3> v1;
    @DexIgnore
    public Provider<UserRepository> w;
    @DexIgnore
    public Provider<HeartRateSampleDao> w0;
    @DexIgnore
    public Provider<rc> w1;
    @DexIgnore
    public Provider<AlarmDatabase> x;
    @DexIgnore
    public Provider<HeartRateSampleRepository> x0;
    @DexIgnore
    public Provider<GoogleApiService> x1;
    @DexIgnore
    public Provider<AlarmDao> y;
    @DexIgnore
    public Provider<HeartRateDailySummaryDao> y0;
    @DexIgnore
    public Provider<ql2> y1;
    @DexIgnore
    public Provider<AlarmsLocalDataSource> z;
    @DexIgnore
    public Provider<HeartRateSummaryRepository> z0;
    @DexIgnore
    public Provider<f42> z1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a0 implements mk3 {
        @DexIgnore
        public pk3 a;

        @DexIgnore
        public final ExploreWatchPresenter a() {
            ExploreWatchPresenter a2 = rk3.a(qk3.a(this.a), x52.this.l(), (DeviceRepository) x52.this.T.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ExploreWatchActivity b(ExploreWatchActivity exploreWatchActivity) {
            dq2.a((BaseActivity) exploreWatchActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) exploreWatchActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) exploreWatchActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) exploreWatchActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) exploreWatchActivity, new gr2());
            lk3.a(exploreWatchActivity, a());
            return exploreWatchActivity;
        }

        @DexIgnore
        public a0(pk3 pk3) {
            a(pk3);
        }

        @DexIgnore
        public final void a(pk3 pk3) {
            n44.a(pk3);
            this.a = pk3;
        }

        @DexIgnore
        public void a(ExploreWatchActivity exploreWatchActivity) {
            b(exploreWatchActivity);
        }

        @DexIgnore
        public final ExploreWatchPresenter a(ExploreWatchPresenter exploreWatchPresenter) {
            sk3.a(exploreWatchPresenter);
            return exploreWatchPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a1 implements an3 {
        @DexIgnore
        public hn3 a;

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase a() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) x52.this.d.get(), (GuestApiService) x52.this.L.get());
        }

        @DexIgnore
        public final LinkDeviceUseCase b() {
            return new LinkDeviceUseCase((DeviceRepository) x52.this.T.get(), a(), (en2) x52.this.c.get(), x52.this.l());
        }

        @DexIgnore
        public final PairingPresenter c() {
            PairingPresenter a2 = kn3.a(in3.a(this.a), b(), (DeviceRepository) x52.this.T.get(), (NotificationsRepository) x52.this.O.get(), x52.this.y(), new px2(), x52.this.l(), (NotificationSettingsDatabase) x52.this.f.get(), x52.this.P(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public a1(hn3 hn3) {
            a(hn3);
        }

        @DexIgnore
        public final void a(hn3 hn3) {
            n44.a(hn3);
            this.a = hn3;
        }

        @DexIgnore
        public void a(PairingActivity pairingActivity) {
            b(pairingActivity);
        }

        @DexIgnore
        public final PairingPresenter a(PairingPresenter pairingPresenter) {
            ln3.a(pairingPresenter);
            return pairingPresenter;
        }

        @DexIgnore
        public final PairingActivity b(PairingActivity pairingActivity) {
            dq2.a((BaseActivity) pairingActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) pairingActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) pairingActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) pairingActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) pairingActivity, new gr2());
            zm3.a(pairingActivity, c());
            return pairingActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements sg3 {
        @DexIgnore
        public wg3 a;

        @DexIgnore
        public final yg3 a() {
            yg3 a2 = zg3.a(xg3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final AboutActivity b(AboutActivity aboutActivity) {
            dq2.a((BaseActivity) aboutActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) aboutActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) aboutActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) aboutActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) aboutActivity, new gr2());
            rg3.a(aboutActivity, a());
            return aboutActivity;
        }

        @DexIgnore
        public b(wg3 wg3) {
            a(wg3);
        }

        @DexIgnore
        public final void a(wg3 wg3) {
            n44.a(wg3);
            this.a = wg3;
        }

        @DexIgnore
        public void a(AboutActivity aboutActivity) {
            b(aboutActivity);
        }

        @DexIgnore
        public final yg3 a(yg3 yg3) {
            ah3.a(yg3);
            return yg3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b0 implements jq3 {
        @DexIgnore
        public mq3 a;

        @DexIgnore
        public final FindDevicePresenter a() {
            FindDevicePresenter a2 = oq3.a((rc) x52.this.w1.get(), (DeviceRepository) x52.this.T.get(), (en2) x52.this.c.get(), nq3.a(this.a), new er2(), c(), b(), new gr2(), (PortfolioApp) x52.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GetAddress b() {
            return new GetAddress((GoogleApiService) x52.this.x1.get());
        }

        @DexIgnore
        public final fr2 c() {
            return new fr2((f42) x52.this.z1.get());
        }

        @DexIgnore
        public b0(mq3 mq3) {
            a(mq3);
        }

        @DexIgnore
        public final FindDeviceActivity b(FindDeviceActivity findDeviceActivity) {
            dq2.a((BaseActivity) findDeviceActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) findDeviceActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) findDeviceActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) findDeviceActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) findDeviceActivity, new gr2());
            iq3.a(findDeviceActivity, a());
            return findDeviceActivity;
        }

        @DexIgnore
        public final void a(mq3 mq3) {
            n44.a(mq3);
            this.a = mq3;
        }

        @DexIgnore
        public void a(FindDeviceActivity findDeviceActivity) {
            b(findDeviceActivity);
        }

        @DexIgnore
        public final FindDevicePresenter a(FindDevicePresenter findDevicePresenter) {
            pq3.a(findDevicePresenter);
            return findDevicePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b1 implements qm3 {
        @DexIgnore
        public tm3 a;

        @DexIgnore
        public final vm3 a() {
            vm3 a2 = wm3.a(um3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PairingInstructionsActivity b(PairingInstructionsActivity pairingInstructionsActivity) {
            dq2.a((BaseActivity) pairingInstructionsActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) pairingInstructionsActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) pairingInstructionsActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) pairingInstructionsActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) pairingInstructionsActivity, new gr2());
            pm3.a(pairingInstructionsActivity, a());
            return pairingInstructionsActivity;
        }

        @DexIgnore
        public b1(tm3 tm3) {
            a(tm3);
        }

        @DexIgnore
        public final void a(tm3 tm3) {
            n44.a(tm3);
            this.a = tm3;
        }

        @DexIgnore
        public void a(PairingInstructionsActivity pairingInstructionsActivity) {
            b(pairingInstructionsActivity);
        }

        @DexIgnore
        public final vm3 a(vm3 vm3) {
            xm3.a(vm3);
            return vm3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements ae3 {
        @DexIgnore
        public ee3 a;

        @DexIgnore
        public final ActiveTimeDetailPresenter a() {
            ActiveTimeDetailPresenter a2 = ge3.a(fe3.a(this.a), (SummariesRepository) x52.this.H.get(), (ActivitiesRepository) x52.this.c0.get(), (UserRepository) x52.this.w.get(), (WorkoutSessionRepository) x52.this.B0.get(), (FitnessDataDao) x52.this.F.get(), (WorkoutDao) x52.this.A0.get(), (FitnessDatabase) x52.this.D.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeDetailActivity b(ActiveTimeDetailActivity activeTimeDetailActivity) {
            dq2.a((BaseActivity) activeTimeDetailActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) activeTimeDetailActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) activeTimeDetailActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) activeTimeDetailActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) activeTimeDetailActivity, new gr2());
            zd3.a(activeTimeDetailActivity, a());
            return activeTimeDetailActivity;
        }

        @DexIgnore
        public c(ee3 ee3) {
            a(ee3);
        }

        @DexIgnore
        public final void a(ee3 ee3) {
            n44.a(ee3);
            this.a = ee3;
        }

        @DexIgnore
        public void a(ActiveTimeDetailActivity activeTimeDetailActivity) {
            b(activeTimeDetailActivity);
        }

        @DexIgnore
        public final ActiveTimeDetailPresenter a(ActiveTimeDetailPresenter activeTimeDetailPresenter) {
            he3.a(activeTimeDetailPresenter);
            return activeTimeDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c0 implements uk3 {
        @DexIgnore
        public xk3 a;

        @DexIgnore
        public final zk3 a() {
            zk3 a2 = al3.a(yk3.a(this.a), b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ResetPasswordUseCase b() {
            return new ResetPasswordUseCase((AuthApiGuestService) x52.this.k.get());
        }

        @DexIgnore
        public c0(xk3 xk3) {
            a(xk3);
        }

        @DexIgnore
        public final ForgotPasswordActivity b(ForgotPasswordActivity forgotPasswordActivity) {
            dq2.a((BaseActivity) forgotPasswordActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) forgotPasswordActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) forgotPasswordActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) forgotPasswordActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) forgotPasswordActivity, new gr2());
            tk3.a(forgotPasswordActivity, a());
            return forgotPasswordActivity;
        }

        @DexIgnore
        public final void a(xk3 xk3) {
            n44.a(xk3);
            this.a = xk3;
        }

        @DexIgnore
        public void a(ForgotPasswordActivity forgotPasswordActivity) {
            b(forgotPasswordActivity);
        }

        @DexIgnore
        public final zk3 a(zk3 zk3) {
            bl3.a(zk3);
            return zk3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c1 implements un3 {
        @DexIgnore
        public yn3 a;

        @DexIgnore
        public final bo3 a() {
            bo3 a2 = co3.a(ao3.a(this.a), zn3.a(this.a), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PermissionActivity b(PermissionActivity permissionActivity) {
            dq2.a((BaseActivity) permissionActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) permissionActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) permissionActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) permissionActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) permissionActivity, new gr2());
            tn3.a(permissionActivity, a());
            return permissionActivity;
        }

        @DexIgnore
        public c1(yn3 yn3) {
            a(yn3);
        }

        @DexIgnore
        public final void a(yn3 yn3) {
            n44.a(yn3);
            this.a = yn3;
        }

        @DexIgnore
        public void a(PermissionActivity permissionActivity) {
            b(permissionActivity);
        }

        @DexIgnore
        public final bo3 a(bo3 bo3) {
            do3.a(bo3);
            return bo3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements d83 {
        @DexIgnore
        public k83 a;

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter a() {
            ActiveTimeOverviewDayPresenter a2 = h83.a(l83.a(this.a), (SummariesRepository) x52.this.H.get(), (ActivitiesRepository) x52.this.c0.get(), (WorkoutSessionRepository) x52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter b() {
            ActiveTimeOverviewMonthPresenter a2 = r83.a(m83.a(this.a), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter c() {
            ActiveTimeOverviewWeekPresenter a2 = w83.a(n83.a(this.a), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public d(k83 k83) {
            a(k83);
        }

        @DexIgnore
        public final ActiveTimeOverviewFragment b(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            j83.a(activeTimeOverviewFragment, a());
            j83.a(activeTimeOverviewFragment, c());
            j83.a(activeTimeOverviewFragment, b());
            return activeTimeOverviewFragment;
        }

        @DexIgnore
        public final void a(k83 k83) {
            n44.a(k83);
            this.a = k83;
        }

        @DexIgnore
        public void a(ActiveTimeOverviewFragment activeTimeOverviewFragment) {
            b(activeTimeOverviewFragment);
        }

        @DexIgnore
        public final ActiveTimeOverviewDayPresenter a(ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
            i83.a(activeTimeOverviewDayPresenter);
            return activeTimeOverviewDayPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewWeekPresenter a(ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter) {
            x83.a(activeTimeOverviewWeekPresenter);
            return activeTimeOverviewWeekPresenter;
        }

        @DexIgnore
        public final ActiveTimeOverviewMonthPresenter a(ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
            s83.a(activeTimeOverviewMonthPresenter);
            return activeTimeOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 implements cf3 {
        @DexIgnore
        public gf3 a;

        @DexIgnore
        public final GoalTrackingDetailPresenter a() {
            GoalTrackingDetailPresenter a2 = if3.a(hf3.a(this.a), (GoalTrackingRepository) x52.this.k0.get(), (GoalTrackingDao) x52.this.j0.get(), (GoalTrackingDatabase) x52.this.i0.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingDetailActivity b(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            dq2.a((BaseActivity) goalTrackingDetailActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) goalTrackingDetailActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) goalTrackingDetailActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) goalTrackingDetailActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) goalTrackingDetailActivity, new gr2());
            bf3.a(goalTrackingDetailActivity, a());
            return goalTrackingDetailActivity;
        }

        @DexIgnore
        public d0(gf3 gf3) {
            a(gf3);
        }

        @DexIgnore
        public final void a(gf3 gf3) {
            n44.a(gf3);
            this.a = gf3;
        }

        @DexIgnore
        public void a(GoalTrackingDetailActivity goalTrackingDetailActivity) {
            b(goalTrackingDetailActivity);
        }

        @DexIgnore
        public final GoalTrackingDetailPresenter a(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            jf3.a(goalTrackingDetailPresenter);
            return goalTrackingDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d1 implements pj3 {
        @DexIgnore
        public sj3 a;

        @DexIgnore
        public final uj3 a() {
            uj3 a2 = vj3.a(tj3.a(this.a), x52.this.V(), x52.this.C());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final PreferredUnitActivity b(PreferredUnitActivity preferredUnitActivity) {
            dq2.a((BaseActivity) preferredUnitActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) preferredUnitActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) preferredUnitActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) preferredUnitActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) preferredUnitActivity, new gr2());
            oj3.a(preferredUnitActivity, a());
            return preferredUnitActivity;
        }

        @DexIgnore
        public d1(sj3 sj3) {
            a(sj3);
        }

        @DexIgnore
        public final void a(sj3 sj3) {
            n44.a(sj3);
            this.a = sj3;
        }

        @DexIgnore
        public void a(PreferredUnitActivity preferredUnitActivity) {
            b(preferredUnitActivity);
        }

        @DexIgnore
        public final uj3 a(uj3 uj3) {
            wj3.a(uj3);
            return uj3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements je3 {
        @DexIgnore
        public ne3 a;

        @DexIgnore
        public final ActivityDetailPresenter a() {
            ActivityDetailPresenter a2 = pe3.a(oe3.a(this.a), (SummariesRepository) x52.this.H.get(), (ActivitiesRepository) x52.this.c0.get(), (UserRepository) x52.this.w.get(), (WorkoutSessionRepository) x52.this.B0.get(), (FitnessDataDao) x52.this.F.get(), (WorkoutDao) x52.this.A0.get(), (FitnessDatabase) x52.this.D.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityDetailActivity b(ActivityDetailActivity activityDetailActivity) {
            dq2.a((BaseActivity) activityDetailActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) activityDetailActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) activityDetailActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) activityDetailActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) activityDetailActivity, new gr2());
            ie3.a(activityDetailActivity, a());
            return activityDetailActivity;
        }

        @DexIgnore
        public e(ne3 ne3) {
            a(ne3);
        }

        @DexIgnore
        public final void a(ne3 ne3) {
            n44.a(ne3);
            this.a = ne3;
        }

        @DexIgnore
        public void a(ActivityDetailActivity activityDetailActivity) {
            b(activityDetailActivity);
        }

        @DexIgnore
        public final ActivityDetailPresenter a(ActivityDetailPresenter activityDetailPresenter) {
            qe3.a(activityDetailPresenter);
            return activityDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e0 implements db3 {
        @DexIgnore
        public kb3 a;

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter a() {
            GoalTrackingOverviewDayPresenter a2 = hb3.a(lb3.a(this.a), (en2) x52.this.c.get(), (GoalTrackingRepository) x52.this.k0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter b() {
            GoalTrackingOverviewMonthPresenter a2 = rb3.a(mb3.a(this.a), (UserRepository) x52.this.w.get(), (en2) x52.this.c.get(), (GoalTrackingRepository) x52.this.k0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter c() {
            GoalTrackingOverviewWeekPresenter a2 = wb3.a(nb3.a(this.a), (UserRepository) x52.this.w.get(), (en2) x52.this.c.get(), (GoalTrackingRepository) x52.this.k0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public e0(kb3 kb3) {
            a(kb3);
        }

        @DexIgnore
        public final void a(kb3 kb3) {
            n44.a(kb3);
            this.a = kb3;
        }

        @DexIgnore
        public void a(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            b(goalTrackingOverviewFragment);
        }

        @DexIgnore
        public final GoalTrackingOverviewFragment b(GoalTrackingOverviewFragment goalTrackingOverviewFragment) {
            jb3.a(goalTrackingOverviewFragment, a());
            jb3.a(goalTrackingOverviewFragment, c());
            jb3.a(goalTrackingOverviewFragment, b());
            return goalTrackingOverviewFragment;
        }

        @DexIgnore
        public final GoalTrackingOverviewDayPresenter a(GoalTrackingOverviewDayPresenter goalTrackingOverviewDayPresenter) {
            ib3.a(goalTrackingOverviewDayPresenter);
            return goalTrackingOverviewDayPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewWeekPresenter a(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
            xb3.a(goalTrackingOverviewWeekPresenter);
            return goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        public final GoalTrackingOverviewMonthPresenter a(GoalTrackingOverviewMonthPresenter goalTrackingOverviewMonthPresenter) {
            sb3.a(goalTrackingOverviewMonthPresenter);
            return goalTrackingOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e1 implements fj3 {
        @DexIgnore
        public jj3 a;

        @DexIgnore
        public final jr2 a() {
            return new jr2((AuthApiUserService) x52.this.t.get());
        }

        @DexIgnore
        public final lj3 b() {
            lj3 a2 = mj3.a(kj3.a(this.a), a(), (j62) x52.this.V.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public e1(jj3 jj3) {
            a(jj3);
        }

        @DexIgnore
        public final void a(jj3 jj3) {
            n44.a(jj3);
            this.a = jj3;
        }

        @DexIgnore
        public void a(ProfileChangePasswordActivity profileChangePasswordActivity) {
            b(profileChangePasswordActivity);
        }

        @DexIgnore
        public final lj3 a(lj3 lj3) {
            nj3.a(lj3);
            return lj3;
        }

        @DexIgnore
        public final ProfileChangePasswordActivity b(ProfileChangePasswordActivity profileChangePasswordActivity) {
            dq2.a((BaseActivity) profileChangePasswordActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) profileChangePasswordActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) profileChangePasswordActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) profileChangePasswordActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) profileChangePasswordActivity, new gr2());
            ej3.a(profileChangePasswordActivity, b());
            return profileChangePasswordActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f implements d93 {
        @DexIgnore
        public k93 a;

        @DexIgnore
        public final ActivityOverviewDayPresenter a() {
            ActivityOverviewDayPresenter a2 = h93.a(l93.a(this.a), (SummariesRepository) x52.this.H.get(), (ActivitiesRepository) x52.this.c0.get(), (WorkoutSessionRepository) x52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter b() {
            ActivityOverviewMonthPresenter a2 = r93.a(m93.a(this.a), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter c() {
            ActivityOverviewWeekPresenter a2 = w93.a(n93.a(this.a), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public f(k93 k93) {
            a(k93);
        }

        @DexIgnore
        public final ActivityOverviewFragment b(ActivityOverviewFragment activityOverviewFragment) {
            j93.a(activityOverviewFragment, a());
            j93.a(activityOverviewFragment, c());
            j93.a(activityOverviewFragment, b());
            return activityOverviewFragment;
        }

        @DexIgnore
        public final void a(k93 k93) {
            n44.a(k93);
            this.a = k93;
        }

        @DexIgnore
        public void a(ActivityOverviewFragment activityOverviewFragment) {
            b(activityOverviewFragment);
        }

        @DexIgnore
        public final ActivityOverviewDayPresenter a(ActivityOverviewDayPresenter activityOverviewDayPresenter) {
            i93.a(activityOverviewDayPresenter);
            return activityOverviewDayPresenter;
        }

        @DexIgnore
        public final ActivityOverviewWeekPresenter a(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
            x93.a(activityOverviewWeekPresenter);
            return activityOverviewWeekPresenter;
        }

        @DexIgnore
        public final ActivityOverviewMonthPresenter a(ActivityOverviewMonthPresenter activityOverviewMonthPresenter) {
            s93.a(activityOverviewMonthPresenter);
            return activityOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f0 implements lf3 {
        @DexIgnore
        public pf3 a;

        @DexIgnore
        public final HeartRateDetailPresenter a() {
            HeartRateDetailPresenter a2 = rf3.a(qf3.a(this.a), (HeartRateSummaryRepository) x52.this.z0.get(), (HeartRateSampleRepository) x52.this.x0.get(), (UserRepository) x52.this.w.get(), (WorkoutSessionRepository) x52.this.B0.get(), (FitnessDataDao) x52.this.F.get(), (WorkoutDao) x52.this.A0.get(), (FitnessDatabase) x52.this.D.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateDetailActivity b(HeartRateDetailActivity heartRateDetailActivity) {
            dq2.a((BaseActivity) heartRateDetailActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) heartRateDetailActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) heartRateDetailActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) heartRateDetailActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) heartRateDetailActivity, new gr2());
            kf3.a(heartRateDetailActivity, a());
            return heartRateDetailActivity;
        }

        @DexIgnore
        public f0(pf3 pf3) {
            a(pf3);
        }

        @DexIgnore
        public final void a(pf3 pf3) {
            n44.a(pf3);
            this.a = pf3;
        }

        @DexIgnore
        public void a(HeartRateDetailActivity heartRateDetailActivity) {
            b(heartRateDetailActivity);
        }

        @DexIgnore
        public final HeartRateDetailPresenter a(HeartRateDetailPresenter heartRateDetailPresenter) {
            sf3.a(heartRateDetailPresenter);
            return heartRateDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f1 implements lh3 {
        @DexIgnore
        public void a(mh3 mh3) {
            b(mh3);
        }

        @DexIgnore
        public final mh3 b(mh3 mh3) {
            oh3.a(mh3, (j42) x52.this.O1.get());
            return mh3;
        }

        @DexIgnore
        public f1(ph3 ph3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements wt2 {
        @DexIgnore
        public au2 a;

        @DexIgnore
        public final AlarmPresenter a() {
            AlarmPresenter a2 = eu2.a(du2.a(this.a), cu2.a(this.a), bu2.a(this.a), this.a.a(), c(), (AlarmHelper) x52.this.C.get(), b(), (UserRepository) x52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DeleteAlarm b() {
            return new DeleteAlarm((AlarmsRepository) x52.this.B.get());
        }

        @DexIgnore
        public final SetAlarms c() {
            return new SetAlarms((PortfolioApp) x52.this.d.get(), (AlarmsRepository) x52.this.B.get());
        }

        @DexIgnore
        public g(au2 au2) {
            a(au2);
        }

        @DexIgnore
        public final AlarmActivity b(AlarmActivity alarmActivity) {
            dq2.a((BaseActivity) alarmActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) alarmActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) alarmActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) alarmActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) alarmActivity, new gr2());
            vt2.a(alarmActivity, a());
            return alarmActivity;
        }

        @DexIgnore
        public final void a(au2 au2) {
            n44.a(au2);
            this.a = au2;
        }

        @DexIgnore
        public void a(AlarmActivity alarmActivity) {
            b(alarmActivity);
        }

        @DexIgnore
        public final AlarmPresenter a(AlarmPresenter alarmPresenter) {
            fu2.a(alarmPresenter);
            return alarmPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g0 implements dc3 {
        @DexIgnore
        public kc3 a;

        @DexIgnore
        public final HeartRateOverviewDayPresenter a() {
            HeartRateOverviewDayPresenter a2 = hc3.a(lc3.a(this.a), (HeartRateSampleRepository) x52.this.x0.get(), (WorkoutSessionRepository) x52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter b() {
            HeartRateOverviewMonthPresenter a2 = rc3.a(mc3.a(this.a), (UserRepository) x52.this.w.get(), (HeartRateSummaryRepository) x52.this.z0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter c() {
            HeartRateOverviewWeekPresenter a2 = wc3.a(nc3.a(this.a), (UserRepository) x52.this.w.get(), (HeartRateSummaryRepository) x52.this.z0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public g0(kc3 kc3) {
            a(kc3);
        }

        @DexIgnore
        public final void a(kc3 kc3) {
            n44.a(kc3);
            this.a = kc3;
        }

        @DexIgnore
        public final HeartRateOverviewFragment b(HeartRateOverviewFragment heartRateOverviewFragment) {
            jc3.a(heartRateOverviewFragment, a());
            jc3.a(heartRateOverviewFragment, c());
            jc3.a(heartRateOverviewFragment, b());
            return heartRateOverviewFragment;
        }

        @DexIgnore
        public void a(HeartRateOverviewFragment heartRateOverviewFragment) {
            b(heartRateOverviewFragment);
        }

        @DexIgnore
        public final HeartRateOverviewDayPresenter a(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            ic3.a(heartRateOverviewDayPresenter);
            return heartRateOverviewDayPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewWeekPresenter a(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
            xc3.a(heartRateOverviewWeekPresenter);
            return heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        public final HeartRateOverviewMonthPresenter a(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            sc3.a(heartRateOverviewMonthPresenter);
            return heartRateOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g1 implements th3 {
        @DexIgnore
        public yh3 a;

        @DexIgnore
        public final ProfileGoalEditPresenter a() {
            ProfileGoalEditPresenter a2 = bi3.a(zh3.a(this.a), (SummariesRepository) x52.this.H.get(), (SleepSummariesRepository) x52.this.K.get(), (GoalTrackingRepository) x52.this.k0.get(), (UserRepository) x52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileGoalEditActivity b(ProfileGoalEditActivity profileGoalEditActivity) {
            dq2.a((BaseActivity) profileGoalEditActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) profileGoalEditActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) profileGoalEditActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) profileGoalEditActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) profileGoalEditActivity, new gr2());
            sh3.a(profileGoalEditActivity, a());
            return profileGoalEditActivity;
        }

        @DexIgnore
        public g1(yh3 yh3) {
            a(yh3);
        }

        @DexIgnore
        public final void a(yh3 yh3) {
            n44.a(yh3);
            this.a = yh3;
        }

        @DexIgnore
        public void a(ProfileGoalEditActivity profileGoalEditActivity) {
            b(profileGoalEditActivity);
        }

        @DexIgnore
        public final ProfileGoalEditPresenter a(ProfileGoalEditPresenter profileGoalEditPresenter) {
            ci3.a(profileGoalEditPresenter);
            return profileGoalEditPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h implements am3 {
        @DexIgnore
        public dm3 a;

        @DexIgnore
        public final fm3 a() {
            fm3 a2 = gm3.a(em3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final hs2 b(hs2 hs2) {
            js2.a(hs2, a());
            return hs2;
        }

        @DexIgnore
        public h(x52 x52, dm3 dm3) {
            a(dm3);
        }

        @DexIgnore
        public final void a(dm3 dm3) {
            n44.a(dm3);
            this.a = dm3;
        }

        @DexIgnore
        public void a(hs2 hs2) {
            b(hs2);
        }

        @DexIgnore
        public final fm3 a(fm3 fm3) {
            hm3.a(fm3);
            return fm3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h0 implements fi3 {
        @DexIgnore
        public ji3 a;

        @DexIgnore
        public final kr2 a() {
            return new kr2((UserRepository) x52.this.w.get(), (DeviceRepository) x52.this.T.get(), (en2) x52.this.c.get());
        }

        @DexIgnore
        public final HelpPresenter b() {
            HelpPresenter a2 = li3.a(ki3.a(this.a), (DeviceRepository) x52.this.T.get(), a(), (AnalyticsHelper) x52.this.E0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public h0(ji3 ji3) {
            a(ji3);
        }

        @DexIgnore
        public final void a(ji3 ji3) {
            n44.a(ji3);
            this.a = ji3;
        }

        @DexIgnore
        public void a(HelpActivity helpActivity) {
            b(helpActivity);
        }

        @DexIgnore
        public final HelpPresenter a(HelpPresenter helpPresenter) {
            mi3.a(helpPresenter);
            return helpPresenter;
        }

        @DexIgnore
        public final HelpActivity b(HelpActivity helpActivity) {
            dq2.a((BaseActivity) helpActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) helpActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) helpActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) helpActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) helpActivity, new gr2());
            ei3.a(helpActivity, b());
            return helpActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class h1 implements wi3 {
        @DexIgnore
        public aj3 a;

        @DexIgnore
        public final ProfileOptInPresenter a() {
            ProfileOptInPresenter a2 = cj3.a(bj3.a(this.a), x52.this.V(), (UserRepository) x52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ProfileOptInActivity b(ProfileOptInActivity profileOptInActivity) {
            dq2.a((BaseActivity) profileOptInActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) profileOptInActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) profileOptInActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) profileOptInActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) profileOptInActivity, new gr2());
            vi3.a(profileOptInActivity, a());
            return profileOptInActivity;
        }

        @DexIgnore
        public h1(aj3 aj3) {
            a(aj3);
        }

        @DexIgnore
        public final void a(aj3 aj3) {
            n44.a(aj3);
            this.a = aj3;
        }

        @DexIgnore
        public void a(ProfileOptInActivity profileOptInActivity) {
            b(profileOptInActivity);
        }

        @DexIgnore
        public final ProfileOptInPresenter a(ProfileOptInPresenter profileOptInPresenter) {
            dj3.a(profileOptInPresenter);
            return profileOptInPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public n42 a;
        @DexIgnore
        public PortfolioDatabaseModule b;
        @DexIgnore
        public RepositoriesModule c;
        @DexIgnore
        public NotificationsRepositoryModule d;
        @DexIgnore
        public MicroAppSettingRepositoryModule e;
        @DexIgnore
        public UAppSystemVersionRepositoryModule f;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public l42 a() {
            if (this.a != null) {
                if (this.b == null) {
                    this.b = new PortfolioDatabaseModule();
                }
                if (this.c == null) {
                    this.c = new RepositoriesModule();
                }
                if (this.d == null) {
                    this.d = new NotificationsRepositoryModule();
                }
                if (this.e == null) {
                    this.e = new MicroAppSettingRepositoryModule();
                }
                if (this.f == null) {
                    this.f = new UAppSystemVersionRepositoryModule();
                }
                return new x52(this);
            }
            throw new IllegalStateException(n42.class.getCanonicalName() + " must be set");
        }

        @DexIgnore
        public i a(n42 n42) {
            n44.a(n42);
            this.a = n42;
            return this;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i0 implements qv2 {
        @DexIgnore
        public vv2 a;

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter a() {
            DoNotDisturbScheduledTimePresenter a2 = ty2.a(wv2.a(this.a), (DNDSettingsDatabase) x52.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final tv2 b(tv2 tv2) {
            uv2.a(tv2, a());
            return tv2;
        }

        @DexIgnore
        public i0(vv2 vv2) {
            a(vv2);
        }

        @DexIgnore
        public final void a(vv2 vv2) {
            n44.a(vv2);
            this.a = vv2;
        }

        @DexIgnore
        public void a(tv2 tv2) {
            b(tv2);
        }

        @DexIgnore
        public final DoNotDisturbScheduledTimePresenter a(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter) {
            uy2.a(doNotDisturbScheduledTimePresenter);
            return doNotDisturbScheduledTimePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class i1 implements tl3 {
        @DexIgnore
        public wl3 a;

        @DexIgnore
        public final GetRecommendedGoalUseCase a() {
            return new GetRecommendedGoalUseCase((SummariesRepository) x52.this.H.get(), (SleepSummariesRepository) x52.this.K.get(), (SleepSessionsRepository) x52.this.h0.get());
        }

        @DexIgnore
        public final ProfileSetupPresenter b() {
            ProfileSetupPresenter a2 = yl3.a(xl3.a(this.a), a(), (UserRepository) x52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public i1(wl3 wl3) {
            a(wl3);
        }

        @DexIgnore
        public final void a(wl3 wl3) {
            n44.a(wl3);
            this.a = wl3;
        }

        @DexIgnore
        public void a(ProfileSetupActivity profileSetupActivity) {
            b(profileSetupActivity);
        }

        @DexIgnore
        public final ProfileSetupPresenter a(ProfileSetupPresenter profileSetupPresenter) {
            zl3.a(profileSetupPresenter, x52.this.Q());
            zl3.a(profileSetupPresenter, x52.this.R());
            zl3.a(profileSetupPresenter, x52.this.C());
            zl3.a(profileSetupPresenter, (AnalyticsHelper) x52.this.E0.get());
            zl3.a(profileSetupPresenter);
            return profileSetupPresenter;
        }

        @DexIgnore
        public final ProfileSetupActivity b(ProfileSetupActivity profileSetupActivity) {
            dq2.a((BaseActivity) profileSetupActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) profileSetupActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) profileSetupActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) profileSetupActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) profileSetupActivity, new gr2());
            sl3.a(profileSetupActivity, b());
            return profileSetupActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j implements zp3 {
        @DexIgnore
        public cq3 a;

        @DexIgnore
        public final eq3 a() {
            eq3 a2 = gq3.a((PortfolioApp) x52.this.d.get(), dq3.a(this.a), (rc) x52.this.w1.get(), (ql2) x52.this.y1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CalibrationActivity b(CalibrationActivity calibrationActivity) {
            dq2.a((BaseActivity) calibrationActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) calibrationActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) calibrationActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) calibrationActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) calibrationActivity, new gr2());
            yp3.a(calibrationActivity, a());
            return calibrationActivity;
        }

        @DexIgnore
        public j(cq3 cq3) {
            a(cq3);
        }

        @DexIgnore
        public final void a(cq3 cq3) {
            n44.a(cq3);
            this.a = cq3;
        }

        @DexIgnore
        public void a(CalibrationActivity calibrationActivity) {
            b(calibrationActivity);
        }

        @DexIgnore
        public final eq3 a(eq3 eq3) {
            hq3.a(eq3);
            return eq3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j0 implements pu2 {
        @DexIgnore
        public av2 a;

        @DexIgnore
        public final qr2 a() {
            return new qr2(d());
        }

        @DexIgnore
        public final kr2 b() {
            return new kr2((UserRepository) x52.this.w.get(), (DeviceRepository) x52.this.T.get(), (en2) x52.this.c.get());
        }

        @DexIgnore
        public final HomePresenter c() {
            HomePresenter a2 = cv2.a(bv2.a(this.a), (en2) x52.this.c.get(), (DeviceRepository) x52.this.T.get(), (PortfolioApp) x52.this.d.get(), x52.this.U(), (j62) x52.this.V.get(), x52.this.l(), a(), (UserUtils) x52.this.U1.get(), d(), b(), (ak3) x52.this.v1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ServerSettingRepository d() {
            return new ServerSettingRepository((ServerSettingDataSource) x52.this.S1.get(), (ServerSettingDataSource) x52.this.T1.get());
        }

        @DexIgnore
        public j0(av2 av2) {
            a(av2);
        }

        @DexIgnore
        public final void a(av2 av2) {
            n44.a(av2);
            this.a = av2;
        }

        @DexIgnore
        public void a(HomeActivity homeActivity) {
            b(homeActivity);
        }

        @DexIgnore
        public final HomePresenter a(HomePresenter homePresenter) {
            dv2.a(homePresenter);
            return homePresenter;
        }

        @DexIgnore
        public final HomeActivity b(HomeActivity homeActivity) {
            dq2.a((BaseActivity) homeActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) homeActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) homeActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) homeActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) homeActivity, new gr2());
            ou2.a(homeActivity, c());
            return homeActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class j1 implements ny2 {
        @DexIgnore
        public oy2 a;

        @DexIgnore
        public final InactivityNudgeTimePresenter a() {
            InactivityNudgeTimePresenter a2 = wx2.a(py2.a(this.a), (RemindersSettingsDatabase) x52.this.C0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final RemindTimePresenter b() {
            RemindTimePresenter a2 = ly2.a(qy2.a(this.a), (RemindersSettingsDatabase) x52.this.C0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public j1(oy2 oy2) {
            a(oy2);
        }

        @DexIgnore
        public final void a(oy2 oy2) {
            n44.a(oy2);
            this.a = oy2;
        }

        @DexIgnore
        public final cy2 b(cy2 cy2) {
            dy2.a(cy2, a());
            dy2.a(cy2, b());
            return cy2;
        }

        @DexIgnore
        public void a(cy2 cy2) {
            b(cy2);
        }

        @DexIgnore
        public final InactivityNudgeTimePresenter a(InactivityNudgeTimePresenter inactivityNudgeTimePresenter) {
            xx2.a(inactivityNudgeTimePresenter);
            return inactivityNudgeTimePresenter;
        }

        @DexIgnore
        public final RemindTimePresenter a(RemindTimePresenter remindTimePresenter) {
            my2.a(remindTimePresenter);
            return remindTimePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k implements te3 {
        @DexIgnore
        public xe3 a;

        @DexIgnore
        public final CaloriesDetailPresenter a() {
            CaloriesDetailPresenter a2 = ze3.a(ye3.a(this.a), (SummariesRepository) x52.this.H.get(), (ActivitiesRepository) x52.this.c0.get(), (UserRepository) x52.this.w.get(), (WorkoutSessionRepository) x52.this.B0.get(), (FitnessDataDao) x52.this.F.get(), (WorkoutDao) x52.this.A0.get(), (FitnessDatabase) x52.this.D.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesDetailActivity b(CaloriesDetailActivity caloriesDetailActivity) {
            dq2.a((BaseActivity) caloriesDetailActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) caloriesDetailActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) caloriesDetailActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) caloriesDetailActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) caloriesDetailActivity, new gr2());
            se3.a(caloriesDetailActivity, a());
            return caloriesDetailActivity;
        }

        @DexIgnore
        public k(xe3 xe3) {
            a(xe3);
        }

        @DexIgnore
        public final void a(xe3 xe3) {
            n44.a(xe3);
            this.a = xe3;
        }

        @DexIgnore
        public void a(CaloriesDetailActivity caloriesDetailActivity) {
            b(caloriesDetailActivity);
        }

        @DexIgnore
        public final CaloriesDetailPresenter a(CaloriesDetailPresenter caloriesDetailPresenter) {
            af3.a(caloriesDetailPresenter);
            return caloriesDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k0 implements fv2 {
        @DexIgnore
        public gv2 a;

        @DexIgnore
        public final HomeAlertsHybridPresenter a() {
            HomeAlertsHybridPresenter a2 = az2.a(hv2.a(this.a), (AlarmHelper) x52.this.C.get(), i(), (AlarmsRepository) x52.this.B.get(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeAlertsPresenter b() {
            HomeAlertsPresenter a2 = xv2.a(iv2.a(this.a), (j62) x52.this.V.get(), (AlarmHelper) x52.this.C.get(), x52.this.y(), new px2(), h(), (NotificationSettingsDatabase) x52.this.f.get(), i(), (AlarmsRepository) x52.this.B.get(), (en2) x52.this.c.get(), (DNDSettingsDatabase) x52.this.e.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDashboardPresenter c() {
            HomeDashboardPresenter a2 = w73.a(jv2.a(this.a), (PortfolioApp) x52.this.d.get(), (DeviceRepository) x52.this.T.get(), x52.this.l(), (SummariesRepository) x52.this.H.get(), (GoalTrackingRepository) x52.this.k0.get(), (SleepSummariesRepository) x52.this.K.get(), (HeartRateSampleRepository) x52.this.x0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter d() {
            HomeDianaCustomizePresenter a2 = e23.a(kv2.a(this.a), x52.this.Y(), x52.this.f(), (DianaPresetRepository) x52.this.p.get(), j(), new wm2(), (CustomizeRealDataRepository) x52.this.s.get(), (UserRepository) x52.this.w.get(), x52.this.a0(), (PortfolioApp) x52.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter e() {
            HomeHybridCustomizePresenter a2 = a63.a((PortfolioApp) x52.this.d.get(), lv2.a(this.a), (MicroAppRepository) x52.this.s0.get(), (HybridPresetRepository) x52.this.Z.get(), k(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final HomeProfilePresenter f() {
            HomeProfilePresenter a2 = pg3.a(mv2.a(this.a), (PortfolioApp) x52.this.d.get(), x52.this.C(), x52.this.V(), (DeviceRepository) x52.this.T.get(), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get(), (SleepSummariesRepository) x52.this.K.get(), x52.this.k());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ig3 g() {
            ig3 a2 = jg3.a(nv2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final iw2 h() {
            return new iw2((NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public final SetAlarms i() {
            return new SetAlarms((PortfolioApp) x52.this.d.get(), (AlarmsRepository) x52.this.B.get());
        }

        @DexIgnore
        public final SetDianaPresetToWatchUseCase j() {
            return new SetDianaPresetToWatchUseCase((DianaPresetRepository) x52.this.p.get(), x52.this.e(), x52.this.X(), x52.this.a0());
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase k() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) x52.this.T.get(), (HybridPresetRepository) x52.this.Z.get(), (MicroAppRepository) x52.this.s0.get(), x52.this.N());
        }

        @DexIgnore
        public k0(gv2 gv2) {
            a(gv2);
        }

        @DexIgnore
        public final void a(gv2 gv2) {
            n44.a(gv2);
            this.a = gv2;
        }

        @DexIgnore
        public void a(wu2 wu2) {
            b(wu2);
        }

        @DexIgnore
        public final HomeDashboardPresenter a(HomeDashboardPresenter homeDashboardPresenter) {
            x73.a(homeDashboardPresenter);
            return homeDashboardPresenter;
        }

        @DexIgnore
        public final HomeDianaCustomizePresenter a(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            f23.a(homeDianaCustomizePresenter);
            return homeDianaCustomizePresenter;
        }

        @DexIgnore
        public final HomeHybridCustomizePresenter a(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            b63.a(homeHybridCustomizePresenter);
            return homeHybridCustomizePresenter;
        }

        @DexIgnore
        public final HomeProfilePresenter a(HomeProfilePresenter homeProfilePresenter) {
            qg3.a(homeProfilePresenter);
            return homeProfilePresenter;
        }

        @DexIgnore
        public final wu2 b(wu2 wu2) {
            yu2.a(wu2, c());
            yu2.a(wu2, d());
            yu2.a(wu2, e());
            yu2.a(wu2, f());
            yu2.a(wu2, b());
            yu2.a(wu2, a());
            yu2.a(wu2, g());
            return wu2;
        }

        @DexIgnore
        public final HomeAlertsPresenter a(HomeAlertsPresenter homeAlertsPresenter) {
            yv2.a(homeAlertsPresenter);
            return homeAlertsPresenter;
        }

        @DexIgnore
        public final HomeAlertsHybridPresenter a(HomeAlertsHybridPresenter homeAlertsHybridPresenter) {
            bz2.a(homeAlertsHybridPresenter);
            return homeAlertsHybridPresenter;
        }

        @DexIgnore
        public final ig3 a(ig3 ig3) {
            kg3.a(ig3);
            return ig3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class k1 implements ch3 {
        @DexIgnore
        public gh3 a;

        @DexIgnore
        public final ih3 a() {
            ih3 a2 = jh3.a(hh3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ReplaceBatteryActivity b(ReplaceBatteryActivity replaceBatteryActivity) {
            dq2.a((BaseActivity) replaceBatteryActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) replaceBatteryActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) replaceBatteryActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) replaceBatteryActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) replaceBatteryActivity, new gr2());
            bh3.a(replaceBatteryActivity, a());
            return replaceBatteryActivity;
        }

        @DexIgnore
        public k1(gh3 gh3) {
            a(gh3);
        }

        @DexIgnore
        public final void a(gh3 gh3) {
            n44.a(gh3);
            this.a = gh3;
        }

        @DexIgnore
        public void a(ReplaceBatteryActivity replaceBatteryActivity) {
            b(replaceBatteryActivity);
        }

        @DexIgnore
        public final ih3 a(ih3 ih3) {
            kh3.a(ih3);
            return ih3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l implements da3 {
        @DexIgnore
        public ka3 a;

        @DexIgnore
        public final CaloriesOverviewDayPresenter a() {
            CaloriesOverviewDayPresenter a2 = ha3.a(la3.a(this.a), (SummariesRepository) x52.this.H.get(), (ActivitiesRepository) x52.this.c0.get(), (WorkoutSessionRepository) x52.this.B0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter b() {
            CaloriesOverviewMonthPresenter a2 = ra3.a(ma3.a(this.a), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter c() {
            CaloriesOverviewWeekPresenter a2 = wa3.a(na3.a(this.a), (UserRepository) x52.this.w.get(), (SummariesRepository) x52.this.H.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public l(ka3 ka3) {
            a(ka3);
        }

        @DexIgnore
        public final CaloriesOverviewFragment b(CaloriesOverviewFragment caloriesOverviewFragment) {
            ja3.a(caloriesOverviewFragment, a());
            ja3.a(caloriesOverviewFragment, c());
            ja3.a(caloriesOverviewFragment, b());
            return caloriesOverviewFragment;
        }

        @DexIgnore
        public final void a(ka3 ka3) {
            n44.a(ka3);
            this.a = ka3;
        }

        @DexIgnore
        public void a(CaloriesOverviewFragment caloriesOverviewFragment) {
            b(caloriesOverviewFragment);
        }

        @DexIgnore
        public final CaloriesOverviewDayPresenter a(CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
            ia3.a(caloriesOverviewDayPresenter);
            return caloriesOverviewDayPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewWeekPresenter a(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
            xa3.a(caloriesOverviewWeekPresenter);
            return caloriesOverviewWeekPresenter;
        }

        @DexIgnore
        public final CaloriesOverviewMonthPresenter a(CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
            sa3.a(caloriesOverviewMonthPresenter);
            return caloriesOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l0 implements j63 {
        @DexIgnore
        public o63 a;

        @DexIgnore
        public final HybridCustomizeEditPresenter a() {
            HybridCustomizeEditPresenter a2 = q63.a(p63.a(this.a), b(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SetHybridPresetToWatchUseCase b() {
            return new SetHybridPresetToWatchUseCase((DeviceRepository) x52.this.T.get(), (HybridPresetRepository) x52.this.Z.get(), (MicroAppRepository) x52.this.s0.get(), x52.this.N());
        }

        @DexIgnore
        public l0(o63 o63) {
            a(o63);
        }

        @DexIgnore
        public final HybridCustomizeEditActivity b(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            dq2.a((BaseActivity) hybridCustomizeEditActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) hybridCustomizeEditActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) hybridCustomizeEditActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) hybridCustomizeEditActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) hybridCustomizeEditActivity, new gr2());
            i63.a(hybridCustomizeEditActivity, a());
            i63.a(hybridCustomizeEditActivity, (j42) x52.this.O1.get());
            return hybridCustomizeEditActivity;
        }

        @DexIgnore
        public final void a(o63 o63) {
            n44.a(o63);
            this.a = o63;
        }

        @DexIgnore
        public void a(HybridCustomizeEditActivity hybridCustomizeEditActivity) {
            b(hybridCustomizeEditActivity);
        }

        @DexIgnore
        public final HybridCustomizeEditPresenter a(HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
            r63.a(hybridCustomizeEditPresenter);
            return hybridCustomizeEditPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class l1 implements z63 {
        @DexIgnore
        public d73 a;

        @DexIgnore
        public final SearchMicroAppPresenter a() {
            SearchMicroAppPresenter a2 = f73.a(e73.a(this.a), (MicroAppRepository) x52.this.s0.get(), (en2) x52.this.c.get(), (PortfolioApp) x52.this.d.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchMicroAppActivity b(SearchMicroAppActivity searchMicroAppActivity) {
            dq2.a((BaseActivity) searchMicroAppActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) searchMicroAppActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) searchMicroAppActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) searchMicroAppActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) searchMicroAppActivity, new gr2());
            y63.a(searchMicroAppActivity, a());
            return searchMicroAppActivity;
        }

        @DexIgnore
        public l1(d73 d73) {
            a(d73);
        }

        @DexIgnore
        public final void a(d73 d73) {
            n44.a(d73);
            this.a = d73;
        }

        @DexIgnore
        public void a(SearchMicroAppActivity searchMicroAppActivity) {
            b(searchMicroAppActivity);
        }

        @DexIgnore
        public final SearchMicroAppPresenter a(SearchMicroAppPresenter searchMicroAppPresenter) {
            g73.a(searchMicroAppPresenter);
            return searchMicroAppPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m implements s23 {
        @DexIgnore
        public d33 a;

        @DexIgnore
        public final CommuteTimeSettingsPresenter a() {
            CommuteTimeSettingsPresenter a2 = f33.a(e33.a(this.a), (en2) x52.this.c.get(), (UserRepository) x52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsActivity b(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            dq2.a((BaseActivity) commuteTimeSettingsActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) commuteTimeSettingsActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) commuteTimeSettingsActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) commuteTimeSettingsActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) commuteTimeSettingsActivity, new gr2());
            r23.a(commuteTimeSettingsActivity, a());
            return commuteTimeSettingsActivity;
        }

        @DexIgnore
        public m(d33 d33) {
            a(d33);
        }

        @DexIgnore
        public final void a(d33 d33) {
            n44.a(d33);
            this.a = d33;
        }

        @DexIgnore
        public void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity) {
            b(commuteTimeSettingsActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsPresenter a(CommuteTimeSettingsPresenter commuteTimeSettingsPresenter) {
            g33.a(commuteTimeSettingsPresenter);
            return commuteTimeSettingsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m0 implements c63 {
        @DexIgnore
        public d63 a;

        @DexIgnore
        public final MicroAppPresenter a() {
            MicroAppPresenter a2 = w63.a(e63.a(this.a), x52.this.b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final m63 b(m63 m63) {
            n63.a(m63, a());
            n63.a(m63, (j42) x52.this.O1.get());
            return m63;
        }

        @DexIgnore
        public m0(d63 d63) {
            a(d63);
        }

        @DexIgnore
        public final void a(d63 d63) {
            n44.a(d63);
            this.a = d63;
        }

        @DexIgnore
        public void a(m63 m63) {
            b(m63);
        }

        @DexIgnore
        public final MicroAppPresenter a(MicroAppPresenter microAppPresenter) {
            x63.a(microAppPresenter);
            return microAppPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class m1 implements i33 {
        @DexIgnore
        public m33 a;

        @DexIgnore
        public final SearchRingPhonePresenter a() {
            SearchRingPhonePresenter a2 = o33.a(n33.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchRingPhoneActivity b(SearchRingPhoneActivity searchRingPhoneActivity) {
            dq2.a((BaseActivity) searchRingPhoneActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) searchRingPhoneActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) searchRingPhoneActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) searchRingPhoneActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) searchRingPhoneActivity, new gr2());
            h33.a(searchRingPhoneActivity, a());
            return searchRingPhoneActivity;
        }

        @DexIgnore
        public m1(m33 m33) {
            a(m33);
        }

        @DexIgnore
        public final void a(m33 m33) {
            n44.a(m33);
            this.a = m33;
        }

        @DexIgnore
        public void a(SearchRingPhoneActivity searchRingPhoneActivity) {
            b(searchRingPhoneActivity);
        }

        @DexIgnore
        public final SearchRingPhonePresenter a(SearchRingPhonePresenter searchRingPhonePresenter) {
            p33.a(searchRingPhonePresenter);
            return searchRingPhonePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n implements w23 {
        @DexIgnore
        public z23 a;

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter a() {
            CommuteTimeSettingsDefaultAddressPresenter a2 = b33.a(a33.a(this.a), (en2) x52.this.c.get(), (UserRepository) x52.this.w.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressActivity b(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            dq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) commuteTimeSettingsDefaultAddressActivity, new gr2());
            v23.a(commuteTimeSettingsDefaultAddressActivity, a());
            return commuteTimeSettingsDefaultAddressActivity;
        }

        @DexIgnore
        public n(z23 z23) {
            a(z23);
        }

        @DexIgnore
        public final void a(z23 z23) {
            n44.a(z23);
            this.a = z23;
        }

        @DexIgnore
        public void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity) {
            b(commuteTimeSettingsDefaultAddressActivity);
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressPresenter a(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
            c33.a(commuteTimeSettingsDefaultAddressPresenter);
            return commuteTimeSettingsDefaultAddressPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n0 implements ck3 {
        @DexIgnore
        public gk3 a;

        @DexIgnore
        public final LoginPresenter a() {
            LoginPresenter a2 = jk3.a(ik3.a(this.a), hk3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final LoginActivity b(LoginActivity loginActivity) {
            dq2.a((BaseActivity) loginActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) loginActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) loginActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) loginActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) loginActivity, new gr2());
            bk3.a(loginActivity, (in2) x52.this.k1.get());
            bk3.a(loginActivity, (jn2) x52.this.l0.get());
            bk3.a(loginActivity, (kn2) x52.this.l1.get());
            bk3.a(loginActivity, (MFLoginWechatManager) x52.this.m1.get());
            bk3.a(loginActivity, a());
            return loginActivity;
        }

        @DexIgnore
        public n0(gk3 gk3) {
            a(gk3);
        }

        @DexIgnore
        public final void a(gk3 gk3) {
            n44.a(gk3);
            this.a = gk3;
        }

        @DexIgnore
        public void a(LoginActivity loginActivity) {
            b(loginActivity);
        }

        @DexIgnore
        public final LoginPresenter a(LoginPresenter loginPresenter) {
            kk3.a(loginPresenter, x52.this.G());
            kk3.a(loginPresenter, x52.this.J());
            kk3.a(loginPresenter, x52.this.n());
            kk3.a(loginPresenter, new tq2());
            kk3.a(loginPresenter, x52.this.l());
            kk3.a(loginPresenter, (UserRepository) x52.this.w.get());
            kk3.a(loginPresenter, (DeviceRepository) x52.this.T.get());
            kk3.a(loginPresenter, (en2) x52.this.c.get());
            kk3.a(loginPresenter, x52.this.p());
            kk3.a(loginPresenter, x52.this.w());
            kk3.a(loginPresenter, (j62) x52.this.V.get());
            kk3.a(loginPresenter, x52.this.u());
            kk3.a(loginPresenter, x52.this.v());
            kk3.a(loginPresenter, x52.this.t());
            kk3.a(loginPresenter, x52.this.r());
            kk3.a(loginPresenter, x52.this.H());
            kk3.a(loginPresenter, (kn2) x52.this.l1.get());
            kk3.a(loginPresenter, x52.this.I());
            kk3.a(loginPresenter, x52.this.L());
            kk3.a(loginPresenter, x52.this.K());
            kk3.a(loginPresenter, x52.this.d());
            kk3.a(loginPresenter, (AnalyticsHelper) x52.this.E0.get());
            kk3.a(loginPresenter, (SummariesRepository) x52.this.H.get());
            kk3.a(loginPresenter, (SleepSummariesRepository) x52.this.K.get());
            kk3.a(loginPresenter, (GoalTrackingRepository) x52.this.k0.get());
            kk3.a(loginPresenter, x52.this.q());
            kk3.a(loginPresenter, x52.this.s());
            kk3.a(loginPresenter, x52.this.B());
            kk3.a(loginPresenter, x52.this.b0());
            kk3.a(loginPresenter, (AlarmsRepository) x52.this.B.get());
            kk3.a(loginPresenter);
            return loginPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class n1 implements r33 {
        @DexIgnore
        public u33 a;

        @DexIgnore
        public final SearchSecondTimezonePresenter a() {
            SearchSecondTimezonePresenter a2 = w33.a(v33.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SearchSecondTimezoneActivity b(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            dq2.a((BaseActivity) searchSecondTimezoneActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) searchSecondTimezoneActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) searchSecondTimezoneActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) searchSecondTimezoneActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) searchSecondTimezoneActivity, new gr2());
            q33.a(searchSecondTimezoneActivity, a());
            return searchSecondTimezoneActivity;
        }

        @DexIgnore
        public n1(u33 u33) {
            a(u33);
        }

        @DexIgnore
        public final void a(u33 u33) {
            n44.a(u33);
            this.a = u33;
        }

        @DexIgnore
        public void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity) {
            b(searchSecondTimezoneActivity);
        }

        @DexIgnore
        public final SearchSecondTimezonePresenter a(SearchSecondTimezonePresenter searchSecondTimezonePresenter) {
            x33.a(searchSecondTimezonePresenter);
            return searchSecondTimezonePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o implements b53 {
        @DexIgnore
        public void a(w43 w43) {
            b(w43);
        }

        @DexIgnore
        public final w43 b(w43 w43) {
            x43.a(w43, (j42) x52.this.O1.get());
            return w43;
        }

        @DexIgnore
        public o(c53 c53) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o0 implements s63 {
        @DexIgnore
        public void a(g63 g63) {
            b(g63);
        }

        @DexIgnore
        public final g63 b(g63 g63) {
            h63.a(g63, (j42) x52.this.O1.get());
            return g63;
        }

        @DexIgnore
        public o0(v63 v63) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class o1 implements fo3 {
        @DexIgnore
        public jo3 a;

        @DexIgnore
        public final SignUpPresenter a() {
            SignUpPresenter a2 = mo3.a(lo3.a(this.a), ko3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SignUpActivity b(SignUpActivity signUpActivity) {
            dq2.a((BaseActivity) signUpActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) signUpActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) signUpActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) signUpActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) signUpActivity, new gr2());
            eo3.a(signUpActivity, a());
            eo3.a(signUpActivity, (in2) x52.this.k1.get());
            eo3.a(signUpActivity, (jn2) x52.this.l0.get());
            eo3.a(signUpActivity, (kn2) x52.this.l1.get());
            eo3.a(signUpActivity, (MFLoginWechatManager) x52.this.m1.get());
            return signUpActivity;
        }

        @DexIgnore
        public o1(jo3 jo3) {
            a(jo3);
        }

        @DexIgnore
        public final void a(jo3 jo3) {
            n44.a(jo3);
            this.a = jo3;
        }

        @DexIgnore
        public void a(SignUpActivity signUpActivity) {
            b(signUpActivity);
        }

        @DexIgnore
        public final SignUpPresenter a(SignUpPresenter signUpPresenter) {
            no3.a(signUpPresenter, x52.this.H());
            no3.a(signUpPresenter, (kn2) x52.this.l1.get());
            no3.a(signUpPresenter, x52.this.I());
            no3.a(signUpPresenter, x52.this.L());
            no3.a(signUpPresenter, x52.this.K());
            no3.a(signUpPresenter, x52.this.J());
            no3.a(signUpPresenter, (UserRepository) x52.this.w.get());
            no3.a(signUpPresenter, (DeviceRepository) x52.this.T.get());
            no3.a(signUpPresenter, (j62) x52.this.V.get());
            no3.a(signUpPresenter, x52.this.u());
            no3.a(signUpPresenter, x52.this.v());
            no3.a(signUpPresenter, x52.this.p());
            no3.a(signUpPresenter, x52.this.w());
            no3.a(signUpPresenter, x52.this.t());
            no3.a(signUpPresenter, x52.this.r());
            no3.a(signUpPresenter, (AlarmsRepository) x52.this.B.get());
            no3.a(signUpPresenter, new tq2());
            no3.a(signUpPresenter, x52.this.l());
            no3.a(signUpPresenter, x52.this.n());
            no3.a(signUpPresenter, (en2) x52.this.c.get());
            no3.a(signUpPresenter, x52.this.c());
            no3.a(signUpPresenter, x52.this.d());
            no3.a(signUpPresenter, (AnalyticsHelper) x52.this.E0.get());
            no3.a(signUpPresenter, x52.this.C());
            no3.a(signUpPresenter, (SummariesRepository) x52.this.H.get());
            no3.a(signUpPresenter, (SleepSummariesRepository) x52.this.K.get());
            no3.a(signUpPresenter, (GoalTrackingRepository) x52.this.k0.get());
            no3.a(signUpPresenter, x52.this.q());
            no3.a(signUpPresenter, x52.this.s());
            no3.a(signUpPresenter, x52.this.O());
            no3.a(signUpPresenter, x52.this.B());
            no3.a(signUpPresenter, x52.this.b0());
            no3.a(signUpPresenter);
            return signUpPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p implements e53 {
        @DexIgnore
        public void a(y43 y43) {
            b(y43);
        }

        @DexIgnore
        public final y43 b(y43 y43) {
            z43.a(y43, (j42) x52.this.O1.get());
            return y43;
        }

        @DexIgnore
        public p(f53 f53) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p0 implements aw2 {
        @DexIgnore
        public ew2 a;

        @DexIgnore
        public final NotificationAppsPresenter a() {
            NotificationAppsPresenter a2 = gw2.a(fw2.a(this.a), (j62) x52.this.V.get(), x52.this.y(), new px2(), b(), (en2) x52.this.c.get(), (NotificationSettingsDatabase) x52.this.f.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final iw2 b() {
            return new iw2((NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public p0(ew2 ew2) {
            a(ew2);
        }

        @DexIgnore
        public final NotificationAppsActivity b(NotificationAppsActivity notificationAppsActivity) {
            dq2.a((BaseActivity) notificationAppsActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationAppsActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationAppsActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationAppsActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationAppsActivity, new gr2());
            zv2.a(notificationAppsActivity, a());
            return notificationAppsActivity;
        }

        @DexIgnore
        public final void a(ew2 ew2) {
            n44.a(ew2);
            this.a = ew2;
        }

        @DexIgnore
        public void a(NotificationAppsActivity notificationAppsActivity) {
            b(notificationAppsActivity);
        }

        @DexIgnore
        public final NotificationAppsPresenter a(NotificationAppsPresenter notificationAppsPresenter) {
            hw2.a(notificationAppsPresenter);
            return notificationAppsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class p1 implements uf3 {
        @DexIgnore
        public yf3 a;

        @DexIgnore
        public final SleepDetailPresenter a() {
            SleepDetailPresenter a2 = ag3.a(zf3.a(this.a), (SleepSummariesRepository) x52.this.K.get(), (SleepSessionsRepository) x52.this.h0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepDetailActivity b(SleepDetailActivity sleepDetailActivity) {
            dq2.a((BaseActivity) sleepDetailActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) sleepDetailActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) sleepDetailActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) sleepDetailActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) sleepDetailActivity, new gr2());
            tf3.a(sleepDetailActivity, a());
            return sleepDetailActivity;
        }

        @DexIgnore
        public p1(yf3 yf3) {
            a(yf3);
        }

        @DexIgnore
        public final void a(yf3 yf3) {
            n44.a(yf3);
            this.a = yf3;
        }

        @DexIgnore
        public void a(SleepDetailActivity sleepDetailActivity) {
            b(sleepDetailActivity);
        }

        @DexIgnore
        public final SleepDetailPresenter a(SleepDetailPresenter sleepDetailPresenter) {
            bg3.a(sleepDetailPresenter);
            return sleepDetailPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q implements z33 {
        @DexIgnore
        public d43 a;

        @DexIgnore
        public final ComplicationSearchPresenter a() {
            ComplicationSearchPresenter a2 = f43.a(e43.a(this.a), x52.this.f(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ComplicationSearchActivity b(ComplicationSearchActivity complicationSearchActivity) {
            dq2.a((BaseActivity) complicationSearchActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) complicationSearchActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) complicationSearchActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) complicationSearchActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) complicationSearchActivity, new gr2());
            y33.a(complicationSearchActivity, a());
            return complicationSearchActivity;
        }

        @DexIgnore
        public q(d43 d43) {
            a(d43);
        }

        @DexIgnore
        public final void a(d43 d43) {
            n44.a(d43);
            this.a = d43;
        }

        @DexIgnore
        public void a(ComplicationSearchActivity complicationSearchActivity) {
            b(complicationSearchActivity);
        }

        @DexIgnore
        public final ComplicationSearchPresenter a(ComplicationSearchPresenter complicationSearchPresenter) {
            g43.a(complicationSearchPresenter);
            return complicationSearchPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q0 implements kw2 {
        @DexIgnore
        public pw2 a;

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter a() {
            NotificationCallsAndMessagesPresenter a2 = rw2.a(qw2.a(this.a), (j62) x52.this.V.get(), new px2(), b(), c(), x52.this.y(), (NotificationSettingsDao) x52.this.P1.get(), (NotificationSettingsDatabase) x52.this.f.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final rx2 b() {
            return new rx2((NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public final sx2 c() {
            return new sx2((NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public q0(pw2 pw2) {
            a(pw2);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesActivity b(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            dq2.a((BaseActivity) notificationCallsAndMessagesActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationCallsAndMessagesActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationCallsAndMessagesActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationCallsAndMessagesActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationCallsAndMessagesActivity, new gr2());
            jw2.a(notificationCallsAndMessagesActivity, a());
            return notificationCallsAndMessagesActivity;
        }

        @DexIgnore
        public final void a(pw2 pw2) {
            n44.a(pw2);
            this.a = pw2;
        }

        @DexIgnore
        public void a(NotificationCallsAndMessagesActivity notificationCallsAndMessagesActivity) {
            b(notificationCallsAndMessagesActivity);
        }

        @DexIgnore
        public final NotificationCallsAndMessagesPresenter a(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            sw2.a(notificationCallsAndMessagesPresenter);
            return notificationCallsAndMessagesPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class q1 implements dd3 {
        @DexIgnore
        public kd3 a;

        @DexIgnore
        public final SleepOverviewDayPresenter a() {
            SleepOverviewDayPresenter a2 = hd3.a(ld3.a(this.a), (SleepSummariesRepository) x52.this.K.get(), (SleepSessionsRepository) x52.this.h0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter b() {
            SleepOverviewMonthPresenter a2 = rd3.a(md3.a(this.a), (UserRepository) x52.this.w.get(), (SleepSummariesRepository) x52.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter c() {
            SleepOverviewWeekPresenter a2 = wd3.a(nd3.a(this.a), (UserRepository) x52.this.w.get(), (SleepSummariesRepository) x52.this.K.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public q1(kd3 kd3) {
            a(kd3);
        }

        @DexIgnore
        public final void a(kd3 kd3) {
            n44.a(kd3);
            this.a = kd3;
        }

        @DexIgnore
        public final SleepOverviewFragment b(SleepOverviewFragment sleepOverviewFragment) {
            jd3.a(sleepOverviewFragment, a());
            jd3.a(sleepOverviewFragment, c());
            jd3.a(sleepOverviewFragment, b());
            return sleepOverviewFragment;
        }

        @DexIgnore
        public void a(SleepOverviewFragment sleepOverviewFragment) {
            b(sleepOverviewFragment);
        }

        @DexIgnore
        public final SleepOverviewDayPresenter a(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            id3.a(sleepOverviewDayPresenter);
            return sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final SleepOverviewWeekPresenter a(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
            xd3.a(sleepOverviewWeekPresenter);
            return sleepOverviewWeekPresenter;
        }

        @DexIgnore
        public final SleepOverviewMonthPresenter a(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            sd3.a(sleepOverviewMonthPresenter);
            return sleepOverviewMonthPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r implements i13 {
        @DexIgnore
        public j13 a;

        @DexIgnore
        public final ComplicationsPresenter a() {
            ComplicationsPresenter a2 = m23.a(k13.a(this.a), x52.this.b());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final CustomizeThemePresenter b() {
            CustomizeThemePresenter a2 = m43.a(l13.a(this.a), x52.this.a0());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppsPresenter c() {
            WatchAppsPresenter a2 = u43.a(m13.a(this.a), x52.this.b(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public r(j13 j13) {
            a(j13);
        }

        @DexIgnore
        public final void a(j13 j13) {
            n44.a(j13);
            this.a = j13;
        }

        @DexIgnore
        public final u13 b(u13 u13) {
            v13.a(u13, a());
            v13.a(u13, c());
            v13.a(u13, b());
            v13.a(u13, (j42) x52.this.O1.get());
            return u13;
        }

        @DexIgnore
        public void a(u13 u13) {
            b(u13);
        }

        @DexIgnore
        public final ComplicationsPresenter a(ComplicationsPresenter complicationsPresenter) {
            n23.a(complicationsPresenter);
            return complicationsPresenter;
        }

        @DexIgnore
        public final WatchAppsPresenter a(WatchAppsPresenter watchAppsPresenter) {
            v43.a(watchAppsPresenter);
            return watchAppsPresenter;
        }

        @DexIgnore
        public final CustomizeThemePresenter a(CustomizeThemePresenter customizeThemePresenter) {
            n43.a(customizeThemePresenter);
            return customizeThemePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r0 implements ez2 {
        @DexIgnore
        public iz2 a;

        @DexIgnore
        public final h03 a() {
            return new h03((Context) x52.this.b.get());
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter b() {
            NotificationContactsAndAppsAssignedPresenter a2 = lz2.a(jz2.a(this.a), kz2.a(this.a), this.a.a(), (j62) x52.this.V.get(), new t03(), a(), c(), d());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final e13 c() {
            return new e13((NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public final vq2 d() {
            return new vq2((NotificationsRepository) x52.this.O.get(), (DeviceRepository) x52.this.T.get());
        }

        @DexIgnore
        public r0(iz2 iz2) {
            a(iz2);
        }

        @DexIgnore
        public final void a(iz2 iz2) {
            n44.a(iz2);
            this.a = iz2;
        }

        @DexIgnore
        public void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            b(notificationContactsAndAppsAssignedActivity);
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedPresenter a(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            mz2.a(notificationContactsAndAppsAssignedPresenter);
            return notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        public final NotificationContactsAndAppsAssignedActivity b(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity) {
            dq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationContactsAndAppsAssignedActivity, new gr2());
            dz2.a(notificationContactsAndAppsAssignedActivity, b());
            return notificationContactsAndAppsAssignedActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class r1 implements yo3 {
        @DexIgnore
        public bp3 a;

        @DexIgnore
        public final SplashPresenter a() {
            SplashPresenter a2 = dp3.a(cp3.a(this.a), (UserRepository) x52.this.w.get(), (MigrationHelper) x52.this.R1.get(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SplashScreenActivity b(SplashScreenActivity splashScreenActivity) {
            dq2.a((BaseActivity) splashScreenActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) splashScreenActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) splashScreenActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) splashScreenActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) splashScreenActivity, new gr2());
            fp3.a(splashScreenActivity, a());
            return splashScreenActivity;
        }

        @DexIgnore
        public r1(bp3 bp3) {
            a(bp3);
        }

        @DexIgnore
        public final void a(bp3 bp3) {
            n44.a(bp3);
            this.a = bp3;
        }

        @DexIgnore
        public void a(SplashScreenActivity splashScreenActivity) {
            b(splashScreenActivity);
        }

        @DexIgnore
        public final SplashPresenter a(SplashPresenter splashPresenter) {
            ep3.a(splashPresenter);
            return splashPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s implements g23 {
        @DexIgnore
        public void a(j23 j23) {
            b(j23);
        }

        @DexIgnore
        public final j23 b(j23 j23) {
            k23.a(j23, (j42) x52.this.O1.get());
            return j23;
        }

        @DexIgnore
        public s(l23 l23) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s0 implements uw2 {
        @DexIgnore
        public yw2 a;

        @DexIgnore
        public final NotificationContactsPresenter a() {
            NotificationContactsPresenter a2 = bx2.a(ax2.a(this.a), (j62) x52.this.V.get(), new px2(), b(), zw2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final sx2 b() {
            return new sx2((NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public s0(yw2 yw2) {
            a(yw2);
        }

        @DexIgnore
        public final NotificationContactsActivity b(NotificationContactsActivity notificationContactsActivity) {
            dq2.a((BaseActivity) notificationContactsActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationContactsActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationContactsActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationContactsActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationContactsActivity, new gr2());
            tw2.a(notificationContactsActivity, a());
            return notificationContactsActivity;
        }

        @DexIgnore
        public final void a(yw2 yw2) {
            n44.a(yw2);
            this.a = yw2;
        }

        @DexIgnore
        public void a(NotificationContactsActivity notificationContactsActivity) {
            b(notificationContactsActivity);
        }

        @DexIgnore
        public final NotificationContactsPresenter a(NotificationContactsPresenter notificationContactsPresenter) {
            cx2.a(notificationContactsPresenter);
            return notificationContactsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class s1 implements hp3 {
        @DexIgnore
        public lp3 a;

        @DexIgnore
        public final np3 a() {
            np3 a2 = op3.a(mp3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final TroubleshootingActivity b(TroubleshootingActivity troubleshootingActivity) {
            dq2.a((BaseActivity) troubleshootingActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) troubleshootingActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) troubleshootingActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) troubleshootingActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) troubleshootingActivity, new gr2());
            gp3.a(troubleshootingActivity, a());
            return troubleshootingActivity;
        }

        @DexIgnore
        public s1(lp3 lp3) {
            a(lp3);
        }

        @DexIgnore
        public final void a(lp3 lp3) {
            n44.a(lp3);
            this.a = lp3;
        }

        @DexIgnore
        public void a(TroubleshootingActivity troubleshootingActivity) {
            b(troubleshootingActivity);
        }

        @DexIgnore
        public final np3 a(np3 np3) {
            pp3.a(np3);
            return np3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t implements hu2 {
        @DexIgnore
        public ku2 a;

        @DexIgnore
        public final ConnectedAppsPresenter a() {
            ConnectedAppsPresenter a2 = mu2.a(lu2.a(this.a), (UserRepository) x52.this.w.get(), (PortfolioApp) x52.this.d.get(), x52.this.E());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final ConnectedAppsActivity b(ConnectedAppsActivity connectedAppsActivity) {
            dq2.a((BaseActivity) connectedAppsActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) connectedAppsActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) connectedAppsActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) connectedAppsActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) connectedAppsActivity, new gr2());
            gu2.a(connectedAppsActivity, a());
            return connectedAppsActivity;
        }

        @DexIgnore
        public t(ku2 ku2) {
            a(ku2);
        }

        @DexIgnore
        public final void a(ku2 ku2) {
            n44.a(ku2);
            this.a = ku2;
        }

        @DexIgnore
        public void a(ConnectedAppsActivity connectedAppsActivity) {
            b(connectedAppsActivity);
        }

        @DexIgnore
        public final ConnectedAppsPresenter a(ConnectedAppsPresenter connectedAppsPresenter) {
            nu2.a(connectedAppsPresenter);
            return connectedAppsPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t0 implements oz2 {
        @DexIgnore
        public sz2 a;

        @DexIgnore
        public final NotificationDialLandingPresenter a() {
            NotificationDialLandingPresenter a2 = vz2.a(uz2.a(this.a), b(), tz2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationsLoader b() {
            return NotificationsLoader_Factory.newNotificationsLoader((Context) x52.this.b.get(), (NotificationsRepository) x52.this.O.get());
        }

        @DexIgnore
        public t0(sz2 sz2) {
            a(sz2);
        }

        @DexIgnore
        public final NotificationDialLandingActivity b(NotificationDialLandingActivity notificationDialLandingActivity) {
            dq2.a((BaseActivity) notificationDialLandingActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationDialLandingActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationDialLandingActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationDialLandingActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationDialLandingActivity, new gr2());
            nz2.a(notificationDialLandingActivity, a());
            return notificationDialLandingActivity;
        }

        @DexIgnore
        public final void a(sz2 sz2) {
            n44.a(sz2);
            this.a = sz2;
        }

        @DexIgnore
        public void a(NotificationDialLandingActivity notificationDialLandingActivity) {
            b(notificationDialLandingActivity);
        }

        @DexIgnore
        public final NotificationDialLandingPresenter a(NotificationDialLandingPresenter notificationDialLandingPresenter) {
            wz2.a(notificationDialLandingPresenter);
            return notificationDialLandingPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class t1 implements kl3 {
        @DexIgnore
        public nl3 a;

        @DexIgnore
        public final DownloadFirmwareByDeviceModelUsecase a() {
            return new DownloadFirmwareByDeviceModelUsecase((PortfolioApp) x52.this.d.get(), (GuestApiService) x52.this.L.get());
        }

        @DexIgnore
        public final UpdateFirmwarePresenter b() {
            UpdateFirmwarePresenter a2 = ql3.a(ol3.a(this.a), (DeviceRepository) x52.this.T.get(), (UserRepository) x52.this.w.get(), x52.this.l(), (en2) x52.this.c.get(), x52.this.U(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public t1(nl3 nl3) {
            a(nl3);
        }

        @DexIgnore
        public final void a(nl3 nl3) {
            n44.a(nl3);
            this.a = nl3;
        }

        @DexIgnore
        public void a(UpdateFirmwareActivity updateFirmwareActivity) {
            b(updateFirmwareActivity);
        }

        @DexIgnore
        public final UpdateFirmwarePresenter a(UpdateFirmwarePresenter updateFirmwarePresenter) {
            rl3.a(updateFirmwarePresenter);
            return updateFirmwarePresenter;
        }

        @DexIgnore
        public final UpdateFirmwareActivity b(UpdateFirmwareActivity updateFirmwareActivity) {
            dq2.a((BaseActivity) updateFirmwareActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) updateFirmwareActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) updateFirmwareActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) updateFirmwareActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) updateFirmwareActivity, new gr2());
            im3.a(updateFirmwareActivity, b());
            return updateFirmwareActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u implements h43 {
        @DexIgnore
        public void a(CustomizeThemeFragment customizeThemeFragment) {
            b(customizeThemeFragment);
        }

        @DexIgnore
        public final CustomizeThemeFragment b(CustomizeThemeFragment customizeThemeFragment) {
            k43.a(customizeThemeFragment, (j42) x52.this.O1.get());
            return customizeThemeFragment;
        }

        @DexIgnore
        public u(l43 l43) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u0 implements yz2 {
        @DexIgnore
        public c03 a;

        @DexIgnore
        public final h03 a() {
            return new h03((Context) x52.this.b.get());
        }

        @DexIgnore
        public final NotificationHybridAppPresenter b() {
            NotificationHybridAppPresenter a2 = f03.a(e03.a(this.a), this.a.a(), d03.a(this.a), (j62) x52.this.V.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public u0(c03 c03) {
            a(c03);
        }

        @DexIgnore
        public final void a(c03 c03) {
            n44.a(c03);
            this.a = c03;
        }

        @DexIgnore
        public void a(NotificationHybridAppActivity notificationHybridAppActivity) {
            b(notificationHybridAppActivity);
        }

        @DexIgnore
        public final NotificationHybridAppPresenter a(NotificationHybridAppPresenter notificationHybridAppPresenter) {
            g03.a(notificationHybridAppPresenter);
            return notificationHybridAppPresenter;
        }

        @DexIgnore
        public final NotificationHybridAppActivity b(NotificationHybridAppActivity notificationHybridAppActivity) {
            dq2.a((BaseActivity) notificationHybridAppActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationHybridAppActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationHybridAppActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationHybridAppActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationHybridAppActivity, new gr2());
            xz2.a(notificationHybridAppActivity, b());
            return notificationHybridAppActivity;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class u1 implements q53 {
        @DexIgnore
        public u53 a;

        @DexIgnore
        public final WatchAppSearchPresenter a() {
            WatchAppSearchPresenter a2 = w53.a(v53.a(this.a), x52.this.Y(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WatchAppSearchActivity b(WatchAppSearchActivity watchAppSearchActivity) {
            dq2.a((BaseActivity) watchAppSearchActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) watchAppSearchActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) watchAppSearchActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) watchAppSearchActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) watchAppSearchActivity, new gr2());
            p53.a(watchAppSearchActivity, a());
            return watchAppSearchActivity;
        }

        @DexIgnore
        public u1(u53 u53) {
            a(u53);
        }

        @DexIgnore
        public final void a(u53 u53) {
            n44.a(u53);
            this.a = u53;
        }

        @DexIgnore
        public void a(WatchAppSearchActivity watchAppSearchActivity) {
            b(watchAppSearchActivity);
        }

        @DexIgnore
        public final WatchAppSearchPresenter a(WatchAppSearchPresenter watchAppSearchPresenter) {
            x53.a(watchAppSearchPresenter);
            return watchAppSearchPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v implements h73 {
        @DexIgnore
        public void a(o13 o13) {
            b(o13);
        }

        @DexIgnore
        public final o13 b(o13 o13) {
            p13.a(o13, (j42) x52.this.O1.get());
            return o13;
        }

        @DexIgnore
        public v(i73 i73) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v0 implements j03 {
        @DexIgnore
        public n03 a;

        @DexIgnore
        public final NotificationHybridContactPresenter a() {
            NotificationHybridContactPresenter a2 = r03.a(q03.a(this.a), this.a.b(), o03.a(this.a), p03.a(this.a), (j62) x52.this.V.get(), new t03());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridContactActivity b(NotificationHybridContactActivity notificationHybridContactActivity) {
            dq2.a((BaseActivity) notificationHybridContactActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationHybridContactActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationHybridContactActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationHybridContactActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationHybridContactActivity, new gr2());
            i03.a(notificationHybridContactActivity, a());
            return notificationHybridContactActivity;
        }

        @DexIgnore
        public v0(n03 n03) {
            a(n03);
        }

        @DexIgnore
        public final void a(n03 n03) {
            n44.a(n03);
            this.a = n03;
        }

        @DexIgnore
        public void a(NotificationHybridContactActivity notificationHybridContactActivity) {
            b(notificationHybridContactActivity);
        }

        @DexIgnore
        public final NotificationHybridContactPresenter a(NotificationHybridContactPresenter notificationHybridContactPresenter) {
            s03.a(notificationHybridContactPresenter);
            return notificationHybridContactPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class v1 implements o43 {
        @DexIgnore
        public void a(r43 r43) {
            b(r43);
        }

        @DexIgnore
        public final r43 b(r43 r43) {
            s43.a(r43, (j42) x52.this.O1.get());
            return r43;
        }

        @DexIgnore
        public v1(t43 t43) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w implements m73 {
        @DexIgnore
        public n73 a;

        @DexIgnore
        public final DashboardActiveTimePresenter a() {
            DashboardActiveTimePresenter a2 = b83.a(o73.a(this.a), (SummariesRepository) x52.this.H.get(), x52.this.x(), (ActivitySummaryDao) x52.this.E.get(), (FitnessDatabase) x52.this.D.get(), (UserRepository) x52.this.w.get(), (h42) x52.this.M.get(), (xk2) x52.this.G.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardActivityPresenter b() {
            DashboardActivityPresenter a2 = b93.a(p73.a(this.a), (SummariesRepository) x52.this.H.get(), x52.this.x(), (ActivitySummaryDao) x52.this.E.get(), (FitnessDatabase) x52.this.D.get(), (UserRepository) x52.this.w.get(), (h42) x52.this.M.get(), (xk2) x52.this.G.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter c() {
            DashboardCaloriesPresenter a2 = ba3.a(q73.a(this.a), (SummariesRepository) x52.this.H.get(), x52.this.x(), (ActivitySummaryDao) x52.this.E.get(), (FitnessDatabase) x52.this.D.get(), (UserRepository) x52.this.w.get(), (h42) x52.this.M.get(), (xk2) x52.this.G.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter d() {
            DashboardGoalTrackingPresenter a2 = bb3.a(r73.a(this.a), (GoalTrackingRepository) x52.this.k0.get(), (UserRepository) x52.this.w.get(), (GoalTrackingDao) x52.this.j0.get(), (GoalTrackingDatabase) x52.this.i0.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter e() {
            DashboardHeartRatePresenter a2 = bc3.a(s73.a(this.a), (HeartRateSummaryRepository) x52.this.z0.get(), x52.this.x(), (HeartRateDailySummaryDao) x52.this.y0.get(), (FitnessDatabase) x52.this.D.get(), (UserRepository) x52.this.w.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final DashboardSleepPresenter f() {
            DashboardSleepPresenter a2 = bd3.a(t73.a(this.a), (SleepSummariesRepository) x52.this.K.get(), (SleepSessionsRepository) x52.this.h0.get(), x52.this.x(), (SleepDao) x52.this.J.get(), (SleepDatabase) x52.this.I.get(), (UserRepository) x52.this.w.get(), (h42) x52.this.M.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public w(n73 n73) {
            a(n73);
        }

        @DexIgnore
        public final void a(n73 n73) {
            n44.a(n73);
            this.a = n73;
        }

        @DexIgnore
        public final su2 b(su2 su2) {
            uu2.a(su2, b());
            uu2.a(su2, a());
            uu2.a(su2, c());
            uu2.a(su2, e());
            uu2.a(su2, f());
            uu2.a(su2, d());
            return su2;
        }

        @DexIgnore
        public void a(su2 su2) {
            b(su2);
        }

        @DexIgnore
        public final DashboardActivityPresenter a(DashboardActivityPresenter dashboardActivityPresenter) {
            c93.a(dashboardActivityPresenter);
            return dashboardActivityPresenter;
        }

        @DexIgnore
        public final DashboardActiveTimePresenter a(DashboardActiveTimePresenter dashboardActiveTimePresenter) {
            c83.a(dashboardActiveTimePresenter);
            return dashboardActiveTimePresenter;
        }

        @DexIgnore
        public final DashboardCaloriesPresenter a(DashboardCaloriesPresenter dashboardCaloriesPresenter) {
            ca3.a(dashboardCaloriesPresenter);
            return dashboardCaloriesPresenter;
        }

        @DexIgnore
        public final DashboardHeartRatePresenter a(DashboardHeartRatePresenter dashboardHeartRatePresenter) {
            cc3.a(dashboardHeartRatePresenter);
            return dashboardHeartRatePresenter;
        }

        @DexIgnore
        public final DashboardSleepPresenter a(DashboardSleepPresenter dashboardSleepPresenter) {
            cd3.a(dashboardSleepPresenter);
            return dashboardSleepPresenter;
        }

        @DexIgnore
        public final DashboardGoalTrackingPresenter a(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter) {
            cb3.a(dashboardGoalTrackingPresenter);
            return dashboardGoalTrackingPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w0 implements v03 {
        @DexIgnore
        public z03 a;

        @DexIgnore
        public final NotificationHybridEveryonePresenter a() {
            NotificationHybridEveryonePresenter a2 = c13.a(b13.a(this.a), this.a.b(), a13.a(this.a), (j62) x52.this.V.get(), new t03());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationHybridEveryoneActivity b(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            dq2.a((BaseActivity) notificationHybridEveryoneActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationHybridEveryoneActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationHybridEveryoneActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationHybridEveryoneActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationHybridEveryoneActivity, new gr2());
            u03.a(notificationHybridEveryoneActivity, a());
            return notificationHybridEveryoneActivity;
        }

        @DexIgnore
        public w0(z03 z03) {
            a(z03);
        }

        @DexIgnore
        public final void a(z03 z03) {
            n44.a(z03);
            this.a = z03;
        }

        @DexIgnore
        public void a(NotificationHybridEveryoneActivity notificationHybridEveryoneActivity) {
            b(notificationHybridEveryoneActivity);
        }

        @DexIgnore
        public final NotificationHybridEveryonePresenter a(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter) {
            d13.a(notificationHybridEveryonePresenter);
            return notificationHybridEveryonePresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class w1 implements sp3 {
        @DexIgnore
        public void a(tp3 tp3) {
            b(tp3);
        }

        @DexIgnore
        public final tp3 b(tp3 tp3) {
            up3.a(tp3, (j42) x52.this.O1.get());
            return tp3;
        }

        @DexIgnore
        public w1(vp3 vp3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x implements oi3 {
        @DexIgnore
        public ri3 a;

        @DexIgnore
        public final DeleteAccountPresenter a() {
            DeleteAccountPresenter a2 = ti3.a(si3.a(this.a), (DeviceRepository) x52.this.T.get(), (AnalyticsHelper) x52.this.E0.get(), b(), x52.this.k());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final kr2 b() {
            return new kr2((UserRepository) x52.this.w.get(), (DeviceRepository) x52.this.T.get(), (en2) x52.this.c.get());
        }

        @DexIgnore
        public x(ri3 ri3) {
            a(ri3);
        }

        @DexIgnore
        public final DeleteAccountActivity b(DeleteAccountActivity deleteAccountActivity) {
            dq2.a((BaseActivity) deleteAccountActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) deleteAccountActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) deleteAccountActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) deleteAccountActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) deleteAccountActivity, new gr2());
            ni3.a(deleteAccountActivity, a());
            return deleteAccountActivity;
        }

        @DexIgnore
        public final void a(ri3 ri3) {
            n44.a(ri3);
            this.a = ri3;
        }

        @DexIgnore
        public void a(DeleteAccountActivity deleteAccountActivity) {
            b(deleteAccountActivity);
        }

        @DexIgnore
        public final DeleteAccountPresenter a(DeleteAccountPresenter deleteAccountPresenter) {
            ui3.a(deleteAccountPresenter);
            return deleteAccountPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x0 implements fx2 {
        @DexIgnore
        public kx2 a;

        @DexIgnore
        public final mx2 a() {
            mx2 a2 = nx2.a(lx2.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public void a(dw2 dw2) {
        }

        @DexIgnore
        public final nw2 b(nw2 nw2) {
            ow2.a(nw2, a());
            ow2.a(nw2, (j42) x52.this.O1.get());
            return nw2;
        }

        @DexIgnore
        public x0(kx2 kx2) {
            a(kx2);
        }

        @DexIgnore
        public final void a(kx2 kx2) {
            n44.a(kx2);
            this.a = kx2;
        }

        @DexIgnore
        public void a(nw2 nw2) {
            b(nw2);
        }

        @DexIgnore
        public final ix2 b(ix2 ix2) {
            jx2.a(ix2, (j42) x52.this.O1.get());
            return ix2;
        }

        @DexIgnore
        public void a(ix2 ix2) {
            b(ix2);
        }

        @DexIgnore
        public final mx2 a(mx2 mx2) {
            ox2.a(mx2);
            return mx2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class x1 implements i53 {
        @DexIgnore
        public l53 a;

        @DexIgnore
        public final WeatherSettingPresenter a() {
            WeatherSettingPresenter a2 = n53.a(m53.a(this.a), (GoogleApiService) x52.this.x1.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WeatherSettingActivity b(WeatherSettingActivity weatherSettingActivity) {
            dq2.a((BaseActivity) weatherSettingActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) weatherSettingActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) weatherSettingActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) weatherSettingActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) weatherSettingActivity, new gr2());
            h53.a(weatherSettingActivity, a());
            return weatherSettingActivity;
        }

        @DexIgnore
        public x1(l53 l53) {
            a(l53);
        }

        @DexIgnore
        public final void a(l53 l53) {
            n44.a(l53);
            this.a = l53;
        }

        @DexIgnore
        public void a(WeatherSettingActivity weatherSettingActivity) {
            b(weatherSettingActivity);
        }

        @DexIgnore
        public final WeatherSettingPresenter a(WeatherSettingPresenter weatherSettingPresenter) {
            o53.a(weatherSettingPresenter);
            return weatherSettingPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y implements r13 {
        @DexIgnore
        public w13 a;

        @DexIgnore
        public final DianaCustomizeEditPresenter a() {
            DianaCustomizeEditPresenter a2 = y13.a(x13.a(this.a), (UserRepository) x52.this.w.get(), this.a.a(), b(), new wm2(), (en2) x52.this.c.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final SetDianaPresetToWatchUseCase b() {
            return new SetDianaPresetToWatchUseCase((DianaPresetRepository) x52.this.p.get(), x52.this.e(), x52.this.X(), x52.this.a0());
        }

        @DexIgnore
        public y(w13 w13) {
            a(w13);
        }

        @DexIgnore
        public final DianaCustomizeEditActivity b(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            dq2.a((BaseActivity) dianaCustomizeEditActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) dianaCustomizeEditActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) dianaCustomizeEditActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) dianaCustomizeEditActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) dianaCustomizeEditActivity, new gr2());
            q13.a(dianaCustomizeEditActivity, a());
            q13.a(dianaCustomizeEditActivity, (j42) x52.this.O1.get());
            return dianaCustomizeEditActivity;
        }

        @DexIgnore
        public final void a(w13 w13) {
            n44.a(w13);
            this.a = w13;
        }

        @DexIgnore
        public void a(DianaCustomizeEditActivity dianaCustomizeEditActivity) {
            b(dianaCustomizeEditActivity);
        }

        @DexIgnore
        public final DianaCustomizeEditPresenter a(DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
            z13.a(dianaCustomizeEditPresenter);
            return dianaCustomizeEditPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y0 implements zx2 {
        @DexIgnore
        public ey2 a;

        @DexIgnore
        public final NotificationWatchRemindersPresenter a() {
            NotificationWatchRemindersPresenter a2 = gy2.a(fy2.a(this.a), (en2) x52.this.c.get(), (RemindersSettingsDatabase) x52.this.C0.get());
            a(a2);
            return a2;
        }

        @DexIgnore
        public final NotificationWatchRemindersActivity b(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            dq2.a((BaseActivity) notificationWatchRemindersActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) notificationWatchRemindersActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) notificationWatchRemindersActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) notificationWatchRemindersActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) notificationWatchRemindersActivity, new gr2());
            yx2.a(notificationWatchRemindersActivity, a());
            return notificationWatchRemindersActivity;
        }

        @DexIgnore
        public y0(ey2 ey2) {
            a(ey2);
        }

        @DexIgnore
        public final void a(ey2 ey2) {
            n44.a(ey2);
            this.a = ey2;
        }

        @DexIgnore
        public void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity) {
            b(notificationWatchRemindersActivity);
        }

        @DexIgnore
        public final NotificationWatchRemindersPresenter a(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            hy2.a(notificationWatchRemindersPresenter);
            return notificationWatchRemindersPresenter;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class y1 implements tq3 {
        @DexIgnore
        public yq3 a;

        @DexIgnore
        public final xq3 a() {
            xq3 a2 = ar3.a(zq3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final WelcomeActivity b(WelcomeActivity welcomeActivity) {
            dq2.a((BaseActivity) welcomeActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) welcomeActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) welcomeActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) welcomeActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) welcomeActivity, new gr2());
            sq3.a(welcomeActivity, a());
            return welcomeActivity;
        }

        @DexIgnore
        public y1(yq3 yq3) {
            a(yq3);
        }

        @DexIgnore
        public final void a(yq3 yq3) {
            n44.a(yq3);
            this.a = yq3;
        }

        @DexIgnore
        public void a(WelcomeActivity welcomeActivity) {
            b(welcomeActivity);
        }

        @DexIgnore
        public final xq3 a(xq3 xq3) {
            br3.a(xq3);
            return xq3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z implements vo3 {
        @DexIgnore
        public wo3 a;

        @DexIgnore
        public final so3 a() {
            so3 a2 = to3.a(xo3.a(this.a));
            a(a2);
            return a2;
        }

        @DexIgnore
        public final VerifyEmailOtp b() {
            return new VerifyEmailOtp((UserRepository) x52.this.w.get());
        }

        @DexIgnore
        public z(wo3 wo3) {
            a(wo3);
        }

        @DexIgnore
        public final EmailOtpVerificationActivity b(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            dq2.a((BaseActivity) emailOtpVerificationActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) emailOtpVerificationActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) emailOtpVerificationActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) emailOtpVerificationActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) emailOtpVerificationActivity, new gr2());
            oo3.a(emailOtpVerificationActivity, a());
            return emailOtpVerificationActivity;
        }

        @DexIgnore
        public final void a(wo3 wo3) {
            n44.a(wo3);
            this.a = wo3;
        }

        @DexIgnore
        public void a(EmailOtpVerificationActivity emailOtpVerificationActivity) {
            b(emailOtpVerificationActivity);
        }

        @DexIgnore
        public final so3 a(so3 so3) {
            uo3.a(so3, x52.this.O());
            uo3.a(so3, b());
            uo3.a(so3);
            return so3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class z0 implements dl3 {
        @DexIgnore
        public gl3 a;

        @DexIgnore
        public final GetRecommendedGoalUseCase a() {
            return new GetRecommendedGoalUseCase((SummariesRepository) x52.this.H.get(), (SleepSummariesRepository) x52.this.K.get(), (SleepSessionsRepository) x52.this.h0.get());
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter b() {
            OnboardingHeightWeightPresenter a2 = il3.a(hl3.a(this.a), (UserRepository) x52.this.w.get(), a());
            a(a2);
            return a2;
        }

        @DexIgnore
        public z0(gl3 gl3) {
            a(gl3);
        }

        @DexIgnore
        public final void a(gl3 gl3) {
            n44.a(gl3);
            this.a = gl3;
        }

        @DexIgnore
        public void a(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            b(onboardingHeightWeightActivity);
        }

        @DexIgnore
        public final OnboardingHeightWeightPresenter a(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter) {
            jl3.a(onboardingHeightWeightPresenter);
            return onboardingHeightWeightPresenter;
        }

        @DexIgnore
        public final OnboardingHeightWeightActivity b(OnboardingHeightWeightActivity onboardingHeightWeightActivity) {
            dq2.a((BaseActivity) onboardingHeightWeightActivity, (UserRepository) x52.this.w.get());
            dq2.a((BaseActivity) onboardingHeightWeightActivity, (en2) x52.this.c.get());
            dq2.a((BaseActivity) onboardingHeightWeightActivity, (DeviceRepository) x52.this.T.get());
            dq2.a((BaseActivity) onboardingHeightWeightActivity, (MigrationManager) x52.this.o1.get());
            dq2.a((BaseActivity) onboardingHeightWeightActivity, new gr2());
            cl3.a(onboardingHeightWeightActivity, b());
            return onboardingHeightWeightActivity;
        }
    }

    @DexIgnore
    public void a(LocaleChangedReceiver localeChangedReceiver) {
    }

    @DexIgnore
    public x52(i iVar) {
        a(iVar);
        b(iVar);
    }

    @DexIgnore
    public static i c0() {
        return new i();
    }

    @DexIgnore
    public final GetHybridDeviceSettingUseCase A() {
        return new GetHybridDeviceSettingUseCase(this.Z.get(), this.s0.get(), this.T.get(), this.O.get(), b(), this.B.get());
    }

    @DexIgnore
    public final GetSecretKeyUseCase B() {
        return new GetSecretKeyUseCase(o(), this.T.get());
    }

    @DexIgnore
    public final nr2 C() {
        return new nr2(this.w.get());
    }

    @DexIgnore
    public final GetWeather D() {
        return new GetWeather(this.n.get());
    }

    @DexIgnore
    public final zk2 E() {
        return a52.a(this.a, this.b.get(), this.M.get(), this.c.get());
    }

    @DexIgnore
    public final HybridSyncUseCase F() {
        return new HybridSyncUseCase(this.s0.get(), this.c.get(), this.T.get(), this.d.get(), this.Z.get(), this.O.get(), this.E0.get(), this.B.get());
    }

    @DexIgnore
    public final LoginEmailUseCase G() {
        return new LoginEmailUseCase(this.w.get());
    }

    @DexIgnore
    public final rr2 H() {
        return new rr2(this.k1.get());
    }

    @DexIgnore
    public final sr2 I() {
        return new sr2(this.l0.get());
    }

    @DexIgnore
    public final LoginSocialUseCase J() {
        return new LoginSocialUseCase(this.w.get());
    }

    @DexIgnore
    public final tr2 K() {
        return new tr2(this.m1.get());
    }

    @DexIgnore
    public final ur2 L() {
        return new ur2(this.l1.get());
    }

    @DexIgnore
    public final Map<Class<? extends ListenableWorker>, Provider<lu3<? extends ListenableWorker>>> M() {
        return ImmutableMap.of(PushPendingDataWorker.class, this.d1);
    }

    @DexIgnore
    public final MicroAppLastSettingRepository N() {
        return new MicroAppLastSettingRepository(this.t0.get());
    }

    @DexIgnore
    public final RequestEmailOtp O() {
        return new RequestEmailOtp(this.w.get());
    }

    @DexIgnore
    public final SetNotificationUseCase P() {
        return new SetNotificationUseCase(y(), new px2(), this.f.get(), this.O.get(), this.T.get(), this.c.get());
    }

    @DexIgnore
    public final SignUpEmailUseCase Q() {
        return new SignUpEmailUseCase(this.w.get(), this.c.get());
    }

    @DexIgnore
    public final SignUpSocialUseCase R() {
        return new SignUpSocialUseCase(this.w.get(), this.c.get());
    }

    @DexIgnore
    public final hr2 S() {
        return ir2.a(this.O.get());
    }

    @DexIgnore
    public final cr2 T() {
        return new cr2(this.T.get(), this.c.get());
    }

    @DexIgnore
    public final UpdateFirmwareUsecase U() {
        return new UpdateFirmwareUsecase(this.T.get(), this.c.get());
    }

    @DexIgnore
    public final UpdateUser V() {
        return new UpdateUser(this.w.get());
    }

    @DexIgnore
    public final VerifySecretKeyUseCase W() {
        return new VerifySecretKeyUseCase(this.T.get(), this.w.get(), j(), this.d.get());
    }

    @DexIgnore
    public final WatchAppLastSettingRepository X() {
        return new WatchAppLastSettingRepository(this.v0.get());
    }

    @DexIgnore
    public final WatchAppRepository Y() {
        return new WatchAppRepository(this.m0.get(), this.n0.get(), this.d.get());
    }

    @DexIgnore
    public final WatchFaceRemoteDataSource Z() {
        return new WatchFaceRemoteDataSource(this.n.get());
    }

    @DexIgnore
    public final CategoryRemoteDataSource a() {
        return new CategoryRemoteDataSource(this.n.get());
    }

    @DexIgnore
    public final WatchFaceRepository a0() {
        return new WatchFaceRepository(this.b.get(), this.L0.get(), Z());
    }

    @DexIgnore
    public final CategoryRepository b() {
        return new CategoryRepository(this.G0.get(), a());
    }

    @DexIgnore
    public final WatchLocalizationRepository b0() {
        return new WatchLocalizationRepository(this.n.get(), this.c.get());
    }

    @DexIgnore
    public final CheckAuthenticationEmailExisting c() {
        return new CheckAuthenticationEmailExisting(this.w.get());
    }

    @DexIgnore
    public final CheckAuthenticationSocialExisting d() {
        return new CheckAuthenticationSocialExisting(this.w.get());
    }

    @DexIgnore
    public final ComplicationLastSettingRepository e() {
        return new ComplicationLastSettingRepository(this.u0.get());
    }

    @DexIgnore
    public final ComplicationRepository f() {
        return new ComplicationRepository(this.o0.get(), this.p0.get(), this.d.get());
    }

    @DexIgnore
    public final xr3 g() {
        xr3 a2 = yr3.a(this.d.get(), this.O.get(), this.T.get(), this.f.get(), this.M.get(), this.U.get(), S(), this.c.get());
        a(a2);
        return a2;
    }

    @DexIgnore
    public final ir3 h() {
        return new ir3(this.p.get(), this.s.get());
    }

    @DexIgnore
    public final ku3 i() {
        return new ku3(M());
    }

    @DexIgnore
    public final kr3 j() {
        return new kr3(this.c.get());
    }

    @DexIgnore
    public final DeleteLogoutUserUseCase k() {
        return new DeleteLogoutUserUseCase(this.w.get(), this.B.get(), this.c.get(), this.Z.get(), this.c0.get(), this.H.get(), this.g0.get(), this.O.get(), this.T.get(), this.h0.get(), this.k0.get(), this.l0.get(), E(), this.K.get(), this.p.get(), Y(), f(), this.f.get(), this.e.get(), this.s0.get(), N(), e(), X(), x(), this.x0.get(), this.z0.get(), this.B0.get(), this.C0.get(), this.D0.get(), this.d.get());
    }

    @DexIgnore
    public final vj2 l() {
        return new vj2(z(), A(), F(), m());
    }

    @DexIgnore
    public final DianaSyncUseCase m() {
        return new DianaSyncUseCase(this.p.get(), this.c.get(), y(), this.d.get(), this.T.get(), this.f.get(), new px2(), W(), U(), this.E0.get(), a0(), this.B.get());
    }

    @DexIgnore
    public final DownloadUserInfoUseCase n() {
        return new DownloadUserInfoUseCase(this.w.get());
    }

    @DexIgnore
    public final mr3 o() {
        return new mr3(this.c.get());
    }

    @DexIgnore
    public final FetchActivities p() {
        return new FetchActivities(this.c0.get(), this.w.get(), x());
    }

    @DexIgnore
    public final FetchDailyGoalTrackingSummaries q() {
        return new FetchDailyGoalTrackingSummaries(this.k0.get(), this.w.get());
    }

    @DexIgnore
    public final FetchDailyHeartRateSummaries r() {
        return new FetchDailyHeartRateSummaries(this.z0.get(), this.w.get(), x());
    }

    @DexIgnore
    public final FetchGoalTrackingData s() {
        return new FetchGoalTrackingData(this.k0.get(), this.w.get());
    }

    @DexIgnore
    public final FetchHeartRateSamples t() {
        return new FetchHeartRateSamples(this.x0.get(), x(), this.w.get());
    }

    @DexIgnore
    public final FetchSleepSessions u() {
        return new FetchSleepSessions(this.h0.get(), this.w.get(), x());
    }

    @DexIgnore
    public final FetchSleepSummaries v() {
        return new FetchSleepSummaries(this.K.get(), this.w.get(), this.h0.get(), x());
    }

    @DexIgnore
    public final FetchSummaries w() {
        return new FetchSummaries(this.H.get(), x(), this.w.get(), this.c0.get());
    }

    @DexIgnore
    public final FitnessDataRepository x() {
        return new FitnessDataRepository(this.F.get(), this.n.get());
    }

    @DexIgnore
    public final vy2 y() {
        return new vy2(this.b.get());
    }

    @DexIgnore
    public final GetDianaDeviceSettingUseCase z() {
        return new GetDianaDeviceSettingUseCase(Y(), f(), this.p.get(), b(), a0(), y(), new px2(), this.f.get(), this.c.get(), b0(), this.B.get());
    }

    @DexIgnore
    public final void a(i iVar) {
        this.b = l44.a(r42.a(iVar.a));
        this.c = l44.a(l52.a(iVar.a, this.b));
        this.d = l44.a(t42.a(iVar.a));
        this.e = l44.a(PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory.create(iVar.b, this.d));
        this.f = l44.a(PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory.create(iVar.b, this.d));
        this.g = l44.a(vp2.a(this.c, this.e, this.f));
        this.h = l44.a(PortfolioDatabaseModule_ProvideDianaCustomizeDatabaseFactory.create(iVar.b, this.d));
        this.i = l44.a(PortfolioDatabaseModule_ProvideDianaPresetDaoFactory.create(iVar.b, this.h));
        this.j = l44.a(k52.a(iVar.a));
        this.k = l44.a(u42.a(iVar.a));
        this.l = l44.a(yo2.a(this.d, this.j, this.k, this.c));
        this.m = l44.a(cp2.a(this.j, this.k, this.c));
        this.n = l44.a(q42.a(iVar.a, this.l, this.m));
        this.o = l44.a(RepositoriesModule_ProvideDianaPresetRemoteDataSourceFactory.create(iVar.c, this.n));
        this.p = l44.a(DianaPresetRepository_Factory.create(this.i, this.o));
        this.q = l44.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDatabaseFactory.create(iVar.b, this.d));
        this.r = l44.a(PortfolioDatabaseModule_ProvideCustomizeRealDataDaoFactory.create(iVar.b, this.q));
        this.s = l44.a(CustomizeRealDataRepository_Factory.create(this.r));
        this.t = l44.a(v42.a(iVar.a, this.l, this.m));
        this.u = l44.a(RepositoriesModule_ProvideUserRemoteDataSourceFactory.create(iVar.c, this.n, this.k, this.t));
        this.v = l44.a(RepositoriesModule_ProvideUserLocalDataSourceFactory.create(iVar.c));
        this.w = l44.a(UserRepository_Factory.create(this.u, this.v, this.c));
        this.x = l44.a(PortfolioDatabaseModule_ProvidesAlarmDatabaseFactory.create(iVar.b, this.d));
        this.y = l44.a(PortfolioDatabaseModule_ProvidesAlarmDaoFactory.create(iVar.b, this.x));
        this.z = l44.a(PortfolioDatabaseModule_ProvideAlarmsLocalDataSourceFactory.create(iVar.b, this.y));
        this.A = l44.a(PortfolioDatabaseModule_ProvideAlarmsRemoteDataSourceFactory.create(iVar.b, this.n));
        this.B = l44.a(AlarmsRepository_Factory.create(this.z, this.A));
        this.C = l44.a(o42.a(iVar.a, this.b, this.c, this.w, this.B));
        this.D = l44.a(PortfolioDatabaseModule_ProvideFitnessDatabaseFactory.create(iVar.b, this.d));
        this.E = l44.a(PortfolioDatabaseModule_ProvideActivitySummaryDaoFactory.create(iVar.b, this.D));
        this.F = l44.a(PortfolioDatabaseModule_ProvideFitnessDataDaoFactory.create(iVar.b, this.D));
        this.G = l44.a(PortfolioDatabaseModule_ProvideFitnessHelperFactory.create(iVar.b, this.c, this.E));
        this.H = l44.a(SummariesRepository_Factory.create(this.n, this.E, this.F, this.G));
        this.I = l44.a(PortfolioDatabaseModule_ProvideSleepDatabaseFactory.create(iVar.b, this.d));
        this.J = l44.a(PortfolioDatabaseModule_ProvideSleepDaoFactory.create(iVar.b, this.I));
        this.K = l44.a(SleepSummariesRepository_Factory.create(this.J, this.n, this.F));
        this.L = l44.a(b52.a(iVar.a));
        this.M = l44.a(i42.a());
        this.N = l44.a(NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory.create(iVar.d));
        this.O = l44.a(NotificationsRepository_Factory.create(this.N));
        this.P = l44.a(PortfolioDatabaseModule_ProvideDeviceDatabaseFactory.create(iVar.b, this.d));
        this.Q = l44.a(PortfolioDatabaseModule_ProvideDeviceDaoFactory.create(iVar.b, this.P));
        this.R = l44.a(PortfolioDatabaseModule_ProvideSkuDaoFactory.create(iVar.b, this.P));
        this.S = DeviceRemoteDataSource_Factory.create(this.n);
        this.T = l44.a(DeviceRepository_Factory.create(this.Q, this.R, this.S));
        this.U = l44.a(w42.a(iVar.a));
        this.V = l44.a(n52.a(iVar.a));
        this.W = l44.a(PortfolioDatabaseModule_ProvideHybridCustomizeDatabaseFactory.create(iVar.b, this.d));
        this.X = l44.a(PortfolioDatabaseModule_ProvidePresetDaoFactory.create(iVar.b, this.W));
        this.Y = HybridPresetRemoteDataSource_Factory.create(this.n);
        this.Z = l44.a(HybridPresetRepository_Factory.create(this.X, this.Y));
        this.a0 = l44.a(PortfolioDatabaseModule_ProvideSampleRawDaoFactory.create(iVar.b, this.D));
        this.b0 = l44.a(PortfolioDatabaseModule_ProvideActivitySampleDaoFactory.create(iVar.b, this.D));
        this.c0 = l44.a(ActivitiesRepository_Factory.create(this.n, this.a0, this.b0, this.D, this.F, this.w, this.G));
        this.d0 = l44.a(m52.a(iVar.a, this.l, this.m));
        this.e0 = l44.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory.create(iVar.e, this.d0, this.M));
        this.f0 = l44.a(MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory.create(iVar.e));
        this.g0 = l44.a(MicroAppSettingRepository_Factory.create(this.e0, this.f0, this.M));
        this.h0 = l44.a(SleepSessionsRepository_Factory.create(this.J, this.n, this.I, this.F));
        this.i0 = l44.a(PortfolioDatabaseModule_ProvideGoalTrackingDatabaseFactory.create(iVar.b, this.d));
        this.j0 = l44.a(PortfolioDatabaseModule_ProvideGoalTrackingDaoFactory.create(iVar.b, this.i0));
        this.k0 = l44.a(GoalTrackingRepository_Factory.create(this.i0, this.j0, this.w, this.c, this.n));
        this.l0 = l44.a(g52.a(iVar.a));
        this.a = iVar.a;
        this.m0 = l44.a(PortfolioDatabaseModule_ProvideWatchAppDaoFactory.create(iVar.b, this.h));
        this.n0 = l44.a(RepositoriesModule_ProvideWatchAppsRemoteDataSourceFactory.create(iVar.c, this.n));
        this.o0 = l44.a(PortfolioDatabaseModule_ProvideComplicationDaoFactory.create(iVar.b, this.h));
        this.p0 = l44.a(RepositoriesModule_ProvideComplicationRemoteDataSourceFactory.create(iVar.c, this.n));
        this.q0 = l44.a(PortfolioDatabaseModule_ProvideMicroAppDaoFactory.create(iVar.b, this.W));
        this.r0 = MicroAppRemoteDataSource_Factory.create(this.n);
        this.s0 = l44.a(MicroAppRepository_Factory.create(this.q0, this.r0, this.d));
        this.t0 = l44.a(PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory.create(iVar.b, this.W));
        this.u0 = l44.a(PortfolioDatabaseModule_ProvideComplicationSettingDaoFactory.create(iVar.b, this.h));
        this.v0 = l44.a(PortfolioDatabaseModule_ProvideWatchAppSettingDaoFactory.create(iVar.b, this.h));
        this.w0 = l44.a(PortfolioDatabaseModule_ProvideHeartRateDaoFactory.create(iVar.b, this.D));
        this.x0 = l44.a(HeartRateSampleRepository_Factory.create(this.w0, this.F, this.n));
        this.y0 = l44.a(PortfolioDatabaseModule_ProvideHeartRateDailySummaryDaoFactory.create(iVar.b, this.D));
        this.z0 = l44.a(HeartRateSummaryRepository_Factory.create(this.y0, this.F, this.n));
        this.A0 = l44.a(PortfolioDatabaseModule_ProvideWorkoutDaoFactory.create(iVar.b, this.D));
        this.B0 = l44.a(WorkoutSessionRepository_Factory.create(this.A0, this.F, this.n));
        this.C0 = l44.a(PortfolioDatabaseModule_ProvideRemindersSettingsDatabaseFactory.create(iVar.b, this.d));
        this.D0 = l44.a(PortfolioDatabaseModule_ProvideThirdPartyDatabaseFactory.create(iVar.b, this.d));
        this.E0 = l44.a(p42.a(iVar.a));
        this.F0 = l44.a(PortfolioDatabaseModule_ProvideCategoryDatabaseFactory.create(iVar.b, this.d));
        this.G0 = l44.a(PortfolioDatabaseModule_ProvideCategoryDaoFactory.create(iVar.b, this.F0));
        this.H0 = CategoryRemoteDataSource_Factory.create(this.n);
        this.I0 = CategoryRepository_Factory.create(this.G0, this.H0);
        this.J0 = WatchAppRepository_Factory.create(this.m0, this.n0, this.d);
        this.K0 = ComplicationRepository_Factory.create(this.o0, this.p0, this.d);
        this.L0 = l44.a(PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory.create(iVar.b, this.h));
        this.M0 = WatchFaceRemoteDataSource_Factory.create(this.n);
        this.N0 = WatchFaceRepository_Factory.create(this.b, this.L0, this.M0);
        this.O0 = wy2.a(this.b);
        this.P0 = WatchLocalizationRepository_Factory.create(this.n, this.c);
        this.Q0 = qn3.a(this.J0, this.K0, this.p, this.I0, this.N0, this.O0, qx2.a(), this.f, this.c, this.P0, this.B);
        this.R0 = rn3.a(this.Z, this.s0, this.T, this.O, this.I0, this.B);
        this.S0 = rq2.a(this.s0, this.c, this.T, this.d, this.Z, this.O, this.E0, this.B);
        this.T0 = lr3.a(this.c);
        this.U0 = qr3.a(this.T, this.w, this.T0, this.d);
        this.V0 = dr2.a(this.T, this.c);
    }

    @DexIgnore
    public final void b(i iVar) {
        this.W0 = qq2.a(this.p, this.c, this.O0, this.d, this.T, this.f, qx2.a(), this.U0, this.V0, this.E0, this.N0, this.B);
        this.X0 = wj2.a(this.Q0, this.R0, this.S0, this.W0);
        this.Y0 = l44.a(s42.a(iVar.a, this.d, this.c, this.Z, (Provider<CategoryRepository>) this.I0, (Provider<WatchAppRepository>) this.J0, (Provider<ComplicationRepository>) this.K0, this.s0, this.p, this.T, this.w, this.B, (Provider<vj2>) this.X0, (Provider<WatchFaceRepository>) this.N0, (Provider<WatchLocalizationRepository>) this.P0));
        this.Z0 = l44.a(ip2.a(this.w));
        this.a1 = FitnessDataRepository_Factory.create(this.F, this.n);
        this.b1 = a52.a(iVar.a, this.b, this.M, this.c);
        this.c1 = l44.a(ThirdPartyRepository_Factory.create(this.b1, this.D0, this.c0, this.d));
        this.d1 = nu3.a(this.c0, this.H, this.h0, this.K, this.k0, this.x0, this.z0, this.a1, this.B, this.c, this.p, this.Z, this.c1, this.d);
        this.e1 = l44.a(ko2.a(this.g, this.c));
        this.f1 = l44.a(mo2.a(this.g, this.c));
        this.g1 = pr3.a(this.O0, qx2.a(), this.f, this.O, this.T, this.c);
        this.h1 = l44.a(e52.a(iVar.a, this.Z, (Provider<vy2>) this.O0, (Provider<px2>) qx2.a(), this.f, this.O, this.T, this.c, this.B, (Provider<SetNotificationUseCase>) this.g1));
        this.i1 = l44.a(q52.a(iVar.a, this.T, this.d));
        this.j1 = l44.a(wp2.a());
        this.k1 = l44.a(x42.a(iVar.a));
        this.l1 = l44.a(s52.a(iVar.a));
        this.m1 = l44.a(r52.a(iVar.a));
        l44.a(UAppSystemVersionRepositoryModule_ProvideUserLocalDataSourceFactory.create(iVar.f));
        this.n1 = MicroAppLastSettingRepository_Factory.create(this.t0);
        this.o1 = l44.a(j52.a(iVar.a, this.c, this.w, (Provider<GetHybridDeviceSettingUseCase>) this.R0, this.O, this.d, this.k0, this.i0, this.Q, this.W, (Provider<MicroAppLastSettingRepository>) this.n1, (Provider<vj2>) this.X0, this.B));
        this.p1 = l44.a(y42.a(iVar.a));
        this.q1 = l44.a(mr2.a());
        this.r1 = l44.a(LocationSource_Factory.create());
        this.s1 = l44.a(PortfolioDatabaseModule_ProvideInAppNotificationDatabaseFactory.create(iVar.b, this.d));
        this.t1 = l44.a(PortfolioDatabaseModule_ProvideInAppNotificationDaoFactory.create(iVar.b, this.s1));
        this.u1 = l44.a(InAppNotificationRepository_Factory.create(this.t1));
        this.v1 = l44.a(c52.a(iVar.a, this.u1));
        this.w1 = l44.a(f52.a(iVar.a));
        this.x1 = l44.a(z42.a(iVar.a, this.l, this.m));
        this.y1 = l44.a(p52.a(iVar.a));
        this.z1 = l44.a(h52.a(iVar.a));
        this.A1 = ComplicationLastSettingRepository_Factory.create(this.u0);
        this.B1 = WatchAppLastSettingRepository_Factory.create(this.v0);
        this.C1 = b23.a(this.p, this.K0, this.A1, this.J0, this.B1, this.N0);
        this.D1 = f63.a(this.Z, this.n1, this.s0);
        this.E1 = pr2.a(this.w);
        this.F1 = or2.a(this.w);
        this.G1 = rh3.a(this.E1, this.F1);
        this.H1 = wq2.a(this.T, this.c);
        this.I1 = br2.a(this.T, this.X0, this.d, this.N0);
        this.J1 = xq2.a(this.w, this.T, this.X0, this.d, this.c);
        this.K1 = xp3.a(this.T, this.H1, this.I1, this.X0, this.c, this.J1, uq2.a(), this.d);
        this.L1 = g53.a(this.c, this.w);
        this.M1 = d53.a(this.c, this.w);
        m44.b a2 = m44.a(8);
        a2.a(DianaCustomizeViewModel.class, this.C1);
        a2.a(HybridCustomizeViewModel.class, this.D1);
        a2.a(ProfileEditViewModel.class, this.G1);
        a2.a(WatchSettingViewModel.class, this.K1);
        a2.a(CommuteTimeWatchAppSettingsViewModel.class, this.L1);
        a2.a(CommuteTimeSettingsDetailViewModel.class, this.M1);
        a2.a(j73.class, k73.a());
        a2.a(dx2.class, ex2.a());
        this.N1 = a2.a();
        this.O1 = l44.a(k42.a(this.N1));
        this.P1 = l44.a(PortfolioDatabaseModule_ProvideNotificationSettingsDaoFactory.create(iVar.b, this.f));
        this.Q1 = l44.a(d52.a(iVar.a, this.c, this.w, this.X, this.O, this.Q, this.d));
        this.R1 = l44.a(i52.a(iVar.a, this.c, this.o1, this.Q1));
        this.S1 = l44.a(RepositoriesModule_ProvideServerSettingLocalDataSourceFactory.create(iVar.c));
        this.T1 = l44.a(RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory.create(iVar.c, this.n));
        this.U1 = l44.a(o52.a(iVar.a));
    }

    @DexIgnore
    public final nn2 b(nn2 nn2) {
        on2.a(nn2, this.c.get());
        on2.a(nn2, h());
        on2.a(nn2, this.C.get());
        return nn2;
    }

    @DexIgnore
    public final PortfolioApp b(PortfolioApp portfolioApp) {
        e62.a(portfolioApp, this.c.get());
        e62.a(portfolioApp, this.w.get());
        e62.a(portfolioApp, this.H.get());
        e62.a(portfolioApp, this.K.get());
        e62.a(portfolioApp, this.C.get());
        e62.a(portfolioApp, this.L.get());
        e62.a(portfolioApp, this.M.get());
        e62.a(portfolioApp, this.n.get());
        e62.a(portfolioApp, g());
        e62.a(portfolioApp, this.V.get());
        e62.a(portfolioApp, k());
        e62.a(portfolioApp, this.E0.get());
        e62.a(portfolioApp, this.Y0.get());
        e62.a(portfolioApp, this.T.get());
        e62.a(portfolioApp, this.G.get());
        e62.a(portfolioApp, this.Z0.get());
        e62.a(portfolioApp, this.p.get());
        e62.a(portfolioApp, i());
        e62.a(portfolioApp, b0());
        e62.a(portfolioApp, new gr3());
        e62.a(portfolioApp, a0());
        e62.a(portfolioApp, this.e1.get());
        e62.a(portfolioApp, this.f1.get());
        return portfolioApp;
    }

    @DexIgnore
    public final DeviceHelper b(DeviceHelper deviceHelper) {
        tk2.a(deviceHelper, this.c.get());
        tk2.a(deviceHelper, this.T.get());
        return deviceHelper;
    }

    @DexIgnore
    public final ql2 b(ql2 ql2) {
        rl2.a(ql2, this.c.get());
        return ql2;
    }

    @DexIgnore
    public final MFDeviceService b(MFDeviceService mFDeviceService) {
        gp2.a(mFDeviceService, this.c.get());
        gp2.a(mFDeviceService, this.T.get());
        gp2.a(mFDeviceService, this.c0.get());
        gp2.a(mFDeviceService, this.H.get());
        gp2.a(mFDeviceService, this.h0.get());
        gp2.a(mFDeviceService, this.K.get());
        gp2.a(mFDeviceService, this.w.get());
        gp2.a(mFDeviceService, this.Z.get());
        gp2.a(mFDeviceService, this.s0.get());
        gp2.a(mFDeviceService, this.x0.get());
        gp2.a(mFDeviceService, this.z0.get());
        gp2.a(mFDeviceService, this.B0.get());
        gp2.a(mFDeviceService, x());
        gp2.a(mFDeviceService, this.k0.get());
        gp2.a(mFDeviceService, this.M.get());
        gp2.a(mFDeviceService, this.E0.get());
        gp2.a(mFDeviceService, this.d.get());
        gp2.a(mFDeviceService, this.h1.get());
        gp2.a(mFDeviceService, this.c1.get());
        gp2.a(mFDeviceService, o());
        gp2.a(mFDeviceService, W());
        gp2.a(mFDeviceService, this.G.get());
        gp2.a(mFDeviceService, this.i1.get());
        return mFDeviceService;
    }

    @DexIgnore
    public final FossilNotificationListenerService b(FossilNotificationListenerService fossilNotificationListenerService) {
        ep2.a(fossilNotificationListenerService, this.g.get());
        ep2.a(fossilNotificationListenerService, this.j1.get());
        ep2.a(fossilNotificationListenerService, this.M.get());
        ep2.a(fossilNotificationListenerService, this.c.get());
        return fossilNotificationListenerService;
    }

    @DexIgnore
    public final AlarmReceiver b(AlarmReceiver alarmReceiver) {
        eo2.a(alarmReceiver, this.w.get());
        eo2.a(alarmReceiver, this.c.get());
        eo2.a(alarmReceiver, this.T.get());
        eo2.a(alarmReceiver, this.C.get());
        eo2.a(alarmReceiver, this.B.get());
        return alarmReceiver;
    }

    @DexIgnore
    public void a(nn2 nn2) {
        b(nn2);
    }

    @DexIgnore
    public void a(PortfolioApp portfolioApp) {
        b(portfolioApp);
    }

    @DexIgnore
    public final NetworkChangedReceiver b(NetworkChangedReceiver networkChangedReceiver) {
        io2.a(networkChangedReceiver, this.w.get());
        return networkChangedReceiver;
    }

    @DexIgnore
    public void a(DeviceHelper deviceHelper) {
        b(deviceHelper);
    }

    @DexIgnore
    public void a(ql2 ql2) {
        b(ql2);
    }

    @DexIgnore
    public void a(MFDeviceService mFDeviceService) {
        b(mFDeviceService);
    }

    @DexIgnore
    public final NotificationReceiver b(NotificationReceiver notificationReceiver) {
        mn2.a(notificationReceiver, this.c.get());
        mn2.a(notificationReceiver, l());
        return notificationReceiver;
    }

    @DexIgnore
    public void a(FossilNotificationListenerService fossilNotificationListenerService) {
        b(fossilNotificationListenerService);
    }

    @DexIgnore
    public void a(AlarmReceiver alarmReceiver) {
        b(alarmReceiver);
    }

    @DexIgnore
    public void a(NetworkChangedReceiver networkChangedReceiver) {
        b(networkChangedReceiver);
    }

    @DexIgnore
    public void a(NotificationReceiver notificationReceiver) {
        b(notificationReceiver);
    }

    @DexIgnore
    public void a(LoginPresenter loginPresenter) {
        b(loginPresenter);
    }

    @DexIgnore
    public final LoginPresenter b(LoginPresenter loginPresenter) {
        kk3.a(loginPresenter, G());
        kk3.a(loginPresenter, J());
        kk3.a(loginPresenter, n());
        kk3.a(loginPresenter, new tq2());
        kk3.a(loginPresenter, l());
        kk3.a(loginPresenter, this.w.get());
        kk3.a(loginPresenter, this.T.get());
        kk3.a(loginPresenter, this.c.get());
        kk3.a(loginPresenter, p());
        kk3.a(loginPresenter, w());
        kk3.a(loginPresenter, this.V.get());
        kk3.a(loginPresenter, u());
        kk3.a(loginPresenter, v());
        kk3.a(loginPresenter, t());
        kk3.a(loginPresenter, r());
        kk3.a(loginPresenter, H());
        kk3.a(loginPresenter, this.l1.get());
        kk3.a(loginPresenter, I());
        kk3.a(loginPresenter, L());
        kk3.a(loginPresenter, K());
        kk3.a(loginPresenter, d());
        kk3.a(loginPresenter, this.E0.get());
        kk3.a(loginPresenter, this.H.get());
        kk3.a(loginPresenter, this.K.get());
        kk3.a(loginPresenter, this.k0.get());
        kk3.a(loginPresenter, q());
        kk3.a(loginPresenter, s());
        kk3.a(loginPresenter, B());
        kk3.a(loginPresenter, b0());
        kk3.a(loginPresenter, this.B.get());
        kk3.a(loginPresenter);
        return loginPresenter;
    }

    @DexIgnore
    public void a(SignUpPresenter signUpPresenter) {
        b(signUpPresenter);
    }

    @DexIgnore
    public void a(ProfileSetupPresenter profileSetupPresenter) {
        b(profileSetupPresenter);
    }

    @DexIgnore
    public void a(BaseActivity baseActivity) {
        b(baseActivity);
    }

    @DexIgnore
    public void a(DebugActivity debugActivity) {
        b(debugActivity);
    }

    @DexIgnore
    public void a(CommuteTimeService commuteTimeService) {
        b(commuteTimeService);
    }

    @DexIgnore
    public void a(BootReceiver bootReceiver) {
        b(bootReceiver);
    }

    @DexIgnore
    public void a(URLRequestTaskHelper uRLRequestTaskHelper) {
        b(uRLRequestTaskHelper);
    }

    @DexIgnore
    public void a(ComplicationWeatherService complicationWeatherService) {
        b(complicationWeatherService);
    }

    @DexIgnore
    public void a(DeviceUtils deviceUtils) {
        b(deviceUtils);
    }

    @DexIgnore
    public void a(nr3 nr3) {
        b(nr3);
    }

    @DexIgnore
    public void a(CloudImageHelper cloudImageHelper) {
        b(cloudImageHelper);
    }

    @DexIgnore
    public void a(UserUtils userUtils) {
        b(userUtils);
    }

    @DexIgnore
    public void a(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService) {
        b(fossilFirebaseInstanceIDService);
    }

    @DexIgnore
    public void a(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        b(fossilFirebaseMessagingService);
    }

    @DexIgnore
    public void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        b(watchAppCommuteTimeManager);
    }

    @DexIgnore
    public void a(AppHelper appHelper) {
        b(appHelper);
    }

    @DexIgnore
    public void a(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        b(appPackageRemoveReceiver);
    }

    @DexIgnore
    public void a(LightAndHapticsManager lightAndHapticsManager) {
        b(lightAndHapticsManager);
    }

    @DexIgnore
    public void a(WeatherManager weatherManager) {
        b(weatherManager);
    }

    @DexIgnore
    public void a(TimeChangeReceiver timeChangeReceiver) {
        b(timeChangeReceiver);
    }

    @DexIgnore
    public zp3 a(cq3 cq3) {
        return new j(cq3);
    }

    @DexIgnore
    public jq3 a(mq3 mq3) {
        return new b0(mq3);
    }

    @DexIgnore
    public kl3 a(nl3 nl3) {
        return new t1(nl3);
    }

    @DexIgnore
    public r13 a(w13 w13) {
        return new y(w13);
    }

    @DexIgnore
    public z33 a(d43 d43) {
        return new q(d43);
    }

    @DexIgnore
    public q53 a(u53 u53) {
        return new u1(u53);
    }

    @DexIgnore
    public je3 a(ne3 ne3) {
        return new e(ne3);
    }

    @DexIgnore
    public te3 a(xe3 xe3) {
        return new k(xe3);
    }

    @DexIgnore
    public ae3 a(ee3 ee3) {
        return new c(ee3);
    }

    @DexIgnore
    public uf3 a(yf3 yf3) {
        return new p1(yf3);
    }

    @DexIgnore
    public cf3 a(gf3 gf3) {
        return new d0(gf3);
    }

    @DexIgnore
    public lf3 a(pf3 pf3) {
        return new f0(pf3);
    }

    @DexIgnore
    public aw2 a(ew2 ew2) {
        return new p0(ew2);
    }

    @DexIgnore
    public kw2 a(pw2 pw2) {
        return new q0(pw2);
    }

    @DexIgnore
    public fx2 a(kx2 kx2) {
        return new x0(kx2);
    }

    @DexIgnore
    public uw2 a(yw2 yw2) {
        return new s0(yw2);
    }

    @DexIgnore
    public qv2 a(vv2 vv2) {
        return new i0(vv2);
    }

    @DexIgnore
    public zx2 a(ey2 ey2) {
        return new y0(ey2);
    }

    @DexIgnore
    public ny2 a(oy2 oy2) {
        return new j1(oy2);
    }

    @DexIgnore
    public d93 a(k93 k93) {
        return new f(k93);
    }

    @DexIgnore
    public dd3 a(kd3 kd3) {
        return new q1(kd3);
    }

    @DexIgnore
    public db3 a(kb3 kb3) {
        return new e0(kb3);
    }

    @DexIgnore
    public da3 a(ka3 ka3) {
        return new l(ka3);
    }

    @DexIgnore
    public d83 a(k83 k83) {
        return new d(k83);
    }

    @DexIgnore
    public dc3 a(kc3 kc3) {
        return new g0(kc3);
    }

    @DexIgnore
    public un3 a(yn3 yn3) {
        return new c1(yn3);
    }

    @DexIgnore
    public th3 a(yh3 yh3) {
        return new g1(yh3);
    }

    @DexIgnore
    public lh3 a(ph3 ph3) {
        return new f1(ph3);
    }

    @DexIgnore
    public e53 a(f53 f53) {
        return new p(f53);
    }

    @DexIgnore
    public b53 a(c53 c53) {
        return new o(c53);
    }

    @DexIgnore
    public pj3 a(sj3 sj3) {
        return new d1(sj3);
    }

    @DexIgnore
    public fi3 a(ji3 ji3) {
        return new h0(ji3);
    }

    @DexIgnore
    public ch3 a(gh3 gh3) {
        return new k1(gh3);
    }

    @DexIgnore
    public sg3 a(wg3 wg3) {
        return new b(wg3);
    }

    @DexIgnore
    public oi3 a(ri3 ri3) {
        return new x(ri3);
    }

    @DexIgnore
    public wi3 a(aj3 aj3) {
        return new h1(aj3);
    }

    @DexIgnore
    public final SignUpPresenter b(SignUpPresenter signUpPresenter) {
        no3.a(signUpPresenter, H());
        no3.a(signUpPresenter, this.l1.get());
        no3.a(signUpPresenter, I());
        no3.a(signUpPresenter, L());
        no3.a(signUpPresenter, K());
        no3.a(signUpPresenter, J());
        no3.a(signUpPresenter, this.w.get());
        no3.a(signUpPresenter, this.T.get());
        no3.a(signUpPresenter, this.V.get());
        no3.a(signUpPresenter, u());
        no3.a(signUpPresenter, v());
        no3.a(signUpPresenter, p());
        no3.a(signUpPresenter, w());
        no3.a(signUpPresenter, t());
        no3.a(signUpPresenter, r());
        no3.a(signUpPresenter, this.B.get());
        no3.a(signUpPresenter, new tq2());
        no3.a(signUpPresenter, l());
        no3.a(signUpPresenter, n());
        no3.a(signUpPresenter, this.c.get());
        no3.a(signUpPresenter, c());
        no3.a(signUpPresenter, d());
        no3.a(signUpPresenter, this.E0.get());
        no3.a(signUpPresenter, C());
        no3.a(signUpPresenter, this.H.get());
        no3.a(signUpPresenter, this.K.get());
        no3.a(signUpPresenter, this.k0.get());
        no3.a(signUpPresenter, q());
        no3.a(signUpPresenter, s());
        no3.a(signUpPresenter, O());
        no3.a(signUpPresenter, B());
        no3.a(signUpPresenter, b0());
        no3.a(signUpPresenter);
        return signUpPresenter;
    }

    @DexIgnore
    public tq3 a(yq3 yq3) {
        return new y1(yq3);
    }

    @DexIgnore
    public oz2 a(sz2 sz2) {
        return new t0(sz2);
    }

    @DexIgnore
    public ez2 a(iz2 iz2) {
        return new r0(iz2);
    }

    @DexIgnore
    public j03 a(n03 n03) {
        return new v0(n03);
    }

    @DexIgnore
    public yz2 a(c03 c03) {
        return new u0(c03);
    }

    @DexIgnore
    public v03 a(z03 z03) {
        return new w0(z03);
    }

    @DexIgnore
    public h73 a(i73 i73) {
        return new v(i73);
    }

    @DexIgnore
    public qm3 a(tm3 tm3) {
        return new b1(tm3);
    }

    @DexIgnore
    public an3 a(hn3 hn3) {
        return new a1(hn3);
    }

    @DexIgnore
    public yo3 a(bp3 bp3) {
        return new r1(bp3);
    }

    @DexIgnore
    public fv2 a(gv2 gv2) {
        return new k0(gv2);
    }

    @DexIgnore
    public pu2 a(av2 av2) {
        return new j0(av2);
    }

    @DexIgnore
    public m73 a(n73 n73) {
        return new w(n73);
    }

    @DexIgnore
    public i13 a(j13 j13) {
        return new r(j13);
    }

    @DexIgnore
    public g23 a(l23 l23) {
        return new s(l23);
    }

    @DexIgnore
    public o43 a(t43 t43) {
        return new v1(t43);
    }

    @DexIgnore
    public h43 a(l43 l43) {
        return new u(l43);
    }

    @DexIgnore
    public s23 a(d33 d33) {
        return new m(d33);
    }

    @DexIgnore
    public w23 a(z23 z23) {
        return new n(z23);
    }

    @DexIgnore
    public r33 a(u33 u33) {
        return new n1(u33);
    }

    @DexIgnore
    public i53 a(l53 l53) {
        return new x1(l53);
    }

    @DexIgnore
    public wt2 a(au2 au2) {
        return new g(au2);
    }

    @DexIgnore
    public hp3 a(lp3 lp3) {
        return new s1(lp3);
    }

    @DexIgnore
    public fo3 a(jo3 jo3) {
        return new o1(jo3);
    }

    @DexIgnore
    public ck3 a(gk3 gk3) {
        return new n0(gk3);
    }

    @DexIgnore
    public uk3 a(xk3 xk3) {
        return new c0(xk3);
    }

    @DexIgnore
    public dl3 a(gl3 gl3) {
        return new z0(gl3);
    }

    @DexIgnore
    public tl3 a(wl3 wl3) {
        return new i1(wl3);
    }

    @DexIgnore
    public am3 a(dm3 dm3) {
        return new h(dm3);
    }

    @DexIgnore
    public mk3 a(pk3 pk3) {
        return new a0(pk3);
    }

    @DexIgnore
    public sp3 a(vp3 vp3) {
        return new w1(vp3);
    }

    @DexIgnore
    public hu2 a(ku2 ku2) {
        return new t(ku2);
    }

    @DexIgnore
    public fj3 a(jj3 jj3) {
        return new e1(jj3);
    }

    @DexIgnore
    public j63 a(o63 o63) {
        return new l0(o63);
    }

    @DexIgnore
    public s63 a(v63 v63) {
        return new o0(v63);
    }

    @DexIgnore
    public c63 a(d63 d63) {
        return new m0(d63);
    }

    @DexIgnore
    public z63 a(d73 d73) {
        return new l1(d73);
    }

    @DexIgnore
    public i33 a(m33 m33) {
        return new m1(m33);
    }

    @DexIgnore
    public vo3 a(wo3 wo3) {
        return new z(wo3);
    }

    @DexIgnore
    public final xr3 a(xr3 xr3) {
        zr3.a(xr3, this.T.get());
        zr3.a(xr3, this.f.get());
        return xr3;
    }

    @DexIgnore
    public final ProfileSetupPresenter b(ProfileSetupPresenter profileSetupPresenter) {
        zl3.a(profileSetupPresenter, Q());
        zl3.a(profileSetupPresenter, R());
        zl3.a(profileSetupPresenter, C());
        zl3.a(profileSetupPresenter, this.E0.get());
        zl3.a(profileSetupPresenter);
        return profileSetupPresenter;
    }

    @DexIgnore
    public final BaseActivity b(BaseActivity baseActivity) {
        dq2.a(baseActivity, this.w.get());
        dq2.a(baseActivity, this.c.get());
        dq2.a(baseActivity, this.T.get());
        dq2.a(baseActivity, this.o1.get());
        dq2.a(baseActivity, new gr2());
        return baseActivity;
    }

    @DexIgnore
    public final DebugActivity b(DebugActivity debugActivity) {
        dq2.a((BaseActivity) debugActivity, this.w.get());
        dq2.a((BaseActivity) debugActivity, this.c.get());
        dq2.a((BaseActivity) debugActivity, this.T.get());
        dq2.a((BaseActivity) debugActivity, this.o1.get());
        dq2.a((BaseActivity) debugActivity, new gr2());
        iq2.a(debugActivity, this.c.get());
        iq2.a(debugActivity, T());
        iq2.a(debugActivity, this.p1.get());
        iq2.a(debugActivity, this.L.get());
        iq2.a(debugActivity, this.p.get());
        iq2.a(debugActivity, this.Z0.get());
        iq2.a(debugActivity, l());
        return debugActivity;
    }

    @DexIgnore
    public final CommuteTimeService b(CommuteTimeService commuteTimeService) {
        qp2.a(commuteTimeService, this.q1.get());
        return commuteTimeService;
    }

    @DexIgnore
    public final BootReceiver b(BootReceiver bootReceiver) {
        ho2.a(bootReceiver, this.C.get());
        return bootReceiver;
    }

    @DexIgnore
    public final URLRequestTaskHelper b(URLRequestTaskHelper uRLRequestTaskHelper) {
        URLRequestTaskHelper_MembersInjector.injectMApiService(uRLRequestTaskHelper, this.n.get());
        return uRLRequestTaskHelper;
    }

    @DexIgnore
    public final ComplicationWeatherService b(ComplicationWeatherService complicationWeatherService) {
        kp2.a(complicationWeatherService, this.r1.get());
        mp2.a(complicationWeatherService, D());
        mp2.a(complicationWeatherService, this.V.get());
        mp2.a(complicationWeatherService, this.c.get());
        mp2.a(complicationWeatherService, this.d.get());
        mp2.a(complicationWeatherService, this.s.get());
        mp2.a(complicationWeatherService, this.w.get());
        return complicationWeatherService;
    }

    @DexIgnore
    public final DeviceUtils b(DeviceUtils deviceUtils) {
        bs3.a(deviceUtils, this.T.get());
        bs3.a(deviceUtils, this.c.get());
        bs3.a(deviceUtils, this.L.get());
        bs3.a(deviceUtils, this.p1.get());
        return deviceUtils;
    }

    @DexIgnore
    public final nr3 b(nr3 nr3) {
        or3.a(nr3, this.c0.get());
        return nr3;
    }

    @DexIgnore
    public final CloudImageHelper b(CloudImageHelper cloudImageHelper) {
        CloudImageHelper_MembersInjector.injectMAppExecutors(cloudImageHelper, this.M.get());
        CloudImageHelper_MembersInjector.injectMApp(cloudImageHelper, this.d.get());
        return cloudImageHelper;
    }

    @DexIgnore
    public final UserUtils b(UserUtils userUtils) {
        ss3.a(userUtils, this.c.get());
        return userUtils;
    }

    @DexIgnore
    public final FossilFirebaseInstanceIDService b(FossilFirebaseInstanceIDService fossilFirebaseInstanceIDService) {
        np2.a(fossilFirebaseInstanceIDService, this.c.get());
        return fossilFirebaseInstanceIDService;
    }

    @DexIgnore
    public final FossilFirebaseMessagingService b(FossilFirebaseMessagingService fossilFirebaseMessagingService) {
        op2.a(fossilFirebaseMessagingService, this.v1.get());
        return fossilFirebaseMessagingService;
    }

    @DexIgnore
    public final WatchAppCommuteTimeManager b(WatchAppCommuteTimeManager watchAppCommuteTimeManager) {
        yp2.a(watchAppCommuteTimeManager, this.d.get());
        yp2.a(watchAppCommuteTimeManager, this.n.get());
        yp2.a(watchAppCommuteTimeManager, this.r1.get());
        yp2.a(watchAppCommuteTimeManager, this.w.get());
        yp2.a(watchAppCommuteTimeManager, this.c.get());
        yp2.a(watchAppCommuteTimeManager, this.w1.get());
        yp2.a(watchAppCommuteTimeManager, this.q1.get());
        yp2.a(watchAppCommuteTimeManager, this.p.get());
        return watchAppCommuteTimeManager;
    }

    @DexIgnore
    public final AppHelper b(AppHelper appHelper) {
        kk2.a(appHelper, this.c.get());
        kk2.a(appHelper, this.T.get());
        kk2.a(appHelper, this.w.get());
        return appHelper;
    }

    @DexIgnore
    public final AppPackageRemoveReceiver b(AppPackageRemoveReceiver appPackageRemoveReceiver) {
        fo2.a(appPackageRemoveReceiver, this.O.get());
        return appPackageRemoveReceiver;
    }

    @DexIgnore
    public final LightAndHapticsManager b(LightAndHapticsManager lightAndHapticsManager) {
        zm2.a(lightAndHapticsManager, this.T.get());
        zm2.a(lightAndHapticsManager, this.c.get());
        return lightAndHapticsManager;
    }

    @DexIgnore
    public final WeatherManager b(WeatherManager weatherManager) {
        hn2.a(weatherManager, this.d.get());
        hn2.a(weatherManager, this.n.get());
        hn2.a(weatherManager, this.r1.get());
        hn2.a(weatherManager, this.w.get());
        hn2.a(weatherManager, this.s.get());
        hn2.a(weatherManager, this.p.get());
        hn2.a(weatherManager, this.x1.get());
        return weatherManager;
    }

    @DexIgnore
    public final TimeChangeReceiver b(TimeChangeReceiver timeChangeReceiver) {
        ou3.a(timeChangeReceiver, this.C.get());
        ou3.a(timeChangeReceiver, this.c.get());
        return timeChangeReceiver;
    }
}
