package com.fossil.blesdk.obfuscated;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c32 extends j32 {
    @DexIgnore
    public b22 a(String str, BarcodeFormat barcodeFormat, int i, int i2, Map<EncodeHintType, ?> map) throws WriterException {
        if (barcodeFormat == BarcodeFormat.EAN_8) {
            return super.a(str, barcodeFormat, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_8, but got " + barcodeFormat);
    }

    @DexIgnore
    public boolean[] a(String str) {
        if (str.length() == 8) {
            boolean[] zArr = new boolean[67];
            int a = g32.a(zArr, 0, i32.a, true) + 0;
            int i = 0;
            while (i <= 3) {
                int i2 = i + 1;
                a += g32.a(zArr, a, i32.d[Integer.parseInt(str.substring(i, i2))], false);
                i = i2;
            }
            int a2 = a + g32.a(zArr, a, i32.b, false);
            int i3 = 4;
            while (i3 <= 7) {
                int i4 = i3 + 1;
                a2 += g32.a(zArr, a2, i32.d[Integer.parseInt(str.substring(i3, i4))], true);
                i3 = i4;
            }
            g32.a(zArr, a2, i32.a, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
