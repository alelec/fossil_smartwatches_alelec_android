package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.model.microapp.enumerate.MicroAppButton;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ja0 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ MicroAppButton b;
    @DexIgnore
    public /* final */ List<ka0> c;

    @DexIgnore
    public ja0(MicroAppButton microAppButton, List<ka0> list) {
        kd4.b(microAppButton, "microAppButton");
        kd4.b(list, "microAppReferenceFrames");
        this.b = microAppButton;
        this.c = list;
        byte[] bArr = new byte[0];
        for (ka0 a2 : this.c) {
            bArr = k90.a(bArr, a2.a());
        }
        ByteBuffer order = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(2 + \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b.getId$blesdk_productionRelease());
        order.put((byte) this.c.size());
        order.put(bArr);
        byte[] array = order.array();
        kd4.a((Object) array, "byteBuffer.array()");
        this.a = array;
    }

    @DexIgnore
    public final byte[] a() {
        return this.a;
    }
}
