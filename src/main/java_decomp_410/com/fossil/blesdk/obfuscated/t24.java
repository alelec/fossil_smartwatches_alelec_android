package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.StatReportStrategy;
import java.lang.Thread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t24 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;

    @DexIgnore
    public t24(Context context) {
        this.e = context;
    }

    @DexIgnore
    public final void run() {
        t04.a(j04.r).h();
        e24.a(this.e, true);
        g14.b(this.e);
        q24.b(this.e);
        Thread.UncaughtExceptionHandler unused = j04.n = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new a14());
        if (h04.o() == StatReportStrategy.APP_LAUNCH) {
            j04.a(this.e, -1);
        }
        if (h04.q()) {
            j04.m.a((Object) "Init MTA StatService success.");
        }
    }
}
