package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uc */
public class C3025uc implements com.fossil.blesdk.obfuscated.C2869sc {

    @DexIgnore
    /* renamed from: a */
    public int f9928a; // = 0;

    @DexIgnore
    /* renamed from: b */
    public int f9929b; // = 0;

    @DexIgnore
    /* renamed from: c */
    public int f9930c; // = 0;

    @DexIgnore
    /* renamed from: d */
    public int f9931d; // = -1;

    @DexIgnore
    /* renamed from: a */
    public int mo16738a() {
        return this.f9929b;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo16739b() {
        int i = this.f9930c;
        int c = mo16740c();
        if (c == 6) {
            i |= 4;
        } else if (c == 7) {
            i |= 1;
        }
        return i & 273;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo16740c() {
        int i = this.f9931d;
        if (i != -1) {
            return i;
        }
        return androidx.media.AudioAttributesCompat.m1248a(false, this.f9930c, this.f9928a);
    }

    @DexIgnore
    /* renamed from: d */
    public int mo16741d() {
        return this.f9928a;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C3025uc)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C3025uc ucVar = (com.fossil.blesdk.obfuscated.C3025uc) obj;
        if (this.f9929b == ucVar.mo16738a() && this.f9930c == ucVar.mo16739b() && this.f9928a == ucVar.mo16741d() && this.f9931d == ucVar.f9931d) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return java.util.Arrays.hashCode(new java.lang.Object[]{java.lang.Integer.valueOf(this.f9929b), java.lang.Integer.valueOf(this.f9930c), java.lang.Integer.valueOf(this.f9928a), java.lang.Integer.valueOf(this.f9931d)});
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("AudioAttributesCompat:");
        if (this.f9931d != -1) {
            sb.append(" stream=");
            sb.append(this.f9931d);
            sb.append(" derived");
        }
        sb.append(" usage=");
        sb.append(androidx.media.AudioAttributesCompat.m1249a(this.f9928a));
        sb.append(" content=");
        sb.append(this.f9929b);
        sb.append(" flags=0x");
        sb.append(java.lang.Integer.toHexString(this.f9930c).toUpperCase());
        return sb.toString();
    }
}
