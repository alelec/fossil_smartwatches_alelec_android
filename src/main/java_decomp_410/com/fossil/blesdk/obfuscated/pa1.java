package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zztv;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pa1 {
    @DexIgnore
    public static /* final */ Class<?> a; // = d();
    @DexIgnore
    public static /* final */ eb1<?, ?> b; // = a(false);
    @DexIgnore
    public static /* final */ eb1<?, ?> c; // = a(true);
    @DexIgnore
    public static /* final */ eb1<?, ?> d; // = new gb1();

    @DexIgnore
    public static void a(Class<?> cls) {
        if (!t81.class.isAssignableFrom(cls)) {
            Class<?> cls2 = a;
            if (cls2 != null && !cls2.isAssignableFrom(cls)) {
                throw new IllegalArgumentException("Message classes must extend GeneratedMessage or GeneratedMessageLite");
            }
        }
    }

    @DexIgnore
    public static void b(int i, List<Float> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzf(i, list, z);
        }
    }

    @DexIgnore
    public static void c(int i, List<Long> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzc(i, list, z);
        }
    }

    @DexIgnore
    public static void d(int i, List<Long> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzd(i, list, z);
        }
    }

    @DexIgnore
    public static void e(int i, List<Long> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzn(i, list, z);
        }
    }

    @DexIgnore
    public static void f(int i, List<Long> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zze(i, list, z);
        }
    }

    @DexIgnore
    public static void g(int i, List<Long> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzl(i, list, z);
        }
    }

    @DexIgnore
    public static void h(int i, List<Integer> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zza(i, list, z);
        }
    }

    @DexIgnore
    public static void i(int i, List<Integer> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzj(i, list, z);
        }
    }

    @DexIgnore
    public static void j(int i, List<Integer> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzm(i, list, z);
        }
    }

    @DexIgnore
    public static void k(int i, List<Integer> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzb(i, list, z);
        }
    }

    @DexIgnore
    public static void l(int i, List<Integer> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzk(i, list, z);
        }
    }

    @DexIgnore
    public static void m(int i, List<Integer> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzh(i, list, z);
        }
    }

    @DexIgnore
    public static void n(int i, List<Boolean> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzi(i, list, z);
        }
    }

    @DexIgnore
    public static void b(int i, List<zzte> list, sb1 sb1) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzb(i, list);
        }
    }

    @DexIgnore
    public static int c(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return a(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int d(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return b(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int e(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return c(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int f(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return d(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int g(int i, List<Integer> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return e(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static int h(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.i(i, 0);
    }

    @DexIgnore
    public static int i(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof m91) {
            m91 m91 = (m91) list;
            i = 0;
            while (i2 < size) {
                i += zztv.d(m91.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.d(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int j(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof m91) {
            m91 m91 = (m91) list;
            i = 0;
            while (i2 < size) {
                i += zztv.e(m91.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.e(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static void a(int i, List<Double> list, sb1 sb1, boolean z) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zzg(i, list, z);
        }
    }

    @DexIgnore
    public static void b(int i, List<?> list, sb1 sb1, na1 na1) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.a(i, list, na1);
        }
    }

    @DexIgnore
    public static int h(List<?> list) {
        return list.size();
    }

    @DexIgnore
    public static void a(int i, List<String> list, sb1 sb1) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.zza(i, list);
        }
    }

    @DexIgnore
    public static int c(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u81) {
            u81 u81 = (u81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.f(u81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.f(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int d(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u81) {
            u81 u81 = (u81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.g(u81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.g(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int e(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u81) {
            u81 u81 = (u81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.h(u81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.h(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int f(List<?> list) {
        return list.size() << 2;
    }

    @DexIgnore
    public static int g(List<?> list) {
        return list.size() << 3;
    }

    @DexIgnore
    public static int b(int i, List<Long> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return j(list) + (size * zztv.e(i));
    }

    @DexIgnore
    public static void a(int i, List<?> list, sb1 sb1, na1 na1) throws IOException {
        if (list != null && !list.isEmpty()) {
            sb1.b(i, list, na1);
        }
    }

    @DexIgnore
    public static int i(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.g(i, 0);
    }

    @DexIgnore
    public static int j(int i, List<?> list, boolean z) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        return size * zztv.b(i, true);
    }

    @DexIgnore
    public static int a(int i, List<Long> list, boolean z) {
        if (list.size() == 0) {
            return 0;
        }
        return i(list) + (list.size() * zztv.e(i));
    }

    @DexIgnore
    public static int b(List<Integer> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof u81) {
            u81 u81 = (u81) list;
            i = 0;
            while (i2 < size) {
                i += zztv.k(u81.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.k(list.get(i2).intValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static eb1<?, ?> c() {
        return d;
    }

    @DexIgnore
    public static Class<?> d() {
        try {
            return Class.forName("com.google.protobuf.GeneratedMessage");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Class<?> e() {
        try {
            return Class.forName("com.google.protobuf.UnknownFieldSetSchema");
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static int a(List<Long> list) {
        int i;
        int size = list.size();
        int i2 = 0;
        if (size == 0) {
            return 0;
        }
        if (list instanceof m91) {
            m91 m91 = (m91) list;
            i = 0;
            while (i2 < size) {
                i += zztv.f(m91.a(i2));
                i2++;
            }
        } else {
            int i3 = 0;
            while (i2 < size) {
                i3 = i + zztv.f(list.get(i2).longValue());
                i2++;
            }
        }
        return i;
    }

    @DexIgnore
    public static int b(int i, List<zzte> list) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = size * zztv.e(i);
        for (int i2 = 0; i2 < list.size(); i2++) {
            e += zztv.a(list.get(i2));
        }
        return e;
    }

    @DexIgnore
    public static int a(int i, List<?> list) {
        int i2;
        int i3;
        int size = list.size();
        int i4 = 0;
        if (size == 0) {
            return 0;
        }
        int e = zztv.e(i) * size;
        if (list instanceof h91) {
            h91 h91 = (h91) list;
            while (i4 < size) {
                Object d2 = h91.d(i4);
                if (d2 instanceof zzte) {
                    i3 = zztv.a((zzte) d2);
                } else {
                    i3 = zztv.a((String) d2);
                }
                e += i3;
                i4++;
            }
        } else {
            while (i4 < size) {
                Object obj = list.get(i4);
                if (obj instanceof zzte) {
                    i2 = zztv.a((zzte) obj);
                } else {
                    i2 = zztv.a((String) obj);
                }
                e += i2;
                i4++;
            }
        }
        return e;
    }

    @DexIgnore
    public static int b(int i, List<w91> list, na1 na1) {
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            i2 += zztv.c(i, list.get(i3), na1);
        }
        return i2;
    }

    @DexIgnore
    public static eb1<?, ?> b() {
        return c;
    }

    @DexIgnore
    public static int a(int i, Object obj, na1 na1) {
        if (obj instanceof f91) {
            return zztv.a(i, (f91) obj);
        }
        return zztv.b(i, (w91) obj, na1);
    }

    @DexIgnore
    public static int a(int i, List<?> list, na1 na1) {
        int i2;
        int size = list.size();
        if (size == 0) {
            return 0;
        }
        int e = zztv.e(i) * size;
        for (int i3 = 0; i3 < size; i3++) {
            Object obj = list.get(i3);
            if (obj instanceof f91) {
                i2 = zztv.a((f91) obj);
            } else {
                i2 = zztv.a((w91) obj, na1);
            }
            e += i2;
        }
        return e;
    }

    @DexIgnore
    public static eb1<?, ?> a() {
        return b;
    }

    @DexIgnore
    public static eb1<?, ?> a(boolean z) {
        try {
            Class<?> e = e();
            if (e == null) {
                return null;
            }
            return (eb1) e.getConstructor(new Class[]{Boolean.TYPE}).newInstance(new Object[]{Boolean.valueOf(z)});
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    @DexIgnore
    public static <T> void a(r91 r91, T t, T t2, long j) {
        kb1.a((Object) t, j, r91.b(kb1.f(t, j), kb1.f(t2, j)));
    }

    @DexIgnore
    public static <T, FT extends o81<FT>> void a(j81<FT> j81, T t, T t2) {
        m81<FT> a2 = j81.a((Object) t2);
        if (!a2.b()) {
            j81.b(t).a(a2);
        }
    }

    @DexIgnore
    public static <T, UT, UB> void a(eb1<UT, UB> eb1, T t, T t2) {
        eb1.a((Object) t, eb1.c(eb1.c(t), eb1.c(t2)));
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, List<Integer> list, y81 y81, UB ub, eb1<UT, UB> eb1) {
        UB ub2;
        if (y81 == null) {
            return ub;
        }
        if (!(list instanceof RandomAccess)) {
            Iterator<Integer> it = list.iterator();
            loop1:
            while (true) {
                ub2 = ub;
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    if (!y81.zzb(intValue)) {
                        ub = a(i, intValue, ub2, eb1);
                        it.remove();
                    }
                }
                break loop1;
            }
        } else {
            int size = list.size();
            ub2 = ub;
            int i2 = 0;
            for (int i3 = 0; i3 < size; i3++) {
                int intValue2 = list.get(i3).intValue();
                if (y81.zzb(intValue2)) {
                    if (i3 != i2) {
                        list.set(i2, Integer.valueOf(intValue2));
                    }
                    i2++;
                } else {
                    ub2 = a(i, intValue2, ub2, eb1);
                }
            }
            if (i2 != size) {
                list.subList(i2, size).clear();
            }
        }
        return ub2;
    }

    @DexIgnore
    public static <UT, UB> UB a(int i, int i2, UB ub, eb1<UT, UB> eb1) {
        if (ub == null) {
            ub = eb1.a();
        }
        eb1.a(ub, i, (long) i2);
        return ub;
    }
}
