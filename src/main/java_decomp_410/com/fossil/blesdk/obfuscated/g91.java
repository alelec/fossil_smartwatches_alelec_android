package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g91 extends m71<String> implements h91, RandomAccess {
    @DexIgnore
    public static /* final */ g91 g;
    @DexIgnore
    public /* final */ List<Object> f;

    /*
    static {
        g91 g91 = new g91();
        g = g91;
        g91.B();
    }
    */

    @DexIgnore
    public g91() {
        this(10);
    }

    @DexIgnore
    public final h91 C() {
        return A() ? new hb1(this) : this;
    }

    @DexIgnore
    public final List<?> E() {
        return Collections.unmodifiableList(this.f);
    }

    @DexIgnore
    public final void a(zzte zzte) {
        a();
        this.f.add(zzte);
        this.modCount++;
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a();
        this.f.add(i, (String) obj);
        this.modCount++;
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @DexIgnore
    public final /* synthetic */ z81 b(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f);
            return new g91((ArrayList<Object>) arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final void clear() {
        a();
        this.f.clear();
        this.modCount++;
    }

    @DexIgnore
    public final Object d(int i) {
        return this.f.get(i);
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        Object obj = this.f.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzte) {
            zzte zzte = (zzte) obj;
            String zzud = zzte.zzud();
            if (zzte.zzue()) {
                this.f.set(i, zzud);
            }
            return zzud;
        }
        byte[] bArr = (byte[]) obj;
        String c = v81.c(bArr);
        if (v81.b(bArr)) {
            this.f.set(i, c);
        }
        return c;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        Object remove = this.f.remove(i);
        this.modCount++;
        return a(remove);
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        a();
        return a(this.f.set(i, (String) obj));
    }

    @DexIgnore
    public final int size() {
        return this.f.size();
    }

    @DexIgnore
    public g91(int i) {
        this((ArrayList<Object>) new ArrayList(i));
    }

    @DexIgnore
    public final boolean addAll(int i, Collection<? extends String> collection) {
        a();
        if (collection instanceof h91) {
            collection = ((h91) collection).E();
        }
        boolean addAll = this.f.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    @DexIgnore
    public g91(ArrayList<Object> arrayList) {
        this.f = arrayList;
    }

    @DexIgnore
    public static String a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzte) {
            return ((zzte) obj).zzud();
        }
        return v81.c((byte[]) obj);
    }
}
