package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class of0 extends ge0 {
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public of0(String str) {
        this.b = str;
    }

    @DexIgnore
    public ud0 a() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public he0<Status> b() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void c() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void d() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public boolean g() {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void a(ge0.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void b(ge0.c cVar) {
        throw new UnsupportedOperationException(this.b);
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        throw new UnsupportedOperationException(this.b);
    }
}
