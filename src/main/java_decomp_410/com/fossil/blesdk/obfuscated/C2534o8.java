package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.o8 */
public final class C2534o8 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2534o8.C2535a f8009a;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.o8$a */
    public interface C2535a {
        @DexIgnore
        /* renamed from: a */
        boolean mo14285a(android.view.MotionEvent motionEvent);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o8$b")
    /* renamed from: com.fossil.blesdk.obfuscated.o8$b */
    public static class C2536b implements com.fossil.blesdk.obfuscated.C2534o8.C2535a {

        @DexIgnore
        /* renamed from: v */
        public static /* final */ int f8010v; // = android.view.ViewConfiguration.getLongPressTimeout();

        @DexIgnore
        /* renamed from: w */
        public static /* final */ int f8011w; // = android.view.ViewConfiguration.getTapTimeout();

        @DexIgnore
        /* renamed from: x */
        public static /* final */ int f8012x; // = android.view.ViewConfiguration.getDoubleTapTimeout();

        @DexIgnore
        /* renamed from: a */
        public int f8013a;

        @DexIgnore
        /* renamed from: b */
        public int f8014b;

        @DexIgnore
        /* renamed from: c */
        public int f8015c;

        @DexIgnore
        /* renamed from: d */
        public int f8016d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.os.Handler f8017e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ android.view.GestureDetector.OnGestureListener f8018f;

        @DexIgnore
        /* renamed from: g */
        public android.view.GestureDetector.OnDoubleTapListener f8019g;

        @DexIgnore
        /* renamed from: h */
        public boolean f8020h;

        @DexIgnore
        /* renamed from: i */
        public boolean f8021i;

        @DexIgnore
        /* renamed from: j */
        public boolean f8022j;

        @DexIgnore
        /* renamed from: k */
        public boolean f8023k;

        @DexIgnore
        /* renamed from: l */
        public boolean f8024l;

        @DexIgnore
        /* renamed from: m */
        public android.view.MotionEvent f8025m;

        @DexIgnore
        /* renamed from: n */
        public android.view.MotionEvent f8026n;

        @DexIgnore
        /* renamed from: o */
        public boolean f8027o;

        @DexIgnore
        /* renamed from: p */
        public float f8028p;

        @DexIgnore
        /* renamed from: q */
        public float f8029q;

        @DexIgnore
        /* renamed from: r */
        public float f8030r;

        @DexIgnore
        /* renamed from: s */
        public float f8031s;

        @DexIgnore
        /* renamed from: t */
        public boolean f8032t;

        @DexIgnore
        /* renamed from: u */
        public android.view.VelocityTracker f8033u;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o8$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.o8$b$a */
        public class C2537a extends android.os.Handler {
            @DexIgnore
            public C2537a() {
            }

            @DexIgnore
            public void handleMessage(android.os.Message message) {
                int i = message.what;
                if (i == 1) {
                    com.fossil.blesdk.obfuscated.C2534o8.C2536b bVar = com.fossil.blesdk.obfuscated.C2534o8.C2536b.this;
                    bVar.f8018f.onShowPress(bVar.f8025m);
                } else if (i == 2) {
                    com.fossil.blesdk.obfuscated.C2534o8.C2536b.this.mo14291c();
                } else if (i == 3) {
                    com.fossil.blesdk.obfuscated.C2534o8.C2536b bVar2 = com.fossil.blesdk.obfuscated.C2534o8.C2536b.this;
                    android.view.GestureDetector.OnDoubleTapListener onDoubleTapListener = bVar2.f8019g;
                    if (onDoubleTapListener == null) {
                        return;
                    }
                    if (!bVar2.f8020h) {
                        onDoubleTapListener.onSingleTapConfirmed(bVar2.f8025m);
                    } else {
                        bVar2.f8021i = true;
                    }
                } else {
                    throw new java.lang.RuntimeException("Unknown message " + message);
                }
            }

            @DexIgnore
            public C2537a(android.os.Handler handler) {
                super(handler.getLooper());
            }
        }

        @DexIgnore
        public C2536b(android.content.Context context, android.view.GestureDetector.OnGestureListener onGestureListener, android.os.Handler handler) {
            if (handler != null) {
                this.f8017e = new com.fossil.blesdk.obfuscated.C2534o8.C2536b.C2537a(handler);
            } else {
                this.f8017e = new com.fossil.blesdk.obfuscated.C2534o8.C2536b.C2537a();
            }
            this.f8018f = onGestureListener;
            if (onGestureListener instanceof android.view.GestureDetector.OnDoubleTapListener) {
                mo14288a((android.view.GestureDetector.OnDoubleTapListener) onGestureListener);
            }
            mo14287a(context);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo14287a(android.content.Context context) {
            if (context == null) {
                throw new java.lang.IllegalArgumentException("Context must not be null");
            } else if (this.f8018f != null) {
                this.f8032t = true;
                android.view.ViewConfiguration viewConfiguration = android.view.ViewConfiguration.get(context);
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                this.f8015c = viewConfiguration.getScaledMinimumFlingVelocity();
                this.f8016d = viewConfiguration.getScaledMaximumFlingVelocity();
                this.f8013a = scaledTouchSlop * scaledTouchSlop;
                this.f8014b = scaledDoubleTapSlop * scaledDoubleTapSlop;
            } else {
                throw new java.lang.IllegalArgumentException("OnGestureListener must not be null");
            }
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo14290b() {
            this.f8017e.removeMessages(1);
            this.f8017e.removeMessages(2);
            this.f8017e.removeMessages(3);
            this.f8027o = false;
            this.f8023k = false;
            this.f8024l = false;
            this.f8021i = false;
            if (this.f8022j) {
                this.f8022j = false;
            }
        }

        @DexIgnore
        /* renamed from: c */
        public void mo14291c() {
            this.f8017e.removeMessages(3);
            this.f8021i = false;
            this.f8022j = true;
            this.f8018f.onLongPress(this.f8025m);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14288a(android.view.GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.f8019g = onDoubleTapListener;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x0208  */
        /* JADX WARNING: Removed duplicated region for block: B:103:0x021f  */
        /* renamed from: a */
        public boolean mo14285a(android.view.MotionEvent motionEvent) {
            boolean z;
            android.view.MotionEvent motionEvent2;
            boolean z2;
            boolean z3;
            int action = motionEvent.getAction();
            if (this.f8033u == null) {
                this.f8033u = android.view.VelocityTracker.obtain();
            }
            this.f8033u.addMovement(motionEvent);
            int i = action & 255;
            boolean z4 = i == 6;
            int actionIndex = z4 ? motionEvent.getActionIndex() : -1;
            int pointerCount = motionEvent.getPointerCount();
            float f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (int i2 = 0; i2 < pointerCount; i2++) {
                if (actionIndex != i2) {
                    f += motionEvent.getX(i2);
                    f2 += motionEvent.getY(i2);
                }
            }
            float f3 = (float) (z4 ? pointerCount - 1 : pointerCount);
            float f4 = f / f3;
            float f5 = f2 / f3;
            if (i != 0) {
                if (i == 1) {
                    this.f8020h = false;
                    android.view.MotionEvent obtain = android.view.MotionEvent.obtain(motionEvent);
                    if (this.f8027o) {
                        z3 = this.f8019g.onDoubleTapEvent(motionEvent) | false;
                    } else {
                        if (this.f8022j) {
                            this.f8017e.removeMessages(3);
                            this.f8022j = false;
                        } else if (this.f8023k) {
                            boolean onSingleTapUp = this.f8018f.onSingleTapUp(motionEvent);
                            if (this.f8021i) {
                                android.view.GestureDetector.OnDoubleTapListener onDoubleTapListener = this.f8019g;
                                if (onDoubleTapListener != null) {
                                    onDoubleTapListener.onSingleTapConfirmed(motionEvent);
                                }
                            }
                            z3 = onSingleTapUp;
                        } else {
                            android.view.VelocityTracker velocityTracker = this.f8033u;
                            int pointerId = motionEvent.getPointerId(0);
                            velocityTracker.computeCurrentVelocity(1000, (float) this.f8016d);
                            float yVelocity = velocityTracker.getYVelocity(pointerId);
                            float xVelocity = velocityTracker.getXVelocity(pointerId);
                            if (java.lang.Math.abs(yVelocity) > ((float) this.f8015c) || java.lang.Math.abs(xVelocity) > ((float) this.f8015c)) {
                                z3 = this.f8018f.onFling(this.f8025m, motionEvent, xVelocity, yVelocity);
                            }
                        }
                        z3 = false;
                    }
                    android.view.MotionEvent motionEvent3 = this.f8026n;
                    if (motionEvent3 != null) {
                        motionEvent3.recycle();
                    }
                    this.f8026n = obtain;
                    android.view.VelocityTracker velocityTracker2 = this.f8033u;
                    if (velocityTracker2 != null) {
                        velocityTracker2.recycle();
                        this.f8033u = null;
                    }
                    this.f8027o = false;
                    this.f8021i = false;
                    this.f8017e.removeMessages(1);
                    this.f8017e.removeMessages(2);
                } else if (i != 2) {
                    if (i == 3) {
                        mo14286a();
                        return false;
                    } else if (i == 5) {
                        this.f8028p = f4;
                        this.f8030r = f4;
                        this.f8029q = f5;
                        this.f8031s = f5;
                        mo14290b();
                        return false;
                    } else if (i != 6) {
                        return false;
                    } else {
                        this.f8028p = f4;
                        this.f8030r = f4;
                        this.f8029q = f5;
                        this.f8031s = f5;
                        this.f8033u.computeCurrentVelocity(1000, (float) this.f8016d);
                        int actionIndex2 = motionEvent.getActionIndex();
                        int pointerId2 = motionEvent.getPointerId(actionIndex2);
                        float xVelocity2 = this.f8033u.getXVelocity(pointerId2);
                        float yVelocity2 = this.f8033u.getYVelocity(pointerId2);
                        for (int i3 = 0; i3 < pointerCount; i3++) {
                            if (i3 != actionIndex2) {
                                int pointerId3 = motionEvent.getPointerId(i3);
                                if ((this.f8033u.getXVelocity(pointerId3) * xVelocity2) + (this.f8033u.getYVelocity(pointerId3) * yVelocity2) < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                    this.f8033u.clear();
                                    return false;
                                }
                            }
                        }
                        return false;
                    }
                } else if (this.f8022j) {
                    return false;
                } else {
                    float f6 = this.f8028p - f4;
                    float f7 = this.f8029q - f5;
                    if (this.f8027o) {
                        return false | this.f8019g.onDoubleTapEvent(motionEvent);
                    }
                    if (this.f8023k) {
                        int i4 = (int) (f4 - this.f8030r);
                        int i5 = (int) (f5 - this.f8031s);
                        int i6 = (i4 * i4) + (i5 * i5);
                        if (i6 > this.f8013a) {
                            z2 = this.f8018f.onScroll(this.f8025m, motionEvent, f6, f7);
                            this.f8028p = f4;
                            this.f8029q = f5;
                            this.f8023k = false;
                            this.f8017e.removeMessages(3);
                            this.f8017e.removeMessages(1);
                            this.f8017e.removeMessages(2);
                        } else {
                            z2 = false;
                        }
                        if (i6 > this.f8013a) {
                            this.f8024l = false;
                        }
                    } else if (java.lang.Math.abs(f6) < 1.0f && java.lang.Math.abs(f7) < 1.0f) {
                        return false;
                    } else {
                        boolean onScroll = this.f8018f.onScroll(this.f8025m, motionEvent, f6, f7);
                        this.f8028p = f4;
                        this.f8029q = f5;
                        return onScroll;
                    }
                }
                return z2;
            }
            if (this.f8019g != null) {
                boolean hasMessages = this.f8017e.hasMessages(3);
                if (hasMessages) {
                    this.f8017e.removeMessages(3);
                }
                android.view.MotionEvent motionEvent4 = this.f8025m;
                if (motionEvent4 != null) {
                    android.view.MotionEvent motionEvent5 = this.f8026n;
                    if (motionEvent5 != null && hasMessages && mo14289a(motionEvent4, motionEvent5, motionEvent)) {
                        this.f8027o = true;
                        z = this.f8019g.onDoubleTap(this.f8025m) | false | this.f8019g.onDoubleTapEvent(motionEvent);
                        this.f8028p = f4;
                        this.f8030r = f4;
                        this.f8029q = f5;
                        this.f8031s = f5;
                        motionEvent2 = this.f8025m;
                        if (motionEvent2 != null) {
                            motionEvent2.recycle();
                        }
                        this.f8025m = android.view.MotionEvent.obtain(motionEvent);
                        this.f8023k = true;
                        this.f8024l = true;
                        this.f8020h = true;
                        this.f8022j = false;
                        this.f8021i = false;
                        if (this.f8032t) {
                            this.f8017e.removeMessages(2);
                            this.f8017e.sendEmptyMessageAtTime(2, this.f8025m.getDownTime() + ((long) f8011w) + ((long) f8010v));
                        }
                        this.f8017e.sendEmptyMessageAtTime(1, this.f8025m.getDownTime() + ((long) f8011w));
                        return z | this.f8018f.onDown(motionEvent);
                    }
                }
                this.f8017e.sendEmptyMessageDelayed(3, (long) f8012x);
            }
            z = false;
            this.f8028p = f4;
            this.f8030r = f4;
            this.f8029q = f5;
            this.f8031s = f5;
            motionEvent2 = this.f8025m;
            if (motionEvent2 != null) {
            }
            this.f8025m = android.view.MotionEvent.obtain(motionEvent);
            this.f8023k = true;
            this.f8024l = true;
            this.f8020h = true;
            this.f8022j = false;
            this.f8021i = false;
            if (this.f8032t) {
            }
            this.f8017e.sendEmptyMessageAtTime(1, this.f8025m.getDownTime() + ((long) f8011w));
            return z | this.f8018f.onDown(motionEvent);
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo14286a() {
            this.f8017e.removeMessages(1);
            this.f8017e.removeMessages(2);
            this.f8017e.removeMessages(3);
            this.f8033u.recycle();
            this.f8033u = null;
            this.f8027o = false;
            this.f8020h = false;
            this.f8023k = false;
            this.f8024l = false;
            this.f8021i = false;
            if (this.f8022j) {
                this.f8022j = false;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo14289a(android.view.MotionEvent motionEvent, android.view.MotionEvent motionEvent2, android.view.MotionEvent motionEvent3) {
            if (!this.f8024l || motionEvent3.getEventTime() - motionEvent2.getEventTime() > ((long) f8012x)) {
                return false;
            }
            int x = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
            int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
            if ((x * x) + (y * y) < this.f8014b) {
                return true;
            }
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o8$c")
    /* renamed from: com.fossil.blesdk.obfuscated.o8$c */
    public static class C2538c implements com.fossil.blesdk.obfuscated.C2534o8.C2535a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.view.GestureDetector f8035a;

        @DexIgnore
        public C2538c(android.content.Context context, android.view.GestureDetector.OnGestureListener onGestureListener, android.os.Handler handler) {
            this.f8035a = new android.view.GestureDetector(context, onGestureListener, handler);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14285a(android.view.MotionEvent motionEvent) {
            return this.f8035a.onTouchEvent(motionEvent);
        }
    }

    @DexIgnore
    public C2534o8(android.content.Context context, android.view.GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, (android.os.Handler) null);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo14284a(android.view.MotionEvent motionEvent) {
        return this.f8009a.mo14285a(motionEvent);
    }

    @DexIgnore
    public C2534o8(android.content.Context context, android.view.GestureDetector.OnGestureListener onGestureListener, android.os.Handler handler) {
        if (android.os.Build.VERSION.SDK_INT > 17) {
            this.f8009a = new com.fossil.blesdk.obfuscated.C2534o8.C2538c(context, onGestureListener, handler);
        } else {
            this.f8009a = new com.fossil.blesdk.obfuscated.C2534o8.C2536b(context, onGestureListener, handler);
        }
    }
}
