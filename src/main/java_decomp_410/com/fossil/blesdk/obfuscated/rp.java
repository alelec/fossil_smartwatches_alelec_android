package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.CallbackException;
import com.bumptech.glide.load.engine.DecodeJob;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.blesdk.obfuscated.vp;
import com.fossil.blesdk.obfuscated.vw;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rp<R> implements DecodeJob.b<R>, vw.f {
    @DexIgnore
    public static /* final */ c C; // = new c();
    @DexIgnore
    public DecodeJob<R> A;
    @DexIgnore
    public volatile boolean B;
    @DexIgnore
    public /* final */ e e;
    @DexIgnore
    public /* final */ xw f;
    @DexIgnore
    public /* final */ vp.a g;
    @DexIgnore
    public /* final */ g8<rp<?>> h;
    @DexIgnore
    public /* final */ c i;
    @DexIgnore
    public /* final */ sp j;
    @DexIgnore
    public /* final */ dr k;
    @DexIgnore
    public /* final */ dr l;
    @DexIgnore
    public /* final */ dr m;
    @DexIgnore
    public /* final */ dr n;
    @DexIgnore
    public /* final */ AtomicInteger o;
    @DexIgnore
    public jo p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public aq<?> u;
    @DexIgnore
    public DataSource v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public GlideException x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public vp<?> z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ sv e;

        @DexIgnore
        public a(sv svVar) {
            this.e = svVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.e.a()) {
                synchronized (rp.this) {
                    if (rp.this.e.a(this.e)) {
                        rp.this.a(this.e);
                    }
                    rp.this.b();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ sv e;

        @DexIgnore
        public b(sv svVar) {
            this.e = svVar;
        }

        @DexIgnore
        public void run() {
            synchronized (this.e.a()) {
                synchronized (rp.this) {
                    if (rp.this.e.a(this.e)) {
                        rp.this.z.d();
                        rp.this.b(this.e);
                        rp.this.c(this.e);
                    }
                    rp.this.b();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <R> vp<R> a(aq<R> aqVar, boolean z, jo joVar, vp.a aVar) {
            return new vp(aqVar, z, true, joVar, aVar);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ sv a;
        @DexIgnore
        public /* final */ Executor b;

        @DexIgnore
        public d(sv svVar, Executor executor) {
            this.a = svVar;
            this.b = executor;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof d) {
                return this.a.equals(((d) obj).a);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Iterable<d> {
        @DexIgnore
        public /* final */ List<d> e;

        @DexIgnore
        public e() {
            this(new ArrayList(2));
        }

        @DexIgnore
        public static d c(sv svVar) {
            return new d(svVar, ow.a());
        }

        @DexIgnore
        public void a(sv svVar, Executor executor) {
            this.e.add(new d(svVar, executor));
        }

        @DexIgnore
        public void b(sv svVar) {
            this.e.remove(c(svVar));
        }

        @DexIgnore
        public void clear() {
            this.e.clear();
        }

        @DexIgnore
        public boolean isEmpty() {
            return this.e.isEmpty();
        }

        @DexIgnore
        public Iterator<d> iterator() {
            return this.e.iterator();
        }

        @DexIgnore
        public int size() {
            return this.e.size();
        }

        @DexIgnore
        public e(List<d> list) {
            this.e = list;
        }

        @DexIgnore
        public boolean a(sv svVar) {
            return this.e.contains(c(svVar));
        }

        @DexIgnore
        public e a() {
            return new e(new ArrayList(this.e));
        }
    }

    @DexIgnore
    public rp(dr drVar, dr drVar2, dr drVar3, dr drVar4, sp spVar, vp.a aVar, g8<rp<?>> g8Var) {
        this(drVar, drVar2, drVar3, drVar4, spVar, aVar, g8Var, C);
    }

    @DexIgnore
    public synchronized rp<R> a(jo joVar, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.p = joVar;
        this.q = z2;
        this.r = z3;
        this.s = z4;
        this.t = z5;
        return this;
    }

    @DexIgnore
    public synchronized void b(DecodeJob<R> decodeJob) {
        this.A = decodeJob;
        (decodeJob.u() ? this.k : c()).execute(decodeJob);
    }

    @DexIgnore
    public synchronized void c(sv svVar) {
        boolean z2;
        this.f.a();
        this.e.b(svVar);
        if (this.e.isEmpty()) {
            a();
            if (!this.w) {
                if (!this.y) {
                    z2 = false;
                    if (z2 && this.o.get() == 0) {
                        h();
                    }
                }
            }
            z2 = true;
            h();
        }
    }

    @DexIgnore
    public final boolean d() {
        return this.y || this.w || this.B;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.j.a(r4, r1, (com.fossil.blesdk.obfuscated.vp<?>) null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.blesdk.obfuscated.rp.a(r4, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    @DexIgnore
    public void e() {
        synchronized (this) {
            this.f.a();
            if (this.B) {
                h();
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("Received an exception without any callbacks to notify");
            } else if (!this.y) {
                this.y = true;
                jo joVar = this.p;
                e a2 = this.e.a();
                a(a2.size() + 1);
            } else {
                throw new IllegalStateException("Already failed once");
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r5.j.a(r5, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if (r0.hasNext() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0055, code lost:
        r1 = r0.next();
        r1.b.execute(new com.fossil.blesdk.obfuscated.rp.b(r5, r1.a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0068, code lost:
        b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        return;
     */
    @DexIgnore
    public void f() {
        synchronized (this) {
            this.f.a();
            if (this.B) {
                this.u.a();
                h();
            } else if (this.e.isEmpty()) {
                throw new IllegalStateException("Received a resource without any callbacks to notify");
            } else if (!this.w) {
                this.z = this.i.a(this.u, this.q, this.p, this.g);
                this.w = true;
                e a2 = this.e.a();
                a(a2.size() + 1);
                jo joVar = this.p;
                vp<?> vpVar = this.z;
            } else {
                throw new IllegalStateException("Already have resource");
            }
        }
    }

    @DexIgnore
    public boolean g() {
        return this.t;
    }

    @DexIgnore
    public final synchronized void h() {
        if (this.p != null) {
            this.e.clear();
            this.p = null;
            this.z = null;
            this.u = null;
            this.y = false;
            this.B = false;
            this.w = false;
            this.A.a(false);
            this.A = null;
            this.x = null;
            this.v = null;
            this.h.a(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public xw i() {
        return this.f;
    }

    @DexIgnore
    public rp(dr drVar, dr drVar2, dr drVar3, dr drVar4, sp spVar, vp.a aVar, g8<rp<?>> g8Var, c cVar) {
        this.e = new e();
        this.f = xw.b();
        this.o = new AtomicInteger();
        this.k = drVar;
        this.l = drVar2;
        this.m = drVar3;
        this.n = drVar4;
        this.j = spVar;
        this.g = aVar;
        this.h = g8Var;
        this.i = cVar;
    }

    @DexIgnore
    public void b(sv svVar) {
        try {
            svVar.a(this.z, this.v);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    @DexIgnore
    public synchronized void a(sv svVar, Executor executor) {
        this.f.a();
        this.e.a(svVar, executor);
        boolean z2 = true;
        if (this.w) {
            a(1);
            executor.execute(new b(svVar));
        } else if (this.y) {
            a(1);
            executor.execute(new a(svVar));
        } else {
            if (this.B) {
                z2 = false;
            }
            tw.a(z2, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    @DexIgnore
    public void b() {
        vp<?> vpVar;
        synchronized (this) {
            this.f.a();
            tw.a(d(), "Not yet complete!");
            int decrementAndGet = this.o.decrementAndGet();
            tw.a(decrementAndGet >= 0, "Can't decrement below 0");
            if (decrementAndGet == 0) {
                vpVar = this.z;
                h();
            } else {
                vpVar = null;
            }
        }
        if (vpVar != null) {
            vpVar.g();
        }
    }

    @DexIgnore
    public final dr c() {
        if (this.r) {
            return this.m;
        }
        return this.s ? this.n : this.l;
    }

    @DexIgnore
    public void a(sv svVar) {
        try {
            svVar.a(this.x);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    @DexIgnore
    public void a() {
        if (!d()) {
            this.B = true;
            this.A.k();
            this.j.a(this, this.p);
        }
    }

    @DexIgnore
    public synchronized void a(int i2) {
        tw.a(d(), "Not yet complete!");
        if (this.o.getAndAdd(i2) == 0 && this.z != null) {
            this.z.d();
        }
    }

    @DexIgnore
    public void a(aq<R> aqVar, DataSource dataSource) {
        synchronized (this) {
            this.u = aqVar;
            this.v = dataSource;
        }
        f();
    }

    @DexIgnore
    public void a(GlideException glideException) {
        synchronized (this) {
            this.x = glideException;
        }
        e();
    }

    @DexIgnore
    public void a(DecodeJob<?> decodeJob) {
        c().execute(decodeJob);
    }
}
