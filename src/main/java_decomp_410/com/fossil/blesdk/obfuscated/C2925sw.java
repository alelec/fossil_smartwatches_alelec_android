package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sw */
public class C2925sw {

    @DexIgnore
    /* renamed from: a */
    public java.lang.Class<?> f9477a;

    @DexIgnore
    /* renamed from: b */
    public java.lang.Class<?> f9478b;

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<?> f9479c;

    @DexIgnore
    public C2925sw() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16203a(java.lang.Class<?> cls, java.lang.Class<?> cls2, java.lang.Class<?> cls3) {
        this.f9477a = cls;
        this.f9478b = cls2;
        this.f9479c = cls3;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C2925sw.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2925sw swVar = (com.fossil.blesdk.obfuscated.C2925sw) obj;
        return this.f9477a.equals(swVar.f9477a) && this.f9478b.equals(swVar.f9478b) && com.fossil.blesdk.obfuscated.C3066uw.m14934b((java.lang.Object) this.f9479c, (java.lang.Object) swVar.f9479c);
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((this.f9477a.hashCode() * 31) + this.f9478b.hashCode()) * 31;
        java.lang.Class<?> cls = this.f9479c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    @DexIgnore
    public java.lang.String toString() {
        return "MultiClassKey{first=" + this.f9477a + ", second=" + this.f9478b + '}';
    }

    @DexIgnore
    public C2925sw(java.lang.Class<?> cls, java.lang.Class<?> cls2, java.lang.Class<?> cls3) {
        mo16203a(cls, cls2, cls3);
    }
}
