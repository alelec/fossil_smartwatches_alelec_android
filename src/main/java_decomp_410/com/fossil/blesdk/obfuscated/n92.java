package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n92 extends m92 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(R.id.iv, 1);
        w.put(R.id.ftv_title, 2);
        w.put(R.id.ftv_description, 3);
        w.put(R.id.tv_not_now, 4);
        w.put(R.id.tv_allow, 5);
    }
    */

    @DexIgnore
    public n92(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 6, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public n92(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[3], objArr[2], objArr[1], objArr[5], objArr[4]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
