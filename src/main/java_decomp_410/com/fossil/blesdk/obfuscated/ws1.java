package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ws1 {
    @DexIgnore
    public static ColorStateList a(Context context, TypedArray typedArray, int i) {
        if (typedArray.hasValue(i)) {
            int resourceId = typedArray.getResourceId(i, 0);
            if (resourceId != 0) {
                ColorStateList b = m0.b(context, resourceId);
                if (b != null) {
                    return b;
                }
            }
        }
        return typedArray.getColorStateList(i);
    }

    @DexIgnore
    public static Drawable b(Context context, TypedArray typedArray, int i) {
        if (typedArray.hasValue(i)) {
            int resourceId = typedArray.getResourceId(i, 0);
            if (resourceId != 0) {
                Drawable c = m0.c(context, resourceId);
                if (c != null) {
                    return c;
                }
            }
        }
        return typedArray.getDrawable(i);
    }

    @DexIgnore
    public static xs1 c(Context context, TypedArray typedArray, int i) {
        if (!typedArray.hasValue(i)) {
            return null;
        }
        int resourceId = typedArray.getResourceId(i, 0);
        if (resourceId != 0) {
            return new xs1(context, resourceId);
        }
        return null;
    }

    @DexIgnore
    public static int a(TypedArray typedArray, int i, int i2) {
        return typedArray.hasValue(i) ? i : i2;
    }
}
