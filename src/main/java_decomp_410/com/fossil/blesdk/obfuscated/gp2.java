package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gp2 implements MembersInjector<MFDeviceService> {
    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, en2 en2) {
        mFDeviceService.f = en2;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, DeviceRepository deviceRepository) {
        mFDeviceService.g = deviceRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ActivitiesRepository activitiesRepository) {
        mFDeviceService.h = activitiesRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, SummariesRepository summariesRepository) {
        mFDeviceService.i = summariesRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, SleepSessionsRepository sleepSessionsRepository) {
        mFDeviceService.j = sleepSessionsRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, SleepSummariesRepository sleepSummariesRepository) {
        mFDeviceService.k = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, UserRepository userRepository) {
        mFDeviceService.l = userRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, HybridPresetRepository hybridPresetRepository) {
        mFDeviceService.m = hybridPresetRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, MicroAppRepository microAppRepository) {
        mFDeviceService.n = microAppRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, HeartRateSampleRepository heartRateSampleRepository) {
        mFDeviceService.o = heartRateSampleRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, HeartRateSummaryRepository heartRateSummaryRepository) {
        mFDeviceService.p = heartRateSummaryRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, WorkoutSessionRepository workoutSessionRepository) {
        mFDeviceService.q = workoutSessionRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, FitnessDataRepository fitnessDataRepository) {
        mFDeviceService.r = fitnessDataRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, GoalTrackingRepository goalTrackingRepository) {
        mFDeviceService.s = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, h42 h42) {
        mFDeviceService.t = h42;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, AnalyticsHelper analyticsHelper) {
        mFDeviceService.u = analyticsHelper;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, PortfolioApp portfolioApp) {
        mFDeviceService.v = portfolioApp;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, LinkStreamingManager linkStreamingManager) {
        mFDeviceService.w = linkStreamingManager;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ThirdPartyRepository thirdPartyRepository) {
        mFDeviceService.x = thirdPartyRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, mr3 mr3) {
        mFDeviceService.y = mr3;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, VerifySecretKeyUseCase verifySecretKeyUseCase) {
        mFDeviceService.z = verifySecretKeyUseCase;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, xk2 xk2) {
        mFDeviceService.A = xk2;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, WatchParamHelper watchParamHelper) {
        mFDeviceService.B = watchParamHelper;
    }
}
