package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class is4 implements gr4<em4, Double> {
    @DexIgnore
    public static /* final */ is4 a; // = new is4();

    @DexIgnore
    public Double a(em4 em4) throws IOException {
        return Double.valueOf(em4.F());
    }
}
