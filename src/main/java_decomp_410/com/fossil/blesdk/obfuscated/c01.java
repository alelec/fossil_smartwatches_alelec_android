package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c01 extends de0.a<zz0, so0> {
    @DexIgnore
    public c01() {
    }

    @DexIgnore
    public final /* synthetic */ de0.f a(Context context, Looper looper, kj0 kj0, Object obj, ge0.b bVar, ge0.c cVar) {
        return new zz0(context, looper, kj0, bVar, cVar);
    }
}
