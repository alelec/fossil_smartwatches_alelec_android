package com.fossil.blesdk.obfuscated;

import android.text.SpannableString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface by2 extends v52<ay2> {
    @DexIgnore
    void C(boolean z);

    @DexIgnore
    void D(boolean z);

    @DexIgnore
    void L(String str);

    @DexIgnore
    void c(SpannableString spannableString);

    @DexIgnore
    void close();

    @DexIgnore
    void d(SpannableString spannableString);

    @DexIgnore
    void e(boolean z);

    @DexIgnore
    void u(boolean z);

    @DexIgnore
    void w(boolean z);
}
