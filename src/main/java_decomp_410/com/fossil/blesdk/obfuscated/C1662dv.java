package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@java.lang.Deprecated
/* renamed from: com.fossil.blesdk.obfuscated.dv */
public final class C1662dv {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f4534a;

    @DexIgnore
    public C1662dv(android.content.Context context) {
        this.f4534a = context;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.fossil.blesdk.obfuscated.C1516bv> mo10139a() {
        if (android.util.Log.isLoggable("ManifestParser", 3)) {
            android.util.Log.d("ManifestParser", "Loading Glide modules");
        }
        java.util.ArrayList arrayList = new java.util.ArrayList();
        try {
            android.content.pm.ApplicationInfo applicationInfo = this.f4534a.getPackageManager().getApplicationInfo(this.f4534a.getPackageName(), 128);
            if (applicationInfo.metaData == null) {
                if (android.util.Log.isLoggable("ManifestParser", 3)) {
                    android.util.Log.d("ManifestParser", "Got null app info metadata");
                }
                return arrayList;
            }
            if (android.util.Log.isLoggable("ManifestParser", 2)) {
                android.util.Log.v("ManifestParser", "Got app info metadata: " + applicationInfo.metaData);
            }
            for (java.lang.String str : applicationInfo.metaData.keySet()) {
                if ("GlideModule".equals(applicationInfo.metaData.get(str))) {
                    arrayList.add(m6064a(str));
                    if (android.util.Log.isLoggable("ManifestParser", 3)) {
                        android.util.Log.d("ManifestParser", "Loaded Glide module: " + str);
                    }
                }
            }
            if (android.util.Log.isLoggable("ManifestParser", 3)) {
                android.util.Log.d("ManifestParser", "Finished loading Glide modules");
            }
            return arrayList;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            throw new java.lang.RuntimeException("Unable to find metadata to parse GlideModules", e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1516bv m6064a(java.lang.String str) {
        try {
            java.lang.Class<?> cls = java.lang.Class.forName(str);
            try {
                java.lang.Object newInstance = cls.getDeclaredConstructor(new java.lang.Class[0]).newInstance(new java.lang.Object[0]);
                if (newInstance instanceof com.fossil.blesdk.obfuscated.C1516bv) {
                    return (com.fossil.blesdk.obfuscated.C1516bv) newInstance;
                }
                throw new java.lang.RuntimeException("Expected instanceof GlideModule, but found: " + newInstance);
            } catch (java.lang.InstantiationException e) {
                m6065a(cls, e);
                throw null;
            } catch (java.lang.IllegalAccessException e2) {
                m6065a(cls, e2);
                throw null;
            } catch (java.lang.NoSuchMethodException e3) {
                m6065a(cls, e3);
                throw null;
            } catch (java.lang.reflect.InvocationTargetException e4) {
                m6065a(cls, e4);
                throw null;
            }
        } catch (java.lang.ClassNotFoundException e5) {
            throw new java.lang.IllegalArgumentException("Unable to find GlideModule implementation", e5);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6065a(java.lang.Class<?> cls, java.lang.Exception exc) {
        throw new java.lang.RuntimeException("Unable to instantiate GlideModule implementation for " + cls, exc);
    }
}
