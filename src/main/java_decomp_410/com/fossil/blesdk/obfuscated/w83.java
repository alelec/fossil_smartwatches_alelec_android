package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w83 implements Factory<ActiveTimeOverviewWeekPresenter> {
    @DexIgnore
    public static ActiveTimeOverviewWeekPresenter a(u83 u83, UserRepository userRepository, SummariesRepository summariesRepository) {
        return new ActiveTimeOverviewWeekPresenter(u83, userRepository, summariesRepository);
    }
}
