package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lm */
public interface C2334lm {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lm$a")
    /* renamed from: com.fossil.blesdk.obfuscated.lm$a */
    public static class C2335a {

        @DexIgnore
        /* renamed from: a */
        public byte[] f7239a;

        @DexIgnore
        /* renamed from: b */
        public java.lang.String f7240b;

        @DexIgnore
        /* renamed from: c */
        public long f7241c;

        @DexIgnore
        /* renamed from: d */
        public long f7242d;

        @DexIgnore
        /* renamed from: e */
        public long f7243e;

        @DexIgnore
        /* renamed from: f */
        public long f7244f;

        @DexIgnore
        /* renamed from: g */
        public java.util.Map<java.lang.String, java.lang.String> f7245g; // = java.util.Collections.emptyMap();

        @DexIgnore
        /* renamed from: h */
        public java.util.List<com.fossil.blesdk.obfuscated.C2660pm> f7246h;

        @DexIgnore
        /* renamed from: a */
        public boolean mo13312a() {
            return this.f7243e < java.lang.System.currentTimeMillis();
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo13313b() {
            return this.f7244f < java.lang.System.currentTimeMillis();
        }
    }

    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C2334lm.C2335a mo9568a(java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    void mo9572a(java.lang.String str, com.fossil.blesdk.obfuscated.C2334lm.C2335a aVar);

    @DexIgnore
    /* renamed from: d */
    void mo9576d();
}
