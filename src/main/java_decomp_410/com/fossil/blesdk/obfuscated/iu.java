package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class iu<Z> implements gu<Z, Z> {
    @DexIgnore
    public static /* final */ iu<?> a; // = new iu<>();

    @DexIgnore
    public static <Z> gu<Z, Z> a() {
        return a;
    }

    @DexIgnore
    public aq<Z> a(aq<Z> aqVar, lo loVar) {
        return aqVar;
    }
}
