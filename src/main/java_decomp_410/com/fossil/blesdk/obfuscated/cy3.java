package com.fossil.blesdk.obfuscated;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cy3 extends InputStream {
    @DexIgnore
    public /* final */ InputStream e;
    @DexIgnore
    public long f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;

    @DexIgnore
    public cy3(InputStream inputStream) {
        this(inputStream, 4096);
    }

    @DexIgnore
    public final void a(long j, long j2) throws IOException {
        while (j < j2) {
            long skip = this.e.skip(j2 - j);
            if (skip == 0) {
                if (read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j += skip;
        }
    }

    @DexIgnore
    public int available() throws IOException {
        return this.e.available();
    }

    @DexIgnore
    public long b(int i2) {
        long j = this.f + ((long) i2);
        if (this.h < j) {
            i(j);
        }
        return this.f;
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public void h(long j) throws IOException {
        if (this.f > this.h || j < this.g) {
            throw new IOException("Cannot reset");
        }
        this.e.reset();
        a(this.g, j);
        this.f = j;
    }

    @DexIgnore
    public final void i(long j) {
        try {
            if (this.g >= this.f || this.f > this.h) {
                this.g = this.f;
                this.e.mark((int) (j - this.f));
            } else {
                this.e.reset();
                this.e.mark((int) (j - this.g));
                a(this.g, this.f);
            }
            this.h = j;
        } catch (IOException e2) {
            throw new IllegalStateException("Unable to mark: " + e2);
        }
    }

    @DexIgnore
    public void mark(int i2) {
        this.i = b(i2);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.e.markSupported();
    }

    @DexIgnore
    public int read() throws IOException {
        int read = this.e.read();
        if (read != -1) {
            this.f++;
        }
        return read;
    }

    @DexIgnore
    public void reset() throws IOException {
        h(this.i);
    }

    @DexIgnore
    public long skip(long j) throws IOException {
        long skip = this.e.skip(j);
        this.f += skip;
        return skip;
    }

    @DexIgnore
    public cy3(InputStream inputStream, int i2) {
        this.i = -1;
        this.e = !inputStream.markSupported() ? new BufferedInputStream(inputStream, i2) : inputStream;
    }

    @DexIgnore
    public int read(byte[] bArr) throws IOException {
        int read = this.e.read(bArr);
        if (read != -1) {
            this.f += (long) read;
        }
        return read;
    }

    @DexIgnore
    public int read(byte[] bArr, int i2, int i3) throws IOException {
        int read = this.e.read(bArr, i2, i3);
        if (read != -1) {
            this.f += (long) read;
        }
        return read;
    }
}
