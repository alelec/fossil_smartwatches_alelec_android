package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import java.lang.ref.WeakReference;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lt3 {
    @DexIgnore
    public static /* final */ ExecutorService f; // = Executors.newCachedThreadPool();
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ WeakReference<Context> b;
    @DexIgnore
    public /* final */ kt3 c;
    @DexIgnore
    public /* final */ Bitmap d;
    @DexIgnore
    public /* final */ b e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.lt3$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.lt3$a$a  reason: collision with other inner class name */
        public class C0095a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ BitmapDrawable e;

            @DexIgnore
            public C0095a(BitmapDrawable bitmapDrawable) {
                this.e = bitmapDrawable;
            }

            @DexIgnore
            public void run() {
                lt3.this.e.a(this.e);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            lt3 lt3 = lt3.this;
            BitmapDrawable bitmapDrawable = new BitmapDrawable(lt3.a, gt3.a((Context) lt3.this.b.get(), lt3.d, lt3.c));
            if (lt3.this.e != null) {
                new Handler(Looper.getMainLooper()).post(new C0095a(bitmapDrawable));
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(BitmapDrawable bitmapDrawable);
    }

    @DexIgnore
    public lt3(Context context, Bitmap bitmap, kt3 kt3, b bVar) {
        this.a = context.getResources();
        this.c = kt3;
        this.e = bVar;
        this.b = new WeakReference<>(context);
        this.d = bitmap;
    }

    @DexIgnore
    public void a() {
        f.execute(new a());
    }
}
