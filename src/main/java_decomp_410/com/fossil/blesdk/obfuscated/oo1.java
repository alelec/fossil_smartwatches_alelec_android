package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oo1<TResult, TContinuationResult> implements qn1, sn1, tn1<TContinuationResult>, qo1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ vn1<TResult, TContinuationResult> b;
    @DexIgnore
    public /* final */ uo1<TContinuationResult> c;

    @DexIgnore
    public oo1(Executor executor, vn1<TResult, TContinuationResult> vn1, uo1<TContinuationResult> uo1) {
        this.a = executor;
        this.b = vn1;
        this.c = uo1;
    }

    @DexIgnore
    public final void onCanceled() {
        this.c.f();
    }

    @DexIgnore
    public final void onComplete(wn1<TResult> wn1) {
        this.a.execute(new po1(this, wn1));
    }

    @DexIgnore
    public final void onFailure(Exception exc) {
        this.c.a(exc);
    }

    @DexIgnore
    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.c.a(tcontinuationresult);
    }
}
