package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ph0 {
    @DexIgnore
    public static /* final */ Status d; // = new Status(8, "The connection to Google Play services was lost");
    @DexIgnore
    public static /* final */ BasePendingResult<?>[] e; // = new BasePendingResult[0];
    @DexIgnore
    public /* final */ Set<BasePendingResult<?>> a; // = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @DexIgnore
    public /* final */ sh0 b; // = new qh0(this);
    @DexIgnore
    public /* final */ Map<de0.c<?>, de0.f> c;

    @DexIgnore
    public ph0(Map<de0.c<?>, de0.f> map) {
        this.c = map;
    }

    @DexIgnore
    public final void a(BasePendingResult<? extends me0> basePendingResult) {
        this.a.add(basePendingResult);
        basePendingResult.a(this.b);
    }

    @DexIgnore
    public final void b() {
        for (BasePendingResult b2 : (BasePendingResult[]) this.a.toArray(e)) {
            b2.b(d);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.fossil.blesdk.obfuscated.sh0, com.fossil.blesdk.obfuscated.ne0, com.fossil.blesdk.obfuscated.qh0, com.fossil.blesdk.obfuscated.vi0] */
    public final void a() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.a.toArray(e)) {
            Object r5 = 0;
            basePendingResult.a((sh0) r5);
            if (basePendingResult.e() != null) {
                basePendingResult.a(r5);
                IBinder m = this.c.get(((te0) basePendingResult).i()).m();
                if (basePendingResult.d()) {
                    basePendingResult.a((sh0) new rh0(basePendingResult, r5, m, r5));
                } else if (m == null || !m.isBinderAlive()) {
                    basePendingResult.a((sh0) r5);
                    basePendingResult.a();
                    r5.a(basePendingResult.e().intValue());
                } else {
                    rh0 rh0 = new rh0(basePendingResult, r5, m, r5);
                    basePendingResult.a((sh0) rh0);
                    try {
                        m.linkToDeath(rh0, 0);
                    } catch (RemoteException unused) {
                        basePendingResult.a();
                        r5.a(basePendingResult.e().intValue());
                    }
                }
                this.a.remove(basePendingResult);
            } else if (basePendingResult.f()) {
                this.a.remove(basePendingResult);
            }
        }
    }
}
