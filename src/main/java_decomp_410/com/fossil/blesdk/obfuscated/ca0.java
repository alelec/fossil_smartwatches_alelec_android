package com.fossil.blesdk.obfuscated;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.setting.SharedPreferenceFileName;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ca0 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ Hashtable<String, DeviceInformation> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> c; // = new Hashtable<>();
    @DexIgnore
    public static ea0 d; // = new ea0(va0.f.b(), va0.f.c(), va0.f.e(), va0.f.d(), "5.8.5-production-release", (String) null, 32, (fd4) null);
    @DexIgnore
    public static /* final */ ca0 e;

    /*
    static {
        ca0 ca0 = new ca0();
        e = ca0;
        String uuid = UUID.randomUUID().toString();
        kd4.a((Object) uuid, "UUID.randomUUID().toString()");
        a = uuid;
        ca0.b();
    }
    */

    @DexIgnore
    public final ea0 a() {
        return d;
    }

    @DexIgnore
    public final void b() {
        SharedPreferences a2 = ya0.a(SharedPreferenceFileName.SDK_LOG_PREFERENCE);
        if (a2 != null) {
            String string = a2.getString("log.device.mapping", (String) null);
            if (string != null) {
                String a3 = kb0.c.a(string);
                if (a3 != null) {
                    try {
                        JSONObject jSONObject = new JSONObject(a3);
                        b.clear();
                        b.putAll(a(jSONObject));
                    } catch (Exception e2) {
                        da0.l.a(e2);
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void c() {
        String jSONObject = a((Map<String, DeviceInformation>) b).toString();
        kd4.a((Object) jSONObject, "convertDeviceInfoMapToJS\u2026formationMaps).toString()");
        String b2 = kb0.c.b(jSONObject);
        SharedPreferences a2 = ya0.a(SharedPreferenceFileName.SDK_LOG_PREFERENCE);
        if (a2 != null) {
            SharedPreferences.Editor edit = a2.edit();
            if (edit != null) {
                SharedPreferences.Editor putString = edit.putString("log.device.mapping", b2);
                if (putString != null) {
                    putString.apply();
                }
            }
        }
    }

    @DexIgnore
    public final DeviceInformation a(String str) {
        kd4.b(str, "macAddress");
        return b.get(str);
    }

    @DexIgnore
    public final void a(String str, DeviceInformation deviceInformation) {
        kd4.b(str, "macAddress");
        kd4.b(deviceInformation, "deviceInformation");
        DeviceInformation deviceInformation2 = b.get(str);
        Hashtable<String, DeviceInformation> hashtable = b;
        if (deviceInformation2 != null) {
            deviceInformation = DeviceInformation.Companion.a(deviceInformation2, deviceInformation);
        }
        hashtable.put(str, deviceInformation);
        c();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        kd4.b(str, "macAddress");
        kd4.b(str2, "sessionId");
        c.put(str, str2);
    }

    @DexIgnore
    public final String b(String str) {
        kd4.b(str, "macAddress");
        String str2 = c.get(str);
        return str2 != null ? str2 : a;
    }

    @DexIgnore
    public final JSONObject a(Map<String, DeviceInformation> map) {
        JSONObject jSONObject = new JSONObject();
        for (String str : map.keySet()) {
            DeviceInformation deviceInformation = map.get(str);
            if (deviceInformation != null) {
                jSONObject.put(str, deviceInformation.toJSONObject());
            }
        }
        return jSONObject;
    }

    @DexIgnore
    public final HashMap<String, DeviceInformation> a(JSONObject jSONObject) {
        HashMap<String, DeviceInformation> hashMap = new HashMap<>();
        Iterator<String> keys = jSONObject.keys();
        kd4.a((Object) keys, "jsonObject.keys()");
        while (keys.hasNext()) {
            String next = keys.next();
            if (BluetoothAdapter.checkBluetoothAddress(next)) {
                JSONObject optJSONObject = jSONObject.optJSONObject(next);
                if (optJSONObject != null) {
                    DeviceInformation a2 = DeviceInformation.Companion.a(optJSONObject);
                    if (a2 != null) {
                        kd4.a((Object) next, "key");
                        hashMap.put(next, a2);
                    }
                }
            }
        }
        return hashMap;
    }
}
