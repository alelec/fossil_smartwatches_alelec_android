package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wn1<TResult> {
    @DexIgnore
    public wn1<TResult> a(rn1<TResult> rn1) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public abstract wn1<TResult> a(sn1 sn1);

    @DexIgnore
    public abstract wn1<TResult> a(tn1<? super TResult> tn1);

    @DexIgnore
    public abstract wn1<TResult> a(Executor executor, sn1 sn1);

    @DexIgnore
    public abstract wn1<TResult> a(Executor executor, tn1<? super TResult> tn1);

    @DexIgnore
    public abstract Exception a();

    @DexIgnore
    public abstract <X extends Throwable> TResult a(Class<X> cls) throws Throwable;

    @DexIgnore
    public <TContinuationResult> wn1<TContinuationResult> b(pn1<TResult, wn1<TContinuationResult>> pn1) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public abstract TResult b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract boolean d();

    @DexIgnore
    public abstract boolean e();

    @DexIgnore
    public wn1<TResult> a(Executor executor, rn1<TResult> rn1) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> wn1<TContinuationResult> b(Executor executor, pn1<TResult, wn1<TContinuationResult>> pn1) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @DexIgnore
    public wn1<TResult> a(Executor executor, qn1 qn1) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> wn1<TContinuationResult> a(pn1<TResult, TContinuationResult> pn1) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> wn1<TContinuationResult> a(Executor executor, pn1<TResult, TContinuationResult> pn1) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> wn1<TContinuationResult> a(vn1<TResult, TContinuationResult> vn1) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }

    @DexIgnore
    public <TContinuationResult> wn1<TContinuationResult> a(Executor executor, vn1<TResult, TContinuationResult> vn1) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
