package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ic4 {
    @DexIgnore
    public static final <T> yb4<T> a(yb4<? super T> yb4) {
        kd4.b(yb4, "completion");
        return yb4;
    }

    @DexIgnore
    public static final void b(yb4<?> yb4) {
        kd4.b(yb4, "frame");
    }

    @DexIgnore
    public static final void c(yb4<?> yb4) {
        kd4.b(yb4, "frame");
    }
}
