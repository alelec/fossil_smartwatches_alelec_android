package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j13 {
    @DexIgnore
    public /* final */ i23 a;
    @DexIgnore
    public /* final */ q43 b;
    @DexIgnore
    public /* final */ j43 c;

    @DexIgnore
    public j13(i23 i23, q43 q43, j43 j43) {
        kd4.b(i23, "mComplicationsContractView");
        kd4.b(q43, "mWatchAppsContractView");
        kd4.b(j43, "mCustomizeThemeContractView");
        this.a = i23;
        this.b = q43;
        this.c = j43;
    }

    @DexIgnore
    public final i23 a() {
        return this.a;
    }

    @DexIgnore
    public final j43 b() {
        return this.c;
    }

    @DexIgnore
    public final q43 c() {
        return this.b;
    }
}
