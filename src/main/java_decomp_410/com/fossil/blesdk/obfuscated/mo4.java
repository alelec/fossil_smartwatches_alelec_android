package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.util.zip.Deflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo4 implements xo4 {
    @DexIgnore
    public /* final */ ko4 e;
    @DexIgnore
    public /* final */ Deflater f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public mo4(xo4 xo4, Deflater deflater) {
        this(so4.a(xo4), deflater);
    }

    @DexIgnore
    public void a(jo4 jo4, long j) throws IOException {
        ap4.a(jo4.f, 0, j);
        while (j > 0) {
            vo4 vo4 = jo4.e;
            int min = (int) Math.min(j, (long) (vo4.c - vo4.b));
            this.f.setInput(vo4.a, vo4.b, min);
            a(false);
            long j2 = (long) min;
            jo4.f -= j2;
            vo4.b += min;
            if (vo4.b == vo4.c) {
                jo4.e = vo4.b();
                wo4.a(vo4);
            }
            j -= j2;
        }
    }

    @DexIgnore
    public zo4 b() {
        return this.e.b();
    }

    @DexIgnore
    public void close() throws IOException {
        if (!this.g) {
            try {
                f();
                th = null;
            } catch (Throwable th) {
                th = th;
            }
            try {
                this.f.end();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                }
            }
            try {
                this.e.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.g = true;
            if (th != null) {
                ap4.a(th);
                throw null;
            }
        }
    }

    @DexIgnore
    public void f() throws IOException {
        this.f.finish();
        a(false);
    }

    @DexIgnore
    public void flush() throws IOException {
        a(true);
        this.e.flush();
    }

    @DexIgnore
    public String toString() {
        return "DeflaterSink(" + this.e + ")";
    }

    @DexIgnore
    public mo4(ko4 ko4, Deflater deflater) {
        if (ko4 == null) {
            throw new IllegalArgumentException("source == null");
        } else if (deflater != null) {
            this.e = ko4;
            this.f = deflater;
        } else {
            throw new IllegalArgumentException("inflater == null");
        }
    }

    @DexIgnore
    public final void a(boolean z) throws IOException {
        vo4 b;
        int i;
        jo4 a = this.e.a();
        while (true) {
            b = a.b(1);
            if (z) {
                Deflater deflater = this.f;
                byte[] bArr = b.a;
                int i2 = b.c;
                i = deflater.deflate(bArr, i2, 8192 - i2, 2);
            } else {
                Deflater deflater2 = this.f;
                byte[] bArr2 = b.a;
                int i3 = b.c;
                i = deflater2.deflate(bArr2, i3, 8192 - i3);
            }
            if (i > 0) {
                b.c += i;
                a.f += (long) i;
                this.e.d();
            } else if (this.f.needsInput()) {
                break;
            }
        }
        if (b.b == b.c) {
            a.e = b.b();
            wo4.a(b);
        }
    }
}
