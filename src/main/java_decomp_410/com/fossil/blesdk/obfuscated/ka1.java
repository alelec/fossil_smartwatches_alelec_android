package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ka1<E> extends m71<E> {
    @DexIgnore
    public static /* final */ ka1<Object> g;
    @DexIgnore
    public /* final */ List<E> f;

    /*
    static {
        ka1<Object> ka1 = new ka1<>();
        g = ka1;
        ka1.B();
    }
    */

    @DexIgnore
    public ka1() {
        this(new ArrayList(10));
    }

    @DexIgnore
    public static <E> ka1<E> b() {
        return g;
    }

    @DexIgnore
    public final void add(int i, E e) {
        a();
        this.f.add(i, e);
        this.modCount++;
    }

    @DexIgnore
    public final E get(int i) {
        return this.f.get(i);
    }

    @DexIgnore
    public final E remove(int i) {
        a();
        E remove = this.f.remove(i);
        this.modCount++;
        return remove;
    }

    @DexIgnore
    public final E set(int i, E e) {
        a();
        E e2 = this.f.set(i, e);
        this.modCount++;
        return e2;
    }

    @DexIgnore
    public final int size() {
        return this.f.size();
    }

    @DexIgnore
    public ka1(List<E> list) {
        this.f = list;
    }

    @DexIgnore
    public final /* synthetic */ z81 b(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f);
            return new ka1(arrayList);
        }
        throw new IllegalArgumentException();
    }
}
