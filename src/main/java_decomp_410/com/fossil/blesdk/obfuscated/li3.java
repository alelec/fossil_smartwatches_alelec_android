package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class li3 implements Factory<HelpPresenter> {
    @DexIgnore
    public static HelpPresenter a(hi3 hi3, DeviceRepository deviceRepository, kr2 kr2, AnalyticsHelper analyticsHelper) {
        return new HelpPresenter(hi3, deviceRepository, kr2, analyticsHelper);
    }
}
