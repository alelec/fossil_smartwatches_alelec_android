package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzuk;
import com.google.android.gms.internal.measurement.zzux;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class p81 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[zzuk.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[zzux.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(14:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
    /*
    static {
        try {
            b[zzux.BYTE_STRING.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            b[zzux.MESSAGE.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            b[zzux.STRING.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        a[zzuk.MAP.ordinal()] = 1;
        a[zzuk.VECTOR.ordinal()] = 2;
        a[zzuk.SCALAR.ordinal()] = 3;
    }
    */
}
