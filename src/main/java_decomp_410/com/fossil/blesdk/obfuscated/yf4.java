package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineStart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yf4 {
    @DexIgnore
    public static final <T> gh4<T> a(zg4 zg4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4) {
        return ag4.a(zg4, coroutineContext, coroutineStart, yc4);
    }

    @DexIgnore
    public static final fi4 b(zg4 zg4, CoroutineContext coroutineContext, CoroutineStart coroutineStart, yc4<? super zg4, ? super yb4<? super qa4>, ? extends Object> yc4) {
        return ag4.b(zg4, coroutineContext, coroutineStart, yc4);
    }

    @DexIgnore
    public static final <T> T a(CoroutineContext coroutineContext, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4) throws InterruptedException {
        return zf4.a(coroutineContext, yc4);
    }

    @DexIgnore
    public static final <T> Object a(CoroutineContext coroutineContext, yc4<? super zg4, ? super yb4<? super T>, ? extends Object> yc4, yb4<? super T> yb4) {
        return ag4.a(coroutineContext, yc4, yb4);
    }
}
