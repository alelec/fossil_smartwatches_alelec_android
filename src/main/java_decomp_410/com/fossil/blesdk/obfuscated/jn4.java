package com.fossil.blesdk.obfuscated;

import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jn4 {
    @DexIgnore
    public static /* final */ ByteString d; // = ByteString.encodeUtf8(":");
    @DexIgnore
    public static /* final */ ByteString e; // = ByteString.encodeUtf8(":status");
    @DexIgnore
    public static /* final */ ByteString f; // = ByteString.encodeUtf8(":method");
    @DexIgnore
    public static /* final */ ByteString g; // = ByteString.encodeUtf8(":path");
    @DexIgnore
    public static /* final */ ByteString h; // = ByteString.encodeUtf8(":scheme");
    @DexIgnore
    public static /* final */ ByteString i; // = ByteString.encodeUtf8(":authority");
    @DexIgnore
    public /* final */ ByteString a;
    @DexIgnore
    public /* final */ ByteString b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(yl4 yl4);
    }

    @DexIgnore
    public jn4(String str, String str2) {
        this(ByteString.encodeUtf8(str), ByteString.encodeUtf8(str2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof jn4)) {
            return false;
        }
        jn4 jn4 = (jn4) obj;
        if (!this.a.equals(jn4.a) || !this.b.equals(jn4.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.a.hashCode()) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return jm4.a("%s: %s", this.a.utf8(), this.b.utf8());
    }

    @DexIgnore
    public jn4(ByteString byteString, String str) {
        this(byteString, ByteString.encodeUtf8(str));
    }

    @DexIgnore
    public jn4(ByteString byteString, ByteString byteString2) {
        this.a = byteString;
        this.b = byteString2;
        this.c = byteString.size() + 32 + byteString2.size();
    }
}
