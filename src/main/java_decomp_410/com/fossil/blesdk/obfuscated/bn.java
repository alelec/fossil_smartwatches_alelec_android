package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bn {
    @DexIgnore
    public static /* final */ Comparator<byte[]> e; // = new a();
    @DexIgnore
    public /* final */ List<byte[]> a; // = new ArrayList();
    @DexIgnore
    public /* final */ List<byte[]> b; // = new ArrayList(64);
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public /* final */ int d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Comparator<byte[]> {
        @DexIgnore
        /* renamed from: a */
        public int compare(byte[] bArr, byte[] bArr2) {
            return bArr.length - bArr2.length;
        }
    }

    @DexIgnore
    public bn(int i) {
        this.d = i;
    }

    @DexIgnore
    public synchronized byte[] a(int i) {
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            byte[] bArr = this.b.get(i2);
            if (bArr.length >= i) {
                this.c -= bArr.length;
                this.b.remove(i2);
                this.a.remove(bArr);
                return bArr;
            }
        }
        return new byte[i];
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        return;
     */
    @DexIgnore
    public synchronized void a(byte[] bArr) {
        if (bArr != null) {
            if (bArr.length <= this.d) {
                this.a.add(bArr);
                int binarySearch = Collections.binarySearch(this.b, bArr, e);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 1;
                }
                this.b.add(binarySearch, bArr);
                this.c += bArr.length;
                a();
            }
        }
    }

    @DexIgnore
    public final synchronized void a() {
        while (this.c > this.d) {
            byte[] remove = this.a.remove(0);
            this.b.remove(remove);
            this.c -= remove.length;
        }
    }
}
