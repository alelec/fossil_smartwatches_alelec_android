package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qo */
public abstract class C2754qo<T> implements com.fossil.blesdk.obfuscated.C2902so<T> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f8748e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.res.AssetManager f8749f;

    @DexIgnore
    /* renamed from: g */
    public T f8750g;

    @DexIgnore
    public C2754qo(android.content.res.AssetManager assetManager, java.lang.String str) {
        this.f8749f = assetManager;
        this.f8748e = str;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract T mo9241a(android.content.res.AssetManager assetManager, java.lang.String str) throws java.io.IOException;

    @DexIgnore
    /* renamed from: a */
    public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super T> aVar) {
        try {
            this.f8750g = mo9241a(this.f8749f, this.f8748e);
            aVar.mo9252a(this.f8750g);
        } catch (java.io.IOException e) {
            if (android.util.Log.isLoggable("AssetPathFetcher", 3)) {
                android.util.Log.d("AssetPathFetcher", "Failed to load data from asset manager", e);
            }
            aVar.mo9251a((java.lang.Exception) e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo9243a(T t) throws java.io.IOException;

    @DexIgnore
    /* renamed from: b */
    public com.bumptech.glide.load.DataSource mo8872b() {
        return com.bumptech.glide.load.DataSource.LOCAL;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8869a() {
        T t = this.f8750g;
        if (t != null) {
            try {
                mo9243a(t);
            } catch (java.io.IOException unused) {
            }
        }
    }
}
