package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qf1 implements Parcelable.Creator<kf1> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        LatLng latLng = null;
        String str = null;
        String str2 = null;
        IBinder iBinder = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f4 = 0.5f;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f6 = 1.0f;
        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    latLng = SafeParcelReader.a(parcel2, a, LatLng.CREATOR);
                    break;
                case 3:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 4:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 5:
                    iBinder = SafeParcelReader.p(parcel2, a);
                    break;
                case 6:
                    f = SafeParcelReader.n(parcel2, a);
                    break;
                case 7:
                    f2 = SafeParcelReader.n(parcel2, a);
                    break;
                case 8:
                    z = SafeParcelReader.i(parcel2, a);
                    break;
                case 9:
                    z2 = SafeParcelReader.i(parcel2, a);
                    break;
                case 10:
                    z3 = SafeParcelReader.i(parcel2, a);
                    break;
                case 11:
                    f3 = SafeParcelReader.n(parcel2, a);
                    break;
                case 12:
                    f4 = SafeParcelReader.n(parcel2, a);
                    break;
                case 13:
                    f5 = SafeParcelReader.n(parcel2, a);
                    break;
                case 14:
                    f6 = SafeParcelReader.n(parcel2, a);
                    break;
                case 15:
                    f7 = SafeParcelReader.n(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new kf1(latLng, str, str2, iBinder, f, f2, z, z2, z3, f3, f4, f5, f6, f7);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new kf1[i];
    }
}
