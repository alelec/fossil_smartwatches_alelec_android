package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ long h;
    @DexIgnore
    public /* final */ /* synthetic */ dj1 i;

    @DexIgnore
    public gj1(dj1 dj1, String str, String str2, Object obj, long j) {
        this.i = dj1;
        this.e = str;
        this.f = str2;
        this.g = obj;
        this.h = j;
    }

    @DexIgnore
    public final void run() {
        this.i.a(this.e, this.f, this.g, this.h);
    }
}
