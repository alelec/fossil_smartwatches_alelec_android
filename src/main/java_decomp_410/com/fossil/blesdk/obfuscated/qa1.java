package com.fossil.blesdk.obfuscated;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qa1<K extends Comparable<K>, V> extends AbstractMap<K, V> {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public List<xa1> f;
    @DexIgnore
    public Map<K, V> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile za1 i;
    @DexIgnore
    public Map<K, V> j;
    @DexIgnore
    public volatile ta1 k;

    @DexIgnore
    public qa1(int i2) {
        this.e = i2;
        this.f = Collections.emptyList();
        this.g = Collections.emptyMap();
        this.j = Collections.emptyMap();
    }

    @DexIgnore
    public static <FieldDescriptorType extends o81<FieldDescriptorType>> qa1<FieldDescriptorType, Object> c(int i2) {
        return new ra1(i2);
    }

    @DexIgnore
    public final boolean a() {
        return this.h;
    }

    @DexIgnore
    public void b() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.h) {
            if (this.g.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.g);
            }
            this.g = map;
            if (this.j.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.j);
            }
            this.j = map2;
            this.h = true;
        }
    }

    @DexIgnore
    public void clear() {
        f();
        if (!this.f.isEmpty()) {
            this.f.clear();
        }
        if (!this.g.isEmpty()) {
            this.g.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return a(comparable) >= 0 || this.g.containsKey(comparable);
    }

    @DexIgnore
    public final Iterable<Map.Entry<K, V>> d() {
        if (this.g.isEmpty()) {
            return ua1.a();
        }
        return this.g.entrySet();
    }

    @DexIgnore
    public final Set<Map.Entry<K, V>> e() {
        if (this.k == null) {
            this.k = new ta1(this, (ra1) null);
        }
        return this.k;
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        if (this.i == null) {
            this.i = new za1(this, (ra1) null);
        }
        return this.i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qa1)) {
            return super.equals(obj);
        }
        qa1 qa1 = (qa1) obj;
        int size = size();
        if (size != qa1.size()) {
            return false;
        }
        int c = c();
        if (c != qa1.c()) {
            return entrySet().equals(qa1.entrySet());
        }
        for (int i2 = 0; i2 < c; i2++) {
            if (!a(i2).equals(qa1.a(i2))) {
                return false;
            }
        }
        if (c != size) {
            return this.g.equals(qa1.g);
        }
        return true;
    }

    @DexIgnore
    public final void f() {
        if (this.h) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final SortedMap<K, V> g() {
        f();
        if (this.g.isEmpty() && !(this.g instanceof TreeMap)) {
            this.g = new TreeMap();
            this.j = ((TreeMap) this.g).descendingMap();
        }
        return (SortedMap) this.g;
    }

    @DexIgnore
    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return this.f.get(a).getValue();
        }
        return this.g.get(comparable);
    }

    @DexIgnore
    public int hashCode() {
        int c = c();
        int i2 = 0;
        for (int i3 = 0; i3 < c; i3++) {
            i2 += this.f.get(i3).hashCode();
        }
        return this.g.size() > 0 ? i2 + this.g.hashCode() : i2;
    }

    @DexIgnore
    public V remove(Object obj) {
        f();
        Comparable comparable = (Comparable) obj;
        int a = a(comparable);
        if (a >= 0) {
            return b(a);
        }
        if (this.g.isEmpty()) {
            return null;
        }
        return this.g.remove(comparable);
    }

    @DexIgnore
    public int size() {
        return this.f.size() + this.g.size();
    }

    @DexIgnore
    public final Map.Entry<K, V> a(int i2) {
        return this.f.get(i2);
    }

    @DexIgnore
    public final int c() {
        return this.f.size();
    }

    @DexIgnore
    /* renamed from: a */
    public final V put(K k2, V v) {
        f();
        int a = a(k2);
        if (a >= 0) {
            return this.f.get(a).setValue(v);
        }
        f();
        if (this.f.isEmpty() && !(this.f instanceof ArrayList)) {
            this.f = new ArrayList(this.e);
        }
        int i2 = -(a + 1);
        if (i2 >= this.e) {
            return g().put(k2, v);
        }
        int size = this.f.size();
        int i3 = this.e;
        if (size == i3) {
            xa1 remove = this.f.remove(i3 - 1);
            g().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.f.add(i2, new xa1(this, k2, v));
        return null;
    }

    @DexIgnore
    public /* synthetic */ qa1(int i2, ra1 ra1) {
        this(i2);
    }

    @DexIgnore
    public final V b(int i2) {
        f();
        V value = this.f.remove(i2).getValue();
        if (!this.g.isEmpty()) {
            Iterator it = g().entrySet().iterator();
            this.f.add(new xa1(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    @DexIgnore
    public final int a(K k2) {
        int size = this.f.size() - 1;
        if (size >= 0) {
            int compareTo = k2.compareTo((Comparable) this.f.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i2 = 0;
        while (i2 <= size) {
            int i3 = (i2 + size) / 2;
            int compareTo2 = k2.compareTo((Comparable) this.f.get(i3).getKey());
            if (compareTo2 < 0) {
                size = i3 - 1;
            } else if (compareTo2 <= 0) {
                return i3;
            } else {
                i2 = i3 + 1;
            }
        }
        return -(i2 + 1);
    }
}
