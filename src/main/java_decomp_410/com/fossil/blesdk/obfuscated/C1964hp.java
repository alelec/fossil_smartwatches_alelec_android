package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.hp */
public class C1964hp {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C1647dp f5815f; // = new com.fossil.blesdk.obfuscated.C1647dp();

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1647dp f5816a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1884gp f5817b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f5818c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.content.ContentResolver f5819d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.List<com.bumptech.glide.load.ImageHeaderParser> f5820e;

    @DexIgnore
    public C1964hp(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, com.fossil.blesdk.obfuscated.C1884gp gpVar, com.fossil.blesdk.obfuscated.C1885gq gqVar, android.content.ContentResolver contentResolver) {
        this(list, f5815f, gpVar, gqVar, contentResolver);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11700a(android.net.Uri uri) {
        java.io.InputStream inputStream = null;
        try {
            java.io.InputStream openInputStream = this.f5819d.openInputStream(uri);
            int a = com.fossil.blesdk.obfuscated.C2049io.m8571a(this.f5820e, openInputStream, this.f5818c);
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (java.io.IOException unused) {
                }
            }
            return a;
        } catch (java.io.IOException | java.lang.NullPointerException e) {
            if (android.util.Log.isLoggable("ThumbStreamOpener", 3)) {
                android.util.Log.d("ThumbStreamOpener", "Failed to open uri: " + uri, e);
            }
            if (inputStream == null) {
                return -1;
            }
            try {
                inputStream.close();
                return -1;
            } catch (java.io.IOException unused2) {
                return -1;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (java.io.IOException unused3) {
                }
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0030 A[Catch:{ all -> 0x004a }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004d  */
    /* renamed from: b */
    public final java.lang.String mo11702b(android.net.Uri uri) {
        android.database.Cursor cursor;
        try {
            cursor = this.f5817b.mo10997a(uri);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        java.lang.String string = cursor.getString(0);
                        if (cursor != null) {
                            cursor.close();
                        }
                        return string;
                    }
                } catch (java.lang.SecurityException e) {
                    e = e;
                    try {
                        if (android.util.Log.isLoggable("ThumbStreamOpener", 3)) {
                            android.util.Log.d("ThumbStreamOpener", "Failed to query for thumbnail for Uri: " + uri, e);
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (java.lang.SecurityException e2) {
            e = e2;
            cursor = null;
            if (android.util.Log.isLoggable("ThumbStreamOpener", 3)) {
            }
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public java.io.InputStream mo11703c(android.net.Uri uri) throws java.io.FileNotFoundException {
        java.lang.String b = mo11702b(uri);
        if (android.text.TextUtils.isEmpty(b)) {
            return null;
        }
        java.io.File a = this.f5816a.mo10088a(b);
        if (!mo11701a(a)) {
            return null;
        }
        android.net.Uri fromFile = android.net.Uri.fromFile(a);
        try {
            return this.f5819d.openInputStream(fromFile);
        } catch (java.lang.NullPointerException e) {
            throw ((java.io.FileNotFoundException) new java.io.FileNotFoundException("NPE opening uri: " + uri + " -> " + fromFile).initCause(e));
        }
    }

    @DexIgnore
    public C1964hp(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, com.fossil.blesdk.obfuscated.C1647dp dpVar, com.fossil.blesdk.obfuscated.C1884gp gpVar, com.fossil.blesdk.obfuscated.C1885gq gqVar, android.content.ContentResolver contentResolver) {
        this.f5816a = dpVar;
        this.f5817b = gpVar;
        this.f5818c = gqVar;
        this.f5819d = contentResolver;
        this.f5820e = list;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11701a(java.io.File file) {
        return this.f5816a.mo10089a(file) && 0 < this.f5816a.mo10090b(file);
    }
}
