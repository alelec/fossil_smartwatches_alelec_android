package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ow */
public final class C2606ow {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.concurrent.Executor f8234a; // = new com.fossil.blesdk.obfuscated.C2606ow.C2607a();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.concurrent.Executor f8235b; // = new com.fossil.blesdk.obfuscated.C2606ow.C2608b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ow$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ow$a */
    public class C2607a implements java.util.concurrent.Executor {

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.os.Handler f8236e; // = new android.os.Handler(android.os.Looper.getMainLooper());

        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            this.f8236e.post(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ow$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ow$b */
    public class C2608b implements java.util.concurrent.Executor {
        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            runnable.run();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.concurrent.Executor m11981a() {
        return f8235b;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.util.concurrent.Executor m11982b() {
        return f8234a;
    }
}
