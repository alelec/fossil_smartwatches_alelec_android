package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn3 implements CoroutineUseCase.a {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;

    @DexIgnore
    public nn3(int i, String str) {
        kd4.b(str, "errorMessage");
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }
}
