package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kf0 implements he0.a {
    @DexIgnore
    public /* final */ /* synthetic */ BasePendingResult a;
    @DexIgnore
    public /* final */ /* synthetic */ jf0 b;

    @DexIgnore
    public kf0(jf0 jf0, BasePendingResult basePendingResult) {
        this.b = jf0;
        this.a = basePendingResult;
    }

    @DexIgnore
    public final void a(Status status) {
        this.b.a.remove(this.a);
    }
}
