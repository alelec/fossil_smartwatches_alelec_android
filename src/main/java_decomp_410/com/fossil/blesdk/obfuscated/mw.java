package com.fossil.blesdk.obfuscated;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mw extends FilterInputStream {
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public int f;

    @DexIgnore
    public mw(InputStream inputStream, long j) {
        super(inputStream);
        this.e = j;
    }

    @DexIgnore
    public static InputStream a(InputStream inputStream, long j) {
        return new mw(inputStream, j);
    }

    @DexIgnore
    public synchronized int available() throws IOException {
        return (int) Math.max(this.e - ((long) this.f), (long) this.in.available());
    }

    @DexIgnore
    public final int b(int i) throws IOException {
        if (i >= 0) {
            this.f += i;
        } else if (this.e - ((long) this.f) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.e + ", but read: " + this.f);
        }
        return i;
    }

    @DexIgnore
    public synchronized int read() throws IOException {
        int read;
        read = super.read();
        b(read >= 0 ? 1 : -1);
        return read;
    }

    @DexIgnore
    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    @DexIgnore
    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int read;
        read = super.read(bArr, i, i2);
        b(read);
        return read;
    }
}
