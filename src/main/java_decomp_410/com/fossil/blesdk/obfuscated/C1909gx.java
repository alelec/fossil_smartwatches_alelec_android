package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gx */
public class C1909gx {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f5577a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.e74 f5578b;

    @DexIgnore
    public C1909gx(android.content.Context context, com.fossil.blesdk.obfuscated.e74 e74) {
        this.f5577a = context;
        this.f5578b = e74;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3395yx mo11420a() throws java.io.IOException {
        if (android.os.Looper.myLooper() != android.os.Looper.getMainLooper()) {
            return new com.fossil.blesdk.obfuscated.C3395yx(this.f5577a, new com.fossil.blesdk.obfuscated.C1666dy(), new com.fossil.blesdk.obfuscated.x54(), new com.fossil.blesdk.obfuscated.u64(this.f5577a, this.f5578b.mo26861a(), "session_analytics.tap", "session_analytics_to_send"));
        }
        throw new java.lang.IllegalStateException("AnswersFilesManagerProvider cannot be called on the main thread");
    }
}
