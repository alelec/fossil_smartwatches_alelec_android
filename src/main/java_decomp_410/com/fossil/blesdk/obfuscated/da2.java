package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class da2 extends ca2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j F; // = new ViewDataBinding.j(18);
    @DexIgnore
    public static /* final */ SparseIntArray G; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public long E;

    /*
    static {
        F.a(1, new String[]{"item_default_place_commute_time", "item_default_place_commute_time"}, new int[]{2, 3}, new int[]{R.layout.item_default_place_commute_time, R.layout.item_default_place_commute_time});
        G.put(R.id.ib_close, 4);
        G.put(R.id.ftv_title, 5);
        G.put(R.id.cl_commute_time, 6);
        G.put(R.id.iv_travel_time, 7);
        G.put(R.id.iv_arrival_time, 8);
        G.put(R.id.autocomplete_places, 9);
        G.put(R.id.close_iv, 10);
        G.put(R.id.line, 11);
        G.put(R.id.ftv_address_error, 12);
        G.put(R.id.ll_recent_container, 13);
        G.put(R.id.rv_recent_addresses, 14);
        G.put(R.id.bottom_line, 15);
        G.put(R.id.ll_avoid_tolls_container, 16);
        G.put(R.id.sc_avoid_tolls, 17);
    }
    */

    @DexIgnore
    public da2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 18, F, G));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.E = 0;
        }
        ViewDataBinding.d(this.u);
        ViewDataBinding.d(this.v);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r6.v.e() == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.u.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.E != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.E = 4;
        }
        this.u.f();
        this.v.f();
        g();
    }

    @DexIgnore
    public da2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 2, objArr[9], objArr[15], objArr[6], objArr[10], objArr[12], objArr[5], objArr[4], objArr[2], objArr[3], objArr[8], objArr[7], objArr[11], objArr[16], objArr[13], objArr[14], objArr[17]);
        this.E = -1;
        this.C = objArr[0];
        this.C.setTag((Object) null);
        this.D = objArr[1];
        this.D.setTag((Object) null);
        a(view);
        f();
    }
}
