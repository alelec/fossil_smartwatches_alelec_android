package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oe */
public class C2552oe extends androidx.recyclerview.widget.RecyclerView.C0235l implements androidx.recyclerview.widget.RecyclerView.C0241n {

    @DexIgnore
    /* renamed from: A */
    public com.fossil.blesdk.obfuscated.C2552oe.C2561g f8053A;

    @DexIgnore
    /* renamed from: B */
    public /* final */ androidx.recyclerview.widget.RecyclerView.C0243p f8054B; // = new com.fossil.blesdk.obfuscated.C2552oe.C2554b();

    @DexIgnore
    /* renamed from: C */
    public android.graphics.Rect f8055C;

    @DexIgnore
    /* renamed from: D */
    public long f8056D;

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<android.view.View> f8057a; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: b */
    public /* final */ float[] f8058b; // = new float[2];

    @DexIgnore
    /* renamed from: c */
    public androidx.recyclerview.widget.RecyclerView.ViewHolder f8059c; // = null;

    @DexIgnore
    /* renamed from: d */
    public float f8060d;

    @DexIgnore
    /* renamed from: e */
    public float f8061e;

    @DexIgnore
    /* renamed from: f */
    public float f8062f;

    @DexIgnore
    /* renamed from: g */
    public float f8063g;

    @DexIgnore
    /* renamed from: h */
    public float f8064h;

    @DexIgnore
    /* renamed from: i */
    public float f8065i;

    @DexIgnore
    /* renamed from: j */
    public float f8066j;

    @DexIgnore
    /* renamed from: k */
    public float f8067k;

    @DexIgnore
    /* renamed from: l */
    public int f8068l; // = -1;

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C2552oe.C2558f f8069m;

    @DexIgnore
    /* renamed from: n */
    public int f8070n; // = 0;

    @DexIgnore
    /* renamed from: o */
    public int f8071o;

    @DexIgnore
    /* renamed from: p */
    public java.util.List<com.fossil.blesdk.obfuscated.C2552oe.C2562h> f8072p; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: q */
    public int f8073q;

    @DexIgnore
    /* renamed from: r */
    public androidx.recyclerview.widget.RecyclerView f8074r;

    @DexIgnore
    /* renamed from: s */
    public /* final */ java.lang.Runnable f8075s; // = new com.fossil.blesdk.obfuscated.C2552oe.C2553a();

    @DexIgnore
    /* renamed from: t */
    public android.view.VelocityTracker f8076t;

    @DexIgnore
    /* renamed from: u */
    public java.util.List<androidx.recyclerview.widget.RecyclerView.ViewHolder> f8077u;

    @DexIgnore
    /* renamed from: v */
    public java.util.List<java.lang.Integer> f8078v;

    @DexIgnore
    /* renamed from: w */
    public androidx.recyclerview.widget.RecyclerView.C0228h f8079w; // = null;

    @DexIgnore
    /* renamed from: x */
    public android.view.View f8080x; // = null;

    @DexIgnore
    /* renamed from: y */
    public int f8081y; // = -1;

    @DexIgnore
    /* renamed from: z */
    public com.fossil.blesdk.obfuscated.C2534o8 f8082z;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$a")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$a */
    public class C2553a implements java.lang.Runnable {
        @DexIgnore
        public C2553a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2552oe oeVar = com.fossil.blesdk.obfuscated.C2552oe.this;
            if (oeVar.f8059c != null && oeVar.mo14361f()) {
                com.fossil.blesdk.obfuscated.C2552oe oeVar2 = com.fossil.blesdk.obfuscated.C2552oe.this;
                androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = oeVar2.f8059c;
                if (viewHolder != null) {
                    oeVar2.mo14353b(viewHolder);
                }
                com.fossil.blesdk.obfuscated.C2552oe oeVar3 = com.fossil.blesdk.obfuscated.C2552oe.this;
                oeVar3.f8074r.removeCallbacks(oeVar3.f8075s);
                com.fossil.blesdk.obfuscated.C1776f9.m6816a((android.view.View) com.fossil.blesdk.obfuscated.C2552oe.this.f8074r, (java.lang.Runnable) this);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$b")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$b */
    public class C2554b implements androidx.recyclerview.widget.RecyclerView.C0243p {
        @DexIgnore
        public C2554b() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo2987a(androidx.recyclerview.widget.RecyclerView recyclerView, android.view.MotionEvent motionEvent) {
            com.fossil.blesdk.obfuscated.C2552oe.this.f8082z.mo14284a(motionEvent);
            android.view.VelocityTracker velocityTracker = com.fossil.blesdk.obfuscated.C2552oe.this.f8076t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (com.fossil.blesdk.obfuscated.C2552oe.this.f8068l != -1) {
                int actionMasked = motionEvent.getActionMasked();
                int findPointerIndex = motionEvent.findPointerIndex(com.fossil.blesdk.obfuscated.C2552oe.this.f8068l);
                if (findPointerIndex >= 0) {
                    com.fossil.blesdk.obfuscated.C2552oe.this.mo14344a(actionMasked, motionEvent, findPointerIndex);
                }
                com.fossil.blesdk.obfuscated.C2552oe oeVar = com.fossil.blesdk.obfuscated.C2552oe.this;
                androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = oeVar.f8059c;
                if (viewHolder != null) {
                    int i = 0;
                    if (actionMasked != 1) {
                        if (actionMasked != 2) {
                            if (actionMasked == 3) {
                                android.view.VelocityTracker velocityTracker2 = oeVar.f8076t;
                                if (velocityTracker2 != null) {
                                    velocityTracker2.clear();
                                }
                            } else if (actionMasked == 6) {
                                int actionIndex = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex) == com.fossil.blesdk.obfuscated.C2552oe.this.f8068l) {
                                    if (actionIndex == 0) {
                                        i = 1;
                                    }
                                    com.fossil.blesdk.obfuscated.C2552oe.this.f8068l = motionEvent.getPointerId(i);
                                    com.fossil.blesdk.obfuscated.C2552oe oeVar2 = com.fossil.blesdk.obfuscated.C2552oe.this;
                                    oeVar2.mo14345a(motionEvent, oeVar2.f8071o, actionIndex);
                                    return;
                                }
                                return;
                            } else {
                                return;
                            }
                        } else if (findPointerIndex >= 0) {
                            oeVar.mo14345a(motionEvent, oeVar.f8071o, findPointerIndex);
                            com.fossil.blesdk.obfuscated.C2552oe.this.mo14353b(viewHolder);
                            com.fossil.blesdk.obfuscated.C2552oe oeVar3 = com.fossil.blesdk.obfuscated.C2552oe.this;
                            oeVar3.f8074r.removeCallbacks(oeVar3.f8075s);
                            com.fossil.blesdk.obfuscated.C2552oe.this.f8075s.run();
                            com.fossil.blesdk.obfuscated.C2552oe.this.f8074r.invalidate();
                            return;
                        } else {
                            return;
                        }
                    }
                    com.fossil.blesdk.obfuscated.C2552oe.this.mo14357c((androidx.recyclerview.widget.RecyclerView.ViewHolder) null, 0);
                    com.fossil.blesdk.obfuscated.C2552oe.this.f8068l = -1;
                }
            }
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo2989b(androidx.recyclerview.widget.RecyclerView recyclerView, android.view.MotionEvent motionEvent) {
            com.fossil.blesdk.obfuscated.C2552oe.this.f8082z.mo14284a(motionEvent);
            int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                com.fossil.blesdk.obfuscated.C2552oe.this.f8068l = motionEvent.getPointerId(0);
                com.fossil.blesdk.obfuscated.C2552oe.this.f8060d = motionEvent.getX();
                com.fossil.blesdk.obfuscated.C2552oe.this.f8061e = motionEvent.getY();
                com.fossil.blesdk.obfuscated.C2552oe.this.mo14359d();
                com.fossil.blesdk.obfuscated.C2552oe oeVar = com.fossil.blesdk.obfuscated.C2552oe.this;
                if (oeVar.f8059c == null) {
                    com.fossil.blesdk.obfuscated.C2552oe.C2562h a = oeVar.mo14341a(motionEvent);
                    if (a != null) {
                        com.fossil.blesdk.obfuscated.C2552oe oeVar2 = com.fossil.blesdk.obfuscated.C2552oe.this;
                        oeVar2.f8060d -= a.f8105i;
                        oeVar2.f8061e -= a.f8106j;
                        oeVar2.mo14346a(a.f8101e, true);
                        if (com.fossil.blesdk.obfuscated.C2552oe.this.f8057a.remove(a.f8101e.itemView)) {
                            com.fossil.blesdk.obfuscated.C2552oe oeVar3 = com.fossil.blesdk.obfuscated.C2552oe.this;
                            oeVar3.f8069m.mo14379a(oeVar3.f8074r, a.f8101e);
                        }
                        com.fossil.blesdk.obfuscated.C2552oe.this.mo14357c(a.f8101e, a.f8102f);
                        com.fossil.blesdk.obfuscated.C2552oe oeVar4 = com.fossil.blesdk.obfuscated.C2552oe.this;
                        oeVar4.mo14345a(motionEvent, oeVar4.f8071o, 0);
                    }
                }
            } else if (actionMasked == 3 || actionMasked == 1) {
                com.fossil.blesdk.obfuscated.C2552oe oeVar5 = com.fossil.blesdk.obfuscated.C2552oe.this;
                oeVar5.f8068l = -1;
                oeVar5.mo14357c((androidx.recyclerview.widget.RecyclerView.ViewHolder) null, 0);
            } else {
                int i = com.fossil.blesdk.obfuscated.C2552oe.this.f8068l;
                if (i != -1) {
                    int findPointerIndex = motionEvent.findPointerIndex(i);
                    if (findPointerIndex >= 0) {
                        com.fossil.blesdk.obfuscated.C2552oe.this.mo14344a(actionMasked, motionEvent, findPointerIndex);
                    }
                }
            }
            android.view.VelocityTracker velocityTracker = com.fossil.blesdk.obfuscated.C2552oe.this.f8076t;
            if (velocityTracker != null) {
                velocityTracker.addMovement(motionEvent);
            }
            if (com.fossil.blesdk.obfuscated.C2552oe.this.f8059c != null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo2988a(boolean z) {
            if (z) {
                com.fossil.blesdk.obfuscated.C2552oe.this.mo14357c((androidx.recyclerview.widget.RecyclerView.ViewHolder) null, 0);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$c")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$c */
    public class C2555c extends com.fossil.blesdk.obfuscated.C2552oe.C2562h {

        @DexIgnore
        /* renamed from: n */
        public /* final */ /* synthetic */ int f8085n;

        @DexIgnore
        /* renamed from: o */
        public /* final */ /* synthetic */ androidx.recyclerview.widget.RecyclerView.ViewHolder f8086o;

        @DexIgnore
        /* renamed from: p */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2552oe f8087p;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C2555c(com.fossil.blesdk.obfuscated.C2552oe oeVar, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, int i2, float f, float f2, float f3, float f4, int i3, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2) {
            super(viewHolder, i, i2, f, f2, f3, f4);
            this.f8087p = oeVar;
            this.f8085n = i3;
            this.f8086o = viewHolder2;
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            super.onAnimationEnd(animator);
            if (!this.f8107k) {
                if (this.f8085n <= 0) {
                    com.fossil.blesdk.obfuscated.C2552oe oeVar = this.f8087p;
                    oeVar.f8069m.mo14379a(oeVar.f8074r, this.f8086o);
                } else {
                    this.f8087p.f8057a.add(this.f8086o.itemView);
                    this.f8104h = true;
                    int i = this.f8085n;
                    if (i > 0) {
                        this.f8087p.mo14348a((com.fossil.blesdk.obfuscated.C2552oe.C2562h) this, i);
                    }
                }
                com.fossil.blesdk.obfuscated.C2552oe oeVar2 = this.f8087p;
                android.view.View view = oeVar2.f8080x;
                android.view.View view2 = this.f8086o.itemView;
                if (view == view2) {
                    oeVar2.mo14356c(view2);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$d")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$d */
    public class C2556d implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2552oe.C2562h f8088e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ int f8089f;

        @DexIgnore
        public C2556d(com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar, int i) {
            this.f8088e = hVar;
            this.f8089f = i;
        }

        @DexIgnore
        public void run() {
            androidx.recyclerview.widget.RecyclerView recyclerView = com.fossil.blesdk.obfuscated.C2552oe.this.f8074r;
            if (recyclerView != null && recyclerView.isAttachedToWindow()) {
                com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar = this.f8088e;
                if (!hVar.f8107k && hVar.f8101e.getAdapterPosition() != -1) {
                    androidx.recyclerview.widget.RecyclerView.C0230j itemAnimator = com.fossil.blesdk.obfuscated.C2552oe.this.f8074r.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.isRunning((androidx.recyclerview.widget.RecyclerView.C0230j.C0231a) null)) && !com.fossil.blesdk.obfuscated.C2552oe.this.mo14358c()) {
                        com.fossil.blesdk.obfuscated.C2552oe.this.f8069m.mo14387b(this.f8088e.f8101e, this.f8089f);
                    } else {
                        com.fossil.blesdk.obfuscated.C2552oe.this.f8074r.post(this);
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$e")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$e */
    public class C2557e implements androidx.recyclerview.widget.RecyclerView.C0228h {
        @DexIgnore
        public C2557e() {
        }

        @DexIgnore
        /* renamed from: a */
        public int mo2840a(int i, int i2) {
            com.fossil.blesdk.obfuscated.C2552oe oeVar = com.fossil.blesdk.obfuscated.C2552oe.this;
            android.view.View view = oeVar.f8080x;
            if (view == null) {
                return i2;
            }
            int i3 = oeVar.f8081y;
            if (i3 == -1) {
                i3 = oeVar.f8074r.indexOfChild(view);
                com.fossil.blesdk.obfuscated.C2552oe.this.f8081y = i3;
            }
            if (i2 == i - 1) {
                return i3;
            }
            return i2 < i3 ? i2 : i2 + 1;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$f")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$f */
    public static abstract class C2558f {

        @DexIgnore
        /* renamed from: b */
        public static /* final */ android.view.animation.Interpolator f8092b; // = new com.fossil.blesdk.obfuscated.C2552oe.C2558f.C2559a();

        @DexIgnore
        /* renamed from: c */
        public static /* final */ android.view.animation.Interpolator f8093c; // = new com.fossil.blesdk.obfuscated.C2552oe.C2558f.C2560b();

        @DexIgnore
        /* renamed from: a */
        public int f8094a; // = -1;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$f$a")
        /* renamed from: com.fossil.blesdk.obfuscated.oe$f$a */
        public static class C2559a implements android.view.animation.Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                return f * f * f * f * f;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$f$b")
        /* renamed from: com.fossil.blesdk.obfuscated.oe$f$b */
        public static class C2560b implements android.view.animation.Interpolator {
            @DexIgnore
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public static int m11724b(int i, int i2) {
            int i3;
            int i4 = i & 789516;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 << 2;
            } else {
                int i6 = i4 << 1;
                i5 |= -789517 & i6;
                i3 = (i6 & 789516) << 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        /* renamed from: c */
        public static int m11725c(int i, int i2) {
            return i2 << (i * 8);
        }

        @DexIgnore
        /* renamed from: d */
        public static int m11726d(int i, int i2) {
            int c = m11725c(0, i2 | i);
            return m11725c(2, i) | m11725c(1, i2) | c;
        }

        @DexIgnore
        /* renamed from: a */
        public float mo14368a(float f) {
            return f;
        }

        @DexIgnore
        /* renamed from: a */
        public float mo14369a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo14370a() {
            return 0;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo14371a(int i, int i2) {
            int i3;
            int i4 = i & 3158064;
            if (i4 == 0) {
                return i;
            }
            int i5 = i & (~i4);
            if (i2 == 0) {
                i3 = i4 >> 2;
            } else {
                int i6 = i4 >> 1;
                i5 |= -3158065 & i6;
                i3 = (i6 & 3158064) >> 2;
            }
            return i5 | i3;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0056  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0078  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x009a  */
        /* renamed from: a */
        public androidx.recyclerview.widget.RecyclerView.ViewHolder mo14375a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, java.util.List<androidx.recyclerview.widget.RecyclerView.ViewHolder> list, int i, int i2) {
            int i3;
            androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2 = viewHolder;
            int width = i + viewHolder2.itemView.getWidth();
            int height = i2 + viewHolder2.itemView.getHeight();
            int left = i - viewHolder2.itemView.getLeft();
            int top = i2 - viewHolder2.itemView.getTop();
            int size = list.size();
            androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder3 = null;
            int i4 = -1;
            for (int i5 = 0; i5 < size; i5++) {
                androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder4 = list.get(i5);
                if (left > 0) {
                    int right = viewHolder4.itemView.getRight() - width;
                    if (right < 0 && viewHolder4.itemView.getRight() > viewHolder2.itemView.getRight()) {
                        i3 = java.lang.Math.abs(right);
                        if (i3 > i4) {
                            viewHolder3 = viewHolder4;
                            if (left < 0) {
                                int left2 = viewHolder4.itemView.getLeft() - i;
                                if (left2 > 0 && viewHolder4.itemView.getLeft() < viewHolder2.itemView.getLeft()) {
                                    int abs = java.lang.Math.abs(left2);
                                    if (abs > i3) {
                                        i3 = abs;
                                        viewHolder3 = viewHolder4;
                                    }
                                }
                            }
                            if (top < 0) {
                                int top2 = viewHolder4.itemView.getTop() - i2;
                                if (top2 > 0 && viewHolder4.itemView.getTop() < viewHolder2.itemView.getTop()) {
                                    int abs2 = java.lang.Math.abs(top2);
                                    if (abs2 > i3) {
                                        i3 = abs2;
                                        viewHolder3 = viewHolder4;
                                    }
                                }
                            }
                            if (top > 0) {
                                int bottom = viewHolder4.itemView.getBottom() - height;
                                if (bottom < 0 && viewHolder4.itemView.getBottom() > viewHolder2.itemView.getBottom()) {
                                    i4 = java.lang.Math.abs(bottom);
                                    if (i4 > i3) {
                                        viewHolder3 = viewHolder4;
                                    }
                                }
                            }
                            i4 = i3;
                        }
                    }
                }
                i3 = i4;
                if (left < 0) {
                }
                if (top < 0) {
                }
                if (top > 0) {
                }
                i4 = i3;
            }
            return viewHolder3;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14381a(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2) {
            return true;
        }

        @DexIgnore
        /* renamed from: b */
        public float mo14382b(float f) {
            return f;
        }

        @DexIgnore
        /* renamed from: b */
        public float mo14383b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo14384b(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return mo14371a(mo14390c(recyclerView, viewHolder), com.fossil.blesdk.obfuscated.C1776f9.m6845k(recyclerView));
        }

        @DexIgnore
        /* renamed from: b */
        public abstract void mo14387b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i);

        @DexIgnore
        /* renamed from: b */
        public boolean mo14388b() {
            return true;
        }

        @DexIgnore
        /* renamed from: b */
        public abstract boolean mo14389b(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2);

        @DexIgnore
        /* renamed from: c */
        public abstract int mo14390c(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder);

        @DexIgnore
        /* renamed from: c */
        public boolean mo14391c() {
            return true;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo14386b(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, java.util.List<com.fossil.blesdk.obfuscated.C2552oe.C2562h> list, int i, float f, float f2) {
            android.graphics.Canvas canvas2 = canvas;
            java.util.List<com.fossil.blesdk.obfuscated.C2552oe.C2562h> list2 = list;
            int size = list.size();
            boolean z = false;
            for (int i2 = 0; i2 < size; i2++) {
                com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar = list2.get(i2);
                int save = canvas.save();
                mo14385b(canvas, recyclerView, hVar.f8101e, hVar.f8105i, hVar.f8106j, hVar.f8102f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                mo14385b(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
            for (int i3 = size - 1; i3 >= 0; i3--) {
                com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar2 = list2.get(i3);
                if (hVar2.f8108l && !hVar2.f8104h) {
                    list2.remove(i3);
                } else if (!hVar2.f8108l) {
                    z = true;
                }
            }
            if (z) {
                recyclerView.invalidate();
            }
        }

        @DexIgnore
        /* renamed from: d */
        public boolean mo14392d(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return (mo14384b(recyclerView, viewHolder) & 16711680) != 0;
        }

        @DexIgnore
        /* renamed from: b */
        public void mo14385b(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            com.fossil.blesdk.obfuscated.C2731qe.f8641a.mo14671a(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14378a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i) {
            if (viewHolder != null) {
                com.fossil.blesdk.obfuscated.C2731qe.f8641a.mo14674b(viewHolder.itemView);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo14372a(androidx.recyclerview.widget.RecyclerView recyclerView) {
            if (this.f8094a == -1) {
                this.f8094a = recyclerView.getResources().getDimensionPixelSize(com.fossil.blesdk.obfuscated.C1550ce.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.f8094a;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14380a(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2, int i2, int i3, int i4) {
            androidx.recyclerview.widget.RecyclerView.C0236m layoutManager = recyclerView.getLayoutManager();
            if (layoutManager instanceof com.fossil.blesdk.obfuscated.C2552oe.C2565j) {
                ((com.fossil.blesdk.obfuscated.C2552oe.C2565j) layoutManager).mo2417a(viewHolder.itemView, viewHolder2.itemView, i3, i4);
                return;
            }
            if (layoutManager.mo2426a()) {
                if (layoutManager.mo2945f(viewHolder2.itemView) <= recyclerView.getPaddingLeft()) {
                    recyclerView.mo2616i(i2);
                }
                if (layoutManager.mo2956i(viewHolder2.itemView) >= recyclerView.getWidth() - recyclerView.getPaddingRight()) {
                    recyclerView.mo2616i(i2);
                }
            }
            if (layoutManager.mo2437b()) {
                if (layoutManager.mo2958j(viewHolder2.itemView) <= recyclerView.getPaddingTop()) {
                    recyclerView.mo2616i(i2);
                }
                if (layoutManager.mo2942e(viewHolder2.itemView) >= recyclerView.getHeight() - recyclerView.getPaddingBottom()) {
                    recyclerView.mo2616i(i2);
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14377a(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, java.util.List<com.fossil.blesdk.obfuscated.C2552oe.C2562h> list, int i, float f, float f2) {
            android.graphics.Canvas canvas2 = canvas;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar = list.get(i2);
                hVar.mo14402c();
                int save = canvas.save();
                mo14376a(canvas, recyclerView, hVar.f8101e, hVar.f8105i, hVar.f8106j, hVar.f8102f, false);
                canvas.restoreToCount(save);
            }
            if (viewHolder != null) {
                int save2 = canvas.save();
                mo14376a(canvas, recyclerView, viewHolder, f, f2, i, true);
                canvas.restoreToCount(save2);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14379a(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            com.fossil.blesdk.obfuscated.C2731qe.f8641a.mo14672a(viewHolder.itemView);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14376a(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            com.fossil.blesdk.obfuscated.C2731qe.f8641a.mo14673b(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        @DexIgnore
        /* renamed from: a */
        public long mo14374a(androidx.recyclerview.widget.RecyclerView recyclerView, int i, float f, float f2) {
            androidx.recyclerview.widget.RecyclerView.C0230j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return i == 8 ? 200 : 250;
            }
            if (i == 8) {
                return itemAnimator.getMoveDuration();
            }
            return itemAnimator.getRemoveDuration();
        }

        @DexIgnore
        /* renamed from: a */
        public int mo14373a(androidx.recyclerview.widget.RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f = 1.0f;
            int signum = (int) (((float) (((int) java.lang.Math.signum((float) i2)) * mo14372a(recyclerView))) * f8093c.getInterpolation(java.lang.Math.min(1.0f, (((float) java.lang.Math.abs(i2)) * 1.0f) / ((float) i))));
            if (j <= 2000) {
                f = ((float) j) / 2000.0f;
            }
            int interpolation = (int) (((float) signum) * f8092b.getInterpolation(f));
            if (interpolation == 0) {
                return i2 > 0 ? 1 : -1;
            }
            return interpolation;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$g")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$g */
    public class C2561g extends android.view.GestureDetector.SimpleOnGestureListener {

        @DexIgnore
        /* renamed from: e */
        public boolean f8095e; // = true;

        @DexIgnore
        public C2561g() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14395a() {
            this.f8095e = false;
        }

        @DexIgnore
        public boolean onDown(android.view.MotionEvent motionEvent) {
            return true;
        }

        @DexIgnore
        public void onLongPress(android.view.MotionEvent motionEvent) {
            if (this.f8095e) {
                android.view.View b = com.fossil.blesdk.obfuscated.C2552oe.this.mo14351b(motionEvent);
                if (b != null) {
                    androidx.recyclerview.widget.RecyclerView.ViewHolder g = com.fossil.blesdk.obfuscated.C2552oe.this.f8074r.mo2586g(b);
                    if (g != null) {
                        com.fossil.blesdk.obfuscated.C2552oe oeVar = com.fossil.blesdk.obfuscated.C2552oe.this;
                        if (oeVar.f8069m.mo14392d(oeVar.f8074r, g)) {
                            int pointerId = motionEvent.getPointerId(0);
                            int i = com.fossil.blesdk.obfuscated.C2552oe.this.f8068l;
                            if (pointerId == i) {
                                int findPointerIndex = motionEvent.findPointerIndex(i);
                                float x = motionEvent.getX(findPointerIndex);
                                float y = motionEvent.getY(findPointerIndex);
                                com.fossil.blesdk.obfuscated.C2552oe oeVar2 = com.fossil.blesdk.obfuscated.C2552oe.this;
                                oeVar2.f8060d = x;
                                oeVar2.f8061e = y;
                                oeVar2.f8065i = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                oeVar2.f8064h = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                if (oeVar2.f8069m.mo14391c()) {
                                    com.fossil.blesdk.obfuscated.C2552oe.this.mo14357c(g, 2);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$h")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$h */
    public static class C2562h implements android.animation.Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ float f8097a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ float f8098b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ float f8099c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ float f8100d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ androidx.recyclerview.widget.RecyclerView.ViewHolder f8101e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ int f8102f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ android.animation.ValueAnimator f8103g;

        @DexIgnore
        /* renamed from: h */
        public boolean f8104h;

        @DexIgnore
        /* renamed from: i */
        public float f8105i;

        @DexIgnore
        /* renamed from: j */
        public float f8106j;

        @DexIgnore
        /* renamed from: k */
        public boolean f8107k; // = false;

        @DexIgnore
        /* renamed from: l */
        public boolean f8108l; // = false;

        @DexIgnore
        /* renamed from: m */
        public float f8109m;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$h$a")
        /* renamed from: com.fossil.blesdk.obfuscated.oe$h$a */
        public class C2563a implements android.animation.ValueAnimator.AnimatorUpdateListener {
            @DexIgnore
            public C2563a() {
            }

            @DexIgnore
            public void onAnimationUpdate(android.animation.ValueAnimator valueAnimator) {
                com.fossil.blesdk.obfuscated.C2552oe.C2562h.this.mo14399a(valueAnimator.getAnimatedFraction());
            }
        }

        @DexIgnore
        public C2562h(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i, int i2, float f, float f2, float f3, float f4) {
            this.f8102f = i2;
            this.f8101e = viewHolder;
            this.f8097a = f;
            this.f8098b = f2;
            this.f8099c = f3;
            this.f8100d = f4;
            this.f8103g = android.animation.ValueAnimator.ofFloat(new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
            this.f8103g.addUpdateListener(new com.fossil.blesdk.obfuscated.C2552oe.C2562h.C2563a());
            this.f8103g.setTarget(viewHolder.itemView);
            this.f8103g.addListener(this);
            mo14399a((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14400a(long j) {
            this.f8103g.setDuration(j);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo14401b() {
            this.f8101e.setIsRecyclable(false);
            this.f8103g.start();
        }

        @DexIgnore
        /* renamed from: c */
        public void mo14402c() {
            float f = this.f8097a;
            float f2 = this.f8099c;
            if (f == f2) {
                this.f8105i = this.f8101e.itemView.getTranslationX();
            } else {
                this.f8105i = f + (this.f8109m * (f2 - f));
            }
            float f3 = this.f8098b;
            float f4 = this.f8100d;
            if (f3 == f4) {
                this.f8106j = this.f8101e.itemView.getTranslationY();
            } else {
                this.f8106j = f3 + (this.f8109m * (f4 - f3));
            }
        }

        @DexIgnore
        public void onAnimationCancel(android.animation.Animator animator) {
            mo14399a(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(android.animation.Animator animator) {
            if (!this.f8108l) {
                this.f8101e.setIsRecyclable(true);
            }
            this.f8108l = true;
        }

        @DexIgnore
        public void onAnimationRepeat(android.animation.Animator animator) {
        }

        @DexIgnore
        public void onAnimationStart(android.animation.Animator animator) {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14398a() {
            this.f8103g.cancel();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14399a(float f) {
            this.f8109m = f;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oe$i")
    /* renamed from: com.fossil.blesdk.obfuscated.oe$i */
    public static abstract class C2564i extends com.fossil.blesdk.obfuscated.C2552oe.C2558f {

        @DexIgnore
        /* renamed from: d */
        public int f8111d;

        @DexIgnore
        /* renamed from: e */
        public int f8112e;

        @DexIgnore
        public C2564i(int i, int i2) {
            this.f8111d = i2;
            this.f8112e = i;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo14390c(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return com.fossil.blesdk.obfuscated.C2552oe.C2558f.m11726d(mo14407e(recyclerView, viewHolder), mo14408f(recyclerView, viewHolder));
        }

        @DexIgnore
        /* renamed from: e */
        public int mo14407e(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return this.f8112e;
        }

        @DexIgnore
        /* renamed from: f */
        public int mo14408f(androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
            return this.f8111d;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.oe$j */
    public interface C2565j {
        @DexIgnore
        /* renamed from: a */
        void mo2417a(android.view.View view, android.view.View view2, int i, int i2);
    }

    @DexIgnore
    public C2552oe(com.fossil.blesdk.obfuscated.C2552oe.C2558f fVar) {
        this.f8069m = fVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m11692a(android.view.View view, float f, float f2, float f3, float f4) {
        return f >= f3 && f <= f3 + ((float) view.getWidth()) && f2 >= f4 && f2 <= f4 + ((float) view.getHeight());
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo14352b() {
        this.f8074r.mo2543b((androidx.recyclerview.widget.RecyclerView.C0235l) this);
        this.f8074r.mo2545b(this.f8054B);
        this.f8074r.mo2544b((androidx.recyclerview.widget.RecyclerView.C0241n) this);
        for (int size = this.f8072p.size() - 1; size >= 0; size--) {
            this.f8069m.mo14379a(this.f8074r, this.f8072p.get(0).f8101e);
        }
        this.f8072p.clear();
        this.f8080x = null;
        this.f8081y = -1;
        mo14360e();
        mo14364i();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo2985b(android.view.View view) {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:47:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0137  */
    /* renamed from: c */
    public void mo14357c(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i) {
        boolean z;
        boolean z2;
        android.view.ViewParent parent;
        int i2;
        float f;
        float f2;
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int i3 = i;
        if (viewHolder2 != this.f8059c || i3 != this.f8070n) {
            this.f8056D = Long.MIN_VALUE;
            int i4 = this.f8070n;
            mo14346a(viewHolder2, true);
            this.f8070n = i3;
            if (i3 == 2) {
                if (viewHolder2 != null) {
                    this.f8080x = viewHolder2.itemView;
                    mo14343a();
                } else {
                    throw new java.lang.IllegalArgumentException("Must pass a ViewHolder when dragging");
                }
            }
            int i5 = (1 << ((i3 * 8) + 8)) - 1;
            androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder3 = this.f8059c;
            if (viewHolder3 != null) {
                if (viewHolder3.itemView.getParent() != null) {
                    if (i4 == 2) {
                        i2 = 0;
                    } else {
                        i2 = mo14354c(viewHolder3);
                    }
                    mo14360e();
                    if (i2 == 1 || i2 == 2) {
                        f = java.lang.Math.signum(this.f8065i) * ((float) this.f8074r.getHeight());
                        f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    } else {
                        if (i2 == 4 || i2 == 8 || i2 == 16 || i2 == 32) {
                            f2 = java.lang.Math.signum(this.f8064h) * ((float) this.f8074r.getWidth());
                        } else {
                            f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        }
                        f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                    }
                    int i6 = i4 == 2 ? 8 : i2 > 0 ? 2 : 4;
                    mo14349a(this.f8058b);
                    float[] fArr = this.f8058b;
                    float f3 = fArr[0];
                    float f4 = fArr[1];
                    com.fossil.blesdk.obfuscated.C2552oe.C2555c cVar = r0;
                    com.fossil.blesdk.obfuscated.C2552oe.C2555c cVar2 = new com.fossil.blesdk.obfuscated.C2552oe.C2555c(this, viewHolder3, i6, i4, f3, f4, f2, f, i2, viewHolder3);
                    cVar.mo14400a(this.f8069m.mo14374a(this.f8074r, i6, f2 - f3, f - f4));
                    this.f8072p.add(cVar);
                    cVar.mo14401b();
                    z = true;
                } else {
                    androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder4 = viewHolder3;
                    mo14356c(viewHolder4.itemView);
                    this.f8069m.mo14379a(this.f8074r, viewHolder4);
                    z = false;
                }
                this.f8059c = null;
            } else {
                z = false;
            }
            if (viewHolder2 != null) {
                this.f8071o = (this.f8069m.mo14384b(this.f8074r, viewHolder2) & i5) >> (this.f8070n * 8);
                this.f8066j = (float) viewHolder2.itemView.getLeft();
                this.f8067k = (float) viewHolder2.itemView.getTop();
                this.f8059c = viewHolder2;
                if (i3 == 2) {
                    z2 = false;
                    this.f8059c.itemView.performHapticFeedback(0);
                    parent = this.f8074r.getParent();
                    if (parent != null) {
                        if (this.f8059c != null) {
                            z2 = true;
                        }
                        parent.requestDisallowInterceptTouchEvent(z2);
                    }
                    if (!z) {
                        this.f8074r.getLayoutManager().mo2880A();
                    }
                    this.f8069m.mo14378a(this.f8059c, this.f8070n);
                    this.f8074r.invalidate();
                }
            }
            z2 = false;
            parent = this.f8074r.getParent();
            if (parent != null) {
            }
            if (!z) {
            }
            this.f8069m.mo14378a(this.f8059c, this.f8070n);
            this.f8074r.invalidate();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo14359d() {
        android.view.VelocityTracker velocityTracker = this.f8076t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
        }
        this.f8076t = android.view.VelocityTracker.obtain();
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo14360e() {
        android.view.VelocityTracker velocityTracker = this.f8076t;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.f8076t = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c5, code lost:
        if (r1 > 0) goto L_0x00c9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0104 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0110  */
    /* renamed from: f */
    public boolean mo14361f() {
        int i;
        int i2;
        int i3;
        if (this.f8059c == null) {
            this.f8056D = Long.MIN_VALUE;
            return false;
        }
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        long j = this.f8056D;
        long j2 = j == Long.MIN_VALUE ? 0 : currentTimeMillis - j;
        androidx.recyclerview.widget.RecyclerView.C0236m layoutManager = this.f8074r.getLayoutManager();
        if (this.f8055C == null) {
            this.f8055C = new android.graphics.Rect();
        }
        layoutManager.mo2891a(this.f8059c.itemView, this.f8055C);
        if (layoutManager.mo2426a()) {
            int i4 = (int) (this.f8066j + this.f8064h);
            int paddingLeft = (i4 - this.f8055C.left) - this.f8074r.getPaddingLeft();
            if (this.f8064h < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && paddingLeft < 0) {
                i = paddingLeft;
                if (layoutManager.mo2437b()) {
                }
                i2 = 0;
                if (i != 0) {
                }
                int i5 = i;
                if (i2 == 0) {
                }
                if (i3 == 0) {
                }
                if (this.f8056D == Long.MIN_VALUE) {
                }
                this.f8074r.scrollBy(i3, i2);
                return true;
            } else if (this.f8064h > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                int width = ((i4 + this.f8059c.itemView.getWidth()) + this.f8055C.right) - (this.f8074r.getWidth() - this.f8074r.getPaddingRight());
                if (width > 0) {
                    i = width;
                    if (layoutManager.mo2437b()) {
                        int i6 = (int) (this.f8067k + this.f8065i);
                        int paddingTop = (i6 - this.f8055C.top) - this.f8074r.getPaddingTop();
                        if (this.f8065i < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && paddingTop < 0) {
                            i2 = paddingTop;
                            if (i != 0) {
                                i = this.f8069m.mo14373a(this.f8074r, this.f8059c.itemView.getWidth(), i, this.f8074r.getWidth(), j2);
                            }
                            int i52 = i;
                            if (i2 == 0) {
                                int a = this.f8069m.mo14373a(this.f8074r, this.f8059c.itemView.getHeight(), i2, this.f8074r.getHeight(), j2);
                                i3 = i52;
                                i2 = a;
                            } else {
                                i3 = i52;
                            }
                            if (i3 == 0 || i2 != 0) {
                                if (this.f8056D == Long.MIN_VALUE) {
                                    this.f8056D = currentTimeMillis;
                                }
                                this.f8074r.scrollBy(i3, i2);
                                return true;
                            }
                            this.f8056D = Long.MIN_VALUE;
                            return false;
                        } else if (this.f8065i > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            i2 = ((i6 + this.f8059c.itemView.getHeight()) + this.f8055C.bottom) - (this.f8074r.getHeight() - this.f8074r.getPaddingBottom());
                        }
                    }
                    i2 = 0;
                    if (i != 0) {
                    }
                    int i522 = i;
                    if (i2 == 0) {
                    }
                    if (i3 == 0) {
                    }
                    if (this.f8056D == Long.MIN_VALUE) {
                    }
                    this.f8074r.scrollBy(i3, i2);
                    return true;
                }
            }
        }
        i = 0;
        if (layoutManager.mo2437b()) {
        }
        i2 = 0;
        if (i != 0) {
        }
        int i5222 = i;
        if (i2 == 0) {
        }
        if (i3 == 0) {
        }
        if (this.f8056D == Long.MIN_VALUE) {
        }
        this.f8074r.scrollBy(i3, i2);
        return true;
    }

    @DexIgnore
    /* renamed from: g */
    public final void mo14362g() {
        this.f8073q = android.view.ViewConfiguration.get(this.f8074r.getContext()).getScaledTouchSlop();
        this.f8074r.mo2522a((androidx.recyclerview.widget.RecyclerView.C0235l) this);
        this.f8074r.mo2525a(this.f8054B);
        this.f8074r.mo2524a((androidx.recyclerview.widget.RecyclerView.C0241n) this);
        mo14363h();
    }

    @DexIgnore
    public void getItemOffsets(android.graphics.Rect rect, android.view.View view, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.State state) {
        rect.setEmpty();
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo14363h() {
        this.f8053A = new com.fossil.blesdk.obfuscated.C2552oe.C2561g();
        this.f8082z = new com.fossil.blesdk.obfuscated.C2534o8(this.f8074r.getContext(), this.f8053A);
    }

    @DexIgnore
    /* renamed from: i */
    public final void mo14364i() {
        com.fossil.blesdk.obfuscated.C2552oe.C2561g gVar = this.f8053A;
        if (gVar != null) {
            gVar.mo14395a();
            this.f8053A = null;
        }
        if (this.f8082z != null) {
            this.f8082z = null;
        }
    }

    @DexIgnore
    public void onDraw(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.State state) {
        float f;
        float f2;
        this.f8081y = -1;
        if (this.f8059c != null) {
            mo14349a(this.f8058b);
            float[] fArr = this.f8058b;
            float f3 = fArr[0];
            f = fArr[1];
            f2 = f3;
        } else {
            f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        this.f8069m.mo14377a(canvas, recyclerView, this.f8059c, this.f8072p, this.f8070n, f2, f);
    }

    @DexIgnore
    public void onDrawOver(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, androidx.recyclerview.widget.RecyclerView.State state) {
        float f;
        float f2;
        if (this.f8059c != null) {
            mo14349a(this.f8058b);
            float[] fArr = this.f8058b;
            float f3 = fArr[0];
            f = fArr[1];
            f2 = f3;
        } else {
            f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        this.f8069m.mo14386b(canvas, recyclerView, this.f8059c, this.f8072p, this.f8070n, f2, f);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14347a(androidx.recyclerview.widget.RecyclerView recyclerView) {
        androidx.recyclerview.widget.RecyclerView recyclerView2 = this.f8074r;
        if (recyclerView2 != recyclerView) {
            if (recyclerView2 != null) {
                mo14352b();
            }
            this.f8074r = recyclerView;
            if (recyclerView != null) {
                android.content.res.Resources resources = recyclerView.getResources();
                this.f8062f = resources.getDimension(com.fossil.blesdk.obfuscated.C1550ce.item_touch_helper_swipe_escape_velocity);
                this.f8063g = resources.getDimension(com.fossil.blesdk.obfuscated.C1550ce.item_touch_helper_swipe_escape_max_velocity);
                mo14362g();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14349a(float[] fArr) {
        if ((this.f8071o & 12) != 0) {
            fArr[0] = (this.f8066j + this.f8064h) - ((float) this.f8059c.itemView.getLeft());
        } else {
            fArr[0] = this.f8059c.itemView.getTranslationX();
        }
        if ((this.f8071o & 3) != 0) {
            fArr[1] = (this.f8067k + this.f8065i) - ((float) this.f8059c.itemView.getTop());
        } else {
            fArr[1] = this.f8059c.itemView.getTranslationY();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14353b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        if (!this.f8074r.isLayoutRequested() && this.f8070n == 2) {
            float a = this.f8069m.mo14369a(viewHolder);
            int i = (int) (this.f8066j + this.f8064h);
            int i2 = (int) (this.f8067k + this.f8065i);
            if (((float) java.lang.Math.abs(i2 - viewHolder.itemView.getTop())) >= ((float) viewHolder.itemView.getHeight()) * a || ((float) java.lang.Math.abs(i - viewHolder.itemView.getLeft())) >= ((float) viewHolder.itemView.getWidth()) * a) {
                java.util.List<androidx.recyclerview.widget.RecyclerView.ViewHolder> a2 = mo14342a(viewHolder);
                if (a2.size() != 0) {
                    androidx.recyclerview.widget.RecyclerView.ViewHolder a3 = this.f8069m.mo14375a(viewHolder, a2, i, i2);
                    if (a3 == null) {
                        this.f8077u.clear();
                        this.f8078v.clear();
                        return;
                    }
                    int adapterPosition = a3.getAdapterPosition();
                    int adapterPosition2 = viewHolder.getAdapterPosition();
                    if (this.f8069m.mo14389b(this.f8074r, viewHolder, a3)) {
                        this.f8069m.mo14380a(this.f8074r, viewHolder, adapterPosition2, a3, adapterPosition, i, i2);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14348a(com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar, int i) {
        this.f8074r.post(new com.fossil.blesdk.obfuscated.C2552oe.C2556d(hVar, i));
    }

    @DexIgnore
    /* renamed from: a */
    public final java.util.List<androidx.recyclerview.widget.RecyclerView.ViewHolder> mo14342a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder2 = viewHolder;
        java.util.List<androidx.recyclerview.widget.RecyclerView.ViewHolder> list = this.f8077u;
        if (list == null) {
            this.f8077u = new java.util.ArrayList();
            this.f8078v = new java.util.ArrayList();
        } else {
            list.clear();
            this.f8078v.clear();
        }
        int a = this.f8069m.mo14370a();
        int round = java.lang.Math.round(this.f8066j + this.f8064h) - a;
        int round2 = java.lang.Math.round(this.f8067k + this.f8065i) - a;
        int i = a * 2;
        int width = viewHolder2.itemView.getWidth() + round + i;
        int height = viewHolder2.itemView.getHeight() + round2 + i;
        int i2 = (round + width) / 2;
        int i3 = (round2 + height) / 2;
        androidx.recyclerview.widget.RecyclerView.C0236m layoutManager = this.f8074r.getLayoutManager();
        int e = layoutManager.mo2941e();
        int i4 = 0;
        while (i4 < e) {
            android.view.View d = layoutManager.mo2936d(i4);
            if (d != viewHolder2.itemView && d.getBottom() >= round2 && d.getTop() <= height && d.getRight() >= round && d.getLeft() <= width) {
                androidx.recyclerview.widget.RecyclerView.ViewHolder g = this.f8074r.mo2586g(d);
                if (this.f8069m.mo14381a(this.f8074r, this.f8059c, g)) {
                    int abs = java.lang.Math.abs(i2 - ((d.getLeft() + d.getRight()) / 2));
                    int abs2 = java.lang.Math.abs(i3 - ((d.getTop() + d.getBottom()) / 2));
                    int i5 = (abs * abs) + (abs2 * abs2);
                    int size = this.f8077u.size();
                    int i6 = 0;
                    int i7 = 0;
                    while (i6 < size && i5 > this.f8078v.get(i6).intValue()) {
                        i7++;
                        i6++;
                        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder3 = viewHolder;
                    }
                    this.f8077u.add(i7, g);
                    this.f8078v.add(i7, java.lang.Integer.valueOf(i5));
                }
            }
            i4++;
            viewHolder2 = viewHolder;
        }
        return this.f8077u;
    }

    @DexIgnore
    /* renamed from: b */
    public android.view.View mo14351b(android.view.MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = this.f8059c;
        if (viewHolder != null) {
            android.view.View view = viewHolder.itemView;
            if (m11692a(view, x, y, this.f8066j + this.f8064h, this.f8067k + this.f8065i)) {
                return view;
            }
        }
        for (int size = this.f8072p.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar = this.f8072p.get(size);
            android.view.View view2 = hVar.f8101e.itemView;
            if (m11692a(view2, x, y, hVar.f8105i, hVar.f8106j)) {
                return view2;
            }
        }
        return this.f8074r.mo2499a(x, y);
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo14358c() {
        int size = this.f8072p.size();
        for (int i = 0; i < size; i++) {
            if (!this.f8072p.get(i).f8108l) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public final int mo14350b(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i) {
        if ((i & 3) == 0) {
            return 0;
        }
        int i2 = 2;
        int i3 = this.f8065i > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 2 : 1;
        android.view.VelocityTracker velocityTracker = this.f8076t;
        if (velocityTracker != null && this.f8068l > -1) {
            com.fossil.blesdk.obfuscated.C2552oe.C2558f fVar = this.f8069m;
            float f = this.f8063g;
            fVar.mo14382b(f);
            velocityTracker.computeCurrentVelocity(1000, f);
            float xVelocity = this.f8076t.getXVelocity(this.f8068l);
            float yVelocity = this.f8076t.getYVelocity(this.f8068l);
            if (yVelocity <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i2 = 1;
            }
            float abs = java.lang.Math.abs(yVelocity);
            if ((i2 & i) != 0 && i2 == i3) {
                com.fossil.blesdk.obfuscated.C2552oe.C2558f fVar2 = this.f8069m;
                float f2 = this.f8062f;
                fVar2.mo14368a(f2);
                if (abs >= f2 && abs > java.lang.Math.abs(xVelocity)) {
                    return i2;
                }
            }
        }
        float height = ((float) this.f8074r.getHeight()) * this.f8069m.mo14383b(viewHolder);
        if ((i & i3) == 0 || java.lang.Math.abs(this.f8065i) <= height) {
            return 0;
        }
        return i3;
    }

    @DexIgnore
    /* renamed from: c */
    public final androidx.recyclerview.widget.RecyclerView.ViewHolder mo14355c(android.view.MotionEvent motionEvent) {
        androidx.recyclerview.widget.RecyclerView.C0236m layoutManager = this.f8074r.getLayoutManager();
        int i = this.f8068l;
        if (i == -1) {
            return null;
        }
        int findPointerIndex = motionEvent.findPointerIndex(i);
        float abs = java.lang.Math.abs(motionEvent.getX(findPointerIndex) - this.f8060d);
        float abs2 = java.lang.Math.abs(motionEvent.getY(findPointerIndex) - this.f8061e);
        int i2 = this.f8073q;
        if (abs < ((float) i2) && abs2 < ((float) i2)) {
            return null;
        }
        if (abs > abs2 && layoutManager.mo2426a()) {
            return null;
        }
        if (abs2 > abs && layoutManager.mo2437b()) {
            return null;
        }
        android.view.View b = mo14351b(motionEvent);
        if (b == null) {
            return null;
        }
        return this.f8074r.mo2586g(b);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo2984a(android.view.View view) {
        mo14356c(view);
        androidx.recyclerview.widget.RecyclerView.ViewHolder g = this.f8074r.mo2586g(view);
        if (g != null) {
            androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder = this.f8059c;
            if (viewHolder == null || g != viewHolder) {
                mo14346a(g, false);
                if (this.f8057a.remove(g.itemView)) {
                    this.f8069m.mo14379a(this.f8074r, g);
                    return;
                }
                return;
            }
            mo14357c((androidx.recyclerview.widget.RecyclerView.ViewHolder) null, 0);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final int mo14354c(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder) {
        if (this.f8070n == 2) {
            return 0;
        }
        int c = this.f8069m.mo14390c(this.f8074r, viewHolder);
        int a = (this.f8069m.mo14371a(c, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f8074r)) & 65280) >> 8;
        if (a == 0) {
            return 0;
        }
        int i = (c & 65280) >> 8;
        if (java.lang.Math.abs(this.f8064h) > java.lang.Math.abs(this.f8065i)) {
            int a2 = mo14340a(viewHolder, a);
            if (a2 > 0) {
                return (i & a2) == 0 ? com.fossil.blesdk.obfuscated.C2552oe.C2558f.m11724b(a2, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f8074r)) : a2;
            }
            int b = mo14350b(viewHolder, a);
            if (b > 0) {
                return b;
            }
        } else {
            int b2 = mo14350b(viewHolder, a);
            if (b2 > 0) {
                return b2;
            }
            int a3 = mo14340a(viewHolder, a);
            if (a3 > 0) {
                return (i & a3) == 0 ? com.fossil.blesdk.obfuscated.C2552oe.C2558f.m11724b(a3, com.fossil.blesdk.obfuscated.C1776f9.m6845k(this.f8074r)) : a3;
            }
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14346a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, boolean z) {
        for (int size = this.f8072p.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar = this.f8072p.get(size);
            if (hVar.f8101e == viewHolder) {
                hVar.f8107k |= z;
                if (!hVar.f8108l) {
                    hVar.mo14398a();
                }
                this.f8072p.remove(size);
                return;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14344a(int i, android.view.MotionEvent motionEvent, int i2) {
        if (this.f8059c == null && i == 2 && this.f8070n != 2 && this.f8069m.mo14388b() && this.f8074r.getScrollState() != 1) {
            androidx.recyclerview.widget.RecyclerView.ViewHolder c = mo14355c(motionEvent);
            if (c != null) {
                int b = (this.f8069m.mo14384b(this.f8074r, c) & 65280) >> 8;
                if (b != 0) {
                    float x = motionEvent.getX(i2);
                    float y = motionEvent.getY(i2);
                    float f = x - this.f8060d;
                    float f2 = y - this.f8061e;
                    float abs = java.lang.Math.abs(f);
                    float abs2 = java.lang.Math.abs(f2);
                    int i3 = this.f8073q;
                    if (abs >= ((float) i3) || abs2 >= ((float) i3)) {
                        if (abs > abs2) {
                            if (f < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b & 4) == 0) {
                                return;
                            }
                            if (f > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b & 8) == 0) {
                                return;
                            }
                        } else if (f2 < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b & 1) == 0) {
                            return;
                        } else {
                            if (f2 > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && (b & 2) == 0) {
                                return;
                            }
                        }
                        this.f8065i = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        this.f8064h = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        this.f8068l = motionEvent.getPointerId(0);
                        mo14357c(c, 1);
                    }
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14356c(android.view.View view) {
        if (view == this.f8080x) {
            this.f8080x = null;
            if (this.f8079w != null) {
                this.f8074r.setChildDrawingOrderCallback((androidx.recyclerview.widget.RecyclerView.C0228h) null);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2552oe.C2562h mo14341a(android.view.MotionEvent motionEvent) {
        if (this.f8072p.isEmpty()) {
            return null;
        }
        android.view.View b = mo14351b(motionEvent);
        for (int size = this.f8072p.size() - 1; size >= 0; size--) {
            com.fossil.blesdk.obfuscated.C2552oe.C2562h hVar = this.f8072p.get(size);
            if (hVar.f8101e.itemView == b) {
                return hVar;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14345a(android.view.MotionEvent motionEvent, int i, int i2) {
        float x = motionEvent.getX(i2);
        float y = motionEvent.getY(i2);
        this.f8064h = x - this.f8060d;
        this.f8065i = y - this.f8061e;
        if ((i & 4) == 0) {
            this.f8064h = java.lang.Math.max(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f8064h);
        }
        if ((i & 8) == 0) {
            this.f8064h = java.lang.Math.min(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f8064h);
        }
        if ((i & 1) == 0) {
            this.f8065i = java.lang.Math.max(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f8065i);
        }
        if ((i & 2) == 0) {
            this.f8065i = java.lang.Math.min(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.f8065i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo14340a(androidx.recyclerview.widget.RecyclerView.ViewHolder viewHolder, int i) {
        if ((i & 12) == 0) {
            return 0;
        }
        int i2 = 8;
        int i3 = this.f8064h > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 8 : 4;
        android.view.VelocityTracker velocityTracker = this.f8076t;
        if (velocityTracker != null && this.f8068l > -1) {
            com.fossil.blesdk.obfuscated.C2552oe.C2558f fVar = this.f8069m;
            float f = this.f8063g;
            fVar.mo14382b(f);
            velocityTracker.computeCurrentVelocity(1000, f);
            float xVelocity = this.f8076t.getXVelocity(this.f8068l);
            float yVelocity = this.f8076t.getYVelocity(this.f8068l);
            if (xVelocity <= com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                i2 = 4;
            }
            float abs = java.lang.Math.abs(xVelocity);
            if ((i2 & i) != 0 && i3 == i2) {
                com.fossil.blesdk.obfuscated.C2552oe.C2558f fVar2 = this.f8069m;
                float f2 = this.f8062f;
                fVar2.mo14368a(f2);
                if (abs >= f2 && abs > java.lang.Math.abs(yVelocity)) {
                    return i2;
                }
            }
        }
        float width = ((float) this.f8074r.getWidth()) * this.f8069m.mo14383b(viewHolder);
        if ((i & i3) == 0 || java.lang.Math.abs(this.f8064h) <= width) {
            return 0;
        }
        return i3;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14343a() {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            if (this.f8079w == null) {
                this.f8079w = new com.fossil.blesdk.obfuscated.C2552oe.C2557e();
            }
            this.f8074r.setChildDrawingOrderCallback(this.f8079w);
        }
    }
}
