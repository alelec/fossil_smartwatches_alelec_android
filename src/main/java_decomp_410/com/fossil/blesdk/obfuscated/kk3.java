package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kk3 implements MembersInjector<LoginPresenter> {
    @DexIgnore
    public static void a(LoginPresenter loginPresenter, LoginEmailUseCase loginEmailUseCase) {
        loginPresenter.h = loginEmailUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, LoginSocialUseCase loginSocialUseCase) {
        loginPresenter.i = loginSocialUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, DownloadUserInfoUseCase downloadUserInfoUseCase) {
        loginPresenter.j = downloadUserInfoUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, tq2 tq2) {
        loginPresenter.k = tq2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, vj2 vj2) {
        loginPresenter.l = vj2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, UserRepository userRepository) {
        loginPresenter.m = userRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, DeviceRepository deviceRepository) {
        loginPresenter.n = deviceRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, en2 en2) {
        loginPresenter.o = en2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchActivities fetchActivities) {
        loginPresenter.p = fetchActivities;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchSummaries fetchSummaries) {
        loginPresenter.q = fetchSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, j62 j62) {
        loginPresenter.r = j62;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchSleepSessions fetchSleepSessions) {
        loginPresenter.s = fetchSleepSessions;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchSleepSummaries fetchSleepSummaries) {
        loginPresenter.t = fetchSleepSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchHeartRateSamples fetchHeartRateSamples) {
        loginPresenter.u = fetchHeartRateSamples;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries) {
        loginPresenter.v = fetchDailyHeartRateSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, rr2 rr2) {
        loginPresenter.w = rr2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, kn2 kn2) {
        loginPresenter.x = kn2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, sr2 sr2) {
        loginPresenter.y = sr2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, ur2 ur2) {
        loginPresenter.z = ur2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, tr2 tr2) {
        loginPresenter.A = tr2;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, CheckAuthenticationSocialExisting checkAuthenticationSocialExisting) {
        loginPresenter.B = checkAuthenticationSocialExisting;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, AnalyticsHelper analyticsHelper) {
        loginPresenter.C = analyticsHelper;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, SummariesRepository summariesRepository) {
        loginPresenter.D = summariesRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, SleepSummariesRepository sleepSummariesRepository) {
        loginPresenter.E = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, GoalTrackingRepository goalTrackingRepository) {
        loginPresenter.F = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries) {
        loginPresenter.G = fetchDailyGoalTrackingSummaries;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, FetchGoalTrackingData fetchGoalTrackingData) {
        loginPresenter.H = fetchGoalTrackingData;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, GetSecretKeyUseCase getSecretKeyUseCase) {
        loginPresenter.I = getSecretKeyUseCase;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, WatchLocalizationRepository watchLocalizationRepository) {
        loginPresenter.J = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter, AlarmsRepository alarmsRepository) {
        loginPresenter.K = alarmsRepository;
    }

    @DexIgnore
    public static void a(LoginPresenter loginPresenter) {
        loginPresenter.C();
    }
}
