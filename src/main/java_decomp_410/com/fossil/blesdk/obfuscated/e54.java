package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.common.CommonUtils;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class e54 {
    @DexIgnore
    public static /* final */ Pattern f; // = Pattern.compile("http(s?)://[^\\/]+", 2);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ z64 b;
    @DexIgnore
    public /* final */ HttpMethod c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ v44 e;

    @DexIgnore
    public e54(v44 v44, String str, String str2, z64 z64, HttpMethod httpMethod) {
        if (str2 == null) {
            throw new IllegalArgumentException("url must not be null.");
        } else if (z64 != null) {
            this.e = v44;
            this.d = str;
            this.a = a(str2);
            this.b = z64;
            this.c = httpMethod;
        } else {
            throw new IllegalArgumentException("requestFactory must not be null.");
        }
    }

    @DexIgnore
    public HttpRequest a() {
        return a((Map<String, String>) Collections.emptyMap());
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public HttpRequest a(Map<String, String> map) {
        HttpRequest a2 = this.b.a(this.c, b(), map);
        a2.a(false);
        a2.a(10000);
        a2.c("User-Agent", "Crashlytics Android SDK/" + this.e.r());
        a2.c("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa");
        return a2;
    }

    @DexIgnore
    public final String a(String str) {
        return !CommonUtils.b(this.d) ? f.matcher(str).replaceFirst(this.d) : str;
    }
}
