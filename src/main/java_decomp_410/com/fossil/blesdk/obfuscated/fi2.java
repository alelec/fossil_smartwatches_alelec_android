package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fi2 extends ei2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public long w;

    /*
    static {
        y.put(R.id.iv_workout, 1);
        y.put(R.id.ftv_workout_title, 2);
        y.put(R.id.ftv_resting_value, 3);
        y.put(R.id.ftv_resting_unit, 4);
        y.put(R.id.separatedLine, 5);
        y.put(R.id.ftv_max_value, 6);
        y.put(R.id.ftv_max_unit, 7);
        y.put(R.id.ftv_workout_time, 8);
    }
    */

    @DexIgnore
    public fi2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 9, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fi2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[7], objArr[6], objArr[4], objArr[3], objArr[8], objArr[2], objArr[1], objArr[5]);
        this.w = -1;
        this.v = objArr[0];
        this.v.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
