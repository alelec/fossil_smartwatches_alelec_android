package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.text.Spanned;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface vl3 extends v52<ul3> {
    @DexIgnore
    void A(boolean z);

    @DexIgnore
    void B(boolean z);

    @DexIgnore
    void H0();

    @DexIgnore
    void K(String str);

    @DexIgnore
    void a(Bundle bundle);

    @DexIgnore
    void a(Spanned spanned);

    @DexIgnore
    void a(SignUpSocialAuth signUpSocialAuth);

    @DexIgnore
    void a(Gender gender);

    @DexIgnore
    void a(boolean z, boolean z2, String str);

    @DexIgnore
    void b(MFUser mFUser);

    @DexIgnore
    void b(boolean z, String str);

    @DexIgnore
    void c(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    void g();

    @DexIgnore
    void g(int i, String str);

    @DexIgnore
    void g0();

    @DexIgnore
    void i();

    @DexIgnore
    void k();

    @DexIgnore
    void t0();
}
