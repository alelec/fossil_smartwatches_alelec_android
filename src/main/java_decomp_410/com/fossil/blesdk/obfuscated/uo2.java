package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.DeclarationFile;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class uo2 {
    @DexIgnore
    public List<wo2> a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public void a(xz1 xz1) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        xz1 xz12 = xz1;
        String str8 = DeclarationFile.COLUMN_FILE_ID;
        String str9 = MicroAppVariant.COLUMN_DECLARATION_FILES;
        String str10 = "createdAt";
        String str11 = "updatedAt";
        String str12 = MicroAppVariant.COLUMN_MINOR_NUMBER;
        String str13 = "appId";
        this.a = new ArrayList();
        if (xz12.d(CloudLogWriter.ITEMS_PARAM)) {
            try {
                tz1 b2 = xz12.b(CloudLogWriter.ITEMS_PARAM);
                if (b2.size() > 0) {
                    int i = 0;
                    while (i < b2.size()) {
                        xz1 d = b2.get(i).d();
                        wo2 wo2 = new wo2();
                        if (d.d(str13)) {
                            str = str13;
                            wo2.a(d.a(str13).f());
                        } else {
                            str = str13;
                        }
                        if (d.d("name")) {
                            wo2.c(d.a("name").f());
                        }
                        if (d.d("description")) {
                            wo2.b(d.a("description").f());
                        }
                        if (d.d(MicroAppVariant.COLUMN_MAJOR_NUMBER)) {
                            wo2.a(d.a(MicroAppVariant.COLUMN_MAJOR_NUMBER).b());
                        }
                        if (d.d(str12)) {
                            wo2.b(d.a(str12).b());
                        }
                        if (d.d(str11)) {
                            str2 = str11;
                            DateTime parseDateTime = ISODateTimeFormat.dateTimeNoMillis().parseDateTime(d.a(str11).f());
                            str3 = str12;
                            wo2.b(parseDateTime.getMillis());
                        } else {
                            str2 = str11;
                            str3 = str12;
                        }
                        if (d.d(str10)) {
                            wo2.a(ISODateTimeFormat.dateTimeNoMillis().parseDateTime(d.a(str10).f()).getMillis());
                        }
                        if (d.d(str9)) {
                            tz1 b3 = d.b(str9);
                            ArrayList arrayList = new ArrayList();
                            if (b3.size() > 0) {
                                str6 = str9;
                                int i2 = 0;
                                while (i2 < b3.size()) {
                                    xz1 d2 = b3.get(i2).d();
                                    String str14 = str10;
                                    DeclarationFile declarationFile = new DeclarationFile();
                                    if (d2.d(str8)) {
                                        str7 = str8;
                                        declarationFile.setFileId(d2.a(str8).f());
                                    } else {
                                        str7 = str8;
                                    }
                                    if (d2.d("description")) {
                                        declarationFile.setDescription(d2.a("description").f());
                                    }
                                    if (d2.d("content")) {
                                        declarationFile.setContent(d2.a("content").f());
                                    }
                                    arrayList.add(declarationFile);
                                    i2++;
                                    str10 = str14;
                                    str8 = str7;
                                }
                                str4 = str8;
                            } else {
                                str4 = str8;
                                str6 = str9;
                            }
                            str5 = str10;
                            wo2.a((List<DeclarationFile>) arrayList);
                        } else {
                            str4 = str8;
                            str6 = str9;
                            str5 = str10;
                        }
                        this.a.add(wo2);
                        i++;
                        str12 = str3;
                        str11 = str2;
                        str13 = str;
                        str9 = str6;
                        str10 = str5;
                        str8 = str4;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (xz12.d("_range")) {
            try {
                this.b = (Range) new Gson().a(xz12.c("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    @DexIgnore
    public Range b() {
        return this.b;
    }

    @DexIgnore
    public List<wo2> a() {
        return this.a;
    }
}
