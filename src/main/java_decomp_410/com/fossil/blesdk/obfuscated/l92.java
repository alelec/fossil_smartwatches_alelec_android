package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l92 extends k92 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout G;
    @DexIgnore
    public long H;

    /*
    static {
        J.put(R.id.iv_close, 1);
        J.put(R.id.iv_delete, 2);
        J.put(R.id.ftv_title, 3);
        J.put(R.id.iv_repeat, 4);
        J.put(R.id.ftv_repeat_label, 5);
        J.put(R.id.sw_repeat, 6);
        J.put(R.id.cl_days_repeat, 7);
        J.put(R.id.day_sunday, 8);
        J.put(R.id.day_monday, 9);
        J.put(R.id.day_tuesday, 10);
        J.put(R.id.day_wednesday, 11);
        J.put(R.id.day_thursday, 12);
        J.put(R.id.day_friday, 13);
        J.put(R.id.day_saturday, 14);
        J.put(R.id.v_line, 15);
        J.put(R.id.numberPickerOne, 16);
        J.put(R.id.numberPickerTwo, 17);
        J.put(R.id.numberPickerThree, 18);
        J.put(R.id.fb_save, 19);
    }
    */

    @DexIgnore
    public l92(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 20, I, J));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.H != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.H = 1;
        }
        g();
    }

    @DexIgnore
    public l92(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[7], objArr[13], objArr[9], objArr[14], objArr[8], objArr[12], objArr[10], objArr[11], objArr[19], objArr[5], objArr[3], objArr[1], objArr[2], objArr[4], objArr[16], objArr[18], objArr[17], objArr[6], objArr[15]);
        this.H = -1;
        this.G = objArr[0];
        this.G.setTag((Object) null);
        a(view);
        f();
    }
}
