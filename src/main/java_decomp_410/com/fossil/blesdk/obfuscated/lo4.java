package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lo4 extends yo4, ReadableByteChannel {
    @DexIgnore
    long a(byte b) throws IOException;

    @DexIgnore
    long a(xo4 xo4) throws IOException;

    @DexIgnore
    jo4 a();

    @DexIgnore
    String a(Charset charset) throws IOException;

    @DexIgnore
    boolean a(long j, ByteString byteString) throws IOException;

    @DexIgnore
    boolean c(long j) throws IOException;

    @DexIgnore
    ByteString d(long j) throws IOException;

    @DexIgnore
    String e(long j) throws IOException;

    @DexIgnore
    byte[] f() throws IOException;

    @DexIgnore
    byte[] f(long j) throws IOException;

    @DexIgnore
    void g(long j) throws IOException;

    @DexIgnore
    boolean g() throws IOException;

    @DexIgnore
    long h() throws IOException;

    @DexIgnore
    String i() throws IOException;

    @DexIgnore
    int j() throws IOException;

    @DexIgnore
    short k() throws IOException;

    @DexIgnore
    long l() throws IOException;

    @DexIgnore
    InputStream m();

    @DexIgnore
    byte readByte() throws IOException;

    @DexIgnore
    void readFully(byte[] bArr) throws IOException;

    @DexIgnore
    int readInt() throws IOException;

    @DexIgnore
    short readShort() throws IOException;

    @DexIgnore
    void skip(long j) throws IOException;
}
