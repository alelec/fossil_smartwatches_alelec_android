package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y51 extends vb1<y51> {
    @DexIgnore
    public Long c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public Integer e; // = null;
    @DexIgnore
    public z51[] f; // = z51.e();
    @DexIgnore
    public x51[] g; // = x51.e();
    @DexIgnore
    public r51[] h; // = r51.e();
    @DexIgnore
    public String i; // = null;
    @DexIgnore
    public Boolean j; // = null;

    @DexIgnore
    public y51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Long l = this.c;
        if (l != null) {
            ub1.b(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            ub1.a(2, str);
        }
        Integer num = this.e;
        if (num != null) {
            ub1.b(3, num.intValue());
        }
        z51[] z51Arr = this.f;
        int i2 = 0;
        if (z51Arr != null && z51Arr.length > 0) {
            int i3 = 0;
            while (true) {
                z51[] z51Arr2 = this.f;
                if (i3 >= z51Arr2.length) {
                    break;
                }
                z51 z51 = z51Arr2[i3];
                if (z51 != null) {
                    ub1.a(4, (ac1) z51);
                }
                i3++;
            }
        }
        x51[] x51Arr = this.g;
        if (x51Arr != null && x51Arr.length > 0) {
            int i4 = 0;
            while (true) {
                x51[] x51Arr2 = this.g;
                if (i4 >= x51Arr2.length) {
                    break;
                }
                x51 x51 = x51Arr2[i4];
                if (x51 != null) {
                    ub1.a(5, (ac1) x51);
                }
                i4++;
            }
        }
        r51[] r51Arr = this.h;
        if (r51Arr != null && r51Arr.length > 0) {
            while (true) {
                r51[] r51Arr2 = this.h;
                if (i2 >= r51Arr2.length) {
                    break;
                }
                r51 r51 = r51Arr2[i2];
                if (r51 != null) {
                    ub1.a(6, (ac1) r51);
                }
                i2++;
            }
        }
        String str2 = this.i;
        if (str2 != null) {
            ub1.a(7, str2);
        }
        Boolean bool = this.j;
        if (bool != null) {
            ub1.a(8, bool.booleanValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof y51)) {
            return false;
        }
        y51 y51 = (y51) obj;
        Long l = this.c;
        if (l == null) {
            if (y51.c != null) {
                return false;
            }
        } else if (!l.equals(y51.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (y51.d != null) {
                return false;
            }
        } else if (!str.equals(y51.d)) {
            return false;
        }
        Integer num = this.e;
        if (num == null) {
            if (y51.e != null) {
                return false;
            }
        } else if (!num.equals(y51.e)) {
            return false;
        }
        if (!zb1.a((Object[]) this.f, (Object[]) y51.f) || !zb1.a((Object[]) this.g, (Object[]) y51.g) || !zb1.a((Object[]) this.h, (Object[]) y51.h)) {
            return false;
        }
        String str2 = this.i;
        if (str2 == null) {
            if (y51.i != null) {
                return false;
            }
        } else if (!str2.equals(y51.i)) {
            return false;
        }
        Boolean bool = this.j;
        if (bool == null) {
            if (y51.j != null) {
                return false;
            }
        } else if (!bool.equals(y51.j)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(y51.b);
        }
        xb1 xb12 = y51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (y51.class.getName().hashCode() + 527) * 31;
        Long l = this.c;
        int i2 = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        Integer num = this.e;
        int hashCode4 = (((((((hashCode3 + (num == null ? 0 : num.hashCode())) * 31) + zb1.a((Object[]) this.f)) * 31) + zb1.a((Object[]) this.g)) * 31) + zb1.a((Object[]) this.h)) * 31;
        String str2 = this.i;
        int hashCode5 = (hashCode4 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Boolean bool = this.j;
        int hashCode6 = (hashCode5 + (bool == null ? 0 : bool.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i2 = this.b.hashCode();
        }
        return hashCode6 + i2;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Long l = this.c;
        if (l != null) {
            a += ub1.c(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            a += ub1.b(2, str);
        }
        Integer num = this.e;
        if (num != null) {
            a += ub1.c(3, num.intValue());
        }
        z51[] z51Arr = this.f;
        int i2 = 0;
        if (z51Arr != null && z51Arr.length > 0) {
            int i3 = a;
            int i4 = 0;
            while (true) {
                z51[] z51Arr2 = this.f;
                if (i4 >= z51Arr2.length) {
                    break;
                }
                z51 z51 = z51Arr2[i4];
                if (z51 != null) {
                    i3 += ub1.b(4, (ac1) z51);
                }
                i4++;
            }
            a = i3;
        }
        x51[] x51Arr = this.g;
        if (x51Arr != null && x51Arr.length > 0) {
            int i5 = a;
            int i6 = 0;
            while (true) {
                x51[] x51Arr2 = this.g;
                if (i6 >= x51Arr2.length) {
                    break;
                }
                x51 x51 = x51Arr2[i6];
                if (x51 != null) {
                    i5 += ub1.b(5, (ac1) x51);
                }
                i6++;
            }
            a = i5;
        }
        r51[] r51Arr = this.h;
        if (r51Arr != null && r51Arr.length > 0) {
            while (true) {
                r51[] r51Arr2 = this.h;
                if (i2 >= r51Arr2.length) {
                    break;
                }
                r51 r51 = r51Arr2[i2];
                if (r51 != null) {
                    a += ub1.b(6, (ac1) r51);
                }
                i2++;
            }
        }
        String str2 = this.i;
        if (str2 != null) {
            a += ub1.b(7, str2);
        }
        Boolean bool = this.j;
        if (bool == null) {
            return a;
        }
        bool.booleanValue();
        return a + ub1.c(8) + 1;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Long.valueOf(tb1.f());
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 24) {
                this.e = Integer.valueOf(tb1.e());
            } else if (c2 == 34) {
                int a = dc1.a(tb1, 34);
                z51[] z51Arr = this.f;
                int length = z51Arr == null ? 0 : z51Arr.length;
                z51[] z51Arr2 = new z51[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.f, 0, z51Arr2, 0, length);
                }
                while (length < z51Arr2.length - 1) {
                    z51Arr2[length] = new z51();
                    tb1.a((ac1) z51Arr2[length]);
                    tb1.c();
                    length++;
                }
                z51Arr2[length] = new z51();
                tb1.a((ac1) z51Arr2[length]);
                this.f = z51Arr2;
            } else if (c2 == 42) {
                int a2 = dc1.a(tb1, 42);
                x51[] x51Arr = this.g;
                int length2 = x51Arr == null ? 0 : x51Arr.length;
                x51[] x51Arr2 = new x51[(a2 + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.g, 0, x51Arr2, 0, length2);
                }
                while (length2 < x51Arr2.length - 1) {
                    x51Arr2[length2] = new x51();
                    tb1.a((ac1) x51Arr2[length2]);
                    tb1.c();
                    length2++;
                }
                x51Arr2[length2] = new x51();
                tb1.a((ac1) x51Arr2[length2]);
                this.g = x51Arr2;
            } else if (c2 == 50) {
                int a3 = dc1.a(tb1, 50);
                r51[] r51Arr = this.h;
                int length3 = r51Arr == null ? 0 : r51Arr.length;
                r51[] r51Arr2 = new r51[(a3 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.h, 0, r51Arr2, 0, length3);
                }
                while (length3 < r51Arr2.length - 1) {
                    r51Arr2[length3] = new r51();
                    tb1.a((ac1) r51Arr2[length3]);
                    tb1.c();
                    length3++;
                }
                r51Arr2[length3] = new r51();
                tb1.a((ac1) r51Arr2[length3]);
                this.h = r51Arr2;
            } else if (c2 == 58) {
                this.i = tb1.b();
            } else if (c2 == 64) {
                this.j = Boolean.valueOf(tb1.d());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
