package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ml */
public final class C2421ml implements com.fossil.blesdk.obfuscated.C2333ll {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.room.RoomDatabase f7532a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2322lf f7533b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ml$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ml$a */
    public class C2422a extends com.fossil.blesdk.obfuscated.C2322lf<com.fossil.blesdk.obfuscated.C2229kl> {
        @DexIgnore
        public C2422a(com.fossil.blesdk.obfuscated.C2421ml mlVar, androidx.room.RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, com.fossil.blesdk.obfuscated.C2229kl klVar) {
            java.lang.String str = klVar.f6916a;
            if (str == null) {
                kgVar.mo11930a(1);
            } else {
                kgVar.mo11932a(1, str);
            }
            java.lang.String str2 = klVar.f6917b;
            if (str2 == null) {
                kgVar.mo11930a(2);
            } else {
                kgVar.mo11932a(2, str2);
            }
        }

        @DexIgnore
        public java.lang.String createQuery() {
            return "INSERT OR IGNORE INTO `WorkTag`(`tag`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public C2421ml(androidx.room.RoomDatabase roomDatabase) {
        this.f7532a = roomDatabase;
        this.f7533b = new com.fossil.blesdk.obfuscated.C2421ml.C2422a(this, roomDatabase);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13307a(com.fossil.blesdk.obfuscated.C2229kl klVar) {
        this.f7532a.assertNotSuspendingTransaction();
        this.f7532a.beginTransaction();
        try {
            this.f7533b.insert(klVar);
            this.f7532a.setTransactionSuccessful();
        } finally {
            this.f7532a.endTransaction();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<java.lang.String> mo13306a(java.lang.String str) {
        com.fossil.blesdk.obfuscated.C3035uf b = com.fossil.blesdk.obfuscated.C3035uf.m14729b("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            b.mo11930a(1);
        } else {
            b.mo11932a(1, str);
        }
        this.f7532a.assertNotSuspendingTransaction();
        android.database.Cursor a = com.fossil.blesdk.obfuscated.C1482bg.m4959a(this.f7532a, b, false);
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(a.getString(0));
            }
            return arrayList;
        } finally {
            a.close();
            b.mo16767c();
        }
    }
}
