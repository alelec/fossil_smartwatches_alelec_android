package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fi4 extends CoroutineContext.a {
    @DexIgnore
    public static final b d = b.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <R> R a(fi4 fi4, R r, yc4<? super R, ? super CoroutineContext.a, ? extends R> yc4) {
            kd4.b(yc4, "operation");
            return CoroutineContext.a.C0169a.a(fi4, r, yc4);
        }

        @DexIgnore
        public static <E extends CoroutineContext.a> E a(fi4 fi4, CoroutineContext.b<E> bVar) {
            kd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) fi4, bVar);
        }

        @DexIgnore
        public static CoroutineContext a(fi4 fi4, CoroutineContext coroutineContext) {
            kd4.b(coroutineContext, "context");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) fi4, coroutineContext);
        }

        @DexIgnore
        public static CoroutineContext b(fi4 fi4, CoroutineContext.b<?> bVar) {
            kd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.b(fi4, bVar);
        }

        @DexIgnore
        public static /* synthetic */ oh4 a(fi4 fi4, boolean z, boolean z2, xc4 xc4, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return fi4.a(z, z2, xc4);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineContext.b<fi4> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();

        /*
        static {
            CoroutineExceptionHandler.a aVar = CoroutineExceptionHandler.c;
        }
        */
    }

    @DexIgnore
    ig4 a(kg4 kg4);

    @DexIgnore
    oh4 a(xc4<? super Throwable, qa4> xc4);

    @DexIgnore
    oh4 a(boolean z, boolean z2, xc4<? super Throwable, qa4> xc4);

    @DexIgnore
    void a(CancellationException cancellationException);

    @DexIgnore
    /* synthetic */ void cancel();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    boolean start();

    @DexIgnore
    CancellationException y();
}
