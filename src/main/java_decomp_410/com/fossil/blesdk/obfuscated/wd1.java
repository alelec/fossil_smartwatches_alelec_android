package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface wd1 extends IInterface {
    @DexIgnore
    void onLocationChanged(Location location) throws RemoteException;
}
