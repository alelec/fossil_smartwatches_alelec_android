package com.fossil.blesdk.obfuscated;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentManagerImpl;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class va extends bb implements FragmentManager.a, FragmentManagerImpl.l {
    @DexIgnore
    public /* final */ FragmentManagerImpl a;
    @DexIgnore
    public ArrayList<a> b; // = new ArrayList<>();
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j; // = true;
    @DexIgnore
    public String k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public int m; // = -1;
    @DexIgnore
    public int n;
    @DexIgnore
    public CharSequence o;
    @DexIgnore
    public int p;
    @DexIgnore
    public CharSequence q;
    @DexIgnore
    public ArrayList<String> r;
    @DexIgnore
    public ArrayList<String> s;
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public ArrayList<Runnable> u;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public Fragment b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public a(int i, Fragment fragment) {
            this.a = i;
            this.b = fragment;
        }
    }

    @DexIgnore
    public va(FragmentManagerImpl fragmentManagerImpl) {
        this.a = fragmentManagerImpl;
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    @DexIgnore
    public bb b(int i2, Fragment fragment, String str) {
        if (i2 != 0) {
            a(i2, fragment, str, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    @DexIgnore
    public bb c(Fragment fragment) {
        a(new a(4, fragment));
        return this;
    }

    @DexIgnore
    public bb d(Fragment fragment) {
        a(new a(3, fragment));
        return this;
    }

    @DexIgnore
    public bb e(Fragment fragment) {
        a(new a(5, fragment));
        return this;
    }

    @DexIgnore
    public void f() {
        int size = this.b.size();
        for (int i2 = 0; i2 < size; i2++) {
            a aVar = this.b.get(i2);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                fragment.setNextTransition(this.g, this.h);
            }
            switch (aVar.a) {
                case 1:
                    fragment.setNextAnim(aVar.c);
                    this.a.a(fragment, false);
                    break;
                case 3:
                    fragment.setNextAnim(aVar.d);
                    this.a.l(fragment);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.d);
                    this.a.f(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.c);
                    this.a.p(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.d);
                    this.a.c(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.c);
                    this.a.a(fragment);
                    break;
                case 8:
                    this.a.o(fragment);
                    break;
                case 9:
                    this.a.o((Fragment) null);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.a);
            }
            if (!(this.t || aVar.a == 1 || fragment == null)) {
                this.a.i(fragment);
            }
        }
        if (!this.t) {
            FragmentManagerImpl fragmentManagerImpl = this.a;
            fragmentManagerImpl.a(fragmentManagerImpl.p, true);
        }
    }

    @DexIgnore
    public String g() {
        return this.k;
    }

    @DexIgnore
    public boolean h() {
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            if (b(this.b.get(i2))) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void i() {
        ArrayList<Runnable> arrayList = this.u;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.u.get(i2).run();
            }
            this.u = null;
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.m >= 0) {
            sb.append(" #");
            sb.append(this.m);
        }
        if (this.k != null) {
            sb.append(" ");
            sb.append(this.k);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.k);
            printWriter.print(" mIndex=");
            printWriter.print(this.m);
            printWriter.print(" mCommitted=");
            printWriter.println(this.l);
            if (this.g != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.g));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.h));
            }
            if (!(this.c == 0 && this.d == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.c));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.d));
            }
            if (!(this.e == 0 && this.f == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.e));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.f));
            }
            if (!(this.n == 0 && this.o == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.n));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.o);
            }
            if (!(this.p == 0 && this.q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.p));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.q);
            }
        }
        if (!this.b.isEmpty()) {
            printWriter.print(str);
            printWriter.println("Operations:");
            str + "    ";
            int size = this.b.size();
            for (int i2 = 0; i2 < size; i2++) {
                a aVar = this.b.get(i2);
                switch (aVar.a) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    case 8:
                        str2 = "SET_PRIMARY_NAV";
                        break;
                    case 9:
                        str2 = "UNSET_PRIMARY_NAV";
                        break;
                    default:
                        str2 = "cmd=" + aVar.a;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(aVar.b);
                if (z) {
                    if (!(aVar.c == 0 && aVar.d == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.c));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.d));
                    }
                    if (aVar.e != 0 || aVar.f != 0) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(aVar.e));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(aVar.f));
                    }
                }
            }
        }
    }

    @DexIgnore
    public void c() {
        e();
        this.a.b((FragmentManagerImpl.l) this, false);
    }

    @DexIgnore
    public void d() {
        e();
        this.a.b((FragmentManagerImpl.l) this, true);
    }

    @DexIgnore
    public bb e() {
        if (!this.i) {
            this.j = false;
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    @DexIgnore
    public bb b(Fragment fragment) {
        a(new a(6, fragment));
        return this;
    }

    @DexIgnore
    public int b() {
        return a(true);
    }

    @DexIgnore
    public boolean b(int i2) {
        int size = this.b.size();
        for (int i3 = 0; i3 < size; i3++) {
            Fragment fragment = this.b.get(i3).b;
            int i4 = fragment != null ? fragment.mContainerId : 0;
            if (i4 != 0 && i4 == i2) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void b(boolean z) {
        for (int size = this.b.size() - 1; size >= 0; size--) {
            a aVar = this.b.get(size);
            Fragment fragment = aVar.b;
            if (fragment != null) {
                fragment.setNextTransition(FragmentManagerImpl.e(this.g), this.h);
            }
            switch (aVar.a) {
                case 1:
                    fragment.setNextAnim(aVar.f);
                    this.a.l(fragment);
                    break;
                case 3:
                    fragment.setNextAnim(aVar.e);
                    this.a.a(fragment, false);
                    break;
                case 4:
                    fragment.setNextAnim(aVar.e);
                    this.a.p(fragment);
                    break;
                case 5:
                    fragment.setNextAnim(aVar.f);
                    this.a.f(fragment);
                    break;
                case 6:
                    fragment.setNextAnim(aVar.e);
                    this.a.a(fragment);
                    break;
                case 7:
                    fragment.setNextAnim(aVar.f);
                    this.a.c(fragment);
                    break;
                case 8:
                    this.a.o((Fragment) null);
                    break;
                case 9:
                    this.a.o(fragment);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + aVar.a);
            }
            if (!(this.t || aVar.a == 3 || fragment == null)) {
                this.a.i(fragment);
            }
        }
        if (!this.t && z) {
            FragmentManagerImpl fragmentManagerImpl = this.a;
            fragmentManagerImpl.a(fragmentManagerImpl.p, true);
        }
    }

    @DexIgnore
    public Fragment b(ArrayList<Fragment> arrayList, Fragment fragment) {
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            a aVar = this.b.get(i2);
            int i3 = aVar.a;
            if (i3 != 1) {
                if (i3 != 3) {
                    switch (i3) {
                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            fragment = null;
                            break;
                        case 9:
                            fragment = aVar.b;
                            break;
                    }
                }
                arrayList.add(aVar.b);
            }
            arrayList.remove(aVar.b);
        }
        return fragment;
    }

    @DexIgnore
    public static boolean b(a aVar) {
        Fragment fragment = aVar.b;
        return fragment != null && fragment.mAdded && fragment.mView != null && !fragment.mDetached && !fragment.mHidden && fragment.isPostponed();
    }

    @DexIgnore
    public void a(a aVar) {
        this.b.add(aVar);
        aVar.c = this.c;
        aVar.d = this.d;
        aVar.e = this.e;
        aVar.f = this.f;
    }

    @DexIgnore
    public bb a(Fragment fragment, String str) {
        a(0, fragment, str, 1);
        return this;
    }

    @DexIgnore
    public bb a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    @DexIgnore
    public final void a(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from" + " instance state.");
        }
        fragment.mFragmentManager = this.a;
        if (str != null) {
            String str2 = fragment.mTag;
            if (str2 == null || str.equals(str2)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 != -1) {
                int i4 = fragment.mFragmentId;
                if (i4 == 0 || i4 == i2) {
                    fragment.mFragmentId = i2;
                    fragment.mContainerId = i2;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i2);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        a(new a(i3, fragment));
    }

    @DexIgnore
    public bb a(int i2, Fragment fragment) {
        b(i2, fragment, (String) null);
        return this;
    }

    @DexIgnore
    public bb a(Fragment fragment) {
        a(new a(7, fragment));
        return this;
    }

    @DexIgnore
    public bb a(String str) {
        if (this.j) {
            this.i = true;
            this.k = str;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    @DexIgnore
    public void a(int i2) {
        if (this.i) {
            if (FragmentManagerImpl.I) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            int size = this.b.size();
            for (int i3 = 0; i3 < size; i3++) {
                a aVar = this.b.get(i3);
                Fragment fragment = aVar.b;
                if (fragment != null) {
                    fragment.mBackStackNesting += i2;
                    if (FragmentManagerImpl.I) {
                        Log.v("FragmentManager", "Bump nesting of " + aVar.b + " to " + aVar.b.mBackStackNesting);
                    }
                }
            }
        }
    }

    @DexIgnore
    public int a() {
        return a(false);
    }

    @DexIgnore
    public int a(boolean z) {
        if (!this.l) {
            if (FragmentManagerImpl.I) {
                Log.v("FragmentManager", "Commit: " + this);
                PrintWriter printWriter = new PrintWriter(new d8("FragmentManager"));
                a("  ", (FileDescriptor) null, printWriter, (String[]) null);
                printWriter.close();
            }
            this.l = true;
            if (this.i) {
                this.m = this.a.b(this);
            } else {
                this.m = -1;
            }
            this.a.a((FragmentManagerImpl.l) this, z);
            return this.m;
        }
        throw new IllegalStateException("commit already called");
    }

    @DexIgnore
    public boolean a(ArrayList<va> arrayList, ArrayList<Boolean> arrayList2) {
        if (FragmentManagerImpl.I) {
            Log.v("FragmentManager", "Run: " + this);
        }
        arrayList.add(this);
        arrayList2.add(false);
        if (!this.i) {
            return true;
        }
        this.a.a(this);
        return true;
    }

    @DexIgnore
    public boolean a(ArrayList<va> arrayList, int i2, int i3) {
        if (i3 == i2) {
            return false;
        }
        int size = this.b.size();
        int i4 = -1;
        for (int i5 = 0; i5 < size; i5++) {
            Fragment fragment = this.b.get(i5).b;
            int i6 = fragment != null ? fragment.mContainerId : 0;
            if (!(i6 == 0 || i6 == i4)) {
                for (int i7 = i2; i7 < i3; i7++) {
                    va vaVar = arrayList.get(i7);
                    int size2 = vaVar.b.size();
                    for (int i8 = 0; i8 < size2; i8++) {
                        Fragment fragment2 = vaVar.b.get(i8).b;
                        if ((fragment2 != null ? fragment2.mContainerId : 0) == i6) {
                            return true;
                        }
                    }
                }
                i4 = i6;
            }
        }
        return false;
    }

    @DexIgnore
    public Fragment a(ArrayList<Fragment> arrayList, Fragment fragment) {
        ArrayList<Fragment> arrayList2 = arrayList;
        Fragment fragment2 = fragment;
        int i2 = 0;
        while (i2 < this.b.size()) {
            a aVar = this.b.get(i2);
            int i3 = aVar.a;
            if (i3 != 1) {
                if (i3 == 2) {
                    Fragment fragment3 = aVar.b;
                    int i4 = fragment3.mContainerId;
                    Fragment fragment4 = fragment2;
                    int i5 = i2;
                    boolean z = false;
                    for (int size = arrayList.size() - 1; size >= 0; size--) {
                        Fragment fragment5 = arrayList2.get(size);
                        if (fragment5.mContainerId == i4) {
                            if (fragment5 == fragment3) {
                                z = true;
                            } else {
                                if (fragment5 == fragment4) {
                                    this.b.add(i5, new a(9, fragment5));
                                    i5++;
                                    fragment4 = null;
                                }
                                a aVar2 = new a(3, fragment5);
                                aVar2.c = aVar.c;
                                aVar2.e = aVar.e;
                                aVar2.d = aVar.d;
                                aVar2.f = aVar.f;
                                this.b.add(i5, aVar2);
                                arrayList2.remove(fragment5);
                                i5++;
                            }
                        }
                    }
                    if (z) {
                        this.b.remove(i5);
                        i5--;
                    } else {
                        aVar.a = 1;
                        arrayList2.add(fragment3);
                    }
                    i2 = i5;
                    fragment2 = fragment4;
                } else if (i3 == 3 || i3 == 6) {
                    arrayList2.remove(aVar.b);
                    Fragment fragment6 = aVar.b;
                    if (fragment6 == fragment2) {
                        this.b.add(i2, new a(9, fragment6));
                        i2++;
                        fragment2 = null;
                    }
                } else if (i3 != 7) {
                    if (i3 == 8) {
                        this.b.add(i2, new a(9, fragment2));
                        i2++;
                        fragment2 = aVar.b;
                    }
                }
                i2++;
            }
            arrayList2.add(aVar.b);
            i2++;
        }
        return fragment2;
    }

    @DexIgnore
    public void a(Fragment.OnStartEnterTransitionListener onStartEnterTransitionListener) {
        for (int i2 = 0; i2 < this.b.size(); i2++) {
            a aVar = this.b.get(i2);
            if (b(aVar)) {
                aVar.b.setOnStartEnterTransitionListener(onStartEnterTransitionListener);
            }
        }
    }
}
