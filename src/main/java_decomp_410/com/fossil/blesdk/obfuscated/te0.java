package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.b;
import com.fossil.blesdk.obfuscated.me0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class te0<R extends me0, A extends de0.b> extends BasePendingResult<R> implements ue0<R> {
    @DexIgnore
    public /* final */ de0.c<A> q;
    @DexIgnore
    public /* final */ de0<?> r;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public te0(de0<?> de0, ge0 ge0) {
        super(ge0);
        bk0.a(ge0, (Object) "GoogleApiClient must not be null");
        bk0.a(de0, (Object) "Api must not be null");
        this.q = de0.a();
        this.r = de0;
    }

    @DexIgnore
    public final void a(RemoteException remoteException) {
        c(new Status(8, remoteException.getLocalizedMessage(), (PendingIntent) null));
    }

    @DexIgnore
    public abstract void a(A a) throws RemoteException;

    @DexIgnore
    public final void b(A a) throws DeadObjectException {
        if (a instanceof gk0) {
            a = ((gk0) a).G();
        }
        try {
            a(a);
        } catch (DeadObjectException e) {
            a((RemoteException) e);
            throw e;
        } catch (RemoteException e2) {
            a(e2);
        }
    }

    @DexIgnore
    public final void c(Status status) {
        bk0.a(!status.L(), (Object) "Failed result must not be success");
        me0 a = a(status);
        a(a);
        d(a);
    }

    @DexIgnore
    public void d(R r2) {
    }

    @DexIgnore
    public final de0<?> h() {
        return this.r;
    }

    @DexIgnore
    public final de0.c<A> i() {
        return this.q;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ void a(Object obj) {
        super.a((me0) obj);
    }
}
