package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@android.annotation.SuppressLint({"CommitPrefEdits"})
/* renamed from: com.fossil.blesdk.obfuscated.sz */
public class C2933sz {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.g74 f9511a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C3069uy f9512b;

    @DexIgnore
    public C2933sz(com.fossil.blesdk.obfuscated.g74 g74, com.fossil.blesdk.obfuscated.C3069uy uyVar) {
        this.f9511a = g74;
        this.f9512b = uyVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2933sz m13966a(com.fossil.blesdk.obfuscated.g74 g74, com.fossil.blesdk.obfuscated.C3069uy uyVar) {
        return new com.fossil.blesdk.obfuscated.C2933sz(g74, uyVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16233a(boolean z) {
        com.fossil.blesdk.obfuscated.g74 g74 = this.f9511a;
        g74.mo27643a(g74.edit().putBoolean("always_send_reports_opt_in", z));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo16234a() {
        if (!this.f9511a.get().contains("preferences_migration_complete")) {
            com.fossil.blesdk.obfuscated.h74 h74 = new com.fossil.blesdk.obfuscated.h74(this.f9512b);
            if (!this.f9511a.get().contains("always_send_reports_opt_in") && h74.get().contains("always_send_reports_opt_in")) {
                boolean z = h74.get().getBoolean("always_send_reports_opt_in", false);
                com.fossil.blesdk.obfuscated.g74 g74 = this.f9511a;
                g74.mo27643a(g74.edit().putBoolean("always_send_reports_opt_in", z));
            }
            com.fossil.blesdk.obfuscated.g74 g742 = this.f9511a;
            g742.mo27643a(g742.edit().putBoolean("preferences_migration_complete", true));
        }
        return this.f9511a.get().getBoolean("always_send_reports_opt_in", false);
    }
}
