package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import libcore.io.Memory;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kb1 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(kb1.class.getName());
    @DexIgnore
    public static /* final */ Unsafe b; // = d();
    @DexIgnore
    public static /* final */ Class<?> c; // = n71.b();
    @DexIgnore
    public static /* final */ boolean d; // = d(Long.TYPE);
    @DexIgnore
    public static /* final */ boolean e; // = d(Integer.TYPE);
    @DexIgnore
    public static /* final */ d f;
    @DexIgnore
    public static /* final */ boolean g; // = f();
    @DexIgnore
    public static /* final */ boolean h; // = e();
    @DexIgnore
    public static /* final */ long i; // = ((long) b(byte[].class));
    @DexIgnore
    public static /* final */ long j;
    @DexIgnore
    public static /* final */ boolean k; // = (ByteOrder.nativeOrder() != ByteOrder.BIG_ENDIAN);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends d {
        @DexIgnore
        public a(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        public final void a(long j, byte b) {
            Memory.pokeByte((int) (j & -1), b);
        }

        @DexIgnore
        public final boolean c(Object obj, long j) {
            if (kb1.k) {
                return kb1.i(obj, j);
            }
            return kb1.j(obj, j);
        }

        @DexIgnore
        public final float d(Object obj, long j) {
            return Float.intBitsToFloat(a(obj, j));
        }

        @DexIgnore
        public final double e(Object obj, long j) {
            return Double.longBitsToDouble(b(obj, j));
        }

        @DexIgnore
        public final byte f(Object obj, long j) {
            if (kb1.k) {
                return kb1.g(obj, j);
            }
            return kb1.h(obj, j);
        }

        @DexIgnore
        public final void a(Object obj, long j, byte b) {
            if (kb1.k) {
                kb1.a(obj, j, b);
            } else {
                kb1.b(obj, j, b);
            }
        }

        @DexIgnore
        public final void a(Object obj, long j, boolean z) {
            if (kb1.k) {
                kb1.b(obj, j, z);
            } else {
                kb1.c(obj, j, z);
            }
        }

        @DexIgnore
        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }

        @DexIgnore
        public final void a(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray((int) (j2 & -1), bArr, (int) j, (int) j3);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends d {
        @DexIgnore
        public b(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        public final void a(long j, byte b) {
            Memory.pokeByte(j, b);
        }

        @DexIgnore
        public final boolean c(Object obj, long j) {
            if (kb1.k) {
                return kb1.i(obj, j);
            }
            return kb1.j(obj, j);
        }

        @DexIgnore
        public final float d(Object obj, long j) {
            return Float.intBitsToFloat(a(obj, j));
        }

        @DexIgnore
        public final double e(Object obj, long j) {
            return Double.longBitsToDouble(b(obj, j));
        }

        @DexIgnore
        public final byte f(Object obj, long j) {
            if (kb1.k) {
                return kb1.g(obj, j);
            }
            return kb1.h(obj, j);
        }

        @DexIgnore
        public final void a(Object obj, long j, byte b) {
            if (kb1.k) {
                kb1.a(obj, j, b);
            } else {
                kb1.b(obj, j, b);
            }
        }

        @DexIgnore
        public final void a(Object obj, long j, boolean z) {
            if (kb1.k) {
                kb1.b(obj, j, z);
            } else {
                kb1.c(obj, j, z);
            }
        }

        @DexIgnore
        public final void a(Object obj, long j, float f) {
            a(obj, j, Float.floatToIntBits(f));
        }

        @DexIgnore
        public final void a(Object obj, long j, double d) {
            a(obj, j, Double.doubleToLongBits(d));
        }

        @DexIgnore
        public final void a(byte[] bArr, long j, long j2, long j3) {
            Memory.pokeByteArray(j2, bArr, (int) j, (int) j3);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends d {
        @DexIgnore
        public c(Unsafe unsafe) {
            super(unsafe);
        }

        @DexIgnore
        public final void a(long j, byte b) {
            this.a.putByte(j, b);
        }

        @DexIgnore
        public final boolean c(Object obj, long j) {
            return this.a.getBoolean(obj, j);
        }

        @DexIgnore
        public final float d(Object obj, long j) {
            return this.a.getFloat(obj, j);
        }

        @DexIgnore
        public final double e(Object obj, long j) {
            return this.a.getDouble(obj, j);
        }

        @DexIgnore
        public final byte f(Object obj, long j) {
            return this.a.getByte(obj, j);
        }

        @DexIgnore
        public final void a(Object obj, long j, byte b) {
            this.a.putByte(obj, j, b);
        }

        @DexIgnore
        public final void a(Object obj, long j, boolean z) {
            this.a.putBoolean(obj, j, z);
        }

        @DexIgnore
        public final void a(Object obj, long j, float f) {
            this.a.putFloat(obj, j, f);
        }

        @DexIgnore
        public final void a(Object obj, long j, double d) {
            this.a.putDouble(obj, j, d);
        }

        @DexIgnore
        public final void a(byte[] bArr, long j, long j2, long j3) {
            this.a.copyMemory(bArr, kb1.i + j, (Object) null, j2, j3);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d {
        @DexIgnore
        public Unsafe a;

        @DexIgnore
        public d(Unsafe unsafe) {
            this.a = unsafe;
        }

        @DexIgnore
        public final int a(Object obj, long j) {
            return this.a.getInt(obj, j);
        }

        @DexIgnore
        public abstract void a(long j, byte b);

        @DexIgnore
        public abstract void a(Object obj, long j, byte b);

        @DexIgnore
        public abstract void a(Object obj, long j, double d);

        @DexIgnore
        public abstract void a(Object obj, long j, float f);

        @DexIgnore
        public abstract void a(Object obj, long j, boolean z);

        @DexIgnore
        public abstract void a(byte[] bArr, long j, long j2, long j3);

        @DexIgnore
        public final long b(Object obj, long j) {
            return this.a.getLong(obj, j);
        }

        @DexIgnore
        public abstract boolean c(Object obj, long j);

        @DexIgnore
        public abstract float d(Object obj, long j);

        @DexIgnore
        public abstract double e(Object obj, long j);

        @DexIgnore
        public abstract byte f(Object obj, long j);

        @DexIgnore
        public final void a(Object obj, long j, int i) {
            this.a.putInt(obj, j, i);
        }

        @DexIgnore
        public final void a(Object obj, long j, long j2) {
            this.a.putLong(obj, j, j2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ba  */
    /*
    static {
        long j2;
        Class<Object[]> cls = Object[].class;
        Class<double[]> cls2 = double[].class;
        Class<float[]> cls3 = float[].class;
        Class<long[]> cls4 = long[].class;
        Class<int[]> cls5 = int[].class;
        Class<boolean[]> cls6 = boolean[].class;
        d dVar = null;
        if (b != null) {
            if (!n71.a()) {
                dVar = new c(b);
            } else if (d) {
                dVar = new b(b);
            } else if (e) {
                dVar = new a(b);
            }
        }
        f = dVar;
        b(cls6);
        c(cls6);
        b(cls5);
        c(cls5);
        b(cls4);
        c(cls4);
        b(cls3);
        c(cls3);
        b(cls2);
        c(cls2);
        b(cls);
        c(cls);
        Field g2 = g();
        if (g2 != null) {
            d dVar2 = f;
            if (dVar2 != null) {
                j2 = dVar2.a.objectFieldOffset(g2);
                j = j2;
            }
        }
        j2 = -1;
        j = j2;
    }
    */

    @DexIgnore
    public static <T> T a(Class<T> cls) {
        try {
            return b.allocateInstance(cls);
        } catch (InstantiationException e2) {
            throw new IllegalStateException(e2);
        }
    }

    @DexIgnore
    public static boolean b() {
        return h;
    }

    @DexIgnore
    public static boolean c() {
        return g;
    }

    @DexIgnore
    public static float d(Object obj, long j2) {
        return f.d(obj, j2);
    }

    @DexIgnore
    public static double e(Object obj, long j2) {
        return f.e(obj, j2);
    }

    @DexIgnore
    public static Object f(Object obj, long j2) {
        return f.a.getObject(obj, j2);
    }

    @DexIgnore
    public static Field g() {
        if (n71.a()) {
            Field a2 = a((Class<?>) Buffer.class, "effectiveDirectAddress");
            if (a2 != null) {
                return a2;
            }
        }
        Field a3 = a((Class<?>) Buffer.class, "address");
        if (a3 == null || a3.getType() != Long.TYPE) {
            return null;
        }
        return a3;
    }

    @DexIgnore
    public static byte h(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) ((j2 & 3) << 3)));
    }

    @DexIgnore
    public static boolean i(Object obj, long j2) {
        return g(obj, j2) != 0;
    }

    @DexIgnore
    public static boolean j(Object obj, long j2) {
        return h(obj, j2) != 0;
    }

    @DexIgnore
    public static int b(Class<?> cls) {
        if (h) {
            return f.a.arrayBaseOffset(cls);
        }
        return -1;
    }

    @DexIgnore
    public static int c(Class<?> cls) {
        if (h) {
            return f.a.arrayIndexScale(cls);
        }
        return -1;
    }

    @DexIgnore
    public static Unsafe d() {
        try {
            return (Unsafe) AccessController.doPrivileged(new lb1());
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean e() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls2.getMethod("arrayBaseOffset", new Class[]{Class.class});
            cls2.getMethod("arrayIndexScale", new Class[]{Class.class});
            cls2.getMethod("getInt", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putInt", new Class[]{cls, Long.TYPE, Integer.TYPE});
            cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putLong", new Class[]{cls, Long.TYPE, Long.TYPE});
            cls2.getMethod("getObject", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putObject", new Class[]{cls, Long.TYPE, cls});
            if (n71.a()) {
                return true;
            }
            cls2.getMethod("getByte", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putByte", new Class[]{cls, Long.TYPE, Byte.TYPE});
            cls2.getMethod("getBoolean", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putBoolean", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls2.getMethod("getFloat", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putFloat", new Class[]{cls, Long.TYPE, Float.TYPE});
            cls2.getMethod("getDouble", new Class[]{cls, Long.TYPE});
            cls2.getMethod("putDouble", new Class[]{cls, Long.TYPE, Double.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static int a(Object obj, long j2) {
        return f.a(obj, j2);
    }

    @DexIgnore
    public static boolean f() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", new Class[]{Field.class});
            cls2.getMethod("getLong", new Class[]{cls, Long.TYPE});
            if (g() == null) {
                return false;
            }
            if (n71.a()) {
                return true;
            }
            cls2.getMethod("getByte", new Class[]{Long.TYPE});
            cls2.getMethod("putByte", new Class[]{Long.TYPE, Byte.TYPE});
            cls2.getMethod("getInt", new Class[]{Long.TYPE});
            cls2.getMethod("putInt", new Class[]{Long.TYPE, Integer.TYPE});
            cls2.getMethod("getLong", new Class[]{Long.TYPE});
            cls2.getMethod("putLong", new Class[]{Long.TYPE, Long.TYPE});
            cls2.getMethod("copyMemory", new Class[]{Long.TYPE, Long.TYPE, Long.TYPE});
            cls2.getMethod("copyMemory", new Class[]{cls, Long.TYPE, cls, Long.TYPE, Long.TYPE});
            return true;
        } catch (Throwable th) {
            Logger logger = a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    @DexIgnore
    public static void a(Object obj, long j2, int i2) {
        f.a(obj, j2, i2);
    }

    @DexIgnore
    public static long b(Object obj, long j2) {
        return f.b(obj, j2);
    }

    @DexIgnore
    public static boolean c(Object obj, long j2) {
        return f.c(obj, j2);
    }

    @DexIgnore
    public static boolean d(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!n71.a()) {
            return false;
        }
        try {
            Class<?> cls3 = c;
            cls3.getMethod("peekLong", new Class[]{cls, Boolean.TYPE});
            cls3.getMethod("pokeLong", new Class[]{cls, Long.TYPE, Boolean.TYPE});
            cls3.getMethod("pokeInt", new Class[]{cls, Integer.TYPE, Boolean.TYPE});
            cls3.getMethod("peekInt", new Class[]{cls, Boolean.TYPE});
            cls3.getMethod("pokeByte", new Class[]{cls, Byte.TYPE});
            cls3.getMethod("peekByte", new Class[]{cls});
            cls3.getMethod("pokeByteArray", new Class[]{cls, cls2, Integer.TYPE, Integer.TYPE});
            cls3.getMethod("peekByteArray", new Class[]{cls, cls2, Integer.TYPE, Integer.TYPE});
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    @DexIgnore
    public static void a(Object obj, long j2, long j3) {
        f.a(obj, j2, j3);
    }

    @DexIgnore
    public static void b(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int i2 = (((int) j2) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a(obj, j3) & (~(255 << i2))));
    }

    @DexIgnore
    public static void c(Object obj, long j2, boolean z) {
        b(obj, j2, z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static byte g(Object obj, long j2) {
        return (byte) (a(obj, -4 & j2) >>> ((int) (((~j2) & 3) << 3)));
    }

    @DexIgnore
    public static void a(Object obj, long j2, boolean z) {
        f.a(obj, j2, z);
    }

    @DexIgnore
    public static void a(Object obj, long j2, float f2) {
        f.a(obj, j2, f2);
    }

    @DexIgnore
    public static void b(Object obj, long j2, boolean z) {
        a(obj, j2, z ? (byte) 1 : 0);
    }

    @DexIgnore
    public static void a(Object obj, long j2, double d2) {
        f.a(obj, j2, d2);
    }

    @DexIgnore
    public static void a(Object obj, long j2, Object obj2) {
        f.a.putObject(obj, j2, obj2);
    }

    @DexIgnore
    public static byte a(byte[] bArr, long j2) {
        return f.f(bArr, i + j2);
    }

    @DexIgnore
    public static void a(byte[] bArr, long j2, byte b2) {
        f.a((Object) bArr, i + j2, b2);
    }

    @DexIgnore
    public static void a(byte[] bArr, long j2, long j3, long j4) {
        f.a(bArr, j2, j3, j4);
    }

    @DexIgnore
    public static void a(long j2, byte b2) {
        f.a(j2, b2);
    }

    @DexIgnore
    public static long a(ByteBuffer byteBuffer) {
        return f.b(byteBuffer, j);
    }

    @DexIgnore
    public static Field a(Class<?> cls, String str) {
        try {
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static void a(Object obj, long j2, byte b2) {
        long j3 = -4 & j2;
        int a2 = a(obj, j3);
        int i2 = ((~((int) j2)) & 3) << 3;
        a(obj, j3, ((255 & b2) << i2) | (a2 & (~(255 << i2))));
    }
}
