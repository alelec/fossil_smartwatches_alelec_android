package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wi */
public abstract class C3225wi {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.database.DataSetObservable f10647a; // = new android.database.DataSetObservable();

    @DexIgnore
    /* renamed from: a */
    public abstract int mo17455a();

    @DexIgnore
    /* renamed from: a */
    public int mo17456a(java.lang.Object obj) {
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.CharSequence mo17457a(int i) {
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo17459a(android.view.ViewGroup viewGroup, int i) {
        mo17458a((android.view.View) viewGroup, i);
        throw null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17461a(android.os.Parcelable parcelable, java.lang.ClassLoader classLoader) {
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public void mo17462a(android.view.View view) {
    }

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo17466a(android.view.View view, java.lang.Object obj);

    @DexIgnore
    /* renamed from: b */
    public float mo17467b(int i) {
        return 1.0f;
    }

    @DexIgnore
    /* renamed from: b */
    public android.os.Parcelable mo17468b() {
        return null;
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: b */
    public void mo17470b(android.view.View view) {
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: b */
    public void mo17471b(android.view.View view, int i, java.lang.Object obj) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17472b(android.view.ViewGroup viewGroup) {
        mo17470b((android.view.View) viewGroup);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo17474c(android.database.DataSetObserver dataSetObserver) {
        this.f10647a.unregisterObserver(dataSetObserver);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17465a(android.view.ViewGroup viewGroup, int i, java.lang.Object obj) {
        mo17463a((android.view.View) viewGroup, i, obj);
        throw null;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17473b(android.view.ViewGroup viewGroup, int i, java.lang.Object obj) {
        mo17471b((android.view.View) viewGroup, i, obj);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17464a(android.view.ViewGroup viewGroup) {
        mo17462a((android.view.View) viewGroup);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo17469b(android.database.DataSetObserver dataSetObserver) {
        synchronized (this) {
        }
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public java.lang.Object mo17458a(android.view.View view, int i) {
        throw new java.lang.UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public void mo17463a(android.view.View view, int i, java.lang.Object obj) {
        throw new java.lang.UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17460a(android.database.DataSetObserver dataSetObserver) {
        this.f10647a.registerObserver(dataSetObserver);
    }
}
