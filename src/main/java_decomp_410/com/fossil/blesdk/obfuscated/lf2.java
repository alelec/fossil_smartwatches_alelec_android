package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lf2 extends kf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public long x;

    /*
    static {
        z.put(R.id.back_iv, 1);
        z.put(R.id.ftv_title, 2);
        z.put(R.id.search_time_zone, 3);
        z.put(R.id.clear_iv, 4);
        z.put(R.id.ftv_label_current, 5);
        z.put(R.id.line, 6);
        z.put(R.id.ftv_current_timezone, 7);
        z.put(R.id.timezone_recycler_view, 8);
        z.put(R.id.rvai_timezone, 9);
    }
    */

    @DexIgnore
    public lf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 10, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[1], objArr[4], objArr[7], objArr[5], objArr[2], objArr[6], objArr[9], objArr[3], objArr[8]);
        this.x = -1;
        this.w = objArr[0];
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
