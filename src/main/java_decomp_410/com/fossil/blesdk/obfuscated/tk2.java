package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tk2 implements MembersInjector<DeviceHelper> {
    @DexIgnore
    public static void a(DeviceHelper deviceHelper, en2 en2) {
        deviceHelper.a = en2;
    }

    @DexIgnore
    public static void a(DeviceHelper deviceHelper, DeviceRepository deviceRepository) {
        deviceHelper.b = deviceRepository;
    }
}
