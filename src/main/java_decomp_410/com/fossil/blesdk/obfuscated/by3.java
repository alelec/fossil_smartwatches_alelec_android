package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import java.util.LinkedHashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class by3 implements tx3 {
    @DexIgnore
    public /* final */ LinkedHashMap<String, Bitmap> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexIgnore
    public by3(Context context) {
        this(oy3.a(context));
    }

    @DexIgnore
    public Bitmap a(String str) {
        if (str != null) {
            synchronized (this) {
                Bitmap bitmap = this.a.get(str);
                if (bitmap != null) {
                    this.f++;
                    return bitmap;
                }
                this.g++;
                return null;
            }
        }
        throw new NullPointerException("key == null");
    }

    @DexIgnore
    public final synchronized int size() {
        return this.c;
    }

    @DexIgnore
    public by3(int i) {
        if (i > 0) {
            this.b = i;
            this.a = new LinkedHashMap<>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("Max size must be positive.");
    }

    @DexIgnore
    public void a(String str, Bitmap bitmap) {
        if (str == null || bitmap == null) {
            throw new NullPointerException("key == null || bitmap == null");
        }
        synchronized (this) {
            this.d++;
            this.c += oy3.a(bitmap);
            Bitmap bitmap2 = (Bitmap) this.a.put(str, bitmap);
            if (bitmap2 != null) {
                this.c -= oy3.a(bitmap2);
            }
        }
        a(this.b);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0070, code lost:
        throw new java.lang.IllegalStateException(getClass().getName() + ".sizeOf() is reporting inconsistent results!");
     */
    @DexIgnore
    public final void a(int i) {
        while (true) {
            synchronized (this) {
                if (this.c < 0 || (this.a.isEmpty() && this.c != 0)) {
                } else if (this.c <= i) {
                    break;
                } else if (this.a.isEmpty()) {
                    break;
                } else {
                    Map.Entry next = this.a.entrySet().iterator().next();
                    this.a.remove((String) next.getKey());
                    this.c -= oy3.a((Bitmap) next.getValue());
                    this.e++;
                }
            }
        }
    }

    @DexIgnore
    public final synchronized int a() {
        return this.b;
    }
}
