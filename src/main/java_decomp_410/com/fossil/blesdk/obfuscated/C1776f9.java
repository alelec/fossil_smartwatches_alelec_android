package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f9 */
public class C1776f9 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field f5100a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f5101b;

    @DexIgnore
    /* renamed from: c */
    public static java.lang.reflect.Field f5102c;

    @DexIgnore
    /* renamed from: d */
    public static boolean f5103d;

    @DexIgnore
    /* renamed from: e */
    public static java.util.WeakHashMap<android.view.View, java.lang.String> f5104e;

    @DexIgnore
    /* renamed from: f */
    public static java.util.WeakHashMap<android.view.View, com.fossil.blesdk.obfuscated.C2110j9> f5105f; // = null;

    @DexIgnore
    /* renamed from: g */
    public static java.lang.reflect.Field f5106g;

    @DexIgnore
    /* renamed from: h */
    public static boolean f5107h; // = false;

    @DexIgnore
    /* renamed from: i */
    public static java.lang.ThreadLocal<android.graphics.Rect> f5108i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f9$a")
    /* renamed from: com.fossil.blesdk.obfuscated.f9$a */
    public static class C1777a implements android.view.View.OnApplyWindowInsetsListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C1468b9 f5109a;

        @DexIgnore
        public C1777a(com.fossil.blesdk.obfuscated.C1468b9 b9Var) {
            this.f5109a = b9Var;
        }

        @DexIgnore
        public android.view.WindowInsets onApplyWindowInsets(android.view.View view, android.view.WindowInsets windowInsets) {
            return (android.view.WindowInsets) com.fossil.blesdk.obfuscated.C2468n9.m11098a(this.f5109a.mo1534a(view, com.fossil.blesdk.obfuscated.C2468n9.m11097a((java.lang.Object) windowInsets)));
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.f9$b */
    public interface C1778b {
        @DexIgnore
        /* renamed from: a */
        boolean mo10792a(android.view.View view, android.view.KeyEvent keyEvent);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.f9$c")
    /* renamed from: com.fossil.blesdk.obfuscated.f9$c */
    public static class C1779c {

        @DexIgnore
        /* renamed from: d */
        public static /* final */ java.util.ArrayList<java.lang.ref.WeakReference<android.view.View>> f5110d; // = new java.util.ArrayList<>();

        @DexIgnore
        /* renamed from: a */
        public java.util.WeakHashMap<android.view.View, java.lang.Boolean> f5111a; // = null;

        @DexIgnore
        /* renamed from: b */
        public android.util.SparseArray<java.lang.ref.WeakReference<android.view.View>> f5112b; // = null;

        @DexIgnore
        /* renamed from: c */
        public java.lang.ref.WeakReference<android.view.KeyEvent> f5113c; // = null;

        @DexIgnore
        /* renamed from: a */
        public final android.util.SparseArray<java.lang.ref.WeakReference<android.view.View>> mo10793a() {
            if (this.f5112b == null) {
                this.f5112b = new android.util.SparseArray<>();
            }
            return this.f5112b;
        }

        @DexIgnore
        /* renamed from: b */
        public final android.view.View mo10796b(android.view.View view, android.view.KeyEvent keyEvent) {
            java.util.WeakHashMap<android.view.View, java.lang.Boolean> weakHashMap = this.f5111a;
            if (weakHashMap != null && weakHashMap.containsKey(view)) {
                if (view instanceof android.view.ViewGroup) {
                    android.view.ViewGroup viewGroup = (android.view.ViewGroup) view;
                    for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                        android.view.View b = mo10796b(viewGroup.getChildAt(childCount), keyEvent);
                        if (b != null) {
                            return b;
                        }
                    }
                }
                if (mo10798c(view, keyEvent)) {
                    return view;
                }
            }
            return null;
        }

        @DexIgnore
        /* renamed from: c */
        public final boolean mo10798c(android.view.View view, android.view.KeyEvent keyEvent) {
            java.util.ArrayList arrayList = (java.util.ArrayList) view.getTag(com.fossil.blesdk.obfuscated.C2942t5.tag_unhandled_key_listeners);
            if (arrayList == null) {
                return false;
            }
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                if (((com.fossil.blesdk.obfuscated.C1776f9.C1778b) arrayList.get(size)).mo10792a(view, keyEvent)) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C1776f9.C1779c m6862a(android.view.View view) {
            com.fossil.blesdk.obfuscated.C1776f9.C1779c cVar = (com.fossil.blesdk.obfuscated.C1776f9.C1779c) view.getTag(com.fossil.blesdk.obfuscated.C2942t5.tag_unhandled_key_event_manager);
            if (cVar != null) {
                return cVar;
            }
            com.fossil.blesdk.obfuscated.C1776f9.C1779c cVar2 = new com.fossil.blesdk.obfuscated.C1776f9.C1779c();
            view.setTag(com.fossil.blesdk.obfuscated.C2942t5.tag_unhandled_key_event_manager, cVar2);
            return cVar2;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo10795a(android.view.View view, android.view.KeyEvent keyEvent) {
            if (keyEvent.getAction() == 0) {
                mo10797b();
            }
            android.view.View b = mo10796b(view, keyEvent);
            if (keyEvent.getAction() == 0) {
                int keyCode = keyEvent.getKeyCode();
                if (b != null && !android.view.KeyEvent.isModifierKey(keyCode)) {
                    mo10793a().put(keyCode, new java.lang.ref.WeakReference(b));
                }
            }
            return b != null;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo10797b() {
            java.util.WeakHashMap<android.view.View, java.lang.Boolean> weakHashMap = this.f5111a;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            if (!f5110d.isEmpty()) {
                synchronized (f5110d) {
                    if (this.f5111a == null) {
                        this.f5111a = new java.util.WeakHashMap<>();
                    }
                    for (int size = f5110d.size() - 1; size >= 0; size--) {
                        android.view.View view = (android.view.View) f5110d.get(size).get();
                        if (view == null) {
                            f5110d.remove(size);
                        } else {
                            this.f5111a.put(view, java.lang.Boolean.TRUE);
                            for (android.view.ViewParent parent = view.getParent(); parent instanceof android.view.View; parent = parent.getParent()) {
                                this.f5111a.put((android.view.View) parent, java.lang.Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo10794a(android.view.KeyEvent keyEvent) {
            java.lang.ref.WeakReference<android.view.KeyEvent> weakReference = this.f5113c;
            if (weakReference != null && weakReference.get() == keyEvent) {
                return false;
            }
            this.f5113c = new java.lang.ref.WeakReference<>(keyEvent);
            java.lang.ref.WeakReference weakReference2 = null;
            android.util.SparseArray<java.lang.ref.WeakReference<android.view.View>> a = mo10793a();
            if (keyEvent.getAction() == 1) {
                int indexOfKey = a.indexOfKey(keyEvent.getKeyCode());
                if (indexOfKey >= 0) {
                    weakReference2 = a.valueAt(indexOfKey);
                    a.removeAt(indexOfKey);
                }
            }
            if (weakReference2 == null) {
                weakReference2 = a.get(keyEvent.getKeyCode());
            }
            if (weakReference2 == null) {
                return false;
            }
            android.view.View view = (android.view.View) weakReference2.get();
            if (view != null && com.fossil.blesdk.obfuscated.C1776f9.m6859y(view)) {
                mo10798c(view, keyEvent);
            }
            return true;
        }
    }

    /*
    static {
        new java.util.concurrent.atomic.AtomicInteger(1);
    }
    */

    @DexIgnore
    /* renamed from: A */
    public static boolean m6794A(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return view.isNestedScrollingEnabled();
        }
        if (view instanceof com.fossil.blesdk.obfuscated.C3173w8) {
            return ((com.fossil.blesdk.obfuscated.C3173w8) view).isNestedScrollingEnabled();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: B */
    public static boolean m6795B(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return view.isPaddingRelative();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: C */
    public static void m6796C(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation();
        } else {
            view.postInvalidate();
        }
    }

    @DexIgnore
    /* renamed from: D */
    public static void m6797D(android.view.View view) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 20) {
            view.requestApplyInsets();
        } else if (i >= 16) {
            view.requestFitSystemWindows();
        }
    }

    @DexIgnore
    /* renamed from: E */
    public static void m6798E(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.stopNestedScroll();
        } else if (view instanceof com.fossil.blesdk.obfuscated.C3173w8) {
            ((com.fossil.blesdk.obfuscated.C3173w8) view).stopNestedScroll();
        }
    }

    @DexIgnore
    /* renamed from: F */
    public static void m6799F(android.view.View view) {
        float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Rect m6800a() {
        if (f5108i == null) {
            f5108i = new java.lang.ThreadLocal<>();
        }
        android.graphics.Rect rect = f5108i.get();
        if (rect == null) {
            rect = new android.graphics.Rect();
            f5108i.set(rect);
        }
        rect.setEmpty();
        return rect;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6827b(android.view.View view, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            view.setHasTransientState(z);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static void m6830c(android.view.View view, float f) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.setTranslationZ(f);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static android.graphics.PorterDuff.Mode m6833d(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintMode();
        }
        if (view instanceof com.fossil.blesdk.obfuscated.C1708e9) {
            return ((com.fossil.blesdk.obfuscated.C1708e9) view).getSupportBackgroundTintMode();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: e */
    public static void m6836e(android.view.View view, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            view.setAccessibilityLiveRegion(i);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public static void m6838f(android.view.View view, int i) {
        int i2 = android.os.Build.VERSION.SDK_INT;
        if (i2 >= 19) {
            view.setImportantForAccessibility(i);
        } else if (i2 >= 16) {
            if (i == 4) {
                i = 2;
            }
            view.setImportantForAccessibility(i);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public static void m6840g(android.view.View view, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            view.setImportantForAutofill(i);
        }
    }

    @DexIgnore
    /* renamed from: h */
    public static boolean m6842h(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.getFitsSystemWindows();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: i */
    public static int m6843i(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.getImportantForAccessibility();
        }
        return 0;
    }

    @DexIgnore
    @android.annotation.SuppressLint({"InlinedApi"})
    /* renamed from: j */
    public static int m6844j(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            return view.getImportantForAutofill();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: k */
    public static int m6845k(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return view.getLayoutDirection();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: l */
    public static int m6846l(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumHeight();
        }
        if (!f5103d) {
            try {
                f5102c = android.view.View.class.getDeclaredField("mMinHeight");
                f5102c.setAccessible(true);
            } catch (java.lang.NoSuchFieldException unused) {
            }
            f5103d = true;
        }
        java.lang.reflect.Field field = f5102c;
        if (field == null) {
            return 0;
        }
        try {
            return ((java.lang.Integer) field.get(view)).intValue();
        } catch (java.lang.Exception unused2) {
            return 0;
        }
    }

    @DexIgnore
    /* renamed from: m */
    public static int m6847m(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.getMinimumWidth();
        }
        if (!f5101b) {
            try {
                f5100a = android.view.View.class.getDeclaredField("mMinWidth");
                f5100a.setAccessible(true);
            } catch (java.lang.NoSuchFieldException unused) {
            }
            f5101b = true;
        }
        java.lang.reflect.Field field = f5100a;
        if (field == null) {
            return 0;
        }
        try {
            return ((java.lang.Integer) field.get(view)).intValue();
        } catch (java.lang.Exception unused2) {
            return 0;
        }
    }

    @DexIgnore
    /* renamed from: n */
    public static int m6848n(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return view.getPaddingEnd();
        }
        return view.getPaddingRight();
    }

    @DexIgnore
    /* renamed from: o */
    public static int m6849o(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return view.getPaddingStart();
        }
        return view.getPaddingLeft();
    }

    @DexIgnore
    /* renamed from: p */
    public static android.view.ViewParent m6850p(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.getParentForAccessibility();
        }
        return view.getParent();
    }

    @DexIgnore
    /* renamed from: q */
    public static java.lang.String m6851q(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return view.getTransitionName();
        }
        java.util.WeakHashMap<android.view.View, java.lang.String> weakHashMap = f5104e;
        if (weakHashMap == null) {
            return null;
        }
        return weakHashMap.get(view);
    }

    @DexIgnore
    /* renamed from: r */
    public static float m6852r(android.view.View view) {
        return android.os.Build.VERSION.SDK_INT >= 21 ? view.getTranslationZ() : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: s */
    public static int m6853s(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.getWindowSystemUiVisibility();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: t */
    public static float m6854t(android.view.View view) {
        return android.os.Build.VERSION.SDK_INT >= 21 ? view.getZ() : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: u */
    public static boolean m6855u(android.view.View view) {
        if (f5107h) {
            return false;
        }
        if (f5106g == null) {
            try {
                f5106g = android.view.View.class.getDeclaredField("mAccessibilityDelegate");
                f5106g.setAccessible(true);
            } catch (Throwable unused) {
                f5107h = true;
                return false;
            }
        }
        try {
            if (f5106g.get(view) != null) {
                return true;
            }
            return false;
        } catch (Throwable unused2) {
            f5107h = true;
            return false;
        }
    }

    @DexIgnore
    /* renamed from: v */
    public static boolean m6856v(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 15) {
            return view.hasOnClickListeners();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: w */
    public static boolean m6857w(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.hasOverlappingRendering();
        }
        return true;
    }

    @DexIgnore
    /* renamed from: x */
    public static boolean m6858x(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.hasTransientState();
        }
        return false;
    }

    @DexIgnore
    /* renamed from: y */
    public static boolean m6859y(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return view.isAttachedToWindow();
        }
        return view.getWindowToken() != null;
    }

    @DexIgnore
    /* renamed from: z */
    public static boolean m6860z(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return view.isLaidOut();
        }
        return view.getWidth() > 0 && view.getHeight() > 0;
    }

    @DexIgnore
    /* renamed from: b */
    public static int m6822b(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            return view.getAccessibilityLiveRegion();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: c */
    public static android.content.res.ColorStateList m6829c(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return view.getBackgroundTintList();
        }
        if (view instanceof com.fossil.blesdk.obfuscated.C1708e9) {
            return ((com.fossil.blesdk.obfuscated.C1708e9) view).getSupportBackgroundTintList();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: e */
    public static android.graphics.Rect m6835e(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            return view.getClipBounds();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: g */
    public static float m6839g(android.view.View view) {
        return android.os.Build.VERSION.SDK_INT >= 21 ? view.getElevation() : com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: h */
    public static void m6841h(android.view.View view, int i) {
        if (view instanceof com.fossil.blesdk.obfuscated.C3099v8) {
            ((com.fossil.blesdk.obfuscated.C3099v8) view).mo1617a(i);
        } else if (i == 0) {
            m6798E(view);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public static android.view.Display m6837f(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            return view.getDisplay();
        }
        if (m6859y(view)) {
            return ((android.view.WindowManager) view.getContext().getSystemService("window")).getDefaultDisplay();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6826b(android.view.View view, int i, int i2, int i3, int i4) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            view.setPaddingRelative(i, i2, i3, i4);
        } else {
            view.setPadding(i, i2, i3, i4);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static void m6834d(android.view.View view, int i) {
        int i2 = android.os.Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            view.offsetTopAndBottom(i);
        } else if (i2 >= 21) {
            android.graphics.Rect a = m6800a();
            boolean z = false;
            android.view.ViewParent parent = view.getParent();
            if (parent instanceof android.view.View) {
                android.view.View view2 = (android.view.View) parent;
                a.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !a.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            m6825b(view, i);
            if (z && a.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((android.view.View) parent).invalidate(a);
            }
        } else {
            m6825b(view, i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6815a(android.view.View view, com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
        view.onInitializeAccessibilityNodeInfo(q9Var.mo15121w());
    }

    @DexIgnore
    /* renamed from: c */
    public static void m6832c(android.view.View view, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.setNestedScrollingEnabled(z);
        } else if (view instanceof com.fossil.blesdk.obfuscated.C3173w8) {
            ((com.fossil.blesdk.obfuscated.C3173w8) view).setNestedScrollingEnabled(z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6814a(android.view.View view, com.fossil.blesdk.obfuscated.C2300l8 l8Var) {
        view.setAccessibilityDelegate(l8Var == null ? null : l8Var.mo13206a());
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6824b(android.view.View view, float f) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.setElevation(f);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6806a(android.view.View view, int i, int i2, int i3, int i4) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation(i, i2, i3, i4);
        } else {
            view.postInvalidate(i, i2, i3, i4);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2468n9 m6823b(android.view.View view, com.fossil.blesdk.obfuscated.C2468n9 n9Var) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return n9Var;
        }
        android.view.WindowInsets windowInsets = (android.view.WindowInsets) com.fossil.blesdk.obfuscated.C2468n9.m11098a(n9Var);
        android.view.WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
        if (onApplyWindowInsets != windowInsets) {
            windowInsets = new android.view.WindowInsets(onApplyWindowInsets);
        }
        return com.fossil.blesdk.obfuscated.C2468n9.m11097a((java.lang.Object) windowInsets);
    }

    @DexIgnore
    /* renamed from: c */
    public static void m6831c(android.view.View view, int i) {
        int i2 = android.os.Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            view.offsetLeftAndRight(i);
        } else if (i2 >= 21) {
            android.graphics.Rect a = m6800a();
            boolean z = false;
            android.view.ViewParent parent = view.getParent();
            if (parent instanceof android.view.View) {
                android.view.View view2 = (android.view.View) parent;
                a.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
                z = !a.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            }
            m6804a(view, i);
            if (z && a.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
                ((android.view.View) parent).invalidate(a);
            }
        } else {
            m6804a(view, i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6816a(android.view.View view, java.lang.Runnable runnable) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimation(runnable);
        } else {
            view.postDelayed(runnable, android.animation.ValueAnimator.getFrameDelay());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6817a(android.view.View view, java.lang.Runnable runnable, long j) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            view.postOnAnimationDelayed(runnable, j);
        } else {
            view.postDelayed(runnable, android.animation.ValueAnimator.getFrameDelay() + j);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6825b(android.view.View view, int i) {
        view.offsetTopAndBottom(i);
        if (view.getVisibility() == 0) {
            m6799F(view);
            android.view.ViewParent parent = view.getParent();
            if (parent instanceof android.view.View) {
                m6799F((android.view.View) parent);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6820a(android.view.View view, int i, android.os.Bundle bundle) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            return view.performAccessibilityAction(i, bundle);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6808a(android.view.View view, android.graphics.Paint paint) {
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            view.setLayerPaint(paint);
            return;
        }
        view.setLayerType(view.getLayerType(), paint);
        view.invalidate();
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m6828b(android.view.View view, android.view.KeyEvent keyEvent) {
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return com.fossil.blesdk.obfuscated.C1776f9.C1779c.m6862a(view).mo10794a(keyEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2110j9 m6801a(android.view.View view) {
        if (f5105f == null) {
            f5105f = new java.util.WeakHashMap<>();
        }
        com.fossil.blesdk.obfuscated.C2110j9 j9Var = f5105f.get(view);
        if (j9Var != null) {
            return j9Var;
        }
        com.fossil.blesdk.obfuscated.C2110j9 j9Var2 = new com.fossil.blesdk.obfuscated.C2110j9(view);
        f5105f.put(view, j9Var2);
        return j9Var2;
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public static void m6803a(android.view.View view, float f) {
        view.setAlpha(f);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6818a(android.view.View view, java.lang.String str) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.setTransitionName(str);
            return;
        }
        if (f5104e == null) {
            f5104e = new java.util.WeakHashMap<>();
        }
        f5104e.put(view, str);
    }

    @DexIgnore
    @java.lang.Deprecated
    /* renamed from: a */
    public static void m6819a(android.view.View view, boolean z) {
        view.setFitsSystemWindows(z);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6812a(android.view.View view, com.fossil.blesdk.obfuscated.C1468b9 b9Var) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return;
        }
        if (b9Var == null) {
            view.setOnApplyWindowInsetsListener((android.view.View.OnApplyWindowInsetsListener) null);
        } else {
            view.setOnApplyWindowInsetsListener(new com.fossil.blesdk.obfuscated.C1776f9.C1777a(b9Var));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2468n9 m6802a(android.view.View view, com.fossil.blesdk.obfuscated.C2468n9 n9Var) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            return n9Var;
        }
        android.view.WindowInsets windowInsets = (android.view.WindowInsets) com.fossil.blesdk.obfuscated.C2468n9.m11098a(n9Var);
        android.view.WindowInsets dispatchApplyWindowInsets = view.dispatchApplyWindowInsets(windowInsets);
        if (dispatchApplyWindowInsets != windowInsets) {
            windowInsets = new android.view.WindowInsets(dispatchApplyWindowInsets);
        }
        return com.fossil.blesdk.obfuscated.C2468n9.m11097a((java.lang.Object) windowInsets);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6811a(android.view.View view, android.graphics.drawable.Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6807a(android.view.View view, android.content.res.ColorStateList colorStateList) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintList(colorStateList);
            if (android.os.Build.VERSION.SDK_INT == 21) {
                android.graphics.drawable.Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof com.fossil.blesdk.obfuscated.C1708e9) {
            ((com.fossil.blesdk.obfuscated.C1708e9) view).setSupportBackgroundTintList(colorStateList);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6809a(android.view.View view, android.graphics.PorterDuff.Mode mode) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            view.setBackgroundTintMode(mode);
            if (android.os.Build.VERSION.SDK_INT == 21) {
                android.graphics.drawable.Drawable background = view.getBackground();
                boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
                if (background != null && z) {
                    if (background.isStateful()) {
                        background.setState(view.getDrawableState());
                    }
                    view.setBackground(background);
                }
            }
        } else if (view instanceof com.fossil.blesdk.obfuscated.C1708e9) {
            ((com.fossil.blesdk.obfuscated.C1708e9) view).setSupportBackgroundTintMode(mode);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6804a(android.view.View view, int i) {
        view.offsetLeftAndRight(i);
        if (view.getVisibility() == 0) {
            m6799F(view);
            android.view.ViewParent parent = view.getParent();
            if (parent instanceof android.view.View) {
                m6799F((android.view.View) parent);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6810a(android.view.View view, android.graphics.Rect rect) {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            view.setClipBounds(rect);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6805a(android.view.View view, int i, int i2) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            view.setScrollIndicators(i, i2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6813a(android.view.View view, com.fossil.blesdk.obfuscated.C1540c9 c9Var) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            view.setPointerIcon((android.view.PointerIcon) (c9Var != null ? c9Var.mo9433a() : null));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6821a(android.view.View view, android.view.KeyEvent keyEvent) {
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        return com.fossil.blesdk.obfuscated.C1776f9.C1779c.m6862a(view).mo10795a(view, keyEvent);
    }
}
