package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorSpace;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.PreferredColorSpace;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.facebook.places.internal.LocationScannerImpl;
import com.zendesk.sdk.network.impl.CachingAuthorizingOkHttp3Downloader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ws {
    @DexIgnore
    public static /* final */ ko<DecodeFormat> f; // = ko.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", DecodeFormat.DEFAULT);
    @DexIgnore
    public static /* final */ ko<PreferredColorSpace> g; // = ko.a("com.bumptech.glide.load.resource.bitmap.Downsampler.PreferredColorSpace", PreferredColorSpace.SRGB);
    @DexIgnore
    public static /* final */ ko<Boolean> h; // = ko.a("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", false);
    @DexIgnore
    public static /* final */ ko<Boolean> i; // = ko.a("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", false);
    @DexIgnore
    public static /* final */ Set<String> j; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"image/vnd.wap.wbmp", "image/x-ico"})));
    @DexIgnore
    public static /* final */ b k; // = new a();
    @DexIgnore
    public static /* final */ Set<ImageHeaderParser.ImageType> l; // = Collections.unmodifiableSet(EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));
    @DexIgnore
    public static /* final */ Queue<BitmapFactory.Options> m; // = uw.a(0);
    @DexIgnore
    public /* final */ jq a;
    @DexIgnore
    public /* final */ DisplayMetrics b;
    @DexIgnore
    public /* final */ gq c;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> d;
    @DexIgnore
    public /* final */ bt e; // = bt.b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public void a(jq jqVar, Bitmap bitmap) {
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(jq jqVar, Bitmap bitmap) throws IOException;
    }

    /*
    static {
        ko<DownsampleStrategy> koVar = DownsampleStrategy.f;
    }
    */

    @DexIgnore
    public ws(List<ImageHeaderParser> list, DisplayMetrics displayMetrics, jq jqVar, gq gqVar) {
        this.d = list;
        tw.a(displayMetrics);
        this.b = displayMetrics;
        tw.a(jqVar);
        this.a = jqVar;
        tw.a(gqVar);
        this.c = gqVar;
    }

    @DexIgnore
    public static boolean a(int i2) {
        return i2 == 90 || i2 == 270;
    }

    @DexIgnore
    public static int b(double d2) {
        if (d2 > 1.0d) {
            d2 = 1.0d / d2;
        }
        return (int) Math.round(d2 * 2.147483647E9d);
    }

    @DexIgnore
    public static int c(double d2) {
        return (int) (d2 + 0.5d);
    }

    @DexIgnore
    public static void c(BitmapFactory.Options options) {
        d(options);
        synchronized (m) {
            m.offer(options);
        }
    }

    @DexIgnore
    public static void d(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        if (Build.VERSION.SDK_INT >= 26) {
            options.inPreferredColorSpace = null;
            options.outColorSpace = null;
            options.outConfig = null;
        }
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    @DexIgnore
    public aq<Bitmap> a(InputStream inputStream, int i2, int i3, lo loVar) throws IOException {
        return a(inputStream, i2, i3, loVar, k);
    }

    @DexIgnore
    public boolean a(InputStream inputStream) {
        return true;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer) {
        return true;
    }

    @DexIgnore
    public static int[] b(InputStream inputStream, BitmapFactory.Options options, b bVar, jq jqVar) throws IOException {
        options.inJustDecodeBounds = true;
        a(inputStream, options, bVar, jqVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    @DexIgnore
    public aq<Bitmap> a(InputStream inputStream, int i2, int i3, lo loVar, b bVar) throws IOException {
        lo loVar2 = loVar;
        tw.a(inputStream.markSupported(), "You must provide an InputStream that supports mark()");
        byte[] bArr = (byte[]) this.c.b(65536, byte[].class);
        BitmapFactory.Options a2 = a();
        a2.inTempStorage = bArr;
        DecodeFormat decodeFormat = (DecodeFormat) loVar2.a(f);
        PreferredColorSpace preferredColorSpace = (PreferredColorSpace) loVar2.a(g);
        try {
            return ps.a(a(inputStream, a2, (DownsampleStrategy) loVar2.a(DownsampleStrategy.f), decodeFormat, preferredColorSpace, loVar2.a(i) != null && ((Boolean) loVar2.a(i)).booleanValue(), i2, i3, ((Boolean) loVar2.a(h)).booleanValue(), bVar), this.a);
        } finally {
            c(a2);
            this.c.put(bArr);
        }
    }

    @DexIgnore
    public static boolean b(BitmapFactory.Options options) {
        int i2 = options.inTargetDensity;
        if (i2 > 0) {
            int i3 = options.inDensity;
            return i3 > 0 && i2 != i3;
        }
    }

    @DexIgnore
    public final Bitmap a(InputStream inputStream, BitmapFactory.Options options, DownsampleStrategy downsampleStrategy, DecodeFormat decodeFormat, PreferredColorSpace preferredColorSpace, boolean z, int i2, int i3, boolean z2, b bVar) throws IOException {
        int i4;
        int i5;
        ws wsVar;
        int i6;
        int i7;
        int i8;
        InputStream inputStream2 = inputStream;
        BitmapFactory.Options options2 = options;
        b bVar2 = bVar;
        long a2 = pw.a();
        int[] b2 = b(inputStream2, options2, bVar2, this.a);
        boolean z3 = false;
        int i9 = b2[0];
        int i10 = b2[1];
        String str = options2.outMimeType;
        boolean z4 = (i9 == -1 || i10 == -1) ? false : z;
        int a3 = io.a(this.d, inputStream2, this.c);
        int a4 = gt.a(a3);
        boolean b3 = gt.b(a3);
        int i11 = i2;
        if (i11 == Integer.MIN_VALUE) {
            i5 = i3;
            i4 = a(a4) ? i10 : i9;
        } else {
            i5 = i3;
            i4 = i11;
        }
        int i12 = i5 == Integer.MIN_VALUE ? a(a4) ? i9 : i10 : i5;
        ImageHeaderParser.ImageType b4 = io.b(this.d, inputStream2, this.c);
        jq jqVar = this.a;
        ImageHeaderParser.ImageType imageType = b4;
        a(b4, inputStream, bVar, jqVar, downsampleStrategy, a4, i9, i10, i4, i12, options);
        int i13 = a3;
        String str2 = str;
        int i14 = i10;
        int i15 = i9;
        b bVar3 = bVar2;
        BitmapFactory.Options options3 = options2;
        a(inputStream, decodeFormat, z4, b3, options, i4, i12);
        boolean z5 = Build.VERSION.SDK_INT >= 19;
        if (options3.inSampleSize == 1 || z5) {
            wsVar = this;
            if (wsVar.a(imageType)) {
                if (i15 < 0 || i14 < 0 || !z2 || !z5) {
                    float f2 = b(options) ? ((float) options3.inTargetDensity) / ((float) options3.inDensity) : 1.0f;
                    int i16 = options3.inSampleSize;
                    float f3 = (float) i16;
                    i8 = Math.round(((float) ((int) Math.ceil((double) (((float) i15) / f3)))) * f2);
                    i7 = Math.round(((float) ((int) Math.ceil((double) (((float) i14) / f3)))) * f2);
                    if (Log.isLoggable("Downsampler", 2)) {
                        Log.v("Downsampler", "Calculated target [" + i8 + "x" + i7 + "] for source [" + i15 + "x" + i14 + "], sampleSize: " + i16 + ", targetDensity: " + options3.inTargetDensity + ", density: " + options3.inDensity + ", density multiplier: " + f2);
                    }
                } else {
                    i8 = i4;
                    i7 = i12;
                }
                if (i8 > 0 && i7 > 0) {
                    a(options3, wsVar.a, i8, i7);
                }
            }
        } else {
            wsVar = this;
        }
        int i17 = Build.VERSION.SDK_INT;
        if (i17 >= 28) {
            if (preferredColorSpace == PreferredColorSpace.DISPLAY_P3) {
                ColorSpace colorSpace = options3.outColorSpace;
                if (colorSpace != null && colorSpace.isWideGamut()) {
                    z3 = true;
                }
            }
            options3.inPreferredColorSpace = ColorSpace.get(z3 ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB);
        } else if (i17 >= 26) {
            options3.inPreferredColorSpace = ColorSpace.get(ColorSpace.Named.SRGB);
        }
        Bitmap a5 = a(inputStream, options3, bVar3, wsVar.a);
        bVar3.a(wsVar.a, a5);
        if (Log.isLoggable("Downsampler", 2)) {
            i6 = i13;
            a(i15, i14, str2, options, a5, i2, i3, a2);
        } else {
            i6 = i13;
        }
        Bitmap bitmap = null;
        if (a5 != null) {
            a5.setDensity(wsVar.b.densityDpi);
            bitmap = gt.a(wsVar.a, a5, i6);
            if (!a5.equals(bitmap)) {
                wsVar.a.a(a5);
            }
        }
        return bitmap;
    }

    @DexIgnore
    public static void a(ImageHeaderParser.ImageType imageType, InputStream inputStream, b bVar, jq jqVar, DownsampleStrategy downsampleStrategy, int i2, int i3, int i4, int i5, int i6, BitmapFactory.Options options) throws IOException {
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        double d2;
        ImageHeaderParser.ImageType imageType2 = imageType;
        DownsampleStrategy downsampleStrategy2 = downsampleStrategy;
        int i13 = i3;
        int i14 = i4;
        int i15 = i5;
        int i16 = i6;
        BitmapFactory.Options options2 = options;
        if (i13 <= 0 || i14 <= 0) {
            String str = "x";
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Unable to determine dimensions for: " + imageType2 + " with target [" + i15 + str + i16 + "]");
                return;
            }
            return;
        }
        if (a(i2)) {
            i7 = i13;
            i8 = i14;
        } else {
            i8 = i13;
            i7 = i14;
        }
        float b2 = downsampleStrategy2.b(i8, i7, i15, i16);
        if (b2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            DownsampleStrategy.SampleSizeRounding a2 = downsampleStrategy2.a(i8, i7, i15, i16);
            if (a2 != null) {
                float f2 = (float) i8;
                float f3 = (float) i7;
                int c2 = i8 / c((double) (b2 * f2));
                int c3 = i7 / c((double) (b2 * f3));
                if (a2 == DownsampleStrategy.SampleSizeRounding.MEMORY) {
                    i9 = Math.max(c2, c3);
                } else {
                    i9 = Math.min(c2, c3);
                }
                String str2 = "x";
                if (Build.VERSION.SDK_INT > 23 || !j.contains(options2.outMimeType)) {
                    i10 = Math.max(1, Integer.highestOneBit(i9));
                    if (a2 == DownsampleStrategy.SampleSizeRounding.MEMORY && ((float) i10) < 1.0f / b2) {
                        i10 <<= 1;
                    }
                } else {
                    i10 = 1;
                }
                options2.inSampleSize = i10;
                if (imageType2 == ImageHeaderParser.ImageType.JPEG) {
                    float min = (float) Math.min(i10, 8);
                    i11 = (int) Math.ceil((double) (f2 / min));
                    i12 = (int) Math.ceil((double) (f3 / min));
                    int i17 = i10 / 8;
                    if (i17 > 0) {
                        i11 /= i17;
                        i12 /= i17;
                    }
                } else {
                    if (imageType2 == ImageHeaderParser.ImageType.PNG || imageType2 == ImageHeaderParser.ImageType.PNG_A) {
                        float f4 = (float) i10;
                        i11 = (int) Math.floor((double) (f2 / f4));
                        d2 = Math.floor((double) (f3 / f4));
                    } else if (imageType2 == ImageHeaderParser.ImageType.WEBP || imageType2 == ImageHeaderParser.ImageType.WEBP_A) {
                        if (Build.VERSION.SDK_INT >= 24) {
                            float f5 = (float) i10;
                            i11 = Math.round(f2 / f5);
                            i12 = Math.round(f3 / f5);
                        } else {
                            float f6 = (float) i10;
                            i11 = (int) Math.floor((double) (f2 / f6));
                            d2 = Math.floor((double) (f3 / f6));
                        }
                    } else if (i8 % i10 == 0 && i7 % i10 == 0) {
                        i11 = i8 / i10;
                        i12 = i7 / i10;
                    } else {
                        int[] b3 = b(inputStream, options2, bVar, jqVar);
                        i11 = b3[0];
                        i12 = b3[1];
                    }
                    i12 = (int) d2;
                }
                double b4 = (double) downsampleStrategy2.b(i11, i12, i15, i16);
                if (Build.VERSION.SDK_INT >= 19) {
                    options2.inTargetDensity = a(b4);
                    options2.inDensity = b(b4);
                }
                if (b(options)) {
                    options2.inScaled = true;
                } else {
                    options2.inTargetDensity = 0;
                    options2.inDensity = 0;
                }
                if (Log.isLoggable("Downsampler", 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Calculate scaling, source: [");
                    sb.append(i3);
                    String str3 = str2;
                    sb.append(str3);
                    sb.append(i4);
                    sb.append("], degreesToRotate: ");
                    sb.append(i2);
                    sb.append(", target: [");
                    sb.append(i15);
                    sb.append(str3);
                    sb.append(i16);
                    sb.append("], power of two scaled: [");
                    sb.append(i11);
                    sb.append(str3);
                    sb.append(i12);
                    sb.append("], exact scale factor: ");
                    sb.append(b2);
                    sb.append(", power of 2 sample size: ");
                    sb.append(i10);
                    sb.append(", adjusted scale factor: ");
                    sb.append(b4);
                    sb.append(", target density: ");
                    sb.append(options2.inTargetDensity);
                    sb.append(", density: ");
                    sb.append(options2.inDensity);
                    Log.v("Downsampler", sb.toString());
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("Cannot round with null rounding");
        }
        String str4 = "x";
        int i18 = i13;
        throw new IllegalArgumentException("Cannot scale with factor: " + b2 + " from: " + downsampleStrategy2 + ", source: [" + i18 + str4 + i14 + "], target: [" + i15 + str4 + i16 + "]");
    }

    @DexIgnore
    public static int a(double d2) {
        int b2 = b(d2);
        int c2 = c(((double) b2) * d2);
        return c((d2 / ((double) (((float) c2) / ((float) b2)))) * ((double) c2));
    }

    @DexIgnore
    public final boolean a(ImageHeaderParser.ImageType imageType) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return l.contains(imageType);
    }

    @DexIgnore
    public final void a(InputStream inputStream, DecodeFormat decodeFormat, boolean z, boolean z2, BitmapFactory.Options options, int i2, int i3) {
        if (!this.e.a(i2, i3, options, z, z2)) {
            if (decodeFormat == DecodeFormat.PREFER_ARGB_8888 || Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return;
            }
            boolean z3 = false;
            try {
                z3 = io.b(this.d, inputStream, this.c).hasAlpha();
            } catch (IOException e2) {
                if (Log.isLoggable("Downsampler", 3)) {
                    Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + decodeFormat, e2);
                }
            }
            options.inPreferredConfig = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            if (options.inPreferredConfig == Bitmap.Config.RGB_565) {
                options.inDither = true;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        throw r1;
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005d */
    public static Bitmap a(InputStream inputStream, BitmapFactory.Options options, b bVar, jq jqVar) throws IOException {
        if (options.inJustDecodeBounds) {
            inputStream.mark(CachingAuthorizingOkHttp3Downloader.MIN_DISK_CACHE_SIZE);
        } else {
            bVar.a();
        }
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        String str = options.outMimeType;
        gt.a().lock();
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, (Rect) null, options);
            gt.a().unlock();
            if (options.inJustDecodeBounds) {
                inputStream.reset();
            }
            return decodeStream;
        } catch (IllegalArgumentException e2) {
            IOException a2 = a(e2, i2, i3, str, options);
            if (Log.isLoggable("Downsampler", 3)) {
                Log.d("Downsampler", "Failed to decode with inBitmap, trying again without Bitmap re-use", a2);
            }
            if (options.inBitmap != null) {
                inputStream.reset();
                jqVar.a(options.inBitmap);
                options.inBitmap = null;
                Bitmap a3 = a(inputStream, options, bVar, jqVar);
                gt.a().unlock();
                return a3;
            }
            throw a2;
        } catch (Throwable th) {
            gt.a().unlock();
            throw th;
        }
    }

    @DexIgnore
    public static void a(int i2, int i3, String str, BitmapFactory.Options options, Bitmap bitmap, int i4, int i5, long j2) {
        Log.v("Downsampler", "Decoded " + a(bitmap) + " from [" + i2 + "x" + i3 + "] " + str + " with inBitmap " + a(options) + " for [" + i4 + "x" + i5 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + Thread.currentThread().getName() + ", duration: " + pw.a(j2));
    }

    @DexIgnore
    public static String a(BitmapFactory.Options options) {
        return a(options.inBitmap);
    }

    @DexIgnore
    @TargetApi(19)
    public static String a(Bitmap bitmap) {
        String str;
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    @DexIgnore
    public static IOException a(IllegalArgumentException illegalArgumentException, int i2, int i3, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i2 + ", outHeight: " + i3 + ", outMimeType: " + str + ", inBitmap: " + a(options), illegalArgumentException);
    }

    @DexIgnore
    @TargetApi(26)
    public static void a(BitmapFactory.Options options, jq jqVar, int i2, int i3) {
        Bitmap.Config config;
        if (Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig != Bitmap.Config.HARDWARE) {
            config = options.outConfig;
        } else {
            return;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = jqVar.b(i2, i3, config);
    }

    @DexIgnore
    public static synchronized BitmapFactory.Options a() {
        BitmapFactory.Options poll;
        synchronized (ws.class) {
            synchronized (m) {
                poll = m.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                d(poll);
            }
        }
        return poll;
    }
}
