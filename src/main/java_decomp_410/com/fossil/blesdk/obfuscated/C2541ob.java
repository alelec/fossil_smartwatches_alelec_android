package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ob */
public final class C2541ob implements java.io.Closeable, com.fossil.blesdk.obfuscated.zg4 {

    @DexIgnore
    /* renamed from: e */
    public /* final */ kotlin.coroutines.CoroutineContext f8041e;

    @DexIgnore
    public C2541ob(kotlin.coroutines.CoroutineContext coroutineContext) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(coroutineContext, "context");
        this.f8041e = coroutineContext;
    }

    @DexIgnore
    /* renamed from: A */
    public kotlin.coroutines.CoroutineContext mo14303A() {
        return this.f8041e;
    }

    @DexIgnore
    public void close() {
        com.fossil.blesdk.obfuscated.ji4.m23978a(mo14303A(), (java.util.concurrent.CancellationException) null, 1, (java.lang.Object) null);
    }
}
