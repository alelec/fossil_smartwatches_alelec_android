package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ze0;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class oc1 extends fe0<Object> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends s31 {
        @DexIgnore
        public /* final */ xn1<Void> e;

        @DexIgnore
        public a(xn1<Void> xn1) {
            this.e = xn1;
        }

        @DexIgnore
        public final void a(o31 o31) {
            gf0.a(o31.G(), this.e);
        }
    }

    @DexIgnore
    public oc1(Context context) {
        super(context, sc1.c, null, (df0) new re0());
    }

    @DexIgnore
    public final r31 a(xn1<Boolean> xn1) {
        return new pd1(this, xn1);
    }

    @DexIgnore
    public wn1<Void> a(qc1 qc1) {
        return gf0.a(a((ze0.a<?>) af0.a(qc1, qc1.class.getSimpleName())));
    }

    @DexIgnore
    public wn1<Void> a(LocationRequest locationRequest, qc1 qc1, Looper looper) {
        i41 a2 = i41.a(locationRequest);
        ze0 a3 = af0.a(qc1, p41.a(looper), qc1.class.getSimpleName());
        return a(new nd1(this, a3, a2, a3), new od1(this, a3.b()));
    }

    @DexIgnore
    public wn1<Location> i() {
        return a(new md1(this));
    }
}
