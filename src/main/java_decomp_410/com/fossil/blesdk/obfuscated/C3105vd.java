package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vd */
public class C3105vd<T> extends com.fossil.blesdk.obfuscated.C2723qd<T> {

    @DexIgnore
    /* renamed from: s */
    public /* final */ boolean f10226s;

    @DexIgnore
    /* renamed from: t */
    public /* final */ java.lang.Object f10227t;

    @DexIgnore
    /* renamed from: u */
    public /* final */ com.fossil.blesdk.obfuscated.C2307ld<?, T> f10228u;

    @DexIgnore
    public C3105vd(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar) {
        super(qdVar.f8608i.mo15941n(), qdVar.f8604e, qdVar.f8605f, (com.fossil.blesdk.obfuscated.C2723qd.C2726c) null, qdVar.f8607h);
        this.f10228u = qdVar.mo12723d();
        this.f10226s = qdVar.mo12726g();
        this.f8609j = qdVar.f8609j;
        this.f10227t = qdVar.mo12724e();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12718a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd.C2728e eVar) {
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C2307ld<?, T> mo12723d() {
        return this.f10228u;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.Object mo12724e() {
        return this.f10227t;
    }

    @DexIgnore
    /* renamed from: g */
    public boolean mo12726g() {
        return this.f10226s;
    }

    @DexIgnore
    /* renamed from: h */
    public void mo12727h(int i) {
    }

    @DexIgnore
    /* renamed from: h */
    public boolean mo15192h() {
        return true;
    }

    @DexIgnore
    /* renamed from: i */
    public boolean mo15194i() {
        return true;
    }
}
