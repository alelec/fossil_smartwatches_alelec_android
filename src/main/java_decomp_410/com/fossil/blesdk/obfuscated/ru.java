package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ru {
    @DexIgnore
    void a();

    @DexIgnore
    void b();

    @DexIgnore
    void c();
}
