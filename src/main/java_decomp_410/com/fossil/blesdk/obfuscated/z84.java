package com.fossil.blesdk.obfuscated;

import io.reactivex.disposables.RunnableDisposable;
import io.reactivex.internal.disposables.EmptyDisposable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z84 {
    @DexIgnore
    public static y84 a(Runnable runnable) {
        k94.a(runnable, "run is null");
        return new RunnableDisposable(runnable);
    }

    @DexIgnore
    public static y84 a() {
        return EmptyDisposable.INSTANCE;
    }
}
