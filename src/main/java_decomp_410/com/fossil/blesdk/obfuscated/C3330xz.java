package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xz */
public class C3330xz {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ java.util.Map<java.lang.String, java.lang.String> f11114g; // = java.util.Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", com.facebook.appevents.AppEventsConstants.EVENT_PARAM_VALUE_YES);

    @DexIgnore
    /* renamed from: h */
    public static /* final */ short[] f11115h; // = {10, 20, 30, 60, 120, 300};

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.Object f11116a; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1588cz f11117b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f11118c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C3330xz.C3333c f11119d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C3330xz.C3332b f11120e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.Thread f11121f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xz$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xz$a */
    public static final class C3331a implements com.fossil.blesdk.obfuscated.C3330xz.C3334d {
        @DexIgnore
        /* renamed from: a */
        public boolean mo4164a() {
            return true;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.xz$b */
    public interface C3332b {
        @DexIgnore
        /* renamed from: a */
        boolean mo4172a();
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.xz$c */
    public interface C3333c {
        @DexIgnore
        /* renamed from: a */
        java.io.File[] mo4168a();

        @DexIgnore
        /* renamed from: b */
        java.io.File[] mo4169b();

        @DexIgnore
        /* renamed from: c */
        java.io.File[] mo4170c();
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.xz$d */
    public interface C3334d {
        @DexIgnore
        /* renamed from: a */
        boolean mo4164a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xz$e")
    /* renamed from: com.fossil.blesdk.obfuscated.xz$e */
    public class C3335e extends com.fossil.blesdk.obfuscated.l54 {

        @DexIgnore
        /* renamed from: e */
        public /* final */ float f11122e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C3330xz.C3334d f11123f;

        @DexIgnore
        public C3335e(float f, com.fossil.blesdk.obfuscated.C3330xz.C3334d dVar) {
            this.f11122e = f;
            this.f11123f = dVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17933a() {
            try {
                mo17934b();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "An unexpected error occurred while attempting to upload crash reports.", e);
            }
            java.lang.Thread unused = com.fossil.blesdk.obfuscated.C3330xz.this.f11121f = null;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo17934b() {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30060d("CrashlyticsCore", "Starting report processing in " + this.f11122e + " second(s)...");
            float f = this.f11122e;
            if (f > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                try {
                    java.lang.Thread.sleep((long) (f * 1000.0f));
                } catch (java.lang.InterruptedException unused) {
                    java.lang.Thread.currentThread().interrupt();
                    return;
                }
            }
            java.util.List<com.crashlytics.android.core.Report> a = com.fossil.blesdk.obfuscated.C3330xz.this.mo17930a();
            if (!com.fossil.blesdk.obfuscated.C3330xz.this.f11120e.mo4172a()) {
                if (a.isEmpty() || this.f11123f.mo4164a()) {
                    int i = 0;
                    while (!a.isEmpty() && !com.fossil.blesdk.obfuscated.C3330xz.this.f11120e.mo4172a()) {
                        com.fossil.blesdk.obfuscated.y44 g2 = com.fossil.blesdk.obfuscated.q44.m26805g();
                        g2.mo30060d("CrashlyticsCore", "Attempting to send " + a.size() + " report(s)");
                        for (com.crashlytics.android.core.Report a2 : a) {
                            com.fossil.blesdk.obfuscated.C3330xz.this.mo17932a(a2);
                        }
                        a = com.fossil.blesdk.obfuscated.C3330xz.this.mo17930a();
                        if (!a.isEmpty()) {
                            int i2 = i + 1;
                            long j = (long) com.fossil.blesdk.obfuscated.C3330xz.f11115h[java.lang.Math.min(i, com.fossil.blesdk.obfuscated.C3330xz.f11115h.length - 1)];
                            com.fossil.blesdk.obfuscated.y44 g3 = com.fossil.blesdk.obfuscated.q44.m26805g();
                            g3.mo30060d("CrashlyticsCore", "Report submisson: scheduling delayed retry in " + j + " seconds");
                            try {
                                java.lang.Thread.sleep(j * 1000);
                                i = i2;
                            } catch (java.lang.InterruptedException unused2) {
                                java.lang.Thread.currentThread().interrupt();
                                return;
                            }
                        }
                    }
                    return;
                }
                com.fossil.blesdk.obfuscated.y44 g4 = com.fossil.blesdk.obfuscated.q44.m26805g();
                g4.mo30060d("CrashlyticsCore", "User declined to send. Removing " + a.size() + " Report(s).");
                for (com.crashlytics.android.core.Report remove : a) {
                    remove.remove();
                }
            }
        }
    }

    @DexIgnore
    public C3330xz(java.lang.String str, com.fossil.blesdk.obfuscated.C1588cz czVar, com.fossil.blesdk.obfuscated.C3330xz.C3333c cVar, com.fossil.blesdk.obfuscated.C3330xz.C3332b bVar) {
        if (czVar != null) {
            this.f11117b = czVar;
            this.f11118c = str;
            this.f11119d = cVar;
            this.f11120e = bVar;
            return;
        }
        throw new java.lang.IllegalArgumentException("createReportCall must not be null.");
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo17931a(float f, com.fossil.blesdk.obfuscated.C3330xz.C3334d dVar) {
        if (this.f11121f != null) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Report upload has already been started.");
            return;
        }
        this.f11121f = new java.lang.Thread(new com.fossil.blesdk.obfuscated.C3330xz.C3335e(f, dVar), "Crashlytics Report Uploader");
        this.f11121f.start();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17932a(com.crashlytics.android.core.Report report) {
        boolean z;
        synchronized (this.f11116a) {
            z = false;
            try {
                boolean a = this.f11117b.mo9713a(new com.fossil.blesdk.obfuscated.C1520bz(this.f11118c, report));
                com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
                java.lang.StringBuilder sb = new java.lang.StringBuilder();
                sb.append("Crashlytics report upload ");
                sb.append(a ? "complete: " : "FAILED: ");
                sb.append(report.mo4193b());
                g.mo30064i("CrashlyticsCore", sb.toString());
                if (a) {
                    report.remove();
                    z = true;
                }
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.y44 g2 = com.fossil.blesdk.obfuscated.q44.m26805g();
                g2.mo30063e("CrashlyticsCore", "Error occurred sending report " + report, e);
            }
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<com.crashlytics.android.core.Report> mo17930a() {
        java.io.File[] c;
        java.io.File[] b;
        java.io.File[] a;
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Checking for crash reports...");
        synchronized (this.f11116a) {
            c = this.f11119d.mo4170c();
            b = this.f11119d.mo4169b();
            a = this.f11119d.mo4168a();
        }
        java.util.LinkedList linkedList = new java.util.LinkedList();
        if (c != null) {
            for (java.io.File path : c) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Found crash report " + path.getPath());
                linkedList.add(new com.fossil.blesdk.obfuscated.a00(c[r6]));
            }
        }
        java.util.HashMap hashMap = new java.util.HashMap();
        if (b != null) {
            for (java.io.File file : b) {
                java.lang.String c2 = com.crashlytics.android.core.CrashlyticsController.m2768c(file);
                if (!hashMap.containsKey(c2)) {
                    hashMap.put(c2, new java.util.LinkedList());
                }
                ((java.util.List) hashMap.get(c2)).add(file);
            }
        }
        for (java.lang.String str : hashMap.keySet()) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Found invalid session: " + str);
            java.util.List list = (java.util.List) hashMap.get(str);
            linkedList.add(new com.fossil.blesdk.obfuscated.C2082iz(str, (java.io.File[]) list.toArray(new java.io.File[list.size()])));
        }
        if (a != null) {
            for (java.io.File qzVar : a) {
                linkedList.add(new com.fossil.blesdk.obfuscated.C2776qz(qzVar));
            }
        }
        if (linkedList.isEmpty()) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "No reports found.");
        }
        return linkedList;
    }
}
