package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y42 implements Factory<FirmwareFileRepository> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public y42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static y42 a(n42 n42) {
        return new y42(n42);
    }

    @DexIgnore
    public static FirmwareFileRepository b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static FirmwareFileRepository c(n42 n42) {
        FirmwareFileRepository g = n42.g();
        n44.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }

    @DexIgnore
    public FirmwareFileRepository get() {
        return b(this.a);
    }
}
