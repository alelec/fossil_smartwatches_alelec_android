package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.database.entity.DeviceFile;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface k00 {
    @DexIgnore
    int a(String str);

    @DexIgnore
    long a(DeviceFile deviceFile);

    @DexIgnore
    List<DeviceFile> a(String str, byte b);

    @DexIgnore
    List<DeviceFile> a(String str, byte b, byte b2);

    @DexIgnore
    int b(String str, byte b, byte b2);

    @DexIgnore
    List<DeviceFile> b(String str, byte b);

    @DexIgnore
    int c(String str, byte b);
}
