package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.enums.Unit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface yj3 extends v52<xj3> {
    @DexIgnore
    void a();

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(Unit unit);

    @DexIgnore
    void b();

    @DexIgnore
    void c(Unit unit);

    @DexIgnore
    void d(Unit unit);

    @DexIgnore
    void e(Unit unit);
}
