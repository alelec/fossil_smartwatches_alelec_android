package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class au3 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public Calendar a;
    @DexIgnore
    public Calendar b;
    @DexIgnore
    public Calendar c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public /* final */ TreeMap<Long, Integer> f; // = new TreeMap<>();
    @DexIgnore
    public /* final */ int[] g; // = new int[49];
    @DexIgnore
    public int h;
    @DexIgnore
    public RecyclerViewHeartRateCalendar.b i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Calendar o; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ Context p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ /* synthetic */ au3 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(au3 au3, View view) {
            super(view);
            kd4.b(view, "itemView");
            this.b = au3;
            this.a = (FlexibleTextView) view;
        }

        @DexIgnore
        public final void a(int i) {
            String str;
            switch ((i / 7) % 7) {
                case 0:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__S_1);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                    break;
                case 1:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__F);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                    break;
                case 2:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__T_1);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                    break;
                case 3:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__W);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                    break;
                case 4:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__T);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                    break;
                case 5:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__M);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                    break;
                case 6:
                    str = sm2.a(this.b.p, (int) R.string.DashboardHybrid_Main_Steps7days_Label__S);
                    kd4.a((Object) str, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                    break;
                default:
                    str = "";
                    break;
            }
            this.a.setText(str);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public au3(Context context) {
        kd4.b(context, "mContext");
        this.p = context;
        int[][] iArr = new int[7][];
        for (int i2 = 0; i2 < 7; i2++) {
            iArr[i2] = new int[7];
        }
        for (int i3 = 0; i3 <= 6; i3++) {
            for (int i4 = 0; i4 <= 6; i4++) {
                iArr[i3][i4] = (i3 * 7) + i4;
            }
        }
        int[][] iArr2 = new int[7][];
        for (int i5 = 0; i5 < 7; i5++) {
            iArr2[i5] = new int[7];
        }
        for (int i6 = 0; i6 <= 6; i6++) {
            for (int i7 = 0; i7 <= 6; i7++) {
                iArr2[i6][i7] = iArr[i7][6 - i6];
            }
        }
        for (int i8 = 0; i8 <= 6; i8++) {
            System.arraycopy(iArr2[i8], 0, this.g, (i8 * 7) + 1, 6);
        }
        this.o = rk2.a(0, this.o);
    }

    @DexIgnore
    public final Calendar b() {
        return this.o;
    }

    @DexIgnore
    public final void c(Calendar calendar) {
        this.a = calendar;
    }

    @DexIgnore
    public final int d() {
        return this.d;
    }

    @DexIgnore
    public final int[] e() {
        return this.g;
    }

    @DexIgnore
    public final int f() {
        return this.m;
    }

    @DexIgnore
    public final int g() {
        return this.n;
    }

    @DexIgnore
    public int getItemCount() {
        if (this.a != null) {
            Calendar calendar = this.b;
            if (calendar != null) {
                if (calendar != null) {
                    int i2 = calendar.get(1);
                    Calendar calendar2 = this.a;
                    if (calendar2 != null) {
                        int i3 = (i2 - calendar2.get(1)) * 12;
                        Calendar calendar3 = this.b;
                        if (calendar3 != null) {
                            int i4 = i3 + calendar3.get(2);
                            Calendar calendar4 = this.a;
                            if (calendar4 != null) {
                                return ((i4 - calendar4.get(2)) + 1) * 49;
                            }
                            kd4.a();
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
        }
        return 0;
    }

    @DexIgnore
    public int getItemViewType(int i2) {
        return i2 % 7 == 0 ? 0 : 1;
    }

    @DexIgnore
    public final int h() {
        return this.l;
    }

    @DexIgnore
    public final TreeMap<Long, Integer> i() {
        return this.f;
    }

    @DexIgnore
    public final int j() {
        return this.j;
    }

    @DexIgnore
    public final RecyclerViewHeartRateCalendar.b k() {
        return this.i;
    }

    @DexIgnore
    public final int l() {
        return this.k;
    }

    @DexIgnore
    public final Calendar m() {
        return this.c;
    }

    @DexIgnore
    public final int n() {
        return this.e;
    }

    @DexIgnore
    public final Calendar o() {
        return this.a;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        kd4.b(viewHolder, "holder");
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == 0) {
            ((c) viewHolder).a(i2);
        } else if (itemViewType == 1) {
            ((b) viewHolder).a(i2);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        kd4.b(viewGroup, "parent");
        if (i2 == 0) {
            View inflate = LayoutInflater.from(this.p).inflate(R.layout.item_calendar_title, viewGroup, false);
            kd4.a((Object) inflate, "titleView");
            inflate.getLayoutParams().width = this.h;
            return new c(this, inflate);
        } else if (i2 != 1) {
            View inflate2 = LayoutInflater.from(this.p).inflate(R.layout.item_heartrate_calendar_day, viewGroup, false);
            kd4.a((Object) inflate2, "itemView");
            inflate2.getLayoutParams().width = this.h;
            return new b(this, inflate2);
        } else {
            View inflate3 = LayoutInflater.from(this.p).inflate(R.layout.item_heartrate_calendar_day, viewGroup, false);
            kd4.a((Object) inflate3, "itemView");
            inflate3.getLayoutParams().width = this.h;
            return new b(this, inflate3);
        }
    }

    @DexIgnore
    public final void a(Calendar calendar) {
        this.b = calendar;
    }

    @DexIgnore
    public final void b(Calendar calendar) {
        kd4.b(calendar, "currentDate");
        this.c = calendar;
        if (this.a == null) {
            this.a = this.c;
        }
        if (this.b == null) {
            this.b = this.c;
        }
    }

    @DexIgnore
    public final Calendar c() {
        return this.b;
    }

    @DexIgnore
    public final void a(RecyclerViewHeartRateCalendar.b bVar) {
        kd4.b(bVar, "listener");
        this.i = bVar;
    }

    @DexIgnore
    public final void c(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public final Calendar a(int i2) {
        Object clone = this.o.clone();
        if (clone != null) {
            Calendar calendar = (Calendar) clone;
            int i3 = this.g[i2 % 49];
            int i4 = calendar.get(7) - 1;
            if (i3 < i4 || i3 >= calendar.getActualMaximum(5) + i4) {
                return null;
            }
            calendar.add(5, i3 - i4);
            FLogger.INSTANCE.getLocal().d("CalendarHeartRateAdapter", "getCalendarItem day=" + calendar.get(5) + " month=" + calendar.get(2));
            return calendar;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.util.Calendar");
    }

    @DexIgnore
    public final void b(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void a(Map<Long, Integer> map, int i2, int i3, Calendar calendar) {
        int i4;
        kd4.b(map, "data");
        kd4.b(calendar, "calendar");
        FLogger.INSTANCE.getLocal().d("CalendarHeartRateAdapter", "setData");
        this.f.putAll(map);
        TreeMap<Long, Integer> treeMap = this.f;
        int i5 = 0;
        if (treeMap.isEmpty()) {
            i4 = 0;
        } else {
            i4 = 0;
            for (Map.Entry<Long, Integer> value : treeMap.entrySet()) {
                if (((Number) value.getValue()).intValue() > 0) {
                    i4++;
                }
            }
        }
        TreeMap<Long, Integer> treeMap2 = this.f;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry next : treeMap2.entrySet()) {
            if (((Number) next.getValue()).intValue() > 0) {
                linkedHashMap.put(next.getKey(), next.getValue());
            }
        }
        ArrayList arrayList = new ArrayList(linkedHashMap.size());
        for (Map.Entry value2 : linkedHashMap.entrySet()) {
            arrayList.add(Integer.valueOf(((Number) value2.getValue()).intValue()));
        }
        int i6 = kb4.i(arrayList);
        if (i4 > 0) {
            i5 = i6 / i4;
        }
        this.d = i5;
        this.o = calendar;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView e;
        @DexIgnore
        public /* final */ ImageView f;
        @DexIgnore
        public /* final */ /* synthetic */ au3 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(au3 au3, View view) {
            super(view);
            kd4.b(view, "itemView");
            this.g = au3;
            View findViewById = view.findViewById(R.id.ftvDay);
            kd4.a((Object) findViewById, "itemView.findViewById(R.id.ftvDay)");
            this.e = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(R.id.ivBackground);
            kd4.a((Object) findViewById2, "itemView.findViewById(R.id.ivBackground)");
            this.f = (ImageView) findViewById2;
            view.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(int i) {
            Integer num;
            if (i != -1) {
                Object clone = this.g.b().clone();
                if (clone != null) {
                    Calendar calendar = (Calendar) clone;
                    int i2 = calendar.get(7) - 1;
                    int i3 = this.g.e()[i % 49];
                    if (i3 < i2 || i3 >= calendar.getActualMaximum(5) + i2) {
                        this.e.setVisibility(4);
                        this.f.setVisibility(4);
                        return;
                    }
                    calendar.add(5, i3 - i2);
                    if (this.g.n() == -1) {
                        Calendar m = this.g.m();
                        if (m == null) {
                            kd4.a();
                            throw null;
                        } else if (rk2.d(m.getTime(), calendar.getTime())) {
                            this.g.c(i);
                            FlexibleTextView flexibleTextView = this.e;
                            flexibleTextView.setTypeface(flexibleTextView.getTypeface(), 1);
                            this.e.setVisibility(0);
                            this.f.setVisibility(4);
                            this.e.setText(String.valueOf(calendar.get(5)));
                            this.e.setTextColor(this.g.l());
                            TreeMap<Long, Integer> i4 = this.g.i();
                            rk2.d(calendar);
                            kd4.a((Object) calendar, "DateHelper.getStartOfDay(calendar)");
                            num = i4.get(Long.valueOf(calendar.getTimeInMillis()));
                            if (num != null && num.intValue() > 0) {
                                this.e.setText(String.valueOf(num.intValue()));
                                this.e.setTextColor(this.g.j());
                                this.f.setVisibility(0);
                                int f2 = num.intValue() > this.g.d() ? this.g.f() : this.g.h();
                                Boolean s = rk2.s(calendar.getTime());
                                kd4.a((Object) s, "DateHelper.isToday(calendar.time)");
                                if (s.booleanValue()) {
                                    a(this.f, f2, true);
                                    return;
                                } else {
                                    a(this, this.f, f2, false, 4, (Object) null);
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    }
                    FlexibleTextView flexibleTextView2 = this.e;
                    flexibleTextView2.setTypeface(flexibleTextView2.getTypeface(), 0);
                    this.e.setVisibility(0);
                    this.f.setVisibility(4);
                    this.e.setText(String.valueOf(calendar.get(5)));
                    this.e.setTextColor(this.g.l());
                    TreeMap<Long, Integer> i42 = this.g.i();
                    rk2.d(calendar);
                    kd4.a((Object) calendar, "DateHelper.getStartOfDay(calendar)");
                    num = i42.get(Long.valueOf(calendar.getTimeInMillis()));
                    if (num != null) {
                        return;
                    }
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type java.util.Calendar");
            }
        }

        @DexIgnore
        public void onClick(View view) {
            kd4.b(view, "v");
            int adapterPosition = getAdapterPosition();
            if (this.g.k() != null && adapterPosition != -1) {
                Calendar a = this.g.a(adapterPosition);
                if (a != null && !a.before(this.g.o()) && !a.after(this.g.c())) {
                    RecyclerViewHeartRateCalendar.b k = this.g.k();
                    if (k != null) {
                        k.a(adapterPosition, a);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, ImageView imageView, int i, boolean z, int i2, Object obj) {
            if ((i2 & 4) != 0) {
                z = false;
            }
            bVar.a(imageView, i, z);
        }

        @DexIgnore
        public final void a(ImageView imageView, int i, boolean z) {
            Drawable background = imageView.getBackground();
            if (background != null) {
                LayerDrawable layerDrawable = (LayerDrawable) background;
                try {
                    Drawable drawable = layerDrawable.getDrawable(0);
                    if (drawable != null) {
                        GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                        if (z) {
                            gradientDrawable.setStroke((int) ts3.a(2.0f), this.g.g());
                        } else {
                            gradientDrawable.setStroke((int) ts3.a(2.0f), 0);
                        }
                        if (layerDrawable.getNumberOfLayers() > 1) {
                            Drawable drawable2 = layerDrawable.getDrawable(1);
                            if (drawable2 != null) {
                                ((GradientDrawable) drawable2).setColor(i);
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                        }
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.GradientDrawable");
                } catch (Exception e2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("CalendarHeartRateAdapter", "DayViewHolder - e=" + e2);
                }
            } else {
                throw new TypeCastException("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
            }
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.j = i2;
        this.k = i3;
        this.l = i5;
        this.m = i6;
        this.n = i7;
    }
}
