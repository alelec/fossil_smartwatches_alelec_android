package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ut2;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m03 extends zr2 implements l03, ws3.g {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public k03 j;
    @DexIgnore
    public tr3<ce2> k;
    @DexIgnore
    public ut2 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return m03.n;
        }

        @DexIgnore
        public final m03 b() {
            return new m03();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ut2.b {
        @DexIgnore
        public /* final */ /* synthetic */ m03 a;

        @DexIgnore
        public b(m03 m03) {
            this.a = m03;
        }

        @DexIgnore
        public void a(ContactWrapper contactWrapper) {
            kd4.b(contactWrapper, "contactWrapper");
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(childFragmentManager, contactWrapper, contactWrapper.getCurrentHandGroup(), m03.b(this.a).h());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ m03 e;

        @DexIgnore
        public c(m03 m03) {
            this.e = m03;
        }

        @DexIgnore
        public final void onClick(View view) {
            m03.b(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ m03 e;
        @DexIgnore
        public /* final */ /* synthetic */ ce2 f;

        @DexIgnore
        public d(m03 m03, ce2 ce2) {
            this.e = m03;
            this.f = ce2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.f.s;
            kd4.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 > 0 ? 0 : 4);
            ut2 a = this.e.l;
            if (a != null) {
                a.a(String.valueOf(charSequence));
            }
            AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = this.f.t;
            kd4.a((Object) alphabetFastScrollRecyclerView, "binding.rvContacts");
            RecyclerView.m layoutManager = alphabetFastScrollRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                ((LinearLayoutManager) layoutManager).f(0, 0);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ce2 e;

        @DexIgnore
        public e(ce2 ce2) {
            this.e = ce2;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.q.setText("");
        }
    }

    /*
    static {
        String simpleName = m03.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationHybridContac\u2026nt::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ k03 b(m03 m03) {
        k03 k03 = m03.j;
        if (k03 != null) {
            return k03;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void H() {
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        k03 k03 = this.j;
        if (k03 != null) {
            k03.i();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Typeface typeface;
        kd4.b(layoutInflater, "inflater");
        ce2 ce2 = (ce2) qa.a(layoutInflater, R.layout.fragment_notification_hybrid_contact, viewGroup, false, O0());
        ce2.r.setOnClickListener(new c(this));
        ce2.q.addTextChangedListener(new d(this, ce2));
        ce2.s.setOnClickListener(new e(ce2));
        ce2.t.setIndexBarVisibility(true);
        ce2.t.setIndexBarHighLateTextVisibility(true);
        ce2.t.setIndexbarHighLateTextColor((int) R.color.greyishBrown);
        ce2.t.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        ce2.t.setIndexTextSize(9);
        ce2.t.setIndexBarTextColor((int) R.color.black);
        if (Build.VERSION.SDK_INT >= 26) {
            typeface = getResources().getFont(R.font.font_bold);
            kd4.a((Object) typeface, "resources.getFont(R.font.font_bold)");
        } else {
            typeface = Typeface.DEFAULT_BOLD;
            kd4.a((Object) typeface, "Typeface.DEFAULT_BOLD");
        }
        ce2.t.setTypeface(typeface);
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = ce2.t;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext()));
        alphabetFastScrollRecyclerView.setAdapter(this.l);
        this.k = new tr3<>(this, ce2);
        kd4.a((Object) ce2, "binding");
        return ce2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        k03 k03 = this.j;
        if (k03 != null) {
            k03.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        k03 k03 = this.j;
        if (k03 != null) {
            k03.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void x0() {
        ut2 ut2 = this.l;
        if (ut2 != null) {
            ut2.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(k03 k03) {
        kd4.b(k03, "presenter");
        this.j = k03;
    }

    @DexIgnore
    public void a(List<ContactWrapper> list, FilterQueryProvider filterQueryProvider, int i) {
        kd4.b(list, "listContactWrapper");
        kd4.b(filterQueryProvider, "filterQueryProvider");
        ut2 ut2 = new ut2((Cursor) null, list, i);
        ut2.a((ut2.b) new b(this));
        this.l = ut2;
        ut2 ut22 = this.l;
        if (ut22 != null) {
            ut22.a(filterQueryProvider);
            tr3<ce2> tr3 = this.k;
            if (tr3 != null) {
                ce2 a2 = tr3.a();
                if (a2 != null) {
                    AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = a2.t;
                    if (alphabetFastScrollRecyclerView != null) {
                        alphabetFastScrollRecyclerView.setAdapter(this.l);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void a(ArrayList<ContactWrapper> arrayList) {
        kd4.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(Cursor cursor) {
        ut2 ut2 = this.l;
        if (ut2 != null) {
            ut2.c(cursor);
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (str.hashCode() != 1018078562 || !str.equals("CONFIRM_REASSIGN_CONTACT")) {
            return;
        }
        if (i != R.id.tv_ok) {
            ut2 ut2 = this.l;
            if (ut2 != null) {
                ut2.notifyDataSetChanged();
            } else {
                kd4.a();
                throw null;
            }
        } else {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                ContactWrapper contactWrapper = (ContactWrapper) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER");
                if (contactWrapper != null) {
                    k03 k03 = this.j;
                    if (k03 != null) {
                        k03.a(contactWrapper);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }
}
