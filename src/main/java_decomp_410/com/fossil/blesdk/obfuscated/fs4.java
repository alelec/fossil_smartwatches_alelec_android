package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fs4 implements gr4<em4, Boolean> {
    @DexIgnore
    public static /* final */ fs4 a; // = new fs4();

    @DexIgnore
    public Boolean a(em4 em4) throws IOException {
        return Boolean.valueOf(em4.F());
    }
}
