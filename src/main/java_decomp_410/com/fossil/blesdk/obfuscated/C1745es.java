package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.es */
public class C1745es implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.Set<java.lang.String> f4923b; // = java.util.Collections.unmodifiableSet(new java.util.HashSet(java.util.Arrays.asList(new java.lang.String[]{"http", com.facebook.internal.Utility.URL_SCHEME})));

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> f4924a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.es$a")
    /* renamed from: com.fossil.blesdk.obfuscated.es$a */
    public static class C1746a implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.InputStream> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1745es(wrVar.mo17509a(com.fossil.blesdk.obfuscated.C2340lr.class, java.io.InputStream.class));
        }
    }

    @DexIgnore
    public C1745es(com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> srVar) {
        this.f4924a = srVar;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.io.InputStream> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return this.f4924a.mo8911a(new com.fossil.blesdk.obfuscated.C2340lr(uri.toString()), i, i2, loVar);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        return f4923b.contains(uri.getScheme());
    }
}
