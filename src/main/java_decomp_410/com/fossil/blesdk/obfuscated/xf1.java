package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.StreetViewPanoramaView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xf1 extends we1 {
    @DexIgnore
    public /* final */ /* synthetic */ ee1 e;

    @DexIgnore
    public xf1(StreetViewPanoramaView.a aVar, ee1 ee1) {
        this.e = ee1;
    }

    @DexIgnore
    public final void a(me1 me1) throws RemoteException {
        this.e.a(new ge1(me1));
    }
}
