package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cn0 implements Parcelable.Creator<ud0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        int i = 0;
        PendingIntent pendingIntent = null;
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                i2 = SafeParcelReader.q(parcel, a);
            } else if (a2 == 3) {
                pendingIntent = (PendingIntent) SafeParcelReader.a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                str = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new ud0(i, i2, pendingIntent, str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ud0[i];
    }
}
