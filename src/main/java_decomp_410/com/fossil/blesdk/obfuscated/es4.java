package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class es4<T> implements gr4<T, RequestBody> {
    @DexIgnore
    public static /* final */ es4<Object> a; // = new es4<>();
    @DexIgnore
    public static /* final */ am4 b; // = am4.b("text/plain; charset=UTF-8");

    @DexIgnore
    public RequestBody a(T t) throws IOException {
        return RequestBody.a(b, String.valueOf(t));
    }
}
