package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qv */
public interface C2771qv<R> {
    @DexIgnore
    /* renamed from: a */
    boolean mo14884a(com.bumptech.glide.load.engine.GlideException glideException, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<R> bwVar, boolean z);

    @DexIgnore
    /* renamed from: a */
    boolean mo14885a(R r, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<R> bwVar, com.bumptech.glide.load.DataSource dataSource, boolean z);
}
