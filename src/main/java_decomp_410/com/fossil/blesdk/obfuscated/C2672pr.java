package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pr */
public final class C2672pr implements com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.File> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f8432a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.pr$a */
    public static final class C2673a implements com.fossil.blesdk.obfuscated.C2984tr<android.net.Uri, java.io.File> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Context f8433a;

        @DexIgnore
        public C2673a(android.content.Context context) {
            this.f8433a = context;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<android.net.Uri, java.io.File> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C2672pr(this.f8433a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.pr$b */
    public static class C2674b implements com.fossil.blesdk.obfuscated.C2902so<java.io.File> {

        @DexIgnore
        /* renamed from: g */
        public static /* final */ java.lang.String[] f8434g; // = {"_data"};

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.content.Context f8435e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ android.net.Uri f8436f;

        @DexIgnore
        public C2674b(android.content.Context context, android.net.Uri uri) {
            this.f8435e = context;
            this.f8436f = uri;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super java.io.File> aVar) {
            android.database.Cursor query = this.f8435e.getContentResolver().query(this.f8436f, f8434g, (java.lang.String) null, (java.lang.String[]) null, (java.lang.String) null);
            java.lang.String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (android.text.TextUtils.isEmpty(str)) {
                aVar.mo9251a((java.lang.Exception) new java.io.FileNotFoundException("Failed to find file path for: " + this.f8436f));
                return;
            }
            aVar.mo9252a(new java.io.File(str));
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return com.bumptech.glide.load.DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public java.lang.Class<java.io.File> getDataClass() {
            return java.io.File.class;
        }
    }

    @DexIgnore
    public C2672pr(android.content.Context context) {
        this.f8432a = context;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.io.File> mo8911a(android.net.Uri uri, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(uri), new com.fossil.blesdk.obfuscated.C2672pr.C2674b(this.f8432a, uri));
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(android.net.Uri uri) {
        return com.fossil.blesdk.obfuscated.C1740ep.m6537b(uri);
    }
}
