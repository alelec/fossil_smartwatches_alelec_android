package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t51 extends vb1<t51> {
    @DexIgnore
    public static volatile t51[] g;
    @DexIgnore
    public w51 c; // = null;
    @DexIgnore
    public u51 d; // = null;
    @DexIgnore
    public Boolean e; // = null;
    @DexIgnore
    public String f; // = null;

    @DexIgnore
    public t51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static t51[] e() {
        if (g == null) {
            synchronized (zb1.b) {
                if (g == null) {
                    g = new t51[0];
                }
            }
        }
        return g;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        w51 w51 = this.c;
        if (w51 != null) {
            ub1.a(1, (ac1) w51);
        }
        u51 u51 = this.d;
        if (u51 != null) {
            ub1.a(2, (ac1) u51);
        }
        Boolean bool = this.e;
        if (bool != null) {
            ub1.a(3, bool.booleanValue());
        }
        String str = this.f;
        if (str != null) {
            ub1.a(4, str);
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof t51)) {
            return false;
        }
        t51 t51 = (t51) obj;
        w51 w51 = this.c;
        if (w51 == null) {
            if (t51.c != null) {
                return false;
            }
        } else if (!w51.equals(t51.c)) {
            return false;
        }
        u51 u51 = this.d;
        if (u51 == null) {
            if (t51.d != null) {
                return false;
            }
        } else if (!u51.equals(t51.d)) {
            return false;
        }
        Boolean bool = this.e;
        if (bool == null) {
            if (t51.e != null) {
                return false;
            }
        } else if (!bool.equals(t51.e)) {
            return false;
        }
        String str = this.f;
        if (str == null) {
            if (t51.f != null) {
                return false;
            }
        } else if (!str.equals(t51.f)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(t51.b);
        }
        xb1 xb12 = t51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i;
        int i2;
        w51 w51 = this.c;
        int hashCode = (t51.class.getName().hashCode() + 527) * 31;
        int i3 = 0;
        if (w51 == null) {
            i = 0;
        } else {
            i = w51.hashCode();
        }
        int i4 = hashCode + i;
        u51 u51 = this.d;
        int i5 = i4 * 31;
        if (u51 == null) {
            i2 = 0;
        } else {
            i2 = u51.hashCode();
        }
        int i6 = (i5 + i2) * 31;
        Boolean bool = this.e;
        int hashCode2 = (i6 + (bool == null ? 0 : bool.hashCode())) * 31;
        String str = this.f;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i3 = this.b.hashCode();
        }
        return hashCode3 + i3;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        w51 w51 = this.c;
        if (w51 != null) {
            a += ub1.b(1, (ac1) w51);
        }
        u51 u51 = this.d;
        if (u51 != null) {
            a += ub1.b(2, (ac1) u51);
        }
        Boolean bool = this.e;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(3) + 1;
        }
        String str = this.f;
        return str != null ? a + ub1.b(4, str) : a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 10) {
                if (this.c == null) {
                    this.c = new w51();
                }
                tb1.a((ac1) this.c);
            } else if (c2 == 18) {
                if (this.d == null) {
                    this.d = new u51();
                }
                tb1.a((ac1) this.d);
            } else if (c2 == 24) {
                this.e = Boolean.valueOf(tb1.d());
            } else if (c2 == 34) {
                this.f = tb1.b();
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
