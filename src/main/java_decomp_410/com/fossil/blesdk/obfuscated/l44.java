package com.fossil.blesdk.obfuscated;

import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l44<T> implements Provider<T>, s34<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Provider<T> a;
    @DexIgnore
    public volatile Object b; // = c;

    @DexIgnore
    public l44(Provider<T> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Object a(Object obj, Object obj2) {
        if (!(obj != c) || obj == obj2) {
            return obj2;
        }
        throw new IllegalStateException("Scoped provider was invoked recursively returning different results: " + obj + " & " + obj2 + ". This is likely due to a circular dependency.");
    }

    @DexIgnore
    public T get() {
        T t = this.b;
        if (t == c) {
            synchronized (this) {
                t = this.b;
                if (t == c) {
                    t = this.a.get();
                    a(this.b, t);
                    this.b = t;
                    this.a = null;
                }
            }
        }
        return t;
    }

    @DexIgnore
    public static <P extends Provider<T>, T> Provider<T> a(P p) {
        n44.a(p);
        if (p instanceof l44) {
            return p;
        }
        return new l44(p);
    }
}
