package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hg3 extends zr2 implements gg3 {
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public fg3 j;
    @DexIgnore
    public s62 k;
    @DexIgnore
    public tr3<gd2> l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final hg3 a() {
            return new hg3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewPager.i {
        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void g(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UpdateFirmwareFragment", "updateOTAProgress progress=" + i);
        if (isActive()) {
            tr3<gd2> tr3 = this.l;
            if (tr3 != null) {
                gd2 a2 = tr3.a();
                if (a2 != null) {
                    ProgressBar progressBar = a2.t;
                    if (progressBar != null) {
                        progressBar.setProgress(i);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void i(List<? extends Explore> list) {
        kd4.b(list, "data");
        tr3<gd2> tr3 = this.l;
        if (tr3 != null) {
            gd2 a2 = tr3.a();
            if (a2 != null) {
                if (FossilDeviceSerialPatternUtil.isDianaDevice(PortfolioApp.W.c().e())) {
                    FlexibleTextView flexibleTextView = a2.q;
                    kd4.a((Object) flexibleTextView, "it.ftvUpdate");
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_DianaCards_Title__ExploreYourWatch));
                } else {
                    FlexibleTextView flexibleTextView2 = a2.q;
                    kd4.a((Object) flexibleTextView2, "it.ftvUpdate");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_PairedTutorial_HybridCards_Title__ExploreYourWatch));
                }
            }
            s62 s62 = this.k;
            if (s62 != null) {
                s62.a(list);
            } else {
                kd4.d("mAdapterUpdateFirmware");
                throw null;
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        gd2 gd2 = (gd2) qa.a(layoutInflater, R.layout.fragment_home_update_firmware, viewGroup, false, O0());
        this.l = new tr3<>(this, gd2);
        kd4.a((Object) gd2, "binding");
        return gd2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        fg3 fg3 = this.j;
        if (fg3 == null) {
            return;
        }
        if (fg3 != null) {
            fg3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        fg3 fg3 = this.j;
        if (fg3 == null) {
            return;
        }
        if (fg3 != null) {
            fg3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.k = new s62(new ArrayList());
        tr3<gd2> tr3 = this.l;
        if (tr3 != null) {
            gd2 a2 = tr3.a();
            if (a2 != null) {
                ProgressBar progressBar = a2.t;
                kd4.a((Object) progressBar, "binding.progressUpdate");
                progressBar.setMax(1000);
                FlexibleTextView flexibleTextView = a2.r;
                kd4.a((Object) flexibleTextView, "binding.ftvUpdateWarning");
                flexibleTextView.setVisibility(0);
                RecyclerViewPager recyclerViewPager = a2.u;
                kd4.a((Object) recyclerViewPager, "binding.rvpTutorial");
                recyclerViewPager.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
                RecyclerViewPager recyclerViewPager2 = a2.u;
                kd4.a((Object) recyclerViewPager2, "binding.rvpTutorial");
                s62 s62 = this.k;
                if (s62 != null) {
                    recyclerViewPager2.setAdapter(s62);
                    a2.s.a((RecyclerView) a2.u, 0);
                    a2.s.setOnPageChangeListener(new b());
                    return;
                }
                kd4.d("mAdapterUpdateFirmware");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(fg3 fg3) {
        kd4.b(fg3, "presenter");
        this.j = fg3;
    }
}
