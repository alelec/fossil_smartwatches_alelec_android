package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ak2 implements sr<bk2, InputStream> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tr<bk2, InputStream> {
        @DexIgnore
        public ak2 a(wr wrVar) {
            kd4.b(wrVar, "multiFactory");
            return new ak2();
        }
    }

    @DexIgnore
    public boolean a(bk2 bk2) {
        kd4.b(bk2, "avatarModel");
        return true;
    }

    @DexIgnore
    public sr.a<InputStream> a(bk2 bk2, int i, int i2, lo loVar) {
        kd4.b(bk2, "avatarModel");
        kd4.b(loVar, "options");
        return new sr.a<>(bk2, new a(this, bk2));
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements so<InputStream> {
        @DexIgnore
        public so<InputStream> e;
        @DexIgnore
        public volatile boolean f;
        @DexIgnore
        public /* final */ bk2 g;

        @DexIgnore
        public a(ak2 ak2, bk2 bk2) {
            kd4.b(bk2, "mAvatarModel");
            this.g = bk2;
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super InputStream> aVar) {
            kd4.b(priority, "priority");
            kd4.b(aVar, Constants.CALLBACK);
            try {
                if (!TextUtils.isEmpty(this.g.c())) {
                    this.e = new yo(new lr(this.g.c()), 100);
                } else if (this.g.b() != null) {
                    this.e = new cp(PortfolioApp.W.c().getContentResolver(), this.g.b());
                }
                so<InputStream> soVar = this.e;
                if (soVar != null) {
                    soVar.a(priority, aVar);
                }
            } catch (Exception unused) {
                so<InputStream> soVar2 = this.e;
                if (soVar2 != null) {
                    soVar2.a();
                }
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String a = this.g.a();
            if (a == null) {
                a = "";
            }
            Bitmap b = vr3.b(a);
            if (b != null) {
                b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            }
            aVar.a(this.f ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.REMOTE;
        }

        @DexIgnore
        public void cancel() {
            this.f = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public void a() {
            so<InputStream> soVar = this.e;
            if (soVar != null) {
                soVar.a();
            }
        }
    }
}
