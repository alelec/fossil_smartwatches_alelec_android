package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class j62 {
    @DexIgnore
    public /* final */ k62 a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends i62.c, E extends i62.a> implements i62.d<R, E> {
        @DexIgnore
        public /* final */ i62.d<R, E> a;
        @DexIgnore
        public /* final */ j62 b;

        @DexIgnore
        public a(i62.d<R, E> dVar, j62 j62) {
            this.a = dVar;
            this.b = j62;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(R r) {
            this.b.a(r, this.a);
        }

        @DexIgnore
        public void a(E e) {
            this.b.a(e, this.a);
        }
    }

    @DexIgnore
    public j62(k62 k62) {
        this.a = k62;
    }

    @DexIgnore
    public <Q extends i62.b, R extends i62.c, E extends i62.a> void a(i62<Q, R, E> i62, Q q, i62.d<R, E> dVar) {
        i62.b(q);
        i62.a((i62.d<R, E>) new a(dVar, this));
        es3.c();
        this.a.execute(new g42(i62));
    }

    @DexIgnore
    public static /* synthetic */ void a(i62 i62) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UseCaseHandler", "execute: getIdlingResource = " + es3.b());
        i62.b();
        if (!es3.b().a()) {
            es3.a();
        }
    }

    @DexIgnore
    public <R extends i62.c, E extends i62.a> void a(R r, i62.d<R, E> dVar) {
        this.a.a(r, dVar);
    }

    @DexIgnore
    public <R extends i62.c, E extends i62.a> void a(E e, i62.d<R, E> dVar) {
        this.a.a(e, dVar);
    }
}
