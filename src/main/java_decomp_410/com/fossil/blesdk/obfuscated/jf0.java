package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.he0;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jf0 {
    @DexIgnore
    public /* final */ Map<BasePendingResult<?>, Boolean> a; // = Collections.synchronizedMap(new WeakHashMap());
    @DexIgnore
    public /* final */ Map<xn1<?>, Boolean> b; // = Collections.synchronizedMap(new WeakHashMap());

    @DexIgnore
    public final void a(BasePendingResult<? extends me0> basePendingResult, boolean z) {
        this.a.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.a((he0.a) new kf0(this, basePendingResult));
    }

    @DexIgnore
    public final void b() {
        a(false, ve0.n);
    }

    @DexIgnore
    public final void c() {
        a(true, ph0.d);
    }

    @DexIgnore
    public final <TResult> void a(xn1<TResult> xn1, boolean z) {
        this.b.put(xn1, Boolean.valueOf(z));
        xn1.a().a(new lf0(this, xn1));
    }

    @DexIgnore
    public final boolean a() {
        return !this.a.isEmpty() || !this.b.isEmpty();
    }

    @DexIgnore
    public final void a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.a) {
            hashMap = new HashMap(this.a);
        }
        synchronized (this.b) {
            hashMap2 = new HashMap(this.b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).b(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((xn1) entry2.getKey()).b((Exception) new ApiException(status));
            }
        }
    }
}
