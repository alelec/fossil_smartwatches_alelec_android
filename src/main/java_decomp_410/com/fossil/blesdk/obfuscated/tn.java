package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.ContextWrapper;
import android.widget.ImageView;
import com.bumptech.glide.Registry;
import com.fossil.blesdk.obfuscated.rn;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tn extends ContextWrapper {
    @DexIgnore
    public static /* final */ yn<?, ?> k; // = new qn();
    @DexIgnore
    public /* final */ gq a;
    @DexIgnore
    public /* final */ Registry b;
    @DexIgnore
    public /* final */ zv c;
    @DexIgnore
    public /* final */ rn.a d;
    @DexIgnore
    public /* final */ List<qv<Object>> e;
    @DexIgnore
    public /* final */ Map<Class<?>, yn<?, ?>> f;
    @DexIgnore
    public /* final */ qp g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public rv j;

    @DexIgnore
    public tn(Context context, gq gqVar, Registry registry, zv zvVar, rn.a aVar, Map<Class<?>, yn<?, ?>> map, List<qv<Object>> list, qp qpVar, boolean z, int i2) {
        super(context.getApplicationContext());
        this.a = gqVar;
        this.b = registry;
        this.c = zvVar;
        this.d = aVar;
        this.e = list;
        this.f = map;
        this.g = qpVar;
        this.h = z;
        this.i = i2;
    }

    @DexIgnore
    public <T> yn<?, T> a(Class<T> cls) {
        yn<?, T> ynVar = this.f.get(cls);
        if (ynVar == null) {
            for (Map.Entry next : this.f.entrySet()) {
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    ynVar = (yn) next.getValue();
                }
            }
        }
        return ynVar == null ? k : ynVar;
    }

    @DexIgnore
    public List<qv<Object>> b() {
        return this.e;
    }

    @DexIgnore
    public synchronized rv c() {
        if (this.j == null) {
            this.j = (rv) this.d.build().O();
        }
        return this.j;
    }

    @DexIgnore
    public qp d() {
        return this.g;
    }

    @DexIgnore
    public int e() {
        return this.i;
    }

    @DexIgnore
    public Registry f() {
        return this.b;
    }

    @DexIgnore
    public boolean g() {
        return this.h;
    }

    @DexIgnore
    public <X> cw<ImageView, X> a(ImageView imageView, Class<X> cls) {
        return this.c.a(imageView, cls);
    }

    @DexIgnore
    public gq a() {
        return this.a;
    }
}
