package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.it */
public class C2065it<T> implements com.fossil.blesdk.obfuscated.C2427mo<T, android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: d */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Long> f6189d; // = com.fossil.blesdk.obfuscated.C2232ko.m9713a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new com.fossil.blesdk.obfuscated.C2065it.C2066a());

    @DexIgnore
    /* renamed from: e */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Integer> f6190e; // = com.fossil.blesdk.obfuscated.C2232ko.m9713a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new com.fossil.blesdk.obfuscated.C2065it.C2067b());

    @DexIgnore
    /* renamed from: f */
    public static /* final */ com.fossil.blesdk.obfuscated.C2065it.C2069d f6191f; // = new com.fossil.blesdk.obfuscated.C2065it.C2069d();

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2065it.C2070e<T> f6192a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f6193b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2065it.C2069d f6194c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.it$a")
    /* renamed from: com.fossil.blesdk.obfuscated.it$a */
    public class C2066a implements com.fossil.blesdk.obfuscated.C2232ko.C2234b<java.lang.Long> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.nio.ByteBuffer f6195a; // = java.nio.ByteBuffer.allocate(8);

        @DexIgnore
        /* renamed from: a */
        public void mo12100a(byte[] bArr, java.lang.Long l, java.security.MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.f6195a) {
                this.f6195a.position(0);
                messageDigest.update(this.f6195a.putLong(l.longValue()).array());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.it$b")
    /* renamed from: com.fossil.blesdk.obfuscated.it$b */
    public class C2067b implements com.fossil.blesdk.obfuscated.C2232ko.C2234b<java.lang.Integer> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.nio.ByteBuffer f6196a; // = java.nio.ByteBuffer.allocate(4);

        @DexIgnore
        /* renamed from: a */
        public void mo12100a(byte[] bArr, java.lang.Integer num, java.security.MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.f6196a) {
                    this.f6196a.position(0);
                    messageDigest.update(this.f6196a.putInt(num.intValue()).array());
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.it$c")
    /* renamed from: com.fossil.blesdk.obfuscated.it$c */
    public static final class C2068c implements com.fossil.blesdk.obfuscated.C2065it.C2070e<android.content.res.AssetFileDescriptor> {
        @DexIgnore
        public C2068c() {
        }

        @DexIgnore
        public /* synthetic */ C2068c(com.fossil.blesdk.obfuscated.C2065it.C2066a aVar) {
            this();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12103a(android.media.MediaMetadataRetriever mediaMetadataRetriever, android.content.res.AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.it$d")
    /* renamed from: com.fossil.blesdk.obfuscated.it$d */
    public static class C2069d {
        @DexIgnore
        /* renamed from: a */
        public android.media.MediaMetadataRetriever mo12104a() {
            return new android.media.MediaMetadataRetriever();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.it$e */
    public interface C2070e<T> {
        @DexIgnore
        /* renamed from: a */
        void mo12103a(android.media.MediaMetadataRetriever mediaMetadataRetriever, T t);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.it$f")
    /* renamed from: com.fossil.blesdk.obfuscated.it$f */
    public static final class C2071f implements com.fossil.blesdk.obfuscated.C2065it.C2070e<android.os.ParcelFileDescriptor> {
        @DexIgnore
        /* renamed from: a */
        public void mo12103a(android.media.MediaMetadataRetriever mediaMetadataRetriever, android.os.ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }

    @DexIgnore
    public C2065it(com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C2065it.C2070e<T> eVar) {
        this(jqVar, eVar, f6191f);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2427mo<android.content.res.AssetFileDescriptor, android.graphics.Bitmap> m8610a(com.fossil.blesdk.obfuscated.C2149jq jqVar) {
        return new com.fossil.blesdk.obfuscated.C2065it(jqVar, new com.fossil.blesdk.obfuscated.C2065it.C2068c((com.fossil.blesdk.obfuscated.C2065it.C2066a) null));
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2427mo<android.os.ParcelFileDescriptor, android.graphics.Bitmap> m8612b(com.fossil.blesdk.obfuscated.C2149jq jqVar) {
        return new com.fossil.blesdk.obfuscated.C2065it(jqVar, new com.fossil.blesdk.obfuscated.C2065it.C2071f());
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(T t, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return true;
    }

    @DexIgnore
    public C2065it(com.fossil.blesdk.obfuscated.C2149jq jqVar, com.fossil.blesdk.obfuscated.C2065it.C2070e<T> eVar, com.fossil.blesdk.obfuscated.C2065it.C2069d dVar) {
        this.f6193b = jqVar;
        this.f6192a = eVar;
        this.f6194c = dVar;
    }

    @DexIgnore
    @android.annotation.TargetApi(27)
    /* renamed from: b */
    public static android.graphics.Bitmap m8611b(android.media.MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy) {
        try {
            int parseInt = java.lang.Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            int parseInt2 = java.lang.Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            int parseInt3 = java.lang.Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            if (parseInt3 == 90 || parseInt3 == 270) {
                int i4 = parseInt2;
                parseInt2 = parseInt;
                parseInt = i4;
            }
            float b = downsampleStrategy.mo4006b(parseInt, parseInt2, i2, i3);
            return mediaMetadataRetriever.getScaledFrameAtTime(j, i, java.lang.Math.round(((float) parseInt) * b), java.lang.Math.round(b * ((float) parseInt2)));
        } catch (Throwable th) {
            if (!android.util.Log.isLoggable("VideoDecoder", 3)) {
                return null;
            }
            android.util.Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo9299a(T t, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        long longValue = ((java.lang.Long) loVar.mo13319a(f6189d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            java.lang.Integer num = (java.lang.Integer) loVar.mo13319a(f6190e);
            if (num == null) {
                num = 2;
            }
            com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy = (com.bumptech.glide.load.resource.bitmap.DownsampleStrategy) loVar.mo13319a(com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2121f);
            if (downsampleStrategy == null) {
                downsampleStrategy = com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2120e;
            }
            com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy2 = downsampleStrategy;
            android.media.MediaMetadataRetriever a = this.f6194c.mo12104a();
            try {
                this.f6192a.mo12103a(a, t);
                android.graphics.Bitmap a2 = m8609a(a, longValue, num.intValue(), i, i2, downsampleStrategy2);
                a.release();
                return com.fossil.blesdk.obfuscated.C2675ps.m12395a(a2, this.f6193b);
            } catch (java.lang.RuntimeException e) {
                throw new java.io.IOException(e);
            } catch (Throwable th) {
                a.release();
                throw th;
            }
        } else {
            throw new java.lang.IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Bitmap m8609a(android.media.MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, com.bumptech.glide.load.resource.bitmap.DownsampleStrategy downsampleStrategy) {
        android.graphics.Bitmap b = (android.os.Build.VERSION.SDK_INT < 27 || i2 == Integer.MIN_VALUE || i3 == Integer.MIN_VALUE || downsampleStrategy == com.bumptech.glide.load.resource.bitmap.DownsampleStrategy.f2119d) ? null : m8611b(mediaMetadataRetriever, j, i, i2, i3, downsampleStrategy);
        return b == null ? m8608a(mediaMetadataRetriever, j, i) : b;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Bitmap m8608a(android.media.MediaMetadataRetriever mediaMetadataRetriever, long j, int i) {
        return mediaMetadataRetriever.getFrameAtTime(j, i);
    }
}
