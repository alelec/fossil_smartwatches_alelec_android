package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jt1 extends ha {
    @DexIgnore
    public static /* final */ Parcelable.Creator<jt1> CREATOR; // = new a();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, Bundle> g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.ClassLoaderCreator<jt1> {
        @DexIgnore
        public jt1[] newArray(int i) {
            return new jt1[i];
        }

        @DexIgnore
        public jt1 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new jt1(parcel, classLoader, (a) null);
        }

        @DexIgnore
        public jt1 createFromParcel(Parcel parcel) {
            return new jt1(parcel, (ClassLoader) null, (a) null);
        }
    }

    @DexIgnore
    public /* synthetic */ jt1(Parcel parcel, ClassLoader classLoader, a aVar) {
        this(parcel, classLoader);
    }

    @DexIgnore
    public String toString() {
        return "ExtendableSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " states=" + this.g + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        int size = this.g.size();
        parcel.writeInt(size);
        String[] strArr = new String[size];
        Bundle[] bundleArr = new Bundle[size];
        for (int i2 = 0; i2 < size; i2++) {
            strArr[i2] = this.g.c(i2);
            bundleArr[i2] = this.g.e(i2);
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }

    @DexIgnore
    public jt1(Parcelable parcelable) {
        super(parcelable);
        this.g = new SimpleArrayMap<>();
    }

    @DexIgnore
    public jt1(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.g = new SimpleArrayMap<>(readInt);
        for (int i = 0; i < readInt; i++) {
            this.g.put(strArr[i], bundleArr[i]);
        }
    }
}
