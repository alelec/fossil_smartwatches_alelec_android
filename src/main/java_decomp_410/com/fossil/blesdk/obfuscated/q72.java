package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.helper.GsonConvertDateTime;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q72 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Resting> {
    }

    @DexIgnore
    public final String a(Resting resting) {
        if (resting == null) {
            return null;
        }
        try {
            rz1 rz1 = new rz1();
            rz1.a(DateTime.class, new GsonConvertDateTime());
            return rz1.a().a((Object) resting);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("RestingConverter.toString()", String.valueOf(qa4.a));
            return null;
        }
    }

    @DexIgnore
    public final Resting a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        rz1 rz1 = new rz1();
        rz1.a(DateTime.class, new GsonConvertDateTime());
        try {
            return (Resting) rz1.a().a(str, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("RestingConverter.toResting()", String.valueOf(qa4.a));
            return null;
        }
    }
}
