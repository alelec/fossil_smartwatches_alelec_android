package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bs<Data> implements sr<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"file", "android.resource", "content"})));
    @DexIgnore
    public /* final */ c<Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements tr<Uri, AssetFileDescriptor>, c<AssetFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public sr<Uri, AssetFileDescriptor> a(wr wrVar) {
            return new bs(this);
        }

        @DexIgnore
        public so<AssetFileDescriptor> a(Uri uri) {
            return new po(this.a, uri);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements tr<Uri, ParcelFileDescriptor>, c<ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public b(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public so<ParcelFileDescriptor> a(Uri uri) {
            return new xo(this.a, uri);
        }

        @DexIgnore
        public sr<Uri, ParcelFileDescriptor> a(wr wrVar) {
            return new bs(this);
        }
    }

    @DexIgnore
    public interface c<Data> {
        @DexIgnore
        so<Data> a(Uri uri);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements tr<Uri, InputStream>, c<InputStream> {
        @DexIgnore
        public /* final */ ContentResolver a;

        @DexIgnore
        public d(ContentResolver contentResolver) {
            this.a = contentResolver;
        }

        @DexIgnore
        public so<InputStream> a(Uri uri) {
            return new cp(this.a, uri);
        }

        @DexIgnore
        public sr<Uri, InputStream> a(wr wrVar) {
            return new bs(this);
        }
    }

    @DexIgnore
    public bs(c<Data> cVar) {
        this.a = cVar;
    }

    @DexIgnore
    public sr.a<Data> a(Uri uri, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(uri), this.a.a(uri));
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
