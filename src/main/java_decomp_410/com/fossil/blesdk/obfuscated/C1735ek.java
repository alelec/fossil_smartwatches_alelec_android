package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ek */
public class C1735ek implements com.fossil.blesdk.obfuscated.C2656pj {

    @DexIgnore
    /* renamed from: j */
    public static /* final */ java.lang.String f4846j; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("SystemJobScheduler");

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.content.Context f4847e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.app.job.JobScheduler f4848f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ com.fossil.blesdk.obfuscated.C2968tj f4849g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2747ql f4850h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ com.fossil.blesdk.obfuscated.C1637dk f4851i;

    @DexIgnore
    public C1735ek(android.content.Context context, com.fossil.blesdk.obfuscated.C2968tj tjVar) {
        this(context, tjVar, (android.app.job.JobScheduler) context.getSystemService("jobscheduler"), new com.fossil.blesdk.obfuscated.C1637dk(context));
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6502b(android.content.Context context) {
        android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            java.util.List<android.app.job.JobInfo> a = m6498a(context, jobScheduler);
            if (a != null && !a.isEmpty()) {
                for (android.app.job.JobInfo next : a) {
                    if (m6497a(next) == null) {
                        m6500a(jobScheduler, next.getId());
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public void mo9223a(com.fossil.blesdk.obfuscated.C1954hl... hlVarArr) {
        int i;
        int i2;
        androidx.work.impl.WorkDatabase g = this.f4849g.mo16462g();
        int length = hlVarArr.length;
        int i3 = 0;
        while (i3 < length) {
            com.fossil.blesdk.obfuscated.C1954hl hlVar = hlVarArr[i3];
            g.beginTransaction();
            try {
                com.fossil.blesdk.obfuscated.C1954hl e = g.mo3780d().mo12029e(hlVar.f5771a);
                if (e == null) {
                    com.fossil.blesdk.obfuscated.C1635dj a = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
                    java.lang.String str = f4846j;
                    a.mo9966e(str, "Skipping scheduling " + hlVar.f5771a + " because it's no longer in the DB", new java.lang.Throwable[0]);
                    g.setTransactionSuccessful();
                } else if (e.f5772b != androidx.work.WorkInfo.State.ENQUEUED) {
                    com.fossil.blesdk.obfuscated.C1635dj a2 = com.fossil.blesdk.obfuscated.C1635dj.m5870a();
                    java.lang.String str2 = f4846j;
                    a2.mo9966e(str2, "Skipping scheduling " + hlVar.f5771a + " because it is no longer enqueued", new java.lang.Throwable[0]);
                    g.setTransactionSuccessful();
                } else {
                    com.fossil.blesdk.obfuscated.C1490bl a3 = g.mo3778b().mo9556a(hlVar.f5771a);
                    if (a3 != null) {
                        i = a3.f3771b;
                    } else {
                        i = this.f4850h.mo15268a(this.f4849g.mo16458c().mo17750f(), this.f4849g.mo16458c().mo17748d());
                    }
                    if (a3 == null) {
                        this.f4849g.mo16462g().mo3778b().mo9557a(new com.fossil.blesdk.obfuscated.C1490bl(hlVar.f5771a, i));
                    }
                    mo10533a(hlVar, i);
                    if (android.os.Build.VERSION.SDK_INT == 23) {
                        java.util.List<java.lang.Integer> a4 = m6499a(this.f4847e, this.f4848f, hlVar.f5771a);
                        if (a4 != null) {
                            int indexOf = a4.indexOf(java.lang.Integer.valueOf(i));
                            if (indexOf >= 0) {
                                a4.remove(indexOf);
                            }
                            if (!a4.isEmpty()) {
                                i2 = a4.get(0).intValue();
                            } else {
                                i2 = this.f4850h.mo15268a(this.f4849g.mo16458c().mo17750f(), this.f4849g.mo16458c().mo17748d());
                            }
                            mo10533a(hlVar, i2);
                        }
                    }
                    g.setTransactionSuccessful();
                }
                g.endTransaction();
                i3++;
            } catch (Throwable th) {
                g.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public C1735ek(android.content.Context context, com.fossil.blesdk.obfuscated.C2968tj tjVar, android.app.job.JobScheduler jobScheduler, com.fossil.blesdk.obfuscated.C1637dk dkVar) {
        this.f4847e = context;
        this.f4849g = tjVar;
        this.f4848f = jobScheduler;
        this.f4850h = new com.fossil.blesdk.obfuscated.C2747ql(context);
        this.f4851i = dkVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10533a(com.fossil.blesdk.obfuscated.C1954hl hlVar, int i) {
        android.app.job.JobInfo a = this.f4851i.mo10004a(hlVar, i);
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f4846j, java.lang.String.format("Scheduling work ID %s Job ID %s", new java.lang.Object[]{hlVar.f5771a, java.lang.Integer.valueOf(i)}), new java.lang.Throwable[0]);
        try {
            this.f4848f.schedule(a);
        } catch (java.lang.IllegalStateException e) {
            java.util.List<android.app.job.JobInfo> a2 = m6498a(this.f4847e, this.f4848f);
            java.lang.String format = java.lang.String.format(java.util.Locale.getDefault(), "JobScheduler 100 job limit exceeded.  We count %d WorkManager jobs in JobScheduler; we have %d tracked jobs in our DB; our Configuration limit is %d.", new java.lang.Object[]{java.lang.Integer.valueOf(a2 != null ? a2.size() : 0), java.lang.Integer.valueOf(this.f4849g.mo16462g().mo3780d().mo12017a().size()), java.lang.Integer.valueOf(this.f4849g.mo16458c().mo17749e())});
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f4846j, format, new java.lang.Throwable[0]);
            throw new java.lang.IllegalStateException(format, e);
        } catch (Throwable th) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f4846j, java.lang.String.format("Unable to schedule %s", new java.lang.Object[]{hlVar}), th);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9222a(java.lang.String str) {
        java.util.List<java.lang.Integer> a = m6499a(this.f4847e, this.f4848f, str);
        if (a != null && !a.isEmpty()) {
            for (java.lang.Integer intValue : a) {
                m6500a(this.f4848f, intValue.intValue());
            }
            this.f4849g.mo16462g().mo3778b().mo9558b(str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6500a(android.app.job.JobScheduler jobScheduler, int i) {
        try {
            jobScheduler.cancel(i);
        } catch (Throwable th) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f4846j, java.lang.String.format(java.util.Locale.getDefault(), "Exception while trying to cancel job (%d)", new java.lang.Object[]{java.lang.Integer.valueOf(i)}), th);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6501a(android.content.Context context) {
        android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService("jobscheduler");
        if (jobScheduler != null) {
            java.util.List<android.app.job.JobInfo> a = m6498a(context, jobScheduler);
            if (a != null && !a.isEmpty()) {
                for (android.app.job.JobInfo id : a) {
                    m6500a(jobScheduler, id.getId());
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<android.app.job.JobInfo> m6498a(android.content.Context context, android.app.job.JobScheduler jobScheduler) {
        java.util.List<android.app.job.JobInfo> list;
        try {
            list = jobScheduler.getAllPendingJobs();
        } catch (Throwable th) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f4846j, "getAllPendingJobs() is not reliable on this device.", th);
            list = null;
        }
        if (list == null) {
            return null;
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(list.size());
        android.content.ComponentName componentName = new android.content.ComponentName(context, androidx.work.impl.background.systemjob.SystemJobService.class);
        for (android.app.job.JobInfo next : list) {
            if (componentName.equals(next.getService())) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<java.lang.Integer> m6499a(android.content.Context context, android.app.job.JobScheduler jobScheduler, java.lang.String str) {
        java.util.List<android.app.job.JobInfo> a = m6498a(context, jobScheduler);
        if (a == null) {
            return null;
        }
        java.util.ArrayList arrayList = new java.util.ArrayList(2);
        for (android.app.job.JobInfo next : a) {
            if (str.equals(m6497a(next))) {
                arrayList.add(java.lang.Integer.valueOf(next.getId()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m6497a(android.app.job.JobInfo jobInfo) {
        android.os.PersistableBundle extras = jobInfo.getExtras();
        if (extras == null) {
            return null;
        }
        try {
            if (extras.containsKey("EXTRA_WORK_SPEC_ID")) {
                return extras.getString("EXTRA_WORK_SPEC_ID");
            }
            return null;
        } catch (java.lang.NullPointerException unused) {
            return null;
        }
    }
}
