package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ag2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ View s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ ScrollView u;

    @DexIgnore
    public ag2(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view2, RTLImageView rTLImageView, ScrollView scrollView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = view2;
        this.t = rTLImageView;
        this.u = scrollView;
    }
}
