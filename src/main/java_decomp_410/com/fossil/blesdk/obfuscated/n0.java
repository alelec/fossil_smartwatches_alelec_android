package com.fossil.blesdk.obfuscated;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.StateSet;
import androidx.collection.SparseArrayCompat;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.o0;
import com.fossil.blesdk.obfuscated.q0;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n0 extends q0 {
    @DexIgnore
    public c s;
    @DexIgnore
    public g t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public boolean w;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends g {
        @DexIgnore
        public /* final */ Animatable a;

        @DexIgnore
        public b(Animatable animatable) {
            super();
            this.a = animatable;
        }

        @DexIgnore
        public void c() {
            this.a.start();
        }

        @DexIgnore
        public void d() {
            this.a.stop();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends q0.a {
        @DexIgnore
        public j4<Long> K;
        @DexIgnore
        public SparseArrayCompat<Integer> L;

        @DexIgnore
        public c(c cVar, n0 n0Var, Resources resources) {
            super(cVar, n0Var, resources);
            if (cVar != null) {
                this.K = cVar.K;
                this.L = cVar.L;
                return;
            }
            this.K = new j4<>();
            this.L = new SparseArrayCompat<>();
        }

        @DexIgnore
        public static long f(int i, int i2) {
            return ((long) i2) | (((long) i) << 32);
        }

        @DexIgnore
        public int a(int i, int i2, Drawable drawable, boolean z) {
            int a = super.a(drawable);
            long f = f(i, i2);
            long j = z ? 8589934592L : 0;
            long j2 = (long) a;
            this.K.a(f, Long.valueOf(j2 | j));
            if (z) {
                this.K.a(f(i2, i), Long.valueOf(4294967296L | j2 | j));
            }
            return a;
        }

        @DexIgnore
        public int b(int[] iArr) {
            int a = super.a(iArr);
            if (a >= 0) {
                return a;
            }
            return super.a(StateSet.WILD_CARD);
        }

        @DexIgnore
        public int c(int i, int i2) {
            return (int) this.K.b(f(i, i2), -1L).longValue();
        }

        @DexIgnore
        public int d(int i) {
            if (i < 0) {
                return 0;
            }
            return this.L.b(i, 0).intValue();
        }

        @DexIgnore
        public boolean e(int i, int i2) {
            return (this.K.b(f(i, i2), -1L).longValue() & 8589934592L) != 0;
        }

        @DexIgnore
        public void n() {
            this.K = this.K.clone();
            this.L = this.L.clone();
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new n0(this, (Resources) null);
        }

        @DexIgnore
        public boolean d(int i, int i2) {
            return (this.K.b(f(i, i2), -1L).longValue() & 4294967296L) != 0;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new n0(this, resources);
        }

        @DexIgnore
        public int a(int[] iArr, Drawable drawable, int i) {
            int a = super.a(iArr, drawable);
            this.L.c(a, Integer.valueOf(i));
            return a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends g {
        @DexIgnore
        public /* final */ mi a;

        @DexIgnore
        public d(mi miVar) {
            super();
            this.a = miVar;
        }

        @DexIgnore
        public void c() {
            this.a.start();
        }

        @DexIgnore
        public void d() {
            this.a.stop();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends g {
        @DexIgnore
        public /* final */ ObjectAnimator a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(AnimationDrawable animationDrawable, boolean z, boolean z2) {
            super();
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            int i = z ? numberOfFrames - 1 : 0;
            int i2 = z ? 0 : numberOfFrames - 1;
            f fVar = new f(animationDrawable, z);
            ObjectAnimator ofInt = ObjectAnimator.ofInt(animationDrawable, "currentIndex", new int[]{i, i2});
            if (Build.VERSION.SDK_INT >= 18) {
                ofInt.setAutoCancel(true);
            }
            ofInt.setDuration((long) fVar.a());
            ofInt.setInterpolator(fVar);
            this.b = z2;
            this.a = ofInt;
        }

        @DexIgnore
        public boolean a() {
            return this.b;
        }

        @DexIgnore
        public void b() {
            this.a.reverse();
        }

        @DexIgnore
        public void c() {
            this.a.start();
        }

        @DexIgnore
        public void d() {
            this.a.cancel();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public abstract void c();

        @DexIgnore
        public abstract void d();
    }

    /*
    static {
        Class<n0> cls = n0.class;
    }
    */

    @DexIgnore
    public n0() {
        this((c) null, (Resources) null);
    }

    @DexIgnore
    public static n0 e(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("animated-selector")) {
            n0 n0Var = new n0();
            n0Var.a(context, resources, xmlPullParser, attributeSet, theme);
            return n0Var;
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid animated-selector tag " + name);
    }

    @DexIgnore
    public final boolean b(int i) {
        int i2;
        g gVar;
        g gVar2 = this.t;
        if (gVar2 == null) {
            i2 = b();
        } else if (i == this.u) {
            return true;
        } else {
            if (i != this.v || !gVar2.a()) {
                i2 = this.u;
                gVar2.d();
            } else {
                gVar2.b();
                this.u = this.v;
                this.v = i;
                return true;
            }
        }
        this.t = null;
        this.v = -1;
        this.u = -1;
        c cVar = this.s;
        int d2 = cVar.d(i2);
        int d3 = cVar.d(i);
        if (!(d3 == 0 || d2 == 0)) {
            int c2 = cVar.c(d2, d3);
            if (c2 < 0) {
                return false;
            }
            boolean e2 = cVar.e(d2, d3);
            a(c2);
            Drawable current = getCurrent();
            if (current instanceof AnimationDrawable) {
                gVar = new e((AnimationDrawable) current, cVar.d(d2, d3), e2);
            } else if (current instanceof mi) {
                gVar = new d((mi) current);
            } else if (current instanceof Animatable) {
                gVar = new b((Animatable) current);
            }
            gVar.c();
            this.t = gVar;
            this.v = i2;
            this.u = i;
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int c(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray a2 = s6.a(resources, theme, attributeSet, a0.AnimatedStateListDrawableItem);
        int resourceId = a2.getResourceId(a0.AnimatedStateListDrawableItem_android_id, 0);
        int resourceId2 = a2.getResourceId(a0.AnimatedStateListDrawableItem_android_drawable, -1);
        Drawable c2 = resourceId2 > 0 ? m0.c(context, resourceId2) : null;
        a2.recycle();
        int[] a3 = a(attributeSet);
        if (c2 == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next != 2) {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
            } else if (xmlPullParser.getName().equals("vector")) {
                c2 = si.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else if (Build.VERSION.SDK_INT >= 21) {
                c2 = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else {
                c2 = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            }
        }
        if (c2 != null) {
            return this.s.a(a3, c2, resourceId);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <item> tag requires a 'drawable' attribute or child tag defining a drawable");
    }

    @DexIgnore
    public final void d() {
        onStateChange(getState());
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public void jumpToCurrentState() {
        super.jumpToCurrentState();
        g gVar = this.t;
        if (gVar != null) {
            gVar.d();
            this.t = null;
            a(this.u);
            this.u = -1;
            this.v = -1;
        }
    }

    @DexIgnore
    public Drawable mutate() {
        if (!this.w) {
            super.mutate();
            if (this == this) {
                this.s.n();
                this.w = true;
            }
        }
        return this;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        int b2 = this.s.b(iArr);
        boolean z = b2 != b() && (b(b2) || a(b2));
        Drawable current = getCurrent();
        return current != null ? z | current.setState(iArr) : z;
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        boolean visible = super.setVisible(z, z2);
        if (this.t != null && (visible || z2)) {
            if (z) {
                this.t.c();
            } else {
                jumpToCurrentState();
            }
        }
        return visible;
    }

    @DexIgnore
    public n0(c cVar, Resources resources) {
        super((q0.a) null);
        this.u = -1;
        this.v = -1;
        a((o0.c) new c(cVar, this, resources));
        onStateChange(getState());
        jumpToCurrentState();
    }

    @DexIgnore
    public final int d(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        TypedArray a2 = s6.a(resources, theme, attributeSet, a0.AnimatedStateListDrawableTransition);
        int resourceId = a2.getResourceId(a0.AnimatedStateListDrawableTransition_android_fromId, -1);
        int resourceId2 = a2.getResourceId(a0.AnimatedStateListDrawableTransition_android_toId, -1);
        int resourceId3 = a2.getResourceId(a0.AnimatedStateListDrawableTransition_android_drawable, -1);
        Drawable c2 = resourceId3 > 0 ? m0.c(context, resourceId3) : null;
        boolean z = a2.getBoolean(a0.AnimatedStateListDrawableTransition_android_reversible, false);
        a2.recycle();
        if (c2 == null) {
            do {
                next = xmlPullParser.next();
            } while (next == 4);
            if (next != 2) {
                throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
            } else if (xmlPullParser.getName().equals("animated-vector")) {
                c2 = mi.a(context, resources, xmlPullParser, attributeSet, theme);
            } else if (Build.VERSION.SDK_INT >= 21) {
                c2 = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet, theme);
            } else {
                c2 = Drawable.createFromXmlInner(resources, xmlPullParser, attributeSet);
            }
        }
        if (c2 == null) {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires a 'drawable' attribute or child tag defining a drawable");
        } else if (resourceId != -1 && resourceId2 != -1) {
            return this.s.a(resourceId, resourceId2, c2, z);
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": <transition> tag requires 'fromId' & 'toId' attributes");
        }
    }

    @DexIgnore
    public void a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        TypedArray a2 = s6.a(resources, theme, attributeSet, a0.AnimatedStateListDrawableCompat);
        setVisible(a2.getBoolean(a0.AnimatedStateListDrawableCompat_android_visible, true), true);
        a(a2);
        a(resources);
        a2.recycle();
        b(context, resources, xmlPullParser, attributeSet, theme);
        d();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements TimeInterpolator {
        @DexIgnore
        public int[] a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;

        @DexIgnore
        public f(AnimationDrawable animationDrawable, boolean z) {
            a(animationDrawable, z);
        }

        @DexIgnore
        public int a(AnimationDrawable animationDrawable, boolean z) {
            int numberOfFrames = animationDrawable.getNumberOfFrames();
            this.b = numberOfFrames;
            int[] iArr = this.a;
            if (iArr == null || iArr.length < numberOfFrames) {
                this.a = new int[numberOfFrames];
            }
            int[] iArr2 = this.a;
            int i = 0;
            for (int i2 = 0; i2 < numberOfFrames; i2++) {
                int duration = animationDrawable.getDuration(z ? (numberOfFrames - i2) - 1 : i2);
                iArr2[i2] = duration;
                i += duration;
            }
            this.c = i;
            return i;
        }

        @DexIgnore
        public float getInterpolation(float f) {
            int i = (int) ((f * ((float) this.c)) + 0.5f);
            int i2 = this.b;
            int[] iArr = this.a;
            int i3 = 0;
            while (i3 < i2 && i >= iArr[i3]) {
                i -= iArr[i3];
                i3++;
            }
            return (((float) i3) / ((float) i2)) + (i3 < i2 ? ((float) i) / ((float) this.c) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        public int a() {
            return this.c;
        }
    }

    @DexIgnore
    public final void a(TypedArray typedArray) {
        c cVar = this.s;
        if (Build.VERSION.SDK_INT >= 21) {
            cVar.d |= typedArray.getChangingConfigurations();
        }
        cVar.b(typedArray.getBoolean(a0.AnimatedStateListDrawableCompat_android_variablePadding, cVar.i));
        cVar.a(typedArray.getBoolean(a0.AnimatedStateListDrawableCompat_android_constantSize, cVar.l));
        cVar.b(typedArray.getInt(a0.AnimatedStateListDrawableCompat_android_enterFadeDuration, cVar.A));
        cVar.c(typedArray.getInt(a0.AnimatedStateListDrawableCompat_android_exitFadeDuration, cVar.B));
        setDither(typedArray.getBoolean(a0.AnimatedStateListDrawableCompat_android_dither, cVar.x));
    }

    @DexIgnore
    public c a() {
        return new c(this.s, this, (Resources) null);
    }

    @DexIgnore
    public void a(o0.c cVar) {
        super.a(cVar);
        if (cVar instanceof c) {
            this.s = (c) cVar;
        }
    }

    @DexIgnore
    public final void b(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth = xmlPullParser.getDepth() + 1;
        while (true) {
            int next = xmlPullParser.next();
            if (next != 1) {
                int depth2 = xmlPullParser.getDepth();
                if (depth2 < depth && next == 3) {
                    return;
                }
                if (next == 2 && depth2 <= depth) {
                    if (xmlPullParser.getName().equals("item")) {
                        c(context, resources, xmlPullParser, attributeSet, theme);
                    } else if (xmlPullParser.getName().equals("transition")) {
                        d(context, resources, xmlPullParser, attributeSet, theme);
                    }
                }
            } else {
                return;
            }
        }
    }
}
