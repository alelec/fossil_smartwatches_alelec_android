package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bd2 extends ad2 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ RelativeLayout x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(R.id.rv_preset, 1);
        A.put(R.id.tv_tap_icon_to_customize, 2);
        A.put(R.id.cpi_preset, 3);
        A.put(R.id.ib_edit_themes, 4);
        A.put(R.id.cl_updating_fw, 5);
        A.put(R.id.tv_title, 6);
        A.put(R.id.tv_desc, 7);
        A.put(R.id.pb_ota, 8);
        A.put(R.id.cl_no_device, 9);
        A.put(R.id.ftv_customize, 10);
        A.put(R.id.iv_avatar, 11);
        A.put(R.id.ftv_pair_watch, 12);
        A.put(R.id.ftv_description, 13);
    }
    */

    @DexIgnore
    public bd2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 14, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public bd2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[9], objArr[5], objArr[3], objArr[10], objArr[13], objArr[12], objArr[4], objArr[11], objArr[8], objArr[1], objArr[7], objArr[2], objArr[6]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
