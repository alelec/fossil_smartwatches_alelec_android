package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lv1 {
    @DexIgnore
    public StringBuilder a; // = new StringBuilder();
    @DexIgnore
    public boolean b;

    @DexIgnore
    public abstract void a(String str, String str2) throws IOException;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001e  */
    public void a(char[] cArr, int i, int i2) throws IOException {
        int i3;
        int i4;
        if (this.b && i2 > 0) {
            boolean z = cArr[i] == 10;
            a(z);
            if (z) {
                i3 = i + 1;
                i4 = i + i2;
                int i5 = i3;
                while (i3 < i4) {
                    char c = cArr[i3];
                    if (c == 10) {
                        this.a.append(cArr, i5, i3 - i5);
                        a(true);
                    } else if (c != 13) {
                        i3++;
                    } else {
                        this.a.append(cArr, i5, i3 - i5);
                        this.b = true;
                        int i6 = i3 + 1;
                        if (i6 < i4) {
                            boolean z2 = cArr[i6] == 10;
                            a(z2);
                            if (z2) {
                                i3 = i6;
                            }
                        }
                    }
                    i5 = i3 + 1;
                    i3++;
                }
                this.a.append(cArr, i5, i4 - i5);
            }
        }
        i3 = i;
        i4 = i + i2;
        int i52 = i3;
        while (i3 < i4) {
        }
        this.a.append(cArr, i52, i4 - i52);
    }

    @DexIgnore
    public final boolean a(boolean z) throws IOException {
        a(this.a.toString(), this.b ? z ? "\r\n" : "\r" : z ? "\n" : "");
        this.a = new StringBuilder();
        this.b = false;
        return z;
    }

    @DexIgnore
    public void a() throws IOException {
        if (this.b || this.a.length() > 0) {
            a(false);
        }
    }
}
