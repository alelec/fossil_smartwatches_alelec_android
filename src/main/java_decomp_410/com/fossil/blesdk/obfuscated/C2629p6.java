package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p6 */
public final class C2629p6 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p6$a")
    /* renamed from: com.fossil.blesdk.obfuscated.p6$a */
    public static final class C2630a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ int[] f8296a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ float[] f8297b;

        @DexIgnore
        public C2630a(java.util.List<java.lang.Integer> list, java.util.List<java.lang.Float> list2) {
            int size = list.size();
            this.f8296a = new int[size];
            this.f8297b = new float[size];
            for (int i = 0; i < size; i++) {
                this.f8296a[i] = list.get(i).intValue();
                this.f8297b[i] = list2.get(i).floatValue();
            }
        }

        @DexIgnore
        public C2630a(int i, int i2) {
            this.f8296a = new int[]{i, i2};
            this.f8297b = new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f};
        }

        @DexIgnore
        public C2630a(int i, int i2, int i3) {
            this.f8296a = new int[]{i, i2, i3};
            this.f8297b = new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Shader m12108a(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws java.io.IOException, org.xmlpull.v1.XmlPullParserException {
        org.xmlpull.v1.XmlPullParser xmlPullParser2 = xmlPullParser;
        java.lang.String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            android.content.res.Resources.Theme theme2 = theme;
            android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme2, attributeSet, com.fossil.blesdk.obfuscated.C3008u5.GradientColor);
            float a2 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "startX", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_startX, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a3 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "startY", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_startY, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a4 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "endX", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_endX, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a5 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "endY", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_endY, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a6 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "centerX", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_centerX, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a7 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "centerY", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_centerY, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            int b = com.fossil.blesdk.obfuscated.C2863s6.m13546b(a, xmlPullParser2, "type", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_type, 0);
            int a8 = com.fossil.blesdk.obfuscated.C2863s6.m13540a(a, xmlPullParser2, "startColor", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_startColor, 0);
            boolean a9 = com.fossil.blesdk.obfuscated.C2863s6.m13545a(xmlPullParser2, "centerColor");
            int a10 = com.fossil.blesdk.obfuscated.C2863s6.m13540a(a, xmlPullParser2, "centerColor", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_centerColor, 0);
            int a11 = com.fossil.blesdk.obfuscated.C2863s6.m13540a(a, xmlPullParser2, "endColor", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_endColor, 0);
            int b2 = com.fossil.blesdk.obfuscated.C2863s6.m13546b(a, xmlPullParser2, "tileMode", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_tileMode, 0);
            float f = a6;
            float a12 = com.fossil.blesdk.obfuscated.C2863s6.m13539a(a, xmlPullParser2, "gradientRadius", com.fossil.blesdk.obfuscated.C3008u5.GradientColor_android_gradientRadius, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a.recycle();
            com.fossil.blesdk.obfuscated.C2629p6.C2630a a13 = m12109a(m12110b(resources, xmlPullParser, attributeSet, theme), a8, a11, a9, a10);
            if (b == 1) {
                float f2 = f;
                if (a12 > com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    int[] iArr = a13.f8296a;
                    android.graphics.RadialGradient radialGradient = new android.graphics.RadialGradient(f2, a7, a12, iArr, a13.f8297b, m12107a(b2));
                    return radialGradient;
                }
                throw new org.xmlpull.v1.XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
            } else if (b != 2) {
                android.graphics.LinearGradient linearGradient = new android.graphics.LinearGradient(a2, a3, a4, a5, a13.f8296a, a13.f8297b, m12107a(b2));
                return linearGradient;
            } else {
                return new android.graphics.SweepGradient(f, a7, a13.f8296a, a13.f8297b);
            }
        } else {
            throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0089, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r9.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' " + "attribute!");
     */
    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2629p6.C2630a m12110b(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int depth = xmlPullParser.getDepth() + 1;
        java.util.ArrayList arrayList = new java.util.ArrayList(20);
        java.util.ArrayList arrayList2 = new java.util.ArrayList(20);
        while (true) {
            int next = xmlPullParser.next();
            if (next == 1) {
                break;
            }
            int depth2 = xmlPullParser.getDepth();
            if (depth2 < depth && next == 3) {
                break;
            } else if (next == 2 && depth2 <= depth && xmlPullParser.getName().equals("item")) {
                android.content.res.TypedArray a = com.fossil.blesdk.obfuscated.C2863s6.m13541a(resources, theme, attributeSet, com.fossil.blesdk.obfuscated.C3008u5.GradientColorItem);
                boolean hasValue = a.hasValue(com.fossil.blesdk.obfuscated.C3008u5.GradientColorItem_android_color);
                boolean hasValue2 = a.hasValue(com.fossil.blesdk.obfuscated.C3008u5.GradientColorItem_android_offset);
                if (!hasValue || !hasValue2) {
                } else {
                    int color = a.getColor(com.fossil.blesdk.obfuscated.C3008u5.GradientColorItem_android_color, 0);
                    float f = a.getFloat(com.fossil.blesdk.obfuscated.C3008u5.GradientColorItem_android_offset, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    a.recycle();
                    arrayList2.add(java.lang.Integer.valueOf(color));
                    arrayList.add(java.lang.Float.valueOf(f));
                }
            }
        }
        if (arrayList2.size() > 0) {
            return new com.fossil.blesdk.obfuscated.C2629p6.C2630a((java.util.List<java.lang.Integer>) arrayList2, (java.util.List<java.lang.Float>) arrayList);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2629p6.C2630a m12109a(com.fossil.blesdk.obfuscated.C2629p6.C2630a aVar, int i, int i2, boolean z, int i3) {
        if (aVar != null) {
            return aVar;
        }
        if (z) {
            return new com.fossil.blesdk.obfuscated.C2629p6.C2630a(i, i3, i2);
        }
        return new com.fossil.blesdk.obfuscated.C2629p6.C2630a(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Shader.TileMode m12107a(int i) {
        if (i == 1) {
            return android.graphics.Shader.TileMode.REPEAT;
        }
        if (i != 2) {
            return android.graphics.Shader.TileMode.CLAMP;
        }
        return android.graphics.Shader.TileMode.MIRROR;
    }
}
