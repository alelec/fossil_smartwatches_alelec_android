package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.a;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface b extends IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements b {
        @DexIgnore
        public a() {
            attachInterface(this, "android.support.customtabs.ICustomTabsService");
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: android.net.Uri} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v19, resolved type: android.os.Bundle} */
        /* JADX WARNING: type inference failed for: r0v1 */
        /* JADX WARNING: type inference failed for: r0v23 */
        /* JADX WARNING: type inference failed for: r0v24 */
        /* JADX WARNING: type inference failed for: r0v25 */
        /* JADX WARNING: type inference failed for: r0v26 */
        /* JADX WARNING: type inference failed for: r0v27 */
        /* JADX WARNING: type inference failed for: r0v28 */
        /* JADX WARNING: Multi-variable type inference failed */
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 1598968902) {
                Object r0 = 0;
                switch (i) {
                    case 2:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        boolean a = a(parcel.readLong());
                        parcel2.writeNoException();
                        parcel2.writeInt(a);
                        return true;
                    case 3:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        boolean a2 = a(a.C0007a.a(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        parcel2.writeInt(a2);
                        return true;
                    case 4:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        a a3 = a.C0007a.a(parcel.readStrongBinder());
                        Uri uri = parcel.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r0 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean a4 = a(a3, uri, r0, (List<Bundle>) parcel.createTypedArrayList(Bundle.CREATOR));
                        parcel2.writeNoException();
                        parcel2.writeInt(a4);
                        return true;
                    case 5:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        String readString = parcel.readString();
                        if (parcel.readInt() != 0) {
                            r0 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        Bundle b = b(readString, r0);
                        parcel2.writeNoException();
                        if (b != null) {
                            parcel2.writeInt(1);
                            b.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 6:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        a a5 = a.C0007a.a(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r0 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean b2 = b(a5, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(b2);
                        return true;
                    case 7:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        a a6 = a.C0007a.a(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            r0 = (Uri) Uri.CREATOR.createFromParcel(parcel);
                        }
                        boolean a7 = a(a6, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(a7);
                        return true;
                    case 8:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        a a8 = a.C0007a.a(parcel.readStrongBinder());
                        String readString2 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            r0 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        int b3 = b(a8, readString2, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(b3);
                        return true;
                    case 9:
                        parcel.enforceInterface("android.support.customtabs.ICustomTabsService");
                        a a9 = a.C0007a.a(parcel.readStrongBinder());
                        int readInt = parcel.readInt();
                        Uri uri2 = parcel.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            r0 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean a10 = a(a9, readInt, uri2, r0);
                        parcel2.writeNoException();
                        parcel2.writeInt(a10);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString("android.support.customtabs.ICustomTabsService");
                return true;
            }
        }
    }

    @DexIgnore
    boolean a(long j) throws RemoteException;

    @DexIgnore
    boolean a(a aVar) throws RemoteException;

    @DexIgnore
    boolean a(a aVar, int i, Uri uri, Bundle bundle) throws RemoteException;

    @DexIgnore
    boolean a(a aVar, Uri uri) throws RemoteException;

    @DexIgnore
    boolean a(a aVar, Uri uri, Bundle bundle, List<Bundle> list) throws RemoteException;

    @DexIgnore
    int b(a aVar, String str, Bundle bundle) throws RemoteException;

    @DexIgnore
    Bundle b(String str, Bundle bundle) throws RemoteException;

    @DexIgnore
    boolean b(a aVar, Bundle bundle) throws RemoteException;
}
