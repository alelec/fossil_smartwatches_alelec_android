package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xp4 {
    @DexIgnore
    public static <T> Collection<T> a(Collection<T> collection, Collection<T> collection2) {
        ArrayList arrayList = new ArrayList(collection);
        for (T remove : collection2) {
            arrayList.remove(remove);
        }
        return arrayList;
    }
}
