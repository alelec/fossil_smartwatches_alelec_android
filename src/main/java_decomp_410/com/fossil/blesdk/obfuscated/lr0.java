package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lr0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ Intent f;
    @DexIgnore
    public /* final */ /* synthetic */ kr0 g;

    @DexIgnore
    public lr0(kr0 kr0, Intent intent, Intent intent2) {
        this.g = kr0;
        this.e = intent;
        this.f = intent2;
    }

    @DexIgnore
    public final void run() {
        this.g.handleIntent(this.e);
        this.g.a(this.f);
    }
}
