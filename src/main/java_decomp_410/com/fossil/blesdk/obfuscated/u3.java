package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u3 {
    @DexIgnore
    public static /* final */ int cardBackgroundColor; // = 2130968898;
    @DexIgnore
    public static /* final */ int cardCornerRadius; // = 2130968899;
    @DexIgnore
    public static /* final */ int cardElevation; // = 2130968900;
    @DexIgnore
    public static /* final */ int cardMaxElevation; // = 2130968901;
    @DexIgnore
    public static /* final */ int cardPreventCornerOverlap; // = 2130968902;
    @DexIgnore
    public static /* final */ int cardUseCompatPadding; // = 2130968903;
    @DexIgnore
    public static /* final */ int cardViewStyle; // = 2130968904;
    @DexIgnore
    public static /* final */ int contentPadding; // = 2130969016;
    @DexIgnore
    public static /* final */ int contentPaddingBottom; // = 2130969017;
    @DexIgnore
    public static /* final */ int contentPaddingLeft; // = 2130969018;
    @DexIgnore
    public static /* final */ int contentPaddingRight; // = 2130969019;
    @DexIgnore
    public static /* final */ int contentPaddingTop; // = 2130969020;
}
