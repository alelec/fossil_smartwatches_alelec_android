package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.internal.VisibilityAwareImageButton;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hs1 extends gs1 {
    @DexIgnore
    public InsetDrawable I;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends GradientDrawable {
        @DexIgnore
        public boolean isStateful() {
            return true;
        }
    }

    @DexIgnore
    public hs1(VisibilityAwareImageButton visibilityAwareImageButton, bt1 bt1) {
        super(visibilityAwareImageButton, bt1);
    }

    @DexIgnore
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        Drawable drawable;
        this.j = c7.i(a());
        c7.a(this.j, colorStateList);
        if (mode != null) {
            c7.a(this.j, mode);
        }
        if (i > 0) {
            this.l = a(i, colorStateList);
            drawable = new LayerDrawable(new Drawable[]{this.l, this.j});
        } else {
            this.l = null;
            drawable = this.j;
        }
        this.k = new RippleDrawable(zs1.a(colorStateList2), drawable, (Drawable) null);
        Drawable drawable2 = this.k;
        this.m = drawable2;
        this.v.a(drawable2);
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        Drawable drawable = this.k;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(zs1.a(colorStateList));
        } else {
            super.b(colorStateList);
        }
    }

    @DexIgnore
    public float f() {
        return this.u.getElevation();
    }

    @DexIgnore
    public void m() {
    }

    @DexIgnore
    public is1 n() {
        return new js1();
    }

    @DexIgnore
    public GradientDrawable o() {
        return new a();
    }

    @DexIgnore
    public void q() {
        x();
    }

    @DexIgnore
    public boolean t() {
        return false;
    }

    @DexIgnore
    public void b(Rect rect) {
        if (this.v.a()) {
            this.I = new InsetDrawable(this.k, rect.left, rect.top, rect.right, rect.bottom);
            this.v.a(this.I);
            return;
        }
        this.v.a(this.k);
    }

    @DexIgnore
    public void a(float f, float f2, float f3) {
        if (Build.VERSION.SDK_INT == 21) {
            this.u.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(gs1.C, a(f, f3));
            stateListAnimator.addState(gs1.D, a(f, f2));
            stateListAnimator.addState(gs1.E, a(f, f2));
            stateListAnimator.addState(gs1.F, a(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.u, "elevation", new float[]{f}).setDuration(0));
            int i = Build.VERSION.SDK_INT;
            if (i >= 22 && i <= 24) {
                VisibilityAwareImageButton visibilityAwareImageButton = this.u;
                arrayList.add(ObjectAnimator.ofFloat(visibilityAwareImageButton, View.TRANSLATION_Z, new float[]{visibilityAwareImageButton.getTranslationZ()}).setDuration(100));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.u, View.TRANSLATION_Z, new float[]{0.0f}).setDuration(100));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(gs1.B);
            stateListAnimator.addState(gs1.G, animatorSet);
            stateListAnimator.addState(gs1.H, a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.u.setStateListAnimator(stateListAnimator);
        }
        if (this.v.a()) {
            x();
        }
    }

    @DexIgnore
    public final Animator a(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(this.u, "elevation", new float[]{f}).setDuration(0)).with(ObjectAnimator.ofFloat(this.u, View.TRANSLATION_Z, new float[]{f2}).setDuration(100));
        animatorSet.setInterpolator(gs1.B);
        return animatorSet;
    }

    @DexIgnore
    public void a(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (this.u.isEnabled()) {
            this.u.setElevation(this.n);
            if (this.u.isPressed()) {
                this.u.setTranslationZ(this.p);
            } else if (this.u.isFocused() || this.u.isHovered()) {
                this.u.setTranslationZ(this.o);
            } else {
                this.u.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        } else {
            this.u.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.u.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public void a(Rect rect) {
        if (this.v.a()) {
            float b = this.v.b();
            float f = f() + this.p;
            int ceil = (int) Math.ceil((double) at1.a(f, b, false));
            int ceil2 = (int) Math.ceil((double) at1.b(f, b, false));
            rect.set(ceil, ceil2, ceil, ceil2);
            return;
        }
        rect.set(0, 0, 0, 0);
    }
}
