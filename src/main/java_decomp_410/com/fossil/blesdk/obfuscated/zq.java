package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import com.fossil.blesdk.obfuscated.ar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zq extends qw<jo, aq<?>> implements ar {
    @DexIgnore
    public ar.a d;

    @DexIgnore
    public zq(long j) {
        super(j);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ aq a(jo joVar, aq aqVar) {
        return (aq) super.b(joVar, aqVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void a(jo joVar, aq<?> aqVar) {
        ar.a aVar = this.d;
        if (aVar != null && aqVar != null) {
            aVar.a(aqVar);
        }
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ aq a(jo joVar) {
        return (aq) super.c(joVar);
    }

    @DexIgnore
    public void a(ar.a aVar) {
        this.d = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public int b(aq<?> aqVar) {
        if (aqVar == null) {
            return super.b(null);
        }
        return aqVar.b();
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            a();
        } else if (i >= 20 || i == 15) {
            a(c() / 2);
        }
    }
}
