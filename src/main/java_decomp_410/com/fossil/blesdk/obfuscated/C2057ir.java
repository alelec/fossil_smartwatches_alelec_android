package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ir */
public class C2057ir implements com.fossil.blesdk.obfuscated.C2912sr<java.io.File, java.nio.ByteBuffer> {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ir$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ir$a */
    public static final class C2058a implements com.fossil.blesdk.obfuscated.C2902so<java.nio.ByteBuffer> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.io.File f6165e;

        @DexIgnore
        public C2058a(java.io.File file) {
            this.f6165e = file;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8869a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo8870a(com.bumptech.glide.Priority priority, com.fossil.blesdk.obfuscated.C2902so.C2903a<? super java.nio.ByteBuffer> aVar) {
            try {
                aVar.mo9252a(com.fossil.blesdk.obfuscated.C2255kw.m9870a(this.f6165e));
            } catch (java.io.IOException e) {
                if (android.util.Log.isLoggable("ByteBufferFileLoader", 3)) {
                    android.util.Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                aVar.mo9251a((java.lang.Exception) e);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public com.bumptech.glide.load.DataSource mo8872b() {
            return com.bumptech.glide.load.DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public java.lang.Class<java.nio.ByteBuffer> getDataClass() {
            return java.nio.ByteBuffer.class;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ir$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ir$b */
    public static class C2059b implements com.fossil.blesdk.obfuscated.C2984tr<java.io.File, java.nio.ByteBuffer> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<java.io.File, java.nio.ByteBuffer> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C2057ir();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(java.io.File file) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.nio.ByteBuffer> mo8911a(java.io.File file, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(new com.fossil.blesdk.obfuscated.C2166jw(file), new com.fossil.blesdk.obfuscated.C2057ir.C2058a(file));
    }
}
