package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.b;
import com.fossil.blesdk.obfuscated.ze0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class bf0<A extends de0.b, L> {
    @DexIgnore
    public /* final */ ze0<L> a;
    @DexIgnore
    public /* final */ wd0[] b; // = null;
    @DexIgnore
    public /* final */ boolean c; // = false;

    @DexIgnore
    public bf0(ze0<L> ze0) {
        this.a = ze0;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }

    @DexIgnore
    public abstract void a(A a2, xn1<Void> xn1) throws RemoteException;

    @DexIgnore
    public ze0.a<L> b() {
        return this.a.b();
    }

    @DexIgnore
    public wd0[] c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }
}
