package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ru0;
import com.google.android.gms.internal.clearcut.zzbb;
import com.google.android.gms.internal.clearcut.zzbn;
import com.google.android.gms.internal.clearcut.zzcb;
import com.google.android.gms.internal.clearcut.zzco;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wv0<T> implements jw0<T> {
    @DexIgnore
    public static /* final */ Unsafe r; // = hx0.d();
    @DexIgnore
    public /* final */ int[] a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ sv0 f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ int[] j;
    @DexIgnore
    public /* final */ int[] k;
    @DexIgnore
    public /* final */ int[] l;
    @DexIgnore
    public /* final */ zv0 m;
    @DexIgnore
    public /* final */ ev0 n;
    @DexIgnore
    public /* final */ ax0<?, ?> o;
    @DexIgnore
    public /* final */ gu0<?> p;
    @DexIgnore
    public /* final */ nv0 q;

    @DexIgnore
    public wv0(int[] iArr, Object[] objArr, int i2, int i3, int i4, sv0 sv0, boolean z, boolean z2, int[] iArr2, int[] iArr3, int[] iArr4, zv0 zv0, ev0 ev0, ax0<?, ?> ax0, gu0<?> gu0, nv0 nv0) {
        sv0 sv02 = sv0;
        gu0<?> gu02 = gu0;
        this.a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        boolean z3 = sv02 instanceof ru0;
        this.h = z;
        this.g = gu02 != null && gu02.a(sv0);
        this.i = false;
        this.j = iArr2;
        this.k = iArr3;
        this.l = iArr4;
        this.m = zv0;
        this.n = ev0;
        this.o = ax0;
        this.p = gu02;
        this.f = sv02;
        this.q = nv0;
    }

    @DexIgnore
    public static int a(int i2, byte[] bArr, int i3, int i4, Object obj, pt0 pt0) throws IOException {
        return ot0.a(i2, bArr, i3, i4, d(obj), pt0);
    }

    @DexIgnore
    public static <UT, UB> int a(ax0<UT, UB> ax0, T t) {
        return ax0.b(ax0.c(t));
    }

    @DexIgnore
    public static int a(jw0<?> jw0, int i2, byte[] bArr, int i3, int i4, wu0<?> wu0, pt0 pt0) throws IOException {
        int a2 = a((jw0) jw0, bArr, i3, i4, pt0);
        while (true) {
            wu0.add(pt0.c);
            if (a2 >= i4) {
                break;
            }
            int a3 = ot0.a(bArr, a2, pt0);
            if (i2 != pt0.a) {
                break;
            }
            a2 = a((jw0) jw0, bArr, a3, i4, pt0);
        }
        return a2;
    }

    @DexIgnore
    public static int a(jw0 jw0, byte[] bArr, int i2, int i3, int i4, pt0 pt0) throws IOException {
        wv0 wv0 = (wv0) jw0;
        Object a2 = wv0.a();
        int a3 = wv0.a(a2, bArr, i2, i3, i4, pt0);
        wv0.zzc(a2);
        pt0.c = a2;
        return a3;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    public static int a(jw0 jw0, byte[] bArr, int i2, int i3, pt0 pt0) throws IOException {
        int i4 = i2 + 1;
        byte b2 = bArr[i2];
        int i5 = b2;
        if (b2 < 0) {
            i4 = ot0.a((int) b2, bArr, i4, pt0);
            i5 = pt0.a;
        }
        int i6 = i4;
        if (i5 < 0 || i5 > i3 - i6) {
            throw zzco.zzbl();
        }
        Object a2 = jw0.a();
        int i7 = i5 + i6;
        jw0.a(a2, bArr, i6, i7, pt0);
        jw0.zzc(a2);
        pt0.c = a2;
        return i7;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x014a  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x016b A[EDGE_INSN: B:74:0x016b->B:70:0x016b ?: BREAK  , SYNTHETIC] */
    public static <T> wv0<T> a(Class<T> cls, qv0 qv0, zv0 zv0, ev0 ev0, ax0<?, ?> ax0, gu0<?> gu0, nv0 nv0) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int d2;
        qv0 qv02 = qv0;
        int[] iArr = null;
        if (qv02 instanceof gw0) {
            gw0 gw0 = (gw0) qv02;
            boolean z = gw0.a() == ru0.e.j;
            if (gw0.d() == 0) {
                i4 = 0;
                i3 = 0;
                i2 = 0;
            } else {
                int f2 = gw0.f();
                int g2 = gw0.g();
                i4 = gw0.k();
                i3 = f2;
                i2 = g2;
            }
            int[] iArr2 = new int[(i4 << 2)];
            Object[] objArr = new Object[(i4 << 1)];
            int[] iArr3 = gw0.h() > 0 ? new int[gw0.h()] : null;
            if (gw0.i() > 0) {
                iArr = new int[gw0.i()];
            }
            int[] iArr4 = iArr;
            hw0 e2 = gw0.e();
            if (e2.a()) {
                int c2 = e2.c();
                int i8 = 0;
                int i9 = 0;
                int i10 = 0;
                while (true) {
                    if (c2 >= gw0.l() || i8 >= ((c2 - i3) << 2)) {
                        if (e2.f()) {
                            i7 = (int) hx0.a(e2.g());
                            i6 = (int) hx0.a(e2.h());
                        } else {
                            i7 = (int) hx0.a(e2.i());
                            if (e2.j()) {
                                i6 = (int) hx0.a(e2.k());
                                i5 = e2.l();
                                iArr2[i8] = e2.c();
                                int i11 = i8 + 1;
                                iArr2[i11] = (!e2.n() ? 536870912 : 0) | (!e2.m() ? 268435456 : 0) | (e2.d() << 20) | i7;
                                iArr2[i8 + 2] = i6 | (i5 << 20);
                                if (e2.q() == null) {
                                    int i12 = (i8 / 4) << 1;
                                    objArr[i12] = e2.q();
                                    if (e2.o() != null) {
                                        objArr[i12 + 1] = e2.o();
                                    } else if (e2.p() != null) {
                                        objArr[i12 + 1] = e2.p();
                                    }
                                } else if (e2.o() != null) {
                                    objArr[((i8 / 4) << 1) + 1] = e2.o();
                                } else if (e2.p() != null) {
                                    objArr[((i8 / 4) << 1) + 1] = e2.p();
                                }
                                d2 = e2.d();
                                if (d2 != zzcb.MAP.ordinal()) {
                                    iArr3[i9] = i8;
                                    i9++;
                                } else if (d2 >= 18 && d2 <= 49) {
                                    iArr4[i10] = iArr2[i11] & 1048575;
                                    i10++;
                                }
                                if (e2.a()) {
                                    break;
                                }
                                c2 = e2.c();
                            } else {
                                i6 = 0;
                            }
                        }
                        i5 = 0;
                        iArr2[i8] = e2.c();
                        int i112 = i8 + 1;
                        iArr2[i112] = (!e2.n() ? 536870912 : 0) | (!e2.m() ? 268435456 : 0) | (e2.d() << 20) | i7;
                        iArr2[i8 + 2] = i6 | (i5 << 20);
                        if (e2.q() == null) {
                        }
                        d2 = e2.d();
                        if (d2 != zzcb.MAP.ordinal()) {
                        }
                        if (e2.a()) {
                        }
                    } else {
                        for (int i13 = 0; i13 < 4; i13++) {
                            iArr2[i8 + i13] = -1;
                        }
                    }
                    i8 += 4;
                }
            }
            return new wv0(iArr2, objArr, i3, i2, gw0.l(), gw0.c(), z, false, gw0.j(), iArr3, iArr4, zv0, ev0, ax0, gu0, nv0);
        }
        ((ww0) qv02).a();
        throw null;
    }

    @DexIgnore
    public static <E> List<E> a(Object obj, long j2) {
        return (List) hx0.f(obj, j2);
    }

    @DexIgnore
    public static void a(int i2, Object obj, ox0 ox0) throws IOException {
        if (obj instanceof String) {
            ox0.zza(i2, (String) obj);
        } else {
            ox0.a(i2, (zzbb) obj);
        }
    }

    @DexIgnore
    public static <UT, UB> void a(ax0<UT, UB> ax0, T t, ox0 ox0) throws IOException {
        ax0.a(ax0.c(t), ox0);
    }

    @DexIgnore
    public static boolean a(Object obj, int i2, jw0 jw0) {
        return jw0.c(hx0.f(obj, (long) (i2 & 1048575)));
    }

    @DexIgnore
    public static <T> double b(T t, long j2) {
        return ((Double) hx0.f(t, j2)).doubleValue();
    }

    @DexIgnore
    public static <T> float c(T t, long j2) {
        return ((Float) hx0.f(t, j2)).floatValue();
    }

    @DexIgnore
    public static <T> int d(T t, long j2) {
        return ((Integer) hx0.f(t, j2)).intValue();
    }

    @DexIgnore
    public static bx0 d(Object obj) {
        ru0 ru0 = (ru0) obj;
        bx0 bx0 = ru0.zzjp;
        if (bx0 != bx0.d()) {
            return bx0;
        }
        bx0 e2 = bx0.e();
        ru0.zzjp = e2;
        return e2;
    }

    @DexIgnore
    public static <T> long e(T t, long j2) {
        return ((Long) hx0.f(t, j2)).longValue();
    }

    @DexIgnore
    public static <T> boolean f(T t, long j2) {
        return ((Boolean) hx0.f(t, j2)).booleanValue();
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0061, code lost:
        r3 = com.fossil.blesdk.obfuscated.hx0.f(r9, r5);
        r2 = r2 * 53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0093, code lost:
        r2 = r2 * 53;
        r3 = d(r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a8, code lost:
        r2 = r2 * 53;
        r3 = e(r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ce, code lost:
        if (r3 != null) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00d1, code lost:
        r2 = r2 * 53;
        r3 = com.fossil.blesdk.obfuscated.hx0.f(r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00d7, code lost:
        r3 = r3.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e0, code lost:
        if (r3 != null) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e2, code lost:
        r7 = r3.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e6, code lost:
        r2 = (r2 * 53) + r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ea, code lost:
        r2 = r2 * 53;
        r3 = ((java.lang.String) com.fossil.blesdk.obfuscated.hx0.f(r9, r5)).hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00fd, code lost:
        r3 = com.fossil.blesdk.obfuscated.tu0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0116, code lost:
        r3 = java.lang.Float.floatToIntBits(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0121, code lost:
        r3 = java.lang.Double.doubleToLongBits(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0125, code lost:
        r3 = com.fossil.blesdk.obfuscated.tu0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0129, code lost:
        r2 = r2 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x012a, code lost:
        r1 = r1 + 4;
     */
    @DexIgnore
    public final int a(T t) {
        double d2;
        int i2;
        float f2;
        int i3;
        boolean z;
        int i4;
        Object obj;
        int length = this.a.length;
        int i5 = 0;
        int i6 = 0;
        while (i5 < length) {
            int d3 = d(i5);
            int i7 = this.a[i5];
            long j2 = (long) (1048575 & d3);
            int i8 = 37;
            switch ((d3 & 267386880) >>> 20) {
                case 0:
                    i2 = i6 * 53;
                    d2 = hx0.e(t, j2);
                    break;
                case 1:
                    i3 = i6 * 53;
                    f2 = hx0.d(t, j2);
                    break;
                case 2:
                case 3:
                case 5:
                case 14:
                case 16:
                    i2 = i6 * 53;
                    long j3 = hx0.b(t, j2);
                    break;
                case 4:
                case 6:
                case 11:
                case 12:
                case 13:
                case 15:
                    int i9 = i6 * 53;
                    int i10 = hx0.a((Object) t, j2);
                    break;
                case 7:
                    i4 = i6 * 53;
                    z = hx0.c(t, j2);
                    break;
                case 8:
                    break;
                case 9:
                    obj = hx0.f(t, j2);
                    break;
                case 10:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                    break;
                case 17:
                    obj = hx0.f(t, j2);
                    break;
                case 51:
                    if (!a(t, i7, i5)) {
                        break;
                    } else {
                        i2 = i6 * 53;
                        d2 = b(t, j2);
                        break;
                    }
                case 52:
                    if (!a(t, i7, i5)) {
                        break;
                    } else {
                        i3 = i6 * 53;
                        f2 = c(t, j2);
                        break;
                    }
                case 53:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 54:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 55:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 56:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 57:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 58:
                    if (!a(t, i7, i5)) {
                        break;
                    } else {
                        i4 = i6 * 53;
                        z = f(t, j2);
                        break;
                    }
                case 59:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 60:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 61:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 62:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 63:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 64:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 65:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 66:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 67:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
                case 68:
                    if (!a(t, i7, i5)) {
                        break;
                    }
                    break;
            }
        }
        int hashCode = (i6 * 53) + this.o.c(t).hashCode();
        return this.g ? (hashCode * 53) + this.p.a((Object) t).hashCode() : hashCode;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00b7, code lost:
        r2 = r2 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x013f, code lost:
        r3 = java.lang.Integer.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x014c, code lost:
        r3 = java.lang.Long.valueOf(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0150, code lost:
        r12.putObject(r1, r9, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x015e, code lost:
        r12.putObject(r1, r9, r2);
        r2 = r4 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x016f, code lost:
        r12.putObject(r1, r9, r2);
        r2 = r4 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0174, code lost:
        r12.putInt(r1, r13, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        return r2;
     */
    @DexIgnore
    public final int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, pt0 pt0) throws IOException {
        int i10;
        Object obj;
        Object obj2;
        Object obj3;
        long j3;
        int b2;
        int i11;
        int i12;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i13 = i2;
        int i14 = i4;
        int i15 = i5;
        int i16 = i6;
        long j4 = j2;
        int i17 = i9;
        pt0 pt02 = pt0;
        Unsafe unsafe = r;
        long j5 = (long) (this.a[i17 + 2] & 1048575);
        switch (i8) {
            case 51:
                if (i16 == 1) {
                    obj = Double.valueOf(ot0.c(bArr, i2));
                    break;
                }
                break;
            case 52:
                if (i16 == 5) {
                    obj2 = Float.valueOf(ot0.d(bArr, i2));
                    break;
                }
                break;
            case 53:
            case 54:
                if (i16 == 0) {
                    b2 = ot0.b(bArr2, i13, pt02);
                    j3 = pt02.b;
                    break;
                }
                break;
            case 55:
            case 62:
                if (i16 == 0) {
                    i10 = ot0.a(bArr2, i13, pt02);
                    i11 = pt02.a;
                    break;
                }
                break;
            case 56:
            case 65:
                if (i16 == 1) {
                    obj = Long.valueOf(ot0.b(bArr, i2));
                    break;
                }
                break;
            case 57:
            case 64:
                if (i16 == 5) {
                    obj2 = Integer.valueOf(ot0.a(bArr, i2));
                    break;
                }
                break;
            case 58:
                if (i16 == 0) {
                    i10 = ot0.b(bArr2, i13, pt02);
                    obj3 = Boolean.valueOf(pt02.b != 0);
                    break;
                }
                break;
            case 59:
                if (i16 == 2) {
                    i10 = ot0.a(bArr2, i13, pt02);
                    i12 = pt02.a;
                    if (i12 == 0) {
                        obj3 = "";
                        break;
                    } else if ((i7 & 536870912) == 0 || jx0.a(bArr2, i10, i10 + i12)) {
                        unsafe.putObject(t2, j4, new String(bArr2, i10, i12, tu0.a));
                        break;
                    } else {
                        throw zzco.zzbp();
                    }
                }
                break;
            case 60:
                if (i16 == 2) {
                    i10 = a(a(i17), bArr2, i13, i3, pt02);
                    Object object = unsafe.getInt(t2, j5) == i15 ? unsafe.getObject(t2, j4) : null;
                    obj3 = pt02.c;
                    if (object != null) {
                        obj3 = tu0.a(object, obj3);
                        break;
                    }
                }
                break;
            case 61:
                if (i16 == 2) {
                    i10 = ot0.a(bArr2, i13, pt02);
                    i12 = pt02.a;
                    if (i12 != 0) {
                        unsafe.putObject(t2, j4, zzbb.zzb(bArr2, i10, i12));
                        break;
                    } else {
                        obj3 = zzbb.zzfi;
                        break;
                    }
                }
                break;
            case 63:
                if (i16 == 0) {
                    int a2 = ot0.a(bArr2, i13, pt02);
                    int i18 = pt02.a;
                    vu0<?> c2 = c(i17);
                    if (c2 == null || c2.zzb(i18) != null) {
                        unsafe.putObject(t2, j4, Integer.valueOf(i18));
                        i10 = a2;
                        break;
                    } else {
                        d((Object) t).a(i14, (Object) Long.valueOf((long) i18));
                        return a2;
                    }
                }
                break;
            case 66:
                if (i16 == 0) {
                    i10 = ot0.a(bArr2, i13, pt02);
                    i11 = yt0.a(pt02.a);
                    break;
                }
                break;
            case 67:
                if (i16 == 0) {
                    b2 = ot0.b(bArr2, i13, pt02);
                    j3 = yt0.a(pt02.b);
                    break;
                }
                break;
            case 68:
                if (i16 == 3) {
                    i10 = a(a(i17), bArr, i2, i3, (i14 & -8) | 4, pt0);
                    Object object2 = unsafe.getInt(t2, j5) == i15 ? unsafe.getObject(t2, j4) : null;
                    obj3 = pt02.c;
                    if (object2 != null) {
                        obj3 = tu0.a(object2, obj3);
                        break;
                    }
                }
                break;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0232, code lost:
        if (r7.b != 0) goto L_0x0234;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x0234, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0236, code lost:
        r6 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0237, code lost:
        r11.a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x023a, code lost:
        if (r4 >= r5) goto L_0x039b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x023c, code lost:
        r6 = com.fossil.blesdk.obfuscated.ot0.a(r3, r4, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0242, code lost:
        if (r2 != r7.a) goto L_0x039b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0244, code lost:
        r4 = com.fossil.blesdk.obfuscated.ot0.b(r3, r6, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x024c, code lost:
        if (r7.b == 0) goto L_0x0236;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:?, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x013a, code lost:
        if (r4 == 0) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x013c, code lost:
        r11.add(com.google.android.gms.internal.clearcut.zzbb.zzfi);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0142, code lost:
        r11.add(com.google.android.gms.internal.clearcut.zzbb.zzb(r3, r1, r4));
        r1 = r1 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x014a, code lost:
        if (r1 >= r5) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x014c, code lost:
        r4 = com.fossil.blesdk.obfuscated.ot0.a(r3, r1, r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0152, code lost:
        if (r2 != r7.a) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        r1 = com.fossil.blesdk.obfuscated.ot0.a(r3, r4, r7);
        r4 = r7.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x015a, code lost:
        if (r4 != 0) goto L_0x0142;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:75:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01d3  */
    public final int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, long j2, int i8, long j3, pt0 pt0) throws IOException {
        int b2;
        int i9;
        int a2;
        int b3;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i2;
        int i11 = i3;
        int i12 = i4;
        int i13 = i6;
        int i14 = i7;
        long j4 = j3;
        pt0 pt02 = pt0;
        wu0 wu0 = (wu0) r.getObject(t2, j4);
        if (!wu0.y()) {
            int size = wu0.size();
            wu0 = wu0.c(size == 0 ? 10 : size << 1);
            r.putObject(t2, j4, wu0);
        }
        switch (i8) {
            case 18:
            case 35:
                if (i13 == 2) {
                    du0 du0 = (du0) wu0;
                    int a3 = ot0.a(bArr2, i10, pt02);
                    int i15 = pt02.a + a3;
                    while (a3 < i15) {
                        du0.a(ot0.c(bArr2, a3));
                        a3 += 8;
                    }
                    if (a3 == i15) {
                        return a3;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 1) {
                    du0 du02 = (du0) wu0;
                    du02.a(ot0.c(bArr, i2));
                    while (true) {
                        int i16 = i10 + 8;
                        if (i16 >= i11) {
                            return i16;
                        }
                        i10 = ot0.a(bArr2, i16, pt02);
                        if (i12 != pt02.a) {
                            return i16;
                        }
                        du02.a(ot0.c(bArr2, i10));
                    }
                }
                break;
            case 19:
            case 36:
                if (i13 == 2) {
                    pu0 pu0 = (pu0) wu0;
                    int a4 = ot0.a(bArr2, i10, pt02);
                    int i17 = pt02.a + a4;
                    while (a4 < i17) {
                        pu0.a(ot0.d(bArr2, a4));
                        a4 += 4;
                    }
                    if (a4 == i17) {
                        return a4;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 5) {
                    pu0 pu02 = (pu0) wu0;
                    pu02.a(ot0.d(bArr, i2));
                    while (true) {
                        int i18 = i10 + 4;
                        if (i18 >= i11) {
                            return i18;
                        }
                        i10 = ot0.a(bArr2, i18, pt02);
                        if (i12 != pt02.a) {
                            return i18;
                        }
                        pu02.a(ot0.d(bArr2, i10));
                    }
                }
                break;
            case 20:
            case 21:
            case 37:
            case 38:
                if (i13 == 2) {
                    iv0 iv0 = (iv0) wu0;
                    int a5 = ot0.a(bArr2, i10, pt02);
                    int i19 = pt02.a + a5;
                    while (a5 < i19) {
                        a5 = ot0.b(bArr2, a5, pt02);
                        iv0.a(pt02.b);
                    }
                    if (a5 == i19) {
                        return a5;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 0) {
                    iv0 iv02 = (iv0) wu0;
                    do {
                        b2 = ot0.b(bArr2, i10, pt02);
                        iv02.a(pt02.b);
                        if (b2 >= i11) {
                            return b2;
                        }
                        i10 = ot0.a(bArr2, b2, pt02);
                    } while (i12 == pt02.a);
                    return b2;
                }
                break;
            case 22:
            case 29:
            case 39:
            case 43:
                if (i13 == 2) {
                    return ot0.a(bArr2, i10, (wu0<?>) wu0, pt02);
                }
                if (i13 == 0) {
                    return ot0.a(i4, bArr, i2, i3, (wu0<?>) wu0, pt0);
                }
                break;
            case 23:
            case 32:
            case 40:
            case 46:
                if (i13 == 2) {
                    iv0 iv03 = (iv0) wu0;
                    int a6 = ot0.a(bArr2, i10, pt02);
                    int i20 = pt02.a + a6;
                    while (a6 < i20) {
                        iv03.a(ot0.b(bArr2, a6));
                        a6 += 8;
                    }
                    if (a6 == i20) {
                        return a6;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 1) {
                    iv0 iv04 = (iv0) wu0;
                    iv04.a(ot0.b(bArr, i2));
                    while (true) {
                        int i21 = i10 + 8;
                        if (i21 >= i11) {
                            return i21;
                        }
                        i10 = ot0.a(bArr2, i21, pt02);
                        if (i12 != pt02.a) {
                            return i21;
                        }
                        iv04.a(ot0.b(bArr2, i10));
                    }
                }
                break;
            case 24:
            case 31:
            case 41:
            case 45:
                if (i13 == 2) {
                    su0 su0 = (su0) wu0;
                    int a7 = ot0.a(bArr2, i10, pt02);
                    int i22 = pt02.a + a7;
                    while (a7 < i22) {
                        su0.f(ot0.a(bArr2, a7));
                        a7 += 4;
                    }
                    if (a7 == i22) {
                        return a7;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 5) {
                    su0 su02 = (su0) wu0;
                    su02.f(ot0.a(bArr, i2));
                    while (true) {
                        int i23 = i10 + 4;
                        if (i23 >= i11) {
                            return i23;
                        }
                        i10 = ot0.a(bArr2, i23, pt02);
                        if (i12 != pt02.a) {
                            return i23;
                        }
                        su02.f(ot0.a(bArr2, i10));
                    }
                }
                break;
            case 25:
            case 42:
                if (i13 != 2) {
                    if (i13 == 0) {
                        qt0 qt0 = (qt0) wu0;
                        int b4 = ot0.b(bArr2, i10, pt02);
                        break;
                    }
                } else {
                    qt0 qt02 = (qt0) wu0;
                    int a8 = ot0.a(bArr2, i10, pt02);
                    int i24 = pt02.a + a8;
                    while (i9 < i24) {
                        a8 = ot0.b(bArr2, i9, pt02);
                        qt02.a(pt02.b != 0);
                    }
                    if (i9 != i24) {
                        throw zzco.zzbl();
                    }
                }
                break;
            case 26:
                if (i13 == 2) {
                    int i25 = ((j2 & 536870912) > 0 ? 1 : ((j2 & 536870912) == 0 ? 0 : -1));
                    int a9 = ot0.a(bArr2, i10, pt02);
                    if (i25 == 0) {
                        int i26 = pt02.a;
                        if (i26 != 0) {
                            String str = new String(bArr2, a9, i26, tu0.a);
                            wu0.add(str);
                            a9 += i26;
                            if (i10 < i11) {
                                int a10 = ot0.a(bArr2, i10, pt02);
                                if (i12 == pt02.a) {
                                    a9 = ot0.a(bArr2, a10, pt02);
                                    i26 = pt02.a;
                                    if (i26 != 0) {
                                        str = new String(bArr2, a9, i26, tu0.a);
                                        wu0.add(str);
                                        a9 += i26;
                                        if (i10 < i11) {
                                        }
                                    }
                                }
                            }
                        }
                        wu0.add("");
                        if (i10 < i11) {
                        }
                    } else {
                        int i27 = pt02.a;
                        if (i27 != 0) {
                            int i28 = a9 + i27;
                            if (jx0.a(bArr2, a9, i28)) {
                                String str2 = new String(bArr2, a9, i27, tu0.a);
                                wu0.add(str2);
                                a9 = i28;
                                if (i10 < i11) {
                                    int a11 = ot0.a(bArr2, i10, pt02);
                                    if (i12 == pt02.a) {
                                        a9 = ot0.a(bArr2, a11, pt02);
                                        int i29 = pt02.a;
                                        if (i29 != 0) {
                                            i28 = a9 + i29;
                                            if (jx0.a(bArr2, a9, i28)) {
                                                str2 = new String(bArr2, a9, i29, tu0.a);
                                                wu0.add(str2);
                                                a9 = i28;
                                                if (i10 < i11) {
                                                }
                                            }
                                            throw zzco.zzbp();
                                        }
                                    }
                                }
                            } else {
                                throw zzco.zzbp();
                            }
                        }
                        wu0.add("");
                        if (i10 < i11) {
                        }
                    }
                }
                break;
            case 27:
                if (i13 == 2) {
                    return a((jw0<?>) a(i14), i4, bArr, i2, i3, (wu0<?>) wu0, pt0);
                }
                break;
            case 28:
                if (i13 == 2) {
                    int a12 = ot0.a(bArr2, i10, pt02);
                    int i30 = pt02.a;
                    break;
                }
                break;
            case 30:
            case 44:
                if (i13 == 2) {
                    i9 = ot0.a(bArr2, i10, (wu0<?>) wu0, pt02);
                } else if (i13 == 0) {
                    i9 = ot0.a(i4, bArr, i2, i3, (wu0<?>) wu0, pt0);
                }
                ru0 ru0 = (ru0) t2;
                bx0 bx0 = ru0.zzjp;
                if (bx0 == bx0.d()) {
                    bx0 = null;
                }
                bx0 bx02 = (bx0) lw0.a(i5, wu0, c(i14), bx0, this.o);
                if (bx02 != null) {
                    ru0.zzjp = bx02;
                    break;
                }
                break;
            case 33:
            case 47:
                if (i13 == 2) {
                    su0 su03 = (su0) wu0;
                    int a13 = ot0.a(bArr2, i10, pt02);
                    int i31 = pt02.a + a13;
                    while (a13 < i31) {
                        a13 = ot0.a(bArr2, a13, pt02);
                        su03.f(yt0.a(pt02.a));
                    }
                    if (a13 == i31) {
                        return a13;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 0) {
                    su0 su04 = (su0) wu0;
                    do {
                        a2 = ot0.a(bArr2, i10, pt02);
                        su04.f(yt0.a(pt02.a));
                        if (a2 >= i11) {
                            return a2;
                        }
                        i10 = ot0.a(bArr2, a2, pt02);
                    } while (i12 == pt02.a);
                    return a2;
                }
                break;
            case 34:
            case 48:
                if (i13 == 2) {
                    iv0 iv05 = (iv0) wu0;
                    int a14 = ot0.a(bArr2, i10, pt02);
                    int i32 = pt02.a + a14;
                    while (a14 < i32) {
                        a14 = ot0.b(bArr2, a14, pt02);
                        iv05.a(yt0.a(pt02.b));
                    }
                    if (a14 == i32) {
                        return a14;
                    }
                    throw zzco.zzbl();
                } else if (i13 == 0) {
                    iv0 iv06 = (iv0) wu0;
                    do {
                        b3 = ot0.b(bArr2, i10, pt02);
                        iv06.a(yt0.a(pt02.b));
                        if (b3 >= i11) {
                            return b3;
                        }
                        i10 = ot0.a(bArr2, b3, pt02);
                    } while (i12 == pt02.a);
                    return b3;
                }
                break;
            case 49:
                if (i13 == 3) {
                    jw0 a15 = a(i14);
                    int i33 = (i12 & -8) | 4;
                    i10 = a(a15, bArr, i2, i3, i33, pt0);
                    while (true) {
                        wu0.add(pt02.c);
                        if (i10 >= i11) {
                            break;
                        } else {
                            int a16 = ot0.a(bArr2, i10, pt02);
                            if (i12 != pt02.a) {
                                break;
                            } else {
                                i10 = a(a15, bArr, a16, i3, i33, pt0);
                            }
                        }
                    }
                }
                break;
        }
    }

    @DexIgnore
    public final <K, V> int a(T t, byte[] bArr, int i2, int i3, int i4, int i5, long j2, pt0 pt0) throws IOException {
        Unsafe unsafe = r;
        Object b2 = b(i4);
        Object object = unsafe.getObject(t, j2);
        if (this.q.d(object)) {
            Object b3 = this.q.b(b2);
            this.q.zzb(b3, object);
            unsafe.putObject(t, j2, b3);
        }
        this.q.a(b2);
        throw null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v0, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r30v6, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r30v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v6, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v42, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v43, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v44, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v32, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v33, resolved type: byte} */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x02f8, code lost:
        if (r0 == r15) goto L_0x0355;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0353, code lost:
        if (r0 == r15) goto L_0x0355;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0355, code lost:
        r7 = r30;
        r6 = r32;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0073, code lost:
        r5 = r4;
        r30 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d6, code lost:
        r12 = r29;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0123, code lost:
        r13 = r31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x013a, code lost:
        r6 = r6 | r21;
        r13 = r31;
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013f, code lost:
        r1 = r9;
        r9 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x018e, code lost:
        r10.putObject(r14, r7, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01c8, code lost:
        r6 = r6 | r21;
        r7 = r30;
        r13 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01ef, code lost:
        r5 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0258, code lost:
        r6 = r6 | r21;
        r7 = r30;
        r13 = r31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x025e, code lost:
        r1 = r9;
        r9 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0260, code lost:
        r8 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0261, code lost:
        r11 = r32;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0265, code lost:
        r20 = r30;
        r2 = r5;
        r21 = r6;
        r7 = r9;
        r26 = r10;
        r6 = r32;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    public final int a(T t, byte[] bArr, int i2, int i3, int i4, pt0 pt0) throws IOException {
        int i5;
        int i6;
        Unsafe unsafe;
        int i7;
        int i8;
        int i9;
        T t2;
        int i10;
        int i11;
        int a2;
        byte[] bArr2;
        int i12;
        pt0 pt02;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        byte[] bArr3;
        pt0 pt03;
        int i19;
        pt0 pt04;
        byte[] bArr4;
        int i20;
        int i21;
        long j2;
        Object obj;
        pt0 pt05;
        int i22;
        pt0 pt06;
        int i23;
        byte[] bArr5;
        wv0 wv0 = this;
        T t3 = t;
        byte[] bArr6 = bArr;
        int i24 = i3;
        int i25 = i4;
        pt0 pt07 = pt0;
        Unsafe unsafe2 = r;
        int i26 = -1;
        int i27 = i2;
        int i28 = 0;
        int i29 = 0;
        int i30 = -1;
        while (true) {
            if (i27 < i24) {
                int i31 = i27 + 1;
                byte b2 = bArr6[i27];
                if (b2 < 0) {
                    i11 = ot0.a((int) b2, bArr6, i31, pt07);
                    i10 = pt07.a;
                } else {
                    i10 = b2;
                    i11 = i31;
                }
                int i32 = i10 >>> 3;
                int i33 = i10 & 7;
                int f2 = wv0.f(i32);
                if (f2 != i26) {
                    int[] iArr = wv0.a;
                    int i34 = iArr[f2 + 1];
                    int i35 = (i34 & 267386880) >>> 20;
                    int i36 = i10;
                    long j3 = (long) (i34 & 1048575);
                    int i37 = i34;
                    if (i35 <= 17) {
                        int i38 = iArr[f2 + 2];
                        int i39 = 1 << (i38 >>> 20);
                        int i40 = i38 & 1048575;
                        if (i40 != i30) {
                            if (i30 != -1) {
                                unsafe2.putInt(t3, (long) i30, i29);
                            }
                            i29 = unsafe2.getInt(t3, (long) i40);
                            i30 = i40;
                        }
                        switch (i35) {
                            case 0:
                                i17 = i36;
                                pt04 = pt0;
                                i18 = i11;
                                i16 = i30;
                                long j4 = j3;
                                bArr4 = bArr;
                                if (i33 == 1) {
                                    hx0.a((Object) t3, j4, ot0.c(bArr4, i18));
                                    i20 = i18 + 8;
                                    break;
                                }
                                break;
                            case 1:
                                i17 = i36;
                                pt04 = pt0;
                                i18 = i11;
                                i16 = i30;
                                long j5 = j3;
                                bArr4 = bArr;
                                if (i33 == 5) {
                                    hx0.a((Object) t3, j5, ot0.d(bArr4, i18));
                                    i20 = i18 + 4;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                i17 = i36;
                                pt0 pt08 = pt0;
                                i18 = i11;
                                i16 = i30;
                                long j6 = j3;
                                bArr3 = bArr;
                                if (i33 == 0) {
                                    int b3 = ot0.b(bArr3, i18, pt08);
                                    unsafe2.putLong(t, j6, pt08.b);
                                    i29 |= i39;
                                    i30 = i16;
                                    int i41 = i3;
                                    i28 = i17;
                                    pt07 = pt08;
                                    i19 = b3;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                i17 = i36;
                                pt04 = pt0;
                                i18 = i11;
                                i16 = i30;
                                long j7 = j3;
                                bArr4 = bArr;
                                if (i33 == 0) {
                                    i20 = ot0.a(bArr4, i18, pt04);
                                    unsafe2.putInt(t3, j7, pt04.a);
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                i17 = i36;
                                int i42 = i3;
                                pt04 = pt0;
                                i16 = i30;
                                long j8 = j3;
                                bArr4 = bArr;
                                if (i33 == 1) {
                                    unsafe2.putLong(t, j8, ot0.b(bArr4, i11));
                                    i20 = i11 + 8;
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                i17 = i36;
                                i21 = i3;
                                pt03 = pt0;
                                i16 = i30;
                                long j9 = j3;
                                bArr3 = bArr;
                                if (i33 == 5) {
                                    unsafe2.putInt(t3, j9, ot0.a(bArr3, i11));
                                    i19 = i11 + 4;
                                    break;
                                }
                                break;
                            case 7:
                                i17 = i36;
                                i21 = i3;
                                pt03 = pt0;
                                i16 = i30;
                                long j10 = j3;
                                bArr3 = bArr;
                                if (i33 == 0) {
                                    i19 = ot0.b(bArr3, i11, pt03);
                                    hx0.a((Object) t3, j10, pt03.b != 0);
                                    break;
                                }
                                break;
                            case 8:
                                i17 = i36;
                                i21 = i3;
                                pt03 = pt0;
                                i16 = i30;
                                j2 = j3;
                                bArr3 = bArr;
                                if (i33 == 2) {
                                    i19 = (i37 & 536870912) == 0 ? ot0.c(bArr3, i11, pt03) : ot0.d(bArr3, i11, pt03);
                                    obj = pt03.c;
                                    break;
                                }
                                break;
                            case 9:
                                i17 = i36;
                                pt03 = pt0;
                                i16 = i30;
                                j2 = j3;
                                bArr3 = bArr;
                                if (i33 != 2) {
                                    int i43 = i3;
                                    break;
                                } else {
                                    i21 = i3;
                                    i19 = a(wv0.a(f2), bArr3, i11, i21, pt03);
                                    if ((i29 & i39) != 0) {
                                        obj = tu0.a(unsafe2.getObject(t3, j2), pt03.c);
                                        break;
                                    } else {
                                        obj = pt03.c;
                                        break;
                                    }
                                }
                            case 10:
                                i17 = i36;
                                pt06 = pt0;
                                long j11 = j3;
                                i23 = -1;
                                bArr5 = bArr;
                                if (i33 == 2) {
                                    i22 = ot0.e(bArr5, i11, pt06);
                                    unsafe2.putObject(t3, j11, pt06.c);
                                    break;
                                }
                                break;
                            case 12:
                                i17 = i36;
                                pt05 = pt0;
                                long j12 = j3;
                                i26 = -1;
                                bArr6 = bArr;
                                if (i33 == 0) {
                                    i27 = ot0.a(bArr6, i11, pt05);
                                    int i44 = pt05.a;
                                    vu0<?> c2 = wv0.c(f2);
                                    if (c2 != null && c2.zzb(i44) == null) {
                                        d((Object) t).a(i17, (Object) Long.valueOf((long) i44));
                                        break;
                                    } else {
                                        unsafe2.putInt(t3, j12, i44);
                                        i29 |= i39;
                                        break;
                                    }
                                }
                                break;
                            case 15:
                                i17 = i36;
                                pt06 = pt0;
                                long j13 = j3;
                                i23 = -1;
                                bArr5 = bArr;
                                if (i33 == 0) {
                                    i22 = ot0.a(bArr5, i11, pt06);
                                    unsafe2.putInt(t3, j13, yt0.a(pt06.a));
                                    break;
                                }
                                break;
                            case 16:
                                i17 = i36;
                                pt0 pt09 = pt0;
                                i26 = -1;
                                if (i33 == 0) {
                                    long j14 = j3;
                                    bArr6 = bArr;
                                    int b4 = ot0.b(bArr6, i11, pt09);
                                    unsafe2.putLong(t, j14, yt0.a(pt09.b));
                                    i29 |= i39;
                                    i28 = i17;
                                    pt07 = pt09;
                                    i27 = b4;
                                    i24 = i3;
                                    break;
                                }
                                break;
                            case 17:
                                if (i33 != 3) {
                                    i17 = i36;
                                    pt0 pt010 = pt0;
                                    break;
                                } else {
                                    i17 = i36;
                                    i26 = -1;
                                    i27 = a(wv0.a(f2), bArr, i11, i3, (i32 << 3) | 4, pt0);
                                    pt05 = pt0;
                                    unsafe2.putObject(t3, j3, (i29 & i39) == 0 ? pt05.c : tu0.a(unsafe2.getObject(t3, j3), pt05.c));
                                    i29 |= i39;
                                    bArr6 = bArr;
                                    break;
                                }
                            default:
                                byte[] bArr7 = bArr;
                                i17 = i36;
                                pt0 pt011 = pt0;
                                break;
                        }
                    } else {
                        int i45 = i36;
                        int i46 = i11;
                        i6 = i30;
                        long j15 = j3;
                        bArr6 = bArr;
                        if (i35 != 27) {
                            i5 = i29;
                            if (i35 <= 49) {
                                int i47 = i46;
                                i14 = i45;
                                unsafe = unsafe2;
                                boolean z = i4;
                                a2 = a(t, bArr, i46, i3, i45, i32, i33, f2, (long) i37, i35, j15, pt0);
                            } else {
                                int i48 = i32;
                                i15 = i46;
                                long j16 = j15;
                                i14 = i45;
                                unsafe = unsafe2;
                                int i49 = i37;
                                int i50 = f2;
                                int i51 = i33;
                                if (i35 == 50) {
                                    if (i51 == 2) {
                                        a(t, bArr, i15, i3, i50, i48, j16, pt0);
                                        throw null;
                                    }
                                    i9 = i14;
                                    i7 = i4;
                                    i8 = i15;
                                    if (i9 == i7 || i7 == 0) {
                                        a2 = a(i9, bArr, i8, i3, (Object) t, pt0);
                                        wv0 = this;
                                        t3 = t;
                                        bArr2 = bArr;
                                        i12 = i3;
                                        pt02 = pt0;
                                        i25 = i7;
                                        i13 = i9;
                                        i30 = i6;
                                        i29 = i5;
                                        unsafe2 = unsafe;
                                        i26 = -1;
                                    }
                                } else {
                                    a2 = a(t, bArr, i15, i3, i14, i48, i51, i49, i35, j16, i50, pt0);
                                }
                            }
                            wv0 = this;
                            t3 = t;
                            bArr2 = bArr;
                            i13 = i14;
                            i12 = i3;
                            i25 = i4;
                            pt02 = pt0;
                            i30 = i6;
                            i29 = i5;
                            unsafe2 = unsafe;
                            i26 = -1;
                        } else if (i33 == 2) {
                            wu0 wu0 = (wu0) unsafe2.getObject(t3, j15);
                            if (!wu0.y()) {
                                int size = wu0.size();
                                wu0 = wu0.c(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t3, j15, wu0);
                            }
                            wu0 wu02 = wu0;
                            jw0 a3 = wv0.a(f2);
                            i28 = i45;
                            i27 = a((jw0<?>) a3, i28, bArr, i46, i3, (wu0<?>) wu02, pt0);
                            i24 = i3;
                            i25 = i4;
                            i30 = i6;
                            i29 = i29;
                            i26 = -1;
                            pt07 = pt0;
                        } else {
                            i5 = i29;
                            i15 = i46;
                            i14 = i45;
                        }
                    }
                } else {
                    i15 = i11;
                    i14 = i10;
                    i5 = i29;
                    i6 = i30;
                }
                unsafe = unsafe2;
                i9 = i14;
                i7 = i4;
                i8 = i15;
                if (i9 == i7) {
                }
                a2 = a(i9, bArr, i8, i3, (Object) t, pt0);
                wv0 = this;
                t3 = t;
                bArr2 = bArr;
                i12 = i3;
                pt02 = pt0;
                i25 = i7;
                i13 = i9;
                i30 = i6;
                i29 = i5;
                unsafe2 = unsafe;
                i26 = -1;
            } else {
                i5 = i29;
                i6 = i30;
                unsafe = unsafe2;
                i7 = i25;
                i8 = i27;
                i9 = i28;
            }
        }
        int i52 = i6;
        int i53 = i5;
        if (i52 != -1) {
            long j17 = (long) i52;
            t2 = t;
            unsafe.putInt(t2, j17, i53);
        } else {
            t2 = t;
        }
        int[] iArr2 = this.k;
        if (iArr2 != null) {
            bx0 bx0 = null;
            for (int i54 : iArr2) {
                ax0<?, ?> ax0 = this.o;
                int i55 = this.a[i54];
                Object f3 = hx0.f(t2, (long) (d(i54) & 1048575));
                if (f3 != null) {
                    vu0<?> c3 = c(i54);
                    if (c3 != null) {
                        a(i54, i55, this.q.f(f3), c3, bx0, ax0);
                        throw null;
                    }
                }
                bx0 = bx0;
            }
            if (bx0 != null) {
                this.o.b((Object) t2, bx0);
            }
        }
        int i56 = i3;
        if (i7 == 0) {
            if (i8 != i56) {
                throw zzco.zzbo();
            }
        } else if (i8 > i56 || i9 != i7) {
            throw zzco.zzbo();
        }
        return i8;
    }

    @DexIgnore
    public final jw0 a(int i2) {
        int i3 = (i2 / 4) << 1;
        jw0 jw0 = (jw0) this.b[i3];
        if (jw0 != null) {
            return jw0;
        }
        jw0 a2 = ew0.a().a((Class) this.b[i3 + 1]);
        this.b[i3] = a2;
        return a2;
    }

    @DexIgnore
    public final T a() {
        return this.m.a(this.f);
    }

    @DexIgnore
    public final <K, V, UT, UB> UB a(int i2, int i3, Map<K, V> map, vu0<?> vu0, UB ub, ax0<UT, UB> ax0) {
        this.q.a(b(i2));
        throw null;
    }

    @DexIgnore
    public final <K, V> void a(ox0 ox0, int i2, Object obj, int i3) throws IOException {
        if (obj != null) {
            this.q.a(b(i3));
            throw null;
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0385, code lost:
        r15.b(r9, com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r8 & 1048575)), a(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x03a0, code lost:
        r15.zzb(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x03b1, code lost:
        r15.zze(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x03c2, code lost:
        r15.a(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x03d3, code lost:
        r15.b(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x03e4, code lost:
        r15.a(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x03f5, code lost:
        r15.zzd(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x0400, code lost:
        r15.a(r9, (com.google.android.gms.internal.clearcut.zzbb) com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r8 & 1048575)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0413, code lost:
        r15.a(r9, com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r8 & 1048575)), a(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x0428, code lost:
        a(r9, com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r8 & 1048575)), r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x043f, code lost:
        r15.a(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0450, code lost:
        r15.zzf(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0460, code lost:
        r15.zzc(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0470, code lost:
        r15.zzc(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0480, code lost:
        r15.zza(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0490, code lost:
        r15.b(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x04a0, code lost:
        r15.zza(r9, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x04b0, code lost:
        r15.zza(r9, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x0843, code lost:
        r15.b(r10, com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r9 & 1048575)), a(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x085e, code lost:
        r15.zzb(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x086f, code lost:
        r15.zze(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x0880, code lost:
        r15.a(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x0891, code lost:
        r15.b(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x08a2, code lost:
        r15.a(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x08b3, code lost:
        r15.zzd(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x08be, code lost:
        r15.a(r10, (com.google.android.gms.internal.clearcut.zzbb) com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r9 & 1048575)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x08d1, code lost:
        r15.a(r10, com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r9 & 1048575)), a(r1));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x08e6, code lost:
        a(r10, com.fossil.blesdk.obfuscated.hx0.f(r14, (long) (r9 & 1048575)), r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x08fd, code lost:
        r15.a(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:324:0x090e, code lost:
        r15.zzf(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x091e, code lost:
        r15.zzc(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:332:0x092e, code lost:
        r15.zzc(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x093e, code lost:
        r15.zza(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x094e, code lost:
        r15.b(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:344:0x095e, code lost:
        r15.zza(r10, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x096e, code lost:
        r15.zza(r10, r11);
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x04b9  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x04f7  */
    /* JADX WARNING: Removed duplicated region for block: B:351:0x0977  */
    public final void a(T t, ox0 ox0) throws IOException {
        Map.Entry entry;
        Iterator it;
        int length;
        Map.Entry entry2;
        int i2;
        double d2;
        float f2;
        long j2;
        long j3;
        int i3;
        long j4;
        int i4;
        boolean z;
        int i5;
        int i6;
        int i7;
        long j5;
        int i8;
        long j6;
        Map.Entry entry3;
        Iterator it2;
        int length2;
        double d3;
        float f3;
        long j7;
        long j8;
        int i9;
        long j9;
        int i10;
        boolean z2;
        int i11;
        int i12;
        int i13;
        long j10;
        int i14;
        long j11;
        if (ox0.a() == ru0.e.l) {
            a(this.o, t, ox0);
            if (this.g) {
                ku0 a2 = this.p.a((Object) t);
                if (!a2.b()) {
                    it2 = a2.a();
                    entry3 = (Map.Entry) it2.next();
                    length2 = this.a.length - 4;
                    while (length2 >= 0) {
                        int d4 = d(length2);
                        int i15 = this.a[length2];
                        while (entry3 != null && this.p.a((Map.Entry<?, ?>) entry3) > i15) {
                            this.p.a(ox0, entry3);
                            entry3 = it2.hasNext() ? (Map.Entry) it2.next() : null;
                        }
                        switch ((d4 & 267386880) >>> 20) {
                            case 0:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    d3 = hx0.e(t, (long) (d4 & 1048575));
                                }
                            case 1:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    f3 = hx0.d(t, (long) (d4 & 1048575));
                                }
                            case 2:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    j7 = hx0.b(t, (long) (d4 & 1048575));
                                }
                            case 3:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    j8 = hx0.b(t, (long) (d4 & 1048575));
                                }
                            case 4:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    i9 = hx0.a((Object) t, (long) (d4 & 1048575));
                                }
                            case 5:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    j9 = hx0.b(t, (long) (d4 & 1048575));
                                }
                            case 6:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    i10 = hx0.a((Object) t, (long) (d4 & 1048575));
                                }
                            case 7:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    z2 = hx0.c(t, (long) (d4 & 1048575));
                                }
                            case 8:
                                if (!a(t, length2)) {
                                    break;
                                }
                            case 9:
                                if (!a(t, length2)) {
                                    break;
                                }
                            case 10:
                                if (!a(t, length2)) {
                                    break;
                                }
                            case 11:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    i11 = hx0.a((Object) t, (long) (d4 & 1048575));
                                }
                            case 12:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    i12 = hx0.a((Object) t, (long) (d4 & 1048575));
                                }
                            case 13:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    i13 = hx0.a((Object) t, (long) (d4 & 1048575));
                                }
                            case 14:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    j10 = hx0.b(t, (long) (d4 & 1048575));
                                }
                            case 15:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    i14 = hx0.a((Object) t, (long) (d4 & 1048575));
                                }
                            case 16:
                                if (!a(t, length2)) {
                                    break;
                                } else {
                                    j11 = hx0.b(t, (long) (d4 & 1048575));
                                }
                            case 17:
                                if (!a(t, length2)) {
                                    break;
                                }
                            case 18:
                                lw0.a(this.a[length2], (List<Double>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 19:
                                lw0.b(this.a[length2], (List<Float>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 20:
                                lw0.c(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 21:
                                lw0.d(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 22:
                                lw0.h(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 23:
                                lw0.f(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 24:
                                lw0.k(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 25:
                                lw0.n(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 26:
                                lw0.a(this.a[length2], (List<String>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0);
                                break;
                            case 27:
                                lw0.a(this.a[length2], (List<?>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0, a(length2));
                                break;
                            case 28:
                                lw0.b(this.a[length2], (List<zzbb>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0);
                                break;
                            case 29:
                                lw0.i(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 30:
                                lw0.m(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 31:
                                lw0.l(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 32:
                                lw0.g(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 33:
                                lw0.j(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 34:
                                lw0.e(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, false);
                                break;
                            case 35:
                                lw0.a(this.a[length2], (List<Double>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 36:
                                lw0.b(this.a[length2], (List<Float>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 37:
                                lw0.c(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 38:
                                lw0.d(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 39:
                                lw0.h(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 40:
                                lw0.f(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 41:
                                lw0.k(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 42:
                                lw0.n(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 43:
                                lw0.i(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 44:
                                lw0.m(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 45:
                                lw0.l(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 46:
                                lw0.g(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 47:
                                lw0.j(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 48:
                                lw0.e(this.a[length2], (List) hx0.f(t, (long) (d4 & 1048575)), ox0, true);
                                break;
                            case 49:
                                lw0.b(this.a[length2], (List<?>) (List) hx0.f(t, (long) (d4 & 1048575)), ox0, a(length2));
                                break;
                            case 50:
                                a(ox0, i15, hx0.f(t, (long) (d4 & 1048575)), length2);
                                break;
                            case 51:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    d3 = b(t, (long) (d4 & 1048575));
                                }
                            case 52:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    f3 = c(t, (long) (d4 & 1048575));
                                }
                            case 53:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    j7 = e(t, (long) (d4 & 1048575));
                                }
                            case 54:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    j8 = e(t, (long) (d4 & 1048575));
                                }
                            case 55:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    i9 = d(t, (long) (d4 & 1048575));
                                }
                            case 56:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    j9 = e(t, (long) (d4 & 1048575));
                                }
                            case 57:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    i10 = d(t, (long) (d4 & 1048575));
                                }
                            case 58:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    z2 = f(t, (long) (d4 & 1048575));
                                }
                            case 59:
                                if (!a(t, i15, length2)) {
                                    break;
                                }
                            case 60:
                                if (!a(t, i15, length2)) {
                                    break;
                                }
                            case 61:
                                if (!a(t, i15, length2)) {
                                    break;
                                }
                            case 62:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    i11 = d(t, (long) (d4 & 1048575));
                                }
                            case 63:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    i12 = d(t, (long) (d4 & 1048575));
                                }
                            case 64:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    i13 = d(t, (long) (d4 & 1048575));
                                }
                            case 65:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    j10 = e(t, (long) (d4 & 1048575));
                                }
                            case 66:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    i14 = d(t, (long) (d4 & 1048575));
                                }
                            case 67:
                                if (!a(t, i15, length2)) {
                                    break;
                                } else {
                                    j11 = e(t, (long) (d4 & 1048575));
                                }
                            case 68:
                                if (!a(t, i15, length2)) {
                                    break;
                                }
                        }
                        length2 -= 4;
                    }
                    while (entry3 != null) {
                        this.p.a(ox0, entry3);
                        entry3 = it2.hasNext() ? (Map.Entry) it2.next() : null;
                    }
                }
            }
            it2 = null;
            entry3 = null;
            length2 = this.a.length - 4;
            while (length2 >= 0) {
            }
            while (entry3 != null) {
            }
        } else if (this.h) {
            if (this.g) {
                ku0 a3 = this.p.a((Object) t);
                if (!a3.b()) {
                    it = a3.e();
                    entry = (Map.Entry) it.next();
                    length = this.a.length;
                    entry2 = entry;
                    i2 = 0;
                    while (i2 < length) {
                        int d5 = d(i2);
                        int i16 = this.a[i2];
                        while (entry2 != null && this.p.a((Map.Entry<?, ?>) entry2) <= i16) {
                            this.p.a(ox0, entry2);
                            entry2 = it.hasNext() ? (Map.Entry) it.next() : null;
                        }
                        switch ((d5 & 267386880) >>> 20) {
                            case 0:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    d2 = hx0.e(t, (long) (d5 & 1048575));
                                }
                            case 1:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    f2 = hx0.d(t, (long) (d5 & 1048575));
                                }
                            case 2:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    j2 = hx0.b(t, (long) (d5 & 1048575));
                                }
                            case 3:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    j3 = hx0.b(t, (long) (d5 & 1048575));
                                }
                            case 4:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    i3 = hx0.a((Object) t, (long) (d5 & 1048575));
                                }
                            case 5:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    j4 = hx0.b(t, (long) (d5 & 1048575));
                                }
                            case 6:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    i4 = hx0.a((Object) t, (long) (d5 & 1048575));
                                }
                            case 7:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    z = hx0.c(t, (long) (d5 & 1048575));
                                }
                            case 8:
                                if (!a(t, i2)) {
                                    break;
                                }
                            case 9:
                                if (!a(t, i2)) {
                                    break;
                                }
                            case 10:
                                if (!a(t, i2)) {
                                    break;
                                }
                            case 11:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    i5 = hx0.a((Object) t, (long) (d5 & 1048575));
                                }
                            case 12:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    i6 = hx0.a((Object) t, (long) (d5 & 1048575));
                                }
                            case 13:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    i7 = hx0.a((Object) t, (long) (d5 & 1048575));
                                }
                            case 14:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    j5 = hx0.b(t, (long) (d5 & 1048575));
                                }
                            case 15:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    i8 = hx0.a((Object) t, (long) (d5 & 1048575));
                                }
                            case 16:
                                if (!a(t, i2)) {
                                    break;
                                } else {
                                    j6 = hx0.b(t, (long) (d5 & 1048575));
                                }
                            case 17:
                                if (!a(t, i2)) {
                                    break;
                                }
                            case 18:
                                lw0.a(this.a[i2], (List<Double>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 19:
                                lw0.b(this.a[i2], (List<Float>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 20:
                                lw0.c(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 21:
                                lw0.d(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 22:
                                lw0.h(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 23:
                                lw0.f(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 24:
                                lw0.k(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 25:
                                lw0.n(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 26:
                                lw0.a(this.a[i2], (List<String>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0);
                                break;
                            case 27:
                                lw0.a(this.a[i2], (List<?>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0, a(i2));
                                break;
                            case 28:
                                lw0.b(this.a[i2], (List<zzbb>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0);
                                break;
                            case 29:
                                lw0.i(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 30:
                                lw0.m(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 31:
                                lw0.l(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 32:
                                lw0.g(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 33:
                                lw0.j(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 34:
                                lw0.e(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, false);
                                break;
                            case 35:
                                lw0.a(this.a[i2], (List<Double>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 36:
                                lw0.b(this.a[i2], (List<Float>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 37:
                                lw0.c(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 38:
                                lw0.d(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 39:
                                lw0.h(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 40:
                                lw0.f(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 41:
                                lw0.k(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 42:
                                lw0.n(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 43:
                                lw0.i(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 44:
                                lw0.m(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 45:
                                lw0.l(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 46:
                                lw0.g(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 47:
                                lw0.j(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 48:
                                lw0.e(this.a[i2], (List) hx0.f(t, (long) (d5 & 1048575)), ox0, true);
                                break;
                            case 49:
                                lw0.b(this.a[i2], (List<?>) (List) hx0.f(t, (long) (d5 & 1048575)), ox0, a(i2));
                                break;
                            case 50:
                                a(ox0, i16, hx0.f(t, (long) (d5 & 1048575)), i2);
                                break;
                            case 51:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    d2 = b(t, (long) (d5 & 1048575));
                                }
                            case 52:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    f2 = c(t, (long) (d5 & 1048575));
                                }
                            case 53:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    j2 = e(t, (long) (d5 & 1048575));
                                }
                            case 54:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    j3 = e(t, (long) (d5 & 1048575));
                                }
                            case 55:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    i3 = d(t, (long) (d5 & 1048575));
                                }
                            case 56:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    j4 = e(t, (long) (d5 & 1048575));
                                }
                            case 57:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    i4 = d(t, (long) (d5 & 1048575));
                                }
                            case 58:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    z = f(t, (long) (d5 & 1048575));
                                }
                            case 59:
                                if (!a(t, i16, i2)) {
                                    break;
                                }
                            case 60:
                                if (!a(t, i16, i2)) {
                                    break;
                                }
                            case 61:
                                if (!a(t, i16, i2)) {
                                    break;
                                }
                            case 62:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    i5 = d(t, (long) (d5 & 1048575));
                                }
                            case 63:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    i6 = d(t, (long) (d5 & 1048575));
                                }
                            case 64:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    i7 = d(t, (long) (d5 & 1048575));
                                }
                            case 65:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    j5 = e(t, (long) (d5 & 1048575));
                                }
                            case 66:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    i8 = d(t, (long) (d5 & 1048575));
                                }
                            case 67:
                                if (!a(t, i16, i2)) {
                                    break;
                                } else {
                                    j6 = e(t, (long) (d5 & 1048575));
                                }
                            case 68:
                                if (!a(t, i16, i2)) {
                                    break;
                                }
                        }
                        i2 += 4;
                    }
                    while (entry2 != null) {
                        this.p.a(ox0, entry2);
                        entry2 = it.hasNext() ? (Map.Entry) it.next() : null;
                    }
                    a(this.o, t, ox0);
                }
            }
            it = null;
            entry = null;
            length = this.a.length;
            entry2 = entry;
            i2 = 0;
            while (i2 < length) {
            }
            while (entry2 != null) {
            }
            a(this.o, t, ox0);
        } else {
            b(t, ox0);
        }
    }

    @DexIgnore
    public final void a(T t, T t2, int i2) {
        long d2 = (long) (d(i2) & 1048575);
        if (a(t2, i2)) {
            Object f2 = hx0.f(t, d2);
            Object f3 = hx0.f(t2, d2);
            if (f2 != null && f3 != null) {
                hx0.a((Object) t, d2, tu0.a(f2, f3));
                b(t, i2);
            } else if (f3 != null) {
                hx0.a((Object) t, d2, f3);
                b(t, i2);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r16v2, resolved type: byte} */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0069, code lost:
        if (r7 == 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0073, code lost:
        r1 = r11.c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0075, code lost:
        r9.putObject(r14, r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ce, code lost:
        if (r7 == 0) goto L_0x00d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00d0, code lost:
        r0 = com.fossil.blesdk.obfuscated.ot0.a(r12, r10, r11);
        r1 = r11.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d6, code lost:
        r9.putInt(r14, r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e3, code lost:
        r9.putLong(r23, r2, r4);
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00f5, code lost:
        r0 = r10 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0102, code lost:
        r0 = r10 + 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0164, code lost:
        if (r0 == r15) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01a0, code lost:
        if (r0 == r15) goto L_0x01a2;
     */
    @DexIgnore
    /* JADX WARNING: Multi-variable type inference failed */
    public final void a(T t, byte[] bArr, int i2, int i3, pt0 pt0) throws IOException {
        byte b2;
        int i4;
        Unsafe unsafe;
        int i5;
        int a2;
        int i6;
        int i7;
        long j2;
        wv0 wv0 = this;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i8 = i3;
        pt0 pt02 = pt0;
        if (wv0.h) {
            Unsafe unsafe2 = r;
            int i9 = i2;
            while (i9 < i8) {
                int i10 = i9 + 1;
                byte b3 = bArr2[i9];
                if (b3 < 0) {
                    i4 = ot0.a((int) b3, bArr2, i10, pt02);
                    b2 = pt02.a;
                } else {
                    b2 = b3;
                    i4 = i10;
                }
                int i11 = b2 >>> 3;
                byte b4 = b2 & 7;
                int f2 = wv0.f(i11);
                if (f2 >= 0) {
                    int i12 = wv0.a[f2 + 1];
                    int i13 = (267386880 & i12) >>> 20;
                    long j3 = (long) (1048575 & i12);
                    if (i13 <= 17) {
                        boolean z = true;
                        switch (i13) {
                            case 0:
                                if (b4 == 1) {
                                    hx0.a((Object) t2, j3, ot0.c(bArr2, i4));
                                    break;
                                }
                                break;
                            case 1:
                                if (b4 == 5) {
                                    hx0.a((Object) t2, j3, ot0.d(bArr2, i4));
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                if (b4 == 0) {
                                    i7 = ot0.b(bArr2, i4, pt02);
                                    j2 = pt02.b;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                break;
                            case 5:
                            case 14:
                                if (b4 == 1) {
                                    unsafe2.putLong(t, j3, ot0.b(bArr2, i4));
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                if (b4 == 5) {
                                    unsafe2.putInt(t2, j3, ot0.a(bArr2, i4));
                                    break;
                                }
                                break;
                            case 7:
                                if (b4 == 0) {
                                    i9 = ot0.b(bArr2, i4, pt02);
                                    if (pt02.b == 0) {
                                        z = false;
                                    }
                                    hx0.a((Object) t2, j3, z);
                                    break;
                                }
                                break;
                            case 8:
                                if (b4 == 2) {
                                    if ((536870912 & i12) != 0) {
                                        i9 = ot0.d(bArr2, i4, pt02);
                                        break;
                                    } else {
                                        i9 = ot0.c(bArr2, i4, pt02);
                                        break;
                                    }
                                }
                                break;
                            case 9:
                                if (b4 == 2) {
                                    i9 = a(wv0.a(f2), bArr2, i4, i8, pt02);
                                    Object object = unsafe2.getObject(t2, j3);
                                    if (object != null) {
                                        Object a3 = tu0.a(object, pt02.c);
                                        break;
                                    }
                                }
                                break;
                            case 10:
                                if (b4 == 2) {
                                    i9 = ot0.e(bArr2, i4, pt02);
                                    break;
                                }
                                break;
                            case 12:
                                break;
                            case 15:
                                if (b4 == 0) {
                                    int a4 = ot0.a(bArr2, i4, pt02);
                                    int i14 = yt0.a(pt02.a);
                                    break;
                                }
                                break;
                            case 16:
                                if (b4 == 0) {
                                    i7 = ot0.b(bArr2, i4, pt02);
                                    j2 = yt0.a(pt02.b);
                                    break;
                                }
                                break;
                        }
                    } else if (i13 != 27) {
                        if (i13 <= 49) {
                            unsafe = unsafe2;
                            int i15 = i4;
                            a2 = a(t, bArr, i4, i3, (int) b2, i11, (int) b4, f2, (long) i12, i13, j3, pt0);
                        } else {
                            long j4 = j3;
                            unsafe = unsafe2;
                            i6 = i4;
                            int i16 = i13;
                            if (i16 == 50) {
                                if (b4 == 2) {
                                    a(t, bArr, i6, i3, f2, i11, j4, pt0);
                                    throw null;
                                }
                                i5 = i6;
                                a2 = a((int) b2, bArr, i5, i3, (Object) t, pt0);
                                wv0 = this;
                                t2 = t;
                                bArr2 = bArr;
                                i8 = i3;
                                pt02 = pt0;
                                unsafe2 = unsafe;
                            } else {
                                a2 = a(t, bArr, i6, i3, (int) b2, i11, (int) b4, i12, i16, j4, f2, pt0);
                            }
                        }
                        i5 = a2;
                        a2 = a((int) b2, bArr, i5, i3, (Object) t, pt0);
                        wv0 = this;
                        t2 = t;
                        bArr2 = bArr;
                        i8 = i3;
                        pt02 = pt0;
                        unsafe2 = unsafe;
                    } else if (b4 == 2) {
                        wu0 wu0 = (wu0) unsafe2.getObject(t2, j3);
                        if (!wu0.y()) {
                            int size = wu0.size();
                            wu0 = wu0.c(size == 0 ? 10 : size << 1);
                            unsafe2.putObject(t2, j3, wu0);
                        }
                        i9 = a((jw0<?>) wv0.a(f2), (int) b2, bArr, i4, i3, (wu0<?>) wu0, pt0);
                    }
                }
                unsafe = unsafe2;
                i6 = i4;
                i5 = i6;
                a2 = a((int) b2, bArr, i5, i3, (Object) t, pt0);
                wv0 = this;
                t2 = t;
                bArr2 = bArr;
                i8 = i3;
                pt02 = pt0;
                unsafe2 = unsafe;
            }
            if (i9 != i8) {
                throw zzco.zzbo();
            }
            return;
        }
        int i17 = i8;
        a(t, bArr, i2, i3, 0, pt0);
    }

    @DexIgnore
    public final boolean a(T t, int i2) {
        if (this.h) {
            int d2 = d(i2);
            long j2 = (long) (d2 & 1048575);
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    return hx0.e(t, j2) != 0.0d;
                case 1:
                    return hx0.d(t, j2) != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                case 2:
                    return hx0.b(t, j2) != 0;
                case 3:
                    return hx0.b(t, j2) != 0;
                case 4:
                    return hx0.a((Object) t, j2) != 0;
                case 5:
                    return hx0.b(t, j2) != 0;
                case 6:
                    return hx0.a((Object) t, j2) != 0;
                case 7:
                    return hx0.c(t, j2);
                case 8:
                    Object f2 = hx0.f(t, j2);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof zzbb) {
                        return !zzbb.zzfi.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return hx0.f(t, j2) != null;
                case 10:
                    return !zzbb.zzfi.equals(hx0.f(t, j2));
                case 11:
                    return hx0.a((Object) t, j2) != 0;
                case 12:
                    return hx0.a((Object) t, j2) != 0;
                case 13:
                    return hx0.a((Object) t, j2) != 0;
                case 14:
                    return hx0.b(t, j2) != 0;
                case 15:
                    return hx0.a((Object) t, j2) != 0;
                case 16:
                    return hx0.b(t, j2) != 0;
                case 17:
                    return hx0.f(t, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int e2 = e(i2);
            return (hx0.a((Object) t, (long) (e2 & 1048575)) & (1 << (e2 >>> 20))) != 0;
        }
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3) {
        return hx0.a((Object) t, (long) (e(i3) & 1048575)) == i2;
    }

    @DexIgnore
    public final boolean a(T t, int i2, int i3, int i4) {
        return this.h ? a(t, i2) : (i3 & i4) != 0;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005c, code lost:
        if (com.fossil.blesdk.obfuscated.lw0.a(com.fossil.blesdk.obfuscated.hx0.f(r10, r6), com.fossil.blesdk.obfuscated.hx0.f(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0070, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.b(r10, r6) == com.fossil.blesdk.obfuscated.hx0.b(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0082, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0096, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.b(r10, r6) == com.fossil.blesdk.obfuscated.hx0.b(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a8, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00ba, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cc, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e2, code lost:
        if (com.fossil.blesdk.obfuscated.lw0.a(com.fossil.blesdk.obfuscated.hx0.f(r10, r6), com.fossil.blesdk.obfuscated.hx0.f(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00f8, code lost:
        if (com.fossil.blesdk.obfuscated.lw0.a(com.fossil.blesdk.obfuscated.hx0.f(r10, r6), com.fossil.blesdk.obfuscated.hx0.f(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x010e, code lost:
        if (com.fossil.blesdk.obfuscated.lw0.a(com.fossil.blesdk.obfuscated.hx0.f(r10, r6), com.fossil.blesdk.obfuscated.hx0.f(r11, r6)) != false) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0120, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.c(r10, r6) == com.fossil.blesdk.obfuscated.hx0.c(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0132, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0145, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.b(r10, r6) == com.fossil.blesdk.obfuscated.hx0.b(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0156, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0169, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.b(r10, r6) == com.fossil.blesdk.obfuscated.hx0.b(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x017c, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.b(r10, r6) == com.fossil.blesdk.obfuscated.hx0.b(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x018d, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r10, r6) == com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01a0, code lost:
        if (com.fossil.blesdk.obfuscated.hx0.b(r10, r6) == com.fossil.blesdk.obfuscated.hx0.b(r11, r6)) goto L_0x01a3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01a2, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.fossil.blesdk.obfuscated.lw0.a(com.fossil.blesdk.obfuscated.hx0.f(r10, r6), com.fossil.blesdk.obfuscated.hx0.f(r11, r6)) != false) goto L_0x01a3;
     */
    @DexIgnore
    public final boolean a(T t, T t2) {
        int length = this.a.length;
        int i2 = 0;
        while (true) {
            boolean z = true;
            if (i2 < length) {
                int d2 = d(i2);
                long j2 = (long) (d2 & 1048575);
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 1:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 2:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 3:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 4:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 5:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 6:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 7:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 8:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 9:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 10:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 11:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 12:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 13:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 14:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 15:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 16:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 17:
                        if (c(t, t2, i2)) {
                            break;
                        }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                    case 50:
                        z = lw0.a(hx0.f(t, j2), hx0.f(t2, j2));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long e2 = (long) (e(i2) & 1048575);
                        if (hx0.a((Object) t, e2) == hx0.a((Object) t2, e2)) {
                            break;
                        }
                }
                if (!z) {
                    return false;
                }
                i2 += 4;
            } else if (!this.o.c(t).equals(this.o.c(t2))) {
                return false;
            } else {
                if (this.g) {
                    return this.p.a((Object) t).equals(this.p.a((Object) t2));
                }
                return true;
            }
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01d8, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01e9, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01fa, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x020b, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x020d, code lost:
        r2.putInt(r1, (long) r14, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0211, code lost:
        r3 = (com.google.android.gms.internal.clearcut.zzbn.e(r3) + com.google.android.gms.internal.clearcut.zzbn.g(r5)) + r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0296, code lost:
        r13 = r13 + r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x029f, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.c(r3, (com.fossil.blesdk.obfuscated.sv0) com.fossil.blesdk.obfuscated.hx0.f(r1, r5), a(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x02b8, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.f(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x02c7, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.h(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:0x02d2, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.h(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x02dd, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.j(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x02ec, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.k(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x02fb, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.g(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0306, code lost:
        r5 = com.fossil.blesdk.obfuscated.hx0.f(r1, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x030a, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.c(r3, (com.google.android.gms.internal.clearcut.zzbb) r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x0317, code lost:
        r3 = com.fossil.blesdk.obfuscated.lw0.a(r3, com.fossil.blesdk.obfuscated.hx0.f(r1, r5), a(r12));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0331, code lost:
        if ((r5 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L_0x030a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0334, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.b(r3, (java.lang.String) r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0342, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.b(r3, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x034e, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.i(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x035a, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.g(r3, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x036a, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.f(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x037a, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.e(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x038a, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.d(r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0396, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.b(r3, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x03a2, code lost:
        r3 = com.google.android.gms.internal.clearcut.zzbn.b(r3, 0.0d);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x03aa, code lost:
        r12 = r12 + 4;
        r3 = 267386880;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0417, code lost:
        if (a(r1, r15, r3) != false) goto L_0x06b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0437, code lost:
        if (a(r1, r15, r3) != false) goto L_0x06e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x043f, code lost:
        if (a(r1, r15, r3) != false) goto L_0x06f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x045f, code lost:
        if (a(r1, r15, r3) != false) goto L_0x0716;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x0467, code lost:
        if (a(r1, r15, r3) != false) goto L_0x0725;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x0477, code lost:
        if ((r4 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L_0x071a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x047f, code lost:
        if (a(r1, r15, r3) != false) goto L_0x074c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x0517, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x0529, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x053b, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x054d, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x055f, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x0571, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:297:0x0583, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x0595, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x05a6, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x05b7, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x05c8, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:317:0x05d9, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:321:0x05ea, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:0x05fb, code lost:
        if (r0.i != false) goto L_0x05fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:326:0x05fd, code lost:
        r2.putInt(r1, (long) r11, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:327:0x0601, code lost:
        r9 = (com.google.android.gms.internal.clearcut.zzbn.e(r15) + com.google.android.gms.internal.clearcut.zzbn.g(r4)) + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:341:0x06ac, code lost:
        r5 = r5 + r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:343:0x06ae, code lost:
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:345:0x06b7, code lost:
        if ((r12 & r18) != 0) goto L_0x06b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:346:0x06b9, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.c(r15, (com.fossil.blesdk.obfuscated.sv0) r2.getObject(r1, r9), a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:0x06d0, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.f(r15, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:0x06dd, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.h(r15, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:0x06e4, code lost:
        if ((r12 & r18) != 0) goto L_0x06e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x06e6, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.h(r15, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x06ef, code lost:
        if ((r12 & r18) != 0) goto L_0x06f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:0x06f1, code lost:
        r9 = com.google.android.gms.internal.clearcut.zzbn.j(r15, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:0x06f6, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:365:0x0700, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.k(r15, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x070d, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.g(r15, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:371:0x0714, code lost:
        if ((r12 & r18) != 0) goto L_0x0716;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:372:0x0716, code lost:
        r4 = r2.getObject(r1, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:373:0x071a, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.c(r15, (com.google.android.gms.internal.clearcut.zzbb) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:375:0x0723, code lost:
        if ((r12 & r18) != 0) goto L_0x0725;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x0725, code lost:
        r4 = com.fossil.blesdk.obfuscated.lw0.a(r15, r2.getObject(r1, r9), a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ab, code lost:
        if ((r5 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L_0x030a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:380:0x073d, code lost:
        if ((r4 instanceof com.google.android.gms.internal.clearcut.zzbb) != false) goto L_0x071a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:381:0x0740, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.b(r15, (java.lang.String) r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:383:0x074a, code lost:
        if ((r12 & r18) != 0) goto L_0x074c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:384:0x074c, code lost:
        r4 = com.google.android.gms.internal.clearcut.zzbn.b(r15, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:0x0799, code lost:
        r5 = r5 + r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:409:0x07bb, code lost:
        r3 = r3 + 4;
        r9 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0127, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0139, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x014b, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x015d, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x016f, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0181, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0193, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01a5, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01b6, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01c7, code lost:
        if (r0.i != false) goto L_0x020d;
     */
    @DexIgnore
    public final int b(T t) {
        int i2;
        int i3;
        long j2;
        int i4;
        int b2;
        Object obj;
        int i5;
        int i6;
        int i7;
        int i8;
        long j3;
        int i9;
        int b3;
        long j4;
        long j5;
        int i10;
        Object obj2;
        int i11;
        int i12;
        int i13;
        long j6;
        int i14;
        T t2 = t;
        int i15 = 267386880;
        if (this.h) {
            Unsafe unsafe = r;
            int i16 = 0;
            int i17 = 0;
            while (i16 < this.a.length) {
                int d2 = d(i16);
                int i18 = (d2 & i15) >>> 20;
                int i19 = this.a[i16];
                long j7 = (long) (d2 & 1048575);
                int i20 = (i18 < zzcb.DOUBLE_LIST_PACKED.id() || i18 > zzcb.SINT64_LIST_PACKED.id()) ? 0 : this.a[i16 + 2] & 1048575;
                switch (i18) {
                    case 0:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 1:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 2:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            j4 = hx0.b(t2, j7);
                            break;
                        }
                    case 3:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            j5 = hx0.b(t2, j7);
                            break;
                        }
                    case 4:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            i10 = hx0.a((Object) t2, j7);
                            break;
                        }
                    case 5:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 6:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 7:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 8:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            obj2 = hx0.f(t2, j7);
                            break;
                        }
                    case 9:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 10:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 11:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            i11 = hx0.a((Object) t2, j7);
                            break;
                        }
                    case 12:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            i12 = hx0.a((Object) t2, j7);
                            break;
                        }
                    case 13:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 14:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 15:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            i13 = hx0.a((Object) t2, j7);
                            break;
                        }
                    case 16:
                        if (!a(t2, i16)) {
                            break;
                        } else {
                            j6 = hx0.b(t2, j7);
                            break;
                        }
                    case 17:
                        if (!a(t2, i16)) {
                            break;
                        }
                        break;
                    case 18:
                    case 23:
                    case 32:
                        b3 = lw0.i(i19, a((Object) t2, j7), false);
                        break;
                    case 19:
                    case 24:
                    case 31:
                        b3 = lw0.h(i19, a((Object) t2, j7), false);
                        break;
                    case 20:
                        b3 = lw0.a(i19, (List<Long>) a((Object) t2, j7), false);
                        break;
                    case 21:
                        b3 = lw0.b(i19, (List<Long>) a((Object) t2, j7), false);
                        break;
                    case 22:
                        b3 = lw0.e(i19, a((Object) t2, j7), false);
                        break;
                    case 25:
                        b3 = lw0.j(i19, a((Object) t2, j7), false);
                        break;
                    case 26:
                        b3 = lw0.a(i19, (List<?>) a((Object) t2, j7));
                        break;
                    case 27:
                        b3 = lw0.a(i19, (List<?>) a((Object) t2, j7), a(i16));
                        break;
                    case 28:
                        b3 = lw0.b(i19, a((Object) t2, j7));
                        break;
                    case 29:
                        b3 = lw0.f(i19, a((Object) t2, j7), false);
                        break;
                    case 30:
                        b3 = lw0.d(i19, a((Object) t2, j7), false);
                        break;
                    case 33:
                        b3 = lw0.g(i19, a((Object) t2, j7), false);
                        break;
                    case 34:
                        b3 = lw0.c(i19, a((Object) t2, j7), false);
                        break;
                    case 35:
                        i14 = lw0.i((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        i14 = lw0.h((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        i14 = lw0.a((List<Long>) (List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        i14 = lw0.b((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        i14 = lw0.e((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        i14 = lw0.i((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        i14 = lw0.h((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        i14 = lw0.j((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        i14 = lw0.f((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        i14 = lw0.d((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        i14 = lw0.h((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        i14 = lw0.i((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        i14 = lw0.g((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        i14 = lw0.c((List) unsafe.getObject(t2, j7));
                        if (i14 > 0) {
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        b3 = lw0.b(i19, (List<sv0>) a((Object) t2, j7), a(i16));
                        break;
                    case 50:
                        b3 = this.q.a(i19, hx0.f(t2, j7), b(i16));
                        break;
                    case 51:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 52:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 53:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            j4 = e(t2, j7);
                            break;
                        }
                    case 54:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            j5 = e(t2, j7);
                            break;
                        }
                    case 55:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            i10 = d(t2, j7);
                            break;
                        }
                    case 56:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 57:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 58:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 59:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            obj2 = hx0.f(t2, j7);
                            break;
                        }
                    case 60:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 61:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 62:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            i11 = d(t2, j7);
                            break;
                        }
                    case 63:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            i12 = d(t2, j7);
                            break;
                        }
                    case 64:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 65:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                    case 66:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            i13 = d(t2, j7);
                            break;
                        }
                    case 67:
                        if (!a(t2, i19, i16)) {
                            break;
                        } else {
                            j6 = e(t2, j7);
                            break;
                        }
                    case 68:
                        if (!a(t2, i19, i16)) {
                            break;
                        }
                        break;
                }
            }
            return i17 + a(this.o, t2);
        }
        Unsafe unsafe2 = r;
        int i21 = 0;
        int i22 = 0;
        int i23 = -1;
        int i24 = 0;
        while (i21 < this.a.length) {
            int d3 = d(i21);
            int[] iArr = this.a;
            int i25 = iArr[i21];
            int i26 = (d3 & 267386880) >>> 20;
            if (i26 <= 17) {
                i3 = iArr[i21 + 2];
                int i27 = i3 & 1048575;
                i2 = 1 << (i3 >>> 20);
                if (i27 != i23) {
                    i24 = unsafe2.getInt(t2, (long) i27);
                } else {
                    i27 = i23;
                }
                i23 = i27;
            } else {
                i3 = (!this.i || i26 < zzcb.DOUBLE_LIST_PACKED.id() || i26 > zzcb.SINT64_LIST_PACKED.id()) ? 0 : this.a[i21 + 2] & 1048575;
                i2 = 0;
            }
            long j8 = (long) (d3 & 1048575);
            switch (i26) {
                case 0:
                    j2 = 0;
                    if ((i24 & i2) != 0) {
                        i22 += zzbn.b(i25, 0.0d);
                        break;
                    }
                case 1:
                    j2 = 0;
                    if ((i24 & i2) != 0) {
                        i22 += zzbn.b(i25, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                        break;
                    }
                case 2:
                    j2 = 0;
                    if ((i24 & i2) != 0) {
                        i4 = zzbn.d(i25, unsafe2.getLong(t2, j8));
                    }
                    break;
                case 3:
                    j2 = 0;
                    if ((i24 & i2) != 0) {
                        i4 = zzbn.e(i25, unsafe2.getLong(t2, j8));
                    }
                    break;
                case 4:
                    j2 = 0;
                    if ((i24 & i2) != 0) {
                        i4 = zzbn.f(i25, unsafe2.getInt(t2, j8));
                    }
                    break;
                case 5:
                    j2 = 0;
                    if ((i24 & i2) != 0) {
                        i4 = zzbn.g(i25, 0);
                    }
                    break;
                case 6:
                    if ((i24 & i2) != 0) {
                        i22 += zzbn.i(i25, 0);
                        break;
                    }
                case 7:
                    break;
                case 8:
                    if ((i24 & i2) != 0) {
                        obj = unsafe2.getObject(t2, j8);
                        break;
                    }
                    break;
                case 9:
                    break;
                case 10:
                    break;
                case 11:
                    if ((i24 & i2) != 0) {
                        i5 = unsafe2.getInt(t2, j8);
                        break;
                    }
                    break;
                case 12:
                    if ((i24 & i2) != 0) {
                        i6 = unsafe2.getInt(t2, j8);
                        break;
                    }
                    break;
                case 13:
                    break;
                case 14:
                    break;
                case 15:
                    if ((i24 & i2) != 0) {
                        i8 = unsafe2.getInt(t2, j8);
                        break;
                    }
                    break;
                case 16:
                    if ((i24 & i2) != 0) {
                        j3 = unsafe2.getLong(t2, j8);
                        break;
                    }
                    break;
                case 17:
                    break;
                case 18:
                case 23:
                case 32:
                    b2 = lw0.i(i25, (List) unsafe2.getObject(t2, j8), false);
                case 19:
                case 24:
                case 31:
                    b2 = lw0.h(i25, (List) unsafe2.getObject(t2, j8), false);
                case 20:
                    b2 = lw0.a(i25, (List<Long>) (List) unsafe2.getObject(t2, j8), false);
                case 21:
                    b2 = lw0.b(i25, (List<Long>) (List) unsafe2.getObject(t2, j8), false);
                case 22:
                    b2 = lw0.e(i25, (List) unsafe2.getObject(t2, j8), false);
                case 25:
                    b2 = lw0.j(i25, (List) unsafe2.getObject(t2, j8), false);
                case 26:
                    b2 = lw0.a(i25, (List<?>) (List) unsafe2.getObject(t2, j8));
                case 27:
                    b2 = lw0.a(i25, (List<?>) (List) unsafe2.getObject(t2, j8), a(i21));
                case 28:
                    b2 = lw0.b(i25, (List) unsafe2.getObject(t2, j8));
                case 29:
                    b2 = lw0.f(i25, (List) unsafe2.getObject(t2, j8), false);
                case 30:
                    b2 = lw0.d(i25, (List) unsafe2.getObject(t2, j8), false);
                case 33:
                    b2 = lw0.g(i25, (List) unsafe2.getObject(t2, j8), false);
                case 34:
                    b2 = lw0.c(i25, (List) unsafe2.getObject(t2, j8), false);
                case 35:
                    i9 = lw0.i((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 36:
                    i9 = lw0.h((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 37:
                    i9 = lw0.a((List<Long>) (List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 38:
                    i9 = lw0.b((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 39:
                    i9 = lw0.e((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 40:
                    i9 = lw0.i((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 41:
                    i9 = lw0.h((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 42:
                    i9 = lw0.j((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 43:
                    i9 = lw0.f((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 44:
                    i9 = lw0.d((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 45:
                    i9 = lw0.h((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 46:
                    i9 = lw0.i((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 47:
                    i9 = lw0.g((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 48:
                    i9 = lw0.c((List) unsafe2.getObject(t2, j8));
                    if (i9 > 0) {
                        break;
                    }
                    break;
                case 49:
                    b2 = lw0.b(i25, (List<sv0>) (List) unsafe2.getObject(t2, j8), a(i21));
                case 50:
                    b2 = this.q.a(i25, unsafe2.getObject(t2, j8), b(i21));
                case 51:
                    if (a(t2, i25, i21)) {
                        b2 = zzbn.b(i25, 0.0d);
                    }
                    break;
                case 52:
                    if (a(t2, i25, i21)) {
                        i7 = zzbn.b(i25, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    }
                    break;
                case 53:
                    if (a(t2, i25, i21)) {
                        b2 = zzbn.d(i25, e(t2, j8));
                    }
                    break;
                case 54:
                    if (a(t2, i25, i21)) {
                        b2 = zzbn.e(i25, e(t2, j8));
                    }
                    break;
                case 55:
                    if (a(t2, i25, i21)) {
                        b2 = zzbn.f(i25, d(t2, j8));
                    }
                    break;
                case 56:
                    if (a(t2, i25, i21)) {
                        b2 = zzbn.g(i25, 0);
                    }
                    break;
                case 57:
                    if (a(t2, i25, i21)) {
                        i7 = zzbn.i(i25, 0);
                    }
                    break;
                case 58:
                    break;
                case 59:
                    if (a(t2, i25, i21)) {
                        obj = unsafe2.getObject(t2, j8);
                        break;
                    }
                    break;
                case 60:
                    break;
                case 61:
                    break;
                case 62:
                    if (a(t2, i25, i21)) {
                        i5 = d(t2, j8);
                        break;
                    }
                    break;
                case 63:
                    if (a(t2, i25, i21)) {
                        i6 = d(t2, j8);
                        break;
                    }
                    break;
                case 64:
                    break;
                case 65:
                    break;
                case 66:
                    if (a(t2, i25, i21)) {
                        i8 = d(t2, j8);
                        break;
                    }
                    break;
                case 67:
                    if (a(t2, i25, i21)) {
                        j3 = e(t2, j8);
                        break;
                    }
                    break;
                case 68:
                    break;
            }
        }
        int a2 = i22 + a(this.o, t2);
        return this.g ? a2 + this.p.a((Object) t2).f() : a2;
    }

    @DexIgnore
    public final Object b(int i2) {
        return this.b[(i2 / 4) << 1];
    }

    @DexIgnore
    public final void b(T t, int i2) {
        if (!this.h) {
            int e2 = e(i2);
            long j2 = (long) (e2 & 1048575);
            hx0.a((Object) t, j2, hx0.a((Object) t, j2) | (1 << (e2 >>> 20)));
        }
    }

    @DexIgnore
    public final void b(T t, int i2, int i3) {
        hx0.a((Object) t, (long) (e(i3) & 1048575), i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x027e, code lost:
        com.fossil.blesdk.obfuscated.lw0.j(r4, (java.util.List) r8.getObject(r1, r12), r2, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x028e, code lost:
        com.fossil.blesdk.obfuscated.lw0.g(r4, (java.util.List) r8.getObject(r1, r12), r2, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x029e, code lost:
        com.fossil.blesdk.obfuscated.lw0.l(r4, (java.util.List) r8.getObject(r1, r12), r2, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02ae, code lost:
        com.fossil.blesdk.obfuscated.lw0.m(r4, (java.util.List) r8.getObject(r1, r12), r2, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x02be, code lost:
        com.fossil.blesdk.obfuscated.lw0.i(r4, (java.util.List) r8.getObject(r1, r12), r2, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0477, code lost:
        r5 = r5 + 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0274, code lost:
        com.fossil.blesdk.obfuscated.lw0.e(r4, r9, r2, r14);
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0481  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    public final void b(T t, ox0 ox0) throws IOException {
        Map.Entry entry;
        Iterator<Map.Entry<?, Object>> it;
        int length;
        int i2;
        Map.Entry entry2;
        int i3;
        boolean z;
        int i4;
        boolean z2;
        int i5;
        boolean z3;
        int i6;
        boolean z4;
        int i7;
        boolean z5;
        int i8;
        boolean z6;
        List list;
        int i9;
        T t2 = t;
        ox0 ox02 = ox0;
        if (this.g) {
            ku0<?> a2 = this.p.a((Object) t2);
            if (!a2.b()) {
                it = a2.e();
                entry = it.next();
                int i10 = -1;
                length = this.a.length;
                Unsafe unsafe = r;
                Map.Entry entry3 = entry;
                i2 = 0;
                int i11 = 0;
                while (i2 < length) {
                    int d2 = d(i2);
                    int[] iArr = this.a;
                    int i12 = iArr[i2];
                    int i13 = (267386880 & d2) >>> 20;
                    if (this.h || i13 > 17) {
                        entry3 = entry3;
                        i3 = 0;
                    } else {
                        int i14 = iArr[i2 + 2];
                        int i15 = i14 & 1048575;
                        Map.Entry entry4 = entry3;
                        if (i15 != i10) {
                            i11 = unsafe.getInt(t2, (long) i15);
                        } else {
                            i15 = i10;
                        }
                        i3 = 1 << (i14 >>> 20);
                        i10 = i15;
                        entry3 = entry4;
                    }
                    while (entry3 != null && this.p.a((Map.Entry<?, ?>) entry3) <= i12) {
                        this.p.a(ox02, entry3);
                        entry3 = it.hasNext() ? it.next() : null;
                    }
                    long j2 = (long) (d2 & 1048575);
                    switch (i13) {
                        case 0:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zza(i12, hx0.e(t2, j2));
                                break;
                            }
                        case 1:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zza(i12, hx0.d(t2, j2));
                                break;
                            }
                        case 2:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.b(i12, unsafe.getLong(t2, j2));
                                break;
                            }
                        case 3:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zza(i12, unsafe.getLong(t2, j2));
                                break;
                            }
                        case 4:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zzc(i12, unsafe.getInt(t2, j2));
                                break;
                            }
                        case 5:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zzc(i12, unsafe.getLong(t2, j2));
                                break;
                            }
                        case 6:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zzf(i12, unsafe.getInt(t2, j2));
                                break;
                            }
                        case 7:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.a(i12, hx0.c(t2, j2));
                                break;
                            }
                        case 8:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                a(i12, unsafe.getObject(t2, j2), ox02);
                                break;
                            }
                        case 9:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.a(i12, unsafe.getObject(t2, j2), a(i2));
                                break;
                            }
                        case 10:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.a(i12, (zzbb) unsafe.getObject(t2, j2));
                                break;
                            }
                        case 11:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zzd(i12, unsafe.getInt(t2, j2));
                                break;
                            }
                        case 12:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.a(i12, unsafe.getInt(t2, j2));
                                break;
                            }
                        case 13:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.b(i12, unsafe.getInt(t2, j2));
                                break;
                            }
                        case 14:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.a(i12, unsafe.getLong(t2, j2));
                                break;
                            }
                        case 15:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zze(i12, unsafe.getInt(t2, j2));
                                break;
                            }
                        case 16:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.zzb(i12, unsafe.getLong(t2, j2));
                                break;
                            }
                        case 17:
                            if ((i11 & i3) == 0) {
                                break;
                            } else {
                                ox02.b(i12, unsafe.getObject(t2, j2), a(i2));
                                break;
                            }
                        case 18:
                            lw0.a(this.a[i2], (List<Double>) (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 19:
                            lw0.b(this.a[i2], (List<Float>) (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 20:
                            lw0.c(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 21:
                            lw0.d(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 22:
                            lw0.h(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 23:
                            lw0.f(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 24:
                            lw0.k(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 25:
                            lw0.n(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, false);
                            break;
                        case 26:
                            lw0.a(this.a[i2], (List<String>) (List) unsafe.getObject(t2, j2), ox02);
                            break;
                        case 27:
                            lw0.a(this.a[i2], (List<?>) (List) unsafe.getObject(t2, j2), ox02, a(i2));
                            break;
                        case 28:
                            lw0.b(this.a[i2], (List<zzbb>) (List) unsafe.getObject(t2, j2), ox02);
                            break;
                        case 29:
                            z = false;
                            i4 = this.a[i2];
                            break;
                        case 30:
                            z2 = false;
                            i5 = this.a[i2];
                            break;
                        case 31:
                            z3 = false;
                            i6 = this.a[i2];
                            break;
                        case 32:
                            z4 = false;
                            i7 = this.a[i2];
                            break;
                        case 33:
                            z5 = false;
                            i8 = this.a[i2];
                            break;
                        case 34:
                            i9 = this.a[i2];
                            list = (List) unsafe.getObject(t2, j2);
                            z6 = false;
                            break;
                        case 35:
                            lw0.a(this.a[i2], (List<Double>) (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 36:
                            lw0.b(this.a[i2], (List<Float>) (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 37:
                            lw0.c(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 38:
                            lw0.d(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 39:
                            lw0.h(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 40:
                            lw0.f(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 41:
                            lw0.k(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 42:
                            lw0.n(this.a[i2], (List) unsafe.getObject(t2, j2), ox02, true);
                            break;
                        case 43:
                            z = true;
                            i4 = this.a[i2];
                            break;
                        case 44:
                            z2 = true;
                            i5 = this.a[i2];
                            break;
                        case 45:
                            z3 = true;
                            i6 = this.a[i2];
                            break;
                        case 46:
                            z4 = true;
                            i7 = this.a[i2];
                            break;
                        case 47:
                            z5 = true;
                            i8 = this.a[i2];
                            break;
                        case 48:
                            i9 = this.a[i2];
                            list = (List) unsafe.getObject(t2, j2);
                            z6 = true;
                            break;
                        case 49:
                            lw0.b(this.a[i2], (List<?>) (List) unsafe.getObject(t2, j2), ox02, a(i2));
                            break;
                        case 50:
                            a(ox02, i12, unsafe.getObject(t2, j2), i2);
                            break;
                        case 51:
                            if (a(t2, i12, i2)) {
                                ox02.zza(i12, b(t2, j2));
                                break;
                            }
                            break;
                        case 52:
                            if (a(t2, i12, i2)) {
                                ox02.zza(i12, c(t2, j2));
                                break;
                            }
                            break;
                        case 53:
                            if (a(t2, i12, i2)) {
                                ox02.b(i12, e(t2, j2));
                                break;
                            }
                            break;
                        case 54:
                            if (a(t2, i12, i2)) {
                                ox02.zza(i12, e(t2, j2));
                                break;
                            }
                            break;
                        case 55:
                            if (a(t2, i12, i2)) {
                                ox02.zzc(i12, d(t2, j2));
                                break;
                            }
                            break;
                        case 56:
                            if (a(t2, i12, i2)) {
                                ox02.zzc(i12, e(t2, j2));
                                break;
                            }
                            break;
                        case 57:
                            if (a(t2, i12, i2)) {
                                ox02.zzf(i12, d(t2, j2));
                                break;
                            }
                            break;
                        case 58:
                            if (a(t2, i12, i2)) {
                                ox02.a(i12, f(t2, j2));
                                break;
                            }
                            break;
                        case 59:
                            if (a(t2, i12, i2)) {
                                a(i12, unsafe.getObject(t2, j2), ox02);
                                break;
                            }
                            break;
                        case 60:
                            if (a(t2, i12, i2)) {
                                ox02.a(i12, unsafe.getObject(t2, j2), a(i2));
                                break;
                            }
                            break;
                        case 61:
                            if (a(t2, i12, i2)) {
                                ox02.a(i12, (zzbb) unsafe.getObject(t2, j2));
                                break;
                            }
                            break;
                        case 62:
                            if (a(t2, i12, i2)) {
                                ox02.zzd(i12, d(t2, j2));
                                break;
                            }
                            break;
                        case 63:
                            if (a(t2, i12, i2)) {
                                ox02.a(i12, d(t2, j2));
                                break;
                            }
                            break;
                        case 64:
                            if (a(t2, i12, i2)) {
                                ox02.b(i12, d(t2, j2));
                                break;
                            }
                            break;
                        case 65:
                            if (a(t2, i12, i2)) {
                                ox02.a(i12, e(t2, j2));
                                break;
                            }
                            break;
                        case 66:
                            if (a(t2, i12, i2)) {
                                ox02.zze(i12, d(t2, j2));
                                break;
                            }
                            break;
                        case 67:
                            if (a(t2, i12, i2)) {
                                ox02.zzb(i12, e(t2, j2));
                                break;
                            }
                            break;
                        case 68:
                            if (a(t2, i12, i2)) {
                                ox02.b(i12, unsafe.getObject(t2, j2), a(i2));
                                break;
                            }
                            break;
                    }
                }
                for (entry2 = entry3; entry2 != null; entry2 = it.hasNext() ? it.next() : null) {
                    this.p.a(ox02, entry2);
                }
                a(this.o, t2, ox02);
            }
        }
        it = null;
        entry = null;
        int i102 = -1;
        length = this.a.length;
        Unsafe unsafe2 = r;
        Map.Entry entry32 = entry;
        i2 = 0;
        int i112 = 0;
        while (i2 < length) {
        }
        while (entry2 != null) {
        }
        a(this.o, t2, ox02);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r7, r2, com.fossil.blesdk.obfuscated.hx0.f(r8, r2));
        b(r7, r4, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0089, code lost:
        com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r7, r2, com.fossil.blesdk.obfuscated.hx0.f(r8, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b3, code lost:
        com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r7, r2, com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r8, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c8, code lost:
        com.fossil.blesdk.obfuscated.hx0.a((java.lang.Object) r7, r2, com.fossil.blesdk.obfuscated.hx0.b(r8, r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00eb, code lost:
        b(r7, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ee, code lost:
        r0 = r0 + 4;
     */
    @DexIgnore
    public final void b(T t, T t2) {
        if (t2 != null) {
            int i2 = 0;
            while (i2 < this.a.length) {
                int d2 = d(i2);
                long j2 = (long) (1048575 & d2);
                int i3 = this.a[i2];
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            hx0.a((Object) t, j2, hx0.e(t2, j2));
                        }
                    case 1:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            hx0.a((Object) t, j2, hx0.d(t2, j2));
                        }
                    case 2:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 3:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 4:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 5:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 6:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 7:
                        if (!a(t2, i2)) {
                            break;
                        } else {
                            hx0.a((Object) t, j2, hx0.c(t2, j2));
                        }
                    case 8:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 9:
                    case 17:
                        a(t, t2, i2);
                        break;
                    case 10:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 11:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 12:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 13:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 14:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 15:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 16:
                        if (!a(t2, i2)) {
                            break;
                        }
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.n.a(t, t2, j2);
                        break;
                    case 50:
                        lw0.a(this.q, t, t2, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!a(t2, i3, i2)) {
                            break;
                        }
                    case 60:
                    case 68:
                        b(t, t2, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!a(t2, i3, i2)) {
                            break;
                        }
                }
            }
            if (!this.h) {
                lw0.a(this.o, t, t2);
                if (this.g) {
                    lw0.a(this.p, t, t2);
                    return;
                }
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(T t, T t2, int i2) {
        int d2 = d(i2);
        int i3 = this.a[i2];
        long j2 = (long) (d2 & 1048575);
        if (a(t2, i3, i2)) {
            Object f2 = hx0.f(t, j2);
            Object f3 = hx0.f(t2, j2);
            if (f2 != null && f3 != null) {
                hx0.a((Object) t, j2, tu0.a(f2, f3));
                b(t, i3, i2);
            } else if (f3 != null) {
                hx0.a((Object) t, j2, f3);
                b(t, i3, i2);
            }
        }
    }

    @DexIgnore
    public final vu0<?> c(int i2) {
        return (vu0) this.b[((i2 / 4) << 1) + 1];
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00d4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00e6 A[SYNTHETIC] */
    public final boolean c(T t) {
        int i2;
        int i3;
        boolean z;
        T t2 = t;
        int[] iArr = this.j;
        int i4 = 1;
        if (iArr == null || iArr.length == 0) {
            return true;
        }
        int length = iArr.length;
        int i5 = 0;
        int i6 = -1;
        int i7 = 0;
        while (i5 < length) {
            int i8 = iArr[i5];
            int f2 = f(i8);
            int d2 = d(f2);
            if (!this.h) {
                int i9 = this.a[f2 + 2];
                int i10 = i9 & 1048575;
                i3 = i4 << (i9 >>> 20);
                if (i10 != i6) {
                    i2 = i5;
                    i7 = r.getInt(t2, (long) i10);
                    i6 = i10;
                } else {
                    i2 = i5;
                }
            } else {
                i2 = i5;
                i3 = 0;
            }
            if (((268435456 & d2) != 0) && !a(t2, f2, i7, i3)) {
                return false;
            }
            int i11 = (267386880 & d2) >>> 20;
            if (i11 != 9 && i11 != 17) {
                if (i11 != 27) {
                    if (i11 == 60 || i11 == 68) {
                        if (a(t2, i8, f2) && !a((Object) t2, d2, a(f2))) {
                            return false;
                        }
                    } else if (i11 != 49) {
                        if (i11 == 50 && !this.q.e(hx0.f(t2, (long) (d2 & 1048575))).isEmpty()) {
                            this.q.a(b(f2));
                            throw null;
                        }
                    }
                }
                List list = (List) hx0.f(t2, (long) (d2 & 1048575));
                if (!list.isEmpty()) {
                    jw0 a2 = a(f2);
                    int i12 = 0;
                    while (true) {
                        if (i12 >= list.size()) {
                            break;
                        } else if (!a2.c(list.get(i12))) {
                            z = false;
                            break;
                        } else {
                            i12++;
                        }
                    }
                    if (z) {
                        return false;
                    }
                }
                z = true;
                if (z) {
                }
            } else if (a(t2, f2, i7, i3) && !a((Object) t2, d2, a(f2))) {
                return false;
            }
            i5 = i2 + 1;
            i4 = 1;
        }
        return !this.g || this.p.a((Object) t2).d();
    }

    @DexIgnore
    public final boolean c(T t, T t2, int i2) {
        return a(t, i2) == a(t2, i2);
    }

    @DexIgnore
    public final int d(int i2) {
        return this.a[i2 + 1];
    }

    @DexIgnore
    public final int e(int i2) {
        return this.a[i2 + 2];
    }

    @DexIgnore
    public final int f(int i2) {
        int i3 = this.c;
        if (i2 >= i3) {
            int i4 = this.e;
            if (i2 < i4) {
                int i5 = (i2 - i3) << 2;
                if (this.a[i5] == i2) {
                    return i5;
                }
                return -1;
            } else if (i2 <= this.d) {
                int i6 = i4 - i3;
                int length = (this.a.length / 4) - 1;
                while (i6 <= length) {
                    int i7 = (length + i6) >>> 1;
                    int i8 = i7 << 2;
                    int i9 = this.a[i8];
                    if (i2 == i9) {
                        return i8;
                    }
                    if (i2 < i9) {
                        length = i7 - 1;
                    } else {
                        i6 = i7 + 1;
                    }
                }
            }
        }
        return -1;
    }

    @DexIgnore
    public final void zzc(T t) {
        int[] iArr = this.k;
        if (iArr != null) {
            for (int d2 : iArr) {
                long d3 = (long) (d(d2) & 1048575);
                Object f2 = hx0.f(t, d3);
                if (f2 != null) {
                    this.q.c(f2);
                    hx0.a((Object) t, d3, f2);
                }
            }
        }
        int[] iArr2 = this.l;
        if (iArr2 != null) {
            for (int i2 : iArr2) {
                this.n.a(t, (long) i2);
            }
        }
        this.o.a(t);
        if (this.g) {
            this.p.c(t);
        }
    }
}
