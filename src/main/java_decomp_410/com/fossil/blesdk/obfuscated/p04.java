package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class p04 extends o04 {
    @DexIgnore
    public static /* final */ k04 m;

    /*
    static {
        k04 k04 = new k04();
        m = k04;
        k04.a("A9VH9B8L4GX4");
    }
    */

    @DexIgnore
    public p04(Context context) {
        super(context, 0, m);
    }

    @DexIgnore
    public f a() {
        return f.NETWORK_DETECTOR;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        j24.a(jSONObject, "actky", h04.b(this.j));
        return true;
    }
}
