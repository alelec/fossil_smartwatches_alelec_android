package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jd2 extends id2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F; // = new SparseIntArray();
    @DexIgnore
    public long D;

    /*
    static {
        F.put(R.id.tv_preset_name, 1);
        F.put(R.id.tv_cancel, 2);
        F.put(R.id.ftv_set_to_watch, 3);
        F.put(R.id.cl_complications, 4);
        F.put(R.id.line_center, 5);
        F.put(R.id.line_top, 6);
        F.put(R.id.line_bottom, 7);
        F.put(R.id.iv_watch_theme_background, 8);
        F.put(R.id.circle, 9);
        F.put(R.id.guideline, 10);
        F.put(R.id.cl_micro_apps, 11);
        F.put(R.id.wa_top, 12);
        F.put(R.id.tv_wa_top, 13);
        F.put(R.id.wa_middle, 14);
        F.put(R.id.tv_wa_middle, 15);
        F.put(R.id.wa_bottom, 16);
        F.put(R.id.tv_wa_bottom, 17);
        F.put(R.id.v_align, 18);
        F.put(R.id.cv_group, 19);
        F.put(R.id.rv_preset, 20);
    }
    */

    @DexIgnore
    public jd2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 21, E, F));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public jd2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[9], objArr[4], objArr[0], objArr[11], objArr[19], objArr[3], objArr[10], objArr[8], objArr[7], objArr[5], objArr[6], objArr[20], objArr[2], objArr[1], objArr[17], objArr[15], objArr[13], objArr[18], objArr[16], objArr[14], objArr[12]);
        this.D = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
