package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nm3 extends ym3 implements en3 {
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public tr3<se2> k;
    @DexIgnore
    public dn3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final nm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            nm3 nm3 = new nm3();
            nm3.setArguments(bundle);
            return nm3;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ FragmentActivity a;

        @DexIgnore
        public b(FragmentActivity fragmentActivity) {
            this.a = fragmentActivity;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.a, R.anim.move_out);
            FlexibleTextView flexibleTextView = (FlexibleTextView) this.a.findViewById(R.id.ftv_title);
            if (flexibleTextView != null) {
                flexibleTextView.startAnimation(loadAnimation);
            }
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) this.a.findViewById(R.id.ftv_desc);
            if (flexibleTextView2 != null) {
                flexibleTextView2.startAnimation(loadAnimation);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nm3 e;

        @DexIgnore
        public c(nm3 nm3) {
            this.e = nm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            nm3.a(this.e).a(1);
        }
    }

    @DexIgnore
    public static final /* synthetic */ dn3 a(nm3 nm3) {
        dn3 dn3 = nm3.l;
        if (dn3 != null) {
            return dn3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public Animation onCreateAnimation(int i, boolean z, int i2) {
        if (!z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Animation loadAnimation = AnimationUtils.loadAnimation(activity, R.anim.fragment_out);
                if (loadAnimation == null) {
                    return loadAnimation;
                }
                loadAnimation.setAnimationListener(new b(activity));
                return loadAnimation;
            }
        }
        return null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        se2 se2 = (se2) qa.a(layoutInflater, R.layout.fragment_pairing_look_for_device, viewGroup, false, O0());
        this.k = new tr3<>(this, se2);
        kd4.a((Object) se2, "binding");
        return se2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<se2> tr3 = this.k;
        if (tr3 != null) {
            se2 a2 = tr3.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.r;
                if (rTLImageView != null) {
                    rTLImageView.setOnClickListener(new c(this));
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                tr3<se2> tr32 = this.k;
                if (tr32 != null) {
                    se2 a3 = tr32.a();
                    if (a3 != null) {
                        DashBar dashBar = a3.s;
                        if (dashBar != null) {
                            gs3.a aVar = gs3.a;
                            kd4.a((Object) dashBar, "this");
                            aVar.d(dashBar, z, 500);
                            return;
                        }
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(dn3 dn3) {
        kd4.b(dn3, "presenter");
        this.l = dn3;
    }
}
