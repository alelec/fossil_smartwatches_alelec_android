package com.fossil.blesdk.obfuscated;

import java.util.List;
import java.util.Map;
import kotlin.reflect.KVisibility;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ge4<R> extends fe4 {
    @DexIgnore
    R call(Object... objArr);

    @DexIgnore
    R callBy(Map<Object, ? extends Object> map);

    @DexIgnore
    List<Object> getParameters();

    @DexIgnore
    ne4 getReturnType();

    @DexIgnore
    List<Object> getTypeParameters();

    @DexIgnore
    KVisibility getVisibility();

    @DexIgnore
    boolean isAbstract();

    @DexIgnore
    boolean isFinal();

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    boolean isSuspend();
}
