package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ig */
public interface C2019ig extends java.io.Closeable {
    @DexIgnore
    /* renamed from: a */
    void mo11930a(int i);

    @DexIgnore
    /* renamed from: a */
    void mo11931a(int i, double d);

    @DexIgnore
    /* renamed from: a */
    void mo11932a(int i, java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    void mo11933a(int i, byte[] bArr);

    @DexIgnore
    /* renamed from: b */
    void mo11934b(int i, long j);
}
