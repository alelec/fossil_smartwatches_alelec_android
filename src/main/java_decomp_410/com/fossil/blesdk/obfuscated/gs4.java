package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gs4 implements gr4<em4, Byte> {
    @DexIgnore
    public static /* final */ gs4 a; // = new gs4();

    @DexIgnore
    public Byte a(em4 em4) throws IOException {
        return Byte.valueOf(em4.F());
    }
}
