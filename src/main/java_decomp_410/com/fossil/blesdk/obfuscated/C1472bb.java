package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bb */
public abstract class C1472bb {
    @DexIgnore
    /* renamed from: a */
    public abstract int mo9123a();

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9124a(int i, androidx.fragment.app.Fragment fragment);

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9125a(int i, androidx.fragment.app.Fragment fragment, java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9126a(androidx.fragment.app.Fragment fragment);

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9127a(androidx.fragment.app.Fragment fragment, java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9128a(java.lang.String str);

    @DexIgnore
    /* renamed from: b */
    public abstract int mo9129b();

    @DexIgnore
    /* renamed from: b */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9130b(int i, androidx.fragment.app.Fragment fragment, java.lang.String str);

    @DexIgnore
    /* renamed from: b */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9131b(androidx.fragment.app.Fragment fragment);

    @DexIgnore
    /* renamed from: c */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9132c(androidx.fragment.app.Fragment fragment);

    @DexIgnore
    /* renamed from: c */
    public abstract void mo9133c();

    @DexIgnore
    /* renamed from: d */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9134d(androidx.fragment.app.Fragment fragment);

    @DexIgnore
    /* renamed from: d */
    public abstract void mo9135d();

    @DexIgnore
    /* renamed from: e */
    public abstract com.fossil.blesdk.obfuscated.C1472bb mo9136e(androidx.fragment.app.Fragment fragment);
}
