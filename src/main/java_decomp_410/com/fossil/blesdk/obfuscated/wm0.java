package com.fossil.blesdk.obfuscated;

import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wm0 implements Runnable {
    @DexIgnore
    public /* final */ Runnable e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public wm0(Runnable runnable, int i) {
        this.e = runnable;
        this.f = i;
    }

    @DexIgnore
    public final void run() {
        Process.setThreadPriority(this.f);
        this.e.run();
    }
}
