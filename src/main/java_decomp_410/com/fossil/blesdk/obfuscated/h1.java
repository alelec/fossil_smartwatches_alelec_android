package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewConfiguration;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h1 implements h7 {
    @DexIgnore
    public static /* final */ int[] A; // = {1, 4, 5, 3, 2, 0};
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Resources b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public a e;
    @DexIgnore
    public ArrayList<k1> f;
    @DexIgnore
    public ArrayList<k1> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public ArrayList<k1> i;
    @DexIgnore
    public ArrayList<k1> j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public int l; // = 0;
    @DexIgnore
    public ContextMenu.ContextMenuInfo m;
    @DexIgnore
    public CharSequence n;
    @DexIgnore
    public Drawable o;
    @DexIgnore
    public View p;
    @DexIgnore
    public boolean q; // = false;
    @DexIgnore
    public boolean r; // = false;
    @DexIgnore
    public boolean s; // = false;
    @DexIgnore
    public boolean t; // = false;
    @DexIgnore
    public boolean u; // = false;
    @DexIgnore
    public ArrayList<k1> v; // = new ArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<WeakReference<p1>> w; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public k1 x;
    @DexIgnore
    public boolean y; // = false;
    @DexIgnore
    public boolean z;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(h1 h1Var);

        @DexIgnore
        boolean a(h1 h1Var, MenuItem menuItem);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(k1 k1Var);
    }

    @DexIgnore
    public h1(Context context) {
        this.a = context;
        this.b = context.getResources();
        this.f = new ArrayList<>();
        this.g = new ArrayList<>();
        this.h = true;
        this.i = new ArrayList<>();
        this.j = new ArrayList<>();
        this.k = true;
        e(true);
    }

    @DexIgnore
    public void a(p1 p1Var) {
        a(p1Var, this.a);
    }

    @DexIgnore
    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    @DexIgnore
    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        PackageManager packageManager = this.a.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            int i7 = resolveInfo.specificIndex;
            Intent intent2 = new Intent(i7 < 0 ? intent : intentArr[i7]);
            intent2.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent3 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null) {
                int i8 = resolveInfo.specificIndex;
                if (i8 >= 0) {
                    menuItemArr[i8] = intent3;
                }
            }
        }
        return size;
    }

    @DexIgnore
    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    @DexIgnore
    public void b(p1 p1Var) {
        Iterator<WeakReference<p1>> it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            p1 p1Var2 = (p1) next.get();
            if (p1Var2 == null || p1Var2 == p1Var) {
                this.w.remove(next);
            }
        }
    }

    @DexIgnore
    public h1 c(int i2) {
        this.l = i2;
        return this;
    }

    @DexIgnore
    public void clear() {
        k1 k1Var = this.x;
        if (k1Var != null) {
            a(k1Var);
        }
        this.f.clear();
        c(true);
    }

    @DexIgnore
    public void clearHeader() {
        this.o = null;
        this.n = null;
        this.p = null;
        c(false);
    }

    @DexIgnore
    public void close() {
        a(true);
    }

    @DexIgnore
    public String d() {
        return "android:menu:actionviewstates";
    }

    @DexIgnore
    public void d(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public void e(Bundle bundle) {
        int size = size();
        SparseArray sparseArray = null;
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = getItem(i2);
            View actionView = item.getActionView();
            if (!(actionView == null || actionView.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                actionView.saveHierarchyState(sparseArray);
                if (item.isActionViewExpanded()) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            if (item.hasSubMenu()) {
                ((v1) item.getSubMenu()).e(bundle);
            }
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(d(), sparseArray);
        }
    }

    @DexIgnore
    public void f(Bundle bundle) {
        b(bundle);
    }

    @DexIgnore
    public MenuItem findItem(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            k1 k1Var = this.f.get(i3);
            if (k1Var.getItemId() == i2) {
                return k1Var;
            }
            if (k1Var.hasSubMenu()) {
                MenuItem findItem = k1Var.getSubMenu().findItem(i2);
                if (findItem != null) {
                    return findItem;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public Drawable g() {
        return this.o;
    }

    @DexIgnore
    public MenuItem getItem(int i2) {
        return this.f.get(i2);
    }

    @DexIgnore
    public CharSequence h() {
        return this.n;
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        if (this.z) {
            return true;
        }
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (this.f.get(i2).isVisible()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public View i() {
        return this.p;
    }

    @DexIgnore
    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    @DexIgnore
    public ArrayList<k1> j() {
        b();
        return this.j;
    }

    @DexIgnore
    public boolean k() {
        return this.t;
    }

    @DexIgnore
    public Resources l() {
        return this.b;
    }

    @DexIgnore
    public h1 m() {
        return this;
    }

    @DexIgnore
    public ArrayList<k1> n() {
        if (!this.h) {
            return this.g;
        }
        this.g.clear();
        int size = this.f.size();
        for (int i2 = 0; i2 < size; i2++) {
            k1 k1Var = this.f.get(i2);
            if (k1Var.isVisible()) {
                this.g.add(k1Var);
            }
        }
        this.h = false;
        this.k = true;
        return this.g;
    }

    @DexIgnore
    public boolean o() {
        return this.y;
    }

    @DexIgnore
    public boolean p() {
        return this.c;
    }

    @DexIgnore
    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), i3);
    }

    @DexIgnore
    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        k1 a2 = a(i2, keyEvent);
        boolean a3 = a2 != null ? a((MenuItem) a2, i3) : false;
        if ((i3 & 2) != 0) {
            a(true);
        }
        return a3;
    }

    @DexIgnore
    public boolean q() {
        return this.d;
    }

    @DexIgnore
    public void r() {
        this.q = false;
        if (this.r) {
            this.r = false;
            c(this.s);
        }
    }

    @DexIgnore
    public void removeGroup(int i2) {
        int a2 = a(i2);
        if (a2 >= 0) {
            int size = this.f.size() - a2;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= size || this.f.get(a2).getGroupId() != i2) {
                    c(true);
                } else {
                    a(a2, false);
                    i3 = i4;
                }
            }
            c(true);
        }
    }

    @DexIgnore
    public void removeItem(int i2) {
        a(b(i2), true);
    }

    @DexIgnore
    public void s() {
        if (!this.q) {
            this.q = true;
            this.r = false;
            this.s = false;
        }
    }

    @DexIgnore
    public void setGroupCheckable(int i2, boolean z2, boolean z3) {
        int size = this.f.size();
        for (int i3 = 0; i3 < size; i3++) {
            k1 k1Var = this.f.get(i3);
            if (k1Var.getGroupId() == i2) {
                k1Var.c(z3);
                k1Var.setCheckable(z2);
            }
        }
    }

    @DexIgnore
    public void setGroupDividerEnabled(boolean z2) {
        this.y = z2;
    }

    @DexIgnore
    public void setGroupEnabled(int i2, boolean z2) {
        int size = this.f.size();
        for (int i3 = 0; i3 < size; i3++) {
            k1 k1Var = this.f.get(i3);
            if (k1Var.getGroupId() == i2) {
                k1Var.setEnabled(z2);
            }
        }
    }

    @DexIgnore
    public void setGroupVisible(int i2, boolean z2) {
        int size = this.f.size();
        boolean z3 = false;
        for (int i3 = 0; i3 < size; i3++) {
            k1 k1Var = this.f.get(i3);
            if (k1Var.getGroupId() == i2 && k1Var.e(z2)) {
                z3 = true;
            }
        }
        if (z3) {
            c(true);
        }
    }

    @DexIgnore
    public void setQwertyMode(boolean z2) {
        this.c = z2;
        c(false);
    }

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    public static int f(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0) {
            int[] iArr = A;
            if (i3 < iArr.length) {
                return (i2 & 65535) | (iArr[i3] << 16);
            }
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    @DexIgnore
    public void a(p1 p1Var, Context context) {
        this.w.add(new WeakReference(p1Var));
        p1Var.a(context, this);
        this.k = true;
    }

    @DexIgnore
    public MenuItem add(int i2) {
        return a(0, 0, 0, this.b.getString(i2));
    }

    @DexIgnore
    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, (CharSequence) this.b.getString(i2));
    }

    @DexIgnore
    public void c(Bundle bundle) {
        if (bundle != null) {
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(d());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View actionView = item.getActionView();
                if (!(actionView == null || actionView.getId() == -1)) {
                    actionView.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((v1) item.getSubMenu()).c(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0) {
                MenuItem findItem = findItem(i3);
                if (findItem != null) {
                    findItem.expandActionView();
                }
            }
        }
    }

    @DexIgnore
    public void d(k1 k1Var) {
        this.h = true;
        c(true);
    }

    @DexIgnore
    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    @DexIgnore
    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        k1 k1Var = (k1) a(i2, i3, i4, charSequence);
        v1 v1Var = new v1(this.a, this, k1Var);
        k1Var.a(v1Var);
        return v1Var;
    }

    @DexIgnore
    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.b.getString(i5));
    }

    @DexIgnore
    public final void b(boolean z2) {
        if (!this.w.isEmpty()) {
            s();
            Iterator<WeakReference<p1>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                p1 p1Var = (p1) next.get();
                if (p1Var == null) {
                    this.w.remove(next);
                } else {
                    p1Var.a(z2);
                }
            }
            r();
        }
    }

    @DexIgnore
    public h1 d(int i2) {
        a(0, (CharSequence) null, i2, (Drawable) null, (View) null);
        return this;
    }

    @DexIgnore
    public final boolean a(v1 v1Var, p1 p1Var) {
        boolean z2 = false;
        if (this.w.isEmpty()) {
            return false;
        }
        if (p1Var != null) {
            z2 = p1Var.a(v1Var);
        }
        Iterator<WeakReference<p1>> it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            p1 p1Var2 = (p1) next.get();
            if (p1Var2 == null) {
                this.w.remove(next);
            } else if (!z2) {
                z2 = p1Var2.a(v1Var);
            }
        }
        return z2;
    }

    @DexIgnore
    public void d(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    public k1 f() {
        return this.x;
    }

    @DexIgnore
    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, (CharSequence) this.b.getString(i5));
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:presenters");
        if (sparseParcelableArray != null && !this.w.isEmpty()) {
            Iterator<WeakReference<p1>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                p1 p1Var = (p1) next.get();
                if (p1Var == null) {
                    this.w.remove(next);
                } else {
                    int id = p1Var.getId();
                    if (id > 0) {
                        Parcelable parcelable = (Parcelable) sparseParcelableArray.get(id);
                        if (parcelable != null) {
                            p1Var.a(parcelable);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        if (!this.w.isEmpty()) {
            SparseArray sparseArray = new SparseArray();
            Iterator<WeakReference<p1>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                p1 p1Var = (p1) next.get();
                if (p1Var == null) {
                    this.w.remove(next);
                } else {
                    int id = p1Var.getId();
                    if (id > 0) {
                        Parcelable b2 = p1Var.b();
                        if (b2 != null) {
                            sparseArray.put(id, b2);
                        }
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:presenters", sparseArray);
        }
    }

    @DexIgnore
    public final void e(boolean z2) {
        boolean z3 = true;
        if (!z2 || this.b.getConfiguration().keyboard == 1 || !g9.d(ViewConfiguration.get(this.a), this.a)) {
            z3 = false;
        }
        this.d = z3;
    }

    @DexIgnore
    public void c(boolean z2) {
        if (!this.q) {
            if (z2) {
                this.h = true;
                this.k = true;
            }
            b(z2);
            return;
        }
        this.r = true;
        if (z2) {
            this.s = true;
        }
    }

    @DexIgnore
    public Context e() {
        return this.a;
    }

    @DexIgnore
    public h1 e(int i2) {
        a(i2, (CharSequence) null, 0, (Drawable) null, (View) null);
        return this;
    }

    @DexIgnore
    public void a(a aVar) {
        this.e = aVar;
    }

    @DexIgnore
    public MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int f2 = f(i4);
        k1 a2 = a(i2, i3, i4, f2, charSequence, this.l);
        ContextMenu.ContextMenuInfo contextMenuInfo = this.m;
        if (contextMenuInfo != null) {
            a2.a(contextMenuInfo);
        }
        ArrayList<k1> arrayList = this.f;
        arrayList.add(a(arrayList, f2), a2);
        c(true);
        return a2;
    }

    @DexIgnore
    public int b(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (this.f.get(i3).getItemId() == i2) {
                return i3;
            }
        }
        return -1;
    }

    @DexIgnore
    public void c(k1 k1Var) {
        this.k = true;
        c(true);
    }

    @DexIgnore
    public void b() {
        ArrayList<k1> n2 = n();
        if (this.k) {
            Iterator<WeakReference<p1>> it = this.w.iterator();
            boolean z2 = false;
            while (it.hasNext()) {
                WeakReference next = it.next();
                p1 p1Var = (p1) next.get();
                if (p1Var == null) {
                    this.w.remove(next);
                } else {
                    z2 |= p1Var.a();
                }
            }
            if (z2) {
                this.i.clear();
                this.j.clear();
                int size = n2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    k1 k1Var = n2.get(i2);
                    if (k1Var.h()) {
                        this.i.add(k1Var);
                    } else {
                        this.j.add(k1Var);
                    }
                }
            } else {
                this.i.clear();
                this.j.clear();
                this.j.addAll(n());
            }
            this.k = false;
        }
    }

    @DexIgnore
    public ArrayList<k1> c() {
        b();
        return this.i;
    }

    @DexIgnore
    public final k1 a(int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        return new k1(this, i2, i3, i4, i5, charSequence, i6);
    }

    @DexIgnore
    public final void a(int i2, boolean z2) {
        if (i2 >= 0 && i2 < this.f.size()) {
            this.f.remove(i2);
            if (z2) {
                c(true);
            }
        }
    }

    @DexIgnore
    public void a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.f.size();
        s();
        for (int i2 = 0; i2 < size; i2++) {
            k1 k1Var = this.f.get(i2);
            if (k1Var.getGroupId() == groupId && k1Var.i() && k1Var.isCheckable()) {
                k1Var.b(k1Var == menuItem);
            }
        }
        r();
    }

    @DexIgnore
    public int a(int i2) {
        return a(i2, 0);
    }

    @DexIgnore
    public int a(int i2, int i3) {
        int size = size();
        if (i3 < 0) {
            i3 = 0;
        }
        while (i3 < size) {
            if (this.f.get(i3).getGroupId() == i2) {
                return i3;
            }
            i3++;
        }
        return -1;
    }

    @DexIgnore
    public boolean b(k1 k1Var) {
        boolean z2 = false;
        if (this.w.isEmpty()) {
            return false;
        }
        s();
        Iterator<WeakReference<p1>> it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference next = it.next();
            p1 p1Var = (p1) next.get();
            if (p1Var == null) {
                this.w.remove(next);
            } else {
                z2 = p1Var.b(this, k1Var);
                if (z2) {
                    break;
                }
            }
        }
        r();
        if (z2) {
            this.x = k1Var;
        }
        return z2;
    }

    @DexIgnore
    public boolean a(h1 h1Var, MenuItem menuItem) {
        a aVar = this.e;
        return aVar != null && aVar.a(h1Var, menuItem);
    }

    @DexIgnore
    public void a() {
        a aVar = this.e;
        if (aVar != null) {
            aVar.a(this);
        }
    }

    @DexIgnore
    public static int a(ArrayList<k1> arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size).c() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    @DexIgnore
    public void a(List<k1> list, int i2, KeyEvent keyEvent) {
        boolean p2 = p();
        int modifiers = keyEvent.getModifiers();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.f.size();
            for (int i3 = 0; i3 < size; i3++) {
                k1 k1Var = this.f.get(i3);
                if (k1Var.hasSubMenu()) {
                    ((h1) k1Var.getSubMenu()).a(list, i2, keyEvent);
                }
                char alphabeticShortcut = p2 ? k1Var.getAlphabeticShortcut() : k1Var.getNumericShortcut();
                if (((modifiers & 69647) == ((p2 ? k1Var.getAlphabeticModifiers() : k1Var.getNumericModifiers()) & 69647)) && alphabeticShortcut != 0) {
                    char[] cArr = keyData.meta;
                    if ((alphabeticShortcut == cArr[0] || alphabeticShortcut == cArr[2] || (p2 && alphabeticShortcut == 8 && i2 == 67)) && k1Var.isEnabled()) {
                        list.add(k1Var);
                    }
                }
            }
        }
    }

    @DexIgnore
    public k1 a(int i2, KeyEvent keyEvent) {
        char c2;
        ArrayList<k1> arrayList = this.v;
        arrayList.clear();
        a((List<k1>) arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return arrayList.get(0);
        }
        boolean p2 = p();
        for (int i3 = 0; i3 < size; i3++) {
            k1 k1Var = arrayList.get(i3);
            if (p2) {
                c2 = k1Var.getAlphabeticShortcut();
            } else {
                c2 = k1Var.getNumericShortcut();
            }
            if ((c2 == keyData.meta[0] && (metaState & 2) == 0) || ((c2 == keyData.meta[2] && (metaState & 2) != 0) || (p2 && c2 == 8 && i2 == 67))) {
                return k1Var;
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(MenuItem menuItem, int i2) {
        return a(menuItem, (p1) null, i2);
    }

    @DexIgnore
    public boolean a(MenuItem menuItem, p1 p1Var, int i2) {
        k1 k1Var = (k1) menuItem;
        if (k1Var == null || !k1Var.isEnabled()) {
            return false;
        }
        boolean g2 = k1Var.g();
        m8 a2 = k1Var.a();
        boolean z2 = a2 != null && a2.hasSubMenu();
        if (k1Var.f()) {
            g2 |= k1Var.expandActionView();
            if (g2) {
                a(true);
            }
        } else if (k1Var.hasSubMenu() || z2) {
            if ((i2 & 4) == 0) {
                a(false);
            }
            if (!k1Var.hasSubMenu()) {
                k1Var.a(new v1(e(), this, k1Var));
            }
            v1 v1Var = (v1) k1Var.getSubMenu();
            if (z2) {
                a2.onPrepareSubMenu(v1Var);
            }
            g2 |= a(v1Var, p1Var);
            if (!g2) {
                a(true);
            }
        } else if ((i2 & 1) == 0) {
            a(true);
        }
        return g2;
    }

    @DexIgnore
    public final void a(boolean z2) {
        if (!this.u) {
            this.u = true;
            Iterator<WeakReference<p1>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                p1 p1Var = (p1) next.get();
                if (p1Var == null) {
                    this.w.remove(next);
                } else {
                    p1Var.a(this, z2);
                }
            }
            this.u = false;
        }
    }

    @DexIgnore
    public final void a(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources l2 = l();
        if (view != null) {
            this.p = view;
            this.n = null;
            this.o = null;
        } else {
            if (i2 > 0) {
                this.n = l2.getText(i2);
            } else if (charSequence != null) {
                this.n = charSequence;
            }
            if (i3 > 0) {
                this.o = k6.c(e(), i3);
            } else if (drawable != null) {
                this.o = drawable;
            }
            this.p = null;
        }
        c(false);
    }

    @DexIgnore
    public h1 a(CharSequence charSequence) {
        a(0, charSequence, 0, (Drawable) null, (View) null);
        return this;
    }

    @DexIgnore
    public h1 a(Drawable drawable) {
        a(0, (CharSequence) null, 0, drawable, (View) null);
        return this;
    }

    @DexIgnore
    public h1 a(View view) {
        a(0, (CharSequence) null, 0, (Drawable) null, view);
        return this;
    }

    @DexIgnore
    public boolean a(k1 k1Var) {
        boolean z2 = false;
        if (!this.w.isEmpty() && this.x == k1Var) {
            s();
            Iterator<WeakReference<p1>> it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                p1 p1Var = (p1) next.get();
                if (p1Var == null) {
                    this.w.remove(next);
                } else {
                    z2 = p1Var.a(this, k1Var);
                    if (z2) {
                        break;
                    }
                }
            }
            r();
            if (z2) {
                this.x = null;
            }
        }
        return z2;
    }
}
