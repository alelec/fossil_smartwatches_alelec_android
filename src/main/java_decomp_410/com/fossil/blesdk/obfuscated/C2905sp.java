package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sp */
public interface C2905sp {
    @DexIgnore
    /* renamed from: a */
    void mo15337a(com.fossil.blesdk.obfuscated.C2819rp<?> rpVar, com.fossil.blesdk.obfuscated.C2143jo joVar);

    @DexIgnore
    /* renamed from: a */
    void mo15338a(com.fossil.blesdk.obfuscated.C2819rp<?> rpVar, com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp<?> vpVar);
}
