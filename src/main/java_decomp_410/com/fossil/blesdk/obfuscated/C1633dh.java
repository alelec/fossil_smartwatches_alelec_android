package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dh */
public class C1633dh {
    @DexIgnore
    /* renamed from: a */
    public static <T> android.animation.ObjectAnimator m5860a(T t, android.util.Property<T, android.graphics.PointF> property, android.graphics.Path path) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return android.animation.ObjectAnimator.ofObject(t, property, (android.animation.TypeConverter) null, path);
        }
        return android.animation.ObjectAnimator.ofFloat(t, new com.fossil.blesdk.obfuscated.C1731eh(property, path), new float[]{com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
    }
}
