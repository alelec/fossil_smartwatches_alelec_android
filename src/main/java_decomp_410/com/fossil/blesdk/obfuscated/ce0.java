package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ce0 extends xa {
    @DexIgnore
    public Dialog e; // = null;
    @DexIgnore
    public DialogInterface.OnCancelListener f; // = null;

    @DexIgnore
    public static ce0 a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        ce0 ce0 = new ce0();
        bk0.a(dialog, (Object) "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener((DialogInterface.OnCancelListener) null);
        dialog2.setOnDismissListener((DialogInterface.OnDismissListener) null);
        ce0.e = dialog2;
        if (onCancelListener != null) {
            ce0.f = onCancelListener;
        }
        return ce0;
    }

    @DexIgnore
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.f;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.e == null) {
            setShowsDialog(false);
        }
        return this.e;
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
