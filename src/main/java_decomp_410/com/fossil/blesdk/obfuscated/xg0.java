package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.ve0;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xg0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ud0 e;
    @DexIgnore
    public /* final */ /* synthetic */ ve0.c f;

    @DexIgnore
    public xg0(ve0.c cVar, ud0 ud0) {
        this.f = cVar;
        this.e = ud0;
    }

    @DexIgnore
    public final void run() {
        if (this.e.L()) {
            boolean unused = this.f.e = true;
            if (this.f.a.l()) {
                this.f.a();
                return;
            }
            try {
                this.f.a.a((tj0) null, Collections.emptySet());
            } catch (SecurityException e2) {
                Log.e("GoogleApiManager", "Failed to get service from broker. ", e2);
                ((ve0.a) ve0.this.i.get(this.f.b)).a(new ud0(10));
            }
        } else {
            ((ve0.a) ve0.this.i.get(this.f.b)).a(this.e);
        }
    }
}
