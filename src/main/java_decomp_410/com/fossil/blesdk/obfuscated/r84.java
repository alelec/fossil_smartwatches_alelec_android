package com.fossil.blesdk.obfuscated;

import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class r84 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable, y84 {
        @DexIgnore
        public /* final */ Runnable e;
        @DexIgnore
        public /* final */ b f;
        @DexIgnore
        public Thread g;

        @DexIgnore
        public a(Runnable runnable, b bVar) {
            this.e = runnable;
            this.f = bVar;
        }

        @DexIgnore
        public void dispose() {
            if (this.g == Thread.currentThread()) {
                b bVar = this.f;
                if (bVar instanceof fa4) {
                    ((fa4) bVar).a();
                    return;
                }
            }
            this.f.dispose();
        }

        @DexIgnore
        public void run() {
            this.g = Thread.currentThread();
            try {
                this.e.run();
            } finally {
                dispose();
                this.g = null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b implements y84 {
        @DexIgnore
        public y84 a(Runnable runnable) {
            return a(runnable, 0, TimeUnit.NANOSECONDS);
        }

        @DexIgnore
        public abstract y84 a(Runnable runnable, long j, TimeUnit timeUnit);

        @DexIgnore
        public long a(TimeUnit timeUnit) {
            return timeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        }
    }

    /*
    static {
        TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15).longValue());
    }
    */

    @DexIgnore
    public abstract b a();

    @DexIgnore
    public y84 a(Runnable runnable) {
        return a(runnable, 0, TimeUnit.NANOSECONDS);
    }

    @DexIgnore
    public y84 a(Runnable runnable, long j, TimeUnit timeUnit) {
        b a2 = a();
        a aVar = new a(ia4.a(runnable), a2);
        a2.a(aVar, j, timeUnit);
        return aVar;
    }
}
