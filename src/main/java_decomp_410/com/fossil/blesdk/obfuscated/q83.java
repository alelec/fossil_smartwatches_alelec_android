package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q83 extends zr2 implements p83 {
    @DexIgnore
    public tr3<w82> j;
    @DexIgnore
    public o83 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.e {
        @DexIgnore
        public /* final */ /* synthetic */ q83 a;

        @DexIgnore
        public b(q83 q83) {
            this.a = q83;
        }

        @DexIgnore
        public final void a(Calendar calendar) {
            o83 a2 = this.a.k;
            if (a2 != null) {
                kd4.a((Object) calendar, "calendar");
                Date time = calendar.getTime();
                kd4.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.d {
        @DexIgnore
        public /* final */ /* synthetic */ q83 e;

        @DexIgnore
        public c(q83 q83) {
            this.e = q83;
        }

        @DexIgnore
        public final void a(int i, Calendar calendar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.e.getActivity();
            if (activity != null && calendar != null) {
                ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.D;
                Date time = calendar.getTime();
                kd4.a((Object) time, "it.time");
                kd4.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActiveTimeOverviewMonthFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onCreateView");
        w82 w82 = (w82) qa.a(layoutInflater, R.layout.fragment_active_time_overview_month, viewGroup, false, O0());
        w82.q.setEndDate(Calendar.getInstance());
        w82.q.setOnCalendarMonthChanged(new b(this));
        w82.q.setOnCalendarItemClickListener(new c(this));
        this.j = new tr3<>(this, w82);
        tr3<w82> tr3 = this.j;
        if (tr3 != null) {
            w82 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onResume");
        o83 o83 = this.k;
        if (o83 != null) {
            o83.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onStop");
        o83 o83 = this.k;
        if (o83 != null) {
            o83.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    public View p(int i) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        View view = (View) this.l.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.l.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    @DexIgnore
    public void a(TreeMap<Long, Float> treeMap) {
        kd4.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        tr3<w82> tr3 = this.j;
        if (tr3 != null) {
            w82 a2 = tr3.a();
            if (a2 != null) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                if (recyclerViewCalendar != null) {
                    recyclerViewCalendar.setTintColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.activeTime));
                }
            }
        }
        tr3<w82> tr32 = this.j;
        if (tr32 != null) {
            w82 a3 = tr32.a();
            if (a3 != null) {
                RecyclerViewCalendar recyclerViewCalendar2 = a3.q;
                if (recyclerViewCalendar2 != null) {
                    recyclerViewCalendar2.setData(treeMap);
                }
            }
        }
        ((RecyclerViewCalendar) p(g62.calendarMonth)).setEnableButtonNextAndPrevMonth(true);
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        kd4.b(date, "selectDate");
        kd4.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        tr3<w82> tr3 = this.j;
        if (tr3 != null) {
            w82 a2 = tr3.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                kd4.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                kd4.a((Object) instance2, "startCalendar");
                instance2.setTime(rk2.n(date2));
                kd4.a((Object) instance3, "endCalendar");
                instance3.setTime(rk2.i(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
            }
        }
    }

    @DexIgnore
    public void a(o83 o83) {
        kd4.b(o83, "presenter");
        this.k = o83;
    }
}
