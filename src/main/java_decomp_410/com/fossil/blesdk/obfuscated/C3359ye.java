package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ye */
public class C3359ye extends com.fossil.blesdk.obfuscated.C2300l8 {

    @DexIgnore
    /* renamed from: c */
    public /* final */ androidx.recyclerview.widget.RecyclerView f11234c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ com.fossil.blesdk.obfuscated.C2300l8 f11235d; // = new com.fossil.blesdk.obfuscated.C3359ye.C3360a(this);

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ye$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ye$a */
    public static class C3360a extends com.fossil.blesdk.obfuscated.C2300l8 {

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C3359ye f11236c;

        @DexIgnore
        public C3360a(com.fossil.blesdk.obfuscated.C3359ye yeVar) {
            this.f11236c = yeVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo1696a(android.view.View view, com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
            super.mo1696a(view, q9Var);
            if (!this.f11236c.mo18084c() && this.f11236c.f11234c.getLayoutManager() != null) {
                this.f11236c.f11234c.getLayoutManager().mo2893a(view, q9Var);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo1697a(android.view.View view, int i, android.os.Bundle bundle) {
            if (super.mo1697a(view, i, bundle)) {
                return true;
            }
            if (this.f11236c.mo18084c() || this.f11236c.f11234c.getLayoutManager() == null) {
                return false;
            }
            return this.f11236c.f11234c.getLayoutManager().mo2907a(view, i, bundle);
        }
    }

    @DexIgnore
    public C3359ye(androidx.recyclerview.widget.RecyclerView recyclerView) {
        this.f11234c = recyclerView;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo1697a(android.view.View view, int i, android.os.Bundle bundle) {
        if (super.mo1697a(view, i, bundle)) {
            return true;
        }
        if (mo18084c() || this.f11234c.getLayoutManager() == null) {
            return false;
        }
        return this.f11234c.getLayoutManager().mo2905a(i, bundle);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo1698b(android.view.View view, android.view.accessibility.AccessibilityEvent accessibilityEvent) {
        super.mo1698b(view, accessibilityEvent);
        accessibilityEvent.setClassName(androidx.recyclerview.widget.RecyclerView.class.getName());
        if ((view instanceof androidx.recyclerview.widget.RecyclerView) && !mo18084c()) {
            androidx.recyclerview.widget.RecyclerView recyclerView = (androidx.recyclerview.widget.RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().mo2418a(accessibilityEvent);
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo18084c() {
        return this.f11234c.mo2643p();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1696a(android.view.View view, com.fossil.blesdk.obfuscated.C2711q9 q9Var) {
        super.mo1696a(view, q9Var);
        q9Var.mo15065a((java.lang.CharSequence) androidx.recyclerview.widget.RecyclerView.class.getName());
        if (!mo18084c() && this.f11234c.getLayoutManager() != null) {
            this.f11234c.getLayoutManager().mo2904a(q9Var);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2300l8 mo18083b() {
        return this.f11235d;
    }
}
