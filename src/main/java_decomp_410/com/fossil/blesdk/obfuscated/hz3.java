package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hz3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore
    public void a(Bundle bundle) {
        this.a = bz3.a(bundle, "_wxapi_basereq_transaction");
        this.b = bz3.a(bundle, "_wxapi_basereq_openid");
    }

    @DexIgnore
    public abstract boolean a();

    @DexIgnore
    public abstract int b();

    @DexIgnore
    public void b(Bundle bundle) {
        bundle.putInt("_wxapi_command_type", b());
        bundle.putString("_wxapi_basereq_transaction", this.a);
        bundle.putString("_wxapi_basereq_openid", this.b);
    }
}
