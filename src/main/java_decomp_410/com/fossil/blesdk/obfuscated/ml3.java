package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.Explore;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ml3 extends v52<ll3> {
    @DexIgnore
    void X();

    @DexIgnore
    void b(boolean z);

    @DexIgnore
    void f0();

    @DexIgnore
    void g();

    @DexIgnore
    void g(int i);

    @DexIgnore
    void h();

    @DexIgnore
    void i(List<? extends Explore> list);

    @DexIgnore
    void n0();

    @DexIgnore
    void t();

    @DexIgnore
    void v0();
}
