package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class hr0 {
    @DexIgnore
    public static hr0 a;

    @DexIgnore
    public static synchronized hr0 a() {
        hr0 hr0;
        synchronized (hr0.class) {
            if (a == null) {
                a = new dr0();
            }
            hr0 = a;
        }
        return hr0;
    }

    @DexIgnore
    public abstract ir0<Boolean> a(String str, boolean z);
}
