package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.graphics.RectF;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ch {
    @DexIgnore
    public static /* final */ Matrix a; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Matrix {
        @DexIgnore
        public void a() {
            throw new IllegalStateException("Matrix can not be modified");
        }

        @DexIgnore
        public boolean postConcat(Matrix matrix) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postRotate(float f, float f2, float f3) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postScale(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postSkew(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postTranslate(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preConcat(Matrix matrix) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preRotate(float f, float f2, float f3) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preScale(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preSkew(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preTranslate(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public void reset() {
            a();
            throw null;
        }

        @DexIgnore
        public void set(Matrix matrix) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean setConcat(Matrix matrix, Matrix matrix2) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean setPolyToPoly(float[] fArr, int i, float[] fArr2, int i2, int i3) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean setRectToRect(RectF rectF, RectF rectF2, Matrix.ScaleToFit scaleToFit) {
            a();
            throw null;
        }

        @DexIgnore
        public void setRotate(float f, float f2, float f3) {
            a();
            throw null;
        }

        @DexIgnore
        public void setScale(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public void setSinCos(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public void setSkew(float f, float f2, float f3, float f4) {
            a();
            throw null;
        }

        @DexIgnore
        public void setTranslate(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public void setValues(float[] fArr) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postRotate(float f) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postScale(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean postSkew(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preRotate(float f) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preScale(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public boolean preSkew(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public void setRotate(float f) {
            a();
            throw null;
        }

        @DexIgnore
        public void setScale(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public void setSinCos(float f, float f2) {
            a();
            throw null;
        }

        @DexIgnore
        public void setSkew(float f, float f2) {
            a();
            throw null;
        }
    }
}
