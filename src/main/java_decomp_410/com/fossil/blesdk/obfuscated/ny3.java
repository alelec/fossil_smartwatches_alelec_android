package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.NetworkPolicy;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ny3 implements Downloader {
    @DexIgnore
    public static volatile Object b;
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> d; // = new a();
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        public StringBuilder initialValue() {
            return new StringBuilder();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public static Object a(Context context) throws IOException {
            File b = oy3.b(context);
            HttpResponseCache installed = HttpResponseCache.getInstalled();
            return installed == null ? HttpResponseCache.install(b, oy3.a(b)) : installed;
        }
    }

    @DexIgnore
    public ny3(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public HttpURLConnection a(Uri uri) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(uri.toString()).openConnection();
        httpURLConnection.setConnectTimeout(15000);
        httpURLConnection.setReadTimeout(20000);
        return httpURLConnection;
    }

    @DexIgnore
    public Downloader.Response load(Uri uri, int i) throws IOException {
        String str;
        if (Build.VERSION.SDK_INT >= 14) {
            a(this.a);
        }
        HttpURLConnection a2 = a(uri);
        a2.setUseCaches(true);
        if (i != 0) {
            if (NetworkPolicy.isOfflineOnly(i)) {
                str = "only-if-cached,max-age=2147483647";
            } else {
                StringBuilder sb = d.get();
                sb.setLength(0);
                if (!NetworkPolicy.shouldReadFromDiskCache(i)) {
                    sb.append("no-cache");
                }
                if (!NetworkPolicy.shouldWriteToDiskCache(i)) {
                    if (sb.length() > 0) {
                        sb.append(',');
                    }
                    sb.append("no-store");
                }
                str = sb.toString();
            }
            a2.setRequestProperty(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER, str);
        }
        int responseCode = a2.getResponseCode();
        if (responseCode < 300) {
            long headerFieldInt = (long) a2.getHeaderFieldInt("Content-Length", -1);
            return new Downloader.Response(a2.getInputStream(), oy3.a(a2.getHeaderField("X-Android-Response-Source")), headerFieldInt);
        }
        a2.disconnect();
        throw new Downloader.ResponseException(responseCode + " " + a2.getResponseMessage(), i, responseCode);
    }

    @DexIgnore
    public static void a(Context context) {
        if (b == null) {
            try {
                synchronized (c) {
                    if (b == null) {
                        b = b.a(context);
                    }
                }
            } catch (IOException unused) {
            }
        }
    }
}
