package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountActivity;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ni3 implements MembersInjector<DeleteAccountActivity> {
    @DexIgnore
    public static void a(DeleteAccountActivity deleteAccountActivity, DeleteAccountPresenter deleteAccountPresenter) {
        deleteAccountActivity.B = deleteAccountPresenter;
    }
}
