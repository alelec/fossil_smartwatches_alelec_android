package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class na2 extends ma2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ScrollView A;
    @DexIgnore
    public long B;

    /*
    static {
        D.put(R.id.cl_container, 1);
        D.put(R.id.rv_categories, 2);
        D.put(R.id.rv_complications, 3);
        D.put(R.id.tv_selected_complication, 4);
        D.put(R.id.cl_complication_setting, 5);
        D.put(R.id.iv_complication_setting, 6);
        D.put(R.id.tv_complication_setting, 7);
        D.put(R.id.switch_ring, 8);
        D.put(R.id.tv_permission_order, 9);
        D.put(R.id.tv_complication_permission, 10);
        D.put(R.id.tv_complication_detail, 11);
    }
    */

    @DexIgnore
    public na2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 12, C, D));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public na2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[5], objArr[1], objArr[6], objArr[2], objArr[3], objArr[8], objArr[11], objArr[10], objArr[7], objArr[9], objArr[4]);
        this.B = -1;
        this.A = objArr[0];
        this.A.setTag((Object) null);
        a(view);
        f();
    }
}
