package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lh1 {
    @DexIgnore
    public /* final */ xh1 a;

    @DexIgnore
    public lh1(xh1 xh1) {
        this.a = xh1;
    }

    @DexIgnore
    public final void a(String str) {
        if (str == null || str.isEmpty()) {
            this.a.d().y().a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.a.a().e();
        if (!a()) {
            this.a.d().y().a("Install Referrer Reporter is not available");
            return;
        }
        this.a.d().y().a("Install Referrer Reporter is initializing");
        mh1 mh1 = new mh1(this, str);
        this.a.a().e();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.a.getContext().getPackageManager();
        if (packageManager == null) {
            this.a.d().v().a("Failed to obtain Package Manager to verify binding conditions");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.a.d().y().a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ResolveInfo resolveInfo = queryIntentServices.get(0);
        ServiceInfo serviceInfo = resolveInfo.serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (resolveInfo.serviceInfo.name == null || !"com.android.vending".equals(str2) || !a()) {
                this.a.d().y().a("Play Store missing or incompatible. Version 8.3.73 or later required");
                return;
            }
            try {
                this.a.d().y().a("Install Referrer Service is", dm0.a().a(this.a.getContext(), new Intent(intent), mh1, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.a.d().s().a("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    @DexIgnore
    public final boolean a() {
        try {
            an0 b = bn0.b(this.a.getContext());
            if (b == null) {
                this.a.d().y().a("Failed to retrieve Package Manager to check Play Store compatibility");
                return false;
            } else if (b.b("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            this.a.d().y().a("Failed to retrieve Play Store version", e);
            return false;
        }
    }

    @DexIgnore
    public final Bundle a(String str, g81 g81) {
        this.a.a().e();
        if (g81 == null) {
            this.a.d().v().a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle zza = g81.zza(bundle);
            if (zza != null) {
                return zza;
            }
            this.a.d().s().a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.a.d().s().a("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }
}
