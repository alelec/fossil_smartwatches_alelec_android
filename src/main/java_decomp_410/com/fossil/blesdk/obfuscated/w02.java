package com.fossil.blesdk.obfuscated;

import java.lang.reflect.AccessibleObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class w02 {
    @DexIgnore
    public static /* final */ w02 a; // = (k02.b() < 9 ? new v02() : new x02());

    @DexIgnore
    public static w02 a() {
        return a;
    }

    @DexIgnore
    public abstract void a(AccessibleObject accessibleObject);
}
