package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import com.facebook.internal.AnalyticsEvents;
import java.lang.ref.WeakReference;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class va0 {
    @DexIgnore
    public static WeakReference<Context> a;
    @DexIgnore
    public static String b; // = new String();
    @DexIgnore
    public static na0 c; // = new na0("", "", "");
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ va0 f; // = new va0();

    /*
    static {
        new na0("", "", "");
        String str = Build.VERSION.RELEASE;
        if (str == null) {
            str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        d = str;
        String str2 = Build.MODEL;
        if (str2 == null) {
            str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        e = str2;
    }
    */

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "<set-?>");
        b = str;
    }

    @DexIgnore
    public final String b() {
        return d;
    }

    @DexIgnore
    public final String c() {
        return e;
    }

    @DexIgnore
    public final int d() {
        return (TimeZone.getDefault().getOffset(System.currentTimeMillis()) / 1000) / 60;
    }

    @DexIgnore
    public final String e() {
        return b;
    }

    @DexIgnore
    public final void a(na0 na0) {
        kd4.b(na0, "value");
        c = na0;
        da0 da0 = da0.l;
        da0.a(new na0(c.c() + "/sdk_log", c.a(), c.b()));
        z90 z90 = z90.l;
        z90.a(new na0(c.c() + "/raw_minute_data", c.a(), c.b()));
        y90 y90 = y90.l;
        y90.a(new na0(c.c() + "/raw_hardware_log", c.a(), c.b()));
    }

    @DexIgnore
    public final void a(Context context) {
        kd4.b(context, "context");
        Context applicationContext = context.getApplicationContext();
        if (applicationContext != null) {
            a = new WeakReference<>(applicationContext);
        }
    }

    @DexIgnore
    public final Context a() {
        WeakReference<Context> weakReference = a;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }
}
