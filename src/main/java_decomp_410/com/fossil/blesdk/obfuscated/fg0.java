package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.pj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fg0 implements pj0.a {
    @DexIgnore
    public /* final */ /* synthetic */ eg0 a;

    @DexIgnore
    public fg0(eg0 eg0) {
        this.a = eg0;
    }

    @DexIgnore
    public final boolean c() {
        return this.a.g();
    }

    @DexIgnore
    public final Bundle n() {
        return null;
    }
}
