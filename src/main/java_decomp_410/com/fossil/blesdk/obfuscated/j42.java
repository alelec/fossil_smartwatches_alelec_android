package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.kc;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j42 implements kc.b {
    @DexIgnore
    public /* final */ Map<Class<? extends ic>, Provider<ic>> a;

    @DexIgnore
    public j42(Map<Class<? extends ic>, Provider<ic>> map) {
        kd4.b(map, "creators");
        this.a = map;
    }

    @DexIgnore
    public <T extends ic> T a(Class<T> cls) {
        T t;
        kd4.b(cls, "modelClass");
        Provider provider = this.a.get(cls);
        if (provider == null) {
            Iterator<T> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (cls.isAssignableFrom((Class) ((Map.Entry) t).getKey())) {
                    break;
                }
            }
            Map.Entry entry = (Map.Entry) t;
            provider = entry != null ? (Provider) entry.getValue() : null;
        }
        if (provider != null) {
            try {
                T t2 = provider.get();
                if (t2 != null) {
                    return (ic) t2;
                }
                throw new TypeCastException("null cannot be cast to non-null type T");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("unknown model class; " + cls);
        }
    }
}
