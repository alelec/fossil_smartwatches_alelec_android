package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zo0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<zo0> CREATOR; // = new qp0();
    @DexIgnore
    public static /* final */ int[] m; // = new int[0];
    @DexIgnore
    public /* final */ DataType e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ap0 h;
    @DexIgnore
    public /* final */ jp0 i;
    @DexIgnore
    public /* final */ String j;
    @DexIgnore
    public /* final */ int[] k;
    @DexIgnore
    public /* final */ String l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public DataType a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public String c;
        @DexIgnore
        public ap0 d;
        @DexIgnore
        public jp0 e;
        @DexIgnore
        public String f; // = "";
        @DexIgnore
        public int[] g;

        @DexIgnore
        public final a a(DataType dataType) {
            this.a = dataType;
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        public final a a(int i) {
            this.b = i;
            return this;
        }

        @DexIgnore
        public final a a(ap0 ap0) {
            this.d = ap0;
            return this;
        }

        @DexIgnore
        public final a a(String str) {
            this.e = jp0.e(str);
            return this;
        }

        @DexIgnore
        public final a a(Context context) {
            a(context.getPackageName());
            return this;
        }

        @DexIgnore
        public final zo0 a() {
            boolean z = true;
            bk0.b(this.a != null, "Must set data type");
            if (this.b < 0) {
                z = false;
            }
            bk0.b(z, "Must set data source type");
            return new zo0(this);
        }
    }

    @DexIgnore
    public zo0(DataType dataType, String str, int i2, ap0 ap0, jp0 jp0, String str2, int[] iArr) {
        this.e = dataType;
        this.g = i2;
        this.f = str;
        this.h = ap0;
        this.i = jp0;
        this.j = str2;
        this.l = R();
        this.k = iArr == null ? m : iArr;
    }

    @DexIgnore
    public int[] H() {
        return this.k;
    }

    @DexIgnore
    public DataType I() {
        return this.e;
    }

    @DexIgnore
    public ap0 J() {
        return this.h;
    }

    @DexIgnore
    public String K() {
        return this.f;
    }

    @DexIgnore
    public String L() {
        return this.l;
    }

    @DexIgnore
    public String M() {
        return this.j;
    }

    @DexIgnore
    public int N() {
        return this.g;
    }

    @DexIgnore
    public final String O() {
        int i2 = this.g;
        if (i2 == 0) {
            return OrmLiteConfigUtil.RAW_DIR_NAME;
        }
        if (i2 == 1) {
            return "derived";
        }
        if (i2 != 2) {
            return i2 != 3 ? "derived" : "converted";
        }
        return "cleaned";
    }

    @DexIgnore
    public final String P() {
        String str;
        String str2;
        int i2 = this.g;
        String str3 = i2 != 0 ? i2 != 1 ? i2 != 2 ? i2 != 3 ? "?" : "v" : "c" : "d" : "r";
        String L = this.e.L();
        jp0 jp0 = this.i;
        String str4 = "";
        if (jp0 == null) {
            str = str4;
        } else if (jp0.equals(jp0.g)) {
            str = ":gms";
        } else {
            String valueOf = String.valueOf(this.i.H());
            str = valueOf.length() != 0 ? ":".concat(valueOf) : new String(":");
        }
        ap0 ap0 = this.h;
        if (ap0 != null) {
            String I = ap0.I();
            String L2 = this.h.L();
            StringBuilder sb = new StringBuilder(String.valueOf(I).length() + 2 + String.valueOf(L2).length());
            sb.append(":");
            sb.append(I);
            sb.append(":");
            sb.append(L2);
            str2 = sb.toString();
        } else {
            str2 = str4;
        }
        String str5 = this.j;
        if (str5 != null) {
            String valueOf2 = String.valueOf(str5);
            str4 = valueOf2.length() != 0 ? ":".concat(valueOf2) : new String(":");
        }
        StringBuilder sb2 = new StringBuilder(str3.length() + 1 + String.valueOf(L).length() + String.valueOf(str).length() + String.valueOf(str2).length() + String.valueOf(str4).length());
        sb2.append(str3);
        sb2.append(":");
        sb2.append(L);
        sb2.append(str);
        sb2.append(str2);
        sb2.append(str4);
        return sb2.toString();
    }

    @DexIgnore
    public final jp0 Q() {
        return this.i;
    }

    @DexIgnore
    public final String R() {
        StringBuilder sb = new StringBuilder();
        sb.append(O());
        sb.append(":");
        sb.append(this.e.I());
        if (this.i != null) {
            sb.append(":");
            sb.append(this.i.H());
        }
        if (this.h != null) {
            sb.append(":");
            sb.append(this.h.J());
        }
        if (this.j != null) {
            sb.append(":");
            sb.append(this.j);
        }
        return sb.toString();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zo0)) {
            return false;
        }
        return this.l.equals(((zo0) obj).l);
    }

    @DexIgnore
    public int hashCode() {
        return this.l.hashCode();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("DataSource{");
        sb.append(O());
        if (this.f != null) {
            sb.append(":");
            sb.append(this.f);
        }
        if (this.i != null) {
            sb.append(":");
            sb.append(this.i);
        }
        if (this.h != null) {
            sb.append(":");
            sb.append(this.h);
        }
        if (this.j != null) {
            sb.append(":");
            sb.append(this.j);
        }
        sb.append(":");
        sb.append(this.e);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) I(), i2, false);
        kk0.a(parcel, 2, K(), false);
        kk0.a(parcel, 3, N());
        kk0.a(parcel, 4, (Parcelable) J(), i2, false);
        kk0.a(parcel, 5, (Parcelable) this.i, i2, false);
        kk0.a(parcel, 6, M(), false);
        kk0.a(parcel, 8, H(), false);
        kk0.a(parcel, a2);
    }

    @DexIgnore
    public zo0(a aVar) {
        this.e = aVar.a;
        this.g = aVar.b;
        this.f = aVar.c;
        this.h = aVar.d;
        this.i = aVar.e;
        this.j = aVar.f;
        this.l = R();
        this.k = aVar.g;
    }
}
