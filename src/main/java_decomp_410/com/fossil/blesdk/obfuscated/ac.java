package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ac<T> extends MutableLiveData<T> {
    @DexIgnore
    public l3<LiveData<?>, a<?>> k; // = new l3<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<V> implements cc<V> {
        @DexIgnore
        public /* final */ LiveData<V> a;
        @DexIgnore
        public /* final */ cc<? super V> b;
        @DexIgnore
        public int c; // = -1;

        @DexIgnore
        public a(LiveData<V> liveData, cc<? super V> ccVar) {
            this.a = liveData;
            this.b = ccVar;
        }

        @DexIgnore
        public void a() {
            this.a.a(this);
        }

        @DexIgnore
        public void b() {
            this.a.b(this);
        }

        @DexIgnore
        public void a(V v) {
            if (this.c != this.a.b()) {
                this.c = this.a.b();
                this.b.a(v);
            }
        }
    }

    @DexIgnore
    public <S> void a(LiveData<S> liveData, cc<? super S> ccVar) {
        a aVar = new a(liveData, ccVar);
        a b = this.k.b(liveData, aVar);
        if (b != null && b.b != ccVar) {
            throw new IllegalArgumentException("This source was already added with the different observer");
        } else if (b == null && c()) {
            aVar.a();
        }
    }

    @DexIgnore
    public void d() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            ((a) it.next().getValue()).a();
        }
    }

    @DexIgnore
    public void e() {
        Iterator<Map.Entry<LiveData<?>, a<?>>> it = this.k.iterator();
        while (it.hasNext()) {
            ((a) it.next().getValue()).b();
        }
    }

    @DexIgnore
    public <S> void a(LiveData<S> liveData) {
        a remove = this.k.remove(liveData);
        if (remove != null) {
            remove.b();
        }
    }
}
