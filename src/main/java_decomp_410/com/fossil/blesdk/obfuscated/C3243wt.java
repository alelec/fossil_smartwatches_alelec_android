package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wt */
public class C3243wt extends com.fossil.blesdk.obfuscated.C2435mt<com.fossil.blesdk.obfuscated.C3060ut> implements com.fossil.blesdk.obfuscated.C3233wp {
    @DexIgnore
    public C3243wt(com.fossil.blesdk.obfuscated.C3060ut utVar) {
        super(utVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8887a() {
        ((com.fossil.blesdk.obfuscated.C3060ut) this.f7580e).stop();
        ((com.fossil.blesdk.obfuscated.C3060ut) this.f7580e).mo16873k();
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return ((com.fossil.blesdk.obfuscated.C3060ut) this.f7580e).mo16870i();
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<com.fossil.blesdk.obfuscated.C3060ut> mo8889c() {
        return com.fossil.blesdk.obfuscated.C3060ut.class;
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10116d() {
        ((com.fossil.blesdk.obfuscated.C3060ut) this.f7580e).mo16862e().prepareToDraw();
    }
}
