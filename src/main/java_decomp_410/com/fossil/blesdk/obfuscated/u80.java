package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u80 extends r80 {
    @DexIgnore
    public short Q;
    @DexIgnore
    public long R;
    @DexIgnore
    public /* final */ byte[] S;
    @DexIgnore
    public byte[] T;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ u80(short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] D() {
        return this.S;
    }

    @DexIgnore
    public byte[] G() {
        return this.T;
    }

    @DexIgnore
    public final short K() {
        return this.Q;
    }

    @DexIgnore
    public final long L() {
        return this.R;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        kd4.b(bArr, "<set-?>");
        this.T = bArr;
    }

    @DexIgnore
    public void c(byte[] bArr) {
        kd4.b(bArr, "receivedData");
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.wrap(received\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.Q = n90.b(order.get(0));
        this.R = n90.b(order.getInt(1));
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.FILE_HANDLE, n90.a(I()));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.NUMBER_OF_FILES, Short.valueOf(this.Q)), JSONKey.TOTAL_FILE_SIZE, Long.valueOf(this.R));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u80(short s, Peripheral peripheral, int i) {
        super(LegacyFileControlOperationCode.LEGACY_LIST_FILE, s, RequestId.LEGACY_LIST_FILE, peripheral, i);
        kd4.b(peripheral, "peripheral");
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_LIST_FILE.getCode()).array();
        kd4.a((Object) array, "ByteBuffer.allocate(1)\n \u2026ode)\n            .array()");
        this.S = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_LIST_FILE.responseCode()).array();
        kd4.a((Object) array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.T = array2;
    }
}
