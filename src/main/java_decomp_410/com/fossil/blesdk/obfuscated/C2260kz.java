package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.kz */
public class C2260kz implements com.fossil.blesdk.obfuscated.e00 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f7037a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.String f7038b;

    @DexIgnore
    public C2260kz(android.content.Context context, java.lang.String str) {
        this.f7037a = context;
        this.f7038b = str;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo10253a() {
        try {
            android.os.Bundle bundle = this.f7037a.getPackageManager().getApplicationInfo(this.f7038b, 128).metaData;
            if (bundle != null) {
                return bundle.getString("io.fabric.unity.crashlytics.version");
            }
            return null;
        } catch (java.lang.Exception unused) {
            return null;
        }
    }
}
