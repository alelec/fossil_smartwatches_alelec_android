package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ql */
public class C2747ql {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f8704a;

    @DexIgnore
    /* renamed from: b */
    public android.content.SharedPreferences f8705b;

    @DexIgnore
    /* renamed from: c */
    public boolean f8706c;

    @DexIgnore
    public C2747ql(android.content.Context context) {
        this.f8704a = context;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo15268a(int i, int i2) {
        synchronized (com.fossil.blesdk.obfuscated.C2747ql.class) {
            mo15270a();
            int a = mo15269a("next_job_scheduler_id");
            if (a >= i) {
                if (a <= i2) {
                    i = a;
                }
            }
            mo15271a("next_job_scheduler_id", i + 1);
        }
        return i;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15272b() {
        int a;
        synchronized (com.fossil.blesdk.obfuscated.C2747ql.class) {
            mo15270a();
            a = mo15269a("next_alarm_manager_id");
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public final int mo15269a(java.lang.String str) {
        int i = 0;
        int i2 = this.f8705b.getInt(str, 0);
        if (i2 != Integer.MAX_VALUE) {
            i = i2 + 1;
        }
        mo15271a(str, i);
        return i2;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15271a(java.lang.String str, int i) {
        this.f8705b.edit().putInt(str, i).apply();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo15270a() {
        if (!this.f8706c) {
            this.f8705b = this.f8704a.getSharedPreferences("androidx.work.util.id", 0);
            this.f8706c = true;
        }
    }
}
