package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sq1 {
    @DexIgnore
    public static /* final */ int design_appbar_state_list_animator; // = 2130837504;
    @DexIgnore
    public static /* final */ int design_fab_hide_motion_spec; // = 2130837505;
    @DexIgnore
    public static /* final */ int design_fab_show_motion_spec; // = 2130837506;
    @DexIgnore
    public static /* final */ int mtrl_btn_state_list_anim; // = 2130837507;
    @DexIgnore
    public static /* final */ int mtrl_btn_unelevated_state_list_anim; // = 2130837508;
    @DexIgnore
    public static /* final */ int mtrl_chip_state_list_anim; // = 2130837509;
    @DexIgnore
    public static /* final */ int mtrl_fab_hide_motion_spec; // = 2130837510;
    @DexIgnore
    public static /* final */ int mtrl_fab_show_motion_spec; // = 2130837511;
    @DexIgnore
    public static /* final */ int mtrl_fab_transformation_sheet_collapse_spec; // = 2130837512;
    @DexIgnore
    public static /* final */ int mtrl_fab_transformation_sheet_expand_spec; // = 2130837513;
}
