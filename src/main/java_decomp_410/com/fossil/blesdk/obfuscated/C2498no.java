package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.no */
public interface C2498no<T> extends com.fossil.blesdk.obfuscated.C1963ho<com.fossil.blesdk.obfuscated.C1438aq<T>> {
    @DexIgnore
    /* renamed from: a */
    com.bumptech.glide.load.EncodeStrategy mo13706a(com.fossil.blesdk.obfuscated.C2337lo loVar);
}
