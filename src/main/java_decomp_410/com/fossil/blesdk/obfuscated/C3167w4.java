package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w4 */
public class C3167w4 {
    @DexIgnore
    /* renamed from: a */
    public static void m15576a(com.fossil.blesdk.obfuscated.C3342y4 y4Var, com.fossil.blesdk.obfuscated.C2703q4 q4Var, int i) {
        int i2;
        com.fossil.blesdk.obfuscated.C3261x4[] x4VarArr;
        int i3;
        if (i == 0) {
            int i4 = y4Var.f11170s0;
            x4VarArr = y4Var.f11173v0;
            i2 = i4;
            i3 = 0;
        } else {
            i3 = 2;
            int i5 = y4Var.f11171t0;
            i2 = i5;
            x4VarArr = y4Var.f11172u0;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            com.fossil.blesdk.obfuscated.C3261x4 x4Var = x4VarArr[i6];
            x4Var.mo17599a();
            if (!y4Var.mo18016u(4)) {
                m15577a(y4Var, q4Var, i, i3, x4Var);
            } else if (!com.fossil.blesdk.obfuscated.C1536c5.m5287a(y4Var, q4Var, i, i3, x4Var)) {
                m15577a(y4Var, q4Var, i, i3, x4Var);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r2.f698e0 == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
        if (r2.f700f0 == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004c, code lost:
        r5 = false;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:190:0x0366  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0386  */
    /* JADX WARNING: Removed duplicated region for block: B:251:0x0454  */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x04ae  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x04b1  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x04b7  */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x04ba  */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:278:0x04ce  */
    /* JADX WARNING: Removed duplicated region for block: B:293:0x0367 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x016b  */
    /* renamed from: a */
    public static void m15577a(com.fossil.blesdk.obfuscated.C3342y4 y4Var, com.fossil.blesdk.obfuscated.C2703q4 q4Var, int i, int i2, com.fossil.blesdk.obfuscated.C3261x4 x4Var) {
        boolean z;
        boolean z2;
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList;
        androidx.constraintlayout.solver.SolverVariable solverVariable;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor3;
        int i3;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2;
        int i4;
        androidx.constraintlayout.solver.SolverVariable solverVariable2;
        androidx.constraintlayout.solver.SolverVariable solverVariable3;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor4;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget4;
        androidx.constraintlayout.solver.SolverVariable solverVariable4;
        androidx.constraintlayout.solver.SolverVariable solverVariable5;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor5;
        float f;
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList2;
        int i5;
        float f2;
        boolean z3;
        int i6;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget5;
        boolean z4;
        int i7;
        com.fossil.blesdk.obfuscated.C3342y4 y4Var2 = y4Var;
        com.fossil.blesdk.obfuscated.C2703q4 q4Var2 = q4Var;
        com.fossil.blesdk.obfuscated.C3261x4 x4Var2 = x4Var;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget6 = x4Var2.f10827a;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget7 = x4Var2.f10829c;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget8 = x4Var2.f10828b;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget9 = x4Var2.f10830d;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget10 = x4Var2.f10831e;
        float f3 = x4Var2.f10837k;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget11 = x4Var2.f10832f;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget12 = x4Var2.f10833g;
        boolean z5 = y4Var2.f665C[i] == androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (i == 0) {
            z2 = constraintWidget10.f698e0 == 0;
            z = constraintWidget10.f698e0 == 1;
        } else {
            z2 = constraintWidget10.f700f0 == 0;
            z = constraintWidget10.f700f0 == 1;
        }
        boolean z6 = true;
        boolean z7 = z2;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget13 = constraintWidget6;
        boolean z8 = z;
        boolean z9 = z6;
        boolean z10 = false;
        while (true) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget14 = null;
            if (z10) {
                break;
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor6 = constraintWidget13.f663A[i2];
            int i8 = (z5 || z9) ? 1 : 4;
            int b = constraintAnchor6.mo1264b();
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor7 = constraintAnchor6.f652d;
            if (!(constraintAnchor7 == null || constraintWidget13 == constraintWidget6)) {
                b += constraintAnchor7.mo1264b();
            }
            int i9 = b;
            if (z9 && constraintWidget13 != constraintWidget6 && constraintWidget13 != constraintWidget8) {
                f2 = f3;
                z3 = z10;
                i6 = 6;
            } else if (!z7 || !z5) {
                f2 = f3;
                i6 = i8;
                z3 = z10;
            } else {
                f2 = f3;
                z3 = z10;
                i6 = 4;
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor8 = constraintAnchor6.f652d;
            if (constraintAnchor8 != null) {
                if (constraintWidget13 == constraintWidget8) {
                    z4 = z7;
                    constraintWidget5 = constraintWidget10;
                    q4Var2.mo15022b(constraintAnchor6.f657i, constraintAnchor8.f657i, i9, 5);
                } else {
                    constraintWidget5 = constraintWidget10;
                    z4 = z7;
                    q4Var2.mo15022b(constraintAnchor6.f657i, constraintAnchor8.f657i, i9, 6);
                }
                q4Var2.mo15011a(constraintAnchor6.f657i, constraintAnchor6.f652d.f657i, i9, i6);
            } else {
                constraintWidget5 = constraintWidget10;
                z4 = z7;
            }
            if (z5) {
                if (constraintWidget13.mo1350s() == 8 || constraintWidget13.f665C[i] != androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    i7 = 0;
                } else {
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr = constraintWidget13.f663A;
                    i7 = 0;
                    q4Var2.mo15022b(constraintAnchorArr[i2 + 1].f657i, constraintAnchorArr[i2].f657i, 0, 5);
                }
                q4Var2.mo15022b(constraintWidget13.f663A[i2].f657i, y4Var2.f663A[i2].f657i, i7, 6);
            }
            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor9 = constraintWidget13.f663A[i2 + 1].f652d;
            if (constraintAnchor9 != null) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget15 = constraintAnchor9.f650b;
                androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr2 = constraintWidget15.f663A;
                if (constraintAnchorArr2[i2].f652d != null && constraintAnchorArr2[i2].f652d.f650b == constraintWidget13) {
                    constraintWidget14 = constraintWidget15;
                }
            }
            if (constraintWidget14 != null) {
                constraintWidget13 = constraintWidget14;
                z10 = z3;
            } else {
                z10 = true;
            }
            f3 = f2;
            z7 = z4;
            constraintWidget10 = constraintWidget5;
        }
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget16 = constraintWidget10;
        float f4 = f3;
        boolean z11 = z7;
        if (constraintWidget9 != null) {
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr3 = constraintWidget7.f663A;
            int i10 = i2 + 1;
            if (constraintAnchorArr3[i10].f652d != null) {
                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor10 = constraintWidget9.f663A[i10];
                q4Var2.mo15027c(constraintAnchor10.f657i, constraintAnchorArr3[i10].f652d.f657i, -constraintAnchor10.mo1264b(), 5);
                if (z5) {
                    int i11 = i2 + 1;
                    androidx.constraintlayout.solver.SolverVariable solverVariable6 = y4Var2.f663A[i11].f657i;
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr4 = constraintWidget7.f663A;
                    q4Var2.mo15022b(solverVariable6, constraintAnchorArr4[i11].f657i, constraintAnchorArr4[i11].mo1264b(), 6);
                }
                arrayList = x4Var2.f10834h;
                if (arrayList != null) {
                    int size = arrayList.size();
                    if (size > 1) {
                        float f5 = (!x4Var2.f10840n || x4Var2.f10842p) ? f4 : (float) x4Var2.f10836j;
                        float f6 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget17 = null;
                        int i12 = 0;
                        float f7 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                        while (i12 < size) {
                            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget18 = arrayList.get(i12);
                            float f8 = constraintWidget18.f702g0[i];
                            if (f8 < f6) {
                                if (x4Var2.f10842p) {
                                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr5 = constraintWidget18.f663A;
                                    q4Var2.mo15011a(constraintAnchorArr5[i2 + 1].f657i, constraintAnchorArr5[i2].f657i, 0, 4);
                                    arrayList2 = arrayList;
                                    i5 = size;
                                    i12++;
                                    size = i5;
                                    arrayList = arrayList2;
                                    f6 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                } else {
                                    f8 = 1.0f;
                                    f6 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                                }
                            }
                            if (f8 == f6) {
                                androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr6 = constraintWidget18.f663A;
                                q4Var2.mo15011a(constraintAnchorArr6[i2 + 1].f657i, constraintAnchorArr6[i2].f657i, 0, 6);
                                arrayList2 = arrayList;
                                i5 = size;
                                i12++;
                                size = i5;
                                arrayList = arrayList2;
                                f6 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            } else {
                                if (constraintWidget17 != null) {
                                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr7 = constraintWidget17.f663A;
                                    androidx.constraintlayout.solver.SolverVariable solverVariable7 = constraintAnchorArr7[i2].f657i;
                                    int i13 = i2 + 1;
                                    androidx.constraintlayout.solver.SolverVariable solverVariable8 = constraintAnchorArr7[i13].f657i;
                                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr8 = constraintWidget18.f663A;
                                    arrayList2 = arrayList;
                                    androidx.constraintlayout.solver.SolverVariable solverVariable9 = constraintAnchorArr8[i2].f657i;
                                    androidx.constraintlayout.solver.SolverVariable solverVariable10 = constraintAnchorArr8[i13].f657i;
                                    i5 = size;
                                    com.fossil.blesdk.obfuscated.C2459n4 c = q4Var.mo15026c();
                                    c.mo13831a(f7, f5, f8, solverVariable7, solverVariable8, solverVariable9, solverVariable10);
                                    q4Var2.mo15018a(c);
                                } else {
                                    arrayList2 = arrayList;
                                    i5 = size;
                                }
                                f7 = f8;
                                constraintWidget17 = constraintWidget18;
                                i12++;
                                size = i5;
                                arrayList = arrayList2;
                                f6 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                            }
                        }
                    }
                }
                if (constraintWidget8 == null && (constraintWidget8 == constraintWidget9 || z9)) {
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr9 = constraintWidget6.f663A;
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor11 = constraintAnchorArr9[i2];
                    int i14 = i2 + 1;
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor12 = constraintWidget7.f663A[i14];
                    androidx.constraintlayout.solver.SolverVariable solverVariable11 = constraintAnchorArr9[i2].f652d != null ? constraintAnchorArr9[i2].f652d.f657i : null;
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr10 = constraintWidget7.f663A;
                    androidx.constraintlayout.solver.SolverVariable solverVariable12 = constraintAnchorArr10[i14].f652d != null ? constraintAnchorArr10[i14].f652d.f657i : null;
                    if (constraintWidget8 == constraintWidget9) {
                        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr11 = constraintWidget8.f663A;
                        constraintAnchor11 = constraintAnchorArr11[i2];
                        constraintAnchor12 = constraintAnchorArr11[i14];
                    }
                    if (!(solverVariable11 == null || solverVariable12 == null)) {
                        if (i == 0) {
                            f = constraintWidget16.f684V;
                        } else {
                            f = constraintWidget16.f685W;
                        }
                        q4Var.mo15014a(constraintAnchor11.f657i, solverVariable11, constraintAnchor11.mo1264b(), f, solverVariable12, constraintAnchor12.f657i, constraintAnchor12.mo1264b(), 5);
                    }
                } else if (z11 || constraintWidget8 == null) {
                    int i15 = 8;
                    if (z8 && constraintWidget8 != null) {
                        int i16 = x4Var2.f10836j;
                        boolean z12 = i16 <= 0 && x4Var2.f10835i == i16;
                        constraintWidget = constraintWidget8;
                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget19 = constraintWidget;
                        while (constraintWidget != null) {
                            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget20 = constraintWidget.f706i0[i];
                            while (constraintWidget20 != null && constraintWidget20.mo1350s() == i15) {
                                constraintWidget20 = constraintWidget20.f706i0[i];
                            }
                            if (constraintWidget == constraintWidget8 || constraintWidget == constraintWidget9 || constraintWidget20 == null) {
                                constraintWidget2 = constraintWidget19;
                                i4 = 8;
                            } else {
                                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget21 = constraintWidget20 == constraintWidget9 ? null : constraintWidget20;
                                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor13 = constraintWidget.f663A[i2];
                                androidx.constraintlayout.solver.SolverVariable solverVariable13 = constraintAnchor13.f657i;
                                androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor14 = constraintAnchor13.f652d;
                                if (constraintAnchor14 != null) {
                                    androidx.constraintlayout.solver.SolverVariable solverVariable14 = constraintAnchor14.f657i;
                                }
                                int i17 = i2 + 1;
                                androidx.constraintlayout.solver.SolverVariable solverVariable15 = constraintWidget19.f663A[i17].f657i;
                                int b2 = constraintAnchor13.mo1264b();
                                int b3 = constraintWidget.f663A[i17].mo1264b();
                                if (constraintWidget21 != null) {
                                    constraintAnchor4 = constraintWidget21.f663A[i2];
                                    solverVariable3 = constraintAnchor4.f657i;
                                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor15 = constraintAnchor4.f652d;
                                    solverVariable2 = constraintAnchor15 != null ? constraintAnchor15.f657i : null;
                                } else {
                                    constraintAnchor4 = constraintWidget.f663A[i17].f652d;
                                    solverVariable3 = constraintAnchor4 != null ? constraintAnchor4.f657i : null;
                                    solverVariable2 = constraintWidget.f663A[i17].f657i;
                                }
                                if (constraintAnchor4 != null) {
                                    b3 += constraintAnchor4.mo1264b();
                                }
                                int i18 = b3;
                                if (constraintWidget19 != null) {
                                    b2 += constraintWidget19.f663A[i17].mo1264b();
                                }
                                int i19 = b2;
                                int i20 = z12 ? 6 : 4;
                                if (solverVariable13 == null || solverVariable15 == null || solverVariable3 == null || solverVariable2 == null) {
                                    constraintWidget3 = constraintWidget21;
                                    constraintWidget2 = constraintWidget19;
                                    i4 = 8;
                                } else {
                                    constraintWidget3 = constraintWidget21;
                                    int i21 = i18;
                                    constraintWidget2 = constraintWidget19;
                                    i4 = 8;
                                    q4Var.mo15014a(solverVariable13, solverVariable15, i19, 0.5f, solverVariable3, solverVariable2, i21, i20);
                                }
                                constraintWidget20 = constraintWidget3;
                            }
                            if (constraintWidget.mo1350s() == i4) {
                                constraintWidget = constraintWidget2;
                            }
                            constraintWidget19 = constraintWidget;
                            i15 = 8;
                            constraintWidget = constraintWidget20;
                        }
                        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor16 = constraintWidget8.f663A[i2];
                        constraintAnchor = constraintWidget6.f663A[i2].f652d;
                        int i22 = i2 + 1;
                        constraintAnchor2 = constraintWidget9.f663A[i22];
                        constraintAnchor3 = constraintWidget7.f663A[i22].f652d;
                        if (constraintAnchor != null) {
                            i3 = 5;
                        } else if (constraintWidget8 != constraintWidget9) {
                            i3 = 5;
                            q4Var2.mo15011a(constraintAnchor16.f657i, constraintAnchor.f657i, constraintAnchor16.mo1264b(), 5);
                        } else {
                            i3 = 5;
                            if (constraintAnchor3 != null) {
                                q4Var.mo15014a(constraintAnchor16.f657i, constraintAnchor.f657i, constraintAnchor16.mo1264b(), 0.5f, constraintAnchor2.f657i, constraintAnchor3.f657i, constraintAnchor2.mo1264b(), 5);
                            }
                        }
                        if (!(constraintAnchor3 == null || constraintWidget8 == constraintWidget9)) {
                            q4Var2.mo15011a(constraintAnchor2.f657i, constraintAnchor3.f657i, -constraintAnchor2.mo1264b(), i3);
                        }
                    }
                } else {
                    int i23 = x4Var2.f10836j;
                    boolean z13 = i23 > 0 && x4Var2.f10835i == i23;
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget22 = constraintWidget8;
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget23 = constraintWidget22;
                    while (constraintWidget22 != null) {
                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget24 = constraintWidget22.f706i0[i];
                        while (true) {
                            if (constraintWidget24 != null) {
                                if (constraintWidget24.mo1350s() != 8) {
                                    break;
                                }
                                constraintWidget24 = constraintWidget24.f706i0[i];
                            } else {
                                break;
                            }
                        }
                        if (constraintWidget24 != null || constraintWidget22 == constraintWidget9) {
                            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor17 = constraintWidget22.f663A[i2];
                            androidx.constraintlayout.solver.SolverVariable solverVariable16 = constraintAnchor17.f657i;
                            androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor18 = constraintAnchor17.f652d;
                            androidx.constraintlayout.solver.SolverVariable solverVariable17 = constraintAnchor18 != null ? constraintAnchor18.f657i : null;
                            if (constraintWidget23 != constraintWidget22) {
                                solverVariable17 = constraintWidget23.f663A[i2 + 1].f657i;
                            } else if (constraintWidget22 == constraintWidget8 && constraintWidget23 == constraintWidget22) {
                                androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr12 = constraintWidget6.f663A;
                                solverVariable17 = constraintAnchorArr12[i2].f652d != null ? constraintAnchorArr12[i2].f652d.f657i : null;
                            }
                            int b4 = constraintAnchor17.mo1264b();
                            int i24 = i2 + 1;
                            int b5 = constraintWidget22.f663A[i24].mo1264b();
                            if (constraintWidget24 != null) {
                                constraintAnchor5 = constraintWidget24.f663A[i2];
                                solverVariable5 = constraintAnchor5.f657i;
                                solverVariable4 = constraintWidget22.f663A[i24].f657i;
                            } else {
                                constraintAnchor5 = constraintWidget7.f663A[i24].f652d;
                                solverVariable5 = constraintAnchor5 != null ? constraintAnchor5.f657i : null;
                                solverVariable4 = constraintWidget22.f663A[i24].f657i;
                            }
                            if (constraintAnchor5 != null) {
                                b5 += constraintAnchor5.mo1264b();
                            }
                            if (constraintWidget23 != null) {
                                b4 += constraintWidget23.f663A[i24].mo1264b();
                            }
                            if (!(solverVariable16 == null || solverVariable17 == null || solverVariable5 == null || solverVariable4 == null)) {
                                if (constraintWidget22 == constraintWidget8) {
                                    b4 = constraintWidget8.f663A[i2].mo1264b();
                                }
                                int i25 = b4;
                                int b6 = constraintWidget22 == constraintWidget9 ? constraintWidget9.f663A[i24].mo1264b() : b5;
                                int i26 = i25;
                                androidx.constraintlayout.solver.SolverVariable solverVariable18 = solverVariable5;
                                androidx.constraintlayout.solver.SolverVariable solverVariable19 = solverVariable4;
                                int i27 = b6;
                                constraintWidget4 = constraintWidget24;
                                q4Var.mo15014a(solverVariable16, solverVariable17, i26, 0.5f, solverVariable18, solverVariable19, i27, z13 ? 6 : 4);
                                if (constraintWidget22.mo1350s() == 8) {
                                    constraintWidget23 = constraintWidget22;
                                }
                                constraintWidget22 = constraintWidget4;
                            }
                        }
                        constraintWidget4 = constraintWidget24;
                        if (constraintWidget22.mo1350s() == 8) {
                        }
                        constraintWidget22 = constraintWidget4;
                    }
                }
                if ((!z11 || z8) && constraintWidget8 != null) {
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor19 = constraintWidget8.f663A[i2];
                    int i28 = i2 + 1;
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor20 = constraintWidget9.f663A[i28];
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor21 = constraintAnchor19.f652d;
                    solverVariable = constraintAnchor21 == null ? constraintAnchor21.f657i : null;
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor22 = constraintAnchor20.f652d;
                    androidx.constraintlayout.solver.SolverVariable solverVariable20 = constraintAnchor22 == null ? constraintAnchor22.f657i : null;
                    if (constraintWidget7 != constraintWidget9) {
                        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor23 = constraintWidget7.f663A[i28].f652d;
                        solverVariable20 = constraintAnchor23 != null ? constraintAnchor23.f657i : null;
                    }
                    androidx.constraintlayout.solver.SolverVariable solverVariable21 = solverVariable20;
                    if (constraintWidget8 == constraintWidget9) {
                        androidx.constraintlayout.solver.widgets.ConstraintAnchor[] constraintAnchorArr13 = constraintWidget8.f663A;
                        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor24 = constraintAnchorArr13[i2];
                        constraintAnchor20 = constraintAnchorArr13[i28];
                        constraintAnchor19 = constraintAnchor24;
                    }
                    if (solverVariable != null && solverVariable21 != null) {
                        int b7 = constraintAnchor19.mo1264b();
                        if (constraintWidget9 != null) {
                            constraintWidget7 = constraintWidget9;
                        }
                        q4Var.mo15014a(constraintAnchor19.f657i, solverVariable, b7, 0.5f, solverVariable21, constraintAnchor20.f657i, constraintWidget7.f663A[i28].mo1264b(), 5);
                        return;
                    }
                }
                return;
            }
        }
        if (z5) {
        }
        arrayList = x4Var2.f10834h;
        if (arrayList != null) {
        }
        if (constraintWidget8 == null) {
        }
        if (z11) {
        }
        int i152 = 8;
        int i162 = x4Var2.f10836j;
        if (i162 <= 0) {
        }
        constraintWidget = constraintWidget8;
        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget192 = constraintWidget;
        while (constraintWidget != null) {
        }
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor162 = constraintWidget8.f663A[i2];
        constraintAnchor = constraintWidget6.f663A[i2].f652d;
        int i222 = i2 + 1;
        constraintAnchor2 = constraintWidget9.f663A[i222];
        constraintAnchor3 = constraintWidget7.f663A[i222].f652d;
        if (constraintAnchor != null) {
        }
        q4Var2.mo15011a(constraintAnchor2.f657i, constraintAnchor3.f657i, -constraintAnchor2.mo1264b(), i3);
        if (!z11) {
        }
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor192 = constraintWidget8.f663A[i2];
        int i282 = i2 + 1;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor202 = constraintWidget9.f663A[i282];
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor212 = constraintAnchor192.f652d;
        if (constraintAnchor212 == null) {
        }
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor222 = constraintAnchor202.f652d;
        if (constraintAnchor222 == null) {
        }
        if (constraintWidget7 != constraintWidget9) {
        }
        androidx.constraintlayout.solver.SolverVariable solverVariable212 = solverVariable20;
        if (constraintWidget8 == constraintWidget9) {
        }
        if (solverVariable != null) {
        }
    }
}
