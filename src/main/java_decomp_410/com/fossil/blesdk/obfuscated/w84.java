package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w84 {
    @DexIgnore
    public static /* final */ r84 a; // = v84.b(new a());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Callable<r84> {
        @DexIgnore
        public r84 call() throws Exception {
            return b.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ r84 a; // = new x84(new Handler(Looper.getMainLooper()));
    }

    @DexIgnore
    public static r84 a() {
        return v84.a(a);
    }
}
