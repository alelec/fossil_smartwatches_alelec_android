package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class br2 implements Factory<UnlinkDeviceUseCase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<vj2> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> d;

    @DexIgnore
    public br2(Provider<DeviceRepository> provider, Provider<vj2> provider2, Provider<PortfolioApp> provider3, Provider<WatchFaceRepository> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static br2 a(Provider<DeviceRepository> provider, Provider<vj2> provider2, Provider<PortfolioApp> provider3, Provider<WatchFaceRepository> provider4) {
        return new br2(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static UnlinkDeviceUseCase b(Provider<DeviceRepository> provider, Provider<vj2> provider2, Provider<PortfolioApp> provider3, Provider<WatchFaceRepository> provider4) {
        return new UnlinkDeviceUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public UnlinkDeviceUseCase get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
