package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gj2 extends cj2 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Bundle c;

    @DexIgnore
    public gj2(String str, int i, Bundle bundle) {
        this.a = i;
        this.b = str;
        this.c = bundle;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public Bundle b() {
        return this.c;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }
}
