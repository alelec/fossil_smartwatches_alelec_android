package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lz2 implements Factory<NotificationContactsAndAppsAssignedPresenter> {
    @DexIgnore
    public static NotificationContactsAndAppsAssignedPresenter a(LoaderManager loaderManager, gz2 gz2, int i, j62 j62, t03 t03, h03 h03, e13 e13, vq2 vq2) {
        return new NotificationContactsAndAppsAssignedPresenter(loaderManager, gz2, i, j62, t03, h03, e13, vq2);
    }
}
