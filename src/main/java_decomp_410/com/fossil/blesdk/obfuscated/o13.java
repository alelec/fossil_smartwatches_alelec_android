package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.j73;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o13 extends zr2 {
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public j42 j;
    @DexIgnore
    public tr3<sa2> k;
    @DexIgnore
    public j73 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final o13 a(String str) {
            kd4.b(str, "watchAppId");
            o13 o13 = new o13();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WATCHAPP_ID", str);
            o13.setArguments(bundle);
            return o13;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ o13 e;

        @DexIgnore
        public b(o13 o13) {
            this.e = o13;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<j73.b> {
        @DexIgnore
        public /* final */ /* synthetic */ o13 a;
        @DexIgnore
        public /* final */ /* synthetic */ sa2 b;

        @DexIgnore
        public c(o13 o13, sa2 sa2) {
            this.a = o13;
            this.b = sa2;
        }

        @DexIgnore
        public final void a(j73.b bVar) {
            Integer a2 = bVar.a();
            if (a2 != null) {
                ((wn) ((wn) rn.a((Fragment) this.a).a(Integer.valueOf(a2.intValue())).a(pp.a)).a(true)).a(this.b.r);
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        sa2 sa2 = (sa2) qa.a(layoutInflater, R.layout.fragment_customize_tutorial, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new i73()).a(this);
        j42 j42 = this.j;
        if (j42 != null) {
            ic a2 = lc.a((Fragment) this, (kc.b) j42).a(j73.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ialViewModel::class.java)");
            this.l = (j73) a2;
            sa2.q.setOnClickListener(new b(this));
            Bundle arguments = getArguments();
            if (arguments != null) {
                j73 j73 = this.l;
                if (j73 != null) {
                    String string = arguments.getString("EXTRA_WATCHAPP_ID");
                    if (string != null) {
                        kd4.a((Object) string, "it.getString(EXTRA_WATCHAPP_ID)!!");
                        j73.b(string);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.d("mViewModel");
                    throw null;
                }
            }
            j73 j732 = this.l;
            if (j732 != null) {
                j732.c().a(getViewLifecycleOwner(), new c(this, sa2));
                this.k = new tr3<>(this, sa2);
                tr3<sa2> tr3 = this.k;
                if (tr3 != null) {
                    sa2 a3 = tr3.a();
                    if (a3 != null) {
                        kd4.a((Object) a3, "mBinding.get()!!");
                        return a3.d();
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.d("mViewModel");
            throw null;
        }
        kd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }
}
