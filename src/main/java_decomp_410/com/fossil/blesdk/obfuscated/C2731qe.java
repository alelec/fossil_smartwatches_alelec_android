package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qe */
public class C2731qe implements com.fossil.blesdk.obfuscated.C2646pe {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2646pe f8641a; // = new com.fossil.blesdk.obfuscated.C2731qe();

    @DexIgnore
    /* renamed from: a */
    public static float m12801a(androidx.recyclerview.widget.RecyclerView recyclerView, android.view.View view) {
        int childCount = recyclerView.getChildCount();
        float f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (int i = 0; i < childCount; i++) {
            android.view.View childAt = recyclerView.getChildAt(i);
            if (childAt != view) {
                float g = com.fossil.blesdk.obfuscated.C1776f9.m6839g(childAt);
                if (g > f) {
                    f = g;
                }
            }
        }
        return f;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14671a(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, android.view.View view, float f, float f2, int i, boolean z) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14673b(android.graphics.Canvas canvas, androidx.recyclerview.widget.RecyclerView recyclerView, android.view.View view, float f, float f2, int i, boolean z) {
        if (android.os.Build.VERSION.SDK_INT >= 21 && z && view.getTag(com.fossil.blesdk.obfuscated.C1615de.item_touch_helper_previous_elevation) == null) {
            java.lang.Float valueOf = java.lang.Float.valueOf(com.fossil.blesdk.obfuscated.C1776f9.m6839g(view));
            com.fossil.blesdk.obfuscated.C1776f9.m6824b(view, m12801a(recyclerView, view) + 1.0f);
            view.setTag(com.fossil.blesdk.obfuscated.C1615de.item_touch_helper_previous_elevation, valueOf);
        }
        view.setTranslationX(f);
        view.setTranslationY(f2);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14674b(android.view.View view) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14672a(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            java.lang.Object tag = view.getTag(com.fossil.blesdk.obfuscated.C1615de.item_touch_helper_previous_elevation);
            if (tag != null && (tag instanceof java.lang.Float)) {
                com.fossil.blesdk.obfuscated.C1776f9.m6824b(view, ((java.lang.Float) tag).floatValue());
            }
            view.setTag(com.fossil.blesdk.obfuscated.C1615de.item_touch_helper_previous_elevation, (java.lang.Object) null);
        }
        view.setTranslationX(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }
}
