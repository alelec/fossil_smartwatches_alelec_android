package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vh {
    @DexIgnore
    public static uh a(ViewGroup viewGroup) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new th(viewGroup);
        }
        return sh.a(viewGroup);
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, boolean z) {
        if (Build.VERSION.SDK_INT >= 18) {
            xh.a(viewGroup, z);
        } else {
            wh.a(viewGroup, z);
        }
    }
}
