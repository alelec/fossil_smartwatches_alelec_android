package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gw2 implements Factory<NotificationAppsPresenter> {
    @DexIgnore
    public static NotificationAppsPresenter a(cw2 cw2, j62 j62, vy2 vy2, px2 px2, iw2 iw2, en2 en2, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationAppsPresenter(cw2, j62, vy2, px2, iw2, en2, notificationSettingsDatabase);
    }
}
