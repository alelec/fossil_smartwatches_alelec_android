package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qt0 extends mt0<Boolean> implements wu0<Boolean>, RandomAccess {
    @DexIgnore
    public boolean[] f;
    @DexIgnore
    public int g;

    /*
    static {
        new qt0().z();
    }
    */

    @DexIgnore
    public qt0() {
        this(new boolean[10], 0);
    }

    @DexIgnore
    public qt0(boolean[] zArr, int i) {
        this.f = zArr;
        this.g = i;
    }

    @DexIgnore
    public final void a(int i) {
        if (i < 0 || i >= this.g) {
            throw new IndexOutOfBoundsException(f(i));
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        a();
        if (i >= 0) {
            int i2 = this.g;
            if (i <= i2) {
                boolean[] zArr = this.f;
                if (i2 < zArr.length) {
                    System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
                } else {
                    boolean[] zArr2 = new boolean[(((i2 * 3) / 2) + 1)];
                    System.arraycopy(zArr, 0, zArr2, 0, i);
                    System.arraycopy(this.f, i, zArr2, i + 1, this.g - i);
                    this.f = zArr2;
                }
                this.f[i] = z;
                this.g++;
                this.modCount++;
                return;
            }
        }
        throw new IndexOutOfBoundsException(f(i));
    }

    @DexIgnore
    public final void a(boolean z) {
        a(this.g, z);
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a(i, ((Boolean) obj).booleanValue());
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends Boolean> collection) {
        a();
        tu0.a(collection);
        if (!(collection instanceof qt0)) {
            return super.addAll(collection);
        }
        qt0 qt0 = (qt0) collection;
        int i = qt0.g;
        if (i == 0) {
            return false;
        }
        int i2 = this.g;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.f;
            if (i3 > zArr.length) {
                this.f = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(qt0.f, 0, this.f, this.g, qt0.g);
            this.g = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final /* synthetic */ wu0 c(int i) {
        if (i >= this.g) {
            return new qt0(Arrays.copyOf(this.f, i), this.g);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qt0)) {
            return super.equals(obj);
        }
        qt0 qt0 = (qt0) obj;
        if (this.g != qt0.g) {
            return false;
        }
        boolean[] zArr = qt0.f;
        for (int i = 0; i < this.g; i++) {
            if (this.f[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final String f(int i) {
        int i2 = this.g;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        a(i);
        return Boolean.valueOf(this.f[i]);
    }

    @DexIgnore
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.g; i2++) {
            i = (i * 31) + tu0.a(this.f[i2]);
        }
        return i;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        a(i);
        boolean[] zArr = this.f;
        boolean z = zArr[i];
        int i2 = this.g;
        if (i < i2 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, i2 - i);
        }
        this.g--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.g; i++) {
            if (obj.equals(Boolean.valueOf(this.f[i]))) {
                boolean[] zArr = this.f;
                System.arraycopy(zArr, i + 1, zArr, i, this.g - i);
                this.g--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            boolean[] zArr = this.f;
            System.arraycopy(zArr, i2, zArr, i, this.g - i2);
            this.g -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        a();
        a(i);
        boolean[] zArr = this.f;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public final int size() {
        return this.g;
    }
}
