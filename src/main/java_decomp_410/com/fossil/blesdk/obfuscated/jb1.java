package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jb1 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> e; // = this.f.e.iterator();
    @DexIgnore
    public /* final */ /* synthetic */ hb1 f;

    @DexIgnore
    public jb1(hb1 hb1) {
        this.f = hb1;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.e.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
