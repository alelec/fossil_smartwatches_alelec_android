package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mg0 {
    @DexIgnore
    <A extends de0.b, T extends te0<? extends me0, A>> T a(T t);

    @DexIgnore
    void a(ud0 ud0, de0<?> de0, boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void e(Bundle bundle);

    @DexIgnore
    void f(int i);
}
