package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo2 implements Factory<SmsMmsReceiver> {
    @DexIgnore
    public /* final */ Provider<DianaNotificationComponent> a;
    @DexIgnore
    public /* final */ Provider<en2> b;

    @DexIgnore
    public mo2(Provider<DianaNotificationComponent> provider, Provider<en2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static mo2 a(Provider<DianaNotificationComponent> provider, Provider<en2> provider2) {
        return new mo2(provider, provider2);
    }

    @DexIgnore
    public static SmsMmsReceiver b(Provider<DianaNotificationComponent> provider, Provider<en2> provider2) {
        SmsMmsReceiver smsMmsReceiver = new SmsMmsReceiver();
        no2.a(smsMmsReceiver, provider.get());
        no2.a(smsMmsReceiver, provider2.get());
        return smsMmsReceiver;
    }

    @DexIgnore
    public SmsMmsReceiver get() {
        return b(this.a, this.b);
    }
}
