package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bj4 {
    @DexIgnore
    public static /* final */ ThreadLocal<sh4> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ bj4 b; // = new bj4();

    @DexIgnore
    public final sh4 a() {
        return a.get();
    }

    @DexIgnore
    public final sh4 b() {
        sh4 sh4 = a.get();
        if (sh4 != null) {
            return sh4;
        }
        sh4 a2 = vh4.a();
        a.set(a2);
        return a2;
    }

    @DexIgnore
    public final void c() {
        a.set((Object) null);
    }

    @DexIgnore
    public final void a(sh4 sh4) {
        kd4.b(sh4, "eventLoop");
        a.set(sh4);
    }
}
