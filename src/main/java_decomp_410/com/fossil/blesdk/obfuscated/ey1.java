package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ey1 extends Service {
    @DexIgnore
    public /* final */ ExecutorService e;
    @DexIgnore
    public Binder f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;

    @DexIgnore
    public ey1() {
        cz0 a = dz0.a();
        String valueOf = String.valueOf(getClass().getSimpleName());
        this.e = a.a(new um0(valueOf.length() != 0 ? "Firebase-".concat(valueOf) : new String("Firebase-")), 9);
        this.g = new Object();
        this.i = 0;
    }

    @DexIgnore
    public final void a(Intent intent) {
        if (intent != null) {
            lb.a(intent);
        }
        synchronized (this.g) {
            this.i--;
            if (this.i == 0) {
                stopSelfResult(this.h);
            }
        }
    }

    @DexIgnore
    public Intent b(Intent intent) {
        return intent;
    }

    @DexIgnore
    public boolean c(Intent intent) {
        return false;
    }

    @DexIgnore
    public abstract void d(Intent intent);

    @DexIgnore
    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.f == null) {
            this.f = new jy1(this);
        }
        return this.f;
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i2, int i3) {
        synchronized (this.g) {
            this.h = i3;
            this.i++;
        }
        Intent b = b(intent);
        if (b == null) {
            a(intent);
            return 2;
        } else if (c(b)) {
            a(intent);
            return 2;
        } else {
            this.e.execute(new gy1(this, b, intent));
            return 3;
        }
    }
}
