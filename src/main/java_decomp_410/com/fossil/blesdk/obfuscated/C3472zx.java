package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zx */
public class C3472zx extends com.fossil.blesdk.obfuscated.e54 implements com.fossil.blesdk.obfuscated.t64 {

    @DexIgnore
    /* renamed from: g */
    public /* final */ java.lang.String f11666g;

    @DexIgnore
    public C3472zx(com.fossil.blesdk.obfuscated.v44 v44, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.z64 z64, java.lang.String str3) {
        super(v44, str, str2, z64, p011io.fabric.sdk.android.services.network.HttpMethod.POST);
        this.f11666g = str3;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12506a(java.util.List<java.io.File> list) {
        p011io.fabric.sdk.android.services.network.HttpRequest a = mo26836a();
        a.mo44611c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a.mo44611c("X-CRASHLYTICS-API-CLIENT-VERSION", this.f14377e.mo9330r());
        a.mo44611c("X-CRASHLYTICS-API-KEY", this.f11666g);
        int i = 0;
        for (java.io.File next : list) {
            a.mo44600a("session_analytics_file_" + i, next.getName(), "application/vnd.crashlytics.android.events", next);
            i++;
        }
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30060d("Answers", "Sending " + list.size() + " analytics files to " + mo26839b());
        int g2 = a.mo44621g();
        com.fossil.blesdk.obfuscated.y44 g3 = com.fossil.blesdk.obfuscated.q44.m26805g();
        g3.mo30060d("Answers", "Response code for analytics file send is " + g2);
        if (com.fossil.blesdk.obfuscated.w54.m29544a(g2) == 0) {
            return true;
        }
        return false;
    }
}
