package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i1 */
public class C1993i1 implements android.content.DialogInterface.OnKeyListener, android.content.DialogInterface.OnClickListener, android.content.DialogInterface.OnDismissListener, com.fossil.blesdk.obfuscated.C2618p1.C2619a {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1915h1 f5916e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1523c0 f5917f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C1763f1 f5918g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2618p1.C2619a f5919h;

    @DexIgnore
    public C1993i1(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        this.f5916e = h1Var;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11808a(android.os.IBinder iBinder) {
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = this.f5916e;
        com.fossil.blesdk.obfuscated.C1523c0.C1524a aVar = new com.fossil.blesdk.obfuscated.C1523c0.C1524a(h1Var.mo11498e());
        this.f5918g = new com.fossil.blesdk.obfuscated.C1763f1(aVar.mo9369b(), com.fossil.blesdk.obfuscated.C3250x.abc_list_menu_item_layout);
        this.f5918g.mo9013a((com.fossil.blesdk.obfuscated.C2618p1.C2619a) this);
        this.f5916e.mo11461a((com.fossil.blesdk.obfuscated.C2618p1) this.f5918g);
        aVar.mo9364a(this.f5918g.mo10684c(), (android.content.DialogInterface.OnClickListener) this);
        android.view.View i = h1Var.mo11509i();
        if (i != null) {
            aVar.mo9363a(i);
        } else {
            aVar.mo9362a(h1Var.mo11505g());
            aVar.mo9371b(h1Var.mo11507h());
        }
        aVar.mo9361a((android.content.DialogInterface.OnKeyListener) this);
        this.f5917f = aVar.mo9368a();
        this.f5917f.setOnDismissListener(this);
        android.view.WindowManager.LayoutParams attributes = this.f5917f.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.f5917f.show();
    }

    @DexIgnore
    public void onClick(android.content.DialogInterface dialogInterface, int i) {
        this.f5916e.mo11465a((android.view.MenuItem) (com.fossil.blesdk.obfuscated.C2179k1) this.f5918g.mo10684c().getItem(i), 0);
    }

    @DexIgnore
    public void onDismiss(android.content.DialogInterface dialogInterface) {
        this.f5918g.mo1160a(this.f5916e, true);
    }

    @DexIgnore
    public boolean onKey(android.content.DialogInterface dialogInterface, int i, android.view.KeyEvent keyEvent) {
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                android.view.Window window = this.f5917f.getWindow();
                if (window != null) {
                    android.view.View decorView = window.getDecorView();
                    if (decorView != null) {
                        android.view.KeyEvent.DispatcherState keyDispatcherState = decorView.getKeyDispatcherState();
                        if (keyDispatcherState != null) {
                            keyDispatcherState.startTracking(keyEvent, this);
                            return true;
                        }
                    }
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled()) {
                android.view.Window window2 = this.f5917f.getWindow();
                if (window2 != null) {
                    android.view.View decorView2 = window2.getDecorView();
                    if (decorView2 != null) {
                        android.view.KeyEvent.DispatcherState keyDispatcherState2 = decorView2.getKeyDispatcherState();
                        if (keyDispatcherState2 != null && keyDispatcherState2.isTracking(keyEvent)) {
                            this.f5916e.mo11464a(true);
                            dialogInterface.dismiss();
                            return true;
                        }
                    }
                }
            }
        }
        return this.f5916e.performShortcut(i, keyEvent, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11807a() {
        com.fossil.blesdk.obfuscated.C1523c0 c0Var = this.f5917f;
        if (c0Var != null) {
            c0Var.dismiss();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo533a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        if (z || h1Var == this.f5916e) {
            mo11807a();
        }
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f5919h;
        if (aVar != null) {
            aVar.mo533a(h1Var, z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo534a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar = this.f5919h;
        if (aVar != null) {
            return aVar.mo534a(h1Var);
        }
        return false;
    }
}
