package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u43 implements Factory<WatchAppsPresenter> {
    @DexIgnore
    public static WatchAppsPresenter a(q43 q43, CategoryRepository categoryRepository, en2 en2) {
        return new WatchAppsPresenter(q43, categoryRepository, en2);
    }
}
