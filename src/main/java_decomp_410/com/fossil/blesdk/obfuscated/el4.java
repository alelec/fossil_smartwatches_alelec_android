package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.dg4;
import com.fossil.blesdk.obfuscated.sj4;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class el4 implements dl4, bl4<Object, dl4> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(el4.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _state;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends b {
        @DexIgnore
        public /* final */ dg4<qa4> i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Object obj, dg4<? super qa4> dg4) {
            super(obj);
            kd4.b(dg4, "cont");
            this.i = dg4;
        }

        @DexIgnore
        public void a(Object obj) {
            kd4.b(obj, "token");
            this.i.b(obj);
        }

        @DexIgnore
        public Object m() {
            return dg4.a.a(this.i, qa4.a, (Object) null, 2, (Object) null);
        }

        @DexIgnore
        public String toString() {
            return "LockCont[" + this.h + ", " + this.i + ']';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b extends sj4 implements oh4 {
        @DexIgnore
        public /* final */ Object h;

        @DexIgnore
        public b(Object obj) {
            this.h = obj;
        }

        @DexIgnore
        public abstract void a(Object obj);

        @DexIgnore
        public final void dispose() {
            j();
        }

        @DexIgnore
        public abstract Object m();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends qj4 {
        @DexIgnore
        public Object h;

        @DexIgnore
        public c(Object obj) {
            kd4.b(obj, "owner");
            this.h = obj;
        }

        @DexIgnore
        public String toString() {
            return "LockedQueue[" + this.h + ']';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends yj4 {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public d(c cVar) {
            kd4.b(cVar, "queue");
            this.a = cVar;
        }

        @DexIgnore
        public Object a(Object obj) {
            Object b = this.a.m() ? fl4.e : this.a;
            if (obj != null) {
                el4 el4 = (el4) obj;
                el4.a.compareAndSet(el4, this, b);
                if (el4._state == this.a) {
                    return fl4.a;
                }
                return null;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends sj4.a {
        @DexIgnore
        public /* final */ /* synthetic */ Object d;
        @DexIgnore
        public /* final */ /* synthetic */ el4 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(sj4 sj4, sj4 sj42, Object obj, dg4 dg4, a aVar, el4 el4, Object obj2) {
            super(sj42);
            this.d = obj;
            this.e = el4;
        }

        @DexIgnore
        /* renamed from: a */
        public Object c(sj4 sj4) {
            kd4.b(sj4, "affected");
            if (this.e._state == this.d) {
                return null;
            }
            return rj4.a();
        }
    }

    @DexIgnore
    public el4(boolean z) {
        this._state = z ? fl4.d : fl4.e;
    }

    @DexIgnore
    public Object a(Object obj, yb4<? super qa4> yb4) {
        if (b(obj)) {
            return qa4.a;
        }
        return b(obj, yb4);
    }

    @DexIgnore
    public boolean b(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof cl4) {
                if (((cl4) obj2).a != fl4.c) {
                    return false;
                }
                if (a.compareAndSet(this, obj2, obj == null ? fl4.d : new cl4(obj))) {
                    return true;
                }
            } else if (obj2 instanceof c) {
                if (((c) obj2).h == obj) {
                    z = false;
                }
                if (z) {
                    return false;
                }
                throw new IllegalStateException(("Already locked by " + obj).toString());
            } else if (obj2 instanceof yj4) {
                ((yj4) obj2).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public String toString() {
        while (true) {
            Object obj = this._state;
            if (obj instanceof cl4) {
                return "Mutex[" + ((cl4) obj).a + ']';
            } else if (obj instanceof yj4) {
                ((yj4) obj).a(this);
            } else if (obj instanceof c) {
                return "Mutex[" + ((c) obj).h + ']';
            } else {
                throw new IllegalStateException(("Illegal state " + obj).toString());
            }
        }
    }

    @DexIgnore
    public void a(Object obj) {
        while (true) {
            Object obj2 = this._state;
            boolean z = true;
            if (obj2 instanceof cl4) {
                if (obj == null) {
                    if (((cl4) obj2).a == fl4.c) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                } else {
                    cl4 cl4 = (cl4) obj2;
                    if (cl4.a != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + cl4.a + " but expected " + obj).toString());
                    }
                }
                if (a.compareAndSet(this, obj2, fl4.e)) {
                    return;
                }
            } else if (obj2 instanceof yj4) {
                ((yj4) obj2).a(this);
            } else if (obj2 instanceof c) {
                if (obj != null) {
                    c cVar = (c) obj2;
                    if (cVar.h != obj) {
                        z = false;
                    }
                    if (!z) {
                        throw new IllegalStateException(("Mutex is locked by " + cVar.h + " but expected " + obj).toString());
                    }
                }
                c cVar2 = (c) obj2;
                sj4 k = cVar2.k();
                if (k == null) {
                    d dVar = new d(cVar2);
                    if (a.compareAndSet(this, obj2, dVar) && dVar.a(this) == null) {
                        return;
                    }
                } else {
                    b bVar = (b) k;
                    Object m = bVar.m();
                    if (m != null) {
                        Object obj3 = bVar.h;
                        if (obj3 == null) {
                            obj3 = fl4.b;
                        }
                        cVar2.h = obj3;
                        bVar.a(m);
                        return;
                    }
                }
            } else {
                throw new IllegalStateException(("Illegal state " + obj2).toString());
            }
        }
    }

    @DexIgnore
    public final /* synthetic */ Object b(Object obj, yb4<? super qa4> yb4) {
        Object obj2 = obj;
        eg4 eg4 = new eg4(IntrinsicsKt__IntrinsicsJvmKt.a(yb4), 0);
        a aVar = new a(obj2, eg4);
        while (true) {
            Object obj3 = this._state;
            if (obj3 instanceof cl4) {
                cl4 cl4 = (cl4) obj3;
                if (cl4.a != fl4.c) {
                    a.compareAndSet(this, obj3, new c(cl4.a));
                } else {
                    if (a.compareAndSet(this, obj3, obj2 == null ? fl4.d : new cl4(obj2))) {
                        qa4 qa4 = qa4.a;
                        Result.a aVar2 = Result.Companion;
                        eg4.resumeWith(Result.m3constructorimpl(qa4));
                        break;
                    }
                }
            } else if (obj3 instanceof c) {
                c cVar = (c) obj3;
                boolean z = true;
                if (cVar.h != obj2) {
                    e eVar = new e(aVar, aVar, obj3, eg4, aVar, this, obj);
                    while (true) {
                        Object e2 = cVar.e();
                        if (e2 != null) {
                            int a2 = ((sj4) e2).a(aVar, cVar, eVar);
                            if (a2 != 1) {
                                if (a2 == 2) {
                                    z = false;
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                    }
                    if (z) {
                        fg4.a((dg4<?>) eg4, (sj4) aVar);
                        break;
                    }
                } else {
                    throw new IllegalStateException(("Already locked by " + obj2).toString());
                }
            } else if (obj3 instanceof yj4) {
                ((yj4) obj3).a(this);
            } else {
                throw new IllegalStateException(("Illegal state " + obj3).toString());
            }
        }
        Object e3 = eg4.e();
        if (e3 == cc4.a()) {
            ic4.c(yb4);
        }
        return e3;
    }
}
