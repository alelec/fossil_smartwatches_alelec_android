package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zx0 extends ru0<zx0, a> implements uv0 {
    @DexIgnore
    public static volatile cw0<zx0> zzbg;
    @DexIgnore
    public static /* final */ zx0 zztx; // = new zx0();
    @DexIgnore
    public int zzbb;
    @DexIgnore
    public int zztu;
    @DexIgnore
    public String zztv; // = "";
    @DexIgnore
    public String zztw; // = "";

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ru0.a<zx0, a> implements uv0 {
        @DexIgnore
        public a() {
            super(zx0.zztx);
        }

        @DexIgnore
        public /* synthetic */ a(ay0 ay0) {
            this();
        }
    }

    /*
    static {
        ru0.a(zx0.class, zztx);
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v14, types: [com.fossil.blesdk.obfuscated.cw0<com.fossil.blesdk.obfuscated.zx0>, com.fossil.blesdk.obfuscated.ru0$b] */
    public final Object a(int i, Object obj, Object obj2) {
        cw0<zx0> cw0;
        switch (ay0.a[i - 1]) {
            case 1:
                return new zx0();
            case 2:
                return new a((ay0) null);
            case 3:
                return ru0.a((sv0) zztx, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\b\u0002", new Object[]{"zzbb", "zztu", "zztv", "zztw"});
            case 4:
                return zztx;
            case 5:
                cw0<zx0> cw02 = zzbg;
                cw0<zx0> cw03 = cw02;
                if (cw02 == null) {
                    synchronized (zx0.class) {
                        cw0<zx0> cw04 = zzbg;
                        cw0 = cw04;
                        if (cw04 == null) {
                            Object bVar = new ru0.b(zztx);
                            zzbg = bVar;
                            cw0 = bVar;
                        }
                    }
                    cw03 = cw0;
                }
                return cw03;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
