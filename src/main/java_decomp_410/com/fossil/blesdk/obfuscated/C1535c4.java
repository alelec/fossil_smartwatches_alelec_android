package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c4 */
public interface C1535c4 {
    @DexIgnore
    /* renamed from: a */
    void mo1209a(int i, int i2);

    @DexIgnore
    /* renamed from: a */
    void mo1210a(int i, int i2, int i3, int i4);

    @DexIgnore
    /* renamed from: a */
    void mo1211a(android.graphics.drawable.Drawable drawable);

    @DexIgnore
    /* renamed from: a */
    boolean mo1212a();

    @DexIgnore
    /* renamed from: b */
    boolean mo1213b();

    @DexIgnore
    /* renamed from: c */
    android.graphics.drawable.Drawable mo1214c();

    @DexIgnore
    /* renamed from: d */
    android.view.View mo1215d();
}
