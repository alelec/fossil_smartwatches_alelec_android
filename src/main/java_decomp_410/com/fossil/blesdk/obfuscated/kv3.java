package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kv3 implements Closeable {
    @DexIgnore
    public abstract fv3 A();

    @DexIgnore
    public abstract lo4 B() throws IOException;

    @DexIgnore
    public void close() throws IOException {
        B().close();
    }

    @DexIgnore
    public final InputStream y() throws IOException {
        return B().m();
    }

    @DexIgnore
    public abstract long z() throws IOException;
}
