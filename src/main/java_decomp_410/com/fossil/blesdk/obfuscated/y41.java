package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y41 extends m31 {
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest s;
    @DexIgnore
    public /* final */ /* synthetic */ rc1 t;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y41(x41 x41, ge0 ge0, LocationRequest locationRequest, rc1 rc1) {
        super(ge0);
        this.s = locationRequest;
        this.t = rc1;
    }

    @DexIgnore
    public final /* synthetic */ void a(de0.b bVar) throws RemoteException {
        ((f41) bVar).a(this.s, (ze0<rc1>) af0.a(this.t, p41.a(), rc1.class.getSimpleName()), (r31) new n31(this));
    }
}
