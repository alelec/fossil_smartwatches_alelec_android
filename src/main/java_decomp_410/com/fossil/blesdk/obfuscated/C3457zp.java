package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zp */
public final class C3457zp<Z> implements com.fossil.blesdk.obfuscated.C1438aq<Z>, com.fossil.blesdk.obfuscated.C3145vw.C3151f {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C3457zp<?>> f11636i; // = com.fossil.blesdk.obfuscated.C3145vw.m15491a(20, new com.fossil.blesdk.obfuscated.C3457zp.C3458a());

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C3324xw f11637e; // = com.fossil.blesdk.obfuscated.C3324xw.m16582b();

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C1438aq<Z> f11638f;

    @DexIgnore
    /* renamed from: g */
    public boolean f11639g;

    @DexIgnore
    /* renamed from: h */
    public boolean f11640h;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.zp$a")
    /* renamed from: com.fossil.blesdk.obfuscated.zp$a */
    public class C3458a implements com.fossil.blesdk.obfuscated.C3145vw.C3149d<com.fossil.blesdk.obfuscated.C3457zp<?>> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3457zp<?> m17517a() {
            return new com.fossil.blesdk.obfuscated.C3457zp<>();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static <Z> com.fossil.blesdk.obfuscated.C3457zp<Z> m17508b(com.fossil.blesdk.obfuscated.C1438aq<Z> aqVar) {
        com.fossil.blesdk.obfuscated.C3457zp<Z> a = f11636i.mo11162a();
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
        com.fossil.blesdk.obfuscated.C3457zp<Z> zpVar = a;
        zpVar.mo18568a(aqVar);
        return zpVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo18568a(com.fossil.blesdk.obfuscated.C1438aq<Z> aqVar) {
        this.f11640h = false;
        this.f11639g = true;
        this.f11638f = aqVar;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Class<Z> mo8889c() {
        return this.f11638f.mo8889c();
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo18569d() {
        this.f11638f = null;
        f11636i.mo11163a(this);
    }

    @DexIgnore
    /* renamed from: e */
    public synchronized void mo18570e() {
        this.f11637e.mo17919a();
        if (this.f11639g) {
            this.f11639g = false;
            if (this.f11640h) {
                mo8887a();
            }
        } else {
            throw new java.lang.IllegalStateException("Already unlocked");
        }
    }

    @DexIgnore
    public Z get() {
        return this.f11638f.get();
    }

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C3324xw mo3959i() {
        return this.f11637e;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo8888b() {
        return this.f11638f.mo8888b();
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo8887a() {
        this.f11637e.mo17919a();
        this.f11640h = true;
        if (!this.f11639g) {
            this.f11638f.mo8887a();
            mo18569d();
        }
    }
}
