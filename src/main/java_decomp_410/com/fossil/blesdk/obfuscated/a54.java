package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class a54<T> implements c54<T> {
    @DexIgnore
    public /* final */ c54<T> a;

    @DexIgnore
    public a54(c54<T> c54) {
        this.a = c54;
    }

    @DexIgnore
    public abstract T a(Context context);

    @DexIgnore
    public final synchronized T a(Context context, d54<T> d54) throws Exception {
        T a2;
        a2 = a(context);
        if (a2 == null) {
            a2 = this.a != null ? this.a.a(context, d54) : d54.a(context);
            a(context, a2);
        }
        return a2;
    }

    @DexIgnore
    public abstract void b(Context context, T t);

    @DexIgnore
    public final void a(Context context, T t) {
        if (t != null) {
            b(context, t);
            return;
        }
        throw new NullPointerException();
    }
}
