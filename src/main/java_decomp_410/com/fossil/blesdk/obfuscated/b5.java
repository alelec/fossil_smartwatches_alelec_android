package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b5 extends ConstraintWidget {
    @DexIgnore
    public ConstraintWidget[] k0; // = new ConstraintWidget[4];
    @DexIgnore
    public int l0; // = 0;

    @DexIgnore
    public void K() {
        this.l0 = 0;
    }

    @DexIgnore
    public void b(ConstraintWidget constraintWidget) {
        int i = this.l0 + 1;
        ConstraintWidget[] constraintWidgetArr = this.k0;
        if (i > constraintWidgetArr.length) {
            this.k0 = (ConstraintWidget[]) Arrays.copyOf(constraintWidgetArr, constraintWidgetArr.length * 2);
        }
        ConstraintWidget[] constraintWidgetArr2 = this.k0;
        int i2 = this.l0;
        constraintWidgetArr2[i2] = constraintWidget;
        this.l0 = i2 + 1;
    }
}
