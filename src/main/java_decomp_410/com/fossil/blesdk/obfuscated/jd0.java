package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jd0 {
    @DexIgnore
    public static /* final */ int common_full_open_on_phone; // = 2131230935;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark; // = 2131230936;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_focused; // = 2131230937;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_normal; // = 2131230938;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_normal_background; // = 2131230939;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_disabled; // = 2131230940;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light; // = 2131230941;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_focused; // = 2131230942;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_normal; // = 2131230943;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_normal_background; // = 2131230944;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark; // = 2131230945;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_focused; // = 2131230946;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_normal; // = 2131230947;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_normal_background; // = 2131230948;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_disabled; // = 2131230949;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light; // = 2131230950;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_focused; // = 2131230951;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_normal; // = 2131230952;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_normal_background; // = 2131230953;
    @DexIgnore
    public static /* final */ int googleg_disabled_color_18; // = 2131230986;
    @DexIgnore
    public static /* final */ int googleg_standard_color_18; // = 2131230987;
}
