package com.fossil.blesdk.obfuscated;

import com.google.android.gms.fitness.data.DataType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cp0 {
    @DexIgnore
    public static /* final */ DataType a; // = new DataType("com.google.blood_pressure", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", dp0.a, dp0.e, dp0.i, dp0.j);
    @DexIgnore
    public static /* final */ DataType b; // = new DataType("com.google.blood_glucose", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", dp0.k, dp0.l, bp0.K, dp0.m, dp0.n);
    @DexIgnore
    public static /* final */ DataType c; // = new DataType("com.google.oxygen_saturation", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", dp0.o, dp0.s, dp0.w, dp0.x, dp0.y);
    @DexIgnore
    public static /* final */ DataType d; // = new DataType("com.google.body.temperature", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", dp0.z, dp0.A);
    @DexIgnore
    public static /* final */ DataType e; // = new DataType("com.google.body.temperature.basal", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", dp0.z, dp0.A);
    @DexIgnore
    public static /* final */ DataType f; // = new DataType("com.google.cervical_mucus", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", dp0.B, dp0.C);
    @DexIgnore
    public static /* final */ DataType g; // = new DataType("com.google.cervical_position", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", dp0.D, dp0.E, dp0.F);
    @DexIgnore
    public static /* final */ DataType h; // = new DataType("com.google.menstruation", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", dp0.G);
    @DexIgnore
    public static /* final */ DataType i; // = new DataType("com.google.ovulation_test", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", dp0.H);
    @DexIgnore
    public static /* final */ DataType j; // = new DataType("com.google.vaginal_spotting", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", bp0.f0);
    @DexIgnore
    public static /* final */ DataType k; // = new DataType("com.google.blood_pressure.summary", "https://www.googleapis.com/auth/fitness.blood_pressure.read", "https://www.googleapis.com/auth/fitness.blood_pressure.write", dp0.b, dp0.d, dp0.c, dp0.f, dp0.h, dp0.g, dp0.i, dp0.j);
    @DexIgnore
    public static /* final */ DataType l; // = new DataType("com.google.blood_glucose.summary", "https://www.googleapis.com/auth/fitness.blood_glucose.read", "https://www.googleapis.com/auth/fitness.blood_glucose.write", bp0.Y, bp0.Z, bp0.a0, dp0.l, bp0.K, dp0.m, dp0.n);
    @DexIgnore
    public static /* final */ DataType m; // = new DataType("com.google.oxygen_saturation.summary", "https://www.googleapis.com/auth/fitness.oxygen_saturation.read", "https://www.googleapis.com/auth/fitness.oxygen_saturation.write", dp0.p, dp0.r, dp0.q, dp0.t, dp0.v, dp0.u, dp0.w, dp0.x, dp0.y);
    @DexIgnore
    public static /* final */ DataType n; // = new DataType("com.google.body.temperature.summary", "https://www.googleapis.com/auth/fitness.body_temperature.read", "https://www.googleapis.com/auth/fitness.body_temperature.write", bp0.Y, bp0.Z, bp0.a0, dp0.A);
    @DexIgnore
    public static /* final */ DataType o; // = new DataType("com.google.body.temperature.basal.summary", "https://www.googleapis.com/auth/fitness.reproductive_health.read", "https://www.googleapis.com/auth/fitness.reproductive_health.write", bp0.Y, bp0.Z, bp0.a0, dp0.A);
}
