package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.ua.UADataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface cr3 {
    @DexIgnore
    @bt4("allday_activity_timeseries/")
    o84<qr4<xz1>> a(@ps4 xz1 xz1, @xs4("api-key") String str, @xs4("authorization") String str2);

    @DexIgnore
    @bt4("data_source/")
    o84<xz1> a(@ps4 UADataSource uADataSource, @xs4("api-key") String str, @xs4("authorization") String str2);

    @DexIgnore
    @us4("device/{pk}/")
    o84<xz1> a(@ft4("pk") String str, @xs4("api-key") String str2, @xs4("authorization") String str3);

    @DexIgnore
    @qs4("oauth2/connection/")
    o84<xz1> a(@gt4("user_id") String str, @gt4("client_id") String str2, @xs4("api-key") String str3, @xs4("authorization") String str4);

    @DexIgnore
    @bt4("oauth2/access_token/")
    @ts4
    o84<xz1> a(@rs4("grant_type") String str, @rs4("client_id") String str2, @rs4("client_secret") String str3, @rs4("code") String str4, @xs4("Content-Type") String str5, @xs4("Api-Key") String str6);

    @DexIgnore
    @bt4("oauth2/access_token/")
    @ts4
    o84<xz1> a(@rs4("grant_type") String str, @rs4("client_id") String str2, @rs4("client_secret") String str3, @rs4("refresh_token") String str4, @xs4("Content-Type") String str5, @xs4("api-key") String str6, @xs4("authorization") String str7);

    @DexIgnore
    @bt4("data_source_priority/")
    o84<xz1> b(@ps4 xz1 xz1, @xs4("api-key") String str, @xs4("authorization") String str2);
}
