package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.t8 */
public final class C2945t8 {
    @DexIgnore
    /* renamed from: a */
    public static android.view.MenuItem m14045a(android.view.MenuItem menuItem, com.fossil.blesdk.obfuscated.C2382m8 m8Var) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            return ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).mo8357a(m8Var);
        }
        android.util.Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m14051b(android.view.MenuItem menuItem, java.lang.CharSequence charSequence) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).setTooltipText(charSequence);
        } else if (android.os.Build.VERSION.SDK_INT >= 26) {
            menuItem.setTooltipText(charSequence);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14049a(android.view.MenuItem menuItem, java.lang.CharSequence charSequence) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).setContentDescription(charSequence);
        } else if (android.os.Build.VERSION.SDK_INT >= 26) {
            menuItem.setContentDescription(charSequence);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m14050b(android.view.MenuItem menuItem, char c, int i) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).setNumericShortcut(c, i);
        } else if (android.os.Build.VERSION.SDK_INT >= 26) {
            menuItem.setNumericShortcut(c, i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14046a(android.view.MenuItem menuItem, char c, int i) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).setAlphabeticShortcut(c, i);
        } else if (android.os.Build.VERSION.SDK_INT >= 26) {
            menuItem.setAlphabeticShortcut(c, i);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14047a(android.view.MenuItem menuItem, android.content.res.ColorStateList colorStateList) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).setIconTintList(colorStateList);
        } else if (android.os.Build.VERSION.SDK_INT >= 26) {
            menuItem.setIconTintList(colorStateList);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m14048a(android.view.MenuItem menuItem, android.graphics.PorterDuff.Mode mode) {
        if (menuItem instanceof com.fossil.blesdk.obfuscated.C2001i7) {
            ((com.fossil.blesdk.obfuscated.C2001i7) menuItem).setIconTintMode(mode);
        } else if (android.os.Build.VERSION.SDK_INT >= 26) {
            menuItem.setIconTintMode(mode);
        }
    }
}
