package com.fossil.blesdk.obfuscated;

import java.lang.annotation.Annotation;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fe4 {
    @DexIgnore
    List<Annotation> getAnnotations();
}
