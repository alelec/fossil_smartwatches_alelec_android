package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vg3 extends zr2 implements ug3, ws3.g {
    @DexIgnore
    public static /* final */ a m; // = new a((fd4) null);
    @DexIgnore
    public tr3<o82> j;
    @DexIgnore
    public tg3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final vg3 a() {
            return new vg3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vg3 e;

        @DexIgnore
        public b(vg3 vg3) {
            this.e = vg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vg3 e;

        @DexIgnore
        public c(vg3 vg3) {
            this.e = vg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            pd4 pd4 = pd4.a;
            Locale a = sm2.a();
            kd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://help.wearablessupport.com/hc/%s/articles/210312606", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Privacy Policy URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vg3 e;

        @DexIgnore
        public d(vg3 vg3) {
            this.e = vg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            pd4 pd4 = pd4.a;
            Locale a = sm2.a();
            kd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://help.wearablessupport.com/hc/%s/articles/210312186", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Term Of Use URL = " + format);
            this.e.U(format);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ vg3 e;

        @DexIgnore
        public e(vg3 vg3) {
            this.e = vg3;
        }

        @DexIgnore
        public final void onClick(View view) {
            pd4 pd4 = pd4.a;
            Locale a = sm2.a();
            kd4.a((Object) a, "LanguageHelper.getLocale()");
            Object[] objArr = {a.getLanguage()};
            String format = String.format("https://c.fossil.com/web/open-source-licenses", Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("AboutFragment", "Open Source Licenses URL = " + format);
            this.e.U(format);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void U(String str) {
        a(new Intent("android.intent.action.VIEW", Uri.parse(str)), ii3.n.a());
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        o82 o82 = (o82) qa.a(LayoutInflater.from(getContext()), R.layout.fragment_about, (ViewGroup) null, false, O0());
        this.j = new tr3<>(this, o82);
        kd4.a((Object) o82, "binding");
        return o82.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        tg3 tg3 = this.k;
        if (tg3 != null) {
            tg3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        tg3 tg3 = this.k;
        if (tg3 != null) {
            tg3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<o82> tr3 = this.j;
        if (tr3 != null) {
            o82 a2 = tr3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.r.setOnClickListener(new e(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(tg3 tg3) {
        kd4.b(tg3, "presenter");
        st1.a(tg3);
        kd4.a((Object) tg3, "Preconditions.checkNotNull(presenter)");
        this.k = tg3;
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AboutFragment", "Inside .onDialogFragmentResult with TAG=" + str);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof BaseActivity)) {
            activity = null;
        }
        BaseActivity baseActivity = (BaseActivity) activity;
        if (baseActivity != null) {
            baseActivity.a(str, i, intent);
        }
    }
}
