package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rf2 extends qf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j K; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray L; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public long J;

    /*
    static {
        L.put(R.id.cl_top, 1);
        L.put(R.id.iv_back, 2);
        L.put(R.id.tv_title, 3);
        L.put(R.id.cl_overview_day, 4);
        L.put(R.id.line_text, 5);
        L.put(R.id.iv_back_date, 6);
        L.put(R.id.ftv_day_of_week, 7);
        L.put(R.id.ftv_day_of_month, 8);
        L.put(R.id.line, 9);
        L.put(R.id.ftv_daily_value, 10);
        L.put(R.id.ftv_daily_unit, 11);
        L.put(R.id.ftv_daily_value2, 12);
        L.put(R.id.ftv_daily_unit2, 13);
        L.put(R.id.iv_next_date, 14);
        L.put(R.id.cl_pb_goal, 15);
        L.put(R.id.pb_goal, 16);
        L.put(R.id.ftv_progress_value, 17);
        L.put(R.id.ftv_goal_value, 18);
        L.put(R.id.cl_detail_day, 19);
        L.put(R.id.rv_sleeps, 20);
        L.put(R.id.cpi_sleep, 21);
        L.put(R.id.clHRSleepSession, 22);
        L.put(R.id.v_elevation, 23);
        L.put(R.id.iv_hrSleepSessionIcon, 24);
        L.put(R.id.ftv_hrSleepSessionTitle, 25);
        L.put(R.id.rv_heart_rate_sleep, 26);
        L.put(R.id.ftv_hrSleepSessionDesc, 27);
    }
    */

    @DexIgnore
    public rf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 28, K, L));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.J = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.J != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.J = 1;
        }
        g();
    }

    @DexIgnore
    public rf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[19], objArr[22], objArr[4], objArr[15], objArr[1], objArr[21], objArr[11], objArr[13], objArr[10], objArr[12], objArr[8], objArr[7], objArr[18], objArr[27], objArr[25], objArr[17], objArr[2], objArr[6], objArr[24], objArr[14], objArr[9], objArr[5], objArr[16], objArr[26], objArr[20], objArr[3], objArr[23]);
        this.J = -1;
        this.I = objArr[0];
        this.I.setTag((Object) null);
        a(view);
        f();
    }
}
