package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.jt0;
import com.fossil.blesdk.obfuscated.kt0;
import com.google.android.gms.internal.clearcut.zzbb;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class jt0<MessageType extends jt0<MessageType, BuilderType>, BuilderType extends kt0<MessageType, BuilderType>> implements sv0 {
    @DexIgnore
    public static boolean zzey;
    @DexIgnore
    public int zzex; // = 0;

    @DexIgnore
    public void a(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final zzbb d() {
        try {
            wt0 zzk = zzbb.zzk(f());
            a(zzk.b());
            return zzk.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public int g() {
        throw new UnsupportedOperationException();
    }
}
