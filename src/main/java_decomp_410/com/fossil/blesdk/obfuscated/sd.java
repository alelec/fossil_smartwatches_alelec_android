package com.fossil.blesdk.obfuscated;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sd<T> extends AbstractList<T> {
    @DexIgnore
    public static /* final */ List n; // = new ArrayList();
    @DexIgnore
    public int e;
    @DexIgnore
    public /* final */ ArrayList<List<T>> f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(int i);

        @DexIgnore
        void a(int i, int i2);

        @DexIgnore
        void a(int i, int i2, int i3);

        @DexIgnore
        void b();

        @DexIgnore
        void b(int i, int i2);

        @DexIgnore
        void b(int i, int i2, int i3);

        @DexIgnore
        void c(int i, int i2);

        @DexIgnore
        void f(int i);
    }

    @DexIgnore
    public sd() {
        this.e = 0;
        this.f = new ArrayList<>();
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 1;
        this.l = 0;
        this.m = 0;
    }

    @DexIgnore
    public final void a(int i2, List<T> list, int i3, int i4) {
        this.e = i2;
        this.f.clear();
        this.f.add(list);
        this.g = i3;
        this.h = i4;
        this.i = list.size();
        this.j = this.i;
        this.k = list.size();
        this.l = 0;
        this.m = 0;
    }

    @DexIgnore
    public int b() {
        int i2 = this.g;
        for (int size = this.f.size() - 1; size >= 0; size--) {
            List list = this.f.get(size);
            if (list != null && list != n) {
                break;
            }
            i2 += this.k;
        }
        return i2;
    }

    @DexIgnore
    public boolean c(int i2, int i3) {
        return a(i2, i3, this.f.size() - 1);
    }

    @DexIgnore
    public boolean d(int i2, int i3) {
        return a(i2, i3, 0);
    }

    @DexIgnore
    public int e() {
        return this.e;
    }

    @DexIgnore
    public int f() {
        return this.e + this.h + (this.j / 2);
    }

    @DexIgnore
    public int g() {
        return this.m;
    }

    @DexIgnore
    public T get(int i2) {
        int i3;
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        int i4 = i2 - this.e;
        if (i4 >= 0 && i4 < this.j) {
            if (m()) {
                int i5 = this.k;
                i3 = i4 / i5;
                i4 %= i5;
            } else {
                int size = this.f.size();
                i3 = 0;
                while (i3 < size) {
                    int size2 = this.f.get(i3).size();
                    if (size2 > i4) {
                        break;
                    }
                    i4 -= size2;
                    i3++;
                }
            }
            List list = this.f.get(i3);
            if (!(list == null || list.size() == 0)) {
                return list.get(i4);
            }
        }
        return null;
    }

    @DexIgnore
    public int h() {
        return this.l;
    }

    @DexIgnore
    public int i() {
        return this.f.size();
    }

    @DexIgnore
    public int j() {
        return this.h;
    }

    @DexIgnore
    public int k() {
        return this.j;
    }

    @DexIgnore
    public int l() {
        return this.g;
    }

    @DexIgnore
    public boolean m() {
        return this.k > 0;
    }

    @DexIgnore
    public sd<T> n() {
        return new sd<>(this);
    }

    @DexIgnore
    public int size() {
        return this.e + this.j + this.g;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("leading " + this.e + ", storage " + this.j + ", trailing " + l());
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            sb.append(" ");
            sb.append(this.f.get(i2));
        }
        return sb.toString();
    }

    @DexIgnore
    public T c() {
        return this.f.get(0).get(0);
    }

    @DexIgnore
    public T d() {
        ArrayList<List<T>> arrayList = this.f;
        List list = arrayList.get(arrayList.size() - 1);
        return list.get(list.size() - 1);
    }

    @DexIgnore
    public boolean b(int i2, int i3, int i4) {
        if (this.i + i4 <= i2 || this.f.size() <= 1 || this.i < i3) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean b(boolean z, int i2, int i3, a aVar) {
        int i4 = 0;
        while (d(i2, i3)) {
            List remove = this.f.remove(0);
            int size = remove == null ? this.k : remove.size();
            i4 += size;
            this.j -= size;
            this.i -= remove == null ? 0 : remove.size();
        }
        if (i4 > 0) {
            if (z) {
                int i5 = this.e;
                this.e = i5 + i4;
                aVar.a(i5, i4);
            } else {
                this.h += i4;
                aVar.b(this.e, i4);
            }
        }
        if (i4 > 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public sd(sd<T> sdVar) {
        this.e = sdVar.e;
        this.f = new ArrayList<>(sdVar.f);
        this.g = sdVar.g;
        this.h = sdVar.h;
        this.i = sdVar.i;
        this.j = sdVar.j;
        this.k = sdVar.k;
        this.l = sdVar.l;
        this.m = sdVar.m;
    }

    @DexIgnore
    public void a(int i2, List<T> list, int i3, int i4, a aVar) {
        a(i2, list, i3, i4);
        aVar.a(size());
    }

    @DexIgnore
    public int a() {
        int i2 = this.e;
        int size = this.f.size();
        for (int i3 = 0; i3 < size; i3++) {
            List list = this.f.get(i3);
            if (list != null && list != n) {
                break;
            }
            i2 += this.k;
        }
        return i2;
    }

    @DexIgnore
    public final boolean a(int i2, int i3, int i4) {
        List list = this.f.get(i4);
        return list == null || (this.i > i2 && this.f.size() > 2 && list != n && this.i - list.size() >= i3);
    }

    @DexIgnore
    public void b(List<T> list, a aVar) {
        int size = list.size();
        if (size == 0) {
            aVar.b();
            return;
        }
        int i2 = this.k;
        if (i2 > 0 && size != i2) {
            if (this.f.size() != 1 || size <= this.k) {
                this.k = -1;
            } else {
                this.k = size;
            }
        }
        this.f.add(0, list);
        this.i += size;
        this.j += size;
        int min = Math.min(this.e, size);
        int i3 = size - min;
        if (min != 0) {
            this.e -= min;
        }
        this.h -= i3;
        this.l += size;
        aVar.a(this.e, min, i3);
    }

    @DexIgnore
    public boolean a(boolean z, int i2, int i3, a aVar) {
        int i4 = 0;
        while (c(i2, i3)) {
            ArrayList<List<T>> arrayList = this.f;
            List remove = arrayList.remove(arrayList.size() - 1);
            int size = remove == null ? this.k : remove.size();
            i4 += size;
            this.j -= size;
            this.i -= remove == null ? 0 : remove.size();
        }
        if (i4 > 0) {
            int i5 = this.e + this.j;
            if (z) {
                this.g += i4;
                aVar.a(i5, i4);
            } else {
                aVar.b(i5, i4);
            }
        }
        if (i4 > 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(List<T> list, a aVar) {
        int size = list.size();
        if (size == 0) {
            aVar.a();
            return;
        }
        if (this.k > 0) {
            ArrayList<List<T>> arrayList = this.f;
            int size2 = arrayList.get(arrayList.size() - 1).size();
            int i2 = this.k;
            if (size2 != i2 || size > i2) {
                this.k = -1;
            }
        }
        this.f.add(list);
        this.i += size;
        this.j += size;
        int min = Math.min(this.g, size);
        int i3 = size - min;
        if (min != 0) {
            this.g -= min;
        }
        this.m += size;
        aVar.b((this.e + this.j) - size, min, i3);
    }

    @DexIgnore
    public void b(int i2, List<T> list, int i3, int i4, int i5, a aVar) {
        boolean z = i4 != Integer.MAX_VALUE;
        boolean z2 = i3 > f();
        if (!z || !b(i4, i5, list.size()) || !a(i2, z2)) {
            a(i2, list, aVar);
        } else {
            this.f.set((i2 - this.e) / this.k, (Object) null);
            this.j -= list.size();
            if (z2) {
                this.f.remove(0);
                this.e += list.size();
            } else {
                ArrayList<List<T>> arrayList = this.f;
                arrayList.remove(arrayList.size() - 1);
                this.g += list.size();
            }
        }
        if (!z) {
            return;
        }
        if (z2) {
            b(true, i4, i5, aVar);
        } else {
            a(true, i4, i5, aVar);
        }
    }

    @DexIgnore
    public boolean a(int i2, boolean z) {
        if (this.k < 1 || this.f.size() < 2) {
            throw new IllegalStateException("Trimming attempt before sufficient load");
        }
        int i3 = this.e;
        if (i2 < i3) {
            return z;
        }
        if (i2 >= this.j + i3) {
            return !z;
        }
        int i4 = (i2 - i3) / this.k;
        if (z) {
            for (int i5 = 0; i5 < i4; i5++) {
                if (this.f.get(i5) != null) {
                    return false;
                }
            }
        } else {
            for (int size = this.f.size() - 1; size > i4; size--) {
                if (this.f.get(size) != null) {
                    return false;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public boolean b(int i2, int i3) {
        int i4 = this.e / i2;
        if (i3 < i4 || i3 >= this.f.size() + i4) {
            return false;
        }
        List list = this.f.get(i3 - i4);
        if (list == null || list == n) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void a(int i2, List<T> list, int i3, int i4, int i5, a aVar) {
        int size = (list.size() + (i5 - 1)) / i5;
        int i6 = 0;
        while (i6 < size) {
            int i7 = i6 * i5;
            int i8 = i6 + 1;
            List<T> subList = list.subList(i7, Math.min(list.size(), i8 * i5));
            if (i6 == 0) {
                a(i2, subList, (list.size() + i3) - subList.size(), i4);
            } else {
                a(i7 + i2, subList, (a) null);
            }
            i6 = i8;
        }
        aVar.a(size());
    }

    @DexIgnore
    public void a(int i2, List<T> list, a aVar) {
        int size = list.size();
        if (size != this.k) {
            int size2 = size();
            int i3 = this.k;
            boolean z = false;
            boolean z2 = i2 == size2 - (size2 % i3) && size < i3;
            if (this.g == 0 && this.f.size() == 1 && size > this.k) {
                z = true;
            }
            if (!z && !z2) {
                throw new IllegalArgumentException("page introduces incorrect tiling");
            } else if (z) {
                this.k = size;
            }
        }
        int i4 = i2 / this.k;
        a(i4, i4);
        int i5 = i4 - (this.e / this.k);
        List list2 = this.f.get(i5);
        if (list2 == null || list2 == n) {
            this.f.set(i5, list);
            this.i += size;
            if (aVar != null) {
                aVar.c(i2, size);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Invalid position " + i2 + ": data already loaded");
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int i4;
        int i5 = this.e / this.k;
        if (i2 < i5) {
            int i6 = 0;
            while (true) {
                i4 = i5 - i2;
                if (i6 >= i4) {
                    break;
                }
                this.f.add(0, (Object) null);
                i6++;
            }
            int i7 = i4 * this.k;
            this.j += i7;
            this.e -= i7;
        } else {
            i2 = i5;
        }
        if (i3 >= this.f.size() + i2) {
            int min = Math.min(this.g, ((i3 + 1) - (this.f.size() + i2)) * this.k);
            for (int size = this.f.size(); size <= i3 - i2; size++) {
                ArrayList<List<T>> arrayList = this.f;
                arrayList.add(arrayList.size(), (Object) null);
            }
            this.j += min;
            this.g -= min;
        }
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, a aVar) {
        int i5 = this.k;
        if (i4 != i5) {
            if (i4 < i5) {
                throw new IllegalArgumentException("Page size cannot be reduced");
            } else if (this.f.size() == 1 && this.g == 0) {
                this.k = i4;
            } else {
                throw new IllegalArgumentException("Page size can change only if last page is only one present");
            }
        }
        int size = size();
        int i6 = this.k;
        int i7 = ((size + i6) - 1) / i6;
        int max = Math.max((i2 - i3) / i6, 0);
        int min = Math.min((i2 + i3) / this.k, i7 - 1);
        a(max, min);
        int i8 = this.e / this.k;
        while (max <= min) {
            int i9 = max - i8;
            if (this.f.get(i9) == null) {
                this.f.set(i9, n);
                aVar.f(max);
            }
            max++;
        }
    }
}
