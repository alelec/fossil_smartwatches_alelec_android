package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wl0 extends yy0 implements ul0 {
    @DexIgnore
    public wl0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    @DexIgnore
    public final sn0 zzb() throws RemoteException {
        Parcel a = a(1, o());
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int zzc() throws RemoteException {
        Parcel a = a(2, o());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
