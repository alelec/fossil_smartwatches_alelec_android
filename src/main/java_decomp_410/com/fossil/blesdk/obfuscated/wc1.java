package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wc1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wc1> CREATOR; // = new hd1();
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ boolean j;

    @DexIgnore
    public wc1(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.e = z;
        this.f = z2;
        this.g = z3;
        this.h = z4;
        this.i = z5;
        this.j = z6;
    }

    @DexIgnore
    public final boolean H() {
        return this.j;
    }

    @DexIgnore
    public final boolean I() {
        return this.g;
    }

    @DexIgnore
    public final boolean J() {
        return this.h;
    }

    @DexIgnore
    public final boolean K() {
        return this.e;
    }

    @DexIgnore
    public final boolean L() {
        return this.i;
    }

    @DexIgnore
    public final boolean M() {
        return this.f;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, K());
        kk0.a(parcel, 2, M());
        kk0.a(parcel, 3, I());
        kk0.a(parcel, 4, J());
        kk0.a(parcel, 5, L());
        kk0.a(parcel, 6, H());
        kk0.a(parcel, a);
    }
}
