package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vo */
public final class C3127vo extends java.io.FilterInputStream {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ byte[] f10346g; // = {-1, -31, 0, 28, 69, 120, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, org.joda.time.DateTimeFieldType.MINUTE_OF_DAY, 0, 2, 0, 0, 0, 1, 0};

    @DexIgnore
    /* renamed from: h */
    public static /* final */ int f10347h; // = f10346g.length;

    @DexIgnore
    /* renamed from: i */
    public static /* final */ int f10348i; // = (f10347h + 2);

    @DexIgnore
    /* renamed from: e */
    public /* final */ byte f10349e;

    @DexIgnore
    /* renamed from: f */
    public int f10350f;

    @DexIgnore
    public C3127vo(java.io.InputStream inputStream, int i) {
        super(inputStream);
        if (i < -1 || i > 8) {
            throw new java.lang.IllegalArgumentException("Cannot add invalid orientation: " + i);
        }
        this.f10349e = (byte) i;
    }

    @DexIgnore
    public void mark(int i) {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public boolean markSupported() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e  */
    public int read() throws java.io.IOException {
        int i;
        int i2 = this.f10350f;
        if (i2 >= 2) {
            int i3 = f10348i;
            if (i2 <= i3) {
                if (i2 == i3) {
                    i = this.f10349e;
                } else {
                    i = f10346g[i2 - 2] & com.fossil.blesdk.device.data.file.FileType.MASKED_INDEX;
                }
                if (i != -1) {
                    this.f10350f++;
                }
                return i;
            }
        }
        i = super.read();
        if (i != -1) {
        }
        return i;
    }

    @DexIgnore
    public void reset() throws java.io.IOException {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    public long skip(long j) throws java.io.IOException {
        long skip = super.skip(j);
        if (skip > 0) {
            this.f10350f = (int) (((long) this.f10350f) + skip);
        }
        return skip;
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) throws java.io.IOException {
        int i3;
        int i4 = this.f10350f;
        int i5 = f10348i;
        if (i4 > i5) {
            i3 = super.read(bArr, i, i2);
        } else if (i4 == i5) {
            bArr[i] = this.f10349e;
            i3 = 1;
        } else if (i4 < 2) {
            i3 = super.read(bArr, i, 2 - i4);
        } else {
            int min = java.lang.Math.min(i5 - i4, i2);
            java.lang.System.arraycopy(f10346g, this.f10350f - 2, bArr, i, min);
            i3 = min;
        }
        if (i3 > 0) {
            this.f10350f += i3;
        }
        return i3;
    }
}
