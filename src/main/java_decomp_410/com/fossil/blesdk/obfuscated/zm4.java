package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zm4 {
    @DexIgnore
    em4 a(Response response) throws IOException;

    @DexIgnore
    xo4 a(dm4 dm4, long j);

    @DexIgnore
    Response.a a(boolean z) throws IOException;

    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void a(dm4 dm4) throws IOException;

    @DexIgnore
    void b() throws IOException;

    @DexIgnore
    void cancel();
}
