package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qr2 extends i62<a.b, a.c, a.C0102a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((fd4) null);
    @DexIgnore
    public /* final */ ServerSettingRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qr2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qr2$a$a  reason: collision with other inner class name */
        public static final class C0102a implements i62.a {
            @DexIgnore
            public C0102a(int i) {
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements i62.b {
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements i62.c {
            @DexIgnore
            public c(ServerSettingList serverSettingList) {
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qr2.e;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ServerSettingDataSource.OnGetServerSettingList {
        @DexIgnore
        public /* final */ /* synthetic */ qr2 a;

        @DexIgnore
        public b(qr2 qr2) {
            this.a = qr2;
        }

        @DexIgnore
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = qr2.f.a();
            local.e(a2, "executeUseCase - onFailed. ErrorCode = " + i);
            this.a.a().a(new a.C0102a(i));
        }

        @DexIgnore
        public void onSuccess(ServerSettingList serverSettingList) {
            FLogger.INSTANCE.getLocal().d(qr2.f.a(), "executeUseCase - onSuccess");
            this.a.a().onSuccess(new a.c(serverSettingList));
        }
    }

    /*
    static {
        String simpleName = qr2.class.getSimpleName();
        kd4.a((Object) simpleName, "GetServerSettingUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public qr2(ServerSettingRepository serverSettingRepository) {
        kd4.b(serverSettingRepository, "serverSettingRepository");
        this.d = serverSettingRepository;
    }

    @DexIgnore
    public void a(a.b bVar) {
        this.d.getServerSettingList(new b(this));
    }
}
