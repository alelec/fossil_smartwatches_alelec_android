package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gy0 extends qx0<gy0> implements Cloneable {
    @DexIgnore
    public String[] g;
    @DexIgnore
    public String[] h;
    @DexIgnore
    public int[] i; // = yx0.a;
    @DexIgnore
    public long[] j;
    @DexIgnore
    public long[] k;

    @DexIgnore
    public gy0() {
        String[] strArr = yx0.c;
        this.g = strArr;
        this.h = strArr;
        long[] jArr = yx0.b;
        this.j = jArr;
        this.k = jArr;
        this.f = null;
        this.e = -1;
    }

    @DexIgnore
    public final void a(px0 px0) throws IOException {
        String[] strArr = this.g;
        int i2 = 0;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            while (true) {
                String[] strArr2 = this.g;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i3];
                if (str != null) {
                    px0.a(1, str);
                }
                i3++;
            }
        }
        String[] strArr3 = this.h;
        if (strArr3 != null && strArr3.length > 0) {
            int i4 = 0;
            while (true) {
                String[] strArr4 = this.h;
                if (i4 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i4];
                if (str2 != null) {
                    px0.a(2, str2);
                }
                i4++;
            }
        }
        int[] iArr = this.i;
        if (iArr != null && iArr.length > 0) {
            int i5 = 0;
            while (true) {
                int[] iArr2 = this.i;
                if (i5 >= iArr2.length) {
                    break;
                }
                px0.b(3, iArr2[i5]);
                i5++;
            }
        }
        long[] jArr = this.j;
        if (jArr != null && jArr.length > 0) {
            int i6 = 0;
            while (true) {
                long[] jArr2 = this.j;
                if (i6 >= jArr2.length) {
                    break;
                }
                px0.a(4, jArr2[i6]);
                i6++;
            }
        }
        long[] jArr3 = this.k;
        if (jArr3 != null && jArr3.length > 0) {
            while (true) {
                long[] jArr4 = this.k;
                if (i2 >= jArr4.length) {
                    break;
                }
                px0.a(5, jArr4[i2]);
                i2++;
            }
        }
        super.a(px0);
    }

    @DexIgnore
    public final int b() {
        long[] jArr;
        int[] iArr;
        int b = super.b();
        String[] strArr = this.g;
        int i2 = 0;
        if (strArr != null && strArr.length > 0) {
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (true) {
                String[] strArr2 = this.g;
                if (i3 >= strArr2.length) {
                    break;
                }
                String str = strArr2[i3];
                if (str != null) {
                    i5++;
                    i4 += px0.a(str);
                }
                i3++;
            }
            b = b + i4 + (i5 * 1);
        }
        String[] strArr3 = this.h;
        if (strArr3 != null && strArr3.length > 0) {
            int i6 = 0;
            int i7 = 0;
            int i8 = 0;
            while (true) {
                String[] strArr4 = this.h;
                if (i6 >= strArr4.length) {
                    break;
                }
                String str2 = strArr4[i6];
                if (str2 != null) {
                    i8++;
                    i7 += px0.a(str2);
                }
                i6++;
            }
            b = b + i7 + (i8 * 1);
        }
        int[] iArr2 = this.i;
        if (iArr2 != null && iArr2.length > 0) {
            int i9 = 0;
            int i10 = 0;
            while (true) {
                iArr = this.i;
                if (i9 >= iArr.length) {
                    break;
                }
                i10 += px0.d(iArr[i9]);
                i9++;
            }
            b = b + i10 + (iArr.length * 1);
        }
        long[] jArr2 = this.j;
        if (jArr2 != null && jArr2.length > 0) {
            int i11 = 0;
            int i12 = 0;
            while (true) {
                jArr = this.j;
                if (i11 >= jArr.length) {
                    break;
                }
                i12 += px0.c(jArr[i11]);
                i11++;
            }
            b = b + i12 + (jArr.length * 1);
        }
        long[] jArr3 = this.k;
        if (jArr3 == null || jArr3.length <= 0) {
            return b;
        }
        int i13 = 0;
        while (true) {
            long[] jArr4 = this.k;
            if (i2 >= jArr4.length) {
                return b + i13 + (jArr4.length * 1);
            }
            i13 += px0.c(jArr4[i2]);
            i2++;
        }
    }

    @DexIgnore
    public final /* synthetic */ vx0 c() throws CloneNotSupportedException {
        return (gy0) clone();
    }

    @DexIgnore
    public final /* synthetic */ qx0 d() throws CloneNotSupportedException {
        return (gy0) clone();
    }

    @DexIgnore
    /* renamed from: e */
    public final gy0 clone() {
        try {
            gy0 gy0 = (gy0) super.clone();
            String[] strArr = this.g;
            if (strArr != null && strArr.length > 0) {
                gy0.g = (String[]) strArr.clone();
            }
            String[] strArr2 = this.h;
            if (strArr2 != null && strArr2.length > 0) {
                gy0.h = (String[]) strArr2.clone();
            }
            int[] iArr = this.i;
            if (iArr != null && iArr.length > 0) {
                gy0.i = (int[]) iArr.clone();
            }
            long[] jArr = this.j;
            if (jArr != null && jArr.length > 0) {
                gy0.j = (long[]) jArr.clone();
            }
            long[] jArr2 = this.k;
            if (jArr2 != null && jArr2.length > 0) {
                gy0.k = (long[]) jArr2.clone();
            }
            return gy0;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gy0)) {
            return false;
        }
        gy0 gy0 = (gy0) obj;
        if (!ux0.a((Object[]) this.g, (Object[]) gy0.g) || !ux0.a((Object[]) this.h, (Object[]) gy0.h) || !ux0.a(this.i, gy0.i) || !ux0.a(this.j, gy0.j) || !ux0.a(this.k, gy0.k)) {
            return false;
        }
        sx0 sx0 = this.f;
        if (sx0 != null && !sx0.a()) {
            return this.f.equals(gy0.f);
        }
        sx0 sx02 = gy0.f;
        return sx02 == null || sx02.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((((((((((gy0.class.getName().hashCode() + 527) * 31) + ux0.a((Object[]) this.g)) * 31) + ux0.a((Object[]) this.h)) * 31) + ux0.a(this.i)) * 31) + ux0.a(this.j)) * 31) + ux0.a(this.k)) * 31;
        sx0 sx0 = this.f;
        return hashCode + ((sx0 == null || sx0.a()) ? 0 : this.f.hashCode());
    }
}
