package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mf0 extends bi0 {
    @DexIgnore
    public /* final */ h4<yh0<?>> j; // = new h4<>();
    @DexIgnore
    public ve0 k;

    @DexIgnore
    public mf0(ye0 ye0) {
        super(ye0);
        this.e.a("ConnectionlessLifecycleHelper", (LifecycleCallback) this);
    }

    @DexIgnore
    public static void a(Activity activity, ve0 ve0, yh0<?> yh0) {
        ye0 a = LifecycleCallback.a(activity);
        mf0 mf0 = (mf0) a.a("ConnectionlessLifecycleHelper", mf0.class);
        if (mf0 == null) {
            mf0 = new mf0(a);
        }
        mf0.k = ve0;
        bk0.a(yh0, (Object) "ApiKey cannot be null");
        mf0.j.add(yh0);
        ve0.a(mf0);
    }

    @DexIgnore
    public void c() {
        super.c();
        i();
    }

    @DexIgnore
    public void d() {
        super.d();
        i();
    }

    @DexIgnore
    public void e() {
        super.e();
        this.k.b(this);
    }

    @DexIgnore
    public final void f() {
        this.k.c();
    }

    @DexIgnore
    public final h4<yh0<?>> h() {
        return this.j;
    }

    @DexIgnore
    public final void i() {
        if (!this.j.isEmpty()) {
            this.k.a(this);
        }
    }

    @DexIgnore
    public final void a(ud0 ud0, int i) {
        this.k.a(ud0, i);
    }
}
