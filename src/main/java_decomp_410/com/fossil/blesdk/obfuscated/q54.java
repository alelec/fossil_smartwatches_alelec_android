package com.fossil.blesdk.obfuscated;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q54 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicLong b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q54$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.q54$a$a  reason: collision with other inner class name */
        public class C0101a extends l54 {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable e;

            @DexIgnore
            public C0101a(a aVar, Runnable runnable) {
                this.e = runnable;
            }

            @DexIgnore
            public void a() {
                this.e.run();
            }
        }

        @DexIgnore
        public a(String str, AtomicLong atomicLong) {
            this.a = str;
            this.b = atomicLong;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new C0101a(this, runnable));
            newThread.setName(this.a + this.b.getAndIncrement());
            return newThread;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends l54 {
        @DexIgnore
        public /* final */ /* synthetic */ String e;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;
        @DexIgnore
        public /* final */ /* synthetic */ TimeUnit h;

        @DexIgnore
        public b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.e = str;
            this.f = executorService;
            this.g = j;
            this.h = timeUnit;
        }

        @DexIgnore
        public void a() {
            try {
                y44 g2 = q44.g();
                g2.d("Fabric", "Executing shutdown hook for " + this.e);
                this.f.shutdown();
                if (!this.f.awaitTermination(this.g, this.h)) {
                    y44 g3 = q44.g();
                    g3.d("Fabric", this.e + " did not shut down in the allocated time. Requesting immediate shutdown.");
                    this.f.shutdownNow();
                }
            } catch (InterruptedException unused) {
                q44.g().d("Fabric", String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", new Object[]{this.e}));
                this.f.shutdownNow();
            }
        }
    }

    @DexIgnore
    public static ExecutorService a(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(c(str));
        a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    @DexIgnore
    public static ScheduledExecutorService b(String str) {
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor(c(str));
        a(str, newSingleThreadScheduledExecutor);
        return newSingleThreadScheduledExecutor;
    }

    @DexIgnore
    public static final ThreadFactory c(String str) {
        return new a(str, new AtomicLong(1));
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService) {
        a(str, executorService, 2, TimeUnit.SECONDS);
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        b bVar = new b(str, executorService, j, timeUnit);
        runtime.addShutdownHook(new Thread(bVar, "Crashlytics Shutdown Hook for " + str));
    }
}
