package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mo<T, Z> {
    @DexIgnore
    aq<Z> a(T t, int i, int i2, lo loVar) throws IOException;

    @DexIgnore
    boolean a(T t, lo loVar) throws IOException;
}
