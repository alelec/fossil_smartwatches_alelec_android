package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.common.constants.Constants;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r60 extends g70 {
    @DexIgnore
    public UUID[] A; // = new UUID[0];
    @DexIgnore
    public GattCharacteristic.CharacteristicId[] B; // = new GattCharacteristic.CharacteristicId[0];
    @DexIgnore
    public long C; // = ButtonService.CONNECT_TIMEOUT;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r60(Peripheral peripheral) {
        super(RequestId.DISCOVER_SERVICES, peripheral);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new h10(i().h());
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId[] B() {
        return this.B;
    }

    @DexIgnore
    public final UUID[] C() {
        return this.A;
    }

    @DexIgnore
    public void a(long j) {
        this.C = j;
    }

    @DexIgnore
    public long m() {
        return this.C;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(wa0.a(super.u(), JSONKey.SERVICES, s60.a(this.A)), JSONKey.CHARACTERISTICS, q10.a(this.B));
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        h10 h10 = (h10) bluetoothCommand;
        this.A = h10.j();
        this.B = h10.i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, wa0.a(wa0.a(new JSONObject(), JSONKey.SERVICES, s60.a(this.A)), JSONKey.CHARACTERISTICS, q10.a(this.B)), 7, (fd4) null));
    }
}
