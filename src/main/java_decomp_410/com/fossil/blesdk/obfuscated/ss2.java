package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ss2 extends RecyclerView.g<a> {
    @DexIgnore
    public /* final */ en2 a; // = this.e.u();
    @DexIgnore
    public /* final */ ArrayList<HomeProfilePresenter.b> b;
    @DexIgnore
    public /* final */ xn c;
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ ImageView a;
        @DexIgnore
        public /* final */ ImageView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ FlexibleTextView d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ ImageView f;
        @DexIgnore
        public /* final */ FlexibleTextView g;
        @DexIgnore
        public /* final */ CardView h;
        @DexIgnore
        public /* final */ /* synthetic */ ss2 i;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ss2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ss2$a$a  reason: collision with other inner class name */
        public static final class C0105a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0105a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.i.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    b c = this.e.i.d;
                    if (c != null) {
                        c.P(((HomeProfilePresenter.b) this.e.i.b.get(this.e.getAdapterPosition())).c());
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public void onImageCallback(String str, String str2) {
                kd4.b(str, "serial");
                kd4.b(str2, "filePath");
                this.a.i.c.a(str2).a(this.a.a);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ss2 ss2, View view) {
            super(view);
            kd4.b(view, "view");
            this.i = ss2;
            this.a = (ImageView) view.findViewById(R.id.iv_device);
            this.b = (ImageView) view.findViewById(R.id.iv_selected);
            this.c = (FlexibleTextView) view.findViewById(R.id.tv_device_name);
            this.d = (FlexibleTextView) view.findViewById(R.id.tv_last_synced_time);
            this.e = (ConstraintLayout) view.findViewById(R.id.cl_container);
            this.f = (ImageView) view.findViewById(R.id.iv_setting);
            this.g = (FlexibleTextView) view.findViewById(R.id.tv_status);
            this.h = (CardView) view.findViewById(R.id.card_view);
            this.f.setOnClickListener(new C0105a(this));
        }

        @DexIgnore
        @SuppressLint({"SetTextI18n"})
        public final void a(HomeProfilePresenter.b bVar) {
            int i2;
            kd4.b(bVar, "device");
            FlexibleTextView flexibleTextView = this.c;
            kd4.a((Object) flexibleTextView, "mTvDeviceName");
            flexibleTextView.setText(rs3.d(bVar.b()));
            if (bVar.d()) {
                CardView cardView = this.h;
                kd4.a((Object) cardView, "mCardView");
                cardView.setRadius(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                CardView cardView2 = this.h;
                kd4.a((Object) cardView2, "mCardView");
                cardView2.setCardElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.b.setImageResource(R.drawable.element_radio_on);
                ConstraintLayout constraintLayout = this.e;
                kd4.a((Object) constraintLayout, "mClContainer");
                constraintLayout.setAlpha(1.0f);
                ConstraintLayout constraintLayout2 = this.e;
                kd4.a((Object) constraintLayout2, "mClContainer");
                constraintLayout2.setBackground(k6.c(this.i.e, R.drawable.bg_border_active_device));
                if (bVar.a() >= 0) {
                    FlexibleTextView flexibleTextView2 = this.g;
                    kd4.a((Object) flexibleTextView2, "mTvDeviceStatus");
                    StringBuilder sb = new StringBuilder();
                    sb.append(bVar.a());
                    sb.append('%');
                    flexibleTextView2.setText(sb.toString());
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, k6.c(this.i.e, DeviceHelper.o.a(bVar.a())), (Drawable) null);
                } else {
                    FlexibleTextView flexibleTextView3 = this.g;
                    kd4.a((Object) flexibleTextView3, "mTvDeviceStatus");
                    flexibleTextView3.setText("");
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                }
                j5 j5Var = new j5();
                ImageView imageView = this.f;
                kd4.a((Object) imageView, "mIvSetting");
                imageView.setVisibility(0);
                j5Var.c(this.e);
                FlexibleTextView flexibleTextView4 = this.d;
                kd4.a((Object) flexibleTextView4, "mTvLastSyncedTime");
                int id = flexibleTextView4.getId();
                ImageView imageView2 = this.f;
                kd4.a((Object) imageView2, "mIvSetting");
                j5Var.a(id, 7, imageView2.getId(), 6);
                j5Var.a(this.e);
                if (bVar.e()) {
                    i2 = k6.a((Context) this.i.e, (int) R.color.activeColorPrimary);
                } else {
                    FlexibleTextView flexibleTextView5 = this.g;
                    kd4.a((Object) flexibleTextView5, "mTvDeviceStatus");
                    flexibleTextView5.setText(sm2.a((Context) this.i.e, (int) R.string.Profile_MyWatch_DianaProfile_Text__Disconnected));
                    this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    i2 = k6.a((Context) this.i.e, (int) R.color.error);
                }
                this.c.setTextColor(i2);
                FlexibleTextView flexibleTextView6 = this.c;
                kd4.a((Object) flexibleTextView6, "mTvDeviceName");
                ft3.a((TextView) flexibleTextView6, i2);
                FlexibleTextView flexibleTextView7 = this.d;
                kd4.a((Object) flexibleTextView7, "mTvLastSyncedTime");
                flexibleTextView7.setText(this.i.a(bVar.c(), true));
            } else {
                CardView cardView3 = this.h;
                kd4.a((Object) cardView3, "mCardView");
                cardView3.setRadius(2.0f);
                CardView cardView4 = this.h;
                kd4.a((Object) cardView4, "mCardView");
                cardView4.setCardElevation(2.0f);
                this.b.setImageResource(R.drawable.element_radio_off);
                ConstraintLayout constraintLayout3 = this.e;
                kd4.a((Object) constraintLayout3, "mClContainer");
                constraintLayout3.setAlpha(0.7f);
                ConstraintLayout constraintLayout4 = this.e;
                kd4.a((Object) constraintLayout4, "mClContainer");
                constraintLayout4.setBackground(k6.c(this.i.e, R.drawable.bg_normal));
                this.c.setTextColor(k6.a((Context) this.i.e, (int) R.color.placeholderText));
                FlexibleTextView flexibleTextView8 = this.g;
                kd4.a((Object) flexibleTextView8, "mTvDeviceStatus");
                flexibleTextView8.setText(sm2.a((Context) this.i.e, (int) R.string.Profile_MyWatch_DianaProfile_Text__Disconnected));
                this.g.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                this.c.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                FlexibleTextView flexibleTextView9 = this.d;
                kd4.a((Object) flexibleTextView9, "mTvLastSyncedTime");
                flexibleTextView9.setText(this.i.a(bVar.c(), false));
            }
            CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(bVar.c()).setSerialPrefix(DeviceHelper.o.b(bVar.c())).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView3 = this.a;
            kd4.a((Object) imageView3, "mIvDevice");
            type.setPlaceHolder(imageView3, DeviceHelper.o.b(bVar.c(), DeviceHelper.ImageStyle.SMALL)).setImageCallback(new b(this)).download();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void P(String str);
    }

    @DexIgnore
    public ss2(ArrayList<HomeProfilePresenter.b> arrayList, xn xnVar, b bVar, PortfolioApp portfolioApp) {
        kd4.b(arrayList, "mData");
        kd4.b(xnVar, "mRequestManager");
        kd4.b(portfolioApp, "mApp");
        this.b = arrayList;
        this.c = xnVar;
        this.d = bVar;
        this.e = portfolioApp;
    }

    @DexIgnore
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final void b() {
        if (getItemCount() > 0) {
            notifyItemChanged(0);
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_home_profile_device, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026le_device, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(ArrayList<HomeProfilePresenter.b> arrayList) {
        kd4.b(arrayList, "data");
        this.b.clear();
        this.b.addAll(arrayList);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            HomeProfilePresenter.b bVar = this.b.get(i);
            kd4.a((Object) bVar, "mData[position]");
            aVar.a(bVar);
        }
    }

    @DexIgnore
    public final String a(String str, boolean z) {
        String str2;
        if (!z || !this.e.h(str)) {
            long g = this.a.g(str);
            if (g == 0) {
                str2 = "";
            } else if (System.currentTimeMillis() - g < 60000) {
                pd4 pd4 = pd4.a;
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_WatchSettings_Label__NumbermAgo);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                Object[] objArr = {1};
                str2 = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) str2, "java.lang.String.format(format, *args)");
            } else {
                str2 = rk2.a(g);
            }
            String string = this.e.getString(R.string.Profile_MyWatch_DianaProfile_Text__LastSyncedDayTime, new Object[]{str2});
            kd4.a((Object) string, "mApp.getString(R.string.\u2026ayTime, lastSyncTimeText)");
            return string;
        }
        String string2 = this.e.getString(R.string.DesignPatterns_AndroidQuickAccessPanel_SyncInProgressExpanded_Text__SyncInProgress);
        kd4.a((Object) string2, "mApp.getString(R.string.\u2026ded_Text__SyncInProgress)");
        return string2;
    }
}
