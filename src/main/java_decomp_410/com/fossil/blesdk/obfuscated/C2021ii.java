package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ii */
public class C2021ii implements com.fossil.blesdk.obfuscated.C2128ji {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.WindowId f6037a;

    @DexIgnore
    public C2021ii(android.view.View view) {
        this.f6037a = view.getWindowId();
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        return (obj instanceof com.fossil.blesdk.obfuscated.C2021ii) && ((com.fossil.blesdk.obfuscated.C2021ii) obj).f6037a.equals(this.f6037a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f6037a.hashCode();
    }
}
