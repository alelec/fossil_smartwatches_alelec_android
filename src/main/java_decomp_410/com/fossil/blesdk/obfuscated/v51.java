package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v51 extends vb1<v51> {
    @DexIgnore
    public static volatile v51[] h;
    @DexIgnore
    public Integer c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public t51 e; // = null;
    @DexIgnore
    public Boolean f; // = null;
    @DexIgnore
    public Boolean g; // = null;

    @DexIgnore
    public v51() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static v51[] e() {
        if (h == null) {
            synchronized (zb1.b) {
                if (h == null) {
                    h = new v51[0];
                }
            }
        }
        return h;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Integer num = this.c;
        if (num != null) {
            ub1.b(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            ub1.a(2, str);
        }
        t51 t51 = this.e;
        if (t51 != null) {
            ub1.a(3, (ac1) t51);
        }
        Boolean bool = this.f;
        if (bool != null) {
            ub1.a(4, bool.booleanValue());
        }
        Boolean bool2 = this.g;
        if (bool2 != null) {
            ub1.a(5, bool2.booleanValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v51)) {
            return false;
        }
        v51 v51 = (v51) obj;
        Integer num = this.c;
        if (num == null) {
            if (v51.c != null) {
                return false;
            }
        } else if (!num.equals(v51.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (v51.d != null) {
                return false;
            }
        } else if (!str.equals(v51.d)) {
            return false;
        }
        t51 t51 = this.e;
        if (t51 == null) {
            if (v51.e != null) {
                return false;
            }
        } else if (!t51.equals(v51.e)) {
            return false;
        }
        Boolean bool = this.f;
        if (bool == null) {
            if (v51.f != null) {
                return false;
            }
        } else if (!bool.equals(v51.f)) {
            return false;
        }
        Boolean bool2 = this.g;
        if (bool2 == null) {
            if (v51.g != null) {
                return false;
            }
        } else if (!bool2.equals(v51.g)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(v51.b);
        }
        xb1 xb12 = v51.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int i;
        int hashCode = (v51.class.getName().hashCode() + 527) * 31;
        Integer num = this.c;
        int i2 = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = hashCode2 + (str == null ? 0 : str.hashCode());
        t51 t51 = this.e;
        int i3 = hashCode3 * 31;
        if (t51 == null) {
            i = 0;
        } else {
            i = t51.hashCode();
        }
        int i4 = (i3 + i) * 31;
        Boolean bool = this.f;
        int hashCode4 = (i4 + (bool == null ? 0 : bool.hashCode())) * 31;
        Boolean bool2 = this.g;
        int hashCode5 = (hashCode4 + (bool2 == null ? 0 : bool2.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i2 = this.b.hashCode();
        }
        return hashCode5 + i2;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Integer num = this.c;
        if (num != null) {
            a += ub1.c(1, num.intValue());
        }
        String str = this.d;
        if (str != null) {
            a += ub1.b(2, str);
        }
        t51 t51 = this.e;
        if (t51 != null) {
            a += ub1.b(3, (ac1) t51);
        }
        Boolean bool = this.f;
        if (bool != null) {
            bool.booleanValue();
            a += ub1.c(4) + 1;
        }
        Boolean bool2 = this.g;
        if (bool2 == null) {
            return a;
        }
        bool2.booleanValue();
        return a + ub1.c(5) + 1;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Integer.valueOf(tb1.e());
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 26) {
                if (this.e == null) {
                    this.e = new t51();
                }
                tb1.a((ac1) this.e);
            } else if (c2 == 32) {
                this.f = Boolean.valueOf(tb1.d());
            } else if (c2 == 40) {
                this.g = Boolean.valueOf(tb1.d());
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
