package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ti1 implements vi1 {
    @DexIgnore
    public /* final */ xh1 a;

    @DexIgnore
    public ti1(xh1 xh1) {
        bk0.a(xh1);
        this.a = xh1;
    }

    @DexIgnore
    public th1 a() {
        return this.a.a();
    }

    @DexIgnore
    public ul1 b() {
        return this.a.b();
    }

    @DexIgnore
    public hm0 c() {
        return this.a.c();
    }

    @DexIgnore
    public tg1 d() {
        return this.a.d();
    }

    @DexIgnore
    public void e() {
        this.a.a().e();
    }

    @DexIgnore
    public void f() {
        this.a.i();
    }

    @DexIgnore
    public void g() {
        this.a.a().g();
    }

    @DexIgnore
    public Context getContext() {
        return this.a.getContext();
    }

    @DexIgnore
    public bg1 h() {
        return this.a.q();
    }

    @DexIgnore
    public rg1 i() {
        return this.a.r();
    }

    @DexIgnore
    public nl1 j() {
        return this.a.s();
    }

    @DexIgnore
    public fh1 k() {
        return this.a.t();
    }

    @DexIgnore
    public xl1 l() {
        return this.a.u();
    }
}
