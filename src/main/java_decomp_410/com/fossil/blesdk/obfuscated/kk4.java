package com.fossil.blesdk.obfuscated;

import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kk4 {
    @DexIgnore
    public static final <T> void a(xc4<? super yb4<? super T>, ? extends Object> xc4, yb4<? super T> yb4) {
        kd4.b(xc4, "$this$startCoroutineCancellable");
        kd4.b(yb4, "completion");
        try {
            lh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(IntrinsicsKt__IntrinsicsJvmKt.a(xc4, yb4)), qa4.a);
        } catch (Throwable th) {
            Result.a aVar = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(yc4<? super R, ? super yb4<? super T>, ? extends Object> yc4, R r, yb4<? super T> yb4) {
        kd4.b(yc4, "$this$startCoroutineCancellable");
        kd4.b(yb4, "completion");
        try {
            lh4.a(IntrinsicsKt__IntrinsicsJvmKt.a(IntrinsicsKt__IntrinsicsJvmKt.a(yc4, r, yb4)), qa4.a);
        } catch (Throwable th) {
            Result.a aVar = Result.Companion;
            yb4.resumeWith(Result.m3constructorimpl(na4.a(th)));
        }
    }
}
