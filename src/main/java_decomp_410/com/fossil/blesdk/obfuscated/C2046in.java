package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.in */
public class C2046in {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.in$a")
    /* renamed from: com.fossil.blesdk.obfuscated.in$a */
    public class C2047a {
        @DexIgnore
        /* renamed from: a */
        public void mo12052a() {
            throw null;
        }

        @DexIgnore
        /* renamed from: b */
        public java.lang.String mo12053b() {
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.in$b */
    public interface C2048b extends com.fossil.blesdk.obfuscated.C3047um.C3048a {
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2046in.C2047a mo12051a(java.lang.String str, com.fossil.blesdk.obfuscated.C2046in.C2048b bVar, int i, int i2, android.widget.ImageView.ScaleType scaleType) {
        throw null;
    }
}
