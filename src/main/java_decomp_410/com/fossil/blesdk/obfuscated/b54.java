package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b54<T> extends a54<T> {
    @DexIgnore
    public T b;

    @DexIgnore
    public b54() {
        this((c54) null);
    }

    @DexIgnore
    public T a(Context context) {
        return this.b;
    }

    @DexIgnore
    public void b(Context context, T t) {
        this.b = t;
    }

    @DexIgnore
    public b54(c54<T> c54) {
        super(c54);
    }
}
