package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import androidx.work.ExistingWorkPolicy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rj extends ij {
    @DexIgnore
    public static /* final */ String j; // = dj.a("WorkContinuationImpl");
    @DexIgnore
    public /* final */ tj a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ExistingWorkPolicy c;
    @DexIgnore
    public /* final */ List<? extends kj> d;
    @DexIgnore
    public /* final */ List<String> e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<rj> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public fj i;

    @DexIgnore
    public rj(tj tjVar, String str, ExistingWorkPolicy existingWorkPolicy, List<? extends kj> list) {
        this(tjVar, str, existingWorkPolicy, list, (List<rj>) null);
    }

    @DexIgnore
    public fj a() {
        if (!this.h) {
            pl plVar = new pl(this);
            this.a.h().a(plVar);
            this.i = plVar.b();
        } else {
            dj.a().e(j, String.format("Already enqueued work ids (%s)", new Object[]{TextUtils.join(", ", this.e)}), new Throwable[0]);
        }
        return this.i;
    }

    @DexIgnore
    public ExistingWorkPolicy b() {
        return this.c;
    }

    @DexIgnore
    public List<String> c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public List<rj> e() {
        return this.g;
    }

    @DexIgnore
    public List<? extends kj> f() {
        return this.d;
    }

    @DexIgnore
    public tj g() {
        return this.a;
    }

    @DexIgnore
    public boolean h() {
        return a(this, new HashSet());
    }

    @DexIgnore
    public boolean i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        this.h = true;
    }

    @DexIgnore
    public rj(tj tjVar, String str, ExistingWorkPolicy existingWorkPolicy, List<? extends kj> list, List<rj> list2) {
        this.a = tjVar;
        this.b = str;
        this.c = existingWorkPolicy;
        this.d = list;
        this.g = list2;
        this.e = new ArrayList(this.d.size());
        this.f = new ArrayList();
        if (list2 != null) {
            for (rj rjVar : list2) {
                this.f.addAll(rjVar.f);
            }
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            String b2 = ((kj) list.get(i2)).b();
            this.e.add(b2);
            this.f.add(b2);
        }
    }

    @DexIgnore
    public static boolean a(rj rjVar, Set<String> set) {
        set.addAll(rjVar.c());
        Set<String> a2 = a(rjVar);
        for (String contains : set) {
            if (a2.contains(contains)) {
                return true;
            }
        }
        List<rj> e2 = rjVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (rj a3 : e2) {
                if (a(a3, set)) {
                    return true;
                }
            }
        }
        set.removeAll(rjVar.c());
        return false;
    }

    @DexIgnore
    public static Set<String> a(rj rjVar) {
        HashSet hashSet = new HashSet();
        List<rj> e2 = rjVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (rj c2 : e2) {
                hashSet.addAll(c2.c());
            }
        }
        return hashSet;
    }
}
