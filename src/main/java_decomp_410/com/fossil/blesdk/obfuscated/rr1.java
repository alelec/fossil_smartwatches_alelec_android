package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rr1 implements p1 {
    @DexIgnore
    public h1 e;
    @DexIgnore
    public BottomNavigationMenuView f;
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public int h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new C0033a();
        @DexIgnore
        public int e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.rr1$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.rr1$a$a  reason: collision with other inner class name */
        public static class C0033a implements Parcelable.Creator<a> {
            @DexIgnore
            public a createFromParcel(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            public a[] newArray(int i) {
                return new a[i];
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.e);
        }

        @DexIgnore
        public a(Parcel parcel) {
            this.e = parcel.readInt();
        }
    }

    @DexIgnore
    public void a(h1 h1Var, boolean z) {
    }

    @DexIgnore
    public void a(BottomNavigationMenuView bottomNavigationMenuView) {
        this.f = bottomNavigationMenuView;
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public boolean a(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public boolean a(v1 v1Var) {
        return false;
    }

    @DexIgnore
    public Parcelable b() {
        a aVar = new a();
        aVar.e = this.f.getSelectedItemId();
        return aVar;
    }

    @DexIgnore
    public boolean b(h1 h1Var, k1 k1Var) {
        return false;
    }

    @DexIgnore
    public int getId() {
        return this.h;
    }

    @DexIgnore
    public void a(Context context, h1 h1Var) {
        this.e = h1Var;
        this.f.a(this.e);
    }

    @DexIgnore
    public void b(boolean z) {
        this.g = z;
    }

    @DexIgnore
    public void a(boolean z) {
        if (!this.g) {
            if (z) {
                this.f.a();
            } else {
                this.f.c();
            }
        }
    }

    @DexIgnore
    public void a(int i) {
        this.h = i;
    }

    @DexIgnore
    public void a(Parcelable parcelable) {
        if (parcelable instanceof a) {
            this.f.b(((a) parcelable).e);
        }
    }
}
