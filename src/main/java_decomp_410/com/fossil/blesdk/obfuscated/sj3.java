package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sj3 {
    @DexIgnore
    public /* final */ yj3 a;

    @DexIgnore
    public sj3(yj3 yj3) {
        kd4.b(yj3, "view");
        st1.a(yj3, "view cannot be null!", new Object[0]);
        kd4.a((Object) yj3, "checkNotNull(view, \"view cannot be null!\")");
        this.a = yj3;
    }

    @DexIgnore
    public final yj3 a() {
        return this.a;
    }
}
