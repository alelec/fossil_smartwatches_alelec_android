package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CirclePageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ob2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ CirclePageIndicator r;
    @DexIgnore
    public /* final */ DashBar s;
    @DexIgnore
    public /* final */ RecyclerViewPager t;

    @DexIgnore
    public ob2(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, CirclePageIndicator circlePageIndicator, DashBar dashBar, RecyclerViewPager recyclerViewPager) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = circlePageIndicator;
        this.s = dashBar;
        this.t = recyclerViewPager;
    }
}
