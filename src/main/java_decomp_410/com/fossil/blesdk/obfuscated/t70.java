package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t70 extends p70 {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t70(Peripheral peripheral) {
        super(DeviceConfigOperationCode.SET_CALIBRATION_POSITION, RequestId.SET_CALIBRATION_POSITION, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        c(true);
    }
}
