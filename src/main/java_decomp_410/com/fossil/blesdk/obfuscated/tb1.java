package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.google.android.gms.internal.measurement.zzuv;
import com.google.android.gms.internal.measurement.zzyh;
import java.io.IOException;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tb1 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h; // = Integer.MAX_VALUE;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j; // = 64;
    @DexIgnore
    public y71 k;

    @DexIgnore
    public tb1(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        int i4 = i3 + i2;
        this.d = i4;
        this.c = i4;
        this.f = i2;
    }

    @DexIgnore
    public static tb1 a(byte[] bArr, int i2, int i3) {
        return new tb1(bArr, 0, i3);
    }

    @DexIgnore
    public final boolean b(int i2) throws IOException {
        int c2;
        int i3 = i2 & 7;
        if (i3 == 0) {
            e();
            return true;
        } else if (i3 == 1) {
            h();
            return true;
        } else if (i3 == 2) {
            e(e());
            return true;
        } else if (i3 == 3) {
            do {
                c2 = c();
                if (c2 == 0) {
                    break;
                }
            } while (b(c2));
            a(((i2 >>> 3) << 3) | 4);
            return true;
        } else if (i3 == 4) {
            return false;
        } else {
            if (i3 == 5) {
                g();
                return true;
            }
            throw new zzyh("Protocol message tag had invalid wire type.");
        }
    }

    @DexIgnore
    public final int c() throws IOException {
        if (this.f == this.d) {
            this.g = 0;
            return 0;
        }
        this.g = e();
        int i2 = this.g;
        if (i2 != 0) {
            return i2;
        }
        throw new zzyh("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public final boolean d() throws IOException {
        return e() != 0;
    }

    @DexIgnore
    public final int e() throws IOException {
        int i2;
        byte j2 = j();
        if (j2 >= 0) {
            return j2;
        }
        byte b2 = j2 & Byte.MAX_VALUE;
        byte j3 = j();
        if (j3 >= 0) {
            i2 = j3 << 7;
        } else {
            b2 |= (j3 & Byte.MAX_VALUE) << 7;
            byte j4 = j();
            if (j4 >= 0) {
                i2 = j4 << DateTimeFieldType.HOUR_OF_HALFDAY;
            } else {
                b2 |= (j4 & Byte.MAX_VALUE) << DateTimeFieldType.HOUR_OF_HALFDAY;
                byte j5 = j();
                if (j5 >= 0) {
                    i2 = j5 << DateTimeFieldType.SECOND_OF_MINUTE;
                } else {
                    byte b3 = b2 | ((j5 & Byte.MAX_VALUE) << DateTimeFieldType.SECOND_OF_MINUTE);
                    byte j6 = j();
                    byte b4 = b3 | (j6 << 28);
                    if (j6 >= 0) {
                        return b4;
                    }
                    for (int i3 = 0; i3 < 5; i3++) {
                        if (j() >= 0) {
                            return b4;
                        }
                    }
                    throw zzyh.zzzf();
                }
            }
        }
        return b2 | i2;
    }

    @DexIgnore
    public final long f() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte j3 = j();
            j2 |= ((long) (j3 & Byte.MAX_VALUE)) << i2;
            if ((j3 & 128) == 0) {
                return j2;
            }
        }
        throw zzyh.zzzf();
    }

    @DexIgnore
    public final int g() throws IOException {
        return (j() & FileType.MASKED_INDEX) | ((j() & FileType.MASKED_INDEX) << 8) | ((j() & FileType.MASKED_INDEX) << DateTimeFieldType.CLOCKHOUR_OF_DAY) | ((j() & FileType.MASKED_INDEX) << 24);
    }

    @DexIgnore
    public final long h() throws IOException {
        byte j2 = j();
        byte j3 = j();
        return ((((long) j3) & 255) << 8) | (((long) j2) & 255) | ((((long) j()) & 255) << 16) | ((((long) j()) & 255) << 24) | ((((long) j()) & 255) << 32) | ((((long) j()) & 255) << 40) | ((((long) j()) & 255) << 48) | ((((long) j()) & 255) << 56);
    }

    @DexIgnore
    public final void i() {
        this.d += this.e;
        int i2 = this.d;
        int i3 = this.h;
        if (i2 > i3) {
            this.e = i2 - i3;
            this.d = i2 - this.e;
            return;
        }
        this.e = 0;
    }

    @DexIgnore
    public final byte j() throws IOException {
        int i2 = this.f;
        if (i2 != this.d) {
            byte[] bArr = this.a;
            this.f = i2 + 1;
            return bArr[i2];
        }
        throw zzyh.zzzd();
    }

    @DexIgnore
    public final y71 k() throws IOException {
        if (this.k == null) {
            this.k = y71.a(this.a, this.b, this.c);
        }
        int t = this.k.t();
        int i2 = this.f - this.b;
        if (t <= i2) {
            this.k.f(i2 - t);
            this.k.c(this.j - this.i);
            return this.k;
        }
        throw new IOException(String.format("CodedInputStream read ahead of CodedInputByteBufferNano: %s > %s", new Object[]{Integer.valueOf(t), Integer.valueOf(i2)}));
    }

    @DexIgnore
    public final int l() {
        int i2 = this.h;
        if (i2 == Integer.MAX_VALUE) {
            return -1;
        }
        return i2 - this.f;
    }

    @DexIgnore
    public final void a(int i2) throws zzyh {
        if (this.g != i2) {
            throw new zzyh("Protocol message end-group tag did not match expected tag.");
        }
    }

    @DexIgnore
    public final void d(int i2) {
        this.h = i2;
        i();
    }

    @DexIgnore
    public final void f(int i2) {
        b(i2, this.g);
    }

    @DexIgnore
    public final void a(ac1 ac1) throws IOException {
        int e2 = e();
        if (this.i < this.j) {
            int c2 = c(e2);
            this.i++;
            ac1.a(this);
            a(0);
            this.i--;
            d(c2);
            return;
        }
        throw zzyh.zzzg();
    }

    @DexIgnore
    public final int c(int i2) throws zzyh {
        if (i2 >= 0) {
            int i3 = i2 + this.f;
            int i4 = this.h;
            if (i3 <= i4) {
                this.h = i3;
                i();
                return i4;
            }
            throw zzyh.zzzd();
        }
        throw zzyh.zzze();
    }

    @DexIgnore
    public final void e(int i2) throws IOException {
        if (i2 >= 0) {
            int i3 = this.f;
            int i4 = i3 + i2;
            int i5 = this.h;
            if (i4 > i5) {
                e(i5 - i3);
                throw zzyh.zzzd();
            } else if (i2 <= this.d - i3) {
                this.f = i3 + i2;
            } else {
                throw zzyh.zzzd();
            }
        } else {
            throw zzyh.zzze();
        }
    }

    @DexIgnore
    public final String b() throws IOException {
        int e2 = e();
        if (e2 >= 0) {
            int i2 = this.d;
            int i3 = this.f;
            if (e2 <= i2 - i3) {
                String str = new String(this.a, i3, e2, zb1.a);
                this.f += e2;
                return str;
            }
            throw zzyh.zzzd();
        }
        throw zzyh.zzze();
    }

    @DexIgnore
    public final <T extends t81<T, ?>> T a(ha1<T> ha1) throws IOException {
        try {
            T t = (t81) k().a(ha1, i81.d());
            b(this.g);
            return t;
        } catch (zzuv e2) {
            throw new zzyh("", e2);
        }
    }

    @DexIgnore
    public final void b(int i2, int i3) {
        int i4 = this.f;
        int i5 = this.b;
        if (i2 > i4 - i5) {
            StringBuilder sb = new StringBuilder(50);
            sb.append("Position ");
            sb.append(i2);
            sb.append(" is beyond current ");
            sb.append(i4 - i5);
            throw new IllegalArgumentException(sb.toString());
        } else if (i2 >= 0) {
            this.f = i5 + i2;
            this.g = i3;
        } else {
            StringBuilder sb2 = new StringBuilder(24);
            sb2.append("Bad position ");
            sb2.append(i2);
            throw new IllegalArgumentException(sb2.toString());
        }
    }

    @DexIgnore
    public final int a() {
        return this.f - this.b;
    }

    @DexIgnore
    public final byte[] a(int i2, int i3) {
        if (i3 == 0) {
            return dc1.d;
        }
        byte[] bArr = new byte[i3];
        System.arraycopy(this.a, this.b + i2, bArr, 0, i3);
        return bArr;
    }
}
