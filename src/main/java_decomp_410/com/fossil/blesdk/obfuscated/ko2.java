package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ko2 implements Factory<jo2> {
    @DexIgnore
    public /* final */ Provider<DianaNotificationComponent> a;
    @DexIgnore
    public /* final */ Provider<en2> b;

    @DexIgnore
    public ko2(Provider<DianaNotificationComponent> provider, Provider<en2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static ko2 a(Provider<DianaNotificationComponent> provider, Provider<en2> provider2) {
        return new ko2(provider, provider2);
    }

    @DexIgnore
    public static jo2 b(Provider<DianaNotificationComponent> provider, Provider<en2> provider2) {
        jo2 jo2 = new jo2();
        lo2.a(jo2, provider.get());
        lo2.a(jo2, provider2.get());
        return jo2;
    }

    @DexIgnore
    public jo2 get() {
        return b(this.a, this.b);
    }
}
