package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jk */
public class C2130jk extends com.fossil.blesdk.obfuscated.C2331lk<java.lang.Boolean> {
    @DexIgnore
    public C2130jk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(com.fossil.blesdk.obfuscated.C3294xk.m16376a(context, zlVar).mo17768a());
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12371a(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        return hlVar.f5780j.mo18117g();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12373b(java.lang.Boolean bool) {
        return !bool.booleanValue();
    }
}
