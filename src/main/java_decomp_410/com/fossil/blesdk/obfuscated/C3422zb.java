package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zb */
public class C3422zb {

    @DexIgnore
    /* renamed from: a */
    public static java.util.Map<java.lang.Class, java.lang.Integer> f11527a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: b */
    public static java.util.Map<java.lang.Class, java.util.List<java.lang.reflect.Constructor<? extends com.fossil.blesdk.obfuscated.C2868sb>>> f11528b; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3103vb m17308a(java.lang.Object obj) {
        boolean z = obj instanceof com.fossil.blesdk.obfuscated.C3103vb;
        boolean z2 = obj instanceof com.fossil.blesdk.obfuscated.C2795rb;
        if (z && z2) {
            return new androidx.lifecycle.FullLifecycleObserverAdapter((com.fossil.blesdk.obfuscated.C2795rb) obj, (com.fossil.blesdk.obfuscated.C3103vb) obj);
        }
        if (z2) {
            return new androidx.lifecycle.FullLifecycleObserverAdapter((com.fossil.blesdk.obfuscated.C2795rb) obj, (com.fossil.blesdk.obfuscated.C3103vb) null);
        }
        if (z) {
            return (com.fossil.blesdk.obfuscated.C3103vb) obj;
        }
        java.lang.Class<?> cls = obj.getClass();
        if (m17311b(cls) != 2) {
            return new androidx.lifecycle.ReflectiveGenericLifecycleObserver(obj);
        }
        java.util.List list = f11528b.get(cls);
        if (list.size() == 1) {
            return new androidx.lifecycle.SingleGeneratedAdapterObserver(m17307a((java.lang.reflect.Constructor) list.get(0), obj));
        }
        com.fossil.blesdk.obfuscated.C2868sb[] sbVarArr = new com.fossil.blesdk.obfuscated.C2868sb[list.size()];
        for (int i = 0; i < list.size(); i++) {
            sbVarArr[i] = m17307a((java.lang.reflect.Constructor) list.get(i), obj);
        }
        return new androidx.lifecycle.CompositeGeneratedAdaptersObserver(sbVarArr);
    }

    @DexIgnore
    /* renamed from: b */
    public static int m17311b(java.lang.Class<?> cls) {
        java.lang.Integer num = f11527a.get(cls);
        if (num != null) {
            return num.intValue();
        }
        int d = m17313d(cls);
        f11527a.put(cls, java.lang.Integer.valueOf(d));
        return d;
    }

    @DexIgnore
    /* renamed from: c */
    public static boolean m17312c(java.lang.Class<?> cls) {
        return cls != null && com.fossil.blesdk.obfuscated.C3179wb.class.isAssignableFrom(cls);
    }

    @DexIgnore
    /* renamed from: d */
    public static int m17313d(java.lang.Class<?> cls) {
        if (cls.getCanonicalName() == null) {
            return 1;
        }
        java.lang.reflect.Constructor<? extends com.fossil.blesdk.obfuscated.C2868sb> a = m17310a(cls);
        if (a != null) {
            f11528b.put(cls, java.util.Collections.singletonList(a));
            return 2;
        } else if (com.fossil.blesdk.obfuscated.C2471nb.f7694c.mo13899c(cls)) {
            return 1;
        } else {
            java.lang.Class<? super java.lang.Object> superclass = cls.getSuperclass();
            java.util.ArrayList arrayList = null;
            if (m17312c(superclass)) {
                if (m17311b(superclass) == 1) {
                    return 1;
                }
                arrayList = new java.util.ArrayList(f11528b.get(superclass));
            }
            for (java.lang.Class cls2 : cls.getInterfaces()) {
                if (m17312c(cls2)) {
                    if (m17311b(cls2) == 1) {
                        return 1;
                    }
                    if (arrayList == null) {
                        arrayList = new java.util.ArrayList();
                    }
                    arrayList.addAll(f11528b.get(cls2));
                }
            }
            if (arrayList == null) {
                return 1;
            }
            f11528b.put(cls, arrayList);
            return 2;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2868sb m17307a(java.lang.reflect.Constructor<? extends com.fossil.blesdk.obfuscated.C2868sb> constructor, java.lang.Object obj) {
        try {
            return (com.fossil.blesdk.obfuscated.C2868sb) constructor.newInstance(new java.lang.Object[]{obj});
        } catch (java.lang.IllegalAccessException e) {
            throw new java.lang.RuntimeException(e);
        } catch (java.lang.InstantiationException e2) {
            throw new java.lang.RuntimeException(e2);
        } catch (java.lang.reflect.InvocationTargetException e3) {
            throw new java.lang.RuntimeException(e3);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Constructor<? extends com.fossil.blesdk.obfuscated.C2868sb> m17310a(java.lang.Class<?> cls) {
        try {
            java.lang.Package packageR = cls.getPackage();
            java.lang.String canonicalName = cls.getCanonicalName();
            java.lang.String name = packageR != null ? packageR.getName() : "";
            if (!name.isEmpty()) {
                canonicalName = canonicalName.substring(name.length() + 1);
            }
            java.lang.String a = m17309a(canonicalName);
            if (!name.isEmpty()) {
                a = name + com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME + a;
            }
            java.lang.reflect.Constructor<?> declaredConstructor = java.lang.Class.forName(a).getDeclaredConstructor(new java.lang.Class[]{cls});
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return declaredConstructor;
        } catch (java.lang.ClassNotFoundException unused) {
            return null;
        } catch (java.lang.NoSuchMethodException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m17309a(java.lang.String str) {
        return str.replace(com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME, "_") + "_LifecycleAdapter";
    }
}
