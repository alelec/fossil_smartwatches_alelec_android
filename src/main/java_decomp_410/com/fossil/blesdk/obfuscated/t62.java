package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t62 extends RecyclerView.g<c> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<WatchApp> c;
    @DexIgnore
    public b d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(WatchApp watchApp);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public WatchApp d;
        @DexIgnore
        public /* final */ /* synthetic */ t62 e;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.e.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    b c = this.e.e.c();
                    if (c != null) {
                        Object obj = this.e.e.c.get(this.e.getAdapterPosition());
                        kd4.a(obj, "mData[adapterPosition]");
                        c.a((WatchApp) obj);
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void a(CustomizeWidget customizeWidget) {
                kd4.b(customizeWidget, "view");
                this.a.e.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(t62 t62, View view) {
            super(view);
            kd4.b(view, "view");
            this.e = t62;
            this.c = view.findViewById(R.id.iv_indicator);
            View findViewById = view.findViewById(R.id.wc_watch_app);
            kd4.a((Object) findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(R.id.tv_watch_app_name);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.a.setOnClickListener(new a(this));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x008a, code lost:
            if (r0 != null) goto L_0x008f;
         */
        @DexIgnore
        public final void a(WatchApp watchApp) {
            String str;
            kd4.b(watchApp, "watchApp");
            this.d = watchApp;
            if (this.d != null) {
                this.a.d(watchApp.getWatchappId());
                this.b.setText(sm2.a(PortfolioApp.W.c(), watchApp.getNameKey(), watchApp.getName()));
            }
            this.a.setSelectedWc(kd4.a((Object) watchApp.getWatchappId(), (Object) this.e.b));
            if (kd4.a((Object) watchApp.getWatchappId(), (Object) this.e.b)) {
                View view = this.c;
                kd4.a((Object) view, "ivIndicator");
                view.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.drawable_line_arrow));
            } else {
                View view2 = this.c;
                kd4.a((Object) view2, "ivIndicator");
                view2.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.drawable_line_grey));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            WatchApp watchApp2 = this.d;
            if (watchApp2 != null) {
                str = watchApp2.getWatchappId();
            }
            str = "";
            Intent putExtra = intent.putExtra("KEY_ID", str);
            kd4.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "WATCH_APP", putExtra, (View.OnDragListener) null, new b(this), 4, (Object) null);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ t62(ArrayList arrayList, b bVar, int i, fd4 fd4) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : bVar);
    }

    @DexIgnore
    public final b c() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d("WatchAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_watch_app, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public t62(ArrayList<WatchApp> arrayList, b bVar) {
        kd4.b(arrayList, "mData");
        this.c = arrayList;
        this.d = bVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<WatchApp> list) {
        kd4.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "watchAppId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        kd4.b(str, "watchAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (kd4.a((Object) ((WatchApp) t).getWatchappId(), (Object) str)) {
                break;
            }
        }
        WatchApp watchApp = (WatchApp) t;
        if (watchApp != null) {
            return this.c.indexOf(watchApp);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        kd4.b(cVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchApp watchApp = this.c.get(i);
            kd4.a((Object) watchApp, "mData[position]");
            cVar.a(watchApp);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.d = bVar;
    }
}
