package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xs2 extends rd<GoalTrackingData, a> {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public b d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ /* synthetic */ xs2 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xs2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.xs2$a$a  reason: collision with other inner class name */
        public static final class C0109a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0109a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1) {
                    a aVar = this.e;
                    GoalTrackingData a = xs2.a(aVar.d, aVar.getAdapterPosition());
                    if (a != null) {
                        b a2 = this.e.d.d;
                        if (a2 != null) {
                            kd4.a((Object) a, "it1");
                            a2.a(a);
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xs2 xs2, View view) {
            super(view);
            kd4.b(view, "view");
            this.d = xs2;
            this.a = (FlexibleTextView) view.findViewById(R.id.ftv_time);
            this.b = (FlexibleTextView) view.findViewById(R.id.ftv_no_time);
            this.c = (FlexibleTextView) view.findViewById(R.id.ftv_delete);
            this.c.setOnClickListener(new C0109a(this));
        }

        @DexIgnore
        public final void a(GoalTrackingData goalTrackingData) {
            String str;
            kd4.b(goalTrackingData, "item");
            if (this.d.c == goalTrackingData.getTimezoneOffsetInSecond()) {
                str = "";
            } else if (goalTrackingData.getTimezoneOffsetInSecond() >= 0) {
                str = '+' + il2.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
            } else {
                str = il2.a(((float) goalTrackingData.getTimezoneOffsetInSecond()) / 3600.0f, 1);
                kd4.a((Object) str, "NumberHelper.decimalForm\u2026ffsetInSecond / 3600F, 1)");
            }
            FlexibleTextView flexibleTextView = this.a;
            kd4.a((Object) flexibleTextView, "mTvTime");
            pd4 pd4 = pd4.a;
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.s_time_zone);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            Object[] objArr = {rk2.a(goalTrackingData.getTrackedAt().getMillis(), goalTrackingData.getTimezoneOffsetInSecond()), str};
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(format, *args)");
            flexibleTextView.setText(format);
            FlexibleTextView flexibleTextView2 = this.b;
            kd4.a((Object) flexibleTextView2, "mTvNoTime");
            flexibleTextView2.setVisibility(8);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(GoalTrackingData goalTrackingData);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xs2(b bVar, a62 a62) {
        super(a62);
        kd4.b(a62, "goalTrackingDataDiff");
        this.d = bVar;
        TimeZone timeZone = TimeZone.getDefault();
        kd4.a((Object) timeZone, "TimeZone.getDefault()");
        this.c = rk2.a(timeZone.getID(), true);
    }

    @DexIgnore
    public static final /* synthetic */ GoalTrackingData a(xs2 xs2, int i) {
        return (GoalTrackingData) xs2.a(i);
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recorded_goal_tracking, viewGroup, false);
        kd4.a((Object) inflate, "LayoutInflater.from(pare\u2026           parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        GoalTrackingData goalTrackingData = (GoalTrackingData) a(i);
        if (goalTrackingData != null) {
            aVar.a(goalTrackingData);
        }
    }
}
