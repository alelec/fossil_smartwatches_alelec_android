package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pp */
public abstract class C2663pp {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2663pp f8415a; // = new com.fossil.blesdk.obfuscated.C2663pp.C2665b();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2663pp f8416b; // = new com.fossil.blesdk.obfuscated.C2663pp.C2666c();

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C2663pp f8417c; // = new com.fossil.blesdk.obfuscated.C2663pp.C2667d();

    @DexIgnore
    /* renamed from: d */
    public static /* final */ com.fossil.blesdk.obfuscated.C2663pp f8418d; // = new com.fossil.blesdk.obfuscated.C2663pp.C2668e();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pp$a")
    /* renamed from: com.fossil.blesdk.obfuscated.pp$a */
    public class C2664a extends com.fossil.blesdk.obfuscated.C2663pp {
        @DexIgnore
        /* renamed from: a */
        public boolean mo14812a() {
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14813a(com.bumptech.glide.load.DataSource dataSource) {
            return dataSource == com.bumptech.glide.load.DataSource.REMOTE;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo14815b() {
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14814a(boolean z, com.bumptech.glide.load.DataSource dataSource, com.bumptech.glide.load.EncodeStrategy encodeStrategy) {
            return (dataSource == com.bumptech.glide.load.DataSource.RESOURCE_DISK_CACHE || dataSource == com.bumptech.glide.load.DataSource.MEMORY_CACHE) ? false : true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pp$b")
    /* renamed from: com.fossil.blesdk.obfuscated.pp$b */
    public class C2665b extends com.fossil.blesdk.obfuscated.C2663pp {
        @DexIgnore
        /* renamed from: a */
        public boolean mo14812a() {
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14813a(com.bumptech.glide.load.DataSource dataSource) {
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14814a(boolean z, com.bumptech.glide.load.DataSource dataSource, com.bumptech.glide.load.EncodeStrategy encodeStrategy) {
            return false;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo14815b() {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pp$c")
    /* renamed from: com.fossil.blesdk.obfuscated.pp$c */
    public class C2666c extends com.fossil.blesdk.obfuscated.C2663pp {
        @DexIgnore
        /* renamed from: a */
        public boolean mo14812a() {
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14813a(com.bumptech.glide.load.DataSource dataSource) {
            return (dataSource == com.bumptech.glide.load.DataSource.DATA_DISK_CACHE || dataSource == com.bumptech.glide.load.DataSource.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14814a(boolean z, com.bumptech.glide.load.DataSource dataSource, com.bumptech.glide.load.EncodeStrategy encodeStrategy) {
            return false;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo14815b() {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pp$d")
    /* renamed from: com.fossil.blesdk.obfuscated.pp$d */
    public class C2667d extends com.fossil.blesdk.obfuscated.C2663pp {
        @DexIgnore
        /* renamed from: a */
        public boolean mo14812a() {
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14813a(com.bumptech.glide.load.DataSource dataSource) {
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14814a(boolean z, com.bumptech.glide.load.DataSource dataSource, com.bumptech.glide.load.EncodeStrategy encodeStrategy) {
            return (dataSource == com.bumptech.glide.load.DataSource.RESOURCE_DISK_CACHE || dataSource == com.bumptech.glide.load.DataSource.MEMORY_CACHE) ? false : true;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo14815b() {
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pp$e")
    /* renamed from: com.fossil.blesdk.obfuscated.pp$e */
    public class C2668e extends com.fossil.blesdk.obfuscated.C2663pp {
        @DexIgnore
        /* renamed from: a */
        public boolean mo14812a() {
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14813a(com.bumptech.glide.load.DataSource dataSource) {
            return dataSource == com.bumptech.glide.load.DataSource.REMOTE;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo14815b() {
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo14814a(boolean z, com.bumptech.glide.load.DataSource dataSource, com.bumptech.glide.load.EncodeStrategy encodeStrategy) {
            return ((z && dataSource == com.bumptech.glide.load.DataSource.DATA_DISK_CACHE) || dataSource == com.bumptech.glide.load.DataSource.LOCAL) && encodeStrategy == com.bumptech.glide.load.EncodeStrategy.TRANSFORMED;
        }
    }

    /*
    static {
        new com.fossil.blesdk.obfuscated.C2663pp.C2664a();
    }
    */

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14812a();

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14813a(com.bumptech.glide.load.DataSource dataSource);

    @DexIgnore
    /* renamed from: a */
    public abstract boolean mo14814a(boolean z, com.bumptech.glide.load.DataSource dataSource, com.bumptech.glide.load.EncodeStrategy encodeStrategy);

    @DexIgnore
    /* renamed from: b */
    public abstract boolean mo14815b();
}
