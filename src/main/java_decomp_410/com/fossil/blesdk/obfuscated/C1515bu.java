package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bu */
public class C1515bu implements com.fossil.blesdk.obfuscated.C2427mo<java.io.InputStream, com.fossil.blesdk.obfuscated.C3060ut> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.List<com.bumptech.glide.load.ImageHeaderParser> f3919a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2427mo<java.nio.ByteBuffer, com.fossil.blesdk.obfuscated.C3060ut> f3920b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C1885gq f3921c;

    @DexIgnore
    public C1515bu(java.util.List<com.bumptech.glide.load.ImageHeaderParser> list, com.fossil.blesdk.obfuscated.C2427mo<java.nio.ByteBuffer, com.fossil.blesdk.obfuscated.C3060ut> moVar, com.fossil.blesdk.obfuscated.C1885gq gqVar) {
        this.f3919a = list;
        this.f3920b = moVar;
        this.f3921c = gqVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9301a(java.io.InputStream inputStream, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        return !((java.lang.Boolean) loVar.mo13319a(com.fossil.blesdk.obfuscated.C1447au.f3606b)).booleanValue() && com.fossil.blesdk.obfuscated.C2049io.m8573b(this.f3919a, inputStream, this.f3921c) == com.bumptech.glide.load.ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<com.fossil.blesdk.obfuscated.C3060ut> mo9299a(java.io.InputStream inputStream, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) throws java.io.IOException {
        byte[] a = m5120a(inputStream);
        if (a == null) {
            return null;
        }
        return this.f3920b.mo9299a(java.nio.ByteBuffer.wrap(a), i, i2, loVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static byte[] m5120a(java.io.InputStream inputStream) {
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream(androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE);
        try {
            byte[] bArr = new byte[androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_SET_A11Y_ITEM_DELEGATE];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (java.io.IOException e) {
            if (!android.util.Log.isLoggable("StreamGifDecoder", 5)) {
                return null;
            }
            android.util.Log.w("StreamGifDecoder", "Error reading data from stream", e);
            return null;
        }
    }
}
