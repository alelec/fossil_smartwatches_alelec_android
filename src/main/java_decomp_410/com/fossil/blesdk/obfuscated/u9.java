package com.fossil.blesdk.obfuscated;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface u9 {
    @DexIgnore
    public static final boolean a = (Build.VERSION.SDK_INT >= 27);
}
