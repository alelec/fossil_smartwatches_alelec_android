package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nl */
public class C2494nl {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.nl$a")
    /* renamed from: com.fossil.blesdk.obfuscated.nl$a */
    public static /* synthetic */ class C2495a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f7819a; // = new int[androidx.work.WorkInfo.State.values().length];

        @DexIgnore
        /* renamed from: b */
        public static /* final */ /* synthetic */ int[] f7820b; // = new int[androidx.work.BackoffPolicy.values().length];

        @DexIgnore
        /* renamed from: c */
        public static /* final */ /* synthetic */ int[] f7821c; // = new int[androidx.work.NetworkType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|(2:17|18)|19|21|22|(2:23|24)|25|27|28|29|30|31|32|33|34|35|36|(3:37|38|40)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(27:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|(2:17|18)|19|21|22|(2:23|24)|25|27|28|29|30|31|32|33|34|35|36|(3:37|38|40)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|21|22|(2:23|24)|25|27|28|29|30|31|32|33|34|35|36|37|38|40) */
        /* JADX WARNING: Can't wrap try/catch for region: R(34:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|21|22|23|24|25|27|28|29|30|31|32|33|34|35|36|37|38|40) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0053 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0070 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0084 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x008e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0098 */
        /*
        static {
            try {
                f7821c[androidx.work.NetworkType.NOT_REQUIRED.ordinal()] = 1;
            } catch (java.lang.NoSuchFieldError unused) {
            }
            try {
                f7821c[androidx.work.NetworkType.CONNECTED.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError unused2) {
            }
            try {
                f7821c[androidx.work.NetworkType.UNMETERED.ordinal()] = 3;
            } catch (java.lang.NoSuchFieldError unused3) {
            }
            try {
                f7821c[androidx.work.NetworkType.NOT_ROAMING.ordinal()] = 4;
            } catch (java.lang.NoSuchFieldError unused4) {
            }
            try {
                f7821c[androidx.work.NetworkType.METERED.ordinal()] = 5;
            } catch (java.lang.NoSuchFieldError unused5) {
            }
            f7820b[androidx.work.BackoffPolicy.EXPONENTIAL.ordinal()] = 1;
            try {
                f7820b[androidx.work.BackoffPolicy.LINEAR.ordinal()] = 2;
            } catch (java.lang.NoSuchFieldError unused6) {
            }
            f7819a[androidx.work.WorkInfo.State.ENQUEUED.ordinal()] = 1;
            f7819a[androidx.work.WorkInfo.State.RUNNING.ordinal()] = 2;
            f7819a[androidx.work.WorkInfo.State.SUCCEEDED.ordinal()] = 3;
            f7819a[androidx.work.WorkInfo.State.FAILED.ordinal()] = 4;
            f7819a[androidx.work.WorkInfo.State.BLOCKED.ordinal()] = 5;
            try {
                f7819a[androidx.work.WorkInfo.State.CANCELLED.ordinal()] = 6;
            } catch (java.lang.NoSuchFieldError unused7) {
            }
        }
        */
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11326a(androidx.work.WorkInfo.State state) {
        switch (com.fossil.blesdk.obfuscated.C2494nl.C2495a.f7819a[state.ordinal()]) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 4;
            case 6:
                return 5;
            default:
                throw new java.lang.IllegalArgumentException("Could not convert " + state + " to int");
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static androidx.work.NetworkType m11330b(int i) {
        if (i == 0) {
            return androidx.work.NetworkType.NOT_REQUIRED;
        }
        if (i == 1) {
            return androidx.work.NetworkType.CONNECTED;
        }
        if (i == 2) {
            return androidx.work.NetworkType.UNMETERED;
        }
        if (i == 3) {
            return androidx.work.NetworkType.NOT_ROAMING;
        }
        if (i == 4) {
            return androidx.work.NetworkType.METERED;
        }
        throw new java.lang.IllegalArgumentException("Could not convert " + i + " to NetworkType");
    }

    @DexIgnore
    /* renamed from: c */
    public static androidx.work.WorkInfo.State m11331c(int i) {
        if (i == 0) {
            return androidx.work.WorkInfo.State.ENQUEUED;
        }
        if (i == 1) {
            return androidx.work.WorkInfo.State.RUNNING;
        }
        if (i == 2) {
            return androidx.work.WorkInfo.State.SUCCEEDED;
        }
        if (i == 3) {
            return androidx.work.WorkInfo.State.FAILED;
        }
        if (i == 4) {
            return androidx.work.WorkInfo.State.BLOCKED;
        }
        if (i == 5) {
            return androidx.work.WorkInfo.State.CANCELLED;
        }
        throw new java.lang.IllegalArgumentException("Could not convert " + i + " to State");
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11324a(androidx.work.BackoffPolicy backoffPolicy) {
        int i = com.fossil.blesdk.obfuscated.C2494nl.C2495a.f7820b[backoffPolicy.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        throw new java.lang.IllegalArgumentException("Could not convert " + backoffPolicy + " to int");
    }

    @DexIgnore
    /* renamed from: a */
    public static androidx.work.BackoffPolicy m11327a(int i) {
        if (i == 0) {
            return androidx.work.BackoffPolicy.EXPONENTIAL;
        }
        if (i == 1) {
            return androidx.work.BackoffPolicy.LINEAR;
        }
        throw new java.lang.IllegalArgumentException("Could not convert " + i + " to BackoffPolicy");
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11325a(androidx.work.NetworkType networkType) {
        int i = com.fossil.blesdk.obfuscated.C2494nl.C2495a.f7821c[networkType.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i == 4) {
            return 3;
        }
        if (i == 5) {
            return 4;
        }
        throw new java.lang.IllegalArgumentException("Could not convert " + networkType + " to int");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a A[SYNTHETIC, Splitter:B:26:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0071 A[SYNTHETIC, Splitter:B:37:0x0071] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:30:0x0062=Splitter:B:30:0x0062, B:15:0x0048=Splitter:B:15:0x0048} */
    /* renamed from: a */
    public static byte[] m11329a(com.fossil.blesdk.obfuscated.C3439zi ziVar) {
        java.io.ObjectOutputStream objectOutputStream = null;
        if (ziVar.mo18496b() == 0) {
            return null;
        }
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        try {
            java.io.ObjectOutputStream objectOutputStream2 = new java.io.ObjectOutputStream(byteArrayOutputStream);
            try {
                objectOutputStream2.writeInt(ziVar.mo18496b());
                for (com.fossil.blesdk.obfuscated.C3439zi.C3440a next : ziVar.mo18494a()) {
                    objectOutputStream2.writeUTF(next.mo18499a().toString());
                    objectOutputStream2.writeBoolean(next.mo18500b());
                }
                try {
                    objectOutputStream2.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
                try {
                    byteArrayOutputStream.close();
                } catch (java.io.IOException e2) {
                    e2.printStackTrace();
                }
            } catch (java.io.IOException e3) {
                e = e3;
                objectOutputStream = objectOutputStream2;
                try {
                    e.printStackTrace();
                    if (objectOutputStream != null) {
                        try {
                            objectOutputStream.close();
                        } catch (java.io.IOException e4) {
                            e4.printStackTrace();
                        }
                    }
                    byteArrayOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                } catch (Throwable th) {
                    th = th;
                    objectOutputStream2 = objectOutputStream;
                    if (objectOutputStream2 != null) {
                        try {
                            objectOutputStream2.close();
                        } catch (java.io.IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (java.io.IOException e6) {
                        e6.printStackTrace();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (objectOutputStream2 != null) {
                }
                byteArrayOutputStream.close();
                throw th;
            }
        } catch (java.io.IOException e7) {
            e = e7;
            e.printStackTrace();
            if (objectOutputStream != null) {
            }
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        }
        return byteArrayOutputStream.toByteArray();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0046 A[SYNTHETIC, Splitter:B:25:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x005a A[SYNTHETIC, Splitter:B:36:0x005a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0033=Splitter:B:15:0x0033, B:29:0x004e=Splitter:B:29:0x004e} */
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3439zi m11328a(byte[] bArr) {
        java.io.ObjectInputStream objectInputStream;
        java.io.IOException e;
        com.fossil.blesdk.obfuscated.C3439zi ziVar = new com.fossil.blesdk.obfuscated.C3439zi();
        if (bArr == null) {
            return ziVar;
        }
        java.io.ByteArrayInputStream byteArrayInputStream = new java.io.ByteArrayInputStream(bArr);
        try {
            objectInputStream = new java.io.ObjectInputStream(byteArrayInputStream);
            try {
                for (int readInt = objectInputStream.readInt(); readInt > 0; readInt--) {
                    ziVar.mo18495a(android.net.Uri.parse(objectInputStream.readUTF()), objectInputStream.readBoolean());
                }
                try {
                    objectInputStream.close();
                } catch (java.io.IOException e2) {
                    e2.printStackTrace();
                }
                try {
                    byteArrayInputStream.close();
                } catch (java.io.IOException e3) {
                    e3.printStackTrace();
                }
            } catch (java.io.IOException e4) {
                e = e4;
                try {
                    e.printStackTrace();
                    if (objectInputStream != null) {
                        try {
                            objectInputStream.close();
                        } catch (java.io.IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                    byteArrayInputStream.close();
                    return ziVar;
                } catch (Throwable th) {
                    th = th;
                    if (objectInputStream != null) {
                        try {
                            objectInputStream.close();
                        } catch (java.io.IOException e6) {
                            e6.printStackTrace();
                        }
                    }
                    try {
                        byteArrayInputStream.close();
                    } catch (java.io.IOException e7) {
                        e7.printStackTrace();
                    }
                    throw th;
                }
            }
        } catch (java.io.IOException e8) {
            java.io.IOException iOException = e8;
            objectInputStream = null;
            e = iOException;
            e.printStackTrace();
            if (objectInputStream != null) {
            }
            byteArrayInputStream.close();
            return ziVar;
        } catch (Throwable th2) {
            objectInputStream = null;
            th = th2;
            if (objectInputStream != null) {
            }
            byteArrayInputStream.close();
            throw th;
        }
        return ziVar;
    }
}
