package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.setting.JSONKey;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z60 extends g70 {
    @DexIgnore
    public long A; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public Peripheral.BondState B; // = Peripheral.BondState.BOND_NONE;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z60(Peripheral peripheral) {
        super(RequestId.REMOVE_BOND, peripheral);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new k10(i().h());
    }

    @DexIgnore
    public void a(long j) {
        this.A = j;
    }

    @DexIgnore
    public long m() {
        return this.A;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.CURRENT_BOND_STATE, i().getBondState().getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.NEW_BOND_STATE, this.B.getLogName$blesdk_productionRelease());
    }

    @DexIgnore
    public void a(BluetoothCommand bluetoothCommand) {
        kd4.b(bluetoothCommand, Constants.COMMAND);
        this.B = ((k10) bluetoothCommand).i();
        a(new Request.ResponseInfo(0, (GattCharacteristic.CharacteristicId) null, (byte[]) null, wa0.a(new JSONObject(), JSONKey.NEW_BOND_STATE, this.B.getLogName$blesdk_productionRelease()), 7, (fd4) null));
    }
}
