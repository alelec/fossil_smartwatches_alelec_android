package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bt2 extends RecyclerView.g<a> {
    @DexIgnore
    public List<WeatherLocationWrapper> a; // = new ArrayList();
    @DexIgnore
    public b b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public CheckBox b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ bt2 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.bt2$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.bt2$a$a  reason: collision with other inner class name */
        public static final class C0082a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a e;

            @DexIgnore
            public C0082a(a aVar) {
                this.e = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d.getItemCount() > this.e.getAdapterPosition() && this.e.getAdapterPosition() != -1 && !((WeatherLocationWrapper) this.e.d.a.get(this.e.getAdapterPosition())).isUseCurrentLocation()) {
                    b a = this.e.d.b;
                    if (a != null) {
                        a.a(this.e.getAdapterPosition() - 1, !((WeatherLocationWrapper) this.e.d.a.get(this.e.getAdapterPosition())).isEnableLocation());
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bt2 bt2, View view) {
            super(view);
            kd4.b(view, "view");
            this.d = bt2;
            View findViewById = view.findViewById(R.id.cl_container);
            if (findViewById != null) {
                this.c = findViewById;
                View findViewById2 = view.findViewById(R.id.cb_location);
                if (findViewById2 != null) {
                    this.b = (CheckBox) findViewById2;
                    View findViewById3 = view.findViewById(R.id.tv_text);
                    if (findViewById3 != null) {
                        this.a = (TextView) findViewById3;
                        View view2 = this.c;
                        if (view2 != null) {
                            view2.setOnClickListener(new C0082a(this));
                            return;
                        }
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public final void a(WeatherLocationWrapper weatherLocationWrapper) {
            kd4.b(weatherLocationWrapper, "locationWrapper");
            TextView textView = this.a;
            if (textView != null) {
                textView.setText(weatherLocationWrapper.getFullName());
            }
            CheckBox checkBox = this.b;
            if (checkBox != null) {
                checkBox.setChecked(weatherLocationWrapper.isEnableLocation());
            }
            CheckBox checkBox2 = this.b;
            if (checkBox2 != null) {
                checkBox2.setEnabled(!weatherLocationWrapper.isUseCurrentLocation());
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i, boolean z);
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        kd4.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            aVar.a(this.a.get(i));
        }
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_weather_location_search, viewGroup, false);
        kd4.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<WeatherLocationWrapper> list) {
        kd4.b(list, "locationSearchList");
        this.a.clear();
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DianaWeatherAddLocation_Text__CurrentLocation);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DianaWeatherAddLocation_Text__CurrentLocation);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        this.a.add(0, new WeatherLocationWrapper("", 0.0d, 0.0d, a2, a3, true, true, 6, (fd4) null));
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        kd4.b(bVar, "listener");
        this.b = bVar;
    }
}
