package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vv extends yv<Bitmap> {
    @DexIgnore
    public vv(ImageView imageView) {
        super(imageView);
    }

    @DexIgnore
    /* renamed from: a */
    public void c(Bitmap bitmap) {
        ((ImageView) this.e).setImageBitmap(bitmap);
    }
}
