package com.fossil.blesdk.obfuscated;

import android.content.Context;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class r42 implements Factory<Context> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public r42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static r42 a(n42 n42) {
        return new r42(n42);
    }

    @DexIgnore
    public static Context b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static Context c(n42 n42) {
        Context c = n42.c();
        n44.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    public Context get() {
        return b(this.a);
    }
}
