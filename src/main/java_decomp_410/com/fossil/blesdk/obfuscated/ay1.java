package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.zzaa;
import java.io.File;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ay1 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ cz1 c;
    @DexIgnore
    public /* final */ Map<String, dz1> d;

    @DexIgnore
    public ay1(Context context) {
        this(context, new cz1());
    }

    @DexIgnore
    public final synchronized void a(String str) {
        this.a.edit().putString("topic_operaion_queue", str).apply();
    }

    @DexIgnore
    public final synchronized String b() {
        return this.a.getString("topic_operaion_queue", "");
    }

    @DexIgnore
    public final synchronized void c() {
        this.d.clear();
        cz1.a(this.b);
        this.a.edit().clear().commit();
    }

    @DexIgnore
    public ay1(Context context, cz1 cz1) {
        this.d = new g4();
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        this.c = cz1;
        File file = new File(k6.b(this.b), "com.google.android.gms.appid-no-backup");
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !a()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    c();
                    FirebaseInstanceId.m().i();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public static String b(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    @DexIgnore
    public final synchronized boolean a() {
        return this.a.getAll().isEmpty();
    }

    @DexIgnore
    public final synchronized dz1 b(String str) {
        dz1 dz1;
        dz1 dz12 = this.d.get(str);
        if (dz12 != null) {
            return dz12;
        }
        try {
            dz1 = this.c.a(this.b, str);
        } catch (zzaa unused) {
            Log.w("FirebaseInstanceId", "Stored data is corrupt, generating new identity");
            FirebaseInstanceId.m().i();
            dz1 = this.c.b(this.b, str);
        }
        this.d.put(str, dz1);
        return dz1;
    }

    @DexIgnore
    public static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public final synchronized by1 a(String str, String str2, String str3) {
        return by1.b(this.a.getString(b(str, str2, str3), (String) null));
    }

    @DexIgnore
    public final synchronized void c(String str) {
        String concat = String.valueOf(str).concat("|T|");
        SharedPreferences.Editor edit = this.a.edit();
        for (String next : this.a.getAll().keySet()) {
            if (next.startsWith(concat)) {
                edit.remove(next);
            }
        }
        edit.commit();
    }

    @DexIgnore
    public final synchronized void a(String str, String str2, String str3, String str4, String str5) {
        String a2 = by1.a(str4, str5, System.currentTimeMillis());
        if (a2 != null) {
            SharedPreferences.Editor edit = this.a.edit();
            edit.putString(b(str, str2, str3), a2);
            edit.commit();
        }
    }
}
