package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bj3 implements Factory<yi3> {
    @DexIgnore
    public static yi3 a(aj3 aj3) {
        yi3 a = aj3.a();
        n44.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
