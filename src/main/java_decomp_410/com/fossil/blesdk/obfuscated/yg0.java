package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yg0<O extends de0.d> extends of0 {
    @DexIgnore
    public /* final */ fe0<O> c;

    @DexIgnore
    public yg0(fe0<O> fe0) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.c = fe0;
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T a(T t) {
        this.c.b(t);
        return t;
    }

    @DexIgnore
    public final void a(mh0 mh0) {
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T b(T t) {
        this.c.c(t);
        return t;
    }

    @DexIgnore
    public final Context e() {
        return this.c.e();
    }

    @DexIgnore
    public final Looper f() {
        return this.c.g();
    }
}
