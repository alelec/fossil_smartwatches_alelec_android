package com.fossil.blesdk.obfuscated;

import com.google.android.gms.measurement.AppMeasurement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ij1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurement.ConditionalUserProperty e;
    @DexIgnore
    public /* final */ /* synthetic */ dj1 f;

    @DexIgnore
    public ij1(dj1 dj1, AppMeasurement.ConditionalUserProperty conditionalUserProperty) {
        this.f = dj1;
        this.e = conditionalUserProperty;
    }

    @DexIgnore
    public final void run() {
        this.f.e(this.e);
    }
}
