package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xw1 extends nw1 {
    @DexIgnore
    public /* final */ Set<Class<?>> a;
    @DexIgnore
    public /* final */ Set<Class<?>> b;
    @DexIgnore
    public /* final */ Set<Class<?>> c;
    @DexIgnore
    public /* final */ jw1 d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ax1 {
        @DexIgnore
        public a(Set<Class<?>> set, ax1 ax1) {
        }
    }

    @DexIgnore
    public xw1(iw1<?> iw1, jw1 jw1) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        for (mw1 next : iw1.b()) {
            if (next.c()) {
                hashSet.add(next.a());
            } else {
                hashSet2.add(next.a());
            }
        }
        if (!iw1.d().isEmpty()) {
            hashSet.add(ax1.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = iw1.d();
        this.d = jw1;
    }

    @DexIgnore
    public final <T> T a(Class<T> cls) {
        if (this.a.contains(cls)) {
            T a2 = this.d.a(cls);
            if (!cls.equals(ax1.class)) {
                return a2;
            }
            return new a(this.c, (ax1) a2);
        }
        throw new IllegalArgumentException(String.format("Requesting %s is not allowed.", new Object[]{cls}));
    }

    @DexIgnore
    public final <T> ez1<T> b(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.d.b(cls);
        }
        throw new IllegalArgumentException(String.format("Requesting Provider<%s> is not allowed.", new Object[]{cls}));
    }
}
