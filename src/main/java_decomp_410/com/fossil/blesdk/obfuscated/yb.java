package com.fossil.blesdk.obfuscated;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yb extends Service implements LifecycleOwner {
    @DexIgnore
    public /* final */ gc e; // = new gc(this);

    @DexIgnore
    public Lifecycle getLifecycle() {
        return this.e.a();
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        this.e.b();
        return null;
    }

    @DexIgnore
    public void onCreate() {
        this.e.c();
        super.onCreate();
    }

    @DexIgnore
    public void onDestroy() {
        this.e.d();
        super.onDestroy();
    }

    @DexIgnore
    public void onStart(Intent intent, int i) {
        this.e.e();
        super.onStart(intent, i);
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        return super.onStartCommand(intent, i, i2);
    }
}
