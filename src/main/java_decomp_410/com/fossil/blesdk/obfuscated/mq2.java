package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mq2 extends lq2 {
    @DexIgnore
    public /* final */ List<String> d;
    @DexIgnore
    public String e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mq2(String str, String str2, List<String> list, String str3) {
        super(str, str2);
        kd4.b(str, "tagName");
        kd4.b(str2, "title");
        kd4.b(list, "values");
        kd4.b(str3, "btnText");
        this.d = list;
        this.e = str3;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final List<String> e() {
        return this.d;
    }
}
