package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ve0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vh0 extends th0<Void> {
    @DexIgnore
    public /* final */ bf0<de0.b, ?> b;
    @DexIgnore
    public /* final */ hf0<de0.b, ?> c;

    @DexIgnore
    public vh0(eh0 eh0, xn1<Void> xn1) {
        super(3, xn1);
        this.b = eh0.a;
        this.c = eh0.b;
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ void a(jf0 jf0, boolean z) {
    }

    @DexIgnore
    public final wd0[] b(ve0.a<?> aVar) {
        return this.b.c();
    }

    @DexIgnore
    public final boolean c(ve0.a<?> aVar) {
        return this.b.d();
    }

    @DexIgnore
    public final void d(ve0.a<?> aVar) throws RemoteException {
        this.b.a(aVar.f(), this.a);
        if (this.b.b() != null) {
            aVar.l().put(this.b.b(), new eh0(this.b, this.c));
        }
    }
}
