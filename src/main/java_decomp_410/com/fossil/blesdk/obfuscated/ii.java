package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.WindowId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ii implements ji {
    @DexIgnore
    public /* final */ WindowId a;

    @DexIgnore
    public ii(View view) {
        this.a = view.getWindowId();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof ii) && ((ii) obj).a.equals(this.a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
