package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.enums.Gender;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ul3 extends u52 {
    @DexIgnore
    public abstract void a(Gender gender);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(Date date, Calendar calendar);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract void b(String str);

    @DexIgnore
    public abstract void b(boolean z);

    @DexIgnore
    public abstract void c(String str);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract Calendar i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
