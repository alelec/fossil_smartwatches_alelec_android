package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface aj4<S> extends CoroutineContext.a {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <S, R> R a(aj4<S> aj4, R r, yc4<? super R, ? super CoroutineContext.a, ? extends R> yc4) {
            kd4.b(yc4, "operation");
            return CoroutineContext.a.C0169a.a(aj4, r, yc4);
        }

        @DexIgnore
        public static <S, E extends CoroutineContext.a> E a(aj4<S> aj4, CoroutineContext.b<E> bVar) {
            kd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) aj4, bVar);
        }

        @DexIgnore
        public static <S> CoroutineContext a(aj4<S> aj4, CoroutineContext coroutineContext) {
            kd4.b(coroutineContext, "context");
            return CoroutineContext.a.C0169a.a((CoroutineContext.a) aj4, coroutineContext);
        }

        @DexIgnore
        public static <S> CoroutineContext b(aj4<S> aj4, CoroutineContext.b<?> bVar) {
            kd4.b(bVar, "key");
            return CoroutineContext.a.C0169a.b(aj4, bVar);
        }
    }

    @DexIgnore
    S a(CoroutineContext coroutineContext);

    @DexIgnore
    void a(CoroutineContext coroutineContext, S s);
}
