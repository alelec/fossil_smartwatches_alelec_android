package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class h82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon21 e;
    @DexIgnore
    private /* final */ /* synthetic */ SavedPreset f;

    @DexIgnore
    public /* synthetic */ h82(PresetRepository.Anon21 anon21, SavedPreset savedPreset) {
        this.e = anon21;
        this.f = savedPreset;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f);
    }
}
