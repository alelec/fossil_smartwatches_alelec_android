package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ek2<TranscodeType> extends wn<TranscodeType> implements Cloneable {
    @DexIgnore
    public ek2(rn rnVar, xn xnVar, Class<TranscodeType> cls, Context context) {
        super(rnVar, xnVar, cls, context);
    }

    @DexIgnore
    public ek2<TranscodeType> c() {
        return (ek2) super.c();
    }

    @DexIgnore
    public ek2<TranscodeType> P() {
        return (ek2) super.P();
    }

    @DexIgnore
    public ek2<TranscodeType> Q() {
        return (ek2) super.Q();
    }

    @DexIgnore
    public ek2<TranscodeType> R() {
        return (ek2) super.R();
    }

    @DexIgnore
    public ek2<TranscodeType> b(boolean z) {
        return (ek2) super.b(z);
    }

    @DexIgnore
    public ek2<TranscodeType> b(int i) {
        return (ek2) super.b(i);
    }

    @DexIgnore
    public ek2<TranscodeType> clone() {
        return (ek2) super.clone();
    }

    @DexIgnore
    public ek2<TranscodeType> b() {
        return (ek2) super.b();
    }

    @DexIgnore
    public ek2<TranscodeType> b(qv<TranscodeType> qvVar) {
        return (ek2) super.b(qvVar);
    }

    @DexIgnore
    public ek2<TranscodeType> a(float f) {
        return (ek2) super.a(f);
    }

    @DexIgnore
    public ek2<TranscodeType> a(pp ppVar) {
        return (ek2) super.a(ppVar);
    }

    @DexIgnore
    public ek2<TranscodeType> a(Priority priority) {
        return (ek2) super.a(priority);
    }

    @DexIgnore
    public ek2<TranscodeType> a(boolean z) {
        return (ek2) super.a(z);
    }

    @DexIgnore
    public ek2<TranscodeType> a(int i, int i2) {
        return (ek2) super.a(i, i2);
    }

    @DexIgnore
    public ek2<TranscodeType> a(jo joVar) {
        return (ek2) super.a(joVar);
    }

    @DexIgnore
    public <Y> ek2<TranscodeType> a(ko<Y> koVar, Y y) {
        return (ek2) super.a(koVar, y);
    }

    @DexIgnore
    public ek2<TranscodeType> a(Class<?> cls) {
        return (ek2) super.a(cls);
    }

    @DexIgnore
    public ek2<TranscodeType> a(DownsampleStrategy downsampleStrategy) {
        return (ek2) super.a(downsampleStrategy);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.fossil.blesdk.obfuscated.oo<android.graphics.Bitmap>, com.fossil.blesdk.obfuscated.oo] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public ek2<TranscodeType> a(oo<Bitmap> r1) {
        return (ek2) super.a((oo<Bitmap>) r1);
    }

    @DexIgnore
    public ek2<TranscodeType> a(lv<?> lvVar) {
        return (ek2) super.a((lv) lvVar);
    }

    @DexIgnore
    public ek2<TranscodeType> a(qv<TranscodeType> qvVar) {
        super.a(qvVar);
        return this;
    }

    @DexIgnore
    public ek2<TranscodeType> a(wn<TranscodeType> wnVar) {
        super.a(wnVar);
        return this;
    }

    @DexIgnore
    public ek2<TranscodeType> a(Object obj) {
        super.a(obj);
        return this;
    }

    @DexIgnore
    public ek2<TranscodeType> a(String str) {
        super.a(str);
        return this;
    }

    @DexIgnore
    public ek2<TranscodeType> a(Uri uri) {
        super.a(uri);
        return this;
    }

    @DexIgnore
    public ek2<TranscodeType> a(Integer num) {
        return (ek2) super.a(num);
    }

    @DexIgnore
    public ek2<TranscodeType> a(byte[] bArr) {
        return (ek2) super.a(bArr);
    }
}
