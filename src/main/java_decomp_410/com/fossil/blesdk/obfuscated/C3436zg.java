package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zg */
public interface C3436zg {
    @DexIgnore
    /* renamed from: a */
    void mo17702a(android.view.ViewGroup viewGroup, android.view.View view);

    @DexIgnore
    void setVisibility(int i);
}
