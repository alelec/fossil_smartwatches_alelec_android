package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k93 {
    @DexIgnore
    public /* final */ f93 a;
    @DexIgnore
    public /* final */ u93 b;
    @DexIgnore
    public /* final */ p93 c;

    @DexIgnore
    public k93(f93 f93, u93 u93, p93 p93) {
        kd4.b(f93, "mActivityOverviewDayView");
        kd4.b(u93, "mActivityOverviewWeekView");
        kd4.b(p93, "mActivityOverviewMonthView");
        this.a = f93;
        this.b = u93;
        this.c = p93;
    }

    @DexIgnore
    public final f93 a() {
        return this.a;
    }

    @DexIgnore
    public final p93 b() {
        return this.c;
    }

    @DexIgnore
    public final u93 c() {
        return this.b;
    }
}
