package com.fossil.blesdk.obfuscated;

import android.database.sqlite.SQLiteProgram;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class og implements ig {
    @DexIgnore
    public /* final */ SQLiteProgram e;

    @DexIgnore
    public og(SQLiteProgram sQLiteProgram) {
        this.e = sQLiteProgram;
    }

    @DexIgnore
    public void a(int i) {
        this.e.bindNull(i);
    }

    @DexIgnore
    public void b(int i, long j) {
        this.e.bindLong(i, j);
    }

    @DexIgnore
    public void close() {
        this.e.close();
    }

    @DexIgnore
    public void a(int i, double d) {
        this.e.bindDouble(i, d);
    }

    @DexIgnore
    public void a(int i, String str) {
        this.e.bindString(i, str);
    }

    @DexIgnore
    public void a(int i, byte[] bArr) {
        this.e.bindBlob(i, bArr);
    }
}
