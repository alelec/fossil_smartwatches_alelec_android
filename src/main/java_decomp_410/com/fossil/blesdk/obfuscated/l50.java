package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.data.music.MusicEvent;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.device.logic.phase.SingleRequestPhase;
import com.fossil.blesdk.device.logic.resource.ResourceType;
import com.fossil.blesdk.setting.JSONKey;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l50 extends SingleRequestPhase {
    @DexIgnore
    public /* final */ ArrayList<ResourceType> B; // = j90.a(super.n(), cb4.a((T[]) new ResourceType[]{ResourceType.ASYNC}));
    @DexIgnore
    public /* final */ MusicEvent C;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l50(Peripheral peripheral, Phase.a aVar, MusicEvent musicEvent) {
        super(peripheral, aVar, PhaseId.NOTIFY_MUSIC_EVENT, new t60(musicEvent, peripheral));
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(musicEvent, "musicEvent");
        this.C = musicEvent;
    }

    @DexIgnore
    public ArrayList<ResourceType> n() {
        return this.B;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.MUSIC_EVENT, this.C.toJSONObject());
    }
}
