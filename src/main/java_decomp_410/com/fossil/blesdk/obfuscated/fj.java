package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface fj {
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final b.c a = new b.c();
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final b.C0016b b = new b.C0016b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends b {
            @DexIgnore
            public /* final */ Throwable a;

            @DexIgnore
            public a(Throwable th) {
                this.a = th;
            }

            @DexIgnore
            public Throwable a() {
                return this.a;
            }

            @DexIgnore
            public String toString() {
                return String.format("FAILURE (%s)", new Object[]{this.a.getMessage()});
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fj$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.fj$b$b  reason: collision with other inner class name */
        public static final class C0016b extends b {
            @DexIgnore
            public String toString() {
                return "IN_PROGRESS";
            }

            @DexIgnore
            public C0016b() {
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends b {
            @DexIgnore
            public String toString() {
                return "SUCCESS";
            }

            @DexIgnore
            public c() {
            }
        }
    }
}
