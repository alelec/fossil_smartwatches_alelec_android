package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k3 */
public class C2182k3<K, V> extends com.fossil.blesdk.obfuscated.C2282l3<K, V> {

    @DexIgnore
    /* renamed from: i */
    public java.util.HashMap<K, com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V>> f6694i; // = new java.util.HashMap<>();

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo12617a(K k) {
        return this.f6694i.get(k);
    }

    @DexIgnore
    /* renamed from: b */
    public V mo12618b(K k, V v) {
        com.fossil.blesdk.obfuscated.C2282l3.C2285c a = mo12617a(k);
        if (a != null) {
            return a.f7119f;
        }
        this.f6694i.put(k, mo13115a(k, v));
        return null;
    }

    @DexIgnore
    public boolean contains(K k) {
        return this.f6694i.containsKey(k);
    }

    @DexIgnore
    public V remove(K k) {
        V remove = super.remove(k);
        this.f6694i.remove(k);
        return remove;
    }

    @DexIgnore
    /* renamed from: b */
    public java.util.Map.Entry<K, V> mo12619b(K k) {
        if (contains(k)) {
            return this.f6694i.get(k).f7121h;
        }
        return null;
    }
}
