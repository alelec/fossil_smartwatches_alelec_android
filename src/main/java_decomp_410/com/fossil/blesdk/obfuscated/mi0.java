package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.d;
import com.fossil.blesdk.obfuscated.ve0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mi0<O extends de0.d> extends fe0<O> {
    @DexIgnore
    public /* final */ de0.f j;
    @DexIgnore
    public /* final */ gi0 k;
    @DexIgnore
    public /* final */ kj0 l;
    @DexIgnore
    public /* final */ de0.a<? extends ln1, vm1> m;

    @DexIgnore
    public mi0(Context context, de0<O> de0, Looper looper, de0.f fVar, gi0 gi0, kj0 kj0, de0.a<? extends ln1, vm1> aVar) {
        super(context, de0, looper);
        this.j = fVar;
        this.k = gi0;
        this.l = kj0;
        this.m = aVar;
        this.i.a((fe0<?>) this);
    }

    @DexIgnore
    public final de0.f a(Looper looper, ve0.a<O> aVar) {
        this.k.a((hi0) aVar);
        return this.j;
    }

    @DexIgnore
    public final de0.f i() {
        return this.j;
    }

    @DexIgnore
    public final hh0 a(Context context, Handler handler) {
        return new hh0(context, handler, this.l, this.m);
    }
}
