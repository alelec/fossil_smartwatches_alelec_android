package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.google.android.gms.common.api.AvailabilityException;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pi0 implements rn1<Map<yh0<?>, String>> {
    @DexIgnore
    public /* final */ /* synthetic */ ni0 a;

    @DexIgnore
    public pi0(ni0 ni0) {
        this.a = ni0;
    }

    @DexIgnore
    public final void onComplete(wn1<Map<yh0<?>, String>> wn1) {
        this.a.j.lock();
        try {
            if (this.a.r) {
                if (wn1.e()) {
                    Map unused = this.a.s = new g4(this.a.e.size());
                    for (mi0 h : this.a.e.values()) {
                        this.a.s.put(h.h(), ud0.i);
                    }
                } else if (wn1.a() instanceof AvailabilityException) {
                    AvailabilityException availabilityException = (AvailabilityException) wn1.a();
                    if (this.a.p) {
                        Map unused2 = this.a.s = new g4(this.a.e.size());
                        for (mi0 mi0 : this.a.e.values()) {
                            yh0 h2 = mi0.h();
                            ud0 connectionResult = availabilityException.getConnectionResult(mi0);
                            if (this.a.a((mi0<?>) mi0, connectionResult)) {
                                this.a.s.put(h2, new ud0(16));
                            } else {
                                this.a.s.put(h2, connectionResult);
                            }
                        }
                    } else {
                        Map unused3 = this.a.s = availabilityException.zaj();
                    }
                    ud0 unused4 = this.a.v = this.a.k();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", wn1.a());
                    Map unused5 = this.a.s = Collections.emptyMap();
                    ud0 unused6 = this.a.v = new ud0(8);
                }
                if (this.a.t != null) {
                    this.a.s.putAll(this.a.t);
                    ud0 unused7 = this.a.v = this.a.k();
                }
                if (this.a.v == null) {
                    this.a.i();
                    this.a.j();
                } else {
                    boolean unused8 = this.a.r = false;
                    this.a.i.a(this.a.v);
                }
                this.a.m.signalAll();
                this.a.j.unlock();
            }
        } finally {
            this.a.j.unlock();
        }
    }
}
