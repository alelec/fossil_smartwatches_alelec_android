package com.fossil.blesdk.obfuscated;

import android.view.View;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface d23 extends bs2<c23> {
    @DexIgnore
    void a(f13 f13, List<? extends f8<View, String>> list, List<? extends f8<CustomizeWidget, String>> list2);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void c(int i);

    @DexIgnore
    void c(List<f13> list);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void e(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void j();

    @DexIgnore
    void l();

    @DexIgnore
    void m();

    @DexIgnore
    void v();
}
