package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yz */
public class C3397yz implements com.fossil.blesdk.obfuscated.e00 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f11436a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.e00 f11437b;

    @DexIgnore
    /* renamed from: c */
    public boolean f11438c; // = false;

    @DexIgnore
    /* renamed from: d */
    public java.lang.String f11439d;

    @DexIgnore
    public C3397yz(android.content.Context context, com.fossil.blesdk.obfuscated.e00 e00) {
        this.f11436a = context;
        this.f11437b = e00;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo10253a() {
        if (!this.f11438c) {
            this.f11439d = p011io.fabric.sdk.android.services.common.CommonUtils.m36899o(this.f11436a);
            this.f11438c = true;
        }
        java.lang.String str = this.f11439d;
        if (str != null) {
            return str;
        }
        com.fossil.blesdk.obfuscated.e00 e00 = this.f11437b;
        if (e00 != null) {
            return e00.mo10253a();
        }
        return null;
    }
}
