package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pd<T> {
    @DexIgnore
    public static /* final */ pd e; // = new pd(Collections.emptyList(), 0);
    @DexIgnore
    public static /* final */ pd f; // = new pd(Collections.emptyList(), 0);
    @DexIgnore
    public /* final */ List<T> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<T> {
        @DexIgnore
        public abstract void a(int i, pd<T> pdVar);
    }

    @DexIgnore
    public pd(List<T> list, int i, int i2, int i3) {
        this.a = list;
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    @DexIgnore
    public static <T> pd<T> b() {
        return e;
    }

    @DexIgnore
    public static <T> pd<T> c() {
        return f;
    }

    @DexIgnore
    public boolean a() {
        return this == f;
    }

    @DexIgnore
    public String toString() {
        return "Result " + this.b + ", " + this.a + ", " + this.c + ", offset " + this.d;
    }

    @DexIgnore
    public pd(List<T> list, int i) {
        this.a = list;
        this.b = 0;
        this.c = 0;
        this.d = i;
    }
}
