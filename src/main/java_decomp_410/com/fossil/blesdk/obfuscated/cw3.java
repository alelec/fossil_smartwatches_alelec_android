package com.fossil.blesdk.obfuscated;

import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cw3 {
    @DexIgnore
    public static /* final */ ByteString d; // = ByteString.encodeUtf8(":status");
    @DexIgnore
    public static /* final */ ByteString e; // = ByteString.encodeUtf8(":method");
    @DexIgnore
    public static /* final */ ByteString f; // = ByteString.encodeUtf8(":path");
    @DexIgnore
    public static /* final */ ByteString g; // = ByteString.encodeUtf8(":scheme");
    @DexIgnore
    public static /* final */ ByteString h; // = ByteString.encodeUtf8(":authority");
    @DexIgnore
    public static /* final */ ByteString i; // = ByteString.encodeUtf8(":host");
    @DexIgnore
    public static /* final */ ByteString j; // = ByteString.encodeUtf8(":version");
    @DexIgnore
    public /* final */ ByteString a;
    @DexIgnore
    public /* final */ ByteString b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public cw3(String str, String str2) {
        this(ByteString.encodeUtf8(str), ByteString.encodeUtf8(str2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof cw3)) {
            return false;
        }
        cw3 cw3 = (cw3) obj;
        if (!this.a.equals(cw3.a) || !this.b.equals(cw3.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.a.hashCode()) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return String.format("%s: %s", new Object[]{this.a.utf8(), this.b.utf8()});
    }

    @DexIgnore
    public cw3(ByteString byteString, String str) {
        this(byteString, ByteString.encodeUtf8(str));
    }

    @DexIgnore
    public cw3(ByteString byteString, ByteString byteString2) {
        this.a = byteString;
        this.b = byteString2;
        this.c = byteString.size() + 32 + byteString2.size();
    }
}
