package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w41 implements Parcelable.Creator<v41> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        t41 t41 = null;
        IBinder iBinder = null;
        IBinder iBinder2 = null;
        int i = 1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 == 2) {
                t41 = (t41) SafeParcelReader.a(parcel, a, t41.CREATOR);
            } else if (a2 == 3) {
                iBinder = SafeParcelReader.p(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                iBinder2 = SafeParcelReader.p(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new v41(i, t41, iBinder, iBinder2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new v41[i];
    }
}
