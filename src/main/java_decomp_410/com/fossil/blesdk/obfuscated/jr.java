package com.fossil.blesdk.obfuscated;

import android.util.Base64;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.fossil.blesdk.obfuscated.so;
import com.fossil.blesdk.obfuscated.sr;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jr<Model, Data> implements sr<Model, Data> {
    @DexIgnore
    public /* final */ a<Data> a;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        Data a(String str) throws IllegalArgumentException;

        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Model> implements tr<Model, InputStream> {
        @DexIgnore
        public /* final */ a<InputStream> a; // = new a(this);

        @DexIgnore
        public sr<Model, InputStream> a(wr wrVar) {
            return new jr(this.a);
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements a<InputStream> {
            @DexIgnore
            public a(c cVar) {
            }

            @DexIgnore
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            public InputStream a(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            @DexIgnore
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }
        }
    }

    @DexIgnore
    public jr(a<Data> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public sr.a<Data> a(Model model, int i, int i2, lo loVar) {
        return new sr.a<>(new jw(model), new b(model.toString(), this.a));
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<Data> implements so<Data> {
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ a<Data> f;
        @DexIgnore
        public Data g;

        @DexIgnore
        public b(String str, a<Data> aVar) {
            this.e = str;
            this.f = aVar;
        }

        @DexIgnore
        public void a(Priority priority, so.a<? super Data> aVar) {
            try {
                this.g = this.f.a(this.e);
                aVar.a(this.g);
            } catch (IllegalArgumentException e2) {
                aVar.a((Exception) e2);
            }
        }

        @DexIgnore
        public DataSource b() {
            return DataSource.LOCAL;
        }

        @DexIgnore
        public void cancel() {
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.f.getDataClass();
        }

        @DexIgnore
        public void a() {
            try {
                this.f.a(this.g);
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }
}
