package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wk */
public class C3227wk extends com.fossil.blesdk.obfuscated.C2971tk<java.lang.Boolean> {

    @DexIgnore
    /* renamed from: i */
    public static /* final */ java.lang.String f10653i; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("StorageNotLowTracker");

    @DexIgnore
    public C3227wk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(context, zlVar);
    }

    @DexIgnore
    /* renamed from: d */
    public android.content.IntentFilter mo15616d() {
        android.content.IntentFilter intentFilter = new android.content.IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Boolean m15858a() {
        android.content.Intent registerReceiver = this.f10017b.registerReceiver((android.content.BroadcastReceiver) null, mo15616d());
        if (!(registerReceiver == null || registerReceiver.getAction() == null)) {
            java.lang.String action = registerReceiver.getAction();
            char c = 65535;
            int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    c = 0;
                }
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                c = 1;
            }
            if (c != 0) {
                if (c != 1) {
                    return null;
                }
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15614a(android.content.Context context, android.content.Intent intent) {
        if (intent.getAction() != null) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10653i, java.lang.String.format("Received %s", new java.lang.Object[]{intent.getAction()}), new java.lang.Throwable[0]);
            java.lang.String action = intent.getAction();
            char c = 65535;
            int hashCode = action.hashCode();
            if (hashCode != -1181163412) {
                if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                    c = 0;
                }
            } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
                c = 1;
            }
            if (c == 0) {
                mo16804a(true);
            } else if (c == 1) {
                mo16804a(false);
            }
        }
    }
}
