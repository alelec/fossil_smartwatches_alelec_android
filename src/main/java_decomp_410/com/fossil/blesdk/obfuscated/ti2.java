package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ti2 extends si2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public long F;

    /*
    static {
        H.put(R.id.v_line_bottom, 1);
        H.put(R.id.iv_workout, 2);
        H.put(R.id.ftv_workout_title, 3);
        H.put(R.id.v_line, 4);
        H.put(R.id.ftv_workout_time, 5);
        H.put(R.id.iv_active_time, 6);
        H.put(R.id.ftv_active_time, 7);
        H.put(R.id.iv_distance, 8);
        H.put(R.id.ftv_distance, 9);
        H.put(R.id.iv_steps, 10);
        H.put(R.id.ftv_steps, 11);
        H.put(R.id.v_center_horizontal, 12);
        H.put(R.id.iv_calories, 13);
        H.put(R.id.ftv_calories, 14);
        H.put(R.id.iv_heart_rate_avg, 15);
        H.put(R.id.ftv_heart_rate_avg, 16);
        H.put(R.id.iv_heart_rate_max, 17);
        H.put(R.id.ftv_heart_rate_max, 18);
    }
    */

    @DexIgnore
    public ti2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 19, G, H));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public ti2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[7], objArr[14], objArr[9], objArr[16], objArr[18], objArr[11], objArr[5], objArr[3], objArr[6], objArr[13], objArr[8], objArr[15], objArr[17], objArr[10], objArr[2], objArr[12], objArr[4], objArr[1]);
        this.F = -1;
        this.E = objArr[0];
        this.E.setTag((Object) null);
        a(view);
        f();
    }
}
