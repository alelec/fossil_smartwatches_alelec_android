package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface s53 extends v52<r53> {
    @DexIgnore
    void a(WatchApp watchApp);

    @DexIgnore
    void a(String str);

    @DexIgnore
    void b(List<Pair<WatchApp, String>> list);

    @DexIgnore
    void e(List<Pair<WatchApp, String>> list);

    @DexIgnore
    void u();
}
