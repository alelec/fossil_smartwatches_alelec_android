package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.internal.framed.ErrorCode;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface jw3 {
    @DexIgnore
    public static final jw3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements jw3 {
        @DexIgnore
        public void a(int i, ErrorCode errorCode) {
        }

        @DexIgnore
        public boolean a(int i, lo4 lo4, int i2, boolean z) throws IOException {
            lo4.skip((long) i2);
            return true;
        }

        @DexIgnore
        public boolean a(int i, List<cw3> list) {
            return true;
        }

        @DexIgnore
        public boolean a(int i, List<cw3> list, boolean z) {
            return true;
        }
    }

    @DexIgnore
    void a(int i, ErrorCode errorCode);

    @DexIgnore
    boolean a(int i, lo4 lo4, int i2, boolean z) throws IOException;

    @DexIgnore
    boolean a(int i, List<cw3> list);

    @DexIgnore
    boolean a(int i, List<cw3> list, boolean z);
}
