package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.ij0;
import com.fossil.blesdk.obfuscated.pj0;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oj0<T extends IInterface> extends ij0<T> implements de0.f, pj0.a {
    @DexIgnore
    public /* final */ kj0 B;
    @DexIgnore
    public /* final */ Set<Scope> C;
    @DexIgnore
    public /* final */ Account D;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public oj0(Context context, Looper looper, int i, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        this(context, looper, r3, r4, i, kj0, bVar, cVar);
        qj0 a = qj0.a(context);
        xd0 a2 = xd0.a();
        bk0.a(bVar);
        bk0.a(cVar);
    }

    @DexIgnore
    public static ij0.a a(ge0.b bVar) {
        if (bVar == null) {
            return null;
        }
        return new dl0(bVar);
    }

    @DexIgnore
    public final kj0 F() {
        return this.B;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return set;
    }

    @DexIgnore
    public final Set<Scope> b(Set<Scope> set) {
        Set<Scope> a = a(set);
        for (Scope contains : a) {
            if (!set.contains(contains)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return a;
    }

    @DexIgnore
    public int i() {
        return super.i();
    }

    @DexIgnore
    public final Account r() {
        return this.D;
    }

    @DexIgnore
    public final Set<Scope> w() {
        return this.C;
    }

    @DexIgnore
    public static ij0.b a(ge0.c cVar) {
        if (cVar == null) {
            return null;
        }
        return new el0(cVar);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oj0(Context context, Looper looper, qj0 qj0, xd0 xd0, int i, kj0 kj0, ge0.b bVar, ge0.c cVar) {
        super(context, looper, qj0, xd0, i, a(bVar), a(cVar), kj0.g());
        this.B = kj0;
        this.D = kj0.a();
        this.C = b(kj0.d());
    }
}
