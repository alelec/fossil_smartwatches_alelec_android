package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dk */
public class C1637dk {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.lang.String f4408b; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("SystemJobInfoConverter");

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.ComponentName f4409a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dk$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dk$a */
    public static /* synthetic */ class C1638a {

        @DexIgnore
        /* renamed from: a */
        public static /* final */ /* synthetic */ int[] f4410a; // = new int[androidx.work.NetworkType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            f4410a[androidx.work.NetworkType.NOT_REQUIRED.ordinal()] = 1;
            f4410a[androidx.work.NetworkType.CONNECTED.ordinal()] = 2;
            f4410a[androidx.work.NetworkType.UNMETERED.ordinal()] = 3;
            f4410a[androidx.work.NetworkType.NOT_ROAMING.ordinal()] = 4;
            f4410a[androidx.work.NetworkType.METERED.ordinal()] = 5;
        }
        */
    }

    @DexIgnore
    public C1637dk(android.content.Context context) {
        this.f4409a = new android.content.ComponentName(context.getApplicationContext(), androidx.work.impl.background.systemjob.SystemJobService.class);
    }

    @DexIgnore
    /* renamed from: a */
    public android.app.job.JobInfo mo10004a(com.fossil.blesdk.obfuscated.C1954hl hlVar, int i) {
        com.fossil.blesdk.obfuscated.C3365yi yiVar = hlVar.f5780j;
        int a = m5925a(yiVar.mo18107b());
        android.os.PersistableBundle persistableBundle = new android.os.PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", hlVar.f5771a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", hlVar.mo11673d());
        android.app.job.JobInfo.Builder extras = new android.app.job.JobInfo.Builder(i, this.f4409a).setRequiredNetworkType(a).setRequiresCharging(yiVar.mo18117g()).setRequiresDeviceIdle(yiVar.mo18118h()).setExtras(persistableBundle);
        if (!yiVar.mo18118h()) {
            extras.setBackoffCriteria(hlVar.f5783m, hlVar.f5782l == androidx.work.BackoffPolicy.LINEAR ? 0 : 1);
        }
        long max = java.lang.Math.max(hlVar.mo11670a() - java.lang.System.currentTimeMillis(), 0);
        if (android.os.Build.VERSION.SDK_INT <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else {
            extras.setImportantWhileForeground(true);
        }
        if (android.os.Build.VERSION.SDK_INT >= 24 && yiVar.mo18114e()) {
            for (com.fossil.blesdk.obfuscated.C3439zi.C3440a a2 : yiVar.mo18102a().mo18494a()) {
                extras.addTriggerContentUri(m5926a(a2));
            }
            extras.setTriggerContentUpdateDelay(yiVar.mo18110c());
            extras.setTriggerContentMaxDelay(yiVar.mo18112d());
        }
        extras.setPersisted(false);
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(yiVar.mo18116f());
            extras.setRequiresStorageNotLow(yiVar.mo18120i());
        }
        return extras.build();
    }

    @DexIgnore
    /* renamed from: a */
    public static android.app.job.JobInfo.TriggerContentUri m5926a(com.fossil.blesdk.obfuscated.C3439zi.C3440a aVar) {
        return new android.app.job.JobInfo.TriggerContentUri(aVar.mo18499a(), aVar.mo18500b() ? 1 : 0);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m5925a(androidx.work.NetworkType networkType) {
        int i = com.fossil.blesdk.obfuscated.C1637dk.C1638a.f4410a[networkType.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i != 4) {
            if (i == 5 && android.os.Build.VERSION.SDK_INT >= 26) {
                return 4;
            }
        } else if (android.os.Build.VERSION.SDK_INT >= 24) {
            return 3;
        }
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f4408b, java.lang.String.format("API version too low. Cannot convert network type value %s", new java.lang.Object[]{networkType}), new java.lang.Throwable[0]);
        return 1;
    }
}
