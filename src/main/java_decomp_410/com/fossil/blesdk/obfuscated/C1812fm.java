package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fm */
public final class C1812fm {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ com.fossil.blesdk.obfuscated.C1812fm f5217c; // = new com.fossil.blesdk.obfuscated.C1812fm();

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.ExecutorService f5218a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.concurrent.Executor f5219b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fm$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fm$b */
    public static class C1814b implements java.util.concurrent.Executor {

        @DexIgnore
        /* renamed from: e */
        public java.lang.ThreadLocal<java.lang.Integer> f5220e;

        @DexIgnore
        public C1814b() {
            this.f5220e = new java.lang.ThreadLocal<>();
        }

        @DexIgnore
        /* renamed from: a */
        public final int mo10963a() {
            java.lang.Integer num = this.f5220e.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.f5220e.remove();
            } else {
                this.f5220e.set(java.lang.Integer.valueOf(intValue));
            }
            return intValue;
        }

        @DexIgnore
        /* renamed from: b */
        public final int mo10964b() {
            java.lang.Integer num = this.f5220e.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.f5220e.set(java.lang.Integer.valueOf(intValue));
            return intValue;
        }

        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            if (mo10964b() <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    mo10963a();
                    throw th;
                }
            } else {
                com.fossil.blesdk.obfuscated.C1812fm.m7055a().execute(runnable);
            }
            mo10963a();
        }
    }

    @DexIgnore
    public C1812fm() {
        this.f5218a = !m7057c() ? java.util.concurrent.Executors.newCachedThreadPool() : com.fossil.blesdk.obfuscated.C1491bm.m5037a();
        java.util.concurrent.Executors.newSingleThreadScheduledExecutor();
        this.f5219b = new com.fossil.blesdk.obfuscated.C1812fm.C1814b();
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.concurrent.ExecutorService m7055a() {
        return f5217c.f5218a;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.util.concurrent.Executor m7056b() {
        return f5217c.f5219b;
    }

    @DexIgnore
    /* renamed from: c */
    public static boolean m7057c() {
        java.lang.String property = java.lang.System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(java.util.Locale.US).contains("android");
    }
}
