package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class le2 extends ke2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.iv_close, 1);
        B.put(R.id.progressBar, 2);
        B.put(R.id.tv_title, 3);
        B.put(R.id.tv_desc, 4);
        B.put(R.id.scroll_unit_layout, 5);
        B.put(R.id.ftv_height, 6);
        B.put(R.id.tl_height_unit, 7);
        B.put(R.id.rvp_height, 8);
        B.put(R.id.ftv_weight, 9);
        B.put(R.id.tl_weight_unit, 10);
        B.put(R.id.rvp_weight, 11);
        B.put(R.id.tv_skip, 12);
        B.put(R.id.bt_continue, 13);
    }
    */

    @DexIgnore
    public le2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 14, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public le2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[13], objArr[6], objArr[9], objArr[1], objArr[2], objArr[8], objArr[11], objArr[5], objArr[7], objArr[10], objArr[4], objArr[12], objArr[3]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
