package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xk {
    @DexIgnore
    public static xk e;
    @DexIgnore
    public rk a;
    @DexIgnore
    public sk b;
    @DexIgnore
    public vk c;
    @DexIgnore
    public wk d;

    @DexIgnore
    public xk(Context context, zl zlVar) {
        Context applicationContext = context.getApplicationContext();
        this.a = new rk(applicationContext, zlVar);
        this.b = new sk(applicationContext, zlVar);
        this.c = new vk(applicationContext, zlVar);
        this.d = new wk(applicationContext, zlVar);
    }

    @DexIgnore
    public static synchronized xk a(Context context, zl zlVar) {
        xk xkVar;
        synchronized (xk.class) {
            if (e == null) {
                e = new xk(context, zlVar);
            }
            xkVar = e;
        }
        return xkVar;
    }

    @DexIgnore
    public sk b() {
        return this.b;
    }

    @DexIgnore
    public vk c() {
        return this.c;
    }

    @DexIgnore
    public wk d() {
        return this.d;
    }

    @DexIgnore
    public rk a() {
        return this.a;
    }
}
