package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.SignInAccount;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ed0 implements Parcelable.Creator<SignInAccount> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = "";
        GoogleSignInAccount googleSignInAccount = null;
        String str2 = str;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 4) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 7) {
                googleSignInAccount = (GoogleSignInAccount) SafeParcelReader.a(parcel, a, GoogleSignInAccount.CREATOR);
            } else if (a2 != 8) {
                SafeParcelReader.v(parcel, a);
            } else {
                str2 = SafeParcelReader.f(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new SignInAccount(str, googleSignInAccount, str2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new SignInAccount[i];
    }
}
