package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class tk<T> extends uk<T> {
    @DexIgnore
    public static /* final */ String h; // = dj.a("BrdcstRcvrCnstrntTrckr");
    @DexIgnore
    public /* final */ BroadcastReceiver g; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                tk.this.a(context, intent);
            }
        }
    }

    @DexIgnore
    public tk(Context context, zl zlVar) {
        super(context, zlVar);
    }

    @DexIgnore
    public abstract void a(Context context, Intent intent);

    @DexIgnore
    public void b() {
        dj.a().a(h, String.format("%s: registering receiver", new Object[]{getClass().getSimpleName()}), new Throwable[0]);
        this.b.registerReceiver(this.g, d());
    }

    @DexIgnore
    public void c() {
        dj.a().a(h, String.format("%s: unregistering receiver", new Object[]{getClass().getSimpleName()}), new Throwable[0]);
        this.b.unregisterReceiver(this.g);
    }

    @DexIgnore
    public abstract IntentFilter d();
}
