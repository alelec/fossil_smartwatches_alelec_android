package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b22 implements Cloneable {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int[] h;

    @DexIgnore
    public b22(int i) {
        this(i, i);
    }

    @DexIgnore
    public boolean a(int i, int i2) {
        return ((this.h[(i2 * this.g) + (i / 32)] >>> (i & 31)) & 1) != 0;
    }

    @DexIgnore
    public void b(int i, int i2) {
        int i3 = (i2 * this.g) + (i / 32);
        int[] iArr = this.h;
        iArr[i3] = (1 << (i & 31)) | iArr[i3];
    }

    @DexIgnore
    public int c() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof b22)) {
            return false;
        }
        b22 b22 = (b22) obj;
        if (this.e == b22.e && this.f == b22.f && this.g == b22.g && Arrays.equals(this.h, b22.h)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.e;
        return (((((((i * 31) + i) * 31) + this.f) * 31) + this.g) * 31) + Arrays.hashCode(this.h);
    }

    @DexIgnore
    public String toString() {
        return a("X ", "  ");
    }

    @DexIgnore
    public b22(int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.e = i;
        this.f = i2;
        this.g = (i + 31) / 32;
        this.h = new int[(this.g * i2)];
    }

    @DexIgnore
    public b22 clone() {
        return new b22(this.e, this.f, this.g, (int[]) this.h.clone());
    }

    @DexIgnore
    public void a() {
        int length = this.h.length;
        for (int i = 0; i < length; i++) {
            this.h[i] = 0;
        }
    }

    @DexIgnore
    public int b() {
        return this.f;
    }

    @DexIgnore
    public void a(int i, int i2, int i3, int i4) {
        if (i2 < 0 || i < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        } else if (i4 <= 0 || i3 <= 0) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        } else {
            int i5 = i3 + i;
            int i6 = i4 + i2;
            if (i6 > this.f || i5 > this.e) {
                throw new IllegalArgumentException("The region must fit inside the matrix");
            }
            while (i2 < i6) {
                int i7 = this.g * i2;
                for (int i8 = i; i8 < i5; i8++) {
                    int[] iArr = this.h;
                    int i9 = (i8 / 32) + i7;
                    iArr[i9] = iArr[i9] | (1 << (i8 & 31));
                }
                i2++;
            }
        }
    }

    @DexIgnore
    public b22(int i, int i2, int i3, int[] iArr) {
        this.e = i;
        this.f = i2;
        this.g = i3;
        this.h = iArr;
    }

    @DexIgnore
    public String a(String str, String str2) {
        return a(str, str2, "\n");
    }

    @DexIgnore
    public final String a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(this.f * (this.e + 1));
        for (int i = 0; i < this.f; i++) {
            for (int i2 = 0; i2 < this.e; i2++) {
                sb.append(a(i2, i) ? str : str2);
            }
            sb.append(str3);
        }
        return sb.toString();
    }
}
