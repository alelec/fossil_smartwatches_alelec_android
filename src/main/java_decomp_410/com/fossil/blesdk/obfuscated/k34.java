package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k34 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131821371;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131821372;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131821373;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131821374;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131821375;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131821376;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131821377;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131821378;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131821401;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131821402;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131821403;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131821404;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131821405;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131821406;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131821407;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131821408;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131821409;
    @DexIgnore
    public static /* final */ int belvedere_dialog_camera; // = 2131821438;
    @DexIgnore
    public static /* final */ int belvedere_dialog_gallery; // = 2131821439;
    @DexIgnore
    public static /* final */ int belvedere_dialog_unknown; // = 2131821440;
    @DexIgnore
    public static /* final */ int belvedere_sdk_fpa_suffix; // = 2131821443;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131821651;
}
