package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62.a;
import com.fossil.blesdk.obfuscated.i62.b;
import com.fossil.blesdk.obfuscated.i62.c;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class i62<Q extends b, R extends c, E extends a> {
    @DexIgnore
    public static /* final */ String c; // = "i62";
    @DexIgnore
    public Q a;
    @DexIgnore
    public d<R, E> b;

    @DexIgnore
    public interface a {
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore
    public interface d<R, E> {
        @DexIgnore
        void a(E e);

        @DexIgnore
        void onSuccess(R r);
    }

    @DexIgnore
    public d<R, E> a() {
        return this.b;
    }

    @DexIgnore
    public abstract void a(Q q);

    @DexIgnore
    public void b(Q q) {
        this.a = q;
    }

    @DexIgnore
    public void a(d<R, E> dVar) {
        this.b = dVar;
    }

    @DexIgnore
    public void b() {
        String simpleName = getClass().getSimpleName();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = c;
        local.d(str, "Inside .run useCase=" + simpleName);
        Thread.currentThread().setName(simpleName);
        a(this.a);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local2.d(str2, "Inside .done useCase=" + simpleName);
    }
}
