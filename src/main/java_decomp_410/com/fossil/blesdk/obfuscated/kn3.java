package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase;
import com.portfolio.platform.uirenew.pairing.scanning.PairingPresenter;
import com.portfolio.platform.usecase.SetNotificationUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kn3 implements Factory<PairingPresenter> {
    @DexIgnore
    public static PairingPresenter a(gn3 gn3, LinkDeviceUseCase linkDeviceUseCase, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, vy2 vy2, px2 px2, vj2 vj2, NotificationSettingsDatabase notificationSettingsDatabase, SetNotificationUseCase setNotificationUseCase, en2 en2) {
        return new PairingPresenter(gn3, linkDeviceUseCase, deviceRepository, notificationsRepository, vy2, px2, vj2, notificationSettingsDatabase, setNotificationUseCase, en2);
    }
}
