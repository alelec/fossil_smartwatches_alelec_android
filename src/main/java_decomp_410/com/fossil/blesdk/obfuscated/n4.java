package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.SolverVariable;
import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.q4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n4 implements q4.a {
    @DexIgnore
    public SolverVariable a; // = null;
    @DexIgnore
    public float b; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ m4 d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public n4(o4 o4Var) {
        this.d = new m4(this, o4Var);
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, SolverVariable solverVariable2, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(solverVariable, -1.0f);
            this.d.a(solverVariable2, 1.0f);
        } else {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public boolean b() {
        SolverVariable solverVariable = this.a;
        return solverVariable != null && (solverVariable.g == SolverVariable.Type.UNRESTRICTED || this.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public n4 c(SolverVariable solverVariable, int i) {
        if (i < 0) {
            this.b = (float) (i * -1);
            this.d.a(solverVariable, 1.0f);
        } else {
            this.b = (float) i;
            this.d.a(solverVariable, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public void clear() {
        this.d.a();
        this.a = null;
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void d() {
        this.a = null;
        this.d.a();
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ce  */
    public String e() {
        String str;
        boolean z;
        String str2;
        String str3;
        if (this.a == null) {
            str = "" + "0";
        } else {
            str = "" + this.a;
        }
        String str4 = str + " = ";
        if (this.b != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            str4 = str4 + this.b;
            z = true;
        } else {
            z = false;
        }
        int i = this.d.a;
        for (int i2 = 0; i2 < i; i2++) {
            SolverVariable a2 = this.d.a(i2);
            if (a2 != null) {
                float b2 = this.d.b(i2);
                int i3 = (b2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (b2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1));
                if (i3 != 0) {
                    String solverVariable = a2.toString();
                    if (!z) {
                        if (b2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                            str2 = str2 + "- ";
                        }
                        if (b2 == 1.0f) {
                            str3 = str2 + solverVariable;
                        } else {
                            str3 = str2 + b2 + " " + solverVariable;
                        }
                        z = true;
                    } else if (i3 > 0) {
                        str2 = str2 + " + ";
                        if (b2 == 1.0f) {
                        }
                        z = true;
                    } else {
                        str2 = str2 + " - ";
                    }
                    b2 *= -1.0f;
                    if (b2 == 1.0f) {
                    }
                    z = true;
                }
            }
        }
        if (z) {
            return str2;
        }
        return str2 + "0.0";
    }

    @DexIgnore
    public SolverVariable getKey() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return e();
    }

    @DexIgnore
    public boolean b(SolverVariable solverVariable) {
        return this.d.a(solverVariable);
    }

    @DexIgnore
    public n4 b(SolverVariable solverVariable, int i) {
        this.a = solverVariable;
        float f = (float) i;
        solverVariable.e = f;
        this.b = f;
        this.e = true;
        return this;
    }

    @DexIgnore
    public SolverVariable c(SolverVariable solverVariable) {
        return this.d.a((boolean[]) null, solverVariable);
    }

    @DexIgnore
    public void d(SolverVariable solverVariable) {
        SolverVariable solverVariable2 = this.a;
        if (solverVariable2 != null) {
            this.d.a(solverVariable2, -1.0f);
            this.a = null;
        }
        float a2 = this.d.a(solverVariable, true) * -1.0f;
        this.a = solverVariable;
        if (a2 != 1.0f) {
            this.b /= a2;
            this.d.a(a2);
        }
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, int i) {
        this.d.a(solverVariable, (float) i);
        return this;
    }

    @DexIgnore
    public boolean c() {
        return this.a == null && this.b == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.d.a == 0;
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(solverVariable, -1.0f);
            this.d.a(solverVariable2, 1.0f);
            this.d.a(solverVariable3, 1.0f);
        } else {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable3, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public n4 b(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(solverVariable, -1.0f);
            this.d.a(solverVariable2, 1.0f);
            this.d.a(solverVariable3, -1.0f);
        } else {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable3, 1.0f);
        }
        return this;
    }

    @DexIgnore
    public n4 a(float f, float f2, float f3, SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4) {
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f == f3) {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable4, 1.0f);
            this.d.a(solverVariable3, -1.0f);
        } else if (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
        } else if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(solverVariable3, 1.0f);
            this.d.a(solverVariable4, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable4, f4);
            this.d.a(solverVariable3, -f4);
        }
        return this;
    }

    @DexIgnore
    public n4 b(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f) {
        this.d.a(solverVariable3, 0.5f);
        this.d.a(solverVariable4, 0.5f);
        this.d.a(solverVariable, -0.5f);
        this.d.a(solverVariable2, -0.5f);
        this.b = -f;
        return this;
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, SolverVariable solverVariable2, int i, float f, SolverVariable solverVariable3, SolverVariable solverVariable4, int i2) {
        if (solverVariable2 == solverVariable3) {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable4, 1.0f);
            this.d.a(solverVariable2, -2.0f);
            return this;
        }
        if (f == 0.5f) {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable3, -1.0f);
            this.d.a(solverVariable4, 1.0f);
            if (i > 0 || i2 > 0) {
                this.b = (float) ((-i) + i2);
            }
        } else if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(solverVariable, -1.0f);
            this.d.a(solverVariable2, 1.0f);
            this.b = (float) i;
        } else if (f >= 1.0f) {
            this.d.a(solverVariable3, -1.0f);
            this.d.a(solverVariable4, 1.0f);
            this.b = (float) i2;
        } else {
            float f2 = 1.0f - f;
            this.d.a(solverVariable, f2 * 1.0f);
            this.d.a(solverVariable2, f2 * -1.0f);
            this.d.a(solverVariable3, -1.0f * f);
            this.d.a(solverVariable4, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.b = (((float) (-i)) * f2) + (((float) i2) * f);
            }
        }
        return this;
    }

    @DexIgnore
    public n4 a(q4 q4Var, int i) {
        this.d.a(q4Var.a(i, "ep"), 1.0f);
        this.d.a(q4Var.a(i, UserDataStore.EMAIL), -1.0f);
        return this;
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, float f) {
        this.d.a(solverVariable, -1.0f);
        this.d.a(solverVariable2, 1.0f - f);
        this.d.a(solverVariable3, f);
        return this;
    }

    @DexIgnore
    public n4 a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f) {
        this.d.a(solverVariable, -1.0f);
        this.d.a(solverVariable2, 1.0f);
        this.d.a(solverVariable3, f);
        this.d.a(solverVariable4, -f);
        return this;
    }

    @DexIgnore
    public void a() {
        float f = this.b;
        if (f < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b = f * -1.0f;
            this.d.b();
        }
    }

    @DexIgnore
    public boolean a(q4 q4Var) {
        boolean z;
        SolverVariable a2 = this.d.a(q4Var);
        if (a2 == null) {
            z = true;
        } else {
            d(a2);
            z = false;
        }
        if (this.d.a == 0) {
            this.e = true;
        }
        return z;
    }

    @DexIgnore
    public SolverVariable a(q4 q4Var, boolean[] zArr) {
        return this.d.a(zArr, (SolverVariable) null);
    }

    @DexIgnore
    public void a(q4.a aVar) {
        if (aVar instanceof n4) {
            n4 n4Var = (n4) aVar;
            this.a = null;
            this.d.a();
            int i = 0;
            while (true) {
                m4 m4Var = n4Var.d;
                if (i < m4Var.a) {
                    this.d.a(m4Var.a(i), n4Var.d.b(i), true);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public void a(SolverVariable solverVariable) {
        int i = solverVariable.d;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.d.a(solverVariable, f);
    }
}
