package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dk4 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public dk4(String str) {
        kd4.b(str, "symbol");
        this.a = str;
    }

    @DexIgnore
    public String toString() {
        return this.a;
    }
}
