package com.fossil.blesdk.obfuscated;

import android.media.session.MediaSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cd implements bd {
    @DexIgnore
    public /* final */ MediaSessionManager.RemoteUserInfo a;

    @DexIgnore
    public cd(String str, int i, int i2) {
        this.a = new MediaSessionManager.RemoteUserInfo(str, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cd)) {
            return false;
        }
        return this.a.equals(((cd) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return e8.a(this.a);
    }
}
