package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ho1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ go1 e;

    @DexIgnore
    public ho1(go1 go1) {
        this.e = go1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.e.b) {
            if (this.e.c != null) {
                this.e.c.onCanceled();
            }
        }
    }
}
