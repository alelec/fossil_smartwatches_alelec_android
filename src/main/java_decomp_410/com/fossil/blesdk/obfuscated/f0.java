package com.fossil.blesdk.obfuscated;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.view.ActionMode;
import com.fossil.blesdk.obfuscated.q8;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f0 extends Dialog implements d0 {
    @DexIgnore
    public AppCompatDelegate e;
    @DexIgnore
    public /* final */ q8.a f; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements q8.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean superDispatchKeyEvent(KeyEvent keyEvent) {
            return f0.this.a(keyEvent);
        }
    }

    @DexIgnore
    public f0(Context context, int i) {
        super(context, a(context, i));
        a().a((Bundle) null);
        a().a();
    }

    @DexIgnore
    public boolean a(int i) {
        return a().b(i);
    }

    @DexIgnore
    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    @DexIgnore
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return q8.a(this.f, getWindow().getDecorView(), this, keyEvent);
    }

    @DexIgnore
    public <T extends View> T findViewById(int i) {
        return a().a(i);
    }

    @DexIgnore
    public void invalidateOptionsMenu() {
        a().f();
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        a().e();
        super.onCreate(bundle);
        a().a(bundle);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        a().j();
    }

    @DexIgnore
    public void onSupportActionModeFinished(ActionMode actionMode) {
    }

    @DexIgnore
    public void onSupportActionModeStarted(ActionMode actionMode) {
    }

    @DexIgnore
    public ActionMode onWindowStartingSupportActionMode(ActionMode.Callback callback) {
        return null;
    }

    @DexIgnore
    public void setContentView(int i) {
        a().c(i);
    }

    @DexIgnore
    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }

    @DexIgnore
    public AppCompatDelegate a() {
        if (this.e == null) {
            this.e = AppCompatDelegate.a((Dialog) this, (d0) this);
        }
        return this.e;
    }

    @DexIgnore
    public void setContentView(View view) {
        a().a(view);
    }

    @DexIgnore
    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    @DexIgnore
    public void setTitle(int i) {
        super.setTitle(i);
        a().a((CharSequence) getContext().getString(i));
    }

    @DexIgnore
    public static int a(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(r.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    @DexIgnore
    public boolean a(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
}
