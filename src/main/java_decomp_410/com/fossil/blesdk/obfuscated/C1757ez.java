package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ez */
public class C1757ez extends com.fossil.blesdk.obfuscated.e54 implements com.fossil.blesdk.obfuscated.C1588cz {
    @DexIgnore
    public C1757ez(com.fossil.blesdk.obfuscated.v44 v44, java.lang.String str, java.lang.String str2, com.fossil.blesdk.obfuscated.z64 z64) {
        super(v44, str, str2, z64, p011io.fabric.sdk.android.services.network.HttpMethod.POST);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9713a(com.fossil.blesdk.obfuscated.C1520bz bzVar) {
        p011io.fabric.sdk.android.services.network.HttpRequest a = mo26836a();
        mo10663a(a, bzVar);
        mo10662a(a, bzVar.f3947b);
        com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
        g.mo30060d("CrashlyticsCore", "Sending report to: " + mo26839b());
        int g2 = a.mo44621g();
        com.fossil.blesdk.obfuscated.y44 g3 = com.fossil.blesdk.obfuscated.q44.m26805g();
        g3.mo30060d("CrashlyticsCore", "Create report request ID: " + a.mo44613c("X-REQUEST-ID"));
        com.fossil.blesdk.obfuscated.y44 g4 = com.fossil.blesdk.obfuscated.q44.m26805g();
        g4.mo30060d("CrashlyticsCore", "Result was: " + g2);
        return com.fossil.blesdk.obfuscated.w54.m29544a(g2) == 0;
    }

    @DexIgnore
    /* renamed from: a */
    public final p011io.fabric.sdk.android.services.network.HttpRequest mo10663a(p011io.fabric.sdk.android.services.network.HttpRequest httpRequest, com.fossil.blesdk.obfuscated.C1520bz bzVar) {
        httpRequest.mo44611c("X-CRASHLYTICS-API-KEY", bzVar.f3946a);
        httpRequest.mo44611c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.mo44611c("X-CRASHLYTICS-API-CLIENT-VERSION", this.f14377e.mo9330r());
        for (java.util.Map.Entry<java.lang.String, java.lang.String> a : bzVar.f3947b.mo4192a().entrySet()) {
            httpRequest.mo44603a(a);
        }
        return httpRequest;
    }

    @DexIgnore
    /* renamed from: a */
    public final p011io.fabric.sdk.android.services.network.HttpRequest mo10662a(p011io.fabric.sdk.android.services.network.HttpRequest httpRequest, com.crashlytics.android.core.Report report) {
        httpRequest.mo44618e("report[identifier]", report.mo4193b());
        if (report.mo4195d().length == 1) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Adding single file " + report.mo4196e() + " to report " + report.mo4193b());
            httpRequest.mo44600a("report[file]", report.mo4196e(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, report.mo4194c());
            return httpRequest;
        }
        int i = 0;
        for (java.io.File file : report.mo4195d()) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Adding file " + file.getName() + " to report " + report.mo4193b());
            java.lang.StringBuilder sb = new java.lang.StringBuilder();
            sb.append("report[file");
            sb.append(i);
            sb.append("]");
            httpRequest.mo44600a(sb.toString(), file.getName(), com.zendesk.sdk.attachment.AttachmentHelper.DEFAULT_MIMETYPE, file);
            i++;
        }
        return httpRequest;
    }
}
