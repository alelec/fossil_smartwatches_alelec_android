package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w70 extends p70 {
    @DexIgnore
    public int L; // = 20;
    @DexIgnore
    public /* final */ boolean M;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w70(Peripheral peripheral) {
        super(DeviceConfigOperationCode.GET_OPTIMAL_PAYLOAD, RequestId.GET_OPTIMAL_PAYLOAD, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public boolean F() {
        return this.M;
    }

    @DexIgnore
    public final int I() {
        return this.L;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 2) {
            this.L = n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            wa0.a(jSONObject, JSONKey.OPTIMAL_PAYLOAD, Integer.valueOf(this.L));
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.SUCCESS, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        } else {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_LENGTH, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
        c(true);
        return jSONObject;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.OPTIMAL_PAYLOAD, Integer.valueOf(this.L));
    }
}
