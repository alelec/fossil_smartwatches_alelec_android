package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y3 {
    @DexIgnore
    public static /* final */ int[] CardView; // = {16843071, 16843072, R.attr.cardBackgroundColor, R.attr.cardCornerRadius, R.attr.cardElevation, R.attr.cardMaxElevation, R.attr.cardPreventCornerOverlap, R.attr.cardUseCompatPadding, R.attr.contentPadding, R.attr.contentPaddingBottom, R.attr.contentPaddingLeft, R.attr.contentPaddingRight, R.attr.contentPaddingTop};
    @DexIgnore
    public static /* final */ int CardView_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int CardView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int CardView_cardBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int CardView_cardCornerRadius; // = 3;
    @DexIgnore
    public static /* final */ int CardView_cardElevation; // = 4;
    @DexIgnore
    public static /* final */ int CardView_cardMaxElevation; // = 5;
    @DexIgnore
    public static /* final */ int CardView_cardPreventCornerOverlap; // = 6;
    @DexIgnore
    public static /* final */ int CardView_cardUseCompatPadding; // = 7;
    @DexIgnore
    public static /* final */ int CardView_contentPadding; // = 8;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingBottom; // = 9;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingLeft; // = 10;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingRight; // = 11;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingTop; // = 12;
}
