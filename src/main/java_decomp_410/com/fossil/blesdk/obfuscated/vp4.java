package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import org.apache.maven.artifact.versioning.ComparableVersion;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vp4 implements up4 {
    @DexIgnore
    public String e;
    @DexIgnore
    public ComparableVersion f;

    @DexIgnore
    public vp4(String str) {
        a(str);
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(up4 up4) {
        if (up4 instanceof vp4) {
            return this.f.compareTo(((vp4) up4).f);
        }
        return compareTo((up4) new vp4(up4.toString()));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof up4) && compareTo((up4) obj) == 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.f.hashCode() + 11;
    }

    @DexIgnore
    public String toString() {
        return this.f.toString();
    }

    @DexIgnore
    public final void a(String str) {
        String str2;
        String str3;
        this.f = new ComparableVersion(str);
        int indexOf = str.indexOf(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        boolean z = false;
        boolean z2 = true;
        if (indexOf < 0) {
            str3 = null;
            str2 = str;
        } else {
            str2 = str.substring(0, indexOf);
            str3 = str.substring(indexOf + 1);
        }
        if (str3 != null) {
            try {
                if (str3.length() != 1) {
                    if (str3.startsWith("0")) {
                        this.e = str3;
                    }
                }
                Integer.valueOf(str3);
            } catch (NumberFormatException unused) {
                this.e = str3;
            }
        }
        if (str2.contains(CodelessMatcher.CURRENT_CLASS_NAME) || str2.startsWith("0")) {
            StringTokenizer stringTokenizer = new StringTokenizer(str2, CodelessMatcher.CURRENT_CLASS_NAME);
            try {
                a(stringTokenizer);
                if (stringTokenizer.hasMoreTokens()) {
                    a(stringTokenizer);
                }
                if (stringTokenizer.hasMoreTokens()) {
                    a(stringTokenizer);
                }
                if (stringTokenizer.hasMoreTokens()) {
                    this.e = stringTokenizer.nextToken();
                    z = Pattern.compile("\\d+").matcher(this.e).matches();
                }
                if (!str2.contains("..") && !str2.startsWith(CodelessMatcher.CURRENT_CLASS_NAME) && !str2.endsWith(CodelessMatcher.CURRENT_CLASS_NAME)) {
                    z2 = z;
                }
            } catch (NumberFormatException unused2) {
            }
            if (z2) {
                this.e = str;
                return;
            }
            return;
        }
        try {
            Integer.valueOf(str2);
        } catch (NumberFormatException unused3) {
            this.e = str;
        }
    }

    @DexIgnore
    public static Integer a(StringTokenizer stringTokenizer) {
        try {
            String nextToken = stringTokenizer.nextToken();
            if (nextToken.length() > 1) {
                if (nextToken.startsWith("0")) {
                    throw new NumberFormatException("Number part has a leading 0: '" + nextToken + "'");
                }
            }
            return Integer.valueOf(nextToken);
        } catch (NoSuchElementException unused) {
            throw new NumberFormatException("Number is invalid");
        }
    }
}
