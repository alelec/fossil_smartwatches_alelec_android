package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z80 extends q80 {
    @DexIgnore
    public long L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;
    @DexIgnore
    public /* final */ long P;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ z80(long j, long j2, long j3, short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(j, j2, j3, s, peripheral, (i2 & 32) != 0 ? 3 : i);
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(12).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).putInt((int) this.P).array();
        kd4.a((Object) array, "ByteBuffer.allocate(12)\n\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean F() {
        return this.M;
    }

    @DexIgnore
    public final long J() {
        return this.L;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (bArr.length >= 4) {
            this.L = n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            wa0.a(a, JSONKey.SEGMENT_CRC, Long.valueOf(this.L));
        }
        return a;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(wa0.a(super.t(), JSONKey.SEGMENT_OFFSET, Long.valueOf(this.N)), JSONKey.SEGMENT_LENGTH, Long.valueOf(this.O)), JSONKey.TOTAL_FILE_LENGTH, Long.valueOf(this.P));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.SEGMENT_CRC, Long.valueOf(this.L));
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public z80(long j, long j2, long j3, short s, Peripheral peripheral, int i) {
        super(LegacyFileControlOperationCode.LEGACY_VERIFY_SEGMENT, r2, RequestId.LEGACY_VERIFY_SEGMENT, peripheral, i);
        kd4.b(peripheral, "peripheral");
        short s2 = s;
        this.N = j;
        this.O = j2;
        this.P = j3;
        this.M = true;
    }
}
