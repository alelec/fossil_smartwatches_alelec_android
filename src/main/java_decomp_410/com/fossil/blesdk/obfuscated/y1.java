package com.fossil.blesdk.obfuscated;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.ActionBarContainer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class y1 extends Drawable {
    @DexIgnore
    public /* final */ ActionBarContainer a;

    @DexIgnore
    public y1(ActionBarContainer actionBarContainer) {
        this.a = actionBarContainer;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        ActionBarContainer actionBarContainer = this.a;
        if (actionBarContainer.l) {
            Drawable drawable = actionBarContainer.k;
            if (drawable != null) {
                drawable.draw(canvas);
                return;
            }
            return;
        }
        Drawable drawable2 = actionBarContainer.i;
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
        ActionBarContainer actionBarContainer2 = this.a;
        Drawable drawable3 = actionBarContainer2.j;
        if (drawable3 != null && actionBarContainer2.m) {
            drawable3.draw(canvas);
        }
    }

    @DexIgnore
    public int getOpacity() {
        return 0;
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        ActionBarContainer actionBarContainer = this.a;
        if (actionBarContainer.l) {
            Drawable drawable = actionBarContainer.k;
            if (drawable != null) {
                drawable.getOutline(outline);
                return;
            }
            return;
        }
        Drawable drawable2 = actionBarContainer.i;
        if (drawable2 != null) {
            drawable2.getOutline(outline);
        }
    }

    @DexIgnore
    public void setAlpha(int i) {
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
    }
}
