package com.fossil.blesdk.obfuscated;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fm {
    @DexIgnore
    public static /* final */ fm c; // = new fm();
    @DexIgnore
    public /* final */ ExecutorService a;
    @DexIgnore
    public /* final */ Executor b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public ThreadLocal<Integer> e;

        @DexIgnore
        public b() {
            this.e = new ThreadLocal<>();
        }

        @DexIgnore
        public final int a() {
            Integer num = this.e.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.e.remove();
            } else {
                this.e.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        @DexIgnore
        public final int b() {
            Integer num = this.e.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.e.set(Integer.valueOf(intValue));
            return intValue;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            if (b() <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a();
                    throw th;
                }
            } else {
                fm.a().execute(runnable);
            }
            a();
        }
    }

    @DexIgnore
    public fm() {
        this.a = !c() ? Executors.newCachedThreadPool() : bm.a();
        Executors.newSingleThreadScheduledExecutor();
        this.b = new b();
    }

    @DexIgnore
    public static ExecutorService a() {
        return c.a;
    }

    @DexIgnore
    public static Executor b() {
        return c.b;
    }

    @DexIgnore
    public static boolean c() {
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(Locale.US).contains("android");
    }
}
