package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pd0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<pd0> CREATOR; // = new qd0();
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;

    @DexIgnore
    public pd0(boolean z, long j, long j2) {
        this.e = z;
        this.f = j;
        this.g = j2;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof pd0) {
            pd0 pd0 = (pd0) obj;
            return this.e == pd0.e && this.f == pd0.f && this.g == pd0.g;
        }
    }

    @DexIgnore
    public final int hashCode() {
        return zj0.a(Boolean.valueOf(this.e), Long.valueOf(this.f), Long.valueOf(this.g));
    }

    @DexIgnore
    public final String toString() {
        return "CollectForDebugParcelable[skipPersistentStorage: " + this.e + ",collectForDebugStartTimeMillis: " + this.f + ",collectForDebugExpiryTimeMillis: " + this.g + "]";
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.g);
        kk0.a(parcel, 3, this.f);
        kk0.a(parcel, a);
    }
}
