package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hs4 implements gr4<em4, Character> {
    @DexIgnore
    public static /* final */ hs4 a; // = new hs4();

    @DexIgnore
    public Character a(em4 em4) throws IOException {
        String F = em4.F();
        if (F.length() == 1) {
            return Character.valueOf(F.charAt(0));
        }
        throw new IOException("Expected body of length 1 for Character conversion but was " + F.length());
    }
}
