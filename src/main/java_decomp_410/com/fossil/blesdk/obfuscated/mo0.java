package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.sn0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mo0 extends yy0 implements lo0 {
    @DexIgnore
    public mo0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    @DexIgnore
    public final sn0 a(sn0 sn0, String str, int i) throws RemoteException {
        Parcel o = o();
        az0.a(o, (IInterface) sn0);
        o.writeString(str);
        o.writeInt(i);
        Parcel a = a(2, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final int b(sn0 sn0, String str, boolean z) throws RemoteException {
        Parcel o = o();
        az0.a(o, (IInterface) sn0);
        o.writeString(str);
        az0.a(o, z);
        Parcel a = a(3, o);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final int n() throws RemoteException {
        Parcel a = a(6, o());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final int a(sn0 sn0, String str, boolean z) throws RemoteException {
        Parcel o = o();
        az0.a(o, (IInterface) sn0);
        o.writeString(str);
        az0.a(o, z);
        Parcel a = a(5, o);
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    @DexIgnore
    public final sn0 b(sn0 sn0, String str, int i) throws RemoteException {
        Parcel o = o();
        az0.a(o, (IInterface) sn0);
        o.writeString(str);
        o.writeInt(i);
        Parcel a = a(4, o);
        sn0 a2 = sn0.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
