package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class of {
    @DexIgnore
    public /* final */ Set<LiveData> a; // = Collections.newSetFromMap(new IdentityHashMap());
    @DexIgnore
    public /* final */ RoomDatabase b;

    @DexIgnore
    public of(RoomDatabase roomDatabase) {
        this.b = roomDatabase;
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return new vf(this.b, this, z, callable, strArr);
    }

    @DexIgnore
    public void b(LiveData liveData) {
        this.a.remove(liveData);
    }

    @DexIgnore
    public void a(LiveData liveData) {
        this.a.add(liveData);
    }
}
