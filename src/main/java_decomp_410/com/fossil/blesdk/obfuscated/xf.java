package com.fossil.blesdk.obfuscated;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xf implements Executor {
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public /* final */ ArrayDeque<Runnable> f; // = new ArrayDeque<>();
    @DexIgnore
    public Runnable g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable e;

        @DexIgnore
        public a(Runnable runnable) {
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.e.run();
            } finally {
                xf.this.a();
            }
        }
    }

    @DexIgnore
    public xf(Executor executor) {
        this.e = executor;
    }

    @DexIgnore
    public synchronized void a() {
        Runnable poll = this.f.poll();
        this.g = poll;
        if (poll != null) {
            this.e.execute(this.g);
        }
    }

    @DexIgnore
    public synchronized void execute(Runnable runnable) {
        this.f.offer(new a(runnable));
        if (this.g == null) {
            a();
        }
    }
}
