package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.data.workoutsession.ActivityType;
import com.fossil.blesdk.device.data.workoutsession.SessionState;
import com.fossil.blesdk.device.data.workoutsession.WorkoutSession;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v70 extends p70 {
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public WorkoutSession M; // = new WorkoutSession(0, ActivityType.IDLE, SessionState.COMPLETED, 0, 0, 0, 0, 0, 0, 0, 0);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public v70(Peripheral peripheral) {
        super(DeviceConfigOperationCode.GET_CURRENT_WORKOUT_SESSION, RequestId.GET_CURRENT_WORKOUT_SESSION, r4, 0, 8, (fd4) null);
        Peripheral peripheral2 = peripheral;
        kd4.b(peripheral2, "peripheral");
    }

    @DexIgnore
    public boolean F() {
        return this.L;
    }

    @DexIgnore
    public final WorkoutSession I() {
        return this.M;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        byte[] bArr2 = bArr;
        kd4.b(bArr2, "responseData");
        JSONObject jSONObject = new JSONObject();
        if (bArr2.length >= 29) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            ActivityType a2 = ActivityType.Companion.a(order.get(4));
            SessionState a3 = SessionState.Companion.a(order.get(5));
            if (a2 == null || a3 == null) {
                b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_DATA, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
            } else {
                this.M = new WorkoutSession(n90.b(order.getInt(0)), a2, a3, n90.b(order.getInt(6)), n90.b(order.getInt(10)), n90.b(order.getInt(14)), n90.b(order.getInt(18)), n90.b(order.getInt(22)), n90.b(order.get(26)), n90.b(order.get(27)), n90.b(order.get(28)));
                wa0.a(jSONObject, JSONKey.WORKOUT_SESSION, this.M.toJSONObject());
                b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.SUCCESS, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
            }
        } else {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_LENGTH, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
        c(true);
        return jSONObject;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.WORKOUT_SESSION, this.M.toJSONObject());
    }
}
