package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vc2 extends uc2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = new ViewDataBinding.j(23);
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        A.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{R.layout.view_no_device});
        B.put(R.id.ftv_alerts, 3);
        B.put(R.id.ftv_alarms_section, 4);
        B.put(R.id.ftv_add, 5);
        B.put(R.id.rv_alarms, 6);
        B.put(R.id.ftv_notifications_section, 7);
        B.put(R.id.ll_assign_notifications, 8);
        B.put(R.id.ftv_assign_notifications_overview, 9);
        B.put(R.id.v_line1, 10);
        B.put(R.id.ftv_do_not_disturb_section, 11);
        B.put(R.id.ftv_scheduled, 12);
        B.put(R.id.sw_scheduled, 13);
        B.put(R.id.v_line2, 14);
        B.put(R.id.cl_scheduled_time_container, 15);
        B.put(R.id.ftv_scheduled_start_label, 16);
        B.put(R.id.ll_scheduled_time_start, 17);
        B.put(R.id.ftv_scheduled_start_value, 18);
        B.put(R.id.ftv_scheduled_end_label, 19);
        B.put(R.id.ll_scheduled_time_end, 20);
        B.put(R.id.ftv_scheduled_end_value, 21);
        B.put(R.id.ftv_no_device_title, 22);
    }
    */

    @DexIgnore
    public vc2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 23, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
        ViewDataBinding.d(this.u);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.u.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 2;
        }
        this.u.f();
        g();
    }

    @DexIgnore
    public vc2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 1, objArr[1], objArr[15], objArr[5], objArr[4], objArr[3], objArr[9], objArr[11], objArr[22], objArr[7], objArr[12], objArr[19], objArr[21], objArr[16], objArr[18], objArr[2], objArr[8], objArr[20], objArr[17], objArr[6], objArr[13], objArr[10], objArr[14]);
        this.z = -1;
        this.q.setTag((Object) null);
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
