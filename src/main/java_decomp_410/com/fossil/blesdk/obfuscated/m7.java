package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.CancellationSignal;
import androidx.core.os.OperationCanceledException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m7 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public a b;
    @DexIgnore
    public Object c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void onCancel();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r0.onCancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0015, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r1 == null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        if (android.os.Build.VERSION.SDK_INT < 16) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001f, code lost:
        ((android.os.CancellationSignal) r1).cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0025, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x002a, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x002e, code lost:
        monitor-enter(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0032, code lost:
        monitor-exit(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0033, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000f, code lost:
        if (r0 == null) goto L_0x0017;
     */
    @DexIgnore
    public void a() {
        synchronized (this) {
            if (!this.a) {
                this.a = true;
                a aVar = this.b;
                Object obj = this.c;
            }
        }
    }

    @DexIgnore
    public Object b() {
        Object obj;
        if (Build.VERSION.SDK_INT < 16) {
            return null;
        }
        synchronized (this) {
            if (this.c == null) {
                this.c = new CancellationSignal();
                if (this.a) {
                    ((CancellationSignal) this.c).cancel();
                }
            }
            obj = this.c;
        }
        return obj;
    }

    @DexIgnore
    public boolean c() {
        boolean z;
        synchronized (this) {
            z = this.a;
        }
        return z;
    }

    @DexIgnore
    public void d() {
        if (c()) {
            throw new OperationCanceledException();
        }
    }
}
