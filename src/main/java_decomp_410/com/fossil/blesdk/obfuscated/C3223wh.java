package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wh */
public class C3223wh {

    @DexIgnore
    /* renamed from: a */
    public static android.animation.LayoutTransition f10636a;

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Field f10637b;

    @DexIgnore
    /* renamed from: c */
    public static boolean f10638c;

    @DexIgnore
    /* renamed from: d */
    public static java.lang.reflect.Method f10639d;

    @DexIgnore
    /* renamed from: e */
    public static boolean f10640e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.wh$a")
    /* renamed from: com.fossil.blesdk.obfuscated.wh$a */
    public static class C3224a extends android.animation.LayoutTransition {
        @DexIgnore
        public boolean isChangingLayout() {
            return true;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    public static void m15821a(android.view.ViewGroup viewGroup, boolean z) {
        android.animation.LayoutTransition layoutTransition;
        boolean z2 = false;
        if (f10636a == null) {
            f10636a = new com.fossil.blesdk.obfuscated.C3223wh.C3224a();
            f10636a.setAnimator(2, (android.animation.Animator) null);
            f10636a.setAnimator(0, (android.animation.Animator) null);
            f10636a.setAnimator(1, (android.animation.Animator) null);
            f10636a.setAnimator(3, (android.animation.Animator) null);
            f10636a.setAnimator(4, (android.animation.Animator) null);
        }
        if (z) {
            android.animation.LayoutTransition layoutTransition2 = viewGroup.getLayoutTransition();
            if (layoutTransition2 != null) {
                if (layoutTransition2.isRunning()) {
                    m15820a(layoutTransition2);
                }
                if (layoutTransition2 != f10636a) {
                    viewGroup.setTag(com.fossil.blesdk.obfuscated.C1875gh.transition_layout_save, layoutTransition2);
                }
            }
            viewGroup.setLayoutTransition(f10636a);
            return;
        }
        viewGroup.setLayoutTransition((android.animation.LayoutTransition) null);
        if (!f10638c) {
            try {
                f10637b = android.view.ViewGroup.class.getDeclaredField("mLayoutSuppressed");
                f10637b.setAccessible(true);
            } catch (java.lang.NoSuchFieldException unused) {
                android.util.Log.i("ViewGroupUtilsApi14", "Failed to access mLayoutSuppressed field by reflection");
            }
            f10638c = true;
        }
        java.lang.reflect.Field field = f10637b;
        if (field != null) {
            try {
                boolean z3 = field.getBoolean(viewGroup);
                if (z3) {
                    try {
                        f10637b.setBoolean(viewGroup, false);
                    } catch (java.lang.IllegalAccessException unused2) {
                        z2 = z3;
                    }
                }
                z2 = z3;
            } catch (java.lang.IllegalAccessException unused3) {
                android.util.Log.i("ViewGroupUtilsApi14", "Failed to get mLayoutSuppressed field by reflection");
                if (z2) {
                }
                layoutTransition = (android.animation.LayoutTransition) viewGroup.getTag(com.fossil.blesdk.obfuscated.C1875gh.transition_layout_save);
                if (layoutTransition == null) {
                }
            }
        }
        if (z2) {
            viewGroup.requestLayout();
        }
        layoutTransition = (android.animation.LayoutTransition) viewGroup.getTag(com.fossil.blesdk.obfuscated.C1875gh.transition_layout_save);
        if (layoutTransition == null) {
            viewGroup.setTag(com.fossil.blesdk.obfuscated.C1875gh.transition_layout_save, (java.lang.Object) null);
            viewGroup.setLayoutTransition(layoutTransition);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15820a(android.animation.LayoutTransition layoutTransition) {
        if (!f10640e) {
            try {
                f10639d = android.animation.LayoutTransition.class.getDeclaredMethod("cancel", new java.lang.Class[0]);
                f10639d.setAccessible(true);
            } catch (java.lang.NoSuchMethodException unused) {
                android.util.Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            }
            f10640e = true;
        }
        java.lang.reflect.Method method = f10639d;
        if (method != null) {
            try {
                method.invoke(layoutTransition, new java.lang.Object[0]);
            } catch (java.lang.IllegalAccessException unused2) {
                android.util.Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            } catch (java.lang.reflect.InvocationTargetException unused3) {
                android.util.Log.i("ViewGroupUtilsApi14", "Failed to invoke cancel method by reflection");
            }
        }
    }
}
