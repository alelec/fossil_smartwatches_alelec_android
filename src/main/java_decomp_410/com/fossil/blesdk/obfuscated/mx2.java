package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import com.fossil.blesdk.obfuscated.dx2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mx2 extends gx2 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public boolean f; // = true;
    @DexIgnore
    public dx2 g;
    @DexIgnore
    public /* final */ hx2 h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<dx2.a> {
        @DexIgnore
        public /* final */ /* synthetic */ mx2 a;

        @DexIgnore
        public b(mx2 mx2) {
            this.a = mx2;
        }

        @DexIgnore
        public final void a(dx2.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String i = mx2.i;
            local.d(i, "NotificationSettingChanged value = " + aVar);
            this.a.f = aVar.b();
            this.a.h.m(aVar.a());
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = mx2.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationSettingsType\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public mx2(hx2 hx2) {
        kd4.b(hx2, "mView");
        this.h = hx2;
    }

    @DexIgnore
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "start: isCall = " + this.f);
        dx2 dx2 = this.g;
        if (dx2 != null) {
            MutableLiveData<dx2.a> c = dx2.c();
            hx2 hx2 = this.h;
            if (hx2 != null) {
                c.a((ix2) hx2, new b(this));
                this.h.d(a(this.f));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        kd4.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "stop");
        dx2 dx2 = this.g;
        if (dx2 != null) {
            MutableLiveData<dx2.a> c = dx2.c();
            hx2 hx2 = this.h;
            if (hx2 != null) {
                c.a((LifecycleOwner) (ix2) hx2);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        kd4.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void h() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(dx2 dx2) {
        kd4.b(dx2, "viewModel");
        this.g = dx2;
    }

    @DexIgnore
    public final String a(boolean z) {
        if (z) {
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_CallsMessagesSettings_Text__AllowCallsFrom);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026ngs_Text__AllowCallsFrom)");
            return a2;
        }
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_CallsMessages_CallsMessagesSettings_Text__AllowMessagesFrom);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026_Text__AllowMessagesFrom)");
        return a3;
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "changeNotificationSettingsTypeTo: settingsType = " + i2);
        dx2.a aVar = new dx2.a(i2, this.f);
        dx2 dx2 = this.g;
        if (dx2 != null) {
            dx2.c().a(aVar);
        } else {
            kd4.d("mNotificationSettingViewModel");
            throw null;
        }
    }
}
