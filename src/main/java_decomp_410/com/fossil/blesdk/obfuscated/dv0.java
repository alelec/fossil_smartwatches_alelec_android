package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dv0 extends List {
    @DexIgnore
    List<?> D();

    @DexIgnore
    dv0 F();

    @DexIgnore
    Object e(int i);
}
