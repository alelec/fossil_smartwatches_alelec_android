package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q33 implements MembersInjector<SearchSecondTimezoneActivity> {
    @DexIgnore
    public static void a(SearchSecondTimezoneActivity searchSecondTimezoneActivity, SearchSecondTimezonePresenter searchSecondTimezonePresenter) {
        searchSecondTimezoneActivity.B = searchSecondTimezonePresenter;
    }
}
