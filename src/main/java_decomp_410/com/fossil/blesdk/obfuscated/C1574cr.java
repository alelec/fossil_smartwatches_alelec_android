package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.cr */
public class C1574cr {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2772qw<com.fossil.blesdk.obfuscated.C2143jo, java.lang.String> f4185a; // = new com.fossil.blesdk.obfuscated.C2772qw<>(1000);

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<com.fossil.blesdk.obfuscated.C1574cr.C1576b> f4186b; // = com.fossil.blesdk.obfuscated.C3145vw.m15491a(10, new com.fossil.blesdk.obfuscated.C1574cr.C1575a(this));

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cr$a")
    /* renamed from: com.fossil.blesdk.obfuscated.cr$a */
    public class C1575a implements com.fossil.blesdk.obfuscated.C3145vw.C3149d<com.fossil.blesdk.obfuscated.C1574cr.C1576b> {
        @DexIgnore
        public C1575a(com.fossil.blesdk.obfuscated.C1574cr crVar) {
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1574cr.C1576b m5516a() {
            try {
                return new com.fossil.blesdk.obfuscated.C1574cr.C1576b(java.security.MessageDigest.getInstance("SHA-256"));
            } catch (java.security.NoSuchAlgorithmException e) {
                throw new java.lang.RuntimeException(e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.cr$b")
    /* renamed from: com.fossil.blesdk.obfuscated.cr$b */
    public static final class C1576b implements com.fossil.blesdk.obfuscated.C3145vw.C3151f {

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.security.MessageDigest f4187e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C3324xw f4188f; // = com.fossil.blesdk.obfuscated.C3324xw.m16582b();

        @DexIgnore
        public C1576b(java.security.MessageDigest messageDigest) {
            this.f4187e = messageDigest;
        }

        @DexIgnore
        /* renamed from: i */
        public com.fossil.blesdk.obfuscated.C3324xw mo3959i() {
            return this.f4188f;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo9617a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        com.fossil.blesdk.obfuscated.C1574cr.C1576b a = this.f4186b.mo11162a();
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(a);
        com.fossil.blesdk.obfuscated.C1574cr.C1576b bVar = a;
        try {
            joVar.mo8934a(bVar.f4187e);
            return com.fossil.blesdk.obfuscated.C3066uw.m14925a(bVar.f4187e.digest());
        } finally {
            this.f4186b.mo11163a(bVar);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo9618b(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        java.lang.String a;
        synchronized (this.f4185a) {
            a = this.f4185a.mo15388a(joVar);
        }
        if (a == null) {
            a = mo9617a(joVar);
        }
        synchronized (this.f4185a) {
            this.f4185a.mo15393b(joVar, a);
        }
        return a;
    }
}
