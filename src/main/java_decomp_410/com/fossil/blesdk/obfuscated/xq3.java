package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xq3 extends uq3 {
    @DexIgnore
    public /* final */ vq3 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        kd4.a((Object) xq3.class.getSimpleName(), "WelcomePresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public xq3(vq3 vq3) {
        kd4.b(vq3, "mView");
        this.f = vq3;
    }

    @DexIgnore
    public void f() {
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        this.f.F0();
    }

    @DexIgnore
    public void i() {
        this.f.C0();
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }
}
