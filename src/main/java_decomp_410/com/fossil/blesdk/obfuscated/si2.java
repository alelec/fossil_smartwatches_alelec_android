package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class si2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ AppCompatImageView A;
    @DexIgnore
    public /* final */ AppCompatImageView B;
    @DexIgnore
    public /* final */ AppCompatImageView C;
    @DexIgnore
    public /* final */ AppCompatImageView D;
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ AppCompatImageView y;
    @DexIgnore
    public /* final */ AppCompatImageView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public si2(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, AppCompatImageView appCompatImageView, AppCompatImageView appCompatImageView2, AppCompatImageView appCompatImageView3, AppCompatImageView appCompatImageView4, AppCompatImageView appCompatImageView5, AppCompatImageView appCompatImageView6, AppCompatImageView appCompatImageView7, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = flexibleTextView3;
        this.t = flexibleTextView4;
        this.u = flexibleTextView5;
        this.v = flexibleTextView6;
        this.w = flexibleTextView7;
        this.x = flexibleTextView8;
        this.y = appCompatImageView;
        this.z = appCompatImageView2;
        this.A = appCompatImageView4;
        this.B = appCompatImageView5;
        this.C = appCompatImageView6;
        this.D = appCompatImageView7;
    }

    @DexIgnore
    public static si2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2) {
        return a(layoutInflater, viewGroup, z2, qa.a());
    }

    @DexIgnore
    @Deprecated
    public static si2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2, Object obj) {
        return (si2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_workout, viewGroup, z2, obj);
    }
}
