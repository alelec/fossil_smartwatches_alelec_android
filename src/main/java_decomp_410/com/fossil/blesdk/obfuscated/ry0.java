package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ry0 extends ts0 implements qy0 {
    @DexIgnore
    public ry0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.clearcut.internal.IClearcutLoggerService");
    }

    @DexIgnore
    public final void a(oy0 oy0, rd0 rd0) throws RemoteException {
        Parcel o = o();
        mu0.a(o, (IInterface) oy0);
        mu0.a(o, (Parcelable) rd0);
        a(1, o);
    }
}
