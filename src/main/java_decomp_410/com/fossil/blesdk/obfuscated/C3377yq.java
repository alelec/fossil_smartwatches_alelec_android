package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yq */
public final class C3377yq extends com.fossil.blesdk.obfuscated.C3234wq {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yq$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yq$a */
    public class C3378a implements com.fossil.blesdk.obfuscated.C3234wq.C3235a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.content.Context f11309a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.lang.String f11310b;

        @DexIgnore
        public C3378a(android.content.Context context, java.lang.String str) {
            this.f11309a = context;
            this.f11310b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public java.io.File mo17506a() {
            java.io.File cacheDir = this.f11309a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            java.lang.String str = this.f11310b;
            return str != null ? new java.io.File(cacheDir, str) : cacheDir;
        }
    }

    @DexIgnore
    public C3377yq(android.content.Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    @DexIgnore
    public C3377yq(android.content.Context context, java.lang.String str, long j) {
        super(new com.fossil.blesdk.obfuscated.C3377yq.C3378a(context, str), j);
    }
}
