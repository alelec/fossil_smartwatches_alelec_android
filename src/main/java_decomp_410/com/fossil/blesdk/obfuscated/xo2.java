package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.dm4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xo2 implements Interceptor {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((fd4) null);
    @DexIgnore
    public AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public Auth b;
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ dn2 d;
    @DexIgnore
    public /* final */ AuthApiGuestService e;
    @DexIgnore
    public /* final */ en2 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return xo2.g;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = xo2.class.getSimpleName();
        kd4.a((Object) simpleName, "AuthenticationInterceptor::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public xo2(PortfolioApp portfolioApp, dn2 dn2, AuthApiGuestService authApiGuestService, en2 en2) {
        kd4.b(portfolioApp, "mApplication");
        kd4.b(dn2, "mProviderManager");
        kd4.b(authApiGuestService, "mAuthApiGuestService");
        kd4.b(en2, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = dn2;
        this.e = authApiGuestService;
        this.f = en2;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) {
        ServerError serverError;
        kd4.b(chain, "chain");
        dm4.a f2 = chain.n().f();
        MFUser b2 = this.d.n().b();
        String userAccessToken = b2 != null ? b2.getUserAccessToken() : null;
        if (!TextUtils.isEmpty(userAccessToken)) {
            long currentTimeMillis = (System.currentTimeMillis() - this.f.a()) / ((long) 1000);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            StringBuilder sb = new StringBuilder();
            sb.append("loginDurationInSeconds = ");
            sb.append(currentTimeMillis);
            sb.append(" tokenExpiresIn ");
            kd4.a((Object) b2, "currentUser");
            sb.append(b2.getAccessTokenExpiresIn());
            local.d(str, sb.toString());
            if (currentTimeMillis >= ((long) b2.getAccessTokenExpiresIn())) {
                int i = 0;
                Auth a2 = a(b2, this.a.getAndIncrement() == 0);
                if (this.a.decrementAndGet() == 0) {
                    this.b = null;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local2.d(str2, "intercept decrementAndGet mCount=" + this.a);
                if ((a2 != null ? a2.getAccessToken() : null) != null) {
                    b2.setUserAccessToken(a2.getAccessToken());
                    b2.setRefreshToken(a2.getRefreshToken());
                    b2.setAccessTokenExpiresAt(rk2.t(a2.getAccessTokenExpiresAt()));
                    Integer accessTokenExpiresIn = a2.getAccessTokenExpiresIn();
                    if (accessTokenExpiresIn != null) {
                        i = accessTokenExpiresIn.intValue();
                    }
                    b2.setAccessTokenExpiresIn(Integer.valueOf(i));
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = g;
                    local3.d(str3, "accessToken = " + a2.getAccessToken() + ", refreshToken = " + a2.getRefreshToken() + " userOnboardingComplete=" + b2.isOnboardingComplete());
                    this.d.n().a(b2);
                    this.f.w(a2.getAccessToken());
                    this.f.a(System.currentTimeMillis());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Bearer ");
                    sb2.append(a2.getAccessToken());
                    f2.a("Authorization", sb2.toString());
                    f2.a();
                } else {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("Bearer ");
                    if (userAccessToken != null) {
                        sb3.append(userAccessToken);
                        f2.a("Authorization", sb3.toString());
                        f2.a();
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Bearer ");
                if (userAccessToken != null) {
                    sb4.append(userAccessToken);
                    f2.a("Authorization", sb4.toString());
                    f2.a();
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        f2.a(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
        f2.a("User-Agent", yi2.b.a());
        f2.a("locale", this.c.m());
        try {
            Response a3 = chain.a(f2.a());
            kd4.a((Object) a3, "chain.proceed(requestBuilder.build())");
            return a3;
        } catch (Exception e2) {
            if (e2 instanceof ServerErrorException) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = g;
                StringBuilder sb5 = new StringBuilder();
                sb5.append("exception=");
                ServerErrorException serverErrorException = (ServerErrorException) e2;
                sb5.append(serverErrorException.getServerError());
                local4.d(str4, sb5.toString());
                serverError = serverErrorException.getServerError();
            } else if (e2 instanceof UnknownHostException) {
                serverError = new ServerError(601, "");
            } else if (e2 instanceof SocketTimeoutException) {
                serverError = new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, "");
            } else {
                serverError = new ServerError(600, "");
            }
            Response.a aVar = new Response.a();
            aVar.a(f2.a());
            aVar.a(Protocol.HTTP_1_1);
            Integer code = serverError.getCode();
            kd4.a((Object) code, "serverError.code");
            aVar.a(code.intValue());
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            aVar.a(message);
            aVar.a(em4.a(am4.b(com.zendesk.sdk.network.Constants.APPLICATION_JSON), new Gson().a((Object) serverError)));
            aVar.a(GraphRequest.CONTENT_TYPE_HEADER, Constants.CONTENT_TYPE);
            aVar.a("User-Agent", yi2.b.a());
            aVar.a("Locale", this.c.m());
            Response a4 = aVar.a();
            kd4.a((Object) a4, "Response.Builder()\n     \u2026                 .build()");
            return a4;
        }
    }

    @DexIgnore
    public final synchronized Auth a(MFUser mFUser, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.d(str, "getAuth needProcess=" + z + " mCount=" + this.a);
        if (z) {
            xz1 xz1 = new xz1();
            xz1.a("refreshToken", mFUser.getRefreshToken());
            try {
                qr4<Auth> r = this.e.tokenRefresh(xz1).r();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a2 = bp2.h.a();
                local2.d(a2, "Refresh Token error=" + r.b() + " body=" + r.c());
                this.b = r.a();
            } catch (IOException e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str2 = g;
                local3.d(str2, "getAuth needProcess=" + z + " mCount=" + this.a + " exception=" + e2.getMessage());
                e2.printStackTrace();
                return null;
            }
        }
        return this.b;
    }
}
