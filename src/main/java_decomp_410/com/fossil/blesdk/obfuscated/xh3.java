package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.enums.GoalType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class xh3 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[GoalType.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[GoalType.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[GoalType.values().length];

    /*
    static {
        a[GoalType.TOTAL_STEPS.ordinal()] = 1;
        a[GoalType.CALORIES.ordinal()] = 2;
        a[GoalType.ACTIVE_TIME.ordinal()] = 3;
        a[GoalType.GOAL_TRACKING.ordinal()] = 4;
        b[GoalType.CALORIES.ordinal()] = 1;
        b[GoalType.ACTIVE_TIME.ordinal()] = 2;
        b[GoalType.TOTAL_STEPS.ordinal()] = 3;
        b[GoalType.TOTAL_SLEEP.ordinal()] = 4;
        b[GoalType.GOAL_TRACKING.ordinal()] = 5;
        c[GoalType.TOTAL_STEPS.ordinal()] = 1;
        c[GoalType.CALORIES.ordinal()] = 2;
        c[GoalType.ACTIVE_TIME.ordinal()] = 3;
        c[GoalType.TOTAL_SLEEP.ordinal()] = 4;
        c[GoalType.GOAL_TRACKING.ordinal()] = 5;
    }
    */
}
