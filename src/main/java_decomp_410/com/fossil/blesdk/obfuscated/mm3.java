package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.pairing.scanning.PairingActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mm3 extends as2 implements sm3 {
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public tr3<qe2> k;
    @DexIgnore
    public rm3 l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final mm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            mm3 mm3 = new mm3();
            mm3.setArguments(bundle);
            return mm3;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mm3 e;

        @DexIgnore
        public b(mm3 mm3) {
            this.e = mm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            mm3.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mm3 e;

        @DexIgnore
        public c(mm3 mm3) {
            this.e = mm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                HomeActivity.a aVar = HomeActivity.C;
                kd4.a((Object) activity, "fragmentActivity");
                aVar.a(activity);
                activity.finish();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mm3 e;

        @DexIgnore
        public d(mm3 mm3) {
            this.e = mm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mm3 e;

        @DexIgnore
        public e(mm3 mm3) {
            this.e = mm3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                CompatibleModelsActivity.a aVar = CompatibleModelsActivity.B;
                kd4.a((Object) activity, "fragmentActivity");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ rm3 a(mm3 mm3) {
        rm3 rm3 = mm3.l;
        if (rm3 != null) {
            return rm3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        rm3 rm3 = this.l;
        if (rm3 == null) {
            kd4.d("mPresenter");
            throw null;
        } else if (rm3.i()) {
            return false;
        } else {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return false;
            }
            activity.finish();
            return false;
        }
    }

    @DexIgnore
    public void c0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingActivity.a aVar = PairingActivity.D;
            kd4.a((Object) activity, "fragmentActivity");
            rm3 rm3 = this.l;
            if (rm3 != null) {
                aVar.a(activity, rm3.i());
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void g() {
        tr3<qe2> tr3 = this.k;
        if (tr3 != null) {
            qe2 a2 = tr3.a();
            if (a2 != null) {
                DashBar dashBar = a2.u;
                if (dashBar != null) {
                    gs3.a aVar = gs3.a;
                    kd4.a((Object) dashBar, "this");
                    aVar.e(dashBar, this.m, 500);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        qe2 qe2 = (qe2) qa.a(layoutInflater, R.layout.fragment_pairing_instructions, viewGroup, false, O0());
        this.k = new tr3<>(this, qe2);
        kd4.a((Object) qe2, "binding");
        return qe2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        rm3 rm3 = this.l;
        if (rm3 != null) {
            rm3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        rm3 rm3 = this.l;
        if (rm3 != null) {
            rm3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<qe2> tr3 = this.k;
        if (tr3 != null) {
            qe2 a2 = tr3.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.s.setOnClickListener(new d(this));
                a2.t.setOnClickListener(new e(this));
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.m = arguments.getBoolean("IS_ONBOARDING_FLOW");
                rm3 rm3 = this.l;
                if (rm3 != null) {
                    rm3.a(this.m);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void s(boolean z) {
        if (isActive()) {
            tr3<qe2> tr3 = this.k;
            if (tr3 != null) {
                qe2 a2 = tr3.a();
                if (a2 != null) {
                    ImageView imageView = a2.s;
                    if (imageView != null) {
                        kd4.a((Object) imageView, "it");
                        imageView.setVisibility(!z ? 4 : 0);
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(rm3 rm3) {
        kd4.b(rm3, "presenter");
        this.l = rm3;
    }
}
