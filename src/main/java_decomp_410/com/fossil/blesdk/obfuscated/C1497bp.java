package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.bp */
public class C1497bp extends com.fossil.blesdk.obfuscated.C2754qo<java.io.InputStream> {
    @DexIgnore
    public C1497bp(android.content.res.AssetManager assetManager, java.lang.String str) {
        super(assetManager, str);
    }

    @DexIgnore
    public java.lang.Class<java.io.InputStream> getDataClass() {
        return java.io.InputStream.class;
    }

    @DexIgnore
    /* renamed from: a */
    public java.io.InputStream m5051a(android.content.res.AssetManager assetManager, java.lang.String str) throws java.io.IOException {
        return assetManager.open(str);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9243a(java.io.InputStream inputStream) throws java.io.IOException {
        inputStream.close();
    }
}
