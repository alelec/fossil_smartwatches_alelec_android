package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i5 */
public class C1998i5 extends androidx.constraintlayout.solver.widgets.ConstraintWidget {

    @DexIgnore
    /* renamed from: k0 */
    public java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> f5944k0; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: E */
    public void mo1278E() {
        this.f5944k0.clear();
        super.mo1278E();
    }

    @DexIgnore
    /* renamed from: I */
    public void mo1282I() {
        super.mo1282I();
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList = this.f5944k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f5944k0.get(i);
                constraintWidget.mo1305b(mo1328h(), mo1330i());
                if (!(constraintWidget instanceof com.fossil.blesdk.obfuscated.C3342y4)) {
                    constraintWidget.mo1282I();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: K */
    public com.fossil.blesdk.obfuscated.C3342y4 mo11825K() {
        androidx.constraintlayout.solver.widgets.ConstraintWidget l = mo1336l();
        com.fossil.blesdk.obfuscated.C3342y4 y4Var = this instanceof com.fossil.blesdk.obfuscated.C3342y4 ? (com.fossil.blesdk.obfuscated.C3342y4) this : null;
        while (l != null) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget l2 = l.mo1336l();
            if (l instanceof com.fossil.blesdk.obfuscated.C3342y4) {
                y4Var = (com.fossil.blesdk.obfuscated.C3342y4) l;
            }
            l = l2;
        }
        return y4Var;
    }

    @DexIgnore
    /* renamed from: L */
    public void mo11826L() {
        mo1282I();
        java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList = this.f5944k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f5944k0.get(i);
                if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C1998i5) {
                    ((com.fossil.blesdk.obfuscated.C1998i5) constraintWidget).mo11826L();
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: M */
    public void mo11827M() {
        this.f5944k0.clear();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo1296a(com.fossil.blesdk.obfuscated.C2526o4 o4Var) {
        super.mo1296a(o4Var);
        int size = this.f5944k0.size();
        for (int i = 0; i < size; i++) {
            this.f5944k0.get(i).mo1296a(o4Var);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11828b(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        this.f5944k0.add(constraintWidget);
        if (constraintWidget.mo1336l() != null) {
            ((com.fossil.blesdk.obfuscated.C1998i5) constraintWidget.mo1336l()).mo11829c(constraintWidget);
        }
        constraintWidget.mo1294a((androidx.constraintlayout.solver.widgets.ConstraintWidget) this);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11829c(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        this.f5944k0.remove(constraintWidget);
        constraintWidget.mo1294a((androidx.constraintlayout.solver.widgets.ConstraintWidget) null);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo1305b(int i, int i2) {
        super.mo1305b(i, i2);
        int size = this.f5944k0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.f5944k0.get(i3).mo1305b(mo1344p(), mo1346q());
        }
    }
}
