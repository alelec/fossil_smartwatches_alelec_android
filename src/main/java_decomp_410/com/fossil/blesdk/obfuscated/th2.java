package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class th2 extends sh2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.iv_icon, 1);
        x.put(R.id.ftv_title, 2);
        x.put(R.id.ftv_content, 3);
        x.put(R.id.iv_edit_button, 4);
        x.put(R.id.line, 5);
    }
    */

    @DexIgnore
    public th2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 6, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public th2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[0], objArr[3], objArr[2], objArr[4], objArr[1], objArr[5]);
        this.v = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
