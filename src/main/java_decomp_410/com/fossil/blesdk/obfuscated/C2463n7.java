package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n7 */
public final class C2463n7 {
    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2631p7 m11079a(android.content.res.Configuration configuration) {
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            return com.fossil.blesdk.obfuscated.C2631p7.m12117a((java.lang.Object) configuration.getLocales());
        }
        return com.fossil.blesdk.obfuscated.C2631p7.m12118b(configuration.locale);
    }
}
