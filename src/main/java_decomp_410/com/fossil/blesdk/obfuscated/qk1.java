package com.fossil.blesdk.obfuscated;

import android.app.job.JobParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class qk1 implements Runnable {
    @DexIgnore
    public /* final */ nk1 e;
    @DexIgnore
    public /* final */ tg1 f;
    @DexIgnore
    public /* final */ JobParameters g;

    @DexIgnore
    public qk1(nk1 nk1, tg1 tg1, JobParameters jobParameters) {
        this.e = nk1;
        this.f = tg1;
        this.g = jobParameters;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
