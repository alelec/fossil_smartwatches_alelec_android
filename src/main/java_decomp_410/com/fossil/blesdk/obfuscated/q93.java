package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q93 extends zr2 implements p93 {
    @DexIgnore
    public tr3<g92> j;
    @DexIgnore
    public o93 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.e {
        @DexIgnore
        public /* final */ /* synthetic */ q93 a;

        @DexIgnore
        public b(q93 q93) {
            this.a = q93;
        }

        @DexIgnore
        public final void a(Calendar calendar) {
            o93 a2 = this.a.k;
            if (a2 != null) {
                kd4.a((Object) calendar, "calendar");
                Date time = calendar.getTime();
                kd4.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.d {
        @DexIgnore
        public /* final */ /* synthetic */ q93 e;

        @DexIgnore
        public c(q93 q93) {
            this.e = q93;
        }

        @DexIgnore
        public final void a(int i, Calendar calendar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActivityOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.e.getActivity();
            if (activity != null && calendar != null) {
                ActivityDetailActivity.a aVar = ActivityDetailActivity.D;
                Date time = calendar.getTime();
                kd4.a((Object) time, "it.time");
                kd4.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActivityOverviewMonthFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onCreateView");
        g92 g92 = (g92) qa.a(layoutInflater, R.layout.fragment_activity_overview_month, viewGroup, false, O0());
        g92.q.setEndDate(Calendar.getInstance());
        g92.q.setOnCalendarMonthChanged(new b(this));
        g92.q.setOnCalendarItemClickListener(new c(this));
        this.j = new tr3<>(this, g92);
        tr3<g92> tr3 = this.j;
        if (tr3 != null) {
            g92 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onResume");
        o93 o93 = this.k;
        if (o93 != null) {
            o93.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onStop");
        o93 o93 = this.k;
        if (o93 != null) {
            o93.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    public View p(int i) {
        if (this.l == null) {
            this.l = new HashMap();
        }
        View view = (View) this.l.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View view2 = getView();
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i);
        this.l.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    @DexIgnore
    public void a(TreeMap<Long, Float> treeMap) {
        kd4.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        tr3<g92> tr3 = this.j;
        if (tr3 != null) {
            g92 a2 = tr3.a();
            if (a2 != null) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                if (recyclerViewCalendar != null) {
                    recyclerViewCalendar.setTintColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.steps));
                }
            }
        }
        tr3<g92> tr32 = this.j;
        if (tr32 != null) {
            g92 a3 = tr32.a();
            if (a3 != null) {
                RecyclerViewCalendar recyclerViewCalendar2 = a3.q;
                if (recyclerViewCalendar2 != null) {
                    recyclerViewCalendar2.setData(treeMap);
                }
            }
        }
        ((RecyclerViewCalendar) p(g62.calendarMonth)).setEnableButtonNextAndPrevMonth(true);
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        kd4.b(date, "selectDate");
        kd4.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        tr3<g92> tr3 = this.j;
        if (tr3 != null) {
            g92 a2 = tr3.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                kd4.a((Object) instance, "selectCalendar");
                instance.setTime(date);
                kd4.a((Object) instance2, "startCalendar");
                instance2.setTime(rk2.n(date2));
                kd4.a((Object) instance3, "endCalendar");
                instance3.setTime(rk2.i(instance3.getTime()));
                a2.q.a(instance, instance2, instance3);
            }
        }
    }

    @DexIgnore
    public void a(o93 o93) {
        kd4.b(o93, "presenter");
        this.k = o93;
    }
}
