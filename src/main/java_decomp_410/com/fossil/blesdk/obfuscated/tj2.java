package com.fossil.blesdk.obfuscated;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.background.BackgroundImgData;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.RingStyle;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tj2 {
    @DexIgnore
    public static final List<WatchFaceWrapper> a(List<WatchFace> list, List<DianaPresetComplicationSetting> list2) {
        kd4.b(list, "$this$toListOfWatchFaceWrappers");
        ArrayList arrayList = new ArrayList();
        for (WatchFace b : list) {
            arrayList.add(b(b, list2));
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x014d  */
    public static final WatchFaceWrapper b(WatchFace watchFace, List<DianaPresetComplicationSetting> list) {
        int i;
        ArrayList<RingStyleItem> ringStyleItems;
        WatchFaceWrapper.MetaData metaData;
        WatchFaceWrapper.MetaData metaData2;
        WatchFaceWrapper.MetaData metaData3;
        WatchFaceWrapper.MetaData metaData4;
        Drawable drawable;
        Drawable drawable2;
        Drawable drawable3;
        Drawable drawable4;
        kd4.b(watchFace, "$this$toWatchFaceWrapper");
        String id = watchFace.getId();
        Drawable c = k6.c(PortfolioApp.W.c(), R.drawable.nothing_bg);
        Drawable drawable5 = null;
        if (c != null) {
            c.getIntrinsicHeight();
            Drawable c2 = k6.c(PortfolioApp.W.c(), R.drawable.nothing_bg);
            Integer valueOf = c2 != null ? Integer.valueOf(c2.getMinimumWidth()) : null;
            if (valueOf != null) {
                i = valueOf.intValue();
                int dimensionPixelSize = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp130);
                Drawable b = vk2.a.b(watchFace.getPreviewUrl(), dimensionPixelSize, dimensionPixelSize);
                Drawable c3 = vk2.a.c(watchFace.getBackground().getData().getPreviewUrl(), i, i);
                int dimensionPixelSize2 = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp50);
                ringStyleItems = watchFace.getRingStyleItems();
                if (ringStyleItems == null) {
                    Drawable drawable6 = null;
                    Drawable drawable7 = null;
                    Drawable drawable8 = null;
                    WatchFaceWrapper.MetaData metaData5 = null;
                    WatchFaceWrapper.MetaData metaData6 = null;
                    WatchFaceWrapper.MetaData metaData7 = null;
                    WatchFaceWrapper.MetaData metaData8 = null;
                    for (RingStyleItem ringStyleItem : ringStyleItems) {
                        String component1 = ringStyleItem.component1();
                        RingStyle component2 = ringStyleItem.component2();
                        switch (component1.hashCode()) {
                            case -1383228885:
                                if (!component1.equals("bottom")) {
                                    break;
                                } else {
                                    Drawable b2 = vk2.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata = component2.getMetadata();
                                    if (metadata != null) {
                                        metaData7 = a(metadata);
                                    }
                                    drawable7 = b2;
                                    break;
                                }
                            case 115029:
                                if (!component1.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                    break;
                                } else {
                                    Drawable b3 = vk2.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata2 = component2.getMetadata();
                                    if (metadata2 != null) {
                                        metaData5 = a(metadata2);
                                    }
                                    drawable5 = b3;
                                    break;
                                }
                            case 3317767:
                                if (!component1.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                    break;
                                } else {
                                    Drawable b4 = vk2.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata3 = component2.getMetadata();
                                    if (metadata3 != null) {
                                        metaData8 = a(metadata3);
                                    }
                                    drawable8 = b4;
                                    break;
                                }
                            case 108511772:
                                if (!component1.equals("right")) {
                                    break;
                                } else {
                                    Drawable b5 = vk2.a.b(component2.getData().getPreviewUrl(), dimensionPixelSize2, dimensionPixelSize2);
                                    MetaData metadata4 = component2.getMetadata();
                                    if (metadata4 != null) {
                                        metaData6 = a(metadata4);
                                    }
                                    drawable6 = b5;
                                    break;
                                }
                        }
                        WatchFace watchFace2 = watchFace;
                    }
                    drawable4 = drawable5;
                    drawable3 = drawable6;
                    drawable2 = drawable7;
                    drawable = drawable8;
                    metaData4 = metaData5;
                    metaData3 = metaData6;
                    metaData2 = metaData7;
                    metaData = metaData8;
                } else {
                    drawable4 = null;
                    drawable3 = null;
                    drawable2 = null;
                    drawable = null;
                    metaData4 = null;
                    metaData3 = null;
                    metaData2 = null;
                    metaData = null;
                }
                return WatchFaceWrapper.Companion.createBackgroundWrapper(id, b, c3, drawable4, drawable3, drawable2, drawable, metaData4, metaData3, metaData2, metaData, watchFace.getName(), a(watchFace, list));
            }
        }
        i = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp490);
        int dimensionPixelSize3 = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp130);
        Drawable b6 = vk2.a.b(watchFace.getPreviewUrl(), dimensionPixelSize3, dimensionPixelSize3);
        Drawable c32 = vk2.a.c(watchFace.getBackground().getData().getPreviewUrl(), i, i);
        int dimensionPixelSize22 = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp50);
        ringStyleItems = watchFace.getRingStyleItems();
        if (ringStyleItems == null) {
        }
        return WatchFaceWrapper.Companion.createBackgroundWrapper(id, b6, c32, drawable4, drawable3, drawable2, drawable, metaData4, metaData3, metaData2, metaData, watchFace.getName(), a(watchFace, list));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0024, code lost:
        if (r0 != null) goto L_0x0028;
     */
    @DexIgnore
    public static final BackgroundConfig a(WatchFace watchFace, List<DianaPresetComplicationSetting> list) {
        String str;
        String str2;
        String str3;
        String str4;
        T t;
        T t2;
        T t3;
        T t4;
        kd4.b(watchFace, "$this$toBackgroundConfig");
        String url = watchFace.getBackground().getData().getUrl();
        String str5 = "";
        if (url != null) {
            vk2 vk2 = vk2.a;
            String a = vm2.a(url);
            kd4.a((Object) a, "StringHelper.fileNameFromUrl(it)");
            str = vk2.b(a);
        }
        str = str5;
        if (list != null) {
            str4 = str5;
            str3 = str4;
            str2 = str3;
            for (DianaPresetComplicationSetting next : list) {
                String component1 = next.component1();
                String component2 = next.component2();
                RingStyleItem ringStyleItem = null;
                switch (component1.hashCode()) {
                    case -1383228885:
                        if (component1.equals("bottom") && (!kd4.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems = watchFace.getRingStyleItems();
                            if (ringStyleItems != null) {
                                Iterator<T> it = ringStyleItems.iterator();
                                while (true) {
                                    if (it.hasNext()) {
                                        t = it.next();
                                        if (kd4.a((Object) ((RingStyleItem) t).getPosition(), (Object) "bottom")) {
                                        }
                                    } else {
                                        t = null;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                vk2 vk22 = vk2.a;
                                String a2 = vm2.a(ringStyleItem.getRingStyle().getData().getUrl());
                                kd4.a((Object) a2, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                str3 = vk22.b(a2);
                                break;
                            }
                        }
                    case 115029:
                        if (component1.equals(ViewHierarchy.DIMENSION_TOP_KEY) && (!kd4.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems2 = watchFace.getRingStyleItems();
                            if (ringStyleItems2 != null) {
                                Iterator<T> it2 = ringStyleItems2.iterator();
                                while (true) {
                                    if (it2.hasNext()) {
                                        t2 = it2.next();
                                        if (kd4.a((Object) ((RingStyleItem) t2).getPosition(), (Object) ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        }
                                    } else {
                                        t2 = null;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t2;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                vk2 vk23 = vk2.a;
                                String a3 = vm2.a(ringStyleItem.getRingStyle().getData().getUrl());
                                kd4.a((Object) a3, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                str5 = vk23.b(a3);
                                break;
                            }
                        }
                    case 3317767:
                        if (component1.equals(ViewHierarchy.DIMENSION_LEFT_KEY) && (!kd4.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems3 = watchFace.getRingStyleItems();
                            if (ringStyleItems3 != null) {
                                Iterator<T> it3 = ringStyleItems3.iterator();
                                while (true) {
                                    if (it3.hasNext()) {
                                        t3 = it3.next();
                                        if (kd4.a((Object) ((RingStyleItem) t3).getPosition(), (Object) ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                        }
                                    } else {
                                        t3 = null;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t3;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                vk2 vk24 = vk2.a;
                                String a4 = vm2.a(ringStyleItem.getRingStyle().getData().getUrl());
                                kd4.a((Object) a4, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                str2 = vk24.b(a4);
                                break;
                            }
                        }
                    case 108511772:
                        if (component1.equals("right") && (!kd4.a((Object) component2, (Object) "empty"))) {
                            ArrayList<RingStyleItem> ringStyleItems4 = watchFace.getRingStyleItems();
                            if (ringStyleItems4 != null) {
                                Iterator<T> it4 = ringStyleItems4.iterator();
                                while (true) {
                                    if (it4.hasNext()) {
                                        t4 = it4.next();
                                        if (kd4.a((Object) ((RingStyleItem) t4).getPosition(), (Object) "right")) {
                                        }
                                    } else {
                                        t4 = null;
                                    }
                                }
                                ringStyleItem = (RingStyleItem) t4;
                            }
                            if (ringStyleItem == null) {
                                break;
                            } else {
                                vk2 vk25 = vk2.a;
                                String a5 = vm2.a(ringStyleItem.getRingStyle().getData().getUrl());
                                kd4.a((Object) a5, "StringHelper.fileNameFro\u2026Style.ringStyle.data.url)");
                                str4 = vk25.b(a5);
                                break;
                            }
                        }
                }
            }
        } else {
            str4 = str5;
            str3 = str4;
            str2 = str3;
        }
        return new BackgroundConfig(System.currentTimeMillis(), new BackgroundImgData("main_bg", str), new BackgroundImgData("top_bg", str5), new BackgroundImgData("right_bg", str4), new BackgroundImgData("bottom_bg", str3), new BackgroundImgData("left_bg", str2));
    }

    @DexIgnore
    public static final WatchFaceWrapper.MetaData a(MetaData metaData) {
        Integer num;
        Integer num2;
        Integer num3;
        kd4.b(metaData, "$this$toColorMetaData");
        Integer num4 = null;
        try {
            num3 = Integer.valueOf(Color.parseColor(metaData.getSelectedForegroundColor()));
            try {
                num2 = Integer.valueOf(Color.parseColor(metaData.getSelectedBackgroundColor()));
                try {
                    num = Integer.valueOf(Color.parseColor(metaData.getUnselectedForegroundColor()));
                } catch (Exception e) {
                    e = e;
                    num = null;
                    e.printStackTrace();
                    return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
                }
                try {
                    num4 = Integer.valueOf(Color.parseColor(metaData.getUnselectedBackgroundColor()));
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
                }
            } catch (Exception e3) {
                e = e3;
                num2 = null;
                num = num2;
                e.printStackTrace();
                return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
            }
        } catch (Exception e4) {
            e = e4;
            num3 = null;
            num2 = null;
            num = num2;
            e.printStackTrace();
            return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
        }
        return new WatchFaceWrapper.MetaData(num3, num2, num, num4);
    }
}
