package com.fossil.blesdk.obfuscated;

import android.os.Binder;
import android.os.Process;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class or0 extends Binder {
    @DexIgnore
    public /* final */ kr0 e;

    @DexIgnore
    public or0(kr0 kr0) {
        this.e = kr0;
    }

    @DexIgnore
    public final void a(mr0 mr0) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "service received new intent via bind strategy");
            }
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "intent being queued for bg execution");
            }
            this.e.e.execute(new pr0(this, mr0));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
