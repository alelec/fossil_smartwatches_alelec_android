package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import com.fossil.blesdk.obfuscated.hg;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tf extends hg.a {
    @DexIgnore
    public jf b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public a(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(gg ggVar);

        @DexIgnore
        public abstract void dropAllTables(gg ggVar);

        @DexIgnore
        public abstract void onCreate(gg ggVar);

        @DexIgnore
        public abstract void onOpen(gg ggVar);

        @DexIgnore
        public abstract void onPostMigrate(gg ggVar);

        @DexIgnore
        public abstract void onPreMigrate(gg ggVar);

        @DexIgnore
        public abstract void validateMigration(gg ggVar);
    }

    @DexIgnore
    public tf(jf jfVar, a aVar, String str, String str2) {
        super(aVar.version);
        this.b = jfVar;
        this.c = aVar;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public static boolean h(gg ggVar) {
        Cursor d2 = ggVar.d("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            boolean z = false;
            if (d2.moveToFirst() && d2.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            d2.close();
        }
    }

    @DexIgnore
    public void a(gg ggVar) {
        super.a(ggVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public void b(gg ggVar, int i, int i2) {
        boolean z;
        jf jfVar = this.b;
        if (jfVar != null) {
            List<yf> a2 = jfVar.d.a(i, i2);
            if (a2 != null) {
                this.c.onPreMigrate(ggVar);
                for (yf migrate : a2) {
                    migrate.migrate(ggVar);
                }
                this.c.validateMigration(ggVar);
                this.c.onPostMigrate(ggVar);
                g(ggVar);
                z = true;
                if (z) {
                    jf jfVar2 = this.b;
                    if (jfVar2 == null || jfVar2.a(i, i2)) {
                        throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
                    }
                    this.c.dropAllTables(ggVar);
                    this.c.createAllTables(ggVar);
                    return;
                }
                return;
            }
        }
        z = false;
        if (z) {
        }
    }

    @DexIgnore
    public void c(gg ggVar) {
        g(ggVar);
        this.c.createAllTables(ggVar);
        this.c.onCreate(ggVar);
    }

    @DexIgnore
    public void d(gg ggVar) {
        super.d(ggVar);
        e(ggVar);
        this.c.onOpen(ggVar);
        this.b = null;
    }

    @DexIgnore
    public final void e(gg ggVar) {
        String str = null;
        if (h(ggVar)) {
            Cursor a2 = ggVar.a(new fg("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (a2.moveToFirst()) {
                    str = a2.getString(0);
                }
            } finally {
                a2.close();
            }
        }
        if (!this.d.equals(str) && !this.e.equals(str)) {
            throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
        }
    }

    @DexIgnore
    public final void f(gg ggVar) {
        ggVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    public final void g(gg ggVar) {
        f(ggVar);
        ggVar.b(sf.a(this.d));
    }

    @DexIgnore
    public void a(gg ggVar, int i, int i2) {
        b(ggVar, i, i2);
    }
}
