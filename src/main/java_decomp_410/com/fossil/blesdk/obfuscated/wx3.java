package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.fossil.blesdk.obfuscated.iy3;
import com.squareup.picasso.Picasso;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wx3 extends iy3 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public wx3(Context context) {
        this.a = context;
    }

    @DexIgnore
    public boolean a(gy3 gy3) {
        return "content".equals(gy3.d.getScheme());
    }

    @DexIgnore
    public InputStream c(gy3 gy3) throws FileNotFoundException {
        return this.a.getContentResolver().openInputStream(gy3.d);
    }

    @DexIgnore
    public iy3.a a(gy3 gy3, int i) throws IOException {
        return new iy3.a(c(gy3), Picasso.LoadedFrom.DISK);
    }
}
