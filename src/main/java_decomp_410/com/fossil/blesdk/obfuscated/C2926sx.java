package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.sx */
public class C2926sx {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f9483a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Bundle f9484b;

    @DexIgnore
    public C2926sx(java.lang.String str, android.os.Bundle bundle) {
        this.f9483a = str;
        this.f9484b = bundle;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo16215a() {
        return this.f9483a;
    }

    @DexIgnore
    /* renamed from: b */
    public android.os.Bundle mo16216b() {
        return this.f9484b;
    }
}
