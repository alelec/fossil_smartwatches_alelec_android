package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.view.accessibility.AccessibilityManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p9 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void onTouchExplorationStateChanged(boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements AccessibilityManager.TouchExplorationStateChangeListener {
        @DexIgnore
        public /* final */ a a;

        @DexIgnore
        public b(a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            return this.a.equals(((b) obj).a);
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public void onTouchExplorationStateChanged(boolean z) {
            this.a.onTouchExplorationStateChanged(z);
        }
    }

    @DexIgnore
    public static boolean a(AccessibilityManager accessibilityManager, a aVar) {
        if (Build.VERSION.SDK_INT < 19 || aVar == null) {
            return false;
        }
        return accessibilityManager.addTouchExplorationStateChangeListener(new b(aVar));
    }

    @DexIgnore
    public static boolean b(AccessibilityManager accessibilityManager, a aVar) {
        if (Build.VERSION.SDK_INT < 19 || aVar == null) {
            return false;
        }
        return accessibilityManager.removeTouchExplorationStateChangeListener(new b(aVar));
    }
}
