package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v12 {
    @DexIgnore
    public static /* final */ int[] a; // = {4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};

    @DexIgnore
    public static int a(int i, boolean z) {
        return ((z ? 88 : 112) + (i << 4)) * i;
    }

    @DexIgnore
    public static t12 a(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        boolean z;
        a22 a22;
        int i6;
        a22 a2 = new w12(bArr).a();
        int i7 = 11;
        int a3 = ((a2.a() * i) / 100) + 11;
        int a4 = a2.a() + a3;
        int i8 = 32;
        int i9 = 0;
        int i10 = 1;
        if (i2 != 0) {
            z = i2 < 0;
            i4 = Math.abs(i2);
            if (z) {
                i8 = 4;
            }
            if (i4 <= i8) {
                i5 = a(i4, z);
                i3 = a[i4];
                int i11 = i5 - (i5 % i3);
                a22 = a(a2, i3);
                if (a22.a() + a3 > i11) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                } else if (z && a22.a() > (i3 << 6)) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                }
            } else {
                throw new IllegalArgumentException(String.format("Illegal value %s for layers", new Object[]{Integer.valueOf(i2)}));
            }
        } else {
            a22 a222 = null;
            int i12 = 0;
            i3 = 0;
            while (i12 <= 32) {
                boolean z2 = i12 <= 3;
                int i13 = z2 ? i12 + 1 : i12;
                int a5 = a(i13, z2);
                if (a4 <= a5) {
                    int[] iArr = a;
                    if (i3 != iArr[i13]) {
                        i3 = iArr[i13];
                        a222 = a(a2, i3);
                    }
                    int i14 = a5 - (a5 % i3);
                    if ((!z2 || a222.a() <= (i3 << 6)) && a222.a() + a3 <= i14) {
                        a22 = a222;
                        z = z2;
                        i4 = i13;
                        i5 = a5;
                    }
                }
                i12++;
                i9 = 0;
                i10 = 1;
            }
            throw new IllegalArgumentException("Data too large for an Aztec code");
        }
        a22 b = b(a22, i5, i3);
        int a6 = a22.a() / i3;
        a22 a7 = a(z, i4, a6);
        if (!z) {
            i7 = 14;
        }
        int i15 = i7 + (i4 << 2);
        int[] iArr2 = new int[i15];
        int i16 = 2;
        if (z) {
            for (int i17 = 0; i17 < iArr2.length; i17++) {
                iArr2[i17] = i17;
            }
            i6 = i15;
        } else {
            int i18 = i15 / 2;
            i6 = i15 + 1 + (((i18 - 1) / 15) * 2);
            int i19 = i6 / 2;
            for (int i20 = 0; i20 < i18; i20++) {
                int i21 = (i20 / 15) + i20;
                iArr2[(i18 - i20) - i10] = (i19 - i21) - 1;
                iArr2[i18 + i20] = i21 + i19 + i10;
            }
        }
        b22 b22 = new b22(i6);
        int i22 = 0;
        int i23 = 0;
        while (i22 < i4) {
            int i24 = ((i4 - i22) << i16) + (z ? 9 : 12);
            int i25 = 0;
            while (i25 < i24) {
                int i26 = i25 << 1;
                while (i9 < i16) {
                    if (b.b(i23 + i26 + i9)) {
                        int i27 = i22 << 1;
                        b22.b(iArr2[i27 + i9], iArr2[i27 + i25]);
                    }
                    if (b.b((i24 << 1) + i23 + i26 + i9)) {
                        int i28 = i22 << 1;
                        b22.b(iArr2[i28 + i25], iArr2[((i15 - 1) - i28) - i9]);
                    }
                    if (b.b((i24 << 2) + i23 + i26 + i9)) {
                        int i29 = (i15 - 1) - (i22 << 1);
                        b22.b(iArr2[i29 - i9], iArr2[i29 - i25]);
                    }
                    if (b.b((i24 * 6) + i23 + i26 + i9)) {
                        int i30 = i22 << 1;
                        b22.b(iArr2[((i15 - 1) - i30) - i25], iArr2[i30 + i9]);
                    }
                    i9++;
                    i16 = 2;
                }
                i25++;
                i9 = 0;
                i16 = 2;
            }
            i23 += i24 << 3;
            i22++;
            i9 = 0;
            i16 = 2;
        }
        a(b22, z, i6, a7);
        if (z) {
            a(b22, i6 / 2, 5);
        } else {
            int i31 = i6 / 2;
            a(b22, i31, 7);
            int i32 = 0;
            int i33 = 0;
            while (i32 < (i15 / 2) - 1) {
                for (int i34 = i31 & 1; i34 < i6; i34 += 2) {
                    int i35 = i31 - i33;
                    b22.b(i35, i34);
                    int i36 = i31 + i33;
                    b22.b(i36, i34);
                    b22.b(i34, i35);
                    b22.b(i34, i36);
                }
                i32 += 15;
                i33 += 16;
            }
        }
        t12 t12 = new t12();
        t12.a(z);
        t12.c(i6);
        t12.b(i4);
        t12.a(a6);
        t12.a(b22);
        return t12;
    }

    @DexIgnore
    public static a22 b(a22 a22, int i, int i2) {
        e22 e22 = new e22(a(i2));
        int i3 = i / i2;
        int[] a2 = a(a22, i2, i3);
        e22.a(a2, i3 - (a22.a() / i2));
        a22 a222 = new a22();
        a222.a(0, i % i2);
        for (int a3 : a2) {
            a222.a(a3, i2);
        }
        return a222;
    }

    @DexIgnore
    public static void a(b22 b22, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3 += 2) {
            int i4 = i - i3;
            int i5 = i4;
            while (true) {
                int i6 = i + i3;
                if (i5 > i6) {
                    break;
                }
                b22.b(i5, i4);
                b22.b(i5, i6);
                b22.b(i4, i5);
                b22.b(i6, i5);
                i5++;
            }
        }
        int i7 = i - i2;
        b22.b(i7, i7);
        int i8 = i7 + 1;
        b22.b(i8, i7);
        b22.b(i7, i8);
        int i9 = i + i2;
        b22.b(i9, i7);
        b22.b(i9, i8);
        b22.b(i9, i9 - 1);
    }

    @DexIgnore
    public static a22 a(boolean z, int i, int i2) {
        a22 a22 = new a22();
        if (z) {
            a22.a(i - 1, 2);
            a22.a(i2 - 1, 6);
            return b(a22, 28, 4);
        }
        a22.a(i - 1, 5);
        a22.a(i2 - 1, 11);
        return b(a22, 40, 4);
    }

    @DexIgnore
    public static void a(b22 b22, boolean z, int i, a22 a22) {
        int i2 = i / 2;
        int i3 = 0;
        if (z) {
            while (i3 < 7) {
                int i4 = (i2 - 3) + i3;
                if (a22.b(i3)) {
                    b22.b(i4, i2 - 5);
                }
                if (a22.b(i3 + 7)) {
                    b22.b(i2 + 5, i4);
                }
                if (a22.b(20 - i3)) {
                    b22.b(i4, i2 + 5);
                }
                if (a22.b(27 - i3)) {
                    b22.b(i2 - 5, i4);
                }
                i3++;
            }
            return;
        }
        while (i3 < 10) {
            int i5 = (i2 - 5) + i3 + (i3 / 5);
            if (a22.b(i3)) {
                b22.b(i5, i2 - 7);
            }
            if (a22.b(i3 + 10)) {
                b22.b(i2 + 7, i5);
            }
            if (a22.b(29 - i3)) {
                b22.b(i5, i2 + 7);
            }
            if (a22.b(39 - i3)) {
                b22.b(i2 - 7, i5);
            }
            i3++;
        }
    }

    @DexIgnore
    public static int[] a(a22 a22, int i, int i2) {
        int[] iArr = new int[i2];
        int a2 = a22.a() / i;
        for (int i3 = 0; i3 < a2; i3++) {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                i4 |= a22.b((i3 * i) + i5) ? 1 << ((i - i5) - 1) : 0;
            }
            iArr[i3] = i4;
        }
        return iArr;
    }

    @DexIgnore
    public static c22 a(int i) {
        if (i == 4) {
            return c22.j;
        }
        if (i == 6) {
            return c22.i;
        }
        if (i == 8) {
            return c22.l;
        }
        if (i == 10) {
            return c22.h;
        }
        if (i == 12) {
            return c22.g;
        }
        throw new IllegalArgumentException("Unsupported word size " + i);
    }

    @DexIgnore
    public static a22 a(a22 a22, int i) {
        a22 a222 = new a22();
        int a2 = a22.a();
        int i2 = (1 << i) - 2;
        int i3 = 0;
        while (i3 < a2) {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                int i6 = i3 + i5;
                if (i6 >= a2 || a22.b(i6)) {
                    i4 |= 1 << ((i - 1) - i5);
                }
            }
            int i7 = i4 & i2;
            if (i7 == i2) {
                a222.a(i7, i);
            } else if (i7 == 0) {
                a222.a(i4 | 1, i);
            } else {
                a222.a(i4, i);
                i3 += i;
            }
            i3--;
            i3 += i;
        }
        return a222;
    }
}
