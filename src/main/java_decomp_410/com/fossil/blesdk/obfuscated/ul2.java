package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ul2 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public AnalyticsHelper c;
    @DexIgnore
    public Map<String, String> d; // = new HashMap();
    @DexIgnore
    public Map<String, Object> e; // = new HashMap();
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public b h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void L0();

        @DexIgnore
        void M0();
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ul2(AnalyticsHelper analyticsHelper, String str, String str2) {
        kd4.b(analyticsHelper, "analyticsHelper");
        kd4.b(str, "traceName");
        this.c = analyticsHelper;
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (str2 != null) {
            valueOf = str2 + "_" + valueOf;
        }
        this.a = valueOf;
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final boolean b() {
        return this.f;
    }

    @DexIgnore
    public final void c() {
        this.h = null;
    }

    @DexIgnore
    public final void d() {
        if (this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is already started");
            return;
        }
        this.f = true;
        Map<String, String> map = this.d;
        if (map != null) {
            String put = map.put("trace_id", this.a);
        }
        AnalyticsHelper analyticsHelper = this.c;
        if (analyticsHelper != null) {
            analyticsHelper.a(this.b + "_start", (Map<String, ? extends Object>) this.d);
        }
        Map<String, String> map2 = this.d;
        if (map2 != null) {
            map2.clear();
        }
        this.g = System.currentTimeMillis();
        b bVar = this.h;
        if (bVar != null) {
            bVar.L0();
        }
    }

    @DexIgnore
    public String toString() {
        return "Tracer name: " + this.b + ", id: " + this.a + ", running: " + this.f;
    }

    @DexIgnore
    public final ul2 a(String str, String str2) {
        kd4.b(str, "paramName");
        kd4.b(str2, "paramValue");
        Map<String, String> map = this.d;
        if (map != null) {
            String put = map.put(str, str2);
        }
        return this;
    }

    @DexIgnore
    public final ul2 a(b bVar) {
        kd4.b(bVar, "tracingCallback");
        this.h = bVar;
        return this;
    }

    @DexIgnore
    public final void a(sl2 sl2) {
        kd4.b(sl2, "analyticsEvent");
        sl2.a("trace_id", this.a);
        sl2.a();
    }

    @DexIgnore
    public final void a(String str) {
        kd4.b(str, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.g;
            Map<String, Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) currentTimeMillis));
                map.put(Constants.RESULT, str);
                AnalyticsHelper analyticsHelper = this.c;
                if (analyticsHelper != null) {
                    analyticsHelper.a(this.b + "_end", (Map<String, ? extends Object>) map);
                }
            }
        }
        b bVar = this.h;
        if (bVar != null) {
            bVar.M0();
        }
    }

    @DexIgnore
    public final void a(boolean z, String str) {
        kd4.b(str, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.g;
            Map<String, Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) currentTimeMillis));
                map.put("cache_hit", Integer.valueOf(z ? 1 : 0));
                map.put(Constants.RESULT, str);
                AnalyticsHelper analyticsHelper = this.c;
                if (analyticsHelper != null) {
                    analyticsHelper.a(this.b + "_end", (Map<String, ? extends Object>) map);
                }
            }
        }
        b bVar = this.h;
        if (bVar != null) {
            bVar.M0();
        }
    }
}
