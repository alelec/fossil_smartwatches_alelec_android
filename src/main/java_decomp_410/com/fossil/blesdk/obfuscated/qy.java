package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qy extends FileOutputStream {
    @DexIgnore
    public static /* final */ FilenameFilter h; // = new a();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public File f;
    @DexIgnore
    public boolean g; // = false;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.endsWith(".cls_temp");
        }
    }

    @DexIgnore
    public qy(File file, String str) throws FileNotFoundException {
        super(new File(file, str + ".cls_temp"));
        this.e = file + File.separator + str;
        StringBuilder sb = new StringBuilder();
        sb.append(this.e);
        sb.append(".cls_temp");
        this.f = new File(sb.toString());
    }

    @DexIgnore
    public synchronized void close() throws IOException {
        if (!this.g) {
            this.g = true;
            super.flush();
            super.close();
            File file = new File(this.e + ".cls");
            if (this.f.renameTo(file)) {
                this.f = null;
                return;
            }
            String str = "";
            if (file.exists()) {
                str = " (target already exists)";
            } else if (!this.f.exists()) {
                str = " (source does not exist)";
            }
            throw new IOException("Could not rename temp file: " + this.f + " -> " + file + str);
        }
    }

    @DexIgnore
    public void y() throws IOException {
        if (!this.g) {
            this.g = true;
            super.flush();
            super.close();
        }
    }
}
