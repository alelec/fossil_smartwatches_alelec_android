package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vf3 extends u52 {
    @DexIgnore
    public abstract void a(Bundle bundle);

    @DexIgnore
    public abstract void a(Date date);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();
}
