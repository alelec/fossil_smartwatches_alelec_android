package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c */
public interface C1521c extends android.os.IInterface {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c$a")
    /* renamed from: com.fossil.blesdk.obfuscated.c$a */
    public static abstract class C1522a extends android.os.Binder implements com.fossil.blesdk.obfuscated.C1521c {
        @DexIgnore
        public C1522a() {
            attachInterface(this, "android.support.customtabs.IPostMessageService");
        }

        @DexIgnore
        public android.os.IBinder asBinder() {
            return this;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v1, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: android.os.Bundle} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: android.os.Bundle} */
        /* JADX WARNING: Multi-variable type inference failed */
        public boolean onTransact(int i, android.os.Parcel parcel, android.os.Parcel parcel2, int i2) throws android.os.RemoteException {
            android.os.Bundle bundle = null;
            if (i == 2) {
                parcel.enforceInterface("android.support.customtabs.IPostMessageService");
                com.fossil.blesdk.obfuscated.C1365a a = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                if (parcel.readInt() != 0) {
                    bundle = android.os.Bundle.CREATOR.createFromParcel(parcel);
                }
                mo9352a(a, bundle);
                parcel2.writeNoException();
                return true;
            } else if (i == 3) {
                parcel.enforceInterface("android.support.customtabs.IPostMessageService");
                com.fossil.blesdk.obfuscated.C1365a a2 = com.fossil.blesdk.obfuscated.C1365a.C1366a.m4085a(parcel.readStrongBinder());
                java.lang.String readString = parcel.readString();
                if (parcel.readInt() != 0) {
                    bundle = android.os.Bundle.CREATOR.createFromParcel(parcel);
                }
                mo9353a(a2, readString, bundle);
                parcel2.writeNoException();
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.customtabs.IPostMessageService");
                return true;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    void mo9352a(com.fossil.blesdk.obfuscated.C1365a aVar, android.os.Bundle bundle) throws android.os.RemoteException;

    @DexIgnore
    /* renamed from: a */
    void mo9353a(com.fossil.blesdk.obfuscated.C1365a aVar, java.lang.String str, android.os.Bundle bundle) throws android.os.RemoteException;
}
