package com.fossil.blesdk.obfuscated;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cl0 extends mj0 {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ ye0 f;
    @DexIgnore
    public /* final */ /* synthetic */ int g;

    @DexIgnore
    public cl0(Intent intent, ye0 ye0, int i) {
        this.e = intent;
        this.f = ye0;
        this.g = i;
    }

    @DexIgnore
    public final void a() {
        Intent intent = this.e;
        if (intent != null) {
            this.f.startActivityForResult(intent, this.g);
        }
    }
}
