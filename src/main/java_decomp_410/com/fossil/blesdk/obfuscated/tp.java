package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tp implements jo {
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Class<?> e;
    @DexIgnore
    public /* final */ Class<?> f;
    @DexIgnore
    public /* final */ jo g;
    @DexIgnore
    public /* final */ Map<Class<?>, oo<?>> h;
    @DexIgnore
    public /* final */ lo i;
    @DexIgnore
    public int j;

    @DexIgnore
    public tp(Object obj, jo joVar, int i2, int i3, Map<Class<?>, oo<?>> map, Class<?> cls, Class<?> cls2, lo loVar) {
        tw.a(obj);
        this.b = obj;
        tw.a(joVar, "Signature must not be null");
        this.g = joVar;
        this.c = i2;
        this.d = i3;
        tw.a(map);
        this.h = map;
        tw.a(cls, "Resource class must not be null");
        this.e = cls;
        tw.a(cls2, "Transcode class must not be null");
        this.f = cls2;
        tw.a(loVar);
        this.i = loVar;
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof tp)) {
            return false;
        }
        tp tpVar = (tp) obj;
        if (!this.b.equals(tpVar.b) || !this.g.equals(tpVar.g) || this.d != tpVar.d || this.c != tpVar.c || !this.h.equals(tpVar.h) || !this.e.equals(tpVar.e) || !this.f.equals(tpVar.f) || !this.i.equals(tpVar.i)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        if (this.j == 0) {
            this.j = this.b.hashCode();
            this.j = (this.j * 31) + this.g.hashCode();
            this.j = (this.j * 31) + this.c;
            this.j = (this.j * 31) + this.d;
            this.j = (this.j * 31) + this.h.hashCode();
            this.j = (this.j * 31) + this.e.hashCode();
            this.j = (this.j * 31) + this.f.hashCode();
            this.j = (this.j * 31) + this.i.hashCode();
        }
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
