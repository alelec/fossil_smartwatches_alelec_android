package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tp2<T, A> {
    @DexIgnore
    public xc4<? super A, ? extends T> a;
    @DexIgnore
    public volatile T b;

    @DexIgnore
    public tp2(xc4<? super A, ? extends T> xc4) {
        kd4.b(xc4, "creator");
        this.a = xc4;
    }

    @DexIgnore
    public final T a(A a2) {
        T t;
        T t2 = this.b;
        if (t2 != null) {
            return t2;
        }
        synchronized (this) {
            t = this.b;
            if (t == null) {
                xc4 xc4 = this.a;
                if (xc4 != null) {
                    t = xc4.invoke(a2);
                    this.b = t;
                    this.a = null;
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        return t;
    }
}
