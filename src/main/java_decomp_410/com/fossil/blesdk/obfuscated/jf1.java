package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jf1 {
    @DexIgnore
    public /* final */ g51 a;

    @DexIgnore
    public jf1(g51 g51) {
        bk0.a(g51);
        this.a = g51;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof jf1)) {
            return false;
        }
        try {
            return this.a.a(((jf1) obj).a);
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.k();
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
