package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n04 extends o04 {
    @DexIgnore
    public String m;
    @DexIgnore
    public int n;
    @DexIgnore
    public Thread o; // = null;

    @DexIgnore
    public n04(Context context, int i, int i2, Throwable th, k04 k04) {
        super(context, i, k04);
        a(i2, th);
    }

    @DexIgnore
    public n04(Context context, int i, int i2, Throwable th, Thread thread, k04 k04) {
        super(context, i, k04);
        a(i2, th);
        this.o = thread;
    }

    @DexIgnore
    public f a() {
        return f.ERROR;
    }

    @DexIgnore
    public final void a(int i, Throwable th) {
        if (th != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            this.m = stringWriter.toString();
            this.n = i;
            printWriter.close();
        }
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        j24.a(jSONObject, "er", this.m);
        jSONObject.put("ea", this.n);
        int i = this.n;
        if (i != 2 && i != 3) {
            return true;
        }
        new v14(this.j).a(jSONObject, this.o);
        return true;
    }
}
