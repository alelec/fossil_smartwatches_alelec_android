package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.blesdk.obfuscated.m62;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tv2 extends as2 implements sv2, ks2 {
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<sc2> k;
    @DexIgnore
    public rv2 l;
    @DexIgnore
    public m62 m;
    @DexIgnore
    public pv2 n;
    @DexIgnore
    public DoNotDisturbScheduledTimePresenter o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final tv2 a() {
            return new tv2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m62.b {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 a;

        @DexIgnore
        public b(tv2 tv2) {
            this.a = tv2;
        }

        @DexIgnore
        public void a(Alarm alarm) {
            kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            tv2.b(this.a).a(alarm, !alarm.isActive());
        }

        @DexIgnore
        public void b(Alarm alarm) {
            kd4.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            tv2.b(this.a).a(alarm);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public c(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
                kd4.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, 2, (Object) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public d(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            tv2.b(this.e).a((Alarm) null);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public e(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersActivity.C.a(this.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public f(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ts3.a(view);
            NotificationAppsActivity.C.a(this.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public g(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (bn2.a(bn2.d, this.e.getContext(), "NOTIFICATION_CONTACTS", false, 4, (Object) null)) {
                NotificationCallsAndMessagesActivity.C.a(this.e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public h(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            tv2.b(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public i(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            pv2 a = this.e.n;
            if (a != null) {
                a.p(0);
            }
            pv2 a2 = this.e.n;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, pv2.r.a());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tv2 e;

        @DexIgnore
        public j(tv2 tv2) {
            this.e = tv2;
        }

        @DexIgnore
        public final void onClick(View view) {
            pv2 a = this.e.n;
            if (a != null) {
                a.p(1);
            }
            pv2 a2 = this.e.n;
            if (a2 != null) {
                FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                kd4.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, pv2.r.a());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ rv2 b(tv2 tv2) {
        rv2 rv2 = tv2.l;
        if (rv2 != null) {
            return rv2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (z) {
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        vl2 Q02 = Q0();
        if (Q02 != null) {
            Q02.a("");
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "HomeAlertsFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public void d(List<Alarm> list) {
        kd4.b(list, "alarms");
        m62 m62 = this.m;
        if (m62 != null) {
            m62.a(list);
        } else {
            kd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void h(String str) {
        kd4.b(str, "notificationAppOverView");
        tr3<sc2> tr3 = this.k;
        if (tr3 != null) {
            sc2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void m(boolean z) {
        Context context;
        int i2;
        tr3<sc2> tr3 = this.k;
        if (tr3 != null) {
            sc2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.E;
                kd4.a((Object) switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                ConstraintLayout constraintLayout = a2.r;
                kd4.a((Object) constraintLayout, "it.clScheduledTimeContainer");
                constraintLayout.setVisibility(z ? 0 : 8);
                FlexibleTextView flexibleTextView = a2.u;
                if (z) {
                    context = getContext();
                    if (context != null) {
                        i2 = R.color.primaryText;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    context = getContext();
                    if (context != null) {
                        i2 = R.color.warmGrey;
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                flexibleTextView.setTextColor(k6.a(context, i2));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        sc2 sc2 = (sc2) qa.a(layoutInflater, R.layout.fragment_home_alerts, viewGroup, false, O0());
        this.n = (pv2) getChildFragmentManager().a(pv2.r.a());
        if (this.n == null) {
            this.n = pv2.r.b();
        }
        sc2.s.setOnClickListener(new d(this));
        sc2.C.setOnClickListener(new e(this));
        sc2.y.setOnClickListener(new f(this));
        sc2.z.setOnClickListener(new g(this));
        sc2.E.setOnClickListener(new h(this));
        sc2.B.setOnClickListener(new i(this));
        sc2.A.setOnClickListener(new j(this));
        ui2 ui2 = sc2.x;
        if (ui2 != null) {
            ConstraintLayout constraintLayout = ui2.q;
            kd4.a((Object) constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            ui2.t.setImageResource(R.drawable.alerts_no_device);
            FlexibleTextView flexibleTextView = ui2.r;
            kd4.a((Object) flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(sm2.a(getContext(), (int) R.string.Onboarding_WithoutDevice_Alerts_Text__SetAlarmsAndGetNotificationUpdates));
            ui2.s.setOnClickListener(new c(this));
        }
        m62 m62 = new m62();
        m62.a((m62.b) new b(this));
        this.m = m62;
        RecyclerView recyclerView = sc2.D;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        m62 m622 = this.m;
        if (m622 != null) {
            recyclerView.setAdapter(m622);
            this.k = new tr3<>(this, sc2);
            l42 g2 = PortfolioApp.W.c().g();
            pv2 pv2 = this.n;
            if (pv2 != null) {
                g2.a(new vv2(pv2)).a(this);
                tr3<sc2> tr3 = this.k;
                if (tr3 != null) {
                    sc2 a2 = tr3.a();
                    if (a2 != null) {
                        kd4.a((Object) a2, "mBinding.get()!!");
                        return a2.d();
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimeContract.View");
        }
        kd4.d("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        rv2 rv2 = this.l;
        if (rv2 != null) {
            if (rv2 != null) {
                rv2.g();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
        super.onPause();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        rv2 rv2 = this.l;
        if (rv2 != null) {
            if (rv2 != null) {
                rv2.f();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.d();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("alert_view");
    }

    @DexIgnore
    public void r() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.y(childFragmentManager);
        }
    }

    @DexIgnore
    public void s() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "notifyListAlarm()");
        m62 m62 = this.m;
        if (m62 != null) {
            m62.notifyDataSetChanged();
        } else {
            kd4.d("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void w() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.G(childFragmentManager);
        }
    }

    @DexIgnore
    public void b(SpannableString spannableString) {
        kd4.b(spannableString, LogBuilder.KEY_TIME);
        tr3<sc2> tr3 = this.k;
        if (tr3 != null) {
            sc2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString.toString());
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(rv2 rv2) {
        kd4.b(rv2, "presenter");
        this.l = rv2;
    }

    @DexIgnore
    public void a(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        kd4.b(str, "deviceId");
        kd4.b(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.C;
        Context context = getContext();
        if (context != null) {
            kd4.a((Object) context, "context!!");
            aVar.a(context, str, arrayList, alarm);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void a(boolean z) {
        tr3<sc2> tr3 = this.k;
        if (tr3 != null) {
            sc2 a2 = tr3.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.q;
                if (constraintLayout != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(SpannableString spannableString) {
        kd4.b(spannableString, LogBuilder.KEY_TIME);
        tr3<sc2> tr3 = this.k;
        if (tr3 != null) {
            sc2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(spannableString);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
