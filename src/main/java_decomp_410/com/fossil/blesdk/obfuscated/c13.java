package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c13 implements Factory<NotificationHybridEveryonePresenter> {
    @DexIgnore
    public static NotificationHybridEveryonePresenter a(x03 x03, int i, ArrayList<ContactWrapper> arrayList, j62 j62, t03 t03) {
        return new NotificationHybridEveryonePresenter(x03, i, arrayList, j62, t03);
    }
}
