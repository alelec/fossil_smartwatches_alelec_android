package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.misfit.frameworks.common.constants.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jl1 extends cl1 {
    @DexIgnore
    public jl1(dl1 dl1) {
        super(dl1);
    }

    @DexIgnore
    public static Object b(c61 c61, String str) {
        d61 a = a(c61, str);
        if (a == null) {
            return null;
        }
        String str2 = a.d;
        if (str2 != null) {
            return str2;
        }
        Long l = a.e;
        if (l != null) {
            return l;
        }
        Double d = a.g;
        if (d != null) {
            return d;
        }
        return null;
    }

    @DexIgnore
    public final void a(i61 i61, Object obj) {
        bk0.a(obj);
        i61.e = null;
        i61.f = null;
        i61.h = null;
        if (obj instanceof String) {
            i61.e = (String) obj;
        } else if (obj instanceof Long) {
            i61.f = (Long) obj;
        } else if (obj instanceof Double) {
            i61.h = (Double) obj;
        } else {
            d().s().a("Ignoring invalid (type) user attribute value", obj);
        }
    }

    @DexIgnore
    public final boolean r() {
        return false;
    }

    @DexIgnore
    public final int[] t() {
        Map<String, String> a = jg1.a(this.b.getContext());
        if (a == null || a.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int intValue = jg1.R.a().intValue();
        Iterator<Map.Entry<String, String>> it = a.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Map.Entry next = it.next();
            if (((String) next.getKey()).startsWith("measurement.id.")) {
                try {
                    int parseInt = Integer.parseInt((String) next.getValue());
                    if (parseInt != 0) {
                        arrayList.add(Integer.valueOf(parseInt));
                        if (arrayList.size() >= intValue) {
                            d().v().a("Too many experiment IDs. Number of IDs", Integer.valueOf(arrayList.size()));
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    d().v().a("Experiment ID NumberFormatException", e);
                }
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        int size = arrayList.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            Object obj = arrayList.get(i);
            i++;
            iArr[i2] = ((Integer) obj).intValue();
            i2++;
        }
        return iArr;
    }

    @DexIgnore
    public final String b(e61 e61) {
        f61[] f61Arr;
        c61[] c61Arr;
        String str;
        c61[] c61Arr2;
        String str2;
        int i;
        int i2;
        f61[] f61Arr2;
        String str3;
        i61[] i61Arr;
        StringBuilder sb = new StringBuilder();
        sb.append("\nbatch {\n");
        f61[] f61Arr3 = e61.c;
        if (f61Arr3 != null) {
            int length = f61Arr3.length;
            int i3 = 0;
            while (i3 < length) {
                f61 f61 = f61Arr3[i3];
                if (f61 == null || f61 == null) {
                    f61Arr = f61Arr3;
                } else {
                    a(sb, 1);
                    sb.append("bundle {\n");
                    a(sb, 1, "protocol_version", (Object) f61.c);
                    a(sb, 1, "platform", (Object) f61.k);
                    a(sb, 1, "gmp_version", (Object) f61.s);
                    a(sb, 1, "uploading_gmp_version", (Object) f61.t);
                    a(sb, 1, "config_version", (Object) f61.I);
                    a(sb, 1, "gmp_app_id", (Object) f61.A);
                    a(sb, 1, "admob_app_id", (Object) f61.N);
                    a(sb, 1, "app_id", (Object) f61.q);
                    a(sb, 1, "app_version", (Object) f61.r);
                    a(sb, 1, "app_version_major", (Object) f61.E);
                    a(sb, 1, "firebase_instance_id", (Object) f61.D);
                    a(sb, 1, "dev_cert_hash", (Object) f61.x);
                    a(sb, 1, "app_store", (Object) f61.p);
                    a(sb, 1, "upload_timestamp_millis", (Object) f61.f);
                    a(sb, 1, "start_timestamp_millis", (Object) f61.g);
                    a(sb, 1, "end_timestamp_millis", (Object) f61.h);
                    a(sb, 1, "previous_bundle_start_timestamp_millis", (Object) f61.i);
                    a(sb, 1, "previous_bundle_end_timestamp_millis", (Object) f61.j);
                    a(sb, 1, "app_instance_id", (Object) f61.w);
                    a(sb, 1, "resettable_device_id", (Object) f61.u);
                    a(sb, 1, "device_id", (Object) f61.H);
                    a(sb, 1, "ds_id", (Object) f61.K);
                    a(sb, 1, "limited_ad_tracking", (Object) f61.v);
                    a(sb, 1, Constants.OS_VERSION, (Object) f61.l);
                    a(sb, 1, "device_model", (Object) f61.m);
                    a(sb, 1, "user_default_language", (Object) f61.n);
                    a(sb, 1, "time_zone_offset_minutes", (Object) f61.o);
                    a(sb, 1, "bundle_sequential_index", (Object) f61.y);
                    a(sb, 1, "service_upload", (Object) f61.B);
                    a(sb, 1, "health_monitor", (Object) f61.z);
                    Long l = f61.J;
                    if (!(l == null || l.longValue() == 0)) {
                        a(sb, 1, "android_id", (Object) f61.J);
                    }
                    Integer num = f61.M;
                    if (num != null) {
                        a(sb, 1, "retry_counter", (Object) num);
                    }
                    i61[] i61Arr2 = f61.e;
                    String str4 = "string_value";
                    String str5 = "name";
                    int i4 = 2;
                    if (i61Arr2 != null) {
                        int length2 = i61Arr2.length;
                        int i5 = 0;
                        while (i5 < length2) {
                            i61 i61 = i61Arr2[i5];
                            if (i61 != null) {
                                a(sb, 2);
                                sb.append("user_property {\n");
                                i61Arr = i61Arr2;
                                a(sb, 2, "set_timestamp_millis", (Object) i61.c);
                                a(sb, 2, str5, (Object) i().c(i61.d));
                                a(sb, 2, str4, (Object) i61.e);
                                a(sb, 2, "int_value", (Object) i61.f);
                                a(sb, 2, "double_value", (Object) i61.h);
                                a(sb, 2);
                                sb.append("}\n");
                            } else {
                                i61Arr = i61Arr2;
                            }
                            i5++;
                            i61Arr2 = i61Arr;
                        }
                    }
                    a61[] a61Arr = f61.C;
                    String str6 = f61.q;
                    if (a61Arr != null) {
                        int length3 = a61Arr.length;
                        int i6 = 0;
                        while (i6 < length3) {
                            a61 a61 = a61Arr[i6];
                            if (a61 != null) {
                                a(sb, i4);
                                sb.append("audience_membership {\n");
                                i2 = i6;
                                i = length3;
                                a(sb, 2, "audience_id", (Object) a61.c);
                                a(sb, 2, "new_audience", (Object) a61.f);
                                g61 g61 = a61.d;
                                StringBuilder sb2 = sb;
                                str2 = str5;
                                f61Arr2 = f61Arr3;
                                str3 = str4;
                                String str7 = str6;
                                a(sb2, 2, "current_data", g61, str7);
                                a(sb2, 2, "previous_data", a61.e, str7);
                                a(sb, 2);
                                sb.append("}\n");
                            } else {
                                i2 = i6;
                                i = length3;
                                str2 = str5;
                                f61Arr2 = f61Arr3;
                                str3 = str4;
                            }
                            i6 = i2 + 1;
                            str4 = str3;
                            f61Arr3 = f61Arr2;
                            length3 = i;
                            str5 = str2;
                            i4 = 2;
                        }
                    }
                    String str8 = str5;
                    f61Arr = f61Arr3;
                    int i7 = 2;
                    String str9 = str4;
                    c61[] c61Arr3 = f61.d;
                    if (c61Arr3 != null) {
                        int length4 = c61Arr3.length;
                        int i8 = 0;
                        while (i8 < length4) {
                            c61 c61 = c61Arr3[i8];
                            if (c61 != null) {
                                a(sb, i7);
                                sb.append("event {\n");
                                str = str8;
                                a(sb, i7, str, (Object) i().a(c61.d));
                                a(sb, i7, "timestamp_millis", (Object) c61.e);
                                a(sb, i7, "previous_timestamp_millis", (Object) c61.f);
                                a(sb, i7, "count", (Object) c61.g);
                                d61[] d61Arr = c61.c;
                                if (d61Arr != null) {
                                    int length5 = d61Arr.length;
                                    int i9 = 0;
                                    while (i9 < length5) {
                                        d61 d61 = d61Arr[i9];
                                        if (d61 != null) {
                                            a(sb, 3);
                                            sb.append("param {\n");
                                            c61Arr2 = c61Arr3;
                                            a(sb, 3, str, (Object) i().b(d61.c));
                                            a(sb, 3, str9, (Object) d61.d);
                                            a(sb, 3, "int_value", (Object) d61.e);
                                            a(sb, 3, "double_value", (Object) d61.g);
                                            a(sb, 3);
                                            sb.append("}\n");
                                        } else {
                                            c61Arr2 = c61Arr3;
                                        }
                                        i9++;
                                        c61Arr3 = c61Arr2;
                                        i7 = 2;
                                    }
                                }
                                c61Arr = c61Arr3;
                                a(sb, i7);
                                sb.append("}\n");
                            } else {
                                c61Arr = c61Arr3;
                                str = str8;
                            }
                            i8++;
                            str8 = str;
                            c61Arr3 = c61Arr;
                        }
                    }
                    a(sb, 1);
                    sb.append("}\n");
                }
                i3++;
                f61Arr3 = f61Arr;
            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final void a(d61 d61, Object obj) {
        bk0.a(obj);
        d61.d = null;
        d61.e = null;
        d61.g = null;
        if (obj instanceof String) {
            d61.d = (String) obj;
        } else if (obj instanceof Long) {
            d61.e = (Long) obj;
        } else if (obj instanceof Double) {
            d61.g = (Double) obj;
        } else {
            d().s().a("Ignoring invalid (type) event param value", obj);
        }
    }

    @DexIgnore
    public final byte[] a(e61 e61) {
        try {
            byte[] bArr = new byte[e61.b()];
            ub1 a = ub1.a(bArr, 0, bArr.length);
            e61.a(a);
            a.b();
            return bArr;
        } catch (IOException e) {
            d().s().a("Data loss. Failed to serialize batch", e);
            return null;
        }
    }

    @DexIgnore
    public static d61 a(c61 c61, String str) {
        for (d61 d61 : c61.c) {
            if (d61.c.equals(str)) {
                return d61;
            }
        }
        return null;
    }

    @DexIgnore
    public static d61[] a(d61[] d61Arr, String str, Object obj) {
        for (d61 d61 : d61Arr) {
            if (str.equals(d61.c)) {
                d61.e = null;
                d61.d = null;
                d61.g = null;
                if (obj instanceof Long) {
                    d61.e = (Long) obj;
                } else if (obj instanceof String) {
                    d61.d = (String) obj;
                } else if (obj instanceof Double) {
                    d61.g = (Double) obj;
                }
                return d61Arr;
            }
        }
        d61[] d61Arr2 = new d61[(d61Arr.length + 1)];
        System.arraycopy(d61Arr, 0, d61Arr2, 0, d61Arr.length);
        d61 d612 = new d61();
        d612.c = str;
        if (obj instanceof Long) {
            d612.e = (Long) obj;
        } else if (obj instanceof String) {
            d612.d = (String) obj;
        } else if (obj instanceof Double) {
            d612.g = (Double) obj;
        }
        d61Arr2[d61Arr.length] = d612;
        return d61Arr2;
    }

    @DexIgnore
    public final String a(s51 s51) {
        if (s51 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nevent_filter {\n");
        a(sb, 0, "filter_id", (Object) s51.c);
        a(sb, 0, BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, (Object) i().a(s51.d));
        a(sb, 1, "event_count_filter", s51.g);
        sb.append("  filters {\n");
        for (t51 a : s51.e) {
            a(sb, 2, a);
        }
        a(sb, 1);
        sb.append("}\n}\n");
        return sb.toString();
    }

    @DexIgnore
    public final String a(v51 v51) {
        if (v51 == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\nproperty_filter {\n");
        a(sb, 0, "filter_id", (Object) v51.c);
        a(sb, 0, "property_name", (Object) i().c(v51.d));
        a(sb, 1, v51.e);
        sb.append("}\n");
        return sb.toString();
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, String str, g61 g61, String str2) {
        StringBuilder sb2 = sb;
        g61 g612 = g61;
        if (g612 != null) {
            a(sb2, 3);
            sb2.append(str);
            sb2.append(" {\n");
            if (g612.d != null) {
                a(sb2, 4);
                sb2.append("results: ");
                long[] jArr = g612.d;
                int length = jArr.length;
                int i2 = 0;
                int i3 = 0;
                while (i2 < length) {
                    Long valueOf = Long.valueOf(jArr[i2]);
                    int i4 = i3 + 1;
                    if (i3 != 0) {
                        sb2.append(", ");
                    }
                    sb2.append(valueOf);
                    i2++;
                    i3 = i4;
                }
                sb2.append(10);
            }
            if (g612.c != null) {
                a(sb2, 4);
                sb2.append("status: ");
                long[] jArr2 = g612.c;
                int length2 = jArr2.length;
                int i5 = 0;
                int i6 = 0;
                while (i5 < length2) {
                    Long valueOf2 = Long.valueOf(jArr2[i5]);
                    int i7 = i6 + 1;
                    if (i6 != 0) {
                        sb2.append(", ");
                    }
                    sb2.append(valueOf2);
                    i5++;
                    i6 = i7;
                }
                sb2.append(10);
            }
            if (l().j(str2)) {
                if (g612.e != null) {
                    a(sb2, 4);
                    sb2.append("dynamic_filter_timestamps: {");
                    b61[] b61Arr = g612.e;
                    int length3 = b61Arr.length;
                    int i8 = 0;
                    int i9 = 0;
                    while (i8 < length3) {
                        b61 b61 = b61Arr[i8];
                        int i10 = i9 + 1;
                        if (i9 != 0) {
                            sb2.append(", ");
                        }
                        sb2.append(b61.c);
                        sb2.append(":");
                        sb2.append(b61.d);
                        i8++;
                        i9 = i10;
                    }
                    sb2.append("}\n");
                }
                if (g612.f != null) {
                    a(sb2, 4);
                    sb2.append("sequence_filter_timestamps: {");
                    h61[] h61Arr = g612.f;
                    int length4 = h61Arr.length;
                    int i11 = 0;
                    int i12 = 0;
                    while (i11 < length4) {
                        h61 h61 = h61Arr[i11];
                        int i13 = i12 + 1;
                        if (i12 != 0) {
                            sb2.append(", ");
                        }
                        sb2.append(h61.c);
                        sb2.append(": [");
                        long[] jArr3 = h61.d;
                        int length5 = jArr3.length;
                        int i14 = 0;
                        int i15 = 0;
                        while (i14 < length5) {
                            long j = jArr3[i14];
                            int i16 = i15 + 1;
                            if (i15 != 0) {
                                sb2.append(", ");
                            }
                            sb2.append(j);
                            i14++;
                            i15 = i16;
                        }
                        sb2.append("]");
                        i11++;
                        i12 = i13;
                    }
                    sb2.append("}\n");
                }
            }
            a(sb2, 3);
            sb2.append("}\n");
        }
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) throws IOException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            d().s().a("Failed to gzip content", e);
            throw e;
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, String str, u51 u51) {
        if (u51 != null) {
            a(sb, i);
            sb.append(str);
            sb.append(" {\n");
            Integer num = u51.c;
            if (num != null) {
                int intValue = num.intValue();
                a(sb, i, "comparison_type", (Object) intValue != 1 ? intValue != 2 ? intValue != 3 ? intValue != 4 ? "UNKNOWN_COMPARISON_TYPE" : "BETWEEN" : "EQUAL" : "GREATER_THAN" : "LESS_THAN");
            }
            a(sb, i, "match_as_float", (Object) u51.d);
            a(sb, i, "comparison_value", (Object) u51.e);
            a(sb, i, "min_comparison_value", (Object) u51.f);
            a(sb, i, "max_comparison_value", (Object) u51.g);
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i, t51 t51) {
        String str;
        if (t51 != null) {
            a(sb, i);
            sb.append("filter {\n");
            a(sb, i, "complement", (Object) t51.e);
            a(sb, i, "param_name", (Object) i().b(t51.f));
            int i2 = i + 1;
            w51 w51 = t51.c;
            if (w51 != null) {
                a(sb, i2);
                sb.append("string_filter");
                sb.append(" {\n");
                Integer num = w51.c;
                if (num != null) {
                    switch (num.intValue()) {
                        case 1:
                            str = "REGEXP";
                            break;
                        case 2:
                            str = "BEGINS_WITH";
                            break;
                        case 3:
                            str = "ENDS_WITH";
                            break;
                        case 4:
                            str = "PARTIAL";
                            break;
                        case 5:
                            str = "EXACT";
                            break;
                        case 6:
                            str = "IN_LIST";
                            break;
                        default:
                            str = "UNKNOWN_MATCH_TYPE";
                            break;
                    }
                    a(sb, i2, "match_type", (Object) str);
                }
                a(sb, i2, "expression", (Object) w51.d);
                a(sb, i2, "case_sensitive", (Object) w51.e);
                if (w51.f.length > 0) {
                    a(sb, i2 + 1);
                    sb.append("expression_list {\n");
                    for (String append : w51.f) {
                        a(sb, i2 + 2);
                        sb.append(append);
                        sb.append("\n");
                    }
                    sb.append("}\n");
                }
                a(sb, i2);
                sb.append("}\n");
            }
            a(sb, i2, "number_filter", t51.d);
            a(sb, i);
            sb.append("}\n");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("  ");
        }
    }

    @DexIgnore
    public static void a(StringBuilder sb, int i, String str, Object obj) {
        if (obj != null) {
            a(sb, i + 1);
            sb.append(str);
            sb.append(": ");
            sb.append(obj);
            sb.append(10);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        d().s().a("Failed to load parcelable from buffer");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
        r1.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        throw r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        r5 = move-exception;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x001c */
    public final <T extends Parcelable> T a(byte[] bArr, Parcelable.Creator<T> creator) {
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        obtain.unmarshall(bArr, 0, bArr.length);
        obtain.setDataPosition(0);
        T t = (Parcelable) creator.createFromParcel(obtain);
        obtain.recycle();
        return t;
    }

    @DexIgnore
    public final boolean a(hg1 hg1, rl1 rl1) {
        bk0.a(hg1);
        bk0.a(rl1);
        if (!TextUtils.isEmpty(rl1.f) || !TextUtils.isEmpty(rl1.v)) {
            return true;
        }
        b();
        return false;
    }

    @DexIgnore
    public static boolean a(String str) {
        return str != null && str.matches("([+-])?([0-9]+\\.?[0-9]*|[0-9]*\\.?[0-9]+)") && str.length() <= 310;
    }

    @DexIgnore
    public static boolean a(long[] jArr, int i) {
        if (i >= (jArr.length << 6)) {
            return false;
        }
        if (((1 << (i % 64)) & jArr[i / 64]) != 0) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static long[] a(BitSet bitSet) {
        int length = (bitSet.length() + 63) / 64;
        long[] jArr = new long[length];
        for (int i = 0; i < length; i++) {
            jArr[i] = 0;
            for (int i2 = 0; i2 < 64; i2++) {
                int i3 = (i << 6) + i2;
                if (i3 >= bitSet.length()) {
                    break;
                }
                if (bitSet.get(i3)) {
                    jArr[i] = jArr[i] | (1 << i2);
                }
            }
        }
        return jArr;
    }

    @DexIgnore
    public final boolean a(long j, long j2) {
        return j == 0 || j2 <= 0 || Math.abs(c().b() - j) > j2;
    }

    @DexIgnore
    public final byte[] a(byte[] bArr) throws IOException {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = gZIPInputStream.read(bArr2);
                if (read > 0) {
                    byteArrayOutputStream.write(bArr2, 0, read);
                } else {
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            d().s().a("Failed to ungzip content", e);
            throw e;
        }
    }
}
