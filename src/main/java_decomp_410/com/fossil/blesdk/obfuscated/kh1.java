package com.fossil.blesdk.obfuscated;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kh1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public String c;
    @DexIgnore
    public /* final */ /* synthetic */ fh1 d;

    @DexIgnore
    public kh1(fh1 fh1, String str, String str2) {
        this.d = fh1;
        bk0.b(str);
        this.a = str;
    }

    @DexIgnore
    public final String a() {
        if (!this.b) {
            this.b = true;
            this.c = this.d.s().getString(this.a, (String) null);
        }
        return this.c;
    }

    @DexIgnore
    public final void a(String str) {
        if (!nl1.e(str, this.c)) {
            SharedPreferences.Editor edit = this.d.s().edit();
            edit.putString(this.a, str);
            edit.apply();
            this.c = str;
        }
    }
}
