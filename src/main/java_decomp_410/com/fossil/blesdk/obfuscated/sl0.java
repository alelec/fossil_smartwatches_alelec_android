package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sl0 {
    /*
    static {
        Uri.parse("https://plus.google.com/").buildUpon().appendPath("circles").appendPath(Constants.FIND).build();
    }
    */

    @DexIgnore
    public static Intent a(String str) {
        Uri fromParts = Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, str, (String) null);
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(fromParts);
        return intent;
    }

    @DexIgnore
    public static Intent a(String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri.Builder appendQueryParameter = Uri.parse("market://details").buildUpon().appendQueryParameter("id", str);
        if (!TextUtils.isEmpty(str2)) {
            appendQueryParameter.appendQueryParameter("pcampaignid", str2);
        }
        intent.setData(appendQueryParameter.build());
        intent.setPackage("com.android.vending");
        intent.addFlags(524288);
        return intent;
    }

    @DexIgnore
    public static Intent a() {
        Intent intent = new Intent("com.google.android.clockwork.home.UPDATE_ANDROID_WEAR_ACTION");
        intent.setPackage("com.google.android.wearable.app");
        return intent;
    }
}
