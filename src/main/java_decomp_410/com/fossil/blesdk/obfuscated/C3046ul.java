package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ul */
public class C3046ul implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2968tj f10024e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.String f10025f;

    @DexIgnore
    /* renamed from: g */
    public androidx.work.WorkerParameters.C0359a f10026g;

    @DexIgnore
    public C3046ul(com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str, androidx.work.WorkerParameters.C0359a aVar) {
        this.f10024e = tjVar;
        this.f10025f = str;
        this.f10026g = aVar;
    }

    @DexIgnore
    public void run() {
        this.f10024e.mo16460e().mo14426a(this.f10025f, this.f10026g);
    }
}
