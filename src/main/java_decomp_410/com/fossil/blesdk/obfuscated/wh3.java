package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.text.Regex;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wh3 extends zr2 implements vh3, ws3.g {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public uh3 j;
    @DexIgnore
    public tr3<cf2> k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public GoalType m; // = GoalType.TOTAL_STEPS;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return wh3.o;
        }

        @DexIgnore
        public final wh3 b() {
            return new wh3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;
        @DexIgnore
        public /* final */ /* synthetic */ cf2 f;

        @DexIgnore
        public b(wh3 wh3, cf2 cf2) {
            this.e = wh3;
            this.f = cf2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                boolean z = true;
                int i = 0;
                if (!(editable.length() == 0)) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.f.w.setText(String.valueOf(parseInt));
                    }
                    if (this.e.m == GoalType.TOTAL_SLEEP) {
                        int i2 = 59;
                        if (parseInt > 59) {
                            this.f.w.setText(String.valueOf(59));
                        } else {
                            i2 = parseInt;
                        }
                        wh3 wh3 = this.e;
                        FlexibleEditText flexibleEditText = this.f.w;
                        kd4.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                        Editable text = flexibleEditText.getText();
                        if (text != null) {
                            kd4.a((Object) text, "binding.fetSleepMinuteValue.text!!");
                            if (text.length() != 0) {
                                z = false;
                            }
                            if (!z) {
                                FlexibleEditText flexibleEditText2 = this.f.v;
                                kd4.a((Object) flexibleEditText2, "binding.fetSleepHourValue");
                                i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                            }
                            parseInt = wh3.d(i2, i);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    this.e.T0().a(parseInt, this.e.m);
                    return;
                }
                this.f.w.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;

        @DexIgnore
        public c(wh3 wh3) {
            this.e = wh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;

        @DexIgnore
        public d(wh3 wh3) {
            this.e = wh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            wh3 wh3 = this.e;
            if (view != null) {
                wh3.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;

        @DexIgnore
        public e(wh3 wh3) {
            this.e = wh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            wh3 wh3 = this.e;
            if (view != null) {
                wh3.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;

        @DexIgnore
        public f(wh3 wh3) {
            this.e = wh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            wh3 wh3 = this.e;
            if (view != null) {
                wh3.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;

        @DexIgnore
        public g(wh3 wh3) {
            this.e = wh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            wh3 wh3 = this.e;
            if (view != null) {
                wh3.a(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;
        @DexIgnore
        public /* final */ /* synthetic */ cf2 f;

        @DexIgnore
        public h(wh3 wh3, cf2 cf2) {
            this.e = wh3;
            this.f = cf2;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (this.e.m == GoalType.TOTAL_SLEEP) {
                wh3 wh3 = this.e;
                FlexibleEditText flexibleEditText = this.f.w;
                kd4.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                wh3.d(z, flexibleEditText.isFocused());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;
        @DexIgnore
        public /* final */ /* synthetic */ cf2 f;

        @DexIgnore
        public i(wh3 wh3, cf2 cf2) {
            this.e = wh3;
            this.f = cf2;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            wh3 wh3 = this.e;
            FlexibleEditText flexibleEditText = this.f.v;
            kd4.a((Object) flexibleEditText, "binding.fetSleepHourValue");
            wh3.d(flexibleEditText.isFocused(), z);
            if (z) {
                FlexibleEditText flexibleEditText2 = this.f.v;
                kd4.a((Object) flexibleEditText2, "binding.fetSleepHourValue");
                if (Integer.parseInt(String.valueOf(flexibleEditText2.getText())) == 16) {
                    this.e.T0().j();
                    ds3 ds3 = ds3.c;
                    FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                    kd4.a((Object) childFragmentManager, "childFragmentManager");
                    ds3.a(childFragmentManager, GoalType.TOTAL_SLEEP, 16);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;
        @DexIgnore
        public /* final */ /* synthetic */ cf2 f;

        @DexIgnore
        public j(wh3 wh3, cf2 cf2) {
            this.e = wh3;
            this.f = cf2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                if (!(editable.length() == 0)) {
                    int a = il2.a(editable.toString());
                    if (a <= 9 && editable.length() > 1) {
                        this.f.u.setText(String.valueOf(a));
                    }
                    this.e.p(a);
                    return;
                }
                this.f.u.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence != null) {
                if (!(charSequence.length() == 0)) {
                    this.f.u.removeTextChangedListener(this);
                    String obj = charSequence.toString();
                    if (StringsKt__StringsKt.a((CharSequence) obj, (CharSequence) ",", false, 2, (Object) null)) {
                        obj = new Regex(",").replace((CharSequence) obj, "");
                    }
                    this.f.u.setText(il2.c(Integer.parseInt(obj)));
                    FlexibleEditText flexibleEditText = this.f.u;
                    kd4.a((Object) flexibleEditText, "binding.fetGoalsValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        flexibleEditText.setSelection(text.length());
                        this.f.u.addTextChangedListener(this);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                this.f.u.setText(String.valueOf(0));
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ wh3 e;
        @DexIgnore
        public /* final */ /* synthetic */ cf2 f;

        @DexIgnore
        public k(wh3 wh3, cf2 cf2) {
            this.e = wh3;
            this.f = cf2;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                boolean z = true;
                int i = 0;
                if (!(editable.length() == 0)) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.f.v.setText(String.valueOf(parseInt));
                    }
                    int i2 = 16;
                    if (parseInt > 16) {
                        this.f.v.setText(String.valueOf(16));
                        ds3 ds3 = ds3.c;
                        FragmentManager childFragmentManager = this.e.getChildFragmentManager();
                        kd4.a((Object) childFragmentManager, "childFragmentManager");
                        ds3.a(childFragmentManager, this.e.m, 16);
                    } else {
                        i2 = parseInt;
                    }
                    wh3 wh3 = this.e;
                    FlexibleEditText flexibleEditText = this.f.w;
                    kd4.a((Object) flexibleEditText, "binding.fetSleepMinuteValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        kd4.a((Object) text, "binding.fetSleepMinuteValue.text!!");
                        if (text.length() != 0) {
                            z = false;
                        }
                        if (!z) {
                            FlexibleEditText flexibleEditText2 = this.f.w;
                            kd4.a((Object) flexibleEditText2, "binding.fetSleepMinuteValue");
                            i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                        }
                        this.e.T0().a(wh3.d(i, i2), this.e.m);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                this.f.v.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ cf2 e;

        @DexIgnore
        public l(cf2 cf2) {
            this.e = cf2;
        }

        @DexIgnore
        public final void run() {
            this.e.F.fullScroll(130);
        }
    }

    /*
    static {
        String simpleName = wh3.class.getSimpleName();
        kd4.a((Object) simpleName, "ProfileGoalEditFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public void I0() {
        a();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void O(boolean z) {
        FLogger.INSTANCE.getLocal().d(o, "showSleepGoalEdit");
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.W.c();
                Object systemService = c2.getSystemService("input_method");
                if (systemService != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) systemService;
                    if (z) {
                        LinearLayout linearLayout = a2.E;
                        kd4.a((Object) linearLayout, "llSleepGoalValue");
                        linearLayout.setVisibility(0);
                        LinearLayout linearLayout2 = a2.D;
                        kd4.a((Object) linearLayout2, "llGoalsValue");
                        linearLayout2.setVisibility(8);
                        a2.v.setTextColor(k6.a((Context) c2, (int) R.color.dianaSleepTab));
                        a2.w.setTextColor(k6.a((Context) c2, (int) R.color.coolGrey));
                        FlexibleEditText flexibleEditText = a2.v;
                        kd4.a((Object) flexibleEditText, "fetSleepHourValue");
                        flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                        FlexibleEditText flexibleEditText2 = a2.w;
                        kd4.a((Object) flexibleEditText2, "fetSleepMinuteValue");
                        flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                        FlexibleTextView flexibleTextView = a2.A;
                        kd4.a((Object) flexibleTextView, "ftvSleepHourUnit");
                        flexibleTextView.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsSleep_Label__Hr));
                        FlexibleTextView flexibleTextView2 = a2.B;
                        kd4.a((Object) flexibleTextView2, "ftvSleepMinuteUnit");
                        flexibleTextView2.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsSleep_Label__Min));
                        int mValue = (this.l ? a2.r : a2.q).getMValue() / 60;
                        CustomEditGoalView customEditGoalView = this.l ? a2.r : a2.q;
                        a2.v.setText(String.valueOf(mValue));
                        a2.w.setText(String.valueOf(customEditGoalView.getMValue() % 60));
                        inputMethodManager.showSoftInput(a2.v, 1);
                    } else {
                        LinearLayout linearLayout3 = a2.E;
                        kd4.a((Object) linearLayout3, "llSleepGoalValue");
                        linearLayout3.setVisibility(8);
                        LinearLayout linearLayout4 = a2.D;
                        kd4.a((Object) linearLayout4, "llGoalsValue");
                        linearLayout4.setVisibility(0);
                        FlexibleEditText flexibleEditText3 = a2.u;
                        kd4.a((Object) flexibleEditText3, "fetGoalsValue");
                        Editable text = flexibleEditText3.getText();
                        if (text != null) {
                            flexibleEditText3.setSelection(text.length());
                            a2.u.requestFocus();
                            inputMethodManager.showSoftInput(a2.u, 1);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    a2.F.post(new l(a2));
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
            }
        }
    }

    @DexIgnore
    public boolean S0() {
        if (getActivity() == null) {
            return true;
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "activity!!");
            if (activity.isFinishing()) {
                return true;
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                kd4.a((Object) activity2, "activity!!");
                if (activity2.isDestroyed()) {
                    return true;
                }
                uh3 uh3 = this.j;
                if (uh3 != null) {
                    uh3.h();
                    return true;
                }
                kd4.d("mPresenter");
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final uh3 T0() {
        uh3 uh3 = this.j;
        if (uh3 != null) {
            return uh3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void b(GoalType goalType) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.W.c();
                if (goalType != null) {
                    int i2 = xh3.b[goalType.ordinal()];
                    if (i2 == 1) {
                        FlexibleTextView flexibleTextView = a2.y;
                        kd4.a((Object) flexibleTextView, "ftvGoalTitle");
                        flexibleTextView.setText(getString(R.string.DashboardDiana_Main_ActiveCaloriesToday_Title__ActiveCalories));
                        FlexibleTextView flexibleTextView2 = a2.x;
                        kd4.a((Object) flexibleTextView2, "ftvDesc");
                        flexibleTextView2.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsActiveCalories_Text__DailyActiveCaloriesGoal));
                        a2.u.setTextColor(k6.a((Context) c2, (int) R.color.dianaActiveCaloriesTab));
                        FlexibleEditText flexibleEditText = a2.u;
                        kd4.a((Object) flexibleEditText, "fetGoalsValue");
                        flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                        a2.u.setText(String.valueOf((this.l ? a2.q : a2.t).getValue()));
                        FlexibleTextView flexibleTextView3 = a2.z;
                        kd4.a((Object) flexibleTextView3, "ftvGoalsUnit");
                        flexibleTextView3.setVisibility(8);
                        O(false);
                    } else if (i2 == 2) {
                        FlexibleTextView flexibleTextView4 = a2.y;
                        kd4.a((Object) flexibleTextView4, "ftvGoalTitle");
                        flexibleTextView4.setText(getString(R.string.DashboardDiana_Main_ActiveMinutesToday_Title__ActiveMinutes));
                        FlexibleTextView flexibleTextView5 = a2.x;
                        kd4.a((Object) flexibleTextView5, "ftvDesc");
                        flexibleTextView5.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsActiveMinutes_Text__DailyActiveMinutesGoal));
                        a2.u.setTextColor(k6.a((Context) c2, (int) R.color.dianaActiveMinutesTab));
                        FlexibleEditText flexibleEditText2 = a2.u;
                        kd4.a((Object) flexibleEditText2, "fetGoalsValue");
                        flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(5)});
                        a2.u.setText(String.valueOf(a2.t.getValue()));
                        FlexibleTextView flexibleTextView6 = a2.z;
                        kd4.a((Object) flexibleTextView6, "ftvGoalsUnit");
                        flexibleTextView6.setVisibility(8);
                        O(false);
                    } else if (i2 == 3) {
                        FlexibleTextView flexibleTextView7 = a2.y;
                        kd4.a((Object) flexibleTextView7, "ftvGoalTitle");
                        flexibleTextView7.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsSteps_Label__Steps));
                        FlexibleTextView flexibleTextView8 = a2.x;
                        kd4.a((Object) flexibleTextView8, "ftvDesc");
                        flexibleTextView8.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsSteps_Text__DailyStepsGoal));
                        a2.u.setTextColor(k6.a((Context) c2, (int) R.color.dianaStepsTab));
                        FlexibleEditText flexibleEditText3 = a2.u;
                        kd4.a((Object) flexibleEditText3, "fetGoalsValue");
                        flexibleEditText3.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                        a2.u.setText(String.valueOf(a2.s.getValue()));
                        FlexibleTextView flexibleTextView9 = a2.z;
                        kd4.a((Object) flexibleTextView9, "ftvGoalsUnit");
                        flexibleTextView9.setVisibility(8);
                        O(false);
                    } else if (i2 == 4) {
                        FlexibleTextView flexibleTextView10 = a2.y;
                        kd4.a((Object) flexibleTextView10, "ftvGoalTitle");
                        flexibleTextView10.setText(getString(R.string.DashboardHybrid_Main_SleepToday_Title__Sleep));
                        FlexibleTextView flexibleTextView11 = a2.x;
                        kd4.a((Object) flexibleTextView11, "ftvDesc");
                        flexibleTextView11.setText(getString(R.string.Profile_MyProfileDiana_SetGoalsSleep_Text__DailySleepGoal));
                        a2.u.setTextColor(k6.a((Context) c2, (int) R.color.dianaSleepTab));
                        O(true);
                    } else if (i2 == 5) {
                        FlexibleTextView flexibleTextView12 = a2.y;
                        kd4.a((Object) flexibleTextView12, "ftvGoalTitle");
                        flexibleTextView12.setText(getString(R.string.DashboardHybrid_GoalTracking_DetailPageNoRecord_Title__GoalTracking));
                        FlexibleTextView flexibleTextView13 = a2.x;
                        kd4.a((Object) flexibleTextView13, "ftvDesc");
                        flexibleTextView13.setText(getString(R.string.Profile_MyProfileHybrid_SetGoalsGoalTracking_Text__DailyGoalTracking));
                        a2.u.setTextColor(k6.a((Context) c2, (int) R.color.hybridGoalTrackingTab));
                        FlexibleEditText flexibleEditText4 = a2.u;
                        kd4.a((Object) flexibleEditText4, "fetGoalsValue");
                        flexibleEditText4.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                        a2.u.setText(String.valueOf(a2.r.getValue()));
                        FlexibleTextView flexibleTextView14 = a2.z;
                        kd4.a((Object) flexibleTextView14, "ftvGoalsUnit");
                        flexibleTextView14.setVisibility(0);
                        FlexibleTextView flexibleTextView15 = a2.z;
                        kd4.a((Object) flexibleTextView15, "ftvGoalsUnit");
                        flexibleTextView15.setText(getString(R.string.Profile_MyProfileHybrid_SetGoalsGoalTracking_Label__Times));
                        O(false);
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void d(boolean z, boolean z2) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.W.c();
                if (z) {
                    a2.v.setTextColor(k6.a((Context) c2, (int) R.color.dianaSleepTab));
                    a2.w.setTextColor(k6.a((Context) c2, (int) R.color.coolGrey));
                } else if (z2) {
                    a2.v.setTextColor(k6.a((Context) c2, (int) R.color.coolGrey));
                    a2.w.setTextColor(k6.a((Context) c2, (int) R.color.dianaSleepTab));
                } else {
                    a2.v.setTextColor(k6.a((Context) c2, (int) R.color.dianaSleepTab));
                    a2.w.setTextColor(k6.a((Context) c2, (int) R.color.dianaSleepTab));
                }
            }
        }
    }

    @DexIgnore
    public void i(int i2) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                a2.v.setText(String.valueOf(i2 / 60));
                a2.w.setText(String.valueOf(i2 % 60));
                uh3 uh3 = this.j;
                if (uh3 != null) {
                    uh3.a(i2, GoalType.TOTAL_SLEEP);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void j(int i2) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null && !this.l) {
                a2.r.setValue(i2);
            }
        }
    }

    @DexIgnore
    public void l(int i2) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 == null) {
                return;
            }
            if (this.l) {
                a2.r.setValue(i2);
            } else {
                a2.q.setValue(i2);
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        boolean z = false;
        cf2 cf2 = (cf2) qa.a(layoutInflater, R.layout.fragment_profile_goals_edit, viewGroup, false, O0());
        uh3 uh3 = this.j;
        if (uh3 != null) {
            if (uh3 != null) {
                if (uh3.i() == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                    z = true;
                }
                this.l = z;
                if (this.l) {
                    cf2.s.setType(GoalType.TOTAL_STEPS);
                    cf2.t.setType(GoalType.ACTIVE_TIME);
                    cf2.q.setType(GoalType.CALORIES);
                    cf2.r.setType(GoalType.TOTAL_SLEEP);
                } else {
                    cf2.s.setType(GoalType.TOTAL_STEPS);
                    cf2.t.setType(GoalType.CALORIES);
                    cf2.q.setType(GoalType.TOTAL_SLEEP);
                    cf2.r.setType(GoalType.GOAL_TRACKING);
                }
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
        cf2.s.setOnClickListener(new d(this));
        cf2.t.setOnClickListener(new e(this));
        cf2.q.setOnClickListener(new f(this));
        cf2.r.setOnClickListener(new g(this));
        FlexibleEditText flexibleEditText = cf2.v;
        kd4.a((Object) flexibleEditText, "binding.fetSleepHourValue");
        flexibleEditText.setOnFocusChangeListener(new h(this, cf2));
        FlexibleEditText flexibleEditText2 = cf2.w;
        kd4.a((Object) flexibleEditText2, "binding.fetSleepMinuteValue");
        flexibleEditText2.setOnFocusChangeListener(new i(this, cf2));
        cf2.u.addTextChangedListener(new j(this, cf2));
        cf2.v.addTextChangedListener(new k(this, cf2));
        cf2.w.addTextChangedListener(new b(this, cf2));
        cf2.C.setOnClickListener(new c(this));
        this.k = new tr3<>(this, cf2);
        R("set_goal_view");
        kd4.a((Object) cf2, "binding");
        return cf2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        FLogger.INSTANCE.getLocal().d(o, "onResume");
        super.onResume();
        uh3 uh3 = this.j;
        if (uh3 != null) {
            uh3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        uh3 uh3 = this.j;
        if (uh3 != null) {
            uh3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002d, code lost:
        if (r9 <= 999) goto L_0x0040;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x006e  */
    public final void p(int i2) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                int i3 = xh3.a[this.m.ordinal()];
                int i4 = 999;
                boolean z = true;
                if (i3 != 1) {
                    if (i3 != 2) {
                        if (i3 != 3) {
                            if (i3 == 4) {
                            }
                        } else if (i2 > 480) {
                            i4 = 480;
                            if (!z) {
                                uh3 uh3 = this.j;
                                if (uh3 != null) {
                                    uh3.a(i4, this.m);
                                    a2.u.setText(il2.c(i4));
                                    ds3 ds3 = ds3.c;
                                    FragmentManager childFragmentManager = getChildFragmentManager();
                                    kd4.a((Object) childFragmentManager, "childFragmentManager");
                                    ds3.a(childFragmentManager, this.m, i4);
                                    return;
                                }
                                kd4.d("mPresenter");
                                throw null;
                            }
                            uh3 uh32 = this.j;
                            if (uh32 != null) {
                                uh32.a(i2, this.m);
                                return;
                            } else {
                                kd4.d("mPresenter");
                                throw null;
                            }
                        }
                    } else if (i2 > 4800) {
                        i4 = 4800;
                        if (!z) {
                        }
                    }
                } else if (i2 > 50000) {
                    i4 = 50000;
                    if (!z) {
                    }
                }
                i4 = 0;
                z = false;
                if (!z) {
                }
            }
        }
    }

    @DexIgnore
    public void r0() {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.D;
                kd4.a((Object) linearLayout, "llGoalsValue");
                linearLayout.setVisibility(4);
                LinearLayout linearLayout2 = a2.E;
                kd4.a((Object) linearLayout2, "llSleepGoalValue");
                linearLayout2.setVisibility(4);
            }
        }
    }

    @DexIgnore
    public void w0() {
        if (isActive()) {
            a();
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.I(childFragmentManager);
        }
    }

    @DexIgnore
    public final void a(GoalType goalType) {
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                if (goalType != null) {
                    this.m = goalType;
                }
                CustomEditGoalView customEditGoalView = a2.s;
                kd4.a((Object) customEditGoalView, "cegvTopLeft");
                boolean z = true;
                customEditGoalView.setSelected(a2.s.getMGoalType() == goalType);
                CustomEditGoalView customEditGoalView2 = a2.t;
                kd4.a((Object) customEditGoalView2, "cegvTopRight");
                customEditGoalView2.setSelected(a2.t.getMGoalType() == goalType);
                CustomEditGoalView customEditGoalView3 = a2.q;
                kd4.a((Object) customEditGoalView3, "cegvBottomLeft");
                customEditGoalView3.setSelected(a2.q.getMGoalType() == goalType);
                CustomEditGoalView customEditGoalView4 = a2.r;
                kd4.a((Object) customEditGoalView4, "cegvBottomRight");
                if (a2.r.getMGoalType() != goalType) {
                    z = false;
                }
                customEditGoalView4.setSelected(z);
                b(goalType);
            }
        }
    }

    @DexIgnore
    public final int d(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = o;
        local.d(str, "updateSleepGoal minute: " + i2 + " hour: " + i3);
        tr3<cf2> tr3 = this.k;
        if (tr3 == null) {
            return 0;
        }
        cf2 a2 = tr3.a();
        if (a2 == null) {
            return 0;
        }
        int i4 = i2 + (i3 * 60);
        if (i4 <= 960) {
            return i4;
        }
        a2.w.setText("0");
        a2.v.setText(String.valueOf(16));
        return 960;
    }

    @DexIgnore
    public void d(int i2, String str) {
        kd4.b(str, "message");
        a();
        ds3 ds3 = ds3.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        ds3.a(i2, str, childFragmentManager);
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            if (hashCode != -1375614559) {
                if (hashCode == 1008390942 && str.equals("NO_INTERNET_CONNECTION") && i2 == R.id.tv_cancel) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                    }
                }
            } else if (!str.equals("UNSAVED_CHANGE")) {
            } else {
                if (i2 == R.id.tv_ok) {
                    b();
                    uh3 uh3 = this.j;
                    if (uh3 != null) {
                        uh3.k();
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                } else if (i2 == R.id.tv_cancel) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        activity2.finish();
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(uh3 uh3) {
        kd4.b(uh3, "presenter");
        this.j = uh3;
    }

    @DexIgnore
    public void a(ActivitySettings activitySettings) {
        kd4.b(activitySettings, "currentSettings");
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                a2.s.setValue(activitySettings.getCurrentStepGoal());
                if (this.l) {
                    a2.t.setValue(activitySettings.getCurrentActiveTimeGoal());
                    a2.q.setValue(activitySettings.getCurrentCaloriesGoal());
                } else {
                    a2.t.setValue(activitySettings.getCurrentCaloriesGoal());
                }
                a(this.m);
            }
        }
    }

    @DexIgnore
    public void a(int i2, GoalType goalType) {
        kd4.b(goalType, "type");
        tr3<cf2> tr3 = this.k;
        if (tr3 != null) {
            cf2 a2 = tr3.a();
            if (a2 != null) {
                int i3 = xh3.c[goalType.ordinal()];
                if (i3 == 1) {
                    a2.s.setValue(i2);
                } else if (i3 != 2) {
                    if (i3 == 3) {
                        a2.t.setValue(i2);
                    } else if (i3 != 4) {
                        if (i3 == 5) {
                            a2.r.setValue(i2);
                        }
                    } else if (this.l) {
                        a2.r.setValue(i2);
                    } else {
                        a2.q.setValue(i2);
                    }
                } else if (this.l) {
                    a2.q.setValue(i2);
                } else {
                    a2.t.setValue(i2);
                }
            }
        }
    }
}
