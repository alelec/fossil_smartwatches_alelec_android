package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pe2 extends oe2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public long x;

    /*
    static {
        z.put(R.id.iv_close, 1);
        z.put(R.id.progressBar, 2);
        z.put(R.id.ftv_title, 3);
        z.put(R.id.rv_list_device, 4);
        z.put(R.id.ftv_dont_see_device, 5);
        z.put(R.id.fb_start_pairing, 6);
    }
    */

    @DexIgnore
    public pe2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 7, y, z));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.x != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.x = 1;
        }
        g();
    }

    @DexIgnore
    public pe2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[6], objArr[5], objArr[3], objArr[1], objArr[2], objArr[4]);
        this.x = -1;
        this.w = objArr[0];
        this.w.setTag((Object) null);
        a(view);
        f();
    }
}
