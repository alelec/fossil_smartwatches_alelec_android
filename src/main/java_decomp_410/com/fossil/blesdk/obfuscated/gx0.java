package com.fossil.blesdk.obfuscated;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gx0 implements Iterator<String> {
    @DexIgnore
    public Iterator<String> e; // = this.f.e.iterator();
    @DexIgnore
    public /* final */ /* synthetic */ ex0 f;

    @DexIgnore
    public gx0(ex0 ex0) {
        this.f = ex0;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e.hasNext();
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return this.e.next();
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
