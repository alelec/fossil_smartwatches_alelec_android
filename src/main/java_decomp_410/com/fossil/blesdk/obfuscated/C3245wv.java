package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wv */
public abstract class C3245wv<T> implements com.fossil.blesdk.obfuscated.C1517bw<T> {

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f10713e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f10714f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2604ov f10715g;

    @DexIgnore
    public C3245wv() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14094a() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9312a(android.graphics.drawable.Drawable drawable) {
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9313a(com.fossil.blesdk.obfuscated.C1449aw awVar) {
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9314a(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        this.f10715g = ovVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14096b() {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9316b(android.graphics.drawable.Drawable drawable) {
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo9317b(com.fossil.blesdk.obfuscated.C1449aw awVar) {
        awVar.mo4027a(this.f10713e, this.f10714f);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14097c() {
    }

    @DexIgnore
    /* renamed from: d */
    public final com.fossil.blesdk.obfuscated.C2604ov mo9319d() {
        return this.f10715g;
    }

    @DexIgnore
    public C3245wv(int i, int i2) {
        if (com.fossil.blesdk.obfuscated.C3066uw.m14933b(i, i2)) {
            this.f10713e = i;
            this.f10714f = i2;
            return;
        }
        throw new java.lang.IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }
}
