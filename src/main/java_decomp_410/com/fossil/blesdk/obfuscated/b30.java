package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.file.FileType;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.model.enumerate.CommuteTimeWatchAppAction;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppId;
import com.fossil.blesdk.model.microapp.enumerate.MicroAppVariantId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class b30 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[FileType.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[MicroAppVariantId.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[MicroAppId.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[AsyncEventType.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e; // = new int[CommuteTimeWatchAppAction.values().length];

    /*
    static {
        a[FileType.HARDWARE_LOG.ordinal()] = 1;
        a[FileType.ALARM.ordinal()] = 2;
        a[FileType.ACTIVITY_FILE.ordinal()] = 3;
        a[FileType.NOTIFICATION_FILTER.ordinal()] = 4;
        a[FileType.DEVICE_CONFIG.ordinal()] = 5;
        a[FileType.DATA_COLLECTION_FILE.ordinal()] = 6;
        a[FileType.OTA.ordinal()] = 7;
        a[FileType.FONT.ordinal()] = 8;
        a[FileType.MUSIC_CONTROL.ordinal()] = 9;
        a[FileType.UI_SCRIPT.ordinal()] = 10;
        a[FileType.ASSET.ordinal()] = 11;
        a[FileType.NOTIFICATION.ordinal()] = 12;
        a[FileType.DEVICE_INFO.ordinal()] = 13;
        a[FileType.ALL_FILE.ordinal()] = 14;
        a[FileType.MICRO_APP.ordinal()] = 15;
        a[FileType.WATCH_PARAMETERS_FILE.ordinal()] = 16;
        a[FileType.UI_PACKAGE_FILE.ordinal()] = 17;
        a[FileType.LUTS_FILE.ordinal()] = 18;
        a[FileType.RATE_FILE.ordinal()] = 19;
        b[MicroAppVariantId.ETA.ordinal()] = 1;
        b[MicroAppVariantId.TRAVEL.ordinal()] = 2;
        c[MicroAppId.COMMUTE_TIME.ordinal()] = 1;
        c[MicroAppId.RING_PHONE.ordinal()] = 2;
        d[AsyncEventType.JSON_FILE_EVENT.ordinal()] = 1;
        d[AsyncEventType.HEARTBEAT_EVENT.ordinal()] = 2;
        d[AsyncEventType.SERVICE_CHANGE_EVENT.ordinal()] = 3;
        d[AsyncEventType.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 4;
        d[AsyncEventType.APP_NOTIFICATION_EVENT.ordinal()] = 5;
        d[AsyncEventType.MUSIC_EVENT.ordinal()] = 6;
        d[AsyncEventType.BACKGROUND_SYNC_EVENT.ordinal()] = 7;
        d[AsyncEventType.MICRO_APP_EVENT.ordinal()] = 8;
        d[AsyncEventType.TIME_SYNC_EVENT.ordinal()] = 9;
        e[CommuteTimeWatchAppAction.START.ordinal()] = 1;
        e[CommuteTimeWatchAppAction.STOP.ordinal()] = 2;
    }
    */
}
