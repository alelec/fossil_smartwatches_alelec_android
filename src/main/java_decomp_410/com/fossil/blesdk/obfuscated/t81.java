package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.t81;
import com.fossil.blesdk.obfuscated.t81.a;
import com.google.android.gms.internal.measurement.zztv;
import com.google.android.gms.internal.measurement.zzuv;
import com.google.android.gms.internal.measurement.zzxc;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class t81<MessageType extends t81<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends j71<MessageType, BuilderType> {
    @DexIgnore
    public static Map<Object, t81<?, ?>> zzbyh; // = new ConcurrentHashMap();
    @DexIgnore
    public fb1 zzbyf; // = fb1.d();
    @DexIgnore
    public int zzbyg; // = -1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends t81<T, ?>> extends l71<T> {
        @DexIgnore
        public /* final */ T a;

        @DexIgnore
        public b(T t) {
            this.a = t;
        }

        @DexIgnore
        public final /* synthetic */ Object a(y71 y71, i81 i81) throws zzuv {
            return t81.a(this.a, y71, i81);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<MessageType extends c<MessageType, BuilderType>, BuilderType> extends t81<MessageType, BuilderType> implements y91 {
        @DexIgnore
        public m81<Object> zzbyl; // = m81.i();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<ContainingType extends w91, Type> extends f81<ContainingType, Type> {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    /* 'enum' modifier removed */
    public static final class e {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ int e; // = 5;
        @DexIgnore
        public static /* final */ int f; // = 6;
        @DexIgnore
        public static /* final */ int g; // = 7;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] h; // = {a, b, c, d, e, f, g};
        @DexIgnore
        public static /* final */ int i; // = 1;
        @DexIgnore
        public static /* final */ int j; // = 2;
        @DexIgnore
        public static /* final */ int k; // = 1;
        @DexIgnore
        public static /* final */ int l; // = 2;

        /*
        static {
            int[] iArr = {i, j};
            int[] iArr2 = {k, l};
        }
        */

        @DexIgnore
        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    @DexIgnore
    public static <E> z81<E> i() {
        return ka1.b();
    }

    @DexIgnore
    public abstract Object a(int i, Object obj, Object obj2);

    @DexIgnore
    public final boolean a() {
        boolean booleanValue = Boolean.TRUE.booleanValue();
        byte byteValue = ((Byte) a(e.a, (Object) null, (Object) null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean c2 = ja1.a().a(this).c(this);
        if (booleanValue) {
            a(e.b, (Object) c2 ? this : null, (Object) null);
        }
        return c2;
    }

    @DexIgnore
    public final /* synthetic */ w91 b() {
        return (t81) a(e.f, (Object) null, (Object) null);
    }

    @DexIgnore
    public final /* synthetic */ x91 c() {
        a aVar = (a) a(e.e, (Object) null, (Object) null);
        aVar.a(this);
        return aVar;
    }

    @DexIgnore
    public final int e() {
        if (this.zzbyg == -1) {
            this.zzbyg = ja1.a().a(this).b(this);
        }
        return this.zzbyg;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!((t81) a(e.f, (Object) null, (Object) null)).getClass().isInstance(obj)) {
            return false;
        }
        return ja1.a().a(this).a(this, (t81) obj);
    }

    @DexIgnore
    public final /* synthetic */ x91 f() {
        return (a) a(e.e, (Object) null, (Object) null);
    }

    @DexIgnore
    public final int g() {
        return this.zzbyg;
    }

    @DexIgnore
    public final BuilderType h() {
        BuilderType buildertype = (a) a(e.e, (Object) null, (Object) null);
        buildertype.a(this);
        return buildertype;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.zzbtk;
        if (i != 0) {
            return i;
        }
        this.zzbtk = ja1.a().a(this).a(this);
        return this.zzbtk;
    }

    @DexIgnore
    public String toString() {
        return z91.a(this, super.toString());
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<MessageType extends t81<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> extends k71<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType e;
        @DexIgnore
        public MessageType f;
        @DexIgnore
        public boolean g; // = false;

        @DexIgnore
        public a(MessageType messagetype) {
            this.e = messagetype;
            this.f = (t81) messagetype.a(e.d, (Object) null, (Object) null);
        }

        @DexIgnore
        public final BuilderType a(MessageType messagetype) {
            g();
            a(this.f, messagetype);
            return this;
        }

        @DexIgnore
        public final /* synthetic */ w91 b() {
            return this.e;
        }

        @DexIgnore
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            a aVar = (a) this.e.a(e.e, (Object) null, (Object) null);
            aVar.a((t81) u());
            return aVar;
        }

        @DexIgnore
        public final void g() {
            if (this.g) {
                MessageType messagetype = (t81) this.f.a(e.d, (Object) null, (Object) null);
                a(messagetype, this.f);
                this.f = messagetype;
                this.g = false;
            }
        }

        @DexIgnore
        /* renamed from: h */
        public MessageType u() {
            if (this.g) {
                return this.f;
            }
            MessageType messagetype = this.f;
            ja1.a().a(messagetype).d(messagetype);
            this.g = true;
            return this.f;
        }

        @DexIgnore
        /* renamed from: i */
        public final MessageType t() {
            MessageType messagetype = (t81) u();
            boolean booleanValue = Boolean.TRUE.booleanValue();
            byte byteValue = ((Byte) messagetype.a(e.a, (Object) null, (Object) null)).byteValue();
            boolean z = true;
            if (byteValue != 1) {
                if (byteValue == 0) {
                    z = false;
                } else {
                    z = ja1.a().a(messagetype).c(messagetype);
                    if (booleanValue) {
                        messagetype.a(e.b, (Object) z ? messagetype : null, (Object) null);
                    }
                }
            }
            if (z) {
                return messagetype;
            }
            throw new zzxc(messagetype);
        }

        @DexIgnore
        public static void a(MessageType messagetype, MessageType messagetype2) {
            ja1.a().a(messagetype).b(messagetype, messagetype2);
        }

        @DexIgnore
        public final /* synthetic */ k71 a(j71 j71) {
            a((t81) j71);
            return this;
        }
    }

    @DexIgnore
    public final void a(int i) {
        this.zzbyg = i;
    }

    @DexIgnore
    public final void a(zztv zztv) throws IOException {
        ja1.a().a(getClass()).a(this, (sb1) d81.a(zztv));
    }

    @DexIgnore
    public static <T extends t81<?, ?>> T a(Class<T> cls) {
        T t = (t81) zzbyh.get(cls);
        if (t == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                t = (t81) zzbyh.get(cls);
            } catch (ClassNotFoundException e2) {
                throw new IllegalStateException("Class initialization cannot fail.", e2);
            }
        }
        if (t == null) {
            t = (t81) ((t81) kb1.a(cls)).a(e.f, (Object) null, (Object) null);
            if (t != null) {
                zzbyh.put(cls, t);
            } else {
                throw new IllegalStateException();
            }
        }
        return t;
    }

    @DexIgnore
    public static <T extends t81<?, ?>> void a(Class<T> cls, T t) {
        zzbyh.put(cls, t);
    }

    @DexIgnore
    public static Object a(w91 w91, String str, Object[] objArr) {
        return new la1(w91, str, objArr);
    }

    @DexIgnore
    public static Object a(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e2);
        } catch (InvocationTargetException e3) {
            Throwable cause = e3.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static <T extends t81<T, ?>> T a(T t, y71 y71, i81 i81) throws zzuv {
        T t2 = (t81) t.a(e.d, (Object) null, (Object) null);
        try {
            ja1.a().a(t2).a(t2, b81.a(y71), i81);
            ja1.a().a(t2).d(t2);
            return t2;
        } catch (IOException e2) {
            if (e2.getCause() instanceof zzuv) {
                throw ((zzuv) e2.getCause());
            }
            throw new zzuv(e2.getMessage()).zzg(t2);
        } catch (RuntimeException e3) {
            if (e3.getCause() instanceof zzuv) {
                throw ((zzuv) e3.getCause());
            }
            throw e3;
        }
    }
}
