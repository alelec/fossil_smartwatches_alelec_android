package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.View;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ck2 {
    @DexIgnore
    public static fk2 a(Context context) {
        return (fk2) rn.d(context);
    }

    @DexIgnore
    public static fk2 a(Fragment fragment) {
        return (fk2) rn.a(fragment);
    }

    @DexIgnore
    public static fk2 a(View view) {
        return (fk2) rn.a(view);
    }
}
