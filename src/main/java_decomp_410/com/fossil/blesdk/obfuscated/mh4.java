package com.fossil.blesdk.obfuscated;

import java.util.concurrent.CancellationException;
import kotlin.Result;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutinesInternalError;
import kotlinx.coroutines.internal.ThreadContextKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mh4<T> extends tk4 {
    @DexIgnore
    public int g;

    @DexIgnore
    public mh4(int i) {
        this.g = i;
    }

    @DexIgnore
    public final Throwable a(Object obj) {
        if (!(obj instanceof ng4)) {
            obj = null;
        }
        ng4 ng4 = (ng4) obj;
        if (ng4 != null) {
            return ng4.a;
        }
        return null;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
        kd4.b(th, "cause");
    }

    @DexIgnore
    public abstract yb4<T> b();

    @DexIgnore
    public abstract Object c();

    @DexIgnore
    public <T> T c(Object obj) {
        return obj;
    }

    @DexIgnore
    public final void run() {
        Object obj;
        CoroutineContext context;
        Object b;
        Object obj2;
        uk4 uk4 = this.f;
        try {
            yb4 b2 = b();
            if (b2 != null) {
                jh4 jh4 = (jh4) b2;
                yb4<T> yb4 = jh4.l;
                context = yb4.getContext();
                Object c = c();
                b = ThreadContextKt.b(context, jh4.j);
                Throwable a = a(c);
                fi4 fi4 = wi4.a(this.g) ? (fi4) context.get(fi4.d) : null;
                if (a == null && fi4 != null && !fi4.isActive()) {
                    CancellationException y = fi4.y();
                    a(c, (Throwable) y);
                    Result.a aVar = Result.Companion;
                    yb4.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(y, (yb4<?>) yb4))));
                } else if (a != null) {
                    Result.a aVar2 = Result.Companion;
                    yb4.resumeWith(Result.m3constructorimpl(na4.a(ck4.a(a, (yb4<?>) yb4))));
                } else {
                    Object c2 = c(c);
                    Result.a aVar3 = Result.Companion;
                    yb4.resumeWith(Result.m3constructorimpl(c2));
                }
                qa4 qa4 = qa4.a;
                ThreadContextKt.a(context, b);
                try {
                    Result.a aVar4 = Result.Companion;
                    uk4.A();
                    obj2 = Result.m3constructorimpl(qa4.a);
                } catch (Throwable th) {
                    Result.a aVar5 = Result.Companion;
                    obj2 = Result.m3constructorimpl(na4.a(th));
                }
                a((Throwable) null, Result.m6exceptionOrNullimpl(obj2));
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        } catch (Throwable th2) {
            try {
                Result.a aVar6 = Result.Companion;
                uk4.A();
                obj = Result.m3constructorimpl(qa4.a);
            } catch (Throwable th3) {
                Result.a aVar7 = Result.Companion;
                obj = Result.m3constructorimpl(na4.a(th3));
            }
            a(th2, Result.m6exceptionOrNullimpl(obj));
        }
    }

    @DexIgnore
    public final void a(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                ja4.a(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            String str = "Fatal exception in coroutines machinery for " + this + ". " + "Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (th != null) {
                wg4.a(b().getContext(), (Throwable) new CoroutinesInternalError(str, th));
                return;
            }
            kd4.a();
            throw null;
        }
    }
}
