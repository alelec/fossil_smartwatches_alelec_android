package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hd1 implements Parcelable.Creator<wc1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    z = SafeParcelReader.i(parcel, a);
                    break;
                case 2:
                    z2 = SafeParcelReader.i(parcel, a);
                    break;
                case 3:
                    z3 = SafeParcelReader.i(parcel, a);
                    break;
                case 4:
                    z4 = SafeParcelReader.i(parcel, a);
                    break;
                case 5:
                    z5 = SafeParcelReader.i(parcel, a);
                    break;
                case 6:
                    z6 = SafeParcelReader.i(parcel, a);
                    break;
                default:
                    SafeParcelReader.v(parcel, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel, b);
        return new wc1(z, z2, z3, z4, z5, z6);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new wc1[i];
    }
}
