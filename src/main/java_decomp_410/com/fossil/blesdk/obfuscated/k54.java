package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.LegacyTokenHelper;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class k54 {
    @DexIgnore
    public String a() {
        return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
    }

    @DexIgnore
    public String a(Context context) {
        return new t54().a(context);
    }

    @DexIgnore
    public String b(Context context) {
        String str = null;
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle == null) {
                return null;
            }
            String string = bundle.getString("io.fabric.ApiKey");
            try {
                if ("@string/twitter_consumer_secret".equals(string)) {
                    q44.g().d("Fabric", "Ignoring bad default value for Fabric ApiKey set by FirebaseUI-Auth");
                } else {
                    str = string;
                }
                if (str != null) {
                    return str;
                }
                q44.g().d("Fabric", "Falling back to Crashlytics key lookup from Manifest");
                return bundle.getString("com.crashlytics.ApiKey");
            } catch (Exception e) {
                e = e;
                str = string;
                y44 g = q44.g();
                g.d("Fabric", "Caught non-fatal exception while retrieving apiKey: " + e);
                return str;
            }
        } catch (Exception e2) {
            e = e2;
            y44 g2 = q44.g();
            g2.d("Fabric", "Caught non-fatal exception while retrieving apiKey: " + e);
            return str;
        }
    }

    @DexIgnore
    public String c(Context context) {
        int a = CommonUtils.a(context, "io.fabric.ApiKey", LegacyTokenHelper.TYPE_STRING);
        if (a == 0) {
            q44.g().d("Fabric", "Falling back to Crashlytics key lookup from Strings");
            a = CommonUtils.a(context, "com.crashlytics.ApiKey", LegacyTokenHelper.TYPE_STRING);
        }
        if (a != 0) {
            return context.getResources().getString(a);
        }
        return null;
    }

    @DexIgnore
    public String d(Context context) {
        String b = b(context);
        if (TextUtils.isEmpty(b)) {
            b = c(context);
        }
        if (TextUtils.isEmpty(b)) {
            b = a(context);
        }
        if (TextUtils.isEmpty(b)) {
            e(context);
        }
        return b;
    }

    @DexIgnore
    public void e(Context context) {
        if (q44.h() || CommonUtils.j(context)) {
            throw new IllegalArgumentException(a());
        }
        q44.g().e("Fabric", a());
    }
}
