package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gc */
public class C1866gc {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.lifecycle.LifecycleRegistry f5412a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Handler f5413b; // = new android.os.Handler();

    @DexIgnore
    /* renamed from: c */
    public com.fossil.blesdk.obfuscated.C1866gc.C1867a f5414c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.gc$a")
    /* renamed from: com.fossil.blesdk.obfuscated.gc$a */
    public static class C1867a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ androidx.lifecycle.LifecycleRegistry f5415e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ androidx.lifecycle.Lifecycle.Event f5416f;

        @DexIgnore
        /* renamed from: g */
        public boolean f5417g; // = false;

        @DexIgnore
        public C1867a(androidx.lifecycle.LifecycleRegistry lifecycleRegistry, androidx.lifecycle.Lifecycle.Event event) {
            this.f5415e = lifecycleRegistry;
            this.f5416f = event;
        }

        @DexIgnore
        public void run() {
            if (!this.f5417g) {
                this.f5415e.mo2263a(this.f5416f);
                this.f5417g = true;
            }
        }
    }

    @DexIgnore
    public C1866gc(androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        this.f5412a = new androidx.lifecycle.LifecycleRegistry(lifecycleOwner);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11187a(androidx.lifecycle.Lifecycle.Event event) {
        com.fossil.blesdk.obfuscated.C1866gc.C1867a aVar = this.f5414c;
        if (aVar != null) {
            aVar.run();
        }
        this.f5414c = new com.fossil.blesdk.obfuscated.C1866gc.C1867a(this.f5412a, event);
        this.f5413b.postAtFrontOfQueue(this.f5414c);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11188b() {
        mo11187a(androidx.lifecycle.Lifecycle.Event.ON_START);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11189c() {
        mo11187a(androidx.lifecycle.Lifecycle.Event.ON_CREATE);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo11190d() {
        mo11187a(androidx.lifecycle.Lifecycle.Event.ON_STOP);
        mo11187a(androidx.lifecycle.Lifecycle.Event.ON_DESTROY);
    }

    @DexIgnore
    /* renamed from: e */
    public void mo11191e() {
        mo11187a(androidx.lifecycle.Lifecycle.Event.ON_START);
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.lifecycle.Lifecycle mo11186a() {
        return this.f5412a;
    }
}
