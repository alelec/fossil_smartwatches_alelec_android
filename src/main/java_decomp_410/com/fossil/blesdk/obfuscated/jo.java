package com.fossil.blesdk.obfuscated;

import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface jo {
    @DexIgnore
    public static final Charset a = Charset.forName("UTF-8");

    @DexIgnore
    void a(MessageDigest messageDigest);

    @DexIgnore
    boolean equals(Object obj);

    @DexIgnore
    int hashCode();
}
