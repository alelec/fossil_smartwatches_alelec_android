package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ci */
public class C1557ci extends com.fossil.blesdk.obfuscated.C1803fi {

    @DexIgnore
    /* renamed from: b */
    public static java.lang.reflect.Method f4100b;

    @DexIgnore
    /* renamed from: c */
    public static boolean f4101c;

    @DexIgnore
    /* renamed from: d */
    public static java.lang.reflect.Method f4102d;

    @DexIgnore
    /* renamed from: e */
    public static boolean f4103e;

    @DexIgnore
    /* renamed from: a */
    public void mo9527a(android.view.View view) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9528a(android.view.View view, float f) {
        mo9530b();
        java.lang.reflect.Method method = f4100b;
        if (method != null) {
            try {
                method.invoke(view, new java.lang.Object[]{java.lang.Float.valueOf(f)});
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        } else {
            view.setAlpha(f);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public float mo9529b(android.view.View view) {
        mo9526a();
        java.lang.reflect.Method method = f4102d;
        if (method != null) {
            try {
                return ((java.lang.Float) method.invoke(view, new java.lang.Object[0])).floatValue();
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
        return super.mo9529b(view);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9531c(android.view.View view) {
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9526a() {
        if (!f4103e) {
            try {
                f4102d = android.view.View.class.getDeclaredMethod("getTransitionAlpha", new java.lang.Class[0]);
                f4102d.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi19", "Failed to retrieve getTransitionAlpha method", e);
            }
            f4103e = true;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo9530b() {
        if (!f4101c) {
            try {
                f4100b = android.view.View.class.getDeclaredMethod("setTransitionAlpha", new java.lang.Class[]{java.lang.Float.TYPE});
                f4100b.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi19", "Failed to retrieve setTransitionAlpha method", e);
            }
            f4101c = true;
        }
    }
}
