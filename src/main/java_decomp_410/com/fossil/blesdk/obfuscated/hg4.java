package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hg4 extends gi4<fi4> {
    @DexIgnore
    public /* final */ eg4<?> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hg4(fi4 fi4, eg4<?> eg4) {
        super(fi4);
        kd4.b(fi4, "parent");
        kd4.b(eg4, "child");
        this.i = eg4;
    }

    @DexIgnore
    public void b(Throwable th) {
        eg4<?> eg4 = this.i;
        eg4.a(eg4.a((fi4) this.h));
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "ChildContinuation[" + this.i + ']';
    }
}
