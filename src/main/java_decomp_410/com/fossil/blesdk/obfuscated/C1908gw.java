package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gw */
public final class C1908gw implements com.fossil.blesdk.obfuscated.C2143jo {

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f5572b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2143jo f5573c;

    @DexIgnore
    public C1908gw(int i, com.fossil.blesdk.obfuscated.C2143jo joVar) {
        this.f5572b = i;
        this.f5573c = joVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2143jo m7612a(android.content.Context context) {
        return new com.fossil.blesdk.obfuscated.C1908gw(context.getResources().getConfiguration().uiMode & 48, com.fossil.blesdk.obfuscated.C1977hw.m8008b(context));
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C1908gw)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1908gw gwVar = (com.fossil.blesdk.obfuscated.C1908gw) obj;
        if (this.f5572b != gwVar.f5572b || !this.f5573c.equals(gwVar.f5573c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return com.fossil.blesdk.obfuscated.C3066uw.m14923a((java.lang.Object) this.f5573c, this.f5572b);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8934a(java.security.MessageDigest messageDigest) {
        this.f5573c.mo8934a(messageDigest);
        messageDigest.update(java.nio.ByteBuffer.allocate(4).putInt(this.f5572b).array());
    }
}
