package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h71 implements t61 {
    @DexIgnore
    public static /* final */ Map<String, h71> f; // = new HashMap();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences.OnSharedPreferenceChangeListener b; // = new i71(this);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, ?> d;
    @DexIgnore
    public /* final */ List<s61> e; // = new ArrayList();

    @DexIgnore
    public h71(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
        this.a.registerOnSharedPreferenceChangeListener(this.b);
    }

    @DexIgnore
    public static h71 a(Context context, String str) {
        h71 h71;
        SharedPreferences sharedPreferences;
        if (!((!n61.a() || str.startsWith("direct_boot:")) ? true : n61.a(context))) {
            return null;
        }
        synchronized (h71.class) {
            h71 = f.get(str);
            if (h71 == null) {
                if (str.startsWith("direct_boot:")) {
                    if (n61.a()) {
                        context = context.createDeviceProtectedStorageContext();
                    }
                    sharedPreferences = context.getSharedPreferences(str.substring(12), 0);
                } else {
                    sharedPreferences = context.getSharedPreferences(str, 0);
                }
                h71 = new h71(sharedPreferences);
                f.put(str, h71);
            }
        }
        return h71;
    }

    @DexIgnore
    public final Object a(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    map = this.a.getAll();
                    this.d = map;
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    @DexIgnore
    public final /* synthetic */ void a(SharedPreferences sharedPreferences, String str) {
        synchronized (this.c) {
            this.d = null;
            a71.f();
        }
        synchronized (this) {
            for (s61 a2 : this.e) {
                a2.a();
            }
        }
    }
}
