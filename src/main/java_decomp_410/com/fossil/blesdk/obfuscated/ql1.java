package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ql1 {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public long B;
    @DexIgnore
    public long C;
    @DexIgnore
    public /* final */ xh1 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public String j;
    @DexIgnore
    public long k;
    @DexIgnore
    public String l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public long p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public String s;
    @DexIgnore
    public long t;
    @DexIgnore
    public long u;
    @DexIgnore
    public long v;
    @DexIgnore
    public long w;
    @DexIgnore
    public long x;
    @DexIgnore
    public long y;
    @DexIgnore
    public String z;

    @DexIgnore
    public ql1(xh1 xh1, String str) {
        bk0.a(xh1);
        bk0.b(str);
        this.a = xh1;
        this.b = str;
        this.a.a().e();
    }

    @DexIgnore
    public final String A() {
        this.a.a().e();
        String str = this.z;
        h((String) null);
        return str;
    }

    @DexIgnore
    public final long B() {
        this.a.a().e();
        return this.p;
    }

    @DexIgnore
    public final boolean C() {
        this.a.a().e();
        return this.q;
    }

    @DexIgnore
    public final boolean D() {
        this.a.a().e();
        return this.r;
    }

    @DexIgnore
    public final String a() {
        this.a.a().e();
        return this.c;
    }

    @DexIgnore
    public final void b(String str) {
        this.a.a().e();
        this.A |= !nl1.e(this.c, str);
        this.c = str;
    }

    @DexIgnore
    public final String c() {
        this.a.a().e();
        return this.d;
    }

    @DexIgnore
    public final void d(String str) {
        this.a.a().e();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.A |= !nl1.e(this.s, str);
        this.s = str;
    }

    @DexIgnore
    public final void e(String str) {
        this.a.a().e();
        this.A |= !nl1.e(this.e, str);
        this.e = str;
    }

    @DexIgnore
    public final String f() {
        this.a.a().e();
        return this.b;
    }

    @DexIgnore
    public final void g() {
        this.a.a().e();
        this.A = false;
    }

    @DexIgnore
    public final String h() {
        this.a.a().e();
        return this.s;
    }

    @DexIgnore
    public final String i() {
        this.a.a().e();
        return this.e;
    }

    @DexIgnore
    public final long j() {
        this.a.a().e();
        return this.h;
    }

    @DexIgnore
    public final long k() {
        this.a.a().e();
        return this.i;
    }

    @DexIgnore
    public final long l() {
        this.a.a().e();
        return this.k;
    }

    @DexIgnore
    public final String m() {
        this.a.a().e();
        return this.l;
    }

    @DexIgnore
    public final long n() {
        this.a.a().e();
        return this.m;
    }

    @DexIgnore
    public final long o() {
        this.a.a().e();
        return this.n;
    }

    @DexIgnore
    public final long p() {
        this.a.a().e();
        return this.g;
    }

    @DexIgnore
    public final long q() {
        this.a.a().e();
        return this.B;
    }

    @DexIgnore
    public final long r() {
        this.a.a().e();
        return this.C;
    }

    @DexIgnore
    public final void s() {
        this.a.a().e();
        long j2 = this.g + 1;
        if (j2 > 2147483647L) {
            this.a.d().v().a("Bundle index overflow. appId", tg1.a(this.b));
            j2 = 0;
        }
        this.A = true;
        this.g = j2;
    }

    @DexIgnore
    public final long t() {
        this.a.a().e();
        return this.t;
    }

    @DexIgnore
    public final long u() {
        this.a.a().e();
        return this.u;
    }

    @DexIgnore
    public final long v() {
        this.a.a().e();
        return this.v;
    }

    @DexIgnore
    public final long w() {
        this.a.a().e();
        return this.w;
    }

    @DexIgnore
    public final long x() {
        this.a.a().e();
        return this.y;
    }

    @DexIgnore
    public final long y() {
        this.a.a().e();
        return this.x;
    }

    @DexIgnore
    public final String z() {
        this.a.a().e();
        return this.z;
    }

    @DexIgnore
    public final void a(String str) {
        this.a.a().e();
        this.A |= !nl1.e(this.j, str);
        this.j = str;
    }

    @DexIgnore
    public final void c(String str) {
        this.a.a().e();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.A |= !nl1.e(this.d, str);
        this.d = str;
    }

    @DexIgnore
    public final void f(String str) {
        this.a.a().e();
        this.A |= !nl1.e(this.f, str);
        this.f = str;
    }

    @DexIgnore
    public final void g(String str) {
        this.a.a().e();
        this.A |= !nl1.e(this.l, str);
        this.l = str;
    }

    @DexIgnore
    public final void h(long j2) {
        this.a.a().e();
        this.A |= this.n != j2;
        this.n = j2;
    }

    @DexIgnore
    public final void i(long j2) {
        boolean z2 = true;
        bk0.a(j2 >= 0);
        this.a.a().e();
        boolean z3 = this.A;
        if (this.g == j2) {
            z2 = false;
        }
        this.A = z2 | z3;
        this.g = j2;
    }

    @DexIgnore
    public final void j(long j2) {
        this.a.a().e();
        this.A |= this.B != j2;
        this.B = j2;
    }

    @DexIgnore
    public final void k(long j2) {
        this.a.a().e();
        this.A |= this.C != j2;
        this.C = j2;
    }

    @DexIgnore
    public final void l(long j2) {
        this.a.a().e();
        this.A |= this.t != j2;
        this.t = j2;
    }

    @DexIgnore
    public final void m(long j2) {
        this.a.a().e();
        this.A |= this.u != j2;
        this.u = j2;
    }

    @DexIgnore
    public final void n(long j2) {
        this.a.a().e();
        this.A |= this.v != j2;
        this.v = j2;
    }

    @DexIgnore
    public final void o(long j2) {
        this.a.a().e();
        this.A |= this.w != j2;
        this.w = j2;
    }

    @DexIgnore
    public final String b() {
        this.a.a().e();
        return this.f;
    }

    @DexIgnore
    public final void e(long j2) {
        this.a.a().e();
        this.A |= this.i != j2;
        this.i = j2;
    }

    @DexIgnore
    public final void d(long j2) {
        this.a.a().e();
        this.A |= this.h != j2;
        this.h = j2;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.a.a().e();
        this.A |= this.o != z2;
        this.o = z2;
    }

    @DexIgnore
    public final void b(long j2) {
        this.a.a().e();
        this.A |= this.x != j2;
        this.x = j2;
    }

    @DexIgnore
    public final void f(long j2) {
        this.a.a().e();
        this.A |= this.k != j2;
        this.k = j2;
    }

    @DexIgnore
    public final void g(long j2) {
        this.a.a().e();
        this.A |= this.m != j2;
        this.m = j2;
    }

    @DexIgnore
    public final void h(String str) {
        this.a.a().e();
        this.A |= !nl1.e(this.z, str);
        this.z = str;
    }

    @DexIgnore
    public final void c(long j2) {
        this.a.a().e();
        this.A |= this.p != j2;
        this.p = j2;
    }

    @DexIgnore
    public final String e() {
        this.a.a().e();
        return this.j;
    }

    @DexIgnore
    public final boolean d() {
        this.a.a().e();
        return this.o;
    }

    @DexIgnore
    public final void a(long j2) {
        this.a.a().e();
        this.A |= this.y != j2;
        this.y = j2;
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.a.a().e();
        this.A = this.q != z2;
        this.q = z2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.a.a().e();
        this.A = this.r != z2;
        this.r = z2;
    }
}
