package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.c2 */
public final class C1526c2 {

    @DexIgnore
    /* renamed from: g */
    public static /* final */ android.graphics.PorterDuff.Mode f3958g; // = android.graphics.PorterDuff.Mode.SRC_IN;

    @DexIgnore
    /* renamed from: h */
    public static com.fossil.blesdk.obfuscated.C1526c2 f3959h;

    @DexIgnore
    /* renamed from: i */
    public static /* final */ com.fossil.blesdk.obfuscated.C1526c2.C1529c f3960i; // = new com.fossil.blesdk.obfuscated.C1526c2.C1529c(6);

    @DexIgnore
    /* renamed from: j */
    public static /* final */ int[] f3961j; // = {com.fossil.blesdk.obfuscated.C3076v.abc_textfield_search_default_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_textfield_default_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_ab_share_pack_mtrl_alpha};

    @DexIgnore
    /* renamed from: k */
    public static /* final */ int[] f3962k; // = {com.fossil.blesdk.obfuscated.C3076v.abc_ic_commit_search_api_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_seekbar_tick_mark_material, com.fossil.blesdk.obfuscated.C3076v.abc_ic_menu_share_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_ic_menu_copy_mtrl_am_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_ic_menu_cut_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_ic_menu_selectall_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_ic_menu_paste_mtrl_am_alpha};

    @DexIgnore
    /* renamed from: l */
    public static /* final */ int[] f3963l; // = {com.fossil.blesdk.obfuscated.C3076v.abc_textfield_activated_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_textfield_search_activated_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_cab_background_top_mtrl_alpha, com.fossil.blesdk.obfuscated.C3076v.abc_text_cursor_material, com.fossil.blesdk.obfuscated.C3076v.abc_text_select_handle_left_mtrl_dark, com.fossil.blesdk.obfuscated.C3076v.abc_text_select_handle_middle_mtrl_dark, com.fossil.blesdk.obfuscated.C3076v.abc_text_select_handle_right_mtrl_dark, com.fossil.blesdk.obfuscated.C3076v.abc_text_select_handle_left_mtrl_light, com.fossil.blesdk.obfuscated.C3076v.abc_text_select_handle_middle_mtrl_light, com.fossil.blesdk.obfuscated.C3076v.abc_text_select_handle_right_mtrl_light};

    @DexIgnore
    /* renamed from: m */
    public static /* final */ int[] f3964m; // = {com.fossil.blesdk.obfuscated.C3076v.abc_popup_background_mtrl_mult, com.fossil.blesdk.obfuscated.C3076v.abc_cab_background_internal_bg, com.fossil.blesdk.obfuscated.C3076v.abc_menu_hardkey_panel_mtrl_mult};

    @DexIgnore
    /* renamed from: n */
    public static /* final */ int[] f3965n; // = {com.fossil.blesdk.obfuscated.C3076v.abc_tab_indicator_material, com.fossil.blesdk.obfuscated.C3076v.abc_textfield_search_material};

    @DexIgnore
    /* renamed from: o */
    public static /* final */ int[] f3966o; // = {com.fossil.blesdk.obfuscated.C3076v.abc_btn_check_material, com.fossil.blesdk.obfuscated.C3076v.abc_btn_radio_material};

    @DexIgnore
    /* renamed from: a */
    public java.util.WeakHashMap<android.content.Context, androidx.collection.SparseArrayCompat<android.content.res.ColorStateList>> f3967a;

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, com.fossil.blesdk.obfuscated.C1526c2.C1530d> f3968b;

    @DexIgnore
    /* renamed from: c */
    public androidx.collection.SparseArrayCompat<java.lang.String> f3969c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.WeakHashMap<android.content.Context, com.fossil.blesdk.obfuscated.C2103j4<java.lang.ref.WeakReference<android.graphics.drawable.Drawable.ConstantState>>> f3970d; // = new java.util.WeakHashMap<>(0);

    @DexIgnore
    /* renamed from: e */
    public android.util.TypedValue f3971e;

    @DexIgnore
    /* renamed from: f */
    public boolean f3972f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c2$a")
    /* renamed from: com.fossil.blesdk.obfuscated.c2$a */
    public static class C1527a implements com.fossil.blesdk.obfuscated.C1526c2.C1530d {
        @DexIgnore
        /* renamed from: a */
        public android.graphics.drawable.Drawable mo9403a(android.content.Context context, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) {
            try {
                return com.fossil.blesdk.obfuscated.C2444n0.m10920e(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (java.lang.Exception e) {
                android.util.Log.e("AsldcInflateDelegate", "Exception while inflating <animated-selector>", e);
                return null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c2$b")
    /* renamed from: com.fossil.blesdk.obfuscated.c2$b */
    public static class C1528b implements com.fossil.blesdk.obfuscated.C1526c2.C1530d {
        @DexIgnore
        /* renamed from: a */
        public android.graphics.drawable.Drawable mo9403a(android.content.Context context, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) {
            try {
                return com.fossil.blesdk.obfuscated.C2415mi.m10769a(context, context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (java.lang.Exception e) {
                android.util.Log.e("AvdcInflateDelegate", "Exception while inflating <animated-vector>", e);
                return null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c2$c")
    /* renamed from: com.fossil.blesdk.obfuscated.c2$c */
    public static class C1529c extends com.fossil.blesdk.obfuscated.C2183k4<java.lang.Integer, android.graphics.PorterDuffColorFilter> {
        @DexIgnore
        public C1529c(int i) {
            super(i);
        }

        @DexIgnore
        /* renamed from: b */
        public static int m5245b(int i, android.graphics.PorterDuff.Mode mode) {
            return ((i + 31) * 31) + mode.hashCode();
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.PorterDuffColorFilter mo9404a(int i, android.graphics.PorterDuff.Mode mode) {
            return (android.graphics.PorterDuffColorFilter) mo12627b(java.lang.Integer.valueOf(m5245b(i, mode)));
        }

        @DexIgnore
        /* renamed from: a */
        public android.graphics.PorterDuffColorFilter mo9405a(int i, android.graphics.PorterDuff.Mode mode, android.graphics.PorterDuffColorFilter porterDuffColorFilter) {
            return (android.graphics.PorterDuffColorFilter) mo12623a(java.lang.Integer.valueOf(m5245b(i, mode)), porterDuffColorFilter);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.c2$d */
    public interface C1530d {
        @DexIgnore
        /* renamed from: a */
        android.graphics.drawable.Drawable mo9403a(android.content.Context context, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.c2$e")
    /* renamed from: com.fossil.blesdk.obfuscated.c2$e */
    public static class C1531e implements com.fossil.blesdk.obfuscated.C1526c2.C1530d {
        @DexIgnore
        /* renamed from: a */
        public android.graphics.drawable.Drawable mo9403a(android.content.Context context, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) {
            try {
                return com.fossil.blesdk.obfuscated.C2879si.createFromXmlInner(context.getResources(), xmlPullParser, attributeSet, theme);
            } catch (java.lang.Exception e) {
                android.util.Log.e("VdcInflateDelegate", "Exception while inflating <vector>", e);
                return null;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static synchronized com.fossil.blesdk.obfuscated.C1526c2 m5217a() {
        com.fossil.blesdk.obfuscated.C1526c2 c2Var;
        synchronized (com.fossil.blesdk.obfuscated.C1526c2.class) {
            if (f3959h == null) {
                f3959h = new com.fossil.blesdk.obfuscated.C1526c2();
                m5220a(f3959h);
            }
            c2Var = f3959h;
        }
        return c2Var;
    }

    @DexIgnore
    /* renamed from: b */
    public final android.graphics.drawable.Drawable mo9394b(android.content.Context context, int i) {
        if (this.f3971e == null) {
            this.f3971e = new android.util.TypedValue();
        }
        android.util.TypedValue typedValue = this.f3971e;
        context.getResources().getValue(i, typedValue, true);
        long a = m5213a(typedValue);
        android.graphics.drawable.Drawable a2 = mo9387a(context, a);
        if (a2 != null) {
            return a2;
        }
        if (i == com.fossil.blesdk.obfuscated.C3076v.abc_cab_background_top_material) {
            a2 = new android.graphics.drawable.LayerDrawable(new android.graphics.drawable.Drawable[]{mo9396c(context, com.fossil.blesdk.obfuscated.C3076v.abc_cab_background_internal_bg), mo9396c(context, com.fossil.blesdk.obfuscated.C3076v.abc_cab_background_top_mtrl_alpha)});
        }
        if (a2 != null) {
            a2.setChangingConfigurations(typedValue.changingConfigurations);
            mo9392a(context, a, a2);
        }
        return a2;
    }

    @DexIgnore
    /* renamed from: c */
    public synchronized android.graphics.drawable.Drawable mo9396c(android.content.Context context, int i) {
        return mo9385a(context, i, false);
    }

    @DexIgnore
    /* renamed from: d */
    public synchronized android.content.res.ColorStateList mo9398d(android.content.Context context, int i) {
        android.content.res.ColorStateList e;
        e = mo9400e(context, i);
        if (e == null) {
            if (i == com.fossil.blesdk.obfuscated.C3076v.abc_edit_text_material) {
                e = com.fossil.blesdk.obfuscated.C2364m0.m10496b(context, com.fossil.blesdk.obfuscated.C2934t.abc_tint_edittext);
            } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_switch_track_mtrl_alpha) {
                e = com.fossil.blesdk.obfuscated.C2364m0.m10496b(context, com.fossil.blesdk.obfuscated.C2934t.abc_tint_switch_track);
            } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_switch_thumb_material) {
                e = mo9399e(context);
            } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_btn_default_mtrl_shape) {
                e = mo9397d(context);
            } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_btn_borderless_material) {
                e = mo9393b(context);
            } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_btn_colored_material) {
                e = mo9395c(context);
            } else {
                if (i != com.fossil.blesdk.obfuscated.C3076v.abc_spinner_mtrl_am_alpha) {
                    if (i != com.fossil.blesdk.obfuscated.C3076v.abc_spinner_textfield_background_material) {
                        if (m5223a(f3962k, i)) {
                            e = com.fossil.blesdk.obfuscated.C3003u2.m14535c(context, com.fossil.blesdk.obfuscated.C2777r.colorControlNormal);
                        } else if (m5223a(f3965n, i)) {
                            e = com.fossil.blesdk.obfuscated.C2364m0.m10496b(context, com.fossil.blesdk.obfuscated.C2934t.abc_tint_default);
                        } else if (m5223a(f3966o, i)) {
                            e = com.fossil.blesdk.obfuscated.C2364m0.m10496b(context, com.fossil.blesdk.obfuscated.C2934t.abc_tint_btn_checkable);
                        } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_seekbar_thumb_material) {
                            e = com.fossil.blesdk.obfuscated.C2364m0.m10496b(context, com.fossil.blesdk.obfuscated.C2934t.abc_tint_seek_thumb);
                        }
                    }
                }
                e = com.fossil.blesdk.obfuscated.C2364m0.m10496b(context, com.fossil.blesdk.obfuscated.C2934t.abc_tint_spinner);
            }
            if (e != null) {
                mo9390a(context, i, e);
            }
        }
        return e;
    }

    @DexIgnore
    /* renamed from: e */
    public final android.content.res.ColorStateList mo9400e(android.content.Context context, int i) {
        java.util.WeakHashMap<android.content.Context, androidx.collection.SparseArrayCompat<android.content.res.ColorStateList>> weakHashMap = this.f3967a;
        if (weakHashMap == null) {
            return null;
        }
        androidx.collection.SparseArrayCompat sparseArrayCompat = weakHashMap.get(context);
        if (sparseArrayCompat != null) {
            return (android.content.res.ColorStateList) sparseArrayCompat.mo1242b(i);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: f */
    public synchronized void mo9402f(android.content.Context context) {
        com.fossil.blesdk.obfuscated.C2103j4 j4Var = this.f3970d.get(context);
        if (j4Var != null) {
            j4Var.mo12213a();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final android.content.res.ColorStateList mo9395c(android.content.Context context) {
        return mo9384a(context, com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorAccent));
    }

    @DexIgnore
    /* renamed from: e */
    public final android.content.res.ColorStateList mo9399e(android.content.Context context) {
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        android.content.res.ColorStateList c = com.fossil.blesdk.obfuscated.C3003u2.m14535c(context, com.fossil.blesdk.obfuscated.C2777r.colorSwitchThumbNormal);
        if (c == null || !c.isStateful()) {
            iArr[0] = com.fossil.blesdk.obfuscated.C3003u2.f9835b;
            iArr2[0] = com.fossil.blesdk.obfuscated.C3003u2.m14531a(context, com.fossil.blesdk.obfuscated.C2777r.colorSwitchThumbNormal);
            iArr[1] = com.fossil.blesdk.obfuscated.C3003u2.f9838e;
            iArr2[1] = com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlActivated);
            iArr[2] = com.fossil.blesdk.obfuscated.C3003u2.f9839f;
            iArr2[2] = com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorSwitchThumbNormal);
        } else {
            iArr[0] = com.fossil.blesdk.obfuscated.C3003u2.f9835b;
            iArr2[0] = c.getColorForState(iArr[0], 0);
            iArr[1] = com.fossil.blesdk.obfuscated.C3003u2.f9838e;
            iArr2[1] = com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlActivated);
            iArr[2] = com.fossil.blesdk.obfuscated.C3003u2.f9839f;
            iArr2[2] = c.getDefaultColor();
        }
        return new android.content.res.ColorStateList(iArr, iArr2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073 A[Catch:{ Exception -> 0x00a2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a A[Catch:{ Exception -> 0x00a2 }] */
    /* renamed from: f */
    public final android.graphics.drawable.Drawable mo9401f(android.content.Context context, int i) {
        int next;
        com.fossil.blesdk.obfuscated.C1855g4<java.lang.String, com.fossil.blesdk.obfuscated.C1526c2.C1530d> g4Var = this.f3968b;
        if (g4Var == null || g4Var.isEmpty()) {
            return null;
        }
        androidx.collection.SparseArrayCompat<java.lang.String> sparseArrayCompat = this.f3969c;
        if (sparseArrayCompat != null) {
            java.lang.String b = sparseArrayCompat.mo1242b(i);
            if ("appcompat_skip_skip".equals(b) || (b != null && this.f3968b.get(b) == null)) {
                return null;
            }
        } else {
            this.f3969c = new androidx.collection.SparseArrayCompat<>();
        }
        if (this.f3971e == null) {
            this.f3971e = new android.util.TypedValue();
        }
        android.util.TypedValue typedValue = this.f3971e;
        android.content.res.Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        long a = m5213a(typedValue);
        android.graphics.drawable.Drawable a2 = mo9387a(context, a);
        if (a2 != null) {
            return a2;
        }
        java.lang.CharSequence charSequence = typedValue.string;
        if (charSequence != null && charSequence.toString().endsWith(".xml")) {
            try {
                android.content.res.XmlResourceParser xml = resources.getXml(i);
                android.util.AttributeSet asAttributeSet = android.util.Xml.asAttributeSet(xml);
                while (true) {
                    next = xml.next();
                    if (next == 2 || next == 1) {
                        if (next != 2) {
                            java.lang.String name = xml.getName();
                            this.f3969c.mo1241a(i, name);
                            com.fossil.blesdk.obfuscated.C1526c2.C1530d dVar = this.f3968b.get(name);
                            if (dVar != null) {
                                a2 = dVar.mo9403a(context, xml, asAttributeSet, context.getTheme());
                            }
                            if (a2 != null) {
                                a2.setChangingConfigurations(typedValue.changingConfigurations);
                                mo9392a(context, a, a2);
                            }
                        } else {
                            throw new org.xmlpull.v1.XmlPullParserException("No start tag found");
                        }
                    }
                }
                if (next != 2) {
                }
            } catch (java.lang.Exception e) {
                android.util.Log.e("AppCompatDrawableManag", "Exception while inflating drawable", e);
            }
        }
        if (a2 == null) {
            this.f3969c.mo1241a(i, "appcompat_skip_skip");
        }
        return a2;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5220a(com.fossil.blesdk.obfuscated.C1526c2 c2Var) {
        if (android.os.Build.VERSION.SDK_INT < 24) {
            c2Var.mo9391a("vector", (com.fossil.blesdk.obfuscated.C1526c2.C1530d) new com.fossil.blesdk.obfuscated.C1526c2.C1531e());
            c2Var.mo9391a("animated-vector", (com.fossil.blesdk.obfuscated.C1526c2.C1530d) new com.fossil.blesdk.obfuscated.C1526c2.C1528b());
            c2Var.mo9391a("animated-selector", (com.fossil.blesdk.obfuscated.C1526c2.C1530d) new com.fossil.blesdk.obfuscated.C1526c2.C1527a());
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized android.graphics.drawable.Drawable mo9385a(android.content.Context context, int i, boolean z) {
        android.graphics.drawable.Drawable f;
        mo9389a(context);
        f = mo9401f(context, i);
        if (f == null) {
            f = mo9394b(context, i);
        }
        if (f == null) {
            f = com.fossil.blesdk.obfuscated.C2185k6.m9358c(context, i);
        }
        if (f != null) {
            f = mo9386a(context, i, z, f);
        }
        if (f != null) {
            com.fossil.blesdk.obfuscated.C2181k2.m9313b(f);
        }
        return f;
    }

    @DexIgnore
    /* renamed from: b */
    public final android.content.res.ColorStateList mo9393b(android.content.Context context) {
        return mo9384a(context, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public static long m5213a(android.util.TypedValue typedValue) {
        return (((long) typedValue.assetCookie) << 32) | ((long) typedValue.data);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo9386a(android.content.Context context, int i, boolean z, android.graphics.drawable.Drawable drawable) {
        android.content.res.ColorStateList d = mo9398d(context, i);
        if (d != null) {
            if (com.fossil.blesdk.obfuscated.C2181k2.m9312a(drawable)) {
                drawable = drawable.mutate();
            }
            android.graphics.drawable.Drawable i2 = com.fossil.blesdk.obfuscated.C1538c7.m5314i(drawable);
            com.fossil.blesdk.obfuscated.C1538c7.m5299a(i2, d);
            android.graphics.PorterDuff.Mode a = m5214a(i);
            if (a == null) {
                return i2;
            }
            com.fossil.blesdk.obfuscated.C1538c7.m5302a(i2, a);
            return i2;
        } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_seekbar_track_material) {
            android.graphics.drawable.LayerDrawable layerDrawable = (android.graphics.drawable.LayerDrawable) drawable;
            m5218a(layerDrawable.findDrawableByLayerId(16908288), com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlNormal), f3958g);
            m5218a(layerDrawable.findDrawableByLayerId(16908303), com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlNormal), f3958g);
            m5218a(layerDrawable.findDrawableByLayerId(16908301), com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlActivated), f3958g);
            return drawable;
        } else if (i == com.fossil.blesdk.obfuscated.C3076v.abc_ratingbar_material || i == com.fossil.blesdk.obfuscated.C3076v.abc_ratingbar_indicator_material || i == com.fossil.blesdk.obfuscated.C3076v.abc_ratingbar_small_material) {
            android.graphics.drawable.LayerDrawable layerDrawable2 = (android.graphics.drawable.LayerDrawable) drawable;
            m5218a(layerDrawable2.findDrawableByLayerId(16908288), com.fossil.blesdk.obfuscated.C3003u2.m14531a(context, com.fossil.blesdk.obfuscated.C2777r.colorControlNormal), f3958g);
            m5218a(layerDrawable2.findDrawableByLayerId(16908303), com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlActivated), f3958g);
            m5218a(layerDrawable2.findDrawableByLayerId(16908301), com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlActivated), f3958g);
            return drawable;
        } else if (m5221a(context, i, drawable) || !z) {
            return drawable;
        } else {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final android.content.res.ColorStateList mo9397d(android.content.Context context) {
        return mo9384a(context, com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorButtonNormal));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        return null;
     */
    @DexIgnore
    /* renamed from: a */
    public final synchronized android.graphics.drawable.Drawable mo9387a(android.content.Context context, long j) {
        com.fossil.blesdk.obfuscated.C2103j4 j4Var = this.f3970d.get(context);
        if (j4Var == null) {
            return null;
        }
        java.lang.ref.WeakReference weakReference = (java.lang.ref.WeakReference) j4Var.mo12216b(j);
        if (weakReference != null) {
            android.graphics.drawable.Drawable.ConstantState constantState = (android.graphics.drawable.Drawable.ConstantState) weakReference.get();
            if (constantState != null) {
                return constantState.newDrawable(context.getResources());
            }
            j4Var.mo12214a(j);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized boolean mo9392a(android.content.Context context, long j, android.graphics.drawable.Drawable drawable) {
        android.graphics.drawable.Drawable.ConstantState constantState = drawable.getConstantState();
        if (constantState == null) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2103j4 j4Var = this.f3970d.get(context);
        if (j4Var == null) {
            j4Var = new com.fossil.blesdk.obfuscated.C2103j4();
            this.f3970d.put(context, j4Var);
        }
        j4Var.mo12223c(j, new java.lang.ref.WeakReference(constantState));
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized android.graphics.drawable.Drawable mo9388a(android.content.Context context, com.fossil.blesdk.obfuscated.C1701e3 e3Var, int i) {
        android.graphics.drawable.Drawable f = mo9401f(context, i);
        if (f == null) {
            f = e3Var.mo10302a(i);
        }
        if (f == null) {
            return null;
        }
        return mo9386a(context, i, false, f);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0061 A[RETURN] */
    /* renamed from: a */
    public static boolean m5221a(android.content.Context context, int i, android.graphics.drawable.Drawable drawable) {
        boolean z;
        int i2;
        android.graphics.PorterDuff.Mode mode = f3958g;
        int i3 = 16842801;
        if (m5223a(f3961j, i)) {
            i3 = com.fossil.blesdk.obfuscated.C2777r.colorControlNormal;
        } else if (m5223a(f3963l, i)) {
            i3 = com.fossil.blesdk.obfuscated.C2777r.colorControlActivated;
        } else if (m5223a(f3964m, i)) {
            mode = android.graphics.PorterDuff.Mode.MULTIPLY;
        } else {
            if (i == com.fossil.blesdk.obfuscated.C3076v.abc_list_divider_mtrl_alpha) {
                i3 = 16842800;
                i2 = java.lang.Math.round(40.8f);
                z = true;
            } else if (i != com.fossil.blesdk.obfuscated.C3076v.abc_dialog_material_background) {
                z = false;
                i2 = -1;
                i3 = 0;
            }
            if (z) {
                return false;
            }
            if (com.fossil.blesdk.obfuscated.C2181k2.m9312a(drawable)) {
                drawable = drawable.mutate();
            }
            drawable.setColorFilter(m5215a(com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, i3), mode));
            if (i2 != -1) {
                drawable.setAlpha(i2);
            }
            return true;
        }
        z = true;
        i2 = -1;
        if (z) {
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9391a(java.lang.String str, com.fossil.blesdk.obfuscated.C1526c2.C1530d dVar) {
        if (this.f3968b == null) {
            this.f3968b = new com.fossil.blesdk.obfuscated.C1855g4<>();
        }
        this.f3968b.put(str, dVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m5223a(int[] iArr, int i) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.PorterDuff.Mode m5214a(int i) {
        if (i == com.fossil.blesdk.obfuscated.C3076v.abc_switch_thumb_material) {
            return android.graphics.PorterDuff.Mode.MULTIPLY;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9390a(android.content.Context context, int i, android.content.res.ColorStateList colorStateList) {
        if (this.f3967a == null) {
            this.f3967a = new java.util.WeakHashMap<>();
        }
        androidx.collection.SparseArrayCompat sparseArrayCompat = this.f3967a.get(context);
        if (sparseArrayCompat == null) {
            sparseArrayCompat = new androidx.collection.SparseArrayCompat();
            this.f3967a.put(context, sparseArrayCompat);
        }
        sparseArrayCompat.mo1241a(i, colorStateList);
    }

    @DexIgnore
    /* renamed from: a */
    public final android.content.res.ColorStateList mo9384a(android.content.Context context, int i) {
        int b = com.fossil.blesdk.obfuscated.C3003u2.m14534b(context, com.fossil.blesdk.obfuscated.C2777r.colorControlHighlight);
        int a = com.fossil.blesdk.obfuscated.C3003u2.m14531a(context, com.fossil.blesdk.obfuscated.C2777r.colorButtonNormal);
        return new android.content.res.ColorStateList(new int[][]{com.fossil.blesdk.obfuscated.C3003u2.f9835b, com.fossil.blesdk.obfuscated.C3003u2.f9837d, com.fossil.blesdk.obfuscated.C3003u2.f9836c, com.fossil.blesdk.obfuscated.C3003u2.f9839f}, new int[]{a, com.fossil.blesdk.obfuscated.C2943t6.m14032b(b, i), com.fossil.blesdk.obfuscated.C2943t6.m14032b(b, i), i});
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5219a(android.graphics.drawable.Drawable drawable, com.fossil.blesdk.obfuscated.C3256x2 x2Var, int[] iArr) {
        if (!com.fossil.blesdk.obfuscated.C2181k2.m9312a(drawable) || drawable.mutate() == drawable) {
            if (x2Var.f10821d || x2Var.f10820c) {
                drawable.setColorFilter(m5216a(x2Var.f10821d ? x2Var.f10818a : null, x2Var.f10820c ? x2Var.f10819b : f3958g, iArr));
            } else {
                drawable.clearColorFilter();
            }
            if (android.os.Build.VERSION.SDK_INT <= 23) {
                drawable.invalidateSelf();
                return;
            }
            return;
        }
        android.util.Log.d("AppCompatDrawableManag", "Mutated drawable is not the same instance as the input.");
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.PorterDuffColorFilter m5216a(android.content.res.ColorStateList colorStateList, android.graphics.PorterDuff.Mode mode, int[] iArr) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return m5215a(colorStateList.getColorForState(iArr, 0), mode);
    }

    @DexIgnore
    /* renamed from: a */
    public static synchronized android.graphics.PorterDuffColorFilter m5215a(int i, android.graphics.PorterDuff.Mode mode) {
        android.graphics.PorterDuffColorFilter a;
        synchronized (com.fossil.blesdk.obfuscated.C1526c2.class) {
            a = f3960i.mo9404a(i, mode);
            if (a == null) {
                a = new android.graphics.PorterDuffColorFilter(i, mode);
                f3960i.mo9405a(i, mode, a);
            }
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m5218a(android.graphics.drawable.Drawable drawable, int i, android.graphics.PorterDuff.Mode mode) {
        if (com.fossil.blesdk.obfuscated.C2181k2.m9312a(drawable)) {
            drawable = drawable.mutate();
        }
        if (mode == null) {
            mode = f3958g;
        }
        drawable.setColorFilter(m5215a(i, mode));
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9389a(android.content.Context context) {
        if (!this.f3972f) {
            this.f3972f = true;
            android.graphics.drawable.Drawable c = mo9396c(context, com.fossil.blesdk.obfuscated.C3076v.abc_vector_test);
            if (c == null || !m5222a(c)) {
                this.f3972f = false;
                throw new java.lang.IllegalStateException("This app has been built with an incorrect configuration. Please configure your build for VectorDrawableCompat.");
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m5222a(android.graphics.drawable.Drawable drawable) {
        return (drawable instanceof com.fossil.blesdk.obfuscated.C2879si) || "android.graphics.drawable.VectorDrawable".equals(drawable.getClass().getName());
    }
}
