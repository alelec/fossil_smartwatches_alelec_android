package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class za0 {
    @DexIgnore
    public static /* final */ za0 a; // = new za0();

    @DexIgnore
    public final File a(String str) {
        kd4.b(str, "filename");
        Context a2 = va0.f.a();
        if (a2 != null) {
            return new File(a2.getFilesDir(), str);
        }
        return null;
    }
}
