package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zl0 extends yy0 implements xl0 {
    @DexIgnore
    public zl0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @DexIgnore
    public final boolean a(mn0 mn0, sn0 sn0) throws RemoteException {
        Parcel o = o();
        az0.a(o, (Parcelable) mn0);
        az0.a(o, (IInterface) sn0);
        Parcel a = a(5, o);
        boolean a2 = az0.a(a);
        a.recycle();
        return a2;
    }
}
