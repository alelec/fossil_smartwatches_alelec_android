package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.blesdk.obfuscated.zj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wd0 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wd0> CREATOR; // = new dn0();
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    @Deprecated
    public /* final */ int f;
    @DexIgnore
    public /* final */ long g;

    @DexIgnore
    public wd0(String str, int i, long j) {
        this.e = str;
        this.f = i;
        this.g = j;
    }

    @DexIgnore
    public String H() {
        return this.e;
    }

    @DexIgnore
    public long I() {
        long j = this.g;
        return j == -1 ? (long) this.f : j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof wd0) {
            wd0 wd0 = (wd0) obj;
            if (((H() == null || !H().equals(wd0.H())) && (H() != null || wd0.H() != null)) || I() != wd0.I()) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return zj0.a(H(), Long.valueOf(I()));
    }

    @DexIgnore
    public String toString() {
        zj0.a a = zj0.a((Object) this);
        a.a("name", H());
        a.a("version", Long.valueOf(I()));
        return a.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, H(), false);
        kk0.a(parcel, 2, this.f);
        kk0.a(parcel, 3, I());
        kk0.a(parcel, a);
    }
}
