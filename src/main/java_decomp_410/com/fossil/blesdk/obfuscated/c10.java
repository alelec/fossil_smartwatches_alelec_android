package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class c10 extends BluetoothCommand {
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public Peripheral.State l; // = Peripheral.State.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean m;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c10(boolean z, Peripheral.c cVar) {
        super(BluetoothCommandId.CONNECT, cVar);
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.m = z;
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        kd4.b(peripheral, "peripheral");
        peripheral.a(this.m);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        if (gattOperationResult instanceof t10) {
            t10 t10 = (t10) gattOperationResult;
            if (t10.b() == Peripheral.State.CONNECTED || t10.b() == Peripheral.State.DISCONNECTED) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int d() {
        return this.k;
    }

    @DexIgnore
    public ra0<GattOperationResult> f() {
        return b().b();
    }

    @DexIgnore
    public final Peripheral.State i() {
        return this.l;
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        BluetoothCommand.Result result;
        kd4.b(gattOperationResult, "gattOperationResult");
        d(gattOperationResult);
        if (gattOperationResult.a().getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            BluetoothCommand.Result a = BluetoothCommand.Result.Companion.a(gattOperationResult.a());
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, a.getResultCode(), a.getGattResult(), 1, (Object) null);
        } else if (this.l == Peripheral.State.CONNECTED) {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.SUCCESS, (GattOperationResult.GattResult) null, 5, (Object) null);
        } else {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.UNEXPECTED_RESULT, (GattOperationResult.GattResult) null, 5, (Object) null);
        }
        a(result);
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        this.l = ((t10) gattOperationResult).b();
    }
}
