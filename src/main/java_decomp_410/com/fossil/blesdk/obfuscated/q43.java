package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface q43 extends v52<p43> {
    @DexIgnore
    void A(String str);

    @DexIgnore
    void E(String str);

    @DexIgnore
    void M(String str);

    @DexIgnore
    void a(int i, int i2, String str, String str2);

    @DexIgnore
    void a(String str, String str2, String str3);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, String str3);

    @DexIgnore
    void b(WatchApp watchApp);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void c(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void f(String str);

    @DexIgnore
    void t(boolean z);

    @DexIgnore
    void v(List<WatchApp> list);
}
