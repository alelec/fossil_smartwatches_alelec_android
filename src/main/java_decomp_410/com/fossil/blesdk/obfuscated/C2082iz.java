package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.iz */
public class C2082iz implements com.crashlytics.android.core.Report {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.io.File[] f6244a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.String, java.lang.String> f6245b; // = new java.util.HashMap(com.fossil.blesdk.obfuscated.C3330xz.f11114g);

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.String f6246c;

    @DexIgnore
    public C2082iz(java.lang.String str, java.io.File[] fileArr) {
        this.f6244a = fileArr;
        this.f6246c = str;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.String> mo4192a() {
        return java.util.Collections.unmodifiableMap(this.f6245b);
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo4193b() {
        return this.f6246c;
    }

    @DexIgnore
    /* renamed from: c */
    public java.io.File mo4194c() {
        return this.f6244a[0];
    }

    @DexIgnore
    /* renamed from: d */
    public java.io.File[] mo4195d() {
        return this.f6244a;
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.String mo4196e() {
        return this.f6244a[0].getName();
    }

    @DexIgnore
    public com.crashlytics.android.core.Report.Type getType() {
        return com.crashlytics.android.core.Report.Type.JAVA;
    }

    @DexIgnore
    public void remove() {
        for (java.io.File path : this.f6244a) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30060d("CrashlyticsCore", "Removing invalid report file at " + path.getPath());
            r0[r2].delete();
        }
    }
}
