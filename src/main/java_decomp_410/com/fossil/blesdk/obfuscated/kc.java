package com.fossil.blesdk.obfuscated;

import android.app.Application;
import androidx.lifecycle.ViewModelStore;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kc {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ ViewModelStore b;

    @DexIgnore
    public interface b {
        @DexIgnore
        <T extends ic> T a(Class<T> cls);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c implements b {
        @DexIgnore
        public <T extends ic> T a(Class<T> cls) {
            throw new UnsupportedOperationException("create(String, Class<?>) must be called on implementaions of KeyedFactory");
        }

        @DexIgnore
        public abstract <T extends ic> T a(String str, Class<T> cls);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements b {
        @DexIgnore
        public <T extends ic> T a(Class<T> cls) {
            try {
                return (ic) cls.newInstance();
            } catch (InstantiationException e) {
                throw new RuntimeException("Cannot create an instance of " + cls, e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            }
        }
    }

    @DexIgnore
    public kc(ViewModelStore viewModelStore, b bVar) {
        this.a = bVar;
        this.b = viewModelStore;
    }

    @DexIgnore
    public <T extends ic> T a(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return a("androidx.lifecycle.ViewModelProvider.DefaultKey:" + canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends d {
        @DexIgnore
        public static a b;
        @DexIgnore
        public Application a;

        @DexIgnore
        public a(Application application) {
            this.a = application;
        }

        @DexIgnore
        public static a a(Application application) {
            if (b == null) {
                b = new a(application);
            }
            return b;
        }

        @DexIgnore
        public <T extends ic> T a(Class<T> cls) {
            if (!mb.class.isAssignableFrom(cls)) {
                return super.a(cls);
            }
            try {
                return (ic) cls.getConstructor(new Class[]{Application.class}).newInstance(new Object[]{this.a});
            } catch (NoSuchMethodException e) {
                throw new RuntimeException("Cannot create an instance of " + cls, e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Cannot create an instance of " + cls, e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException("Cannot create an instance of " + cls, e3);
            } catch (InvocationTargetException e4) {
                throw new RuntimeException("Cannot create an instance of " + cls, e4);
            }
        }
    }

    @DexIgnore
    public <T extends ic> T a(String str, Class<T> cls) {
        T t;
        T a2 = this.b.a(str);
        if (cls.isInstance(a2)) {
            return a2;
        }
        b bVar = this.a;
        if (bVar instanceof c) {
            t = ((c) bVar).a(str, cls);
        } else {
            t = bVar.a(cls);
        }
        this.b.a(str, t);
        return t;
    }
}
