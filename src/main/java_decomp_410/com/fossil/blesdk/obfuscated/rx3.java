package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.iy3;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rx3 extends iy3 {
    @DexIgnore
    public static /* final */ int b; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;

    @DexIgnore
    public rx3(Context context) {
        this.a = context.getAssets();
    }

    @DexIgnore
    public static String c(gy3 gy3) {
        return gy3.d.toString().substring(b);
    }

    @DexIgnore
    public boolean a(gy3 gy3) {
        Uri uri = gy3.d;
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public iy3.a a(gy3 gy3, int i) throws IOException {
        return new iy3.a(this.a.open(c(gy3)), Picasso.LoadedFrom.DISK);
    }
}
