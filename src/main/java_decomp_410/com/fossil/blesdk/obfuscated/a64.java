package com.fossil.blesdk.obfuscated;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a64<T> {
    @DexIgnore
    void a(T t);

    @DexIgnore
    boolean b();

    @DexIgnore
    Collection<T> c();
}
