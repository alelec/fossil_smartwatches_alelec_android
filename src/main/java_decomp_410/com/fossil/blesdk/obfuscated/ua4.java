package com.fossil.blesdk.obfuscated;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import kotlin.collections.AbstractCollection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ua4<E> extends AbstractCollection<E> implements List<E>, rd4 {
    @DexIgnore
    public static /* final */ a e; // = new a((fd4) null);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(int i, int i2) {
            if (i < 0 || i >= i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public final void b(int i, int i2) {
            if (i < 0 || i > i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            if (i < 0 || i2 > i3) {
                throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2 + ", size: " + i3);
            } else if (i > i2) {
                throw new IllegalArgumentException("fromIndex: " + i + " > toIndex: " + i2);
            }
        }

        @DexIgnore
        public final int a(Collection<?> collection) {
            kd4.b(collection, "c");
            Iterator<?> it = collection.iterator();
            int i = 1;
            while (it.hasNext()) {
                Object next = it.next();
                i = (i * 31) + (next != null ? next.hashCode() : 0);
            }
            return i;
        }

        @DexIgnore
        public final boolean a(Collection<?> collection, Collection<?> collection2) {
            kd4.b(collection, "c");
            kd4.b(collection2, FacebookRequestErrorClassification.KEY_OTHER);
            if (collection.size() != collection2.size()) {
                return false;
            }
            Iterator<?> it = collection2.iterator();
            for (Object a : collection) {
                if (!kd4.a((Object) a, (Object) it.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Iterator<E>, rd4 {
        @DexIgnore
        public int e;

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.e < ua4.this.size();
        }

        @DexIgnore
        public E next() {
            if (hasNext()) {
                ua4 ua4 = ua4.this;
                int i = this.e;
                this.e = i + 1;
                return ua4.get(i);
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public final void a(int i) {
            this.e = i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ua4<E>.b implements ListIterator<E>, rd4 {
        @DexIgnore
        public c(int i) {
            super();
            ua4.e.b(i, ua4.this.size());
            a(i);
        }

        @DexIgnore
        public void add(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public boolean hasPrevious() {
            return a() > 0;
        }

        @DexIgnore
        public int nextIndex() {
            return a();
        }

        @DexIgnore
        public E previous() {
            if (hasPrevious()) {
                ua4 ua4 = ua4.this;
                a(a() - 1);
                return ua4.get(a());
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return a() - 1;
        }

        @DexIgnore
        public void set(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<E> extends ua4<E> implements RandomAccess {
        @DexIgnore
        public int f;
        @DexIgnore
        public /* final */ ua4<E> g;
        @DexIgnore
        public /* final */ int h;

        @DexIgnore
        public d(ua4<? extends E> ua4, int i, int i2) {
            kd4.b(ua4, "list");
            this.g = ua4;
            this.h = i;
            ua4.e.a(this.h, i2, this.g.size());
            this.f = i2 - this.h;
        }

        @DexIgnore
        public int a() {
            return this.f;
        }

        @DexIgnore
        public E get(int i) {
            ua4.e.a(i, this.f);
            return this.g.get(this.h + i);
        }
    }

    @DexIgnore
    public void add(int i, E e2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        return e.a((Collection<?>) this, (Collection<?>) (Collection) obj);
    }

    @DexIgnore
    public abstract E get(int i);

    @DexIgnore
    public int hashCode() {
        return e.a(this);
    }

    @DexIgnore
    public int indexOf(Object obj) {
        int i = 0;
        for (Object a2 : this) {
            if (kd4.a(a2, obj)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public Iterator<E> iterator() {
        return new b();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        ListIterator listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (kd4.a(listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    @DexIgnore
    public ListIterator<E> listIterator() {
        return new c(0);
    }

    @DexIgnore
    public E remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public E set(int i, E e2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public List<E> subList(int i, int i2) {
        return new d(this, i, i2);
    }

    @DexIgnore
    public ListIterator<E> listIterator(int i) {
        return new c(i);
    }
}
