package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.th4;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class uh4 extends sh4 {
    @DexIgnore
    public abstract Thread I();

    @DexIgnore
    public final void J() {
        Thread I = I();
        if (Thread.currentThread() != I) {
            cj4 a = dj4.a();
            if (a != null) {
                a.a(I);
            } else {
                LockSupport.unpark(I);
            }
        }
    }

    @DexIgnore
    public final void a(long j, th4.c cVar) {
        kd4.b(cVar, "delayedTask");
        if (ch4.a()) {
            if (!(this != eh4.k)) {
                throw new AssertionError();
            }
        }
        eh4.k.b(j, cVar);
    }
}
