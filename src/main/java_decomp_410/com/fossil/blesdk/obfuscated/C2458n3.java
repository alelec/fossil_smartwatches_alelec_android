package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n3 */
public final class C2458n3 {
    @DexIgnore
    public static /* final */ int browser_actions_context_menu_max_width; // = 2131165274;
    @DexIgnore
    public static /* final */ int browser_actions_context_menu_min_padding; // = 2131165275;
    @DexIgnore
    public static /* final */ int compat_button_inset_horizontal_material; // = 2131165296;
    @DexIgnore
    public static /* final */ int compat_button_inset_vertical_material; // = 2131165297;
    @DexIgnore
    public static /* final */ int compat_button_padding_horizontal_material; // = 2131165298;
    @DexIgnore
    public static /* final */ int compat_button_padding_vertical_material; // = 2131165299;
    @DexIgnore
    public static /* final */ int compat_control_corner_material; // = 2131165300;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_height; // = 2131165301;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_width; // = 2131165302;
    @DexIgnore
    public static /* final */ int notification_action_icon_size; // = 2131165539;
    @DexIgnore
    public static /* final */ int notification_action_text_size; // = 2131165540;
    @DexIgnore
    public static /* final */ int notification_big_circle_margin; // = 2131165541;
    @DexIgnore
    public static /* final */ int notification_content_margin_start; // = 2131165542;
    @DexIgnore
    public static /* final */ int notification_large_icon_height; // = 2131165543;
    @DexIgnore
    public static /* final */ int notification_large_icon_width; // = 2131165544;
    @DexIgnore
    public static /* final */ int notification_main_column_padding_top; // = 2131165545;
    @DexIgnore
    public static /* final */ int notification_media_narrow_margin; // = 2131165546;
    @DexIgnore
    public static /* final */ int notification_right_icon_size; // = 2131165547;
    @DexIgnore
    public static /* final */ int notification_right_side_padding_top; // = 2131165548;
    @DexIgnore
    public static /* final */ int notification_small_icon_background_padding; // = 2131165549;
    @DexIgnore
    public static /* final */ int notification_small_icon_size_as_large; // = 2131165550;
    @DexIgnore
    public static /* final */ int notification_subtext_size; // = 2131165551;
    @DexIgnore
    public static /* final */ int notification_top_pad; // = 2131165552;
    @DexIgnore
    public static /* final */ int notification_top_pad_large_text; // = 2131165553;
}
