package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ex3 {
    @DexIgnore
    public static final ex3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ex3 {
        @DexIgnore
        public yo4 a(File file) throws FileNotFoundException {
            return so4.c(file);
        }

        @DexIgnore
        public xo4 b(File file) throws FileNotFoundException {
            try {
                return so4.b(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return so4.b(file);
            }
        }

        @DexIgnore
        public void c(File file) throws IOException {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                int length = listFiles.length;
                int i = 0;
                while (i < length) {
                    File file2 = listFiles[i];
                    if (file2.isDirectory()) {
                        c(file2);
                    }
                    if (file2.delete()) {
                        i++;
                    } else {
                        throw new IOException("failed to delete " + file2);
                    }
                }
                return;
            }
            throw new IOException("not a readable directory: " + file);
        }

        @DexIgnore
        public boolean d(File file) throws IOException {
            return file.exists();
        }

        @DexIgnore
        public void e(File file) throws IOException {
            if (!file.delete() && file.exists()) {
                throw new IOException("failed to delete " + file);
            }
        }

        @DexIgnore
        public xo4 f(File file) throws FileNotFoundException {
            try {
                return so4.a(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return so4.a(file);
            }
        }

        @DexIgnore
        public long g(File file) {
            return file.length();
        }

        @DexIgnore
        public void a(File file, File file2) throws IOException {
            e(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        }
    }

    @DexIgnore
    yo4 a(File file) throws FileNotFoundException;

    @DexIgnore
    void a(File file, File file2) throws IOException;

    @DexIgnore
    xo4 b(File file) throws FileNotFoundException;

    @DexIgnore
    void c(File file) throws IOException;

    @DexIgnore
    boolean d(File file) throws IOException;

    @DexIgnore
    void e(File file) throws IOException;

    @DexIgnore
    xo4 f(File file) throws FileNotFoundException;

    @DexIgnore
    long g(File file);
}
