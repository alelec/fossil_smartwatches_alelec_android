package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qk extends lk<Boolean> {
    @DexIgnore
    public qk(Context context, zl zlVar) {
        super(xk.a(context, zlVar).d());
    }

    @DexIgnore
    public boolean a(hl hlVar) {
        return hlVar.j.i();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
