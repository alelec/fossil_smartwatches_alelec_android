package com.fossil.blesdk.obfuscated;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rr<A, B> {
    @DexIgnore
    public /* final */ qw<b<A>, B> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qw<b<A>, B> {
        @DexIgnore
        public a(rr rrVar, long j) {
            super(j);
        }

        @DexIgnore
        public void a(b<A> bVar, B b) {
            bVar.a();
        }
    }

    @DexIgnore
    public rr(long j) {
        this.a = new a(this, j);
    }

    @DexIgnore
    public B a(A a2, int i, int i2) {
        b b2 = b.b(a2, i, i2);
        B a3 = this.a.a(b2);
        b2.a();
        return a3;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<A> {
        @DexIgnore
        public static /* final */ Queue<b<?>> d; // = uw.a(0);
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public A c;

        @DexIgnore
        public static <A> b<A> b(A a2, int i, int i2) {
            b<A> poll;
            synchronized (d) {
                poll = d.poll();
            }
            if (poll == null) {
                poll = new b<>();
            }
            poll.a(a2, i, i2);
            return poll;
        }

        @DexIgnore
        public final void a(A a2, int i, int i2) {
            this.c = a2;
            this.b = i;
            this.a = i2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b == bVar.b && this.a == bVar.a && this.c.equals(bVar.c)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
        }

        @DexIgnore
        public void a() {
            synchronized (d) {
                d.offer(this);
            }
        }
    }

    @DexIgnore
    public void a(A a2, int i, int i2, B b2) {
        this.a.b(b.b(a2, i, i2), b2);
    }
}
