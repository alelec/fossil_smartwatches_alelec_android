package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@DatabaseTable(tableName = "hourNotification")
public class sn2 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<sn2> CREATOR; // = new a();
    @DexIgnore
    @DatabaseField(columnName = "hour")
    public int e;
    @DexIgnore
    @DatabaseField(columnName = "isVibrationOnly")
    public boolean f;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long g;
    @DexIgnore
    @DatabaseField(columnName = "extraId")
    public String h;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String i;
    @DexIgnore
    @DatabaseField(columnName = "deviceFamily")
    public String j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<sn2> {
        @DexIgnore
        public sn2 createFromParcel(Parcel parcel) {
            return new sn2(parcel);
        }

        @DexIgnore
        public sn2[] newArray(int i) {
            return new sn2[i];
        }
    }

    @DexIgnore
    public sn2() {
    }

    @DexIgnore
    public void a(String str) {
        this.j = str;
    }

    @DexIgnore
    public String b() {
        return this.j;
    }

    @DexIgnore
    public void c(String str) {
        this.i = str;
    }

    @DexIgnore
    public int d() {
        return this.e;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String e() {
        return this.i;
    }

    @DexIgnore
    public boolean f() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.e);
        parcel.writeByte(this.f ? (byte) 1 : 0);
        parcel.writeLong(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
    }

    @DexIgnore
    public sn2(int i2, boolean z, String str, String str2) {
        this.e = i2;
        this.f = z;
        this.h = str;
        this.j = str2;
        this.i = str + str2;
    }

    @DexIgnore
    public void a(int i2) {
        this.e = i2;
    }

    @DexIgnore
    public void b(String str) {
        this.h = str;
    }

    @DexIgnore
    public String c() {
        return this.h;
    }

    @DexIgnore
    public void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public long a() {
        return this.g;
    }

    @DexIgnore
    public void a(long j2) {
        this.g = j2;
    }

    @DexIgnore
    public sn2(Parcel parcel) {
        this.e = parcel.readInt();
        this.f = parcel.readByte() != 0;
        this.g = parcel.readLong();
        this.h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readString();
    }
}
