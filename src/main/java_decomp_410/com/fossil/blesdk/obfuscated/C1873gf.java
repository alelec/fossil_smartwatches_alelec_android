package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gf */
public class C1873gf extends com.fossil.blesdk.obfuscated.C1725ef {

    @DexIgnore
    /* renamed from: d */
    public boolean f5440d; // = false;

    @DexIgnore
    public C1873gf(long j, androidx.renderscript.RenderScript renderScript) {
        super(j, renderScript);
        new android.util.SparseArray();
        new android.util.SparseArray();
        new android.util.SparseArray();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11218a(boolean z) {
        this.f5440d = z;
    }

    @DexIgnore
    /* renamed from: a */
    public long mo11214a(androidx.renderscript.Allocation allocation) {
        if (allocation == null) {
            return 0;
        }
        androidx.renderscript.Type e = allocation.mo3169e();
        long b = e.mo3245g().mo3173b(this.f4782c);
        androidx.renderscript.RenderScript renderScript = this.f4782c;
        long a = renderScript.mo3182a(allocation.mo10463a(renderScript), e.mo3242a(this.f4782c, b), e.mo3246h() * e.mo3245g().mo3174e());
        allocation.mo3161a(a);
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11216a(int i, androidx.renderscript.Allocation allocation, androidx.renderscript.Allocation allocation2, com.fossil.blesdk.obfuscated.C1800ff ffVar) {
        androidx.renderscript.Allocation allocation3 = allocation;
        androidx.renderscript.Allocation allocation4 = allocation2;
        if (allocation3 == null && allocation4 == null) {
            throw new androidx.renderscript.RSIllegalArgumentException("At least one of ain or aout is required to be non-null.");
        }
        long j = 0;
        long a = allocation3 != null ? allocation3.mo10463a(this.f4782c) : 0;
        if (allocation4 != null) {
            j = allocation4.mo10463a(this.f4782c);
        }
        long j2 = j;
        if (ffVar != null) {
            ffVar.mo10902a();
            throw null;
        } else if (this.f5440d) {
            long a2 = mo11214a(allocation3);
            long a3 = mo11214a(allocation4);
            androidx.renderscript.RenderScript renderScript = this.f4782c;
            renderScript.mo3186a(mo10463a(renderScript), i, a2, a3, (byte[]) null, this.f5440d);
        } else {
            androidx.renderscript.RenderScript renderScript2 = this.f4782c;
            renderScript2.mo3186a(mo10463a(renderScript2), i, a, j2, (byte[]) null, this.f5440d);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11215a(int i, float f) {
        androidx.renderscript.RenderScript renderScript = this.f4782c;
        renderScript.mo3185a(mo10463a(renderScript), i, f, this.f5440d);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11217a(int i, com.fossil.blesdk.obfuscated.C1725ef efVar) {
        com.fossil.blesdk.obfuscated.C1725ef efVar2 = efVar;
        long j = 0;
        if (this.f5440d) {
            long a = mo11214a((androidx.renderscript.Allocation) efVar2);
            androidx.renderscript.RenderScript renderScript = this.f4782c;
            renderScript.mo3187a(mo10463a(renderScript), i, efVar2 == null ? 0 : a, this.f5440d);
            return;
        }
        androidx.renderscript.RenderScript renderScript2 = this.f4782c;
        long a2 = mo10463a(renderScript2);
        if (efVar2 != null) {
            j = efVar2.mo10463a(this.f4782c);
        }
        renderScript2.mo3187a(a2, i, j, this.f5440d);
    }
}
