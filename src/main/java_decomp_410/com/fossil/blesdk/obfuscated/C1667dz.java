package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dz */
public class C1667dz implements com.fossil.blesdk.obfuscated.C2440my {

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.util.List<java.lang.Class<?>> f4542c; // = java.util.Collections.unmodifiableList(java.util.Arrays.asList(new java.lang.Class[]{java.lang.String.class, java.lang.String.class, android.os.Bundle.class, java.lang.Long.class}));

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3069uy f4543a;

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object f4544b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.dz$a")
    /* renamed from: com.fossil.blesdk.obfuscated.dz$a */
    public class C1668a implements java.lang.reflect.InvocationHandler {
        @DexIgnore
        public C1668a() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo10162a(java.lang.Object obj, java.lang.Object obj2) {
            if (obj == obj2) {
                return true;
            }
            if (obj2 == null || !java.lang.reflect.Proxy.isProxyClass(obj2.getClass()) || !super.equals(java.lang.reflect.Proxy.getInvocationHandler(obj2))) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public java.lang.Object invoke(java.lang.Object obj, java.lang.reflect.Method method, java.lang.Object[] objArr) {
            java.lang.String name = method.getName();
            if (objArr == null) {
                objArr = new java.lang.Object[0];
            }
            if (objArr.length == 1 && name.equals("equals")) {
                return java.lang.Boolean.valueOf(mo10162a(obj, objArr[0]));
            }
            if (objArr.length == 0 && name.equals("hashCode")) {
                return java.lang.Integer.valueOf(super.hashCode());
            }
            if (objArr.length == 0 && name.equals("toString")) {
                return super.toString();
            }
            if (objArr.length == 4 && name.equals("onEvent") && com.fossil.blesdk.obfuscated.C1667dz.m6095a(objArr)) {
                java.lang.String str = (java.lang.String) objArr[0];
                java.lang.String str2 = (java.lang.String) objArr[1];
                android.os.Bundle bundle = (android.os.Bundle) objArr[2];
                if (str != null && !str.equals("crash")) {
                    com.fossil.blesdk.obfuscated.C1667dz.m6096b(com.fossil.blesdk.obfuscated.C1667dz.this.f4543a, str2, bundle);
                    return null;
                }
            }
            java.lang.StringBuilder sb = new java.lang.StringBuilder("Unexpected method invoked on AppMeasurement.EventListener: " + name + "(");
            for (int i = 0; i < objArr.length; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(objArr[i].getClass().getName());
            }
            sb.append("); returning null");
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30062e("CrashlyticsCore", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public C1667dz(com.fossil.blesdk.obfuscated.C3069uy uyVar) {
        this.f4543a = uyVar;
    }

    @DexIgnore
    /* renamed from: b */
    public synchronized java.lang.Object mo10160b(java.lang.Class cls) {
        if (this.f4544b == null) {
            this.f4544b = java.lang.reflect.Proxy.newProxyInstance(this.f4543a.mo31733l().getClassLoader(), new java.lang.Class[]{cls}, new com.fossil.blesdk.obfuscated.C1667dz.C1668a());
        }
        return this.f4544b;
    }

    @DexIgnore
    public boolean register() {
        java.lang.Class<?> a = mo10158a("com.google.android.gms.measurement.AppMeasurement");
        if (a == null) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Firebase Analytics is not present; you will not see automatic logging of events before a crash occurs.");
            return false;
        }
        java.lang.Object a2 = mo10159a(a);
        if (a2 == null) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30066w("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not create an instance of Firebase Analytics.");
            return false;
        }
        java.lang.Class<?> a3 = mo10158a("com.google.android.gms.measurement.AppMeasurement$OnEventListener");
        if (a3 == null) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30066w("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not get class com.google.android.gms.measurement.AppMeasurement$OnEventListener");
            return false;
        }
        try {
            a.getDeclaredMethod("registerOnMeasurementEventListener", new java.lang.Class[]{a3}).invoke(a2, new java.lang.Object[]{mo10160b(a3)});
        } catch (java.lang.NoSuchMethodException e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30056a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Method registerOnMeasurementEventListener not found.", (java.lang.Throwable) e);
            return false;
        } catch (java.lang.Exception e2) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30056a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: " + e2.getMessage(), (java.lang.Throwable) e2);
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.Class<?> mo10158a(java.lang.String str) {
        try {
            return this.f4543a.mo31733l().getClassLoader().loadClass(str);
        } catch (java.lang.Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.Object mo10159a(java.lang.Class<?> cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new java.lang.Class[]{android.content.Context.class}).invoke(cls, new java.lang.Object[]{this.f4543a.mo31733l()});
        } catch (java.lang.Exception unused) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6095a(java.lang.Object[] objArr) {
        if (objArr.length != f4542c.size()) {
            return false;
        }
        java.util.Iterator<java.lang.Class<?>> it = f4542c.iterator();
        for (java.lang.Object obj : objArr) {
            if (!obj.getClass().equals(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6096b(com.fossil.blesdk.obfuscated.C3069uy uyVar, java.lang.String str, android.os.Bundle bundle) {
        try {
            uyVar.mo16921a("$A$:" + m6093a(str, bundle));
        } catch (org.json.JSONException unused) {
            com.fossil.blesdk.obfuscated.y44 g = com.fossil.blesdk.obfuscated.q44.m26805g();
            g.mo30066w("CrashlyticsCore", "Unable to serialize Firebase Analytics event; " + str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m6093a(java.lang.String str, android.os.Bundle bundle) throws org.json.JSONException {
        org.json.JSONObject jSONObject = new org.json.JSONObject();
        org.json.JSONObject jSONObject2 = new org.json.JSONObject();
        for (java.lang.String str2 : bundle.keySet()) {
            jSONObject2.put(str2, bundle.get(str2));
        }
        jSONObject.put("name", str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }
}
