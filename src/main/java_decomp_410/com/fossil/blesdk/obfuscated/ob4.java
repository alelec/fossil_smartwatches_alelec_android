package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ob4<K, V> extends Map<K, V>, rd4 {
    @DexIgnore
    V a(K k);
}
