package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vm */
public interface C3124vm {
    @DexIgnore
    /* renamed from: a */
    void mo14455a(com.android.volley.Request<?> request, com.android.volley.VolleyError volleyError);

    @DexIgnore
    /* renamed from: a */
    void mo14456a(com.android.volley.Request<?> request, com.fossil.blesdk.obfuscated.C3047um<?> umVar);

    @DexIgnore
    /* renamed from: a */
    void mo14457a(com.android.volley.Request<?> request, com.fossil.blesdk.obfuscated.C3047um<?> umVar, java.lang.Runnable runnable);
}
