package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ix */
public class C2080ix {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.g74 f6234a;

    @DexIgnore
    public C2080ix(com.fossil.blesdk.obfuscated.g74 g74) {
        this.f6234a = g74;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2080ix m8688a(android.content.Context context) {
        return new com.fossil.blesdk.obfuscated.C2080ix(new com.fossil.blesdk.obfuscated.h74(context, com.misfit.frameworks.common.constants.Constants.USER_SETTING));
    }

    @DexIgnore
    @android.annotation.SuppressLint({"CommitPrefEdits"})
    /* renamed from: b */
    public void mo12163b() {
        com.fossil.blesdk.obfuscated.g74 g74 = this.f6234a;
        g74.mo27643a(g74.edit().putBoolean("analytics_launched", true));
    }

    @DexIgnore
    @android.annotation.SuppressLint({"CommitPrefEdits"})
    /* renamed from: a */
    public boolean mo12162a() {
        return this.f6234a.get().getBoolean("analytics_launched", false);
    }
}
