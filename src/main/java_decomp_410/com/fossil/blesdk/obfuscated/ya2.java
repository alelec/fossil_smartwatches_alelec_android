package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ya2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerView q;

    @DexIgnore
    public ya2(Object obj, View view, int i, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = recyclerView;
    }
}
