package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.dm4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.helper.AppHelper;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bp2 implements hl4 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a((fd4) null);
    @DexIgnore
    public AtomicInteger b; // = new AtomicInteger(0);
    @DexIgnore
    public Auth c;
    @DexIgnore
    public /* final */ dn2 d;
    @DexIgnore
    public /* final */ AuthApiGuestService e;
    @DexIgnore
    public /* final */ en2 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bp2.g;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        String simpleName = bp2.class.getSimpleName();
        kd4.a((Object) simpleName, "TokenAuthenticator::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public bp2(dn2 dn2, AuthApiGuestService authApiGuestService, en2 en2) {
        kd4.b(dn2, "mProviderManager");
        kd4.b(authApiGuestService, "mAuthApiGuestService");
        kd4.b(en2, "mSharedPreferencesManager");
        this.d = dn2;
        this.e = authApiGuestService;
        this.f = en2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x015a, code lost:
        if (r11 != null) goto L_0x0161;
     */
    @DexIgnore
    public dm4 authenticate(fm4 fm4, Response response) {
        String str;
        String str2;
        kd4.b(response, "response");
        MFUser b2 = this.d.n().b();
        if ((b2 != null ? b2.getUserAccessToken() : null) == null || TextUtils.isEmpty(b2.getUserAccessToken())) {
            FLogger.INSTANCE.getLocal().d(g, "unauthorized: userAccessToken is null or empty");
            return null;
        }
        int i = 0;
        boolean z = this.b.getAndIncrement() == 0;
        this.c = null;
        qr4<Auth> a2 = a(b2, response, z);
        Auth auth = this.c;
        if (this.b.decrementAndGet() == 0) {
            this.c = null;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = g;
        local.d(str3, "authenticate decrementAndGet mCount=" + this.b + " address=" + this);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = g;
        local2.d(str4, "old headers=" + response.L().c().toString());
        if (auth != null) {
            String accessToken = auth.getAccessToken();
            if (accessToken != null) {
                b2.setUserAccessToken(accessToken);
                b2.setRefreshToken(auth.getRefreshToken());
                b2.setAccessTokenExpiresAt(rk2.t(auth.getAccessTokenExpiresAt()));
                Integer accessTokenExpiresIn = auth.getAccessTokenExpiresIn();
                if (accessTokenExpiresIn != null) {
                    i = accessTokenExpiresIn.intValue();
                }
                b2.setAccessTokenExpiresIn(Integer.valueOf(i));
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str5 = g;
                local3.d(str5, "accessToken = " + accessToken + ", refreshToken = " + auth.getRefreshToken() + ", userOnboardingComplete=" + b2.isOnboardingComplete());
                this.f.w(accessToken);
                this.f.a(System.currentTimeMillis());
                this.d.n().a(b2);
                dm4.a f2 = response.L().f();
                f2.b("Authorization", "Bearer " + accessToken);
                return f2.a();
            }
        }
        if (a2 == null || a2.b() < 500 || a2.b() >= 600) {
            if (z && a2 != null) {
                try {
                    Gson gson = new Gson();
                    em4 y = a2.f().y();
                    if (y != null) {
                        str = y.F();
                        if (str != null) {
                            ServerError serverError = (ServerError) gson.a(str, ServerError.class);
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String str6 = g;
                            StringBuilder sb = new StringBuilder();
                            sb.append("forceLogout userMessage=");
                            kd4.a((Object) serverError, "serverError");
                            sb.append(serverError.getUserMessage());
                            local4.d(str6, sb.toString());
                            PortfolioApp.W.c().a(serverError);
                        }
                    }
                    str = a2.e();
                    ServerError serverError2 = (ServerError) gson.a(str, ServerError.class);
                    ILocalFLogger local42 = FLogger.INSTANCE.getLocal();
                    String str62 = g;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("forceLogout userMessage=");
                    kd4.a((Object) serverError2, "serverError");
                    sb2.append(serverError2.getUserMessage());
                    local42.d(str62, sb2.toString());
                    PortfolioApp.W.c().a(serverError2);
                } catch (Exception e2) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str7 = g;
                    local5.d(str7, "forceLogout ex=" + e2.getMessage());
                    e2.printStackTrace();
                    PortfolioApp.W.c().a((ServerError) null);
                }
            }
            return null;
        }
        ServerError serverError3 = new ServerError();
        serverError3.setCode(Integer.valueOf(a2.b()));
        em4 c2 = a2.c();
        if (c2 != null) {
            str2 = c2.F();
        }
        str2 = a2.e();
        serverError3.setMessage(str2);
        throw new ServerErrorException(serverError3);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00d6 A[Catch:{ Exception -> 0x0165 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d7 A[Catch:{ Exception -> 0x0165 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f1 A[Catch:{ Exception -> 0x0165 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f3 A[Catch:{ Exception -> 0x0165 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a3 A[SYNTHETIC, Splitter:B:54:0x01a3] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x020d A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0216 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0221 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0226 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0233 A[Catch:{ Exception -> 0x023f }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x023a A[Catch:{ Exception -> 0x023f }] */
    public final synchronized qr4<Auth> a(MFUser mFUser, Response response, boolean z) {
        qr4<Auth> qr4;
        Integer num;
        qr4<Auth> r;
        FLogger.INSTANCE.getLocal().d(g, "getAuth needProcess=" + z + " mCount=" + this.b);
        qr4 = null;
        if (z) {
            if (TextUtils.isEmpty(mFUser.getRefreshToken())) {
                xz1 xz1 = new xz1();
                FLogger.INSTANCE.getLocal().d(g, "Exchange Legacy Token");
                xz1.a(Constants.PROFILE_KEY_ACCESS_TOKEN, mFUser.getUserAccessToken());
                AppHelper.Companion companion = AppHelper.f;
                String userId = mFUser.getUserId();
                kd4.a((Object) userId, "user.userId");
                xz1.a("clientId", companion.a(userId));
                this.c = this.e.tokenExchangeLegacy(xz1).r().a();
            } else {
                em4 y = response.y();
                if (y != null) {
                    StringBuilder sb = new StringBuilder();
                    String readLine = new BufferedReader(new InputStreamReader(y.y())).readLine();
                    int length = readLine.length();
                    for (int i = 0; i < length; i++) {
                        sb.append(readLine.charAt(i));
                    }
                    String sb2 = sb.toString();
                    kd4.a((Object) sb2, "sb.toString()");
                    JsonElement a2 = new yz1().a(sb2);
                    if (a2 != null) {
                        xz1 d2 = a2.d();
                        if (d2 != null) {
                            JsonElement a3 = d2.a("_error");
                            if (a3 != null) {
                                num = Integer.valueOf(a3.b());
                                if (num == null) {
                                    if (num.intValue() == 401026) {
                                        FLogger.INSTANCE.getLocal().d(g, "Exchange Legacy Token failed");
                                    }
                                }
                                if (num != null) {
                                    if (num != null) {
                                        if (num.intValue() == 401005) {
                                            FLogger.INSTANCE.getLocal().d(g, "Token is in black list");
                                        }
                                    }
                                    FLogger.INSTANCE.getLocal().d(g, "unauthorized: error = " + sb2);
                                    FLogger.INSTANCE.getLocal().d(g, "Refresh Token");
                                    xz1 xz12 = new xz1();
                                    xz12.a("refreshToken", mFUser.getRefreshToken());
                                    try {
                                        r = this.e.tokenRefresh(xz12).r();
                                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                        String str = g;
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("Refresh Token error=");
                                        sb3.append(r == null ? Integer.valueOf(r.b()) : null);
                                        sb3.append(" body=");
                                        sb3.append(r == null ? r.c() : null);
                                        local.d(str, sb3.toString());
                                        this.c = r == null ? r.a() : null;
                                    } catch (Exception e2) {
                                        FLogger.INSTANCE.getLocal().d(xo2.h.a(), "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e2.getMessage());
                                        e2.printStackTrace();
                                        return null;
                                    }
                                } else {
                                    if (num.intValue() == 401011) {
                                        FLogger.INSTANCE.getLocal().d(g, "Refresh Token");
                                        xz1 xz13 = new xz1();
                                        xz13.a("refreshToken", mFUser.getRefreshToken());
                                        try {
                                            r = this.e.tokenRefresh(xz13).r();
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            String str2 = g;
                                            StringBuilder sb4 = new StringBuilder();
                                            sb4.append("Refresh Token error=");
                                            sb4.append(r != null ? Integer.valueOf(r.b()) : null);
                                            sb4.append(" body=");
                                            sb4.append(r != null ? r.c() : null);
                                            local2.d(str2, sb4.toString());
                                            this.c = r != null ? r.a() : null;
                                        } catch (Exception e3) {
                                            FLogger.INSTANCE.getLocal().d(g, "getAuth needProcess=" + z + " mCount=" + this.b + " exception=" + e3.getMessage());
                                            e3.printStackTrace();
                                            return null;
                                        }
                                    }
                                    if (num != null) {
                                    }
                                    FLogger.INSTANCE.getLocal().d(g, "unauthorized: error = " + sb2);
                                    FLogger.INSTANCE.getLocal().d(g, "Refresh Token");
                                    xz1 xz122 = new xz1();
                                    xz122.a("refreshToken", mFUser.getRefreshToken());
                                    r = this.e.tokenRefresh(xz122).r();
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String str3 = g;
                                    StringBuilder sb32 = new StringBuilder();
                                    sb32.append("Refresh Token error=");
                                    sb32.append(r == null ? Integer.valueOf(r.b()) : null);
                                    sb32.append(" body=");
                                    sb32.append(r == null ? r.c() : null);
                                    local3.d(str3, sb32.toString());
                                    this.c = r == null ? r.a() : null;
                                }
                                qr4 = r;
                            }
                        }
                    }
                    num = null;
                    if (num == null) {
                    }
                    if (num != null) {
                    }
                    qr4 = r;
                }
            }
        }
        return qr4;
    }
}
