package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.intrinsics.CoroutineSingletons;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cc4 extends IntrinsicsKt__IntrinsicsJvmKt {
    @DexIgnore
    public static final Object a() {
        return CoroutineSingletons.COROUTINE_SUSPENDED;
    }
}
