package com.fossil.blesdk.obfuscated;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class sx1 implements kw1 {
    @DexIgnore
    public static /* final */ kw1 a; // = new sx1();

    @DexIgnore
    public final Object a(jw1 jw1) {
        return new FirebaseInstanceId((FirebaseApp) jw1.a(FirebaseApp.class), (bx1) jw1.a(bx1.class));
    }
}
