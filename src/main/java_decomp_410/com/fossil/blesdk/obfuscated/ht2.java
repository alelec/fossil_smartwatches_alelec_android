package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ht2 extends rd<SleepSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ jt2 e;
    @DexIgnore
    public /* final */ FragmentManager f;
    @DexIgnore
    public /* final */ zr2 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public int f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4) {
            kd4.b(str, "mDayOfWeek");
            kd4.b(str2, "mDayOfMonth");
            kd4.b(str3, "mDailyUnit");
            kd4.b(str4, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = i;
            this.g = str3;
            this.h = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final void c(String str) {
            kd4.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4, int i2, fd4 fd4) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) != 0 ? false : z2, (r0 & 8) != 0 ? r6 : str, (r0 & 16) != 0 ? r6 : str2, (r0 & 32) == 0 ? i : 0, (r0 & 64) != 0 ? r6 : str3, (r0 & 128) == 0 ? str4 : r6);
            int i3 = i2;
            Date date2 = (i3 & 1) != 0 ? null : date;
            String str5 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final int c() {
            return this.f;
        }

        @DexIgnore
        public final void d(String str) {
            kd4.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void a(int i) {
            this.f = i;
        }

        @DexIgnore
        public final void b(String str) {
            kd4.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void a(String str) {
            kd4.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ oi2 b;
        @DexIgnore
        public /* final */ /* synthetic */ ht2 c;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c e;

            @DexIgnore
            public a(c cVar) {
                this.e = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a = this.e.a;
                if (a != null) {
                    this.e.c.e.b(a);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ht2 ht2, oi2 oi2, View view) {
            super(view);
            kd4.b(oi2, "binding");
            kd4.b(view, "root");
            this.c = ht2;
            this.b = oi2;
            this.b.d().setOnClickListener(new a(this));
        }

        @DexIgnore
        public void a(SleepSummary sleepSummary) {
            b a2 = this.c.a(sleepSummary);
            this.a = a2.d();
            FlexibleTextView flexibleTextView = this.b.u;
            kd4.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            kd4.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.e());
            if (a2.c() <= 0) {
                ConstraintLayout constraintLayout = this.b.q;
                kd4.a((Object) constraintLayout, "binding.clDailyValue");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.s;
                kd4.a((Object) flexibleTextView3, "binding.ftvDailyUnit");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                kd4.a((Object) constraintLayout2, "binding.clDailyValue");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.x;
                kd4.a((Object) flexibleTextView4, "binding.tvMin");
                flexibleTextView4.setText(String.valueOf(nl2.b(a2.c())));
                FlexibleTextView flexibleTextView5 = this.b.w;
                kd4.a((Object) flexibleTextView5, "binding.tvHour");
                flexibleTextView5.setText(String.valueOf(nl2.a(a2.c())));
                FlexibleTextView flexibleTextView6 = this.b.s;
                kd4.a((Object) flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setVisibility(8);
            }
            FlexibleTextView flexibleTextView7 = this.b.s;
            kd4.a((Object) flexibleTextView7, "binding.ftvDailyUnit");
            flexibleTextView7.setText(a2.b());
            FlexibleTextView flexibleTextView8 = this.b.v;
            kd4.a((Object) flexibleTextView8, "binding.ftvEst");
            flexibleTextView8.setText(a2.a());
            if (a2.g()) {
                this.b.s.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.disabledCalendarDay));
                FlexibleTextView flexibleTextView9 = this.b.s;
                kd4.a((Object) flexibleTextView9, "binding.ftvDailyUnit");
                flexibleTextView9.setAllCaps(true);
            } else {
                this.b.s.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.nonReachGoal));
                FlexibleTextView flexibleTextView10 = this.b.s;
                kd4.a((Object) flexibleTextView10, "binding.ftvDailyUnit");
                flexibleTextView10.setAllCaps(false);
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            kd4.a((Object) constraintLayout3, "binding.container");
            constraintLayout3.setSelected(true ^ a2.g());
            FlexibleTextView flexibleTextView11 = this.b.u;
            kd4.a((Object) flexibleTextView11, "binding.ftvDayOfWeek");
            flexibleTextView11.setSelected(a2.h());
            FlexibleTextView flexibleTextView12 = this.b.t;
            kd4.a((Object) flexibleTextView12, "binding.ftvDayOfMonth");
            flexibleTextView12.setSelected(a2.h());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            kd4.b(str, "mWeekly");
            kd4.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, fd4 fd4) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            kd4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            kd4.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ qi2 f;
        @DexIgnore
        public /* final */ /* synthetic */ ht2 g;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e e;

            @DexIgnore
            public a(e eVar) {
                this.e = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.e.d != null && this.e.e != null) {
                    jt2 c = this.e.g.e;
                    Date b = this.e.d;
                    if (b != null) {
                        Date a = this.e.e;
                        if (a != null) {
                            c.b(b, a);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(ht2 ht2, qi2 qi2) {
            super(ht2, r0, r1);
            kd4.b(qi2, "binding");
            this.g = ht2;
            oi2 oi2 = qi2.r;
            if (oi2 != null) {
                kd4.a((Object) oi2, "binding.dailyItem!!");
                View d2 = qi2.d();
                kd4.a((Object) d2, "binding.root");
                this.f = qi2;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            kd4.a();
            throw null;
        }

        @DexIgnore
        public void a(SleepSummary sleepSummary) {
            d b = this.g.b(sleepSummary);
            this.e = b.a();
            this.d = b.b();
            FlexibleTextView flexibleTextView = this.f.s;
            kd4.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.f.t;
            kd4.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(sleepSummary);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ ht2 e;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder f;
        @DexIgnore
        public /* final */ /* synthetic */ boolean g;

        @DexIgnore
        public f(ht2 ht2, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.e = ht2;
            this.f = viewHolder;
            this.g = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            kd4.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id=" + this.e.g.getId() + ", isAdded=" + this.e.g.isAdded());
            this.f.itemView.removeOnAttachStateChangeListener(this);
            Fragment a = this.e.f.a(this.e.g.R0());
            if (a == null) {
                FLogger.INSTANCE.getLocal().d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                bb a2 = this.e.f.a();
                a2.a(view.getId(), this.e.g, this.e.g.R0());
                a2.d();
            } else if (this.g) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
                bb a3 = this.e.f.a();
                a3.d(a);
                a3.d();
                bb a4 = this.e.f.a();
                a4.a(view.getId(), this.e.g, this.e.g.R0());
                a4.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + a.getId() + ", isAdded=" + a.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.e.g.getId() + ", isAdded2=" + this.e.g.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            kd4.b(view, "v");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ht2(kt2 kt2, PortfolioApp portfolioApp, jt2 jt2, FragmentManager fragmentManager, zr2 zr2) {
        super(kt2);
        kd4.b(kt2, "sleepDifference");
        kd4.b(portfolioApp, "mApp");
        kd4.b(jt2, "mOnItemClick");
        kd4.b(fragmentManager, "mFragmentManager");
        kd4.b(zr2, "mFragment");
        this.d = portfolioApp;
        this.e = jt2;
        this.f = fragmentManager;
        this.g = zr2;
    }

    @DexIgnore
    public long getItemId(int i) {
        if (getItemViewType(i) != 0) {
            return super.getItemId(i);
        }
        if (this.g.getId() == 0) {
            return 1010101;
        }
        return (long) this.g.getId();
    }

    @DexIgnore
    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        SleepSummary sleepSummary = (SleepSummary) a(i);
        if (sleepSummary == null) {
            return 1;
        }
        Calendar calendar = this.c;
        kd4.a((Object) calendar, "mCalendar");
        calendar.setTime(sleepSummary.getDate());
        Calendar calendar2 = this.c;
        kd4.a((Object) calendar2, "mCalendar");
        Boolean s = rk2.s(calendar2.getTime());
        kd4.a((Object) s, "DateHelper.isToday(mCalendar.time)");
        if (s.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        kd4.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepsAdapter", "onBindViewHolder - position=" + i + ", viewType=" + getItemViewType(i));
        int itemViewType = getItemViewType(i);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            kd4.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            kd4.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardSleepsAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            kd4.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((SleepSummary) a(i));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((SleepSummary) a(i));
        } else {
            ((e) viewHolder).a((SleepSummary) a(i));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i == 1) {
            oi2 a2 = oi2.a(from, viewGroup, false);
            kd4.a((Object) a2, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View d2 = a2.d();
            kd4.a((Object) d2, "itemSleepDayBinding.root");
            return new c(this, a2, d2);
        } else if (i != 2) {
            oi2 a3 = oi2.a(from, viewGroup, false);
            kd4.a((Object) a3, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View d3 = a3.d();
            kd4.a((Object) d3, "itemSleepDayBinding.root");
            return new c(this, a3, d3);
        } else {
            qi2 a4 = qi2.a(from, viewGroup, false);
            kd4.a((Object) a4, "ItemSleepWeekBinding.inf\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(qd<SleepSummary> qdVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList pagedList=");
        sb.append(qdVar != null ? Integer.valueOf(qdVar.size()) : null);
        local.d("DashboardSleepsAdapter", sb.toString());
        super.b(qdVar);
    }

    @DexIgnore
    public final b a(SleepSummary sleepSummary) {
        b bVar = new b((Date) null, false, false, (String) null, (String) null, 0, (String) null, (String) null, 255, (fd4) null);
        if (sleepSummary != null) {
            MFSleepDay sleepDay = sleepSummary.getSleepDay();
            if (sleepDay != null) {
                Calendar instance = Calendar.getInstance();
                kd4.a((Object) instance, "calendar");
                instance.setTime(sleepDay.getDate());
                int i = instance.get(7);
                Boolean s = rk2.s(instance.getTime());
                kd4.a((Object) s, "DateHelper.isToday(calendar.time)");
                if (s.booleanValue()) {
                    String a2 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_SleepToday_Text__Today);
                    kd4.a((Object) a2, "LanguageHelper.getString\u2026n_SleepToday_Text__Today)");
                    bVar.d(a2);
                } else {
                    bVar.d(ll2.b.b(i));
                }
                bVar.a(instance.getTime());
                bVar.c(String.valueOf(instance.get(5)));
                int sleepMinutes = sleepDay.getSleepMinutes();
                boolean z = true;
                if (sleepMinutes > 0) {
                    bVar.a(sleepMinutes);
                    bVar.b("");
                    List<MFSleepSession> sleepSessions = sleepSummary.getSleepSessions();
                    if (sleepSessions != null && (!sleepSessions.isEmpty())) {
                        MFSleepSession mFSleepSession = sleepSessions.get(0);
                        int startTime = mFSleepSession.getStartTime();
                        int endTime = mFSleepSession.getEndTime();
                        ll2 ll2 = ll2.b;
                        String a3 = rk2.a(((long) startTime) * 1000, mFSleepSession.getTimezoneOffset());
                        kd4.a((Object) a3, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                        String b2 = ll2.b(a3);
                        ll2 ll22 = ll2.b;
                        String a4 = rk2.a(((long) endTime) * 1000, mFSleepSession.getTimezoneOffset());
                        kd4.a((Object) a4, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                        String b3 = ll22.b(a4);
                        pd4 pd4 = pd4.a;
                        String a5 = sm2.a((Context) this.d, (int) R.string.sleep_start_end_time);
                        kd4.a((Object) a5, "LanguageHelper.getString\u2026ing.sleep_start_end_time)");
                        Object[] objArr = {b2, b3};
                        String format = String.format(a5, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(format, *args)");
                        int size = sleepSessions.size();
                        if (size > 1) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(format);
                            sb.append("\n");
                            pd4 pd42 = pd4.a;
                            String a6 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_SleepTodayMultiple_Label__NumberMore);
                            kd4.a((Object) a6, "LanguageHelper.getString\u2026ltiple_Label__NumberMore)");
                            Object[] objArr2 = {Integer.valueOf(size - 1)};
                            String format2 = String.format(a6, Arrays.copyOf(objArr2, objArr2.length));
                            kd4.a((Object) format2, "java.lang.String.format(format, *args)");
                            sb.append(format2);
                            bVar.a(sb.toString());
                        } else {
                            bVar.a(format);
                        }
                    }
                    if (sleepDay.getGoalMinutes() > 0) {
                        if (sleepMinutes < sleepDay.getGoalMinutes()) {
                            z = false;
                        }
                        bVar.b(z);
                    } else {
                        bVar.b(false);
                    }
                } else {
                    String a7 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_SleepToday_Text__NoRecord);
                    kd4.a((Object) a7, "LanguageHelper.getString\u2026leepToday_Text__NoRecord)");
                    bVar.b(a7);
                    bVar.a(true);
                }
            }
        }
        return bVar;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0109  */
    public final d b(SleepSummary sleepSummary) {
        String str;
        double d2;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (fd4) null);
        if (sleepSummary != null) {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "calendar");
            instance.setTime(sleepSummary.getDate());
            Boolean s = rk2.s(instance.getTime());
            int i = instance.get(5);
            int i2 = instance.get(2);
            String b2 = rk2.b(i2);
            int i3 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i4 = instance.get(5);
            int i5 = instance.get(2);
            String b3 = rk2.b(i5);
            int i6 = instance.get(1);
            dVar.b(instance.getTime());
            kd4.a((Object) s, "isToday");
            if (s.booleanValue()) {
                str = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_SleepToday_Title__ThisWeek);
                kd4.a((Object) str, "LanguageHelper.getString\u2026eepToday_Title__ThisWeek)");
            } else if (i2 == i5) {
                str = b3 + ' ' + i4 + " - " + b3 + ' ' + i;
            } else if (i6 == i3) {
                str = b3 + ' ' + i4 + " - " + b2 + ' ' + i;
            } else {
                str = b3 + ' ' + i4 + ", " + i6 + " - " + b2 + ' ' + i + ", " + i3;
            }
            dVar.a(str);
            MFSleepDay sleepDay = sleepSummary.getSleepDay();
            if (sleepDay != null) {
                Double averageSleepOfWeek = sleepDay.getAverageSleepOfWeek();
                if (averageSleepOfWeek != null) {
                    d2 = averageSleepOfWeek.doubleValue();
                    if (d2 > 0.0d) {
                        dVar.b("");
                    } else {
                        double d3 = (double) 60;
                        pd4 pd4 = pd4.a;
                        String a2 = sm2.a((Context) this.d, (int) R.string.DashboardDiana_Main_SleepToday_Text__NumberHrNumberMinAvg);
                        kd4.a((Object) a2, "LanguageHelper.getString\u2026xt__NumberHrNumberMinAvg)");
                        Object[] objArr = {Integer.valueOf((int) (d2 / d3)), Integer.valueOf((int) (d2 % d3))};
                        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                        kd4.a((Object) format, "java.lang.String.format(format, *args)");
                        dVar.b(format);
                    }
                }
            }
            d2 = 0.0d;
            if (d2 > 0.0d) {
            }
        }
        return dVar;
    }
}
