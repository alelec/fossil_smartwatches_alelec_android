package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.ws2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Triple;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bo3 extends vn3 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public /* final */ List<ws2.c> f; // = new ArrayList();
    @DexIgnore
    public /* final */ wn3 g;
    @DexIgnore
    public /* final */ ArrayList<Integer> h;
    @DexIgnore
    public /* final */ en2 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = bo3.class.getSimpleName();
        kd4.a((Object) simpleName, "PermissionPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public bo3(wn3 wn3, ArrayList<Integer> arrayList, en2 en2) {
        kd4.b(wn3, "mView");
        kd4.b(arrayList, "mListPerms");
        kd4.b(en2, "mSharedPreferencesManager");
        this.g = wn3;
        this.h = arrayList;
        this.i = en2;
    }

    @DexIgnore
    public void f() {
        Context context;
        FLogger.INSTANCE.getLocal().d(j, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        wn3 wn3 = this.g;
        if (wn3 instanceof xn3) {
            context = ((xn3) wn3).getContext();
        } else if (wn3 != null) {
            context = ((sn3) wn3).getContext();
        } else {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.permission.AllowNotificationServiceFragment");
        }
        if (this.f.isEmpty()) {
            for (Number intValue : this.h) {
                int intValue2 = intValue.intValue();
                Triple<String, String, String> b = bn2.d.b(intValue2);
                this.f.add(new ws2.c(intValue2, b.component1(), b.component2(), bn2.d.a(intValue2), b.component3(), bn2.d.a(context, intValue2)));
            }
        } else {
            for (ws2.c cVar : this.f) {
                cVar.a(bn2.d.a(context, cVar.d()));
            }
        }
        List<ws2.c> list = this.f;
        int i2 = 0;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (ws2.c b2 : list) {
                if (b2.b()) {
                    i2++;
                    if (i2 < 0) {
                        cb4.b();
                        throw null;
                    }
                }
            }
        }
        if (i2 == this.f.size()) {
            this.g.close();
        } else {
            this.g.t(this.f);
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(j, "stop");
    }

    @DexIgnore
    public void h() {
        this.i.q(false);
        this.g.close();
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }
}
