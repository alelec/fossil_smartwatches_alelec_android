package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oo2 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = oo2.class.getSimpleName();
        kd4.a((Object) simpleName, "TimeZoneReceiver::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        kd4.b(context, "context");
        kd4.b(intent, "intent");
        String action = intent.getAction();
        if (kd4.a((Object) "android.intent.action.TIME_SET", (Object) action) || kd4.a((Object) "android.intent.action.TIMEZONE_CHANGED", (Object) action)) {
            FLogger.INSTANCE.getLocal().d(a, "Inside .timeZoneChangeReceiver");
            rk2.b();
        } else if (kd4.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
            TimeZone timeZone = TimeZone.getDefault();
            try {
                Calendar instance = Calendar.getInstance();
                kd4.a((Object) instance, "calendar");
                Calendar instance2 = Calendar.getInstance();
                kd4.a((Object) instance2, "Calendar.getInstance()");
                instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                Calendar instance3 = Calendar.getInstance();
                kd4.a((Object) instance3, "Calendar.getInstance()");
                if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                    FLogger.INSTANCE.getLocal().d(a, "Inside .timeZoneChangeReceiver - DST change");
                    rk2.b();
                    PortfolioApp.W.c().M();
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.e(str, ".timeZoneChangeReceiver - ex=" + e);
            }
        }
    }
}
