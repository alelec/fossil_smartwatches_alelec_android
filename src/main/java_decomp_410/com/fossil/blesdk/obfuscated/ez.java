package com.fossil.blesdk.obfuscated;

import com.crashlytics.android.core.Report;
import com.zendesk.sdk.attachment.AttachmentHelper;
import io.fabric.sdk.android.services.network.HttpMethod;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ez extends e54 implements cz {
    @DexIgnore
    public ez(v44 v44, String str, String str2, z64 z64) {
        super(v44, str, str2, z64, HttpMethod.POST);
    }

    @DexIgnore
    public boolean a(bz bzVar) {
        HttpRequest a = a();
        a(a, bzVar);
        a(a, bzVar.b);
        y44 g = q44.g();
        g.d("CrashlyticsCore", "Sending report to: " + b());
        int g2 = a.g();
        y44 g3 = q44.g();
        g3.d("CrashlyticsCore", "Create report request ID: " + a.c("X-REQUEST-ID"));
        y44 g4 = q44.g();
        g4.d("CrashlyticsCore", "Result was: " + g2);
        return w54.a(g2) == 0;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, bz bzVar) {
        httpRequest.c("X-CRASHLYTICS-API-KEY", bzVar.a);
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        httpRequest.c("X-CRASHLYTICS-API-CLIENT-VERSION", this.e.r());
        for (Map.Entry<String, String> a : bzVar.b.a().entrySet()) {
            httpRequest.a(a);
        }
        return httpRequest;
    }

    @DexIgnore
    public final HttpRequest a(HttpRequest httpRequest, Report report) {
        httpRequest.e("report[identifier]", report.b());
        if (report.d().length == 1) {
            q44.g().d("CrashlyticsCore", "Adding single file " + report.e() + " to report " + report.b());
            httpRequest.a("report[file]", report.e(), AttachmentHelper.DEFAULT_MIMETYPE, report.c());
            return httpRequest;
        }
        int i = 0;
        for (File file : report.d()) {
            q44.g().d("CrashlyticsCore", "Adding file " + file.getName() + " to report " + report.b());
            StringBuilder sb = new StringBuilder();
            sb.append("report[file");
            sb.append(i);
            sb.append("]");
            httpRequest.a(sb.toString(), file.getName(), AttachmentHelper.DEFAULT_MIMETYPE, file);
            i++;
        }
        return httpRequest;
    }
}
