package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f33 implements Factory<CommuteTimeSettingsPresenter> {
    @DexIgnore
    public static CommuteTimeSettingsPresenter a(u23 u23, en2 en2, UserRepository userRepository) {
        return new CommuteTimeSettingsPresenter(u23, en2, userRepository);
    }
}
