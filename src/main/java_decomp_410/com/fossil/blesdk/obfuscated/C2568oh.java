package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.oh */
public class C2568oh {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ boolean f8125a; // = (android.os.Build.VERSION.SDK_INT >= 19);

    @DexIgnore
    /* renamed from: b */
    public static /* final */ boolean f8126b; // = (android.os.Build.VERSION.SDK_INT >= 18);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ boolean f8127c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.oh$a")
    /* renamed from: com.fossil.blesdk.obfuscated.oh$a */
    public static class C2569a implements android.animation.TypeEvaluator<android.graphics.Matrix> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ float[] f8128a; // = new float[9];

        @DexIgnore
        /* renamed from: b */
        public /* final */ float[] f8129b; // = new float[9];

        @DexIgnore
        /* renamed from: c */
        public /* final */ android.graphics.Matrix f8130c; // = new android.graphics.Matrix();

        @DexIgnore
        /* renamed from: a */
        public android.graphics.Matrix evaluate(float f, android.graphics.Matrix matrix, android.graphics.Matrix matrix2) {
            matrix.getValues(this.f8128a);
            matrix2.getValues(this.f8129b);
            for (int i = 0; i < 9; i++) {
                float[] fArr = this.f8129b;
                float f2 = fArr[i];
                float[] fArr2 = this.f8128a;
                fArr[i] = fArr2[i] + ((f2 - fArr2[i]) * f);
            }
            this.f8130c.setValues(this.f8129b);
            return this.f8130c;
        }
    }

    /*
    static {
        boolean z = true;
        if (android.os.Build.VERSION.SDK_INT < 28) {
            z = false;
        }
        f8127c = z;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static android.view.View m11786a(android.view.ViewGroup viewGroup, android.view.View view, android.view.View view2) {
        android.graphics.Matrix matrix = new android.graphics.Matrix();
        matrix.setTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        com.fossil.blesdk.obfuscated.C1485bi.m4989b(view, matrix);
        com.fossil.blesdk.obfuscated.C1485bi.m4991c(viewGroup, matrix);
        android.graphics.RectF rectF = new android.graphics.RectF(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) view.getWidth(), (float) view.getHeight());
        matrix.mapRect(rectF);
        int round = java.lang.Math.round(rectF.left);
        int round2 = java.lang.Math.round(rectF.top);
        int round3 = java.lang.Math.round(rectF.right);
        int round4 = java.lang.Math.round(rectF.bottom);
        android.widget.ImageView imageView = new android.widget.ImageView(view.getContext());
        imageView.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
        android.graphics.Bitmap a = m11785a(view, matrix, rectF, viewGroup);
        if (a != null) {
            imageView.setImageBitmap(a);
        }
        imageView.measure(android.view.View.MeasureSpec.makeMeasureSpec(round3 - round, 1073741824), android.view.View.MeasureSpec.makeMeasureSpec(round4 - round2, 1073741824));
        imageView.layout(round, round2, round3, round4);
        return imageView;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008a  */
    /* renamed from: a */
    public static android.graphics.Bitmap m11785a(android.view.View view, android.graphics.Matrix matrix, android.graphics.RectF rectF, android.view.ViewGroup viewGroup) {
        boolean z;
        boolean z2;
        android.view.ViewGroup viewGroup2;
        int round;
        int round2;
        int i = 0;
        if (f8125a) {
            z2 = !view.isAttachedToWindow();
            if (viewGroup != null) {
                z = viewGroup.isAttachedToWindow();
                android.graphics.Bitmap bitmap = null;
                if (f8126b || !z2) {
                    viewGroup2 = null;
                } else if (!z) {
                    return null;
                } else {
                    android.view.ViewGroup viewGroup3 = (android.view.ViewGroup) view.getParent();
                    int indexOfChild = viewGroup3.indexOfChild(view);
                    viewGroup.getOverlay().add(view);
                    int i2 = indexOfChild;
                    viewGroup2 = viewGroup3;
                    i = i2;
                }
                round = java.lang.Math.round(rectF.width());
                round2 = java.lang.Math.round(rectF.height());
                if (round > 0 && round2 > 0) {
                    float min = java.lang.Math.min(1.0f, 1048576.0f / ((float) (round * round2)));
                    int round3 = java.lang.Math.round(((float) round) * min);
                    int round4 = java.lang.Math.round(((float) round2) * min);
                    matrix.postTranslate(-rectF.left, -rectF.top);
                    matrix.postScale(min, min);
                    if (!f8127c) {
                        android.graphics.Picture picture = new android.graphics.Picture();
                        android.graphics.Canvas beginRecording = picture.beginRecording(round3, round4);
                        beginRecording.concat(matrix);
                        view.draw(beginRecording);
                        picture.endRecording();
                        bitmap = android.graphics.Bitmap.createBitmap(picture);
                    } else {
                        bitmap = android.graphics.Bitmap.createBitmap(round3, round4, android.graphics.Bitmap.Config.ARGB_8888);
                        android.graphics.Canvas canvas = new android.graphics.Canvas(bitmap);
                        canvas.concat(matrix);
                        view.draw(canvas);
                    }
                }
                if (f8126b && z2) {
                    viewGroup.getOverlay().remove(view);
                    viewGroup2.addView(view, i);
                }
                return bitmap;
            }
        } else {
            z2 = false;
        }
        z = false;
        android.graphics.Bitmap bitmap2 = null;
        if (f8126b) {
        }
        viewGroup2 = null;
        round = java.lang.Math.round(rectF.width());
        round2 = java.lang.Math.round(rectF.height());
        float min2 = java.lang.Math.min(1.0f, 1048576.0f / ((float) (round * round2)));
        int round32 = java.lang.Math.round(((float) round) * min2);
        int round42 = java.lang.Math.round(((float) round2) * min2);
        matrix.postTranslate(-rectF.left, -rectF.top);
        matrix.postScale(min2, min2);
        if (!f8127c) {
        }
        viewGroup.getOverlay().remove(view);
        viewGroup2.addView(view, i);
        return bitmap2;
    }

    @DexIgnore
    /* renamed from: a */
    public static android.animation.Animator m11784a(android.animation.Animator animator, android.animation.Animator animator2) {
        if (animator == null) {
            return animator2;
        }
        if (animator2 == null) {
            return animator;
        }
        android.animation.AnimatorSet animatorSet = new android.animation.AnimatorSet();
        animatorSet.playTogether(new android.animation.Animator[]{animator, animator2});
        return animatorSet;
    }
}
