package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.IntentFilter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t04 {
    @DexIgnore
    public static t04 i;
    @DexIgnore
    public List<String> a; // = null;
    @DexIgnore
    public volatile int b; // = 2;
    @DexIgnore
    public volatile String c; // = "";
    @DexIgnore
    public volatile HttpHost d; // = null;
    @DexIgnore
    public y14 e; // = null;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public Context g; // = null;
    @DexIgnore
    public t14 h; // = null;

    @DexIgnore
    public t04(Context context) {
        this.g = context.getApplicationContext();
        this.e = new y14();
        q24.a(context);
        this.h = e24.b();
        l();
        i();
        g();
    }

    @DexIgnore
    public static t04 a(Context context) {
        if (i == null) {
            synchronized (t04.class) {
                if (i == null) {
                    i = new t04(context);
                }
            }
        }
        return i;
    }

    @DexIgnore
    public HttpHost a() {
        return this.d;
    }

    @DexIgnore
    public void a(String str) {
        if (h04.q()) {
            this.h.e("updateIpList " + str);
        }
        try {
            if (e24.c(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.length() > 0) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String string = jSONObject.getString(keys.next());
                        if (e24.c(string)) {
                            for (String str2 : string.split(";")) {
                                if (e24.c(str2)) {
                                    String[] split = str2.split(":");
                                    if (split.length > 1) {
                                        String str3 = split[0];
                                        if (b(str3) && !this.a.contains(str3)) {
                                            if (h04.q()) {
                                                this.h.e("add new ip:" + str3);
                                            }
                                            this.a.add(str3);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            this.h.a((Throwable) e2);
        }
        this.f = new Random().nextInt(this.a.size());
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public final boolean b(String str) {
        return Pattern.compile("(2[5][0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})").matcher(str).matches();
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public void d() {
        this.f = (this.f + 1) % this.a.size();
    }

    @DexIgnore
    public boolean e() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean f() {
        return this.b != 0;
    }

    @DexIgnore
    public void g() {
        if (j24.f(this.g)) {
            if (h04.v) {
                k();
            }
            this.c = e24.o(this.g);
            if (h04.q()) {
                t14 t14 = this.h;
                t14.e("NETWORK name:" + this.c);
            }
            if (e24.c(this.c)) {
                this.b = "WIFI".equalsIgnoreCase(this.c) ? 1 : 2;
                this.d = e24.d(this.g);
            }
            if (j04.a()) {
                j04.d(this.g);
                return;
            }
            return;
        }
        if (h04.q()) {
            this.h.e("NETWORK TYPE: network is close.");
        }
        l();
    }

    @DexIgnore
    public void h() {
        this.g.getApplicationContext().registerReceiver(new m14(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @DexIgnore
    public final void i() {
        this.a = new ArrayList(10);
        this.a.add("117.135.169.101");
        this.a.add("140.207.54.125");
        this.a.add("180.153.8.53");
        this.a.add("120.198.203.175");
        this.a.add("14.17.43.18");
        this.a.add("163.177.71.186");
        this.a.add("111.30.131.31");
        this.a.add("123.126.121.167");
        this.a.add("123.151.152.111");
        this.a.add("113.142.45.79");
        this.a.add("123.138.162.90");
        this.a.add("103.7.30.94");
    }

    @DexIgnore
    public final String j() {
        try {
            return !b("pingma.qq.com") ? InetAddress.getByName("pingma.qq.com").getHostAddress() : "";
        } catch (Exception e2) {
            this.h.a((Throwable) e2);
            return "";
        }
    }

    @DexIgnore
    public final void k() {
        String j = j();
        if (h04.q()) {
            t14 t14 = this.h;
            t14.e("remoteIp ip is " + j);
        }
        if (e24.c(j)) {
            if (!this.a.contains(j)) {
                String str = this.a.get(this.f);
                if (h04.q()) {
                    t14 t142 = this.h;
                    t142.g(j + " not in ip list, change to:" + str);
                }
                j = str;
            }
            h04.c("http://" + j + ":80/mstat/report");
        }
    }

    @DexIgnore
    public final void l() {
        this.b = 0;
        this.d = null;
        this.c = null;
    }
}
