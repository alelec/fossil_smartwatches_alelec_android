package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.Version;
import com.fossil.blesdk.device.data.file.FileFormatException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class l20<T> {
    @DexIgnore
    public /* final */ Version a;

    @DexIgnore
    public l20(Version version) {
        kd4.b(version, "baseVersion");
        this.a = version;
    }

    @DexIgnore
    public final Version a() {
        return this.a;
    }

    @DexIgnore
    public abstract T a(byte[] bArr) throws FileFormatException;
}
