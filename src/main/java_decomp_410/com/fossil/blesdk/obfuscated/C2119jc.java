package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.jc */
public final class C2119jc {
    @DexIgnore
    /* renamed from: a */
    public static final com.fossil.blesdk.obfuscated.zg4 m8926a(com.fossil.blesdk.obfuscated.C2010ic icVar) {
        com.fossil.blesdk.obfuscated.kd4.m24411b(icVar, "$this$viewModelScope");
        com.fossil.blesdk.obfuscated.zg4 zg4 = (com.fossil.blesdk.obfuscated.zg4) icVar.mo11900a("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (zg4 != null) {
            return zg4;
        }
        java.lang.Object a = icVar.mo11901a("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new com.fossil.blesdk.obfuscated.C2541ob(com.fossil.blesdk.obfuscated.zi4.m31378a((com.fossil.blesdk.obfuscated.fi4) null, 1, (java.lang.Object) null).plus(com.fossil.blesdk.obfuscated.nh4.m25693c().mo30120C())));
        com.fossil.blesdk.obfuscated.kd4.m24407a(a, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (com.fossil.blesdk.obfuscated.zg4) a;
    }
}
