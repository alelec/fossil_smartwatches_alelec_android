package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jz3 extends iz3 {
    @DexIgnore
    public List<kz3> c;

    @DexIgnore
    public jz3(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    public int a() {
        return 9;
    }

    @DexIgnore
    public void a(Bundle bundle) {
        super.a(bundle);
        if (this.c == null) {
            this.c = new LinkedList();
        }
        String string = bundle.getString("_wxapi_add_card_to_wx_card_list");
        if (string != null && string.length() > 0) {
            try {
                JSONArray jSONArray = ((JSONObject) new JSONTokener(string).nextValue()).getJSONArray("card_list");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    kz3 kz3 = new kz3();
                    jSONObject.optString("card_id");
                    jSONObject.optString("card_ext");
                    jSONObject.optInt("is_succ");
                    this.c.add(kz3);
                }
            } catch (Exception unused) {
            }
        }
    }
}
