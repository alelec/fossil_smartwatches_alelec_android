package com.fossil.blesdk.obfuscated;

import androidx.collection.SimpleArrayMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g4<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    @DexIgnore
    public l4<K, V> l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends l4<K, V> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public Object a(int i, int i2) {
            return g4.this.f[(i << 1) + i2];
        }

        @DexIgnore
        public int b(Object obj) {
            return g4.this.c(obj);
        }

        @DexIgnore
        public int c() {
            return g4.this.g;
        }

        @DexIgnore
        public int a(Object obj) {
            return g4.this.b(obj);
        }

        @DexIgnore
        public Map<K, V> b() {
            return g4.this;
        }

        @DexIgnore
        public void a(K k, V v) {
            g4.this.put(k, v);
        }

        @DexIgnore
        public V a(int i, V v) {
            return g4.this.a(i, v);
        }

        @DexIgnore
        public void a(int i) {
            g4.this.d(i);
        }

        @DexIgnore
        public void a() {
            g4.this.clear();
        }
    }

    @DexIgnore
    public g4() {
    }

    @DexIgnore
    public boolean a(Collection<?> collection) {
        return l4.c(this, collection);
    }

    @DexIgnore
    public final l4<K, V> b() {
        if (this.l == null) {
            this.l = new a();
        }
        return this.l;
    }

    @DexIgnore
    public Set<Map.Entry<K, V>> entrySet() {
        return b().d();
    }

    @DexIgnore
    public Set<K> keySet() {
        return b().e();
    }

    @DexIgnore
    public void putAll(Map<? extends K, ? extends V> map) {
        b(this.g + map.size());
        for (Map.Entry next : map.entrySet()) {
            put(next.getKey(), next.getValue());
        }
    }

    @DexIgnore
    public Collection<V> values() {
        return b().f();
    }

    @DexIgnore
    public g4(int i) {
        super(i);
    }

    @DexIgnore
    public g4(SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }
}
