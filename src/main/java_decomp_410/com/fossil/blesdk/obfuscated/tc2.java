package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tc2 extends sc2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = new ViewDataBinding.j(26);
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout F;
    @DexIgnore
    public long G;

    /*
    static {
        H.a(1, new String[]{"view_no_device"}, new int[]{2}, new int[]{R.layout.view_no_device});
        I.put(R.id.ftv_alerts, 3);
        I.put(R.id.ftv_alarms_section, 4);
        I.put(R.id.ftv_add, 5);
        I.put(R.id.rv_alarms, 6);
        I.put(R.id.ftv_notifications_section, 7);
        I.put(R.id.ll_calls_and_messages, 8);
        I.put(R.id.v_line1, 9);
        I.put(R.id.ll_apps, 10);
        I.put(R.id.ftv_apps_overview, 11);
        I.put(R.id.v_line2, 12);
        I.put(R.id.ll_watch_reminders, 13);
        I.put(R.id.ftv_watch_reminders_overview, 14);
        I.put(R.id.ftv_do_not_disturb_section, 15);
        I.put(R.id.ftv_scheduled, 16);
        I.put(R.id.sw_scheduled, 17);
        I.put(R.id.cl_scheduled_time_container, 18);
        I.put(R.id.ftv_scheduled_start_label, 19);
        I.put(R.id.ll_scheduled_time_start, 20);
        I.put(R.id.ftv_scheduled_start_value, 21);
        I.put(R.id.ftv_scheduled_end_label, 22);
        I.put(R.id.ll_scheduled_time_end, 23);
        I.put(R.id.ftv_scheduled_end_value, 24);
        I.put(R.id.ftv_no_device_title, 25);
    }
    */

    @DexIgnore
    public tc2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 26, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
        ViewDataBinding.d(this.x);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        if (r6.x.e() == false) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        return true;
     */
    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 2;
        }
        this.x.f();
        g();
    }

    @DexIgnore
    public tc2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 1, objArr[1], objArr[18], objArr[5], objArr[4], objArr[3], objArr[11], objArr[15], objArr[25], objArr[7], objArr[16], objArr[22], objArr[24], objArr[19], objArr[21], objArr[14], objArr[2], objArr[10], objArr[8], objArr[23], objArr[20], objArr[13], objArr[6], objArr[17], objArr[9], objArr[12]);
        this.G = -1;
        this.q.setTag((Object) null);
        this.F = objArr[0];
        this.F.setTag((Object) null);
        a(view);
        f();
    }
}
