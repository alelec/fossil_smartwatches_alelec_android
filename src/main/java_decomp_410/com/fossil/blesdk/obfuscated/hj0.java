package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hj0 {
    @DexIgnore
    public static ApiException a(Status status) {
        if (status.K()) {
            return new ResolvableApiException(status);
        }
        return new ApiException(status);
    }
}
