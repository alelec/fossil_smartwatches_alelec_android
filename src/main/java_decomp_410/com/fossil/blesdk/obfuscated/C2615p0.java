package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p0 */
public class C2615p0 extends android.graphics.drawable.Drawable implements android.graphics.drawable.Drawable.Callback {

    @DexIgnore
    /* renamed from: e */
    public android.graphics.drawable.Drawable f8261e;

    @DexIgnore
    public C2615p0(android.graphics.drawable.Drawable drawable) {
        mo14575a(drawable);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.drawable.Drawable mo14574a() {
        return this.f8261e;
    }

    @DexIgnore
    public void draw(android.graphics.Canvas canvas) {
        this.f8261e.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return this.f8261e.getChangingConfigurations();
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getCurrent() {
        return this.f8261e.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f8261e.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.f8261e.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.f8261e.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.f8261e.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.f8261e.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(android.graphics.Rect rect) {
        return this.f8261e.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.f8261e.getState();
    }

    @DexIgnore
    public android.graphics.Region getTransparentRegion() {
        return this.f8261e.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(android.graphics.drawable.Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return com.fossil.blesdk.obfuscated.C1538c7.m5311f(this.f8261e);
    }

    @DexIgnore
    public boolean isStateful() {
        return this.f8261e.isStateful();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        com.fossil.blesdk.obfuscated.C1538c7.m5312g(this.f8261e);
    }

    @DexIgnore
    public void onBoundsChange(android.graphics.Rect rect) {
        this.f8261e.setBounds(rect);
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.f8261e.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.f8261e.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        com.fossil.blesdk.obfuscated.C1538c7.m5303a(this.f8261e, z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.f8261e.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(android.graphics.ColorFilter colorFilter) {
        this.f8261e.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.f8261e.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.f8261e.setFilterBitmap(z);
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        com.fossil.blesdk.obfuscated.C1538c7.m5297a(this.f8261e, f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        com.fossil.blesdk.obfuscated.C1538c7.m5298a(this.f8261e, i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return this.f8261e.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i) {
        com.fossil.blesdk.obfuscated.C1538c7.m5307b(this.f8261e, i);
    }

    @DexIgnore
    public void setTintList(android.content.res.ColorStateList colorStateList) {
        com.fossil.blesdk.obfuscated.C1538c7.m5299a(this.f8261e, colorStateList);
    }

    @DexIgnore
    public void setTintMode(android.graphics.PorterDuff.Mode mode) {
        com.fossil.blesdk.obfuscated.C1538c7.m5302a(this.f8261e, mode);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f8261e.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(android.graphics.drawable.Drawable drawable, java.lang.Runnable runnable) {
        unscheduleSelf(runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14575a(android.graphics.drawable.Drawable drawable) {
        android.graphics.drawable.Drawable drawable2 = this.f8261e;
        if (drawable2 != null) {
            drawable2.setCallback((android.graphics.drawable.Drawable.Callback) null);
        }
        this.f8261e = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
