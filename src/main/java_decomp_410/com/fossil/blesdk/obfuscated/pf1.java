package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pf1 implements Parcelable.Creator<LatLng> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        double d = 0.0d;
        double d2 = 0.0d;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                d = SafeParcelReader.l(parcel, a);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                d2 = SafeParcelReader.l(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new LatLng(d, d2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LatLng[i];
    }
}
