package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p53 implements MembersInjector<WatchAppSearchActivity> {
    @DexIgnore
    public static void a(WatchAppSearchActivity watchAppSearchActivity, WatchAppSearchPresenter watchAppSearchPresenter) {
        watchAppSearchActivity.B = watchAppSearchPresenter;
    }
}
