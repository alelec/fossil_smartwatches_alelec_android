package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* compiled from: lambda */
public final /* synthetic */ class d82 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon17 e;
    @DexIgnore
    private /* final */ /* synthetic */ List f;
    @DexIgnore
    private /* final */ /* synthetic */ PresetDataSource.DeleteMappingSetCallback g;

    @DexIgnore
    public /* synthetic */ d82(PresetRepository.Anon17 anon17, List list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        this.e = anon17;
        this.f = list;
        this.g = deleteMappingSetCallback;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.f, this.g);
    }
}
