package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rc2 extends qc2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ RelativeLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.cl_toolbar, 1);
        B.put(R.id.aciv_back, 2);
        B.put(R.id.tv_title, 3);
        B.put(R.id.ln_get_supports, 4);
        B.put(R.id.bt_faq, 5);
        B.put(R.id.bt_tutorial, 6);
        B.put(R.id.bt_repair_center, 7);
        B.put(R.id.tv_contacts, 8);
        B.put(R.id.wc_email, 9);
        B.put(R.id.tv_email, 10);
        B.put(R.id.wc_live_chat, 11);
        B.put(R.id.tv_live_chat, 12);
        B.put(R.id.wc_call_us, 13);
        B.put(R.id.tv_call_us, 14);
        B.put(R.id.tv_delete_account, 15);
        B.put(R.id.tv_app_version, 16);
    }
    */

    @DexIgnore
    public rc2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 17, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public rc2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[2], objArr[5], objArr[7], objArr[6], objArr[1], objArr[4], objArr[16], objArr[14], objArr[8], objArr[15], objArr[10], objArr[12], objArr[3], objArr[13], objArr[9], objArr[11]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
