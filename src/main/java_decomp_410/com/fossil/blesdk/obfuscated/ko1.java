package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ko1<TResult> implements qo1<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public sn1 c;

    @DexIgnore
    public ko1(Executor executor, sn1 sn1) {
        this.a = executor;
        this.c = sn1;
    }

    @DexIgnore
    public final void onComplete(wn1<TResult> wn1) {
        if (!wn1.e() && !wn1.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new lo1(this, wn1));
                }
            }
        }
    }
}
