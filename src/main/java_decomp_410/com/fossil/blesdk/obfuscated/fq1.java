package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fq1 implements Parcelable.Creator<eq1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i = 0;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 3:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 4:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 5:
                    str3 = SafeParcelReader.f(parcel2, a);
                    break;
                case 6:
                    str4 = SafeParcelReader.f(parcel2, a);
                    break;
                case 7:
                    str5 = SafeParcelReader.f(parcel2, a);
                    break;
                case 8:
                    str6 = SafeParcelReader.f(parcel2, a);
                    break;
                case 9:
                    b2 = SafeParcelReader.k(parcel2, a);
                    break;
                case 10:
                    b3 = SafeParcelReader.k(parcel2, a);
                    break;
                case 11:
                    b4 = SafeParcelReader.k(parcel2, a);
                    break;
                case 12:
                    b5 = SafeParcelReader.k(parcel2, a);
                    break;
                case 13:
                    str7 = SafeParcelReader.f(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new eq1(i, str, str2, str3, str4, str5, str6, b2, b3, b4, b5, str7);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new eq1[i];
    }
}
