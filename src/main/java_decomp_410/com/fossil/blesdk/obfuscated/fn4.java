package com.fossil.blesdk.obfuscated;

import java.net.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fn4 {
    @DexIgnore
    public static String a(dm4 dm4, Proxy.Type type) {
        StringBuilder sb = new StringBuilder();
        sb.append(dm4.e());
        sb.append(' ');
        if (b(dm4, type)) {
            sb.append(dm4.g());
        } else {
            sb.append(a(dm4.g()));
        }
        sb.append(" HTTP/1.1");
        return sb.toString();
    }

    @DexIgnore
    public static boolean b(dm4 dm4, Proxy.Type type) {
        return !dm4.d() && type == Proxy.Type.HTTP;
    }

    @DexIgnore
    public static String a(zl4 zl4) {
        String c = zl4.c();
        String e = zl4.e();
        if (e == null) {
            return c;
        }
        return c + '?' + e;
    }
}
