package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ao */
public class C1433ao implements java.io.Closeable {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.io.InputStream f3546e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ java.nio.charset.Charset f3547f;

    @DexIgnore
    /* renamed from: g */
    public byte[] f3548g;

    @DexIgnore
    /* renamed from: h */
    public int f3549h;

    @DexIgnore
    /* renamed from: i */
    public int f3550i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ao$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ao$a */
    public class C1434a extends java.io.ByteArrayOutputStream {
        @DexIgnore
        public C1434a(int i) {
            super(i);
        }

        @DexIgnore
        public java.lang.String toString() {
            int i = this.count;
            try {
                return new java.lang.String(this.buf, 0, (i <= 0 || this.buf[i + -1] != 13) ? this.count : i - 1, com.fossil.blesdk.obfuscated.C1433ao.this.f3547f.name());
            } catch (java.io.UnsupportedEncodingException e) {
                throw new java.lang.AssertionError(e);
            }
        }
    }

    @DexIgnore
    public C1433ao(java.io.InputStream inputStream, java.nio.charset.Charset charset) {
        this(inputStream, 8192, charset);
    }

    @DexIgnore
    /* renamed from: A */
    public java.lang.String mo8860A() throws java.io.IOException {
        int i;
        int i2;
        synchronized (this.f3546e) {
            if (this.f3548g != null) {
                if (this.f3549h >= this.f3550i) {
                    mo8862y();
                }
                for (int i3 = this.f3549h; i3 != this.f3550i; i3++) {
                    if (this.f3548g[i3] == 10) {
                        if (i3 != this.f3549h) {
                            i2 = i3 - 1;
                            if (this.f3548g[i2] == 13) {
                                java.lang.String str = new java.lang.String(this.f3548g, this.f3549h, i2 - this.f3549h, this.f3547f.name());
                                this.f3549h = i3 + 1;
                                return str;
                            }
                        }
                        i2 = i3;
                        java.lang.String str2 = new java.lang.String(this.f3548g, this.f3549h, i2 - this.f3549h, this.f3547f.name());
                        this.f3549h = i3 + 1;
                        return str2;
                    }
                }
                com.fossil.blesdk.obfuscated.C1433ao.C1434a aVar = new com.fossil.blesdk.obfuscated.C1433ao.C1434a((this.f3550i - this.f3549h) + 80);
                loop1:
                while (true) {
                    aVar.write(this.f3548g, this.f3549h, this.f3550i - this.f3549h);
                    this.f3550i = -1;
                    mo8862y();
                    i = this.f3549h;
                    while (true) {
                        if (i != this.f3550i) {
                            if (this.f3548g[i] == 10) {
                                break loop1;
                            }
                            i++;
                        }
                    }
                }
                if (i != this.f3549h) {
                    aVar.write(this.f3548g, this.f3549h, i - this.f3549h);
                }
                this.f3549h = i + 1;
                java.lang.String aVar2 = aVar.toString();
                return aVar2;
            }
            throw new java.io.IOException("LineReader is closed");
        }
    }

    @DexIgnore
    public void close() throws java.io.IOException {
        synchronized (this.f3546e) {
            if (this.f3548g != null) {
                this.f3548g = null;
                this.f3546e.close();
            }
        }
    }

    @DexIgnore
    /* renamed from: y */
    public final void mo8862y() throws java.io.IOException {
        java.io.InputStream inputStream = this.f3546e;
        byte[] bArr = this.f3548g;
        int read = inputStream.read(bArr, 0, bArr.length);
        if (read != -1) {
            this.f3549h = 0;
            this.f3550i = read;
            return;
        }
        throw new java.io.EOFException();
    }

    @DexIgnore
    /* renamed from: z */
    public boolean mo8863z() {
        return this.f3550i == -1;
    }

    @DexIgnore
    public C1433ao(java.io.InputStream inputStream, int i, java.nio.charset.Charset charset) {
        if (inputStream == null || charset == null) {
            throw new java.lang.NullPointerException();
        } else if (i < 0) {
            throw new java.lang.IllegalArgumentException("capacity <= 0");
        } else if (charset.equals(com.fossil.blesdk.obfuscated.C1496bo.f3793a)) {
            this.f3546e = inputStream;
            this.f3547f = charset;
            this.f3548g = new byte[i];
        } else {
            throw new java.lang.IllegalArgumentException("Unsupported encoding");
        }
    }
}
