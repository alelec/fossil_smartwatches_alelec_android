package com.fossil.blesdk.obfuscated;

import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class ny1 implements ThreadFactory {
    @DexIgnore
    public static /* final */ ThreadFactory a; // = new ny1();

    @DexIgnore
    public final Thread newThread(Runnable runnable) {
        return my1.a(runnable);
    }
}
