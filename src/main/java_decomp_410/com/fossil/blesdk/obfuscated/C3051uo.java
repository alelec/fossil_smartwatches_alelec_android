package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uo */
public class C3051uo {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2978to.C2979a<?> f10034b; // = new com.fossil.blesdk.obfuscated.C3051uo.C3052a();

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<java.lang.Class<?>, com.fossil.blesdk.obfuscated.C2978to.C2979a<?>> f10035a; // = new java.util.HashMap();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uo$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uo$a */
    public class C3052a implements com.fossil.blesdk.obfuscated.C2978to.C2979a<java.lang.Object> {
        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2978to<java.lang.Object> mo12468a(java.lang.Object obj) {
            return new com.fossil.blesdk.obfuscated.C3051uo.C3053b(obj);
        }

        @DexIgnore
        public java.lang.Class<java.lang.Object> getDataClass() {
            throw new java.lang.UnsupportedOperationException("Not implemented");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uo$b")
    /* renamed from: com.fossil.blesdk.obfuscated.uo$b */
    public static final class C3053b implements com.fossil.blesdk.obfuscated.C2978to<java.lang.Object> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.Object f10036a;

        @DexIgnore
        public C3053b(java.lang.Object obj) {
            this.f10036a = obj;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12466a() {
        }

        @DexIgnore
        /* renamed from: b */
        public java.lang.Object mo12467b() {
            return this.f10036a;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo16812a(com.fossil.blesdk.obfuscated.C2978to.C2979a<?> aVar) {
        this.f10035a.put(aVar.getDataClass(), aVar);
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized <T> com.fossil.blesdk.obfuscated.C2978to<T> mo16811a(T t) {
        com.fossil.blesdk.obfuscated.C2978to.C2979a<?> aVar;
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(t);
        aVar = this.f10035a.get(t.getClass());
        if (aVar == null) {
            java.util.Iterator<com.fossil.blesdk.obfuscated.C2978to.C2979a<?>> it = this.f10035a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                com.fossil.blesdk.obfuscated.C2978to.C2979a<?> next = it.next();
                if (next.getDataClass().isAssignableFrom(t.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = f10034b;
        }
        return aVar.mo12468a(t);
    }
}
