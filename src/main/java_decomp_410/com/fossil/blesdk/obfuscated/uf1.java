package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class uf1 implements Parcelable.Creator<GoogleMapOptions> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        CameraPosition cameraPosition = null;
        Float f = null;
        Float f2 = null;
        LatLngBounds latLngBounds = null;
        byte b2 = -1;
        byte b3 = -1;
        int i = 0;
        byte b4 = -1;
        byte b5 = -1;
        byte b6 = -1;
        byte b7 = -1;
        byte b8 = -1;
        byte b9 = -1;
        byte b10 = -1;
        byte b11 = -1;
        byte b12 = -1;
        byte b13 = -1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    b2 = SafeParcelReader.k(parcel2, a);
                    break;
                case 3:
                    b3 = SafeParcelReader.k(parcel2, a);
                    break;
                case 4:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 5:
                    cameraPosition = SafeParcelReader.a(parcel2, a, CameraPosition.CREATOR);
                    break;
                case 6:
                    b4 = SafeParcelReader.k(parcel2, a);
                    break;
                case 7:
                    b5 = SafeParcelReader.k(parcel2, a);
                    break;
                case 8:
                    b6 = SafeParcelReader.k(parcel2, a);
                    break;
                case 9:
                    b7 = SafeParcelReader.k(parcel2, a);
                    break;
                case 10:
                    b8 = SafeParcelReader.k(parcel2, a);
                    break;
                case 11:
                    b9 = SafeParcelReader.k(parcel2, a);
                    break;
                case 12:
                    b10 = SafeParcelReader.k(parcel2, a);
                    break;
                case 14:
                    b11 = SafeParcelReader.k(parcel2, a);
                    break;
                case 15:
                    b12 = SafeParcelReader.k(parcel2, a);
                    break;
                case 16:
                    f = SafeParcelReader.o(parcel2, a);
                    break;
                case 17:
                    f2 = SafeParcelReader.o(parcel2, a);
                    break;
                case 18:
                    latLngBounds = SafeParcelReader.a(parcel2, a, LatLngBounds.CREATOR);
                    break;
                case 19:
                    b13 = SafeParcelReader.k(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new GoogleMapOptions(b2, b3, i, cameraPosition, b4, b5, b6, b7, b8, b9, b10, b11, b12, f, f2, latLngBounds, b13);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleMapOptions[i];
    }
}
