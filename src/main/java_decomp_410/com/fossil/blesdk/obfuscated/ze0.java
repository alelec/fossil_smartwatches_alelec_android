package com.fossil.blesdk.obfuscated;

import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ze0<L> {
    @DexIgnore
    public /* final */ c a;
    @DexIgnore
    public volatile L b;
    @DexIgnore
    public /* final */ a<L> c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<L> {
        @DexIgnore
        public /* final */ L a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public a(L l, String str) {
            this.a = l;
            this.b = str;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.b.equals(aVar.b);
        }

        @DexIgnore
        public final int hashCode() {
            return (System.identityHashCode(this.a) * 31) + this.b.hashCode();
        }
    }

    @DexIgnore
    public interface b<L> {
        @DexIgnore
        void a();

        @DexIgnore
        void a(L l);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends ss0 {
        @DexIgnore
        public c(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            boolean z = true;
            if (message.what != 1) {
                z = false;
            }
            bk0.a(z);
            ze0.this.b((b) message.obj);
        }
    }

    @DexIgnore
    public ze0(Looper looper, L l, String str) {
        this.a = new c(looper);
        bk0.a(l, (Object) "Listener must not be null");
        this.b = l;
        bk0.b(str);
        this.c = new a<>(l, str);
    }

    @DexIgnore
    public final void a(b<? super L> bVar) {
        bk0.a(bVar, (Object) "Notifier must not be null");
        this.a.sendMessage(this.a.obtainMessage(1, bVar));
    }

    @DexIgnore
    public final a<L> b() {
        return this.c;
    }

    @DexIgnore
    public final void b(b<? super L> bVar) {
        L l = this.b;
        if (l == null) {
            bVar.a();
            return;
        }
        try {
            bVar.a(l);
        } catch (RuntimeException e) {
            bVar.a();
            throw e;
        }
    }

    @DexIgnore
    public final void a() {
        this.b = null;
    }
}
