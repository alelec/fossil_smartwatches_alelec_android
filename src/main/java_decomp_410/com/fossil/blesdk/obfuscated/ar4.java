package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ar4<T> {
    @DexIgnore
    public T a;

    @DexIgnore
    public ar4(T t) {
        this.a = t;
    }

    @DexIgnore
    public static ar4<? extends Activity> a(Activity activity) {
        if (Build.VERSION.SDK_INT < 23) {
            return new zq4(activity);
        }
        if (activity instanceof AppCompatActivity) {
            return new xq4((AppCompatActivity) activity);
        }
        return new wq4(activity);
    }

    @DexIgnore
    public abstract Context a();

    @DexIgnore
    public abstract void a(int i, String... strArr);

    @DexIgnore
    public T b() {
        return this.a;
    }

    @DexIgnore
    public abstract void b(String str, String str2, String str3, int i, int i2, String... strArr);

    @DexIgnore
    public abstract boolean b(String str);

    @DexIgnore
    public static ar4<Fragment> a(Fragment fragment) {
        if (Build.VERSION.SDK_INT < 23) {
            return new zq4(fragment);
        }
        return new br4(fragment);
    }

    @DexIgnore
    public final boolean a(String... strArr) {
        for (String b : strArr) {
            if (b(b)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void a(String str, String str2, String str3, int i, int i2, String... strArr) {
        if (a(strArr)) {
            b(str, str2, str3, i, i2, strArr);
        } else {
            a(i2, strArr);
        }
    }

    @DexIgnore
    public boolean a(List<String> list) {
        for (String a2 : list) {
            if (a(a2)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(String str) {
        return !b(str);
    }
}
