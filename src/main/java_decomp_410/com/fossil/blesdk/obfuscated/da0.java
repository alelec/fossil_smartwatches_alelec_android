package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.JSONKey;
import com.fossil.blesdk.setting.SharedPreferenceFileName;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class da0 extends aa0 {
    @DexIgnore
    public static long k; // = 60000;
    @DexIgnore
    public static /* final */ da0 l; // = new da0();

    @DexIgnore
    public da0() {
        super("sdk_log", 204800, "sdk_log", "sdklog", new na0("", "", ""), 1800, new ba0(), SharedPreferenceFileName.SDK_LOG_PREFERENCE, true);
    }

    @DexIgnore
    public final void a(Exception exc) {
        kd4.b(exc, "e");
        JSONObject jSONObject = new JSONObject();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        exc.printStackTrace(new PrintWriter(byteArrayOutputStream, true));
        JSONKey jSONKey = JSONKey.STACK_TRACE;
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        kd4.a((Object) byteArray, "byteArrayOutputStream.toByteArray()");
        wa0.a(jSONObject, jSONKey, new String(byteArray, ua0.y.f()));
        String localizedMessage = exc.getLocalizedMessage();
        kd4.a((Object) localizedMessage, "e.localizedMessage");
        EventType eventType = EventType.EXCEPTION;
        String canonicalName = exc.getClass().getCanonicalName();
        if (canonicalName == null) {
            canonicalName = "";
        }
        String uuid = UUID.randomUUID().toString();
        kd4.a((Object) uuid, "UUID.randomUUID().toString()");
        b(new SdkLogEntry(localizedMessage, eventType, "", canonicalName, uuid, false, (String) null, (DeviceInformation) null, (ea0) null, jSONObject, 448, (fd4) null));
    }

    @DexIgnore
    public long b() {
        return k;
    }
}
