package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ra2 extends qa2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j t; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray u; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public long s;

    /*
    static {
        u.put(R.id.tv_change_watch_face, 1);
        u.put(R.id.rv_background, 2);
    }
    */

    @DexIgnore
    public ra2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 3, t, u));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.s = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.s != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.s = 1;
        }
        g();
    }

    @DexIgnore
    public ra2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[2], objArr[1]);
        this.s = -1;
        this.r = objArr[0];
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
