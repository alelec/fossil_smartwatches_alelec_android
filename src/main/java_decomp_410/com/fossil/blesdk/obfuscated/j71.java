package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.j71;
import com.fossil.blesdk.obfuscated.k71;
import com.google.android.gms.internal.measurement.zzte;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class j71<MessageType extends j71<MessageType, BuilderType>, BuilderType extends k71<MessageType, BuilderType>> implements w91 {
    @DexIgnore
    public static boolean zzbtl;
    @DexIgnore
    public int zzbtk; // = 0;

    @DexIgnore
    public void a(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final zzte d() {
        try {
            w71 zzao = zzte.zzao(e());
            a(zzao.b());
            return zzao.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public int g() {
        throw new UnsupportedOperationException();
    }
}
