package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ja3 implements MembersInjector<CaloriesOverviewFragment> {
    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, CaloriesOverviewDayPresenter caloriesOverviewDayPresenter) {
        caloriesOverviewFragment.k = caloriesOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
        caloriesOverviewFragment.l = caloriesOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(CaloriesOverviewFragment caloriesOverviewFragment, CaloriesOverviewMonthPresenter caloriesOverviewMonthPresenter) {
        caloriesOverviewFragment.m = caloriesOverviewMonthPresenter;
    }
}
