package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SessionEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class rx {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ tx b;
    @DexIgnore
    public qx c;

    @DexIgnore
    public rx(Context context) {
        this(context, new tx());
    }

    @DexIgnore
    public qx a() {
        if (this.c == null) {
            this.c = kx.b(this.a);
        }
        return this.c;
    }

    @DexIgnore
    public rx(Context context, tx txVar) {
        this.a = context;
        this.b = txVar;
    }

    @DexIgnore
    public void a(SessionEvent sessionEvent) {
        qx a2 = a();
        if (a2 == null) {
            q44.g().d("Answers", "Firebase analytics logging was enabled, but not available...");
            return;
        }
        sx a3 = this.b.a(sessionEvent);
        if (a3 == null) {
            y44 g = q44.g();
            g.d("Answers", "Fabric event was not mappable to Firebase event: " + sessionEvent);
            return;
        }
        a2.a(a3.a(), a3.b());
        if ("levelEnd".equals(sessionEvent.g)) {
            a2.a("post_score", a3.b());
        }
    }
}
