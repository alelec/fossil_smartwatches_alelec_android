package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class p91 implements v91 {
    @DexIgnore
    public v91[] a;

    @DexIgnore
    public p91(v91... v91Arr) {
        this.a = v91Arr;
    }

    @DexIgnore
    public final u91 a(Class<?> cls) {
        for (v91 v91 : this.a) {
            if (v91.b(cls)) {
                return v91.a(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }

    @DexIgnore
    public final boolean b(Class<?> cls) {
        for (v91 b : this.a) {
            if (b.b(cls)) {
                return true;
            }
        }
        return false;
    }
}
