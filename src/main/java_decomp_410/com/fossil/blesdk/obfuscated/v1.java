package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.fossil.blesdk.obfuscated.h1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v1 extends h1 implements SubMenu {
    @DexIgnore
    public h1 B;
    @DexIgnore
    public k1 C;

    @DexIgnore
    public v1(Context context, h1 h1Var, k1 k1Var) {
        super(context);
        this.B = h1Var;
        this.C = k1Var;
    }

    @DexIgnore
    public void a(h1.a aVar) {
        this.B.a(aVar);
    }

    @DexIgnore
    public boolean b(k1 k1Var) {
        return this.B.b(k1Var);
    }

    @DexIgnore
    public String d() {
        k1 k1Var = this.C;
        int itemId = k1Var != null ? k1Var.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.d() + ":" + itemId;
    }

    @DexIgnore
    public MenuItem getItem() {
        return this.C;
    }

    @DexIgnore
    public h1 m() {
        return this.B.m();
    }

    @DexIgnore
    public boolean o() {
        return this.B.o();
    }

    @DexIgnore
    public boolean p() {
        return this.B.p();
    }

    @DexIgnore
    public boolean q() {
        return this.B.q();
    }

    @DexIgnore
    public void setGroupDividerEnabled(boolean z) {
        this.B.setGroupDividerEnabled(z);
    }

    @DexIgnore
    public SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(Drawable drawable) {
        this.C.setIcon(drawable);
        return this;
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        this.B.setQwertyMode(z);
    }

    @DexIgnore
    public Menu t() {
        return this.B;
    }

    @DexIgnore
    public boolean a(h1 h1Var, MenuItem menuItem) {
        return super.a(h1Var, menuItem) || this.B.a(h1Var, menuItem);
    }

    @DexIgnore
    public SubMenu setHeaderIcon(int i) {
        super.d(i);
        return this;
    }

    @DexIgnore
    public SubMenu setHeaderTitle(int i) {
        super.e(i);
        return this;
    }

    @DexIgnore
    public SubMenu setIcon(int i) {
        this.C.setIcon(i);
        return this;
    }

    @DexIgnore
    public boolean a(k1 k1Var) {
        return this.B.a(k1Var);
    }
}
