package com.fossil.blesdk.obfuscated;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z31 extends ud1 {
    @DexIgnore
    public /* final */ ze0<qc1> e;

    @DexIgnore
    public z31(ze0<qc1> ze0) {
        this.e = ze0;
    }

    @DexIgnore
    public final void a(LocationAvailability locationAvailability) {
        this.e.a(new b41(this, locationAvailability));
    }

    @DexIgnore
    public final void a(LocationResult locationResult) {
        this.e.a(new a41(this, locationResult));
    }

    @DexIgnore
    public final synchronized void o() {
        this.e.a();
    }
}
