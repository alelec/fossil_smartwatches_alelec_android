package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class i91 {
    @DexIgnore
    public static /* final */ i91 a; // = new k91();
    @DexIgnore
    public static /* final */ i91 b; // = new l91();

    @DexIgnore
    public i91() {
    }

    @DexIgnore
    public static i91 a() {
        return a;
    }

    @DexIgnore
    public static i91 b() {
        return b;
    }

    @DexIgnore
    public abstract <L> List<L> a(Object obj, long j);

    @DexIgnore
    public abstract <L> void a(Object obj, Object obj2, long j);

    @DexIgnore
    public abstract void b(Object obj, long j);
}
