package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.ij0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wm1 extends oj0<cn1> implements ln1 {
    @DexIgnore
    public /* final */ boolean E;
    @DexIgnore
    public /* final */ kj0 F;
    @DexIgnore
    public /* final */ Bundle G;
    @DexIgnore
    public Integer H;

    @DexIgnore
    public wm1(Context context, Looper looper, boolean z, kj0 kj0, Bundle bundle, ge0.b bVar, ge0.c cVar) {
        super(context, looper, 44, kj0, bVar, cVar);
        this.E = true;
        this.F = kj0;
        this.G = bundle;
        this.H = kj0.e();
    }

    @DexIgnore
    public final void a(tj0 tj0, boolean z) {
        try {
            ((cn1) x()).a(tj0, this.H.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    @DexIgnore
    public final void b() {
        a((ij0.c) new ij0.d());
    }

    @DexIgnore
    public final void g() {
        try {
            ((cn1) x()).c(this.H.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    @DexIgnore
    public int i() {
        return zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public boolean l() {
        return this.E;
    }

    @DexIgnore
    public Bundle u() {
        if (!t().getPackageName().equals(this.F.h())) {
            this.G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.F.h());
        }
        return this.G;
    }

    @DexIgnore
    public String y() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    @DexIgnore
    public String z() {
        return "com.google.android.gms.signin.service.START";
    }

    @DexIgnore
    public final void a(an1 an1) {
        bk0.a(an1, (Object) "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.F.c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = cc0.a(t()).b();
            }
            ((cn1) x()).a(new en1(new ck0(c, this.H.intValue(), googleSignInAccount)), an1);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                an1.a(new gn1(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    @DexIgnore
    public wm1(Context context, Looper looper, boolean z, kj0 kj0, vm1 vm1, ge0.b bVar, ge0.c cVar) {
        this(context, looper, true, kj0, a(kj0), bVar, cVar);
    }

    @DexIgnore
    public static Bundle a(kj0 kj0) {
        vm1 j = kj0.j();
        Integer e = kj0.e();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", kj0.a());
        if (e != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", e.intValue());
        }
        if (j != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", j.g());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", j.f());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", j.d());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", j.e());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", j.b());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", j.h());
            if (j.a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", j.a().longValue());
            }
            if (j.c() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", j.c().longValue());
            }
        }
        return bundle;
    }

    @DexIgnore
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof cn1) {
            return (cn1) queryLocalInterface;
        }
        return new dn1(iBinder);
    }
}
