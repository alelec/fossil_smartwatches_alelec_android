package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qc0 extends IInterface {
    @DexIgnore
    void k() throws RemoteException;

    @DexIgnore
    void l() throws RemoteException;
}
