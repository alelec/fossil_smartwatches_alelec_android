package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vs2 extends RecyclerView.g<b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public c a;
    @DexIgnore
    public List<? extends Object> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ rv d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ gi2 a;
        @DexIgnore
        public /* final */ /* synthetic */ vs2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public a(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List a = this.e.b.b;
                    if (a != null) {
                        Object obj = a.get(adapterPosition);
                        if (obj instanceof ContactWrapper) {
                            ContactWrapper contactWrapper = (ContactWrapper) obj;
                            Contact contact = contactWrapper.getContact();
                            if (contact == null || contact.getContactId() != -100) {
                                Contact contact2 = contactWrapper.getContact();
                                if (contact2 == null || contact2.getContactId() != -200) {
                                    c b = this.e.b.a;
                                    if (b != null) {
                                        b.a(contactWrapper);
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vs2$b$b")
        /* renamed from: com.fossil.blesdk.obfuscated.vs2$b$b  reason: collision with other inner class name */
        public static final class C0108b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public C0108b(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                FLogger.INSTANCE.getLocal().d(vs2.e, "ivRemove.setOnClickListener");
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List a = this.e.b.b;
                    if (a != null) {
                        Object obj = a.get(adapterPosition);
                        if (obj instanceof ContactWrapper) {
                            ContactWrapper contactWrapper = (ContactWrapper) obj;
                            contactWrapper.setAdded(!contactWrapper.isAdded());
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String c = vs2.e;
                            local.d(c, "isAdded=" + contactWrapper.isAdded());
                            if (contactWrapper.isAdded()) {
                                vs2 vs2 = this.e.b;
                                vs2.c = vs2.c + 1;
                            } else {
                                vs2 vs22 = this.e.b;
                                vs22.c = vs22.c - 1;
                            }
                        } else if (obj != null) {
                            AppWrapper appWrapper = (AppWrapper) obj;
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            if (installedApp != null) {
                                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                                Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                                if (isSelected != null) {
                                    installedApp.setSelected(!isSelected.booleanValue());
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String c2 = vs2.e;
                            StringBuilder sb = new StringBuilder();
                            sb.append("isSelected=");
                            InstalledApp installedApp3 = appWrapper.getInstalledApp();
                            Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                            if (isSelected2 != null) {
                                sb.append(isSelected2.booleanValue());
                                local2.d(c2, sb.toString());
                                InstalledApp installedApp4 = appWrapper.getInstalledApp();
                                Boolean isSelected3 = installedApp4 != null ? installedApp4.isSelected() : null;
                                if (isSelected3 == null) {
                                    kd4.a();
                                    throw null;
                                } else if (isSelected3.booleanValue()) {
                                    vs2 vs23 = this.e.b;
                                    vs23.c = vs23.c + 1;
                                } else {
                                    vs2 vs24 = this.e.b;
                                    vs24.c = vs24.c - 1;
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                        }
                        c b = this.e.b.a;
                        if (b != null) {
                            b.a();
                        }
                        this.e.b.notifyItemChanged(adapterPosition);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements qv<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ b e;
            @DexIgnore
            public /* final */ /* synthetic */ ContactWrapper f;

            @DexIgnore
            public c(b bVar, ContactWrapper contactWrapper) {
                this.e = bVar;
                this.f = contactWrapper;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, bw<Drawable> bwVar, DataSource dataSource, boolean z) {
                FLogger.INSTANCE.getLocal().d(vs2.e, "renderContactData onResourceReady");
                this.e.a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            public boolean a(GlideException glideException, Object obj, bw<Drawable> bwVar, boolean z) {
                FLogger.INSTANCE.getLocal().d(vs2.e, "renderContactData onLoadFailed");
                Contact contact = this.f.getContact();
                if (contact == null) {
                    return false;
                }
                contact.setPhotoThumbUri((String) null);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(vs2 vs2, gi2 gi2) {
            super(gi2.d());
            kd4.b(gi2, "binding");
            this.b = vs2;
            this.a = gi2;
            this.a.q.setOnClickListener(new a(this));
            this.a.u.setOnClickListener(new C0108b(this));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
            if (r2 != null) goto L_0x003a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
            if (r4 != null) goto L_0x0048;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:86:0x0226  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x0231  */
        public final void a(ContactWrapper contactWrapper) {
            Uri uri;
            String str;
            String str2;
            String str3;
            String str4;
            kd4.b(contactWrapper, "contactWrapper");
            Contact contact = contactWrapper.getContact();
            if (!TextUtils.isEmpty(contact != null ? contact.getPhotoThumbUri() : null)) {
                Contact contact2 = contactWrapper.getContact();
                uri = Uri.parse(contact2 != null ? contact2.getPhotoThumbUri() : null);
            } else {
                uri = null;
            }
            Contact contact3 = contactWrapper.getContact();
            String str5 = "";
            if (contact3 != null) {
                str = contact3.getFirstName();
            }
            str = str5;
            Contact contact4 = contactWrapper.getContact();
            if (contact4 != null) {
                str2 = contact4.getLastName();
            }
            str2 = str5;
            Contact contact5 = contactWrapper.getContact();
            if (contact5 == null || contact5.getContactId() != -100) {
                Contact contact6 = contactWrapper.getContact();
                if (contact6 == null || contact6.getContactId() != -200) {
                    String str6 = str + " " + str2;
                    fk2 a2 = ck2.a((View) this.a.t);
                    Contact contact7 = contactWrapper.getContact();
                    ek2<Drawable> a3 = a2.a((Object) new bk2(uri, contact7 != null ? contact7.getDisplayName() : null)).a((lv<?>) this.b.d);
                    fk2 a4 = ck2.a((View) this.a.t);
                    Contact contact8 = contactWrapper.getContact();
                    kd4.a((Object) a3.a((wn<Drawable>) a4.a((Object) new bk2((Uri) null, contact8 != null ? contact8.getDisplayName() : null)).a((lv<?>) this.b.d)).b(new c(this, contactWrapper)).a(this.a.t), "GlideApp.with(binding.iv\u2026nto(binding.ivHybridIcon)");
                    str3 = str6;
                } else {
                    this.a.t.setImageDrawable(k6.c(PortfolioApp.W.c(), R.drawable.ic_notifications_texts));
                    str3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_Everyone_Text__MessagesFromEveryone);
                    kd4.a((Object) str3, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
                }
            } else {
                this.a.t.setImageDrawable(k6.c(PortfolioApp.W.c(), R.drawable.ic_notifications_calls));
                str3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_Everyone_Text__CallsFromEveryone);
                kd4.a((Object) str3, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
            }
            FlexibleTextView flexibleTextView = this.a.s;
            kd4.a((Object) flexibleTextView, "binding.ftvHybridName");
            flexibleTextView.setText(str3);
            Contact contact9 = contactWrapper.getContact();
            if (contact9 == null || contact9.getContactId() != -100) {
                Contact contact10 = contactWrapper.getContact();
                if (contact10 == null || contact10.getContactId() != -200) {
                    Contact contact11 = contactWrapper.getContact();
                    Boolean valueOf = contact11 != null ? Boolean.valueOf(contact11.isUseSms()) : null;
                    if (valueOf != null) {
                        if (valueOf.booleanValue()) {
                            Contact contact12 = contactWrapper.getContact();
                            Boolean valueOf2 = contact12 != null ? Boolean.valueOf(contact12.isUseCall()) : null;
                            if (valueOf2 == null) {
                                kd4.a();
                                throw null;
                            } else if (valueOf2.booleanValue()) {
                                str4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactAssignedTapped_Text__CallsMessages);
                                kd4.a((Object) str4, "LanguageHelper.getString\u2026pped_Text__CallsMessages)");
                                FlexibleTextView flexibleTextView2 = this.a.r;
                                kd4.a((Object) flexibleTextView2, "binding.ftvHybridFeature");
                                flexibleTextView2.setText(str4);
                                FlexibleTextView flexibleTextView3 = this.a.r;
                                kd4.a((Object) flexibleTextView3, "binding.ftvHybridFeature");
                                flexibleTextView3.setVisibility(0);
                                if (contactWrapper.isAdded()) {
                                    this.a.u.setImageResource(R.drawable.ic_sat);
                                    return;
                                } else {
                                    this.a.u.setImageResource(R.drawable.ic_plus_circle);
                                    return;
                                }
                            }
                        }
                        Contact contact13 = contactWrapper.getContact();
                        Boolean valueOf3 = contact13 != null ? Boolean.valueOf(contact13.isUseSms()) : null;
                        if (valueOf3 != null) {
                            if (valueOf3.booleanValue()) {
                                str5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactAssignedTapped_Text__Messages);
                                kd4.a((Object) str5, "LanguageHelper.getString\u2026nedTapped_Text__Messages)");
                            }
                            str4 = str5;
                            Contact contact14 = contactWrapper.getContact();
                            Boolean valueOf4 = contact14 != null ? Boolean.valueOf(contact14.isUseCall()) : null;
                            if (valueOf4 != null) {
                                if (valueOf4.booleanValue()) {
                                    str4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactAssignedTapped_Text__Calls);
                                    kd4.a((Object) str4, "LanguageHelper.getString\u2026signedTapped_Text__Calls)");
                                }
                                FlexibleTextView flexibleTextView22 = this.a.r;
                                kd4.a((Object) flexibleTextView22, "binding.ftvHybridFeature");
                                flexibleTextView22.setText(str4);
                                FlexibleTextView flexibleTextView32 = this.a.r;
                                kd4.a((Object) flexibleTextView32, "binding.ftvHybridFeature");
                                flexibleTextView32.setVisibility(0);
                                if (contactWrapper.isAdded()) {
                                }
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
            FlexibleTextView flexibleTextView4 = this.a.r;
            kd4.a((Object) flexibleTextView4, "binding.ftvHybridFeature");
            flexibleTextView4.setText(str5);
            FlexibleTextView flexibleTextView5 = this.a.r;
            kd4.a((Object) flexibleTextView5, "binding.ftvHybridFeature");
            flexibleTextView5.setVisibility(8);
            if (contactWrapper.isAdded()) {
            }
        }

        @DexIgnore
        public final void a(AppWrapper appWrapper) {
            kd4.b(appWrapper, "appWrapper");
            fk2 a2 = ck2.a((View) this.a.t);
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new zj2(installedApp)).a((lv<?>) ((rv) new rv().a((oo<Bitmap>) new nk2())).c()).a(this.a.t);
                FlexibleTextView flexibleTextView = this.a.s;
                kd4.a((Object) flexibleTextView, "binding.ftvHybridName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                FlexibleTextView flexibleTextView2 = this.a.r;
                kd4.a((Object) flexibleTextView2, "binding.ftvHybridFeature");
                flexibleTextView2.setVisibility(8);
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected == null) {
                    kd4.a();
                    throw null;
                } else if (isSelected.booleanValue()) {
                    this.a.u.setImageResource(R.drawable.ic_sat);
                } else {
                    this.a.u.setImageResource(R.drawable.ic_plus_circle);
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(ContactWrapper contactWrapper);
    }

    /*
    static {
        new a((fd4) null);
        String name = vs2.class.getName();
        kd4.a((Object) name, "NotificationContactsAndA\u2026dAdapter::class.java.name");
        e = name;
    }
    */

    @DexIgnore
    public vs2() {
        lv a2 = ((rv) ((rv) ((rv) new rv().a((oo<Bitmap>) new nk2())).c()).a(pp.a)).a(true);
        kd4.a((Object) a2, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        this.d = (rv) a2;
    }

    @DexIgnore
    public int getItemCount() {
        List<? extends Object> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final boolean b() {
        return getItemCount() + this.c <= 12;
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        gi2 a2 = gi2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        kd4.a((Object) a2, "ItemNotificationHybridBi\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    public final void a(List<? extends Object> list) {
        kd4.b(list, "data");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        kd4.b(bVar, "holder");
        List<? extends Object> list = this.b;
        if (list != null) {
            Object obj = list.get(i);
            if (obj instanceof ContactWrapper) {
                bVar.a((ContactWrapper) obj);
            } else if (obj != null) {
                bVar.a((AppWrapper) obj);
            } else {
                throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        kd4.b(cVar, "listener");
        this.a = cVar;
    }
}
