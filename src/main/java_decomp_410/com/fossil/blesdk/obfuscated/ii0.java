package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ii0 implements bh0 {
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ eg0 f;
    @DexIgnore
    public /* final */ Looper g;
    @DexIgnore
    public /* final */ ng0 h;
    @DexIgnore
    public /* final */ ng0 i;
    @DexIgnore
    public /* final */ Map<de0.c<?>, ng0> j;
    @DexIgnore
    public /* final */ Set<cf0> k; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ de0.f l;
    @DexIgnore
    public Bundle m;
    @DexIgnore
    public ud0 n; // = null;
    @DexIgnore
    public ud0 o; // = null;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public /* final */ Lock q;
    @DexIgnore
    public int r; // = 0;

    @DexIgnore
    public ii0(Context context, eg0 eg0, Lock lock, Looper looper, yd0 yd0, Map<de0.c<?>, de0.f> map, Map<de0.c<?>, de0.f> map2, kj0 kj0, de0.a<? extends ln1, vm1> aVar, de0.f fVar, ArrayList<gi0> arrayList, ArrayList<gi0> arrayList2, Map<de0<?>, Boolean> map3, Map<de0<?>, Boolean> map4) {
        this.e = context;
        this.f = eg0;
        this.q = lock;
        this.g = looper;
        this.l = fVar;
        Context context2 = context;
        Lock lock2 = lock;
        Looper looper2 = looper;
        yd0 yd02 = yd0;
        ng0 ng0 = r3;
        ng0 ng02 = new ng0(context2, this.f, lock2, looper2, yd02, map2, (kj0) null, map4, (de0.a<? extends ln1, vm1>) null, arrayList2, new ki0(this, (ji0) null));
        this.h = ng0;
        this.i = new ng0(context2, this.f, lock2, looper2, yd02, map, kj0, map3, aVar, arrayList, new li0(this, (ji0) null));
        g4 g4Var = new g4();
        for (de0.c<?> put : map2.keySet()) {
            g4Var.put(put, this.h);
        }
        for (de0.c<?> put2 : map.keySet()) {
            g4Var.put(put2, this.i);
        }
        this.j = Collections.unmodifiableMap(g4Var);
    }

    @DexIgnore
    public static ii0 a(Context context, eg0 eg0, Lock lock, Looper looper, yd0 yd0, Map<de0.c<?>, de0.f> map, kj0 kj0, Map<de0<?>, Boolean> map2, de0.a<? extends ln1, vm1> aVar, ArrayList<gi0> arrayList) {
        Map<de0<?>, Boolean> map3 = map2;
        g4 g4Var = new g4();
        g4 g4Var2 = new g4();
        de0.f fVar = null;
        for (Map.Entry next : map.entrySet()) {
            de0.f fVar2 = (de0.f) next.getValue();
            if (fVar2.d()) {
                fVar = fVar2;
            }
            if (fVar2.l()) {
                g4Var.put((de0.c) next.getKey(), fVar2);
            } else {
                g4Var2.put((de0.c) next.getKey(), fVar2);
            }
        }
        bk0.b(!g4Var.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        g4 g4Var3 = new g4();
        g4 g4Var4 = new g4();
        for (de0 next2 : map2.keySet()) {
            de0.c<?> a = next2.a();
            if (g4Var.containsKey(a)) {
                g4Var3.put(next2, map3.get(next2));
            } else if (g4Var2.containsKey(a)) {
                g4Var4.put(next2, map3.get(next2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            gi0 gi0 = arrayList.get(i2);
            i2++;
            gi0 gi02 = gi0;
            if (g4Var3.containsKey(gi02.e)) {
                arrayList2.add(gi02);
            } else if (g4Var4.containsKey(gi02.e)) {
                arrayList3.add(gi02);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new ii0(context, eg0, lock, looper, yd0, g4Var, g4Var2, kj0, aVar, fVar, arrayList2, arrayList3, g4Var3, g4Var4);
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t) {
        if (!c((te0<? extends me0, ? extends de0.b>) t)) {
            return this.h.b(t);
        }
        if (!k()) {
            return this.i.b(t);
        }
        t.c(new Status(4, (String) null, h()));
        return t;
    }

    @DexIgnore
    public final boolean c() {
        this.q.lock();
        try {
            boolean z = true;
            if (!this.h.c() || (!this.i.c() && !k() && this.r != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void d() {
        this.h.d();
        this.i.d();
    }

    @DexIgnore
    public final void e() {
        this.q.lock();
        try {
            boolean g2 = g();
            this.i.a();
            this.o = new ud0(4);
            if (g2) {
                new ss0(this.g).post(new ji0(this));
            } else {
                j();
            }
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final ud0 f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean g() {
        this.q.lock();
        try {
            return this.r == 2;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final PendingIntent h() {
        if (this.l == null) {
            return null;
        }
        return PendingIntent.getActivity(this.e, System.identityHashCode(this.f), this.l.k(), 134217728);
    }

    @DexIgnore
    public final void i() {
        if (b(this.n)) {
            if (b(this.o) || k()) {
                int i2 = this.r;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.r = 0;
                        return;
                    }
                    this.f.a(this.m);
                }
                j();
                this.r = 0;
                return;
            }
            ud0 ud0 = this.o;
            if (ud0 == null) {
                return;
            }
            if (this.r == 1) {
                j();
                return;
            }
            a(ud0);
            this.h.a();
        } else if (this.n == null || !b(this.o)) {
            ud0 ud02 = this.n;
            if (ud02 != null) {
                ud0 ud03 = this.o;
                if (ud03 != null) {
                    if (this.i.q < this.h.q) {
                        ud02 = ud03;
                    }
                    a(ud02);
                }
            }
        } else {
            this.i.a();
            a(this.n);
        }
    }

    @DexIgnore
    public final void j() {
        for (cf0 onComplete : this.k) {
            onComplete.onComplete();
        }
        this.k.clear();
    }

    @DexIgnore
    public final boolean k() {
        ud0 ud0 = this.o;
        return ud0 != null && ud0.H() == 4;
    }

    @DexIgnore
    public final boolean c(te0<? extends me0, ? extends de0.b> te0) {
        de0.c<? extends de0.b> i2 = te0.i();
        bk0.a(this.j.containsKey(i2), (Object) "GoogleApiClient is not configured to use the API required for this call.");
        return this.j.get(i2).equals(this.i);
    }

    @DexIgnore
    public final void b() {
        this.r = 2;
        this.p = false;
        this.o = null;
        this.n = null;
        this.h.b();
        this.i.b();
    }

    @DexIgnore
    public static boolean b(ud0 ud0) {
        return ud0 != null && ud0.L();
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(T t) {
        if (!c((te0<? extends me0, ? extends de0.b>) t)) {
            return this.h.a(t);
        }
        if (!k()) {
            return this.i.a(t);
        }
        t.c(new Status(4, (String) null, h()));
        return t;
    }

    @DexIgnore
    public final void a() {
        this.o = null;
        this.n = null;
        this.r = 0;
        this.h.a();
        this.i.a();
        j();
    }

    @DexIgnore
    public final boolean a(cf0 cf0) {
        this.q.lock();
        try {
            if ((g() || c()) && !this.i.c()) {
                this.k.add(cf0);
                if (this.r == 0) {
                    this.r = 1;
                }
                this.o = null;
                this.i.b();
                return true;
            }
            this.q.unlock();
            return false;
        } finally {
            this.q.unlock();
        }
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        int i2 = this.r;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.r = 0;
            }
            this.f.a(ud0);
        }
        j();
        this.r = 0;
    }

    @DexIgnore
    public final void a(int i2, boolean z) {
        this.f.a(i2, z);
        this.o = null;
        this.n = null;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        Bundle bundle2 = this.m;
        if (bundle2 == null) {
            this.m = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    @DexIgnore
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append(str).append("authClient").println(":");
        this.i.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append(str).append("anonClient").println(":");
        this.h.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }
}
