package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.q0 */
public class C2691q0 extends com.fossil.blesdk.obfuscated.C2514o0 {

    @DexIgnore
    /* renamed from: q */
    public com.fossil.blesdk.obfuscated.C2691q0.C2692a f8493q;

    @DexIgnore
    /* renamed from: r */
    public boolean f8494r;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.q0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.q0$a */
    public static class C2692a extends com.fossil.blesdk.obfuscated.C2514o0.C2517c {

        @DexIgnore
        /* renamed from: J */
        public int[][] f8495J;

        @DexIgnore
        public C2692a(com.fossil.blesdk.obfuscated.C2691q0.C2692a aVar, com.fossil.blesdk.obfuscated.C2691q0 q0Var, android.content.res.Resources resources) {
            super(aVar, q0Var, resources);
            if (aVar != null) {
                this.f8495J = aVar.f8495J;
            } else {
                this.f8495J = new int[mo14219d()][];
            }
        }

        @DexIgnore
        /* renamed from: a */
        public int mo14931a(int[] iArr, android.graphics.drawable.Drawable drawable) {
            int a = mo14204a(drawable);
            this.f8495J[a] = iArr;
            return a;
        }

        @DexIgnore
        /* renamed from: n */
        public void mo13780n() {
            int[][] iArr = this.f8495J;
            int[][] iArr2 = new int[iArr.length][];
            for (int length = iArr.length - 1; length >= 0; length--) {
                int[][] iArr3 = this.f8495J;
                iArr2[length] = iArr3[length] != null ? (int[]) iArr3[length].clone() : null;
            }
            this.f8495J = iArr2;
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable() {
            return new com.fossil.blesdk.obfuscated.C2691q0(this, (android.content.res.Resources) null);
        }

        @DexIgnore
        public android.graphics.drawable.Drawable newDrawable(android.content.res.Resources resources) {
            return new com.fossil.blesdk.obfuscated.C2691q0(this, resources);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo14930a(int[] iArr) {
            int[][] iArr2 = this.f8495J;
            int e = mo14220e();
            for (int i = 0; i < e; i++) {
                if (android.util.StateSet.stateSetMatches(iArr2[i], iArr)) {
                    return i;
                }
            }
            return -1;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14206a(int i, int i2) {
            super.mo14206a(i, i2);
            int[][] iArr = new int[i2][];
            java.lang.System.arraycopy(this.f8495J, 0, iArr, 0, i);
            this.f8495J = iArr;
        }
    }

    @DexIgnore
    public C2691q0(com.fossil.blesdk.obfuscated.C2691q0.C2692a aVar, android.content.res.Resources resources) {
        mo13760a((com.fossil.blesdk.obfuscated.C2514o0.C2517c) new com.fossil.blesdk.obfuscated.C2691q0.C2692a(aVar, this, resources));
        onStateChange(getState());
    }

    @DexIgnore
    public void applyTheme(android.content.res.Resources.Theme theme) {
        super.applyTheme(theme);
        onStateChange(getState());
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    public android.graphics.drawable.Drawable mutate() {
        if (!this.f8494r) {
            super.mutate();
            if (this == this) {
                this.f8493q.mo13780n();
                this.f8494r = true;
            }
        }
        return this;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        int a = this.f8493q.mo14930a(iArr);
        if (a < 0) {
            a = this.f8493q.mo14930a(android.util.StateSet.WILD_CARD);
        }
        return mo14165a(a) || onStateChange;
    }

    @DexIgnore
    /* renamed from: a */
    public int[] mo14929a(android.util.AttributeSet attributeSet) {
        int attributeCount = attributeSet.getAttributeCount();
        int[] iArr = new int[attributeCount];
        int i = 0;
        for (int i2 = 0; i2 < attributeCount; i2++) {
            int attributeNameResource = attributeSet.getAttributeNameResource(i2);
            if (!(attributeNameResource == 0 || attributeNameResource == 16842960 || attributeNameResource == 16843161)) {
                int i3 = i + 1;
                if (!attributeSet.getAttributeBooleanValue(i2, false)) {
                    attributeNameResource = -attributeNameResource;
                }
                iArr[i] = attributeNameResource;
                i = i3;
            }
        }
        return android.util.StateSet.trimStateSet(iArr, i);
    }

    @DexIgnore
    public C2691q0(com.fossil.blesdk.obfuscated.C2691q0.C2692a aVar) {
        if (aVar != null) {
            mo13760a((com.fossil.blesdk.obfuscated.C2514o0.C2517c) aVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2691q0.C2692a m12513a() {
        return new com.fossil.blesdk.obfuscated.C2691q0.C2692a(this.f8493q, this, (android.content.res.Resources) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13760a(com.fossil.blesdk.obfuscated.C2514o0.C2517c cVar) {
        super.mo13760a(cVar);
        if (cVar instanceof com.fossil.blesdk.obfuscated.C2691q0.C2692a) {
            this.f8493q = (com.fossil.blesdk.obfuscated.C2691q0.C2692a) cVar;
        }
    }
}
