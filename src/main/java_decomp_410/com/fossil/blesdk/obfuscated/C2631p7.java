package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p7 */
public final class C2631p7 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2789r7 f8300a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.p7$a */
    public static class C2632a implements com.fossil.blesdk.obfuscated.C2789r7 {

        @DexIgnore
        /* renamed from: a */
        public android.os.LocaleList f8301a; // = new android.os.LocaleList(new java.util.Locale[0]);

        @DexIgnore
        /* renamed from: a */
        public void mo14641a(java.util.Locale... localeArr) {
            this.f8301a = new android.os.LocaleList(localeArr);
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            return this.f8301a.equals(((com.fossil.blesdk.obfuscated.C2631p7) obj).mo14633a());
        }

        @DexIgnore
        public java.util.Locale get(int i) {
            return this.f8301a.get(i);
        }

        @DexIgnore
        public int hashCode() {
            return this.f8301a.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return this.f8301a.toString();
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Object mo14640a() {
            return this.f8301a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p7$b")
    /* renamed from: com.fossil.blesdk.obfuscated.p7$b */
    public static class C2633b implements com.fossil.blesdk.obfuscated.C2789r7 {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2707q7 f8302a; // = new com.fossil.blesdk.obfuscated.C2707q7(new java.util.Locale[0]);

        @DexIgnore
        /* renamed from: a */
        public void mo14641a(java.util.Locale... localeArr) {
            this.f8302a = new com.fossil.blesdk.obfuscated.C2707q7(localeArr);
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            return this.f8302a.equals(((com.fossil.blesdk.obfuscated.C2631p7) obj).mo14633a());
        }

        @DexIgnore
        public java.util.Locale get(int i) {
            return this.f8302a.mo15039a(i);
        }

        @DexIgnore
        public int hashCode() {
            return this.f8302a.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return this.f8302a.toString();
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.Object mo14640a() {
            return this.f8302a;
        }
    }

    /*
    static {
        new com.fossil.blesdk.obfuscated.C2631p7();
        if (android.os.Build.VERSION.SDK_INT >= 24) {
            f8300a = new com.fossil.blesdk.obfuscated.C2631p7.C2632a();
        } else {
            f8300a = new com.fossil.blesdk.obfuscated.C2631p7.C2633b();
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2631p7 m12117a(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.C2631p7 p7Var = new com.fossil.blesdk.obfuscated.C2631p7();
        if (obj instanceof android.os.LocaleList) {
            p7Var.mo14635a((android.os.LocaleList) obj);
        }
        return p7Var;
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2631p7 m12118b(java.util.Locale... localeArr) {
        com.fossil.blesdk.obfuscated.C2631p7 p7Var = new com.fossil.blesdk.obfuscated.C2631p7();
        p7Var.mo14636a(localeArr);
        return p7Var;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        return f8300a.equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return f8300a.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        return f8300a.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo14633a() {
        return f8300a.mo14640a();
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Locale mo14634a(int i) {
        return f8300a.get(i);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14635a(android.os.LocaleList localeList) {
        int size = localeList.size();
        if (size > 0) {
            java.util.Locale[] localeArr = new java.util.Locale[size];
            for (int i = 0; i < size; i++) {
                localeArr[i] = localeList.get(i);
            }
            f8300a.mo14641a(localeArr);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14636a(java.util.Locale... localeArr) {
        f8300a.mo14641a(localeArr);
    }
}
