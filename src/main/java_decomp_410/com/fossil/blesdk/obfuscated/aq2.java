package com.fossil.blesdk.obfuscated;

import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class aq2 {
    @DexIgnore
    public static /* final */ aq2 a; // = new aq2();

    @DexIgnore
    public final Transition a() {
        Fade fade = new Fade();
        fade.excludeTarget(16908336, true);
        fade.excludeTarget(16908335, true);
        return fade;
    }

    @DexIgnore
    public final void a(View view) {
        kd4.b(view, "view");
        view.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) view.getHeight(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        view.startAnimation(translateAnimation);
    }

    @DexIgnore
    public final TransitionSet a(long j) {
        TransitionSet transitionSet = new TransitionSet();
        Slide slide = new Slide(80);
        slide.setInterpolator(AnimationUtils.loadInterpolator(PortfolioApp.W.c(), 17563662));
        slide.setDuration(j);
        slide.excludeTarget(16908336, true);
        slide.excludeTarget(16908335, true);
        transitionSet.addTransition(slide);
        return transitionSet;
    }

    @DexIgnore
    public final Transition a(PortfolioApp portfolioApp) {
        PortfolioApp portfolioApp2 = portfolioApp;
        kd4.b(portfolioApp2, "mApp");
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.setOrdering(0);
        transitionSet.setDuration(500);
        ChangeImageTransform changeImageTransform = new ChangeImageTransform();
        changeImageTransform.addTarget(portfolioApp2.getString(R.string.transition_customize_hands_background));
        changeImageTransform.addTarget(portfolioApp2.getString(R.string.transition_customize_theme_background));
        transitionSet.addTransition(changeImageTransform);
        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_customize_hands_background));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_customize_theme_background));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_preset_name));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_complication_top));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_complication_bottom));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_complication_start));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_complication_end));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_watch_app_top));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_watch_app_middle));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_watch_app_bottom));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_line_center));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_line_bottom));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_line_top));
        changeBounds.addTarget(portfolioApp2.getString(R.string.transition_underline_preset_name));
        transitionSet.addTransition(changeBounds);
        ChangeTransform changeTransform = new ChangeTransform();
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_customize_hands_background));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_customize_theme_background));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_preset_name));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_complication_top));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_complication_bottom));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_complication_start));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_complication_end));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_watch_app_top));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_watch_app_middle));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_watch_app_bottom));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_line_center));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_line_top));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_line_bottom));
        changeTransform.addTarget(portfolioApp2.getString(R.string.transition_underline_preset_name));
        transitionSet.addTransition(changeTransform);
        Fade fade = new Fade();
        fade.addTarget(portfolioApp2.getString(R.string.transition_underline_preset_name));
        fade.addTarget(portfolioApp2.getString(R.string.transition_customize_theme_edit));
        transitionSet.addTransition(fade);
        cq2 cq2 = new cq2();
        cq2.addTarget(portfolioApp2.getString(R.string.transition_complication_top));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_complication_top));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_complication_bottom));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_complication_start));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_complication_end));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_watch_app_top));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_watch_app_middle));
        cq2.addTarget(portfolioApp2.getString(R.string.transition_watch_app_bottom));
        transitionSet.addTransition(cq2);
        bq2 bq2 = new bq2();
        bq2.addTarget(portfolioApp2.getString(R.string.transition_preset_name));
        transitionSet.addTransition(bq2);
        return transitionSet;
    }
}
