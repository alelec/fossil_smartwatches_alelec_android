package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class mj0 implements DialogInterface.OnClickListener {
    @DexIgnore
    public static mj0 a(Activity activity, Intent intent, int i) {
        return new al0(intent, activity, i);
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            a();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }

    @DexIgnore
    public static mj0 a(Fragment fragment, Intent intent, int i) {
        return new bl0(intent, fragment, i);
    }

    @DexIgnore
    public static mj0 a(ye0 ye0, Intent intent, int i) {
        return new cl0(intent, ye0, i);
    }
}
