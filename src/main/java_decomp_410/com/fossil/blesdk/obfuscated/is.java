package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.os.Build;
import android.util.Log;
import android.util.Size;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.PreferredColorSpace;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class is<T> implements mo<ImageDecoder.Source, T> {
    @DexIgnore
    public /* final */ bt a; // = bt.b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ImageDecoder.OnHeaderDecodedListener {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ DecodeFormat d;
        @DexIgnore
        public /* final */ /* synthetic */ DownsampleStrategy e;
        @DexIgnore
        public /* final */ /* synthetic */ PreferredColorSpace f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.is$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.is$a$a  reason: collision with other inner class name */
        public class C0019a implements ImageDecoder.OnPartialImageListener {
            @DexIgnore
            public C0019a(a aVar) {
            }

            @DexIgnore
            public boolean onPartialImage(ImageDecoder.DecodeException decodeException) {
                return false;
            }
        }

        @DexIgnore
        public a(int i, int i2, boolean z, DecodeFormat decodeFormat, DownsampleStrategy downsampleStrategy, PreferredColorSpace preferredColorSpace) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.d = decodeFormat;
            this.e = downsampleStrategy;
            this.f = preferredColorSpace;
        }

        @DexIgnore
        @SuppressLint({"Override"})
        public void onHeaderDecoded(ImageDecoder imageDecoder, ImageDecoder.ImageInfo imageInfo, ImageDecoder.Source source) {
            boolean z = true;
            if (is.this.a.a(this.a, this.b, this.c, false)) {
                imageDecoder.setAllocator(3);
            } else {
                imageDecoder.setAllocator(1);
            }
            if (this.d == DecodeFormat.PREFER_RGB_565) {
                imageDecoder.setMemorySizePolicy(0);
            }
            imageDecoder.setOnPartialImageListener(new C0019a(this));
            Size size = imageInfo.getSize();
            int i = this.a;
            if (i == Integer.MIN_VALUE) {
                i = size.getWidth();
            }
            int i2 = this.b;
            if (i2 == Integer.MIN_VALUE) {
                i2 = size.getHeight();
            }
            float b2 = this.e.b(size.getWidth(), size.getHeight(), i, i2);
            int round = Math.round(((float) size.getWidth()) * b2);
            int round2 = Math.round(((float) size.getHeight()) * b2);
            if (Log.isLoggable("ImageDecoder", 2)) {
                Log.v("ImageDecoder", "Resizing from [" + size.getWidth() + "x" + size.getHeight() + "] to [" + round + "x" + round2 + "] scaleFactor: " + b2);
            }
            imageDecoder.setTargetSize(round, round2);
            int i3 = Build.VERSION.SDK_INT;
            if (i3 >= 28) {
                if (this.f != PreferredColorSpace.DISPLAY_P3 || imageInfo.getColorSpace() == null || !imageInfo.getColorSpace().isWideGamut()) {
                    z = false;
                }
                imageDecoder.setTargetColorSpace(ColorSpace.get(z ? ColorSpace.Named.DISPLAY_P3 : ColorSpace.Named.SRGB));
            } else if (i3 >= 26) {
                imageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
            }
        }
    }

    @DexIgnore
    public abstract aq<T> a(ImageDecoder.Source source, int i, int i2, ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws IOException;

    @DexIgnore
    public final boolean a(ImageDecoder.Source source, lo loVar) {
        return true;
    }

    @DexIgnore
    public final aq<T> a(ImageDecoder.Source source, int i, int i2, lo loVar) throws IOException {
        return a(source, i, i2, (ImageDecoder.OnHeaderDecodedListener) new a(i, i2, loVar.a(ws.i) != null && ((Boolean) loVar.a(ws.i)).booleanValue(), (DecodeFormat) loVar.a(ws.f), (DownsampleStrategy) loVar.a(DownsampleStrategy.f), (PreferredColorSpace) loVar.a(ws.g)));
    }
}
