package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface na1<T> {
    @DexIgnore
    int a(T t);

    @DexIgnore
    T a();

    @DexIgnore
    void a(T t, ma1 ma1, i81 i81) throws IOException;

    @DexIgnore
    void a(T t, sb1 sb1) throws IOException;

    @DexIgnore
    boolean a(T t, T t2);

    @DexIgnore
    int b(T t);

    @DexIgnore
    void b(T t, T t2);

    @DexIgnore
    boolean c(T t);

    @DexIgnore
    void d(T t);
}
