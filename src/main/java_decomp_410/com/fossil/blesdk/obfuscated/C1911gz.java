package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gz */
public class C1911gz {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f5595a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.x74 f5596b;

    @DexIgnore
    public C1911gz(android.content.Context context, com.fossil.blesdk.obfuscated.x74 x74) {
        this.f5595a = context;
        this.f5596b = x74;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo11437a() {
        return mo11438a("com.crashlytics.CrashSubmissionAlwaysSendTitle", this.f5596b.f20203g);
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.String mo11440b() {
        return mo11438a("com.crashlytics.CrashSubmissionCancelTitle", this.f5596b.f20201e);
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.String mo11442c() {
        return mo11438a("com.crashlytics.CrashSubmissionPromptMessage", this.f5596b.f20198b);
    }

    @DexIgnore
    /* renamed from: d */
    public java.lang.String mo11443d() {
        return mo11438a("com.crashlytics.CrashSubmissionSendTitle", this.f5596b.f20199c);
    }

    @DexIgnore
    /* renamed from: e */
    public java.lang.String mo11444e() {
        return mo11438a("com.crashlytics.CrashSubmissionPromptTitle", this.f5596b.f20197a);
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo11438a(java.lang.String str, java.lang.String str2) {
        return mo11441b(p011io.fabric.sdk.android.services.common.CommonUtils.m36879b(this.f5595a, str), str2);
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.String mo11441b(java.lang.String str, java.lang.String str2) {
        return mo11439a(str) ? str2 : str;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo11439a(java.lang.String str) {
        return str == null || str.length() == 0;
    }
}
