package com.fossil.blesdk.obfuscated;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dp {
    @DexIgnore
    public boolean a(File file) {
        return file.exists();
    }

    @DexIgnore
    public long b(File file) {
        return file.length();
    }

    @DexIgnore
    public File a(String str) {
        return new File(str);
    }
}
