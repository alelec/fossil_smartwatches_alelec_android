package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class se extends RecyclerView.v {
    @DexIgnore
    public /* final */ LinearInterpolator i; // = new LinearInterpolator();
    @DexIgnore
    public /* final */ DecelerateInterpolator j; // = new DecelerateInterpolator();
    @DexIgnore
    public PointF k;
    @DexIgnore
    public /* final */ float l;
    @DexIgnore
    public int m; // = 0;
    @DexIgnore
    public int n; // = 0;

    @DexIgnore
    public se(Context context) {
        this.l = a(context.getResources().getDisplayMetrics());
    }

    @DexIgnore
    public void a(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
        int a = a(view, i());
        int b = b(view, j());
        int d = d((int) Math.sqrt((double) ((a * a) + (b * b))));
        if (d > 0) {
            aVar.a(-a, -b, d, this.j);
        }
    }

    @DexIgnore
    public final int b(int i2, int i3) {
        int i4 = i2 - i3;
        if (i2 * i4 <= 0) {
            return 0;
        }
        return i4;
    }

    @DexIgnore
    public int b(View view, int i2) {
        RecyclerView.m b = b();
        if (b == null || !b.b()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return a(b.j(view) - layoutParams.topMargin, b.e(view) + layoutParams.bottomMargin, b.q(), b.h() - b.n(), i2);
    }

    @DexIgnore
    public int d(int i2) {
        return (int) Math.ceil(((double) e(i2)) / 0.3356d);
    }

    @DexIgnore
    public int e(int i2) {
        return (int) Math.ceil((double) (((float) Math.abs(i2)) * this.l));
    }

    @DexIgnore
    public void f() {
    }

    @DexIgnore
    public void g() {
        this.n = 0;
        this.m = 0;
        this.k = null;
    }

    @DexIgnore
    public int i() {
        PointF pointF = this.k;
        if (pointF != null) {
            float f = pointF.x;
            if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : -1;
            }
        }
        return 0;
    }

    @DexIgnore
    public int j() {
        PointF pointF = this.k;
        if (pointF != null) {
            float f = pointF.y;
            if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : -1;
            }
        }
        return 0;
    }

    @DexIgnore
    public void a(int i2, int i3, RecyclerView.State state, RecyclerView.v.a aVar) {
        if (a() == 0) {
            h();
            return;
        }
        this.m = b(this.m, i2);
        this.n = b(this.n, i3);
        if (this.m == 0 && this.n == 0) {
            a(aVar);
        }
    }

    @DexIgnore
    public float a(DisplayMetrics displayMetrics) {
        return 25.0f / ((float) displayMetrics.densityDpi);
    }

    @DexIgnore
    public void a(RecyclerView.v.a aVar) {
        PointF a = a(c());
        if (a == null || (a.x == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && a.y == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            aVar.a(c());
            h();
            return;
        }
        a(a);
        this.k = a;
        this.m = (int) (a.x * 10000.0f);
        this.n = (int) (a.y * 10000.0f);
        aVar.a((int) (((float) this.m) * 1.2f), (int) (((float) this.n) * 1.2f), (int) (((float) e(10000)) * 1.2f), this.i);
    }

    @DexIgnore
    public int a(int i2, int i3, int i4, int i5, int i6) {
        if (i6 == -1) {
            return i4 - i2;
        }
        if (i6 == 0) {
            int i7 = i4 - i2;
            if (i7 > 0) {
                return i7;
            }
            int i8 = i5 - i3;
            if (i8 < 0) {
                return i8;
            }
            return 0;
        } else if (i6 == 1) {
            return i5 - i3;
        } else {
            throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
    }

    @DexIgnore
    public int a(View view, int i2) {
        RecyclerView.m b = b();
        if (b == null || !b.a()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return a(b.f(view) - layoutParams.leftMargin, b.i(view) + layoutParams.rightMargin, b.o(), b.r() - b.p(), i2);
    }
}
