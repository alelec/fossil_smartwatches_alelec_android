package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.dx */
public abstract class C1665dx<T extends com.fossil.blesdk.obfuscated.C1665dx> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1752ex f4539a; // = new com.fossil.blesdk.obfuscated.C1752ex(20, 100, com.fossil.blesdk.obfuscated.q44.m26806h());

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1586cx f4540b; // = new com.fossil.blesdk.obfuscated.C1586cx(this.f4539a);

    @DexIgnore
    /* renamed from: a */
    public java.util.Map<java.lang.String, java.lang.Object> mo10146a() {
        return this.f4540b.f4213b;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo10145a(java.lang.String str, java.lang.String str2) {
        this.f4540b.mo9706a(str, str2);
        return this;
    }

    @DexIgnore
    /* renamed from: a */
    public T mo10144a(java.lang.String str, java.lang.Number number) {
        this.f4540b.mo9704a(str, number);
        return this;
    }
}
