package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.notification.AppNotification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h22 implements m22 {
    @DexIgnore
    public int a() {
        return 5;
    }

    @DexIgnore
    public void a(n22 n22) {
        StringBuilder sb = new StringBuilder();
        sb.append(0);
        while (true) {
            if (!n22.i()) {
                break;
            }
            sb.append(n22.c());
            n22.f++;
            int a = p22.a(n22.d(), n22.f, a());
            if (a != a()) {
                n22.b(a);
                break;
            }
        }
        int length = sb.length() - 1;
        int a2 = n22.a() + length + 1;
        n22.c(a2);
        boolean z = n22.g().a() - a2 > 0;
        if (n22.i() || z) {
            if (length <= 249) {
                sb.setCharAt(0, (char) length);
            } else if (length <= 1555) {
                sb.setCharAt(0, (char) ((length / 250) + AppNotification.MAX_MESSAGE_LENGTH_IN_BYTE));
                sb.insert(1, (char) (length % 250));
            } else {
                throw new IllegalStateException("Message length not in valid ranges: " + length);
            }
        }
        int length2 = sb.length();
        for (int i = 0; i < length2; i++) {
            n22.a(a(sb.charAt(i), n22.a() + 1));
        }
    }

    @DexIgnore
    public static char a(char c, int i) {
        int i2 = c + ((i * 149) % 255) + 1;
        return i2 <= 255 ? (char) i2 : (char) (i2 - 256);
    }
}
