package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.fossil.wearables.fossil.R;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.LocationBias;
import com.google.android.libraries.places.api.model.LocationRestriction;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q62 extends ArrayAdapter<AutocompletePrediction> implements Filterable {
    @DexIgnore
    public static /* final */ String j; // = j;
    @DexIgnore
    public static /* final */ StyleSpan k; // = new StyleSpan(1);
    @DexIgnore
    public List<? extends AutocompletePrediction> e; // = new ArrayList();
    @DexIgnore
    public b f;
    @DexIgnore
    public AutocompleteSessionToken g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public /* final */ PlacesClient i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void M(boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ q62 a;

        @DexIgnore
        public c(q62 q62) {
            this.a = q62;
        }

        @DexIgnore
        public CharSequence convertResultToString(Object obj) {
            kd4.b(obj, "resultValue");
            AutocompletePrediction autocompletePrediction = (AutocompletePrediction) (!(obj instanceof AutocompletePrediction) ? null : obj);
            if (autocompletePrediction != null) {
                SpannableString fullText = autocompletePrediction.getFullText((CharacterStyle) null);
                if (fullText != null) {
                    return fullText;
                }
            }
            CharSequence convertResultToString = super.convertResultToString(obj);
            kd4.a((Object) convertResultToString, "super.convertResultToString(resultValue)");
            return convertResultToString;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            List arrayList = new ArrayList();
            if (charSequence != null) {
                arrayList = this.a.a(charSequence);
            }
            filterResults.values = arrayList;
            if (arrayList != null) {
                filterResults.count = arrayList.size();
            } else {
                filterResults.count = 0;
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.count <= 0) {
                this.a.notifyDataSetInvalidated();
                if (this.a.f != null) {
                    b a2 = this.a.f;
                    if (a2 != null) {
                        a2.M(true);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                q62 q62 = this.a;
                Object obj = filterResults.values;
                if (obj != null) {
                    q62.e = (List) obj;
                    this.a.notifyDataSetChanged();
                    if (this.a.f != null) {
                        b a3 = this.a.f;
                        if (a3 != null) {
                            a3.M(false);
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.List<com.google.android.libraries.places.api.model.AutocompletePrediction>");
                }
            }
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q62(Context context, PlacesClient placesClient) {
        super(context, R.layout.expandable_list_item, R.id.primary_tv);
        kd4.b(context, "context");
        this.i = placesClient;
    }

    @DexIgnore
    public final AutocompleteSessionToken b() {
        return this.g;
    }

    @DexIgnore
    public int getCount() {
        return this.e.size();
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    public View getView(int i2, View view, ViewGroup viewGroup) {
        kd4.b(viewGroup, "parent");
        View view2 = super.getView(i2, view, viewGroup);
        kd4.a((Object) view2, "super.getView(position, convertView, parent)");
        AutocompletePrediction item = getItem(i2);
        if (item != null) {
            FlexibleTextView flexibleTextView = (FlexibleTextView) view2.findViewById(R.id.full_tv);
            FlexibleTextView flexibleTextView2 = (FlexibleTextView) view2.findViewById(R.id.primary_tv);
            FlexibleTextView flexibleTextView3 = (FlexibleTextView) view2.findViewById(R.id.secondary_tv);
            kd4.a((Object) flexibleTextView, "fullTv");
            flexibleTextView.setText(item.getFullText(k));
            kd4.a((Object) flexibleTextView2, "textView1");
            flexibleTextView2.setText(item.getPrimaryText(k));
            kd4.a((Object) flexibleTextView3, "textView2");
            flexibleTextView3.setText(item.getSecondaryText(k));
        }
        return view2;
    }

    @DexIgnore
    public AutocompletePrediction getItem(int i2) {
        return (AutocompletePrediction) this.e.get(i2);
    }

    @DexIgnore
    public final void a(b bVar) {
        this.f = bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        if ((r2 - r0.getTime()) >= ((long) 180000)) goto L_0x002c;
     */
    @DexIgnore
    public final List<AutocompletePrediction> a(CharSequence charSequence) {
        if (this.i != null) {
            if (this.g != null) {
                if (this.h != null) {
                    long time = new Date().getTime();
                    Date date = this.h;
                    if (date == null) {
                        kd4.a();
                        throw null;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = j;
                local.d(str, "Starting autocomplete query for: " + charSequence);
                FindAutocompletePredictionsRequest.Builder typeFilter = FindAutocompletePredictionsRequest.builder().setQuery(charSequence.toString()).setCountry((String) null).setLocationBias((LocationBias) null).setLocationRestriction((LocationRestriction) null).setTypeFilter((TypeFilter) null);
                kd4.a((Object) typeFilter, "FindAutocompletePredicti\u2026     .setTypeFilter(null)");
                typeFilter.setSessionToken(this.g);
                wn1 findAutocompletePredictions = this.i.findAutocompletePredictions(typeFilter.build());
                kd4.a((Object) findAutocompletePredictions, "mPlacesClient.findAutoco\u2026s(requestBuilder.build())");
                FindAutocompletePredictionsResponse findAutocompletePredictionsResponse = (FindAutocompletePredictionsResponse) zn1.a(findAutocompletePredictions);
                kd4.a((Object) findAutocompletePredictionsResponse, "response");
                return findAutocompletePredictionsResponse.getAutocompletePredictions();
            }
            this.g = AutocompleteSessionToken.newInstance();
            this.h = new Date();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = j;
            local2.d(str2, "Starting autocomplete query for: " + charSequence);
            FindAutocompletePredictionsRequest.Builder typeFilter2 = FindAutocompletePredictionsRequest.builder().setQuery(charSequence.toString()).setCountry((String) null).setLocationBias((LocationBias) null).setLocationRestriction((LocationRestriction) null).setTypeFilter((TypeFilter) null);
            kd4.a((Object) typeFilter2, "FindAutocompletePredicti\u2026     .setTypeFilter(null)");
            typeFilter2.setSessionToken(this.g);
            wn1 findAutocompletePredictions2 = this.i.findAutocompletePredictions(typeFilter2.build());
            kd4.a((Object) findAutocompletePredictions2, "mPlacesClient.findAutoco\u2026s(requestBuilder.build())");
            try {
                FindAutocompletePredictionsResponse findAutocompletePredictionsResponse2 = (FindAutocompletePredictionsResponse) zn1.a(findAutocompletePredictions2);
                kd4.a((Object) findAutocompletePredictionsResponse2, "response");
                return findAutocompletePredictionsResponse2.getAutocompletePredictions();
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = j;
                local3.d(str3, "Query failed. Received Exception=" + e2);
                e2.printStackTrace();
                return null;
            }
        } else {
            FLogger.INSTANCE.getLocal().e(j, "client is not connected for autocomplete query.");
            return null;
        }
    }

    @DexIgnore
    public final void a() {
        this.g = null;
    }
}
