package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i4 */
public class C1997i4 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ int[] f5933a; // = new int[0];

    @DexIgnore
    /* renamed from: b */
    public static /* final */ long[] f5934b; // = new long[0];

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.lang.Object[] f5935c; // = new java.lang.Object[0];

    @DexIgnore
    /* renamed from: a */
    public static int m8222a(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            int i3 = (1 << i2) - 12;
            if (i <= i3) {
                return i3;
            }
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m8225a(java.lang.Object obj, java.lang.Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    /* renamed from: b */
    public static int m8226b(int i) {
        return m8222a(i * 4) / 4;
    }

    @DexIgnore
    /* renamed from: c */
    public static int m8227c(int i) {
        return m8222a(i * 8) / 8;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m8223a(int[] iArr, int i, int i2) {
        int i3 = i - 1;
        int i4 = 0;
        while (i4 <= i3) {
            int i5 = (i4 + i3) >>> 1;
            int i6 = iArr[i5];
            if (i6 < i2) {
                i4 = i5 + 1;
            } else if (i6 <= i2) {
                return i5;
            } else {
                i3 = i5 - 1;
            }
        }
        return ~i4;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m8224a(long[] jArr, int i, long j) {
        int i2 = i - 1;
        int i3 = 0;
        while (i3 <= i2) {
            int i4 = (i3 + i2) >>> 1;
            int i5 = (jArr[i4] > j ? 1 : (jArr[i4] == j ? 0 : -1));
            if (i5 < 0) {
                i3 = i4 + 1;
            } else if (i5 <= 0) {
                return i4;
            } else {
                i2 = i4 - 1;
            }
        }
        return ~i3;
    }
}
