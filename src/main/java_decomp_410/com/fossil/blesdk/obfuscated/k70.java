package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.AuthenticationOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k70 extends j70 {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] L;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k70(Peripheral peripheral, byte[] bArr) {
        super(peripheral, AuthenticationOperationCode.EXCHANGE_PUBLIC_KEYS, RequestId.EXCHANGE_PUBLIC_KEYS, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(bArr, "phonePublicKey");
        this.L = bArr;
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(this.L.length).order(ByteOrder.LITTLE_ENDIAN).put(this.L).array();
        kd4.a((Object) array, "ByteBuffer.allocate(phon\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] I() {
        return this.K;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a2 = super.a(bArr);
        if (bArr.length >= 32) {
            this.K = ya4.a(bArr, 0, 32);
            wa0.a(a2, JSONKey.DEVICE_PUBLIC_KEY, k90.a(this.K, (String) null, 1, (Object) null));
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.SUCCESS, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        } else {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_LENGTH, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
        c(true);
        return a2;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.PHONE_PUBLIC_KEY, k90.a(this.L, (String) null, 1, (Object) null));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.DEVICE_PUBLIC_KEY, k90.a(this.K, (String) null, 1, (Object) null));
    }
}
