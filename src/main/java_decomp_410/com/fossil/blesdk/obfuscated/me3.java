package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activity.WorkoutPagedAdapter;
import com.portfolio.platform.view.FlexibleTextView;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class me3 extends zr2 implements le3, View.OnClickListener {
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public WorkoutPagedAdapter j;
    @DexIgnore
    public Date k; // = new Date();
    @DexIgnore
    public tr3<a92> l;
    @DexIgnore
    public ke3 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final me3 a(Date date) {
            kd4.b(date, "date");
            me3 me3 = new me3();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            me3.setArguments(bundle);
            return me3;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ qd b;

        @DexIgnore
        public b(me3 me3, boolean z, qd qdVar, Unit unit) {
            this.a = z;
            this.b = qdVar;
        }

        @DexIgnore
        public boolean a(AppBarLayout appBarLayout) {
            kd4.b(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ActivityDetailFragment";
    }

    @DexIgnore
    public void onClick(View view) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        local.d("ActivityDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case R.id.iv_back /*2131362398*/:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case R.id.iv_back_date /*2131362399*/:
                    ke3 ke3 = this.m;
                    if (ke3 != null) {
                        ke3.j();
                        return;
                    }
                    return;
                case R.id.iv_next_date /*2131362447*/:
                    ke3 ke32 = this.m;
                    if (ke32 != null) {
                        ke32.i();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long j2;
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        a92 a92 = (a92) qa.a(layoutInflater, R.layout.fragment_activity_detail, viewGroup, false, O0());
        Bundle arguments = getArguments();
        if (arguments != null) {
            j2 = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            kd4.a((Object) instance, "Calendar.getInstance()");
            j2 = instance.getTimeInMillis();
        }
        this.k = new Date(j2);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.k = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        kd4.a((Object) a92, "binding");
        a(a92);
        ke3 ke3 = this.m;
        if (ke3 != null) {
            ke3.a(this.k);
        }
        this.l = new tr3<>(this, a92);
        tr3<a92> tr3 = this.l;
        if (tr3 != null) {
            a92 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public void onDestroyView() {
        ke3 ke3 = this.m;
        if (ke3 != null) {
            ke3.h();
        }
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        ke3 ke3 = this.m;
        if (ke3 != null) {
            ke3.g();
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ke3 ke3 = this.m;
        if (ke3 != null) {
            ke3.b(this.k);
        }
        ke3 ke32 = this.m;
        if (ke32 != null) {
            ke32.f();
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        ke3 ke3 = this.m;
        if (ke3 != null) {
            ke3.a(bundle);
        }
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    public final void a(a92 a92) {
        a92.B.setOnClickListener(this);
        a92.C.setOnClickListener(this);
        a92.D.setOnClickListener(this);
        this.j = new WorkoutPagedAdapter(WorkoutPagedAdapter.WorkoutItem.STEPS, Unit.IMPERIAL, new WorkoutSessionDifference());
        RecyclerView recyclerView = a92.H;
        kd4.a((Object) recyclerView, "it");
        recyclerView.setAdapter(this.j);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable c = k6.c(recyclerView.getContext(), R.drawable.bg_item_decoration_eggshell_80a_line_1w);
        if (c != null) {
            yd3 yd3 = new yd3(linearLayoutManager.M(), false, false, 6, (fd4) null);
            kd4.a((Object) c, ResourceManager.DRAWABLE);
            yd3.a(c);
            recyclerView.a((RecyclerView.l) yd3);
        }
    }

    @DexIgnore
    public void a(ke3 ke3) {
        kd4.b(ke3, "presenter");
        this.m = ke3;
    }

    @DexIgnore
    public void a(Date date, boolean z, boolean z2, boolean z3) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3);
        this.k = date;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "calendar");
        instance.setTime(date);
        int i = instance.get(7);
        tr3<a92> tr3 = this.l;
        if (tr3 != null) {
            a92 a2 = tr3.a();
            if (a2 != null) {
                a2.q.a(true, true);
                FlexibleTextView flexibleTextView = a2.v;
                kd4.a((Object) flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(instance.get(5)));
                if (z) {
                    ImageView imageView = a2.C;
                    kd4.a((Object) imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.C;
                    kd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.D;
                    kd4.a((Object) imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.w;
                        kd4.a((Object) flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(sm2.a(getContext(), (int) R.string.DashboardDiana_Main_Steps7days_CTA__Today));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.w;
                    kd4.a((Object) flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(ll2.b.b(i));
                    return;
                }
                ImageView imageView4 = a2.D;
                kd4.a((Object) imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.w;
                kd4.a((Object) flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(ll2.b.b(i));
            }
        }
    }

    @DexIgnore
    public void a(Unit unit, ActivitySummary activitySummary) {
        double d;
        int i;
        int i2;
        int i3;
        int i4;
        String str;
        Unit unit2 = unit;
        ActivitySummary activitySummary2 = activitySummary;
        kd4.b(unit2, MFUser.DISTANCE_UNIT);
        FLogger.INSTANCE.getLocal().d("ActivityDetailFragment", "showDayDetail - distanceUnit=" + unit2 + ", activitySummary=" + activitySummary2);
        tr3<a92> tr3 = this.l;
        if (tr3 != null) {
            a92 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "binding");
                View d2 = a2.d();
                kd4.a((Object) d2, "binding.root");
                Context context = d2.getContext();
                if (activitySummary2 != null) {
                    i2 = (int) activitySummary.getSteps();
                    i = activitySummary.getStepGoal();
                    d = activitySummary.getDistance();
                } else {
                    d = 0.0d;
                    i2 = 0;
                    i = 0;
                }
                if (i2 > 0) {
                    FlexibleTextView flexibleTextView = a2.u;
                    kd4.a((Object) flexibleTextView, "binding.ftvDailyValue");
                    flexibleTextView.setText(ol2.a.b(Integer.valueOf(i2)));
                    FlexibleTextView flexibleTextView2 = a2.t;
                    kd4.a((Object) flexibleTextView2, "binding.ftvDailyUnit");
                    String a3 = sm2.a(context, (int) R.string.DashboardDiana_Main_StepsToday_Label__Steps);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    if (a3 != null) {
                        String lowerCase = a3.toLowerCase();
                        kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                        flexibleTextView2.setText(lowerCase);
                        FlexibleTextView flexibleTextView3 = a2.x;
                        kd4.a((Object) flexibleTextView3, "binding.ftvEst");
                        if (unit2 == Unit.IMPERIAL) {
                            StringBuilder sb = new StringBuilder();
                            pd4 pd4 = pd4.a;
                            String a4 = sm2.a(context, (int) R.string.DashboardDiana_Main_StepsToday_Text__EstNumberUnit);
                            kd4.a((Object) a4, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                            i3 = i2;
                            Object[] objArr = {ol2.a.a(Float.valueOf((float) d), unit2)};
                            String format = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                            kd4.a((Object) format, "java.lang.String.format(format, *args)");
                            sb.append(format);
                            sb.append(" ");
                            sb.append(PortfolioApp.W.c().getString(R.string.General_Measurement_Abbreviations_Miles__Mi));
                            str = sb.toString();
                        } else {
                            i3 = i2;
                            StringBuilder sb2 = new StringBuilder();
                            pd4 pd42 = pd4.a;
                            String a5 = sm2.a(context, (int) R.string.DashboardDiana_Main_StepsToday_Text__EstNumberUnit);
                            kd4.a((Object) a5, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                            Object[] objArr2 = {ol2.a.a(Float.valueOf((float) d), unit2)};
                            String format2 = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                            kd4.a((Object) format2, "java.lang.String.format(format, *args)");
                            sb2.append(format2);
                            sb2.append(" ");
                            sb2.append(PortfolioApp.W.c().getString(R.string.General_Measurement_Abbreviations_Kilometers__Km));
                            str = sb2.toString();
                        }
                        flexibleTextView3.setText(str);
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    i3 = i2;
                    FlexibleTextView flexibleTextView4 = a2.u;
                    kd4.a((Object) flexibleTextView4, "binding.ftvDailyValue");
                    flexibleTextView4.setText("");
                    FlexibleTextView flexibleTextView5 = a2.t;
                    kd4.a((Object) flexibleTextView5, "binding.ftvDailyUnit");
                    String a6 = sm2.a(context, (int) R.string.DashboardDiana_Steps_DetailPageNoRecord_Text__NoRecord);
                    kd4.a((Object) a6, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                    if (a6 != null) {
                        String upperCase = a6.toUpperCase();
                        kd4.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                        flexibleTextView5.setText(upperCase);
                        FlexibleTextView flexibleTextView6 = a2.x;
                        kd4.a((Object) flexibleTextView6, "binding.ftvEst");
                        flexibleTextView6.setText("");
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                }
                int i5 = i > 0 ? (i3 * 100) / i : -1;
                if (i5 >= 100) {
                    ImageView imageView = a2.D;
                    kd4.a((Object) imageView, "binding.ivNextDate");
                    imageView.setSelected(true);
                    ImageView imageView2 = a2.C;
                    kd4.a((Object) imageView2, "binding.ivBackDate");
                    imageView2.setSelected(true);
                    ConstraintLayout constraintLayout = a2.r;
                    kd4.a((Object) constraintLayout, "binding.clOverviewDay");
                    constraintLayout.setSelected(true);
                    FlexibleTextView flexibleTextView7 = a2.w;
                    kd4.a((Object) flexibleTextView7, "binding.ftvDayOfWeek");
                    flexibleTextView7.setSelected(true);
                    FlexibleTextView flexibleTextView8 = a2.v;
                    kd4.a((Object) flexibleTextView8, "binding.ftvDayOfMonth");
                    flexibleTextView8.setSelected(true);
                    View view = a2.E;
                    kd4.a((Object) view, "binding.line");
                    view.setSelected(true);
                    FlexibleTextView flexibleTextView9 = a2.u;
                    kd4.a((Object) flexibleTextView9, "binding.ftvDailyValue");
                    flexibleTextView9.setSelected(true);
                    FlexibleTextView flexibleTextView10 = a2.t;
                    kd4.a((Object) flexibleTextView10, "binding.ftvDailyUnit");
                    flexibleTextView10.setSelected(true);
                    FlexibleTextView flexibleTextView11 = a2.x;
                    kd4.a((Object) flexibleTextView11, "binding.ftvEst");
                    flexibleTextView11.setSelected(true);
                    i4 = 0;
                } else {
                    ImageView imageView3 = a2.D;
                    kd4.a((Object) imageView3, "binding.ivNextDate");
                    i4 = 0;
                    imageView3.setSelected(false);
                    ImageView imageView4 = a2.C;
                    kd4.a((Object) imageView4, "binding.ivBackDate");
                    imageView4.setSelected(false);
                    ConstraintLayout constraintLayout2 = a2.r;
                    kd4.a((Object) constraintLayout2, "binding.clOverviewDay");
                    constraintLayout2.setSelected(false);
                    FlexibleTextView flexibleTextView12 = a2.w;
                    kd4.a((Object) flexibleTextView12, "binding.ftvDayOfWeek");
                    flexibleTextView12.setSelected(false);
                    FlexibleTextView flexibleTextView13 = a2.v;
                    kd4.a((Object) flexibleTextView13, "binding.ftvDayOfMonth");
                    flexibleTextView13.setSelected(false);
                    View view2 = a2.E;
                    kd4.a((Object) view2, "binding.line");
                    view2.setSelected(false);
                    FlexibleTextView flexibleTextView14 = a2.u;
                    kd4.a((Object) flexibleTextView14, "binding.ftvDailyValue");
                    flexibleTextView14.setSelected(false);
                    FlexibleTextView flexibleTextView15 = a2.t;
                    kd4.a((Object) flexibleTextView15, "binding.ftvDailyUnit");
                    flexibleTextView15.setSelected(false);
                    FlexibleTextView flexibleTextView16 = a2.x;
                    kd4.a((Object) flexibleTextView16, "binding.ftvEst");
                    flexibleTextView16.setSelected(false);
                }
                if (i5 == -1) {
                    ProgressBar progressBar = a2.G;
                    kd4.a((Object) progressBar, "binding.pbGoal");
                    progressBar.setProgress(i4);
                    FlexibleTextView flexibleTextView17 = a2.A;
                    kd4.a((Object) flexibleTextView17, "binding.ftvProgressValue");
                    flexibleTextView17.setText(sm2.a(context, (int) R.string.character_dash_double));
                } else {
                    ProgressBar progressBar2 = a2.G;
                    kd4.a((Object) progressBar2, "binding.pbGoal");
                    progressBar2.setProgress(i5);
                    FlexibleTextView flexibleTextView18 = a2.A;
                    kd4.a((Object) flexibleTextView18, "binding.ftvProgressValue");
                    flexibleTextView18.setText(i5 + "%");
                }
                FlexibleTextView flexibleTextView19 = a2.y;
                kd4.a((Object) flexibleTextView19, "binding.ftvGoalValue");
                pd4 pd43 = pd4.a;
                String a7 = sm2.a(context, (int) R.string.DashboardDiana_Steps_DetailPage_Title__OfNumberSteps);
                kd4.a((Object) a7, "LanguageHelper.getString\u2026age_Title__OfNumberSteps)");
                Object[] objArr3 = {ol2.a.b(Integer.valueOf(i))};
                String format3 = String.format(a7, Arrays.copyOf(objArr3, objArr3.length));
                kd4.a((Object) format3, "java.lang.String.format(format, *args)");
                flexibleTextView19.setText(format3);
            }
        }
    }

    @DexIgnore
    public void a(wr2 wr2, ArrayList<String> arrayList) {
        kd4.b(wr2, "baseModel");
        kd4.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityDetailFragment", "showDayDetailChart - baseModel=" + wr2);
        tr3<a92> tr3 = this.l;
        if (tr3 != null) {
            a92 a2 = tr3.a();
            if (a2 != null) {
                OverviewDayChart overviewDayChart = a2.s;
                if (overviewDayChart != null) {
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    if (!arrayList.isEmpty()) {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
                    } else {
                        BarChart.a((BarChart) overviewDayChart, (ArrayList) ll2.b.a(), false, 2, (Object) null);
                    }
                    overviewDayChart.a(wr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(boolean z, Unit unit, qd<WorkoutSession> qdVar) {
        kd4.b(unit, MFUser.DISTANCE_UNIT);
        kd4.b(qdVar, "workoutSessions");
        tr3<a92> tr3 = this.l;
        if (tr3 != null) {
            a92 a2 = tr3.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.F;
                    kd4.a((Object) linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!qdVar.isEmpty()) {
                        FlexibleTextView flexibleTextView = a2.z;
                        kd4.a((Object) flexibleTextView, "it.ftvNoWorkoutRecorded");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.H;
                        kd4.a((Object) recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        WorkoutPagedAdapter workoutPagedAdapter = this.j;
                        if (workoutPagedAdapter != null) {
                            workoutPagedAdapter.a(unit, qdVar);
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = a2.z;
                        kd4.a((Object) flexibleTextView2, "it.ftvNoWorkoutRecorded");
                        flexibleTextView2.setVisibility(0);
                        RecyclerView recyclerView2 = a2.H;
                        kd4.a((Object) recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        WorkoutPagedAdapter workoutPagedAdapter2 = this.j;
                        if (workoutPagedAdapter2 != null) {
                            workoutPagedAdapter2.a(unit, qdVar);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.F;
                    kd4.a((Object) linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                kd4.a((Object) appBarLayout, "it.appBarLayout");
                ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.d();
                    if (behavior == null) {
                        behavior = new AppBarLayout.Behavior();
                    }
                    behavior.setDragCallback(new b(this, z, qdVar, unit));
                    eVar.a((CoordinatorLayout.Behavior) behavior);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
        }
    }
}
