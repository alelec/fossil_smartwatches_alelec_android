package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface i64 {
    @DexIgnore
    void a(Throwable th);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    boolean a();
}
