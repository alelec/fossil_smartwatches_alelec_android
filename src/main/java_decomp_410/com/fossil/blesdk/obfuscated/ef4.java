package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ef4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ yd4 b;

    @DexIgnore
    public ef4(String str, yd4 yd4) {
        kd4.b(str, "value");
        kd4.b(yd4, "range");
        this.a = str;
        this.b = yd4;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ef4)) {
            return false;
        }
        ef4 ef4 = (ef4) obj;
        return kd4.a((Object) this.a, (Object) ef4.a) && kd4.a((Object) this.b, (Object) ef4.b);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        yd4 yd4 = this.b;
        if (yd4 != null) {
            i = yd4.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "MatchGroup(value=" + this.a + ", range=" + this.b + ")";
    }
}
