package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d53 implements Factory<CommuteTimeSettingsDetailViewModel> {
    @DexIgnore
    public /* final */ Provider<en2> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public d53(Provider<en2> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static d53 a(Provider<en2> provider, Provider<UserRepository> provider2) {
        return new d53(provider, provider2);
    }

    @DexIgnore
    public static CommuteTimeSettingsDetailViewModel b(Provider<en2> provider, Provider<UserRepository> provider2) {
        return new CommuteTimeSettingsDetailViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public CommuteTimeSettingsDetailViewModel get() {
        return b(this.a, this.b);
    }
}
