package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Rect;
import android.text.method.TransformationMethod;
import android.view.View;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class r0 implements TransformationMethod {
    @DexIgnore
    public Locale e;

    @DexIgnore
    public r0(Context context) {
        this.e = context.getResources().getConfiguration().locale;
    }

    @DexIgnore
    public CharSequence getTransformation(CharSequence charSequence, View view) {
        if (charSequence != null) {
            return charSequence.toString().toUpperCase(this.e);
        }
        return null;
    }

    @DexIgnore
    public void onFocusChanged(View view, CharSequence charSequence, boolean z, int i, Rect rect) {
    }
}
