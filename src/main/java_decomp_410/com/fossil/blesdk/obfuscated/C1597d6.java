package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d6 */
public class C1597d6 {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.d6$a")
    /* renamed from: com.fossil.blesdk.obfuscated.d6$a */
    public static class C1598a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.os.Bundle f4261a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C1926h6[] f4262b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C1926h6[] f4263c;

        @DexIgnore
        /* renamed from: d */
        public boolean f4264d;

        @DexIgnore
        /* renamed from: e */
        public boolean f4265e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ int f4266f;

        @DexIgnore
        /* renamed from: g */
        public int f4267g;

        @DexIgnore
        /* renamed from: h */
        public java.lang.CharSequence f4268h;

        @DexIgnore
        /* renamed from: i */
        public android.app.PendingIntent f4269i;

        @DexIgnore
        public C1598a(int i, java.lang.CharSequence charSequence, android.app.PendingIntent pendingIntent) {
            this(i, charSequence, pendingIntent, new android.os.Bundle(), (com.fossil.blesdk.obfuscated.C1926h6[]) null, (com.fossil.blesdk.obfuscated.C1926h6[]) null, true, 0, true);
        }

        @DexIgnore
        /* renamed from: a */
        public android.app.PendingIntent mo9759a() {
            return this.f4269i;
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo9760b() {
            return this.f4264d;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1926h6[] mo9761c() {
            return this.f4263c;
        }

        @DexIgnore
        /* renamed from: d */
        public android.os.Bundle mo9762d() {
            return this.f4261a;
        }

        @DexIgnore
        /* renamed from: e */
        public int mo9763e() {
            return this.f4267g;
        }

        @DexIgnore
        /* renamed from: f */
        public com.fossil.blesdk.obfuscated.C1926h6[] mo9764f() {
            return this.f4262b;
        }

        @DexIgnore
        /* renamed from: g */
        public int mo9765g() {
            return this.f4266f;
        }

        @DexIgnore
        /* renamed from: h */
        public boolean mo9766h() {
            return this.f4265e;
        }

        @DexIgnore
        /* renamed from: i */
        public java.lang.CharSequence mo9767i() {
            return this.f4268h;
        }

        @DexIgnore
        public C1598a(int i, java.lang.CharSequence charSequence, android.app.PendingIntent pendingIntent, android.os.Bundle bundle, com.fossil.blesdk.obfuscated.C1926h6[] h6VarArr, com.fossil.blesdk.obfuscated.C1926h6[] h6VarArr2, boolean z, int i2, boolean z2) {
            this.f4265e = true;
            this.f4267g = i;
            this.f4268h = com.fossil.blesdk.obfuscated.C1597d6.C1600c.m5702d(charSequence);
            this.f4269i = pendingIntent;
            this.f4261a = bundle == null ? new android.os.Bundle() : bundle;
            this.f4262b = h6VarArr;
            this.f4263c = h6VarArr2;
            this.f4264d = z;
            this.f4266f = i2;
            this.f4265e = z2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.d6$b")
    /* renamed from: com.fossil.blesdk.obfuscated.d6$b */
    public static class C1599b extends com.fossil.blesdk.obfuscated.C1597d6.C1601d {

        @DexIgnore
        /* renamed from: e */
        public java.lang.CharSequence f4270e;

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1599b mo9768a(java.lang.CharSequence charSequence) {
            this.f4270e = com.fossil.blesdk.obfuscated.C1597d6.C1600c.m5702d(charSequence);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9769a(com.fossil.blesdk.obfuscated.C1537c6 c6Var) {
            if (android.os.Build.VERSION.SDK_INT >= 16) {
                android.app.Notification.BigTextStyle bigText = new android.app.Notification.BigTextStyle(c6Var.mo9428a()).setBigContentTitle(this.f4313b).bigText(this.f4270e);
                if (this.f4315d) {
                    bigText.setSummaryText(this.f4314c);
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.d6$c")
    /* renamed from: com.fossil.blesdk.obfuscated.d6$c */
    public static class C1600c {

        @DexIgnore
        /* renamed from: A */
        public java.lang.String f4271A;

        @DexIgnore
        /* renamed from: B */
        public android.os.Bundle f4272B;

        @DexIgnore
        /* renamed from: C */
        public int f4273C;

        @DexIgnore
        /* renamed from: D */
        public int f4274D;

        @DexIgnore
        /* renamed from: E */
        public android.app.Notification f4275E;

        @DexIgnore
        /* renamed from: F */
        public android.widget.RemoteViews f4276F;

        @DexIgnore
        /* renamed from: G */
        public android.widget.RemoteViews f4277G;

        @DexIgnore
        /* renamed from: H */
        public android.widget.RemoteViews f4278H;

        @DexIgnore
        /* renamed from: I */
        public java.lang.String f4279I;

        @DexIgnore
        /* renamed from: J */
        public int f4280J;

        @DexIgnore
        /* renamed from: K */
        public java.lang.String f4281K;

        @DexIgnore
        /* renamed from: L */
        public long f4282L;

        @DexIgnore
        /* renamed from: M */
        public int f4283M;

        @DexIgnore
        /* renamed from: N */
        public android.app.Notification f4284N;
        @java.lang.Deprecated

        @DexIgnore
        /* renamed from: O */
        public java.util.ArrayList<java.lang.String> f4285O;

        @DexIgnore
        /* renamed from: a */
        public android.content.Context f4286a;

        @DexIgnore
        /* renamed from: b */
        public java.util.ArrayList<com.fossil.blesdk.obfuscated.C1597d6.C1598a> f4287b;

        @DexIgnore
        /* renamed from: c */
        public java.util.ArrayList<com.fossil.blesdk.obfuscated.C1597d6.C1598a> f4288c;

        @DexIgnore
        /* renamed from: d */
        public java.lang.CharSequence f4289d;

        @DexIgnore
        /* renamed from: e */
        public java.lang.CharSequence f4290e;

        @DexIgnore
        /* renamed from: f */
        public android.app.PendingIntent f4291f;

        @DexIgnore
        /* renamed from: g */
        public android.app.PendingIntent f4292g;

        @DexIgnore
        /* renamed from: h */
        public android.widget.RemoteViews f4293h;

        @DexIgnore
        /* renamed from: i */
        public android.graphics.Bitmap f4294i;

        @DexIgnore
        /* renamed from: j */
        public java.lang.CharSequence f4295j;

        @DexIgnore
        /* renamed from: k */
        public int f4296k;

        @DexIgnore
        /* renamed from: l */
        public int f4297l;

        @DexIgnore
        /* renamed from: m */
        public boolean f4298m;

        @DexIgnore
        /* renamed from: n */
        public boolean f4299n;

        @DexIgnore
        /* renamed from: o */
        public com.fossil.blesdk.obfuscated.C1597d6.C1601d f4300o;

        @DexIgnore
        /* renamed from: p */
        public java.lang.CharSequence f4301p;

        @DexIgnore
        /* renamed from: q */
        public java.lang.CharSequence[] f4302q;

        @DexIgnore
        /* renamed from: r */
        public int f4303r;

        @DexIgnore
        /* renamed from: s */
        public int f4304s;

        @DexIgnore
        /* renamed from: t */
        public boolean f4305t;

        @DexIgnore
        /* renamed from: u */
        public java.lang.String f4306u;

        @DexIgnore
        /* renamed from: v */
        public boolean f4307v;

        @DexIgnore
        /* renamed from: w */
        public java.lang.String f4308w;

        @DexIgnore
        /* renamed from: x */
        public boolean f4309x;

        @DexIgnore
        /* renamed from: y */
        public boolean f4310y;

        @DexIgnore
        /* renamed from: z */
        public boolean f4311z;

        @DexIgnore
        public C1600c(android.content.Context context, java.lang.String str) {
            this.f4287b = new java.util.ArrayList<>();
            this.f4288c = new java.util.ArrayList<>();
            this.f4298m = true;
            this.f4309x = false;
            this.f4273C = 0;
            this.f4274D = 0;
            this.f4280J = 0;
            this.f4283M = 0;
            this.f4284N = new android.app.Notification();
            this.f4286a = context;
            this.f4279I = str;
            this.f4284N.when = java.lang.System.currentTimeMillis();
            this.f4284N.audioStreamType = -1;
            this.f4297l = 0;
            this.f4285O = new java.util.ArrayList<>();
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9774a(long j) {
            this.f4284N.when = j;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9788b(java.lang.CharSequence charSequence) {
            this.f4289d = m5702d(charSequence);
            return this;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9791c(int i) {
            this.f4284N.icon = i;
            return this;
        }

        @DexIgnore
        /* renamed from: d */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9794d(boolean z) {
            this.f4298m = z;
            return this;
        }

        @DexIgnore
        /* renamed from: d */
        public static java.lang.CharSequence m5702d(java.lang.CharSequence charSequence) {
            return (charSequence != null && charSequence.length() > 5120) ? charSequence.subSequence(0, 5120) : charSequence;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9779a(java.lang.CharSequence charSequence) {
            this.f4290e = m5702d(charSequence);
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9786b(android.app.PendingIntent pendingIntent) {
            this.f4284N.deleteIntent = pendingIntent;
            return this;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9792c(java.lang.CharSequence charSequence) {
            this.f4284N.tickerText = m5702d(charSequence);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9775a(android.app.PendingIntent pendingIntent) {
            this.f4291f = pendingIntent;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9787b(android.graphics.Bitmap bitmap) {
            this.f4294i = mo9771a(bitmap);
            return this;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9793c(boolean z) {
            mo9783a(2, z);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public final android.graphics.Bitmap mo9771a(android.graphics.Bitmap bitmap) {
            if (bitmap == null || android.os.Build.VERSION.SDK_INT >= 27) {
                return bitmap;
            }
            android.content.res.Resources resources = this.f4286a.getResources();
            int dimensionPixelSize = resources.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2862s5.compat_notification_large_icon_max_width);
            int dimensionPixelSize2 = resources.getDimensionPixelSize(com.fossil.blesdk.obfuscated.C2862s5.compat_notification_large_icon_max_height);
            if (bitmap.getWidth() <= dimensionPixelSize && bitmap.getHeight() <= dimensionPixelSize2) {
                return bitmap;
            }
            double min = java.lang.Math.min(((double) dimensionPixelSize) / ((double) java.lang.Math.max(1, bitmap.getWidth())), ((double) dimensionPixelSize2) / ((double) java.lang.Math.max(1, bitmap.getHeight())));
            return android.graphics.Bitmap.createScaledBitmap(bitmap, (int) java.lang.Math.ceil(((double) bitmap.getWidth()) * min), (int) java.lang.Math.ceil(((double) bitmap.getHeight()) * min), true);
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9790b(boolean z) {
            this.f4309x = z;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9785b(int i) {
            this.f4297l = i;
            return this;
        }

        @DexIgnore
        /* renamed from: b */
        public android.os.Bundle mo9784b() {
            if (this.f4272B == null) {
                this.f4272B = new android.os.Bundle();
            }
            return this.f4272B;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9789b(java.lang.String str) {
            this.f4279I = str;
            return this;
        }

        @DexIgnore
        @java.lang.Deprecated
        public C1600c(android.content.Context context) {
            this(context, (java.lang.String) null);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9776a(android.net.Uri uri) {
            android.app.Notification notification = this.f4284N;
            notification.sound = uri;
            notification.audioStreamType = -1;
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                notification.audioAttributes = new android.media.AudioAttributes.Builder().setContentType(4).setUsage(5).build();
            }
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9782a(long[] jArr) {
            this.f4284N.vibrate = jArr;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9781a(boolean z) {
            mo9783a(16, z);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9780a(java.lang.String str) {
            this.f4271A = str;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo9783a(int i, boolean z) {
            if (z) {
                android.app.Notification notification = this.f4284N;
                notification.flags = i | notification.flags;
                return;
            }
            android.app.Notification notification2 = this.f4284N;
            notification2.flags = (~i) & notification2.flags;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9773a(int i, java.lang.CharSequence charSequence, android.app.PendingIntent pendingIntent) {
            this.f4287b.add(new com.fossil.blesdk.obfuscated.C1597d6.C1598a(i, charSequence, pendingIntent));
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9777a(com.fossil.blesdk.obfuscated.C1597d6.C1598a aVar) {
            this.f4287b.add(aVar);
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9778a(com.fossil.blesdk.obfuscated.C1597d6.C1601d dVar) {
            if (this.f4300o != dVar) {
                this.f4300o = dVar;
                com.fossil.blesdk.obfuscated.C1597d6.C1601d dVar2 = this.f4300o;
                if (dVar2 != null) {
                    dVar2.mo9796a(this);
                }
            }
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c mo9772a(int i) {
            this.f4273C = i;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public android.app.Notification mo9770a() {
            return new com.fossil.blesdk.obfuscated.C1704e6(this).mo10339b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.d6$d")
    /* renamed from: com.fossil.blesdk.obfuscated.d6$d */
    public static abstract class C1601d {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C1597d6.C1600c f4312a;

        @DexIgnore
        /* renamed from: b */
        public java.lang.CharSequence f4313b;

        @DexIgnore
        /* renamed from: c */
        public java.lang.CharSequence f4314c;

        @DexIgnore
        /* renamed from: d */
        public boolean f4315d; // = false;

        @DexIgnore
        /* renamed from: a */
        public void mo9795a(android.os.Bundle bundle) {
        }

        @DexIgnore
        /* renamed from: a */
        public abstract void mo9769a(com.fossil.blesdk.obfuscated.C1537c6 c6Var);

        @DexIgnore
        /* renamed from: a */
        public void mo9796a(com.fossil.blesdk.obfuscated.C1597d6.C1600c cVar) {
            if (this.f4312a != cVar) {
                this.f4312a = cVar;
                com.fossil.blesdk.obfuscated.C1597d6.C1600c cVar2 = this.f4312a;
                if (cVar2 != null) {
                    cVar2.mo9778a(this);
                }
            }
        }

        @DexIgnore
        /* renamed from: b */
        public android.widget.RemoteViews mo9797b(com.fossil.blesdk.obfuscated.C1537c6 c6Var) {
            return null;
        }

        @DexIgnore
        /* renamed from: c */
        public android.widget.RemoteViews mo9798c(com.fossil.blesdk.obfuscated.C1537c6 c6Var) {
            return null;
        }

        @DexIgnore
        /* renamed from: d */
        public android.widget.RemoteViews mo9799d(com.fossil.blesdk.obfuscated.C1537c6 c6Var) {
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.os.Bundle m5690a(android.app.Notification notification) {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i >= 19) {
            return notification.extras;
        }
        if (i >= 16) {
            return com.fossil.blesdk.obfuscated.C1771f6.m6744a(notification);
        }
        return null;
    }
}
