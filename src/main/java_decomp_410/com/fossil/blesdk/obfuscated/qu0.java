package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ru0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qu0 implements rv0 {
    @DexIgnore
    public static /* final */ qu0 a; // = new qu0();

    @DexIgnore
    public static qu0 a() {
        return a;
    }

    @DexIgnore
    public final boolean zza(Class<?> cls) {
        return ru0.class.isAssignableFrom(cls);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [java.lang.Class<?>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final qv0 zzb(Class<?> r5) {
        Class cls = ru0.class;
        if (!cls.isAssignableFrom(r5)) {
            String valueOf = String.valueOf(r5.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (qv0) ru0.a(r5.asSubclass(cls)).a(ru0.e.c, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(r5.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }
}
