package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sy3 extends uy3 {
    @DexIgnore
    public sy3(Context context) {
        super(context);
    }

    @DexIgnore
    public final void a(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to sharedPreferences");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
            edit.putString(wy3.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
            edit.commit();
        }
    }

    @DexIgnore
    public final boolean a() {
        return true;
    }

    @DexIgnore
    public final String b() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from sharedPreferences");
            string = PreferenceManager.getDefaultSharedPreferences(this.a).getString(wy3.c("4kU71lN96TJUomD1vOU9lgj9Tw=="), (String) null);
        }
        return string;
    }
}
