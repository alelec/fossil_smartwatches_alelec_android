package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dz2 implements MembersInjector<NotificationContactsAndAppsAssignedActivity> {
    @DexIgnore
    public static void a(NotificationContactsAndAppsAssignedActivity notificationContactsAndAppsAssignedActivity, NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
        notificationContactsAndAppsAssignedActivity.B = notificationContactsAndAppsAssignedPresenter;
    }
}
