package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.contact.ContactGroup;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mw2 extends bs2<lw2> {
    @DexIgnore
    int E();

    @DexIgnore
    int J();

    @DexIgnore
    void N();

    @DexIgnore
    void P();

    @DexIgnore
    List<ContactGroup> Q();

    @DexIgnore
    void T();

    @DexIgnore
    void a();

    @DexIgnore
    void a(ContactGroup contactGroup);

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void close();

    @DexIgnore
    void i(String str);

    @DexIgnore
    void l(List<ContactGroup> list);

    @DexIgnore
    void m(String str);
}
