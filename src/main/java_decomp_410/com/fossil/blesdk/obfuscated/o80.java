package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.LegacyFileControlStatusCode;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o80 extends f70 {
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId G;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ o80(short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public GattCharacteristic.CharacteristicId B() {
        return this.H;
    }

    @DexIgnore
    public byte[] D() {
        return this.I;
    }

    @DexIgnore
    public GattCharacteristic.CharacteristicId E() {
        return this.G;
    }

    @DexIgnore
    public byte[] G() {
        return this.J;
    }

    @DexIgnore
    public o70 b(byte b) {
        return LegacyFileControlStatusCode.Companion.a(b);
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.FILE_HANDLE, n90.a(this.K));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o80(short s, Peripheral peripheral, int i) {
        super(RequestId.LEGACY_ERASE_ACTIVITY_FILE, peripheral, i);
        kd4.b(peripheral, "peripheral");
        this.K = s;
        GattCharacteristic.CharacteristicId characteristicId = GattCharacteristic.CharacteristicId.FTC;
        this.G = characteristicId;
        this.H = characteristicId;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_ERASE_FILE.getCode()).putShort(this.K).array();
        kd4.a((Object) array, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(LegacyFileControlOperationCode.LEGACY_ERASE_FILE.responseCode()).putShort(this.K).array();
        kd4.a((Object) array2, "ByteBuffer.allocate(3)\n \u2026dle)\n            .array()");
        this.J = array2;
    }
}
