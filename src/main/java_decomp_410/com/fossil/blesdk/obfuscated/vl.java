package com.fossil.blesdk.obfuscated;

import androidx.work.WorkInfo;
import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vl implements Runnable {
    @DexIgnore
    public static /* final */ String g; // = dj.a("StopWorkRunnable");
    @DexIgnore
    public tj e;
    @DexIgnore
    public String f;

    @DexIgnore
    public vl(tj tjVar, String str) {
        this.e = tjVar;
        this.f = str;
    }

    @DexIgnore
    public void run() {
        WorkDatabase g2 = this.e.g();
        il d = g2.d();
        g2.beginTransaction();
        try {
            if (d.d(this.f) == WorkInfo.State.RUNNING) {
                d.a(WorkInfo.State.ENQUEUED, this.f);
            }
            boolean e2 = this.e.e().e(this.f);
            dj.a().a(g, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", new Object[]{this.f, Boolean.valueOf(e2)}), new Throwable[0]);
            g2.setTransactionSuccessful();
        } finally {
            g2.endTransaction();
        }
    }
}
