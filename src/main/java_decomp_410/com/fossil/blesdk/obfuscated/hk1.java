package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ij0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hk1 implements ServiceConnection, ij0.a, ij0.b {
    @DexIgnore
    public volatile boolean a;
    @DexIgnore
    public volatile sg1 b;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 c;

    @DexIgnore
    public hk1(vj1 vj1) {
        this.c = vj1;
    }

    @DexIgnore
    public final void a(Intent intent) {
        this.c.e();
        Context context = this.c.getContext();
        dm0 a2 = dm0.a();
        synchronized (this) {
            if (this.a) {
                this.c.d().A().a("Connection attempt already in progress");
                return;
            }
            this.c.d().A().a("Using local app measurement service");
            this.a = true;
            a2.a(context, intent, this.c.c, 129);
        }
    }

    @DexIgnore
    public final void b() {
        this.c.e();
        Context context = this.c.getContext();
        synchronized (this) {
            if (this.a) {
                this.c.d().A().a("Connection attempt already in progress");
            } else if (this.b == null || (!this.b.e() && !this.b.c())) {
                this.b = new sg1(context, Looper.getMainLooper(), this, this);
                this.c.d().A().a("Connecting to remote service");
                this.a = true;
                this.b.o();
            } else {
                this.c.d().A().a("Already awaiting connection attempt");
            }
        }
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        bk0.a("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.c.a().a((Runnable) new kk1(this, (kg1) this.b.x()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.b = null;
                this.a = false;
            }
        }
    }

    @DexIgnore
    public final void f(int i) {
        bk0.a("MeasurementServiceConnection.onConnectionSuspended");
        this.c.d().z().a("Service connection suspended");
        this.c.a().a((Runnable) new lk1(this));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r3.c.d().s().a("Service connect failed to get IMeasurementService");
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0062 */
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        kg1 kg1;
        bk0.a("MeasurementServiceConnection.onServiceConnected");
        synchronized (this) {
            if (iBinder == null) {
                this.a = false;
                this.c.d().s().a("Service connected with null binder");
                return;
            }
            kg1 kg12 = null;
            String interfaceDescriptor = iBinder.getInterfaceDescriptor();
            if ("com.google.android.gms.measurement.internal.IMeasurementService".equals(interfaceDescriptor)) {
                if (iBinder != null) {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
                    if (queryLocalInterface instanceof kg1) {
                        kg1 = (kg1) queryLocalInterface;
                    } else {
                        kg1 = new mg1(iBinder);
                    }
                    kg12 = kg1;
                }
                this.c.d().A().a("Bound to IMeasurementService interface");
            } else {
                this.c.d().s().a("Got binder with a wrong descriptor", interfaceDescriptor);
            }
            if (kg12 == null) {
                this.a = false;
                try {
                    dm0.a().a(this.c.getContext(), this.c.c);
                } catch (IllegalArgumentException unused) {
                }
            } else {
                this.c.a().a((Runnable) new ik1(this, kg12));
            }
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        bk0.a("MeasurementServiceConnection.onServiceDisconnected");
        this.c.d().z().a("Service disconnected");
        this.c.a().a((Runnable) new jk1(this, componentName));
    }

    @DexIgnore
    public final void a() {
        if (this.b != null && (this.b.c() || this.b.e())) {
            this.b.a();
        }
        this.b = null;
    }

    @DexIgnore
    public final void a(ud0 ud0) {
        bk0.a("MeasurementServiceConnection.onConnectionFailed");
        tg1 v = this.c.a.v();
        if (v != null) {
            v.v().a("Service connection failed", ud0);
        }
        synchronized (this) {
            this.a = false;
            this.b = null;
        }
        this.c.a().a((Runnable) new mk1(this));
    }
}
