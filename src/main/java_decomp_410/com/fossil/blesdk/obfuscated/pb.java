package com.fossil.blesdk.obfuscated;

import androidx.lifecycle.LiveData;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class pb<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ LiveData<T> b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable e; // = new b();
    @DexIgnore
    public /* final */ Runnable f; // = new c();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LiveData<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void d() {
            pb pbVar = pb.this;
            pbVar.a.execute(pbVar.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            boolean z;
            do {
                if (pb.this.d.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (pb.this.c.compareAndSet(true, false)) {
                        try {
                            obj = pb.this.a();
                            z = true;
                        } finally {
                            pb.this.d.set(false);
                        }
                    }
                    if (z) {
                        pb.this.b.a(obj);
                    }
                } else {
                    z = false;
                }
                if (!z) {
                    return;
                }
            } while (pb.this.c.get());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            boolean c = pb.this.b.c();
            if (pb.this.c.compareAndSet(false, true) && c) {
                pb pbVar = pb.this;
                pbVar.a.execute(pbVar.e);
            }
        }
    }

    @DexIgnore
    public pb(Executor executor) {
        this.a = executor;
        this.b = new a();
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public LiveData<T> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        h3.c().b(this.f);
    }
}
