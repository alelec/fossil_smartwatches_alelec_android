package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cs<Data> implements sr<Uri, Data> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(new String[]{"http", Utility.URL_SCHEME})));
    @DexIgnore
    public /* final */ sr<lr, Data> a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tr<Uri, InputStream> {
        @DexIgnore
        public sr<Uri, InputStream> a(wr wrVar) {
            return new cs(wrVar.a(lr.class, InputStream.class));
        }
    }

    @DexIgnore
    public cs(sr<lr, Data> srVar) {
        this.a = srVar;
    }

    @DexIgnore
    public sr.a<Data> a(Uri uri, int i, int i2, lo loVar) {
        return this.a.a(new lr(uri.toString()), i, i2, loVar);
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
