package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.ze0;
import com.google.android.gms.location.LocationRequest;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y31 {
    @DexIgnore
    public /* final */ m41<u31> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ Map<ze0.a<rc1>, d41> d; // = new HashMap();
    @DexIgnore
    public /* final */ Map<ze0.a<Object>, c41> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<ze0.a<qc1>, z31> f; // = new HashMap();

    @DexIgnore
    public y31(Context context, m41<u31> m41) {
        this.b = context;
        this.a = m41;
    }

    @DexIgnore
    public final Location a() throws RemoteException {
        this.a.a();
        return this.a.b().b(this.b.getPackageName());
    }

    @DexIgnore
    public final d41 a(ze0<rc1> ze0) {
        d41 d41;
        synchronized (this.d) {
            d41 = this.d.get(ze0.b());
            if (d41 == null) {
                d41 = new d41(ze0);
            }
            this.d.put(ze0.b(), d41);
        }
        return d41;
    }

    @DexIgnore
    public final void a(i41 i41, ze0<qc1> ze0, r31 r31) throws RemoteException {
        this.a.a();
        this.a.b().a(new k41(1, i41, (IBinder) null, (PendingIntent) null, b(ze0).asBinder(), r31 != null ? r31.asBinder() : null));
    }

    @DexIgnore
    public final void a(ze0.a<rc1> aVar, r31 r31) throws RemoteException {
        this.a.a();
        bk0.a(aVar, (Object) "Invalid null listener key");
        synchronized (this.d) {
            d41 remove = this.d.remove(aVar);
            if (remove != null) {
                remove.o();
                this.a.b().a(k41.a((wd1) remove, r31));
            }
        }
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, ze0<rc1> ze0, r31 r31) throws RemoteException {
        this.a.a();
        this.a.b().a(new k41(1, i41.a(locationRequest), a(ze0).asBinder(), (PendingIntent) null, (IBinder) null, r31 != null ? r31.asBinder() : null));
    }

    @DexIgnore
    public final void a(boolean z) throws RemoteException {
        this.a.a();
        this.a.b().e(z);
        this.c = z;
    }

    @DexIgnore
    public final z31 b(ze0<qc1> ze0) {
        z31 z31;
        synchronized (this.f) {
            z31 = this.f.get(ze0.b());
            if (z31 == null) {
                z31 = new z31(ze0);
            }
            this.f.put(ze0.b(), z31);
        }
        return z31;
    }

    @DexIgnore
    public final void b() throws RemoteException {
        synchronized (this.d) {
            for (d41 next : this.d.values()) {
                if (next != null) {
                    this.a.b().a(k41.a((wd1) next, (r31) null));
                }
            }
            this.d.clear();
        }
        synchronized (this.f) {
            for (z31 next2 : this.f.values()) {
                if (next2 != null) {
                    this.a.b().a(k41.a((td1) next2, (r31) null));
                }
            }
            this.f.clear();
        }
        synchronized (this.e) {
            for (c41 next3 : this.e.values()) {
                if (next3 != null) {
                    this.a.b().a(new v41(2, (t41) null, next3.asBinder(), (IBinder) null));
                }
            }
            this.e.clear();
        }
    }

    @DexIgnore
    public final void b(ze0.a<qc1> aVar, r31 r31) throws RemoteException {
        this.a.a();
        bk0.a(aVar, (Object) "Invalid null listener key");
        synchronized (this.f) {
            z31 remove = this.f.remove(aVar);
            if (remove != null) {
                remove.o();
                this.a.b().a(k41.a((td1) remove, r31));
            }
        }
    }

    @DexIgnore
    public final void c() throws RemoteException {
        if (this.c) {
            a(false);
        }
    }
}
