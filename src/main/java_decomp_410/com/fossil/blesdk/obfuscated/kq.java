package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kq implements jq {
    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(int i) {
    }

    @DexIgnore
    public void a(Bitmap bitmap) {
        bitmap.recycle();
    }

    @DexIgnore
    public Bitmap b(int i, int i2, Bitmap.Config config) {
        return a(i, i2, config);
    }

    @DexIgnore
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }
}
