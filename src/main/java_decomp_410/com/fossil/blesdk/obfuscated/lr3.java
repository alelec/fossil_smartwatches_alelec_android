package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lr3 implements Factory<kr3> {
    @DexIgnore
    public /* final */ Provider<en2> a;

    @DexIgnore
    public lr3(Provider<en2> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static lr3 a(Provider<en2> provider) {
        return new lr3(provider);
    }

    @DexIgnore
    public static kr3 b(Provider<en2> provider) {
        return new kr3(provider.get());
    }

    @DexIgnore
    public kr3 get() {
        return b(this.a);
    }
}
