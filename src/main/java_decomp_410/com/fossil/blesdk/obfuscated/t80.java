package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.legacy.LegacyFileControlOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t80 extends q80 {
    @DexIgnore
    public long L;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ t80(short s, Peripheral peripheral, int i, int i2, fd4 fd4) {
        this(s, peripheral, (i2 & 4) != 0 ? 3 : i);
    }

    @DexIgnore
    public final long J() {
        return this.L;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        if (bArr.length >= 4) {
            this.L = n90.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            wa0.a(a, JSONKey.SIZE_WRITTEN, Long.valueOf(this.L));
        }
        return a;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.SIZE_WRITTEN, Long.valueOf(this.L));
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t80(short s, Peripheral peripheral, int i) {
        super(LegacyFileControlOperationCode.LEGACY_GET_SIZE_WRITTEN, s, RequestId.LEGACY_GET_FILE_SIZE_WRITTEN, peripheral, i);
        kd4.b(peripheral, "peripheral");
    }
}
