package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n1 */
public abstract class C2452n1 implements com.fossil.blesdk.obfuscated.C2936t1, com.fossil.blesdk.obfuscated.C2618p1, android.widget.AdapterView.OnItemClickListener {

    @DexIgnore
    /* renamed from: e */
    public android.graphics.Rect f7624e;

    @DexIgnore
    /* renamed from: b */
    public static boolean m10961b(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        int size = h1Var.size();
        for (int i = 0; i < size; i++) {
            android.view.MenuItem item = h1Var.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo10258a(int i);

    @DexIgnore
    /* renamed from: a */
    public void mo1158a(android.content.Context context, com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13788a(android.graphics.Rect rect) {
        this.f7624e = rect;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo10259a(android.view.View view);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo10260a(android.widget.PopupWindow.OnDismissListener onDismissListener);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo10261a(com.fossil.blesdk.obfuscated.C1915h1 h1Var);

    @DexIgnore
    /* renamed from: a */
    public boolean mo1163a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public abstract void mo10262b(int i);

    @DexIgnore
    /* renamed from: b */
    public abstract void mo10263b(boolean z);

    @DexIgnore
    /* renamed from: b */
    public boolean mo1166b(com.fossil.blesdk.obfuscated.C1915h1 h1Var, com.fossil.blesdk.obfuscated.C2179k1 k1Var) {
        return false;
    }

    @DexIgnore
    /* renamed from: c */
    public abstract void mo10265c(int i);

    @DexIgnore
    /* renamed from: c */
    public abstract void mo10266c(boolean z);

    @DexIgnore
    /* renamed from: f */
    public boolean mo10269f() {
        return true;
    }

    @DexIgnore
    /* renamed from: g */
    public android.graphics.Rect mo13789g() {
        return this.f7624e;
    }

    @DexIgnore
    public int getId() {
        return 0;
    }

    @DexIgnore
    public void onItemClick(android.widget.AdapterView<?> adapterView, android.view.View view, int i, long j) {
        android.widget.ListAdapter listAdapter = (android.widget.ListAdapter) adapterView.getAdapter();
        m10960a(listAdapter).f5342e.mo11466a((android.view.MenuItem) listAdapter.getItem(i), (com.fossil.blesdk.obfuscated.C2618p1) this, mo10269f() ? 0 : 4);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m10959a(android.widget.ListAdapter listAdapter, android.view.ViewGroup viewGroup, android.content.Context context, int i) {
        int makeMeasureSpec = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = listAdapter.getCount();
        android.view.ViewGroup viewGroup2 = viewGroup;
        android.view.View view = null;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < count; i4++) {
            int itemViewType = listAdapter.getItemViewType(i4);
            if (itemViewType != i3) {
                view = null;
                i3 = itemViewType;
            }
            if (viewGroup2 == null) {
                viewGroup2 = new android.widget.FrameLayout(context);
            }
            view = listAdapter.getView(i4, view, viewGroup2);
            view.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view.getMeasuredWidth();
            if (measuredWidth >= i) {
                return i;
            }
            if (measuredWidth > i2) {
                i2 = measuredWidth;
            }
        }
        return i2;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1852g1 m10960a(android.widget.ListAdapter listAdapter) {
        if (listAdapter instanceof android.widget.HeaderViewListAdapter) {
            return (com.fossil.blesdk.obfuscated.C1852g1) ((android.widget.HeaderViewListAdapter) listAdapter).getWrappedAdapter();
        }
        return (com.fossil.blesdk.obfuscated.C1852g1) listAdapter;
    }
}
