package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l1 extends c1<i7> implements MenuItem {
    @DexIgnore
    public Method e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends m8 {
        @DexIgnore
        public /* final */ ActionProvider a;

        @DexIgnore
        public a(Context context, ActionProvider actionProvider) {
            super(context);
            this.a = actionProvider;
        }

        @DexIgnore
        public boolean hasSubMenu() {
            return this.a.hasSubMenu();
        }

        @DexIgnore
        public View onCreateActionView() {
            return this.a.onCreateActionView();
        }

        @DexIgnore
        public boolean onPerformDefaultAction() {
            return this.a.onPerformDefaultAction();
        }

        @DexIgnore
        public void onPrepareSubMenu(SubMenu subMenu) {
            this.a.onPrepareSubMenu(l1.this.a(subMenu));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends FrameLayout implements t0 {
        @DexIgnore
        public /* final */ CollapsibleActionView e;

        @DexIgnore
        public b(View view) {
            super(view.getContext());
            this.e = (CollapsibleActionView) view;
            addView(view);
        }

        @DexIgnore
        public View a() {
            return (View) this.e;
        }

        @DexIgnore
        public void onActionViewCollapsed() {
            this.e.onActionViewCollapsed();
        }

        @DexIgnore
        public void onActionViewExpanded() {
            this.e.onActionViewExpanded();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends d1<MenuItem.OnActionExpandListener> implements MenuItem.OnActionExpandListener {
        @DexIgnore
        public c(MenuItem.OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        @DexIgnore
        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.a).onMenuItemActionCollapse(l1.this.a(menuItem));
        }

        @DexIgnore
        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.a).onMenuItemActionExpand(l1.this.a(menuItem));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends d1<MenuItem.OnMenuItemClickListener> implements MenuItem.OnMenuItemClickListener {
        @DexIgnore
        public d(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            return ((MenuItem.OnMenuItemClickListener) this.a).onMenuItemClick(l1.this.a(menuItem));
        }
    }

    @DexIgnore
    public l1(Context context, i7 i7Var) {
        super(context, i7Var);
    }

    @DexIgnore
    public void a(boolean z) {
        try {
            if (this.e == null) {
                this.e = ((i7) this.a).getClass().getDeclaredMethod("setExclusiveCheckable", new Class[]{Boolean.TYPE});
            }
            this.e.invoke(this.a, new Object[]{Boolean.valueOf(z)});
        } catch (Exception e2) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e2);
        }
    }

    @DexIgnore
    public boolean collapseActionView() {
        return ((i7) this.a).collapseActionView();
    }

    @DexIgnore
    public boolean expandActionView() {
        return ((i7) this.a).expandActionView();
    }

    @DexIgnore
    public ActionProvider getActionProvider() {
        m8 a2 = ((i7) this.a).a();
        if (a2 instanceof a) {
            return ((a) a2).a;
        }
        return null;
    }

    @DexIgnore
    public View getActionView() {
        View actionView = ((i7) this.a).getActionView();
        return actionView instanceof b ? ((b) actionView).a() : actionView;
    }

    @DexIgnore
    public int getAlphabeticModifiers() {
        return ((i7) this.a).getAlphabeticModifiers();
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return ((i7) this.a).getAlphabeticShortcut();
    }

    @DexIgnore
    public CharSequence getContentDescription() {
        return ((i7) this.a).getContentDescription();
    }

    @DexIgnore
    public int getGroupId() {
        return ((i7) this.a).getGroupId();
    }

    @DexIgnore
    public Drawable getIcon() {
        return ((i7) this.a).getIcon();
    }

    @DexIgnore
    public ColorStateList getIconTintList() {
        return ((i7) this.a).getIconTintList();
    }

    @DexIgnore
    public PorterDuff.Mode getIconTintMode() {
        return ((i7) this.a).getIconTintMode();
    }

    @DexIgnore
    public Intent getIntent() {
        return ((i7) this.a).getIntent();
    }

    @DexIgnore
    public int getItemId() {
        return ((i7) this.a).getItemId();
    }

    @DexIgnore
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((i7) this.a).getMenuInfo();
    }

    @DexIgnore
    public int getNumericModifiers() {
        return ((i7) this.a).getNumericModifiers();
    }

    @DexIgnore
    public char getNumericShortcut() {
        return ((i7) this.a).getNumericShortcut();
    }

    @DexIgnore
    public int getOrder() {
        return ((i7) this.a).getOrder();
    }

    @DexIgnore
    public SubMenu getSubMenu() {
        return a(((i7) this.a).getSubMenu());
    }

    @DexIgnore
    public CharSequence getTitle() {
        return ((i7) this.a).getTitle();
    }

    @DexIgnore
    public CharSequence getTitleCondensed() {
        return ((i7) this.a).getTitleCondensed();
    }

    @DexIgnore
    public CharSequence getTooltipText() {
        return ((i7) this.a).getTooltipText();
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return ((i7) this.a).hasSubMenu();
    }

    @DexIgnore
    public boolean isActionViewExpanded() {
        return ((i7) this.a).isActionViewExpanded();
    }

    @DexIgnore
    public boolean isCheckable() {
        return ((i7) this.a).isCheckable();
    }

    @DexIgnore
    public boolean isChecked() {
        return ((i7) this.a).isChecked();
    }

    @DexIgnore
    public boolean isEnabled() {
        return ((i7) this.a).isEnabled();
    }

    @DexIgnore
    public boolean isVisible() {
        return ((i7) this.a).isVisible();
    }

    @DexIgnore
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((i7) this.a).a(actionProvider != null ? a(actionProvider) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new b(view);
        }
        ((i7) this.a).setActionView(view);
        return this;
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2) {
        ((i7) this.a).setAlphabeticShortcut(c2);
        return this;
    }

    @DexIgnore
    public MenuItem setCheckable(boolean z) {
        ((i7) this.a).setCheckable(z);
        return this;
    }

    @DexIgnore
    public MenuItem setChecked(boolean z) {
        ((i7) this.a).setChecked(z);
        return this;
    }

    @DexIgnore
    public MenuItem setContentDescription(CharSequence charSequence) {
        ((i7) this.a).setContentDescription(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setEnabled(boolean z) {
        ((i7) this.a).setEnabled(z);
        return this;
    }

    @DexIgnore
    public MenuItem setIcon(Drawable drawable) {
        ((i7) this.a).setIcon(drawable);
        return this;
    }

    @DexIgnore
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        ((i7) this.a).setIconTintList(colorStateList);
        return this;
    }

    @DexIgnore
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        ((i7) this.a).setIconTintMode(mode);
        return this;
    }

    @DexIgnore
    public MenuItem setIntent(Intent intent) {
        ((i7) this.a).setIntent(intent);
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2) {
        ((i7) this.a).setNumericShortcut(c2);
        return this;
    }

    @DexIgnore
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((i7) this.a).setOnActionExpandListener(onActionExpandListener != null ? new c(onActionExpandListener) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((i7) this.a).setOnMenuItemClickListener(onMenuItemClickListener != null ? new d(onMenuItemClickListener) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3) {
        ((i7) this.a).setShortcut(c2, c3);
        return this;
    }

    @DexIgnore
    public void setShowAsAction(int i) {
        ((i7) this.a).setShowAsAction(i);
    }

    @DexIgnore
    public MenuItem setShowAsActionFlags(int i) {
        ((i7) this.a).setShowAsActionFlags(i);
        return this;
    }

    @DexIgnore
    public MenuItem setTitle(CharSequence charSequence) {
        ((i7) this.a).setTitle(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((i7) this.a).setTitleCondensed(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setTooltipText(CharSequence charSequence) {
        ((i7) this.a).setTooltipText(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setVisible(boolean z) {
        return ((i7) this.a).setVisible(z);
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2, int i) {
        ((i7) this.a).setAlphabeticShortcut(c2, i);
        return this;
    }

    @DexIgnore
    public MenuItem setIcon(int i) {
        ((i7) this.a).setIcon(i);
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2, int i) {
        ((i7) this.a).setNumericShortcut(c2, i);
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3, int i, int i2) {
        ((i7) this.a).setShortcut(c2, c3, i, i2);
        return this;
    }

    @DexIgnore
    public MenuItem setTitle(int i) {
        ((i7) this.a).setTitle(i);
        return this;
    }

    @DexIgnore
    public MenuItem setActionView(int i) {
        ((i7) this.a).setActionView(i);
        View actionView = ((i7) this.a).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((i7) this.a).setActionView((View) new b(actionView));
        }
        return this;
    }

    @DexIgnore
    public a a(ActionProvider actionProvider) {
        return new a(this.b, actionProvider);
    }
}
