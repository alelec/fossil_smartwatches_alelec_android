package com.fossil.blesdk.obfuscated;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rc1 {
    @DexIgnore
    void onLocationChanged(Location location);
}
