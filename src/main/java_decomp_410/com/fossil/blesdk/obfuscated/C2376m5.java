package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m5 */
public final class C2376m5 {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968743;
    @DexIgnore
    public static /* final */ int coordinatorLayoutStyle; // = 2130969039;
    @DexIgnore
    public static /* final */ int font; // = 2130969182;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969187;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969188;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969189;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969190;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969191;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969192;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969193;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969194;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969195;
    @DexIgnore
    public static /* final */ int keylines; // = 2130969308;
    @DexIgnore
    public static /* final */ int layout_anchor; // = 2130969317;
    @DexIgnore
    public static /* final */ int layout_anchorGravity; // = 2130969318;
    @DexIgnore
    public static /* final */ int layout_behavior; // = 2130969320;
    @DexIgnore
    public static /* final */ int layout_dodgeInsetEdges; // = 2130969364;
    @DexIgnore
    public static /* final */ int layout_insetEdge; // = 2130969374;
    @DexIgnore
    public static /* final */ int layout_keyline; // = 2130969375;
    @DexIgnore
    public static /* final */ int statusBarBackground; // = 2130969672;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969834;
}
