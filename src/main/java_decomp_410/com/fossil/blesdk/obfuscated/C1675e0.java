package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e0 */
public class C1675e0 extends androidx.appcompat.app.AppCompatDelegate implements com.fossil.blesdk.obfuscated.C1915h1.C1916a, android.view.LayoutInflater.Factory2 {

    @DexIgnore
    /* renamed from: W */
    public static /* final */ boolean f4551W; // = (android.os.Build.VERSION.SDK_INT < 21);

    @DexIgnore
    /* renamed from: X */
    public static /* final */ int[] f4552X; // = {16842836};

    @DexIgnore
    /* renamed from: Y */
    public static boolean f4553Y; // = true;

    @DexIgnore
    /* renamed from: A */
    public boolean f4554A;

    @DexIgnore
    /* renamed from: B */
    public boolean f4555B;

    @DexIgnore
    /* renamed from: C */
    public boolean f4556C;

    @DexIgnore
    /* renamed from: D */
    public boolean f4557D;

    @DexIgnore
    /* renamed from: E */
    public boolean f4558E;

    @DexIgnore
    /* renamed from: F */
    public boolean f4559F;

    @DexIgnore
    /* renamed from: G */
    public boolean f4560G;

    @DexIgnore
    /* renamed from: H */
    public boolean f4561H;

    @DexIgnore
    /* renamed from: I */
    public com.fossil.blesdk.obfuscated.C1675e0.C1692n[] f4562I;

    @DexIgnore
    /* renamed from: J */
    public com.fossil.blesdk.obfuscated.C1675e0.C1692n f4563J;

    @DexIgnore
    /* renamed from: K */
    public boolean f4564K;

    @DexIgnore
    /* renamed from: L */
    public boolean f4565L;

    @DexIgnore
    /* renamed from: M */
    public int f4566M; // = -100;

    @DexIgnore
    /* renamed from: N */
    public boolean f4567N;

    @DexIgnore
    /* renamed from: O */
    public com.fossil.blesdk.obfuscated.C1675e0.C1689l f4568O;

    @DexIgnore
    /* renamed from: P */
    public boolean f4569P;

    @DexIgnore
    /* renamed from: Q */
    public int f4570Q;

    @DexIgnore
    /* renamed from: R */
    public /* final */ java.lang.Runnable f4571R; // = new com.fossil.blesdk.obfuscated.C1675e0.C1677b();

    @DexIgnore
    /* renamed from: S */
    public boolean f4572S;

    @DexIgnore
    /* renamed from: T */
    public android.graphics.Rect f4573T;

    @DexIgnore
    /* renamed from: U */
    public android.graphics.Rect f4574U;

    @DexIgnore
    /* renamed from: V */
    public androidx.appcompat.app.AppCompatViewInflater f4575V;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.content.Context f4576f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.view.Window f4577g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ android.view.Window.Callback f4578h;

    @DexIgnore
    /* renamed from: i */
    public /* final */ android.view.Window.Callback f4579i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ com.fossil.blesdk.obfuscated.C1591d0 f4580j;

    @DexIgnore
    /* renamed from: k */
    public androidx.appcompat.app.ActionBar f4581k;

    @DexIgnore
    /* renamed from: l */
    public android.view.MenuInflater f4582l;

    @DexIgnore
    /* renamed from: m */
    public java.lang.CharSequence f4583m;

    @DexIgnore
    /* renamed from: n */
    public com.fossil.blesdk.obfuscated.C1994i2 f4584n;

    @DexIgnore
    /* renamed from: o */
    public com.fossil.blesdk.obfuscated.C1675e0.C1685i f4585o;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C1675e0.C1693o f4586p;

    @DexIgnore
    /* renamed from: q */
    public androidx.appcompat.view.ActionMode f4587q;

    @DexIgnore
    /* renamed from: r */
    public androidx.appcompat.widget.ActionBarContextView f4588r;

    @DexIgnore
    /* renamed from: s */
    public android.widget.PopupWindow f4589s;

    @DexIgnore
    /* renamed from: t */
    public java.lang.Runnable f4590t;

    @DexIgnore
    /* renamed from: u */
    public com.fossil.blesdk.obfuscated.C2110j9 f4591u; // = null;

    @DexIgnore
    /* renamed from: v */
    public boolean f4592v; // = true;

    @DexIgnore
    /* renamed from: w */
    public boolean f4593w;

    @DexIgnore
    /* renamed from: x */
    public android.view.ViewGroup f4594x;

    @DexIgnore
    /* renamed from: y */
    public android.widget.TextView f4595y;

    @DexIgnore
    /* renamed from: z */
    public android.view.View f4596z;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$a */
    public static class C1676a implements java.lang.Thread.UncaughtExceptionHandler {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ java.lang.Thread.UncaughtExceptionHandler f4597a;

        @DexIgnore
        public C1676a(java.lang.Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.f4597a = uncaughtExceptionHandler;
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo10224a(java.lang.Throwable th) {
            if (!(th instanceof android.content.res.Resources.NotFoundException)) {
                return false;
            }
            java.lang.String message = th.getMessage();
            if (message == null) {
                return false;
            }
            if (message.contains(com.sina.weibo.sdk.utils.ResourceManager.DRAWABLE) || message.contains("Drawable")) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public void uncaughtException(java.lang.Thread thread, java.lang.Throwable th) {
            if (mo10224a(th)) {
                android.content.res.Resources.NotFoundException notFoundException = new android.content.res.Resources.NotFoundException(th.getMessage() + ". If the resource you are trying to use is a vector resource, you may be referencing it in an unsupported way. See AppCompatDelegate.setCompatVectorFromResourcesEnabled() for more info.");
                notFoundException.initCause(th.getCause());
                notFoundException.setStackTrace(th.getStackTrace());
                this.f4597a.uncaughtException(thread, notFoundException);
                return;
            }
            this.f4597a.uncaughtException(thread, th);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$b")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$b */
    public class C1677b implements java.lang.Runnable {
        @DexIgnore
        public C1677b() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1675e0 e0Var = com.fossil.blesdk.obfuscated.C1675e0.this;
            if ((e0Var.f4570Q & 1) != 0) {
                e0Var.mo10198e(0);
            }
            com.fossil.blesdk.obfuscated.C1675e0 e0Var2 = com.fossil.blesdk.obfuscated.C1675e0.this;
            if ((e0Var2.f4570Q & 4096) != 0) {
                e0Var2.mo10198e(108);
            }
            com.fossil.blesdk.obfuscated.C1675e0 e0Var3 = com.fossil.blesdk.obfuscated.C1675e0.this;
            e0Var3.f4569P = false;
            e0Var3.f4570Q = 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$c")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$c */
    public class C1678c implements com.fossil.blesdk.obfuscated.C1468b9 {
        @DexIgnore
        public C1678c() {
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2468n9 mo1534a(android.view.View view, com.fossil.blesdk.obfuscated.C2468n9 n9Var) {
            int e = n9Var.mo13879e();
            int l = com.fossil.blesdk.obfuscated.C1675e0.this.mo10206l(e);
            if (e != l) {
                n9Var = n9Var.mo13875a(n9Var.mo13877c(), l, n9Var.mo13878d(), n9Var.mo13876b());
            }
            return com.fossil.blesdk.obfuscated.C1776f9.m6823b(view, n9Var);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$d")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$d */
    public class C1679d implements com.fossil.blesdk.obfuscated.C2368m2.C2369a {
        @DexIgnore
        public C1679d() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10227a(android.graphics.Rect rect) {
            rect.top = com.fossil.blesdk.obfuscated.C1675e0.this.mo10206l(rect.top);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$e")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$e */
    public class C1680e implements androidx.appcompat.widget.ContentFrameLayout.C0085a {
        @DexIgnore
        public C1680e() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo797a() {
        }

        @DexIgnore
        public void onDetachedFromWindow() {
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10209n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$f")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$f */
    public class C1681f implements java.lang.Runnable {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$f$a")
        /* renamed from: com.fossil.blesdk.obfuscated.e0$f$a */
        public class C1682a extends com.fossil.blesdk.obfuscated.C2302l9 {
            @DexIgnore
            public C1682a() {
            }

            @DexIgnore
            /* renamed from: b */
            public void mo8506b(android.view.View view) {
                com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setAlpha(1.0f);
                com.fossil.blesdk.obfuscated.C1675e0.this.f4591u.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) null);
                com.fossil.blesdk.obfuscated.C1675e0.this.f4591u = null;
            }

            @DexIgnore
            /* renamed from: c */
            public void mo8507c(android.view.View view) {
                com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setVisibility(0);
            }
        }

        @DexIgnore
        public C1681f() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1675e0 e0Var = com.fossil.blesdk.obfuscated.C1675e0.this;
            e0Var.f4589s.showAtLocation(e0Var.f4588r, 55, 0, 0);
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10210o();
            if (com.fossil.blesdk.obfuscated.C1675e0.this.mo10223z()) {
                com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setAlpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                com.fossil.blesdk.obfuscated.C1675e0 e0Var2 = com.fossil.blesdk.obfuscated.C1675e0.this;
                com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(e0Var2.f4588r);
                a.mo12275a(1.0f);
                e0Var2.f4591u = a;
                com.fossil.blesdk.obfuscated.C1675e0.this.f4591u.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) new com.fossil.blesdk.obfuscated.C1675e0.C1681f.C1682a());
                return;
            }
            com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setAlpha(1.0f);
            com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setVisibility(0);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$g")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$g */
    public class C1683g extends com.fossil.blesdk.obfuscated.C2302l9 {
        @DexIgnore
        public C1683g() {
        }

        @DexIgnore
        /* renamed from: b */
        public void mo8506b(android.view.View view) {
            com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setAlpha(1.0f);
            com.fossil.blesdk.obfuscated.C1675e0.this.f4591u.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) null);
            com.fossil.blesdk.obfuscated.C1675e0.this.f4591u = null;
        }

        @DexIgnore
        /* renamed from: c */
        public void mo8507c(android.view.View view) {
            com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setVisibility(0);
            com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.sendAccessibilityEvent(32);
            if (com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.getParent() instanceof android.view.View) {
                com.fossil.blesdk.obfuscated.C1776f9.m6797D((android.view.View) com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.getParent());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$h")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$h */
    public class C1684h implements androidx.appcompat.app.ActionBarDrawerToggle$Delegate {
        @DexIgnore
        public C1684h(com.fossil.blesdk.obfuscated.C1675e0 e0Var) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$i")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$i */
    public final class C1685i implements com.fossil.blesdk.obfuscated.C2618p1.C2619a {
        @DexIgnore
        public C1685i() {
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo534a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            android.view.Window.Callback u = com.fossil.blesdk.obfuscated.C1675e0.this.mo10218u();
            if (u == null) {
                return true;
            }
            u.onMenuOpened(108, h1Var);
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo533a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10190b(h1Var);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$j")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$j */
    public class C1686j implements androidx.appcompat.view.ActionMode.Callback {

        @DexIgnore
        /* renamed from: a */
        public androidx.appcompat.view.ActionMode.Callback f4606a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$j$a")
        /* renamed from: com.fossil.blesdk.obfuscated.e0$j$a */
        public class C1687a extends com.fossil.blesdk.obfuscated.C2302l9 {
            @DexIgnore
            public C1687a() {
            }

            @DexIgnore
            /* renamed from: b */
            public void mo8506b(android.view.View view) {
                com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.setVisibility(8);
                com.fossil.blesdk.obfuscated.C1675e0 e0Var = com.fossil.blesdk.obfuscated.C1675e0.this;
                android.widget.PopupWindow popupWindow = e0Var.f4589s;
                if (popupWindow != null) {
                    popupWindow.dismiss();
                } else if (e0Var.f4588r.getParent() instanceof android.view.View) {
                    com.fossil.blesdk.obfuscated.C1776f9.m6797D((android.view.View) com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.getParent());
                }
                com.fossil.blesdk.obfuscated.C1675e0.this.f4588r.removeAllViews();
                com.fossil.blesdk.obfuscated.C1675e0.this.f4591u.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) null);
                com.fossil.blesdk.obfuscated.C1675e0.this.f4591u = null;
            }
        }

        @DexIgnore
        public C1686j(androidx.appcompat.view.ActionMode.Callback callback) {
            this.f4606a = callback;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo346a(androidx.appcompat.view.ActionMode actionMode, android.view.Menu menu) {
            return this.f4606a.mo346a(actionMode, menu);
        }

        @DexIgnore
        /* renamed from: b */
        public boolean mo348b(androidx.appcompat.view.ActionMode actionMode, android.view.Menu menu) {
            return this.f4606a.mo348b(actionMode, menu);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo347a(androidx.appcompat.view.ActionMode actionMode, android.view.MenuItem menuItem) {
            return this.f4606a.mo347a(actionMode, menuItem);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo345a(androidx.appcompat.view.ActionMode actionMode) {
            this.f4606a.mo345a(actionMode);
            com.fossil.blesdk.obfuscated.C1675e0 e0Var = com.fossil.blesdk.obfuscated.C1675e0.this;
            if (e0Var.f4589s != null) {
                e0Var.f4577g.getDecorView().removeCallbacks(com.fossil.blesdk.obfuscated.C1675e0.this.f4590t);
            }
            com.fossil.blesdk.obfuscated.C1675e0 e0Var2 = com.fossil.blesdk.obfuscated.C1675e0.this;
            if (e0Var2.f4588r != null) {
                e0Var2.mo10210o();
                com.fossil.blesdk.obfuscated.C1675e0 e0Var3 = com.fossil.blesdk.obfuscated.C1675e0.this;
                com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(e0Var3.f4588r);
                a.mo12275a((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                e0Var3.f4591u = a;
                com.fossil.blesdk.obfuscated.C1675e0.this.f4591u.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) new com.fossil.blesdk.obfuscated.C1675e0.C1686j.C1687a());
            }
            com.fossil.blesdk.obfuscated.C1675e0 e0Var4 = com.fossil.blesdk.obfuscated.C1675e0.this;
            com.fossil.blesdk.obfuscated.C1591d0 d0Var = e0Var4.f4580j;
            if (d0Var != null) {
                d0Var.onSupportActionModeFinished(e0Var4.f4587q);
            }
            com.fossil.blesdk.obfuscated.C1675e0.this.f4587q = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$k")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$k */
    public class C1688k extends com.fossil.blesdk.obfuscated.C3399z0 {
        @DexIgnore
        public C1688k(android.view.Window.Callback callback) {
            super(callback);
        }

        @DexIgnore
        /* renamed from: a */
        public final android.view.ActionMode mo10229a(android.view.ActionMode.Callback callback) {
            com.fossil.blesdk.obfuscated.C3159w0.C3160a aVar = new com.fossil.blesdk.obfuscated.C3159w0.C3160a(com.fossil.blesdk.obfuscated.C1675e0.this.f4576f, callback);
            androidx.appcompat.view.ActionMode a = com.fossil.blesdk.obfuscated.C1675e0.this.mo289a((androidx.appcompat.view.ActionMode.Callback) aVar);
            if (a != null) {
                return aVar.mo17262b(a);
            }
            return null;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(android.view.KeyEvent keyEvent) {
            return com.fossil.blesdk.obfuscated.C1675e0.this.mo10185a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean dispatchKeyShortcutEvent(android.view.KeyEvent keyEvent) {
            return super.dispatchKeyShortcutEvent(keyEvent) || com.fossil.blesdk.obfuscated.C1675e0.this.mo10194c(keyEvent.getKeyCode(), keyEvent);
        }

        @DexIgnore
        public void onContentChanged() {
        }

        @DexIgnore
        public boolean onCreatePanelMenu(int i, android.view.Menu menu) {
            if (i != 0 || (menu instanceof com.fossil.blesdk.obfuscated.C1915h1)) {
                return super.onCreatePanelMenu(i, menu);
            }
            return false;
        }

        @DexIgnore
        public boolean onMenuOpened(int i, android.view.Menu menu) {
            super.onMenuOpened(i, menu);
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10202h(i);
            return true;
        }

        @DexIgnore
        public void onPanelClosed(int i, android.view.Menu menu) {
            super.onPanelClosed(i, menu);
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10203i(i);
        }

        @DexIgnore
        public boolean onPreparePanel(int i, android.view.View view, android.view.Menu menu) {
            com.fossil.blesdk.obfuscated.C1915h1 h1Var = menu instanceof com.fossil.blesdk.obfuscated.C1915h1 ? (com.fossil.blesdk.obfuscated.C1915h1) menu : null;
            if (i == 0 && h1Var == null) {
                return false;
            }
            if (h1Var != null) {
                h1Var.mo11497d(true);
            }
            boolean onPreparePanel = super.onPreparePanel(i, view, menu);
            if (h1Var != null) {
                h1Var.mo11497d(false);
            }
            return onPreparePanel;
        }

        @DexIgnore
        public void onProvideKeyboardShortcuts(java.util.List<android.view.KeyboardShortcutGroup> list, android.view.Menu menu, int i) {
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a = com.fossil.blesdk.obfuscated.C1675e0.this.mo10177a(0, true);
            if (a != null) {
                com.fossil.blesdk.obfuscated.C1915h1 h1Var = a.f4626j;
                if (h1Var != null) {
                    super.onProvideKeyboardShortcuts(list, h1Var, i);
                    return;
                }
            }
            super.onProvideKeyboardShortcuts(list, menu, i);
        }

        @DexIgnore
        public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode.Callback callback) {
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                return null;
            }
            if (com.fossil.blesdk.obfuscated.C1675e0.this.mo10220w()) {
                return mo10229a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        @DexIgnore
        public android.view.ActionMode onWindowStartingActionMode(android.view.ActionMode.Callback callback, int i) {
            if (!com.fossil.blesdk.obfuscated.C1675e0.this.mo10220w() || i != 0) {
                return super.onWindowStartingActionMode(callback, i);
            }
            return mo10229a(callback);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$l")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$l */
    public final class C1689l {

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2177k0 f4610a;

        @DexIgnore
        /* renamed from: b */
        public boolean f4611b;

        @DexIgnore
        /* renamed from: c */
        public android.content.BroadcastReceiver f4612c;

        @DexIgnore
        /* renamed from: d */
        public android.content.IntentFilter f4613d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$l$a")
        /* renamed from: com.fossil.blesdk.obfuscated.e0$l$a */
        public class C1690a extends android.content.BroadcastReceiver {
            @DexIgnore
            public C1690a() {
            }

            @DexIgnore
            public void onReceive(android.content.Context context, android.content.Intent intent) {
                com.fossil.blesdk.obfuscated.C1675e0.C1689l.this.mo10241b();
            }
        }

        @DexIgnore
        public C1689l(com.fossil.blesdk.obfuscated.C2177k0 k0Var) {
            this.f4610a = k0Var;
            this.f4611b = k0Var.mo12536b();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10240a() {
            android.content.BroadcastReceiver broadcastReceiver = this.f4612c;
            if (broadcastReceiver != null) {
                com.fossil.blesdk.obfuscated.C1675e0.this.f4576f.unregisterReceiver(broadcastReceiver);
                this.f4612c = null;
            }
        }

        @DexIgnore
        /* renamed from: b */
        public void mo10241b() {
            boolean b = this.f4610a.mo12536b();
            if (b != this.f4611b) {
                this.f4611b = b;
                com.fossil.blesdk.obfuscated.C1675e0.this.mo296a();
            }
        }

        @DexIgnore
        /* renamed from: c */
        public int mo10242c() {
            this.f4611b = this.f4610a.mo12536b();
            return this.f4611b ? 2 : 1;
        }

        @DexIgnore
        /* renamed from: d */
        public void mo10243d() {
            mo10240a();
            if (this.f4612c == null) {
                this.f4612c = new com.fossil.blesdk.obfuscated.C1675e0.C1689l.C1690a();
            }
            if (this.f4613d == null) {
                this.f4613d = new android.content.IntentFilter();
                this.f4613d.addAction("android.intent.action.TIME_SET");
                this.f4613d.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.f4613d.addAction("android.intent.action.TIME_TICK");
            }
            com.fossil.blesdk.obfuscated.C1675e0.this.f4576f.registerReceiver(this.f4612c, this.f4613d);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$m")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$m */
    public class C1691m extends androidx.appcompat.widget.ContentFrameLayout {
        @DexIgnore
        public C1691m(android.content.Context context) {
            super(context);
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo10245a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }

        @DexIgnore
        public boolean dispatchKeyEvent(android.view.KeyEvent keyEvent) {
            return com.fossil.blesdk.obfuscated.C1675e0.this.mo10185a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        @DexIgnore
        public boolean onInterceptTouchEvent(android.view.MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !mo10245a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10196d(0);
            return true;
        }

        @DexIgnore
        public void setBackgroundResource(int i) {
            setBackgroundDrawable(com.fossil.blesdk.obfuscated.C2364m0.m10497c(getContext(), i));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$n")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$n */
    public static final class C1692n {

        @DexIgnore
        /* renamed from: a */
        public int f4617a;

        @DexIgnore
        /* renamed from: b */
        public int f4618b;

        @DexIgnore
        /* renamed from: c */
        public int f4619c;

        @DexIgnore
        /* renamed from: d */
        public int f4620d;

        @DexIgnore
        /* renamed from: e */
        public int f4621e;

        @DexIgnore
        /* renamed from: f */
        public int f4622f;

        @DexIgnore
        /* renamed from: g */
        public android.view.ViewGroup f4623g;

        @DexIgnore
        /* renamed from: h */
        public android.view.View f4624h;

        @DexIgnore
        /* renamed from: i */
        public android.view.View f4625i;

        @DexIgnore
        /* renamed from: j */
        public com.fossil.blesdk.obfuscated.C1915h1 f4626j;

        @DexIgnore
        /* renamed from: k */
        public com.fossil.blesdk.obfuscated.C1763f1 f4627k;

        @DexIgnore
        /* renamed from: l */
        public android.content.Context f4628l;

        @DexIgnore
        /* renamed from: m */
        public boolean f4629m;

        @DexIgnore
        /* renamed from: n */
        public boolean f4630n;

        @DexIgnore
        /* renamed from: o */
        public boolean f4631o;

        @DexIgnore
        /* renamed from: p */
        public boolean f4632p;

        @DexIgnore
        /* renamed from: q */
        public boolean f4633q; // = false;

        @DexIgnore
        /* renamed from: r */
        public boolean f4634r;

        @DexIgnore
        /* renamed from: s */
        public android.os.Bundle f4635s;

        @DexIgnore
        public C1692n(int i) {
            this.f4617a = i;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo10252a() {
            if (this.f4624h == null) {
                return false;
            }
            if (this.f4625i == null && this.f4627k.mo10684c().getCount() <= 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10250a(android.content.Context context) {
            android.util.TypedValue typedValue = new android.util.TypedValue();
            android.content.res.Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarPopupTheme, typedValue, true);
            int i = typedValue.resourceId;
            if (i != 0) {
                newTheme.applyStyle(i, true);
            }
            newTheme.resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.panelMenuListTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                newTheme.applyStyle(i2, true);
            } else {
                newTheme.applyStyle(com.fossil.blesdk.obfuscated.C3398z.Theme_AppCompat_CompactMenu, true);
            }
            com.fossil.blesdk.obfuscated.C2999u0 u0Var = new com.fossil.blesdk.obfuscated.C2999u0(context, 0);
            u0Var.getTheme().setTo(newTheme);
            this.f4628l = u0Var;
            android.content.res.TypedArray obtainStyledAttributes = u0Var.obtainStyledAttributes(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme);
            this.f4618b = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_panelBackground, 0);
            this.f4622f = obtainStyledAttributes.getResourceId(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10251a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            com.fossil.blesdk.obfuscated.C1915h1 h1Var2 = this.f4626j;
            if (h1Var != h1Var2) {
                if (h1Var2 != null) {
                    h1Var2.mo11482b((com.fossil.blesdk.obfuscated.C2618p1) this.f4627k);
                }
                this.f4626j = h1Var;
                if (h1Var != null) {
                    com.fossil.blesdk.obfuscated.C1763f1 f1Var = this.f4627k;
                    if (f1Var != null) {
                        h1Var.mo11461a((com.fossil.blesdk.obfuscated.C2618p1) f1Var);
                    }
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2697q1 mo10249a(com.fossil.blesdk.obfuscated.C2618p1.C2619a aVar) {
            if (this.f4626j == null) {
                return null;
            }
            if (this.f4627k == null) {
                this.f4627k = new com.fossil.blesdk.obfuscated.C1763f1(this.f4628l, com.fossil.blesdk.obfuscated.C3250x.abc_list_menu_item_layout);
                this.f4627k.mo9013a(aVar);
                this.f4626j.mo11461a((com.fossil.blesdk.obfuscated.C2618p1) this.f4627k);
            }
            return this.f4627k.mo10681a(this.f4623g);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.e0$o")
    /* renamed from: com.fossil.blesdk.obfuscated.e0$o */
    public final class C1693o implements com.fossil.blesdk.obfuscated.C2618p1.C2619a {
        @DexIgnore
        public C1693o() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo533a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
            com.fossil.blesdk.obfuscated.C1915h1 m = h1Var.mo11514m();
            boolean z2 = m != h1Var;
            com.fossil.blesdk.obfuscated.C1675e0 e0Var = com.fossil.blesdk.obfuscated.C1675e0.this;
            if (z2) {
                h1Var = m;
            }
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a = e0Var.mo10178a((android.view.Menu) h1Var);
            if (a == null) {
                return;
            }
            if (z2) {
                com.fossil.blesdk.obfuscated.C1675e0.this.mo10179a(a.f4617a, a, m);
                com.fossil.blesdk.obfuscated.C1675e0.this.mo10182a(a, true);
                return;
            }
            com.fossil.blesdk.obfuscated.C1675e0.this.mo10182a(a, z);
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo534a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
            if (h1Var != null) {
                return true;
            }
            com.fossil.blesdk.obfuscated.C1675e0 e0Var = com.fossil.blesdk.obfuscated.C1675e0.this;
            if (!e0Var.f4556C) {
                return true;
            }
            android.view.Window.Callback u = e0Var.mo10218u();
            if (u == null || com.fossil.blesdk.obfuscated.C1675e0.this.f4565L) {
                return true;
            }
            u.onMenuOpened(108, h1Var);
            return true;
        }
    }

    /*
    static {
        if (f4551W && !f4553Y) {
            java.lang.Thread.setDefaultUncaughtExceptionHandler(new com.fossil.blesdk.obfuscated.C1675e0.C1676a(java.lang.Thread.getDefaultUncaughtExceptionHandler()));
        }
    }
    */

    @DexIgnore
    public C1675e0(android.content.Context context, android.view.Window window, com.fossil.blesdk.obfuscated.C1591d0 d0Var) {
        this.f4576f = context;
        this.f4577g = window;
        this.f4580j = d0Var;
        this.f4578h = this.f4577g.getCallback();
        android.view.Window.Callback callback = this.f4578h;
        if (!(callback instanceof com.fossil.blesdk.obfuscated.C1675e0.C1688k)) {
            this.f4579i = new com.fossil.blesdk.obfuscated.C1675e0.C1688k(callback);
            this.f4577g.setCallback(this.f4579i);
            com.fossil.blesdk.obfuscated.C3410z2 a = com.fossil.blesdk.obfuscated.C3410z2.m17204a(context, (android.util.AttributeSet) null, f4552X);
            android.graphics.drawable.Drawable c = a.mo18424c(0);
            if (c != null) {
                this.f4577g.setBackgroundDrawable(c);
            }
            a.mo18418a();
            return;
        }
        throw new java.lang.IllegalStateException("AppCompat has already installed itself into the Window");
    }

    @DexIgnore
    /* renamed from: A */
    public final boolean mo10174A() {
        if (this.f4567N) {
            android.content.Context context = this.f4576f;
            if (context instanceof android.app.Activity) {
                try {
                    if ((context.getPackageManager().getActivityInfo(new android.content.ComponentName(this.f4576f, this.f4576f.getClass()), 0).configChanges & androidx.recyclerview.widget.RecyclerView.ViewHolder.FLAG_ADAPTER_POSITION_UNKNOWN) == 0) {
                        return true;
                    }
                    return false;
                } catch (android.content.pm.PackageManager.NameNotFoundException e) {
                    android.util.Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e);
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: B */
    public final void mo10175B() {
        if (this.f4593w) {
            throw new android.util.AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo291a(android.os.Bundle bundle) {
        android.view.Window.Callback callback = this.f4578h;
        if (callback instanceof android.app.Activity) {
            java.lang.String str = null;
            try {
                str = com.fossil.blesdk.obfuscated.C1465b6.m4792b((android.app.Activity) callback);
            } catch (java.lang.IllegalArgumentException unused) {
            }
            if (str != null) {
                androidx.appcompat.app.ActionBar y = mo10222y();
                if (y == null) {
                    this.f4572S = true;
                } else {
                    y.mo184c(true);
                }
            }
        }
        if (bundle != null && this.f4566M == -100) {
            this.f4566M = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10180a(android.view.ViewGroup viewGroup) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo298b(android.os.Bundle bundle) {
        mo10214q();
    }

    @DexIgnore
    /* renamed from: c */
    public android.view.MenuInflater mo301c() {
        if (this.f4582l == null) {
            mo10219v();
            androidx.appcompat.app.ActionBar actionBar = this.f4581k;
            this.f4582l = new com.fossil.blesdk.obfuscated.C3251x0(actionBar != null ? actionBar.mo190h() : this.f4576f);
        }
        return this.f4582l;
    }

    @DexIgnore
    /* renamed from: d */
    public androidx.appcompat.app.ActionBar mo304d() {
        mo10219v();
        return this.f4581k;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo305e() {
        android.view.LayoutInflater from = android.view.LayoutInflater.from(this.f4576f);
        if (from.getFactory() == null) {
            com.fossil.blesdk.obfuscated.C2790r8.m13096b(from, this);
        } else if (!(from.getFactory2() instanceof com.fossil.blesdk.obfuscated.C1675e0)) {
            android.util.Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    @DexIgnore
    /* renamed from: f */
    public void mo306f() {
        androidx.appcompat.app.ActionBar d = mo304d();
        if (d == null || !d.mo191i()) {
            mo10200f(0);
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo307g() {
        if (this.f4569P) {
            this.f4577g.getDecorView().removeCallbacks(this.f4571R);
        }
        this.f4565L = true;
        androidx.appcompat.app.ActionBar actionBar = this.f4581k;
        if (actionBar != null) {
            actionBar.mo192j();
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1689l lVar = this.f4568O;
        if (lVar != null) {
            lVar.mo10240a();
        }
    }

    @DexIgnore
    /* renamed from: h */
    public void mo308h() {
        androidx.appcompat.app.ActionBar d = mo304d();
        if (d != null) {
            d.mo186e(true);
        }
    }

    @DexIgnore
    /* renamed from: i */
    public void mo309i() {
        mo296a();
    }

    @DexIgnore
    /* renamed from: j */
    public void mo310j() {
        androidx.appcompat.app.ActionBar d = mo304d();
        if (d != null) {
            d.mo186e(false);
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1689l lVar = this.f4568O;
        if (lVar != null) {
            lVar.mo10240a();
        }
    }

    @DexIgnore
    /* renamed from: k */
    public final boolean mo10205k(int i) {
        android.content.res.Resources resources = this.f4576f.getResources();
        android.content.res.Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        if (mo10174A()) {
            ((android.app.Activity) this.f4576f).recreate();
            return true;
        }
        android.content.res.Configuration configuration2 = new android.content.res.Configuration(configuration);
        android.util.DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        configuration2.uiMode = i3 | (configuration2.uiMode & -49);
        resources.updateConfiguration(configuration2, displayMetrics);
        if (android.os.Build.VERSION.SDK_INT >= 26) {
            return true;
        }
        com.fossil.blesdk.obfuscated.C1914h0.m7650a(resources);
        return true;
    }

    @DexIgnore
    /* renamed from: l */
    public final void mo10207l() {
        androidx.appcompat.widget.ContentFrameLayout contentFrameLayout = (androidx.appcompat.widget.ContentFrameLayout) this.f4594x.findViewById(16908290);
        android.view.View decorView = this.f4577g.getDecorView();
        contentFrameLayout.mo785a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        android.content.res.TypedArray obtainStyledAttributes = this.f4576f.obtainStyledAttributes(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme);
        obtainStyledAttributes.getValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    @DexIgnore
    /* renamed from: m */
    public final android.view.ViewGroup mo10208m() {
        android.view.ViewGroup viewGroup;
        android.content.Context context;
        android.content.res.TypedArray obtainStyledAttributes = this.f4576f.obtainStyledAttributes(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme);
        if (obtainStyledAttributes.hasValue(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowActionBar)) {
            if (obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowNoTitle, false)) {
                mo300b(1);
            } else if (obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowActionBar, false)) {
                mo300b(108);
            }
            if (obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowActionBarOverlay, false)) {
                mo300b(109);
            }
            if (obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_windowActionModeOverlay, false)) {
                mo300b(10);
            }
            this.f4559F = obtainStyledAttributes.getBoolean(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_android_windowIsFloating, false);
            obtainStyledAttributes.recycle();
            this.f4577g.getDecorView();
            android.view.LayoutInflater from = android.view.LayoutInflater.from(this.f4576f);
            if (this.f4560G) {
                if (this.f4558E) {
                    viewGroup = (android.view.ViewGroup) from.inflate(com.fossil.blesdk.obfuscated.C3250x.abc_screen_simple_overlay_action_mode, (android.view.ViewGroup) null);
                } else {
                    viewGroup = (android.view.ViewGroup) from.inflate(com.fossil.blesdk.obfuscated.C3250x.abc_screen_simple, (android.view.ViewGroup) null);
                }
                if (android.os.Build.VERSION.SDK_INT >= 21) {
                    com.fossil.blesdk.obfuscated.C1776f9.m6812a((android.view.View) viewGroup, (com.fossil.blesdk.obfuscated.C1468b9) new com.fossil.blesdk.obfuscated.C1675e0.C1678c());
                } else {
                    ((com.fossil.blesdk.obfuscated.C2368m2) viewGroup).setOnFitSystemWindowsListener(new com.fossil.blesdk.obfuscated.C1675e0.C1679d());
                }
            } else if (this.f4559F) {
                viewGroup = (android.view.ViewGroup) from.inflate(com.fossil.blesdk.obfuscated.C3250x.abc_dialog_title_material, (android.view.ViewGroup) null);
                this.f4557D = false;
                this.f4556C = false;
            } else if (this.f4556C) {
                android.util.TypedValue typedValue = new android.util.TypedValue();
                this.f4576f.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarTheme, typedValue, true);
                int i = typedValue.resourceId;
                if (i != 0) {
                    context = new com.fossil.blesdk.obfuscated.C2999u0(this.f4576f, i);
                } else {
                    context = this.f4576f;
                }
                viewGroup = (android.view.ViewGroup) android.view.LayoutInflater.from(context).inflate(com.fossil.blesdk.obfuscated.C3250x.abc_screen_toolbar, (android.view.ViewGroup) null);
                this.f4584n = (com.fossil.blesdk.obfuscated.C1994i2) viewGroup.findViewById(com.fossil.blesdk.obfuscated.C3158w.decor_content_parent);
                this.f4584n.setWindowCallback(mo10218u());
                if (this.f4557D) {
                    this.f4584n.mo439a(109);
                }
                if (this.f4554A) {
                    this.f4584n.mo439a(2);
                }
                if (this.f4555B) {
                    this.f4584n.mo439a(5);
                }
            } else {
                viewGroup = null;
            }
            if (viewGroup != null) {
                if (this.f4584n == null) {
                    this.f4595y = (android.widget.TextView) viewGroup.findViewById(com.fossil.blesdk.obfuscated.C3158w.title);
                }
                com.fossil.blesdk.obfuscated.C1766f3.m6704b(viewGroup);
                androidx.appcompat.widget.ContentFrameLayout contentFrameLayout = (androidx.appcompat.widget.ContentFrameLayout) viewGroup.findViewById(com.fossil.blesdk.obfuscated.C3158w.action_bar_activity_content);
                android.view.ViewGroup viewGroup2 = (android.view.ViewGroup) this.f4577g.findViewById(16908290);
                if (viewGroup2 != null) {
                    while (viewGroup2.getChildCount() > 0) {
                        android.view.View childAt = viewGroup2.getChildAt(0);
                        viewGroup2.removeViewAt(0);
                        contentFrameLayout.addView(childAt);
                    }
                    viewGroup2.setId(-1);
                    contentFrameLayout.setId(16908290);
                    if (viewGroup2 instanceof android.widget.FrameLayout) {
                        ((android.widget.FrameLayout) viewGroup2).setForeground((android.graphics.drawable.Drawable) null);
                    }
                }
                this.f4577g.setContentView(viewGroup);
                contentFrameLayout.setAttachListener(new com.fossil.blesdk.obfuscated.C1675e0.C1680e());
                return viewGroup;
            }
            throw new java.lang.IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.f4556C + ", windowActionBarOverlay: " + this.f4557D + ", android:windowIsFloating: " + this.f4559F + ", windowActionModeOverlay: " + this.f4558E + ", windowNoTitle: " + this.f4560G + " }");
        }
        obtainStyledAttributes.recycle();
        throw new java.lang.IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
    }

    @DexIgnore
    /* renamed from: n */
    public void mo10209n() {
        com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
        if (i2Var != null) {
            i2Var.mo453g();
        }
        if (this.f4589s != null) {
            this.f4577g.getDecorView().removeCallbacks(this.f4590t);
            if (this.f4589s.isShowing()) {
                try {
                    this.f4589s.dismiss();
                } catch (java.lang.IllegalArgumentException unused) {
                }
            }
            this.f4589s = null;
        }
        mo10210o();
        com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(0, false);
        if (a != null) {
            com.fossil.blesdk.obfuscated.C1915h1 h1Var = a.f4626j;
            if (h1Var != null) {
                h1Var.close();
            }
        }
    }

    @DexIgnore
    /* renamed from: o */
    public void mo10210o() {
        com.fossil.blesdk.obfuscated.C2110j9 j9Var = this.f4591u;
        if (j9Var != null) {
            j9Var.mo12280a();
        }
    }

    @DexIgnore
    public final android.view.View onCreateView(android.view.View view, java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet) {
        return mo10176a(view, str, context, attributeSet);
    }

    @DexIgnore
    /* renamed from: p */
    public final void mo10213p() {
        if (this.f4568O == null) {
            this.f4568O = new com.fossil.blesdk.obfuscated.C1675e0.C1689l(com.fossil.blesdk.obfuscated.C2177k0.m9259a(this.f4576f));
        }
    }

    @DexIgnore
    /* renamed from: q */
    public final void mo10214q() {
        if (!this.f4593w) {
            this.f4594x = mo10208m();
            java.lang.CharSequence t = mo10217t();
            if (!android.text.TextUtils.isEmpty(t)) {
                com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
                if (i2Var != null) {
                    i2Var.setWindowTitle(t);
                } else if (mo10222y() != null) {
                    mo10222y().mo182b(t);
                } else {
                    android.widget.TextView textView = this.f4595y;
                    if (textView != null) {
                        textView.setText(t);
                    }
                }
            }
            mo10207l();
            mo10180a(this.f4594x);
            this.f4593w = true;
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(0, false);
            if (this.f4565L) {
                return;
            }
            if (a == null || a.f4626j == null) {
                mo10200f(108);
            }
        }
    }

    @DexIgnore
    /* renamed from: r */
    public final android.content.Context mo10215r() {
        androidx.appcompat.app.ActionBar d = mo304d();
        android.content.Context h = d != null ? d.mo190h() : null;
        return h == null ? this.f4576f : h;
    }

    @DexIgnore
    /* renamed from: s */
    public final int mo10216s() {
        int i = this.f4566M;
        return i != -100 ? i : androidx.appcompat.app.AppCompatDelegate.m218k();
    }

    @DexIgnore
    /* renamed from: t */
    public final java.lang.CharSequence mo10217t() {
        android.view.Window.Callback callback = this.f4578h;
        if (callback instanceof android.app.Activity) {
            return ((android.app.Activity) callback).getTitle();
        }
        return this.f4583m;
    }

    @DexIgnore
    /* renamed from: u */
    public final android.view.Window.Callback mo10218u() {
        return this.f4577g.getCallback();
    }

    @DexIgnore
    /* renamed from: v */
    public final void mo10219v() {
        mo10214q();
        if (this.f4556C && this.f4581k == null) {
            android.view.Window.Callback callback = this.f4578h;
            if (callback instanceof android.app.Activity) {
                this.f4581k = new com.fossil.blesdk.obfuscated.C2265l0((android.app.Activity) callback, this.f4557D);
            } else if (callback instanceof android.app.Dialog) {
                this.f4581k = new com.fossil.blesdk.obfuscated.C2265l0((android.app.Dialog) callback);
            }
            androidx.appcompat.app.ActionBar actionBar = this.f4581k;
            if (actionBar != null) {
                actionBar.mo184c(this.f4572S);
            }
        }
    }

    @DexIgnore
    /* renamed from: w */
    public boolean mo10220w() {
        return this.f4592v;
    }

    @DexIgnore
    /* renamed from: x */
    public boolean mo10221x() {
        androidx.appcompat.view.ActionMode actionMode = this.f4587q;
        if (actionMode != null) {
            actionMode.mo328a();
            return true;
        }
        androidx.appcompat.app.ActionBar d = mo304d();
        if (d == null || !d.mo188f()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: y */
    public final androidx.appcompat.app.ActionBar mo10222y() {
        return this.f4581k;
    }

    @DexIgnore
    /* renamed from: z */
    public final boolean mo10223z() {
        if (this.f4593w) {
            android.view.ViewGroup viewGroup = this.f4594x;
            return viewGroup != null && com.fossil.blesdk.obfuscated.C1776f9.m6860z(viewGroup);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo299b(android.view.View view, android.view.ViewGroup.LayoutParams layoutParams) {
        mo10214q();
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) this.f4594x.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.f4578h.onContentChanged();
    }

    @DexIgnore
    /* renamed from: i */
    public void mo10203i(int i) {
        if (i == 108) {
            androidx.appcompat.app.ActionBar d = mo304d();
            if (d != null) {
                d.mo183b(false);
            }
        } else if (i == 0) {
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(i, true);
            if (a.f4631o) {
                mo10182a(a, false);
            }
        }
    }

    @DexIgnore
    public android.view.View onCreateView(java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet) {
        return onCreateView((android.view.View) null, str, context, attributeSet);
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo10197d(int i, android.view.KeyEvent keyEvent) {
        if (i == 4) {
            boolean z = this.f4564K;
            this.f4564K = false;
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(0, false);
            if (a != null && a.f4631o) {
                if (!z) {
                    mo10182a(a, true);
                }
                return true;
            } else if (mo10221x()) {
                return true;
            }
        } else if (i == 82) {
            mo10199e(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: h */
    public void mo10202h(int i) {
        if (i == 108) {
            androidx.appcompat.app.ActionBar d = mo304d();
            if (d != null) {
                d.mo183b(true);
            }
        }
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo10200f(int i) {
        this.f4570Q = (1 << i) | this.f4570Q;
        if (!this.f4569P) {
            com.fossil.blesdk.obfuscated.C1776f9.m6816a(this.f4577g.getDecorView(), this.f4571R);
            this.f4569P = true;
        }
    }

    @DexIgnore
    /* renamed from: j */
    public final int mo10204j(int i) {
        if (i == 8) {
            android.util.Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i != 9) {
            return i;
        } else {
            android.util.Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo302c(int i) {
        mo10214q();
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) this.f4594x.findViewById(16908290);
        viewGroup.removeAllViews();
        android.view.LayoutInflater.from(this.f4576f).inflate(i, viewGroup);
        this.f4578h.onContentChanged();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x006c  */
    /* renamed from: e */
    public final boolean mo10199e(int i, android.view.KeyEvent keyEvent) {
        boolean z;
        boolean z2;
        if (this.f4587q != null) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(i, true);
        if (i == 0) {
            com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
            if (i2Var != null && i2Var.mo446c() && !android.view.ViewConfiguration.get(this.f4576f).hasPermanentMenuKey()) {
                if (!this.f4584n.mo442a()) {
                    if (!this.f4565L && mo10193b(a, keyEvent)) {
                        z = this.f4584n.mo451f();
                    }
                    z = false;
                } else {
                    z = this.f4584n.mo450e();
                }
                if (z) {
                    android.media.AudioManager audioManager = (android.media.AudioManager) this.f4576f.getSystemService("audio");
                    if (audioManager != null) {
                        audioManager.playSoundEffect(0);
                    } else {
                        android.util.Log.w("AppCompatDelegate", "Couldn't get audio manager");
                    }
                }
                return z;
            }
        }
        if (a.f4631o || a.f4630n) {
            z = a.f4631o;
            mo10182a(a, true);
            if (z) {
            }
            return z;
        }
        if (a.f4629m) {
            if (a.f4634r) {
                a.f4629m = false;
                z2 = mo10193b(a, keyEvent);
            } else {
                z2 = true;
            }
            if (z2) {
                mo10181a(a, keyEvent);
                z = true;
                if (z) {
                }
                return z;
            }
        }
        z = false;
        if (z) {
        }
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo300b(int i) {
        int j = mo10204j(i);
        if (this.f4560G && j == 108) {
            return false;
        }
        if (this.f4556C && j == 1) {
            this.f4556C = false;
        }
        if (j == 1) {
            mo10175B();
            this.f4560G = true;
            return true;
        } else if (j == 2) {
            mo10175B();
            this.f4554A = true;
            return true;
        } else if (j == 5) {
            mo10175B();
            this.f4555B = true;
            return true;
        } else if (j == 10) {
            mo10175B();
            this.f4558E = true;
            return true;
        } else if (j == 108) {
            mo10175B();
            this.f4556C = true;
            return true;
        } else if (j != 109) {
            return this.f4577g.requestFeature(j);
        } else {
            mo10175B();
            this.f4557D = true;
            return true;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo294a(androidx.appcompat.widget.Toolbar toolbar) {
        if (this.f4578h instanceof android.app.Activity) {
            androidx.appcompat.app.ActionBar d = mo304d();
            if (!(d instanceof com.fossil.blesdk.obfuscated.C2265l0)) {
                this.f4582l = null;
                if (d != null) {
                    d.mo192j();
                }
                if (toolbar != null) {
                    com.fossil.blesdk.obfuscated.C1987i0 i0Var = new com.fossil.blesdk.obfuscated.C1987i0(toolbar, ((android.app.Activity) this.f4578h).getTitle(), this.f4579i);
                    this.f4581k = i0Var;
                    this.f4577g.setCallback(i0Var.mo11803m());
                } else {
                    this.f4581k = null;
                    this.f4577g.setCallback(this.f4579i);
                }
                mo306f();
                return;
            }
            throw new java.lang.IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    @DexIgnore
    /* renamed from: g */
    public int mo10201g(int i) {
        if (i == -100) {
            return -1;
        }
        if (i != 0) {
            return i;
        }
        if (android.os.Build.VERSION.SDK_INT >= 23 && ((android.app.UiModeManager) this.f4576f.getSystemService(android.app.UiModeManager.class)).getNightMode() == 0) {
            return -1;
        }
        mo10213p();
        return this.f4568O.mo10242c();
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10196d(int i) {
        mo10182a(mo10177a(i, true), true);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo303c(android.os.Bundle bundle) {
        int i = this.f4566M;
        if (i != -100) {
            bundle.putInt("appcompat:local_night_mode", i);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo10194c(int i, android.view.KeyEvent keyEvent) {
        androidx.appcompat.app.ActionBar d = mo304d();
        if (d != null && d.mo180a(i, keyEvent)) {
            return true;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar = this.f4563J;
        if (nVar == null || !mo10188a(nVar, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.f4563J == null) {
                com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(0, true);
                mo10193b(a, keyEvent);
                boolean a2 = mo10188a(a, keyEvent.getKeyCode(), keyEvent, 1);
                a.f4629m = false;
                if (a2) {
                    return true;
                }
            }
            return false;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar2 = this.f4563J;
        if (nVar2 != null) {
            nVar2.f4630n = true;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public <T extends android.view.View> T mo288a(int i) {
        mo10214q();
        return this.f4577g.findViewById(i);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo290a(android.content.res.Configuration configuration) {
        if (this.f4556C && this.f4593w) {
            androidx.appcompat.app.ActionBar d = mo304d();
            if (d != null) {
                d.mo178a(configuration);
            }
        }
        com.fossil.blesdk.obfuscated.C1526c2.m5217a().mo9402f(this.f4576f);
        mo296a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0169  */
    /* renamed from: b */
    public androidx.appcompat.view.ActionMode mo10189b(androidx.appcompat.view.ActionMode.Callback callback) {
        androidx.appcompat.view.ActionMode actionMode;
        androidx.appcompat.view.ActionMode actionMode2;
        com.fossil.blesdk.obfuscated.C2999u0 u0Var;
        mo10210o();
        androidx.appcompat.view.ActionMode actionMode3 = this.f4587q;
        if (actionMode3 != null) {
            actionMode3.mo328a();
        }
        if (!(callback instanceof com.fossil.blesdk.obfuscated.C1675e0.C1686j)) {
            callback = new com.fossil.blesdk.obfuscated.C1675e0.C1686j(callback);
        }
        com.fossil.blesdk.obfuscated.C1591d0 d0Var = this.f4580j;
        if (d0Var != null && !this.f4565L) {
            try {
                actionMode = d0Var.onWindowStartingSupportActionMode(callback);
            } catch (java.lang.AbstractMethodError unused) {
            }
            if (actionMode == null) {
                this.f4587q = actionMode;
            } else {
                boolean z = true;
                if (this.f4588r == null) {
                    if (this.f4559F) {
                        android.util.TypedValue typedValue = new android.util.TypedValue();
                        android.content.res.Resources.Theme theme = this.f4576f.getTheme();
                        theme.resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarTheme, typedValue, true);
                        if (typedValue.resourceId != 0) {
                            android.content.res.Resources.Theme newTheme = this.f4576f.getResources().newTheme();
                            newTheme.setTo(theme);
                            newTheme.applyStyle(typedValue.resourceId, true);
                            com.fossil.blesdk.obfuscated.C2999u0 u0Var2 = new com.fossil.blesdk.obfuscated.C2999u0(this.f4576f, 0);
                            u0Var2.getTheme().setTo(newTheme);
                            u0Var = u0Var2;
                        } else {
                            u0Var = this.f4576f;
                        }
                        this.f4588r = new androidx.appcompat.widget.ActionBarContextView(u0Var);
                        this.f4589s = new android.widget.PopupWindow(u0Var, (android.util.AttributeSet) null, com.fossil.blesdk.obfuscated.C2777r.actionModePopupWindowStyle);
                        com.fossil.blesdk.obfuscated.C1408aa.m4344a(this.f4589s, 2);
                        this.f4589s.setContentView(this.f4588r);
                        this.f4589s.setWidth(-1);
                        u0Var.getTheme().resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarSize, typedValue, true);
                        this.f4588r.setContentHeight(android.util.TypedValue.complexToDimensionPixelSize(typedValue.data, u0Var.getResources().getDisplayMetrics()));
                        this.f4589s.setHeight(-2);
                        this.f4590t = new com.fossil.blesdk.obfuscated.C1675e0.C1681f();
                    } else {
                        androidx.appcompat.widget.ViewStubCompat viewStubCompat = (androidx.appcompat.widget.ViewStubCompat) this.f4594x.findViewById(com.fossil.blesdk.obfuscated.C3158w.action_mode_bar_stub);
                        if (viewStubCompat != null) {
                            viewStubCompat.setLayoutInflater(android.view.LayoutInflater.from(mo10215r()));
                            this.f4588r = (androidx.appcompat.widget.ActionBarContextView) viewStubCompat.mo1172a();
                        }
                    }
                }
                if (this.f4588r != null) {
                    mo10210o();
                    this.f4588r.mo418d();
                    android.content.Context context = this.f4588r.getContext();
                    androidx.appcompat.widget.ActionBarContextView actionBarContextView = this.f4588r;
                    if (this.f4589s != null) {
                        z = false;
                    }
                    com.fossil.blesdk.obfuscated.C3077v0 v0Var = new com.fossil.blesdk.obfuscated.C3077v0(context, actionBarContextView, callback, z);
                    if (callback.mo346a((androidx.appcompat.view.ActionMode) v0Var, v0Var.mo337c())) {
                        v0Var.mo343i();
                        this.f4588r.mo415a(v0Var);
                        this.f4587q = v0Var;
                        if (mo10223z()) {
                            this.f4588r.setAlpha(com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                            com.fossil.blesdk.obfuscated.C2110j9 a = com.fossil.blesdk.obfuscated.C1776f9.m6801a(this.f4588r);
                            a.mo12275a(1.0f);
                            this.f4591u = a;
                            this.f4591u.mo12278a((com.fossil.blesdk.obfuscated.C2190k9) new com.fossil.blesdk.obfuscated.C1675e0.C1683g());
                        } else {
                            this.f4588r.setAlpha(1.0f);
                            this.f4588r.setVisibility(0);
                            this.f4588r.sendAccessibilityEvent(32);
                            if (this.f4588r.getParent() instanceof android.view.View) {
                                com.fossil.blesdk.obfuscated.C1776f9.m6797D((android.view.View) this.f4588r.getParent());
                            }
                        }
                        if (this.f4589s != null) {
                            this.f4577g.getDecorView().post(this.f4590t);
                        }
                    } else {
                        this.f4587q = null;
                    }
                }
            }
            actionMode2 = this.f4587q;
            if (actionMode2 != null) {
                com.fossil.blesdk.obfuscated.C1591d0 d0Var2 = this.f4580j;
                if (d0Var2 != null) {
                    d0Var2.onSupportActionModeStarted(actionMode2);
                }
            }
            return this.f4587q;
        }
        actionMode = null;
        if (actionMode == null) {
        }
        actionMode2 = this.f4587q;
        if (actionMode2 != null) {
        }
        return this.f4587q;
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo10195c(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar) {
        android.content.Context context = this.f4576f;
        int i = nVar.f4617a;
        if ((i == 0 || i == 108) && this.f4584n != null) {
            android.util.TypedValue typedValue = new android.util.TypedValue();
            android.content.res.Resources.Theme theme = context.getTheme();
            theme.resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarTheme, typedValue, true);
            android.content.res.Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(com.fossil.blesdk.obfuscated.C2777r.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            if (theme2 != null) {
                com.fossil.blesdk.obfuscated.C2999u0 u0Var = new com.fossil.blesdk.obfuscated.C2999u0(context, 0);
                u0Var.getTheme().setTo(theme2);
                context = u0Var;
            }
        }
        com.fossil.blesdk.obfuscated.C1915h1 h1Var = new com.fossil.blesdk.obfuscated.C1915h1(context);
        h1Var.mo11460a((com.fossil.blesdk.obfuscated.C1915h1.C1916a) this);
        nVar.mo10251a(h1Var);
        return true;
    }

    @DexIgnore
    /* renamed from: e */
    public void mo10198e(int i) {
        com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(i, true);
        if (a.f4626j != null) {
            android.os.Bundle bundle = new android.os.Bundle();
            a.f4626j.mo11500e(bundle);
            if (bundle.size() > 0) {
                a.f4635s = bundle;
            }
            a.f4626j.mo11524s();
            a.f4626j.clear();
        }
        a.f4634r = true;
        a.f4633q = true;
        if ((i == 108 || i == 0) && this.f4584n != null) {
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a2 = mo10177a(0, false);
            if (a2 != null) {
                a2.f4629m = false;
                mo10193b(a2, (android.view.KeyEvent) null);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo292a(android.view.View view) {
        mo10214q();
        android.view.ViewGroup viewGroup = (android.view.ViewGroup) this.f4594x.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.f4578h.onContentChanged();
    }

    @DexIgnore
    /* renamed from: l */
    public int mo10206l(int i) {
        boolean z;
        boolean z2;
        boolean z3;
        androidx.appcompat.widget.ActionBarContextView actionBarContextView = this.f4588r;
        int i2 = 0;
        if (actionBarContextView == null || !(actionBarContextView.getLayoutParams() instanceof android.view.ViewGroup.MarginLayoutParams)) {
            z = false;
        } else {
            android.view.ViewGroup.MarginLayoutParams marginLayoutParams = (android.view.ViewGroup.MarginLayoutParams) this.f4588r.getLayoutParams();
            z = true;
            if (this.f4588r.isShown()) {
                if (this.f4573T == null) {
                    this.f4573T = new android.graphics.Rect();
                    this.f4574U = new android.graphics.Rect();
                }
                android.graphics.Rect rect = this.f4573T;
                android.graphics.Rect rect2 = this.f4574U;
                rect.set(0, i, 0, 0);
                com.fossil.blesdk.obfuscated.C1766f3.m6702a(this.f4594x, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    android.view.View view = this.f4596z;
                    if (view == null) {
                        this.f4596z = new android.view.View(this.f4576f);
                        this.f4596z.setBackgroundColor(this.f4576f.getResources().getColor(com.fossil.blesdk.obfuscated.C2934t.abc_input_method_navigation_guard));
                        this.f4594x.addView(this.f4596z, -1, new android.view.ViewGroup.LayoutParams(-1, i));
                    } else {
                        android.view.ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.f4596z.setLayoutParams(layoutParams);
                        }
                    }
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (this.f4596z == null) {
                    z = false;
                }
                if (!this.f4558E && z) {
                    i = 0;
                }
            } else {
                if (marginLayoutParams.topMargin != 0) {
                    marginLayoutParams.topMargin = 0;
                    z3 = true;
                } else {
                    z3 = false;
                }
                z = false;
            }
            if (z2) {
                this.f4588r.setLayoutParams(marginLayoutParams);
            }
        }
        android.view.View view2 = this.f4596z;
        if (view2 != null) {
            if (!z) {
                i2 = 8;
            }
            view2.setVisibility(i2);
        }
        return i;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo293a(android.view.View view, android.view.ViewGroup.LayoutParams layoutParams) {
        mo10214q();
        ((android.view.ViewGroup) this.f4594x.findViewById(16908290)).addView(view, layoutParams);
        this.f4578h.onContentChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo295a(java.lang.CharSequence charSequence) {
        this.f4583m = charSequence;
        com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
        if (i2Var != null) {
            i2Var.setWindowTitle(charSequence);
        } else if (mo10222y() != null) {
            mo10222y().mo182b(charSequence);
        } else {
            android.widget.TextView textView = this.f4595y;
            if (textView != null) {
                textView.setText(charSequence);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo536a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, android.view.MenuItem menuItem) {
        android.view.Window.Callback u = mo10218u();
        if (u == null || this.f4565L) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10178a((android.view.Menu) h1Var.mo11514m());
        if (a != null) {
            return u.onMenuItemSelected(a.f4617a, menuItem);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo535a(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        mo10183a(h1Var, true);
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.appcompat.view.ActionMode mo289a(androidx.appcompat.view.ActionMode.Callback callback) {
        if (callback != null) {
            androidx.appcompat.view.ActionMode actionMode = this.f4587q;
            if (actionMode != null) {
                actionMode.mo328a();
            }
            com.fossil.blesdk.obfuscated.C1675e0.C1686j jVar = new com.fossil.blesdk.obfuscated.C1675e0.C1686j(callback);
            androidx.appcompat.app.ActionBar d = mo304d();
            if (d != null) {
                this.f4587q = d.mo176a((androidx.appcompat.view.ActionMode.Callback) jVar);
                androidx.appcompat.view.ActionMode actionMode2 = this.f4587q;
                if (actionMode2 != null) {
                    com.fossil.blesdk.obfuscated.C1591d0 d0Var = this.f4580j;
                    if (d0Var != null) {
                        d0Var.onSupportActionModeStarted(actionMode2);
                    }
                }
            }
            if (this.f4587q == null) {
                this.f4587q = mo10189b((androidx.appcompat.view.ActionMode.Callback) jVar);
            }
            return this.f4587q;
        }
        throw new java.lang.IllegalArgumentException("ActionMode callback can not be null.");
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10185a(android.view.KeyEvent keyEvent) {
        android.view.Window.Callback callback = this.f4578h;
        boolean z = true;
        if ((callback instanceof com.fossil.blesdk.obfuscated.C2709q8.C2710a) || (callback instanceof com.fossil.blesdk.obfuscated.C1761f0)) {
            android.view.View decorView = this.f4577g.getDecorView();
            if (decorView != null && com.fossil.blesdk.obfuscated.C2709q8.m12638a(decorView, keyEvent)) {
                return true;
            }
        }
        if (keyEvent.getKeyCode() == 82 && this.f4578h.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z = false;
        }
        return z ? mo10184a(keyCode, keyEvent) : mo10197d(keyCode, keyEvent);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo10184a(int i, android.view.KeyEvent keyEvent) {
        boolean z = true;
        if (i == 4) {
            if ((keyEvent.getFlags() & 128) == 0) {
                z = false;
            }
            this.f4564K = z;
        } else if (i == 82) {
            mo10191b(0, keyEvent);
            return true;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View mo10176a(android.view.View view, java.lang.String str, android.content.Context context, android.util.AttributeSet attributeSet) {
        boolean z;
        boolean z2 = false;
        if (this.f4575V == null) {
            java.lang.String string = this.f4576f.obtainStyledAttributes(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme).getString(com.fossil.blesdk.obfuscated.C1368a0.AppCompatTheme_viewInflaterClass);
            if (string == null || androidx.appcompat.app.AppCompatViewInflater.class.getName().equals(string)) {
                this.f4575V = new androidx.appcompat.app.AppCompatViewInflater();
            } else {
                try {
                    this.f4575V = (androidx.appcompat.app.AppCompatViewInflater) java.lang.Class.forName(string).getDeclaredConstructor(new java.lang.Class[0]).newInstance(new java.lang.Object[0]);
                } catch (Throwable th) {
                    android.util.Log.i("AppCompatDelegate", "Failed to instantiate custom view inflater " + string + ". Falling back to default.", th);
                    this.f4575V = new androidx.appcompat.app.AppCompatViewInflater();
                }
            }
        }
        if (f4551W) {
            if (!(attributeSet instanceof org.xmlpull.v1.XmlPullParser)) {
                z2 = mo10186a((android.view.ViewParent) view);
            } else if (((org.xmlpull.v1.XmlPullParser) attributeSet).getDepth() > 1) {
                z2 = true;
            }
            z = z2;
        } else {
            z = false;
        }
        return this.f4575V.createView(view, str, context, attributeSet, z, f4551W, true, com.fossil.blesdk.obfuscated.C1701e3.m6278b());
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo10192b(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar) {
        nVar.mo10250a(mo10215r());
        nVar.f4623g = new com.fossil.blesdk.obfuscated.C1675e0.C1691m(nVar.f4628l);
        nVar.f4619c = 81;
        return true;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo10193b(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar, android.view.KeyEvent keyEvent) {
        if (this.f4565L) {
            return false;
        }
        if (nVar.f4629m) {
            return true;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar2 = this.f4563J;
        if (!(nVar2 == null || nVar2 == nVar)) {
            mo10182a(nVar2, false);
        }
        android.view.Window.Callback u = mo10218u();
        if (u != null) {
            nVar.f4625i = u.onCreatePanelView(nVar.f4617a);
        }
        int i = nVar.f4617a;
        boolean z = i == 0 || i == 108;
        if (z) {
            com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
            if (i2Var != null) {
                i2Var.mo445b();
            }
        }
        if (nVar.f4625i == null && (!z || !(mo10222y() instanceof com.fossil.blesdk.obfuscated.C1987i0))) {
            if (nVar.f4626j == null || nVar.f4634r) {
                if (nVar.f4626j == null && (!mo10195c(nVar) || nVar.f4626j == null)) {
                    return false;
                }
                if (z && this.f4584n != null) {
                    if (this.f4585o == null) {
                        this.f4585o = new com.fossil.blesdk.obfuscated.C1675e0.C1685i();
                    }
                    this.f4584n.mo441a(nVar.f4626j, this.f4585o);
                }
                nVar.f4626j.mo11524s();
                if (!u.onCreatePanelMenu(nVar.f4617a, nVar.f4626j)) {
                    nVar.mo10251a((com.fossil.blesdk.obfuscated.C1915h1) null);
                    if (z) {
                        com.fossil.blesdk.obfuscated.C1994i2 i2Var2 = this.f4584n;
                        if (i2Var2 != null) {
                            i2Var2.mo441a((android.view.Menu) null, this.f4585o);
                        }
                    }
                    return false;
                }
                nVar.f4634r = false;
            }
            nVar.f4626j.mo11524s();
            android.os.Bundle bundle = nVar.f4635s;
            if (bundle != null) {
                nVar.f4626j.mo11487c(bundle);
                nVar.f4635s = null;
            }
            if (!u.onPreparePanel(0, nVar.f4625i, nVar.f4626j)) {
                if (z) {
                    com.fossil.blesdk.obfuscated.C1994i2 i2Var3 = this.f4584n;
                    if (i2Var3 != null) {
                        i2Var3.mo441a((android.view.Menu) null, this.f4585o);
                    }
                }
                nVar.f4626j.mo11521r();
                return false;
            }
            nVar.f4632p = android.view.KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
            nVar.f4626j.setQwertyMode(nVar.f4632p);
            nVar.f4626j.mo11521r();
        }
        nVar.f4629m = true;
        nVar.f4630n = false;
        this.f4563J = nVar;
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo10186a(android.view.ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        android.view.View decorView = this.f4577g.getDecorView();
        while (viewParent != null) {
            if (viewParent == decorView || !(viewParent instanceof android.view.View) || com.fossil.blesdk.obfuscated.C1776f9.m6859y((android.view.View) viewParent)) {
                return false;
            }
            viewParent = viewParent.getParent();
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10181a(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar, android.view.KeyEvent keyEvent) {
        int i;
        if (!nVar.f4631o && !this.f4565L) {
            if (nVar.f4617a == 0) {
                if ((this.f4576f.getResources().getConfiguration().screenLayout & 15) == 4) {
                    return;
                }
            }
            android.view.Window.Callback u = mo10218u();
            if (u == null || u.onMenuOpened(nVar.f4617a, nVar.f4626j)) {
                android.view.WindowManager windowManager = (android.view.WindowManager) this.f4576f.getSystemService("window");
                if (windowManager != null && mo10193b(nVar, keyEvent)) {
                    if (nVar.f4623g == null || nVar.f4633q) {
                        android.view.ViewGroup viewGroup = nVar.f4623g;
                        if (viewGroup == null) {
                            if (!mo10192b(nVar) || nVar.f4623g == null) {
                                return;
                            }
                        } else if (nVar.f4633q && viewGroup.getChildCount() > 0) {
                            nVar.f4623g.removeAllViews();
                        }
                        if (mo10187a(nVar) && nVar.mo10252a()) {
                            android.view.ViewGroup.LayoutParams layoutParams = nVar.f4624h.getLayoutParams();
                            if (layoutParams == null) {
                                layoutParams = new android.view.ViewGroup.LayoutParams(-2, -2);
                            }
                            nVar.f4623g.setBackgroundResource(nVar.f4618b);
                            android.view.ViewParent parent = nVar.f4624h.getParent();
                            if (parent != null && (parent instanceof android.view.ViewGroup)) {
                                ((android.view.ViewGroup) parent).removeView(nVar.f4624h);
                            }
                            nVar.f4623g.addView(nVar.f4624h, layoutParams);
                            if (!nVar.f4624h.hasFocus()) {
                                nVar.f4624h.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else {
                        android.view.View view = nVar.f4625i;
                        if (view != null) {
                            android.view.ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
                            if (layoutParams2 != null && layoutParams2.width == -1) {
                                i = -1;
                                nVar.f4630n = false;
                                android.view.WindowManager.LayoutParams layoutParams3 = new android.view.WindowManager.LayoutParams(i, -2, nVar.f4620d, nVar.f4621e, 1002, 8519680, -3);
                                layoutParams3.gravity = nVar.f4619c;
                                layoutParams3.windowAnimations = nVar.f4622f;
                                windowManager.addView(nVar.f4623g, layoutParams3);
                                nVar.f4631o = true;
                                return;
                            }
                        }
                    }
                    i = -2;
                    nVar.f4630n = false;
                    android.view.WindowManager.LayoutParams layoutParams32 = new android.view.WindowManager.LayoutParams(i, -2, nVar.f4620d, nVar.f4621e, 1002, 8519680, -3);
                    layoutParams32.gravity = nVar.f4619c;
                    layoutParams32.windowAnimations = nVar.f4622f;
                    windowManager.addView(nVar.f4623g, layoutParams32);
                    nVar.f4631o = true;
                    return;
                }
                return;
            }
            mo10182a(nVar, true);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10190b(com.fossil.blesdk.obfuscated.C1915h1 h1Var) {
        if (!this.f4561H) {
            this.f4561H = true;
            this.f4584n.mo453g();
            android.view.Window.Callback u = mo10218u();
            if (u != null && !this.f4565L) {
                u.onPanelClosed(108, h1Var);
            }
            this.f4561H = false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10183a(com.fossil.blesdk.obfuscated.C1915h1 h1Var, boolean z) {
        com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
        if (i2Var == null || !i2Var.mo446c() || (android.view.ViewConfiguration.get(this.f4576f).hasPermanentMenuKey() && !this.f4584n.mo448d())) {
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(0, true);
            a.f4633q = true;
            mo10182a(a, false);
            mo10181a(a, (android.view.KeyEvent) null);
            return;
        }
        android.view.Window.Callback u = mo10218u();
        if (this.f4584n.mo442a() && z) {
            this.f4584n.mo450e();
            if (!this.f4565L) {
                u.onPanelClosed(108, mo10177a(0, true).f4626j);
            }
        } else if (u != null && !this.f4565L) {
            if (this.f4569P && (this.f4570Q & 1) != 0) {
                this.f4577g.getDecorView().removeCallbacks(this.f4571R);
                this.f4571R.run();
            }
            com.fossil.blesdk.obfuscated.C1675e0.C1692n a2 = mo10177a(0, true);
            com.fossil.blesdk.obfuscated.C1915h1 h1Var2 = a2.f4626j;
            if (h1Var2 != null && !a2.f4634r && u.onPreparePanel(0, a2.f4625i, h1Var2)) {
                u.onMenuOpened(108, a2.f4626j);
                this.f4584n.mo451f();
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo10191b(int i, android.view.KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() != 0) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n a = mo10177a(i, true);
        if (!a.f4631o) {
            return mo10193b(a, keyEvent);
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public final androidx.appcompat.app.ActionBarDrawerToggle$Delegate mo297b() {
        return new com.fossil.blesdk.obfuscated.C1675e0.C1684h(this);
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo10187a(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar) {
        android.view.View view = nVar.f4625i;
        if (view != null) {
            nVar.f4624h = view;
            return true;
        } else if (nVar.f4626j == null) {
            return false;
        } else {
            if (this.f4586p == null) {
                this.f4586p = new com.fossil.blesdk.obfuscated.C1675e0.C1693o();
            }
            nVar.f4624h = (android.view.View) nVar.mo10249a((com.fossil.blesdk.obfuscated.C2618p1.C2619a) this.f4586p);
            if (nVar.f4624h != null) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10182a(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar, boolean z) {
        if (z && nVar.f4617a == 0) {
            com.fossil.blesdk.obfuscated.C1994i2 i2Var = this.f4584n;
            if (i2Var != null && i2Var.mo442a()) {
                mo10190b(nVar.f4626j);
                return;
            }
        }
        android.view.WindowManager windowManager = (android.view.WindowManager) this.f4576f.getSystemService("window");
        if (windowManager != null && nVar.f4631o) {
            android.view.ViewGroup viewGroup = nVar.f4623g;
            if (viewGroup != null) {
                windowManager.removeView(viewGroup);
                if (z) {
                    mo10179a(nVar.f4617a, nVar, (android.view.Menu) null);
                }
            }
        }
        nVar.f4629m = false;
        nVar.f4630n = false;
        nVar.f4631o = false;
        nVar.f4624h = null;
        nVar.f4633q = true;
        if (this.f4563J == nVar) {
            this.f4563J = null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10179a(int i, com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar, android.view.Menu menu) {
        if (menu == null) {
            if (nVar == null && i >= 0) {
                com.fossil.blesdk.obfuscated.C1675e0.C1692n[] nVarArr = this.f4562I;
                if (i < nVarArr.length) {
                    nVar = nVarArr[i];
                }
            }
            if (nVar != null) {
                menu = nVar.f4626j;
            }
        }
        if ((nVar == null || nVar.f4631o) && !this.f4565L) {
            this.f4578h.onPanelClosed(i, menu);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1675e0.C1692n mo10178a(android.view.Menu menu) {
        com.fossil.blesdk.obfuscated.C1675e0.C1692n[] nVarArr = this.f4562I;
        int length = nVarArr != null ? nVarArr.length : 0;
        for (int i = 0; i < length; i++) {
            com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar = nVarArr[i];
            if (nVar != null && nVar.f4626j == menu) {
                return nVar;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1675e0.C1692n mo10177a(int i, boolean z) {
        com.fossil.blesdk.obfuscated.C1675e0.C1692n[] nVarArr = this.f4562I;
        if (nVarArr == null || nVarArr.length <= i) {
            com.fossil.blesdk.obfuscated.C1675e0.C1692n[] nVarArr2 = new com.fossil.blesdk.obfuscated.C1675e0.C1692n[(i + 1)];
            if (nVarArr != null) {
                java.lang.System.arraycopy(nVarArr, 0, nVarArr2, 0, nVarArr.length);
            }
            this.f4562I = nVarArr2;
            nVarArr = nVarArr2;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar = nVarArr[i];
        if (nVar != null) {
            return nVar;
        }
        com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar2 = new com.fossil.blesdk.obfuscated.C1675e0.C1692n(i);
        nVarArr[i] = nVar2;
        return nVar2;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo10188a(com.fossil.blesdk.obfuscated.C1675e0.C1692n nVar, int i, android.view.KeyEvent keyEvent, int i2) {
        boolean z = false;
        if (keyEvent.isSystem()) {
            return false;
        }
        if (nVar.f4629m || mo10193b(nVar, keyEvent)) {
            com.fossil.blesdk.obfuscated.C1915h1 h1Var = nVar.f4626j;
            if (h1Var != null) {
                z = h1Var.performShortcut(i, keyEvent, i2);
            }
        }
        if (z && (i2 & 1) == 0 && this.f4584n == null) {
            mo10182a(nVar, true);
        }
        return z;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo296a() {
        int s = mo10216s();
        int g = mo10201g(s);
        boolean k = g != -1 ? mo10205k(g) : false;
        if (s == 0) {
            mo10213p();
            this.f4568O.mo10243d();
        }
        this.f4567N = true;
        return k;
    }
}
