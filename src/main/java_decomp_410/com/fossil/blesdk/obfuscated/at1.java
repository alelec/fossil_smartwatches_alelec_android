package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class at1 extends p0 {
    @DexIgnore
    public static /* final */ double u; // = Math.cos(Math.toRadians(45.0d));
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public /* final */ RectF h;
    @DexIgnore
    public float i;
    @DexIgnore
    public Path j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ int q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public float s;
    @DexIgnore
    public boolean t; // = false;

    @DexIgnore
    public at1(Context context, Drawable drawable, float f2, float f3, float f4) {
        super(drawable);
        this.o = k6.a(context, uq1.design_fab_shadow_start_color);
        this.p = k6.a(context, uq1.design_fab_shadow_mid_color);
        this.q = k6.a(context, uq1.design_fab_shadow_end_color);
        this.f = new Paint(5);
        this.f.setStyle(Paint.Style.FILL);
        this.i = (float) Math.round(f2);
        this.h = new RectF();
        this.g = new Paint(this.f);
        this.g.setAntiAlias(false);
        a(f3, f4);
    }

    @DexIgnore
    public static int c(float f2) {
        int round = Math.round(f2);
        return round % 2 == 1 ? round - 1 : round;
    }

    @DexIgnore
    public void a(boolean z) {
        this.r = z;
        invalidateSelf();
    }

    @DexIgnore
    public void b(float f2) {
        a(f2, this.k);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (this.n) {
            a(getBounds());
            this.n = false;
        }
        a(canvas);
        super.draw(canvas);
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) b(this.k, this.i, this.r));
        int ceil2 = (int) Math.ceil((double) a(this.k, this.i, this.r));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.n = true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        super.setAlpha(i2);
        this.f.setAlpha(i2);
        this.g.setAlpha(i2);
    }

    @DexIgnore
    public static float b(float f2, float f3, boolean z) {
        return z ? (float) (((double) (f2 * 1.5f)) + ((1.0d - u) * ((double) f3))) : f2 * 1.5f;
    }

    @DexIgnore
    public void a(float f2, float f3) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f3 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new IllegalArgumentException("invalid shadow size");
        }
        float c = (float) c(f2);
        float c2 = (float) c(f3);
        if (c > c2) {
            if (!this.t) {
                this.t = true;
            }
            c = c2;
        }
        if (this.m != c || this.k != c2) {
            this.m = c;
            this.k = c2;
            this.l = (float) Math.round(c * 1.5f);
            this.n = true;
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void b() {
        float f2 = this.i;
        RectF rectF = new RectF(-f2, -f2, f2, f2);
        RectF rectF2 = new RectF(rectF);
        float f3 = this.l;
        rectF2.inset(-f3, -f3);
        Path path = this.j;
        if (path == null) {
            this.j = new Path();
        } else {
            path.reset();
        }
        this.j.setFillType(Path.FillType.EVEN_ODD);
        this.j.moveTo(-this.i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.j.rLineTo(-this.l, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.j.arcTo(rectF2, 180.0f, 90.0f, false);
        this.j.arcTo(rectF, 270.0f, -90.0f, false);
        this.j.close();
        float f4 = -rectF2.top;
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f5 = this.i / f4;
            Paint paint = this.f;
            RadialGradient radialGradient = r8;
            RadialGradient radialGradient2 = new RadialGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4, new int[]{0, this.o, this.p, this.q}, new float[]{0.0f, f5, ((1.0f - f5) / 2.0f) + f5, 1.0f}, Shader.TileMode.CLAMP);
            paint.setShader(radialGradient);
        }
        this.g.setShader(new LinearGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, rectF.top, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, rectF2.top, new int[]{this.o, this.p, this.q}, new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
        this.g.setAntiAlias(false);
    }

    @DexIgnore
    public float c() {
        return this.m;
    }

    @DexIgnore
    public static float a(float f2, float f3, boolean z) {
        return z ? (float) (((double) f2) + ((1.0d - u) * ((double) f3))) : f2;
    }

    @DexIgnore
    public final void a(float f2) {
        if (this.s != f2) {
            this.s = f2;
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        float f2;
        int i2;
        int i3;
        float f3;
        float f4;
        float f5;
        Canvas canvas2 = canvas;
        int save = canvas.save();
        canvas2.rotate(this.s, this.h.centerX(), this.h.centerY());
        float f6 = this.i;
        float f7 = (-f6) - this.l;
        float f8 = f6 * 2.0f;
        boolean z = this.h.width() - f8 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = this.h.height() - f8 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f9 = this.m;
        float f10 = f6 / ((f9 - (0.5f * f9)) + f6);
        float f11 = f6 / ((f9 - (0.25f * f9)) + f6);
        float f12 = f6 / ((f9 - (f9 * 1.0f)) + f6);
        int save2 = canvas.save();
        RectF rectF = this.h;
        canvas2.translate(rectF.left + f6, rectF.top + f6);
        canvas2.scale(f10, f11);
        canvas2.drawPath(this.j, this.f);
        if (z) {
            canvas2.scale(1.0f / f10, 1.0f);
            i3 = save2;
            f2 = f12;
            i2 = save;
            f3 = f11;
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7, this.h.width() - f8, -this.i, this.g);
        } else {
            i3 = save2;
            f2 = f12;
            i2 = save;
            f3 = f11;
        }
        canvas2.restoreToCount(i3);
        int save3 = canvas.save();
        RectF rectF2 = this.h;
        canvas2.translate(rectF2.right - f6, rectF2.bottom - f6);
        float f13 = f2;
        canvas2.scale(f10, f13);
        canvas2.rotate(180.0f);
        canvas2.drawPath(this.j, this.f);
        if (z) {
            canvas2.scale(1.0f / f10, 1.0f);
            f4 = f3;
            f5 = f13;
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7, this.h.width() - f8, (-this.i) + this.l, this.g);
        } else {
            f4 = f3;
            f5 = f13;
        }
        canvas2.restoreToCount(save3);
        int save4 = canvas.save();
        RectF rectF3 = this.h;
        canvas2.translate(rectF3.left + f6, rectF3.bottom - f6);
        canvas2.scale(f10, f5);
        canvas2.rotate(270.0f);
        canvas2.drawPath(this.j, this.f);
        if (z2) {
            canvas2.scale(1.0f / f5, 1.0f);
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7, this.h.height() - f8, -this.i, this.g);
        }
        canvas2.restoreToCount(save4);
        int save5 = canvas.save();
        RectF rectF4 = this.h;
        canvas2.translate(rectF4.right - f6, rectF4.top + f6);
        float f14 = f4;
        canvas2.scale(f10, f14);
        canvas2.rotate(90.0f);
        canvas2.drawPath(this.j, this.f);
        if (z2) {
            canvas2.scale(1.0f / f14, 1.0f);
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f7, this.h.height() - f8, -this.i, this.g);
        }
        canvas2.restoreToCount(save5);
        canvas2.restoreToCount(i2);
    }

    @DexIgnore
    public final void a(Rect rect) {
        float f2 = this.k;
        float f3 = 1.5f * f2;
        this.h.set(((float) rect.left) + f2, ((float) rect.top) + f3, ((float) rect.right) - f2, ((float) rect.bottom) - f3);
        Drawable a = a();
        RectF rectF = this.h;
        a.setBounds((int) rectF.left, (int) rectF.top, (int) rectF.right, (int) rectF.bottom);
        b();
    }
}
