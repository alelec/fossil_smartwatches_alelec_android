package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.a4 */
public class C1381a4 implements com.fossil.blesdk.obfuscated.C1595d4 {
    @DexIgnore
    /* renamed from: a */
    public void mo8511a() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8513a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, android.content.Context context, android.content.res.ColorStateList colorStateList, float f, float f2, float f3) {
        c4Var.mo1211a(new com.fossil.blesdk.obfuscated.C1702e4(colorStateList, f));
        android.view.View d = c4Var.mo1215d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        mo8518c(c4Var, f3);
    }

    @DexIgnore
    /* renamed from: b */
    public float mo8515b(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo8525j(c4Var).mo10312c();
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8518c(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f) {
        mo8525j(c4Var).mo10307a(f, c4Var.mo1213b(), c4Var.mo1212a());
        mo8521f(c4Var);
    }

    @DexIgnore
    /* renamed from: d */
    public float mo8519d(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo8525j(c4Var).mo10310b();
    }

    @DexIgnore
    /* renamed from: e */
    public android.content.res.ColorStateList mo8520e(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo8525j(c4Var).mo10304a();
    }

    @DexIgnore
    /* renamed from: f */
    public void mo8521f(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        if (!c4Var.mo1213b()) {
            c4Var.mo1210a(0, 0, 0, 0);
            return;
        }
        float d = mo8519d(c4Var);
        float b = mo8515b(c4Var);
        int ceil = (int) java.lang.Math.ceil((double) com.fossil.blesdk.obfuscated.C1768f4.m6708a(d, b, c4Var.mo1212a()));
        int ceil2 = (int) java.lang.Math.ceil((double) com.fossil.blesdk.obfuscated.C1768f4.m6709b(d, b, c4Var.mo1212a()));
        c4Var.mo1210a(ceil, ceil2, ceil, ceil2);
    }

    @DexIgnore
    /* renamed from: g */
    public float mo8522g(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo8515b(c4Var) * 2.0f;
    }

    @DexIgnore
    /* renamed from: h */
    public float mo8523h(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return mo8515b(c4Var) * 2.0f;
    }

    @DexIgnore
    /* renamed from: i */
    public void mo8524i(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        mo8518c(c4Var, mo8519d(c4Var));
    }

    @DexIgnore
    /* renamed from: j */
    public final com.fossil.blesdk.obfuscated.C1702e4 mo8525j(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return (com.fossil.blesdk.obfuscated.C1702e4) c4Var.mo1214c();
    }

    @DexIgnore
    /* renamed from: b */
    public void mo8516b(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f) {
        c4Var.mo1215d().setElevation(f);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo8517c(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        mo8518c(c4Var, mo8519d(c4Var));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8512a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, float f) {
        mo8525j(c4Var).mo10306a(f);
    }

    @DexIgnore
    /* renamed from: a */
    public float mo8510a(com.fossil.blesdk.obfuscated.C1535c4 c4Var) {
        return c4Var.mo1215d().getElevation();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo8514a(com.fossil.blesdk.obfuscated.C1535c4 c4Var, android.content.res.ColorStateList colorStateList) {
        mo8525j(c4Var).mo10311b(colorStateList);
    }
}
