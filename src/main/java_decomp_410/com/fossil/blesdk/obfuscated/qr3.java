package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qr3 implements Factory<VerifySecretKeyUseCase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<kr3> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;

    @DexIgnore
    public qr3(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<kr3> provider3, Provider<PortfolioApp> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static qr3 a(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<kr3> provider3, Provider<PortfolioApp> provider4) {
        return new qr3(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static VerifySecretKeyUseCase b(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<kr3> provider3, Provider<PortfolioApp> provider4) {
        return new VerifySecretKeyUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get());
    }

    @DexIgnore
    public VerifySecretKeyUseCase get() {
        return b(this.a, this.b, this.c, this.d);
    }
}
