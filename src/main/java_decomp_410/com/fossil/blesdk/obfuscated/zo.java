package com.fossil.blesdk.obfuscated;

import com.bumptech.glide.load.resource.bitmap.RecyclableBufferedInputStream;
import com.fossil.blesdk.obfuscated.to;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zo implements to<InputStream> {
    @DexIgnore
    public /* final */ RecyclableBufferedInputStream a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements to.a<InputStream> {
        @DexIgnore
        public /* final */ gq a;

        @DexIgnore
        public a(gq gqVar) {
            this.a = gqVar;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public to<InputStream> a(InputStream inputStream) {
            return new zo(inputStream, this.a);
        }
    }

    @DexIgnore
    public zo(InputStream inputStream, gq gqVar) {
        this.a = new RecyclableBufferedInputStream(inputStream, gqVar);
        this.a.mark(5242880);
    }

    @DexIgnore
    public void a() {
        this.a.z();
    }

    @DexIgnore
    public InputStream b() throws IOException {
        this.a.reset();
        return this.a;
    }
}
