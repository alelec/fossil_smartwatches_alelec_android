package com.fossil.blesdk.obfuscated;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class se4<T> extends te4<T> implements Iterator<T>, yb4<qa4>, rd4 {
    @DexIgnore
    public int e;
    @DexIgnore
    public T f;
    @DexIgnore
    public Iterator<? extends T> g;
    @DexIgnore
    public yb4<? super qa4> h;

    @DexIgnore
    public final void a(yb4<? super qa4> yb4) {
        this.h = yb4;
    }

    @DexIgnore
    public final T b() {
        if (hasNext()) {
            return next();
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public CoroutineContext getContext() {
        return EmptyCoroutineContext.INSTANCE;
    }

    @DexIgnore
    public boolean hasNext() {
        while (true) {
            int i = this.e;
            if (i != 0) {
                if (i == 1) {
                    Iterator<? extends T> it = this.g;
                    if (it == null) {
                        kd4.a();
                        throw null;
                    } else if (it.hasNext()) {
                        this.e = 2;
                        return true;
                    } else {
                        this.g = null;
                    }
                } else if (i == 2 || i == 3) {
                    return true;
                } else {
                    if (i == 4) {
                        return false;
                    }
                    throw a();
                }
            }
            this.e = 5;
            yb4<? super qa4> yb4 = this.h;
            if (yb4 != null) {
                this.h = null;
                qa4 qa4 = qa4.a;
                Result.a aVar = Result.Companion;
                yb4.resumeWith(Result.m3constructorimpl(qa4));
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public T next() {
        int i = this.e;
        if (i == 0 || i == 1) {
            return b();
        }
        if (i == 2) {
            this.e = 1;
            Iterator<? extends T> it = this.g;
            if (it != null) {
                return it.next();
            }
            kd4.a();
            throw null;
        } else if (i == 3) {
            this.e = 0;
            T t = this.f;
            this.f = null;
            return t;
        } else {
            throw a();
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        na4.a(obj);
        this.e = 4;
    }

    @DexIgnore
    public final Throwable a() {
        int i = this.e;
        if (i == 4) {
            return new NoSuchElementException();
        }
        if (i == 5) {
            return new IllegalStateException("Iterator has failed.");
        }
        return new IllegalStateException("Unexpected state of the iterator: " + this.e);
    }

    @DexIgnore
    public Object a(T t, yb4<? super qa4> yb4) {
        this.f = t;
        this.e = 3;
        a(yb4);
        Object a = cc4.a();
        if (a == cc4.a()) {
            ic4.c(yb4);
        }
        return a;
    }
}
