package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.r84;
import io.reactivex.internal.disposables.EmptyDisposable;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ga4 extends r84 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Runnable {
        @DexIgnore
        public /* final */ Runnable e;
        @DexIgnore
        public /* final */ c f;
        @DexIgnore
        public /* final */ long g;

        @DexIgnore
        public a(Runnable runnable, c cVar, long j) {
            this.e = runnable;
            this.f = cVar;
            this.g = j;
        }

        @DexIgnore
        public void run() {
            if (!this.f.h) {
                long a = this.f.a(TimeUnit.MILLISECONDS);
                long j = this.g;
                if (j > a) {
                    long j2 = j - a;
                    if (j2 > 0) {
                        try {
                            Thread.sleep(j2);
                        } catch (InterruptedException e2) {
                            Thread.currentThread().interrupt();
                            ia4.b(e2);
                            return;
                        }
                    }
                }
                if (!this.f.h) {
                    this.e.run();
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Comparable<b> {
        @DexIgnore
        public /* final */ Runnable e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ int g;
        @DexIgnore
        public volatile boolean h;

        @DexIgnore
        public b(Runnable runnable, Long l, int i) {
            this.e = runnable;
            this.f = l.longValue();
            this.g = i;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(b bVar) {
            int a = k94.a(this.f, bVar.f);
            return a == 0 ? k94.a(this.g, bVar.g) : a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends r84.b implements y84 {
        @DexIgnore
        public /* final */ PriorityBlockingQueue<b> e; // = new PriorityBlockingQueue<>();
        @DexIgnore
        public /* final */ AtomicInteger f; // = new AtomicInteger();
        @DexIgnore
        public /* final */ AtomicInteger g; // = new AtomicInteger();
        @DexIgnore
        public volatile boolean h;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a implements Runnable {
            @DexIgnore
            public /* final */ b e;

            @DexIgnore
            public a(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public void run() {
                b bVar = this.e;
                bVar.h = true;
                c.this.e.remove(bVar);
            }
        }

        @DexIgnore
        public y84 a(Runnable runnable) {
            return a(runnable, a(TimeUnit.MILLISECONDS));
        }

        @DexIgnore
        public void dispose() {
            this.h = true;
        }

        @DexIgnore
        public y84 a(Runnable runnable, long j, TimeUnit timeUnit) {
            long a2 = a(TimeUnit.MILLISECONDS) + timeUnit.toMillis(j);
            return a(new a(runnable, this, a2), a2);
        }

        @DexIgnore
        public y84 a(Runnable runnable, long j) {
            if (this.h) {
                return EmptyDisposable.INSTANCE;
            }
            b bVar = new b(runnable, Long.valueOf(j), this.g.incrementAndGet());
            this.e.add(bVar);
            if (this.f.getAndIncrement() != 0) {
                return z84.a(new a(bVar));
            }
            int i = 1;
            while (!this.h) {
                b poll = this.e.poll();
                if (poll == null) {
                    i = this.f.addAndGet(-i);
                    if (i == 0) {
                        return EmptyDisposable.INSTANCE;
                    }
                } else if (!poll.h) {
                    poll.e.run();
                }
            }
            this.e.clear();
            return EmptyDisposable.INSTANCE;
        }
    }

    /*
    static {
        new ga4();
    }
    */

    @DexIgnore
    public r84.b a() {
        return new c();
    }

    @DexIgnore
    public y84 a(Runnable runnable) {
        runnable.run();
        return EmptyDisposable.INSTANCE;
    }

    @DexIgnore
    public y84 a(Runnable runnable, long j, TimeUnit timeUnit) {
        try {
            timeUnit.sleep(j);
            runnable.run();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            ia4.b(e);
        }
        return EmptyDisposable.INSTANCE;
    }
}
