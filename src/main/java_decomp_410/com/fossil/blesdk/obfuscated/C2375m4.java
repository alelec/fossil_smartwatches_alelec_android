package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m4 */
public class C2375m4 {

    @DexIgnore
    /* renamed from: a */
    public int f7388a; // = 0;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2459n4 f7389b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2526o4 f7390c;

    @DexIgnore
    /* renamed from: d */
    public int f7391d; // = 8;

    @DexIgnore
    /* renamed from: e */
    public androidx.constraintlayout.solver.SolverVariable f7392e; // = null;

    @DexIgnore
    /* renamed from: f */
    public int[] f7393f;

    @DexIgnore
    /* renamed from: g */
    public int[] f7394g;

    @DexIgnore
    /* renamed from: h */
    public float[] f7395h;

    @DexIgnore
    /* renamed from: i */
    public int f7396i;

    @DexIgnore
    /* renamed from: j */
    public int f7397j;

    @DexIgnore
    /* renamed from: k */
    public boolean f7398k;

    @DexIgnore
    public C2375m4(com.fossil.blesdk.obfuscated.C2459n4 n4Var, com.fossil.blesdk.obfuscated.C2526o4 o4Var) {
        int i = this.f7391d;
        this.f7393f = new int[i];
        this.f7394g = new int[i];
        this.f7395h = new float[i];
        this.f7396i = -1;
        this.f7397j = -1;
        this.f7398k = false;
        this.f7389b = n4Var;
        this.f7390c = o4Var;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13509a(androidx.constraintlayout.solver.SolverVariable solverVariable, float f) {
        if (f == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            mo13503a(solverVariable, true);
            return;
        }
        int i = this.f7396i;
        if (i == -1) {
            this.f7396i = 0;
            float[] fArr = this.f7395h;
            int i2 = this.f7396i;
            fArr[i2] = f;
            this.f7393f[i2] = solverVariable.f639b;
            this.f7394g[i2] = -1;
            solverVariable.f647j++;
            solverVariable.mo1255a(this.f7389b);
            this.f7388a++;
            if (!this.f7398k) {
                this.f7397j++;
                int i3 = this.f7397j;
                int[] iArr = this.f7393f;
                if (i3 >= iArr.length) {
                    this.f7398k = true;
                    this.f7397j = iArr.length - 1;
                    return;
                }
                return;
            }
            return;
        }
        int i4 = 0;
        int i5 = -1;
        while (i != -1 && i4 < this.f7388a) {
            int[] iArr2 = this.f7393f;
            int i6 = iArr2[i];
            int i7 = solverVariable.f639b;
            if (i6 == i7) {
                this.f7395h[i] = f;
                return;
            }
            if (iArr2[i] < i7) {
                i5 = i;
            }
            i = this.f7394g[i];
            i4++;
        }
        int i8 = this.f7397j;
        int i9 = i8 + 1;
        if (this.f7398k) {
            int[] iArr3 = this.f7393f;
            if (iArr3[i8] != -1) {
                i8 = iArr3.length;
            }
        } else {
            i8 = i9;
        }
        int[] iArr4 = this.f7393f;
        if (i8 >= iArr4.length && this.f7388a < iArr4.length) {
            int i10 = 0;
            while (true) {
                int[] iArr5 = this.f7393f;
                if (i10 >= iArr5.length) {
                    break;
                } else if (iArr5[i10] == -1) {
                    i8 = i10;
                    break;
                } else {
                    i10++;
                }
            }
        }
        int[] iArr6 = this.f7393f;
        if (i8 >= iArr6.length) {
            i8 = iArr6.length;
            this.f7391d *= 2;
            this.f7398k = false;
            this.f7397j = i8 - 1;
            this.f7395h = java.util.Arrays.copyOf(this.f7395h, this.f7391d);
            this.f7393f = java.util.Arrays.copyOf(this.f7393f, this.f7391d);
            this.f7394g = java.util.Arrays.copyOf(this.f7394g, this.f7391d);
        }
        this.f7393f[i8] = solverVariable.f639b;
        this.f7395h[i8] = f;
        if (i5 != -1) {
            int[] iArr7 = this.f7394g;
            iArr7[i8] = iArr7[i5];
            iArr7[i5] = i8;
        } else {
            this.f7394g[i8] = this.f7396i;
            this.f7396i = i8;
        }
        solverVariable.f647j++;
        solverVariable.mo1255a(this.f7389b);
        this.f7388a++;
        if (!this.f7398k) {
            this.f7397j++;
        }
        if (this.f7388a >= this.f7393f.length) {
            this.f7398k = true;
        }
        int i11 = this.f7397j;
        int[] iArr8 = this.f7393f;
        if (i11 >= iArr8.length) {
            this.f7398k = true;
            this.f7397j = iArr8.length - 1;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo13517b() {
        int i = this.f7396i;
        int i2 = 0;
        while (i != -1 && i2 < this.f7388a) {
            float[] fArr = this.f7395h;
            fArr[i] = fArr[i] * -1.0f;
            i = this.f7394g[i];
            i2++;
        }
    }

    @DexIgnore
    public java.lang.String toString() {
        int i = this.f7396i;
        java.lang.String str = "";
        int i2 = 0;
        while (i != -1 && i2 < this.f7388a) {
            java.lang.String str2 = str + " -> ";
            java.lang.String str3 = str2 + this.f7395h[i] + " : ";
            str = str3 + this.f7390c.f7983c[this.f7393f[i]];
            i = this.f7394g[i];
            i2++;
        }
        return str;
    }

    @DexIgnore
    /* renamed from: b */
    public final float mo13515b(int i) {
        int i2 = this.f7396i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.f7388a) {
            if (i3 == i) {
                return this.f7395h[i2];
            }
            i2 = this.f7394g[i2];
            i3++;
        }
        return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: b */
    public final float mo13516b(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        int i = this.f7396i;
        int i2 = 0;
        while (i != -1 && i2 < this.f7388a) {
            if (this.f7393f[i] == solverVariable.f639b) {
                return this.f7395h[i];
            }
            i = this.f7394g[i];
            i2++;
        }
        return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13510a(androidx.constraintlayout.solver.SolverVariable solverVariable, float f, boolean z) {
        if (f != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            int i = this.f7396i;
            if (i == -1) {
                this.f7396i = 0;
                float[] fArr = this.f7395h;
                int i2 = this.f7396i;
                fArr[i2] = f;
                this.f7393f[i2] = solverVariable.f639b;
                this.f7394g[i2] = -1;
                solverVariable.f647j++;
                solverVariable.mo1255a(this.f7389b);
                this.f7388a++;
                if (!this.f7398k) {
                    this.f7397j++;
                    int i3 = this.f7397j;
                    int[] iArr = this.f7393f;
                    if (i3 >= iArr.length) {
                        this.f7398k = true;
                        this.f7397j = iArr.length - 1;
                        return;
                    }
                    return;
                }
                return;
            }
            int i4 = 0;
            int i5 = -1;
            while (i != -1 && i4 < this.f7388a) {
                int[] iArr2 = this.f7393f;
                int i6 = iArr2[i];
                int i7 = solverVariable.f639b;
                if (i6 == i7) {
                    float[] fArr2 = this.f7395h;
                    fArr2[i] = fArr2[i] + f;
                    if (fArr2[i] == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (i == this.f7396i) {
                            this.f7396i = this.f7394g[i];
                        } else {
                            int[] iArr3 = this.f7394g;
                            iArr3[i5] = iArr3[i];
                        }
                        if (z) {
                            solverVariable.mo1256b(this.f7389b);
                        }
                        if (this.f7398k) {
                            this.f7397j = i;
                        }
                        solverVariable.f647j--;
                        this.f7388a--;
                        return;
                    }
                    return;
                }
                if (iArr2[i] < i7) {
                    i5 = i;
                }
                i = this.f7394g[i];
                i4++;
            }
            int i8 = this.f7397j;
            int i9 = i8 + 1;
            if (this.f7398k) {
                int[] iArr4 = this.f7393f;
                if (iArr4[i8] != -1) {
                    i8 = iArr4.length;
                }
            } else {
                i8 = i9;
            }
            int[] iArr5 = this.f7393f;
            if (i8 >= iArr5.length && this.f7388a < iArr5.length) {
                int i10 = 0;
                while (true) {
                    int[] iArr6 = this.f7393f;
                    if (i10 >= iArr6.length) {
                        break;
                    } else if (iArr6[i10] == -1) {
                        i8 = i10;
                        break;
                    } else {
                        i10++;
                    }
                }
            }
            int[] iArr7 = this.f7393f;
            if (i8 >= iArr7.length) {
                i8 = iArr7.length;
                this.f7391d *= 2;
                this.f7398k = false;
                this.f7397j = i8 - 1;
                this.f7395h = java.util.Arrays.copyOf(this.f7395h, this.f7391d);
                this.f7393f = java.util.Arrays.copyOf(this.f7393f, this.f7391d);
                this.f7394g = java.util.Arrays.copyOf(this.f7394g, this.f7391d);
            }
            this.f7393f[i8] = solverVariable.f639b;
            this.f7395h[i8] = f;
            if (i5 != -1) {
                int[] iArr8 = this.f7394g;
                iArr8[i8] = iArr8[i5];
                iArr8[i5] = i8;
            } else {
                this.f7394g[i8] = this.f7396i;
                this.f7396i = i8;
            }
            solverVariable.f647j++;
            solverVariable.mo1255a(this.f7389b);
            this.f7388a++;
            if (!this.f7398k) {
                this.f7397j++;
            }
            int i11 = this.f7397j;
            int[] iArr9 = this.f7393f;
            if (i11 >= iArr9.length) {
                this.f7398k = true;
                this.f7397j = iArr9.length - 1;
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final float mo13503a(androidx.constraintlayout.solver.SolverVariable solverVariable, boolean z) {
        if (this.f7392e == solverVariable) {
            this.f7392e = null;
        }
        int i = this.f7396i;
        if (i == -1) {
            return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i2 = 0;
        int i3 = -1;
        while (i != -1 && i2 < this.f7388a) {
            if (this.f7393f[i] == solverVariable.f639b) {
                if (i == this.f7396i) {
                    this.f7396i = this.f7394g[i];
                } else {
                    int[] iArr = this.f7394g;
                    iArr[i3] = iArr[i];
                }
                if (z) {
                    solverVariable.mo1256b(this.f7389b);
                }
                solverVariable.f647j--;
                this.f7388a--;
                this.f7393f[i] = -1;
                if (this.f7398k) {
                    this.f7397j = i;
                }
                return this.f7395h[i];
            }
            i2++;
            i3 = i;
            i = this.f7394g[i];
        }
        return com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13507a() {
        int i = this.f7396i;
        int i2 = 0;
        while (i != -1 && i2 < this.f7388a) {
            androidx.constraintlayout.solver.SolverVariable solverVariable = this.f7390c.f7983c[this.f7393f[i]];
            if (solverVariable != null) {
                solverVariable.mo1256b(this.f7389b);
            }
            i = this.f7394g[i];
            i2++;
        }
        this.f7396i = -1;
        this.f7397j = -1;
        this.f7398k = false;
        this.f7388a = 0;
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo13513a(androidx.constraintlayout.solver.SolverVariable solverVariable) {
        int i = this.f7396i;
        if (i == -1) {
            return false;
        }
        int i2 = 0;
        while (i != -1 && i2 < this.f7388a) {
            if (this.f7393f[i] == solverVariable.f639b) {
                return true;
            }
            i = this.f7394g[i];
            i2++;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13508a(float f) {
        int i = this.f7396i;
        int i2 = 0;
        while (i != -1 && i2 < this.f7388a) {
            float[] fArr = this.f7395h;
            fArr[i] = fArr[i] / f;
            i = this.f7394g[i];
            i2++;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final boolean mo13514a(androidx.constraintlayout.solver.SolverVariable solverVariable, com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        return solverVariable.f647j <= 1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x008f A[SYNTHETIC] */
    /* renamed from: a */
    public androidx.constraintlayout.solver.SolverVariable mo13505a(com.fossil.blesdk.obfuscated.C2703q4 q4Var) {
        boolean a;
        boolean a2;
        int i = this.f7396i;
        androidx.constraintlayout.solver.SolverVariable solverVariable = null;
        int i2 = 0;
        androidx.constraintlayout.solver.SolverVariable solverVariable2 = null;
        float f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        float f2 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = false;
        while (i != -1 && i2 < this.f7388a) {
            float[] fArr = this.f7395h;
            float f3 = fArr[i];
            androidx.constraintlayout.solver.SolverVariable solverVariable3 = this.f7390c.f7983c[this.f7393f[i]];
            if (f3 < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (f3 > -0.001f) {
                    fArr[i] = 0.0f;
                    solverVariable3.mo1256b(this.f7389b);
                }
                if (f3 != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    if (solverVariable3.f644g == androidx.constraintlayout.solver.SolverVariable.Type.UNRESTRICTED) {
                        if (solverVariable2 == null) {
                            a2 = mo13514a(solverVariable3, q4Var);
                        } else if (f > f3) {
                            a2 = mo13514a(solverVariable3, q4Var);
                        } else if (!z && mo13514a(solverVariable3, q4Var)) {
                            f = f3;
                            solverVariable2 = solverVariable3;
                            z = true;
                        }
                        z = a2;
                        f = f3;
                        solverVariable2 = solverVariable3;
                    } else if (solverVariable2 == null && f3 < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (solverVariable == null) {
                            a = mo13514a(solverVariable3, q4Var);
                        } else if (f2 > f3) {
                            a = mo13514a(solverVariable3, q4Var);
                        } else if (!z2 && mo13514a(solverVariable3, q4Var)) {
                            f2 = f3;
                            solverVariable = solverVariable3;
                            z2 = true;
                        }
                        z2 = a;
                        f2 = f3;
                        solverVariable = solverVariable3;
                    }
                }
                i = this.f7394g[i];
                i2++;
            } else {
                if (f3 < 0.001f) {
                    fArr[i] = 0.0f;
                    solverVariable3.mo1256b(this.f7389b);
                }
                if (f3 != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                }
                i = this.f7394g[i];
                i2++;
            }
            f3 = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            if (f3 != com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            }
            i = this.f7394g[i];
            i2++;
        }
        return solverVariable2 != null ? solverVariable2 : solverVariable;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo13511a(com.fossil.blesdk.obfuscated.C2459n4 n4Var, com.fossil.blesdk.obfuscated.C2459n4 n4Var2, boolean z) {
        int i = this.f7396i;
        while (true) {
            int i2 = 0;
            while (i != -1 && i2 < this.f7388a) {
                int i3 = this.f7393f[i];
                androidx.constraintlayout.solver.SolverVariable solverVariable = n4Var2.f7655a;
                if (i3 == solverVariable.f639b) {
                    float f = this.f7395h[i];
                    mo13503a(solverVariable, z);
                    com.fossil.blesdk.obfuscated.C2375m4 m4Var = n4Var2.f7658d;
                    int i4 = m4Var.f7396i;
                    int i5 = 0;
                    while (i4 != -1 && i5 < m4Var.f7388a) {
                        mo13510a(this.f7390c.f7983c[m4Var.f7393f[i4]], m4Var.f7395h[i4] * f, z);
                        i4 = m4Var.f7394g[i4];
                        i5++;
                    }
                    n4Var.f7656b += n4Var2.f7656b * f;
                    if (z) {
                        n4Var2.f7655a.mo1256b(n4Var);
                    }
                    i = this.f7396i;
                } else {
                    i = this.f7394g[i];
                    i2++;
                }
            }
            return;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13512a(com.fossil.blesdk.obfuscated.C2459n4 n4Var, com.fossil.blesdk.obfuscated.C2459n4[] n4VarArr) {
        int i = this.f7396i;
        while (true) {
            int i2 = 0;
            while (i != -1 && i2 < this.f7388a) {
                androidx.constraintlayout.solver.SolverVariable solverVariable = this.f7390c.f7983c[this.f7393f[i]];
                if (solverVariable.f640c != -1) {
                    float f = this.f7395h[i];
                    mo13503a(solverVariable, true);
                    com.fossil.blesdk.obfuscated.C2459n4 n4Var2 = n4VarArr[solverVariable.f640c];
                    if (!n4Var2.f7659e) {
                        com.fossil.blesdk.obfuscated.C2375m4 m4Var = n4Var2.f7658d;
                        int i3 = m4Var.f7396i;
                        int i4 = 0;
                        while (i3 != -1 && i4 < m4Var.f7388a) {
                            mo13510a(this.f7390c.f7983c[m4Var.f7393f[i3]], m4Var.f7395h[i3] * f, true);
                            i3 = m4Var.f7394g[i3];
                            i4++;
                        }
                    }
                    n4Var.f7656b += n4Var2.f7656b * f;
                    n4Var2.f7655a.mo1256b(n4Var);
                    i = this.f7396i;
                } else {
                    i = this.f7394g[i];
                    i2++;
                }
            }
            return;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public androidx.constraintlayout.solver.SolverVariable mo13506a(boolean[] zArr, androidx.constraintlayout.solver.SolverVariable solverVariable) {
        int i = this.f7396i;
        int i2 = 0;
        androidx.constraintlayout.solver.SolverVariable solverVariable2 = null;
        float f = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (i != -1 && i2 < this.f7388a) {
            if (this.f7395h[i] < com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                androidx.constraintlayout.solver.SolverVariable solverVariable3 = this.f7390c.f7983c[this.f7393f[i]];
                if ((zArr == null || !zArr[solverVariable3.f639b]) && solverVariable3 != solverVariable) {
                    androidx.constraintlayout.solver.SolverVariable.Type type = solverVariable3.f644g;
                    if (type == androidx.constraintlayout.solver.SolverVariable.Type.SLACK || type == androidx.constraintlayout.solver.SolverVariable.Type.ERROR) {
                        float f2 = this.f7395h[i];
                        if (f2 < f) {
                            solverVariable2 = solverVariable3;
                            f = f2;
                        }
                    }
                }
            }
            i = this.f7394g[i];
            i2++;
        }
        return solverVariable2;
    }

    @DexIgnore
    /* renamed from: a */
    public final androidx.constraintlayout.solver.SolverVariable mo13504a(int i) {
        int i2 = this.f7396i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.f7388a) {
            if (i3 == i) {
                return this.f7390c.f7983c[this.f7393f[i2]];
            }
            i2 = this.f7394g[i2];
            i3++;
        }
        return null;
    }
}
