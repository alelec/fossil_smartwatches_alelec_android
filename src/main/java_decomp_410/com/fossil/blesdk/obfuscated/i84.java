package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface i84 {
    @DexIgnore
    void onComplete();

    @DexIgnore
    void onError(Throwable th);

    @DexIgnore
    void onSubscribe(y84 y84);
}
