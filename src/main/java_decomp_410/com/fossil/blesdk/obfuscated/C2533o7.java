package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.o7 */
public final class C2533o7 {
    @DexIgnore
    /* renamed from: a */
    public static java.util.Locale m11628a(java.lang.String str) {
        if (str.contains(com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
            java.lang.String[] split = str.split(com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, -1);
            if (split.length > 2) {
                return new java.util.Locale(split[0], split[1], split[2]);
            }
            if (split.length > 1) {
                return new java.util.Locale(split[0], split[1]);
            }
            if (split.length == 1) {
                return new java.util.Locale(split[0]);
            }
        } else if (!str.contains("_")) {
            return new java.util.Locale(str);
        } else {
            java.lang.String[] split2 = str.split("_", -1);
            if (split2.length > 2) {
                return new java.util.Locale(split2[0], split2[1], split2[2]);
            }
            if (split2.length > 1) {
                return new java.util.Locale(split2[0], split2[1]);
            }
            if (split2.length == 1) {
                return new java.util.Locale(split2[0]);
            }
        }
        throw new java.lang.IllegalArgumentException("Can not parse language tag: [" + str + "]");
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m11627a(java.util.Locale locale) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append(locale.getLanguage());
        java.lang.String country = locale.getCountry();
        if (country != null && !country.isEmpty()) {
            sb.append(com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
            sb.append(locale.getCountry());
        }
        return sb.toString();
    }
}
