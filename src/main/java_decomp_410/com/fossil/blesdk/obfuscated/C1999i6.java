package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i6 */
public class C1999i6 {

    @DexIgnore
    /* renamed from: a */
    public android.app.Activity f5946a;

    @DexIgnore
    /* renamed from: b */
    public android.content.Intent f5947b; // = new android.content.Intent().setAction("android.intent.action.SEND");

    @DexIgnore
    /* renamed from: c */
    public java.lang.CharSequence f5948c;

    @DexIgnore
    /* renamed from: d */
    public java.util.ArrayList<java.lang.String> f5949d;

    @DexIgnore
    /* renamed from: e */
    public java.util.ArrayList<java.lang.String> f5950e;

    @DexIgnore
    /* renamed from: f */
    public java.util.ArrayList<java.lang.String> f5951f;

    @DexIgnore
    /* renamed from: g */
    public java.util.ArrayList<android.net.Uri> f5952g;

    @DexIgnore
    public C1999i6(android.app.Activity activity) {
        this.f5946a = activity;
        this.f5947b.putExtra("androidx.core.app.EXTRA_CALLING_PACKAGE", activity.getPackageName());
        this.f5947b.putExtra("androidx.core.app.EXTRA_CALLING_ACTIVITY", activity.getComponentName());
        this.f5947b.addFlags(524288);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1999i6 m8243a(android.app.Activity activity) {
        return new com.fossil.blesdk.obfuscated.C1999i6(activity);
    }

    @DexIgnore
    /* renamed from: b */
    public android.content.Intent mo11833b() {
        java.util.ArrayList<java.lang.String> arrayList = this.f5949d;
        if (arrayList != null) {
            mo11832a("android.intent.extra.EMAIL", arrayList);
            this.f5949d = null;
        }
        java.util.ArrayList<java.lang.String> arrayList2 = this.f5950e;
        if (arrayList2 != null) {
            mo11832a("android.intent.extra.CC", arrayList2);
            this.f5950e = null;
        }
        java.util.ArrayList<java.lang.String> arrayList3 = this.f5951f;
        if (arrayList3 != null) {
            mo11832a("android.intent.extra.BCC", arrayList3);
            this.f5951f = null;
        }
        java.util.ArrayList<android.net.Uri> arrayList4 = this.f5952g;
        boolean z = true;
        if (arrayList4 == null || arrayList4.size() <= 1) {
            z = false;
        }
        boolean equals = this.f5947b.getAction().equals("android.intent.action.SEND_MULTIPLE");
        if (!z && equals) {
            this.f5947b.setAction("android.intent.action.SEND");
            java.util.ArrayList<android.net.Uri> arrayList5 = this.f5952g;
            if (arrayList5 == null || arrayList5.isEmpty()) {
                this.f5947b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.f5947b.putExtra("android.intent.extra.STREAM", this.f5952g.get(0));
            }
            this.f5952g = null;
        }
        if (z && !equals) {
            this.f5947b.setAction("android.intent.action.SEND_MULTIPLE");
            java.util.ArrayList<android.net.Uri> arrayList6 = this.f5952g;
            if (arrayList6 == null || arrayList6.isEmpty()) {
                this.f5947b.removeExtra("android.intent.extra.STREAM");
            } else {
                this.f5947b.putParcelableArrayListExtra("android.intent.extra.STREAM", this.f5952g);
            }
        }
        return this.f5947b;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11835c() {
        this.f5946a.startActivity(mo11830a());
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11832a(java.lang.String str, java.util.ArrayList<java.lang.String> arrayList) {
        java.lang.String[] stringArrayExtra = this.f5947b.getStringArrayExtra(str);
        int length = stringArrayExtra != null ? stringArrayExtra.length : 0;
        java.lang.String[] strArr = new java.lang.String[(arrayList.size() + length)];
        arrayList.toArray(strArr);
        if (stringArrayExtra != null) {
            java.lang.System.arraycopy(stringArrayExtra, 0, strArr, arrayList.size(), length);
        }
        this.f5947b.putExtra(str, strArr);
    }

    @DexIgnore
    /* renamed from: a */
    public android.content.Intent mo11830a() {
        return android.content.Intent.createChooser(mo11833b(), this.f5948c);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1999i6 mo11831a(java.lang.String str) {
        if (this.f5949d == null) {
            this.f5949d = new java.util.ArrayList<>();
        }
        this.f5949d.add(str);
        return this;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C1999i6 mo11834b(java.lang.String str) {
        this.f5947b.setType(str);
        return this;
    }
}
