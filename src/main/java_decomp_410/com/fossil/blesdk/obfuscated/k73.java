package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k73 implements Factory<j73> {
    @DexIgnore
    public static /* final */ k73 a; // = new k73();

    @DexIgnore
    public static k73 a() {
        return a;
    }

    @DexIgnore
    public static j73 b() {
        return new j73();
    }

    @DexIgnore
    public j73 get() {
        return b();
    }
}
