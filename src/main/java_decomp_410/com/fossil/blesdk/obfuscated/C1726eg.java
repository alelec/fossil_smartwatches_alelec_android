package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.eg */
public class C1726eg {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.String f4783a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1726eg.C1727a> f4784b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1728b> f4785c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1730d> f4786d;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eg$a")
    /* renamed from: com.fossil.blesdk.obfuscated.eg$a */
    public static class C1727a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f4787a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f4788b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f4789c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ boolean f4790d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f4791e;

        @DexIgnore
        public C1727a(java.lang.String str, java.lang.String str2, boolean z, int i) {
            this.f4787a = str;
            this.f4788b = str2;
            this.f4790d = z;
            this.f4791e = i;
            this.f4789c = m6446a(str2);
        }

        @DexIgnore
        /* renamed from: a */
        public static int m6446a(java.lang.String str) {
            if (str == null) {
                return 5;
            }
            java.lang.String upperCase = str.toUpperCase(java.util.Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1726eg.C1727a.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1726eg.C1727a aVar = (com.fossil.blesdk.obfuscated.C1726eg.C1727a) obj;
            if (android.os.Build.VERSION.SDK_INT >= 20) {
                if (this.f4791e != aVar.f4791e) {
                    return false;
                }
            } else if (mo10478a() != aVar.mo10478a()) {
                return false;
            }
            if (this.f4787a.equals(aVar.f4787a) && this.f4790d == aVar.f4790d && this.f4789c == aVar.f4789c) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.f4787a.hashCode() * 31) + this.f4789c) * 31) + (this.f4790d ? 1231 : 1237)) * 31) + this.f4791e;
        }

        @DexIgnore
        public java.lang.String toString() {
            return "Column{name='" + this.f4787a + '\'' + ", type='" + this.f4788b + '\'' + ", affinity='" + this.f4789c + '\'' + ", notNull=" + this.f4790d + ", primaryKeyPosition=" + this.f4791e + '}';
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo10478a() {
            return this.f4791e > 0;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eg$b")
    /* renamed from: com.fossil.blesdk.obfuscated.eg$b */
    public static class C1728b {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f4792a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ java.lang.String f4793b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.lang.String f4794c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ java.util.List<java.lang.String> f4795d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ java.util.List<java.lang.String> f4796e;

        @DexIgnore
        public C1728b(java.lang.String str, java.lang.String str2, java.lang.String str3, java.util.List<java.lang.String> list, java.util.List<java.lang.String> list2) {
            this.f4792a = str;
            this.f4793b = str2;
            this.f4794c = str3;
            this.f4795d = java.util.Collections.unmodifiableList(list);
            this.f4796e = java.util.Collections.unmodifiableList(list2);
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1726eg.C1728b.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1726eg.C1728b bVar = (com.fossil.blesdk.obfuscated.C1726eg.C1728b) obj;
            if (this.f4792a.equals(bVar.f4792a) && this.f4793b.equals(bVar.f4793b) && this.f4794c.equals(bVar.f4794c) && this.f4795d.equals(bVar.f4795d)) {
                return this.f4796e.equals(bVar.f4796e);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.f4792a.hashCode() * 31) + this.f4793b.hashCode()) * 31) + this.f4794c.hashCode()) * 31) + this.f4795d.hashCode()) * 31) + this.f4796e.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return "ForeignKey{referenceTable='" + this.f4792a + '\'' + ", onDelete='" + this.f4793b + '\'' + ", onUpdate='" + this.f4794c + '\'' + ", columnNames=" + this.f4795d + ", referenceColumnNames=" + this.f4796e + '}';
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eg$c")
    /* renamed from: com.fossil.blesdk.obfuscated.eg$c */
    public static class C1729c implements java.lang.Comparable<com.fossil.blesdk.obfuscated.C1726eg.C1729c> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f4797e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ int f4798f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ java.lang.String f4799g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ java.lang.String f4800h;

        @DexIgnore
        public C1729c(int i, int i2, java.lang.String str, java.lang.String str2) {
            this.f4797e = i;
            this.f4798f = i2;
            this.f4799g = str;
            this.f4800h = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(com.fossil.blesdk.obfuscated.C1726eg.C1729c cVar) {
            int i = this.f4797e - cVar.f4797e;
            return i == 0 ? this.f4798f - cVar.f4798f : i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.eg$d")
    /* renamed from: com.fossil.blesdk.obfuscated.eg$d */
    public static class C1730d {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f4801a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f4802b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ java.util.List<java.lang.String> f4803c;

        @DexIgnore
        public C1730d(java.lang.String str, boolean z, java.util.List<java.lang.String> list) {
            this.f4801a = str;
            this.f4802b = z;
            this.f4803c = list;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || com.fossil.blesdk.obfuscated.C1726eg.C1730d.class != obj.getClass()) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C1726eg.C1730d dVar = (com.fossil.blesdk.obfuscated.C1726eg.C1730d) obj;
            if (this.f4802b != dVar.f4802b || !this.f4803c.equals(dVar.f4803c)) {
                return false;
            }
            if (this.f4801a.startsWith("index_")) {
                return dVar.f4801a.startsWith("index_");
            }
            return this.f4801a.equals(dVar.f4801a);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.f4801a.startsWith("index_")) {
                i = "index_".hashCode();
            } else {
                i = this.f4801a.hashCode();
            }
            return (((i * 31) + (this.f4802b ? 1 : 0)) * 31) + this.f4803c.hashCode();
        }

        @DexIgnore
        public java.lang.String toString() {
            return "Index{name='" + this.f4801a + '\'' + ", unique=" + this.f4802b + ", columns=" + this.f4803c + '}';
        }
    }

    @DexIgnore
    public C1726eg(java.lang.String str, java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1726eg.C1727a> map, java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1728b> set, java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1730d> set2) {
        java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1730d> set3;
        this.f4783a = str;
        this.f4784b = java.util.Collections.unmodifiableMap(map);
        this.f4785c = java.util.Collections.unmodifiableSet(set);
        if (set2 == null) {
            set3 = null;
        } else {
            set3 = java.util.Collections.unmodifiableSet(set2);
        }
        this.f4786d = set3;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1726eg m6441a(com.fossil.blesdk.obfuscated.C1874gg ggVar, java.lang.String str) {
        return new com.fossil.blesdk.obfuscated.C1726eg(str, m6443b(ggVar, str), m6444c(ggVar, str), m6445d(ggVar, str));
    }

    @DexIgnore
    /* renamed from: b */
    public static java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1726eg.C1727a> m6443b(com.fossil.blesdk.obfuscated.C1874gg ggVar, java.lang.String str) {
        android.database.Cursor d = ggVar.mo11232d("PRAGMA table_info(`" + str + "`)");
        java.util.HashMap hashMap = new java.util.HashMap();
        try {
            if (d.getColumnCount() > 0) {
                int columnIndex = d.getColumnIndex("name");
                int columnIndex2 = d.getColumnIndex("type");
                int columnIndex3 = d.getColumnIndex("notnull");
                int columnIndex4 = d.getColumnIndex("pk");
                while (d.moveToNext()) {
                    java.lang.String string = d.getString(columnIndex);
                    hashMap.put(string, new com.fossil.blesdk.obfuscated.C1726eg.C1727a(string, d.getString(columnIndex2), d.getInt(columnIndex3) != 0, d.getInt(columnIndex4)));
                }
            }
            return hashMap;
        } finally {
            d.close();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1728b> m6444c(com.fossil.blesdk.obfuscated.C1874gg ggVar, java.lang.String str) {
        java.util.HashSet hashSet = new java.util.HashSet();
        android.database.Cursor d = ggVar.mo11232d("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = d.getColumnIndex("id");
            int columnIndex2 = d.getColumnIndex("seq");
            int columnIndex3 = d.getColumnIndex("table");
            int columnIndex4 = d.getColumnIndex("on_delete");
            int columnIndex5 = d.getColumnIndex("on_update");
            java.util.List<com.fossil.blesdk.obfuscated.C1726eg.C1729c> a = m6442a(d);
            int count = d.getCount();
            for (int i = 0; i < count; i++) {
                d.moveToPosition(i);
                if (d.getInt(columnIndex2) == 0) {
                    int i2 = d.getInt(columnIndex);
                    java.util.ArrayList arrayList = new java.util.ArrayList();
                    java.util.ArrayList arrayList2 = new java.util.ArrayList();
                    for (com.fossil.blesdk.obfuscated.C1726eg.C1729c next : a) {
                        if (next.f4797e == i2) {
                            arrayList.add(next.f4799g);
                            arrayList2.add(next.f4800h);
                        }
                    }
                    com.fossil.blesdk.obfuscated.C1726eg.C1728b bVar = new com.fossil.blesdk.obfuscated.C1726eg.C1728b(d.getString(columnIndex3), d.getString(columnIndex4), d.getString(columnIndex5), arrayList, arrayList2);
                    hashSet.add(bVar);
                }
            }
            return hashSet;
        } finally {
            d.close();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public static java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1730d> m6445d(com.fossil.blesdk.obfuscated.C1874gg ggVar, java.lang.String str) {
        android.database.Cursor d = ggVar.mo11232d("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = d.getColumnIndex("name");
            int columnIndex2 = d.getColumnIndex("origin");
            int columnIndex3 = d.getColumnIndex(com.j256.ormlite.field.DatabaseFieldConfigLoader.FIELD_NAME_UNIQUE);
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    java.util.HashSet hashSet = new java.util.HashSet();
                    while (d.moveToNext()) {
                        if ("c".equals(d.getString(columnIndex2))) {
                            java.lang.String string = d.getString(columnIndex);
                            boolean z = true;
                            if (d.getInt(columnIndex3) != 1) {
                                z = false;
                            }
                            com.fossil.blesdk.obfuscated.C1726eg.C1730d a = m6440a(ggVar, string, z);
                            if (a == null) {
                                d.close();
                                return null;
                            }
                            hashSet.add(a);
                        }
                    }
                    d.close();
                    return hashSet;
                }
            }
            return null;
        } finally {
            d.close();
        }
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C1726eg.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1726eg egVar = (com.fossil.blesdk.obfuscated.C1726eg) obj;
        java.lang.String str = this.f4783a;
        if (str == null ? egVar.f4783a != null : !str.equals(egVar.f4783a)) {
            return false;
        }
        java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1726eg.C1727a> map = this.f4784b;
        if (map == null ? egVar.f4784b != null : !map.equals(egVar.f4784b)) {
            return false;
        }
        java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1728b> set = this.f4785c;
        if (set == null ? egVar.f4785c != null : !set.equals(egVar.f4785c)) {
            return false;
        }
        java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1730d> set2 = this.f4786d;
        if (set2 != null) {
            java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1730d> set3 = egVar.f4786d;
            if (set3 != null) {
                return set2.equals(set3);
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        java.lang.String str = this.f4783a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        java.util.Map<java.lang.String, com.fossil.blesdk.obfuscated.C1726eg.C1727a> map = this.f4784b;
        int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
        java.util.Set<com.fossil.blesdk.obfuscated.C1726eg.C1728b> set = this.f4785c;
        if (set != null) {
            i = set.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "TableInfo{name='" + this.f4783a + '\'' + ", columns=" + this.f4784b + ", foreignKeys=" + this.f4785c + ", indices=" + this.f4786d + '}';
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<com.fossil.blesdk.obfuscated.C1726eg.C1729c> m6442a(android.database.Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new com.fossil.blesdk.obfuscated.C1726eg.C1729c(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        java.util.Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1726eg.C1730d m6440a(com.fossil.blesdk.obfuscated.C1874gg ggVar, java.lang.String str, boolean z) {
        android.database.Cursor d = ggVar.mo11232d("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = d.getColumnIndex("seqno");
            int columnIndex2 = d.getColumnIndex("cid");
            int columnIndex3 = d.getColumnIndex("name");
            if (!(columnIndex == -1 || columnIndex2 == -1)) {
                if (columnIndex3 != -1) {
                    java.util.TreeMap treeMap = new java.util.TreeMap();
                    while (d.moveToNext()) {
                        if (d.getInt(columnIndex2) >= 0) {
                            int i = d.getInt(columnIndex);
                            treeMap.put(java.lang.Integer.valueOf(i), d.getString(columnIndex3));
                        }
                    }
                    java.util.ArrayList arrayList = new java.util.ArrayList(treeMap.size());
                    arrayList.addAll(treeMap.values());
                    com.fossil.blesdk.obfuscated.C1726eg.C1730d dVar = new com.fossil.blesdk.obfuscated.C1726eg.C1730d(str, z, arrayList);
                    d.close();
                    return dVar;
                }
            }
            return null;
        } finally {
            d.close();
        }
    }
}
