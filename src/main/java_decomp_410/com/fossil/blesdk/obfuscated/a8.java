package com.fossil.blesdk.obfuscated;

import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a8 {
    @DexIgnore
    public static /* final */ z7 a; // = new e((c) null, false);
    @DexIgnore
    public static /* final */ z7 b; // = new e((c) null, true);
    @DexIgnore
    public static /* final */ z7 c; // = new e(b.a, false);
    @DexIgnore
    public static /* final */ z7 d; // = new e(b.a, true);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c {
        @DexIgnore
        public static /* final */ a b; // = new a(true);
        @DexIgnore
        public /* final */ boolean a;

        /*
        static {
            new a(false);
        }
        */

        @DexIgnore
        public a(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i2 + i;
            boolean z = false;
            while (i < i3) {
                int a2 = a8.a(Character.getDirectionality(charSequence.charAt(i)));
                if (a2 != 0) {
                    if (a2 != 1) {
                        continue;
                        i++;
                    } else if (!this.a) {
                        return 1;
                    }
                } else if (this.a) {
                    return 0;
                }
                z = true;
                i++;
            }
            if (z) {
                return this.a ? 1 : 0;
            }
            return 2;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public int a(CharSequence charSequence, int i, int i2) {
            int i3 = i2 + i;
            int i4 = 2;
            while (i < i3 && i4 == 2) {
                i4 = a8.b(Character.getDirectionality(charSequence.charAt(i)));
                i++;
            }
            return i4;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        int a(CharSequence charSequence, int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d implements z7 {
        @DexIgnore
        public /* final */ c a;

        @DexIgnore
        public d(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public abstract boolean a();

        @DexIgnore
        public boolean a(CharSequence charSequence, int i, int i2) {
            if (charSequence == null || i < 0 || i2 < 0 || charSequence.length() - i2 < i) {
                throw new IllegalArgumentException();
            } else if (this.a == null) {
                return a();
            } else {
                return b(charSequence, i, i2);
            }
        }

        @DexIgnore
        public final boolean b(CharSequence charSequence, int i, int i2) {
            int a2 = this.a.a(charSequence, i, i2);
            if (a2 == 0) {
                return true;
            }
            if (a2 != 1) {
                return a();
            }
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends d {
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public e(c cVar, boolean z) {
            super(cVar);
            this.b = z;
        }

        @DexIgnore
        public boolean a() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends d {
        @DexIgnore
        public static /* final */ f b; // = new f();

        @DexIgnore
        public f() {
            super((c) null);
        }

        @DexIgnore
        public boolean a() {
            return b8.b(Locale.getDefault()) == 1;
        }
    }

    /*
    static {
        new e(a.b, false);
        f fVar = f.b;
    }
    */

    @DexIgnore
    public static int a(int i) {
        if (i != 0) {
            return (i == 1 || i == 2) ? 0 : 2;
        }
        return 1;
    }

    @DexIgnore
    public static int b(int i) {
        if (i != 0) {
            if (i == 1 || i == 2) {
                return 0;
            }
            switch (i) {
                case 14:
                case 15:
                    break;
                case 16:
                case 17:
                    return 0;
                default:
                    return 2;
            }
        }
        return 1;
    }
}
