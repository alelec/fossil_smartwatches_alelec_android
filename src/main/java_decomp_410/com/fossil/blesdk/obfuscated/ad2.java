package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ad2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ CustomPageIndicator r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageButton t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ RecyclerViewPager v;
    @DexIgnore
    public /* final */ TextView w;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ad2(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CustomPageIndicator customPageIndicator, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageButton imageButton, ImageView imageView, ProgressBar progressBar, RecyclerViewPager recyclerViewPager, FlexibleTextView flexibleTextView4, TextView textView, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = customPageIndicator;
        this.s = flexibleTextView3;
        this.t = imageButton;
        this.u = imageView;
        this.v = recyclerViewPager;
        this.w = textView;
    }
}
