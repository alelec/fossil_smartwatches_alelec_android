package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zw */
public class C3471zw implements com.crashlytics.android.CrashlyticsInitProvider.C0390a {
    @DexIgnore
    /* renamed from: a */
    public boolean mo4060a(android.content.Context context) {
        return com.fossil.blesdk.obfuscated.o54.m26050a(context).mo29819b();
    }
}
