package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m0 */
public final class C2364m0 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.ThreadLocal<android.util.TypedValue> f7376a; // = new java.lang.ThreadLocal<>();

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.util.WeakHashMap<android.content.Context, android.util.SparseArray<com.fossil.blesdk.obfuscated.C2364m0.C2365a>> f7377b; // = new java.util.WeakHashMap<>(0);

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.lang.Object f7378c; // = new java.lang.Object();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.m0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.m0$a */
    public static class C2365a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.res.ColorStateList f7379a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.content.res.Configuration f7380b;

        @DexIgnore
        public C2365a(android.content.res.ColorStateList colorStateList, android.content.res.Configuration configuration) {
            this.f7379a = colorStateList;
            this.f7380b = configuration;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        return null;
     */
    @DexIgnore
    /* renamed from: a */
    public static android.content.res.ColorStateList m10493a(android.content.Context context, int i) {
        synchronized (f7378c) {
            android.util.SparseArray sparseArray = f7377b.get(context);
            if (sparseArray != null && sparseArray.size() > 0) {
                com.fossil.blesdk.obfuscated.C2364m0.C2365a aVar = (com.fossil.blesdk.obfuscated.C2364m0.C2365a) sparseArray.get(i);
                if (aVar != null) {
                    if (aVar.f7380b.equals(context.getResources().getConfiguration())) {
                        android.content.res.ColorStateList colorStateList = aVar.f7379a;
                        return colorStateList;
                    }
                    sparseArray.remove(i);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static android.content.res.ColorStateList m10496b(android.content.Context context, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        android.content.res.ColorStateList a = m10493a(context, i);
        if (a != null) {
            return a;
        }
        android.content.res.ColorStateList d = m10498d(context, i);
        if (d == null) {
            return com.fossil.blesdk.obfuscated.C2185k6.m9355b(context, i);
        }
        m10495a(context, i, d);
        return d;
    }

    @DexIgnore
    /* renamed from: c */
    public static android.graphics.drawable.Drawable m10497c(android.content.Context context, int i) {
        return com.fossil.blesdk.obfuscated.C1526c2.m5217a().mo9396c(context, i);
    }

    @DexIgnore
    /* renamed from: d */
    public static android.content.res.ColorStateList m10498d(android.content.Context context, int i) {
        if (m10499e(context, i)) {
            return null;
        }
        android.content.res.Resources resources = context.getResources();
        try {
            return com.fossil.blesdk.obfuscated.C2377m6.m10555a(resources, resources.getXml(i), context.getTheme());
        } catch (java.lang.Exception e) {
            android.util.Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public static boolean m10499e(android.content.Context context, int i) {
        android.content.res.Resources resources = context.getResources();
        android.util.TypedValue a = m10494a();
        resources.getValue(i, a, true);
        int i2 = a.type;
        if (i2 < 28 || i2 > 31) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m10495a(android.content.Context context, int i, android.content.res.ColorStateList colorStateList) {
        synchronized (f7378c) {
            android.util.SparseArray sparseArray = f7377b.get(context);
            if (sparseArray == null) {
                sparseArray = new android.util.SparseArray();
                f7377b.put(context, sparseArray);
            }
            sparseArray.append(i, new com.fossil.blesdk.obfuscated.C2364m0.C2365a(colorStateList, context.getResources().getConfiguration()));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.util.TypedValue m10494a() {
        android.util.TypedValue typedValue = f7376a.get();
        if (typedValue != null) {
            return typedValue;
        }
        android.util.TypedValue typedValue2 = new android.util.TypedValue();
        f7376a.set(typedValue2);
        return typedValue2;
    }
}
