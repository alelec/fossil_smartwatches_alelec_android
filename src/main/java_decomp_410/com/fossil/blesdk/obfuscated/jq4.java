package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jq4 implements bq4 {
    @DexIgnore
    public boolean a; // = false;
    @DexIgnore
    public /* final */ Map<String, iq4> b; // = new HashMap();
    @DexIgnore
    public /* final */ LinkedBlockingQueue<gq4> c; // = new LinkedBlockingQueue<>();

    @DexIgnore
    public synchronized cq4 a(String str) {
        iq4 iq4;
        iq4 = this.b.get(str);
        if (iq4 == null) {
            iq4 = new iq4(str, this.c, this.a);
            this.b.put(str, iq4);
        }
        return iq4;
    }

    @DexIgnore
    public LinkedBlockingQueue<gq4> b() {
        return this.c;
    }

    @DexIgnore
    public List<iq4> c() {
        return new ArrayList(this.b.values());
    }

    @DexIgnore
    public void d() {
        this.a = true;
    }

    @DexIgnore
    public void a() {
        this.b.clear();
        this.c.clear();
    }
}
