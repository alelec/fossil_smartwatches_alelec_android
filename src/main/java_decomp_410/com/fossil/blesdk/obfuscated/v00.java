package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class v00<V, E> {
    @DexIgnore
    public V a;
    @DexIgnore
    public E b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public CopyOnWriteArrayList<xc4<V, qa4>> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<xc4<E, qa4>> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<xc4<qa4, qa4>> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<xc4<Float, qa4>> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<xc4<E, qa4>> h; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public final boolean a() {
        return c() || b();
    }

    @DexIgnore
    public final boolean b() {
        return this.b != null;
    }

    @DexIgnore
    public final boolean c() {
        return this.a != null;
    }

    @DexIgnore
    public v00<V, E> d(xc4<? super Float, qa4> xc4) {
        kd4.b(xc4, "actionOnProgressChange");
        if (!a()) {
            this.g.add(xc4);
        }
        return this;
    }

    @DexIgnore
    public v00<V, E> e(xc4<? super V, qa4> xc4) {
        kd4.b(xc4, "actionOnSuccess");
        if (!a()) {
            this.d.add(xc4);
        } else if (c()) {
            try {
                V v = this.a;
                if (v != null) {
                    xc4.invoke(v);
                } else {
                    kd4.a();
                    throw null;
                }
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
        return this;
    }

    @DexIgnore
    public v00<V, E> a(xc4<? super qa4, qa4> xc4) {
        kd4.b(xc4, "actionOnFinal");
        if (a()) {
            try {
                xc4.invoke(qa4.a);
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        } else {
            this.f.add(xc4);
        }
        return this;
    }

    @DexIgnore
    public v00<V, E> b(xc4<? super E, qa4> xc4) {
        kd4.b(xc4, "actionOnCancel");
        if (!a()) {
            this.h.add(xc4);
        }
        return this;
    }

    @DexIgnore
    public v00<V, E> c(xc4<? super E, qa4> xc4) {
        kd4.b(xc4, "actionOnError");
        if (!a()) {
            this.e.add(xc4);
        } else if (b()) {
            try {
                E e2 = this.b;
                if (e2 != null) {
                    xc4.invoke(e2);
                } else {
                    kd4.a();
                    throw null;
                }
            } catch (Exception e3) {
                da0.l.a(e3);
            }
        }
        return this;
    }

    @DexIgnore
    public final synchronized void d() {
        for (xc4 invoke : this.f) {
            try {
                invoke.invoke(qa4.a);
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h.clear();
    }

    @DexIgnore
    public final synchronized void b(E e2) {
        kd4.b(e2, "error");
        if (!a()) {
            this.b = e2;
            for (xc4 xc4 : this.e) {
                try {
                    E e3 = this.b;
                    if (e3 != null) {
                        xc4.invoke(e3);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } catch (Exception e4) {
                    da0.l.a(e4);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void a(float f2) {
        for (xc4 invoke : this.g) {
            try {
                invoke.invoke(Float.valueOf(f2));
            } catch (Exception e2) {
                da0.l.a(e2);
            }
        }
    }

    @DexIgnore
    public final synchronized void c(V v) {
        kd4.b(v, Constants.RESULT);
        if (!a()) {
            this.a = v;
            for (xc4 xc4 : this.d) {
                try {
                    V v2 = this.a;
                    if (v2 != null) {
                        xc4.invoke(v2);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } catch (Exception e2) {
                    da0.l.a(e2);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void a(E e2) {
        kd4.b(e2, "error");
        if (!a() && !this.c) {
            this.c = true;
            if (true ^ this.h.isEmpty()) {
                for (xc4 invoke : this.h) {
                    try {
                        invoke.invoke(e2);
                    } catch (Exception e3) {
                        da0.l.a(e3);
                    }
                }
                this.c = false;
            } else {
                b(e2);
            }
        }
    }
}
