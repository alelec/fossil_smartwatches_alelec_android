package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dc2 extends cc2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(R.id.calendarMonth, 1);
        w.put(R.id.clTracking, 2);
        w.put(R.id.tvTrackingTitle, 3);
        w.put(R.id.tvTracking1, 4);
        w.put(R.id.tvTracking2, 5);
        w.put(R.id.tvGoToCustomize, 6);
    }
    */

    @DexIgnore
    public dc2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 7, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public dc2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[1], objArr[2], objArr[6], objArr[4], objArr[5], objArr[3]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
