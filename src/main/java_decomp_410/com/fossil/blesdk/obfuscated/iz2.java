package com.fossil.blesdk.obfuscated;

import androidx.loader.app.LoaderManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iz2 {
    @DexIgnore
    public /* final */ gz2 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ LoaderManager c;

    @DexIgnore
    public iz2(gz2 gz2, int i, LoaderManager loaderManager) {
        kd4.b(gz2, "mView");
        kd4.b(loaderManager, "mLoaderManager");
        this.a = gz2;
        this.b = i;
        this.c = loaderManager;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager b() {
        return this.c;
    }

    @DexIgnore
    public final gz2 c() {
        return this.a;
    }
}
