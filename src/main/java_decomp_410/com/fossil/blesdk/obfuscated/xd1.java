package com.fossil.blesdk.obfuscated;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class xd1 extends g41 implements wd1 {
    @DexIgnore
    public xd1() {
        super("com.google.android.gms.location.ILocationListener");
    }

    @DexIgnore
    public static wd1 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationListener");
        return queryLocalInterface instanceof wd1 ? (wd1) queryLocalInterface : new yd1(iBinder);
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onLocationChanged((Location) q41.a(parcel, Location.CREATOR));
        return true;
    }
}
