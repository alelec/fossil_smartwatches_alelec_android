package com.fossil.blesdk.obfuscated;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oy {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public volatile int b; // = 0;

    /*
    static {
        new oy(new byte[0]);
    }
    */

    @DexIgnore
    public oy(byte[] bArr) {
        this.a = bArr;
    }

    @DexIgnore
    public static oy a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return new oy(bArr2);
    }

    @DexIgnore
    public int b() {
        return this.a.length;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof oy)) {
            return false;
        }
        byte[] bArr = this.a;
        int length = bArr.length;
        byte[] bArr2 = ((oy) obj).a;
        if (length != bArr2.length) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        if (i == 0) {
            int i2 = r1;
            for (byte b2 : this.a) {
                i2 = (i2 * 31) + b2;
            }
            i = i2 == 0 ? 1 : i2;
            this.b = i;
        }
        return i;
    }

    @DexIgnore
    public static oy a(String str) {
        try {
            return new oy(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.", e);
        }
    }

    @DexIgnore
    public void a(byte[] bArr, int i, int i2, int i3) {
        System.arraycopy(this.a, i, bArr, i2, i3);
    }

    @DexIgnore
    public InputStream a() {
        return new ByteArrayInputStream(this.a);
    }
}
