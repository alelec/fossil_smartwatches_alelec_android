package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import io.fabric.sdk.android.services.concurrency.DependencyPriorityBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h64 extends ThreadPoolExecutor {
    @DexIgnore
    public static /* final */ int e; // = Runtime.getRuntime().availableProcessors();
    @DexIgnore
    public static /* final */ int f;
    @DexIgnore
    public static /* final */ int g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.a);
            thread.setName("Queue");
            return thread;
        }
    }

    /*
    static {
        int i = e;
        f = i + 1;
        g = (i * 2) + 1;
    }
    */

    @DexIgnore
    public <T extends Runnable & a64 & i64 & f64> h64(int i, int i2, long j, TimeUnit timeUnit, DependencyPriorityBlockingQueue<T> dependencyPriorityBlockingQueue, ThreadFactory threadFactory) {
        super(i, i2, j, timeUnit, dependencyPriorityBlockingQueue, threadFactory);
        prestartAllCoreThreads();
    }

    @DexIgnore
    public static <T extends Runnable & a64 & i64 & f64> h64 a(int i, int i2) {
        return new h64(i, i2, 1, TimeUnit.SECONDS, new DependencyPriorityBlockingQueue(), new a(10));
    }

    @DexIgnore
    public void afterExecute(Runnable runnable, Throwable th) {
        i64 i64 = (i64) runnable;
        i64.a(true);
        i64.a(th);
        getQueue().recycleBlockedQueue();
        super.afterExecute(runnable, th);
    }

    @DexIgnore
    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (g64.b(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(newTaskFor(runnable, (Object) null));
        }
    }

    @DexIgnore
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new e64(runnable, t);
    }

    @DexIgnore
    public static h64 a() {
        return a(f, g);
    }

    @DexIgnore
    public DependencyPriorityBlockingQueue getQueue() {
        return (DependencyPriorityBlockingQueue) super.getQueue();
    }

    @DexIgnore
    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new e64(callable);
    }
}
