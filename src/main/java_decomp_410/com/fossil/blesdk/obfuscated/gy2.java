package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gy2 implements Factory<NotificationWatchRemindersPresenter> {
    @DexIgnore
    public static NotificationWatchRemindersPresenter a(by2 by2, en2 en2, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new NotificationWatchRemindersPresenter(by2, en2, remindersSettingsDatabase);
    }
}
