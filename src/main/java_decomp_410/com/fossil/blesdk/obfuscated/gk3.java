package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gk3 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ ek3 b;

    @DexIgnore
    public gk3(BaseActivity baseActivity, ek3 ek3) {
        kd4.b(baseActivity, "baseActivity");
        kd4.b(ek3, "mView");
        this.a = baseActivity;
        this.b = ek3;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final ek3 b() {
        return this.b;
    }
}
