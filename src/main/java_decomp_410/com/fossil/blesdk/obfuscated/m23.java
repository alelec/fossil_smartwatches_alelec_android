package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m23 implements Factory<ComplicationsPresenter> {
    @DexIgnore
    public static ComplicationsPresenter a(i23 i23, CategoryRepository categoryRepository) {
        return new ComplicationsPresenter(i23, categoryRepository);
    }
}
