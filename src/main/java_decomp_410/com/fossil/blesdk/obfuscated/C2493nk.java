package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nk */
public class C2493nk extends com.fossil.blesdk.obfuscated.C2331lk<com.fossil.blesdk.obfuscated.C1878gk> {

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f7817e; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("NetworkMeteredCtrlr");

    @DexIgnore
    public C2493nk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(com.fossil.blesdk.obfuscated.C3294xk.m16376a(context, zlVar).mo17770c());
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12371a(com.fossil.blesdk.obfuscated.C1954hl hlVar) {
        return hlVar.f5780j.mo18107b() == androidx.work.NetworkType.METERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo12373b(com.fossil.blesdk.obfuscated.C1878gk gkVar) {
        if (android.os.Build.VERSION.SDK_INT < 26) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f7817e, "Metered network constraint is not supported before API 26, only checking for connected state.", new java.lang.Throwable[0]);
            return !gkVar.mo11251a();
        } else if (!gkVar.mo11251a() || !gkVar.mo11252b()) {
            return true;
        } else {
            return false;
        }
    }
}
