package com.fossil.blesdk.obfuscated;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xd4 extends mb4 {
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore
    public xd4(int i, int i2, int i3) {
        this.h = i3;
        this.e = i2;
        boolean z = true;
        if (this.h <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.f = z;
        this.g = !this.f ? this.e : i;
    }

    @DexIgnore
    public int a() {
        int i = this.g;
        if (i != this.e) {
            this.g = this.h + i;
        } else if (this.f) {
            this.f = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.f;
    }
}
