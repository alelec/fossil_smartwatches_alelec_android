package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class x74 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public x74(String str, String str2, String str3, boolean z, String str4, boolean z2, String str5) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = z;
        this.e = str4;
        this.f = z2;
        this.g = str5;
    }
}
