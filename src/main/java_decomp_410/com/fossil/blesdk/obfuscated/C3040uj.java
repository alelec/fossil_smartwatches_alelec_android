package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uj */
public class C3040uj implements java.lang.Runnable {

    @DexIgnore
    /* renamed from: w */
    public static /* final */ java.lang.String f9981w; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("WorkerWrapper");

    @DexIgnore
    /* renamed from: e */
    public android.content.Context f9982e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.String f9983f;

    @DexIgnore
    /* renamed from: g */
    public java.util.List<com.fossil.blesdk.obfuscated.C2656pj> f9984g;

    @DexIgnore
    /* renamed from: h */
    public androidx.work.WorkerParameters.C0359a f9985h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C1954hl f9986i;

    @DexIgnore
    /* renamed from: j */
    public androidx.work.ListenableWorker f9987j;

    @DexIgnore
    /* renamed from: k */
    public androidx.work.ListenableWorker.C0354a f9988k; // = androidx.work.ListenableWorker.C0354a.m2465a();

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C3290xi f9989l;

    @DexIgnore
    /* renamed from: m */
    public com.fossil.blesdk.obfuscated.C3444zl f9990m;

    @DexIgnore
    /* renamed from: n */
    public androidx.work.impl.WorkDatabase f9991n;

    @DexIgnore
    /* renamed from: o */
    public com.fossil.blesdk.obfuscated.C2036il f9992o;

    @DexIgnore
    /* renamed from: p */
    public com.fossil.blesdk.obfuscated.C3443zk f9993p;

    @DexIgnore
    /* renamed from: q */
    public com.fossil.blesdk.obfuscated.C2333ll f9994q;

    @DexIgnore
    /* renamed from: r */
    public java.util.List<java.lang.String> f9995r;

    @DexIgnore
    /* renamed from: s */
    public java.lang.String f9996s;

    @DexIgnore
    /* renamed from: t */
    public com.fossil.blesdk.obfuscated.C3369yl<java.lang.Boolean> f9997t; // = com.fossil.blesdk.obfuscated.C3369yl.m16890e();

    @DexIgnore
    /* renamed from: u */
    public com.fossil.blesdk.obfuscated.zv1<androidx.work.ListenableWorker.C0354a> f9998u; // = null;

    @DexIgnore
    /* renamed from: v */
    public volatile boolean f9999v;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uj$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uj$a */
    public class C3041a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3369yl f10000e;

        @DexIgnore
        public C3041a(com.fossil.blesdk.obfuscated.C3369yl ylVar) {
            this.f10000e = ylVar;
        }

        @DexIgnore
        public void run() {
            try {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C3040uj.f9981w, java.lang.String.format("Starting work for %s", new java.lang.Object[]{com.fossil.blesdk.obfuscated.C3040uj.this.f9986i.f5773c}), new java.lang.Throwable[0]);
                com.fossil.blesdk.obfuscated.C3040uj.this.f9998u = com.fossil.blesdk.obfuscated.C3040uj.this.f9987j.mo3738j();
                this.f10000e.mo3820a(com.fossil.blesdk.obfuscated.C3040uj.this.f9998u);
            } catch (Throwable th) {
                this.f10000e.mo3821a(th);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uj$b")
    /* renamed from: com.fossil.blesdk.obfuscated.uj$b */
    public class C3042b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C3369yl f10002e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.lang.String f10003f;

        @DexIgnore
        public C3042b(com.fossil.blesdk.obfuscated.C3369yl ylVar, java.lang.String str) {
            this.f10002e = ylVar;
            this.f10003f = str;
        }

        @DexIgnore
        @android.annotation.SuppressLint({"SyntheticAccessor"})
        public void run() {
            try {
                androidx.work.ListenableWorker.C0354a aVar = (androidx.work.ListenableWorker.C0354a) this.f10002e.get();
                if (aVar == null) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(com.fossil.blesdk.obfuscated.C3040uj.f9981w, java.lang.String.format("%s returned a null result. Treating it as a failure.", new java.lang.Object[]{com.fossil.blesdk.obfuscated.C3040uj.this.f9986i.f5773c}), new java.lang.Throwable[0]);
                } else {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C3040uj.f9981w, java.lang.String.format("%s returned a %s result.", new java.lang.Object[]{com.fossil.blesdk.obfuscated.C3040uj.this.f9986i.f5773c, aVar}), new java.lang.Throwable[0]);
                    com.fossil.blesdk.obfuscated.C3040uj.this.f9988k = aVar;
                }
            } catch (java.util.concurrent.CancellationException e) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(com.fossil.blesdk.obfuscated.C3040uj.f9981w, java.lang.String.format("%s was cancelled", new java.lang.Object[]{this.f10003f}), e);
            } catch (java.lang.InterruptedException | java.util.concurrent.ExecutionException e2) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(com.fossil.blesdk.obfuscated.C3040uj.f9981w, java.lang.String.format("%s failed because it threw an exception/error", new java.lang.Object[]{this.f10003f}), e2);
            } catch (Throwable th) {
                com.fossil.blesdk.obfuscated.C3040uj.this.mo16785b();
                throw th;
            }
            com.fossil.blesdk.obfuscated.C3040uj.this.mo16785b();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uj$c")
    /* renamed from: com.fossil.blesdk.obfuscated.uj$c */
    public static class C3043c {

        @DexIgnore
        /* renamed from: a */
        public android.content.Context f10005a;

        @DexIgnore
        /* renamed from: b */
        public androidx.work.ListenableWorker f10006b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C3444zl f10007c;

        @DexIgnore
        /* renamed from: d */
        public com.fossil.blesdk.obfuscated.C3290xi f10008d;

        @DexIgnore
        /* renamed from: e */
        public androidx.work.impl.WorkDatabase f10009e;

        @DexIgnore
        /* renamed from: f */
        public java.lang.String f10010f;

        @DexIgnore
        /* renamed from: g */
        public java.util.List<com.fossil.blesdk.obfuscated.C2656pj> f10011g;

        @DexIgnore
        /* renamed from: h */
        public androidx.work.WorkerParameters.C0359a f10012h; // = new androidx.work.WorkerParameters.C0359a();

        @DexIgnore
        public C3043c(android.content.Context context, com.fossil.blesdk.obfuscated.C3290xi xiVar, com.fossil.blesdk.obfuscated.C3444zl zlVar, androidx.work.impl.WorkDatabase workDatabase, java.lang.String str) {
            this.f10005a = context.getApplicationContext();
            this.f10007c = zlVar;
            this.f10008d = xiVar;
            this.f10009e = workDatabase;
            this.f10010f = str;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3040uj.C3043c mo16799a(java.util.List<com.fossil.blesdk.obfuscated.C2656pj> list) {
            this.f10011g = list;
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3040uj.C3043c mo16798a(androidx.work.WorkerParameters.C0359a aVar) {
            if (aVar != null) {
                this.f10012h = aVar;
            }
            return this;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3040uj mo16800a() {
            return new com.fossil.blesdk.obfuscated.C3040uj(this);
        }
    }

    @DexIgnore
    public C3040uj(com.fossil.blesdk.obfuscated.C3040uj.C3043c cVar) {
        this.f9982e = cVar.f10005a;
        this.f9990m = cVar.f10007c;
        this.f9983f = cVar.f10010f;
        this.f9984g = cVar.f10011g;
        this.f9985h = cVar.f10012h;
        this.f9987j = cVar.f10006b;
        this.f9989l = cVar.f10008d;
        this.f9991n = cVar.f10009e;
        this.f9992o = this.f9991n.mo3780d();
        this.f9993p = this.f9991n.mo3777a();
        this.f9994q = this.f9991n.mo3781e();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.zv1<java.lang.Boolean> mo16780a() {
        return this.f9997t;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16785b() {
        boolean z = false;
        if (!mo16793i()) {
            this.f9991n.beginTransaction();
            try {
                androidx.work.WorkInfo.State d = this.f9992o.mo12028d(this.f9983f);
                if (d == null) {
                    mo16786b(false);
                    z = true;
                } else if (d == androidx.work.WorkInfo.State.RUNNING) {
                    mo16782a(this.f9988k);
                    z = this.f9992o.mo12028d(this.f9983f).isFinished();
                } else if (!d.isFinished()) {
                    mo16787c();
                }
                this.f9991n.setTransactionSuccessful();
            } finally {
                this.f9991n.endTransaction();
            }
        }
        java.util.List<com.fossil.blesdk.obfuscated.C2656pj> list = this.f9984g;
        if (list != null) {
            if (z) {
                for (com.fossil.blesdk.obfuscated.C2656pj a : list) {
                    a.mo9222a(this.f9983f);
                }
            }
            com.fossil.blesdk.obfuscated.C2744qj.m12826a(this.f9989l, this.f9991n, this.f9984g);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo16787c() {
        this.f9991n.beginTransaction();
        try {
            this.f9992o.mo12015a(androidx.work.WorkInfo.State.ENQUEUED, this.f9983f);
            this.f9992o.mo12024b(this.f9983f, java.lang.System.currentTimeMillis());
            this.f9992o.mo12016a(this.f9983f, -1);
            this.f9991n.setTransactionSuccessful();
        } finally {
            this.f9991n.endTransaction();
            mo16786b(true);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo16788d() {
        this.f9991n.beginTransaction();
        try {
            this.f9992o.mo12024b(this.f9983f, java.lang.System.currentTimeMillis());
            this.f9992o.mo12015a(androidx.work.WorkInfo.State.ENQUEUED, this.f9983f);
            this.f9992o.mo12030f(this.f9983f);
            this.f9992o.mo12016a(this.f9983f, -1);
            this.f9991n.setTransactionSuccessful();
        } finally {
            this.f9991n.endTransaction();
            mo16786b(false);
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo16789e() {
        androidx.work.WorkInfo.State d = this.f9992o.mo12028d(this.f9983f);
        if (d == androidx.work.WorkInfo.State.RUNNING) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9981w, java.lang.String.format("Status for %s is RUNNING;not doing any work and rescheduling for later execution", new java.lang.Object[]{this.f9983f}), new java.lang.Throwable[0]);
            mo16786b(true);
            return;
        }
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9981w, java.lang.String.format("Status for %s is %s; not doing any work", new java.lang.Object[]{this.f9983f, d}), new java.lang.Throwable[0]);
        mo16786b(false);
    }

    @DexIgnore
    /* renamed from: f */
    public final void mo16790f() {
        com.fossil.blesdk.obfuscated.C1419aj a;
        if (!mo16793i()) {
            this.f9991n.beginTransaction();
            try {
                this.f9986i = this.f9992o.mo12029e(this.f9983f);
                if (this.f9986i == null) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f9981w, java.lang.String.format("Didn't find WorkSpec for id %s", new java.lang.Object[]{this.f9983f}), new java.lang.Throwable[0]);
                    mo16786b(false);
                } else if (this.f9986i.f5772b != androidx.work.WorkInfo.State.ENQUEUED) {
                    mo16789e();
                    this.f9991n.setTransactionSuccessful();
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9981w, java.lang.String.format("%s is not in ENQUEUED state. Nothing more to do.", new java.lang.Object[]{this.f9986i.f5773c}), new java.lang.Throwable[0]);
                    this.f9991n.endTransaction();
                } else {
                    if (this.f9986i.mo11673d() || this.f9986i.mo11672c()) {
                        long currentTimeMillis = java.lang.System.currentTimeMillis();
                        if (!(this.f9986i.f5784n == 0) && currentTimeMillis < this.f9986i.mo11670a()) {
                            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9981w, java.lang.String.format("Delaying execution for %s because it is being executed before schedule.", new java.lang.Object[]{this.f9986i.f5773c}), new java.lang.Throwable[0]);
                            mo16786b(true);
                            this.f9991n.endTransaction();
                            return;
                        }
                    }
                    this.f9991n.setTransactionSuccessful();
                    this.f9991n.endTransaction();
                    if (this.f9986i.mo11673d()) {
                        a = this.f9986i.f5775e;
                    } else {
                        com.fossil.blesdk.obfuscated.C1488bj b = this.f9989l.mo17747c().mo9536b(this.f9986i.f5774d);
                        if (b == null) {
                            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f9981w, java.lang.String.format("Could not create Input Merger %s", new java.lang.Object[]{this.f9986i.f5774d}), new java.lang.Throwable[0]);
                            mo16791g();
                            return;
                        }
                        java.util.ArrayList arrayList = new java.util.ArrayList();
                        arrayList.add(this.f9986i.f5775e);
                        arrayList.addAll(this.f9992o.mo12031g(this.f9983f));
                        a = b.mo3731a((java.util.List<com.fossil.blesdk.obfuscated.C1419aj>) arrayList);
                    }
                    androidx.work.WorkerParameters workerParameters = new androidx.work.WorkerParameters(java.util.UUID.fromString(this.f9983f), a, this.f9995r, this.f9985h, this.f9986i.f5781k, this.f9989l.mo17746b(), this.f9990m, this.f9989l.mo17753i(), new com.fossil.blesdk.obfuscated.C3295xl(this.f9991n, this.f9990m));
                    if (this.f9987j == null) {
                        this.f9987j = this.f9989l.mo17753i().mo13298b(this.f9982e, this.f9986i.f5773c, workerParameters);
                    }
                    androidx.work.ListenableWorker listenableWorker = this.f9987j;
                    if (listenableWorker == null) {
                        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f9981w, java.lang.String.format("Could not create Worker %s", new java.lang.Object[]{this.f9986i.f5773c}), new java.lang.Throwable[0]);
                        mo16791g();
                    } else if (listenableWorker.mo3752g()) {
                        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f9981w, java.lang.String.format("Received an already-used Worker %s; WorkerFactory should return new instances", new java.lang.Object[]{this.f9986i.f5773c}), new java.lang.Throwable[0]);
                        mo16791g();
                    } else {
                        this.f9987j.mo3753i();
                        if (!mo16794j()) {
                            mo16789e();
                        } else if (!mo16793i()) {
                            com.fossil.blesdk.obfuscated.C3369yl e = com.fossil.blesdk.obfuscated.C3369yl.m16890e();
                            this.f9990m.mo8787a().execute(new com.fossil.blesdk.obfuscated.C3040uj.C3041a(e));
                            e.mo3818a((java.lang.Runnable) new com.fossil.blesdk.obfuscated.C3040uj.C3042b(e, this.f9996s), (java.util.concurrent.Executor) this.f9990m.mo8789b());
                        }
                    }
                }
            } finally {
                this.f9991n.endTransaction();
            }
        }
    }

    @DexIgnore
    /* renamed from: g */
    public void mo16791g() {
        this.f9991n.beginTransaction();
        try {
            mo16783a(this.f9983f);
            this.f9992o.mo12021a(this.f9983f, ((androidx.work.ListenableWorker.C0354a.C0355a) this.f9988k).mo3755d());
            this.f9991n.setTransactionSuccessful();
        } finally {
            this.f9991n.endTransaction();
            mo16786b(false);
        }
    }

    @DexIgnore
    /* renamed from: h */
    public final void mo16792h() {
        this.f9991n.beginTransaction();
        try {
            this.f9992o.mo12015a(androidx.work.WorkInfo.State.SUCCEEDED, this.f9983f);
            this.f9992o.mo12021a(this.f9983f, ((androidx.work.ListenableWorker.C0354a.C0357c) this.f9988k).mo3762d());
            long currentTimeMillis = java.lang.System.currentTimeMillis();
            for (java.lang.String next : this.f9993p.mo8779a(this.f9983f)) {
                if (this.f9992o.mo12028d(next) == androidx.work.WorkInfo.State.BLOCKED && this.f9993p.mo8781b(next)) {
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(f9981w, java.lang.String.format("Setting status to enqueued for %s", new java.lang.Object[]{next}), new java.lang.Throwable[0]);
                    this.f9992o.mo12015a(androidx.work.WorkInfo.State.ENQUEUED, next);
                    this.f9992o.mo12024b(next, currentTimeMillis);
                }
            }
            this.f9991n.setTransactionSuccessful();
        } finally {
            this.f9991n.endTransaction();
            mo16786b(false);
        }
    }

    @DexIgnore
    /* renamed from: i */
    public final boolean mo16793i() {
        if (!this.f9999v) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9981w, java.lang.String.format("Work interrupted for %s", new java.lang.Object[]{this.f9996s}), new java.lang.Throwable[0]);
        androidx.work.WorkInfo.State d = this.f9992o.mo12028d(this.f9983f);
        if (d == null) {
            mo16786b(false);
        } else {
            mo16786b(!d.isFinished());
        }
        return true;
    }

    @DexIgnore
    /* renamed from: j */
    public final boolean mo16794j() {
        this.f9991n.beginTransaction();
        try {
            boolean z = true;
            if (this.f9992o.mo12028d(this.f9983f) == androidx.work.WorkInfo.State.ENQUEUED) {
                this.f9992o.mo12015a(androidx.work.WorkInfo.State.RUNNING, this.f9983f);
                this.f9992o.mo12032h(this.f9983f);
            } else {
                z = false;
            }
            this.f9991n.setTransactionSuccessful();
            return z;
        } finally {
            this.f9991n.endTransaction();
        }
    }

    @DexIgnore
    public void run() {
        this.f9995r = this.f9994q.mo13306a(this.f9983f);
        this.f9996s = mo16781a(this.f9995r);
        mo16790f();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo16784a(boolean z) {
        this.f9999v = true;
        mo16793i();
        com.fossil.blesdk.obfuscated.zv1<androidx.work.ListenableWorker.C0354a> zv1 = this.f9998u;
        if (zv1 != null) {
            zv1.cancel(true);
        }
        androidx.work.ListenableWorker listenableWorker = this.f9987j;
        if (listenableWorker != null) {
            listenableWorker.mo3754k();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16782a(androidx.work.ListenableWorker.C0354a aVar) {
        if (aVar instanceof androidx.work.ListenableWorker.C0354a.C0357c) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(f9981w, java.lang.String.format("Worker result SUCCESS for %s", new java.lang.Object[]{this.f9996s}), new java.lang.Throwable[0]);
            if (this.f9986i.mo11673d()) {
                mo16788d();
            } else {
                mo16792h();
            }
        } else if (aVar instanceof androidx.work.ListenableWorker.C0354a.C0356b) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(f9981w, java.lang.String.format("Worker result RETRY for %s", new java.lang.Object[]{this.f9996s}), new java.lang.Throwable[0]);
            mo16787c();
        } else {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9964c(f9981w, java.lang.String.format("Worker result FAILURE for %s", new java.lang.Object[]{this.f9996s}), new java.lang.Throwable[0]);
            if (this.f9986i.mo11673d()) {
                mo16788d();
            } else {
                mo16791g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x001e A[Catch:{ all -> 0x0039 }] */
    /* renamed from: b */
    public final void mo16786b(boolean z) {
        boolean z2;
        this.f9991n.beginTransaction();
        try {
            java.util.List<java.lang.String> c = this.f9991n.mo3780d().mo12025c();
            if (c != null) {
                if (!c.isEmpty()) {
                    z2 = false;
                    if (z2) {
                        com.fossil.blesdk.obfuscated.C2813rl.m13254a(this.f9982e, androidx.work.impl.background.systemalarm.RescheduleReceiver.class, false);
                    }
                    this.f9991n.setTransactionSuccessful();
                    this.f9991n.endTransaction();
                    this.f9997t.mo3823b(java.lang.Boolean.valueOf(z));
                }
            }
            z2 = true;
            if (z2) {
            }
            this.f9991n.setTransactionSuccessful();
            this.f9991n.endTransaction();
            this.f9997t.mo3823b(java.lang.Boolean.valueOf(z));
        } catch (Throwable th) {
            this.f9991n.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo16783a(java.lang.String str) {
        java.util.LinkedList linkedList = new java.util.LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            java.lang.String str2 = (java.lang.String) linkedList.remove();
            if (this.f9992o.mo12028d(str2) != androidx.work.WorkInfo.State.CANCELLED) {
                this.f9992o.mo12015a(androidx.work.WorkInfo.State.FAILED, str2);
            }
            linkedList.addAll(this.f9993p.mo8779a(str2));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo16781a(java.util.List<java.lang.String> list) {
        java.lang.StringBuilder sb = new java.lang.StringBuilder("Work [ id=");
        sb.append(this.f9983f);
        sb.append(", tags={ ");
        boolean z = true;
        for (java.lang.String next : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(next);
        }
        sb.append(" } ]");
        return sb.toString();
    }
}
