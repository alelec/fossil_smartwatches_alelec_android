package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.qa */
public class C2715qa {

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2469na f8584a; // = new com.fossil.blesdk.obfuscated.C2540oa();

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2638pa f8585b; // = null;

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2638pa m12734a() {
        return f8585b;
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12729a(android.view.LayoutInflater layoutInflater, int i, android.view.ViewGroup viewGroup, boolean z) {
        return m12730a(layoutInflater, i, viewGroup, z, f8585b);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12730a(android.view.LayoutInflater layoutInflater, int i, android.view.ViewGroup viewGroup, boolean z, com.fossil.blesdk.obfuscated.C2638pa paVar) {
        int i2 = 0;
        boolean z2 = viewGroup != null && z;
        if (z2) {
            i2 = viewGroup.getChildCount();
        }
        android.view.View inflate = layoutInflater.inflate(i, viewGroup, z);
        if (z2) {
            return m12732a(paVar, viewGroup, i2, i);
        }
        return m12731a(paVar, inflate, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12733a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View[] viewArr, int i) {
        return f8584a.mo13886a(paVar, viewArr, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12731a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.View view, int i) {
        return f8584a.mo13885a(paVar, view, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12727a(android.app.Activity activity, int i) {
        return m12728a(activity, i, f8585b);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12728a(android.app.Activity activity, int i, com.fossil.blesdk.obfuscated.C2638pa paVar) {
        activity.setContentView(i);
        return m12732a(paVar, (android.view.ViewGroup) activity.getWindow().getDecorView().findViewById(16908290), 0, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends androidx.databinding.ViewDataBinding> T m12732a(com.fossil.blesdk.obfuscated.C2638pa paVar, android.view.ViewGroup viewGroup, int i, int i2) {
        int childCount = viewGroup.getChildCount();
        int i3 = childCount - i;
        if (i3 == 1) {
            return m12731a(paVar, viewGroup.getChildAt(childCount - 1), i2);
        }
        android.view.View[] viewArr = new android.view.View[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            viewArr[i4] = viewGroup.getChildAt(i4 + i);
        }
        return m12733a(paVar, viewArr, i2);
    }
}
