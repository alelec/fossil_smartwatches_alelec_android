package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ot1<F, T> {
    @DexIgnore
    T apply(F f);

    @DexIgnore
    boolean equals(Object obj);
}
