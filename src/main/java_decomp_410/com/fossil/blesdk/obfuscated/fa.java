package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.widget.Filter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fa extends Filter {
    @DexIgnore
    public a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        Cursor a();

        @DexIgnore
        Cursor a(CharSequence charSequence);

        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        CharSequence b(Cursor cursor);
    }

    @DexIgnore
    public fa(a aVar) {
        this.a = aVar;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        return this.a.b((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Cursor a2 = this.a.a(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (a2 != null) {
            filterResults.count = a2.getCount();
            filterResults.values = a2;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Cursor a2 = this.a.a();
        Object obj = filterResults.values;
        if (obj != null && obj != a2) {
            this.a.a((Cursor) obj);
        }
    }
}
