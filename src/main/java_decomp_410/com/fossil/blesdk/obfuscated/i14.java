package com.fossil.blesdk.obfuscated;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i14 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ g14 h;

    @DexIgnore
    public i14(g14 g14, List list, boolean z, boolean z2) {
        this.h = g14;
        this.e = list;
        this.f = z;
        this.g = z2;
    }

    @DexIgnore
    public void run() {
        this.h.a((List<q14>) this.e, this.f);
        if (this.g) {
            this.e.clear();
        }
    }
}
