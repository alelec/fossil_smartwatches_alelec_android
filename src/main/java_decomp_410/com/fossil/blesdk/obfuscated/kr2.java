package com.fossil.blesdk.obfuscated;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.zendesk.sdk.model.request.CustomField;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kr2 extends CoroutineUseCase<c, d, b> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ long h; // = (kd4.a((Object) "release", (Object) "release") ? 25044663 : 360000146526L);
    @DexIgnore
    public static /* final */ long i; // = (kd4.a((Object) "release", (Object) "release") ? 23451689 : 24435966);
    @DexIgnore
    public static /* final */ long j; // = (kd4.a((Object) "release", (Object) "release") ? 360000024083L : 360000148503L);
    @DexIgnore
    public static /* final */ long k; // = (kd4.a((Object) "release", (Object) "release") ? 24416029 : 24436006);
    @DexIgnore
    public static /* final */ long l; // = (kd4.a((Object) "release", (Object) "release") ? 24504683 : 24881463);
    @DexIgnore
    public static /* final */ long m; // = (kd4.a((Object) "release", (Object) "release") ? 23777683 : 24435986);
    @DexIgnore
    public static /* final */ long n; // = (kd4.a((Object) "release", (Object) "release") ? 24545186 : 24881503);
    @DexIgnore
    public static /* final */ long o; // = (kd4.a((Object) "release", (Object) "release") ? 24545246 : 24945726);
    @DexIgnore
    public static /* final */ long p; // = (kd4.a((Object) "release", (Object) "release") ? 23780847 : 24436086);
    @DexIgnore
    public static /* final */ long q; // = (kd4.a((Object) "release", (Object) "release") ? 24935086 : 24881543);
    @DexIgnore
    public static /* final */ long r; // = (kd4.a((Object) "release", (Object) "release") ? 24506223 : 24881523);
    @DexIgnore
    public static /* final */ long s; // = (kd4.a((Object) "release", (Object) "release") ? 360000156783L : 360000156723L);
    @DexIgnore
    public static /* final */ long t; // = (kd4.a((Object) "release", (Object) "release") ? 360000156803L : 360000156743L);
    @DexIgnore
    public static /* final */ long u; // = (kd4.a((Object) "release", (Object) "release") ? 360000156823L : 360000156763L);
    @DexIgnore
    public static /* final */ long v; // = (kd4.a((Object) "release", (Object) "release") ? 60000157103L : 360000148563L);
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ en2 f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public c(String str) {
            kd4.b(str, "subject");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ List<CustomField> a;
        @DexIgnore
        public /* final */ List<String> b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ String f;

        @DexIgnore
        public d(List<CustomField> list, List<String> list2, String str, String str2, String str3, String str4) {
            kd4.b(list, "customFieldList");
            kd4.b(list2, "tags");
            kd4.b(str, "email");
            kd4.b(str2, "userName");
            kd4.b(str3, "subject");
            kd4.b(str4, "additionalInfo");
            this.a = list;
            this.b = list2;
            this.c = str;
            this.d = str2;
            this.e = str3;
            this.f = str4;
        }

        @DexIgnore
        public final String a() {
            return this.f;
        }

        @DexIgnore
        public final List<CustomField> b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final List<String> e() {
            return this.b;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = kr2.class.getSimpleName();
        kd4.a((Object) simpleName, "GetZendeskInformation::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public kr2(UserRepository userRepository, DeviceRepository deviceRepository, en2 en2) {
        kd4.b(userRepository, "mUserRepository");
        kd4.b(deviceRepository, "mDeviceRepository");
        kd4.b(en2, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = deviceRepository;
        this.f = en2;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0069, code lost:
        if (r12 != null) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0233, code lost:
        if (r1 != null) goto L_0x0238;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008f A[SYNTHETIC, Splitter:B:27:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b1 A[SYNTHETIC, Splitter:B:36:0x00b1] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0135  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0264  */
    public Object a(c cVar, yb4<? super qa4> yb4) {
        CustomField customField;
        String str;
        CustomField customField2;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        Object systemService;
        String str8;
        String str9;
        String str10;
        String str11;
        MFUser currentUser = this.d.getCurrentUser();
        if (currentUser == null) {
            a(new b());
            return qa4.a;
        }
        Device deviceBySerial = this.e.getDeviceBySerial(PortfolioApp.W.c().e());
        PortfolioApp c2 = PortfolioApp.W.c();
        CustomField customField3 = new CustomField(dc4.a(h), "");
        CustomField customField4 = new CustomField(dc4.a(i), currentUser.getEmail());
        List d2 = cb4.d("android", "app_feedback");
        try {
            String packageName = c2.getPackageName();
            PackageManager packageManager = c2.getPackageManager();
            PackageInfo packageInfo = packageManager != null ? packageManager.getPackageInfo(packageName, 0) : null;
            if (packageInfo != null) {
                str = packageInfo.versionName;
            }
            str = "";
            try {
                customField = new CustomField(dc4.a(k), str);
                if (packageManager != null) {
                    try {
                        CharSequence applicationLabel = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0));
                        if (applicationLabel != null) {
                            str2 = applicationLabel.toString();
                            if (str2 == null) {
                                try {
                                    customField2 = new CustomField(dc4.a(j), str2);
                                } catch (Exception unused) {
                                    customField2 = null;
                                    FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                                    CustomField customField5 = new CustomField(dc4.a(l), "android");
                                    CustomField customField6 = new CustomField(dc4.a(m), Build.VERSION.RELEASE);
                                    Long a2 = dc4.a(n);
                                    str3 = Build.MANUFACTURER;
                                    if (str3 == null) {
                                    }
                                    CustomField customField7 = new CustomField(a2, str4);
                                    CustomField customField8 = new CustomField(dc4.a(o), Build.MODEL);
                                    str5 = Build.MANUFACTURER;
                                    if (str5 == null) {
                                    }
                                    String str12 = Build.MODEL;
                                    List list = d2;
                                    MFUser mFUser = currentUser;
                                    Long a3 = dc4.a(p);
                                    if (deviceBySerial == null) {
                                    }
                                    CustomField customField9 = new CustomField(a3, str7);
                                    systemService = PortfolioApp.W.c().getSystemService(PlaceFields.PHONE);
                                    if (systemService == null) {
                                    }
                                }
                                try {
                                    String a4 = qf4.a(str2, ' ', '_', false, 4, (Object) null);
                                    if (a4 == null) {
                                        a4 = "";
                                    }
                                    d2.add(a4);
                                } catch (Exception unused2) {
                                    FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                                    CustomField customField52 = new CustomField(dc4.a(l), "android");
                                    CustomField customField62 = new CustomField(dc4.a(m), Build.VERSION.RELEASE);
                                    Long a22 = dc4.a(n);
                                    str3 = Build.MANUFACTURER;
                                    if (str3 == null) {
                                    }
                                    CustomField customField72 = new CustomField(a22, str4);
                                    CustomField customField82 = new CustomField(dc4.a(o), Build.MODEL);
                                    str5 = Build.MANUFACTURER;
                                    if (str5 == null) {
                                    }
                                    String str122 = Build.MODEL;
                                    List list2 = d2;
                                    MFUser mFUser2 = currentUser;
                                    Long a32 = dc4.a(p);
                                    if (deviceBySerial == null) {
                                    }
                                    CustomField customField92 = new CustomField(a32, str7);
                                    systemService = PortfolioApp.W.c().getSystemService(PlaceFields.PHONE);
                                    if (systemService == null) {
                                    }
                                }
                                CustomField customField522 = new CustomField(dc4.a(l), "android");
                                CustomField customField622 = new CustomField(dc4.a(m), Build.VERSION.RELEASE);
                                Long a222 = dc4.a(n);
                                str3 = Build.MANUFACTURER;
                                if (str3 == null) {
                                    str4 = str3;
                                } else {
                                    str4 = "";
                                }
                                CustomField customField722 = new CustomField(a222, str4);
                                CustomField customField822 = new CustomField(dc4.a(o), Build.MODEL);
                                str5 = Build.MANUFACTURER;
                                if (str5 == null) {
                                    str5 = "";
                                }
                                String str1222 = Build.MODEL;
                                List list22 = d2;
                                MFUser mFUser22 = currentUser;
                                Long a322 = dc4.a(p);
                                if (deviceBySerial == null) {
                                    str6 = str1222;
                                    str7 = deviceBySerial.getDeviceId();
                                } else {
                                    str6 = str1222;
                                    str7 = null;
                                }
                                CustomField customField922 = new CustomField(a322, str7);
                                systemService = PortfolioApp.W.c().getSystemService(PlaceFields.PHONE);
                                if (systemService == null) {
                                    TelephonyManager telephonyManager = (TelephonyManager) systemService;
                                    String str13 = str5;
                                    String str14 = str;
                                    CustomField customField10 = new CustomField(dc4.a(q), telephonyManager.getNetworkOperatorName());
                                    Long a5 = dc4.a(r);
                                    if (deviceBySerial != null) {
                                        str8 = str2;
                                        str9 = deviceBySerial.getFirmwareRevision();
                                    } else {
                                        str8 = str2;
                                        str9 = null;
                                    }
                                    CustomField customField11 = new CustomField(a5, str9);
                                    Locale locale = Locale.getDefault();
                                    String deviceId = deviceBySerial != null ? deviceBySerial.getDeviceId() : null;
                                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                                    Long a6 = dc4.a(s);
                                    String str15 = deviceId;
                                    StringBuilder sb = new StringBuilder();
                                    CustomField customField12 = customField2;
                                    kd4.a((Object) locale, "currentLocale");
                                    sb.append(locale.getLanguage());
                                    sb.append('-');
                                    sb.append(locale.getScript());
                                    List d3 = cb4.d(customField3, customField4, customField522, customField622, customField722, customField822, customField922, customField10, customField11, new CustomField(a6, sb.toString()), new CustomField(dc4.a(t), locale.getCountry()), new CustomField(dc4.a(u), ""), new CustomField(dc4.a(v), ButtonService.Companion.getSDKVersion()));
                                    if (customField != null) {
                                        dc4.a(d3.add(customField));
                                    }
                                    if (customField12 != null) {
                                        dc4.a(d3.add(customField12));
                                    }
                                    StringBuilder a7 = a(str8, str14, str15, str13, str6, networkOperatorName);
                                    if (cVar != null) {
                                        str10 = cVar.a();
                                    }
                                    str10 = "Feedback - From app [Fossil] - [Android]";
                                    String email = mFUser22.getEmail();
                                    kd4.a((Object) email, "user.email");
                                    String username = mFUser22.getUsername();
                                    kd4.a((Object) username, "user.username");
                                    String sb2 = a7.toString();
                                    kd4.a((Object) sb2, "additionalInfo.toString()");
                                    a(new d(d3, list22, email, username, str10, sb2));
                                    return qa4.a;
                                }
                                throw new TypeCastException("null cannot be cast to non-null type android.telephony.TelephonyManager");
                            }
                            kd4.a();
                            throw null;
                        }
                    } catch (Exception unused3) {
                        str2 = "";
                        customField2 = null;
                        FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                        CustomField customField5222 = new CustomField(dc4.a(l), "android");
                        CustomField customField6222 = new CustomField(dc4.a(m), Build.VERSION.RELEASE);
                        Long a2222 = dc4.a(n);
                        str3 = Build.MANUFACTURER;
                        if (str3 == null) {
                        }
                        CustomField customField7222 = new CustomField(a2222, str4);
                        CustomField customField8222 = new CustomField(dc4.a(o), Build.MODEL);
                        str5 = Build.MANUFACTURER;
                        if (str5 == null) {
                        }
                        String str12222 = Build.MODEL;
                        List list222 = d2;
                        MFUser mFUser222 = currentUser;
                        Long a3222 = dc4.a(p);
                        if (deviceBySerial == null) {
                        }
                        CustomField customField9222 = new CustomField(a3222, str7);
                        systemService = PortfolioApp.W.c().getSystemService(PlaceFields.PHONE);
                        if (systemService == null) {
                        }
                    }
                }
                str2 = null;
                if (str2 == null) {
                }
            } catch (Exception unused4) {
                str11 = "";
                customField2 = null;
                customField = null;
                FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
                CustomField customField52222 = new CustomField(dc4.a(l), "android");
                CustomField customField62222 = new CustomField(dc4.a(m), Build.VERSION.RELEASE);
                Long a22222 = dc4.a(n);
                str3 = Build.MANUFACTURER;
                if (str3 == null) {
                }
                CustomField customField72222 = new CustomField(a22222, str4);
                CustomField customField82222 = new CustomField(dc4.a(o), Build.MODEL);
                str5 = Build.MANUFACTURER;
                if (str5 == null) {
                }
                String str122222 = Build.MODEL;
                List list2222 = d2;
                MFUser mFUser2222 = currentUser;
                Long a32222 = dc4.a(p);
                if (deviceBySerial == null) {
                }
                CustomField customField92222 = new CustomField(a32222, str7);
                systemService = PortfolioApp.W.c().getSystemService(PlaceFields.PHONE);
                if (systemService == null) {
                }
            }
        } catch (Exception unused5) {
            str11 = "";
            str = str11;
            customField2 = null;
            customField = null;
            FLogger.INSTANCE.getLocal().e(g, "Cannot load package info; will not be saved to installation");
            CustomField customField522222 = new CustomField(dc4.a(l), "android");
            CustomField customField622222 = new CustomField(dc4.a(m), Build.VERSION.RELEASE);
            Long a222222 = dc4.a(n);
            str3 = Build.MANUFACTURER;
            if (str3 == null) {
            }
            CustomField customField722222 = new CustomField(a222222, str4);
            CustomField customField822222 = new CustomField(dc4.a(o), Build.MODEL);
            str5 = Build.MANUFACTURER;
            if (str5 == null) {
            }
            String str1222222 = Build.MODEL;
            List list22222 = d2;
            MFUser mFUser22222 = currentUser;
            Long a322222 = dc4.a(p);
            if (deviceBySerial == null) {
            }
            CustomField customField922222 = new CustomField(a322222, str7);
            systemService = PortfolioApp.W.c().getSystemService(PlaceFields.PHONE);
            if (systemService == null) {
            }
        }
    }

    @DexIgnore
    public final StringBuilder a(String str, String str2, String str3, String str4, String str5, String str6) {
        StringBuilder sb = new StringBuilder();
        String str7 = Build.DEVICE + " - " + Build.MODEL;
        String str8 = Build.VERSION.RELEASE;
        StringBuilder sb2 = new StringBuilder();
        Locale locale = Locale.getDefault();
        kd4.a((Object) locale, "Locale.getDefault()");
        sb2.append(locale.getLanguage());
        sb2.append("_t");
        String sb3 = sb2.toString();
        String str9 = "";
        String str10 = str9;
        String str11 = str10;
        for (Device device : this.e.getAllDevice()) {
            str10 = str10 + " " + device.getDeviceId();
            if (device.isActive()) {
                long g2 = this.f.g(device.getDeviceId());
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str11);
                pd4 pd4 = pd4.a;
                Object[] objArr = {Long.valueOf(g2 / ((long) 1000)), rk2.c(new Date(g2))};
                String format = String.format(" %d (%s)", Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                sb4.append(format);
                str11 = sb4.toString();
            }
        }
        sb.append("App Name: " + str);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("App Version: " + str2);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Build number: " + "23949-2019-10-29");
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Phone Info: " + str7);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("System version: " + str8);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Host Maker: " + str4);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Host Model: " + str5);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Carrier: " + str6);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Language code: " + sb3);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("_______________");
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        StringBuilder sb5 = new StringBuilder();
        sb5.append("Serial ");
        if (str3 != null) {
            str9 = str3;
        }
        sb5.append(str9);
        sb.append(sb5.toString());
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("Last successful sync: " + str11);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        sb.append("List of paired device: " + str10);
        kd4.a((Object) sb, "append(value)");
        mf4.a(sb);
        return sb;
    }
}
