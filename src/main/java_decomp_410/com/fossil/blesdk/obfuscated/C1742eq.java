package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.eq */
public class C1742eq implements com.fossil.blesdk.obfuscated.C2428mp, com.fossil.blesdk.obfuscated.C2902so.C2903a<java.lang.Object>, com.fossil.blesdk.obfuscated.C2428mp.C2429a {

    @DexIgnore
    /* renamed from: e */
    public /* final */ com.fossil.blesdk.obfuscated.C2499np<?> f4903e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C2428mp.C2429a f4904f;

    @DexIgnore
    /* renamed from: g */
    public int f4905g;

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C2144jp f4906h;

    @DexIgnore
    /* renamed from: i */
    public java.lang.Object f4907i;

    @DexIgnore
    /* renamed from: j */
    public volatile com.fossil.blesdk.obfuscated.C2912sr.C2913a<?> f4908j;

    @DexIgnore
    /* renamed from: k */
    public com.fossil.blesdk.obfuscated.C2235kp f4909k;

    @DexIgnore
    public C1742eq(com.fossil.blesdk.obfuscated.C2499np<?> npVar, com.fossil.blesdk.obfuscated.C2428mp.C2429a aVar) {
        this.f4903e = npVar;
        this.f4904f = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9253a() {
        java.lang.Object obj = this.f4907i;
        if (obj != null) {
            this.f4907i = null;
            mo10565b(obj);
        }
        com.fossil.blesdk.obfuscated.C2144jp jpVar = this.f4906h;
        if (jpVar != null && jpVar.mo9253a()) {
            return true;
        }
        this.f4906h = null;
        this.f4908j = null;
        boolean z = false;
        while (!z && mo10566b()) {
            java.util.List<com.fossil.blesdk.obfuscated.C2912sr.C2913a<?>> g = this.f4903e.mo14066g();
            int i = this.f4905g;
            this.f4905g = i + 1;
            this.f4908j = g.get(i);
            if (this.f4908j != null && (this.f4903e.mo14064e().mo14813a(this.f4908j.f9451c.mo8872b()) || this.f4903e.mo14062c(this.f4908j.f9451c.getDataClass()))) {
                this.f4908j.f9451c.mo8870a(this.f4903e.mo14069j(), this);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    /* renamed from: b */
    public final boolean mo10566b() {
        return this.f4905g < this.f4903e.mo14066g().size();
    }

    @DexIgnore
    public void cancel() {
        com.fossil.blesdk.obfuscated.C2912sr.C2913a<?> aVar = this.f4908j;
        if (aVar != null) {
            aVar.f9451c.cancel();
        }
    }

    @DexIgnore
    /* renamed from: j */
    public void mo3960j() {
        throw new java.lang.UnsupportedOperationException();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public final void mo10565b(java.lang.Object obj) {
        long a = com.fossil.blesdk.obfuscated.C2682pw.m12452a();
        try {
            com.fossil.blesdk.obfuscated.C1963ho<X> a2 = this.f4903e.mo14051a(obj);
            com.fossil.blesdk.obfuscated.C2338lp lpVar = new com.fossil.blesdk.obfuscated.C2338lp(a2, obj, this.f4903e.mo14068i());
            this.f4909k = new com.fossil.blesdk.obfuscated.C2235kp(this.f4908j.f9449a, this.f4903e.mo14071l());
            this.f4903e.mo14063d().mo16536a(this.f4909k, lpVar);
            if (android.util.Log.isLoggable("SourceGenerator", 2)) {
                android.util.Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.f4909k + ", data: " + obj + ", encoder: " + a2 + ", duration: " + com.fossil.blesdk.obfuscated.C2682pw.m12451a(a));
            }
            this.f4908j.f9451c.mo8869a();
            this.f4906h = new com.fossil.blesdk.obfuscated.C2144jp(java.util.Collections.singletonList(this.f4908j.f9449a), this.f4903e, this);
        } catch (Throwable th) {
            this.f4908j.f9451c.mo8869a();
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9252a(java.lang.Object obj) {
        com.fossil.blesdk.obfuscated.C2663pp e = this.f4903e.mo14064e();
        if (obj == null || !e.mo14813a(this.f4908j.f9451c.mo8872b())) {
            this.f4904f.mo3952a(this.f4908j.f9449a, obj, this.f4908j.f9451c, this.f4908j.f9451c.mo8872b(), this.f4909k);
            return;
        }
        this.f4907i = obj;
        this.f4904f.mo3960j();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9251a(java.lang.Exception exc) {
        this.f4904f.mo3951a(this.f4909k, exc, this.f4908j.f9451c, this.f4908j.f9451c.mo8872b());
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3952a(com.fossil.blesdk.obfuscated.C2143jo joVar, java.lang.Object obj, com.fossil.blesdk.obfuscated.C2902so<?> soVar, com.bumptech.glide.load.DataSource dataSource, com.fossil.blesdk.obfuscated.C2143jo joVar2) {
        this.f4904f.mo3952a(joVar, obj, soVar, this.f4908j.f9451c.mo8872b(), joVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo3951a(com.fossil.blesdk.obfuscated.C2143jo joVar, java.lang.Exception exc, com.fossil.blesdk.obfuscated.C2902so<?> soVar, com.bumptech.glide.load.DataSource dataSource) {
        this.f4904f.mo3951a(joVar, exc, soVar, this.f4908j.f9451c.mo8872b());
    }
}
