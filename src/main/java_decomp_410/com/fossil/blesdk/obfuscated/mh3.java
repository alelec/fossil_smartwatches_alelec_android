package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.google.android.material.textfield.TextInputEditText;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.enums.Gender;
import com.portfolio.platform.enums.Unit;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Date;
import java.util.HashMap;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mh3 extends zr2 implements ws3.g {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((fd4) null);
    @DexIgnore
    public ProfileEditViewModel j;
    @DexIgnore
    public tr3<af2> k;
    @DexIgnore
    public fk2 l;
    @DexIgnore
    public j42 m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final mh3 a() {
            return new mh3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public b(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.b(Gender.OTHER);
            mh3.d(this.e).a(Gender.OTHER);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements cc<ProfileEditViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 a;

        @DexIgnore
        public c(mh3 mh3) {
            this.a = mh3;
        }

        @DexIgnore
        public final void a(ProfileEditViewModel.b bVar) {
            MFUser f = bVar.f();
            if (f != null) {
                this.a.c(f);
            }
            Boolean g = bVar.g();
            if (g != null) {
                this.a.O(g.booleanValue());
            }
            Uri a2 = bVar.a();
            if (a2 != null) {
                this.a.a(a2);
            }
            if (bVar.c()) {
                this.a.e();
            } else {
                this.a.d();
            }
            if (bVar.e() != null) {
                this.a.n();
            }
            Pair<Integer, String> d = bVar.d();
            if (d != null) {
                this.a.a(d.getFirst().intValue(), d.getSecond());
            }
            if (bVar.b()) {
                this.a.o();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public d(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.S0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public e(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0030  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Editable editable;
            String valueOf;
            ProfileEditViewModel d = mh3.d(this.e);
            af2 af2 = (af2) mh3.c(this.e).a();
            if (af2 != null) {
                TextInputEditText textInputEditText = af2.r;
                if (textInputEditText != null) {
                    editable = textInputEditText.getEditableText();
                    valueOf = String.valueOf(editable);
                    if (valueOf == null) {
                        d.b(StringsKt__StringsKt.d(valueOf).toString());
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            editable = null;
            valueOf = String.valueOf(editable);
            if (valueOf == null) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public f(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0030  */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Editable editable;
            String valueOf;
            ProfileEditViewModel d = mh3.d(this.e);
            af2 af2 = (af2) mh3.c(this.e).a();
            if (af2 != null) {
                TextInputEditText textInputEditText = af2.s;
                if (textInputEditText != null) {
                    editable = textInputEditText.getEditableText();
                    valueOf = String.valueOf(editable);
                    if (valueOf == null) {
                        d.c(StringsKt__StringsKt.d(valueOf).toString());
                        return;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            editable = null;
            valueOf = String.valueOf(editable);
            if (valueOf == null) {
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public g(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.T0();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public h(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.doCameraTask();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements gu3 {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 a;
        @DexIgnore
        public /* final */ /* synthetic */ af2 b;

        @DexIgnore
        public i(mh3 mh3, af2 af2) {
            this.a = mh3;
            this.b = af2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.b.z;
            kd4.a((Object) rulerValuePicker, "binding.rvpHeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                mh3.d(this.a).a(i);
                return;
            }
            mh3.d(this.a).a(Math.round(pk2.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements gu3 {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 a;
        @DexIgnore
        public /* final */ /* synthetic */ af2 b;

        @DexIgnore
        public j(mh3 mh3, af2 af2) {
            this.a = mh3;
            this.b = af2;
        }

        @DexIgnore
        public void a(int i) {
            RulerValuePicker rulerValuePicker = this.b.A;
            kd4.a((Object) rulerValuePicker, "binding.rvpWeight");
            if (rulerValuePicker.getUnit() == Unit.METRIC) {
                mh3.d(this.a).b(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            mh3.d(this.a).b(Math.round(pk2.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        public void a(boolean z) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public k(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.b(Gender.MALE);
            mh3.d(this.e).a(Gender.MALE);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mh3 e;

        @DexIgnore
        public l(mh3 mh3) {
            this.e = mh3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.b(Gender.FEMALE);
            mh3.d(this.e).a(Gender.FEMALE);
        }
    }

    /*
    static {
        String simpleName = mh3.class.getSimpleName();
        kd4.a((Object) simpleName, "ProfileEditFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ tr3 c(mh3 mh3) {
        tr3<af2> tr3 = mh3.k;
        if (tr3 != null) {
            return tr3;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProfileEditViewModel d(mh3 mh3) {
        ProfileEditViewModel profileEditViewModel = mh3.j;
        if (profileEditViewModel != null) {
            return profileEditViewModel;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @nq4(1222)
    public final void doCameraTask() {
        if (bn2.a(bn2.d, getActivity(), "EDIT_AVATAR", false, 4, (Object) null)) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Intent b2 = al2.b(activity);
                if (b2 != null) {
                    startActivityForResult(b2, 1234);
                }
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
        if ((kotlin.text.StringsKt__StringsKt.d(r6).length() > 0) != false) goto L_0x0057;
     */
    @DexIgnore
    public final void O(boolean z) {
        tr3<af2> tr3 = this.k;
        if (tr3 != null) {
            af2 a2 = tr3.a();
            if (a2 != null) {
                ProgressButton progressButton = a2.B;
                kd4.a((Object) progressButton, "it.save");
                boolean z2 = true;
                if (z) {
                    TextInputEditText textInputEditText = a2.r;
                    kd4.a((Object) textInputEditText, "it.etFirstName");
                    Editable editableText = textInputEditText.getEditableText();
                    kd4.a((Object) editableText, "it.etFirstName.editableText");
                    if (StringsKt__StringsKt.d(editableText).length() > 0) {
                        TextInputEditText textInputEditText2 = a2.s;
                        kd4.a((Object) textInputEditText2, "it.etLastName");
                        Editable editableText2 = textInputEditText2.getEditableText();
                        kd4.a((Object) editableText2, "it.etLastName.editableText");
                    }
                }
                z2 = false;
                progressButton.setEnabled(z2);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public boolean S0() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        kd4.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            ProfileEditViewModel profileEditViewModel = this.j;
                            if (profileEditViewModel != null) {
                                if (profileEditViewModel.e()) {
                                    ds3 ds3 = ds3.c;
                                    FragmentManager childFragmentManager = getChildFragmentManager();
                                    kd4.a((Object) childFragmentManager, "childFragmentManager");
                                    ds3.I(childFragmentManager);
                                } else {
                                    FragmentActivity activity3 = getActivity();
                                    if (activity3 != null) {
                                        activity3.finish();
                                    }
                                }
                                return true;
                            }
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        }
        return true;
    }

    @DexIgnore
    public final void T0() {
        ProfileEditViewModel profileEditViewModel = this.j;
        if (profileEditViewModel != null) {
            profileEditViewModel.g();
        } else {
            kd4.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                if (!activity.isFinishing()) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        kd4.a((Object) activity2, "activity!!");
                        if (!activity2.isDestroyed()) {
                            FragmentActivity activity3 = getActivity();
                            if (activity3 != null) {
                                activity3.finish();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void o() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.k(childFragmentManager);
        }
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            ProfileEditViewModel profileEditViewModel = this.j;
            if (profileEditViewModel != null) {
                profileEditViewModel.a(intent);
            } else {
                kd4.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        af2 af2 = (af2) qa.a(LayoutInflater.from(getContext()), R.layout.fragment_profile_edit, (ViewGroup) null, false, O0());
        PortfolioApp.W.c().g().a(new ph3()).a(this);
        j42 j42 = this.m;
        if (j42 != null) {
            ic a2 = lc.a((Fragment) this, (kc.b) j42).a(ProfileEditViewModel.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
            this.j = (ProfileEditViewModel) a2;
            af2.y.setOnClickListener(new d(this));
            af2.r.addTextChangedListener(new e(this));
            af2.s.addTextChangedListener(new f(this));
            af2.B.setOnClickListener(new g(this));
            af2.q.setOnClickListener(new h(this));
            af2.z.setValuePickerListener(new i(this, af2));
            af2.A.setValuePickerListener(new j(this, af2));
            af2.u.setOnClickListener(new k(this));
            af2.t.setOnClickListener(new l(this));
            af2.v.setOnClickListener(new b(this));
            ProfileEditViewModel profileEditViewModel = this.j;
            if (profileEditViewModel != null) {
                profileEditViewModel.c().a(getViewLifecycleOwner(), new c(this));
                this.k = new tr3<>(this, af2);
                kd4.a((Object) af2, "binding");
                return af2.d();
            }
            kd4.d("mViewModel");
            throw null;
        }
        kd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        vl2 Q0 = Q0();
        if (Q0 != null) {
            Q0.a("");
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        ProfileEditViewModel profileEditViewModel = this.j;
        if (profileEditViewModel != null) {
            profileEditViewModel.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("edit_profile_view");
        fk2 a2 = ck2.a((Fragment) this);
        kd4.a((Object) a2, "GlideApp.with(this)");
        this.l = a2;
    }

    @DexIgnore
    public final void b(int i2, Unit unit) {
        int i3 = nh3.b[unit.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = o;
            local.d(str, "updateData weight=" + i2 + " metric");
            tr3<af2> tr3 = this.k;
            if (tr3 != null) {
                af2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.x;
                    kd4.a((Object) flexibleTextView, "it.ftvWeightUnit");
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Kg));
                    RulerValuePicker rulerValuePicker = a2.A;
                    kd4.a((Object) rulerValuePicker, "it.rvpWeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.A.setFormatter(new ProfileFormatter(4));
                    a2.A.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = o;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            tr3<af2> tr32 = this.k;
            if (tr32 != null) {
                af2 a3 = tr32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.x;
                    kd4.a((Object) flexibleTextView2, "it.ftvWeightUnit");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ProfileSetup_AdditionalInformation_Label__Lbs));
                    float f2 = pk2.f((float) i2);
                    RulerValuePicker rulerValuePicker2 = a3.A;
                    kd4.a((Object) rulerValuePicker2, "it.rvpWeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.A.setFormatter(new ProfileFormatter(4));
                    a3.A.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @SuppressLint({"SimpleDateFormat"})
    public final void c(MFUser mFUser) {
        String profilePicture = mFUser.getProfilePicture();
        String str = mFUser.getFirstName() + " " + mFUser.getLastName();
        if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
            fk2 fk2 = this.l;
            if (fk2 != null) {
                ek2<Drawable> a2 = fk2.a((Object) new bk2("", str)).a((lv<?>) new rv().a((oo<Bitmap>) new nk2()));
                tr3<af2> tr3 = this.k;
                if (tr3 != null) {
                    af2 a3 = tr3.a();
                    FossilCircleImageView fossilCircleImageView = a3 != null ? a3.q : null;
                    if (fossilCircleImageView != null) {
                        a2.a((ImageView) fossilCircleImageView);
                        tr3<af2> tr32 = this.k;
                        if (tr32 != null) {
                            af2 a4 = tr32.a();
                            if (a4 != null) {
                                FossilCircleImageView fossilCircleImageView2 = a4.q;
                                if (fossilCircleImageView2 != null) {
                                    fossilCircleImageView2.setBorderColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.gray));
                                }
                            }
                            tr3<af2> tr33 = this.k;
                            if (tr33 != null) {
                                af2 a5 = tr33.a();
                                if (a5 != null) {
                                    FossilCircleImageView fossilCircleImageView3 = a5.q;
                                    if (fossilCircleImageView3 != null) {
                                        fossilCircleImageView3.setBorderWidth(3);
                                    }
                                }
                                tr3<af2> tr34 = this.k;
                                if (tr34 != null) {
                                    af2 a6 = tr34.a();
                                    if (a6 != null) {
                                        FossilCircleImageView fossilCircleImageView4 = a6.q;
                                        if (fossilCircleImageView4 != null) {
                                            fossilCircleImageView4.setBackground(k6.c(PortfolioApp.W.c(), R.drawable.oval_solid_light_grey));
                                        }
                                    }
                                } else {
                                    kd4.d("mBinding");
                                    throw null;
                                }
                            } else {
                                kd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            kd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            } else {
                kd4.d("mGlideRequests");
                throw null;
            }
        } else {
            tr3<af2> tr35 = this.k;
            if (tr35 != null) {
                af2 a7 = tr35.a();
                if (a7 != null) {
                    FossilCircleImageView fossilCircleImageView5 = a7.q;
                    if (fossilCircleImageView5 != null) {
                        fk2 fk22 = this.l;
                        if (fk22 != null) {
                            fossilCircleImageView5.a(fk22, profilePicture, str);
                        } else {
                            kd4.d("mGlideRequests");
                            throw null;
                        }
                    }
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
        tr3<af2> tr36 = this.k;
        if (tr36 != null) {
            af2 a8 = tr36.a();
            if (a8 != null) {
                TextInputEditText textInputEditText = a8.r;
                if (textInputEditText != null) {
                    textInputEditText.setText(mFUser.getFirstName());
                }
            }
            tr3<af2> tr37 = this.k;
            if (tr37 != null) {
                af2 a9 = tr37.a();
                if (a9 != null) {
                    TextInputEditText textInputEditText2 = a9.s;
                    if (textInputEditText2 != null) {
                        textInputEditText2.setText(mFUser.getLastName());
                    }
                }
                tr3<af2> tr38 = this.k;
                if (tr38 != null) {
                    af2 a10 = tr38.a();
                    if (a10 != null) {
                        FlexibleTextView flexibleTextView = a10.D;
                        if (flexibleTextView != null) {
                            flexibleTextView.setText(mFUser.getEmail());
                        }
                    }
                    mk2 mk2 = mk2.a;
                    Gender gender = mFUser.getGender();
                    kd4.a((Object) gender, "user.gender");
                    Pair<Integer, Integer> a11 = mk2.a(gender, MFUser.getAge(mFUser.getBirthday()));
                    if (mFUser.getHeightInCentimeters() == 0) {
                        mFUser.setHeightInCentimeters(a11.getFirst().intValue());
                    }
                    if (mFUser.getHeightInCentimeters() > 0) {
                        int heightInCentimeters = mFUser.getHeightInCentimeters();
                        Unit heightUnit = mFUser.getHeightUnit();
                        kd4.a((Object) heightUnit, "user.heightUnit");
                        a(heightInCentimeters, heightUnit);
                    }
                    if (mFUser.getWeightInGrams() == 0) {
                        mFUser.setWeightInGrams(a11.getSecond().intValue() * 1000);
                    }
                    if (mFUser.getWeightInGrams() > 0) {
                        int weightInGrams = mFUser.getWeightInGrams();
                        Unit weightUnit = mFUser.getWeightUnit();
                        kd4.a((Object) weightUnit, "user.weightUnit");
                        b(weightInGrams, weightUnit);
                    }
                    String birthday = mFUser.getBirthday();
                    if (!TextUtils.isEmpty(birthday)) {
                        try {
                            Date e2 = rk2.e(birthday);
                            tr3<af2> tr39 = this.k;
                            if (tr39 != null) {
                                af2 a12 = tr39.a();
                                if (a12 != null) {
                                    FlexibleTextView flexibleTextView2 = a12.C;
                                    if (flexibleTextView2 != null) {
                                        flexibleTextView2.setText(rk2.b(e2));
                                    }
                                }
                            } else {
                                kd4.d("mBinding");
                                throw null;
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                    Gender gender2 = mFUser.getGender();
                    kd4.a((Object) gender2, "user.gender");
                    b(gender2);
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void d() {
        tr3<af2> tr3 = this.k;
        if (tr3 != null) {
            af2 a2 = tr3.a();
            if (a2 != null) {
                ProgressButton progressButton = a2.B;
                if (progressButton != null) {
                    progressButton.b();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e() {
        tr3<af2> tr3 = this.k;
        if (tr3 != null) {
            af2 a2 = tr3.a();
            if (a2 != null) {
                ProgressButton progressButton = a2.B;
                if (progressButton != null) {
                    progressButton.c();
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(int i2, Unit unit) {
        int i3 = nh3.a[unit.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = o;
            local.d(str, "updateData height=" + i2 + " metric");
            tr3<af2> tr3 = this.k;
            if (tr3 != null) {
                af2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.w;
                    kd4.a((Object) flexibleTextView, "it.ftvHeightUnit");
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_PreferredUnits_ChooseUnits_Label__Cm));
                    RulerValuePicker rulerValuePicker = a2.z;
                    kd4.a((Object) rulerValuePicker, "it.rvpHeight");
                    rulerValuePicker.setUnit(Unit.METRIC);
                    a2.z.setFormatter(new ProfileFormatter(-1));
                    a2.z.a(100, 251, i2);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = o;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            tr3<af2> tr32 = this.k;
            if (tr32 != null) {
                af2 a3 = tr32.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.w;
                    kd4.a((Object) flexibleTextView2, "it.ftvHeightUnit");
                    flexibleTextView2.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_PreferredUnits_ChooseUnits_Label__Ft));
                    Pair<Integer, Integer> b2 = pk2.b((float) i2);
                    RulerValuePicker rulerValuePicker2 = a3.z;
                    kd4.a((Object) rulerValuePicker2, "it.rvpHeight");
                    rulerValuePicker2.setUnit(Unit.IMPERIAL);
                    a3.z.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker3 = a3.z;
                    Integer first = b2.getFirst();
                    kd4.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = pk2.a(first.intValue());
                    Integer second = b2.getSecond();
                    kd4.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker3.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void b(Gender gender) {
        tr3<af2> tr3 = this.k;
        if (tr3 != null) {
            af2 a2 = tr3.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.W.c();
                int a3 = k6.a((Context) c2, (int) R.color.primaryColor);
                int a4 = k6.a((Context) c2, (int) R.color.surface);
                Drawable c3 = k6.c(c2, R.drawable.bg_border_disable_color_primary);
                Drawable c4 = k6.c(c2, R.drawable.bg_border_active_color_primary);
                a2.t.setTextColor(a3);
                a2.u.setTextColor(a3);
                a2.v.setTextColor(a3);
                FlexibleButton flexibleButton = a2.t;
                kd4.a((Object) flexibleButton, "it.fbFemale");
                flexibleButton.setBackground(c3);
                FlexibleButton flexibleButton2 = a2.u;
                kd4.a((Object) flexibleButton2, "it.fbMale");
                flexibleButton2.setBackground(c3);
                FlexibleButton flexibleButton3 = a2.v;
                kd4.a((Object) flexibleButton3, "it.fbOther");
                flexibleButton3.setBackground(c3);
                int i2 = nh3.c[gender.ordinal()];
                if (i2 == 1) {
                    tr3<af2> tr32 = this.k;
                    if (tr32 != null) {
                        af2 a5 = tr32.a();
                        if (a5 != null) {
                            FlexibleButton flexibleButton4 = a5.t;
                            if (flexibleButton4 != null) {
                                flexibleButton4.setTextColor(a4);
                                flexibleButton4.setBackground(c4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    kd4.d("mBinding");
                    throw null;
                } else if (i2 != 2) {
                    tr3<af2> tr33 = this.k;
                    if (tr33 != null) {
                        af2 a6 = tr33.a();
                        if (a6 != null) {
                            FlexibleButton flexibleButton5 = a6.v;
                            if (flexibleButton5 != null) {
                                flexibleButton5.setTextColor(a4);
                                flexibleButton5.setBackground(c4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    kd4.d("mBinding");
                    throw null;
                } else {
                    tr3<af2> tr34 = this.k;
                    if (tr34 != null) {
                        af2 a7 = tr34.a();
                        if (a7 != null) {
                            FlexibleButton flexibleButton6 = a7.u;
                            if (flexibleButton6 != null) {
                                flexibleButton6.setTextColor(a4);
                                flexibleButton6.setBackground(c4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Uri uri) {
        if (isActive()) {
            fk2 fk2 = this.l;
            if (fk2 != null) {
                ek2<Drawable> a2 = fk2.a(uri).a(pp.a).a((lv<?>) new rv().a((oo<Bitmap>) new nk2()));
                tr3<af2> tr3 = this.k;
                if (tr3 != null) {
                    af2 a3 = tr3.a();
                    FossilCircleImageView fossilCircleImageView = a3 != null ? a3.q : null;
                    if (fossilCircleImageView != null) {
                        a2.a((ImageView) fossilCircleImageView);
                        O(true);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.d("mGlideRequests");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        kd4.b(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).a(str, i2, intent);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == R.id.tv_cancel) {
            n();
        } else if (i2 == R.id.tv_ok) {
            T0();
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }
}
