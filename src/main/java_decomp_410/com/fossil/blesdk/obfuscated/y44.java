package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface y44 {
    @DexIgnore
    void a(int i, String str, String str2);

    @DexIgnore
    void a(int i, String str, String str2, boolean z);

    @DexIgnore
    void a(String str, String str2, Throwable th);

    @DexIgnore
    boolean a(String str, int i);

    @DexIgnore
    void b(String str, String str2, Throwable th);

    @DexIgnore
    void d(String str, String str2);

    @DexIgnore
    void e(String str, String str2);

    @DexIgnore
    void e(String str, String str2, Throwable th);

    @DexIgnore
    void i(String str, String str2);

    @DexIgnore
    void v(String str, String str2);

    @DexIgnore
    void w(String str, String str2);
}
