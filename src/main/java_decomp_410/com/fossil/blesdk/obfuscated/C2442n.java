package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n */
public class C2442n {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n$a")
    /* renamed from: com.fossil.blesdk.obfuscated.n$a */
    public static final class C2443a {
        @DexIgnore
        /* renamed from: a */
        public static java.lang.String m10916a(java.lang.Object obj) {
            return ((android.media.session.PlaybackState.CustomAction) obj).getAction();
        }

        @DexIgnore
        /* renamed from: b */
        public static android.os.Bundle m10917b(java.lang.Object obj) {
            return ((android.media.session.PlaybackState.CustomAction) obj).getExtras();
        }

        @DexIgnore
        /* renamed from: c */
        public static int m10918c(java.lang.Object obj) {
            return ((android.media.session.PlaybackState.CustomAction) obj).getIcon();
        }

        @DexIgnore
        /* renamed from: d */
        public static java.lang.CharSequence m10919d(java.lang.Object obj) {
            return ((android.media.session.PlaybackState.CustomAction) obj).getName();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static long m10907a(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getActions();
    }

    @DexIgnore
    /* renamed from: b */
    public static long m10908b(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getActiveQueueItemId();
    }

    @DexIgnore
    /* renamed from: c */
    public static long m10909c(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getBufferedPosition();
    }

    @DexIgnore
    /* renamed from: d */
    public static java.util.List<java.lang.Object> m10910d(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getCustomActions();
    }

    @DexIgnore
    /* renamed from: e */
    public static java.lang.CharSequence m10911e(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getErrorMessage();
    }

    @DexIgnore
    /* renamed from: f */
    public static long m10912f(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getLastPositionUpdateTime();
    }

    @DexIgnore
    /* renamed from: g */
    public static float m10913g(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getPlaybackSpeed();
    }

    @DexIgnore
    /* renamed from: h */
    public static long m10914h(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getPosition();
    }

    @DexIgnore
    /* renamed from: i */
    public static int m10915i(java.lang.Object obj) {
        return ((android.media.session.PlaybackState) obj).getState();
    }
}
