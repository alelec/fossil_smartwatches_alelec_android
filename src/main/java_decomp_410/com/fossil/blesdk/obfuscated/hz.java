package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface hz {
    @DexIgnore
    void a();

    @DexIgnore
    void a(long j, String str);

    @DexIgnore
    oy b();

    @DexIgnore
    byte[] c();

    @DexIgnore
    void d();
}
