package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g6 */
public abstract class C1858g6 extends android.app.Service {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.g6$a")
    /* renamed from: com.fossil.blesdk.obfuscated.g6$a */
    public class C1859a extends com.fossil.blesdk.obfuscated.C1589d.C1590a {
        @DexIgnore
        public C1859a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9725a(java.lang.String str, int i, java.lang.String str2, android.app.Notification notification) throws android.os.RemoteException {
            com.fossil.blesdk.obfuscated.C1858g6.this.mo11143a(android.os.Binder.getCallingUid(), str);
            long clearCallingIdentity = android.os.Binder.clearCallingIdentity();
            try {
                com.fossil.blesdk.obfuscated.C1858g6.this.mo11146a(str, i, str2, notification);
            } finally {
                android.os.Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9724a(java.lang.String str, int i, java.lang.String str2) throws android.os.RemoteException {
            com.fossil.blesdk.obfuscated.C1858g6.this.mo11143a(android.os.Binder.getCallingUid(), str);
            long clearCallingIdentity = android.os.Binder.clearCallingIdentity();
            try {
                com.fossil.blesdk.obfuscated.C1858g6.this.mo11145a(str, i, str2);
            } finally {
                android.os.Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo9723a(java.lang.String str) {
            com.fossil.blesdk.obfuscated.C1858g6.this.mo11143a(android.os.Binder.getCallingUid(), str);
            long clearCallingIdentity = android.os.Binder.clearCallingIdentity();
            try {
                com.fossil.blesdk.obfuscated.C1858g6.this.mo11144a(str);
            } finally {
                android.os.Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11143a(int i, java.lang.String str) {
        java.lang.String[] packagesForUid = getPackageManager().getPackagesForUid(i);
        int length = packagesForUid.length;
        int i2 = 0;
        while (i2 < length) {
            if (!packagesForUid[i2].equals(str)) {
                i2++;
            } else {
                return;
            }
        }
        throw new java.lang.SecurityException("NotificationSideChannelService: Uid " + i + " is not authorized for package " + str);
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11144a(java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11145a(java.lang.String str, int i, java.lang.String str2);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo11146a(java.lang.String str, int i, java.lang.String str2, android.app.Notification notification);

    @DexIgnore
    public android.os.IBinder onBind(android.content.Intent intent) {
        if (!intent.getAction().equals("android.support.BIND_NOTIFICATION_SIDE_CHANNEL") || android.os.Build.VERSION.SDK_INT > 19) {
            return null;
        }
        return new com.fossil.blesdk.obfuscated.C1858g6.C1859a();
    }
}
