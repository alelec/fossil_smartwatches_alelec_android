package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ec */
public class C1718ec implements androidx.lifecycle.LifecycleOwner {

    @DexIgnore
    /* renamed from: m */
    public static /* final */ com.fossil.blesdk.obfuscated.C1718ec f4763m; // = new com.fossil.blesdk.obfuscated.C1718ec();

    @DexIgnore
    /* renamed from: e */
    public int f4764e; // = 0;

    @DexIgnore
    /* renamed from: f */
    public int f4765f; // = 0;

    @DexIgnore
    /* renamed from: g */
    public boolean f4766g; // = true;

    @DexIgnore
    /* renamed from: h */
    public boolean f4767h; // = true;

    @DexIgnore
    /* renamed from: i */
    public android.os.Handler f4768i;

    @DexIgnore
    /* renamed from: j */
    public /* final */ androidx.lifecycle.LifecycleRegistry f4769j; // = new androidx.lifecycle.LifecycleRegistry(this);

    @DexIgnore
    /* renamed from: k */
    public java.lang.Runnable f4770k; // = new com.fossil.blesdk.obfuscated.C1718ec.C1719a();

    @DexIgnore
    /* renamed from: l */
    public com.fossil.blesdk.obfuscated.C1785fc.C1786a f4771l; // = new com.fossil.blesdk.obfuscated.C1718ec.C1720b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ec$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ec$a */
    public class C1719a implements java.lang.Runnable {
        @DexIgnore
        public C1719a() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C1718ec.this.mo10442e();
            com.fossil.blesdk.obfuscated.C1718ec.this.mo10443f();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ec$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ec$b */
    public class C1720b implements com.fossil.blesdk.obfuscated.C1785fc.C1786a {
        @DexIgnore
        public C1720b() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo10445a() {
            com.fossil.blesdk.obfuscated.C1718ec.this.mo10440c();
        }

        @DexIgnore
        /* renamed from: d */
        public void mo10446d() {
            com.fossil.blesdk.obfuscated.C1718ec.this.mo10439b();
        }

        @DexIgnore
        /* renamed from: e */
        public void mo10447e() {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ec$c")
    /* renamed from: com.fossil.blesdk.obfuscated.ec$c */
    public class C1721c extends com.fossil.blesdk.obfuscated.C2716qb {
        @DexIgnore
        public C1721c() {
        }

        @DexIgnore
        public void onActivityCreated(android.app.Activity activity, android.os.Bundle bundle) {
            com.fossil.blesdk.obfuscated.C1785fc.m6900a(activity).mo10839d(com.fossil.blesdk.obfuscated.C1718ec.this.f4771l);
        }

        @DexIgnore
        public void onActivityPaused(android.app.Activity activity) {
            com.fossil.blesdk.obfuscated.C1718ec.this.mo10437a();
        }

        @DexIgnore
        public void onActivityStopped(android.app.Activity activity) {
            com.fossil.blesdk.obfuscated.C1718ec.this.mo10441d();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6407b(android.content.Context context) {
        f4763m.mo10438a(context);
    }

    @DexIgnore
    /* renamed from: g */
    public static androidx.lifecycle.LifecycleOwner m6408g() {
        return f4763m;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10437a() {
        this.f4765f--;
        if (this.f4765f == 0) {
            this.f4768i.postDelayed(this.f4770k, 700);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo10440c() {
        this.f4764e++;
        if (this.f4764e == 1 && this.f4767h) {
            this.f4769j.mo2263a(androidx.lifecycle.Lifecycle.Event.ON_START);
            this.f4767h = false;
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10441d() {
        this.f4764e--;
        mo10443f();
    }

    @DexIgnore
    /* renamed from: e */
    public void mo10442e() {
        if (this.f4765f == 0) {
            this.f4766g = true;
            this.f4769j.mo2263a(androidx.lifecycle.Lifecycle.Event.ON_PAUSE);
        }
    }

    @DexIgnore
    /* renamed from: f */
    public void mo10443f() {
        if (this.f4764e == 0 && this.f4766g) {
            this.f4769j.mo2263a(androidx.lifecycle.Lifecycle.Event.ON_STOP);
            this.f4767h = true;
        }
    }

    @DexIgnore
    public androidx.lifecycle.Lifecycle getLifecycle() {
        return this.f4769j;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo10439b() {
        this.f4765f++;
        if (this.f4765f != 1) {
            return;
        }
        if (this.f4766g) {
            this.f4769j.mo2263a(androidx.lifecycle.Lifecycle.Event.ON_RESUME);
            this.f4766g = false;
            return;
        }
        this.f4768i.removeCallbacks(this.f4770k);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo10438a(android.content.Context context) {
        this.f4768i = new android.os.Handler();
        this.f4769j.mo2263a(androidx.lifecycle.Lifecycle.Event.ON_CREATE);
        ((android.app.Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new com.fossil.blesdk.obfuscated.C1718ec.C1721c());
    }
}
