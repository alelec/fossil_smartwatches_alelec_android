package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.iq */
public abstract class C2056iq<T extends com.fossil.blesdk.obfuscated.C2825rq> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Queue<T> f6164a; // = com.fossil.blesdk.obfuscated.C3066uw.m14928a(20);

    @DexIgnore
    /* renamed from: a */
    public abstract T mo11718a();

    @DexIgnore
    /* renamed from: a */
    public void mo12068a(T t) {
        if (this.f6164a.size() < 20) {
            this.f6164a.offer(t);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public T mo12069b() {
        T t = (com.fossil.blesdk.obfuscated.C2825rq) this.f6164a.poll();
        return t == null ? mo11718a() : t;
    }
}
