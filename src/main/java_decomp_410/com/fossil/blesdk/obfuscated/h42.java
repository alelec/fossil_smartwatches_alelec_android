package com.fossil.blesdk.obfuscated;

import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class h42 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler e; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            kd4.b(runnable, Constants.COMMAND);
            this.e.post(runnable);
        }
    }

    @DexIgnore
    public h42(Executor executor, Executor executor2, Executor executor3) {
        this.a = executor;
        this.b = executor2;
    }

    @DexIgnore
    public Executor a() {
        return this.a;
    }

    @DexIgnore
    public Executor b() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public h42() {
        this(r0, r1, new a());
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        kd4.a((Object) newSingleThreadExecutor, "Executors.newSingleThreadExecutor()");
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(3);
        kd4.a((Object) newFixedThreadPool, "Executors.newFixedThreadPool(3)");
    }
}
