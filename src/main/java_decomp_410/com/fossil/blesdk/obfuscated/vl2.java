package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vl2 extends ul2 {
    @DexIgnore
    public String i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vl2(AnalyticsHelper analyticsHelper, String str, String str2) {
        super(analyticsHelper, str, str2);
        kd4.b(analyticsHelper, "analyticsHelper");
        kd4.b(str, "traceName");
        hp4.a(kd4.a((Object) str, (Object) "view_appearance"), "traceName should be view_appearance", new Object[0]);
    }

    @DexIgnore
    public final boolean a(vl2 vl2) {
        if (vl2 == null) {
            return false;
        }
        return kd4.a((Object) vl2.i, (Object) this.i);
    }

    @DexIgnore
    public final vl2 b(String str) {
        kd4.b(str, "viewName");
        this.i = str;
        a("view_name", str);
        return this;
    }

    @DexIgnore
    public final String e() {
        return this.i;
    }

    @DexIgnore
    public String toString() {
        return "View name: " + this.i + ", running: " + b();
    }
}
