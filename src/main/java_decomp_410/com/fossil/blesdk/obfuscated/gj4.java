package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gj4<T> extends ak4<T> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gj4(CoroutineContext coroutineContext, yb4<? super T> yb4) {
        super(coroutineContext, yb4);
        kd4.b(coroutineContext, "context");
        kd4.b(yb4, "uCont");
    }

    @DexIgnore
    public int j() {
        return 3;
    }
}
