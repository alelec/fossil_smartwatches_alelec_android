package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zv2 implements MembersInjector<NotificationAppsActivity> {
    @DexIgnore
    public static void a(NotificationAppsActivity notificationAppsActivity, NotificationAppsPresenter notificationAppsPresenter) {
        notificationAppsActivity.B = notificationAppsPresenter;
    }
}
