package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class he2 extends ge2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        B.put(R.id.ftv_title, 1);
        B.put(R.id.ftv_everyone, 2);
        B.put(R.id.iv_everyone_check, 3);
        B.put(R.id.v_line1, 4);
        B.put(R.id.ftv_favorite_contacts, 5);
        B.put(R.id.iv_favorite_contacts_check, 6);
        B.put(R.id.v_line2, 7);
        B.put(R.id.ftv_no_one, 8);
        B.put(R.id.iv_no_one_check, 9);
        B.put(R.id.v_line3, 10);
        B.put(R.id.iv_close, 11);
    }
    */

    @DexIgnore
    public he2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 12, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public he2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[2], objArr[5], objArr[8], objArr[1], objArr[11], objArr[3], objArr[6], objArr[9], objArr[4], objArr[7], objArr[10]);
        this.z = -1;
        this.y = objArr[0];
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
