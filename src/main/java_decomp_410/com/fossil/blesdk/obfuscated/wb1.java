package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.vb1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wb1<M extends vb1<M>, T> {
    @DexIgnore
    public /* final */ Class<T> a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public final int a(Object obj) {
        throw null;
    }

    @DexIgnore
    public final T a(List<cc1> list) {
        throw null;
    }

    @DexIgnore
    public final void a(Object obj, ub1 ub1) {
        throw null;
    }
}
