package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u {
    @DexIgnore
    public static /* final */ int abc_action_bar_content_inset_material; // = 2131165184;
    @DexIgnore
    public static /* final */ int abc_action_bar_content_inset_with_nav; // = 2131165185;
    @DexIgnore
    public static /* final */ int abc_action_bar_default_height_material; // = 2131165186;
    @DexIgnore
    public static /* final */ int abc_action_bar_default_padding_end_material; // = 2131165187;
    @DexIgnore
    public static /* final */ int abc_action_bar_default_padding_start_material; // = 2131165188;
    @DexIgnore
    public static /* final */ int abc_action_bar_elevation_material; // = 2131165189;
    @DexIgnore
    public static /* final */ int abc_action_bar_icon_vertical_padding_material; // = 2131165190;
    @DexIgnore
    public static /* final */ int abc_action_bar_overflow_padding_end_material; // = 2131165191;
    @DexIgnore
    public static /* final */ int abc_action_bar_overflow_padding_start_material; // = 2131165192;
    @DexIgnore
    public static /* final */ int abc_action_bar_stacked_max_height; // = 2131165193;
    @DexIgnore
    public static /* final */ int abc_action_bar_stacked_tab_max_width; // = 2131165194;
    @DexIgnore
    public static /* final */ int abc_action_bar_subtitle_bottom_margin_material; // = 2131165195;
    @DexIgnore
    public static /* final */ int abc_action_bar_subtitle_top_margin_material; // = 2131165196;
    @DexIgnore
    public static /* final */ int abc_action_button_min_height_material; // = 2131165197;
    @DexIgnore
    public static /* final */ int abc_action_button_min_width_material; // = 2131165198;
    @DexIgnore
    public static /* final */ int abc_action_button_min_width_overflow_material; // = 2131165199;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_bar_height; // = 2131165200;
    @DexIgnore
    public static /* final */ int abc_alert_dialog_button_dimen; // = 2131165201;
    @DexIgnore
    public static /* final */ int abc_button_inset_horizontal_material; // = 2131165202;
    @DexIgnore
    public static /* final */ int abc_button_inset_vertical_material; // = 2131165203;
    @DexIgnore
    public static /* final */ int abc_button_padding_horizontal_material; // = 2131165204;
    @DexIgnore
    public static /* final */ int abc_button_padding_vertical_material; // = 2131165205;
    @DexIgnore
    public static /* final */ int abc_cascading_menus_min_smallest_width; // = 2131165206;
    @DexIgnore
    public static /* final */ int abc_config_prefDialogWidth; // = 2131165207;
    @DexIgnore
    public static /* final */ int abc_control_corner_material; // = 2131165208;
    @DexIgnore
    public static /* final */ int abc_control_inset_material; // = 2131165209;
    @DexIgnore
    public static /* final */ int abc_control_padding_material; // = 2131165210;
    @DexIgnore
    public static /* final */ int abc_dialog_corner_radius_material; // = 2131165211;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_height_major; // = 2131165212;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_height_minor; // = 2131165213;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_width_major; // = 2131165214;
    @DexIgnore
    public static /* final */ int abc_dialog_fixed_width_minor; // = 2131165215;
    @DexIgnore
    public static /* final */ int abc_dialog_list_padding_bottom_no_buttons; // = 2131165216;
    @DexIgnore
    public static /* final */ int abc_dialog_list_padding_top_no_title; // = 2131165217;
    @DexIgnore
    public static /* final */ int abc_dialog_min_width_major; // = 2131165218;
    @DexIgnore
    public static /* final */ int abc_dialog_min_width_minor; // = 2131165219;
    @DexIgnore
    public static /* final */ int abc_dialog_padding_material; // = 2131165220;
    @DexIgnore
    public static /* final */ int abc_dialog_padding_top_material; // = 2131165221;
    @DexIgnore
    public static /* final */ int abc_dialog_title_divider_material; // = 2131165222;
    @DexIgnore
    public static /* final */ int abc_disabled_alpha_material_dark; // = 2131165223;
    @DexIgnore
    public static /* final */ int abc_disabled_alpha_material_light; // = 2131165224;
    @DexIgnore
    public static /* final */ int abc_dropdownitem_icon_width; // = 2131165225;
    @DexIgnore
    public static /* final */ int abc_dropdownitem_text_padding_left; // = 2131165226;
    @DexIgnore
    public static /* final */ int abc_dropdownitem_text_padding_right; // = 2131165227;
    @DexIgnore
    public static /* final */ int abc_edit_text_inset_bottom_material; // = 2131165228;
    @DexIgnore
    public static /* final */ int abc_edit_text_inset_horizontal_material; // = 2131165229;
    @DexIgnore
    public static /* final */ int abc_edit_text_inset_top_material; // = 2131165230;
    @DexIgnore
    public static /* final */ int abc_floating_window_z; // = 2131165231;
    @DexIgnore
    public static /* final */ int abc_list_item_padding_horizontal_material; // = 2131165232;
    @DexIgnore
    public static /* final */ int abc_panel_menu_list_width; // = 2131165233;
    @DexIgnore
    public static /* final */ int abc_progress_bar_height_material; // = 2131165234;
    @DexIgnore
    public static /* final */ int abc_search_view_preferred_height; // = 2131165235;
    @DexIgnore
    public static /* final */ int abc_search_view_preferred_width; // = 2131165236;
    @DexIgnore
    public static /* final */ int abc_seekbar_track_background_height_material; // = 2131165237;
    @DexIgnore
    public static /* final */ int abc_seekbar_track_progress_height_material; // = 2131165238;
    @DexIgnore
    public static /* final */ int abc_select_dialog_padding_start_material; // = 2131165239;
    @DexIgnore
    public static /* final */ int abc_switch_padding; // = 2131165240;
    @DexIgnore
    public static /* final */ int abc_text_size_body_1_material; // = 2131165241;
    @DexIgnore
    public static /* final */ int abc_text_size_body_2_material; // = 2131165242;
    @DexIgnore
    public static /* final */ int abc_text_size_button_material; // = 2131165243;
    @DexIgnore
    public static /* final */ int abc_text_size_caption_material; // = 2131165244;
    @DexIgnore
    public static /* final */ int abc_text_size_display_1_material; // = 2131165245;
    @DexIgnore
    public static /* final */ int abc_text_size_display_2_material; // = 2131165246;
    @DexIgnore
    public static /* final */ int abc_text_size_display_3_material; // = 2131165247;
    @DexIgnore
    public static /* final */ int abc_text_size_display_4_material; // = 2131165248;
    @DexIgnore
    public static /* final */ int abc_text_size_headline_material; // = 2131165249;
    @DexIgnore
    public static /* final */ int abc_text_size_large_material; // = 2131165250;
    @DexIgnore
    public static /* final */ int abc_text_size_medium_material; // = 2131165251;
    @DexIgnore
    public static /* final */ int abc_text_size_menu_header_material; // = 2131165252;
    @DexIgnore
    public static /* final */ int abc_text_size_menu_material; // = 2131165253;
    @DexIgnore
    public static /* final */ int abc_text_size_small_material; // = 2131165254;
    @DexIgnore
    public static /* final */ int abc_text_size_subhead_material; // = 2131165255;
    @DexIgnore
    public static /* final */ int abc_text_size_subtitle_material_toolbar; // = 2131165256;
    @DexIgnore
    public static /* final */ int abc_text_size_title_material; // = 2131165257;
    @DexIgnore
    public static /* final */ int abc_text_size_title_material_toolbar; // = 2131165258;
    @DexIgnore
    public static /* final */ int compat_button_inset_horizontal_material; // = 2131165296;
    @DexIgnore
    public static /* final */ int compat_button_inset_vertical_material; // = 2131165297;
    @DexIgnore
    public static /* final */ int compat_button_padding_horizontal_material; // = 2131165298;
    @DexIgnore
    public static /* final */ int compat_button_padding_vertical_material; // = 2131165299;
    @DexIgnore
    public static /* final */ int compat_control_corner_material; // = 2131165300;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_height; // = 2131165301;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_width; // = 2131165302;
    @DexIgnore
    public static /* final */ int disabled_alpha_material_dark; // = 2131165351;
    @DexIgnore
    public static /* final */ int disabled_alpha_material_light; // = 2131165352;
    @DexIgnore
    public static /* final */ int highlight_alpha_material_colored; // = 2131165469;
    @DexIgnore
    public static /* final */ int highlight_alpha_material_dark; // = 2131165470;
    @DexIgnore
    public static /* final */ int highlight_alpha_material_light; // = 2131165471;
    @DexIgnore
    public static /* final */ int hint_alpha_material_dark; // = 2131165472;
    @DexIgnore
    public static /* final */ int hint_alpha_material_light; // = 2131165473;
    @DexIgnore
    public static /* final */ int hint_pressed_alpha_material_dark; // = 2131165474;
    @DexIgnore
    public static /* final */ int hint_pressed_alpha_material_light; // = 2131165475;
    @DexIgnore
    public static /* final */ int notification_action_icon_size; // = 2131165539;
    @DexIgnore
    public static /* final */ int notification_action_text_size; // = 2131165540;
    @DexIgnore
    public static /* final */ int notification_big_circle_margin; // = 2131165541;
    @DexIgnore
    public static /* final */ int notification_content_margin_start; // = 2131165542;
    @DexIgnore
    public static /* final */ int notification_large_icon_height; // = 2131165543;
    @DexIgnore
    public static /* final */ int notification_large_icon_width; // = 2131165544;
    @DexIgnore
    public static /* final */ int notification_main_column_padding_top; // = 2131165545;
    @DexIgnore
    public static /* final */ int notification_media_narrow_margin; // = 2131165546;
    @DexIgnore
    public static /* final */ int notification_right_icon_size; // = 2131165547;
    @DexIgnore
    public static /* final */ int notification_right_side_padding_top; // = 2131165548;
    @DexIgnore
    public static /* final */ int notification_small_icon_background_padding; // = 2131165549;
    @DexIgnore
    public static /* final */ int notification_small_icon_size_as_large; // = 2131165550;
    @DexIgnore
    public static /* final */ int notification_subtext_size; // = 2131165551;
    @DexIgnore
    public static /* final */ int notification_top_pad; // = 2131165552;
    @DexIgnore
    public static /* final */ int notification_top_pad_large_text; // = 2131165553;
    @DexIgnore
    public static /* final */ int tooltip_corner_radius; // = 2131165619;
    @DexIgnore
    public static /* final */ int tooltip_horizontal_padding; // = 2131165620;
    @DexIgnore
    public static /* final */ int tooltip_margin; // = 2131165621;
    @DexIgnore
    public static /* final */ int tooltip_precise_anchor_extra_offset; // = 2131165622;
    @DexIgnore
    public static /* final */ int tooltip_precise_anchor_threshold; // = 2131165623;
    @DexIgnore
    public static /* final */ int tooltip_vertical_padding; // = 2131165624;
    @DexIgnore
    public static /* final */ int tooltip_y_offset_non_touch; // = 2131165625;
    @DexIgnore
    public static /* final */ int tooltip_y_offset_touch; // = 2131165626;
}
