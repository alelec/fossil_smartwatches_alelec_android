package com.fossil.blesdk.obfuscated;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lo implements jo {
    @DexIgnore
    public /* final */ g4<ko<?>, Object> b; // = new lw();

    @DexIgnore
    public void a(lo loVar) {
        this.b.a(loVar.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof lo) {
            return this.b.equals(((lo) obj).b);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Options{values=" + this.b + '}';
    }

    @DexIgnore
    public <T> lo a(ko<T> koVar, T t) {
        this.b.put(koVar, t);
        return this;
    }

    @DexIgnore
    public <T> T a(ko<T> koVar) {
        return this.b.containsKey(koVar) ? this.b.get(koVar) : koVar.a();
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            a(this.b.c(i), this.b.e(i), messageDigest);
        }
    }

    @DexIgnore
    public static <T> void a(ko<T> koVar, Object obj, MessageDigest messageDigest) {
        koVar.a(obj, messageDigest);
    }
}
