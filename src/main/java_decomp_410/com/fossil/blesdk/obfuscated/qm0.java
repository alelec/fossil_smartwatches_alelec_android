package com.fossil.blesdk.obfuscated;

import android.os.Process;
import android.os.StrictMode;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qm0 {
    @DexIgnore
    public static String a;
    @DexIgnore
    public static int b;

    @DexIgnore
    public static String a() {
        if (a == null) {
            if (b == 0) {
                b = Process.myPid();
            }
            a = a(b);
        }
        return a;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.String} */
    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v2, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    public static String a(int i) {
        BufferedReader bufferedReader;
        Object r0 = 0;
        if (i <= 0) {
            return null;
        }
        try {
            StringBuilder sb = new StringBuilder(25);
            sb.append("/proc/");
            sb.append(i);
            sb.append("/cmdline");
            bufferedReader = a(sb.toString());
            try {
                String trim = bufferedReader.readLine().trim();
                om0.a(bufferedReader);
                r0 = trim;
            } catch (IOException unused) {
                om0.a(bufferedReader);
                return r0;
            } catch (Throwable th) {
                Throwable th2 = th;
                r0 = bufferedReader;
                th = th2;
                om0.a(r0);
                throw th;
            }
        } catch (IOException unused2) {
            bufferedReader = null;
            om0.a(bufferedReader);
            return r0;
        } catch (Throwable th3) {
            th = th3;
            om0.a(r0);
            throw th;
        }
        return r0;
    }

    @DexIgnore
    public static BufferedReader a(String str) throws IOException {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return new BufferedReader(new FileReader(str));
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }
}
