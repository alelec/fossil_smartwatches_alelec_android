package com.fossil.blesdk.obfuscated;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.internal.VisibilityAwareImageButton;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gs1 {
    @DexIgnore
    public static /* final */ TimeInterpolator B; // = dr1.c;
    @DexIgnore
    public static /* final */ int[] C; // = {16842919, 16842910};
    @DexIgnore
    public static /* final */ int[] D; // = {16843623, 16842908, 16842910};
    @DexIgnore
    public static /* final */ int[] E; // = {16842908, 16842910};
    @DexIgnore
    public static /* final */ int[] F; // = {16843623, 16842910};
    @DexIgnore
    public static /* final */ int[] G; // = {16842910};
    @DexIgnore
    public static /* final */ int[] H; // = new int[0];
    @DexIgnore
    public ViewTreeObserver.OnPreDrawListener A;
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public Animator b;
    @DexIgnore
    public kr1 c;
    @DexIgnore
    public kr1 d;
    @DexIgnore
    public kr1 e;
    @DexIgnore
    public kr1 f;
    @DexIgnore
    public /* final */ rs1 g;
    @DexIgnore
    public at1 h;
    @DexIgnore
    public float i;
    @DexIgnore
    public Drawable j;
    @DexIgnore
    public Drawable k;
    @DexIgnore
    public is1 l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public float r; // = 1.0f;
    @DexIgnore
    public ArrayList<Animator.AnimatorListener> s;
    @DexIgnore
    public ArrayList<Animator.AnimatorListener> t;
    @DexIgnore
    public /* final */ VisibilityAwareImageButton u;
    @DexIgnore
    public /* final */ bt1 v;
    @DexIgnore
    public /* final */ Rect w; // = new Rect();
    @DexIgnore
    public /* final */ RectF x; // = new RectF();
    @DexIgnore
    public /* final */ RectF y; // = new RectF();
    @DexIgnore
    public /* final */ Matrix z; // = new Matrix();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ g c;

        @DexIgnore
        public a(boolean z, g gVar) {
            this.b = z;
            this.c = gVar;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            gs1 gs1 = gs1.this;
            gs1.a = 0;
            gs1.b = null;
            if (!this.a) {
                gs1.u.a(this.b ? 8 : 4, this.b);
                g gVar = this.c;
                if (gVar != null) {
                    gVar.b();
                }
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            gs1.this.u.a(0, this.b);
            gs1 gs1 = gs1.this;
            gs1.a = 1;
            gs1.b = animator;
            this.a = false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ g b;

        @DexIgnore
        public b(boolean z, g gVar) {
            this.a = z;
            this.b = gVar;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            gs1 gs1 = gs1.this;
            gs1.a = 0;
            gs1.b = null;
            g gVar = this.b;
            if (gVar != null) {
                gVar.a();
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            gs1.this.u.a(0, this.a);
            gs1 gs1 = gs1.this;
            gs1.a = 2;
            gs1.b = animator;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            gs1.this.s();
            return true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends i {
        @DexIgnore
        public d(gs1 gs1) {
            super(gs1, (a) null);
        }

        @DexIgnore
        public float a() {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends i {
        @DexIgnore
        public e() {
            super(gs1.this, (a) null);
        }

        @DexIgnore
        public float a() {
            gs1 gs1 = gs1.this;
            return gs1.n + gs1.o;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends i {
        @DexIgnore
        public f() {
            super(gs1.this, (a) null);
        }

        @DexIgnore
        public float a() {
            gs1 gs1 = gs1.this;
            return gs1.n + gs1.p;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a();

        @DexIgnore
        void b();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends i {
        @DexIgnore
        public h() {
            super(gs1.this, (a) null);
        }

        @DexIgnore
        public float a() {
            return gs1.this.n;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class i extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public abstract float a();

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            gs1.this.h.b(this.c);
            this.a = false;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (!this.a) {
                this.b = gs1.this.h.c();
                this.c = a();
                this.a = true;
            }
            at1 at1 = gs1.this.h;
            float f = this.b;
            at1.b(f + ((this.c - f) * valueAnimator.getAnimatedFraction()));
        }

        @DexIgnore
        public /* synthetic */ i(gs1 gs1, a aVar) {
            this();
        }
    }

    @DexIgnore
    public gs1(VisibilityAwareImageButton visibilityAwareImageButton, bt1 bt1) {
        this.u = visibilityAwareImageButton;
        this.v = bt1;
        this.g = new rs1();
        this.g.a(C, a((i) new f()));
        this.g.a(D, a((i) new e()));
        this.g.a(E, a((i) new e()));
        this.g.a(F, a((i) new e()));
        this.g.a(G, a((i) new h()));
        this.g.a(H, a((i) new d(this)));
        this.i = this.u.getRotation();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i2) {
        Drawable[] drawableArr;
        this.j = c7.i(a());
        c7.a(this.j, colorStateList);
        if (mode != null) {
            c7.a(this.j, mode);
        }
        this.k = c7.i(a());
        c7.a(this.k, zs1.a(colorStateList2));
        if (i2 > 0) {
            this.l = a(i2, colorStateList);
            drawableArr = new Drawable[]{this.l, this.j, this.k};
        } else {
            this.l = null;
            drawableArr = new Drawable[]{this.j, this.k};
        }
        this.m = new LayerDrawable(drawableArr);
        Context context = this.u.getContext();
        Drawable drawable = this.m;
        float b2 = this.v.b();
        float f2 = this.n;
        this.h = new at1(context, drawable, b2, f2, f2 + this.p);
        this.h.a(false);
        this.v.a(this.h);
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        Drawable drawable = this.k;
        if (drawable != null) {
            c7.a(drawable, zs1.a(colorStateList));
        }
    }

    @DexIgnore
    public void b(Rect rect) {
    }

    @DexIgnore
    public final void c(float f2) {
        this.r = f2;
        Matrix matrix = this.z;
        a(f2, matrix);
        this.u.setImageMatrix(matrix);
    }

    @DexIgnore
    public final void d(float f2) {
        if (this.p != f2) {
            this.p = f2;
            a(this.n, this.o, this.p);
        }
    }

    @DexIgnore
    public final kr1 e() {
        if (this.e == null) {
            this.e = kr1.a(this.u.getContext(), sq1.design_fab_show_motion_spec);
        }
        return this.e;
    }

    @DexIgnore
    public float f() {
        return this.n;
    }

    @DexIgnore
    public final kr1 g() {
        return this.d;
    }

    @DexIgnore
    public float h() {
        return this.o;
    }

    @DexIgnore
    public float i() {
        return this.p;
    }

    @DexIgnore
    public final kr1 j() {
        return this.c;
    }

    @DexIgnore
    public boolean k() {
        if (this.u.getVisibility() == 0) {
            if (this.a == 1) {
                return true;
            }
            return false;
        } else if (this.a != 2) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public boolean l() {
        if (this.u.getVisibility() != 0) {
            if (this.a == 2) {
                return true;
            }
            return false;
        } else if (this.a != 1) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public void m() {
        this.g.b();
    }

    @DexIgnore
    public is1 n() {
        return new is1();
    }

    @DexIgnore
    public GradientDrawable o() {
        return new GradientDrawable();
    }

    @DexIgnore
    public void p() {
        if (t()) {
            b();
            this.u.getViewTreeObserver().addOnPreDrawListener(this.A);
        }
    }

    @DexIgnore
    public void q() {
    }

    @DexIgnore
    public void r() {
        if (this.A != null) {
            this.u.getViewTreeObserver().removeOnPreDrawListener(this.A);
            this.A = null;
        }
    }

    @DexIgnore
    public void s() {
        float rotation = this.u.getRotation();
        if (this.i != rotation) {
            this.i = rotation;
            v();
        }
    }

    @DexIgnore
    public boolean t() {
        return true;
    }

    @DexIgnore
    public final boolean u() {
        return f9.z(this.u) && !this.u.isInEditMode();
    }

    @DexIgnore
    public final void v() {
        if (Build.VERSION.SDK_INT == 19) {
            if (this.i % 90.0f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (this.u.getLayerType() != 1) {
                    this.u.setLayerType(1, (Paint) null);
                }
            } else if (this.u.getLayerType() != 0) {
                this.u.setLayerType(0, (Paint) null);
            }
        }
        at1 at1 = this.h;
        if (at1 != null) {
            at1.a(-this.i);
        }
        is1 is1 = this.l;
        if (is1 != null) {
            is1.b(-this.i);
        }
    }

    @DexIgnore
    public final void w() {
        c(this.r);
    }

    @DexIgnore
    public final void x() {
        Rect rect = this.w;
        a(rect);
        b(rect);
        this.v.a(rect.left, rect.top, rect.right, rect.bottom);
    }

    @DexIgnore
    public final void b(float f2) {
        if (this.o != f2) {
            this.o = f2;
            a(this.n, this.o, this.p);
        }
    }

    @DexIgnore
    public void d(Animator.AnimatorListener animatorListener) {
        ArrayList<Animator.AnimatorListener> arrayList = this.s;
        if (arrayList != null) {
            arrayList.remove(animatorListener);
        }
    }

    @DexIgnore
    public void c(Animator.AnimatorListener animatorListener) {
        ArrayList<Animator.AnimatorListener> arrayList = this.t;
        if (arrayList != null) {
            arrayList.remove(animatorListener);
        }
    }

    @DexIgnore
    public final kr1 d() {
        if (this.f == null) {
            this.f = kr1.a(this.u.getContext(), sq1.design_fab_hide_motion_spec);
        }
        return this.f;
    }

    @DexIgnore
    public final void b(kr1 kr1) {
        this.c = kr1;
    }

    @DexIgnore
    public final Drawable c() {
        return this.m;
    }

    @DexIgnore
    public void b(Animator.AnimatorListener animatorListener) {
        if (this.s == null) {
            this.s = new ArrayList<>();
        }
        this.s.add(animatorListener);
    }

    @DexIgnore
    public void b(g gVar, boolean z2) {
        if (!l()) {
            Animator animator = this.b;
            if (animator != null) {
                animator.cancel();
            }
            if (u()) {
                if (this.u.getVisibility() != 0) {
                    this.u.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.u.setScaleY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.u.setScaleX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    c((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                kr1 kr1 = this.c;
                if (kr1 == null) {
                    kr1 = e();
                }
                AnimatorSet a2 = a(kr1, 1.0f, 1.0f, 1.0f);
                a2.addListener(new b(z2, gVar));
                ArrayList<Animator.AnimatorListener> arrayList = this.s;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            this.u.a(0, z2);
            this.u.setAlpha(1.0f);
            this.u.setScaleY(1.0f);
            this.u.setScaleX(1.0f);
            c(1.0f);
            if (gVar != null) {
                gVar.a();
            }
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        Drawable drawable = this.j;
        if (drawable != null) {
            c7.a(drawable, colorStateList);
        }
        is1 is1 = this.l;
        if (is1 != null) {
            is1.a(colorStateList);
        }
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        Drawable drawable = this.j;
        if (drawable != null) {
            c7.a(drawable, mode);
        }
    }

    @DexIgnore
    public final void a(float f2) {
        if (this.n != f2) {
            this.n = f2;
            a(this.n, this.o, this.p);
        }
    }

    @DexIgnore
    public final void a(int i2) {
        if (this.q != i2) {
            this.q = i2;
            w();
        }
    }

    @DexIgnore
    public final void a(float f2, Matrix matrix) {
        matrix.reset();
        Drawable drawable = this.u.getDrawable();
        if (drawable != null && this.q != 0) {
            RectF rectF = this.x;
            RectF rectF2 = this.y;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) drawable.getIntrinsicWidth(), (float) drawable.getIntrinsicHeight());
            int i2 = this.q;
            rectF2.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, (float) i2);
            matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.CENTER);
            int i3 = this.q;
            matrix.postScale(f2, f2, ((float) i3) / 2.0f, ((float) i3) / 2.0f);
        }
    }

    @DexIgnore
    public final void b() {
        if (this.A == null) {
            this.A = new c();
        }
    }

    @DexIgnore
    public final void a(kr1 kr1) {
        this.d = kr1;
    }

    @DexIgnore
    public void a(float f2, float f3, float f4) {
        at1 at1 = this.h;
        if (at1 != null) {
            at1.a(f2, this.p + f2);
            x();
        }
    }

    @DexIgnore
    public void a(int[] iArr) {
        this.g.a(iArr);
    }

    @DexIgnore
    public void a(Animator.AnimatorListener animatorListener) {
        if (this.t == null) {
            this.t = new ArrayList<>();
        }
        this.t.add(animatorListener);
    }

    @DexIgnore
    public void a(g gVar, boolean z2) {
        if (!k()) {
            Animator animator = this.b;
            if (animator != null) {
                animator.cancel();
            }
            if (u()) {
                kr1 kr1 = this.d;
                if (kr1 == null) {
                    kr1 = d();
                }
                AnimatorSet a2 = a(kr1, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                a2.addListener(new a(z2, gVar));
                ArrayList<Animator.AnimatorListener> arrayList = this.t;
                if (arrayList != null) {
                    Iterator<Animator.AnimatorListener> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a2.addListener(it.next());
                    }
                }
                a2.start();
                return;
            }
            this.u.a(z2 ? 8 : 4, z2);
            if (gVar != null) {
                gVar.b();
            }
        }
    }

    @DexIgnore
    public final AnimatorSet a(kr1 kr1, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.u, View.ALPHA, new float[]{f2});
        kr1.a("opacity").a((Animator) ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.u, View.SCALE_X, new float[]{f3});
        kr1.a("scale").a((Animator) ofFloat2);
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.u, View.SCALE_Y, new float[]{f3});
        kr1.a("scale").a((Animator) ofFloat3);
        arrayList.add(ofFloat3);
        a(f4, this.z);
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.u, new ir1(), new jr1(), new Matrix[]{new Matrix(this.z)});
        kr1.a("iconScale").a((Animator) ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        er1.a(animatorSet, arrayList);
        return animatorSet;
    }

    @DexIgnore
    public void a(Rect rect) {
        this.h.getPadding(rect);
    }

    @DexIgnore
    public is1 a(int i2, ColorStateList colorStateList) {
        Context context = this.u.getContext();
        is1 n2 = n();
        n2.a(k6.a(context, uq1.design_fab_stroke_top_outer_color), k6.a(context, uq1.design_fab_stroke_top_inner_color), k6.a(context, uq1.design_fab_stroke_end_inner_color), k6.a(context, uq1.design_fab_stroke_end_outer_color));
        n2.a((float) i2);
        n2.a(colorStateList);
        return n2;
    }

    @DexIgnore
    public GradientDrawable a() {
        GradientDrawable o2 = o();
        o2.setShape(1);
        o2.setColor(-1);
        return o2;
    }

    @DexIgnore
    public final ValueAnimator a(i iVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(B);
        valueAnimator.setDuration(100);
        valueAnimator.addListener(iVar);
        valueAnimator.addUpdateListener(iVar);
        valueAnimator.setFloatValues(new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f});
        return valueAnimator;
    }
}
