package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fb */
public class C1782fb extends androidx.fragment.app.Fragment {
    @DexIgnore
    public static /* final */ int INTERNAL_EMPTY_ID; // = 16711681;
    @DexIgnore
    public static /* final */ int INTERNAL_LIST_CONTAINER_ID; // = 16711683;
    @DexIgnore
    public static /* final */ int INTERNAL_PROGRESS_CONTAINER_ID; // = 16711682;
    @DexIgnore
    public android.widget.ListAdapter mAdapter;
    @DexIgnore
    public java.lang.CharSequence mEmptyText;
    @DexIgnore
    public android.view.View mEmptyView;
    @DexIgnore
    public /* final */ android.os.Handler mHandler; // = new android.os.Handler();
    @DexIgnore
    public android.widget.ListView mList;
    @DexIgnore
    public android.view.View mListContainer;
    @DexIgnore
    public boolean mListShown;
    @DexIgnore
    public /* final */ android.widget.AdapterView.OnItemClickListener mOnClickListener; // = new com.fossil.blesdk.obfuscated.C1782fb.C1784b();
    @DexIgnore
    public android.view.View mProgressContainer;
    @DexIgnore
    public /* final */ java.lang.Runnable mRequestFocus; // = new com.fossil.blesdk.obfuscated.C1782fb.C1783a();
    @DexIgnore
    public android.widget.TextView mStandardEmptyView;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fb$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fb$a */
    public class C1783a implements java.lang.Runnable {
        @DexIgnore
        public C1783a() {
        }

        @DexIgnore
        public void run() {
            android.widget.ListView listView = com.fossil.blesdk.obfuscated.C1782fb.this.mList;
            listView.focusableViewAvailable(listView);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fb$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fb$b */
    public class C1784b implements android.widget.AdapterView.OnItemClickListener {
        @DexIgnore
        public C1784b() {
        }

        @DexIgnore
        public void onItemClick(android.widget.AdapterView<?> adapterView, android.view.View view, int i, long j) {
            com.fossil.blesdk.obfuscated.C1782fb.this.onListItemClick((android.widget.ListView) adapterView, view, i, j);
        }
    }

    @DexIgnore
    private void ensureList() {
        if (this.mList == null) {
            android.view.View view = getView();
            if (view != null) {
                if (view instanceof android.widget.ListView) {
                    this.mList = (android.widget.ListView) view;
                } else {
                    this.mStandardEmptyView = (android.widget.TextView) view.findViewById(INTERNAL_EMPTY_ID);
                    android.widget.TextView textView = this.mStandardEmptyView;
                    if (textView == null) {
                        this.mEmptyView = view.findViewById(16908292);
                    } else {
                        textView.setVisibility(8);
                    }
                    this.mProgressContainer = view.findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
                    this.mListContainer = view.findViewById(INTERNAL_LIST_CONTAINER_ID);
                    android.view.View findViewById = view.findViewById(16908298);
                    if (findViewById instanceof android.widget.ListView) {
                        this.mList = (android.widget.ListView) findViewById;
                        android.view.View view2 = this.mEmptyView;
                        if (view2 != null) {
                            this.mList.setEmptyView(view2);
                        } else {
                            java.lang.CharSequence charSequence = this.mEmptyText;
                            if (charSequence != null) {
                                this.mStandardEmptyView.setText(charSequence);
                                this.mList.setEmptyView(this.mStandardEmptyView);
                            }
                        }
                    } else if (findViewById == null) {
                        throw new java.lang.RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
                    } else {
                        throw new java.lang.RuntimeException("Content has view with id attribute 'android.R.id.list' that is not a ListView class");
                    }
                }
                this.mListShown = true;
                this.mList.setOnItemClickListener(this.mOnClickListener);
                android.widget.ListAdapter listAdapter = this.mAdapter;
                if (listAdapter != null) {
                    this.mAdapter = null;
                    setListAdapter(listAdapter);
                } else if (this.mProgressContainer != null) {
                    setListShown(false, false);
                }
                this.mHandler.post(this.mRequestFocus);
                return;
            }
            throw new java.lang.IllegalStateException("Content view not yet created");
        }
    }

    @DexIgnore
    public android.widget.ListAdapter getListAdapter() {
        return this.mAdapter;
    }

    @DexIgnore
    public android.widget.ListView getListView() {
        ensureList();
        return this.mList;
    }

    @DexIgnore
    public long getSelectedItemId() {
        ensureList();
        return this.mList.getSelectedItemId();
    }

    @DexIgnore
    public int getSelectedItemPosition() {
        ensureList();
        return this.mList.getSelectedItemPosition();
    }

    @DexIgnore
    public android.view.View onCreateView(android.view.LayoutInflater layoutInflater, android.view.ViewGroup viewGroup, android.os.Bundle bundle) {
        throw null;
    }

    @DexIgnore
    public void onDestroyView() {
        this.mHandler.removeCallbacks(this.mRequestFocus);
        this.mList = null;
        this.mListShown = false;
        this.mListContainer = null;
        this.mProgressContainer = null;
        this.mEmptyView = null;
        this.mStandardEmptyView = null;
        super.onDestroyView();
    }

    @DexIgnore
    public void onListItemClick(android.widget.ListView listView, android.view.View view, int i, long j) {
    }

    @DexIgnore
    public void onViewCreated(android.view.View view, android.os.Bundle bundle) {
        super.onViewCreated(view, bundle);
        ensureList();
    }

    @DexIgnore
    public void setEmptyText(java.lang.CharSequence charSequence) {
        ensureList();
        android.widget.TextView textView = this.mStandardEmptyView;
        if (textView != null) {
            textView.setText(charSequence);
            if (this.mEmptyText == null) {
                this.mList.setEmptyView(this.mStandardEmptyView);
            }
            this.mEmptyText = charSequence;
            return;
        }
        throw new java.lang.IllegalStateException("Can't be used with a custom content view");
    }

    @DexIgnore
    public void setListAdapter(android.widget.ListAdapter listAdapter) {
        boolean z = false;
        boolean z2 = this.mAdapter != null;
        this.mAdapter = listAdapter;
        android.widget.ListView listView = this.mList;
        if (listView != null) {
            listView.setAdapter(listAdapter);
            if (!this.mListShown && !z2) {
                if (getView().getWindowToken() != null) {
                    z = true;
                }
                setListShown(true, z);
            }
        }
    }

    @DexIgnore
    public void setListShown(boolean z) {
        setListShown(z, true);
    }

    @DexIgnore
    public void setListShownNoAnimation(boolean z) {
        setListShown(z, false);
    }

    @DexIgnore
    public void setSelection(int i) {
        ensureList();
        this.mList.setSelection(i);
    }

    @DexIgnore
    private void setListShown(boolean z, boolean z2) {
        ensureList();
        android.view.View view = this.mProgressContainer;
        if (view == null) {
            throw new java.lang.IllegalStateException("Can't be used with a custom content view");
        } else if (this.mListShown != z) {
            this.mListShown = z;
            if (z) {
                if (z2) {
                    view.startAnimation(android.view.animation.AnimationUtils.loadAnimation(getContext(), 17432577));
                    this.mListContainer.startAnimation(android.view.animation.AnimationUtils.loadAnimation(getContext(), 17432576));
                } else {
                    view.clearAnimation();
                    this.mListContainer.clearAnimation();
                }
                this.mProgressContainer.setVisibility(8);
                this.mListContainer.setVisibility(0);
                return;
            }
            if (z2) {
                view.startAnimation(android.view.animation.AnimationUtils.loadAnimation(getContext(), 17432576));
                this.mListContainer.startAnimation(android.view.animation.AnimationUtils.loadAnimation(getContext(), 17432577));
            } else {
                view.clearAnimation();
                this.mListContainer.clearAnimation();
            }
            this.mProgressContainer.setVisibility(0);
            this.mListContainer.setVisibility(8);
        }
    }
}
