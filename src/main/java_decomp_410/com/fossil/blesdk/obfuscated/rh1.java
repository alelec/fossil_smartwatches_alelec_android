package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rh1 {
    @DexIgnore
    BroadcastReceiver.PendingResult a();

    @DexIgnore
    void a(Context context, Intent intent);
}
