package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tt0 implements Iterator {
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public /* final */ int f; // = this.g.size();
    @DexIgnore
    public /* final */ /* synthetic */ zzbb g;

    @DexIgnore
    public tt0(zzbb zzbb) {
        this.g = zzbb;
    }

    @DexIgnore
    public final boolean hasNext() {
        return this.e < this.f;
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        return Byte.valueOf(nextByte());
    }

    @DexIgnore
    public final byte nextByte() {
        try {
            zzbb zzbb = this.g;
            int i = this.e;
            this.e = i + 1;
            return zzbb.zzj(i);
        } catch (IndexOutOfBoundsException e2) {
            throw new NoSuchElementException(e2.getMessage());
        }
    }

    @DexIgnore
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
