package com.fossil.blesdk.obfuscated;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class so4 {
    @DexIgnore
    public static /* final */ Logger a; // = Logger.getLogger(so4.class.getName());

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements xo4 {
        @DexIgnore
        public /* final */ /* synthetic */ zo4 e;
        @DexIgnore
        public /* final */ /* synthetic */ OutputStream f;

        @DexIgnore
        public a(zo4 zo4, OutputStream outputStream) {
            this.e = zo4;
            this.f = outputStream;
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            ap4.a(jo4.f, 0, j);
            while (j > 0) {
                this.e.e();
                vo4 vo4 = jo4.e;
                int min = (int) Math.min(j, (long) (vo4.c - vo4.b));
                this.f.write(vo4.a, vo4.b, min);
                vo4.b += min;
                long j2 = (long) min;
                j -= j2;
                jo4.f -= j2;
                if (vo4.b == vo4.c) {
                    jo4.e = vo4.b();
                    wo4.a(vo4);
                }
            }
        }

        @DexIgnore
        public zo4 b() {
            return this.e;
        }

        @DexIgnore
        public void close() throws IOException {
            this.f.close();
        }

        @DexIgnore
        public void flush() throws IOException {
            this.f.flush();
        }

        @DexIgnore
        public String toString() {
            return "sink(" + this.f + ")";
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements xo4 {
        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            jo4.skip(j);
        }

        @DexIgnore
        public zo4 b() {
            return zo4.d;
        }

        @DexIgnore
        public void close() throws IOException {
        }

        @DexIgnore
        public void flush() throws IOException {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ho4 {
        @DexIgnore
        public /* final */ /* synthetic */ Socket k;

        @DexIgnore
        public d(Socket socket) {
            this.k = socket;
        }

        @DexIgnore
        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        public void i() {
            try {
                this.k.close();
            } catch (Exception e) {
                Logger logger = so4.a;
                Level level = Level.WARNING;
                logger.log(level, "Failed to close timed out socket " + this.k, e);
            } catch (AssertionError e2) {
                if (so4.a(e2)) {
                    Logger logger2 = so4.a;
                    Level level2 = Level.WARNING;
                    logger2.log(level2, "Failed to close timed out socket " + this.k, e2);
                    return;
                }
                throw e2;
            }
        }
    }

    @DexIgnore
    public static lo4 a(yo4 yo4) {
        return new uo4(yo4);
    }

    @DexIgnore
    public static xo4 b(File file) throws FileNotFoundException {
        if (file != null) {
            return a((OutputStream) new FileOutputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    @DexIgnore
    public static yo4 c(File file) throws FileNotFoundException {
        if (file != null) {
            return a((InputStream) new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    @DexIgnore
    public static ko4 a(xo4 xo4) {
        return new to4(xo4);
    }

    @DexIgnore
    public static xo4 a(OutputStream outputStream) {
        return a(outputStream, new zo4());
    }

    @DexIgnore
    public static yo4 b(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getInputStream() != null) {
            ho4 c2 = c(socket);
            return c2.a(a(socket.getInputStream(), (zo4) c2));
        } else {
            throw new IOException("socket's input stream == null");
        }
    }

    @DexIgnore
    public static ho4 c(Socket socket) {
        return new d(socket);
    }

    @DexIgnore
    public static xo4 a(OutputStream outputStream, zo4 zo4) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (zo4 != null) {
            return new a(zo4, outputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    @DexIgnore
    public static xo4 a(Socket socket) throws IOException {
        if (socket == null) {
            throw new IllegalArgumentException("socket == null");
        } else if (socket.getOutputStream() != null) {
            ho4 c2 = c(socket);
            return c2.a(a(socket.getOutputStream(), (zo4) c2));
        } else {
            throw new IOException("socket's output stream == null");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements yo4 {
        @DexIgnore
        public /* final */ /* synthetic */ zo4 e;
        @DexIgnore
        public /* final */ /* synthetic */ InputStream f;

        @DexIgnore
        public b(zo4 zo4, InputStream inputStream) {
            this.e = zo4;
            this.f = inputStream;
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
            if (i < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (i == 0) {
                return 0;
            } else {
                try {
                    this.e.e();
                    vo4 b = jo4.b(1);
                    int read = this.f.read(b.a, b.c, (int) Math.min(j, (long) (8192 - b.c)));
                    if (read == -1) {
                        return -1;
                    }
                    b.c += read;
                    long j2 = (long) read;
                    jo4.f += j2;
                    return j2;
                } catch (AssertionError e2) {
                    if (so4.a(e2)) {
                        throw new IOException(e2);
                    }
                    throw e2;
                }
            }
        }

        @DexIgnore
        public void close() throws IOException {
            this.f.close();
        }

        @DexIgnore
        public String toString() {
            return "source(" + this.f + ")";
        }

        @DexIgnore
        public zo4 b() {
            return this.e;
        }
    }

    @DexIgnore
    public static yo4 a(InputStream inputStream) {
        return a(inputStream, new zo4());
    }

    @DexIgnore
    public static yo4 a(InputStream inputStream, zo4 zo4) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (zo4 != null) {
            return new b(zo4, inputStream);
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    @DexIgnore
    public static xo4 a(File file) throws FileNotFoundException {
        if (file != null) {
            return a((OutputStream) new FileOutputStream(file, true));
        }
        throw new IllegalArgumentException("file == null");
    }

    @DexIgnore
    public static xo4 a() {
        return new c();
    }

    @DexIgnore
    public static boolean a(AssertionError assertionError) {
        return (assertionError.getCause() == null || assertionError.getMessage() == null || !assertionError.getMessage().contains("getsockname failed")) ? false : true;
    }
}
