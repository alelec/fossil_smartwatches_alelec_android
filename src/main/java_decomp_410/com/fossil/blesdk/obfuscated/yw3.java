package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yw3 extends kv3 {
    @DexIgnore
    public /* final */ cv3 e;
    @DexIgnore
    public /* final */ lo4 f;

    @DexIgnore
    public yw3(cv3 cv3, lo4 lo4) {
        this.e = cv3;
        this.f = lo4;
    }

    @DexIgnore
    public fv3 A() {
        String a = this.e.a(GraphRequest.CONTENT_TYPE_HEADER);
        if (a != null) {
            return fv3.a(a);
        }
        return null;
    }

    @DexIgnore
    public lo4 B() {
        return this.f;
    }

    @DexIgnore
    public long z() {
        return xw3.a(this.e);
    }
}
