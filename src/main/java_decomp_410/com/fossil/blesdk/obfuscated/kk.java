package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class kk extends lk<Boolean> {
    @DexIgnore
    public kk(Context context, zl zlVar) {
        super(xk.a(context, zlVar).b());
    }

    @DexIgnore
    public boolean a(hl hlVar) {
        return hlVar.j.f();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
