package com.fossil.blesdk.obfuscated;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface r91 {
    @DexIgnore
    int a(int i, Object obj, Object obj2);

    @DexIgnore
    Object a(Object obj);

    @DexIgnore
    Object b(Object obj);

    @DexIgnore
    Object b(Object obj, Object obj2);

    @DexIgnore
    q91<?, ?> c(Object obj);

    @DexIgnore
    Map<?, ?> d(Object obj);

    @DexIgnore
    Map<?, ?> e(Object obj);

    @DexIgnore
    boolean f(Object obj);
}
