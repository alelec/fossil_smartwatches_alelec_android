package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.WritableByteChannel;
import okio.ByteString;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ko4 extends xo4, WritableByteChannel {
    @DexIgnore
    long a(yo4 yo4) throws IOException;

    @DexIgnore
    jo4 a();

    @DexIgnore
    ko4 a(long j) throws IOException;

    @DexIgnore
    ko4 a(String str) throws IOException;

    @DexIgnore
    ko4 a(ByteString byteString) throws IOException;

    @DexIgnore
    ko4 b(long j) throws IOException;

    @DexIgnore
    ko4 c() throws IOException;

    @DexIgnore
    ko4 d() throws IOException;

    @DexIgnore
    OutputStream e();

    @DexIgnore
    void flush() throws IOException;

    @DexIgnore
    ko4 write(byte[] bArr) throws IOException;

    @DexIgnore
    ko4 write(byte[] bArr, int i, int i2) throws IOException;

    @DexIgnore
    ko4 writeByte(int i) throws IOException;

    @DexIgnore
    ko4 writeInt(int i) throws IOException;

    @DexIgnore
    ko4 writeShort(int i) throws IOException;
}
