package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ProtocolException;
import java.net.UnknownServiceException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sm4 {
    @DexIgnore
    public /* final */ List<pl4> a;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public sm4(List<pl4> list) {
        this.a = list;
    }

    @DexIgnore
    public pl4 a(SSLSocket sSLSocket) throws IOException {
        pl4 pl4;
        int i = this.b;
        int size = this.a.size();
        while (true) {
            if (i >= size) {
                pl4 = null;
                break;
            }
            pl4 = this.a.get(i);
            if (pl4.a(sSLSocket)) {
                this.b = i + 1;
                break;
            }
            i++;
        }
        if (pl4 != null) {
            this.c = b(sSLSocket);
            hm4.a.a(pl4, sSLSocket, this.d);
            return pl4;
        }
        throw new UnknownServiceException("Unable to find acceptable protocols. isFallback=" + this.d + ", modes=" + this.a + ", supported protocols=" + Arrays.toString(sSLSocket.getEnabledProtocols()));
    }

    @DexIgnore
    public final boolean b(SSLSocket sSLSocket) {
        for (int i = this.b; i < this.a.size(); i++) {
            if (this.a.get(i).a(sSLSocket)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(IOException iOException) {
        this.d = true;
        if (!this.c || (iOException instanceof ProtocolException) || (iOException instanceof InterruptedIOException)) {
            return false;
        }
        boolean z = iOException instanceof SSLHandshakeException;
        if ((z && (iOException.getCause() instanceof CertificateException)) || (iOException instanceof SSLPeerUnverifiedException)) {
            return false;
        }
        if (z || (iOException instanceof SSLProtocolException) || (iOException instanceof SSLException)) {
            return true;
        }
        return false;
    }
}
