package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.fitness.data.RawDataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zp0 implements Parcelable.Creator<RawDataPoint> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        fp0[] fp0Arr = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 2:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 3:
                    fp0Arr = SafeParcelReader.b(parcel2, a, fp0.CREATOR);
                    break;
                case 4:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 5:
                    i2 = SafeParcelReader.q(parcel2, a);
                    break;
                case 6:
                    j3 = SafeParcelReader.s(parcel2, a);
                    break;
                case 7:
                    j4 = SafeParcelReader.s(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new RawDataPoint(j, j2, fp0Arr, i, i2, j3, j4);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new RawDataPoint[i];
    }
}
