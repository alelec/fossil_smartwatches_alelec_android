package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n8 */
public class C2464n8 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.view.View f7671a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2464n8.C2467c f7672b;

    @DexIgnore
    /* renamed from: c */
    public int f7673c;

    @DexIgnore
    /* renamed from: d */
    public int f7674d;

    @DexIgnore
    /* renamed from: e */
    public boolean f7675e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.view.View.OnLongClickListener f7676f; // = new com.fossil.blesdk.obfuscated.C2464n8.C2465a();

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.view.View.OnTouchListener f7677g; // = new com.fossil.blesdk.obfuscated.C2464n8.C2466b();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n8$a")
    /* renamed from: com.fossil.blesdk.obfuscated.n8$a */
    public class C2465a implements android.view.View.OnLongClickListener {
        @DexIgnore
        public C2465a() {
        }

        @DexIgnore
        public boolean onLongClick(android.view.View view) {
            return com.fossil.blesdk.obfuscated.C2464n8.this.mo13869a(view);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.n8$b")
    /* renamed from: com.fossil.blesdk.obfuscated.n8$b */
    public class C2466b implements android.view.View.OnTouchListener {
        @DexIgnore
        public C2466b() {
        }

        @DexIgnore
        public boolean onTouch(android.view.View view, android.view.MotionEvent motionEvent) {
            return com.fossil.blesdk.obfuscated.C2464n8.this.mo13870a(view, motionEvent);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.n8$c */
    public interface C2467c {
        @DexIgnore
        /* renamed from: a */
        boolean mo13873a(android.view.View view, com.fossil.blesdk.obfuscated.C2464n8 n8Var);
    }

    @DexIgnore
    public C2464n8(android.view.View view, com.fossil.blesdk.obfuscated.C2464n8.C2467c cVar) {
        this.f7671a = view;
        this.f7672b = cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13867a() {
        this.f7671a.setOnLongClickListener(this.f7676f);
        this.f7671a.setOnTouchListener(this.f7677g);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (r2 != 3) goto L_0x004f;
     */
    @DexIgnore
    /* renamed from: a */
    public boolean mo13870a(android.view.View view, android.view.MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    if (com.fossil.blesdk.obfuscated.C3020u8.m14623a(motionEvent, 8194) && (motionEvent.getButtonState() & 1) != 0 && !this.f7675e && !(this.f7673c == x && this.f7674d == y)) {
                        this.f7673c = x;
                        this.f7674d = y;
                        this.f7675e = this.f7672b.mo13873a(view, this);
                        return this.f7675e;
                    }
                }
            }
            this.f7675e = false;
        } else {
            this.f7673c = x;
            this.f7674d = y;
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo13869a(android.view.View view) {
        return this.f7672b.mo13873a(view, this);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo13868a(android.graphics.Point point) {
        point.set(this.f7673c, this.f7674d);
    }
}
