package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ml1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ Object e;

    @DexIgnore
    public ml1(String str, String str2, String str3, long j, Object obj) {
        bk0.b(str);
        bk0.b(str3);
        bk0.a(obj);
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = j;
        this.e = obj;
    }
}
