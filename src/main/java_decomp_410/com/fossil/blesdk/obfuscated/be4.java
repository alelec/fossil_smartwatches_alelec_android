package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class be4 extends zd4 implements vd4<Long> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        new be4(1, 0);
    }
    */

    @DexIgnore
    public be4(long j, long j2) {
        super(j, j2, 1);
    }

    @DexIgnore
    public boolean a(long j) {
        return a() <= j && j <= b();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof be4) {
            if (!isEmpty() || !((be4) obj).isEmpty()) {
                be4 be4 = (be4) obj;
                if (!(a() == be4.a() && b() == be4.b())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (int) ((((long) 31) * (a() ^ (a() >>> 32))) + (b() ^ (b() >>> 32)));
    }

    @DexIgnore
    public boolean isEmpty() {
        return a() > b();
    }

    @DexIgnore
    public String toString() {
        return a() + ".." + b();
    }
}
