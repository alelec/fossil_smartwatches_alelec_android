package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p3 */
public final class C2625p3 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Intent f8280a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.os.Bundle f8281b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.p3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.p3$a */
    public static final class C2626a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.content.Intent f8282a;

        @DexIgnore
        /* renamed from: b */
        public java.util.ArrayList<android.os.Bundle> f8283b;

        @DexIgnore
        /* renamed from: c */
        public android.os.Bundle f8284c;

        @DexIgnore
        /* renamed from: d */
        public java.util.ArrayList<android.os.Bundle> f8285d;

        @DexIgnore
        /* renamed from: e */
        public boolean f8286e;

        @DexIgnore
        public C2626a() {
            this((com.fossil.blesdk.obfuscated.C2782r3) null);
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2625p3 mo14614a() {
            java.util.ArrayList<android.os.Bundle> arrayList = this.f8283b;
            if (arrayList != null) {
                this.f8282a.putParcelableArrayListExtra("android.support.customtabs.extra.MENU_ITEMS", arrayList);
            }
            java.util.ArrayList<android.os.Bundle> arrayList2 = this.f8285d;
            if (arrayList2 != null) {
                this.f8282a.putParcelableArrayListExtra("android.support.customtabs.extra.TOOLBAR_ITEMS", arrayList2);
            }
            this.f8282a.putExtra("android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS", this.f8286e);
            return new com.fossil.blesdk.obfuscated.C2625p3(this.f8282a, this.f8284c);
        }

        @DexIgnore
        public C2626a(com.fossil.blesdk.obfuscated.C2782r3 r3Var) {
            this.f8282a = new android.content.Intent("android.intent.action.VIEW");
            this.f8283b = null;
            this.f8284c = null;
            this.f8285d = null;
            this.f8286e = true;
            if (r3Var == null) {
                android.os.Bundle bundle = new android.os.Bundle();
                if (r3Var == null) {
                    com.fossil.blesdk.obfuscated.C3414z5.m17266a(bundle, "android.support.customtabs.extra.SESSION", (android.os.IBinder) null);
                    this.f8282a.putExtras(bundle);
                    return;
                }
                r3Var.mo15437a();
                throw null;
            }
            r3Var.mo15438b();
            throw null;
        }
    }

    @DexIgnore
    public C2625p3(android.content.Intent intent, android.os.Bundle bundle) {
        this.f8280a = intent;
        this.f8281b = bundle;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14613a(android.content.Context context, android.net.Uri uri) {
        this.f8280a.setData(uri);
        com.fossil.blesdk.obfuscated.C2185k6.m9352a(context, this.f8280a, this.f8281b);
    }
}
