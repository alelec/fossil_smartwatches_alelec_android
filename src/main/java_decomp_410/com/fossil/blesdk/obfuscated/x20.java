package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.data.music.TrackInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class x20 extends s20<TrackInfo, qa4> {
    @DexIgnore
    public static /* final */ k20<TrackInfo>[] a; // = {new a(), new b()};
    @DexIgnore
    public static /* final */ l20<qa4>[] b; // = new l20[0];
    @DexIgnore
    public static /* final */ x20 c; // = new x20();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends q20<TrackInfo> {
        @DexIgnore
        public byte[] a(TrackInfo trackInfo) {
            kd4.b(trackInfo, "entries");
            return x20.c.a(trackInfo);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends r20<TrackInfo> {
        @DexIgnore
        public byte[] a(TrackInfo trackInfo) {
            kd4.b(trackInfo, "entries");
            return x20.c.a(trackInfo);
        }
    }

    @DexIgnore
    public l20<qa4>[] b() {
        return b;
    }

    @DexIgnore
    public k20<TrackInfo>[] a() {
        return a;
    }

    @DexIgnore
    public final byte[] a(TrackInfo trackInfo) {
        return trackInfo.getData$blesdk_productionRelease();
    }
}
