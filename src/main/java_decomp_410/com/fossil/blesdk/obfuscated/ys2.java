package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ys2 extends RecyclerView.g<c> {
    @DexIgnore
    public List<Pair<ShineDevice, String>> a; // = new ArrayList();
    @DexIgnore
    public /* final */ xn b;
    @DexIgnore
    public b c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, c cVar, int i);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView e;
        @DexIgnore
        public /* final */ FlexibleTextView f;
        @DexIgnore
        public /* final */ ImageView g;
        @DexIgnore
        public /* final */ View h;
        @DexIgnore
        public /* final */ /* synthetic */ ys2 i;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public void onImageCallback(String str, String str2) {
                kd4.b(str, "serial");
                kd4.b(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ScanningDeviceAdapter", "set image for " + str + " path " + str2);
                this.a.i.b.a(str2).a(((rv) new rv().b((int) R.drawable.silhouette_watch_large)).c()).a(this.a.g);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ys2 ys2, View view) {
            super(view);
            kd4.b(view, "view");
            this.i = ys2;
            View findViewById = view.findViewById(R.id.ftv_device_serial);
            kd4.a((Object) findViewById, "view.findViewById(R.id.ftv_device_serial)");
            this.e = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(R.id.ftv_device_name);
            kd4.a((Object) findViewById2, "view.findViewById(R.id.ftv_device_name)");
            this.f = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.iv_device_image);
            kd4.a((Object) findViewById3, "view.findViewById(R.id.iv_device_image)");
            this.g = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(R.id.scan_device_container);
            kd4.a((Object) findViewById4, "view.findViewById(R.id.scan_device_container)");
            this.h = findViewById4;
            this.h.setOnClickListener(this);
        }

        @DexIgnore
        public void onClick(View view) {
            kd4.b(view, "view");
            if (this.i.getItemCount() > getAdapterPosition() && getAdapterPosition() != -1) {
                b a2 = this.i.c;
                if (a2 != null) {
                    a2.a(view, this, getAdapterPosition());
                }
            }
        }

        @DexIgnore
        public final void a(ShineDevice shineDevice, String str) {
            kd4.b(shineDevice, "shineDevice");
            kd4.b(str, "deviceName");
            this.f.setText(str);
            FlexibleTextView flexibleTextView = this.e;
            pd4 pd4 = pd4.a;
            Locale locale = Locale.US;
            kd4.a((Object) locale, "Locale.US");
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Serial_pattern);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026 R.string.Serial_pattern)");
            Object[] objArr = {shineDevice.getSerial()};
            String format = String.format(locale, a2, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) format, "java.lang.String.format(locale, format, *args)");
            flexibleTextView.setText(format);
            CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
            String serial = shineDevice.getSerial();
            kd4.a((Object) serial, "shineDevice.serial");
            CloudImageHelper.ItemImage type = with.setSerialNumber(serial).setSerialPrefix(DeviceHelper.o.b(shineDevice.getSerial())).setType(Constants.DeviceType.TYPE_LARGE);
            ImageView imageView = this.g;
            DeviceHelper.a aVar = DeviceHelper.o;
            String serial2 = shineDevice.getSerial();
            kd4.a((Object) serial2, "shineDevice.serial");
            type.setPlaceHolder(imageView, aVar.b(serial2, DeviceHelper.ImageStyle.LARGE)).setImageCallback(new a(this)).download();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements Comparator<Pair<? extends ShineDevice, ? extends String>> {
        @DexIgnore
        public static /* final */ d e; // = new d();

        @DexIgnore
        /* renamed from: a */
        public final int compare(Pair<ShineDevice, String> pair, Pair<ShineDevice, String> pair2) {
            int i;
            if (pair == null || pair2 == null) {
                int i2 = -1;
                int i3 = pair == null ? -1 : 1;
                if (pair2 != null) {
                    i2 = 1;
                }
                i = i3 - i2;
            } else {
                i = pair.getFirst().getRssi() - pair2.getFirst().getRssi();
            }
            return -i;
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public ys2(xn xnVar, b bVar) {
        kd4.b(xnVar, "mRequestManager");
        this.b = xnVar;
        this.c = bVar;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void a(List<Pair<ShineDevice, String>> list) {
        kd4.b(list, "data");
        this.a.clear();
        gb4.a(list, d.e);
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_scan_device_new, viewGroup, false);
        kd4.a((Object) inflate, "v");
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.width = (int) (((float) viewGroup.getMeasuredWidth()) * 0.5f);
        inflate.setLayoutParams(layoutParams);
        return new c(this, inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        kd4.b(cVar, "viewHolder");
        cVar.a((ShineDevice) this.a.get(i).getFirst(), (String) this.a.get(i).getSecond());
    }
}
