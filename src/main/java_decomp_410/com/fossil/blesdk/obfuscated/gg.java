package com.fossil.blesdk.obfuscated;

import android.database.Cursor;
import android.database.SQLException;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface gg extends Closeable {
    @DexIgnore
    Cursor a(jg jgVar);

    @DexIgnore
    void b(String str) throws SQLException;

    @DexIgnore
    kg c(String str);

    @DexIgnore
    Cursor d(String str);

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    void s();

    @DexIgnore
    List<Pair<String, String>> t();

    @DexIgnore
    void u();

    @DexIgnore
    void v();

    @DexIgnore
    String w();

    @DexIgnore
    boolean x();
}
