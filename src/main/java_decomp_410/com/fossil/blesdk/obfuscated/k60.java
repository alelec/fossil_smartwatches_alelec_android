package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k60 extends j60 {
    @DexIgnore
    public /* final */ int g; // = 1;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k60(byte[] bArr, int i, GattCharacteristic.CharacteristicId characteristicId) {
        super(bArr, i, characteristicId);
        kd4.b(bArr, "data");
        kd4.b(characteristicId, "characteristicId");
    }

    @DexIgnore
    public byte[] c() {
        return new byte[]{(byte) (h() % 256)};
    }

    @DexIgnore
    public int f() {
        return this.g;
    }
}
