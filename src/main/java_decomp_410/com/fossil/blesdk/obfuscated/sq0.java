package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.gcm.PendingCallback;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class sq0 extends Service {
    @DexIgnore
    public /* final */ Object e; // = new Object();
    @DexIgnore
    public int f;
    @DexIgnore
    public ExecutorService g;
    @DexIgnore
    public Messenger h;
    @DexIgnore
    public ComponentName i;
    @DexIgnore
    public qq0 j;
    @DexIgnore
    public a31 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(21)
    public class a extends z21 {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            if (!sm0.a(sq0.this, message.sendingUid, "com.google.android.gms")) {
                Log.e("GcmTaskService", "unable to verify presence of Google Play Services");
                return;
            }
            int i = message.what;
            if (i == 1) {
                Bundle data = message.getData();
                if (!data.isEmpty()) {
                    Messenger messenger = message.replyTo;
                    if (messenger != null) {
                        String string = data.getString("tag");
                        ArrayList parcelableArrayList = data.getParcelableArrayList("triggered_uris");
                        long j = data.getLong("max_exec_duration", 180);
                        if (!sq0.this.a(string)) {
                            sq0.this.a(new b(string, messenger, data.getBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY), j, (List<Uri>) parcelableArrayList));
                        }
                    }
                }
            } else if (i != 2) {
                if (i != 4) {
                    String valueOf = String.valueOf(message);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 31);
                    sb.append("Unrecognized message received: ");
                    sb.append(valueOf);
                    Log.e("GcmTaskService", sb.toString());
                    return;
                }
                sq0.this.a();
            } else if (Log.isLoggable("GcmTaskService", 3)) {
                String valueOf2 = String.valueOf(message);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("ignoring unimplemented stop message for now: ");
                sb2.append(valueOf2);
                Log.d("GcmTaskService", sb2.toString());
            }
        }
    }

    @DexIgnore
    public abstract int a(uq0 uq0);

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public final boolean a(String str) {
        boolean z;
        synchronized (this.e) {
            z = !this.j.a(str, this.i.getClassName());
            if (z) {
                String packageName = getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 44 + String.valueOf(str).length());
                sb.append(packageName);
                sb.append(" ");
                sb.append(str);
                sb.append(": Task already running, won't start another");
                Log.w("GcmTaskService", sb.toString());
            }
        }
        return z;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (intent == null || !pm0.g() || !"com.google.android.gms.gcm.ACTION_TASK_READY".equals(intent.getAction())) {
            return null;
        }
        return this.h.getBinder();
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.j = qq0.a((Context) this);
        this.g = w21.a().a(10, new wq0(this), 10);
        this.h = new Messenger(new a(Looper.getMainLooper()));
        this.i = new ComponentName(this, sq0.class);
        b31.a();
        Class<sq0> cls = sq0.class;
        this.k = b31.a;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        List<Runnable> shutdownNow = this.g.shutdownNow();
        if (!shutdownNow.isEmpty()) {
            int size = shutdownNow.size();
            StringBuilder sb = new StringBuilder(79);
            sb.append("Shutting down, but not all tasks are finished executing. Remaining: ");
            sb.append(size);
            Log.e("GcmTaskService", sb.toString());
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        if (intent == null) {
            a(i3);
            return 2;
        }
        try {
            intent.setExtrasClassLoader(PendingCallback.class.getClassLoader());
            String action = intent.getAction();
            if ("com.google.android.gms.gcm.ACTION_TASK_READY".equals(action)) {
                String stringExtra = intent.getStringExtra("tag");
                Parcelable parcelableExtra = intent.getParcelableExtra(Constants.CALLBACK);
                Bundle bundleExtra = intent.getBundleExtra(AppLinkData.ARGUMENTS_EXTRAS_KEY);
                ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("triggered_uris");
                long longExtra = intent.getLongExtra("max_exec_duration", 180);
                if (!(parcelableExtra instanceof PendingCallback)) {
                    String packageName = getPackageName();
                    StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 47 + String.valueOf(stringExtra).length());
                    sb.append(packageName);
                    sb.append(" ");
                    sb.append(stringExtra);
                    sb.append(": Could not process request, invalid callback.");
                    Log.e("GcmTaskService", sb.toString());
                    return 2;
                } else if (a(stringExtra)) {
                    a(i3);
                    return 2;
                } else {
                    a(new b(stringExtra, ((PendingCallback) parcelableExtra).e, bundleExtra, longExtra, (List<Uri>) parcelableArrayListExtra));
                }
            } else if ("com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE".equals(action)) {
                a();
            } else {
                StringBuilder sb2 = new StringBuilder(String.valueOf(action).length() + 37);
                sb2.append("Unknown action received ");
                sb2.append(action);
                sb2.append(", terminating");
                Log.e("GcmTaskService", sb2.toString());
            }
            a(i3);
            return 2;
        } finally {
            a(i3);
        }
    }

    @DexIgnore
    public final void a(int i2) {
        synchronized (this.e) {
            this.f = i2;
            if (!this.j.a(this.i.getClassName())) {
                stopSelf(this.f);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ String e;
        @DexIgnore
        public /* final */ Bundle f;
        @DexIgnore
        public /* final */ List<Uri> g;
        @DexIgnore
        public /* final */ long h;
        @DexIgnore
        public /* final */ xq0 i;
        @DexIgnore
        public /* final */ Messenger j;

        @DexIgnore
        public b(String str, IBinder iBinder, Bundle bundle, long j2, List<Uri> list) {
            xq0 xq0;
            this.e = str;
            if (iBinder == null) {
                xq0 = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gcm.INetworkTaskCallback");
                if (queryLocalInterface instanceof xq0) {
                    xq0 = (xq0) queryLocalInterface;
                } else {
                    xq0 = new yq0(iBinder);
                }
            }
            this.i = xq0;
            this.f = bundle;
            this.h = j2;
            this.g = list;
            this.j = null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x005c, code lost:
            return;
         */
        @DexIgnore
        public final void a(int i2) {
            synchronized (sq0.this.e) {
                try {
                    if (sq0.this.j.c(this.e, sq0.this.i.getClassName())) {
                        sq0.this.j.b(this.e, sq0.this.i.getClassName());
                        if (!a() && !sq0.this.j.a(sq0.this.i.getClassName())) {
                            sq0.this.stopSelf(sq0.this.f);
                        }
                    } else {
                        if (a()) {
                            Messenger messenger = this.j;
                            Message obtain = Message.obtain();
                            obtain.what = 3;
                            obtain.arg1 = i2;
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("component", sq0.this.i);
                            bundle.putString("tag", this.e);
                            obtain.setData(bundle);
                            messenger.send(obtain);
                        } else {
                            this.i.d(i2);
                        }
                        sq0.this.j.b(this.e, sq0.this.i.getClassName());
                        if (!a() && !sq0.this.j.a(sq0.this.i.getClassName())) {
                            sq0.this.stopSelf(sq0.this.f);
                        }
                    }
                } catch (RemoteException unused) {
                    try {
                        String valueOf = String.valueOf(this.e);
                        Log.e("GcmTaskService", valueOf.length() != 0 ? "Error reporting result of operation to scheduler for ".concat(valueOf) : new String("Error reporting result of operation to scheduler for "));
                        sq0.this.j.b(this.e, sq0.this.i.getClassName());
                        if (!a() && !sq0.this.j.a(sq0.this.i.getClassName())) {
                            sq0.this.stopSelf(sq0.this.f);
                        }
                    } catch (Throwable th) {
                        sq0.this.j.b(this.e, sq0.this.i.getClassName());
                        if (!a() && !sq0.this.j.a(sq0.this.i.getClassName())) {
                            sq0.this.stopSelf(sq0.this.f);
                        }
                        throw th;
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
            a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
            throw r2;
         */
        @DexIgnore
        public final void run() {
            String valueOf = String.valueOf(this.e);
            ar0 ar0 = new ar0(valueOf.length() != 0 ? "nts:client:onRunTask:".concat(valueOf) : new String("nts:client:onRunTask:"));
            uq0 uq0 = new uq0(this.e, this.f, this.h, this.g);
            sq0.this.k.a("onRunTask", e31.a);
            a(sq0.this.a(uq0));
            a((Throwable) null, ar0);
        }

        @DexIgnore
        public b(String str, Messenger messenger, Bundle bundle, long j2, List<Uri> list) {
            this.e = str;
            this.j = messenger;
            this.f = bundle;
            this.h = j2;
            this.g = list;
            this.i = null;
        }

        @DexIgnore
        public final boolean a() {
            return this.j != null;
        }

        @DexIgnore
        public static /* synthetic */ void a(Throwable th, ar0 ar0) {
            if (th != null) {
                try {
                    ar0.close();
                } catch (Throwable th2) {
                    f31.a(th, th2);
                }
            } else {
                ar0.close();
            }
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        try {
            this.g.execute(bVar);
        } catch (RejectedExecutionException e2) {
            Log.e("GcmTaskService", "Executor is shutdown. onDestroy was called but main looper had an unprocessed start task message. The task will be retried with backoff delay.", e2);
            bVar.a(1);
        }
    }
}
