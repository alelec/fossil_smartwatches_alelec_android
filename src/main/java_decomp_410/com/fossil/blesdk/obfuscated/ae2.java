package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ae2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ RecyclerView t;

    @DexIgnore
    public ae2(Object obj, View view, int i, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, View view2, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleEditText;
        this.r = imageView;
        this.s = imageView2;
        this.t = recyclerView;
    }
}
