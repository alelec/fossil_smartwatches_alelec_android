package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.tq;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class wq implements tq.a {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        File a();
    }

    @DexIgnore
    public wq(a aVar, long j) {
        this.a = j;
        this.b = aVar;
    }

    @DexIgnore
    public tq build() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return xq.a(a2, this.a);
        }
        return null;
    }
}
