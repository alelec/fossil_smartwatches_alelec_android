package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.xd */
public class C3278xd<K, A, B> extends com.fossil.blesdk.obfuscated.C2390md<K, B> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2390md<K, A> f10897a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> f10898b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.IdentityHashMap<B, K> f10899c; // = new java.util.IdentityHashMap<>();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.xd$a */
    public class C3279a extends com.fossil.blesdk.obfuscated.C2390md.C2393c<A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2390md.C2393c f10900a;

        @DexIgnore
        public C3279a(com.fossil.blesdk.obfuscated.C2390md.C2393c cVar) {
            this.f10900a = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13573a(java.util.List<A> list) {
            this.f10900a.mo13573a(com.fossil.blesdk.obfuscated.C3278xd.this.mo17672a(list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.xd$b */
    public class C3280b extends com.fossil.blesdk.obfuscated.C2390md.C2391a<A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2390md.C2391a f10902a;

        @DexIgnore
        public C3280b(com.fossil.blesdk.obfuscated.C2390md.C2391a aVar) {
            this.f10902a = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13573a(java.util.List<A> list) {
            this.f10902a.mo13573a(com.fossil.blesdk.obfuscated.C3278xd.this.mo17672a(list));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.xd$c")
    /* renamed from: com.fossil.blesdk.obfuscated.xd$c */
    public class C3281c extends com.fossil.blesdk.obfuscated.C2390md.C2391a<A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2390md.C2391a f10904a;

        @DexIgnore
        public C3281c(com.fossil.blesdk.obfuscated.C2390md.C2391a aVar) {
            this.f10904a = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13573a(java.util.List<A> list) {
            this.f10904a.mo13573a(com.fossil.blesdk.obfuscated.C3278xd.this.mo17672a(list));
        }
    }

    @DexIgnore
    public C3278xd(com.fossil.blesdk.obfuscated.C2390md<K, A> mdVar, com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> m3Var) {
        this.f10897a = mdVar;
        this.f10898b = m3Var;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<B> mo17672a(java.util.List<A> list) {
        java.util.List<B> convert = com.fossil.blesdk.obfuscated.C2307ld.convert(this.f10898b, list);
        synchronized (this.f10899c) {
            for (int i = 0; i < convert.size(); i++) {
                this.f10899c.put(convert.get(i), this.f10897a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    public void addInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.f10897a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public K getKey(B b) {
        K k;
        synchronized (this.f10899c) {
            k = this.f10899c.get(b);
        }
        return k;
    }

    @DexIgnore
    public void invalidate() {
        this.f10897a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.f10897a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(com.fossil.blesdk.obfuscated.C2390md.C2396f<K> fVar, com.fossil.blesdk.obfuscated.C2390md.C2391a<B> aVar) {
        this.f10897a.loadAfter(fVar, new com.fossil.blesdk.obfuscated.C3278xd.C3280b(aVar));
    }

    @DexIgnore
    public void loadBefore(com.fossil.blesdk.obfuscated.C2390md.C2396f<K> fVar, com.fossil.blesdk.obfuscated.C2390md.C2391a<B> aVar) {
        this.f10897a.loadBefore(fVar, new com.fossil.blesdk.obfuscated.C3278xd.C3281c(aVar));
    }

    @DexIgnore
    public void loadInitial(com.fossil.blesdk.obfuscated.C2390md.C2395e<K> eVar, com.fossil.blesdk.obfuscated.C2390md.C2393c<B> cVar) {
        this.f10897a.loadInitial(eVar, new com.fossil.blesdk.obfuscated.C3278xd.C3279a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.f10897a.removeInvalidatedCallback(cVar);
    }
}
