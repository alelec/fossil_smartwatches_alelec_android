package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface lm {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public byte[] a;
        @DexIgnore
        public String b;
        @DexIgnore
        public long c;
        @DexIgnore
        public long d;
        @DexIgnore
        public long e;
        @DexIgnore
        public long f;
        @DexIgnore
        public Map<String, String> g; // = Collections.emptyMap();
        @DexIgnore
        public List<pm> h;

        @DexIgnore
        public boolean a() {
            return this.e < System.currentTimeMillis();
        }

        @DexIgnore
        public boolean b() {
            return this.f < System.currentTimeMillis();
        }
    }

    @DexIgnore
    a a(String str);

    @DexIgnore
    void a(String str, a aVar);

    @DexIgnore
    void d();
}
