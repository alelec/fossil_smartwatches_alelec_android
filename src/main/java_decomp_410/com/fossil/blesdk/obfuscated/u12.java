package com.fossil.blesdk.obfuscated;

import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u12 extends z12 {
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore
    public u12(z12 z12, int i, int i2) {
        super(z12);
        this.c = (short) i;
        this.d = (short) i2;
    }

    @DexIgnore
    public void a(a22 a22, byte[] bArr) {
        int i = 0;
        while (true) {
            short s = this.d;
            if (i < s) {
                if (i == 0 || (i == 31 && s <= 62)) {
                    a22.a(31, 5);
                    short s2 = this.d;
                    if (s2 > 62) {
                        a22.a(s2 - 31, 16);
                    } else if (i == 0) {
                        a22.a(Math.min(s2, 31), 5);
                    } else {
                        a22.a(s2 - 31, 5);
                    }
                }
                a22.a(bArr[this.c + i], 8);
                i++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(SimpleComparison.LESS_THAN_OPERATION);
        sb.append(this.c);
        sb.append("::");
        sb.append((this.c + this.d) - 1);
        sb.append('>');
        return sb.toString();
    }
}
