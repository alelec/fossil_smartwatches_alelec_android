package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface x03 extends v52<w03> {
    @DexIgnore
    void a(ContactWrapper contactWrapper);

    @DexIgnore
    void a(ArrayList<ContactWrapper> arrayList);

    @DexIgnore
    void b(List<ContactWrapper> list, int i);
}
