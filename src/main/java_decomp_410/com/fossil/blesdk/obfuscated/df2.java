package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class df2 extends cf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout G;
    @DexIgnore
    public long H;

    /*
    static {
        J.put(R.id.tv_title, 1);
        J.put(R.id.iv_back, 2);
        J.put(R.id.v_separator, 3);
        J.put(R.id.sv_goals_edit, 4);
        J.put(R.id.card_profile_info, 5);
        J.put(R.id.vertical_guideline, 6);
        J.put(R.id.horizontal_guideline, 7);
        J.put(R.id.cegv_top_left, 8);
        J.put(R.id.cegv_top_right, 9);
        J.put(R.id.cegv_bottom_left, 10);
        J.put(R.id.cegv_bottom_right, 11);
        J.put(R.id.ftv_goal_title, 12);
        J.put(R.id.ll_goals_value, 13);
        J.put(R.id.fet_goals_value, 14);
        J.put(R.id.ftv_goals_unit, 15);
        J.put(R.id.ll_sleep_goal_value, 16);
        J.put(R.id.fet_sleep_hour_value, 17);
        J.put(R.id.ftv_sleep_hour_unit, 18);
        J.put(R.id.fet_sleep_minute_value, 19);
        J.put(R.id.ftv_sleep_minute_unit, 20);
        J.put(R.id.barrier_edit_goal_bottom, 21);
        J.put(R.id.ftv_desc, 22);
        J.put(R.id.v_shadow, 23);
    }
    */

    @DexIgnore
    public df2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 24, I, J));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.H != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.H = 1;
        }
        g();
    }

    @DexIgnore
    public df2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[21], objArr[5], objArr[10], objArr[11], objArr[8], objArr[9], objArr[14], objArr[17], objArr[19], objArr[22], objArr[12], objArr[15], objArr[18], objArr[20], objArr[7], objArr[2], objArr[13], objArr[16], objArr[4], objArr[1], objArr[3], objArr[23], objArr[6]);
        this.H = -1;
        this.G = objArr[0];
        this.G.setTag((Object) null);
        a(view);
        f();
    }
}
