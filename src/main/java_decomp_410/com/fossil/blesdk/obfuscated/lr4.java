package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lr4 extends gr4.a {
    @DexIgnore
    public static /* final */ gr4.a a; // = new lr4();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements gr4<em4, Optional<T>> {
        @DexIgnore
        public /* final */ gr4<em4, T> a;

        @DexIgnore
        public a(gr4<em4, T> gr4) {
            this.a = gr4;
        }

        @DexIgnore
        public Optional<T> a(em4 em4) throws IOException {
            return Optional.ofNullable(this.a.a(em4));
        }
    }

    @DexIgnore
    public gr4<em4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (gr4.a.a(type) != Optional.class) {
            return null;
        }
        return new a(retrofit3.b(gr4.a.a(0, (ParameterizedType) type), annotationArr));
    }
}
