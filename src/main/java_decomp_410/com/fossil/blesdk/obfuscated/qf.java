package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import com.fossil.blesdk.obfuscated.mf;
import com.fossil.blesdk.obfuscated.nf;
import com.fossil.blesdk.obfuscated.pf;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qf {
    @DexIgnore
    public Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ pf d;
    @DexIgnore
    public /* final */ pf.c e;
    @DexIgnore
    public nf f;
    @DexIgnore
    public /* final */ Executor g;
    @DexIgnore
    public /* final */ mf h; // = new a();
    @DexIgnore
    public /* final */ AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ServiceConnection j; // = new b();
    @DexIgnore
    public /* final */ Runnable k; // = new c();
    @DexIgnore
    public /* final */ Runnable l; // = new d();
    @DexIgnore
    public /* final */ Runnable m; // = new e();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends mf.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.qf$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.qf$a$a  reason: collision with other inner class name */
        public class C0030a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String[] e;

            @DexIgnore
            public C0030a(String[] strArr) {
                this.e = strArr;
            }

            @DexIgnore
            public void run() {
                qf.this.d.a(this.e);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(String[] strArr) {
            qf.this.g.execute(new C0030a(strArr));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            qf.this.f = nf.a.a(iBinder);
            qf qfVar = qf.this;
            qfVar.g.execute(qfVar.k);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            qf qfVar = qf.this;
            qfVar.g.execute(qfVar.l);
            qf qfVar2 = qf.this;
            qfVar2.f = null;
            qfVar2.a = null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                nf nfVar = qf.this.f;
                if (nfVar != null) {
                    qf.this.c = nfVar.a(qf.this.h, qf.this.b);
                    qf.this.d.a(qf.this.e);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            qf qfVar = qf.this;
            qfVar.d.c(qfVar.e);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            qf qfVar = qf.this;
            qfVar.d.c(qfVar.e);
            try {
                nf nfVar = qf.this.f;
                if (nfVar != null) {
                    nfVar.a(qf.this.h, qf.this.c);
                }
            } catch (RemoteException e2) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e2);
            }
            qf qfVar2 = qf.this;
            Context context = qfVar2.a;
            if (context != null) {
                context.unbindService(qfVar2.j);
                qf.this.a = null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends pf.c {
        @DexIgnore
        public f(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            if (!qf.this.i.get()) {
                try {
                    qf.this.f.a(qf.this.c, (String[]) set.toArray(new String[0]));
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public qf(Context context, String str, pf pfVar, Executor executor) {
        this.a = context.getApplicationContext();
        this.b = str;
        this.d = pfVar;
        this.g = executor;
        this.e = new f(pfVar.b);
        this.a.bindService(new Intent(this.a, MultiInstanceInvalidationService.class), this.j, 1);
    }

    @DexIgnore
    public void a() {
        if (this.i.compareAndSet(false, true)) {
            this.g.execute(this.m);
        }
    }
}
