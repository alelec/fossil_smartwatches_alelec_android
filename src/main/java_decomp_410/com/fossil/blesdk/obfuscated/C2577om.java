package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.om */
public class C2577om implements com.fossil.blesdk.obfuscated.C3124vm {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.Executor f8162a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.om$a")
    /* renamed from: com.fossil.blesdk.obfuscated.om$a */
    public class C2578a implements java.util.concurrent.Executor {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ android.os.Handler f8163e;

        @DexIgnore
        public C2578a(com.fossil.blesdk.obfuscated.C2577om omVar, android.os.Handler handler) {
            this.f8163e = handler;
        }

        @DexIgnore
        public void execute(java.lang.Runnable runnable) {
            this.f8163e.post(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.om$b")
    /* renamed from: com.fossil.blesdk.obfuscated.om$b */
    public static class C2579b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ com.android.volley.Request f8164e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ com.fossil.blesdk.obfuscated.C3047um f8165f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ java.lang.Runnable f8166g;

        @DexIgnore
        public C2579b(com.android.volley.Request request, com.fossil.blesdk.obfuscated.C3047um umVar, java.lang.Runnable runnable) {
            this.f8164e = request;
            this.f8165f = umVar;
            this.f8166g = runnable;
        }

        @DexIgnore
        public void run() {
            if (this.f8164e.isCanceled()) {
                this.f8164e.finish("canceled-at-delivery");
                return;
            }
            if (this.f8165f.mo16808a()) {
                this.f8164e.deliverResponse(this.f8165f.f10027a);
            } else {
                this.f8164e.deliverError(this.f8165f.f10029c);
            }
            if (this.f8165f.f10030d) {
                this.f8164e.addMarker("intermediate-response");
            } else {
                this.f8164e.finish("done");
            }
            java.lang.Runnable runnable = this.f8166g;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public C2577om(android.os.Handler handler) {
        this.f8162a = new com.fossil.blesdk.obfuscated.C2577om.C2578a(this, handler);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14456a(com.android.volley.Request<?> request, com.fossil.blesdk.obfuscated.C3047um<?> umVar) {
        mo14457a(request, umVar, (java.lang.Runnable) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14457a(com.android.volley.Request<?> request, com.fossil.blesdk.obfuscated.C3047um<?> umVar, java.lang.Runnable runnable) {
        request.markDelivered();
        request.addMarker("post-response");
        this.f8162a.execute(new com.fossil.blesdk.obfuscated.C2577om.C2579b(request, umVar, runnable));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14455a(com.android.volley.Request<?> request, com.android.volley.VolleyError volleyError) {
        request.addMarker("post-error");
        this.f8162a.execute(new com.fossil.blesdk.obfuscated.C2577om.C2579b(request, com.fossil.blesdk.obfuscated.C3047um.m14805a(volleyError), (java.lang.Runnable) null));
    }
}
