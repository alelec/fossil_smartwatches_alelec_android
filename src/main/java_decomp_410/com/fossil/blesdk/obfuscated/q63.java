package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q63 implements Factory<HybridCustomizeEditPresenter> {
    @DexIgnore
    public static HybridCustomizeEditPresenter a(l63 l63, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, en2 en2) {
        return new HybridCustomizeEditPresenter(l63, setHybridPresetToWatchUseCase, en2);
    }
}
