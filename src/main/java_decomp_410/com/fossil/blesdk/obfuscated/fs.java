package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.net.Uri;
import com.fossil.blesdk.obfuscated.sr;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fs implements sr<Uri, InputStream> {
    @DexIgnore
    public /* final */ Context a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements tr<Uri, InputStream> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public a(Context context) {
            this.a = context;
        }

        @DexIgnore
        public sr<Uri, InputStream> a(wr wrVar) {
            return new fs(this.a);
        }
    }

    @DexIgnore
    public fs(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public sr.a<InputStream> a(Uri uri, int i, int i2, lo loVar) {
        if (ep.a(i, i2)) {
            return new sr.a<>(new jw(uri), fp.a(this.a, uri));
        }
        return null;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        return ep.a(uri);
    }
}
