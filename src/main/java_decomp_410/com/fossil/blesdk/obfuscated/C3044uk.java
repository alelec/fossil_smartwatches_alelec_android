package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.uk */
public abstract class C3044uk<T> {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f10015f; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("ConstraintTracker");

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3444zl f10016a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.Context f10017b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.Object f10018c; // = new java.lang.Object();

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C1810fk<T>> f10019d; // = new java.util.LinkedHashSet();

    @DexIgnore
    /* renamed from: e */
    public T f10020e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.uk$a")
    /* renamed from: com.fossil.blesdk.obfuscated.uk$a */
    public class C3045a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.List f10021e;

        @DexIgnore
        public C3045a(java.util.List list) {
            this.f10021e = list;
        }

        @DexIgnore
        public void run() {
            for (com.fossil.blesdk.obfuscated.C1810fk a : this.f10021e) {
                a.mo10956a(com.fossil.blesdk.obfuscated.C3044uk.this.f10020e);
            }
        }
    }

    @DexIgnore
    public C3044uk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        this.f10017b = context.getApplicationContext();
        this.f10016a = zlVar;
    }

    @DexIgnore
    /* renamed from: a */
    public abstract T mo15613a();

    @DexIgnore
    /* renamed from: a */
    public void mo16803a(com.fossil.blesdk.obfuscated.C1810fk<T> fkVar) {
        synchronized (this.f10018c) {
            if (this.f10019d.add(fkVar)) {
                if (this.f10019d.size() == 1) {
                    this.f10020e = mo15613a();
                    com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10015f, java.lang.String.format("%s: initial state = %s", new java.lang.Object[]{getClass().getSimpleName(), this.f10020e}), new java.lang.Throwable[0]);
                    mo16475b();
                }
                fkVar.mo10956a(this.f10020e);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public abstract void mo16475b();

    @DexIgnore
    /* renamed from: b */
    public void mo16805b(com.fossil.blesdk.obfuscated.C1810fk<T> fkVar) {
        synchronized (this.f10018c) {
            if (this.f10019d.remove(fkVar) && this.f10019d.isEmpty()) {
                mo16476c();
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public abstract void mo16476c();

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    @DexIgnore
    /* renamed from: a */
    public void mo16804a(T t) {
        synchronized (this.f10018c) {
            if (this.f10020e != t) {
                if (this.f10020e == null || !this.f10020e.equals(t)) {
                    this.f10020e = t;
                    this.f10016a.mo8787a().execute(new com.fossil.blesdk.obfuscated.C3044uk.C3045a(new java.util.ArrayList(this.f10019d)));
                }
            }
        }
    }
}
