package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.p;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class q implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<q> CREATOR; // = new a();
    @DexIgnore
    public /* final */ boolean e; // = false;
    @DexIgnore
    public /* final */ Handler f; // = null;
    @DexIgnore
    public p g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<q> {
        @DexIgnore
        public q createFromParcel(Parcel parcel) {
            return new q(parcel);
        }

        @DexIgnore
        public q[] newArray(int i) {
            return new q[i];
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends p.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(int i, Bundle bundle) {
            q qVar = q.this;
            Handler handler = qVar.f;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                qVar.a(i, bundle);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ Bundle f;

        @DexIgnore
        public c(int i, Bundle bundle) {
            this.e = i;
            this.f = bundle;
        }

        @DexIgnore
        public void run() {
            q.this.a(this.e, this.f);
        }
    }

    @DexIgnore
    public q(Parcel parcel) {
        this.g = p.a.a(parcel.readStrongBinder());
    }

    @DexIgnore
    public void a(int i, Bundle bundle) {
    }

    @DexIgnore
    public void b(int i, Bundle bundle) {
        if (this.e) {
            Handler handler = this.f;
            if (handler != null) {
                handler.post(new c(i, bundle));
            } else {
                a(i, bundle);
            }
        } else {
            p pVar = this.g;
            if (pVar != null) {
                try {
                    pVar.a(i, bundle);
                } catch (RemoteException unused) {
                }
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.g == null) {
                this.g = new b();
            }
            parcel.writeStrongBinder(this.g.asBinder());
        }
    }
}
