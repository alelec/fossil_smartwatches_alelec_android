package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dm0 {
    @DexIgnore
    public static /* final */ Object a; // = new Object();
    @DexIgnore
    public static volatile dm0 b;

    @DexIgnore
    public dm0() {
        List list = Collections.EMPTY_LIST;
    }

    @DexIgnore
    public static dm0 a() {
        if (b == null) {
            synchronized (a) {
                if (b == null) {
                    b = new dm0();
                }
            }
        }
        return b;
    }

    @DexIgnore
    public final boolean a(Context context, String str, Intent intent, ServiceConnection serviceConnection, int i) {
        boolean z;
        ComponentName component = intent.getComponent();
        if (component == null) {
            z = false;
        } else {
            z = gm0.c(context, component.getPackageName());
        }
        if (!z) {
            return context.bindService(intent, serviceConnection, i);
        }
        Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
        return false;
    }

    @DexIgnore
    public boolean a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        return a(context, context.getClass().getName(), intent, serviceConnection, i);
    }

    @DexIgnore
    @SuppressLint({"UntrackedBindService"})
    public void a(Context context, ServiceConnection serviceConnection) {
        context.unbindService(serviceConnection);
    }
}
