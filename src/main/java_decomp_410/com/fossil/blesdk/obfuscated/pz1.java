package com.fossil.blesdk.obfuscated;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pz1 {
    @DexIgnore
    public /* final */ Field a;

    @DexIgnore
    public pz1(Field field) {
        i02.a(field);
        this.a = field;
    }

    @DexIgnore
    public <T extends Annotation> T a(Class<T> cls) {
        return this.a.getAnnotation(cls);
    }
}
