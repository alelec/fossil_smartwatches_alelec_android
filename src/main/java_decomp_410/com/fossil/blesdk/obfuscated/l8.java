package com.fossil.blesdk.obfuscated;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class l8 {
    @DexIgnore
    public static /* final */ View.AccessibilityDelegate b; // = new View.AccessibilityDelegate();
    @DexIgnore
    public /* final */ View.AccessibilityDelegate a; // = new a(this);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends View.AccessibilityDelegate {
        @DexIgnore
        public /* final */ l8 a;

        @DexIgnore
        public a(l8 l8Var) {
            this.a = l8Var;
        }

        @DexIgnore
        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            return this.a.a(view, accessibilityEvent);
        }

        @DexIgnore
        public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
            r9 a2 = this.a.a(view);
            if (a2 != null) {
                return (AccessibilityNodeProvider) a2.a();
            }
            return null;
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.a.b(view, accessibilityEvent);
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            this.a.a(view, q9.a(accessibilityNodeInfo));
        }

        @DexIgnore
        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.a.c(view, accessibilityEvent);
        }

        @DexIgnore
        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return this.a.a(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            return this.a.a(view, i, bundle);
        }

        @DexIgnore
        public void sendAccessibilityEvent(View view, int i) {
            this.a.a(view, i);
        }

        @DexIgnore
        public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
            this.a.d(view, accessibilityEvent);
        }
    }

    @DexIgnore
    public View.AccessibilityDelegate a() {
        return this.a;
    }

    @DexIgnore
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        b.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void c(View view, AccessibilityEvent accessibilityEvent) {
        b.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void d(View view, AccessibilityEvent accessibilityEvent) {
        b.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }

    @DexIgnore
    public void a(View view, int i) {
        b.sendAccessibilityEvent(view, i);
    }

    @DexIgnore
    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return b.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void a(View view, q9 q9Var) {
        b.onInitializeAccessibilityNodeInfo(view, q9Var.w());
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return b.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    @DexIgnore
    public r9 a(View view) {
        if (Build.VERSION.SDK_INT < 16) {
            return null;
        }
        AccessibilityNodeProvider accessibilityNodeProvider = b.getAccessibilityNodeProvider(view);
        if (accessibilityNodeProvider != null) {
            return new r9(accessibilityNodeProvider);
        }
        return null;
    }

    @DexIgnore
    public boolean a(View view, int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return b.performAccessibilityAction(view, i, bundle);
        }
        return false;
    }
}
