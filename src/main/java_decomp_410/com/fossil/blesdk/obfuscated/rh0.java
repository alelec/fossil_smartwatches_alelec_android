package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rh0 implements IBinder.DeathRecipient, sh0 {
    @DexIgnore
    public /* final */ WeakReference<BasePendingResult<?>> a;
    @DexIgnore
    public /* final */ WeakReference<vi0> b;
    @DexIgnore
    public /* final */ WeakReference<IBinder> c;

    @DexIgnore
    public rh0(BasePendingResult<?> basePendingResult, vi0 vi0, IBinder iBinder) {
        this.b = new WeakReference<>(vi0);
        this.a = new WeakReference<>(basePendingResult);
        this.c = new WeakReference<>(iBinder);
    }

    @DexIgnore
    public final void a(BasePendingResult<?> basePendingResult) {
        a();
    }

    @DexIgnore
    public final void binderDied() {
        a();
    }

    @DexIgnore
    public final void a() {
        BasePendingResult basePendingResult = (BasePendingResult) this.a.get();
        vi0 vi0 = (vi0) this.b.get();
        if (!(vi0 == null || basePendingResult == null)) {
            vi0.a(basePendingResult.e().intValue());
        }
        IBinder iBinder = (IBinder) this.c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    @DexIgnore
    public /* synthetic */ rh0(BasePendingResult basePendingResult, vi0 vi0, IBinder iBinder, qh0 qh0) {
        this(basePendingResult, (vi0) null, iBinder);
    }
}
