package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ie1 extends IInterface {
    @DexIgnore
    sn0 a(float f) throws RemoteException;

    @DexIgnore
    sn0 a(LatLng latLng, float f) throws RemoteException;
}
