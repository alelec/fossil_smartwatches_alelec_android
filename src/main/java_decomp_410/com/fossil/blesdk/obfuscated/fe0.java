package com.fossil.blesdk.obfuscated;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.de0.d;
import com.fossil.blesdk.obfuscated.kj0;
import com.fossil.blesdk.obfuscated.ve0;
import com.fossil.blesdk.obfuscated.ze0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fe0<O extends de0.d> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ de0<O> b;
    @DexIgnore
    public /* final */ O c;
    @DexIgnore
    public /* final */ yh0<O> d;
    @DexIgnore
    public /* final */ Looper e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ ge0 g;
    @DexIgnore
    public /* final */ df0 h;
    @DexIgnore
    public /* final */ ve0 i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a c; // = new C0015a().a();
        @DexIgnore
        public /* final */ df0 a;
        @DexIgnore
        public /* final */ Looper b;

        @DexIgnore
        public a(df0 df0, Account account, Looper looper) {
            this.a = df0;
            this.b = looper;
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fe0$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.fe0$a$a  reason: collision with other inner class name */
        public static class C0015a {
            @DexIgnore
            public df0 a;
            @DexIgnore
            public Looper b;

            @DexIgnore
            public C0015a a(df0 df0) {
                bk0.a(df0, (Object) "StatusExceptionMapper must not be null.");
                this.a = df0;
                return this;
            }

            @DexIgnore
            public C0015a a(Looper looper) {
                bk0.a(looper, (Object) "Looper must not be null.");
                this.b = looper;
                return this;
            }

            @DexIgnore
            public a a() {
                if (this.a == null) {
                    this.a = new re0();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new a(this.a, this.b);
            }
        }
    }

    @DexIgnore
    public fe0(Context context, de0<O> de0, Looper looper) {
        bk0.a(context, (Object) "Null context is not permitted.");
        bk0.a(de0, (Object) "Api must not be null.");
        bk0.a(looper, (Object) "Looper must not be null.");
        this.a = context.getApplicationContext();
        this.b = de0;
        this.c = null;
        this.e = looper;
        this.d = yh0.a(de0);
        this.g = new yg0(this);
        this.i = ve0.a(this.a);
        this.f = this.i.b();
        this.h = new re0();
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(int i2, T t) {
        t.g();
        this.i.a(this, i2, (te0<? extends me0, de0.b>) t);
        return t;
    }

    @DexIgnore
    public <A extends de0.b, T extends te0<? extends me0, A>> T b(T t) {
        a(0, t);
        return t;
    }

    @DexIgnore
    public <A extends de0.b, T extends te0<? extends me0, A>> T c(T t) {
        a(1, t);
        return t;
    }

    @DexIgnore
    public O d() {
        return this.c;
    }

    @DexIgnore
    public Context e() {
        return this.a;
    }

    @DexIgnore
    public final int f() {
        return this.f;
    }

    @DexIgnore
    public Looper g() {
        return this.e;
    }

    @DexIgnore
    public final yh0<O> h() {
        return this.d;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    public kj0.a b() {
        Account account;
        O o;
        Set<Scope> set;
        kj0.a aVar = new kj0.a();
        O o2 = this.c;
        if (o2 instanceof de0.d.b) {
            GoogleSignInAccount a2 = ((de0.d.b) o2).a();
            if (a2 != null) {
                account = a2.H();
                aVar.a(account);
                o = this.c;
                if (o instanceof de0.d.b) {
                    GoogleSignInAccount a3 = ((de0.d.b) o).a();
                    if (a3 != null) {
                        set = a3.Q();
                        aVar.a((Collection<Scope>) set);
                        aVar.a(this.a.getClass().getName());
                        aVar.b(this.a.getPackageName());
                        return aVar;
                    }
                }
                set = Collections.emptySet();
                aVar.a((Collection<Scope>) set);
                aVar.a(this.a.getClass().getName());
                aVar.b(this.a.getPackageName());
                return aVar;
            }
        }
        O o3 = this.c;
        account = o3 instanceof de0.d.a ? ((de0.d.a) o3).h() : null;
        aVar.a(account);
        o = this.c;
        if (o instanceof de0.d.b) {
        }
        set = Collections.emptySet();
        aVar.a((Collection<Scope>) set);
        aVar.a(this.a.getClass().getName());
        aVar.b(this.a.getPackageName());
        return aVar;
    }

    @DexIgnore
    public final de0<O> c() {
        return this.b;
    }

    @DexIgnore
    public final <TResult, A extends de0.b> wn1<TResult> a(int i2, ff0<A, TResult> ff0) {
        xn1 xn1 = new xn1();
        this.i.a(this, i2, ff0, xn1, this.h);
        return xn1.a();
    }

    @DexIgnore
    public <TResult, A extends de0.b> wn1<TResult> a(ff0<A, TResult> ff0) {
        return a(0, ff0);
    }

    @DexIgnore
    public <A extends de0.b, T extends te0<? extends me0, A>> T a(T t) {
        a(2, t);
        return t;
    }

    @DexIgnore
    @Deprecated
    public <A extends de0.b, T extends bf0<A, ?>, U extends hf0<A, ?>> wn1<Void> a(T t, U u) {
        bk0.a(t);
        bk0.a(u);
        bk0.a(t.b(), (Object) "Listener has already been released.");
        bk0.a(u.a(), (Object) "Listener has already been released.");
        bk0.a(t.b().equals(u.a()), (Object) "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.i.a(this, (bf0<de0.b, ?>) t, (hf0<de0.b, ?>) u);
    }

    @DexIgnore
    public fe0(Activity activity, de0<O> de0, O o, a aVar) {
        bk0.a(activity, (Object) "Null activity is not permitted.");
        bk0.a(de0, (Object) "Api must not be null.");
        bk0.a(aVar, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = activity.getApplicationContext();
        this.b = de0;
        this.c = o;
        this.e = aVar.b;
        this.d = yh0.a(this.b, this.c);
        this.g = new yg0(this);
        this.i = ve0.a(this.a);
        this.f = this.i.b();
        this.h = aVar.a;
        if (!(activity instanceof GoogleApiActivity)) {
            mf0.a(activity, this.i, this.d);
        }
        this.i.a((fe0<?>) this);
    }

    @DexIgnore
    public wn1<Boolean> a(ze0.a<?> aVar) {
        bk0.a(aVar, (Object) "Listener key cannot be null.");
        return this.i.a(this, aVar);
    }

    @DexIgnore
    public de0.f a(Looper looper, ve0.a<O> aVar) {
        return this.b.d().a(this.a, looper, b().a(), this.c, aVar, aVar);
    }

    @DexIgnore
    public ge0 a() {
        return this.g;
    }

    @DexIgnore
    public hh0 a(Context context, Handler handler) {
        return new hh0(context, handler, b().a());
    }

    @DexIgnore
    public fe0(Context context, de0<O> de0, O o, a aVar) {
        bk0.a(context, (Object) "Null context is not permitted.");
        bk0.a(de0, (Object) "Api must not be null.");
        bk0.a(aVar, (Object) "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = context.getApplicationContext();
        this.b = de0;
        this.c = o;
        this.e = aVar.b;
        this.d = yh0.a(this.b, this.c);
        this.g = new yg0(this);
        this.i = ve0.a(this.a);
        this.f = this.i.b();
        this.h = aVar.a;
        this.i.a((fe0<?>) this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @Deprecated
    public fe0(Activity activity, de0<O> de0, O o, df0 df0) {
        this(activity, de0, o, r0.a());
        a.C0015a aVar = new a.C0015a();
        aVar.a(df0);
        aVar.a(activity.getMainLooper());
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @Deprecated
    public fe0(Context context, de0<O> de0, O o, df0 df0) {
        this(context, de0, o, r0.a());
        a.C0015a aVar = new a.C0015a();
        aVar.a(df0);
    }
}
