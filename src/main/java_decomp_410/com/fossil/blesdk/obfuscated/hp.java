package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hp {
    @DexIgnore
    public static /* final */ dp f; // = new dp();
    @DexIgnore
    public /* final */ dp a;
    @DexIgnore
    public /* final */ gp b;
    @DexIgnore
    public /* final */ gq c;
    @DexIgnore
    public /* final */ ContentResolver d;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> e;

    @DexIgnore
    public hp(List<ImageHeaderParser> list, gp gpVar, gq gqVar, ContentResolver contentResolver) {
        this(list, f, gpVar, gqVar, contentResolver);
    }

    @DexIgnore
    public int a(Uri uri) {
        InputStream inputStream = null;
        try {
            InputStream openInputStream = this.d.openInputStream(uri);
            int a2 = io.a(this.e, openInputStream, this.c);
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (IOException unused) {
                }
            }
            return a2;
        } catch (IOException | NullPointerException e2) {
            if (Log.isLoggable("ThumbStreamOpener", 3)) {
                Log.d("ThumbStreamOpener", "Failed to open uri: " + uri, e2);
            }
            if (inputStream == null) {
                return -1;
            }
            try {
                inputStream.close();
                return -1;
            } catch (IOException unused2) {
                return -1;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused3) {
                }
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0030 A[Catch:{ all -> 0x004a }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004d  */
    public final String b(Uri uri) {
        Cursor cursor;
        try {
            cursor = this.b.a(uri);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        String string = cursor.getString(0);
                        if (cursor != null) {
                            cursor.close();
                        }
                        return string;
                    }
                } catch (SecurityException e2) {
                    e = e2;
                    try {
                        if (Log.isLoggable("ThumbStreamOpener", 3)) {
                            Log.d("ThumbStreamOpener", "Failed to query for thumbnail for Uri: " + uri, e);
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (SecurityException e3) {
            e = e3;
            cursor = null;
            if (Log.isLoggable("ThumbStreamOpener", 3)) {
            }
            if (cursor != null) {
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    @DexIgnore
    public InputStream c(Uri uri) throws FileNotFoundException {
        String b2 = b(uri);
        if (TextUtils.isEmpty(b2)) {
            return null;
        }
        File a2 = this.a.a(b2);
        if (!a(a2)) {
            return null;
        }
        Uri fromFile = Uri.fromFile(a2);
        try {
            return this.d.openInputStream(fromFile);
        } catch (NullPointerException e2) {
            throw ((FileNotFoundException) new FileNotFoundException("NPE opening uri: " + uri + " -> " + fromFile).initCause(e2));
        }
    }

    @DexIgnore
    public hp(List<ImageHeaderParser> list, dp dpVar, gp gpVar, gq gqVar, ContentResolver contentResolver) {
        this.a = dpVar;
        this.b = gpVar;
        this.c = gqVar;
        this.d = contentResolver;
        this.e = list;
    }

    @DexIgnore
    public final boolean a(File file) {
        return this.a.a(file) && 0 < this.a.b(file);
    }
}
