package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class vh2 extends uh2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public long u;

    /*
    static {
        w.put(R.id.iv_favorite_contact_icon, 1);
        w.put(R.id.ftv_favorite_contact_name, 2);
        w.put(R.id.ib_remove, 3);
    }
    */

    @DexIgnore
    public vh2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 4, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public vh2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[0], objArr[2], objArr[3], objArr[1]);
        this.u = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
