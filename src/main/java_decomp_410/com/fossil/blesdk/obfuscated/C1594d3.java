package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d3 */
public class C1594d3 {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f4248a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.view.View f4249b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.widget.TextView f4250c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.view.WindowManager.LayoutParams f4251d; // = new android.view.WindowManager.LayoutParams();

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.graphics.Rect f4252e; // = new android.graphics.Rect();

    @DexIgnore
    /* renamed from: f */
    public /* final */ int[] f4253f; // = new int[2];

    @DexIgnore
    /* renamed from: g */
    public /* final */ int[] f4254g; // = new int[2];

    @DexIgnore
    public C1594d3(android.content.Context context) {
        this.f4248a = context;
        this.f4249b = android.view.LayoutInflater.from(this.f4248a).inflate(com.fossil.blesdk.obfuscated.C3250x.abc_tooltip, (android.view.ViewGroup) null);
        this.f4250c = (android.widget.TextView) this.f4249b.findViewById(com.fossil.blesdk.obfuscated.C3158w.message);
        this.f4251d.setTitle(com.fossil.blesdk.obfuscated.C1594d3.class.getSimpleName());
        this.f4251d.packageName = this.f4248a.getPackageName();
        android.view.WindowManager.LayoutParams layoutParams = this.f4251d;
        layoutParams.type = 1002;
        layoutParams.width = -2;
        layoutParams.height = -2;
        layoutParams.format = -3;
        layoutParams.windowAnimations = com.fossil.blesdk.obfuscated.C3398z.Animation_AppCompat_Tooltip;
        layoutParams.flags = 24;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9745a(android.view.View view, int i, int i2, boolean z, java.lang.CharSequence charSequence) {
        if (mo9746b()) {
            mo9743a();
        }
        this.f4250c.setText(charSequence);
        mo9744a(view, i, i2, z, this.f4251d);
        ((android.view.WindowManager) this.f4248a.getSystemService("window")).addView(this.f4249b, this.f4251d);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo9746b() {
        return this.f4249b.getParent() != null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9743a() {
        if (mo9746b()) {
            ((android.view.WindowManager) this.f4248a.getSystemService("window")).removeView(this.f4249b);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo9744a(android.view.View view, int i, int i2, boolean z, android.view.WindowManager.LayoutParams layoutParams) {
        int i3;
        int i4;
        layoutParams.token = view.getApplicationWindowToken();
        int dimensionPixelOffset = this.f4248a.getResources().getDimensionPixelOffset(com.fossil.blesdk.obfuscated.C2998u.tooltip_precise_anchor_threshold);
        if (view.getWidth() < dimensionPixelOffset) {
            i = view.getWidth() / 2;
        }
        if (view.getHeight() >= dimensionPixelOffset) {
            int dimensionPixelOffset2 = this.f4248a.getResources().getDimensionPixelOffset(com.fossil.blesdk.obfuscated.C2998u.tooltip_precise_anchor_extra_offset);
            i4 = i2 + dimensionPixelOffset2;
            i3 = i2 - dimensionPixelOffset2;
        } else {
            i4 = view.getHeight();
            i3 = 0;
        }
        layoutParams.gravity = 49;
        int dimensionPixelOffset3 = this.f4248a.getResources().getDimensionPixelOffset(z ? com.fossil.blesdk.obfuscated.C2998u.tooltip_y_offset_touch : com.fossil.blesdk.obfuscated.C2998u.tooltip_y_offset_non_touch);
        android.view.View a = m5654a(view);
        if (a == null) {
            android.util.Log.e("TooltipPopup", "Cannot find app view");
            return;
        }
        a.getWindowVisibleDisplayFrame(this.f4252e);
        android.graphics.Rect rect = this.f4252e;
        if (rect.left < 0 && rect.top < 0) {
            android.content.res.Resources resources = this.f4248a.getResources();
            int identifier = resources.getIdentifier("status_bar_height", "dimen", "android");
            int dimensionPixelSize = identifier != 0 ? resources.getDimensionPixelSize(identifier) : 0;
            android.util.DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            this.f4252e.set(0, dimensionPixelSize, displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        a.getLocationOnScreen(this.f4254g);
        view.getLocationOnScreen(this.f4253f);
        int[] iArr = this.f4253f;
        int i5 = iArr[0];
        int[] iArr2 = this.f4254g;
        iArr[0] = i5 - iArr2[0];
        iArr[1] = iArr[1] - iArr2[1];
        layoutParams.x = (iArr[0] + i) - (a.getWidth() / 2);
        int makeMeasureSpec = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        this.f4249b.measure(makeMeasureSpec, makeMeasureSpec);
        int measuredHeight = this.f4249b.getMeasuredHeight();
        int[] iArr3 = this.f4253f;
        int i6 = ((iArr3[1] + i3) - dimensionPixelOffset3) - measuredHeight;
        int i7 = iArr3[1] + i4 + dimensionPixelOffset3;
        if (z) {
            if (i6 >= 0) {
                layoutParams.y = i6;
            } else {
                layoutParams.y = i7;
            }
        } else if (measuredHeight + i7 <= this.f4252e.height()) {
            layoutParams.y = i7;
        } else {
            layoutParams.y = i6;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.view.View m5654a(android.view.View view) {
        android.view.View rootView = view.getRootView();
        android.view.ViewGroup.LayoutParams layoutParams = rootView.getLayoutParams();
        if ((layoutParams instanceof android.view.WindowManager.LayoutParams) && ((android.view.WindowManager.LayoutParams) layoutParams).type == 2) {
            return rootView;
        }
        for (android.content.Context context = view.getContext(); context instanceof android.content.ContextWrapper; context = ((android.content.ContextWrapper) context).getBaseContext()) {
            if (context instanceof android.app.Activity) {
                return ((android.app.Activity) context).getWindow().getDecorView();
            }
        }
        return rootView;
    }
}
