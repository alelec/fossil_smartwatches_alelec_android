package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class jc3 implements MembersInjector<HeartRateOverviewFragment> {
    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
        heartRateOverviewFragment.k = heartRateOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
        heartRateOverviewFragment.l = heartRateOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(HeartRateOverviewFragment heartRateOverviewFragment, HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        heartRateOverviewFragment.m = heartRateOverviewMonthPresenter;
    }
}
