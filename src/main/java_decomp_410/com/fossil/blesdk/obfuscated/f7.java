package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f7 extends Drawable implements Drawable.Callback, e7, d7 {
    @DexIgnore
    public static /* final */ PorterDuff.Mode k; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public int e;
    @DexIgnore
    public PorterDuff.Mode f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public a h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public Drawable j;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Drawable.ConstantState {
        @DexIgnore
        public int a;
        @DexIgnore
        public Drawable.ConstantState b;
        @DexIgnore
        public ColorStateList c; // = null;
        @DexIgnore
        public PorterDuff.Mode d; // = f7.k;

        @DexIgnore
        public a(a aVar, Resources resources) {
            if (aVar != null) {
                this.a = aVar.a;
                this.b = aVar.b;
                this.c = aVar.c;
                this.d = aVar.d;
            }
        }

        @DexIgnore
        public boolean a() {
            return this.b != null;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            int i = this.a;
            Drawable.ConstantState constantState = this.b;
            return i | (constantState != null ? constantState.getChangingConfigurations() : 0);
        }

        @DexIgnore
        public Drawable newDrawable() {
            return newDrawable((Resources) null);
        }

        @DexIgnore
        public abstract Drawable newDrawable(Resources resources);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends a {
        @DexIgnore
        public b(a aVar, Resources resources) {
            super(aVar, resources);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new f7(this, resources);
        }
    }

    @DexIgnore
    public f7(a aVar, Resources resources) {
        this.h = aVar;
        a(resources);
    }

    @DexIgnore
    public final void a(Resources resources) {
        a aVar = this.h;
        if (aVar != null) {
            Drawable.ConstantState constantState = aVar.b;
            if (constantState != null) {
                a(constantState.newDrawable(resources));
            }
        }
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public a c() {
        return new b(this.h, (Resources) null);
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.j.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int changingConfigurations = super.getChangingConfigurations();
        a aVar = this.h;
        return changingConfigurations | (aVar != null ? aVar.getChangingConfigurations() : 0) | this.j.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        a aVar = this.h;
        if (aVar == null || !aVar.a()) {
            return null;
        }
        this.h.a = getChangingConfigurations();
        return this.h;
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.j.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.j.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.j.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.j.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.j.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.j.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        return this.j.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.j.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        return this.j.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return this.j.isAutoMirrored();
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList;
        if (b()) {
            a aVar = this.h;
            if (aVar != null) {
                colorStateList = aVar.c;
                return (colorStateList == null && colorStateList.isStateful()) || this.j.isStateful();
            }
        }
        colorStateList = null;
        if (colorStateList == null) {
        }
    }

    @DexIgnore
    public void jumpToCurrentState() {
        this.j.jumpToCurrentState();
    }

    @DexIgnore
    public Drawable mutate() {
        if (!this.i && super.mutate() == this) {
            this.h = c();
            Drawable drawable = this.j;
            if (drawable != null) {
                drawable.mutate();
            }
            a aVar = this.h;
            if (aVar != null) {
                Drawable drawable2 = this.j;
                aVar.b = drawable2 != null ? drawable2.getConstantState() : null;
            }
            this.i = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.j;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onLevelChange(int i2) {
        return this.j.setLevel(i2);
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j2) {
        scheduleSelf(runnable, j2);
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.j.setAlpha(i2);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        this.j.setAutoMirrored(z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i2) {
        this.j.setChangingConfigurations(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.j.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.j.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.j.setFilterBitmap(z);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return a(iArr) || this.j.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        this.h.c = colorStateList;
        a(getState());
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        this.h.d = mode;
        a(getState());
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.j.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    @DexIgnore
    public final boolean a(int[] iArr) {
        if (!b()) {
            return false;
        }
        a aVar = this.h;
        ColorStateList colorStateList = aVar.c;
        PorterDuff.Mode mode = aVar.d;
        if (colorStateList == null || mode == null) {
            this.g = false;
            clearColorFilter();
        } else {
            int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
            if (!(this.g && colorForState == this.e && mode == this.f)) {
                setColorFilter(colorForState, mode);
                this.e = colorForState;
                this.f = mode;
                this.g = true;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public f7(Drawable drawable) {
        this.h = c();
        a(drawable);
    }

    @DexIgnore
    public final Drawable a() {
        return this.j;
    }

    @DexIgnore
    public final void a(Drawable drawable) {
        Drawable drawable2 = this.j;
        if (drawable2 != null) {
            drawable2.setCallback((Drawable.Callback) null);
        }
        this.j = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
            setVisible(drawable.isVisible(), true);
            setState(drawable.getState());
            setLevel(drawable.getLevel());
            setBounds(drawable.getBounds());
            a aVar = this.h;
            if (aVar != null) {
                aVar.b = drawable.getConstantState();
            }
        }
        invalidateSelf();
    }
}
