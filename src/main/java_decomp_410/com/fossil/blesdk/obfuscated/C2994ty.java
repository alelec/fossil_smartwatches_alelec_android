package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ty */
public class C2994ty {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.ExecutorService f9785a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ty$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ty$a */
    public class C2995a implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.lang.Runnable f9786e;

        @DexIgnore
        public C2995a(com.fossil.blesdk.obfuscated.C2994ty tyVar, java.lang.Runnable runnable) {
            this.f9786e = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.f9786e.run();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Failed to execute task.", e);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ty$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ty$b */
    public class C2996b implements java.util.concurrent.Callable<T> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.util.concurrent.Callable f9787e;

        @DexIgnore
        public C2996b(com.fossil.blesdk.obfuscated.C2994ty tyVar, java.util.concurrent.Callable callable) {
            this.f9787e = callable;
        }

        @DexIgnore
        public T call() throws java.lang.Exception {
            try {
                return this.f9787e.call();
            } catch (java.lang.Exception e) {
                com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Failed to execute task.", e);
                return null;
            }
        }
    }

    @DexIgnore
    public C2994ty(java.util.concurrent.ExecutorService executorService) {
        this.f9785a = executorService;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.concurrent.Future<?> mo16606a(java.lang.Runnable runnable) {
        try {
            return this.f9785a.submit(new com.fossil.blesdk.obfuscated.C2994ty.C2995a(this, runnable));
        } catch (java.util.concurrent.RejectedExecutionException unused) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public <T> T mo16608b(java.util.concurrent.Callable<T> callable) {
        try {
            if (android.os.Looper.getMainLooper() == android.os.Looper.myLooper()) {
                return this.f9785a.submit(callable).get(4, java.util.concurrent.TimeUnit.SECONDS);
            }
            return this.f9785a.submit(callable).get();
        } catch (java.util.concurrent.RejectedExecutionException unused) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "Failed to execute task.", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public <T> java.util.concurrent.Future<T> mo16607a(java.util.concurrent.Callable<T> callable) {
        try {
            return this.f9785a.submit(new com.fossil.blesdk.obfuscated.C2994ty.C2996b(this, callable));
        } catch (java.util.concurrent.RejectedExecutionException unused) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Executor is shut down because we're handling a fatal crash.");
            return null;
        }
    }
}
