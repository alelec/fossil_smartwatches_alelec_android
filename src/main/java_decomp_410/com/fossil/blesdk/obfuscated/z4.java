package com.fossil.blesdk.obfuscated;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class z4 {
    @DexIgnore
    public List<ConstraintWidget> a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public /* final */ int[] e; // = {this.b, this.c};
    @DexIgnore
    public List<ConstraintWidget> f; // = new ArrayList();
    @DexIgnore
    public List<ConstraintWidget> g; // = new ArrayList();
    @DexIgnore
    public HashSet<ConstraintWidget> h; // = new HashSet<>();
    @DexIgnore
    public HashSet<ConstraintWidget> i; // = new HashSet<>();
    @DexIgnore
    public List<ConstraintWidget> j; // = new ArrayList();
    @DexIgnore
    public List<ConstraintWidget> k; // = new ArrayList();

    @DexIgnore
    public z4(List<ConstraintWidget> list) {
        this.a = list;
    }

    @DexIgnore
    public List<ConstraintWidget> a(int i2) {
        if (i2 == 0) {
            return this.f;
        }
        if (i2 == 1) {
            return this.g;
        }
        return null;
    }

    @DexIgnore
    public Set<ConstraintWidget> b(int i2) {
        if (i2 == 0) {
            return this.h;
        }
        if (i2 == 1) {
            return this.i;
        }
        return null;
    }

    @DexIgnore
    public void a(ConstraintWidget constraintWidget, int i2) {
        if (i2 == 0) {
            this.h.add(constraintWidget);
        } else if (i2 == 1) {
            this.i.add(constraintWidget);
        }
    }

    @DexIgnore
    public void b() {
        int size = this.k.size();
        for (int i2 = 0; i2 < size; i2++) {
            a(this.k.get(i2));
        }
    }

    @DexIgnore
    public List<ConstraintWidget> a() {
        if (!this.j.isEmpty()) {
            return this.j;
        }
        int size = this.a.size();
        for (int i2 = 0; i2 < size; i2++) {
            ConstraintWidget constraintWidget = this.a.get(i2);
            if (!constraintWidget.b0) {
                a((ArrayList<ConstraintWidget>) (ArrayList) this.j, constraintWidget);
            }
        }
        this.k.clear();
        this.k.addAll(this.a);
        this.k.removeAll(this.j);
        return this.j;
    }

    @DexIgnore
    public z4(List<ConstraintWidget> list, boolean z) {
        this.a = list;
        this.d = z;
    }

    @DexIgnore
    public final void a(ArrayList<ConstraintWidget> arrayList, ConstraintWidget constraintWidget) {
        if (!constraintWidget.d0) {
            arrayList.add(constraintWidget);
            constraintWidget.d0 = true;
            if (!constraintWidget.z()) {
                if (constraintWidget instanceof b5) {
                    b5 b5Var = (b5) constraintWidget;
                    int i2 = b5Var.l0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        a(arrayList, b5Var.k0[i3]);
                    }
                }
                for (ConstraintAnchor constraintAnchor : constraintWidget.A) {
                    ConstraintAnchor constraintAnchor2 = constraintAnchor.d;
                    if (constraintAnchor2 != null) {
                        ConstraintWidget constraintWidget2 = constraintAnchor2.b;
                        if (!(constraintAnchor2 == null || constraintWidget2 == constraintWidget.l())) {
                            a(arrayList, constraintWidget2);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0087  */
    public final void a(ConstraintWidget constraintWidget) {
        ConstraintAnchor constraintAnchor;
        int i2;
        int i3;
        ConstraintAnchor constraintAnchor2;
        ConstraintAnchor constraintAnchor3;
        int i4;
        if (constraintWidget.b0 && !constraintWidget.z()) {
            boolean z = false;
            boolean z2 = constraintWidget.u.d != null;
            if (z2) {
                constraintAnchor = constraintWidget.u.d;
            } else {
                constraintAnchor = constraintWidget.s.d;
            }
            if (constraintAnchor != null) {
                ConstraintWidget constraintWidget2 = constraintAnchor.b;
                if (!constraintWidget2.c0) {
                    a(constraintWidget2);
                }
                ConstraintAnchor.Type type = constraintAnchor.c;
                if (type == ConstraintAnchor.Type.RIGHT) {
                    ConstraintWidget constraintWidget3 = constraintAnchor.b;
                    i2 = constraintWidget3.t() + constraintWidget3.I;
                } else if (type == ConstraintAnchor.Type.LEFT) {
                    i2 = constraintAnchor.b.I;
                }
                if (!z2) {
                    i3 = i2 - constraintWidget.u.b();
                } else {
                    i3 = i2 + constraintWidget.s.b() + constraintWidget.t();
                }
                constraintWidget.a(i3 - constraintWidget.t(), i3);
                constraintAnchor2 = constraintWidget.w.d;
                if (constraintAnchor2 == null) {
                    ConstraintWidget constraintWidget4 = constraintAnchor2.b;
                    if (!constraintWidget4.c0) {
                        a(constraintWidget4);
                    }
                    ConstraintWidget constraintWidget5 = constraintAnchor2.b;
                    int i5 = (constraintWidget5.J + constraintWidget5.Q) - constraintWidget.Q;
                    constraintWidget.e(i5, constraintWidget.F + i5);
                    constraintWidget.c0 = true;
                    return;
                }
                if (constraintWidget.v.d != null) {
                    z = true;
                }
                if (z) {
                    constraintAnchor3 = constraintWidget.v.d;
                } else {
                    constraintAnchor3 = constraintWidget.t.d;
                }
                if (constraintAnchor3 != null) {
                    ConstraintWidget constraintWidget6 = constraintAnchor3.b;
                    if (!constraintWidget6.c0) {
                        a(constraintWidget6);
                    }
                    ConstraintAnchor.Type type2 = constraintAnchor3.c;
                    if (type2 == ConstraintAnchor.Type.BOTTOM) {
                        ConstraintWidget constraintWidget7 = constraintAnchor3.b;
                        i3 = constraintWidget7.J + constraintWidget7.j();
                    } else if (type2 == ConstraintAnchor.Type.TOP) {
                        i3 = constraintAnchor3.b.J;
                    }
                }
                if (z) {
                    i4 = i3 - constraintWidget.v.b();
                } else {
                    i4 = i3 + constraintWidget.t.b() + constraintWidget.j();
                }
                constraintWidget.e(i4 - constraintWidget.j(), i4);
                constraintWidget.c0 = true;
                return;
            }
            i2 = 0;
            if (!z2) {
            }
            constraintWidget.a(i3 - constraintWidget.t(), i3);
            constraintAnchor2 = constraintWidget.w.d;
            if (constraintAnchor2 == null) {
            }
        }
    }
}
