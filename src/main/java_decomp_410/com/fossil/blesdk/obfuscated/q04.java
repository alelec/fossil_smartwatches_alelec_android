package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.tencent.wxop.stat.a.f;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class q04 extends o04 {
    @DexIgnore
    public static String o;
    @DexIgnore
    public String m; // = null;
    @DexIgnore
    public String n; // = null;

    @DexIgnore
    public q04(Context context, int i, k04 k04) {
        super(context, i, k04);
        this.m = t04.a(context).b();
        if (o == null) {
            o = e24.l(context);
        }
    }

    @DexIgnore
    public f a() {
        return f.NETWORK_MONITOR;
    }

    @DexIgnore
    public void a(String str) {
        this.n = str;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        j24.a(jSONObject, "op", o);
        j24.a(jSONObject, "cn", this.m);
        jSONObject.put("sp", this.n);
        return true;
    }
}
