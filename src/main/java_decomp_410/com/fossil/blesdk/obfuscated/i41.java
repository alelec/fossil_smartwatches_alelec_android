package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i41 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<i41> CREATOR; // = new j41();
    @DexIgnore
    public static /* final */ List<jj0> l; // = Collections.emptyList();
    @DexIgnore
    public LocationRequest e;
    @DexIgnore
    public List<jj0> f;
    @DexIgnore
    public String g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public String k;

    @DexIgnore
    public i41(LocationRequest locationRequest, List<jj0> list, String str, boolean z, boolean z2, boolean z3, String str2) {
        this.e = locationRequest;
        this.f = list;
        this.g = str;
        this.h = z;
        this.i = z2;
        this.j = z3;
        this.k = str2;
    }

    @DexIgnore
    @Deprecated
    public static i41 a(LocationRequest locationRequest) {
        return new i41(locationRequest, l, (String) null, false, false, false, (String) null);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof i41)) {
            return false;
        }
        i41 i41 = (i41) obj;
        return zj0.a(this.e, i41.e) && zj0.a(this.f, i41.f) && zj0.a(this.g, i41.g) && this.h == i41.h && this.i == i41.i && this.j == i41.j && zj0.a(this.k, i41.k);
    }

    @DexIgnore
    public final int hashCode() {
        return this.e.hashCode();
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.e);
        if (this.g != null) {
            sb.append(" tag=");
            sb.append(this.g);
        }
        if (this.k != null) {
            sb.append(" moduleId=");
            sb.append(this.k);
        }
        sb.append(" hideAppOps=");
        sb.append(this.h);
        sb.append(" clients=");
        sb.append(this.f);
        sb.append(" forceCoarseLocation=");
        sb.append(this.i);
        if (this.j) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, (Parcelable) this.e, i2, false);
        kk0.b(parcel, 5, this.f, false);
        kk0.a(parcel, 6, this.g, false);
        kk0.a(parcel, 7, this.h);
        kk0.a(parcel, 8, this.i);
        kk0.a(parcel, 9, this.j);
        kk0.a(parcel, 10, this.k, false);
        kk0.a(parcel, a);
    }
}
