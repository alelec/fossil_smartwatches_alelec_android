package com.fossil.blesdk.obfuscated;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qv1 {
    @DexIgnore
    public static /* final */ byte[] a; // = a();

    @DexIgnore
    public static byte[] a() {
        byte[] bArr = new byte[128];
        Arrays.fill(bArr, (byte) -1);
        for (int i = 0; i <= 9; i++) {
            bArr[i + 48] = (byte) i;
        }
        for (int i2 = 0; i2 <= 26; i2++) {
            byte b = (byte) (i2 + 10);
            bArr[i2 + 65] = b;
            bArr[i2 + 97] = b;
        }
        return bArr;
    }

    @DexIgnore
    public static int a(char c) {
        if (c < 128) {
            return a[c];
        }
        return -1;
    }

    @DexIgnore
    public static Long a(String str) {
        return a(str, 10);
    }

    @DexIgnore
    public static Long a(String str, int i) {
        String str2 = str;
        int i2 = i;
        st1.a(str);
        if (str2.isEmpty()) {
            return null;
        }
        if (i2 < 2 || i2 > 36) {
            throw new IllegalArgumentException("radix must be between MIN_RADIX and MAX_RADIX but was " + i2);
        }
        int i3 = 0;
        if (str2.charAt(0) == '-') {
            i3 = 1;
        }
        if (i3 == str.length()) {
            return null;
        }
        int i4 = i3 + 1;
        int a2 = a(str2.charAt(i3));
        if (a2 < 0 || a2 >= i2) {
            return null;
        }
        long j = (long) (-a2);
        long j2 = (long) i2;
        long j3 = Long.MIN_VALUE / j2;
        while (i4 < str.length()) {
            int i5 = i4 + 1;
            int a3 = a(str2.charAt(i4));
            if (a3 < 0 || a3 >= i2 || j < j3) {
                return null;
            }
            long j4 = j * j2;
            long j5 = (long) a3;
            if (j4 < j5 - Long.MIN_VALUE) {
                return null;
            }
            j = j4 - j5;
            i4 = i5;
        }
        if (i3 != 0) {
            return Long.valueOf(j);
        }
        if (j == Long.MIN_VALUE) {
            return null;
        }
        return Long.valueOf(-j);
    }
}
