package com.fossil.blesdk.obfuscated;

import io.reactivex.internal.operators.observable.ObservableScalarXMap$ScalarDisposable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ba4<T> extends o84<T> implements q94<T> {
    @DexIgnore
    public /* final */ T e;

    @DexIgnore
    public ba4(T t) {
        this.e = t;
    }

    @DexIgnore
    public void b(q84<? super T> q84) {
        ObservableScalarXMap$ScalarDisposable observableScalarXMap$ScalarDisposable = new ObservableScalarXMap$ScalarDisposable(q84, this.e);
        q84.onSubscribe(observableScalarXMap$ScalarDisposable);
        observableScalarXMap$ScalarDisposable.run();
    }

    @DexIgnore
    public T call() {
        return this.e;
    }
}
