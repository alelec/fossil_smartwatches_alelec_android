package com.fossil.blesdk.obfuscated;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface bo2 extends BaseProvider {
    @DexIgnore
    void a(List<ServerSetting> list);

    @DexIgnore
    boolean a();

    @DexIgnore
    boolean addOrUpdateServerSetting(ServerSetting serverSetting);

    @DexIgnore
    ServerSetting getServerSettingByKey(String str);
}
