package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vb0 {
    @DexIgnore
    public static xb0 a(Context context, GoogleSignInOptions googleSignInOptions) {
        bk0.a(googleSignInOptions);
        return new xb0(context, googleSignInOptions);
    }

    @DexIgnore
    public static GoogleSignInAccount a(Context context) {
        return pc0.a(context).b();
    }

    @DexIgnore
    public static boolean a(GoogleSignInAccount googleSignInAccount, yb0 yb0) {
        bk0.a(yb0, (Object) "Please provide a non-null GoogleSignInOptionsExtension");
        return a(googleSignInAccount, a(yb0.b()));
    }

    @DexIgnore
    public static boolean a(GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        if (googleSignInAccount == null) {
            return false;
        }
        HashSet hashSet = new HashSet();
        Collections.addAll(hashSet, scopeArr);
        return googleSignInAccount.M().containsAll(hashSet);
    }

    @DexIgnore
    public static void a(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, yb0 yb0) {
        bk0.a(fragment, (Object) "Please provide a non-null Fragment");
        bk0.a(yb0, (Object) "Please provide a non-null GoogleSignInOptionsExtension");
        a(fragment, i, googleSignInAccount, a(yb0.b()));
    }

    @DexIgnore
    public static void a(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        bk0.a(fragment, (Object) "Please provide a non-null Fragment");
        bk0.a(scopeArr, (Object) "Please provide at least one scope");
        fragment.startActivityForResult(a(fragment.getActivity(), googleSignInAccount, scopeArr), i);
    }

    @DexIgnore
    public static Intent a(Activity activity, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        GoogleSignInOptions.a aVar = new GoogleSignInOptions.a();
        if (scopeArr.length > 0) {
            aVar.a(scopeArr[0], scopeArr);
        }
        if (googleSignInAccount != null && !TextUtils.isEmpty(googleSignInAccount.J())) {
            aVar.c(googleSignInAccount.J());
        }
        return new xb0(activity, aVar.a()).i();
    }

    @DexIgnore
    public static Scope[] a(List<Scope> list) {
        return list == null ? new Scope[0] : (Scope[]) list.toArray(new Scope[list.size()]);
    }
}
