package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ip */
public final class C2050ip {

    @DexIgnore
    /* renamed from: a */
    public /* final */ boolean f6153a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.util.Map<com.fossil.blesdk.obfuscated.C2143jo, com.fossil.blesdk.obfuscated.C2050ip.C2055d> f6154b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.ref.ReferenceQueue<com.fossil.blesdk.obfuscated.C3128vp<?>> f6155c;

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C3128vp.C3129a f6156d;

    @DexIgnore
    /* renamed from: e */
    public volatile boolean f6157e;

    @DexIgnore
    /* renamed from: f */
    public volatile com.fossil.blesdk.obfuscated.C2050ip.C2054c f6158f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ip$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ip$a */
    public class C2051a implements java.util.concurrent.ThreadFactory {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ip$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.ip$a$a */
        public class C2052a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ java.lang.Runnable f6159e;

            @DexIgnore
            public C2052a(com.fossil.blesdk.obfuscated.C2050ip.C2051a aVar, java.lang.Runnable runnable) {
                this.f6159e = runnable;
            }

            @DexIgnore
            public void run() {
                android.os.Process.setThreadPriority(10);
                this.f6159e.run();
            }
        }

        @DexIgnore
        public java.lang.Thread newThread(java.lang.Runnable runnable) {
            return new java.lang.Thread(new com.fossil.blesdk.obfuscated.C2050ip.C2051a.C2052a(this, runnable), "glide-active-resources");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ip$b")
    /* renamed from: com.fossil.blesdk.obfuscated.ip$b */
    public class C2053b implements java.lang.Runnable {
        @DexIgnore
        public C2053b() {
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2050ip.this.mo12055a();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.ip$c */
    public interface C2054c {
        @DexIgnore
        /* renamed from: a */
        void mo12064a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ip$d")
    /* renamed from: com.fossil.blesdk.obfuscated.ip$d */
    public static final class C2055d extends java.lang.ref.WeakReference<com.fossil.blesdk.obfuscated.C3128vp<?>> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2143jo f6161a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f6162b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C1438aq<?> f6163c;

        @DexIgnore
        public C2055d(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp<?> vpVar, java.lang.ref.ReferenceQueue<? super com.fossil.blesdk.obfuscated.C3128vp<?>> referenceQueue, boolean z) {
            super(vpVar, referenceQueue);
            com.fossil.blesdk.obfuscated.C1438aq<?> aqVar;
            com.fossil.blesdk.obfuscated.C2992tw.m14457a(joVar);
            this.f6161a = joVar;
            if (!vpVar.mo17155f() || !z) {
                aqVar = null;
            } else {
                com.fossil.blesdk.obfuscated.C1438aq<?> e = vpVar.mo17154e();
                com.fossil.blesdk.obfuscated.C2992tw.m14457a(e);
                aqVar = e;
            }
            this.f6163c = aqVar;
            this.f6162b = vpVar.mo17155f();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo12065a() {
            this.f6163c = null;
            clear();
        }
    }

    @DexIgnore
    public C2050ip(boolean z) {
        this(z, java.util.concurrent.Executors.newSingleThreadExecutor(new com.fossil.blesdk.obfuscated.C2050ip.C2051a()));
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12059a(com.fossil.blesdk.obfuscated.C3128vp.C3129a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.f6156d = aVar;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return r0;
     */
    @DexIgnore
    /* renamed from: b */
    public synchronized com.fossil.blesdk.obfuscated.C3128vp<?> mo12060b(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        com.fossil.blesdk.obfuscated.C2050ip.C2055d dVar = this.f6154b.get(joVar);
        if (dVar == null) {
            return null;
        }
        com.fossil.blesdk.obfuscated.C3128vp<?> vpVar = (com.fossil.blesdk.obfuscated.C3128vp) dVar.get();
        if (vpVar == null) {
            mo12056a(dVar);
        }
    }

    @DexIgnore
    public C2050ip(boolean z, java.util.concurrent.Executor executor) {
        this.f6154b = new java.util.HashMap();
        this.f6155c = new java.lang.ref.ReferenceQueue<>();
        this.f6153a = z;
        executor.execute(new com.fossil.blesdk.obfuscated.C2050ip.C2053b());
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo12058a(com.fossil.blesdk.obfuscated.C2143jo joVar, com.fossil.blesdk.obfuscated.C3128vp<?> vpVar) {
        com.fossil.blesdk.obfuscated.C2050ip.C2055d put = this.f6154b.put(joVar, new com.fossil.blesdk.obfuscated.C2050ip.C2055d(joVar, vpVar, this.f6155c, this.f6153a));
        if (put != null) {
            put.mo12065a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo12057a(com.fossil.blesdk.obfuscated.C2143jo joVar) {
        com.fossil.blesdk.obfuscated.C2050ip.C2055d remove = this.f6154b.remove(joVar);
        if (remove != null) {
            remove.mo12065a();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12056a(com.fossil.blesdk.obfuscated.C2050ip.C2055d dVar) {
        synchronized (this) {
            this.f6154b.remove(dVar.f6161a);
            if (dVar.f6162b) {
                if (dVar.f6163c != null) {
                    com.fossil.blesdk.obfuscated.C3128vp vpVar = new com.fossil.blesdk.obfuscated.C3128vp(dVar.f6163c, true, false, dVar.f6161a, this.f6156d);
                    this.f6156d.mo15336a(dVar.f6161a, vpVar);
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12055a() {
        while (!this.f6157e) {
            try {
                mo12056a((com.fossil.blesdk.obfuscated.C2050ip.C2055d) this.f6155c.remove());
                com.fossil.blesdk.obfuscated.C2050ip.C2054c cVar = this.f6158f;
                if (cVar != null) {
                    cVar.mo12064a();
                }
            } catch (java.lang.InterruptedException unused) {
                java.lang.Thread.currentThread().interrupt();
            }
        }
    }
}
