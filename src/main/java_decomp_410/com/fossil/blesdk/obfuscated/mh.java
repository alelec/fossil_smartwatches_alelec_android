package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.transition.AutoTransition;
import androidx.transition.Transition;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mh {
    @DexIgnore
    public static Transition a; // = new AutoTransition();
    @DexIgnore
    public static ThreadLocal<WeakReference<g4<ViewGroup, ArrayList<Transition>>>> b; // = new ThreadLocal<>();
    @DexIgnore
    public static ArrayList<ViewGroup> c; // = new ArrayList<>();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
        @DexIgnore
        public Transition e;
        @DexIgnore
        public ViewGroup f;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.mh$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.mh$a$a  reason: collision with other inner class name */
        public class C0023a extends lh {
            @DexIgnore
            public /* final */ /* synthetic */ g4 a;

            @DexIgnore
            public C0023a(g4 g4Var) {
                this.a = g4Var;
            }

            @DexIgnore
            public void c(Transition transition) {
                ((ArrayList) this.a.get(a.this.f)).remove(transition);
            }
        }

        @DexIgnore
        public a(Transition transition, ViewGroup viewGroup) {
            this.e = transition;
            this.f = viewGroup;
        }

        @DexIgnore
        public final void a() {
            this.f.getViewTreeObserver().removeOnPreDrawListener(this);
            this.f.removeOnAttachStateChangeListener(this);
        }

        @DexIgnore
        public boolean onPreDraw() {
            a();
            if (!mh.c.remove(this.f)) {
                return true;
            }
            g4<ViewGroup, ArrayList<Transition>> a = mh.a();
            ArrayList arrayList = a.get(this.f);
            ArrayList arrayList2 = null;
            if (arrayList == null) {
                arrayList = new ArrayList();
                a.put(this.f, arrayList);
            } else if (arrayList.size() > 0) {
                arrayList2 = new ArrayList(arrayList);
            }
            arrayList.add(this.e);
            this.e.a((Transition.f) new C0023a(a));
            this.e.a(this.f, false);
            if (arrayList2 != null) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    ((Transition) it.next()).e(this.f);
                }
            }
            this.e.a(this.f);
            return true;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            a();
            mh.c.remove(this.f);
            ArrayList arrayList = mh.a().get(this.f);
            if (arrayList != null && arrayList.size() > 0) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((Transition) it.next()).e(this.f);
                }
            }
            this.e.a(true);
        }
    }

    @DexIgnore
    public static g4<ViewGroup, ArrayList<Transition>> a() {
        WeakReference weakReference = b.get();
        if (weakReference != null) {
            g4<ViewGroup, ArrayList<Transition>> g4Var = (g4) weakReference.get();
            if (g4Var != null) {
                return g4Var;
            }
        }
        g4<ViewGroup, ArrayList<Transition>> g4Var2 = new g4<>();
        b.set(new WeakReference(g4Var2));
        return g4Var2;
    }

    @DexIgnore
    public static void b(ViewGroup viewGroup, Transition transition) {
        if (transition != null && viewGroup != null) {
            a aVar = new a(transition, viewGroup);
            viewGroup.addOnAttachStateChangeListener(aVar);
            viewGroup.getViewTreeObserver().addOnPreDrawListener(aVar);
        }
    }

    @DexIgnore
    public static void c(ViewGroup viewGroup, Transition transition) {
        ArrayList arrayList = a().get(viewGroup);
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                ((Transition) it.next()).c((View) viewGroup);
            }
        }
        if (transition != null) {
            transition.a(viewGroup, true);
        }
        ih a2 = ih.a(viewGroup);
        if (a2 != null) {
            a2.a();
        }
    }

    @DexIgnore
    public static void a(ViewGroup viewGroup, Transition transition) {
        if (!c.contains(viewGroup) && f9.z(viewGroup)) {
            c.add(viewGroup);
            if (transition == null) {
                transition = a;
            }
            Transition clone = transition.clone();
            c(viewGroup, clone);
            ih.a(viewGroup, (ih) null);
            b(viewGroup, clone);
        }
    }
}
