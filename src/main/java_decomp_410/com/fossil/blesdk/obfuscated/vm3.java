package com.fossil.blesdk.obfuscated;

import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vm3 extends rm3 {
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ sm3 g;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public vm3(sm3 sm3) {
        kd4.b(sm3, "mPairingInstructionsView");
        this.g = sm3;
    }

    @DexIgnore
    public void a(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public void f() {
        this.g.g();
        this.g.s(!this.f);
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h() {
        bn2 bn2 = bn2.d;
        sm3 sm3 = this.g;
        if (sm3 == null) {
            throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment");
        } else if (bn2.a(bn2, ((mm3) sm3).getContext(), "PAIR_DEVICE", false, 4, (Object) null)) {
            this.g.c0();
        }
    }

    @DexIgnore
    public boolean i() {
        return this.f;
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }
}
