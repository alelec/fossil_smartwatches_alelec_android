package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.u6;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class si extends ri {
    @DexIgnore
    public static /* final */ PorterDuff.Mode n; // = PorterDuff.Mode.SRC_IN;
    @DexIgnore
    public h f;
    @DexIgnore
    public PorterDuffColorFilter g;
    @DexIgnore
    public ColorFilter h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ float[] k;
    @DexIgnore
    public /* final */ Matrix l;
    @DexIgnore
    public /* final */ Rect m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            if (s6.a(xmlPullParser, "pathData")) {
                TypedArray a = s6.a(resources, theme, attributeSet, ki.d);
                a(a);
                a.recycle();
            }
        }

        @DexIgnore
        public boolean b() {
            return true;
        }

        @DexIgnore
        public b(b bVar) {
            super(bVar);
        }

        @DexIgnore
        public final void a(TypedArray typedArray) {
            String string = typedArray.getString(0);
            if (string != null) {
                this.b = string;
            }
            String string2 = typedArray.getString(1);
            if (string2 != null) {
                this.a = u6.a(string2);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public boolean a() {
            return false;
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h extends Drawable.ConstantState {
        @DexIgnore
        public int a;
        @DexIgnore
        public g b;
        @DexIgnore
        public ColorStateList c;
        @DexIgnore
        public PorterDuff.Mode d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Bitmap f;
        @DexIgnore
        public ColorStateList g;
        @DexIgnore
        public PorterDuff.Mode h;
        @DexIgnore
        public int i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public Paint l;

        @DexIgnore
        public h(h hVar) {
            this.c = null;
            this.d = si.n;
            if (hVar != null) {
                this.a = hVar.a;
                this.b = new g(hVar.b);
                Paint paint = hVar.b.e;
                if (paint != null) {
                    this.b.e = new Paint(paint);
                }
                Paint paint2 = hVar.b.d;
                if (paint2 != null) {
                    this.b.d = new Paint(paint2);
                }
                this.c = hVar.c;
                this.d = hVar.d;
                this.e = hVar.e;
            }
        }

        @DexIgnore
        public void a(Canvas canvas, ColorFilter colorFilter, Rect rect) {
            canvas.drawBitmap(this.f, (Rect) null, rect, a(colorFilter));
        }

        @DexIgnore
        public boolean b() {
            return this.b.getRootAlpha() < 255;
        }

        @DexIgnore
        public void c(int i2, int i3) {
            this.f.eraseColor(0);
            this.b.a(new Canvas(this.f), i2, i3, (ColorFilter) null);
        }

        @DexIgnore
        public void d() {
            this.g = this.c;
            this.h = this.d;
            this.i = this.b.getRootAlpha();
            this.j = this.e;
            this.k = false;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new si(this);
        }

        @DexIgnore
        public void b(int i2, int i3) {
            if (this.f == null || !a(i2, i3)) {
                this.f = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
                this.k = true;
            }
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new si(this);
        }

        @DexIgnore
        public Paint a(ColorFilter colorFilter) {
            if (!b() && colorFilter == null) {
                return null;
            }
            if (this.l == null) {
                this.l = new Paint();
                this.l.setFilterBitmap(true);
            }
            this.l.setAlpha(this.b.getRootAlpha());
            this.l.setColorFilter(colorFilter);
            return this.l;
        }

        @DexIgnore
        public boolean c() {
            return this.b.a();
        }

        @DexIgnore
        public boolean a(int i2, int i3) {
            return i2 == this.f.getWidth() && i3 == this.f.getHeight();
        }

        @DexIgnore
        public boolean a() {
            return !this.k && this.g == this.c && this.h == this.d && this.j == this.e && this.i == this.b.getRootAlpha();
        }

        @DexIgnore
        public h() {
            this.c = null;
            this.d = si.n;
            this.b = new g();
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            boolean a2 = this.b.a(iArr);
            this.k |= a2;
            return a2;
        }
    }

    @DexIgnore
    public si() {
        this.j = true;
        this.k = new float[9];
        this.l = new Matrix();
        this.m = new Rect();
        this.f = new h();
    }

    @DexIgnore
    public static si createFromXmlInner(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        si siVar = new si();
        siVar.inflate(resources, xmlPullParser, attributeSet, theme);
        return siVar;
    }

    @DexIgnore
    public Object a(String str) {
        return this.f.b.p.get(str);
    }

    @DexIgnore
    public boolean canApplyTheme() {
        Drawable drawable = this.e;
        if (drawable == null) {
            return false;
        }
        c7.a(drawable);
        return false;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.draw(canvas);
            return;
        }
        copyBounds(this.m);
        if (this.m.width() > 0 && this.m.height() > 0) {
            ColorFilter colorFilter = this.h;
            if (colorFilter == null) {
                colorFilter = this.g;
            }
            canvas.getMatrix(this.l);
            this.l.getValues(this.k);
            float abs = Math.abs(this.k[0]);
            float abs2 = Math.abs(this.k[4]);
            float abs3 = Math.abs(this.k[1]);
            float abs4 = Math.abs(this.k[3]);
            if (!(abs3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && abs4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                abs = 1.0f;
                abs2 = 1.0f;
            }
            int min = Math.min(2048, (int) (((float) this.m.width()) * abs));
            int min2 = Math.min(2048, (int) (((float) this.m.height()) * abs2));
            if (min > 0 && min2 > 0) {
                int save = canvas.save();
                Rect rect = this.m;
                canvas.translate((float) rect.left, (float) rect.top);
                if (a()) {
                    canvas.translate((float) this.m.width(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    canvas.scale(-1.0f, 1.0f);
                }
                this.m.offsetTo(0, 0);
                this.f.b(min, min2);
                if (!this.j) {
                    this.f.c(min, min2);
                } else if (!this.f.a()) {
                    this.f.c(min, min2);
                    this.f.d();
                }
                this.f.a(canvas, colorFilter, this.m);
                canvas.restoreToCount(save);
            }
        }
    }

    @DexIgnore
    public int getAlpha() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return c7.c(drawable);
        }
        return this.f.b.getRootAlpha();
    }

    @DexIgnore
    public int getChangingConfigurations() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.f.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        Drawable drawable = this.e;
        if (drawable != null && Build.VERSION.SDK_INT >= 24) {
            return new i(drawable.getConstantState());
        }
        this.f.a = getChangingConfigurations();
        return this.f;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getIntrinsicHeight();
        }
        return (int) this.f.b.j;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getIntrinsicWidth();
        }
        return (int) this.f.b.i;
    }

    @DexIgnore
    public int getOpacity() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.getOpacity();
        }
        return -3;
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.inflate(resources, xmlPullParser, attributeSet);
        } else {
            inflate(resources, xmlPullParser, attributeSet, (Resources.Theme) null);
        }
    }

    @DexIgnore
    public void invalidateSelf() {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.invalidateSelf();
        } else {
            super.invalidateSelf();
        }
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return c7.f(drawable);
        }
        return this.f.e;
    }

    @DexIgnore
    public boolean isStateful() {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.isStateful();
        }
        if (!super.isStateful()) {
            h hVar = this.f;
            if (hVar != null) {
                if (!hVar.c()) {
                    ColorStateList colorStateList = this.f.c;
                    if (colorStateList == null || !colorStateList.isStateful()) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public Drawable mutate() {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.mutate();
            return this;
        }
        if (!this.i && super.mutate() == this) {
            this.f = new h(this.f);
            this.i = true;
        }
        return this;
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setBounds(rect);
        }
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setState(iArr);
        }
        boolean z = false;
        h hVar = this.f;
        ColorStateList colorStateList = hVar.c;
        if (colorStateList != null) {
            PorterDuff.Mode mode = hVar.d;
            if (mode != null) {
                this.g = a(this.g, colorStateList, mode);
                invalidateSelf();
                z = true;
            }
        }
        if (!hVar.c() || !hVar.a(iArr)) {
            return z;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void scheduleSelf(Runnable runnable, long j2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.scheduleSelf(runnable, j2);
        } else {
            super.scheduleSelf(runnable, j2);
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setAlpha(i2);
        } else if (this.f.b.getRootAlpha() != i2) {
            this.f.b.setRootAlpha(i2);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, z);
        } else {
            this.f.e = z;
        }
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.setColorFilter(colorFilter);
            return;
        }
        this.h = colorFilter;
        invalidateSelf();
    }

    @DexIgnore
    public void setTint(int i2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.b(drawable, i2);
        } else {
            setTintList(ColorStateList.valueOf(i2));
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, colorStateList);
            return;
        }
        h hVar = this.f;
        if (hVar.c != colorStateList) {
            hVar.c = colorStateList;
            this.g = a(this.g, colorStateList, hVar.d);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, mode);
            return;
        }
        h hVar = this.f;
        if (hVar.d != mode) {
            hVar.d = mode;
            this.g = a(this.g, hVar.c, mode);
            invalidateSelf();
        }
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Drawable drawable = this.e;
        if (drawable != null) {
            return drawable.setVisible(z, z2);
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleSelf(Runnable runnable) {
        Drawable drawable = this.e;
        if (drawable != null) {
            drawable.unscheduleSelf(runnable);
        } else {
            super.unscheduleSelf(runnable);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f extends e {
        @DexIgnore
        public u6.b[] a; // = null;
        @DexIgnore
        public String b;
        @DexIgnore
        public int c;

        @DexIgnore
        public f() {
            super();
        }

        @DexIgnore
        public void a(Path path) {
            path.reset();
            u6.b[] bVarArr = this.a;
            if (bVarArr != null) {
                u6.b.a(bVarArr, path);
            }
        }

        @DexIgnore
        public boolean b() {
            return false;
        }

        @DexIgnore
        public u6.b[] getPathData() {
            return this.a;
        }

        @DexIgnore
        public String getPathName() {
            return this.b;
        }

        @DexIgnore
        public void setPathData(u6.b[] bVarArr) {
            if (!u6.a(this.a, bVarArr)) {
                this.a = u6.a(bVarArr);
            } else {
                u6.b(this.a, bVarArr);
            }
        }

        @DexIgnore
        public f(f fVar) {
            super();
            this.b = fVar.b;
            this.c = fVar.c;
            this.a = u6.a(fVar.a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ Drawable.ConstantState a;

        @DexIgnore
        public i(Drawable.ConstantState constantState) {
            this.a = constantState;
        }

        @DexIgnore
        public boolean canApplyTheme() {
            return this.a.canApplyTheme();
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return this.a.getChangingConfigurations();
        }

        @DexIgnore
        public Drawable newDrawable() {
            si siVar = new si();
            siVar.e = (VectorDrawable) this.a.newDrawable();
            return siVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            si siVar = new si();
            siVar.e = (VectorDrawable) this.a.newDrawable(resources);
            return siVar;
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            si siVar = new si();
            siVar.e = (VectorDrawable) this.a.newDrawable(resources, theme);
            return siVar;
        }
    }

    @DexIgnore
    public PorterDuffColorFilter a(PorterDuffColorFilter porterDuffColorFilter, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends f {
        @DexIgnore
        public int[] d;
        @DexIgnore
        public n6 e;
        @DexIgnore
        public float f; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public n6 g;
        @DexIgnore
        public float h; // = 1.0f;
        @DexIgnore
        public int i; // = 0;
        @DexIgnore
        public float j; // = 1.0f;
        @DexIgnore
        public float k; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float l; // = 1.0f;
        @DexIgnore
        public float m; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public Paint.Cap n; // = Paint.Cap.BUTT;
        @DexIgnore
        public Paint.Join o; // = Paint.Join.MITER;
        @DexIgnore
        public float p; // = 4.0f;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final Paint.Cap a(int i2, Paint.Cap cap) {
            if (i2 == 0) {
                return Paint.Cap.BUTT;
            }
            if (i2 != 1) {
                return i2 != 2 ? cap : Paint.Cap.SQUARE;
            }
            return Paint.Cap.ROUND;
        }

        @DexIgnore
        public float getFillAlpha() {
            return this.j;
        }

        @DexIgnore
        public int getFillColor() {
            return this.g.a();
        }

        @DexIgnore
        public float getStrokeAlpha() {
            return this.h;
        }

        @DexIgnore
        public int getStrokeColor() {
            return this.e.a();
        }

        @DexIgnore
        public float getStrokeWidth() {
            return this.f;
        }

        @DexIgnore
        public float getTrimPathEnd() {
            return this.l;
        }

        @DexIgnore
        public float getTrimPathOffset() {
            return this.m;
        }

        @DexIgnore
        public float getTrimPathStart() {
            return this.k;
        }

        @DexIgnore
        public void setFillAlpha(float f2) {
            this.j = f2;
        }

        @DexIgnore
        public void setFillColor(int i2) {
            this.g.a(i2);
        }

        @DexIgnore
        public void setStrokeAlpha(float f2) {
            this.h = f2;
        }

        @DexIgnore
        public void setStrokeColor(int i2) {
            this.e.a(i2);
        }

        @DexIgnore
        public void setStrokeWidth(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public void setTrimPathEnd(float f2) {
            this.l = f2;
        }

        @DexIgnore
        public void setTrimPathOffset(float f2) {
            this.m = f2;
        }

        @DexIgnore
        public void setTrimPathStart(float f2) {
            this.k = f2;
        }

        @DexIgnore
        public final Paint.Join a(int i2, Paint.Join join) {
            if (i2 == 0) {
                return Paint.Join.MITER;
            }
            if (i2 != 1) {
                return i2 != 2 ? join : Paint.Join.BEVEL;
            }
            return Paint.Join.ROUND;
        }

        @DexIgnore
        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a = s6.a(resources, theme, attributeSet, ki.c);
            a(a, xmlPullParser, theme);
            a.recycle();
        }

        @DexIgnore
        public final void a(TypedArray typedArray, XmlPullParser xmlPullParser, Resources.Theme theme) {
            this.d = null;
            if (s6.a(xmlPullParser, "pathData")) {
                String string = typedArray.getString(0);
                if (string != null) {
                    this.b = string;
                }
                String string2 = typedArray.getString(2);
                if (string2 != null) {
                    this.a = u6.a(string2);
                }
                Resources.Theme theme2 = theme;
                this.g = s6.a(typedArray, xmlPullParser, theme2, "fillColor", 1, 0);
                this.j = s6.a(typedArray, xmlPullParser, "fillAlpha", 12, this.j);
                this.n = a(s6.b(typedArray, xmlPullParser, "strokeLineCap", 8, -1), this.n);
                this.o = a(s6.b(typedArray, xmlPullParser, "strokeLineJoin", 9, -1), this.o);
                this.p = s6.a(typedArray, xmlPullParser, "strokeMiterLimit", 10, this.p);
                this.e = s6.a(typedArray, xmlPullParser, theme2, "strokeColor", 3, 0);
                this.h = s6.a(typedArray, xmlPullParser, "strokeAlpha", 11, this.h);
                this.f = s6.a(typedArray, xmlPullParser, "strokeWidth", 4, this.f);
                this.l = s6.a(typedArray, xmlPullParser, "trimPathEnd", 6, this.l);
                this.m = s6.a(typedArray, xmlPullParser, "trimPathOffset", 7, this.m);
                this.k = s6.a(typedArray, xmlPullParser, "trimPathStart", 5, this.k);
                this.i = s6.b(typedArray, xmlPullParser, "fillType", 13, this.i);
            }
        }

        @DexIgnore
        public c(c cVar) {
            super(cVar);
            this.d = cVar.d;
            this.e = cVar.e;
            this.f = cVar.f;
            this.h = cVar.h;
            this.g = cVar.g;
            this.i = cVar.i;
            this.j = cVar.j;
            this.k = cVar.k;
            this.l = cVar.l;
            this.m = cVar.m;
            this.n = cVar.n;
            this.o = cVar.o;
            this.p = cVar.p;
        }

        @DexIgnore
        public boolean a() {
            return this.g.d() || this.e.d();
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            return this.e.a(iArr) | this.g.a(iArr);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends e {
        @DexIgnore
        public /* final */ Matrix a; // = new Matrix();
        @DexIgnore
        public /* final */ ArrayList<e> b; // = new ArrayList<>();
        @DexIgnore
        public float c; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float d; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float e; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float f; // = 1.0f;
        @DexIgnore
        public float g; // = 1.0f;
        @DexIgnore
        public float h; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float i; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public /* final */ Matrix j; // = new Matrix();
        @DexIgnore
        public int k;
        @DexIgnore
        public int[] l;
        @DexIgnore
        public String m; // = null;

        @DexIgnore
        public d(d dVar, g4<String, Object> g4Var) {
            super();
            f fVar;
            this.c = dVar.c;
            this.d = dVar.d;
            this.e = dVar.e;
            this.f = dVar.f;
            this.g = dVar.g;
            this.h = dVar.h;
            this.i = dVar.i;
            this.l = dVar.l;
            this.m = dVar.m;
            this.k = dVar.k;
            String str = this.m;
            if (str != null) {
                g4Var.put(str, this);
            }
            this.j.set(dVar.j);
            ArrayList<e> arrayList = dVar.b;
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                e eVar = arrayList.get(i2);
                if (eVar instanceof d) {
                    this.b.add(new d((d) eVar, g4Var));
                } else {
                    if (eVar instanceof c) {
                        fVar = new c((c) eVar);
                    } else if (eVar instanceof b) {
                        fVar = new b((b) eVar);
                    } else {
                        throw new IllegalStateException("Unknown object in the tree!");
                    }
                    this.b.add(fVar);
                    String str2 = fVar.b;
                    if (str2 != null) {
                        g4Var.put(str2, fVar);
                    }
                }
            }
        }

        @DexIgnore
        public void a(Resources resources, AttributeSet attributeSet, Resources.Theme theme, XmlPullParser xmlPullParser) {
            TypedArray a2 = s6.a(resources, theme, attributeSet, ki.b);
            a(a2, xmlPullParser);
            a2.recycle();
        }

        @DexIgnore
        public final void b() {
            this.j.reset();
            this.j.postTranslate(-this.d, -this.e);
            this.j.postScale(this.f, this.g);
            this.j.postRotate(this.c, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.j.postTranslate(this.h + this.d, this.i + this.e);
        }

        @DexIgnore
        public String getGroupName() {
            return this.m;
        }

        @DexIgnore
        public Matrix getLocalMatrix() {
            return this.j;
        }

        @DexIgnore
        public float getPivotX() {
            return this.d;
        }

        @DexIgnore
        public float getPivotY() {
            return this.e;
        }

        @DexIgnore
        public float getRotation() {
            return this.c;
        }

        @DexIgnore
        public float getScaleX() {
            return this.f;
        }

        @DexIgnore
        public float getScaleY() {
            return this.g;
        }

        @DexIgnore
        public float getTranslateX() {
            return this.h;
        }

        @DexIgnore
        public float getTranslateY() {
            return this.i;
        }

        @DexIgnore
        public void setPivotX(float f2) {
            if (f2 != this.d) {
                this.d = f2;
                b();
            }
        }

        @DexIgnore
        public void setPivotY(float f2) {
            if (f2 != this.e) {
                this.e = f2;
                b();
            }
        }

        @DexIgnore
        public void setRotation(float f2) {
            if (f2 != this.c) {
                this.c = f2;
                b();
            }
        }

        @DexIgnore
        public void setScaleX(float f2) {
            if (f2 != this.f) {
                this.f = f2;
                b();
            }
        }

        @DexIgnore
        public void setScaleY(float f2) {
            if (f2 != this.g) {
                this.g = f2;
                b();
            }
        }

        @DexIgnore
        public void setTranslateX(float f2) {
            if (f2 != this.h) {
                this.h = f2;
                b();
            }
        }

        @DexIgnore
        public void setTranslateY(float f2) {
            if (f2 != this.i) {
                this.i = f2;
                b();
            }
        }

        @DexIgnore
        public final void a(TypedArray typedArray, XmlPullParser xmlPullParser) {
            this.l = null;
            this.c = s6.a(typedArray, xmlPullParser, "rotation", 5, this.c);
            this.d = typedArray.getFloat(1, this.d);
            this.e = typedArray.getFloat(2, this.e);
            this.f = s6.a(typedArray, xmlPullParser, "scaleX", 3, this.f);
            this.g = s6.a(typedArray, xmlPullParser, "scaleY", 4, this.g);
            this.h = s6.a(typedArray, xmlPullParser, "translateX", 6, this.h);
            this.i = s6.a(typedArray, xmlPullParser, "translateY", 7, this.i);
            String string = typedArray.getString(0);
            if (string != null) {
                this.m = string;
            }
            b();
        }

        @DexIgnore
        public boolean a() {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                if (this.b.get(i2).a()) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            boolean z = false;
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                z |= this.b.get(i2).a(iArr);
            }
            return z;
        }

        @DexIgnore
        public d() {
            super();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036 A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003b A[Catch:{ XmlPullParserException -> 0x0048, IOException -> 0x0043 }] */
    public static si a(Resources resources, int i2, Resources.Theme theme) {
        int next;
        if (Build.VERSION.SDK_INT >= 24) {
            si siVar = new si();
            siVar.e = r6.a(resources, i2, theme);
            new i(siVar.e.getConstantState());
            return siVar;
        }
        try {
            XmlResourceParser xml = resources.getXml(i2);
            AttributeSet asAttributeSet = Xml.asAttributeSet(xml);
            while (true) {
                next = xml.next();
                if (next == 2 || next == 1) {
                    if (next != 2) {
                        return createFromXmlInner(resources, xml, asAttributeSet, theme);
                    }
                    throw new XmlPullParserException("No start tag found");
                }
            }
            if (next != 2) {
            }
        } catch (XmlPullParserException e2) {
            Log.e("VectorDrawableCompat", "parser error", e2);
            return null;
        } catch (IOException e3) {
            Log.e("VectorDrawableCompat", "parser error", e3);
            return null;
        }
    }

    @DexIgnore
    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        Drawable drawable = this.e;
        if (drawable != null) {
            c7.a(drawable, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        h hVar = this.f;
        hVar.b = new g();
        TypedArray a2 = s6.a(resources, theme, attributeSet, ki.a);
        a(a2, xmlPullParser);
        a2.recycle();
        hVar.a = getChangingConfigurations();
        hVar.k = true;
        a(resources, xmlPullParser, attributeSet, theme);
        this.g = a(this.g, hVar.c, hVar.d);
    }

    @DexIgnore
    public si(h hVar) {
        this.j = true;
        this.k = new float[9];
        this.l = new Matrix();
        this.m = new Rect();
        this.f = hVar;
        this.g = a(this.g, hVar.c, hVar.d);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public static /* final */ Matrix q; // = new Matrix();
        @DexIgnore
        public /* final */ Path a;
        @DexIgnore
        public /* final */ Path b;
        @DexIgnore
        public /* final */ Matrix c;
        @DexIgnore
        public Paint d;
        @DexIgnore
        public Paint e;
        @DexIgnore
        public PathMeasure f;
        @DexIgnore
        public int g;
        @DexIgnore
        public /* final */ d h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public float k;
        @DexIgnore
        public float l;
        @DexIgnore
        public int m;
        @DexIgnore
        public String n;
        @DexIgnore
        public Boolean o;
        @DexIgnore
        public /* final */ g4<String, Object> p;

        @DexIgnore
        public g() {
            this.c = new Matrix();
            this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new g4<>();
            this.h = new d();
            this.a = new Path();
            this.b = new Path();
        }

        @DexIgnore
        public static float a(float f2, float f3, float f4, float f5) {
            return (f2 * f5) - (f3 * f4);
        }

        @DexIgnore
        public final void a(d dVar, Matrix matrix, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            dVar.a.set(matrix);
            dVar.a.preConcat(dVar.j);
            canvas.save();
            for (int i4 = 0; i4 < dVar.b.size(); i4++) {
                e eVar = dVar.b.get(i4);
                if (eVar instanceof d) {
                    a((d) eVar, dVar.a, canvas, i2, i3, colorFilter);
                } else if (eVar instanceof f) {
                    a(dVar, (f) eVar, canvas, i2, i3, colorFilter);
                }
            }
            canvas.restore();
        }

        @DexIgnore
        public float getAlpha() {
            return ((float) getRootAlpha()) / 255.0f;
        }

        @DexIgnore
        public int getRootAlpha() {
            return this.m;
        }

        @DexIgnore
        public void setAlpha(float f2) {
            setRootAlpha((int) (f2 * 255.0f));
        }

        @DexIgnore
        public void setRootAlpha(int i2) {
            this.m = i2;
        }

        @DexIgnore
        public void a(Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            a(this.h, q, canvas, i2, i3, colorFilter);
        }

        @DexIgnore
        public g(g gVar) {
            this.c = new Matrix();
            this.i = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.j = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = 255;
            this.n = null;
            this.o = null;
            this.p = new g4<>();
            this.h = new d(gVar.h, this.p);
            this.a = new Path(gVar.a);
            this.b = new Path(gVar.b);
            this.i = gVar.i;
            this.j = gVar.j;
            this.k = gVar.k;
            this.l = gVar.l;
            this.g = gVar.g;
            this.m = gVar.m;
            this.n = gVar.n;
            String str = gVar.n;
            if (str != null) {
                this.p.put(str, this);
            }
            this.o = gVar.o;
        }

        @DexIgnore
        public final void a(d dVar, f fVar, Canvas canvas, int i2, int i3, ColorFilter colorFilter) {
            float f2 = ((float) i2) / this.k;
            float f3 = ((float) i3) / this.l;
            float min = Math.min(f2, f3);
            Matrix matrix = dVar.a;
            this.c.set(matrix);
            this.c.postScale(f2, f3);
            float a2 = a(matrix);
            if (a2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                fVar.a(this.a);
                Path path = this.a;
                this.b.reset();
                if (fVar.b()) {
                    this.b.addPath(path, this.c);
                    canvas.clipPath(this.b);
                    return;
                }
                c cVar = (c) fVar;
                if (!(cVar.k == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && cVar.l == 1.0f)) {
                    float f4 = cVar.k;
                    float f5 = cVar.m;
                    float f6 = (f4 + f5) % 1.0f;
                    float f7 = (cVar.l + f5) % 1.0f;
                    if (this.f == null) {
                        this.f = new PathMeasure();
                    }
                    this.f.setPath(this.a, false);
                    float length = this.f.getLength();
                    float f8 = f6 * length;
                    float f9 = f7 * length;
                    path.reset();
                    if (f8 > f9) {
                        this.f.getSegment(f8, length, path, true);
                        this.f.getSegment(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9, path, true);
                    } else {
                        this.f.getSegment(f8, f9, path, true);
                    }
                    path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                }
                this.b.addPath(path, this.c);
                if (cVar.g.e()) {
                    n6 n6Var = cVar.g;
                    if (this.e == null) {
                        this.e = new Paint(1);
                        this.e.setStyle(Paint.Style.FILL);
                    }
                    Paint paint = this.e;
                    if (n6Var.c()) {
                        Shader b2 = n6Var.b();
                        b2.setLocalMatrix(this.c);
                        paint.setShader(b2);
                        paint.setAlpha(Math.round(cVar.j * 255.0f));
                    } else {
                        paint.setShader((Shader) null);
                        paint.setAlpha(255);
                        paint.setColor(si.a(n6Var.a(), cVar.j));
                    }
                    paint.setColorFilter(colorFilter);
                    this.b.setFillType(cVar.i == 0 ? Path.FillType.WINDING : Path.FillType.EVEN_ODD);
                    canvas.drawPath(this.b, paint);
                }
                if (cVar.e.e()) {
                    n6 n6Var2 = cVar.e;
                    if (this.d == null) {
                        this.d = new Paint(1);
                        this.d.setStyle(Paint.Style.STROKE);
                    }
                    Paint paint2 = this.d;
                    Paint.Join join = cVar.o;
                    if (join != null) {
                        paint2.setStrokeJoin(join);
                    }
                    Paint.Cap cap = cVar.n;
                    if (cap != null) {
                        paint2.setStrokeCap(cap);
                    }
                    paint2.setStrokeMiter(cVar.p);
                    if (n6Var2.c()) {
                        Shader b3 = n6Var2.b();
                        b3.setLocalMatrix(this.c);
                        paint2.setShader(b3);
                        paint2.setAlpha(Math.round(cVar.h * 255.0f));
                    } else {
                        paint2.setShader((Shader) null);
                        paint2.setAlpha(255);
                        paint2.setColor(si.a(n6Var2.a(), cVar.h));
                    }
                    paint2.setColorFilter(colorFilter);
                    paint2.setStrokeWidth(cVar.f * min * a2);
                    canvas.drawPath(this.b, paint2);
                }
            }
        }

        @DexIgnore
        public final float a(Matrix matrix) {
            float[] fArr = {LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f, 1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES};
            matrix.mapVectors(fArr);
            float a2 = a(fArr[0], fArr[1], fArr[2], fArr[3]);
            float max = Math.max((float) Math.hypot((double) fArr[0], (double) fArr[1]), (float) Math.hypot((double) fArr[2], (double) fArr[3]));
            if (max > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return Math.abs(a2) / max;
            }
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }

        @DexIgnore
        public boolean a() {
            if (this.o == null) {
                this.o = Boolean.valueOf(this.h.a());
            }
            return this.o.booleanValue();
        }

        @DexIgnore
        public boolean a(int[] iArr) {
            return this.h.a(iArr);
        }
    }

    @DexIgnore
    public static int a(int i2, float f2) {
        return (i2 & 16777215) | (((int) (((float) Color.alpha(i2)) * f2)) << 24);
    }

    @DexIgnore
    public static PorterDuff.Mode a(int i2, PorterDuff.Mode mode) {
        if (i2 == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i2 == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i2 == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i2) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    public final void a(TypedArray typedArray, XmlPullParser xmlPullParser) throws XmlPullParserException {
        h hVar = this.f;
        g gVar = hVar.b;
        hVar.d = a(s6.b(typedArray, xmlPullParser, "tintMode", 6, -1), PorterDuff.Mode.SRC_IN);
        ColorStateList colorStateList = typedArray.getColorStateList(1);
        if (colorStateList != null) {
            hVar.c = colorStateList;
        }
        hVar.e = s6.a(typedArray, xmlPullParser, "autoMirrored", 5, hVar.e);
        gVar.k = s6.a(typedArray, xmlPullParser, "viewportWidth", 7, gVar.k);
        gVar.l = s6.a(typedArray, xmlPullParser, "viewportHeight", 8, gVar.l);
        if (gVar.k <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportWidth > 0");
        } else if (gVar.l > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            gVar.i = typedArray.getDimension(3, gVar.i);
            gVar.j = typedArray.getDimension(2, gVar.j);
            if (gVar.i <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires width > 0");
            } else if (gVar.j > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                gVar.setAlpha(s6.a(typedArray, xmlPullParser, "alpha", 4, gVar.getAlpha()));
                String string = typedArray.getString(0);
                if (string != null) {
                    gVar.n = string;
                    gVar.p.put(string, gVar);
                }
            } else {
                throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires height > 0");
            }
        } else {
            throw new XmlPullParserException(typedArray.getPositionDescription() + "<vector> tag requires viewportHeight > 0");
        }
    }

    @DexIgnore
    public final void a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        h hVar = this.f;
        g gVar = hVar.b;
        ArrayDeque arrayDeque = new ArrayDeque();
        arrayDeque.push(gVar.h);
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        boolean z = true;
        while (eventType != 1 && (xmlPullParser.getDepth() >= depth || eventType != 3)) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                d dVar = (d) arrayDeque.peek();
                if ("path".equals(name)) {
                    c cVar = new c();
                    cVar.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(cVar);
                    if (cVar.getPathName() != null) {
                        gVar.p.put(cVar.getPathName(), cVar);
                    }
                    z = false;
                    hVar.a = cVar.c | hVar.a;
                } else if ("clip-path".equals(name)) {
                    b bVar = new b();
                    bVar.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(bVar);
                    if (bVar.getPathName() != null) {
                        gVar.p.put(bVar.getPathName(), bVar);
                    }
                    hVar.a = bVar.c | hVar.a;
                } else if ("group".equals(name)) {
                    d dVar2 = new d();
                    dVar2.a(resources, attributeSet, theme, xmlPullParser);
                    dVar.b.add(dVar2);
                    arrayDeque.push(dVar2);
                    if (dVar2.getGroupName() != null) {
                        gVar.p.put(dVar2.getGroupName(), dVar2);
                    }
                    hVar.a = dVar2.k | hVar.a;
                }
            } else if (eventType == 3 && "group".equals(xmlPullParser.getName())) {
                arrayDeque.pop();
            }
            eventType = xmlPullParser.next();
        }
        if (z) {
            throw new XmlPullParserException("no path defined");
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.j = z;
    }

    @DexIgnore
    public final boolean a() {
        if (Build.VERSION.SDK_INT < 17 || !isAutoMirrored() || c7.e(this) != 1) {
            return false;
        }
        return true;
    }
}
