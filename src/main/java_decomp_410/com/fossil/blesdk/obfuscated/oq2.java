package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class oq2 extends lq2 {
    @DexIgnore
    public String d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oq2(String str, String str2, String str3) {
        super(str, str2);
        kd4.b(str, "tagName");
        kd4.b(str2, "title");
        kd4.b(str3, "text");
        this.d = str3;
    }

    @DexIgnore
    public final void b(String str) {
        kd4.b(str, "<set-?>");
        this.d = str;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }
}
