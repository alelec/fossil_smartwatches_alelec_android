package com.fossil.blesdk.obfuscated;

import kotlin.Result;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class na4 {
    @DexIgnore
    public static final Object a(Throwable th) {
        kd4.b(th, "exception");
        return new Result.Failure(th);
    }

    @DexIgnore
    public static final void a(Object obj) {
        if (obj instanceof Result.Failure) {
            throw ((Result.Failure) obj).exception;
        }
    }
}
