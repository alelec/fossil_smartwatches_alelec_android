package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface yj0 extends IInterface {
    @DexIgnore
    sn0 a(sn0 sn0, ek0 ek0) throws RemoteException;
}
