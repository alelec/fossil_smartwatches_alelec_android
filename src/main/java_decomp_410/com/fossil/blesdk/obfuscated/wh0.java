package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ve0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wh0<ResultT> extends fh0 {
    @DexIgnore
    public /* final */ ff0<de0.b, ResultT> a;
    @DexIgnore
    public /* final */ xn1<ResultT> b;
    @DexIgnore
    public /* final */ df0 c;

    @DexIgnore
    public wh0(int i, ff0<de0.b, ResultT> ff0, xn1<ResultT> xn1, df0 df0) {
        super(i);
        this.b = xn1;
        this.a = ff0;
        this.c = df0;
    }

    @DexIgnore
    public final void a(ve0.a<?> aVar) throws DeadObjectException {
        try {
            this.a.a(aVar.f(), this.b);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            a(ig0.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    @DexIgnore
    public final wd0[] b(ve0.a<?> aVar) {
        return this.a.b();
    }

    @DexIgnore
    public final boolean c(ve0.a<?> aVar) {
        return this.a.a();
    }

    @DexIgnore
    public final void a(Status status) {
        this.b.b(this.c.a(status));
    }

    @DexIgnore
    public final void a(RuntimeException runtimeException) {
        this.b.b((Exception) runtimeException);
    }

    @DexIgnore
    public final void a(jf0 jf0, boolean z) {
        jf0.a(this.b, z);
    }
}
