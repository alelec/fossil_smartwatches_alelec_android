package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.request.code.AuthenticationOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l70 extends j70 {
    @DexIgnore
    public /* final */ AuthenticationKeyType K;
    @DexIgnore
    public /* final */ byte[] L;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l70(Peripheral peripheral, AuthenticationKeyType authenticationKeyType, byte[] bArr) {
        super(peripheral, AuthenticationOperationCode.SEND_BOTH_SIDES_RANDOM_NUMBERS, RequestId.SEND_BOTH_SIDES_RANDOM_NUMBERS, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(authenticationKeyType, "keyType");
        kd4.b(bArr, "bothSidesRandomNumbers");
        this.K = authenticationKeyType;
        this.L = bArr;
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(this.L.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.K.getId$blesdk_productionRelease()).put(this.L).array();
        kd4.a((Object) array, "ByteBuffer.allocate(KEY_\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(super.t(), JSONKey.AUTHENTICATION_KEY_TYPE, this.K.getLogName$blesdk_productionRelease()), JSONKey.BOTH_SIDES_RANDOM_NUMBERS, k90.a(this.L, (String) null, 1, (Object) null));
    }
}
