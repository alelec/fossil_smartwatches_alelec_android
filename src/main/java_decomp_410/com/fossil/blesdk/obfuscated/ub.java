package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ub {
    @DexIgnore
    public static AtomicBoolean a; // = new AtomicBoolean(false);

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends qb {
        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            fc.b(activity);
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static void a(Context context) {
        if (!a.getAndSet(true)) {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new a());
        }
    }
}
