package com.fossil.blesdk.obfuscated;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.Registrar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class tx1 implements kw1 {
    @DexIgnore
    public static /* final */ kw1 a; // = new tx1();

    @DexIgnore
    public final Object a(jw1 jw1) {
        return new Registrar.a((FirebaseInstanceId) jw1.a(FirebaseInstanceId.class));
    }
}
