package com.fossil.blesdk.obfuscated;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nv2 implements Factory<gg3> {
    @DexIgnore
    public static gg3 a(gv2 gv2) {
        gg3 g = gv2.g();
        n44.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }
}
