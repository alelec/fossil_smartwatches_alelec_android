package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mq0 implements Parcelable.Creator<kq0> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        zo0 zo0 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                zo0 = SafeParcelReader.a(parcel, a, zo0.CREATOR);
            } else if (a2 == 2) {
                iBinder = SafeParcelReader.p(parcel, a);
            } else if (a2 == 3) {
                j = SafeParcelReader.s(parcel, a);
            } else if (a2 != 4) {
                SafeParcelReader.v(parcel, a);
            } else {
                j2 = SafeParcelReader.s(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new kq0(zo0, iBinder, j, j2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new kq0[i];
    }
}
