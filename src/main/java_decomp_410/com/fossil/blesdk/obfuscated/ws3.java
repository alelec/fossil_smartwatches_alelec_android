package com.fossil.blesdk.obfuscated;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.ht3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.NumberPickerLarge;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
@SuppressLint({"UseSparseArrays"})
public class ws3 extends xa implements DialogInterface.OnKeyListener {
    @DexIgnore
    public g A;
    @DexIgnore
    public h B;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i; // = true;
    @DexIgnore
    public ArrayList<Integer> j;
    @DexIgnore
    public HashMap<Integer, Integer> k;
    @DexIgnore
    public HashMap<Integer, Bitmap> l;
    @DexIgnore
    public HashMap<Integer, String> m;
    @DexIgnore
    public HashMap<Integer, String> n;
    @DexIgnore
    public DashbarData o;
    @DexIgnore
    public HashMap<Integer, SpannableString> p;
    @DexIgnore
    public ArrayList<Integer> q;
    @DexIgnore
    public ArrayList<Integer> r;
    @DexIgnore
    public ArrayList<Integer> s;
    @DexIgnore
    public ArrayList<Integer> t;
    @DexIgnore
    public HashMap<Integer, List<Serializable>> u;
    @DexIgnore
    public ArrayList<f8<Integer, Integer>> v;
    @DexIgnore
    public HashMap<Integer, Boolean> w;
    @DexIgnore
    public HashMap<Integer, Integer> x;
    @DexIgnore
    public HashMap<Integer, Integer> y;
    @DexIgnore
    public Intent z;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            rn.a((Fragment) ws3.this).a(str2).a(new rv().a(pp.c)).a((ImageView) this.a);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public b(View view, int i) {
            this.e = view;
            this.f = i;
        }

        @DexIgnore
        public void onClick(View view) {
            ws3.this.dismiss();
            ws3 ws3 = ws3.this;
            HashMap<Integer, Boolean> hashMap = ws3.w;
            if (hashMap != null) {
                ws3.z.putExtra("EXTRA_SWITCH_RESULTS", hashMap);
            }
            ws3 ws32 = ws3.this;
            HashMap<Integer, Integer> hashMap2 = ws32.x;
            if (hashMap2 != null) {
                ws32.z.putExtra("EXTRA_NUMBER_PICKER_RESULTS", hashMap2);
            }
            ws3 ws33 = ws3.this;
            HashMap<Integer, Integer> hashMap3 = ws33.y;
            if (hashMap3 != null) {
                ws33.z.putExtra("EXTRA_RADIO_GROUPS_RESULTS", hashMap3);
            }
            if (ws3.this.t != null) {
                HashMap hashMap4 = new HashMap();
                Iterator<Integer> it = ws3.this.t.iterator();
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    hashMap4.put(Integer.valueOf(intValue), ((FlexibleEditText) this.e.findViewById(intValue)).getText().toString());
                }
                ws3.this.z.putExtra("EXTRA_EDIT_TEXT_RESULTS", hashMap4);
            }
            ws3 ws34 = ws3.this;
            g gVar = ws34.A;
            if (gVar != null) {
                gVar.a(ws34.e, this.f, ws34.z);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public c(View view, int i) {
            this.e = view;
            this.f = i;
        }

        @DexIgnore
        public void onClick(View view) {
            ws3 ws3 = ws3.this;
            HashMap<Integer, Boolean> hashMap = ws3.w;
            if (hashMap != null) {
                ws3.z.putExtra("EXTRA_SWITCH_RESULTS", hashMap);
            }
            ws3 ws32 = ws3.this;
            HashMap<Integer, Integer> hashMap2 = ws32.x;
            if (hashMap2 != null) {
                ws32.z.putExtra("EXTRA_NUMBER_PICKER_RESULTS", hashMap2);
            }
            ws3 ws33 = ws3.this;
            HashMap<Integer, Integer> hashMap3 = ws33.y;
            if (hashMap3 != null) {
                ws33.z.putExtra("EXTRA_RADIO_GROUPS_RESULTS", hashMap3);
            }
            if (ws3.this.t != null) {
                HashMap hashMap4 = new HashMap();
                Iterator<Integer> it = ws3.this.t.iterator();
                while (it.hasNext()) {
                    int intValue = it.next().intValue();
                    hashMap4.put(Integer.valueOf(intValue), ((FlexibleEditText) this.e.findViewById(intValue)).getText().toString());
                }
                ws3.this.z.putExtra("EXTRA_EDIT_TEXT_RESULTS", hashMap4);
            }
            ws3 ws34 = ws3.this;
            g gVar = ws34.A;
            if (gVar != null) {
                gVar.a(ws34.e, this.f, ws34.z);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ws3 ws3 = ws3.this;
            if (ws3.w == null) {
                ws3.w = new HashMap<>();
            }
            ws3.this.w.put(Integer.valueOf(compoundButton.getId()), Boolean.valueOf(z));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements NumberPickerLarge.g {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void a(NumberPickerLarge numberPickerLarge, int i, int i2) {
            ws3 ws3 = ws3.this;
            if (ws3.x == null) {
                ws3.x = new HashMap<>();
            }
            ws3.this.x.put(Integer.valueOf(numberPickerLarge.getId()), Integer.valueOf(i2));
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d; // = true;
        @DexIgnore
        public /* final */ ArrayList<Integer> e; // = new ArrayList<>();
        @DexIgnore
        public /* final */ HashMap<Integer, Integer> f; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, Bitmap> g; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, String> h; // = new HashMap<>();
        @DexIgnore
        public /* final */ HashMap<Integer, String> i; // = new HashMap<>();
        @DexIgnore
        public DashbarData j;
        @DexIgnore
        public /* final */ HashMap<Integer, SpannableString> k; // = new HashMap<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> l; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> m; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> n; // = new ArrayList<>();
        @DexIgnore
        public /* final */ ArrayList<Integer> o; // = new ArrayList<>();
        @DexIgnore
        public /* final */ HashMap<Integer, List<Serializable>> p; // = new HashMap<>();
        @DexIgnore
        public /* final */ ArrayList<f8<Integer, Integer>> q; // = new ArrayList<>();

        @DexIgnore
        public f(int i2) {
            this.a = i2;
        }

        @DexIgnore
        public f a(boolean z) {
            this.d = z;
            return this;
        }

        @DexIgnore
        public f b(int i2) {
            this.b = i2;
            return this;
        }

        @DexIgnore
        public f a(int i2, String str) {
            this.i.put(Integer.valueOf(i2), str);
            return this;
        }

        @DexIgnore
        public f b(boolean z) {
            this.c = z;
            return this;
        }

        @DexIgnore
        public f a(DashbarData dashbarData) {
            this.j = dashbarData;
            return this;
        }

        @DexIgnore
        public f a(int i2) {
            this.n.add(Integer.valueOf(i2));
            return this;
        }

        @DexIgnore
        public f a(int i2, int i3, int i4, int i5) {
            a(i2, i3, i4, i5, (NumberPickerLarge.Formatter) null, (String[]) null);
            return this;
        }

        @DexIgnore
        public f a(int i2, int i3, int i4, int i5, NumberPickerLarge.Formatter formatter, String[] strArr) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(Integer.valueOf(i3));
            arrayList.add(Integer.valueOf(i4));
            arrayList.add(Integer.valueOf(i5));
            if (formatter != null) {
                arrayList.add(formatter);
            }
            if (strArr != null) {
                arrayList.add(strArr);
            }
            this.p.put(Integer.valueOf(i2), arrayList);
            return this;
        }

        @DexIgnore
        public ws3 a(String str) {
            return a(str, (Bundle) null);
        }

        @DexIgnore
        public ws3 a(String str, Bundle bundle) {
            String str2 = str;
            return ws3.a(str, bundle, this.a, this.b, this.c, this.e, this.f, this.g, this.h, this.i, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.d, this.j);
        }

        @DexIgnore
        public ws3 a(FragmentManager fragmentManager, String str) {
            return a(fragmentManager, str, 1, (int) R.style.AppTheme);
        }

        @DexIgnore
        public ws3 a(FragmentManager fragmentManager, String str, int i2, int i3) {
            ws3 a2 = a(str);
            a2.setStyle(i2, i3);
            a2.show(fragmentManager, str);
            return a2;
        }

        @DexIgnore
        public ws3 a(FragmentManager fragmentManager, String str, Bundle bundle) {
            return a(fragmentManager, str, bundle, 1, R.style.AppTheme);
        }

        @DexIgnore
        public ws3 a(FragmentManager fragmentManager, String str, Bundle bundle, int i2, int i3) {
            ws3 a2 = a(str, bundle);
            a2.setStyle(i2, i3);
            a2.show(fragmentManager, str);
            return a2;
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(String str, int i, Intent intent);
    }

    @DexIgnore
    public interface h {
        @DexIgnore
        void Q(String str);
    }

    /*
    static {
        Class<ws3> cls = ws3.class;
    }
    */

    @DexIgnore
    public static ws3 a(String str, Bundle bundle, int i2, int i3, boolean z2, ArrayList<Integer> arrayList, HashMap<Integer, Integer> hashMap, HashMap<Integer, Bitmap> hashMap2, HashMap<Integer, String> hashMap3, HashMap<Integer, String> hashMap4, HashMap<Integer, SpannableString> hashMap5, ArrayList<Integer> arrayList2, ArrayList<Integer> arrayList3, ArrayList<Integer> arrayList4, ArrayList<Integer> arrayList5, HashMap<Integer, List<Serializable>> hashMap6, ArrayList<f8<Integer, Integer>> arrayList6, boolean z3, DashbarData dashbarData) {
        ws3 ws3 = new ws3();
        Bundle bundle2 = new Bundle();
        String str2 = str;
        bundle2.putString("ARGUMENTS_TAG", str);
        Bundle bundle3 = bundle;
        bundle2.putBundle("ARGUMENTS_BUNDLE", bundle);
        int i4 = i2;
        bundle2.putInt("ARGUMENTS_LAYOUT_ID", i2);
        int i5 = i3;
        bundle2.putInt("ARGUMENTS_STATUS_BAR_COLOR_ID", i3);
        boolean z4 = z2;
        bundle2.putBoolean("ARGUMENTS_STATUS_BAR_DARK_ICON", z2);
        ArrayList<Integer> arrayList7 = arrayList;
        bundle2.putSerializable("ARGUMENTS_STATUS_BAR_FLAGS", arrayList);
        HashMap<Integer, Integer> hashMap7 = hashMap;
        bundle2.putSerializable("ARGUMENTS_IMAGE_VIEWS", hashMap);
        HashMap<Integer, Bitmap> hashMap8 = hashMap2;
        bundle2.putSerializable("ARGUMENTS_BLUR_IMAGE_VIEWS", hashMap2);
        HashMap<Integer, String> hashMap9 = hashMap3;
        bundle2.putSerializable("ARGUMENTS_DEVICE_IMAGE_VIEWS", hashMap3);
        HashMap<Integer, String> hashMap10 = hashMap4;
        bundle2.putSerializable("ARGUMENTS_TEXT_VIEWS", hashMap4);
        HashMap<Integer, SpannableString> hashMap11 = hashMap5;
        bundle2.putSerializable("ARGUMENTS_TEXT_VIEWS_SPANNABLE", hashMap5);
        bundle2.putSerializable("ARGUMENTS_DISMISS_VIEWS", arrayList4);
        ArrayList<Integer> arrayList8 = arrayList2;
        bundle2.putSerializable("ARGUMENTS_ACTION_VIEWS", arrayList2);
        bundle2.putSerializable("ARGUMENTS_SWITCH_VIEWS", arrayList3);
        bundle2.putSerializable("ARGUMENTS_EDIT_TEXT_VIEWS", arrayList5);
        bundle2.putSerializable("ARGUMENTS_NUMBER_PICKERS", hashMap6);
        bundle2.putSerializable("ARGUMENTS_RADIO_GROUPS", arrayList6);
        bundle2.putBoolean("ARGUMENTS_ALLOW_BACK_PRESS", z3);
        bundle2.putParcelable("ARGUMENTS_DASH_BAR_DATA", dashbarData);
        ws3.setArguments(bundle2);
        return ws3;
    }

    @DexIgnore
    public boolean N0() {
        dismiss();
        return true;
    }

    @DexIgnore
    public void dismiss() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            bb a2 = fragmentManager.a();
            a2.d(this);
            a2.b();
        }
    }

    @DexIgnore
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            if (parentFragment instanceof g) {
                this.A = (g) parentFragment;
            }
            if (parentFragment instanceof h) {
                this.B = (h) parentFragment;
            }
        }
        if (this.A == null && (context instanceof g)) {
            this.A = (g) context;
        }
        if (this.B == null && (context instanceof h)) {
            this.B = (h) context;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
        this.z = new Intent();
        Bundle arguments = getArguments();
        this.e = arguments.getString("ARGUMENTS_TAG");
        Bundle bundle2 = arguments.getBundle("ARGUMENTS_BUNDLE");
        if (bundle2 != null) {
            this.z.putExtras(bundle2);
        }
        this.f = arguments.getInt("ARGUMENTS_LAYOUT_ID");
        this.g = arguments.getInt("ARGUMENTS_STATUS_BAR_COLOR_ID");
        this.h = arguments.getBoolean("ARGUMENTS_STATUS_BAR_DARK_ICON");
        this.j = (ArrayList) arguments.getSerializable("ARGUMENTS_STATUS_BAR_FLAGS");
        this.k = (HashMap) arguments.getSerializable("ARGUMENTS_IMAGE_VIEWS");
        this.l = (HashMap) arguments.getSerializable("ARGUMENTS_BLUR_IMAGE_VIEWS");
        this.m = (HashMap) arguments.getSerializable("ARGUMENTS_DEVICE_IMAGE_VIEWS");
        this.n = (HashMap) arguments.getSerializable("ARGUMENTS_TEXT_VIEWS");
        this.p = (HashMap) arguments.getSerializable("ARGUMENTS_TEXT_VIEWS_SPANNABLE");
        this.q = (ArrayList) arguments.getSerializable("ARGUMENTS_ACTION_VIEWS");
        this.r = (ArrayList) arguments.getSerializable("ARGUMENTS_DISMISS_VIEWS");
        this.s = (ArrayList) arguments.getSerializable("ARGUMENTS_SWITCH_VIEWS");
        this.t = (ArrayList) arguments.getSerializable("ARGUMENTS_EDIT_TEXT_VIEWS");
        this.u = (HashMap) arguments.getSerializable("ARGUMENTS_NUMBER_PICKERS");
        this.v = (ArrayList) arguments.getSerializable("ARGUMENTS_RADIO_GROUPS");
        this.i = arguments.getBoolean("ARGUMENTS_ALLOW_BACK_PRESS", true);
        this.o = (DashbarData) arguments.getParcelable("ARGUMENTS_DASH_BAR_DATA");
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getDialog().getWindow();
            if (window != null) {
                if (this.g != 0) {
                    window.addFlags(Integer.MIN_VALUE);
                    window.setStatusBarColor(k6.a((Context) PortfolioApp.R, this.g));
                }
                ArrayList<Integer> arrayList = this.j;
                if (arrayList != null && !arrayList.isEmpty()) {
                    Iterator<Integer> it = this.j.iterator();
                    while (it.hasNext()) {
                        window.addFlags(it.next().intValue());
                    }
                }
                if (Build.VERSION.SDK_INT >= 23 && this.h) {
                    window.getDecorView().setSystemUiVisibility(8192);
                }
            }
        }
        return layoutInflater.inflate(this.f, viewGroup);
    }

    @DexIgnore
    public void onDetach() {
        super.onDetach();
        this.A = null;
    }

    @DexIgnore
    public void onDismiss(DialogInterface dialogInterface) {
        FLogger.INSTANCE.getLocal().d("AlertDialogFragment", "onDismiss");
        h hVar = this.B;
        if (hVar != null) {
            hVar.Q(getTag());
        }
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i2, KeyEvent keyEvent) {
        if (!this.i) {
            return true;
        }
        if (keyEvent.getAction() != 1 || i2 != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d("AlertDialogFragment", "onKey KEYCODE_BACK");
        return N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        HashMap<Integer, Integer> hashMap = this.k;
        if (hashMap != null) {
            for (Map.Entry next : hashMap.entrySet()) {
                View findViewById = view.findViewById(((Integer) next.getKey()).intValue());
                if (findViewById == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("AlertDialogFragment", "Set ImageViews - view is null on mTag = " + this.e);
                } else {
                    ((ImageView) findViewById).setImageResource(((Integer) next.getValue()).intValue());
                }
            }
        }
        HashMap<Integer, Bitmap> hashMap2 = this.l;
        if (hashMap2 != null) {
            for (Map.Entry next2 : hashMap2.entrySet()) {
                View findViewById2 = view.findViewById(((Integer) next2.getKey()).intValue());
                if (findViewById2 == null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("AlertDialogFragment", "Set mBlurImageViews - view is null on mTag = " + this.e);
                } else {
                    ht3.b a2 = ht3.a(getContext());
                    a2.a(25);
                    a2.b(2);
                    a2.a((Bitmap) next2.getValue()).a((ImageView) findViewById2);
                }
            }
        }
        HashMap<Integer, String> hashMap3 = this.m;
        if (hashMap3 != null) {
            for (Map.Entry next3 : hashMap3.entrySet()) {
                View findViewById3 = view.findViewById(((Integer) next3.getKey()).intValue());
                if (findViewById3 == null) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.e("AlertDialogFragment", "Set mDeviceImageViews - view is null on mTag = " + this.e);
                } else {
                    String str = (String) next3.getValue();
                    CloudImageHelper.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.DeviceType.TYPE_LARGE).setPlaceHolder((ImageView) findViewById3, DeviceHelper.o.b(str, DeviceHelper.ImageStyle.LARGE)).setImageCallback(new a(findViewById3)).download();
                }
            }
        }
        HashMap<Integer, String> hashMap4 = this.n;
        if (hashMap4 != null) {
            for (Map.Entry next4 : hashMap4.entrySet()) {
                View findViewById4 = view.findViewById(((Integer) next4.getKey()).intValue());
                if (findViewById4 == null) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    local4.e("AlertDialogFragment", "Set TextViews - view is null on mTag = " + this.e);
                } else {
                    ((TextView) findViewById4).setText((CharSequence) next4.getValue());
                }
            }
        }
        DashbarData dashbarData = this.o;
        if (dashbarData != null) {
            DashBar dashBar = (DashBar) view.findViewById(dashbarData.getViewId());
            if (dashBar == null) {
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                local5.e("AlertDialogFragment", "Set DashBar - view is null on mTag = " + this.e);
            } else {
                dashBar.setProgress(this.o.getStartProgress());
                ObjectAnimator ofInt = ObjectAnimator.ofInt(dashBar, "progress", new int[]{this.o.getStartProgress(), this.o.getEndProgress()});
                ofInt.setDuration(500);
                ofInt.start();
            }
        }
        HashMap<Integer, SpannableString> hashMap5 = this.p;
        if (hashMap5 != null) {
            for (Map.Entry next5 : hashMap5.entrySet()) {
                View findViewById5 = view.findViewById(((Integer) next5.getKey()).intValue());
                if (findViewById5 == null) {
                    ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                    local6.e("AlertDialogFragment", "Set TextViews - view is null on mTag = " + this.e);
                } else {
                    TextView textView = (TextView) findViewById5;
                    textView.setText((CharSequence) next5.getValue());
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    textView.setHighlightColor(0);
                }
            }
        }
        ArrayList<Integer> arrayList = this.r;
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator<Integer> it = this.r.iterator();
            while (it.hasNext()) {
                int intValue = it.next().intValue();
                if (view.findViewById(intValue) == null) {
                    ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                    local7.e("AlertDialogFragment", "Set action - view is null on mTag = " + this.e);
                } else {
                    view.findViewById(intValue).setOnClickListener(new b(view, intValue));
                }
            }
        }
        ArrayList<Integer> arrayList2 = this.q;
        if (arrayList2 != null) {
            Iterator<Integer> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                int intValue2 = it2.next().intValue();
                if (view.findViewById(intValue2) == null) {
                    ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                    local8.e("AlertDialogFragment", "Set action - view is null on mTag = " + this.e);
                } else {
                    view.findViewById(intValue2).setOnClickListener(new c(view, intValue2));
                }
            }
        }
        ArrayList<Integer> arrayList3 = this.s;
        if (arrayList3 != null) {
            Iterator<Integer> it3 = arrayList3.iterator();
            while (it3.hasNext()) {
                int intValue3 = it3.next().intValue();
                View findViewById6 = view.findViewById(intValue3);
                if (findViewById6 == null) {
                    ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                    local9.e("AlertDialogFragment", "Set SwitchViews - view is null on mTag = " + this.e);
                } else {
                    if (this.w == null) {
                        this.w = new HashMap<>();
                    }
                    this.w.put(Integer.valueOf(intValue3), false);
                    ((SwitchCompat) findViewById6).setOnCheckedChangeListener(new d());
                }
            }
        }
        ArrayList<Integer> arrayList4 = this.t;
        if (arrayList4 != null) {
            Iterator<Integer> it4 = arrayList4.iterator();
            while (it4.hasNext()) {
                if (((FlexibleEditText) view.findViewById(it4.next().intValue())) == null) {
                    ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                    local10.e("AlertDialogFragment", "Set EditTextViews - view is null on mTag = " + this.e);
                }
            }
        }
        HashMap<Integer, List<Serializable>> hashMap6 = this.u;
        if (hashMap6 != null) {
            for (Map.Entry next6 : hashMap6.entrySet()) {
                View findViewById7 = view.findViewById(((Integer) next6.getKey()).intValue());
                if (findViewById7 == null) {
                    ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                    local11.e("AlertDialogFragment", "Set NumberPickers - view is null on mTag = " + this.e);
                } else {
                    NumberPickerLarge numberPickerLarge = (NumberPickerLarge) findViewById7;
                    numberPickerLarge.setOnValueChangedListener(new e());
                    List list = (List) next6.getValue();
                    int intValue4 = ((Integer) list.get(2)).intValue();
                    numberPickerLarge.setMinValue(((Integer) list.get(0)).intValue());
                    numberPickerLarge.setMaxValue(((Integer) list.get(1)).intValue());
                    numberPickerLarge.setValue(intValue4);
                    if (list.size() > 3) {
                        numberPickerLarge.setFormatter((NumberPickerLarge.Formatter) list.get(3));
                    }
                    if (list.size() > 4) {
                        numberPickerLarge.setDisplayedValues((String[]) list.get(4));
                    }
                    if (this.x == null) {
                        this.x = new HashMap<>();
                    }
                    this.x.put(next6.getKey(), Integer.valueOf(intValue4));
                }
            }
        }
        ArrayList<f8<Integer, Integer>> arrayList5 = this.v;
        if (arrayList5 != null) {
            Iterator<f8<Integer, Integer>> it5 = arrayList5.iterator();
            while (it5.hasNext()) {
                f8 next7 = it5.next();
                Integer num = (Integer) next7.a;
                Integer num2 = (Integer) next7.b;
                if (!(num == null || num2 == null)) {
                    RadioGroup radioGroup = (RadioGroup) view.findViewById(num.intValue());
                    if (radioGroup == null) {
                        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                        local12.e("AlertDialogFragment", "Set RadioGroup - radioGroup is null on mTag = " + this.e);
                    } else {
                        RadioButton radioButton = (RadioButton) view.findViewById(num2.intValue());
                        if (radioButton != null) {
                            radioButton.setChecked(true);
                        } else {
                            ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                            local13.e("AlertDialogFragment", "Set RadioGroup - defaultRadioButtonChecked is null on mTag = " + this.e + ", do not set check default");
                        }
                        if (this.y == null) {
                            this.y = new HashMap<>();
                        }
                        this.y.put(num, num2);
                        radioGroup.setOnCheckedChangeListener(new vs3(this, num));
                    }
                }
            }
        }
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i2) {
        dialog.requestWindowFeature(1);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().getDecorView().setSystemUiVisibility(3328);
            dialog.setOnKeyListener(this);
        }
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("AlertDialogFragment", "show - tag: " + str);
        bb a2 = fragmentManager.a();
        Fragment a3 = fragmentManager.a(str);
        if (a3 != null) {
            a2.d(a3);
        }
        a2.a((String) null);
        a2.a((Fragment) this, str);
        a2.b();
    }

    @DexIgnore
    public /* synthetic */ void a(Integer num, RadioGroup radioGroup, int i2) {
        if (this.y == null) {
            this.y = new HashMap<>();
        }
        this.y.put(num, Integer.valueOf(i2));
    }
}
