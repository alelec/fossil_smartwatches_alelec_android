package com.fossil.blesdk.obfuscated;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.phase.Phase;
import com.fossil.blesdk.device.logic.phase.PhaseId;
import com.fossil.blesdk.model.file.LocalizationFile;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class q50 extends g60 {
    @DexIgnore
    public /* final */ LocalizationFile Q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q50(Peripheral peripheral, Phase.a aVar, LocalizationFile localizationFile) {
        super(peripheral, aVar, PhaseId.PUT_LOCALIZATION_FILE, true, localizationFile.getFileHandle$blesdk_productionRelease(), localizationFile.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(aVar, "delegate");
        kd4.b(localizationFile, "localizationFile");
        this.Q = localizationFile;
    }

    @DexIgnore
    public final LocalizationFile O() {
        return this.Q;
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.LOCALIZATION_FILE, this.Q.toJSONObject());
    }

    @DexIgnore
    public String i() {
        return this.Q.getLocaleString();
    }
}
