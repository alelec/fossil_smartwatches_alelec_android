package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qs implements oo<Bitmap> {
    @DexIgnore
    public abstract Bitmap a(jq jqVar, Bitmap bitmap, int i, int i2);

    @DexIgnore
    public final aq<Bitmap> a(Context context, aq<Bitmap> aqVar, int i, int i2) {
        if (uw.b(i, i2)) {
            jq c = rn.a(context).c();
            Bitmap bitmap = aqVar.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap a = a(c, bitmap, i, i2);
            return bitmap.equals(a) ? aqVar : ps.a(a, c);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
