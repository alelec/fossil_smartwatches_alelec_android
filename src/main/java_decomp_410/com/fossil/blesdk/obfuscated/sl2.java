package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sl2 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public Map<String, String> c; // = new HashMap();
    @DexIgnore
    public AnalyticsHelper d;

    @DexIgnore
    public sl2(AnalyticsHelper analyticsHelper, String str) {
        kd4.b(analyticsHelper, "analyticsHelper");
        kd4.b(str, "eventName");
        this.d = analyticsHelper;
        this.a = str;
    }

    @DexIgnore
    public final sl2 a(String str, String str2) {
        kd4.b(str, "paramName");
        kd4.b(str2, "paramValue");
        Map<String, String> map = this.c;
        if (map != null) {
            map.put(str, str2);
            return this;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void a() {
        Map<String, String> map = this.c;
        if (map != null) {
            if (map.isEmpty()) {
                String str = this.b;
                if (str != null) {
                    if (str != null) {
                        AnalyticsHelper analyticsHelper = this.d;
                        if (analyticsHelper != null) {
                            String str2 = this.a;
                            if (str != null) {
                                analyticsHelper.a(str2, str);
                                return;
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        AnalyticsHelper analyticsHelper2 = this.d;
                        if (analyticsHelper2 != null) {
                            analyticsHelper2.a(this.a);
                            return;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
            }
            AnalyticsHelper analyticsHelper3 = this.d;
            if (analyticsHelper3 != null) {
                analyticsHelper3.a(this.a, (Map<String, ? extends Object>) this.c);
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }
}
