package com.fossil.blesdk.obfuscated;

import androidx.room.RoomDatabase;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wf {
    @DexIgnore
    public /* final */ RoomDatabase mDatabase;
    @DexIgnore
    public /* final */ AtomicBoolean mLock; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile kg mStmt;

    @DexIgnore
    public wf(RoomDatabase roomDatabase) {
        this.mDatabase = roomDatabase;
    }

    @DexIgnore
    private kg createNewStatement() {
        return this.mDatabase.compileStatement(createQuery());
    }

    @DexIgnore
    private kg getStmt(boolean z) {
        if (!z) {
            return createNewStatement();
        }
        if (this.mStmt == null) {
            this.mStmt = createNewStatement();
        }
        return this.mStmt;
    }

    @DexIgnore
    public kg acquire() {
        assertNotMainThread();
        return getStmt(this.mLock.compareAndSet(false, true));
    }

    @DexIgnore
    public void assertNotMainThread() {
        this.mDatabase.assertNotMainThread();
    }

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public void release(kg kgVar) {
        if (kgVar == this.mStmt) {
            this.mLock.set(false);
        }
    }
}
