package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f8 */
public class C1775f8<F, S> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ F f5093a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ S f5094b;

    @DexIgnore
    public C1775f8(F f, S s) {
        this.f5093a = f;
        this.f5094b = s;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C1775f8)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1775f8 f8Var = (com.fossil.blesdk.obfuscated.C1775f8) obj;
        if (!com.fossil.blesdk.obfuscated.C1706e8.m6326a(f8Var.f5093a, this.f5093a) || !com.fossil.blesdk.obfuscated.C1706e8.m6326a(f8Var.f5094b, this.f5094b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        F f = this.f5093a;
        int i = 0;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.f5094b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public java.lang.String toString() {
        return "Pair{" + java.lang.String.valueOf(this.f5093a) + " " + java.lang.String.valueOf(this.f5094b) + "}";
    }
}
