package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.og */
public class C2567og implements com.fossil.blesdk.obfuscated.C2019ig {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.database.sqlite.SQLiteProgram f8116e;

    @DexIgnore
    public C2567og(android.database.sqlite.SQLiteProgram sQLiteProgram) {
        this.f8116e = sQLiteProgram;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11930a(int i) {
        this.f8116e.bindNull(i);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11934b(int i, long j) {
        this.f8116e.bindLong(i, j);
    }

    @DexIgnore
    public void close() {
        this.f8116e.close();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11931a(int i, double d) {
        this.f8116e.bindDouble(i, d);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11932a(int i, java.lang.String str) {
        this.f8116e.bindString(i, str);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11933a(int i, byte[] bArr) {
        this.f8116e.bindBlob(i, bArr);
    }
}
