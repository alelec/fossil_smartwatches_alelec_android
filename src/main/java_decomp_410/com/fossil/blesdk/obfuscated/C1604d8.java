package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d8 */
public class C1604d8 extends java.io.Writer {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.lang.String f4327e;

    @DexIgnore
    /* renamed from: f */
    public java.lang.StringBuilder f4328f; // = new java.lang.StringBuilder(128);

    @DexIgnore
    public C1604d8(java.lang.String str) {
        this.f4327e = str;
    }

    @DexIgnore
    public void close() {
        mo9809y();
    }

    @DexIgnore
    public void flush() {
        mo9809y();
    }

    @DexIgnore
    public void write(char[] cArr, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            char c = cArr[i + i3];
            if (c == 10) {
                mo9809y();
            } else {
                this.f4328f.append(c);
            }
        }
    }

    @DexIgnore
    /* renamed from: y */
    public final void mo9809y() {
        if (this.f4328f.length() > 0) {
            android.util.Log.d(this.f4327e, this.f4328f.toString());
            java.lang.StringBuilder sb = this.f4328f;
            sb.delete(0, sb.length());
        }
    }
}
