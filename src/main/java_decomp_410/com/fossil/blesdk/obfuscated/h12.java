package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h12 extends l12 {
    @DexIgnore
    public g12[] a;

    @DexIgnore
    public h12() {
        a();
    }

    @DexIgnore
    public h12 a() {
        this.a = g12.b();
        return this;
    }

    @DexIgnore
    public h12 a(j12 j12) throws IOException {
        while (true) {
            int j = j12.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                int a2 = n12.a(j12, 10);
                g12[] g12Arr = this.a;
                int length = g12Arr == null ? 0 : g12Arr.length;
                g12[] g12Arr2 = new g12[(a2 + length)];
                if (length != 0) {
                    System.arraycopy(this.a, 0, g12Arr2, 0, length);
                }
                while (length < g12Arr2.length - 1) {
                    g12Arr2[length] = new g12();
                    j12.a((l12) g12Arr2[length]);
                    j12.j();
                    length++;
                }
                g12Arr2[length] = new g12();
                j12.a((l12) g12Arr2[length]);
                this.a = g12Arr2;
            } else if (!n12.b(j12, j)) {
                return this;
            }
        }
    }
}
