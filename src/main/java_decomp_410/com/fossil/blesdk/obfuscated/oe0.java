package com.fossil.blesdk.obfuscated;

import android.util.Log;
import com.fossil.blesdk.obfuscated.me0;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oe0<R extends me0> implements ne0<R> {
    @DexIgnore
    public abstract void a(R r);

    @DexIgnore
    public abstract void a(Status status);

    @DexIgnore
    public final void onResult(R r) {
        Status G = r.G();
        if (G.L()) {
            a(r);
            return;
        }
        a(G);
        if (r instanceof je0) {
            try {
                ((je0) r).a();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(r);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("ResultCallbacks", sb.toString(), e);
            }
        }
    }
}
