package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class k5 {
    @DexIgnore
    public static /* final */ int bottom; // = 2131361873;
    @DexIgnore
    public static /* final */ int end; // = 2131362079;
    @DexIgnore
    public static /* final */ int gone; // = 2131362325;
    @DexIgnore
    public static /* final */ int invisible; // = 2131362381;
    @DexIgnore
    public static /* final */ int left; // = 2131362479;
    @DexIgnore
    public static /* final */ int packed; // = 2131362565;
    @DexIgnore
    public static /* final */ int parent; // = 2131362569;
    @DexIgnore
    public static /* final */ int percent; // = 2131362578;
    @DexIgnore
    public static /* final */ int right; // = 2131362617;
    @DexIgnore
    public static /* final */ int spread; // = 2131362730;
    @DexIgnore
    public static /* final */ int spread_inside; // = 2131362731;
    @DexIgnore
    public static /* final */ int start; // = 2131362737;
    @DexIgnore
    public static /* final */ int top; // = 2131362787;
    @DexIgnore
    public static /* final */ int wrap; // = 2131363046;
}
