package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.au */
public final class C1447au {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<com.bumptech.glide.load.DecodeFormat> f3605a; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.gif.GifOptions.DecodeFormat", com.bumptech.glide.load.DecodeFormat.DEFAULT);

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Boolean> f3606b; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.resource.gif.GifOptions.DisableAnimation", false);
}
