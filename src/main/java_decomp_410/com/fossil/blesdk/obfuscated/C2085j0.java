package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j0 */
public class C2085j0 {

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C2085j0 f6247d;

    @DexIgnore
    /* renamed from: a */
    public long f6248a;

    @DexIgnore
    /* renamed from: b */
    public long f6249b;

    @DexIgnore
    /* renamed from: c */
    public int f6250c;

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2085j0 m8720a() {
        if (f6247d == null) {
            f6247d = new com.fossil.blesdk.obfuscated.C2085j0();
        }
        return f6247d;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo12178a(long j, double d, double d2) {
        float f = ((float) (j - 946728000000L)) / 8.64E7f;
        float f2 = (0.01720197f * f) + 6.24006f;
        double d3 = (double) f2;
        double sin = (java.lang.Math.sin(d3) * 0.03341960161924362d) + d3 + (java.lang.Math.sin((double) (2.0f * f2)) * 3.4906598739326E-4d) + (java.lang.Math.sin((double) (f2 * 3.0f)) * 5.236000106378924E-6d) + 1.796593063d + 3.141592653589793d;
        double d4 = (-d2) / 360.0d;
        double round = ((double) (((float) java.lang.Math.round(((double) (f - 9.0E-4f)) - d4)) + 9.0E-4f)) + d4 + (java.lang.Math.sin(d3) * 0.0053d) + (java.lang.Math.sin(2.0d * sin) * -0.0069d);
        double asin = java.lang.Math.asin(java.lang.Math.sin(sin) * java.lang.Math.sin(0.4092797040939331d));
        double d5 = 0.01745329238474369d * d;
        double sin2 = (java.lang.Math.sin(-0.10471975803375244d) - (java.lang.Math.sin(d5) * java.lang.Math.sin(asin))) / (java.lang.Math.cos(d5) * java.lang.Math.cos(asin));
        if (sin2 >= 1.0d) {
            this.f6250c = 1;
            this.f6248a = -1;
            this.f6249b = -1;
        } else if (sin2 <= -1.0d) {
            this.f6250c = 0;
            this.f6248a = -1;
            this.f6249b = -1;
        } else {
            double acos = (double) ((float) (java.lang.Math.acos(sin2) / 6.283185307179586d));
            this.f6248a = java.lang.Math.round((round + acos) * 8.64E7d) + 946728000000L;
            this.f6249b = java.lang.Math.round((round - acos) * 8.64E7d) + 946728000000L;
            if (this.f6249b >= j || this.f6248a <= j) {
                this.f6250c = 1;
            } else {
                this.f6250c = 0;
            }
        }
    }
}
