package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class f43 implements Factory<ComplicationSearchPresenter> {
    @DexIgnore
    public static ComplicationSearchPresenter a(b43 b43, ComplicationRepository complicationRepository, en2 en2) {
        return new ComplicationSearchPresenter(b43, complicationRepository, en2);
    }
}
