package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.fragment.app.Fragment;
import com.fossil.blesdk.obfuscated.pq4;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sq4 implements DialogInterface.OnClickListener {
    @DexIgnore
    public Object e;
    @DexIgnore
    public tq4 f;
    @DexIgnore
    public pq4.a g;
    @DexIgnore
    public pq4.b h;

    @DexIgnore
    public sq4(vq4 vq4, tq4 tq4, pq4.a aVar, pq4.b bVar) {
        Object obj;
        if (vq4.getParentFragment() != null) {
            obj = vq4.getParentFragment();
        } else {
            obj = vq4.getActivity();
        }
        this.e = obj;
        this.f = tq4;
        this.g = aVar;
        this.h = bVar;
    }

    @DexIgnore
    public final void a() {
        pq4.a aVar = this.g;
        if (aVar != null) {
            tq4 tq4 = this.f;
            aVar.a(tq4.d, Arrays.asList(tq4.f));
        }
    }

    @DexIgnore
    public void onClick(DialogInterface dialogInterface, int i) {
        tq4 tq4 = this.f;
        int i2 = tq4.d;
        if (i == -1) {
            String[] strArr = tq4.f;
            pq4.b bVar = this.h;
            if (bVar != null) {
                bVar.a(i2);
            }
            Object obj = this.e;
            if (obj instanceof Fragment) {
                ar4.a((Fragment) obj).a(i2, strArr);
            } else if (obj instanceof Activity) {
                ar4.a((Activity) obj).a(i2, strArr);
            } else {
                throw new RuntimeException("Host must be an Activity or Fragment!");
            }
        } else {
            pq4.b bVar2 = this.h;
            if (bVar2 != null) {
                bVar2.b(i2);
            }
            a();
        }
    }

    @DexIgnore
    public sq4(uq4 uq4, tq4 tq4, pq4.a aVar, pq4.b bVar) {
        this.e = uq4.getActivity();
        this.f = tq4;
        this.g = aVar;
        this.h = bVar;
    }
}
