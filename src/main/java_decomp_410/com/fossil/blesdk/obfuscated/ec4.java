package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ec4 implements yb4<Object> {
    @DexIgnore
    public static /* final */ ec4 e; // = new ec4();

    @DexIgnore
    public CoroutineContext getContext() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public String toString() {
        return "This continuation is already complete";
    }
}
