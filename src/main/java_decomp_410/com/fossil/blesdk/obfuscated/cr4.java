package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.gr4;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import okhttp3.RequestBody;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cr4 extends gr4.a {
    @DexIgnore
    public boolean a; // = true;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements gr4<em4, em4> {
        @DexIgnore
        public static /* final */ a a; // = new a();

        @DexIgnore
        public em4 a(em4 em4) throws IOException {
            try {
                return ur4.a(em4);
            } finally {
                em4.close();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gr4<RequestBody, RequestBody> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj) throws IOException {
            RequestBody requestBody = (RequestBody) obj;
            a(requestBody);
            return requestBody;
        }

        @DexIgnore
        public RequestBody a(RequestBody requestBody) {
            return requestBody;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements gr4<em4, em4> {
        @DexIgnore
        public static /* final */ c a; // = new c();

        @DexIgnore
        public em4 a(em4 em4) {
            return em4;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object a(Object obj) throws IOException {
            em4 em4 = (em4) obj;
            a(em4);
            return em4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements gr4<Object, String> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        public String a(Object obj) {
            return obj.toString();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements gr4<em4, qa4> {
        @DexIgnore
        public static /* final */ e a; // = new e();

        @DexIgnore
        public qa4 a(em4 em4) {
            em4.close();
            return qa4.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements gr4<em4, Void> {
        @DexIgnore
        public static /* final */ f a; // = new f();

        @DexIgnore
        public Void a(em4 em4) {
            em4.close();
            return null;
        }
    }

    @DexIgnore
    public gr4<em4, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (type == em4.class) {
            if (ur4.a(annotationArr, (Class<? extends Annotation>) jt4.class)) {
                return c.a;
            }
            return a.a;
        } else if (type == Void.class) {
            return f.a;
        } else {
            if (!this.a || type != qa4.class) {
                return null;
            }
            try {
                return e.a;
            } catch (NoClassDefFoundError unused) {
                this.a = false;
                return null;
            }
        }
    }

    @DexIgnore
    public gr4<?, RequestBody> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, Retrofit retrofit3) {
        if (RequestBody.class.isAssignableFrom(ur4.b(type))) {
            return b.a;
        }
        return null;
    }
}
