package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ug */
public class C3037ug extends com.fossil.blesdk.obfuscated.C1876gi {

    @DexIgnore
    /* renamed from: b */
    public float f9961b; // = 3.0f;

    @DexIgnore
    /* renamed from: a */
    public long mo12355a(android.view.ViewGroup viewGroup, androidx.transition.Transition transition, com.fossil.blesdk.obfuscated.C2654ph phVar, com.fossil.blesdk.obfuscated.C2654ph phVar2) {
        int i;
        int i2;
        int i3;
        if (phVar == null && phVar2 == null) {
            return 0;
        }
        if (phVar2 == null || mo11244b(phVar) == 0) {
            i = -1;
        } else {
            phVar = phVar2;
            i = 1;
        }
        int c = mo11245c(phVar);
        int d = mo11246d(phVar);
        android.graphics.Rect c2 = transition.mo3546c();
        if (c2 != null) {
            i3 = c2.centerX();
            i2 = c2.centerY();
        } else {
            int[] iArr = new int[2];
            viewGroup.getLocationOnScreen(iArr);
            int round = java.lang.Math.round(((float) (iArr[0] + (viewGroup.getWidth() / 2))) + viewGroup.getTranslationX());
            i2 = java.lang.Math.round(((float) (iArr[1] + (viewGroup.getHeight() / 2))) + viewGroup.getTranslationY());
            i3 = round;
        }
        float a = m14749a((float) c, (float) d, (float) i3, (float) i2) / m14749a((float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) viewGroup.getWidth(), (float) viewGroup.getHeight());
        long b = transition.mo3539b();
        if (b < 0) {
            b = 300;
        }
        return (long) java.lang.Math.round((((float) (b * ((long) i))) / this.f9961b) * a);
    }

    @DexIgnore
    /* renamed from: a */
    public static float m14749a(float f, float f2, float f3, float f4) {
        float f5 = f3 - f;
        float f6 = f4 - f2;
        return (float) java.lang.Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }
}
