package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ei4 extends ki4<fi4> {
    @DexIgnore
    public /* final */ xc4<Throwable, qa4> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ei4(fi4 fi4, xc4<? super Throwable, qa4> xc4) {
        super(fi4);
        kd4.b(fi4, "job");
        kd4.b(xc4, "handler");
        this.i = xc4;
    }

    @DexIgnore
    public void b(Throwable th) {
        this.i.invoke(th);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        b((Throwable) obj);
        return qa4.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCompletion[" + dh4.a((Object) this) + '@' + dh4.b(this) + ']';
    }
}
