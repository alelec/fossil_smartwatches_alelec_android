package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.blesdk.obfuscated.gs3;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewPager;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ds2 extends zr2 implements ok3 {
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public nk3 j;
    @DexIgnore
    public n62 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public tr3<ob2> m;
    @DexIgnore
    public HashMap n;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ds2 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            ds2 ds2 = new ds2();
            ds2.setArguments(bundle);
            return ds2;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ds2 e;

        @DexIgnore
        public b(ds2 ds2) {
            this.e = ds2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ds2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewPager.i {
        @DexIgnore
        public void a(int i) {
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
        }

        @DexIgnore
        public void b(int i) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ nk3 a(ds2 ds2) {
        nk3 nk3 = ds2.j;
        if (nk3 != null) {
            return nk3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.n;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "ExploreWatchFragment";
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void g() {
        tr3<ob2> tr3 = this.m;
        if (tr3 != null) {
            ob2 a2 = tr3.a();
            if (a2 != null) {
                DashBar dashBar = a2.s;
                if (dashBar != null) {
                    gs3.a aVar = gs3.a;
                    kd4.a((Object) dashBar, "this");
                    aVar.a(dashBar, this.l, 500);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.m = new tr3<>(this, (ob2) qa.a(layoutInflater, R.layout.fragment_explore_watch, viewGroup, false, O0()));
        tr3<ob2> tr3 = this.m;
        if (tr3 != null) {
            ob2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        n62 n62 = this.k;
        if (n62 != null) {
            n62.b();
            nk3 nk3 = this.j;
            if (nk3 != null) {
                nk3.g();
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        nk3 nk3 = this.j;
        if (nk3 != null) {
            nk3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        this.k = new n62();
        tr3<ob2> tr3 = this.m;
        if (tr3 != null) {
            ob2 a2 = tr3.a();
            if (a2 != null) {
                RecyclerViewPager recyclerViewPager = a2.t;
                kd4.a((Object) recyclerViewPager, "binding.vpExplore");
                recyclerViewPager.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
                RecyclerViewPager recyclerViewPager2 = a2.t;
                kd4.a((Object) recyclerViewPager2, "binding.vpExplore");
                n62 n62 = this.k;
                if (n62 != null) {
                    recyclerViewPager2.setAdapter(n62);
                    a2.r.a((RecyclerView) a2.t, 0);
                    a2.r.setOnPageChangeListener(new c());
                    a2.q.setOnClickListener(new b(this));
                } else {
                    kd4.d("mAdapter");
                    throw null;
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                this.l = arguments.getBoolean("IS_ONBOARDING_FLOW");
                nk3 nk3 = this.j;
                if (nk3 != null) {
                    nk3.a(this.l);
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void s0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    public void u(List<? extends Explore> list) {
        kd4.b(list, "data");
        n62 n62 = this.k;
        if (n62 != null) {
            n62.a(list);
        } else {
            kd4.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(nk3 nk3) {
        kd4.b(nk3, "presenter");
        this.j = nk3;
    }
}
