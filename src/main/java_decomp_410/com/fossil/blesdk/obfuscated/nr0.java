package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nr0 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent e;
    @DexIgnore
    public /* final */ /* synthetic */ mr0 f;

    @DexIgnore
    public nr0(mr0 mr0, Intent intent) {
        this.f = mr0;
        this.e = intent;
    }

    @DexIgnore
    public final void run() {
        String action = this.e.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("EnhancedIntentService", sb.toString());
        this.f.a();
    }
}
