package com.fossil.blesdk.obfuscated;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;
    @DexIgnore
    public /* final */ /* synthetic */ int f;

    @DexIgnore
    public u04(Context context, int i) {
        this.e = context;
        this.f = i;
    }

    @DexIgnore
    public final void run() {
        try {
            j04.f(this.e);
            g14.b(this.e).a(this.f);
        } catch (Throwable th) {
            j04.m.a(th);
            j04.a(this.e, th);
        }
    }
}
