package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class z23 {
    @DexIgnore
    public /* final */ y23 a;

    @DexIgnore
    public z23(y23 y23) {
        kd4.b(y23, "mCommuteTimeSettingsDefaultAddressContractView");
        this.a = y23;
    }

    @DexIgnore
    public final y23 a() {
        return this.a;
    }
}
