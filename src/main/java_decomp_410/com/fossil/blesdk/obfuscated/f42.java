package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import com.facebook.internal.ServerProtocol;
import com.facebook.places.model.PlaceFields;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationRequest;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f42 implements ge0.b, ge0.c, rc1 {
    @DexIgnore
    public static /* final */ String l; // = "f42";
    @DexIgnore
    public static f42 m;
    @DexIgnore
    public ge0 e;
    @DexIgnore
    public CopyOnWriteArrayList<b> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Context g;
    @DexIgnore
    public Timer h;
    @DexIgnore
    public TimerTask i;
    @DexIgnore
    public double j;
    @DexIgnore
    public double k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TimerTask {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            SecureRandom secureRandom = new SecureRandom();
            f42 f42 = f42.this;
            double unused = f42.j = f42.j + (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            f42 f422 = f42.this;
            double unused2 = f422.k = f422.k + (secureRandom.nextDouble() * 8.0E-5d) + 1.0E-5d;
            Location location = new Location("");
            location.setLatitude(f42.this.j + 10.7604877d);
            location.setLongitude(f42.this.k + 106.698541d);
            Iterator it = f42.this.f.iterator();
            while (it.hasNext()) {
                ((b) it.next()).a(location, 1);
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Location location, int i);
    }

    @DexIgnore
    public final void d() {
        Timer timer = this.h;
        if (timer != null) {
            timer.cancel();
        }
        TimerTask timerTask = this.i;
        if (timerTask != null) {
            timerTask.cancel();
        }
    }

    @DexIgnore
    public void e(Bundle bundle) {
        Log.i(l, "MFLocationService is connected");
        c();
    }

    @DexIgnore
    public void f(int i2) {
        String str = l;
        Log.i(str, "MFLocationService is suspended - i=" + i2);
    }

    @DexIgnore
    public void onLocationChanged(Location location) {
        String str = l;
        Log.d(str, "Inside " + l + ".onLocationUpdated - location=" + location);
        CopyOnWriteArrayList<b> copyOnWriteArrayList = this.f;
        if (copyOnWriteArrayList != null) {
            Iterator<b> it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                it.next().a(location, 1);
            }
        }
    }

    @DexIgnore
    public final void c() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.i(0);
        locationRequest.j(1000);
        locationRequest.g(100);
        sc1.d.a(this.e, locationRequest, this);
    }

    @DexIgnore
    public static synchronized f42 a(Context context) {
        f42 f42;
        synchronized (f42.class) {
            if (m == null) {
                m = new f42();
            }
            m.g = context.getApplicationContext();
            f42 = m;
        }
        return f42;
    }

    @DexIgnore
    public final void b() {
        d();
        this.h = new Timer();
        this.i = new a();
        this.h.schedule(this.i, 0, 1000);
    }

    @DexIgnore
    public void a(b bVar) {
        if (!e42.a(this.g)) {
            bVar.a((Location) null, -1);
            return;
        }
        String str = l;
        Log.i(str, "Register Location Service - callback=" + bVar + ", size=" + this.f.size());
        this.f.add(bVar);
        if (d42.a(this.g, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            b();
            return;
        }
        if (this.e == null) {
            ge0.a aVar = new ge0.a(this.g);
            aVar.a((de0<? extends de0.d.C0010d>) sc1.c);
            aVar.a((ge0.b) this);
            aVar.a((ge0.c) this);
            this.e = aVar.a();
        }
        int a2 = a();
        if (a2 != 0) {
            bVar.a((Location) null, a2);
        } else {
            this.e.c();
        }
    }

    @DexIgnore
    public void b(b bVar) {
        this.f.remove(bVar);
        String str = l;
        Log.i(str, "Unregister Location Service - callback=" + bVar + ", size=" + this.f.size());
        if (d42.a(this.g, "fake").equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
            d();
            return;
        }
        ge0 ge0 = this.e;
        if (ge0 != null && ge0.g()) {
            sc1.d.a(this.e, this);
            this.e.d();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0026 A[ADDED_TO_REGION] */
    public int a() {
        boolean z;
        boolean z2;
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.g) != 0) {
            return -2;
        }
        LocationManager locationManager = (LocationManager) this.g.getSystemService(PlaceFields.LOCATION);
        try {
            z = locationManager.isProviderEnabled("gps");
            try {
                z2 = locationManager.isProviderEnabled("network");
            } catch (Exception unused) {
                z2 = false;
                if (!z) {
                }
                return 0;
            }
        } catch (Exception unused2) {
            z = false;
            z2 = false;
            if (!z) {
            }
            return 0;
        }
        if (!z || z2) {
            return 0;
        }
        return -1;
    }

    @DexIgnore
    public void a(ud0 ud0) {
        Log.e(l, "MFLocationService is failed to connect");
    }
}
