package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.id */
public class C2011id<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C3034ue f5998a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1941he<T> f5999b;

    @DexIgnore
    /* renamed from: c */
    public java.util.concurrent.Executor f6000c; // = com.fossil.blesdk.obfuscated.C1919h3.m7769d();

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2011id.C2015c<T>> f6001d; // = new java.util.concurrent.CopyOnWriteArrayList();

    @DexIgnore
    /* renamed from: e */
    public boolean f6002e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2723qd<T> f6003f;

    @DexIgnore
    /* renamed from: g */
    public com.fossil.blesdk.obfuscated.C2723qd<T> f6004g;

    @DexIgnore
    /* renamed from: h */
    public int f6005h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C2723qd.C2728e f6006i; // = new com.fossil.blesdk.obfuscated.C2011id.C2012a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.id$a")
    /* renamed from: com.fossil.blesdk.obfuscated.id$a */
    public class C2012a extends com.fossil.blesdk.obfuscated.C2723qd.C2728e {
        @DexIgnore
        public C2012a() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo11912a(int i, int i2) {
            com.fossil.blesdk.obfuscated.C2011id.this.f5998a.mo11200a(i, i2, (java.lang.Object) null);
        }

        @DexIgnore
        /* renamed from: b */
        public void mo11913b(int i, int i2) {
            com.fossil.blesdk.obfuscated.C2011id.this.f5998a.mo11201b(i, i2);
        }

        @DexIgnore
        /* renamed from: c */
        public void mo11914c(int i, int i2) {
            com.fossil.blesdk.obfuscated.C2011id.this.f5998a.mo11202c(i, i2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.id$b")
    /* renamed from: com.fossil.blesdk.obfuscated.id$b */
    public class C2013b implements java.lang.Runnable {

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2723qd f6008e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2723qd f6009f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ /* synthetic */ int f6010g;

        @DexIgnore
        /* renamed from: h */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2723qd f6011h;

        @DexIgnore
        /* renamed from: i */
        public /* final */ /* synthetic */ java.lang.Runnable f6012i;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.id$b$a")
        /* renamed from: com.fossil.blesdk.obfuscated.id$b$a */
        public class C2014a implements java.lang.Runnable {

            @DexIgnore
            /* renamed from: e */
            public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2314le.C2317c f6014e;

            @DexIgnore
            public C2014a(com.fossil.blesdk.obfuscated.C2314le.C2317c cVar) {
                this.f6014e = cVar;
            }

            @DexIgnore
            public void run() {
                com.fossil.blesdk.obfuscated.C2011id.C2013b bVar = com.fossil.blesdk.obfuscated.C2011id.C2013b.this;
                com.fossil.blesdk.obfuscated.C2011id idVar = com.fossil.blesdk.obfuscated.C2011id.this;
                if (idVar.f6005h == bVar.f6010g) {
                    idVar.mo11909a(bVar.f6011h, bVar.f6009f, this.f6014e, bVar.f6008e.f8609j, bVar.f6012i);
                }
            }
        }

        @DexIgnore
        public C2013b(com.fossil.blesdk.obfuscated.C2723qd qdVar, com.fossil.blesdk.obfuscated.C2723qd qdVar2, int i, com.fossil.blesdk.obfuscated.C2723qd qdVar3, java.lang.Runnable runnable) {
            this.f6008e = qdVar;
            this.f6009f = qdVar2;
            this.f6010g = i;
            this.f6011h = qdVar3;
            this.f6012i = runnable;
        }

        @DexIgnore
        public void run() {
            com.fossil.blesdk.obfuscated.C2011id.this.f6000c.execute(new com.fossil.blesdk.obfuscated.C2011id.C2013b.C2014a(com.fossil.blesdk.obfuscated.C2958td.m14152a(this.f6008e.f8608i, this.f6009f.f8608i, com.fossil.blesdk.obfuscated.C2011id.this.f5999b.mo11624b())));
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.id$c */
    public interface C2015c<T> {
        @DexIgnore
        /* renamed from: a */
        void mo11917a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2);
    }

    @DexIgnore
    public C2011id(androidx.recyclerview.widget.RecyclerView.C0227g gVar, com.fossil.blesdk.obfuscated.C2314le.C2318d<T> dVar) {
        this.f5998a = new com.fossil.blesdk.obfuscated.C1869ge(gVar);
        this.f5999b = new com.fossil.blesdk.obfuscated.C1941he.C1942a(dVar).mo11625a();
    }

    @DexIgnore
    /* renamed from: a */
    public T mo11906a(int i) {
        com.fossil.blesdk.obfuscated.C2723qd<T> qdVar = this.f6003f;
        if (qdVar == null) {
            com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2 = this.f6004g;
            if (qdVar2 != null) {
                return qdVar2.get(i);
            }
            throw new java.lang.IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        qdVar.mo15190g(i);
        return this.f6003f.get(i);
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11905a() {
        com.fossil.blesdk.obfuscated.C2723qd<T> qdVar = this.f6003f;
        if (qdVar != null) {
            return qdVar.size();
        }
        com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2 = this.f6004g;
        if (qdVar2 == null) {
            return 0;
        }
        return qdVar2.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11908a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar) {
        mo11911a(qdVar, (java.lang.Runnable) null);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11911a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, java.lang.Runnable runnable) {
        if (qdVar != null) {
            if (this.f6003f == null && this.f6004g == null) {
                this.f6002e = qdVar.mo12726g();
            } else if (qdVar.mo12726g() != this.f6002e) {
                throw new java.lang.IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i = this.f6005h + 1;
        this.f6005h = i;
        com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2 = this.f6003f;
        if (qdVar != qdVar2) {
            com.fossil.blesdk.obfuscated.C2723qd<T> qdVar3 = this.f6004g;
            if (qdVar3 != null) {
                qdVar2 = qdVar3;
            }
            if (qdVar == null) {
                int a = mo11905a();
                com.fossil.blesdk.obfuscated.C2723qd<T> qdVar4 = this.f6003f;
                if (qdVar4 != null) {
                    qdVar4.mo15180a(this.f6006i);
                    this.f6003f = null;
                } else if (this.f6004g != null) {
                    this.f6004g = null;
                }
                this.f5998a.mo11202c(0, a);
                mo11910a(qdVar2, (com.fossil.blesdk.obfuscated.C2723qd<T>) null, runnable);
            } else if (this.f6003f == null && this.f6004g == null) {
                this.f6003f = qdVar;
                qdVar.mo15181a((java.util.List<T>) null, this.f6006i);
                this.f5998a.mo11201b(0, qdVar.size());
                mo11910a((com.fossil.blesdk.obfuscated.C2723qd) null, qdVar, runnable);
            } else {
                com.fossil.blesdk.obfuscated.C2723qd<T> qdVar5 = this.f6003f;
                if (qdVar5 != null) {
                    qdVar5.mo15180a(this.f6006i);
                    this.f6004g = (com.fossil.blesdk.obfuscated.C2723qd) this.f6003f.mo15195j();
                    this.f6003f = null;
                }
                com.fossil.blesdk.obfuscated.C2723qd<T> qdVar6 = this.f6004g;
                if (qdVar6 == null || this.f6003f != null) {
                    throw new java.lang.IllegalStateException("must be in snapshot state to diff");
                }
                java.util.concurrent.Executor a2 = this.f5999b.mo11623a();
                com.fossil.blesdk.obfuscated.C2011id.C2013b bVar = new com.fossil.blesdk.obfuscated.C2011id.C2013b(qdVar6, (com.fossil.blesdk.obfuscated.C2723qd) qdVar.mo15195j(), i, qdVar, runnable);
                a2.execute(bVar);
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11909a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2, com.fossil.blesdk.obfuscated.C2314le.C2317c cVar, int i, java.lang.Runnable runnable) {
        com.fossil.blesdk.obfuscated.C2723qd<T> qdVar3 = this.f6004g;
        if (qdVar3 == null || this.f6003f != null) {
            throw new java.lang.IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f6003f = qdVar;
        this.f6004g = null;
        com.fossil.blesdk.obfuscated.C2958td.m14153a(this.f5998a, qdVar3.f8608i, qdVar.f8608i, cVar);
        qdVar.mo15181a((java.util.List<T>) qdVar2, this.f6006i);
        if (!this.f6003f.isEmpty()) {
            int a = com.fossil.blesdk.obfuscated.C2958td.m14151a(cVar, (com.fossil.blesdk.obfuscated.C2871sd) qdVar3.f8608i, (com.fossil.blesdk.obfuscated.C2871sd) qdVar2.f8608i, i);
            com.fossil.blesdk.obfuscated.C2723qd<T> qdVar4 = this.f6003f;
            qdVar4.mo15190g(java.lang.Math.max(0, java.lang.Math.min(qdVar4.size() - 1, a)));
        }
        mo11910a(qdVar3, this.f6003f, runnable);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo11910a(com.fossil.blesdk.obfuscated.C2723qd<T> qdVar, com.fossil.blesdk.obfuscated.C2723qd<T> qdVar2, java.lang.Runnable runnable) {
        for (com.fossil.blesdk.obfuscated.C2011id.C2015c<T> a : this.f6001d) {
            a.mo11917a(qdVar, qdVar2);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11907a(com.fossil.blesdk.obfuscated.C2011id.C2015c<T> cVar) {
        this.f6001d.add(cVar);
    }
}
