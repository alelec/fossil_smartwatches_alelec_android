package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rb3 implements Factory<GoalTrackingOverviewMonthPresenter> {
    @DexIgnore
    public static GoalTrackingOverviewMonthPresenter a(pb3 pb3, UserRepository userRepository, en2 en2, GoalTrackingRepository goalTrackingRepository) {
        return new GoalTrackingOverviewMonthPresenter(pb3, userRepository, en2, goalTrackingRepository);
    }
}
