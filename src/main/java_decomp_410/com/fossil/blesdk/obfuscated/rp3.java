package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class rp3 extends as2 implements lq3, ws3.g {
    @DexIgnore
    public static /* final */ a r; // = new a((fd4) null);
    @DexIgnore
    public kq3 k;
    @DexIgnore
    public tr3<qb2> l;
    @DexIgnore
    public be1 m;
    @DexIgnore
    public View n;
    @DexIgnore
    public Bitmap o;
    @DexIgnore
    public xn p;
    @DexIgnore
    public HashMap q;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final rp3 a() {
            return new rp3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ rp3 e;

        @DexIgnore
        public b(rp3 rp3) {
            this.e = rp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements de1 {
        @DexIgnore
        public /* final */ /* synthetic */ rp3 a;

        @DexIgnore
        public c(rp3 rp3) {
            this.a = rp3;
        }

        @DexIgnore
        public final void a(be1 be1) {
            this.a.m = be1;
            kd4.a((Object) be1, "googleMap");
            he1 b = be1.b();
            kd4.a((Object) b, "googleMap.uiSettings");
            b.a(false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ rp3 a;
        @DexIgnore
        public /* final */ /* synthetic */ qb2 b;

        @DexIgnore
        public d(rp3 rp3, qb2 qb2) {
            this.a = rp3;
            this.b = qb2;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ts3.a((View) this.b.x);
            ConstraintLayout constraintLayout = this.b.q;
            kd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(z ? 0 : 4);
            rp3.a(this.a).a(z);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ qb2 a;
        @DexIgnore
        public /* final */ /* synthetic */ rp3 b;

        @DexIgnore
        public e(qb2 qb2, rp3 rp3, WatchSettingViewModel.c cVar) {
            this.a = qb2;
            this.b = rp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            kd4.b(str, "serial");
            kd4.b(str2, "filePath");
            rp3.b(this.b).a(str2).a(this.a.w);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ rp3 b;

        @DexIgnore
        public f(View view, String str, rp3 rp3, WatchSettingViewModel.c cVar) {
            this.a = view;
            this.b = rp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            kd4.b(str, "serial");
            kd4.b(str2, "filePath");
            if (this.b.isActive()) {
                View findViewById = this.a.findViewById(R.id.image);
                if (findViewById != null) {
                    ((ImageView) findViewById).setImageBitmap(al2.a(str2));
                    int dimensionPixelSize = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp96);
                    this.a.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
                    View view = this.a;
                    view.layout(0, 0, view.getMeasuredWidth(), this.a.getMeasuredHeight());
                    this.b.o = al2.a(this.a);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.widget.ImageView");
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ kq3 a(rp3 rp3) {
        kq3 kq3 = rp3.k;
        if (kq3 != null) {
            return kq3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ xn b(rp3 rp3) {
        xn xnVar = rp3.p;
        if (xnVar != null) {
            return xnVar;
        }
        kd4.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public void K0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationNotFound");
        qb2 T0 = T0();
        if (T0 != null) {
            SwitchCompat switchCompat = T0.x;
            kd4.a((Object) switchCompat, "binding.swLocate");
            switchCompat.setEnabled(false);
            T0.u.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.disabled));
            ConstraintLayout constraintLayout = T0.q;
            kd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            FlexibleTextView flexibleTextView = T0.r;
            kd4.a((Object) flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(0);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final qb2 T0() {
        tr3<qb2> tr3 = this.l;
        if (tr3 != null) {
            return tr3.a();
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b0() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationPermissionError");
        qb2 T0 = T0();
        if (T0 != null) {
            SwitchCompat switchCompat = T0.x;
            kd4.a((Object) switchCompat, "binding.swLocate");
            switchCompat.setChecked(false);
            ConstraintLayout constraintLayout = T0.q;
            kd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
        }
    }

    @DexIgnore
    public void k(int i) {
        tr3<qb2> tr3 = this.l;
        if (tr3 != null) {
            qb2 a2 = tr3.a();
            if (a2 != null) {
                float b2 = DeviceHelper.o.b(i);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDeviceFragment", "showProximity - rssi: " + i + ", distanceInFt: " + b2);
                if (b2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && b2 <= 5.0f) {
                    FlexibleTextView flexibleTextView = a2.z;
                    kd4.a((Object) flexibleTextView, "binding.tvLocationStatus");
                    flexibleTextView.setText(sm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_FindDevice_Label__VeryClose));
                    FlexibleTextView flexibleTextView2 = a2.z;
                    kd4.a((Object) flexibleTextView2, "binding.tvLocationStatus");
                    flexibleTextView2.setEnabled(true);
                } else if (b2 >= 5.0f && b2 <= 10.0f) {
                    FlexibleTextView flexibleTextView3 = a2.z;
                    kd4.a((Object) flexibleTextView3, "binding.tvLocationStatus");
                    flexibleTextView3.setText(sm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_FindDevice_Label__Close));
                    FlexibleTextView flexibleTextView4 = a2.z;
                    kd4.a((Object) flexibleTextView4, "binding.tvLocationStatus");
                    flexibleTextView4.setEnabled(true);
                } else if (b2 < 10.0f || b2 > 30.0f) {
                    FlexibleTextView flexibleTextView5 = a2.z;
                    kd4.a((Object) flexibleTextView5, "binding.tvLocationStatus");
                    flexibleTextView5.setText(sm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__OutOfRange));
                    FlexibleTextView flexibleTextView6 = a2.z;
                    kd4.a((Object) flexibleTextView6, "binding.tvLocationStatus");
                    flexibleTextView6.setEnabled(false);
                } else {
                    FlexibleTextView flexibleTextView7 = a2.z;
                    kd4.a((Object) flexibleTextView7, "binding.tvLocationStatus");
                    flexibleTextView7.setText(sm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_FindDevice_Label__Nearby));
                    FlexibleTextView flexibleTextView8 = a2.z;
                    kd4.a((Object) flexibleTextView8, "binding.tvLocationStatus");
                    flexibleTextView8.setEnabled(true);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        xn a2 = rn.a((Fragment) this);
        kd4.a((Object) a2, "Glide.with(this)");
        this.p = a2;
    }

    @DexIgnore
    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        qb2 qb2 = (qb2) qa.a(layoutInflater, R.layout.fragment_find_device, viewGroup, false, O0());
        this.n = layoutInflater.inflate(R.layout.marker, (ViewGroup) null);
        View view = this.n;
        if (view != null) {
            int dimensionPixelSize = PortfolioApp.W.c().getResources().getDimensionPixelSize(R.dimen.dp96);
            view.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            this.o = al2.a(view);
        }
        qb2.v.setOnClickListener(new b(this));
        Fragment a2 = getChildFragmentManager().a((int) R.id.f_map);
        if (a2 != null) {
            ((SupportMapFragment) a2).a(new c(this));
            qb2.x.setOnCheckedChangeListener(new d(this, qb2));
            this.l = new tr3<>(this, qb2);
            kd4.a((Object) qb2, "binding");
            return qb2.d();
        }
        throw new TypeCastException("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onPause");
        super.onPause();
        kq3 kq3 = this.k;
        if (kq3 != null) {
            kq3.g();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onResume() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onResume");
        super.onResume();
        kq3 kq3 = this.k;
        if (kq3 != null) {
            kq3.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void y(String str) {
        kd4.b(str, "address");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showAddress: address = " + str);
        tr3<qb2> tr3 = this.l;
        if (tr3 != null) {
            qb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(str);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void b(boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocationEnable: enable = " + z + ", needWarning = " + z2);
        tr3<qb2> tr3 = this.l;
        if (tr3 != null) {
            qb2 a2 = tr3.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.x;
                kd4.a((Object) switchCompat, "binding.swLocate");
                switchCompat.setChecked(z);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(WatchSettingViewModel.c cVar) {
        kd4.b(cVar, "watchSetting");
        if (isActive()) {
            tr3<qb2> tr3 = this.l;
            if (tr3 != null) {
                qb2 a2 = tr3.a();
                if (a2 != null) {
                    String deviceId = cVar.a().getDeviceId();
                    FlexibleTextView flexibleTextView = a2.y;
                    kd4.a((Object) flexibleTextView, "it.tvDeviceName");
                    flexibleTextView.setText(cVar.b());
                    a2.y.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    FlexibleTextView flexibleTextView2 = a2.r;
                    kd4.a((Object) flexibleTextView2, "it.ftvError");
                    pd4 pd4 = pd4.a;
                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchFindDevice_NoConnectedLocationInfo_Text__YourWatchNameHasntBeenConnected);
                    kd4.a((Object) a3, "LanguageHelper.getString\u2026chNameHasntBeenConnected)");
                    Object[] objArr = {cVar.b()};
                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                    kd4.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView2.setText(format);
                    boolean d2 = cVar.d();
                    if (d2) {
                        if (cVar.e()) {
                            a2.y.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.success));
                            FlexibleTextView flexibleTextView3 = a2.y;
                            kd4.a((Object) flexibleTextView3, "it.tvDeviceName");
                            flexibleTextView3.setAlpha(1.0f);
                            a2.y.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, k6.c(PortfolioApp.W.c(), R.drawable.ic_device_status), (Drawable) null);
                        } else {
                            a2.y.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryText));
                            FlexibleTextView flexibleTextView4 = a2.y;
                            kd4.a((Object) flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView5 = a2.z;
                            kd4.a((Object) flexibleTextView5, "it.tvLocationStatus");
                            flexibleTextView5.setText(sm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__OutOfRange));
                            FlexibleTextView flexibleTextView6 = a2.z;
                            kd4.a((Object) flexibleTextView6, "it.tvLocationStatus");
                            flexibleTextView6.setEnabled(false);
                        }
                    } else if (!d2) {
                        a2.y.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryText));
                        FlexibleTextView flexibleTextView7 = a2.y;
                        kd4.a((Object) flexibleTextView7, "it.tvDeviceName");
                        flexibleTextView7.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView8 = a2.z;
                        kd4.a((Object) flexibleTextView8, "it.tvLocationStatus");
                        flexibleTextView8.setText(sm2.a(getContext(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__OutOfRange));
                        FlexibleTextView flexibleTextView9 = a2.z;
                        kd4.a((Object) flexibleTextView9, "it.tvLocationStatus");
                        flexibleTextView9.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.w;
                    kd4.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.b(deviceId, DeviceHelper.ImageStyle.SMALL)).setImageCallback(new e(a2, this, cVar)).download();
                    View view = this.n;
                    if (view != null) {
                        new CloudImageHelper().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.b(deviceId)).setType(Constants.DeviceType.TYPE_LARGE).setImageCallback(new f(view, deviceId, this, cVar)).download();
                        return;
                    }
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(kq3 kq3) {
        kd4.b(kq3, "presenter");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "setPresenter: presenter = " + kq3);
        this.k = kq3;
    }

    @DexIgnore
    public void a(Double d2, Double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocation: latitude = " + d2 + ", longitude = " + d3);
        qb2 T0 = T0();
        if (T0 != null) {
            ConstraintLayout constraintLayout = T0.q;
            kd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(0);
            FlexibleTextView flexibleTextView = T0.r;
            kd4.a((Object) flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(8);
            if (d2 != null && d3 != null) {
                LatLng latLng = new LatLng(d2.doubleValue(), d3.doubleValue());
                be1 be1 = this.m;
                if (be1 != null) {
                    be1.b(ae1.a(latLng, 13.0f));
                    be1.a(ae1.a(18.0f));
                    be1.a();
                    Bitmap bitmap = this.o;
                    if (bitmap != null) {
                        kf1 kf1 = new kf1();
                        kf1.e(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatchFindDevice_DeviceisOutofRange_Label__LastDetectedLocation));
                        kf1.a(if1.a(bitmap));
                        kf1.a(latLng);
                        be1.a(kf1);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(long j) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showTime: time = " + j);
        tr3<qb2> tr3 = this.l;
        if (tr3 != null) {
            qb2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(DateUtils.getRelativeTimeSpanString(j));
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(int i, String str) {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showErrorDialog");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i, str, childFragmentManager);
        }
        qb2 T0 = T0();
        if (T0 != null) {
            SwitchCompat switchCompat = T0.x;
            kd4.a((Object) switchCompat, "binding.swLocate");
            switchCompat.setChecked(false);
            ConstraintLayout constraintLayout = T0.q;
            kd4.a((Object) constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
        }
    }
}
