package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import com.fossil.blesdk.obfuscated.ge0;
import com.fossil.blesdk.obfuscated.ij0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dl0 implements ij0.a {
    @DexIgnore
    public /* final */ /* synthetic */ ge0.b a;

    @DexIgnore
    public dl0(ge0.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        this.a.e(bundle);
    }

    @DexIgnore
    public final void f(int i) {
        this.a.f(i);
    }
}
