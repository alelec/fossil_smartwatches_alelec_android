package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ph */
public class C2654ph {

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.Map<java.lang.String, java.lang.Object> f8382a; // = new java.util.HashMap();

    @DexIgnore
    /* renamed from: b */
    public android.view.View f8383b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.ArrayList<androidx.transition.Transition> f8384c; // = new java.util.ArrayList<>();

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2654ph)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2654ph phVar = (com.fossil.blesdk.obfuscated.C2654ph) obj;
        return this.f8383b == phVar.f8383b && this.f8382a.equals(phVar.f8382a);
    }

    @DexIgnore
    public int hashCode() {
        return (this.f8383b.hashCode() * 31) + this.f8382a.hashCode();
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.String str = "TransitionValues@" + java.lang.Integer.toHexString(hashCode()) + ":\n";
        java.lang.String str2 = str + "    view = " + this.f8383b + "\n";
        java.lang.String str3 = str2 + "    values:";
        for (java.lang.String next : this.f8382a.keySet()) {
            str3 = str3 + "    " + next + ": " + this.f8382a.get(next) + "\n";
        }
        return str3;
    }
}
