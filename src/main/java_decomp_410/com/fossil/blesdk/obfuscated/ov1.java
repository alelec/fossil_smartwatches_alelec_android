package com.fossil.blesdk.obfuscated;

import com.google.common.primitives.Ints;
import java.math.RoundingMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ov1 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[RoundingMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[RoundingMode.UNNECESSARY.ordinal()] = 1;
            a[RoundingMode.DOWN.ordinal()] = 2;
            a[RoundingMode.FLOOR.ordinal()] = 3;
            a[RoundingMode.UP.ordinal()] = 4;
            a[RoundingMode.CEILING.ordinal()] = 5;
            a[RoundingMode.HALF_DOWN.ordinal()] = 6;
            a[RoundingMode.HALF_UP.ordinal()] = 7;
            try {
                a[RoundingMode.HALF_EVEN.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public static int a(int i, int i2) {
        return (~(~(i - i2))) >>> 31;
    }

    @DexIgnore
    public static int a(int i, RoundingMode roundingMode) {
        pv1.a("x", i);
        switch (a.a[roundingMode.ordinal()]) {
            case 1:
                pv1.a(a(i));
                break;
            case 2:
            case 3:
                break;
            case 4:
            case 5:
                return 32 - Integer.numberOfLeadingZeros(i - 1);
            case 6:
            case 7:
            case 8:
                int numberOfLeadingZeros = Integer.numberOfLeadingZeros(i);
                return (31 - numberOfLeadingZeros) + a(-1257966797 >>> numberOfLeadingZeros, i);
            default:
                throw new AssertionError();
        }
        return 31 - Integer.numberOfLeadingZeros(i);
    }

    @DexIgnore
    public static boolean a(int i) {
        boolean z = false;
        boolean z2 = i > 0;
        if ((i & (i - 1)) == 0) {
            z = true;
        }
        return z2 & z;
    }

    @DexIgnore
    public static int b(int i, int i2) {
        return Ints.a(((long) i) + ((long) i2));
    }
}
