package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewDayPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewMonthPresenter;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j83 implements MembersInjector<ActiveTimeOverviewFragment> {
    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, ActiveTimeOverviewDayPresenter activeTimeOverviewDayPresenter) {
        activeTimeOverviewFragment.k = activeTimeOverviewDayPresenter;
    }

    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, ActiveTimeOverviewWeekPresenter activeTimeOverviewWeekPresenter) {
        activeTimeOverviewFragment.l = activeTimeOverviewWeekPresenter;
    }

    @DexIgnore
    public static void a(ActiveTimeOverviewFragment activeTimeOverviewFragment, ActiveTimeOverviewMonthPresenter activeTimeOverviewMonthPresenter) {
        activeTimeOverviewFragment.m = activeTimeOverviewMonthPresenter;
    }
}
