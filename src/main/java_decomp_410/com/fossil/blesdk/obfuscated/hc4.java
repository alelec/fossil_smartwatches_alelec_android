package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Field;
import kotlin.coroutines.jvm.internal.BaseContinuationImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hc4 {
    @DexIgnore
    public static final gc4 a(BaseContinuationImpl baseContinuationImpl) {
        return (gc4) baseContinuationImpl.getClass().getAnnotation(gc4.class);
    }

    @DexIgnore
    public static final int b(BaseContinuationImpl baseContinuationImpl) {
        try {
            Field declaredField = baseContinuationImpl.getClass().getDeclaredField("label");
            kd4.a((Object) declaredField, "field");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(baseContinuationImpl);
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            return (num != null ? num.intValue() : 0) - 1;
        } catch (Exception unused) {
            return -1;
        }
    }

    @DexIgnore
    public static final StackTraceElement c(BaseContinuationImpl baseContinuationImpl) {
        int i;
        String str;
        kd4.b(baseContinuationImpl, "$this$getStackTraceElementImpl");
        gc4 a = a(baseContinuationImpl);
        if (a == null) {
            return null;
        }
        a(1, a.v());
        int b = b(baseContinuationImpl);
        if (b < 0) {
            i = -1;
        } else {
            i = a.l()[b];
        }
        String b2 = jc4.c.b(baseContinuationImpl);
        if (b2 == null) {
            str = a.c();
        } else {
            str = b2 + '/' + a.c();
        }
        return new StackTraceElement(str, a.m(), a.f(), i);
    }

    @DexIgnore
    public static final void a(int i, int i2) {
        if (i2 > i) {
            throw new IllegalStateException(("Debug metadata version mismatch. Expected: " + i + ", got " + i2 + ". Please update the Kotlin standard library.").toString());
        }
    }
}
