package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ob implements Closeable, zg4 {
    @DexIgnore
    public /* final */ CoroutineContext e;

    @DexIgnore
    public ob(CoroutineContext coroutineContext) {
        kd4.b(coroutineContext, "context");
        this.e = coroutineContext;
    }

    @DexIgnore
    public CoroutineContext A() {
        return this.e;
    }

    @DexIgnore
    public void close() {
        ji4.a(A(), (CancellationException) null, 1, (Object) null);
    }
}
