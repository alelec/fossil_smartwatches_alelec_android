package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class aa2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewWeekChart q;

    @DexIgnore
    public aa2(Object obj, View view, int i, OverviewWeekChart overviewWeekChart) {
        super(obj, view, i);
        this.q = overviewWeekChart;
    }
}
