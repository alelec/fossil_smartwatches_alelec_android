package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class no1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ wn1 e;
    @DexIgnore
    public /* final */ /* synthetic */ mo1 f;

    @DexIgnore
    public no1(mo1 mo1, wn1 wn1) {
        this.f = mo1;
        this.e = wn1;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.f.b) {
            if (this.f.c != null) {
                this.f.c.onSuccess(this.e.b());
            }
        }
    }
}
