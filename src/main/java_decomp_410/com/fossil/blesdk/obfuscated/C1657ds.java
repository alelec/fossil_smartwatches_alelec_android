package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ds */
public class C1657ds implements com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> {

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2232ko<java.lang.Integer> f4524b; // = com.fossil.blesdk.obfuscated.C2232ko.m9712a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2826rr<com.fossil.blesdk.obfuscated.C2340lr, com.fossil.blesdk.obfuscated.C2340lr> f4525a;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.ds$a")
    /* renamed from: com.fossil.blesdk.obfuscated.ds$a */
    public static class C1658a implements com.fossil.blesdk.obfuscated.C2984tr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2826rr<com.fossil.blesdk.obfuscated.C2340lr, com.fossil.blesdk.obfuscated.C2340lr> f4526a; // = new com.fossil.blesdk.obfuscated.C2826rr<>(500);

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2912sr<com.fossil.blesdk.obfuscated.C2340lr, java.io.InputStream> mo8913a(com.fossil.blesdk.obfuscated.C3236wr wrVar) {
            return new com.fossil.blesdk.obfuscated.C1657ds(this.f4526a);
        }
    }

    @DexIgnore
    public C1657ds(com.fossil.blesdk.obfuscated.C2826rr<com.fossil.blesdk.obfuscated.C2340lr, com.fossil.blesdk.obfuscated.C2340lr> rrVar) {
        this.f4525a = rrVar;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8912a(com.fossil.blesdk.obfuscated.C2340lr lrVar) {
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2912sr.C2913a<java.io.InputStream> mo8911a(com.fossil.blesdk.obfuscated.C2340lr lrVar, int i, int i2, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        com.fossil.blesdk.obfuscated.C2826rr<com.fossil.blesdk.obfuscated.C2340lr, com.fossil.blesdk.obfuscated.C2340lr> rrVar = this.f4525a;
        if (rrVar != null) {
            com.fossil.blesdk.obfuscated.C2340lr a = rrVar.mo15717a(lrVar, 0, 0);
            if (a == null) {
                this.f4525a.mo15718a(lrVar, 0, 0, lrVar);
            } else {
                lrVar = a;
            }
        }
        return new com.fossil.blesdk.obfuscated.C2912sr.C2913a<>(lrVar, new com.fossil.blesdk.obfuscated.C3373yo(lrVar, ((java.lang.Integer) loVar.mo13319a(f4524b)).intValue()));
    }
}
