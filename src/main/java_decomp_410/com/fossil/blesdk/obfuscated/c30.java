package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.logic.request.code.AsyncEventType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class c30 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[AsyncEventType.values().length];

    /*
    static {
        a[AsyncEventType.JSON_FILE_EVENT.ordinal()] = 1;
        a[AsyncEventType.HEARTBEAT_EVENT.ordinal()] = 2;
        a[AsyncEventType.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 3;
        a[AsyncEventType.APP_NOTIFICATION_EVENT.ordinal()] = 4;
        a[AsyncEventType.MUSIC_EVENT.ordinal()] = 5;
        a[AsyncEventType.BACKGROUND_SYNC_EVENT.ordinal()] = 6;
        a[AsyncEventType.SERVICE_CHANGE_EVENT.ordinal()] = 7;
        a[AsyncEventType.MICRO_APP_EVENT.ordinal()] = 8;
        a[AsyncEventType.TIME_SYNC_EVENT.ordinal()] = 9;
    }
    */
}
