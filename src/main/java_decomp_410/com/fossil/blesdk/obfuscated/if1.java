package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;
import android.os.RemoteException;
import com.google.android.gms.maps.model.RuntimeRemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class if1 {
    @DexIgnore
    public static d51 a;

    @DexIgnore
    public static d51 a() {
        d51 d51 = a;
        bk0.a(d51, (Object) "IBitmapDescriptorFactory is not initialized");
        return d51;
    }

    @DexIgnore
    public static void a(d51 d51) {
        if (a == null) {
            bk0.a(d51);
            a = d51;
        }
    }

    @DexIgnore
    public static hf1 a(Bitmap bitmap) {
        try {
            return new hf1(a().zza(bitmap));
        } catch (RemoteException e) {
            throw new RuntimeRemoteException(e);
        }
    }
}
