package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ga */
public abstract class C1864ga extends com.fossil.blesdk.obfuscated.C1710ea {

    @DexIgnore
    /* renamed from: m */
    public int f5402m;

    @DexIgnore
    /* renamed from: n */
    public int f5403n;

    @DexIgnore
    /* renamed from: o */
    public android.view.LayoutInflater f5404o;

    @DexIgnore
    @java.lang.Deprecated
    public C1864ga(android.content.Context context, int i, android.database.Cursor cursor, boolean z) {
        super(context, cursor, z);
        this.f5403n = i;
        this.f5402m = i;
        this.f5404o = (android.view.LayoutInflater) context.getSystemService("layout_inflater");
    }

    @DexIgnore
    /* renamed from: a */
    public android.view.View mo10375a(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup viewGroup) {
        return this.f5404o.inflate(this.f5403n, viewGroup, false);
    }

    @DexIgnore
    /* renamed from: b */
    public android.view.View mo10379b(android.content.Context context, android.database.Cursor cursor, android.view.ViewGroup viewGroup) {
        return this.f5404o.inflate(this.f5402m, viewGroup, false);
    }
}
