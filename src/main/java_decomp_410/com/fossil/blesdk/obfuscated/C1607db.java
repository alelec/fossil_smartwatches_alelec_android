package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.db */
public class C1607db extends com.fossil.blesdk.obfuscated.C1713eb {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.db$a")
    /* renamed from: com.fossil.blesdk.obfuscated.db$a */
    public class C1608a extends android.transition.Transition.EpicenterCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.graphics.Rect f4335a;

        @DexIgnore
        public C1608a(com.fossil.blesdk.obfuscated.C1607db dbVar, android.graphics.Rect rect) {
            this.f4335a = rect;
        }

        @DexIgnore
        public android.graphics.Rect onGetEpicenter(android.transition.Transition transition) {
            return this.f4335a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.db$b")
    /* renamed from: com.fossil.blesdk.obfuscated.db$b */
    public class C1609b implements android.transition.Transition.TransitionListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.view.View f4336a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.util.ArrayList f4337b;

        @DexIgnore
        public C1609b(com.fossil.blesdk.obfuscated.C1607db dbVar, android.view.View view, java.util.ArrayList arrayList) {
            this.f4336a = view;
            this.f4337b = arrayList;
        }

        @DexIgnore
        public void onTransitionCancel(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(android.transition.Transition transition) {
            transition.removeListener(this);
            this.f4336a.setVisibility(8);
            int size = this.f4337b.size();
            for (int i = 0; i < size; i++) {
                ((android.view.View) this.f4337b.get(i)).setVisibility(0);
            }
        }

        @DexIgnore
        public void onTransitionPause(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(android.transition.Transition transition) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.db$c")
    /* renamed from: com.fossil.blesdk.obfuscated.db$c */
    public class C1610c implements android.transition.Transition.TransitionListener {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ java.lang.Object f4338a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ /* synthetic */ java.util.ArrayList f4339b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ /* synthetic */ java.lang.Object f4340c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ /* synthetic */ java.util.ArrayList f4341d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ /* synthetic */ java.lang.Object f4342e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ /* synthetic */ java.util.ArrayList f4343f;

        @DexIgnore
        public C1610c(java.lang.Object obj, java.util.ArrayList arrayList, java.lang.Object obj2, java.util.ArrayList arrayList2, java.lang.Object obj3, java.util.ArrayList arrayList3) {
            this.f4338a = obj;
            this.f4339b = arrayList;
            this.f4340c = obj2;
            this.f4341d = arrayList2;
            this.f4342e = obj3;
            this.f4343f = arrayList3;
        }

        @DexIgnore
        public void onTransitionCancel(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(android.transition.Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(android.transition.Transition transition) {
            java.lang.Object obj = this.f4338a;
            if (obj != null) {
                com.fossil.blesdk.obfuscated.C1607db.this.mo9866a(obj, (java.util.ArrayList<android.view.View>) this.f4339b, (java.util.ArrayList<android.view.View>) null);
            }
            java.lang.Object obj2 = this.f4340c;
            if (obj2 != null) {
                com.fossil.blesdk.obfuscated.C1607db.this.mo9866a(obj2, (java.util.ArrayList<android.view.View>) this.f4341d, (java.util.ArrayList<android.view.View>) null);
            }
            java.lang.Object obj3 = this.f4342e;
            if (obj3 != null) {
                com.fossil.blesdk.obfuscated.C1607db.this.mo9866a(obj3, (java.util.ArrayList<android.view.View>) this.f4343f, (java.util.ArrayList<android.view.View>) null);
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.db$d")
    /* renamed from: com.fossil.blesdk.obfuscated.db$d */
    public class C1611d extends android.transition.Transition.EpicenterCallback {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ android.graphics.Rect f4345a;

        @DexIgnore
        public C1611d(com.fossil.blesdk.obfuscated.C1607db dbVar, android.graphics.Rect rect) {
            this.f4345a = rect;
        }

        @DexIgnore
        public android.graphics.Rect onGetEpicenter(android.transition.Transition transition) {
            android.graphics.Rect rect = this.f4345a;
            if (rect == null || rect.isEmpty()) {
                return null;
            }
            return this.f4345a;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo9867a(java.lang.Object obj) {
        return obj instanceof android.transition.Transition;
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object mo9868b(java.lang.Object obj) {
        if (obj != null) {
            return ((android.transition.Transition) obj).clone();
        }
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public java.lang.Object mo9873c(java.lang.Object obj) {
        if (obj == null) {
            return null;
        }
        android.transition.TransitionSet transitionSet = new android.transition.TransitionSet();
        transitionSet.addTransition((android.transition.Transition) obj);
        return transitionSet;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9865a(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList) {
        android.transition.Transition transition = (android.transition.Transition) obj;
        if (transition != null) {
            int i = 0;
            if (transition instanceof android.transition.TransitionSet) {
                android.transition.TransitionSet transitionSet = (android.transition.TransitionSet) transition;
                int transitionCount = transitionSet.getTransitionCount();
                while (i < transitionCount) {
                    mo9865a((java.lang.Object) transitionSet.getTransitionAt(i), arrayList);
                    i++;
                }
            } else if (!m5769a(transition) && com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.getTargets())) {
                int size = arrayList.size();
                while (i < size) {
                    transition.addTarget(arrayList.get(i));
                    i++;
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9871b(java.lang.Object obj, android.view.View view, java.util.ArrayList<android.view.View> arrayList) {
        android.transition.TransitionSet transitionSet = (android.transition.TransitionSet) obj;
        java.util.List targets = transitionSet.getTargets();
        targets.clear();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            com.fossil.blesdk.obfuscated.C1713eb.m6362a((java.util.List<android.view.View>) targets, arrayList.get(i));
        }
        targets.add(view);
        arrayList.add(view);
        mo9865a((java.lang.Object) transitionSet, arrayList);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9874c(java.lang.Object obj, android.view.View view) {
        if (view != null) {
            android.graphics.Rect rect = new android.graphics.Rect();
            mo10405a(view, rect);
            ((android.transition.Transition) obj).setEpicenterCallback(new com.fossil.blesdk.obfuscated.C1607db.C1608a(this, rect));
        }
    }

    @DexIgnore
    /* renamed from: b */
    public java.lang.Object mo9869b(java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3) {
        android.transition.TransitionSet transitionSet = new android.transition.TransitionSet();
        if (obj != null) {
            transitionSet.addTransition((android.transition.Transition) obj);
        }
        if (obj2 != null) {
            transitionSet.addTransition((android.transition.Transition) obj2);
        }
        if (obj3 != null) {
            transitionSet.addTransition((android.transition.Transition) obj3);
        }
        return transitionSet;
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m5769a(android.transition.Transition transition) {
        return !com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.getTargetIds()) || !com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.getTargetNames()) || !com.fossil.blesdk.obfuscated.C1713eb.m6363a((java.util.List) transition.getTargetTypes());
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9872b(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2) {
        android.transition.TransitionSet transitionSet = (android.transition.TransitionSet) obj;
        if (transitionSet != null) {
            transitionSet.getTargets().clear();
            transitionSet.getTargets().addAll(arrayList2);
            mo9866a((java.lang.Object) transitionSet, arrayList, arrayList2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9863a(java.lang.Object obj, android.view.View view, java.util.ArrayList<android.view.View> arrayList) {
        ((android.transition.Transition) obj).addListener(new com.fossil.blesdk.obfuscated.C1607db.C1609b(this, view, arrayList));
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.Object mo9859a(java.lang.Object obj, java.lang.Object obj2, java.lang.Object obj3) {
        android.transition.Transition transition = (android.transition.Transition) obj;
        android.transition.Transition transition2 = (android.transition.Transition) obj2;
        android.transition.Transition transition3 = (android.transition.Transition) obj3;
        if (transition != null && transition2 != null) {
            transition = new android.transition.TransitionSet().addTransition(transition).addTransition(transition2).setOrdering(1);
        } else if (transition == null) {
            transition = transition2 != null ? transition2 : null;
        }
        if (transition3 == null) {
            return transition;
        }
        android.transition.TransitionSet transitionSet = new android.transition.TransitionSet();
        if (transition != null) {
            transitionSet.addTransition(transition);
        }
        transitionSet.addTransition(transition3);
        return transitionSet;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9870b(java.lang.Object obj, android.view.View view) {
        if (obj != null) {
            ((android.transition.Transition) obj).removeTarget(view);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9860a(android.view.ViewGroup viewGroup, java.lang.Object obj) {
        android.transition.TransitionManager.beginDelayedTransition(viewGroup, (android.transition.Transition) obj);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9864a(java.lang.Object obj, java.lang.Object obj2, java.util.ArrayList<android.view.View> arrayList, java.lang.Object obj3, java.util.ArrayList<android.view.View> arrayList2, java.lang.Object obj4, java.util.ArrayList<android.view.View> arrayList3) {
        com.fossil.blesdk.obfuscated.C1607db.C1610c cVar = new com.fossil.blesdk.obfuscated.C1607db.C1610c(obj2, arrayList, obj3, arrayList2, obj4, arrayList3);
        ((android.transition.Transition) obj).addListener(cVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9866a(java.lang.Object obj, java.util.ArrayList<android.view.View> arrayList, java.util.ArrayList<android.view.View> arrayList2) {
        int i;
        android.transition.Transition transition = (android.transition.Transition) obj;
        int i2 = 0;
        if (transition instanceof android.transition.TransitionSet) {
            android.transition.TransitionSet transitionSet = (android.transition.TransitionSet) transition;
            int transitionCount = transitionSet.getTransitionCount();
            while (i2 < transitionCount) {
                mo9866a((java.lang.Object) transitionSet.getTransitionAt(i2), arrayList, arrayList2);
                i2++;
            }
        } else if (!m5769a(transition)) {
            java.util.List<android.view.View> targets = transition.getTargets();
            if (targets != null && targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
                if (arrayList2 == null) {
                    i = 0;
                } else {
                    i = arrayList2.size();
                }
                while (i2 < i) {
                    transition.addTarget(arrayList2.get(i2));
                    i2++;
                }
                for (int size = arrayList.size() - 1; size >= 0; size--) {
                    transition.removeTarget(arrayList.get(size));
                }
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9862a(java.lang.Object obj, android.view.View view) {
        if (obj != null) {
            ((android.transition.Transition) obj).addTarget(view);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9861a(java.lang.Object obj, android.graphics.Rect rect) {
        if (obj != null) {
            ((android.transition.Transition) obj).setEpicenterCallback(new com.fossil.blesdk.obfuscated.C1607db.C1611d(this, rect));
        }
    }
}
