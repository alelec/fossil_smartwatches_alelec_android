package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final /* synthetic */ class r00 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[DeviceInformation.DeviceInfoItemKey.values().length];

    /*
    static {
        a[DeviceInformation.DeviceInfoItemKey.SERIAL_NUMBER.ordinal()] = 1;
        a[DeviceInformation.DeviceInfoItemKey.HARDWARE_REVISION.ordinal()] = 2;
        a[DeviceInformation.DeviceInfoItemKey.FIRMWARE_VERSION.ordinal()] = 3;
        a[DeviceInformation.DeviceInfoItemKey.MODEL_NUMBER.ordinal()] = 4;
        a[DeviceInformation.DeviceInfoItemKey.HEART_RATE_SERIAL_NUMBER.ordinal()] = 5;
        a[DeviceInformation.DeviceInfoItemKey.BOOTLOADER_VERSION.ordinal()] = 6;
        a[DeviceInformation.DeviceInfoItemKey.WATCH_APP_VERSION.ordinal()] = 7;
        a[DeviceInformation.DeviceInfoItemKey.FONT_VERSION.ordinal()] = 8;
        a[DeviceInformation.DeviceInfoItemKey.LUTS_VERSION.ordinal()] = 9;
        a[DeviceInformation.DeviceInfoItemKey.SUPPORTED_FILES_VERSION.ordinal()] = 10;
        a[DeviceInformation.DeviceInfoItemKey.CURRENT_FILES_VERSION.ordinal()] = 11;
        a[DeviceInformation.DeviceInfoItemKey.BOND_REQUIREMENT.ordinal()] = 12;
        a[DeviceInformation.DeviceInfoItemKey.SUPPORTED_DEVICE_CONFIGS.ordinal()] = 13;
        a[DeviceInformation.DeviceInfoItemKey.DEVICE_SECURITY_VERSION.ordinal()] = 14;
        a[DeviceInformation.DeviceInfoItemKey.SOCKET_INFO.ordinal()] = 15;
        a[DeviceInformation.DeviceInfoItemKey.LOCALE.ordinal()] = 16;
        a[DeviceInformation.DeviceInfoItemKey.LOCALE_VERSION.ordinal()] = 17;
        a[DeviceInformation.DeviceInfoItemKey.MICRO_APP_SYSTEM_VERSION.ordinal()] = 18;
        a[DeviceInformation.DeviceInfoItemKey.UNKNOWN.ordinal()] = 19;
    }
    */
}
