package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nf4 extends mf4 {
    @DexIgnore
    public static final <T> void a(Appendable appendable, T t, xc4<? super T, ? extends CharSequence> xc4) {
        kd4.b(appendable, "$this$appendElement");
        if (xc4 != null) {
            appendable.append((CharSequence) xc4.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append((CharSequence) t);
        } else if (t instanceof Character) {
            appendable.append(((Character) t).charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
