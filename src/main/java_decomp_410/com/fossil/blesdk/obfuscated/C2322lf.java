package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.lf */
public abstract class C2322lf<T> extends com.fossil.blesdk.obfuscated.C3217wf {
    @DexIgnore
    public C2322lf(androidx.room.RoomDatabase roomDatabase) {
        super(roomDatabase);
    }

    @DexIgnore
    public abstract void bind(com.fossil.blesdk.obfuscated.C2221kg kgVar, T t);

    @DexIgnore
    public final void insert(T t) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            bind(acquire, t);
            acquire.mo12782o();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long insertAndReturnId(T t) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.mo12782o();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(java.util.Collection<T> collection) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            long[] jArr = new long[collection.size()];
            int i = 0;
            for (T bind : collection) {
                bind(acquire, bind);
                jArr[i] = acquire.mo12782o();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final java.lang.Long[] insertAndReturnIdsArrayBox(java.util.Collection<T> collection) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            java.lang.Long[] lArr = new java.lang.Long[collection.size()];
            int i = 0;
            for (T bind : collection) {
                bind(acquire, bind);
                lArr[i] = java.lang.Long.valueOf(acquire.mo12782o());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final java.util.List<java.lang.Long> insertAndReturnIdsList(T[] tArr) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(tArr.length);
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                arrayList.add(i, java.lang.Long.valueOf(acquire.mo12782o()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(T[] tArr) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            for (T bind : tArr) {
                bind(acquire, bind);
                acquire.mo12782o();
            }
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final long[] insertAndReturnIdsArray(T[] tArr) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            long[] jArr = new long[tArr.length];
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                jArr[i] = acquire.mo12782o();
                i++;
            }
            return jArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final java.lang.Long[] insertAndReturnIdsArrayBox(T[] tArr) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            java.lang.Long[] lArr = new java.lang.Long[tArr.length];
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                lArr[i] = java.lang.Long.valueOf(acquire.mo12782o());
                i++;
            }
            return lArr;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final java.util.List<java.lang.Long> insertAndReturnIdsList(java.util.Collection<T> collection) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            java.util.ArrayList arrayList = new java.util.ArrayList(collection.size());
            int i = 0;
            for (T bind : collection) {
                bind(acquire, bind);
                arrayList.add(i, java.lang.Long.valueOf(acquire.mo12782o()));
                i++;
            }
            return arrayList;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final void insert(java.lang.Iterable<T> iterable) {
        com.fossil.blesdk.obfuscated.C2221kg acquire = acquire();
        try {
            for (T bind : iterable) {
                bind(acquire, bind);
                acquire.mo12782o();
            }
        } finally {
            release(acquire);
        }
    }
}
