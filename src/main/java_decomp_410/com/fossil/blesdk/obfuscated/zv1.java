package com.fossil.blesdk.obfuscated;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface zv1<V> extends Future<V> {
    @DexIgnore
    void a(Runnable runnable, Executor executor);
}
