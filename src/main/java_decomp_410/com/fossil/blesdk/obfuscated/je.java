package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class je {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ a b; // = new a();
    @DexIgnore
    public /* final */ List<View> c; // = new ArrayList();

    @DexIgnore
    public interface b {
        @DexIgnore
        int a();

        @DexIgnore
        View a(int i);

        @DexIgnore
        void a(View view);

        @DexIgnore
        void a(View view, int i);

        @DexIgnore
        void a(View view, int i, ViewGroup.LayoutParams layoutParams);

        @DexIgnore
        int b(View view);

        @DexIgnore
        void b();

        @DexIgnore
        void b(int i);

        @DexIgnore
        RecyclerView.ViewHolder c(View view);

        @DexIgnore
        void c(int i);

        @DexIgnore
        void d(View view);
    }

    @DexIgnore
    public je(b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public void a(View view, boolean z) {
        a(view, -1, z);
    }

    @DexIgnore
    public final void b(View view) {
        this.c.add(view);
        this.a.a(view);
    }

    @DexIgnore
    public View c(int i) {
        return this.a.a(d(i));
    }

    @DexIgnore
    public final int d(int i) {
        if (i < 0) {
            return -1;
        }
        int a2 = this.a.a();
        int i2 = i;
        while (i2 < a2) {
            int b2 = i - (i2 - this.b.b(i2));
            if (b2 == 0) {
                while (this.b.c(i2)) {
                    i2++;
                }
                return i2;
            }
            i2 += b2;
        }
        return -1;
    }

    @DexIgnore
    public void e(View view) {
        int b2 = this.a.b(view);
        if (b2 >= 0) {
            if (this.b.d(b2)) {
                h(view);
            }
            this.a.c(b2);
        }
    }

    @DexIgnore
    public void f(int i) {
        int d = d(i);
        View a2 = this.a.a(d);
        if (a2 != null) {
            if (this.b.d(d)) {
                h(a2);
            }
            this.a.c(d);
        }
    }

    @DexIgnore
    public void g(View view) {
        int b2 = this.a.b(view);
        if (b2 < 0) {
            throw new IllegalArgumentException("view is not a child, cannot hide " + view);
        } else if (this.b.c(b2)) {
            this.b.a(b2);
            h(view);
        } else {
            throw new RuntimeException("trying to unhide a view that was not hidden" + view);
        }
    }

    @DexIgnore
    public final boolean h(View view) {
        if (!this.c.remove(view)) {
            return false;
        }
        this.a.d(view);
        return true;
    }

    @DexIgnore
    public String toString() {
        return this.b.toString() + ", hidden list:" + this.c.size();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public long a; // = 0;
        @DexIgnore
        public a b;

        @DexIgnore
        public final void a() {
            if (this.b == null) {
                this.b = new a();
            }
        }

        @DexIgnore
        public void b() {
            this.a = 0;
            a aVar = this.b;
            if (aVar != null) {
                aVar.b();
            }
        }

        @DexIgnore
        public boolean c(int i) {
            if (i < 64) {
                return (this.a & (1 << i)) != 0;
            }
            a();
            return this.b.c(i - 64);
        }

        @DexIgnore
        public boolean d(int i) {
            if (i >= 64) {
                a();
                return this.b.d(i - 64);
            }
            long j = 1 << i;
            boolean z = (this.a & j) != 0;
            this.a &= ~j;
            long j2 = j - 1;
            long j3 = this.a;
            this.a = Long.rotateRight(j3 & (~j2), 1) | (j3 & j2);
            a aVar = this.b;
            if (aVar != null) {
                if (aVar.c(0)) {
                    e(63);
                }
                this.b.d(0);
            }
            return z;
        }

        @DexIgnore
        public void e(int i) {
            if (i >= 64) {
                a();
                this.b.e(i - 64);
                return;
            }
            this.a |= 1 << i;
        }

        @DexIgnore
        public String toString() {
            if (this.b == null) {
                return Long.toBinaryString(this.a);
            }
            return this.b.toString() + "xx" + Long.toBinaryString(this.a);
        }

        @DexIgnore
        public void a(int i) {
            if (i >= 64) {
                a aVar = this.b;
                if (aVar != null) {
                    aVar.a(i - 64);
                    return;
                }
                return;
            }
            this.a &= ~(1 << i);
        }

        @DexIgnore
        public int b(int i) {
            a aVar = this.b;
            if (aVar == null) {
                if (i >= 64) {
                    return Long.bitCount(this.a);
                }
                return Long.bitCount(this.a & ((1 << i) - 1));
            } else if (i < 64) {
                return Long.bitCount(this.a & ((1 << i) - 1));
            } else {
                return aVar.b(i - 64) + Long.bitCount(this.a);
            }
        }

        @DexIgnore
        public void a(int i, boolean z) {
            if (i >= 64) {
                a();
                this.b.a(i - 64, z);
                return;
            }
            boolean z2 = (this.a & Long.MIN_VALUE) != 0;
            long j = (1 << i) - 1;
            long j2 = this.a;
            this.a = ((j2 & (~j)) << 1) | (j2 & j);
            if (z) {
                e(i);
            } else {
                a(i);
            }
            if (z2 || this.b != null) {
                a();
                this.b.a(0, z2);
            }
        }
    }

    @DexIgnore
    public void a(View view, int i, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.a.a();
        } else {
            i2 = d(i);
        }
        this.b.a(i2, z);
        if (z) {
            b(view);
        }
        this.a.a(view, i2);
    }

    @DexIgnore
    public View b(int i) {
        int size = this.c.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = this.c.get(i2);
            RecyclerView.ViewHolder c2 = this.a.c(view);
            if (c2.getLayoutPosition() == i && !c2.isInvalid() && !c2.isRemoved()) {
                return view;
            }
        }
        return null;
    }

    @DexIgnore
    public void c() {
        this.b.b();
        for (int size = this.c.size() - 1; size >= 0; size--) {
            this.a.d(this.c.get(size));
            this.c.remove(size);
        }
        this.a.b();
    }

    @DexIgnore
    public boolean d(View view) {
        return this.c.contains(view);
    }

    @DexIgnore
    public View e(int i) {
        return this.a.a(i);
    }

    @DexIgnore
    public boolean f(View view) {
        int b2 = this.a.b(view);
        if (b2 == -1) {
            h(view);
            return true;
        } else if (!this.b.c(b2)) {
            return false;
        } else {
            this.b.d(b2);
            h(view);
            this.a.c(b2);
            return true;
        }
    }

    @DexIgnore
    public void a(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int i2;
        if (i < 0) {
            i2 = this.a.a();
        } else {
            i2 = d(i);
        }
        this.b.a(i2, z);
        if (z) {
            b(view);
        }
        this.a.a(view, i2, layoutParams);
    }

    @DexIgnore
    public int c(View view) {
        int b2 = this.a.b(view);
        if (b2 != -1 && !this.b.c(b2)) {
            return b2 - this.b.b(b2);
        }
        return -1;
    }

    @DexIgnore
    public int b() {
        return this.a.a();
    }

    @DexIgnore
    public int a() {
        return this.a.a() - this.c.size();
    }

    @DexIgnore
    public void a(int i) {
        int d = d(i);
        this.b.d(d);
        this.a.b(d);
    }

    @DexIgnore
    public void a(View view) {
        int b2 = this.a.b(view);
        if (b2 >= 0) {
            this.b.e(b2);
            b(view);
            return;
        }
        throw new IllegalArgumentException("view is not a child, cannot hide " + view);
    }
}
