package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qt3 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public qt3() {
        this(0, 0, 0, 7, (fd4) null);
    }

    @DexIgnore
    public qt3(int i, int i2, int i3) {
        this.a = i;
        this.b = i2;
        this.c = i3;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final void a(int i) {
        this.c = i;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final void b(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final void c(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof qt3) {
                qt3 qt3 = (qt3) obj;
                if (this.a == qt3.a) {
                    if (this.b == qt3.b) {
                        if (this.c == qt3.c) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a * 31) + this.b) * 31) + this.c;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateSleepSessionModel(value=" + this.a + ", time=" + this.b + ", sleepState=" + this.c + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ qt3(int i, int i2, int i3, int i4, fd4 fd4) {
        this((i4 & 1) != 0 ? -1 : i, (i4 & 2) != 0 ? -1 : i2, (i4 & 4) != 0 ? -1 : i3);
    }
}
