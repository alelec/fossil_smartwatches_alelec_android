package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.fossil.wearables.fossil.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.HashMap;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ky2 extends xs3 implements jy2 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a((fd4) null);
    @DexIgnore
    public /* final */ pa m; // = new v62(this);
    @DexIgnore
    public tr3<gf2> n;
    @DexIgnore
    public iy2 o;
    @DexIgnore
    public HashMap p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ky2.q;
        }

        @DexIgnore
        public final ky2 b() {
            return new ky2();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ky2 a;
        @DexIgnore
        public /* final */ /* synthetic */ gf2 b;

        @DexIgnore
        public b(ky2 ky2, gf2 gf2) {
            this.a = ky2;
            this.b = gf2;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            iy2 a2 = ky2.a(this.a);
            NumberPicker numberPicker2 = this.b.s;
            kd4.a((Object) numberPicker2, "binding.numberPicker");
            a2.a(numberPicker2.getValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ky2 e;

        @DexIgnore
        public c(ky2 ky2) {
            this.e = ky2;
        }

        @DexIgnore
        public final void onClick(View view) {
            ky2.a(this.e).h();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ gf2 e;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef f;

        @DexIgnore
        public d(gf2 gf2, Ref$ObjectRef ref$ObjectRef) {
            this.e = gf2;
            this.f = ref$ObjectRef;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.e.q;
            kd4.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.c(3);
                        gf2 gf2 = this.e;
                        kd4.a((Object) gf2, "it");
                        View d = gf2.d();
                        kd4.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.f.element);
                        return;
                    }
                    kd4.a();
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new TypeCastException("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = ky2.class.getSimpleName();
        kd4.a((Object) simpleName, "RemindTimeFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ iy2 a(ky2 ky2) {
        iy2 iy2 = ky2.o;
        if (iy2 != null) {
            return iy2;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        gf2 gf2 = (gf2) qa.a(layoutInflater, R.layout.fragment_remind_time, viewGroup, false, this.m);
        gf2.r.setOnClickListener(new c(this));
        kd4.a((Object) gf2, "binding");
        a(gf2);
        this.n = new tr3<>(this, gf2);
        return gf2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        iy2 iy2 = this.o;
        if (iy2 != null) {
            iy2.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        iy2 iy2 = this.o;
        if (iy2 != null) {
            iy2.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<gf2> tr3 = this.n;
        if (tr3 != null) {
            gf2 a2 = tr3.a();
            if (a2 != null) {
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                ref$ObjectRef.element = null;
                ref$ObjectRef.element = new d(a2, ref$ObjectRef);
                kd4.a((Object) a2, "it");
                View d2 = a2.d();
                kd4.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) ref$ObjectRef.element);
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(iy2 iy2) {
        kd4.b(iy2, "presenter");
        this.o = iy2;
    }

    @DexIgnore
    public final void a(gf2 gf2) {
        ArrayList arrayList = new ArrayList();
        wd4 a2 = ee4.a((wd4) new yd4(20, 120), 20);
        int a3 = a2.a();
        int b2 = a2.b();
        int c2 = a2.c();
        if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
            while (true) {
                String d2 = nl2.d(a3);
                kd4.a((Object) d2, "timeString");
                arrayList.add(d2);
                if (a3 == b2) {
                    break;
                }
                a3 += c2;
            }
        }
        NumberPicker numberPicker = gf2.s;
        kd4.a((Object) numberPicker, "binding.numberPicker");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = gf2.s;
        kd4.a((Object) numberPicker2, "binding.numberPicker");
        numberPicker2.setMaxValue(6);
        NumberPicker numberPicker3 = gf2.s;
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            numberPicker3.setDisplayedValues((String[]) array);
            gf2.s.setOnValueChangedListener(new b(this, gf2));
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void a(int i) {
        tr3<gf2> tr3 = this.n;
        if (tr3 != null) {
            gf2 a2 = tr3.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.s;
                if (numberPicker != null) {
                    numberPicker.setValue(i / 20);
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
