package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.w7 */
public final class C3170w7 {

    @DexIgnore
    /* renamed from: d */
    public static /* final */ com.fossil.blesdk.obfuscated.C3417z7 f10477d; // = com.fossil.blesdk.obfuscated.C1398a8.f3405c;

    @DexIgnore
    /* renamed from: e */
    public static /* final */ java.lang.String f10478e; // = java.lang.Character.toString(8206);

    @DexIgnore
    /* renamed from: f */
    public static /* final */ java.lang.String f10479f; // = java.lang.Character.toString(8207);

    @DexIgnore
    /* renamed from: g */
    public static /* final */ com.fossil.blesdk.obfuscated.C3170w7 f10480g; // = new com.fossil.blesdk.obfuscated.C3170w7(false, 2, f10477d);

    @DexIgnore
    /* renamed from: h */
    public static /* final */ com.fossil.blesdk.obfuscated.C3170w7 f10481h; // = new com.fossil.blesdk.obfuscated.C3170w7(true, 2, f10477d);

    @DexIgnore
    /* renamed from: a */
    public /* final */ boolean f10482a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ int f10483b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C3417z7 f10484c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.w7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.w7$a */
    public static final class C3171a {

        @DexIgnore
        /* renamed from: a */
        public boolean f10485a;

        @DexIgnore
        /* renamed from: b */
        public int f10486b;

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C3417z7 f10487c;

        @DexIgnore
        public C3171a() {
            mo17313a(com.fossil.blesdk.obfuscated.C3170w7.m15595a(java.util.Locale.getDefault()));
        }

        @DexIgnore
        /* renamed from: b */
        public static com.fossil.blesdk.obfuscated.C3170w7 m15604b(boolean z) {
            return z ? com.fossil.blesdk.obfuscated.C3170w7.f10481h : com.fossil.blesdk.obfuscated.C3170w7.f10480g;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo17313a(boolean z) {
            this.f10485a = z;
            this.f10487c = com.fossil.blesdk.obfuscated.C3170w7.f10477d;
            this.f10486b = 2;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C3170w7 mo17312a() {
            if (this.f10486b == 2 && this.f10487c == com.fossil.blesdk.obfuscated.C3170w7.f10477d) {
                return m15604b(this.f10485a);
            }
            return new com.fossil.blesdk.obfuscated.C3170w7(this.f10485a, this.f10486b, this.f10487c);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.w7$b")
    /* renamed from: com.fossil.blesdk.obfuscated.w7$b */
    public static class C3172b {

        @DexIgnore
        /* renamed from: f */
        public static /* final */ byte[] f10488f; // = new byte[1792];

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.CharSequence f10489a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ boolean f10490b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f10491c;

        @DexIgnore
        /* renamed from: d */
        public int f10492d;

        @DexIgnore
        /* renamed from: e */
        public char f10493e;

        /*
        static {
            for (int i = 0; i < 1792; i++) {
                f10488f[i] = java.lang.Character.getDirectionality(i);
            }
        }
        */

        @DexIgnore
        public C3172b(java.lang.CharSequence charSequence, boolean z) {
            this.f10489a = charSequence;
            this.f10490b = z;
            this.f10491c = charSequence.length();
        }

        @DexIgnore
        /* renamed from: a */
        public static byte m15607a(char c) {
            return c < 1792 ? f10488f[c] : java.lang.Character.getDirectionality(c);
        }

        @DexIgnore
        /* renamed from: b */
        public byte mo17315b() {
            this.f10493e = this.f10489a.charAt(this.f10492d);
            if (java.lang.Character.isHighSurrogate(this.f10493e)) {
                int codePointAt = java.lang.Character.codePointAt(this.f10489a, this.f10492d);
                this.f10492d += java.lang.Character.charCount(codePointAt);
                return java.lang.Character.getDirectionality(codePointAt);
            }
            this.f10492d++;
            byte a = m15607a(this.f10493e);
            if (!this.f10490b) {
                return a;
            }
            char c = this.f10493e;
            if (c == '<') {
                return mo17321h();
            }
            return c == '&' ? mo17319f() : a;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17316c() {
            this.f10492d = 0;
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            while (this.f10492d < this.f10491c && i == 0) {
                byte b = mo17315b();
                if (b != 0) {
                    if (b == 1 || b == 2) {
                        if (i3 == 0) {
                            return 1;
                        }
                    } else if (b != 9) {
                        switch (b) {
                            case 14:
                            case 15:
                                i3++;
                                i2 = -1;
                                continue;
                            case 16:
                            case 17:
                                i3++;
                                i2 = 1;
                                continue;
                            case 18:
                                i3--;
                                i2 = 0;
                                continue;
                        }
                    }
                } else if (i3 == 0) {
                    return -1;
                }
                i = i3;
            }
            if (i == 0) {
                return 0;
            }
            if (i2 != 0) {
                return i2;
            }
            while (this.f10492d > 0) {
                switch (mo17314a()) {
                    case 14:
                    case 15:
                        if (i == i3) {
                            return -1;
                        }
                        break;
                    case 16:
                    case 17:
                        if (i == i3) {
                            return 1;
                        }
                        break;
                    case 18:
                        i3++;
                        continue;
                }
                i3--;
            }
            return 0;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x002b, code lost:
            r2 = r2 - 1;
         */
        @DexIgnore
        /* renamed from: d */
        public int mo17317d() {
            this.f10492d = this.f10491c;
            int i = 0;
            int i2 = 0;
            while (this.f10492d > 0) {
                byte a = mo17314a();
                if (a != 0) {
                    if (a == 1 || a == 2) {
                        if (i2 == 0) {
                            return 1;
                        }
                        if (i != 0) {
                        }
                    } else if (a != 9) {
                        switch (a) {
                            case 14:
                            case 15:
                                if (i == i2) {
                                    return -1;
                                }
                                break;
                            case 16:
                            case 17:
                                if (i == i2) {
                                    return 1;
                                }
                                break;
                            case 18:
                                i2++;
                                break;
                            default:
                                if (i != 0) {
                                    break;
                                }
                                break;
                        }
                    } else {
                        continue;
                    }
                } else if (i2 == 0) {
                    return -1;
                } else {
                    if (i != 0) {
                    }
                }
                i = i2;
            }
            return 0;
        }

        @DexIgnore
        /* renamed from: e */
        public final byte mo17318e() {
            char c;
            int i = this.f10492d;
            do {
                int i2 = this.f10492d;
                if (i2 <= 0) {
                    break;
                }
                java.lang.CharSequence charSequence = this.f10489a;
                int i3 = i2 - 1;
                this.f10492d = i3;
                this.f10493e = charSequence.charAt(i3);
                c = this.f10493e;
                if (c == '&') {
                    return 12;
                }
            } while (c != ';');
            this.f10492d = i;
            this.f10493e = ';';
            return org.joda.time.DateTimeFieldType.HALFDAY_OF_DAY;
        }

        @DexIgnore
        /* renamed from: f */
        public final byte mo17319f() {
            char charAt;
            do {
                int i = this.f10492d;
                if (i >= this.f10491c) {
                    return 12;
                }
                java.lang.CharSequence charSequence = this.f10489a;
                this.f10492d = i + 1;
                charAt = charSequence.charAt(i);
                this.f10493e = charAt;
            } while (charAt != ';');
            return 12;
        }

        @DexIgnore
        /* renamed from: g */
        public final byte mo17320g() {
            char charAt;
            int i = this.f10492d;
            while (true) {
                int i2 = this.f10492d;
                if (i2 <= 0) {
                    break;
                }
                java.lang.CharSequence charSequence = this.f10489a;
                int i3 = i2 - 1;
                this.f10492d = i3;
                this.f10493e = charSequence.charAt(i3);
                char c = this.f10493e;
                if (c == '<') {
                    return 12;
                }
                if (c == '>') {
                    break;
                } else if (c == '\"' || c == '\'') {
                    char c2 = this.f10493e;
                    do {
                        int i4 = this.f10492d;
                        if (i4 <= 0) {
                            break;
                        }
                        java.lang.CharSequence charSequence2 = this.f10489a;
                        int i5 = i4 - 1;
                        this.f10492d = i5;
                        charAt = charSequence2.charAt(i5);
                        this.f10493e = charAt;
                    } while (charAt != c2);
                }
            }
            this.f10492d = i;
            this.f10493e = '>';
            return org.joda.time.DateTimeFieldType.HALFDAY_OF_DAY;
        }

        @DexIgnore
        /* renamed from: h */
        public final byte mo17321h() {
            char charAt;
            int i = this.f10492d;
            while (true) {
                int i2 = this.f10492d;
                if (i2 < this.f10491c) {
                    java.lang.CharSequence charSequence = this.f10489a;
                    this.f10492d = i2 + 1;
                    this.f10493e = charSequence.charAt(i2);
                    char c = this.f10493e;
                    if (c == '>') {
                        return 12;
                    }
                    if (c == '\"' || c == '\'') {
                        char c2 = this.f10493e;
                        do {
                            int i3 = this.f10492d;
                            if (i3 >= this.f10491c) {
                                break;
                            }
                            java.lang.CharSequence charSequence2 = this.f10489a;
                            this.f10492d = i3 + 1;
                            charAt = charSequence2.charAt(i3);
                            this.f10493e = charAt;
                        } while (charAt != c2);
                    }
                } else {
                    this.f10492d = i;
                    this.f10493e = '<';
                    return org.joda.time.DateTimeFieldType.HALFDAY_OF_DAY;
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public byte mo17314a() {
            this.f10493e = this.f10489a.charAt(this.f10492d - 1);
            if (java.lang.Character.isLowSurrogate(this.f10493e)) {
                int codePointBefore = java.lang.Character.codePointBefore(this.f10489a, this.f10492d);
                this.f10492d -= java.lang.Character.charCount(codePointBefore);
                return java.lang.Character.getDirectionality(codePointBefore);
            }
            this.f10492d--;
            byte a = m15607a(this.f10493e);
            if (!this.f10490b) {
                return a;
            }
            char c = this.f10493e;
            if (c == '>') {
                return mo17320g();
            }
            return c == ';' ? mo17318e() : a;
        }
    }

    @DexIgnore
    public C3170w7(boolean z, int i, com.fossil.blesdk.obfuscated.C3417z7 z7Var) {
        this.f10482a = z;
        this.f10483b = i;
        this.f10484c = z7Var;
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C3170w7 m15597b() {
        return new com.fossil.blesdk.obfuscated.C3170w7.C3171a().mo17312a();
    }

    @DexIgnore
    /* renamed from: c */
    public static int m15598c(java.lang.CharSequence charSequence) {
        return new com.fossil.blesdk.obfuscated.C3170w7.C3172b(charSequence, false).mo17317d();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17310a() {
        return (this.f10483b & 2) != 0;
    }

    @DexIgnore
    /* renamed from: a */
    public final java.lang.String mo17309a(java.lang.CharSequence charSequence, com.fossil.blesdk.obfuscated.C3417z7 z7Var) {
        boolean a = z7Var.mo8597a(charSequence, 0, charSequence.length());
        if (!this.f10482a && (a || m15598c(charSequence) == 1)) {
            return f10478e;
        }
        if (this.f10482a) {
            return (!a || m15598c(charSequence) == -1) ? f10479f : "";
        }
        return "";
    }

    @DexIgnore
    /* renamed from: b */
    public final java.lang.String mo17311b(java.lang.CharSequence charSequence, com.fossil.blesdk.obfuscated.C3417z7 z7Var) {
        boolean a = z7Var.mo8597a(charSequence, 0, charSequence.length());
        if (!this.f10482a && (a || m15596b(charSequence) == 1)) {
            return f10478e;
        }
        if (this.f10482a) {
            return (!a || m15596b(charSequence) == -1) ? f10479f : "";
        }
        return "";
    }

    @DexIgnore
    /* renamed from: b */
    public static int m15596b(java.lang.CharSequence charSequence) {
        return new com.fossil.blesdk.obfuscated.C3170w7.C3172b(charSequence, false).mo17316c();
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.CharSequence mo17308a(java.lang.CharSequence charSequence, com.fossil.blesdk.obfuscated.C3417z7 z7Var, boolean z) {
        if (charSequence == null) {
            return null;
        }
        boolean a = z7Var.mo8597a(charSequence, 0, charSequence.length());
        android.text.SpannableStringBuilder spannableStringBuilder = new android.text.SpannableStringBuilder();
        if (mo17310a() && z) {
            spannableStringBuilder.append(mo17311b(charSequence, a ? com.fossil.blesdk.obfuscated.C1398a8.f3404b : com.fossil.blesdk.obfuscated.C1398a8.f3403a));
        }
        if (a != this.f10482a) {
            spannableStringBuilder.append(a ? (char) 8235 : 8234);
            spannableStringBuilder.append(charSequence);
            spannableStringBuilder.append(8236);
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (z) {
            spannableStringBuilder.append(mo17309a(charSequence, a ? com.fossil.blesdk.obfuscated.C1398a8.f3404b : com.fossil.blesdk.obfuscated.C1398a8.f3403a));
        }
        return spannableStringBuilder;
    }

    @DexIgnore
    /* renamed from: a */
    public java.lang.CharSequence mo17307a(java.lang.CharSequence charSequence) {
        return mo17308a(charSequence, this.f10484c, true);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m15595a(java.util.Locale locale) {
        return com.fossil.blesdk.obfuscated.C1467b8.m4811b(locale) == 1;
    }
}
