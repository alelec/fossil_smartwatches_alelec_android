package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ad */
public final class C1413ad {

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1474bd f3472a;

    @DexIgnore
    public C1413ad(java.lang.String str, int i, int i2) {
        if (android.os.Build.VERSION.SDK_INT >= 28) {
            this.f3472a = new com.fossil.blesdk.obfuscated.C1549cd(str, i, i2);
        } else {
            this.f3472a = new com.fossil.blesdk.obfuscated.C1614dd(str, i, i2);
        }
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C1413ad)) {
            return false;
        }
        return this.f3472a.equals(((com.fossil.blesdk.obfuscated.C1413ad) obj).f3472a);
    }

    @DexIgnore
    public int hashCode() {
        return this.f3472a.hashCode();
    }
}
