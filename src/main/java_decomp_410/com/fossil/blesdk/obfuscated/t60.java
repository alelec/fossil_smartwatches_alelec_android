package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.data.music.MusicEvent;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.AsyncEventType;
import com.fossil.blesdk.device.logic.request.code.AsyncOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t60 extends g70 {
    @DexIgnore
    public /* final */ MusicEvent A;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t60(MusicEvent musicEvent, Peripheral peripheral) {
        super(RequestId.NOTIFY_MUSIC_EVENT, peripheral);
        kd4.b(musicEvent, "musicEvent");
        kd4.b(peripheral, "peripheral");
        this.A = musicEvent;
    }

    @DexIgnore
    public BluetoothCommand A() {
        return new n10(GattCharacteristic.CharacteristicId.ASYNC, a(this.A), i().h());
    }

    @DexIgnore
    public final byte[] a(MusicEvent musicEvent) {
        ByteBuffer order = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        kd4.a((Object) order, "ByteBuffer.allocate(4).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(AsyncOperationCode.NOTIFY.getId$blesdk_productionRelease());
        order.put(AsyncEventType.MUSIC_EVENT.getId$blesdk_productionRelease());
        order.put(musicEvent.getAction().getId$blesdk_productionRelease());
        order.put(musicEvent.getActionStatus().getId$blesdk_productionRelease());
        byte[] array = order.array();
        kd4.a((Object) array, "musicEventData.array()");
        return array;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(super.t(), JSONKey.MUSIC_EVENT, this.A.toJSONObject());
    }
}
