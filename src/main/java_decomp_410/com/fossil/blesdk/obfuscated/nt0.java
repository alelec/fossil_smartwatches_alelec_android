package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nt0 {
    @DexIgnore
    public static /* final */ Class<?> a; // = a("libcore.io.Memory");
    @DexIgnore
    public static /* final */ boolean b; // = (a("org.robolectric.Robolectric") != null);

    @DexIgnore
    public static <T> Class<T> a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a() {
        return a != null && !b;
    }

    @DexIgnore
    public static Class<?> b() {
        return a;
    }
}
