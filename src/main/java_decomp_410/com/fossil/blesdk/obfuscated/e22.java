package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e22 {
    @DexIgnore
    public /* final */ c22 a;
    @DexIgnore
    public /* final */ List<d22> b; // = new ArrayList();

    @DexIgnore
    public e22(c22 c22) {
        this.a = c22;
        this.b.add(new d22(c22, new int[]{1}));
    }

    @DexIgnore
    public final d22 a(int i) {
        if (i >= this.b.size()) {
            List<d22> list = this.b;
            d22 d22 = list.get(list.size() - 1);
            for (int size = this.b.size(); size <= i; size++) {
                c22 c22 = this.a;
                d22 = d22.c(new d22(c22, new int[]{1, c22.a((size - 1) + c22.a())}));
                this.b.add(d22);
            }
        }
        return this.b.get(i);
    }

    @DexIgnore
    public void a(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                d22 a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] a3 = new d22(this.a, iArr2).a(i, 1).b(a2)[1].a();
                int length2 = i - a3.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(a3, 0, iArr, length + length2, a3.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
