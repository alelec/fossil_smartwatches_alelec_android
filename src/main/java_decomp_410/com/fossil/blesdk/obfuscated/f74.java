package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class f74 implements e74 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public f74(v44 v44) {
        if (v44.l() != null) {
            this.a = v44.l();
            v44.q();
            "Android/" + this.a.getPackageName();
            return;
        }
        throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
    }

    @DexIgnore
    public File a() {
        return a(this.a.getFilesDir());
    }

    @DexIgnore
    public File a(File file) {
        if (file == null) {
            q44.g().d("Fabric", "Null File");
            return null;
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            q44.g().w("Fabric", "Couldn't create file");
            return null;
        }
    }
}
