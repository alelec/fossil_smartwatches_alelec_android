package com.fossil.blesdk.obfuscated;

import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface qz1 {
    @DexIgnore
    String translateName(Field field);
}
