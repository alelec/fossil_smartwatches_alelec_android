package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import java.io.IOException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ox0 {
    @DexIgnore
    int a();

    @DexIgnore
    @Deprecated
    void a(int i) throws IOException;

    @DexIgnore
    void a(int i, int i2) throws IOException;

    @DexIgnore
    void a(int i, long j) throws IOException;

    @DexIgnore
    void a(int i, zzbb zzbb) throws IOException;

    @DexIgnore
    void a(int i, Object obj, jw0 jw0) throws IOException;

    @DexIgnore
    @Deprecated
    void a(int i, List<?> list, jw0 jw0) throws IOException;

    @DexIgnore
    void a(int i, boolean z) throws IOException;

    @DexIgnore
    @Deprecated
    void b(int i) throws IOException;

    @DexIgnore
    void b(int i, int i2) throws IOException;

    @DexIgnore
    void b(int i, long j) throws IOException;

    @DexIgnore
    @Deprecated
    void b(int i, Object obj, jw0 jw0) throws IOException;

    @DexIgnore
    void b(int i, List<?> list, jw0 jw0) throws IOException;

    @DexIgnore
    void zza(int i, double d) throws IOException;

    @DexIgnore
    void zza(int i, float f) throws IOException;

    @DexIgnore
    void zza(int i, long j) throws IOException;

    @DexIgnore
    void zza(int i, Object obj) throws IOException;

    @DexIgnore
    void zza(int i, String str) throws IOException;

    @DexIgnore
    void zza(int i, List<String> list) throws IOException;

    @DexIgnore
    void zza(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzb(int i, long j) throws IOException;

    @DexIgnore
    void zzb(int i, List<zzbb> list) throws IOException;

    @DexIgnore
    void zzb(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzc(int i, int i2) throws IOException;

    @DexIgnore
    void zzc(int i, long j) throws IOException;

    @DexIgnore
    void zzc(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zzd(int i, int i2) throws IOException;

    @DexIgnore
    void zzd(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zze(int i, int i2) throws IOException;

    @DexIgnore
    void zze(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zzf(int i, int i2) throws IOException;

    @DexIgnore
    void zzf(int i, List<Float> list, boolean z) throws IOException;

    @DexIgnore
    void zzg(int i, List<Double> list, boolean z) throws IOException;

    @DexIgnore
    void zzh(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzi(int i, List<Boolean> list, boolean z) throws IOException;

    @DexIgnore
    void zzj(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzk(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzl(int i, List<Long> list, boolean z) throws IOException;

    @DexIgnore
    void zzm(int i, List<Integer> list, boolean z) throws IOException;

    @DexIgnore
    void zzn(int i, List<Long> list, boolean z) throws IOException;
}
