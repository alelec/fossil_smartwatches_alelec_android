package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.dm4;
import com.fossil.blesdk.obfuscated.yl4;
import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xm4 implements Interceptor {
    @DexIgnore
    public /* final */ rl4 a;

    @DexIgnore
    public xm4(rl4 rl4) {
        this.a = rl4;
    }

    @DexIgnore
    public final String a(List<ql4> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            ql4 ql4 = list.get(i);
            sb.append(ql4.a());
            sb.append('=');
            sb.append(ql4.b());
        }
        return sb.toString();
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        dm4 n = chain.n();
        dm4.a f = n.f();
        RequestBody a2 = n.a();
        if (a2 != null) {
            am4 b = a2.b();
            if (b != null) {
                f.b(GraphRequest.CONTENT_TYPE_HEADER, b.toString());
            }
            long a3 = a2.a();
            if (a3 != -1) {
                f.b("Content-Length", Long.toString(a3));
                f.a("Transfer-Encoding");
            } else {
                f.b("Transfer-Encoding", "chunked");
                f.a("Content-Length");
            }
        }
        boolean z = false;
        if (n.a("Host") == null) {
            f.b("Host", jm4.a(n.g(), false));
        }
        if (n.a("Connection") == null) {
            f.b("Connection", "Keep-Alive");
        }
        if (n.a("Accept-Encoding") == null && n.a("Range") == null) {
            z = true;
            f.b("Accept-Encoding", "gzip");
        }
        List<ql4> a4 = this.a.a(n.g());
        if (!a4.isEmpty()) {
            f.b("Cookie", a(a4));
        }
        if (n.a("User-Agent") == null) {
            f.b("User-Agent", km4.a());
        }
        Response a5 = chain.a(f.a());
        bn4.a(this.a, n.g(), a5.D());
        Response.a H = a5.H();
        H.a(n);
        if (z && "gzip".equalsIgnoreCase(a5.e(GraphRequest.CONTENT_ENCODING_HEADER)) && bn4.b(a5)) {
            qo4 qo4 = new qo4(a5.y().E());
            yl4.a a6 = a5.D().a();
            a6.c(GraphRequest.CONTENT_ENCODING_HEADER);
            a6.c("Content-Length");
            H.a(a6.a());
            H.a((em4) new en4(a5.e(GraphRequest.CONTENT_TYPE_HEADER), -1, so4.a((yo4) qo4)));
        }
        return H.a();
    }
}
