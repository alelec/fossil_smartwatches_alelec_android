package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r2 */
public class C2781r2 {

    @DexIgnore
    /* renamed from: a */
    public int f8821a; // = 0;

    @DexIgnore
    /* renamed from: b */
    public int f8822b; // = 0;

    @DexIgnore
    /* renamed from: c */
    public int f8823c; // = Integer.MIN_VALUE;

    @DexIgnore
    /* renamed from: d */
    public int f8824d; // = Integer.MIN_VALUE;

    @DexIgnore
    /* renamed from: e */
    public int f8825e; // = 0;

    @DexIgnore
    /* renamed from: f */
    public int f8826f; // = 0;

    @DexIgnore
    /* renamed from: g */
    public boolean f8827g; // = false;

    @DexIgnore
    /* renamed from: h */
    public boolean f8828h; // = false;

    @DexIgnore
    /* renamed from: a */
    public int mo15430a() {
        return this.f8827g ? this.f8821a : this.f8822b;
    }

    @DexIgnore
    /* renamed from: b */
    public int mo15433b() {
        return this.f8821a;
    }

    @DexIgnore
    /* renamed from: c */
    public int mo15435c() {
        return this.f8822b;
    }

    @DexIgnore
    /* renamed from: d */
    public int mo15436d() {
        return this.f8827g ? this.f8822b : this.f8821a;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15431a(int i, int i2) {
        this.f8828h = false;
        if (i != Integer.MIN_VALUE) {
            this.f8825e = i;
            this.f8821a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f8826f = i2;
            this.f8822b = i2;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo15434b(int i, int i2) {
        this.f8823c = i;
        this.f8824d = i2;
        this.f8828h = true;
        if (this.f8827g) {
            if (i2 != Integer.MIN_VALUE) {
                this.f8821a = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.f8822b = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.f8821a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f8822b = i2;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void mo15432a(boolean z) {
        if (z != this.f8827g) {
            this.f8827g = z;
            if (!this.f8828h) {
                this.f8821a = this.f8825e;
                this.f8822b = this.f8826f;
            } else if (z) {
                int i = this.f8824d;
                if (i == Integer.MIN_VALUE) {
                    i = this.f8825e;
                }
                this.f8821a = i;
                int i2 = this.f8823c;
                if (i2 == Integer.MIN_VALUE) {
                    i2 = this.f8826f;
                }
                this.f8822b = i2;
            } else {
                int i3 = this.f8823c;
                if (i3 == Integer.MIN_VALUE) {
                    i3 = this.f8825e;
                }
                this.f8821a = i3;
                int i4 = this.f8824d;
                if (i4 == Integer.MIN_VALUE) {
                    i4 = this.f8826f;
                }
                this.f8822b = i4;
            }
        }
    }
}
