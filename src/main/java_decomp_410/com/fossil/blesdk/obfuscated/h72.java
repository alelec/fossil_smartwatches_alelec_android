package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h72 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new b().getType();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends GFitWODistance>> {
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public final String a(List<GFitWODistance> list) {
        kd4.b(list, "distances");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = this.a.a((Object) list, this.b);
            kd4.a((Object) a2, "mGson.toJson(distances, mType)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("GFitWODistancesConverter", sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final List<GFitWODistance> a(String str) {
        kd4.b(str, "data");
        if (str.length() == 0) {
            return cb4.a();
        }
        try {
            Object a2 = this.a.a(str, this.b);
            kd4.a(a2, "mGson.fromJson(data, mType)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListGFitWODistance: ");
            e.printStackTrace();
            sb.append(qa4.a);
            local.e("GFitWODistancesConverter", sb.toString());
            return cb4.a();
        }
    }
}
