package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class hc3 implements Factory<HeartRateOverviewDayPresenter> {
    @DexIgnore
    public static HeartRateOverviewDayPresenter a(fc3 fc3, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        return new HeartRateOverviewDayPresenter(fc3, heartRateSampleRepository, workoutSessionRepository);
    }
}
