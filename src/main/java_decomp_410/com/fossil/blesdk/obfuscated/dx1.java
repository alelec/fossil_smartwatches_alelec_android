package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dx1 {
    @DexIgnore
    wn1<Void> a(String str);

    @DexIgnore
    wn1<Void> a(String str, String str2, String str3);

    @DexIgnore
    wn1<String> a(String str, String str2, String str3, String str4);

    @DexIgnore
    boolean a();

    @DexIgnore
    wn1<Void> b(String str, String str2, String str3);

    @DexIgnore
    boolean b();
}
