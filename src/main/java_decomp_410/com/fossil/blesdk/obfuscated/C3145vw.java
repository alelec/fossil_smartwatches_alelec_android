package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vw */
public final class C3145vw {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C3145vw.C3152g<java.lang.Object> f10415a; // = new com.fossil.blesdk.obfuscated.C3145vw.C3146a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vw$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vw$a */
    public class C3146a implements com.fossil.blesdk.obfuscated.C3145vw.C3152g<java.lang.Object> {
        @DexIgnore
        /* renamed from: a */
        public void mo17220a(java.lang.Object obj) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vw$b")
    /* renamed from: com.fossil.blesdk.obfuscated.vw$b */
    public class C3147b implements com.fossil.blesdk.obfuscated.C3145vw.C3149d<java.util.List<T>> {
        @DexIgnore
        /* renamed from: a */
        public java.util.List<T> m15498a() {
            return new java.util.ArrayList();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vw$c")
    /* renamed from: com.fossil.blesdk.obfuscated.vw$c */
    public class C3148c implements com.fossil.blesdk.obfuscated.C3145vw.C3152g<java.util.List<T>> {
        @DexIgnore
        /* renamed from: a */
        public void mo17220a(java.util.List<T> list) {
            list.clear();
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.vw$d */
    public interface C3149d<T> {
        @DexIgnore
        /* renamed from: a */
        T mo9619a();
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vw$e")
    /* renamed from: com.fossil.blesdk.obfuscated.vw$e */
    public static final class C3150e<T> implements com.fossil.blesdk.obfuscated.C1862g8<T> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C3145vw.C3149d<T> f10416a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ com.fossil.blesdk.obfuscated.C3145vw.C3152g<T> f10417b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ com.fossil.blesdk.obfuscated.C1862g8<T> f10418c;

        @DexIgnore
        public C3150e(com.fossil.blesdk.obfuscated.C1862g8<T> g8Var, com.fossil.blesdk.obfuscated.C3145vw.C3149d<T> dVar, com.fossil.blesdk.obfuscated.C3145vw.C3152g<T> gVar) {
            this.f10418c = g8Var;
            this.f10416a = dVar;
            this.f10417b = gVar;
        }

        @DexIgnore
        /* renamed from: a */
        public T mo11162a() {
            T a = this.f10418c.mo11162a();
            if (a == null) {
                a = this.f10416a.mo9619a();
                if (android.util.Log.isLoggable("FactoryPools", 2)) {
                    android.util.Log.v("FactoryPools", "Created new " + a.getClass());
                }
            }
            if (a instanceof com.fossil.blesdk.obfuscated.C3145vw.C3151f) {
                ((com.fossil.blesdk.obfuscated.C3145vw.C3151f) a).mo3959i().mo17920a(false);
            }
            return a;
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo11163a(T t) {
            if (t instanceof com.fossil.blesdk.obfuscated.C3145vw.C3151f) {
                ((com.fossil.blesdk.obfuscated.C3145vw.C3151f) t).mo3959i().mo17920a(true);
            }
            this.f10417b.mo17220a(t);
            return this.f10418c.mo11163a(t);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.vw$f */
    public interface C3151f {
        @DexIgnore
        /* renamed from: i */
        com.fossil.blesdk.obfuscated.C3324xw mo3959i();
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.vw$g */
    public interface C3152g<T> {
        @DexIgnore
        /* renamed from: a */
        void mo17220a(T t);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends com.fossil.blesdk.obfuscated.C3145vw.C3151f> com.fossil.blesdk.obfuscated.C1862g8<T> m15491a(int i, com.fossil.blesdk.obfuscated.C3145vw.C3149d<T> dVar) {
        return m15492a(new com.fossil.blesdk.obfuscated.C2002i8(i), dVar);
    }

    @DexIgnore
    /* renamed from: b */
    public static <T> com.fossil.blesdk.obfuscated.C1862g8<java.util.List<T>> m15495b() {
        return m15490a(20);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C1862g8<java.util.List<T>> m15490a(int i) {
        return m15493a(new com.fossil.blesdk.obfuscated.C2002i8(i), new com.fossil.blesdk.obfuscated.C3145vw.C3147b(), new com.fossil.blesdk.obfuscated.C3145vw.C3148c());
    }

    @DexIgnore
    /* renamed from: a */
    public static <T extends com.fossil.blesdk.obfuscated.C3145vw.C3151f> com.fossil.blesdk.obfuscated.C1862g8<T> m15492a(com.fossil.blesdk.obfuscated.C1862g8<T> g8Var, com.fossil.blesdk.obfuscated.C3145vw.C3149d<T> dVar) {
        return m15493a(g8Var, dVar, m15494a());
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C1862g8<T> m15493a(com.fossil.blesdk.obfuscated.C1862g8<T> g8Var, com.fossil.blesdk.obfuscated.C3145vw.C3149d<T> dVar, com.fossil.blesdk.obfuscated.C3145vw.C3152g<T> gVar) {
        return new com.fossil.blesdk.obfuscated.C3145vw.C3150e(g8Var, dVar, gVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static <T> com.fossil.blesdk.obfuscated.C3145vw.C3152g<T> m15494a() {
        return f10415a;
    }
}
