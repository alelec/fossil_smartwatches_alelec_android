package com.fossil.blesdk.obfuscated;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CompoundButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b2 {
    @DexIgnore
    public /* final */ CompoundButton a;
    @DexIgnore
    public ColorStateList b; // = null;
    @DexIgnore
    public PorterDuff.Mode c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public b2(CompoundButton compoundButton) {
        this.a = compoundButton;
    }

    @DexIgnore
    public void a(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = this.a.getContext().obtainStyledAttributes(attributeSet, a0.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(a0.CompoundButton_android_button)) {
                int resourceId = obtainStyledAttributes.getResourceId(a0.CompoundButton_android_button, 0);
                if (resourceId != 0) {
                    this.a.setButtonDrawable(m0.c(this.a.getContext(), resourceId));
                }
            }
            if (obtainStyledAttributes.hasValue(a0.CompoundButton_buttonTint)) {
                v9.a(this.a, obtainStyledAttributes.getColorStateList(a0.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(a0.CompoundButton_buttonTintMode)) {
                v9.a(this.a, k2.a(obtainStyledAttributes.getInt(a0.CompoundButton_buttonTintMode, -1), (PorterDuff.Mode) null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public ColorStateList b() {
        return this.b;
    }

    @DexIgnore
    public PorterDuff.Mode c() {
        return this.c;
    }

    @DexIgnore
    public void d() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        a();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.b = colorStateList;
        this.d = true;
        a();
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        this.c = mode;
        this.e = true;
        a();
    }

    @DexIgnore
    public void a() {
        Drawable a2 = v9.a(this.a);
        if (a2 == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = c7.i(a2).mutate();
            if (this.d) {
                c7.a(mutate, this.b);
            }
            if (this.e) {
                c7.a(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.a.getDrawableState());
            }
            this.a.setButtonDrawable(mutate);
        }
    }

    @DexIgnore
    public int a(int i) {
        if (Build.VERSION.SDK_INT >= 17) {
            return i;
        }
        Drawable a2 = v9.a(this.a);
        return a2 != null ? i + a2.getIntrinsicWidth() : i;
    }
}
