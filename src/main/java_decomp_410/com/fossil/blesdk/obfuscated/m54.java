package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class m54 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends m54 {
        @DexIgnore
        public a(String str, String str2) {
            super(str, str2);
        }
    }

    @DexIgnore
    public m54(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public String b() {
        return this.a;
    }
}
