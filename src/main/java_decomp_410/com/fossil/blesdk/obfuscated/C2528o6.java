package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.o6 */
public class C2528o6 {

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.o6$a */
    public interface C2529a {
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o6$b")
    /* renamed from: com.fossil.blesdk.obfuscated.o6$b */
    public static final class C2530b implements com.fossil.blesdk.obfuscated.C2528o6.C2529a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2528o6.C2531c[] f7988a;

        @DexIgnore
        public C2530b(com.fossil.blesdk.obfuscated.C2528o6.C2531c[] cVarArr) {
            this.f7988a = cVarArr;
        }

        @DexIgnore
        /* renamed from: a */
        public com.fossil.blesdk.obfuscated.C2528o6.C2531c[] mo14259a() {
            return this.f7988a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o6$c")
    /* renamed from: com.fossil.blesdk.obfuscated.o6$c */
    public static final class C2531c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.lang.String f7989a;

        @DexIgnore
        /* renamed from: b */
        public int f7990b;

        @DexIgnore
        /* renamed from: c */
        public boolean f7991c;

        @DexIgnore
        /* renamed from: d */
        public java.lang.String f7992d;

        @DexIgnore
        /* renamed from: e */
        public int f7993e;

        @DexIgnore
        /* renamed from: f */
        public int f7994f;

        @DexIgnore
        public C2531c(java.lang.String str, int i, boolean z, java.lang.String str2, int i2, int i3) {
            this.f7989a = str;
            this.f7990b = i;
            this.f7991c = z;
            this.f7992d = str2;
            this.f7993e = i2;
            this.f7994f = i3;
        }

        @DexIgnore
        /* renamed from: a */
        public java.lang.String mo14260a() {
            return this.f7989a;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo14261b() {
            return this.f7994f;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo14262c() {
            return this.f7993e;
        }

        @DexIgnore
        /* renamed from: d */
        public java.lang.String mo14263d() {
            return this.f7992d;
        }

        @DexIgnore
        /* renamed from: e */
        public int mo14264e() {
            return this.f7990b;
        }

        @DexIgnore
        /* renamed from: f */
        public boolean mo14265f() {
            return this.f7991c;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.o6$d")
    /* renamed from: com.fossil.blesdk.obfuscated.o6$d */
    public static final class C2532d implements com.fossil.blesdk.obfuscated.C2528o6.C2529a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ com.fossil.blesdk.obfuscated.C2944t7 f7995a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int f7996b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f7997c;

        @DexIgnore
        public C2532d(com.fossil.blesdk.obfuscated.C2944t7 t7Var, int i, int i2) {
            this.f7995a = t7Var;
            this.f7997c = i;
            this.f7996b = i2;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo14266a() {
            return this.f7997c;
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2944t7 mo14267b() {
            return this.f7995a;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo14268c() {
            return this.f7996b;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2528o6.C2529a m11602a(org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources resources) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int next;
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return m11606b(xmlPullParser, resources);
        }
        throw new org.xmlpull.v1.XmlPullParserException("No start tag found");
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C2528o6.C2529a m11606b(org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources resources) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        xmlPullParser.require(2, (java.lang.String) null, "font-family");
        if (xmlPullParser.getName().equals("font-family")) {
            return m11607c(xmlPullParser, resources);
        }
        m11605a(xmlPullParser);
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public static com.fossil.blesdk.obfuscated.C2528o6.C2529a m11607c(org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources resources) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.content.res.TypedArray obtainAttributes = resources.obtainAttributes(android.util.Xml.asAttributeSet(xmlPullParser), com.fossil.blesdk.obfuscated.C3008u5.FontFamily);
        java.lang.String string = obtainAttributes.getString(com.fossil.blesdk.obfuscated.C3008u5.FontFamily_fontProviderAuthority);
        java.lang.String string2 = obtainAttributes.getString(com.fossil.blesdk.obfuscated.C3008u5.FontFamily_fontProviderPackage);
        java.lang.String string3 = obtainAttributes.getString(com.fossil.blesdk.obfuscated.C3008u5.FontFamily_fontProviderQuery);
        int resourceId = obtainAttributes.getResourceId(com.fossil.blesdk.obfuscated.C3008u5.FontFamily_fontProviderCerts, 0);
        int integer = obtainAttributes.getInteger(com.fossil.blesdk.obfuscated.C3008u5.FontFamily_fontProviderFetchStrategy, 1);
        int integer2 = obtainAttributes.getInteger(com.fossil.blesdk.obfuscated.C3008u5.FontFamily_fontProviderFetchTimeout, 500);
        obtainAttributes.recycle();
        if (string == null || string2 == null || string3 == null) {
            java.util.ArrayList arrayList = new java.util.ArrayList();
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2) {
                    if (xmlPullParser.getName().equals("font")) {
                        arrayList.add(m11608d(xmlPullParser, resources));
                    } else {
                        m11605a(xmlPullParser);
                    }
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            return new com.fossil.blesdk.obfuscated.C2528o6.C2530b((com.fossil.blesdk.obfuscated.C2528o6.C2531c[]) arrayList.toArray(new com.fossil.blesdk.obfuscated.C2528o6.C2531c[arrayList.size()]));
        }
        while (xmlPullParser.next() != 3) {
            m11605a(xmlPullParser);
        }
        return new com.fossil.blesdk.obfuscated.C2528o6.C2532d(new com.fossil.blesdk.obfuscated.C2944t7(string, string2, string3, m11603a(resources, resourceId)), integer, integer2);
    }

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C2528o6.C2531c m11608d(org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources resources) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.content.res.TypedArray obtainAttributes = resources.obtainAttributes(android.util.Xml.asAttributeSet(xmlPullParser), com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont);
        int i = obtainAttributes.getInt(obtainAttributes.hasValue(com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_fontWeight) ? com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_fontWeight : com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_android_fontWeight, com.misfit.frameworks.common.constants.MFNetworkReturnCode.BAD_REQUEST);
        boolean z = 1 == obtainAttributes.getInt(obtainAttributes.hasValue(com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_fontStyle) ? com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_fontStyle : com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_android_fontStyle, 0);
        int i2 = obtainAttributes.hasValue(com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_ttcIndex) ? com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_ttcIndex : com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_android_ttcIndex;
        java.lang.String string = obtainAttributes.getString(obtainAttributes.hasValue(com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_fontVariationSettings) ? com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_fontVariationSettings : com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_android_fontVariationSettings);
        int i3 = obtainAttributes.getInt(i2, 0);
        int i4 = obtainAttributes.hasValue(com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_font) ? com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_font : com.fossil.blesdk.obfuscated.C3008u5.FontFamilyFont_android_font;
        int resourceId = obtainAttributes.getResourceId(i4, 0);
        java.lang.String string2 = obtainAttributes.getString(i4);
        obtainAttributes.recycle();
        while (xmlPullParser.next() != 3) {
            m11605a(xmlPullParser);
        }
        com.fossil.blesdk.obfuscated.C2528o6.C2531c cVar = new com.fossil.blesdk.obfuscated.C2528o6.C2531c(string2, i, z, string, i3, resourceId);
        return cVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m11601a(android.content.res.TypedArray typedArray, int i) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return typedArray.getType(i);
        }
        android.util.TypedValue typedValue = new android.util.TypedValue();
        typedArray.getValue(i, typedValue);
        return typedValue.type;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<java.util.List<byte[]>> m11603a(android.content.res.Resources resources, int i) {
        if (i == 0) {
            return java.util.Collections.emptyList();
        }
        android.content.res.TypedArray obtainTypedArray = resources.obtainTypedArray(i);
        try {
            if (obtainTypedArray.length() == 0) {
                return java.util.Collections.emptyList();
            }
            java.util.ArrayList arrayList = new java.util.ArrayList();
            if (m11601a(obtainTypedArray, 0) == 1) {
                for (int i2 = 0; i2 < obtainTypedArray.length(); i2++) {
                    int resourceId = obtainTypedArray.getResourceId(i2, 0);
                    if (resourceId != 0) {
                        arrayList.add(m11604a(resources.getStringArray(resourceId)));
                    }
                }
            } else {
                arrayList.add(m11604a(resources.getStringArray(i)));
            }
            obtainTypedArray.recycle();
            return arrayList;
        } finally {
            obtainTypedArray.recycle();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.util.List<byte[]> m11604a(java.lang.String[] strArr) {
        java.util.ArrayList arrayList = new java.util.ArrayList();
        for (java.lang.String decode : strArr) {
            arrayList.add(android.util.Base64.decode(decode, 0));
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m11605a(org.xmlpull.v1.XmlPullParser xmlPullParser) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int i = 1;
        while (i > 0) {
            int next = xmlPullParser.next();
            if (next == 2) {
                i++;
            } else if (next == 3) {
                i--;
            }
        }
    }
}
