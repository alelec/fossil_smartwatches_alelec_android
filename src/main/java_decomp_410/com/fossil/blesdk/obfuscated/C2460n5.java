package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.n5 */
public final class C2460n5 {
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131886606;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131886607;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131886609;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131886612;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131886614;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131886802;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131886803;
    @DexIgnore
    public static /* final */ int Widget_Support_CoordinatorLayout; // = 2131886850;
}
