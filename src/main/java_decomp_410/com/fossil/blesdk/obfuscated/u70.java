package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.logic.data.connectionparameter.ConnectionParameters;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.DeviceConfigOperationCode;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class u70 extends p70 {
    @DexIgnore
    public ConnectionParameters L; // = new ConnectionParameters(0, 0, 0);

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u70(Peripheral peripheral) {
        super(DeviceConfigOperationCode.GET_CONNECTION_PARAMETERS, RequestId.GET_CONNECTION_PARAMS, peripheral, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public final ConnectionParameters I() {
        return this.L;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a = super.a(bArr);
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        this.L = new ConnectionParameters(n90.b(order.getShort(0)), n90.b(order.getShort(2)), n90.b(order.getShort(4)));
        return m90.a(a, this.L.toJSONObject());
    }

    @DexIgnore
    public JSONObject u() {
        return m90.a(super.u(), this.L.toJSONObject());
    }
}
