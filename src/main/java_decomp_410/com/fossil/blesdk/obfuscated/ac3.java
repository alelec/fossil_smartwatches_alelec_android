package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import java.util.Date;
import java.util.HashMap;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ac3 extends zr2 implements zb3, nt2, ks2 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<cb2> j;
    @DexIgnore
    public yb3 k;
    @DexIgnore
    public mt2 l;
    @DexIgnore
    public HeartRateOverviewFragment m;
    @DexIgnore
    public bu3 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ac3.p;
        }

        @DexIgnore
        public final ac3 b() {
            return new ac3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends bu3 {
        @DexIgnore
        public /* final */ /* synthetic */ ac3 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ac3 ac3, LinearLayoutManager linearLayoutManager, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager2);
            this.e = ac3;
        }

        @DexIgnore
        public void a(int i) {
            ac3.a(this.e).j();
        }

        @DexIgnore
        public void a(int i, int i2) {
        }
    }

    /*
    static {
        String simpleName = ac3.class.getSimpleName();
        if (simpleName != null) {
            kd4.a((Object) simpleName, "DashboardHeartRateFragme\u2026::class.java.simpleName!!");
            p = simpleName;
            return;
        }
        kd4.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ yb3 a(ac3 ac3) {
        yb3 yb3 = ac3.k;
        if (yb3 != null) {
            return yb3;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N(boolean z) {
        if (isVisible()) {
            tr3<cb2> tr3 = this.j;
            if (tr3 != null) {
                RecyclerView recyclerView = null;
                if (tr3 != null) {
                    cb2 a2 = tr3.a();
                    if (a2 != null) {
                        recyclerView = a2.q;
                    }
                    if (recyclerView != null) {
                        RecyclerView.ViewHolder c = recyclerView.c(0);
                        if (c != null) {
                            View view = c.itemView;
                            if (view != null && view.getY() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                return;
                            }
                        }
                    }
                    if (recyclerView != null) {
                        recyclerView.j(0);
                    }
                    bu3 bu3 = this.n;
                    if (bu3 != null) {
                        bu3.a();
                        return;
                    }
                    return;
                }
                kd4.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(Date date) {
        kd4.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            HeartRateDetailActivity.a aVar = HeartRateDetailActivity.D;
            kd4.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void f() {
        bu3 bu3 = this.n;
        if (bu3 != null) {
            bu3.a();
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        cb2 cb2 = (cb2) qa.a(layoutInflater, R.layout.fragment_dashboard_heartrate, viewGroup, false, O0());
        this.m = (HeartRateOverviewFragment) getChildFragmentManager().a("HeartRateOverviewFragment");
        if (this.m == null) {
            this.m = new HeartRateOverviewFragment();
        }
        lt2 lt2 = new lt2();
        PortfolioApp c = PortfolioApp.W.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        kd4.a((Object) childFragmentManager, "childFragmentManager");
        HeartRateOverviewFragment heartRateOverviewFragment = this.m;
        if (heartRateOverviewFragment != null) {
            this.l = new mt2(lt2, c, this, childFragmentManager, heartRateOverviewFragment);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            this.n = new b(this, linearLayoutManager, linearLayoutManager);
            l73 l73 = new l73(linearLayoutManager.M());
            Context context = getContext();
            if (context != null) {
                Drawable c2 = k6.c(context, R.drawable.bg_item_decoration_dashboard_line_1w);
                if (c2 != null) {
                    kd4.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                    l73.a(c2);
                    RecyclerView recyclerView = cb2.q;
                    kd4.a((Object) recyclerView, "this");
                    recyclerView.setLayoutManager(linearLayoutManager);
                    mt2 mt2 = this.l;
                    if (mt2 != null) {
                        recyclerView.setAdapter(mt2);
                        bu3 bu3 = this.n;
                        if (bu3 != null) {
                            recyclerView.a((RecyclerView.q) bu3);
                            recyclerView.setItemViewCacheSize(0);
                            recyclerView.a((RecyclerView.l) l73);
                            if (recyclerView.getItemAnimator() instanceof af) {
                                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                                if (itemAnimator != null) {
                                    ((af) itemAnimator).setSupportsChangeAnimations(false);
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type androidx.recyclerview.widget.SimpleItemAnimator");
                                }
                            }
                            yb3 yb3 = this.k;
                            if (yb3 != null) {
                                yb3.h();
                                this.j = new tr3<>(this, cb2);
                                tr3<cb2> tr3 = this.j;
                                if (tr3 != null) {
                                    cb2 a2 = tr3.a();
                                    if (a2 != null) {
                                        kd4.a((Object) a2, "mBinding.get()!!");
                                        return a2.d();
                                    }
                                    kd4.a();
                                    throw null;
                                }
                                kd4.d("mBinding");
                                throw null;
                            }
                            kd4.d("mPresenter");
                            throw null;
                        }
                        kd4.a();
                        throw null;
                    }
                    kd4.d("mDashboardHeartRatesAdapter");
                    throw null;
                }
                kd4.a();
                throw null;
            }
            kd4.a();
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d(p, "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    public void onDestroyView() {
        yb3 yb3 = this.k;
        if (yb3 != null) {
            yb3.i();
            super.onDestroyView();
            N0();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onPause() {
        yb3 yb3 = this.k;
        if (yb3 != null) {
            yb3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
            }
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        yb3 yb3 = this.k;
        if (yb3 != null) {
            yb3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        R("heart_rate_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ic a2 = lc.a(activity).a(iu3.class);
            kd4.a((Object) a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            iu3 iu3 = (iu3) a2;
        }
    }

    @DexIgnore
    public void a(yb3 yb3) {
        kd4.b(yb3, "presenter");
        this.k = yb3;
    }

    @DexIgnore
    public void b(Date date, Date date2) {
        kd4.b(date, "startWeekDate");
        kd4.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    public void b(qd<DailyHeartRateSummary> qdVar) {
        mt2 mt2 = this.l;
        if (mt2 != null) {
            mt2.c(qdVar);
        } else {
            kd4.d("mDashboardHeartRatesAdapter");
            throw null;
        }
    }
}
