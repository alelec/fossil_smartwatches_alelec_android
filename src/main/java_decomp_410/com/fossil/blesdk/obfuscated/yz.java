package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yz implements e00 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ e00 b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public String d;

    @DexIgnore
    public yz(Context context, e00 e00) {
        this.a = context;
        this.b = e00;
    }

    @DexIgnore
    public String a() {
        if (!this.c) {
            this.d = CommonUtils.o(this.a);
            this.c = true;
        }
        String str = this.d;
        if (str != null) {
            return str;
        }
        e00 e00 = this.b;
        if (e00 != null) {
            return e00.a();
        }
        return null;
    }
}
