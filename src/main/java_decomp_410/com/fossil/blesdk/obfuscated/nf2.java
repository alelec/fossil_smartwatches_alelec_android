package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nf2 extends mf2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout E;
    @DexIgnore
    public long F;

    /*
    static {
        H.put(R.id.ll_icClose, 1);
        H.put(R.id.iv_close, 2);
        H.put(R.id.tv_forgot, 3);
        H.put(R.id.tv_title, 4);
        H.put(R.id.svLogin, 5);
        H.put(R.id.input_email, 6);
        H.put(R.id.et_email, 7);
        H.put(R.id.input_password, 8);
        H.put(R.id.et_password, 9);
        H.put(R.id.tv_signup_via, 10);
        H.put(R.id.iv_facebook, 11);
        H.put(R.id.iv_google, 12);
        H.put(R.id.iv_apple, 13);
        H.put(R.id.br_top, 14);
        H.put(R.id.br_bottom, 15);
        H.put(R.id.iv_weibo, 16);
        H.put(R.id.iv_wechat, 17);
        H.put(R.id.tv_have_account, 18);
        H.put(R.id.bt_continue, 19);
        H.put(R.id.tv_signup, 20);
    }
    */

    @DexIgnore
    public nf2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 21, G, H));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public nf2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[15], objArr[14], objArr[19], objArr[7], objArr[9], objArr[6], objArr[8], objArr[13], objArr[2], objArr[11], objArr[12], objArr[17], objArr[16], objArr[1], objArr[5], objArr[3], objArr[18], objArr[20], objArr[10], objArr[4]);
        this.F = -1;
        this.E = objArr[0];
        this.E.setTag((Object) null);
        a(view);
        f();
    }
}
