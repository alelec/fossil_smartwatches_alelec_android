package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ks4 implements gr4<em4, Integer> {
    @DexIgnore
    public static /* final */ ks4 a; // = new ks4();

    @DexIgnore
    public Integer a(em4 em4) throws IOException {
        return Integer.valueOf(em4.F());
    }
}
