package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class u82 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ sg2 s;
    @DexIgnore
    public /* final */ sg2 t;
    @DexIgnore
    public /* final */ LinearLayout u;

    @DexIgnore
    public u82(Object obj, View view, int i, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, sg2 sg2, sg2 sg22, LinearLayout linearLayout) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleTextView;
        this.s = sg2;
        a((ViewDataBinding) this.s);
        this.t = sg22;
        a((ViewDataBinding) this.t);
        this.u = linearLayout;
    }
}
