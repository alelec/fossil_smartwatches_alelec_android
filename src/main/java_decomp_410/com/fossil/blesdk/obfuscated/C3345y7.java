package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y7 */
public class C3345y7 implements android.text.Spannable {

    @DexIgnore
    /* renamed from: e */
    public /* final */ android.text.Spannable f11199e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ com.fossil.blesdk.obfuscated.C3345y7.C3346a f11200f;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.y7$a")
    /* renamed from: com.fossil.blesdk.obfuscated.y7$a */
    public static final class C3346a {

        @DexIgnore
        /* renamed from: a */
        public /* final */ android.text.TextPaint f11201a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ android.text.TextDirectionHeuristic f11202b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int f11203c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ int f11204d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ android.text.PrecomputedText.Params f11205e; // = null;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.y7$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.y7$a$a */
        public static class C3347a {

            @DexIgnore
            /* renamed from: a */
            public /* final */ android.text.TextPaint f11206a;

            @DexIgnore
            /* renamed from: b */
            public android.text.TextDirectionHeuristic f11207b;

            @DexIgnore
            /* renamed from: c */
            public int f11208c;

            @DexIgnore
            /* renamed from: d */
            public int f11209d;

            @DexIgnore
            public C3347a(android.text.TextPaint textPaint) {
                this.f11206a = textPaint;
                if (android.os.Build.VERSION.SDK_INT >= 23) {
                    this.f11208c = 1;
                    this.f11209d = 1;
                } else {
                    this.f11209d = 0;
                    this.f11208c = 0;
                }
                if (android.os.Build.VERSION.SDK_INT >= 18) {
                    this.f11207b = android.text.TextDirectionHeuristics.FIRSTSTRONG_LTR;
                } else {
                    this.f11207b = null;
                }
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C3345y7.C3346a.C3347a mo18056a(int i) {
                this.f11208c = i;
                return this;
            }

            @DexIgnore
            /* renamed from: b */
            public com.fossil.blesdk.obfuscated.C3345y7.C3346a.C3347a mo18059b(int i) {
                this.f11209d = i;
                return this;
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C3345y7.C3346a.C3347a mo18057a(android.text.TextDirectionHeuristic textDirectionHeuristic) {
                this.f11207b = textDirectionHeuristic;
                return this;
            }

            @DexIgnore
            /* renamed from: a */
            public com.fossil.blesdk.obfuscated.C3345y7.C3346a mo18058a() {
                return new com.fossil.blesdk.obfuscated.C3345y7.C3346a(this.f11206a, this.f11207b, this.f11208c, this.f11209d);
            }
        }

        @DexIgnore
        public C3346a(android.text.TextPaint textPaint, android.text.TextDirectionHeuristic textDirectionHeuristic, int i, int i2) {
            this.f11201a = textPaint;
            this.f11202b = textDirectionHeuristic;
            this.f11203c = i;
            this.f11204d = i2;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo18048a() {
            return this.f11203c;
        }

        @DexIgnore
        /* renamed from: b */
        public int mo18050b() {
            return this.f11204d;
        }

        @DexIgnore
        /* renamed from: c */
        public android.text.TextDirectionHeuristic mo18051c() {
            return this.f11202b;
        }

        @DexIgnore
        /* renamed from: d */
        public android.text.TextPaint mo18052d() {
            return this.f11201a;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof com.fossil.blesdk.obfuscated.C3345y7.C3346a)) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C3345y7.C3346a aVar = (com.fossil.blesdk.obfuscated.C3345y7.C3346a) obj;
            if (!mo18049a(aVar)) {
                return false;
            }
            return android.os.Build.VERSION.SDK_INT < 18 || this.f11202b == aVar.mo18051c();
        }

        @DexIgnore
        public int hashCode() {
            int i = android.os.Build.VERSION.SDK_INT;
            if (i >= 24) {
                return com.fossil.blesdk.obfuscated.C1706e8.m6325a(java.lang.Float.valueOf(this.f11201a.getTextSize()), java.lang.Float.valueOf(this.f11201a.getTextScaleX()), java.lang.Float.valueOf(this.f11201a.getTextSkewX()), java.lang.Float.valueOf(this.f11201a.getLetterSpacing()), java.lang.Integer.valueOf(this.f11201a.getFlags()), this.f11201a.getTextLocales(), this.f11201a.getTypeface(), java.lang.Boolean.valueOf(this.f11201a.isElegantTextHeight()), this.f11202b, java.lang.Integer.valueOf(this.f11203c), java.lang.Integer.valueOf(this.f11204d));
            } else if (i >= 21) {
                return com.fossil.blesdk.obfuscated.C1706e8.m6325a(java.lang.Float.valueOf(this.f11201a.getTextSize()), java.lang.Float.valueOf(this.f11201a.getTextScaleX()), java.lang.Float.valueOf(this.f11201a.getTextSkewX()), java.lang.Float.valueOf(this.f11201a.getLetterSpacing()), java.lang.Integer.valueOf(this.f11201a.getFlags()), this.f11201a.getTextLocale(), this.f11201a.getTypeface(), java.lang.Boolean.valueOf(this.f11201a.isElegantTextHeight()), this.f11202b, java.lang.Integer.valueOf(this.f11203c), java.lang.Integer.valueOf(this.f11204d));
            } else if (i >= 18) {
                return com.fossil.blesdk.obfuscated.C1706e8.m6325a(java.lang.Float.valueOf(this.f11201a.getTextSize()), java.lang.Float.valueOf(this.f11201a.getTextScaleX()), java.lang.Float.valueOf(this.f11201a.getTextSkewX()), java.lang.Integer.valueOf(this.f11201a.getFlags()), this.f11201a.getTextLocale(), this.f11201a.getTypeface(), this.f11202b, java.lang.Integer.valueOf(this.f11203c), java.lang.Integer.valueOf(this.f11204d));
            } else if (i >= 17) {
                return com.fossil.blesdk.obfuscated.C1706e8.m6325a(java.lang.Float.valueOf(this.f11201a.getTextSize()), java.lang.Float.valueOf(this.f11201a.getTextScaleX()), java.lang.Float.valueOf(this.f11201a.getTextSkewX()), java.lang.Integer.valueOf(this.f11201a.getFlags()), this.f11201a.getTextLocale(), this.f11201a.getTypeface(), this.f11202b, java.lang.Integer.valueOf(this.f11203c), java.lang.Integer.valueOf(this.f11204d));
            } else {
                return com.fossil.blesdk.obfuscated.C1706e8.m6325a(java.lang.Float.valueOf(this.f11201a.getTextSize()), java.lang.Float.valueOf(this.f11201a.getTextScaleX()), java.lang.Float.valueOf(this.f11201a.getTextSkewX()), java.lang.Integer.valueOf(this.f11201a.getFlags()), this.f11201a.getTypeface(), this.f11202b, java.lang.Integer.valueOf(this.f11203c), java.lang.Integer.valueOf(this.f11204d));
            }
        }

        @DexIgnore
        public java.lang.String toString() {
            java.lang.StringBuilder sb = new java.lang.StringBuilder("{");
            sb.append("textSize=" + this.f11201a.getTextSize());
            sb.append(", textScaleX=" + this.f11201a.getTextScaleX());
            sb.append(", textSkewX=" + this.f11201a.getTextSkewX());
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                sb.append(", letterSpacing=" + this.f11201a.getLetterSpacing());
                sb.append(", elegantTextHeight=" + this.f11201a.isElegantTextHeight());
            }
            int i = android.os.Build.VERSION.SDK_INT;
            if (i >= 24) {
                sb.append(", textLocale=" + this.f11201a.getTextLocales());
            } else if (i >= 17) {
                sb.append(", textLocale=" + this.f11201a.getTextLocale());
            }
            sb.append(", typeface=" + this.f11201a.getTypeface());
            if (android.os.Build.VERSION.SDK_INT >= 26) {
                sb.append(", variationSettings=" + this.f11201a.getFontVariationSettings());
            }
            sb.append(", textDir=" + this.f11202b);
            sb.append(", breakStrategy=" + this.f11203c);
            sb.append(", hyphenationFrequency=" + this.f11204d);
            sb.append("}");
            return sb.toString();
        }

        @DexIgnore
        /* renamed from: a */
        public boolean mo18049a(com.fossil.blesdk.obfuscated.C3345y7.C3346a aVar) {
            android.text.PrecomputedText.Params params = this.f11205e;
            if (params != null) {
                return params.equals(aVar.f11205e);
            }
            if ((android.os.Build.VERSION.SDK_INT >= 23 && (this.f11203c != aVar.mo18048a() || this.f11204d != aVar.mo18050b())) || this.f11201a.getTextSize() != aVar.mo18052d().getTextSize() || this.f11201a.getTextScaleX() != aVar.mo18052d().getTextScaleX() || this.f11201a.getTextSkewX() != aVar.mo18052d().getTextSkewX()) {
                return false;
            }
            if ((android.os.Build.VERSION.SDK_INT >= 21 && (this.f11201a.getLetterSpacing() != aVar.mo18052d().getLetterSpacing() || !android.text.TextUtils.equals(this.f11201a.getFontFeatureSettings(), aVar.mo18052d().getFontFeatureSettings()))) || this.f11201a.getFlags() != aVar.mo18052d().getFlags()) {
                return false;
            }
            int i = android.os.Build.VERSION.SDK_INT;
            if (i >= 24) {
                if (!this.f11201a.getTextLocales().equals(aVar.mo18052d().getTextLocales())) {
                    return false;
                }
            } else if (i >= 17 && !this.f11201a.getTextLocale().equals(aVar.mo18052d().getTextLocale())) {
                return false;
            }
            if (this.f11201a.getTypeface() == null) {
                if (aVar.mo18052d().getTypeface() != null) {
                    return false;
                }
                return true;
            } else if (!this.f11201a.getTypeface().equals(aVar.mo18052d().getTypeface())) {
                return false;
            } else {
                return true;
            }
        }

        @DexIgnore
        public C3346a(android.text.PrecomputedText.Params params) {
            this.f11201a = params.getTextPaint();
            this.f11202b = params.getTextDirection();
            this.f11203c = params.getBreakStrategy();
            this.f11204d = params.getHyphenationFrequency();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C3345y7.C3346a mo18036a() {
        return this.f11200f;
    }

    @DexIgnore
    public char charAt(int i) {
        return this.f11199e.charAt(i);
    }

    @DexIgnore
    public int getSpanEnd(java.lang.Object obj) {
        return this.f11199e.getSpanEnd(obj);
    }

    @DexIgnore
    public int getSpanFlags(java.lang.Object obj) {
        return this.f11199e.getSpanFlags(obj);
    }

    @DexIgnore
    public int getSpanStart(java.lang.Object obj) {
        return this.f11199e.getSpanStart(obj);
    }

    @DexIgnore
    public <T> T[] getSpans(int i, int i2, java.lang.Class<T> cls) {
        return this.f11199e.getSpans(i, i2, cls);
    }

    @DexIgnore
    public int length() {
        return this.f11199e.length();
    }

    @DexIgnore
    public int nextSpanTransition(int i, int i2, java.lang.Class cls) {
        return this.f11199e.nextSpanTransition(i, i2, cls);
    }

    @DexIgnore
    public void removeSpan(java.lang.Object obj) {
        if (!(obj instanceof android.text.style.MetricAffectingSpan)) {
            this.f11199e.removeSpan(obj);
            return;
        }
        throw new java.lang.IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
    }

    @DexIgnore
    public void setSpan(java.lang.Object obj, int i, int i2, int i3) {
        if (!(obj instanceof android.text.style.MetricAffectingSpan)) {
            this.f11199e.setSpan(obj, i, i2, i3);
            return;
        }
        throw new java.lang.IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
    }

    @DexIgnore
    public java.lang.CharSequence subSequence(int i, int i2) {
        return this.f11199e.subSequence(i, i2);
    }

    @DexIgnore
    public java.lang.String toString() {
        return this.f11199e.toString();
    }
}
