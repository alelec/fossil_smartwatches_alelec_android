package com.fossil.blesdk.obfuscated;

import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.helper.AppHelper;
import java.io.FileNotFoundException;
import java.util.Iterator;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fn2 implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static fn2 n;
    @DexIgnore
    public static /* final */ a o; // = new a((fd4) null);
    @DexIgnore
    public MediaPlayer a;
    @DexIgnore
    public AudioManager b;
    @DexIgnore
    public volatile boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public long i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public AudioAttributes k;
    @DexIgnore
    public /* final */ Runnable l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(fn2 fn2) {
            fn2.n = fn2;
        }

        @DexIgnore
        public final fn2 b() {
            return fn2.n;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final synchronized fn2 a() {
            fn2 b;
            if (fn2.o.b() == null) {
                fn2.o.a(new fn2((fd4) null));
            }
            b = fn2.o.b();
            if (b == null) {
                kd4.a();
                throw null;
            }
            return b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Runnable {
        @DexIgnore
        public /* final */ Ringtone e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ /* synthetic */ fn2 g;

        @DexIgnore
        public b(fn2 fn2, Ringtone ringtone, long j) {
            kd4.b(ringtone, "mRingtone");
            this.g = fn2;
            this.e = ringtone;
            this.f = j;
        }

        @DexIgnore
        public void run() {
            this.g.a(this.e, Integer.MAX_VALUE, this.f, false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements Runnable {
        @DexIgnore
        public /* final */ Ringtone e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ /* synthetic */ fn2 g;

        @DexIgnore
        public c(fn2 fn2, Ringtone ringtone, int i) {
            kd4.b(ringtone, "mRingtone");
            this.g = fn2;
            this.e = ringtone;
            this.f = i;
        }

        @DexIgnore
        public void run() {
            this.g.a(this.e, this.f, 0, false);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ fn2 e;

        @DexIgnore
        public d(fn2 fn2) {
            this.e = fn2;
        }

        @DexIgnore
        public final void run() {
            if (this.e.a != null) {
                MediaPlayer c = this.e.a;
                if (c != null) {
                    c.stop();
                    if (this.e.d) {
                        this.e.a();
                        this.e.b.setStreamVolume(4, this.e.f, 1);
                    }
                    FLogger.INSTANCE.getLocal().d(fn2.m, "On play ringtone complete");
                    this.e.c = false;
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = fn2.class.getSimpleName();
        kd4.a((Object) simpleName, "SoundManager::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public fn2() {
        this.i = ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        this.j = new Handler(PortfolioApp.W.c().getMainLooper());
        this.a = new MediaPlayer();
        Object systemService = PortfolioApp.W.c().getApplicationContext().getSystemService("audio");
        if (systemService != null) {
            this.b = (AudioManager) systemService;
            this.e = this.b.getStreamMaxVolume(4);
            AudioAttributes build = new AudioAttributes.Builder().setUsage(4).build();
            kd4.a((Object) build, "AudioAttributes.Builder(\u2026utes.USAGE_ALARM).build()");
            this.k = build;
            this.l = new d(this);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.media.AudioManager");
    }

    @DexIgnore
    public void onCompletion(MediaPlayer mediaPlayer) {
        kd4.b(mediaPlayer, "mp");
        if (this.g > 0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            local.d(str, "current loop is" + this.g);
            this.g = this.g + -1;
            MediaPlayer mediaPlayer2 = this.a;
            if (mediaPlayer2 != null) {
                mediaPlayer2.seekTo(0);
                MediaPlayer mediaPlayer3 = this.a;
                if (mediaPlayer3 != null) {
                    mediaPlayer3.start();
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            MediaPlayer mediaPlayer4 = this.a;
            if (mediaPlayer4 != null) {
                mediaPlayer4.stop();
                if (this.d) {
                    a();
                    this.b.setStreamVolume(4, this.f, 1);
                }
                FLogger.INSTANCE.getLocal().d(m, "On play ringtone complete");
                this.c = false;
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void onPrepared(MediaPlayer mediaPlayer) {
        kd4.b(mediaPlayer, "mp");
        this.f = this.b.getStreamVolume(4);
        if (this.d) {
            this.b.setStreamVolume(4, this.e, 1);
        }
        if (!this.h) {
            this.j.postDelayed(this.l, (long) (mediaPlayer.getDuration() * this.g));
        } else {
            long j2 = this.i;
            if (j2 < ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                this.j.postDelayed(this.l, j2);
            }
            MediaPlayer mediaPlayer2 = this.a;
            if (mediaPlayer2 != null) {
                mediaPlayer2.setLooping(true);
            } else {
                kd4.a();
                throw null;
            }
        }
        MediaPlayer mediaPlayer3 = this.a;
        if (mediaPlayer3 != null) {
            mediaPlayer3.start();
            this.c = true;
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public final void b(Ringtone ringtone) {
        kd4.b(ringtone, Constants.RINGTONE);
        a(ringtone, 2);
    }

    @DexIgnore
    public final void a(Ringtone ringtone, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "Inside " + m + ".playRingtoneInfinitive - ringtone=" + ringtone);
        if (ringtone != null) {
            this.d = true;
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            if (!this.c) {
                FLogger.INSTANCE.getLocal().d(m, "On button play ringtone event");
                new Thread(new b(this, ringtone, j2)).start();
            }
        }
    }

    @DexIgnore
    public final void b() {
        MediaPlayer mediaPlayer = this.a;
        if (mediaPlayer != null) {
            if (mediaPlayer == null) {
                kd4.a();
                throw null;
            } else if (mediaPlayer.isPlaying()) {
                FLogger.INSTANCE.getLocal().d(m, "On stop playing ringtone");
                MediaPlayer mediaPlayer2 = this.a;
                if (mediaPlayer2 != null) {
                    mediaPlayer2.stop();
                    this.j.removeCallbacks(this.l);
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
        if (this.d) {
            this.b.setStreamVolume(4, this.f, 1);
            this.d = false;
        }
        this.c = false;
    }

    @DexIgnore
    public /* synthetic */ fn2(fd4 fd4) {
        this();
    }

    @DexIgnore
    public final void a(Ringtone ringtone, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "Inside " + m + ".playRingtone - ringtone=" + ringtone);
        if (ringtone != null) {
            this.d = true;
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            if (this.c) {
                FLogger.INSTANCE.getLocal().d(m, "On button stop ringtone event");
                a();
                return;
            }
            FLogger.INSTANCE.getLocal().d(m, "On button play ringtone event");
            new Thread(new c(this, ringtone, i2)).start();
        }
    }

    @DexIgnore
    public final void a(Ringtone ringtone) {
        if (ringtone != null) {
            if (this.a == null) {
                this.a = new MediaPlayer();
            }
            b();
            new Thread(new c(this, ringtone, 1)).start();
        }
    }

    @DexIgnore
    public final synchronized void a() {
        FLogger.INSTANCE.getLocal().d(m, "On release media event");
        if (this.c) {
            b();
        }
        if (!this.d) {
            if (this.a != null) {
                MediaPlayer mediaPlayer = this.a;
                if (mediaPlayer != null) {
                    mediaPlayer.reset();
                    MediaPlayer mediaPlayer2 = this.a;
                    if (mediaPlayer2 != null) {
                        mediaPlayer2.release();
                        this.a = null;
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.d = false;
            this.c = false;
        }
    }

    @DexIgnore
    public final void a(Ringtone ringtone, int i2, long j2, boolean z) {
        T t;
        kd4.b(ringtone, Constants.RINGTONE);
        try {
            MediaPlayer mediaPlayer = this.a;
            if (mediaPlayer != null) {
                mediaPlayer.setOnPreparedListener(this);
                MediaPlayer mediaPlayer2 = this.a;
                if (mediaPlayer2 != null) {
                    mediaPlayer2.setOnCompletionListener(this);
                    if (i2 >= Integer.MAX_VALUE) {
                        this.h = true;
                        this.g = 0;
                        this.i = j2;
                    } else {
                        this.h = false;
                        this.i = 0;
                        this.g = i2;
                    }
                    MediaPlayer mediaPlayer3 = this.a;
                    if (mediaPlayer3 != null) {
                        mediaPlayer3.reset();
                        if (TextUtils.isEmpty(ringtone.getRingtoneId())) {
                            AssetFileDescriptor openFd = PortfolioApp.W.c().getAssets().openFd("ringtones/" + ringtone.getRingtoneName() + CodelessMatcher.CURRENT_CLASS_NAME + Constants.MP3_EXTENSION);
                            kd4.a((Object) openFd, "PortfolioApp.instance.as\u2026me + \".\" + MP3_EXTENSION)");
                            MediaPlayer mediaPlayer4 = this.a;
                            if (mediaPlayer4 != null) {
                                mediaPlayer4.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
                                openFd.close();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            Iterator<T> it = AppHelper.f.c().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    t = null;
                                    break;
                                }
                                t = it.next();
                                if (kd4.a((Object) ((Ringtone) t).getRingtoneName(), (Object) ringtone.getRingtoneName())) {
                                    break;
                                }
                            }
                            if (((Ringtone) t) != null) {
                                MediaPlayer mediaPlayer5 = this.a;
                                if (mediaPlayer5 != null) {
                                    mediaPlayer5.setDataSource(PortfolioApp.W.c(), Uri.parse(MediaStore.Audio.Media.INTERNAL_CONTENT_URI + '/' + ringtone.getRingtoneId()));
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                        }
                        MediaPlayer mediaPlayer6 = this.a;
                        if (mediaPlayer6 != null) {
                            mediaPlayer6.setAudioAttributes(this.k);
                            MediaPlayer mediaPlayer7 = this.a;
                            if (mediaPlayer7 != null) {
                                mediaPlayer7.prepare();
                            } else {
                                kd4.a();
                                throw null;
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } catch (FileNotFoundException e2) {
            FLogger.INSTANCE.getLocal().e(m, "Error Inside " + m + ".playRingtoneFromAsset - Cant find ringtone, play default instead, ex=" + e2);
            Ringtone ringtone2 = new Ringtone(Constants.RINGTONE_DEFAULT, "");
            if (!z) {
                a(ringtone2, i2, j2, true);
                return;
            }
            FLogger.INSTANCE.getLocal().e(m, "Error Inside " + m + ".playRingtoneFromAsset - Cant find ringtone, play default instead");
        } catch (Exception e3) {
            FLogger.INSTANCE.getLocal().e(m, "Error when playing ringtone " + e3);
        }
    }
}
