package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.clearcut.zzbb;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cv0 extends mt0<String> implements dv0, RandomAccess {
    @DexIgnore
    public static /* final */ cv0 g;
    @DexIgnore
    public /* final */ List<Object> f;

    /*
    static {
        cv0 cv0 = new cv0();
        g = cv0;
        cv0.z();
    }
    */

    @DexIgnore
    public cv0() {
        this(10);
    }

    @DexIgnore
    public cv0(int i) {
        this((ArrayList<Object>) new ArrayList(i));
    }

    @DexIgnore
    public cv0(ArrayList<Object> arrayList) {
        this.f = arrayList;
    }

    @DexIgnore
    public static String a(Object obj) {
        return obj instanceof String ? (String) obj : obj instanceof zzbb ? ((zzbb) obj).zzz() : tu0.c((byte[]) obj);
    }

    @DexIgnore
    public final List<?> D() {
        return Collections.unmodifiableList(this.f);
    }

    @DexIgnore
    public final dv0 F() {
        return y() ? new ex0(this) : this;
    }

    @DexIgnore
    public final /* synthetic */ void add(int i, Object obj) {
        a();
        this.f.add(i, (String) obj);
        this.modCount++;
    }

    @DexIgnore
    public final boolean addAll(int i, Collection<? extends String> collection) {
        a();
        if (collection instanceof dv0) {
            collection = ((dv0) collection).D();
        }
        boolean addAll = this.f.addAll(i, collection);
        this.modCount++;
        return addAll;
    }

    @DexIgnore
    public final boolean addAll(Collection<? extends String> collection) {
        return addAll(size(), collection);
    }

    @DexIgnore
    public final /* synthetic */ wu0 c(int i) {
        if (i >= size()) {
            ArrayList arrayList = new ArrayList(i);
            arrayList.addAll(this.f);
            return new cv0((ArrayList<Object>) arrayList);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final void clear() {
        a();
        this.f.clear();
        this.modCount++;
    }

    @DexIgnore
    public final Object e(int i) {
        return this.f.get(i);
    }

    @DexIgnore
    public final /* synthetic */ Object get(int i) {
        Object obj = this.f.get(i);
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof zzbb) {
            zzbb zzbb = (zzbb) obj;
            String zzz = zzbb.zzz();
            if (zzbb.zzaa()) {
                this.f.set(i, zzz);
            }
            return zzz;
        }
        byte[] bArr = (byte[]) obj;
        String c = tu0.c(bArr);
        if (tu0.b(bArr)) {
            this.f.set(i, c);
        }
        return c;
    }

    @DexIgnore
    public final /* synthetic */ Object remove(int i) {
        a();
        Object remove = this.f.remove(i);
        this.modCount++;
        return a(remove);
    }

    @DexIgnore
    public final /* synthetic */ Object set(int i, Object obj) {
        a();
        return a(this.f.set(i, (String) obj));
    }

    @DexIgnore
    public final int size() {
        return this.f.size();
    }
}
