package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cj0<T> extends yi0<T> {
    @DexIgnore
    public boolean f; // = false;
    @DexIgnore
    public ArrayList<Integer> g;

    @DexIgnore
    public cj0(DataHolder dataHolder) {
        super(dataHolder);
    }

    @DexIgnore
    public final int a(int i) {
        if (i >= 0 && i < this.g.size()) {
            return this.g.get(i).intValue();
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(i);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    public abstract T a(int i, int i2);

    @DexIgnore
    public String b() {
        return null;
    }

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public final void d() {
        synchronized (this) {
            if (!this.f) {
                int count = this.e.getCount();
                this.g = new ArrayList<>();
                if (count > 0) {
                    this.g.add(0);
                    String c = c();
                    String c2 = this.e.c(c, 0, this.e.f(0));
                    int i = 1;
                    while (i < count) {
                        int f2 = this.e.f(i);
                        String c3 = this.e.c(c, i, f2);
                        if (c3 != null) {
                            if (!c3.equals(c2)) {
                                this.g.add(Integer.valueOf(i));
                                c2 = c3;
                            }
                            i++;
                        } else {
                            StringBuilder sb = new StringBuilder(String.valueOf(c).length() + 78);
                            sb.append("Missing value for markerColumn: ");
                            sb.append(c);
                            sb.append(", at row: ");
                            sb.append(i);
                            sb.append(", for window: ");
                            sb.append(f2);
                            throw new NullPointerException(sb.toString());
                        }
                    }
                }
                this.f = true;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0063, code lost:
        if (r6.e.c(r4, r7, r3) == null) goto L_0x0067;
     */
    @DexIgnore
    public final T get(int i) {
        int i2;
        int i3;
        d();
        int a = a(i);
        int i4 = 0;
        if (i >= 0 && i != this.g.size()) {
            if (i == this.g.size() - 1) {
                i3 = this.e.getCount();
                i2 = this.g.get(i).intValue();
            } else {
                i3 = this.g.get(i + 1).intValue();
                i2 = this.g.get(i).intValue();
            }
            int i5 = i3 - i2;
            if (i5 == 1) {
                int a2 = a(i);
                int f2 = this.e.f(a2);
                String b = b();
                if (b != null) {
                }
            }
            i4 = i5;
        }
        return a(a, i4);
    }

    @DexIgnore
    public int getCount() {
        d();
        return this.g.size();
    }
}
