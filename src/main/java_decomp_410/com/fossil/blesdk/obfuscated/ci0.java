package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ci0 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ ud0 b;

    @DexIgnore
    public ci0(ud0 ud0, int i) {
        bk0.a(ud0);
        this.b = ud0;
        this.a = i;
    }

    @DexIgnore
    public final ud0 a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }
}
