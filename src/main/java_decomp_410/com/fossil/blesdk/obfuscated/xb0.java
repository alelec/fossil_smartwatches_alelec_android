package com.fossil.blesdk.obfuscated;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.fossil.blesdk.obfuscated.ak0;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xb0 extends fe0<GoogleSignInOptions> {
    @DexIgnore
    public static int j; // = b.a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ak0.a<zb0, GoogleSignInAccount> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final /* synthetic */ Object a(me0 me0) {
            return ((zb0) me0).a();
        }

        @DexIgnore
        public /* synthetic */ a(dd0 dd0) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    /* 'enum' modifier removed */
    public static final class b {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] e; // = {a, b, c, d};

        @DexIgnore
        public static int[] a() {
            return (int[]) e.clone();
        }
    }

    /*
    static {
        new a((dd0) null);
    }
    */

    @DexIgnore
    public xb0(Context context, GoogleSignInOptions googleSignInOptions) {
        super(context, qb0.e, googleSignInOptions, (df0) new re0());
    }

    @DexIgnore
    public Intent i() {
        Context e = e();
        int i = dd0.a[k() - 1];
        if (i == 1) {
            return jc0.b(e, (GoogleSignInOptions) d());
        }
        if (i != 2) {
            return jc0.c(e, (GoogleSignInOptions) d());
        }
        return jc0.a(e, (GoogleSignInOptions) d());
    }

    @DexIgnore
    public wn1<Void> j() {
        return ak0.a(jc0.a(a(), e(), k() == b.c));
    }

    @DexIgnore
    public final synchronized int k() {
        if (j == b.a) {
            Context e = e();
            xd0 a2 = xd0.a();
            int a3 = a2.a(e, (int) zd0.GOOGLE_PLAY_SERVICES_VERSION_CODE);
            if (a3 == 0) {
                j = b.d;
            } else if (a2.a(e, a3, (String) null) != null || DynamiteModule.a(e, "com.google.android.gms.auth.api.fallback") == 0) {
                j = b.b;
            } else {
                j = b.c;
            }
        }
        return j;
    }

    @DexIgnore
    public xb0(Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, qb0.e, googleSignInOptions, (df0) new re0());
    }
}
