package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pk extends lk<gk> {
    @DexIgnore
    public pk(Context context, zl zlVar) {
        super(xk.a(context, zlVar).c());
    }

    @DexIgnore
    public boolean a(hl hlVar) {
        return hlVar.j.b() == NetworkType.UNMETERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(gk gkVar) {
        return !gkVar.a() || gkVar.b();
    }
}
