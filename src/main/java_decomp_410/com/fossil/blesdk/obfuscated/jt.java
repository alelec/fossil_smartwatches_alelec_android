package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.to;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class jt implements to<ByteBuffer> {
    @DexIgnore
    public /* final */ ByteBuffer a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements to.a<ByteBuffer> {
        @DexIgnore
        public Class<ByteBuffer> getDataClass() {
            return ByteBuffer.class;
        }

        @DexIgnore
        public to<ByteBuffer> a(ByteBuffer byteBuffer) {
            return new jt(byteBuffer);
        }
    }

    @DexIgnore
    public jt(ByteBuffer byteBuffer) {
        this.a = byteBuffer;
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public ByteBuffer b() {
        this.a.position(0);
        return this.a;
    }
}
