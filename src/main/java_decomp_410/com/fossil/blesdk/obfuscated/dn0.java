package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class dn0 implements Parcelable.Creator<wd0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        int i = 0;
        long j = -1;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 == 2) {
                i = SafeParcelReader.q(parcel, a);
            } else if (a2 != 3) {
                SafeParcelReader.v(parcel, a);
            } else {
                j = SafeParcelReader.s(parcel, a);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new wd0(str, i, j);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new wd0[i];
    }
}
