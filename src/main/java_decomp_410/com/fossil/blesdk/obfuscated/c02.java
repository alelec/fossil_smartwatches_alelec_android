package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface c02 {
    @DexIgnore
    <T> TypeAdapter<T> a(Gson gson, TypeToken<T> typeToken);
}
