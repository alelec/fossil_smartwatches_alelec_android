package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.TextView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class qa2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RecyclerView q;

    @DexIgnore
    public qa2(Object obj, View view, int i, RecyclerView recyclerView, TextView textView) {
        super(obj, view, i);
        this.q = recyclerView;
    }
}
