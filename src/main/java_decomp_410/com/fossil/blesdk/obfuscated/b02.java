package com.fossil.blesdk.obfuscated;

import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface b02<T> {
    @DexIgnore
    JsonElement serialize(T t, Type type, a02 a02);
}
