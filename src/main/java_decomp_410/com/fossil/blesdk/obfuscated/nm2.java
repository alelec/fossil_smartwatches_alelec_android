package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nm2 extends cm2 {
    @DexIgnore
    public nm2(hm2 hm2, fm2 fm2) {
        super(hm2, fm2);
    }

    @DexIgnore
    public am2 a() {
        return this.a.a(137);
    }

    @DexIgnore
    public byte[] b() {
        return this.a.e(132);
    }
}
