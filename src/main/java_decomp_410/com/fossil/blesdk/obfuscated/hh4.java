package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface hh4 {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static oh4 a(hh4 hh4, long j, Runnable runnable) {
            kd4.b(runnable, "block");
            return fh4.a().a(j, runnable);
        }
    }

    @DexIgnore
    oh4 a(long j, Runnable runnable);

    @DexIgnore
    void a(long j, dg4<? super qa4> dg4);
}
