package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ky */
public class C2259ky extends com.fossil.blesdk.obfuscated.v44<java.lang.Boolean> implements com.fossil.blesdk.obfuscated.p54 {
    @DexIgnore
    /* renamed from: j */
    public java.util.Map<p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType, java.lang.String> mo12979j() {
        return java.util.Collections.emptyMap();
    }

    @DexIgnore
    /* renamed from: p */
    public java.lang.String mo9329p() {
        return "com.crashlytics.sdk.android:beta";
    }

    @DexIgnore
    /* renamed from: r */
    public java.lang.String mo9330r() {
        return "1.2.10.27";
    }

    @DexIgnore
    /* renamed from: k */
    public java.lang.Boolean m9890k() {
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("Beta", "Beta kit initializing...");
        return true;
    }
}
