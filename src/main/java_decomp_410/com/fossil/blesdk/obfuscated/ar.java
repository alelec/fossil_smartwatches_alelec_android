package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface ar {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(aq<?> aqVar);
    }

    @DexIgnore
    aq<?> a(jo joVar);

    @DexIgnore
    aq<?> a(jo joVar, aq<?> aqVar);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(a aVar);
}
