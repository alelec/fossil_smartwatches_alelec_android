package com.fossil.blesdk.obfuscated;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zt implements mo<co, Bitmap> {
    @DexIgnore
    public /* final */ jq a;

    @DexIgnore
    public zt(jq jqVar) {
        this.a = jqVar;
    }

    @DexIgnore
    public boolean a(co coVar, lo loVar) {
        return true;
    }

    @DexIgnore
    public aq<Bitmap> a(co coVar, int i, int i2, lo loVar) {
        return ps.a(coVar.a(), this.a);
    }
}
