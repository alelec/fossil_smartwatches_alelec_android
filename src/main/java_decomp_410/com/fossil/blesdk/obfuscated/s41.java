package com.fossil.blesdk.obfuscated;

import android.os.DeadObjectException;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s41 implements m41<u31> {
    @DexIgnore
    public /* final */ /* synthetic */ r41 a;

    @DexIgnore
    public s41(r41 r41) {
        this.a = r41;
    }

    @DexIgnore
    public final void a() {
        this.a.p();
    }

    @DexIgnore
    public final /* synthetic */ IInterface b() throws DeadObjectException {
        return (u31) this.a.x();
    }
}
