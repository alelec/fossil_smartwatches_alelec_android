package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.blesdk.obfuscated.kc;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.uirenew.watchsetting.calibration.CalibrationActivity;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Ref$ObjectRef;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tp3 extends as2 implements ws3.g {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((fd4) null);
    @DexIgnore
    public tr3<kg2> k;
    @DexIgnore
    public WatchSettingViewModel l;
    @DexIgnore
    public xn m;
    @DexIgnore
    public j42 n;
    @DexIgnore
    public HashMap o;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return tp3.p;
        }

        @DexIgnore
        public final tp3 b() {
            return new tp3();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements cc<WatchSettingViewModel.b> {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ref$ObjectRef b;

        @DexIgnore
        public b(tp3 tp3, Ref$ObjectRef ref$ObjectRef) {
            this.a = tp3;
            this.b = ref$ObjectRef;
        }

        @DexIgnore
        public final void a(WatchSettingViewModel.b bVar) {
            if (bVar != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = tp3.q.a();
                local.d(a2, "onUIState changed, modelWrapper=" + bVar);
                if (bVar.k()) {
                    this.a.k();
                }
                if (bVar.l()) {
                    this.a.i();
                }
                if (bVar.n() != null) {
                    tp3 tp3 = this.a;
                    WatchSettingViewModel.c n = bVar.n();
                    if (n != null) {
                        tp3.a(n);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.a()) {
                    this.a.n();
                }
                if (bVar.o() != null) {
                    tp3 tp32 = this.a;
                    String o = bVar.o();
                    if (o != null) {
                        tp32.a0(o);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                Integer p = bVar.p();
                if (p != null) {
                    p.intValue();
                    tp3 tp33 = this.a;
                    Integer p2 = bVar.p();
                    if (p2 != null) {
                        tp33.p(p2.intValue());
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.g() != null) {
                    tp3 tp34 = this.a;
                    Pair<Integer, String> g = bVar.g();
                    if (g != null) {
                        int intValue = g.getFirst().intValue();
                        Pair<Integer, String> g2 = bVar.g();
                        if (g2 != null) {
                            tp34.a(intValue, g2.getSecond());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.e()) {
                    this.a.W((String) this.b.element);
                }
                if (bVar.d() != null) {
                    tp3 tp35 = this.a;
                    String d = bVar.d();
                    if (d != null) {
                        tp35.V(d);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.j() != null) {
                    tp3 tp36 = this.a;
                    String j = bVar.j();
                    if (j != null) {
                        tp36.Z(j);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.f() != null) {
                    tp3 tp37 = this.a;
                    String f = bVar.f();
                    if (f != null) {
                        tp37.X(f);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.c() != null) {
                    tp3 tp38 = this.a;
                    String c = bVar.c();
                    if (c != null) {
                        tp38.U(c);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.i() != null) {
                    tp3 tp39 = this.a;
                    String i = bVar.i();
                    if (i != null) {
                        tp39.Y(i);
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                if (bVar.h()) {
                    this.a.c();
                }
                if (bVar.m()) {
                    this.a.U0();
                }
                if (bVar.b() != null) {
                    ArrayList<PermissionCodes> b2 = bVar.b();
                    tp3 tp310 = this.a;
                    if (b2 != null) {
                        Object[] array = b2.toArray(new PermissionCodes[0]);
                        if (array != null) {
                            PermissionCodes[] permissionCodesArr = (PermissionCodes[]) array;
                            tp310.a((PermissionCodes[]) Arrays.copyOf(permissionCodesArr, permissionCodesArr.length));
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public c(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = this.e.getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.b(childFragmentManager, tp3.a(this.e).e());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public d(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a = tp3.q.a();
            local.d(a, "getWatchSerial=" + tp3.a(this.e).i());
            if (this.e.isActive() && !TextUtils.isEmpty(tp3.a(this.e).i())) {
                FindDeviceActivity.a aVar = FindDeviceActivity.C;
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    kd4.a((Object) activity, "activity!!");
                    String i = tp3.a(this.e).i();
                    if (i != null) {
                        aVar.a(activity, i);
                    } else {
                        kd4.a();
                        throw null;
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public e(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.e.isActive() && !TextUtils.isEmpty(tp3.a(this.e).i())) {
                CalibrationActivity.a aVar = CalibrationActivity.C;
                FragmentActivity activity = this.e.getActivity();
                if (activity != null) {
                    kd4.a((Object) activity, "activity!!");
                    aVar.a(activity);
                    return;
                }
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public f(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            tp3.a(this.e).a(100);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public g(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            tp3.a(this.e).a(50);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public h(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            tp3.a(this.e).a(25);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public i(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.e.n();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp3 e;

        @DexIgnore
        public j(tp3 tp3) {
            this.e = tp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            tp3.a(this.e).c();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ kg2 a;
        @DexIgnore
        public /* final */ /* synthetic */ tp3 b;

        @DexIgnore
        public k(kg2 kg2, tp3 tp3, WatchSettingViewModel.c cVar) {
            this.a = kg2;
            this.b = tp3;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            kd4.b(str, "serial");
            kd4.b(str2, "filePath");
            this.b.T0().a(str2).a(this.a.w);
        }
    }

    /*
    static {
        String simpleName = tp3.class.getSimpleName();
        kd4.a((Object) simpleName, "WatchSettingFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ WatchSettingViewModel a(tp3 tp3) {
        WatchSettingViewModel watchSettingViewModel = tp3.l;
        if (watchSettingViewModel != null) {
            return watchSettingViewModel;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return p;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public final xn T0() {
        xn xnVar = this.m;
        if (xnVar != null) {
            return xnVar;
        }
        kd4.d("mRequestManager");
        throw null;
    }

    @DexIgnore
    public final void U(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.b(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void U0() {
        tr3<kg2> tr3 = this.k;
        if (tr3 != null) {
            kg2 a2 = tr3.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                if (flexibleTextView != null) {
                    flexibleTextView.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___CTA__Connect));
                    return;
                }
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.e(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void W(String str) {
        kd4.b(str, "serial");
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            Context context = getContext();
            if (context != null) {
                kd4.a((Object) context, "context!!");
                aVar.a(context, str);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void X(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.d(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void Y(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.c(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void Z(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.f(str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void a0(String str) {
        kd4.b(str, "lastSync");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "updateLastSync, lastSync=" + str);
        if (isActive()) {
            tr3<kg2> tr3 = this.k;
            if (tr3 != null) {
                kg2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.C;
                    kd4.a((Object) flexibleTextView, "it.tvLastSyncValue");
                    flexibleTextView.setText(str);
                    FlexibleTextView flexibleTextView2 = a2.C;
                    kd4.a((Object) flexibleTextView2, "it.tvLastSyncValue");
                    flexibleTextView2.setSelected(true);
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void c() {
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.h(childFragmentManager);
        }
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d(p, "stopLoading");
        a();
    }

    @DexIgnore
    public final void k() {
        FLogger.INSTANCE.getLocal().d(p, "startLoading");
        b();
    }

    @DexIgnore
    public final void n() {
        FLogger.INSTANCE.getLocal().d(p, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        kg2 kg2 = (kg2) qa.a(layoutInflater, R.layout.fragment_watch_setting, viewGroup, false, O0());
        PortfolioApp.W.c().g().a(new vp3()).a(this);
        j42 j42 = this.n;
        if (j42 != null) {
            ic a2 = lc.a((Fragment) this, (kc.b) j42).a(WatchSettingViewModel.class);
            kd4.a((Object) a2, "ViewModelProviders.of(th\u2026ingViewModel::class.java)");
            this.l = (WatchSettingViewModel) a2;
            WatchSettingViewModel watchSettingViewModel = this.l;
            if (watchSettingViewModel != null) {
                watchSettingViewModel.l();
                Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
                Bundle arguments = getArguments();
                T t = arguments != null ? arguments.get("SERIAL") : null;
                if (t != null) {
                    ref$ObjectRef.element = (String) t;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = p;
                    local.d(str, "serial=" + ((String) ref$ObjectRef.element));
                    WatchSettingViewModel watchSettingViewModel2 = this.l;
                    if (watchSettingViewModel2 != null) {
                        watchSettingViewModel2.j((String) ref$ObjectRef.element);
                        WatchSettingViewModel watchSettingViewModel3 = this.l;
                        if (watchSettingViewModel3 != null) {
                            watchSettingViewModel3.h().a(getViewLifecycleOwner(), new b(this, ref$ObjectRef));
                            this.k = new tr3<>(this, kg2);
                            tr3<kg2> tr3 = this.k;
                            if (tr3 != null) {
                                kg2 a3 = tr3.a();
                                if (a3 != null) {
                                    kd4.a((Object) a3, "mBinding.get()!!");
                                    return a3.d();
                                }
                                kd4.a();
                                throw null;
                            }
                            kd4.d("mBinding");
                            throw null;
                        }
                        kd4.d("mViewModel");
                        throw null;
                    }
                    kd4.d("mViewModel");
                    throw null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.String");
            }
            kd4.d("mViewModel");
            throw null;
        }
        kd4.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        WatchSettingViewModel watchSettingViewModel = this.l;
        if (watchSettingViewModel != null) {
            watchSettingViewModel.m();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        WatchSettingViewModel watchSettingViewModel = this.l;
        if (watchSettingViewModel != null) {
            watchSettingViewModel.l();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        xn a2 = rn.a((Fragment) this);
        kd4.a((Object) a2, "Glide.with(this)");
        this.m = a2;
        tr3<kg2> tr3 = this.k;
        if (tr3 != null) {
            kg2 a3 = tr3.a();
            if (a3 != null) {
                a3.D.setOnClickListener(new c(this));
                a3.A.setOnClickListener(new d(this));
                a3.x.setOnClickListener(new e(this));
                a3.t.setOnClickListener(new f(this));
                a3.v.setOnClickListener(new g(this));
                a3.u.setOnClickListener(new h(this));
                a3.r.setOnClickListener(new i(this));
                a3.s.setOnClickListener(new j(this));
            }
            R("watch_setting_view");
            return;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void p(int i2) {
        tr3<kg2> tr3 = this.k;
        if (tr3 != null) {
            kg2 a2 = tr3.a();
            if (a2 != null) {
                PortfolioApp c2 = PortfolioApp.W.c();
                int a3 = k6.a((Context) c2, (int) R.color.primaryColor);
                int a4 = k6.a((Context) c2, (int) R.color.surface);
                Drawable c3 = k6.c(c2, R.drawable.bg_border_disable_color_primary);
                Drawable c4 = k6.c(c2, R.drawable.bg_border_active_color_primary);
                a2.t.setTextColor(a3);
                a2.v.setTextColor(a3);
                a2.u.setTextColor(a3);
                FlexibleButton flexibleButton = a2.t;
                kd4.a((Object) flexibleButton, "it.fbVibrationHigh");
                flexibleButton.setBackground(c3);
                FlexibleButton flexibleButton2 = a2.v;
                kd4.a((Object) flexibleButton2, "it.fbVibrationMedium");
                flexibleButton2.setBackground(c3);
                FlexibleButton flexibleButton3 = a2.u;
                kd4.a((Object) flexibleButton3, "it.fbVibrationLow");
                flexibleButton3.setBackground(c3);
                if (i2 == 25) {
                    a2.u.setTextColor(a4);
                    FlexibleButton flexibleButton4 = a2.u;
                    kd4.a((Object) flexibleButton4, "it.fbVibrationLow");
                    flexibleButton4.setBackground(c4);
                } else if (i2 == 50) {
                    a2.v.setTextColor(a4);
                    FlexibleButton flexibleButton5 = a2.v;
                    kd4.a((Object) flexibleButton5, "it.fbVibrationMedium");
                    flexibleButton5.setBackground(c4);
                } else if (i2 == 100) {
                    a2.t.setTextColor(a4);
                    FlexibleButton flexibleButton6 = a2.t;
                    kd4.a((Object) flexibleButton6, "it.fbVibrationHigh");
                    flexibleButton6.setBackground(c4);
                }
            }
        } else {
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(WatchSettingViewModel.c cVar) {
        WatchSettingViewModel.c cVar2 = cVar;
        kd4.b(cVar2, "watchSetting");
        FLogger.INSTANCE.getLocal().d(p, "updateDeviceInfo");
        if (isActive()) {
            tr3<kg2> tr3 = this.k;
            if (tr3 != null) {
                kg2 a2 = tr3.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.E;
                    kd4.a((Object) flexibleTextView, "it.tvSerialValue");
                    flexibleTextView.setText(cVar.a().getDeviceId());
                    FlexibleTextView flexibleTextView2 = a2.B;
                    kd4.a((Object) flexibleTextView2, "it.tvFwVersionValue");
                    flexibleTextView2.setText(cVar.a().getFirmwareRevision());
                    FlexibleTextView flexibleTextView3 = a2.z;
                    kd4.a((Object) flexibleTextView3, "it.tvDeviceName");
                    flexibleTextView3.setText(cVar.b());
                    FlexibleTextView flexibleTextView4 = a2.F;
                    kd4.a((Object) flexibleTextView4, "it.tvVibration");
                    flexibleTextView4.setVisibility(8);
                    FlexibleButton flexibleButton = a2.u;
                    kd4.a((Object) flexibleButton, "it.fbVibrationLow");
                    flexibleButton.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.v;
                    kd4.a((Object) flexibleButton2, "it.fbVibrationMedium");
                    flexibleButton2.setVisibility(8);
                    FlexibleButton flexibleButton3 = a2.t;
                    kd4.a((Object) flexibleButton3, "it.fbVibrationHigh");
                    flexibleButton3.setVisibility(8);
                    Boolean c2 = cVar.c();
                    if (c2 != null && c2.booleanValue()) {
                        FlexibleTextView flexibleTextView5 = a2.F;
                        kd4.a((Object) flexibleTextView5, "it.tvVibration");
                        flexibleTextView5.setVisibility(0);
                        FlexibleButton flexibleButton4 = a2.u;
                        kd4.a((Object) flexibleButton4, "it.fbVibrationLow");
                        flexibleButton4.setVisibility(0);
                        FlexibleButton flexibleButton5 = a2.v;
                        kd4.a((Object) flexibleButton5, "it.fbVibrationMedium");
                        flexibleButton5.setVisibility(0);
                        FlexibleButton flexibleButton6 = a2.t;
                        kd4.a((Object) flexibleButton6, "it.fbVibrationHigh");
                        flexibleButton6.setVisibility(0);
                    }
                    boolean d2 = cVar.d();
                    if (d2) {
                        Boolean c3 = cVar.c();
                        if (c3 != null && c3.booleanValue()) {
                            Integer vibrationStrength = cVar.a().getVibrationStrength();
                            if (vibrationStrength != null) {
                                p(vibrationStrength.intValue());
                            } else {
                                kd4.a();
                                throw null;
                            }
                        }
                        if (cVar.e()) {
                            FlexibleTextView flexibleTextView6 = a2.z;
                            kd4.a((Object) flexibleTextView6, "it.tvDeviceName");
                            flexibleTextView6.setAlpha(1.0f);
                            a2.z.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, k6.c(PortfolioApp.W.c(), R.drawable.ic_device_status), (Drawable) null);
                            FlexibleTextView flexibleTextView7 = a2.y;
                            kd4.a((Object) flexibleTextView7, "it.tvConnectionStatus");
                            flexibleTextView7.setAlpha(1.0f);
                            FlexibleTextView flexibleTextView8 = a2.y;
                            kd4.a((Object) flexibleTextView8, "it.tvConnectionStatus");
                            flexibleTextView8.setText(a(true, cVar.a().getBatteryLevel()));
                            if (cVar.a().getBatteryLevel() >= 0) {
                                int batteryLevel = cVar.a().getBatteryLevel();
                                a2.y.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, k6.c(PortfolioApp.W.c(), (batteryLevel >= 0 && 25 >= batteryLevel) ? R.drawable.ic_battery_25_vertical : (25 <= batteryLevel && 50 >= batteryLevel) ? R.drawable.ic_battery_50_vertical : (50 <= batteryLevel && 75 >= batteryLevel) ? R.drawable.ic_battery_75_vertical : R.drawable.ic_battery_100_vertical), (Drawable) null);
                            }
                            FlexibleTextView flexibleTextView9 = a2.q;
                            kd4.a((Object) flexibleTextView9, "it.btActive");
                            flexibleTextView9.setVisibility(0);
                            FlexibleTextView flexibleTextView10 = a2.s;
                            kd4.a((Object) flexibleTextView10, "it.btConnect");
                            flexibleTextView10.setVisibility(8);
                            FlexibleTextView flexibleTextView11 = a2.x;
                            kd4.a((Object) flexibleTextView11, "it.tvCalibration");
                            flexibleTextView11.setAlpha(1.0f);
                            FlexibleButton flexibleButton7 = a2.t;
                            kd4.a((Object) flexibleButton7, "it.fbVibrationHigh");
                            flexibleButton7.setEnabled(true);
                            FlexibleButton flexibleButton8 = a2.u;
                            kd4.a((Object) flexibleButton8, "it.fbVibrationLow");
                            flexibleButton8.setEnabled(true);
                            FlexibleButton flexibleButton9 = a2.v;
                            kd4.a((Object) flexibleButton9, "it.fbVibrationMedium");
                            flexibleButton9.setEnabled(true);
                            FlexibleTextView flexibleTextView12 = a2.x;
                            kd4.a((Object) flexibleTextView12, "it.tvCalibration");
                            flexibleTextView12.setEnabled(true);
                        } else {
                            a2.z.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                            a2.z.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryText));
                            FlexibleTextView flexibleTextView13 = a2.z;
                            kd4.a((Object) flexibleTextView13, "it.tvDeviceName");
                            flexibleTextView13.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView14 = a2.y;
                            kd4.a((Object) flexibleTextView14, "it.tvConnectionStatus");
                            flexibleTextView14.setText(a(false, cVar.a().getBatteryLevel()));
                            FlexibleTextView flexibleTextView15 = a2.y;
                            kd4.a((Object) flexibleTextView15, "it.tvConnectionStatus");
                            flexibleTextView15.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView16 = a2.q;
                            kd4.a((Object) flexibleTextView16, "it.btActive");
                            flexibleTextView16.setVisibility(8);
                            FlexibleTextView flexibleTextView17 = a2.s;
                            kd4.a((Object) flexibleTextView17, "it.btConnect");
                            flexibleTextView17.setVisibility(0);
                            FlexibleTextView flexibleTextView18 = a2.s;
                            kd4.a((Object) flexibleTextView18, "it.btConnect");
                            flexibleTextView18.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___CTA__Connect));
                            FlexibleTextView flexibleTextView19 = a2.x;
                            kd4.a((Object) flexibleTextView19, "it.tvCalibration");
                            flexibleTextView19.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView20 = a2.x;
                            kd4.a((Object) flexibleTextView20, "it.tvCalibration");
                            flexibleTextView20.setEnabled(false);
                        }
                    } else if (!d2) {
                        a2.z.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                        a2.z.setTextColor(k6.a((Context) PortfolioApp.W.c(), (int) R.color.primaryText));
                        FlexibleTextView flexibleTextView21 = a2.z;
                        kd4.a((Object) flexibleTextView21, "it.tvDeviceName");
                        flexibleTextView21.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView22 = a2.y;
                        kd4.a((Object) flexibleTextView22, "it.tvConnectionStatus");
                        flexibleTextView22.setText(a(false, cVar.a().getBatteryLevel()));
                        FlexibleTextView flexibleTextView23 = a2.y;
                        kd4.a((Object) flexibleTextView23, "it.tvConnectionStatus");
                        flexibleTextView23.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView24 = a2.q;
                        kd4.a((Object) flexibleTextView24, "it.btActive");
                        flexibleTextView24.setVisibility(8);
                        FlexibleTextView flexibleTextView25 = a2.s;
                        kd4.a((Object) flexibleTextView25, "it.btConnect");
                        flexibleTextView25.setText(sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___CTA__MakeActive));
                        FlexibleTextView flexibleTextView26 = a2.x;
                        kd4.a((Object) flexibleTextView26, "it.tvCalibration");
                        flexibleTextView26.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView27 = a2.x;
                        kd4.a((Object) flexibleTextView27, "it.tvCalibration");
                        flexibleTextView27.setEnabled(false);
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(cVar.a().getDeviceId()).setSerialPrefix(DeviceHelper.o.b(cVar.a().getDeviceId())).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.w;
                    kd4.a((Object) imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.b(cVar.a().getDeviceId(), DeviceHelper.ImageStyle.SMALL)).setImageCallback(new k(a2, this, cVar2)).download();
                    return;
                }
                return;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        kd4.b(str, "message");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        kd4.b(str, "tag");
        switch (str.hashCode()) {
            case -2051261777:
                if (str.equals("REMOVE_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i2 == R.id.tv_cancel) {
                        WatchSettingViewModel watchSettingViewModel = this.l;
                        if (watchSettingViewModel != null) {
                            watchSettingViewModel.h(stringExtra);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == R.id.tv_ok) {
                        WatchSettingViewModel watchSettingViewModel2 = this.l;
                        if (watchSettingViewModel2 != null) {
                            watchSettingViewModel2.d(stringExtra);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -1138109835:
                if (str.equals("SWITCH_DEVICE_ERASE_FAIL")) {
                    String stringExtra2 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra2 != null && i2 == R.id.tv_ok) {
                        WatchSettingViewModel watchSettingViewModel3 = this.l;
                        if (watchSettingViewModel3 != null) {
                            watchSettingViewModel3.f(stringExtra2);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -693701870:
                if (str.equals("CONFIRM_REMOVE_DEVICE")) {
                    if (i2 == R.id.tv_ok) {
                        WatchSettingViewModel watchSettingViewModel4 = this.l;
                        if (watchSettingViewModel4 != null) {
                            watchSettingViewModel4.k();
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case -454228492:
                if (str.equals("REMOVE_DEVICE_SYNC_FAIL")) {
                    String stringExtra3 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra3 != null && i2 == R.id.tv_ok) {
                        WatchSettingViewModel watchSettingViewModel5 = this.l;
                        if (watchSettingViewModel5 != null) {
                            watchSettingViewModel5.c(stringExtra3);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    String stringExtra4 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra4 != null && i2 == R.id.tv_ok) {
                        WatchSettingViewModel watchSettingViewModel6 = this.l;
                        if (watchSettingViewModel6 != null) {
                            watchSettingViewModel6.g(stringExtra4);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra5 = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra5 == null) {
                        return;
                    }
                    if (i2 == R.id.tv_cancel) {
                        WatchSettingViewModel watchSettingViewModel7 = this.l;
                        if (watchSettingViewModel7 != null) {
                            watchSettingViewModel7.i(stringExtra5);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else if (i2 == R.id.tv_ok) {
                        WatchSettingViewModel watchSettingViewModel8 = this.l;
                        if (watchSettingViewModel8 != null) {
                            watchSettingViewModel8.e(stringExtra5);
                            return;
                        } else {
                            kd4.d("mViewModel");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
        }
        super.a(str, i2, intent);
    }

    @DexIgnore
    public final String a(boolean z, int i2) {
        String a2 = sm2.a((Context) PortfolioApp.W.c(), z ? R.string.Profile_MyWatch_WatchSettings_Text__Connected : R.string.Profile_MyWatch_DianaProfile_Text__Disconnected);
        if (!z || i2 <= 0) {
            kd4.a((Object) a2, "connectedString");
            return a2;
        }
        return a2 + ", " + i2 + '%';
    }
}
