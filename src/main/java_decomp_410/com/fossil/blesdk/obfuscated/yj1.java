package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ rl1 e;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 f;

    @DexIgnore
    public yj1(vj1 vj1, rl1 rl1) {
        this.f = vj1;
        this.e = rl1;
    }

    @DexIgnore
    public final void run() {
        kg1 d = this.f.d;
        if (d == null) {
            this.f.d().s().a("Discarding data. Failed to send app launch");
            return;
        }
        try {
            d.a(this.e);
            this.f.a(d, (jk0) null, this.e);
            this.f.C();
        } catch (RemoteException e2) {
            this.f.d().s().a("Failed to send app launch to the service", e2);
        }
    }
}
