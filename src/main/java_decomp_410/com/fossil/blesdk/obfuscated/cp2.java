package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cp2 implements Factory<bp2> {
    @DexIgnore
    public /* final */ Provider<dn2> a;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> b;
    @DexIgnore
    public /* final */ Provider<en2> c;

    @DexIgnore
    public cp2(Provider<dn2> provider, Provider<AuthApiGuestService> provider2, Provider<en2> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static cp2 a(Provider<dn2> provider, Provider<AuthApiGuestService> provider2, Provider<en2> provider3) {
        return new cp2(provider, provider2, provider3);
    }

    @DexIgnore
    public static bp2 b(Provider<dn2> provider, Provider<AuthApiGuestService> provider2, Provider<en2> provider3) {
        return new bp2(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public bp2 get() {
        return b(this.a, this.b, this.c);
    }
}
