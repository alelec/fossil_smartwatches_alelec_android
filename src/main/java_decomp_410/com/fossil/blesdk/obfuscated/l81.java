package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class l81 {
    @DexIgnore
    public static /* final */ j81<?> a; // = new k81();
    @DexIgnore
    public static /* final */ j81<?> b; // = a();

    @DexIgnore
    public static j81<?> a() {
        try {
            return (j81) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static j81<?> b() {
        return a;
    }

    @DexIgnore
    public static j81<?> c() {
        j81<?> j81 = b;
        if (j81 != null) {
            return j81;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }
}
