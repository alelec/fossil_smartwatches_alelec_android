package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fv {
    @DexIgnore
    public /* final */ List<a<?>> a; // = new ArrayList();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ ho<T> b;

        @DexIgnore
        public a(Class<T> cls, ho<T> hoVar) {
            this.a = cls;
            this.b = hoVar;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public synchronized <T> ho<T> a(Class<T> cls) {
        for (a next : this.a) {
            if (next.a(cls)) {
                return next.b;
            }
        }
        return null;
    }

    @DexIgnore
    public synchronized <T> void a(Class<T> cls, ho<T> hoVar) {
        this.a.add(new a(cls, hoVar));
    }
}
