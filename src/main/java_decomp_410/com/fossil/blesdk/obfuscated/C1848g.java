package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.g */
public class C1848g {

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.g$a")
    /* renamed from: com.fossil.blesdk.obfuscated.g$a */
    public static class C1849a {
        @DexIgnore
        /* renamed from: a */
        public static java.lang.Object m7207a() {
            return new android.media.MediaDescription.Builder();
        }

        @DexIgnore
        /* renamed from: b */
        public static void m7214b(java.lang.Object obj, java.lang.CharSequence charSequence) {
            ((android.media.MediaDescription.Builder) obj).setSubtitle(charSequence);
        }

        @DexIgnore
        /* renamed from: c */
        public static void m7215c(java.lang.Object obj, java.lang.CharSequence charSequence) {
            ((android.media.MediaDescription.Builder) obj).setTitle(charSequence);
        }

        @DexIgnore
        /* renamed from: a */
        public static void m7213a(java.lang.Object obj, java.lang.String str) {
            ((android.media.MediaDescription.Builder) obj).setMediaId(str);
        }

        @DexIgnore
        /* renamed from: a */
        public static void m7212a(java.lang.Object obj, java.lang.CharSequence charSequence) {
            ((android.media.MediaDescription.Builder) obj).setDescription(charSequence);
        }

        @DexIgnore
        /* renamed from: a */
        public static void m7209a(java.lang.Object obj, android.graphics.Bitmap bitmap) {
            ((android.media.MediaDescription.Builder) obj).setIconBitmap(bitmap);
        }

        @DexIgnore
        /* renamed from: a */
        public static void m7210a(java.lang.Object obj, android.net.Uri uri) {
            ((android.media.MediaDescription.Builder) obj).setIconUri(uri);
        }

        @DexIgnore
        /* renamed from: a */
        public static void m7211a(java.lang.Object obj, android.os.Bundle bundle) {
            ((android.media.MediaDescription.Builder) obj).setExtras(bundle);
        }

        @DexIgnore
        /* renamed from: a */
        public static java.lang.Object m7208a(java.lang.Object obj) {
            return ((android.media.MediaDescription.Builder) obj).build();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.CharSequence m7198a(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getDescription();
    }

    @DexIgnore
    /* renamed from: b */
    public static android.os.Bundle m7201b(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getExtras();
    }

    @DexIgnore
    /* renamed from: c */
    public static android.graphics.Bitmap m7202c(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getIconBitmap();
    }

    @DexIgnore
    /* renamed from: d */
    public static android.net.Uri m7203d(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getIconUri();
    }

    @DexIgnore
    /* renamed from: e */
    public static java.lang.String m7204e(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getMediaId();
    }

    @DexIgnore
    /* renamed from: f */
    public static java.lang.CharSequence m7205f(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getSubtitle();
    }

    @DexIgnore
    /* renamed from: g */
    public static java.lang.CharSequence m7206g(java.lang.Object obj) {
        return ((android.media.MediaDescription) obj).getTitle();
    }

    @DexIgnore
    /* renamed from: a */
    public static void m7200a(java.lang.Object obj, android.os.Parcel parcel, int i) {
        ((android.media.MediaDescription) obj).writeToParcel(parcel, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static java.lang.Object m7199a(android.os.Parcel parcel) {
        return android.media.MediaDescription.CREATOR.createFromParcel(parcel);
    }
}
