package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.wj */
public class C3226wj {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.String f10650a; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("Alarms");

    @DexIgnore
    /* renamed from: a */
    public static void m15852a(android.content.Context context, com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str, long j) {
        com.fossil.blesdk.obfuscated.C1564cl b = tjVar.mo16462g().mo3778b();
        com.fossil.blesdk.obfuscated.C1490bl a = b.mo9556a(str);
        if (a != null) {
            m15853a(context, str, a.f3771b);
            m15854a(context, str, a.f3771b, j);
            return;
        }
        int b2 = new com.fossil.blesdk.obfuscated.C2747ql(context).mo15272b();
        b.mo9557a(new com.fossil.blesdk.obfuscated.C1490bl(str, b2));
        m15854a(context, str, b2, j);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15851a(android.content.Context context, com.fossil.blesdk.obfuscated.C2968tj tjVar, java.lang.String str) {
        com.fossil.blesdk.obfuscated.C1564cl b = tjVar.mo16462g().mo3778b();
        com.fossil.blesdk.obfuscated.C1490bl a = b.mo9556a(str);
        if (a != null) {
            m15853a(context, str, a.f3771b);
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10650a, java.lang.String.format("Removing SystemIdInfo for workSpecId (%s)", new java.lang.Object[]{str}), new java.lang.Throwable[0]);
            b.mo9558b(str);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15853a(android.content.Context context, java.lang.String str, int i) {
        android.app.AlarmManager alarmManager = (android.app.AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        android.app.PendingIntent service = android.app.PendingIntent.getService(context, i, com.fossil.blesdk.obfuscated.C3293xj.m16360a(context, str), 536870912);
        if (service != null && alarmManager != null) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10650a, java.lang.String.format("Cancelling existing alarm with (workSpecId, systemId) (%s, %s)", new java.lang.Object[]{str, java.lang.Integer.valueOf(i)}), new java.lang.Throwable[0]);
            alarmManager.cancel(service);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m15854a(android.content.Context context, java.lang.String str, int i, long j) {
        android.app.AlarmManager alarmManager = (android.app.AlarmManager) context.getSystemService(com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        android.app.PendingIntent service = android.app.PendingIntent.getService(context, i, com.fossil.blesdk.obfuscated.C3293xj.m16360a(context, str), 1073741824);
        if (alarmManager == null) {
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= 19) {
            alarmManager.setExact(0, j, service);
        } else {
            alarmManager.set(0, j, service);
        }
    }
}
