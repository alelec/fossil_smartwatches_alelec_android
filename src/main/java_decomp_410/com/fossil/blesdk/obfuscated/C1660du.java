package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.du */
public class C1660du implements com.fossil.blesdk.obfuscated.C1906gu<android.graphics.Bitmap, android.graphics.drawable.BitmapDrawable> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.res.Resources f4529a;

    @DexIgnore
    public C1660du(android.content.res.Resources resources) {
        com.fossil.blesdk.obfuscated.C2992tw.m14457a(resources);
        this.f4529a = resources;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.drawable.BitmapDrawable> mo9632a(com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> aqVar, com.fossil.blesdk.obfuscated.C2337lo loVar) {
        return com.fossil.blesdk.obfuscated.C1659dt.m6045a(this.f4529a, aqVar);
    }
}
