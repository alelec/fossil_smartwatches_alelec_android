package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y5 */
public final class C3343y5 {
    @DexIgnore
    /* renamed from: a */
    public static java.lang.String m16713a(java.lang.String str) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return android.app.AppOpsManager.permissionToOp(str);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m16712a(android.content.Context context, java.lang.String str, java.lang.String str2) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            return ((android.app.AppOpsManager) context.getSystemService(android.app.AppOpsManager.class)).noteProxyOpNoThrow(str, str2);
        }
        return 1;
    }
}
