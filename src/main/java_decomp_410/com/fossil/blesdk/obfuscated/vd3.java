package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.blesdk.obfuscated.wr2;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vd3 extends zr2 implements ud3 {
    @DexIgnore
    public tr3<yf2> j;
    @DexIgnore
    public td3 k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return "SleepOverviewWeekFragment";
    }

    @DexIgnore
    public boolean S0() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onCreateView");
        this.j = new tr3<>(this, (yf2) qa.a(layoutInflater, R.layout.fragment_sleep_overview_week, viewGroup, false, O0()));
        tr3<yf2> tr3 = this.j;
        if (tr3 != null) {
            yf2 a2 = tr3.a();
            if (a2 != null) {
                return a2.d();
            }
        }
        return null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onResume");
        td3 td3 = this.k;
        if (td3 != null) {
            td3.f();
        }
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onStop");
        td3 td3 = this.k;
        if (td3 != null) {
            td3.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(wr2 wr2) {
        kd4.b(wr2, "baseModel");
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "showWeekDetails");
        tr3<yf2> tr3 = this.j;
        if (tr3 != null) {
            yf2 a2 = tr3.a();
            if (a2 != null) {
                OverviewSleepWeekChart overviewSleepWeekChart = a2.q;
                if (overviewSleepWeekChart != null) {
                    new ArrayList();
                    BarChart.c cVar = (BarChart.c) wr2;
                    cVar.b(wr2.a.a(cVar.c()));
                    wr2.a aVar = wr2.a;
                    kd4.a((Object) overviewSleepWeekChart, "it");
                    Context context = overviewSleepWeekChart.getContext();
                    kd4.a((Object) context, "it.context");
                    BarChart.a((BarChart) overviewSleepWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
                    overviewSleepWeekChart.a(wr2);
                }
            }
        }
    }

    @DexIgnore
    public void a(td3 td3) {
        kd4.b(td3, "presenter");
        this.k = td3;
    }
}
