package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.r8 */
public final class C2790r8 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Field f8873a;

    @DexIgnore
    /* renamed from: b */
    public static boolean f8874b;

    @DexIgnore
    /* renamed from: a */
    public static void m13095a(android.view.LayoutInflater layoutInflater, android.view.LayoutInflater.Factory2 factory2) {
        if (!f8874b) {
            try {
                f8873a = android.view.LayoutInflater.class.getDeclaredField("mFactory2");
                f8873a.setAccessible(true);
            } catch (java.lang.NoSuchFieldException e) {
                android.util.Log.e("LayoutInflaterCompatHC", "forceSetFactory2 Could not find field 'mFactory2' on class " + android.view.LayoutInflater.class.getName() + "; inflation may have unexpected results.", e);
            }
            f8874b = true;
        }
        java.lang.reflect.Field field = f8873a;
        if (field != null) {
            try {
                field.set(layoutInflater, factory2);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.e("LayoutInflaterCompatHC", "forceSetFactory2 could not set the Factory2 on LayoutInflater " + layoutInflater + "; inflation may have unexpected results.", e2);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static void m13096b(android.view.LayoutInflater layoutInflater, android.view.LayoutInflater.Factory2 factory2) {
        layoutInflater.setFactory2(factory2);
        if (android.os.Build.VERSION.SDK_INT < 21) {
            android.view.LayoutInflater.Factory factory = layoutInflater.getFactory();
            if (factory instanceof android.view.LayoutInflater.Factory2) {
                m13095a(layoutInflater, (android.view.LayoutInflater.Factory2) factory);
            } else {
                m13095a(layoutInflater, factory2);
            }
        }
    }
}
