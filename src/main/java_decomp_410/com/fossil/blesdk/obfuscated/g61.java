package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class g61 extends vb1<g61> {
    @DexIgnore
    public long[] c;
    @DexIgnore
    public long[] d;
    @DexIgnore
    public b61[] e; // = b61.e();
    @DexIgnore
    public h61[] f; // = h61.e();

    @DexIgnore
    public g61() {
        long[] jArr = dc1.b;
        this.c = jArr;
        this.d = jArr;
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        long[] jArr = this.c;
        int i = 0;
        if (jArr != null && jArr.length > 0) {
            int i2 = 0;
            while (true) {
                long[] jArr2 = this.c;
                if (i2 >= jArr2.length) {
                    break;
                }
                ub1.a(1, jArr2[i2]);
                i2++;
            }
        }
        long[] jArr3 = this.d;
        if (jArr3 != null && jArr3.length > 0) {
            int i3 = 0;
            while (true) {
                long[] jArr4 = this.d;
                if (i3 >= jArr4.length) {
                    break;
                }
                ub1.a(2, jArr4[i3]);
                i3++;
            }
        }
        b61[] b61Arr = this.e;
        if (b61Arr != null && b61Arr.length > 0) {
            int i4 = 0;
            while (true) {
                b61[] b61Arr2 = this.e;
                if (i4 >= b61Arr2.length) {
                    break;
                }
                b61 b61 = b61Arr2[i4];
                if (b61 != null) {
                    ub1.a(3, (ac1) b61);
                }
                i4++;
            }
        }
        h61[] h61Arr = this.f;
        if (h61Arr != null && h61Arr.length > 0) {
            while (true) {
                h61[] h61Arr2 = this.f;
                if (i >= h61Arr2.length) {
                    break;
                }
                h61 h61 = h61Arr2[i];
                if (h61 != null) {
                    ub1.a(4, (ac1) h61);
                }
                i++;
            }
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof g61)) {
            return false;
        }
        g61 g61 = (g61) obj;
        if (!zb1.a(this.c, g61.c) || !zb1.a(this.d, g61.d) || !zb1.a((Object[]) this.e, (Object[]) g61.e) || !zb1.a((Object[]) this.f, (Object[]) g61.f)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(g61.b);
        }
        xb1 xb12 = g61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((((((((g61.class.getName().hashCode() + 527) * 31) + zb1.a(this.c)) * 31) + zb1.a(this.d)) * 31) + zb1.a((Object[]) this.e)) * 31) + zb1.a((Object[]) this.f)) * 31;
        xb1 xb1 = this.b;
        return hashCode + ((xb1 == null || xb1.a()) ? 0 : this.b.hashCode());
    }

    @DexIgnore
    public final int a() {
        long[] jArr;
        long[] jArr2;
        int a = super.a();
        long[] jArr3 = this.c;
        int i = 0;
        if (jArr3 != null && jArr3.length > 0) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                jArr2 = this.c;
                if (i2 >= jArr2.length) {
                    break;
                }
                i3 += ub1.b(jArr2[i2]);
                i2++;
            }
            a = a + i3 + (jArr2.length * 1);
        }
        long[] jArr4 = this.d;
        if (jArr4 != null && jArr4.length > 0) {
            int i4 = 0;
            int i5 = 0;
            while (true) {
                jArr = this.d;
                if (i4 >= jArr.length) {
                    break;
                }
                i5 += ub1.b(jArr[i4]);
                i4++;
            }
            a = a + i5 + (jArr.length * 1);
        }
        b61[] b61Arr = this.e;
        if (b61Arr != null && b61Arr.length > 0) {
            int i6 = a;
            int i7 = 0;
            while (true) {
                b61[] b61Arr2 = this.e;
                if (i7 >= b61Arr2.length) {
                    break;
                }
                b61 b61 = b61Arr2[i7];
                if (b61 != null) {
                    i6 += ub1.b(3, (ac1) b61);
                }
                i7++;
            }
            a = i6;
        }
        h61[] h61Arr = this.f;
        if (h61Arr != null && h61Arr.length > 0) {
            while (true) {
                h61[] h61Arr2 = this.f;
                if (i >= h61Arr2.length) {
                    break;
                }
                h61 h61 = h61Arr2[i];
                if (h61 != null) {
                    a += ub1.b(4, (ac1) h61);
                }
                i++;
            }
        }
        return a;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                int a = dc1.a(tb1, 8);
                long[] jArr = this.c;
                int length = jArr == null ? 0 : jArr.length;
                long[] jArr2 = new long[(a + length)];
                if (length != 0) {
                    System.arraycopy(this.c, 0, jArr2, 0, length);
                }
                while (length < jArr2.length - 1) {
                    jArr2[length] = tb1.f();
                    tb1.c();
                    length++;
                }
                jArr2[length] = tb1.f();
                this.c = jArr2;
            } else if (c2 == 10) {
                int c3 = tb1.c(tb1.e());
                int a2 = tb1.a();
                int i = 0;
                while (tb1.l() > 0) {
                    tb1.f();
                    i++;
                }
                tb1.f(a2);
                long[] jArr3 = this.c;
                int length2 = jArr3 == null ? 0 : jArr3.length;
                long[] jArr4 = new long[(i + length2)];
                if (length2 != 0) {
                    System.arraycopy(this.c, 0, jArr4, 0, length2);
                }
                while (length2 < jArr4.length) {
                    jArr4[length2] = tb1.f();
                    length2++;
                }
                this.c = jArr4;
                tb1.d(c3);
            } else if (c2 == 16) {
                int a3 = dc1.a(tb1, 16);
                long[] jArr5 = this.d;
                int length3 = jArr5 == null ? 0 : jArr5.length;
                long[] jArr6 = new long[(a3 + length3)];
                if (length3 != 0) {
                    System.arraycopy(this.d, 0, jArr6, 0, length3);
                }
                while (length3 < jArr6.length - 1) {
                    jArr6[length3] = tb1.f();
                    tb1.c();
                    length3++;
                }
                jArr6[length3] = tb1.f();
                this.d = jArr6;
            } else if (c2 == 18) {
                int c4 = tb1.c(tb1.e());
                int a4 = tb1.a();
                int i2 = 0;
                while (tb1.l() > 0) {
                    tb1.f();
                    i2++;
                }
                tb1.f(a4);
                long[] jArr7 = this.d;
                int length4 = jArr7 == null ? 0 : jArr7.length;
                long[] jArr8 = new long[(i2 + length4)];
                if (length4 != 0) {
                    System.arraycopy(this.d, 0, jArr8, 0, length4);
                }
                while (length4 < jArr8.length) {
                    jArr8[length4] = tb1.f();
                    length4++;
                }
                this.d = jArr8;
                tb1.d(c4);
            } else if (c2 == 26) {
                int a5 = dc1.a(tb1, 26);
                b61[] b61Arr = this.e;
                int length5 = b61Arr == null ? 0 : b61Arr.length;
                b61[] b61Arr2 = new b61[(a5 + length5)];
                if (length5 != 0) {
                    System.arraycopy(this.e, 0, b61Arr2, 0, length5);
                }
                while (length5 < b61Arr2.length - 1) {
                    b61Arr2[length5] = new b61();
                    tb1.a((ac1) b61Arr2[length5]);
                    tb1.c();
                    length5++;
                }
                b61Arr2[length5] = new b61();
                tb1.a((ac1) b61Arr2[length5]);
                this.e = b61Arr2;
            } else if (c2 == 34) {
                int a6 = dc1.a(tb1, 34);
                h61[] h61Arr = this.f;
                int length6 = h61Arr == null ? 0 : h61Arr.length;
                h61[] h61Arr2 = new h61[(a6 + length6)];
                if (length6 != 0) {
                    System.arraycopy(this.f, 0, h61Arr2, 0, length6);
                }
                while (length6 < h61Arr2.length - 1) {
                    h61Arr2[length6] = new h61();
                    tb1.a((ac1) h61Arr2[length6]);
                    tb1.c();
                    length6++;
                }
                h61Arr2[length6] = new h61();
                tb1.a((ac1) h61Arr2[length6]);
                this.f = h61Arr2;
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
