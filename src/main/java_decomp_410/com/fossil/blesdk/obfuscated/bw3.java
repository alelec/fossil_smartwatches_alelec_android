package com.fossil.blesdk.obfuscated;

import com.squareup.okhttp.internal.framed.ErrorCode;
import com.squareup.okhttp.internal.framed.HeadersMode;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bw3 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ aw3 d;
    @DexIgnore
    public List<cw3> e;
    @DexIgnore
    public /* final */ c f;
    @DexIgnore
    public /* final */ b g;
    @DexIgnore
    public /* final */ d h; // = new d();
    @DexIgnore
    public /* final */ d i; // = new d();
    @DexIgnore
    public ErrorCode j; // = null;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements xo4 {
        @DexIgnore
        public /* final */ jo4 e; // = new jo4();
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public b() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
            if (r8.e.B() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0027, code lost:
            if (r8.e.B() <= 0) goto L_0x0040;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
            com.fossil.blesdk.obfuscated.bw3.d(r8.h).a(com.fossil.blesdk.obfuscated.bw3.e(r8.h), true, (com.fossil.blesdk.obfuscated.jo4) null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
            r2 = r8.h;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0042, code lost:
            monitor-enter(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r8.f = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0045, code lost:
            monitor-exit(r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0046, code lost:
            com.fossil.blesdk.obfuscated.bw3.d(r8.h).flush();
            com.fossil.blesdk.obfuscated.bw3.a(r8.h);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0054, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
            if (r8.h.g.g != false) goto L_0x0040;
         */
        @DexIgnore
        public void close() throws IOException {
            synchronized (bw3.this) {
                if (this.f) {
                }
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            synchronized (bw3.this) {
                bw3.this.b();
            }
            while (this.e.B() > 0) {
                a(false);
                bw3.this.d.flush();
            }
        }

        @DexIgnore
        public zo4 b() {
            return bw3.this.i;
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            this.e.a(jo4, j);
            while (this.e.B() >= 16384) {
                a(false);
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void a(boolean z) throws IOException {
            long min;
            synchronized (bw3.this) {
                bw3.this.i.g();
                while (bw3.this.b <= 0 && !this.g && !this.f && bw3.this.j == null) {
                    try {
                        bw3.this.k();
                    } catch (Throwable th) {
                        bw3.this.i.k();
                        throw th;
                    }
                }
                bw3.this.i.k();
                bw3.this.b();
                min = Math.min(bw3.this.b, this.e.B());
                bw3.this.b -= min;
            }
            bw3.this.i.g();
            try {
                bw3.this.d.a(bw3.this.c, z && min == this.e.B(), this.e, min);
            } finally {
                bw3.this.i.k();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements yo4 {
        @DexIgnore
        public /* final */ jo4 e;
        @DexIgnore
        public /* final */ jo4 f;
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public final void c() throws IOException {
            if (this.h) {
                throw new IOException("stream closed");
            } else if (bw3.this.j != null) {
                throw new IOException("stream was reset: " + bw3.this.j);
            }
        }

        @DexIgnore
        public void close() throws IOException {
            synchronized (bw3.this) {
                this.h = true;
                this.f.w();
                bw3.this.notifyAll();
            }
            bw3.this.a();
        }

        @DexIgnore
        public final void d() throws IOException {
            bw3.this.h.g();
            while (this.f.B() == 0 && !this.i && !this.h && bw3.this.j == null) {
                try {
                    bw3.this.k();
                } finally {
                    bw3.this.h.k();
                }
            }
        }

        @DexIgnore
        public c(long j2) {
            this.e = new jo4();
            this.f = new jo4();
            this.g = j2;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x0065, code lost:
            r11 = com.fossil.blesdk.obfuscated.bw3.d(r8.j);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x006b, code lost:
            monitor-enter(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            com.fossil.blesdk.obfuscated.bw3.d(r8.j).q += r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0090, code lost:
            if (com.fossil.blesdk.obfuscated.bw3.d(r8.j).q < ((long) (com.fossil.blesdk.obfuscated.bw3.d(r8.j).s.c(65536) / 2))) goto L_0x00ac;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0092, code lost:
            com.fossil.blesdk.obfuscated.bw3.d(r8.j).c(0, com.fossil.blesdk.obfuscated.bw3.d(r8.j).q);
            com.fossil.blesdk.obfuscated.bw3.d(r8.j).q = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00ac, code lost:
            monitor-exit(r11);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00ad, code lost:
            return r9;
         */
        @DexIgnore
        public long b(jo4 jo4, long j2) throws IOException {
            if (j2 >= 0) {
                synchronized (bw3.this) {
                    d();
                    c();
                    if (this.f.B() == 0) {
                        return -1;
                    }
                    long b = this.f.b(jo4, Math.min(j2, this.f.B()));
                    bw3.this.a += b;
                    if (bw3.this.a >= ((long) (bw3.this.d.s.c(65536) / 2))) {
                        bw3.this.d.c(bw3.this.c, bw3.this.a);
                        bw3.this.a = 0;
                    }
                }
            } else {
                throw new IllegalArgumentException("byteCount < 0: " + j2);
            }
        }

        @DexIgnore
        public void a(lo4 lo4, long j2) throws IOException {
            boolean z;
            boolean z2;
            boolean z3;
            while (j2 > 0) {
                synchronized (bw3.this) {
                    z = this.i;
                    z2 = true;
                    z3 = this.f.B() + j2 > this.g;
                }
                if (z3) {
                    lo4.skip(j2);
                    bw3.this.c(ErrorCode.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    lo4.skip(j2);
                    return;
                } else {
                    long b = lo4.b(this.e, j2);
                    if (b != -1) {
                        j2 -= b;
                        synchronized (bw3.this) {
                            if (this.f.B() != 0) {
                                z2 = false;
                            }
                            this.f.a((yo4) this.e);
                            if (z2) {
                                bw3.this.notifyAll();
                            }
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        @DexIgnore
        public zo4 b() {
            return bw3.this.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ho4 {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public IOException b(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        public void i() {
            bw3.this.c(ErrorCode.CANCEL);
        }

        @DexIgnore
        public void k() throws IOException {
            if (h()) {
                throw b((IOException) null);
            }
        }
    }

    @DexIgnore
    public bw3(int i2, aw3 aw3, boolean z, boolean z2, List<cw3> list) {
        if (aw3 == null) {
            throw new NullPointerException("connection == null");
        } else if (list != null) {
            this.c = i2;
            this.d = aw3;
            this.b = (long) aw3.t.c(65536);
            this.f = new c((long) aw3.s.c(65536));
            this.g = new b();
            boolean unused = this.f.i = z2;
            boolean unused2 = this.g.g = z;
        } else {
            throw new NullPointerException("requestHeaders == null");
        }
    }

    @DexIgnore
    public zo4 i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        boolean h2;
        synchronized (this) {
            boolean unused = this.f.i = true;
            h2 = h();
            notifyAll();
        }
        if (!h2) {
            this.d.e(this.c);
        }
    }

    @DexIgnore
    public final void k() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException unused) {
            throw new InterruptedIOException();
        }
    }

    @DexIgnore
    public void a(ErrorCode errorCode) throws IOException {
        if (b(errorCode)) {
            this.d.c(this.c, errorCode);
        }
    }

    @DexIgnore
    public final boolean b(ErrorCode errorCode) {
        synchronized (this) {
            if (this.j != null) {
                return false;
            }
            if (this.f.i && this.g.g) {
                return false;
            }
            this.j = errorCode;
            notifyAll();
            this.d.e(this.c);
            return true;
        }
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0038, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        r3.h.k();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003e, code lost:
        throw r0;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public synchronized List<cw3> d() throws IOException {
        this.h.g();
        while (this.e == null && this.j == null) {
            k();
        }
        this.h.k();
        if (this.e != null) {
        } else {
            throw new IOException("stream was reset: " + this.j);
        }
        return this.e;
    }

    @DexIgnore
    public xo4 e() {
        synchronized (this) {
            if (this.e == null) {
                if (!g()) {
                    throw new IllegalStateException("reply before requesting the sink");
                }
            }
        }
        return this.g;
    }

    @DexIgnore
    public yo4 f() {
        return this.f;
    }

    @DexIgnore
    public boolean g() {
        if (this.d.f == ((this.c & 1) == 1)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public synchronized boolean h() {
        if (this.j != null) {
            return false;
        }
        if ((this.f.i || this.f.h) && ((this.g.g || this.g.f) && this.e != null)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public void c(ErrorCode errorCode) {
        if (b(errorCode)) {
            this.d.d(this.c, errorCode);
        }
    }

    @DexIgnore
    public void a(List<cw3> list, HeadersMode headersMode) {
        ErrorCode errorCode = null;
        boolean z = true;
        synchronized (this) {
            if (this.e == null) {
                if (headersMode.failIfHeadersAbsent()) {
                    errorCode = ErrorCode.PROTOCOL_ERROR;
                } else {
                    this.e = list;
                    z = h();
                    notifyAll();
                }
            } else if (headersMode.failIfHeadersPresent()) {
                errorCode = ErrorCode.STREAM_IN_USE;
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.e);
                arrayList.addAll(list);
                this.e = arrayList;
            }
        }
        if (errorCode != null) {
            c(errorCode);
        } else if (!z) {
            this.d.e(this.c);
        }
    }

    @DexIgnore
    public synchronized void d(ErrorCode errorCode) {
        if (this.j == null) {
            this.j = errorCode;
            notifyAll();
        }
    }

    @DexIgnore
    public final void b() throws IOException {
        if (this.g.f) {
            throw new IOException("stream closed");
        } else if (this.g.g) {
            throw new IOException("stream finished");
        } else if (this.j != null) {
            throw new IOException("stream was reset: " + this.j);
        }
    }

    @DexIgnore
    public void a(lo4 lo4, int i2) throws IOException {
        this.f.a(lo4, (long) i2);
    }

    @DexIgnore
    public final void a() throws IOException {
        boolean z;
        boolean h2;
        synchronized (this) {
            z = !this.f.i && this.f.h && (this.g.g || this.g.f);
            h2 = h();
        }
        if (z) {
            a(ErrorCode.CANCEL);
        } else if (!h2) {
            this.d.e(this.c);
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }
}
