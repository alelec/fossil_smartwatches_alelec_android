package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.concurrency.Priority;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class g64 implements a64<i64>, f64, i64 {
    @DexIgnore
    public /* final */ List<i64> e; // = new ArrayList();
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicReference<Throwable> g; // = new AtomicReference<>((Object) null);

    @DexIgnore
    public boolean b() {
        for (i64 a : c()) {
            if (!a.a()) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public synchronized Collection<i64> c() {
        return Collections.unmodifiableCollection(this.e);
    }

    @DexIgnore
    public int compareTo(Object obj) {
        return Priority.compareTo(this, obj);
    }

    @DexIgnore
    public Priority getPriority() {
        return Priority.NORMAL;
    }

    @DexIgnore
    public synchronized void a(i64 i64) {
        this.e.add(i64);
    }

    @DexIgnore
    public static boolean b(Object obj) {
        try {
            a64 a64 = (a64) obj;
            i64 i64 = (i64) obj;
            f64 f64 = (f64) obj;
            if (a64 == null || i64 == null || f64 == null) {
                return false;
            }
            return true;
        } catch (ClassCastException unused) {
            return false;
        }
    }

    @DexIgnore
    public synchronized void a(boolean z) {
        this.f.set(z);
    }

    @DexIgnore
    public boolean a() {
        return this.f.get();
    }

    @DexIgnore
    public void a(Throwable th) {
        this.g.set(th);
    }
}
