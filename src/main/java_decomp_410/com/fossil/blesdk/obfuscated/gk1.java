package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gk1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ kl1 f;
    @DexIgnore
    public /* final */ /* synthetic */ rl1 g;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 h;

    @DexIgnore
    public gk1(vj1 vj1, boolean z, kl1 kl1, rl1 rl1) {
        this.h = vj1;
        this.e = z;
        this.f = kl1;
        this.g = rl1;
    }

    @DexIgnore
    public final void run() {
        kg1 d = this.h.d;
        if (d == null) {
            this.h.d().s().a("Discarding data. Failed to set user attribute");
            return;
        }
        this.h.a(d, this.e ? null : this.f, this.g);
        this.h.C();
    }
}
