package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.z4 */
public class C3413z4 {

    @DexIgnore
    /* renamed from: a */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11487a;

    @DexIgnore
    /* renamed from: b */
    public int f11488b; // = -1;

    @DexIgnore
    /* renamed from: c */
    public int f11489c; // = -1;

    @DexIgnore
    /* renamed from: d */
    public boolean f11490d; // = false;

    @DexIgnore
    /* renamed from: e */
    public /* final */ int[] f11491e; // = {this.f11488b, this.f11489c};

    @DexIgnore
    /* renamed from: f */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11492f; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: g */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11493g; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: h */
    public java.util.HashSet<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11494h; // = new java.util.HashSet<>();

    @DexIgnore
    /* renamed from: i */
    public java.util.HashSet<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11495i; // = new java.util.HashSet<>();

    @DexIgnore
    /* renamed from: j */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11496j; // = new java.util.ArrayList();

    @DexIgnore
    /* renamed from: k */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> f11497k; // = new java.util.ArrayList();

    @DexIgnore
    public C3413z4(java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> list) {
        this.f11487a = list;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> mo18436a(int i) {
        if (i == 0) {
            return this.f11492f;
        }
        if (i == 1) {
            return this.f11493g;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: b */
    public java.util.Set<androidx.constraintlayout.solver.widgets.ConstraintWidget> mo18440b(int i) {
        if (i == 0) {
            return this.f11494h;
        }
        if (i == 1) {
            return this.f11495i;
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo18438a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget, int i) {
        if (i == 0) {
            this.f11494h.add(constraintWidget);
        } else if (i == 1) {
            this.f11495i.add(constraintWidget);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo18441b() {
        int size = this.f11497k.size();
        for (int i = 0; i < size; i++) {
            mo18437a(this.f11497k.get(i));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> mo18435a() {
        if (!this.f11496j.isEmpty()) {
            return this.f11496j;
        }
        int size = this.f11487a.size();
        for (int i = 0; i < size; i++) {
            androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget = this.f11487a.get(i);
            if (!constraintWidget.f692b0) {
                mo18439a((java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget>) (java.util.ArrayList) this.f11496j, constraintWidget);
            }
        }
        this.f11497k.clear();
        this.f11497k.addAll(this.f11487a);
        this.f11497k.removeAll(this.f11496j);
        return this.f11496j;
    }

    @DexIgnore
    public C3413z4(java.util.List<androidx.constraintlayout.solver.widgets.ConstraintWidget> list, boolean z) {
        this.f11487a = list;
        this.f11490d = z;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo18439a(java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> arrayList, androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        if (!constraintWidget.f696d0) {
            arrayList.add(constraintWidget);
            constraintWidget.f696d0 = true;
            if (!constraintWidget.mo1360z()) {
                if (constraintWidget instanceof com.fossil.blesdk.obfuscated.C1464b5) {
                    com.fossil.blesdk.obfuscated.C1464b5 b5Var = (com.fossil.blesdk.obfuscated.C1464b5) constraintWidget;
                    int i = b5Var.f3674l0;
                    for (int i2 = 0; i2 < i; i2++) {
                        mo18439a(arrayList, b5Var.f3673k0[i2]);
                    }
                }
                for (androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor : constraintWidget.f663A) {
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2 = constraintAnchor.f652d;
                    if (constraintAnchor2 != null) {
                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintAnchor2.f650b;
                        if (!(constraintAnchor2 == null || constraintWidget2 == constraintWidget.mo1336l())) {
                            mo18439a(arrayList, constraintWidget2);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0087  */
    /* renamed from: a */
    public final void mo18437a(androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget) {
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor;
        int i;
        int i2;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor2;
        androidx.constraintlayout.solver.widgets.ConstraintAnchor constraintAnchor3;
        int i3;
        if (constraintWidget.f692b0 && !constraintWidget.mo1360z()) {
            boolean z = false;
            boolean z2 = constraintWidget.f718u.f652d != null;
            if (z2) {
                constraintAnchor = constraintWidget.f718u.f652d;
            } else {
                constraintAnchor = constraintWidget.f716s.f652d;
            }
            if (constraintAnchor != null) {
                androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget2 = constraintAnchor.f650b;
                if (!constraintWidget2.f694c0) {
                    mo18437a(constraintWidget2);
                }
                androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type type = constraintAnchor.f651c;
                if (type == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.RIGHT) {
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget3 = constraintAnchor.f650b;
                    i = constraintWidget3.mo1352t() + constraintWidget3.f671I;
                } else if (type == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.LEFT) {
                    i = constraintAnchor.f650b.f671I;
                }
                if (!z2) {
                    i2 = i - constraintWidget.f718u.mo1264b();
                } else {
                    i2 = i + constraintWidget.f716s.mo1264b() + constraintWidget.mo1352t();
                }
                constraintWidget.mo1288a(i2 - constraintWidget.mo1352t(), i2);
                constraintAnchor2 = constraintWidget.f720w.f652d;
                if (constraintAnchor2 == null) {
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget4 = constraintAnchor2.f650b;
                    if (!constraintWidget4.f694c0) {
                        mo18437a(constraintWidget4);
                    }
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget5 = constraintAnchor2.f650b;
                    int i4 = (constraintWidget5.f672J + constraintWidget5.f679Q) - constraintWidget.f679Q;
                    constraintWidget.mo1323e(i4, constraintWidget.f668F + i4);
                    constraintWidget.f694c0 = true;
                    return;
                }
                if (constraintWidget.f719v.f652d != null) {
                    z = true;
                }
                if (z) {
                    constraintAnchor3 = constraintWidget.f719v.f652d;
                } else {
                    constraintAnchor3 = constraintWidget.f717t.f652d;
                }
                if (constraintAnchor3 != null) {
                    androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget6 = constraintAnchor3.f650b;
                    if (!constraintWidget6.f694c0) {
                        mo18437a(constraintWidget6);
                    }
                    androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type type2 = constraintAnchor3.f651c;
                    if (type2 == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.BOTTOM) {
                        androidx.constraintlayout.solver.widgets.ConstraintWidget constraintWidget7 = constraintAnchor3.f650b;
                        i2 = constraintWidget7.f672J + constraintWidget7.mo1332j();
                    } else if (type2 == androidx.constraintlayout.solver.widgets.ConstraintAnchor.Type.TOP) {
                        i2 = constraintAnchor3.f650b.f672J;
                    }
                }
                if (z) {
                    i3 = i2 - constraintWidget.f719v.mo1264b();
                } else {
                    i3 = i2 + constraintWidget.f717t.mo1264b() + constraintWidget.mo1332j();
                }
                constraintWidget.mo1323e(i3 - constraintWidget.mo1332j(), i3);
                constraintWidget.f694c0 = true;
                return;
            }
            i = 0;
            if (!z2) {
            }
            constraintWidget.mo1288a(i2 - constraintWidget.mo1352t(), i2);
            constraintAnchor2 = constraintWidget.f720w.f652d;
            if (constraintAnchor2 == null) {
            }
        }
    }
}
