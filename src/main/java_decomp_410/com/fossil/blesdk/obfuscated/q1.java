package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface q1 {

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(k1 k1Var, int i);

        @DexIgnore
        boolean a();

        @DexIgnore
        k1 getItemData();
    }

    @DexIgnore
    void a(h1 h1Var);
}
