package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class j52 implements Factory<MigrationManager> {
    @DexIgnore
    public /* final */ n42 a;
    @DexIgnore
    public /* final */ Provider<en2> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<GetHybridDeviceSettingUseCase> d;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> e;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> f;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> g;
    @DexIgnore
    public /* final */ Provider<GoalTrackingDatabase> h;
    @DexIgnore
    public /* final */ Provider<DeviceDao> i;
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> j;
    @DexIgnore
    public /* final */ Provider<MicroAppLastSettingRepository> k;
    @DexIgnore
    public /* final */ Provider<vj2> l;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> m;

    @DexIgnore
    public j52(n42 n42, Provider<en2> provider, Provider<UserRepository> provider2, Provider<GetHybridDeviceSettingUseCase> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<GoalTrackingDatabase> provider7, Provider<DeviceDao> provider8, Provider<HybridCustomizeDatabase> provider9, Provider<MicroAppLastSettingRepository> provider10, Provider<vj2> provider11, Provider<AlarmsRepository> provider12) {
        this.a = n42;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
        this.j = provider9;
        this.k = provider10;
        this.l = provider11;
        this.m = provider12;
    }

    @DexIgnore
    public static j52 a(n42 n42, Provider<en2> provider, Provider<UserRepository> provider2, Provider<GetHybridDeviceSettingUseCase> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<GoalTrackingDatabase> provider7, Provider<DeviceDao> provider8, Provider<HybridCustomizeDatabase> provider9, Provider<MicroAppLastSettingRepository> provider10, Provider<vj2> provider11, Provider<AlarmsRepository> provider12) {
        return new j52(n42, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12);
    }

    @DexIgnore
    public static MigrationManager b(n42 n42, Provider<en2> provider, Provider<UserRepository> provider2, Provider<GetHybridDeviceSettingUseCase> provider3, Provider<NotificationsRepository> provider4, Provider<PortfolioApp> provider5, Provider<GoalTrackingRepository> provider6, Provider<GoalTrackingDatabase> provider7, Provider<DeviceDao> provider8, Provider<HybridCustomizeDatabase> provider9, Provider<MicroAppLastSettingRepository> provider10, Provider<vj2> provider11, Provider<AlarmsRepository> provider12) {
        return a(n42, provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get(), provider10.get(), provider11.get(), provider12.get());
    }

    @DexIgnore
    public static MigrationManager a(n42 n42, en2 en2, UserRepository userRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, NotificationsRepository notificationsRepository, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, vj2 vj2, AlarmsRepository alarmsRepository) {
        MigrationManager a2 = n42.a(en2, userRepository, getHybridDeviceSettingUseCase, notificationsRepository, portfolioApp, goalTrackingRepository, goalTrackingDatabase, deviceDao, hybridCustomizeDatabase, microAppLastSettingRepository, vj2, alarmsRepository);
        n44.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public MigrationManager get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m);
    }
}
