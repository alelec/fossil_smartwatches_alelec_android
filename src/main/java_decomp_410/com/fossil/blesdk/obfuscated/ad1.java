package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ad1 implements Parcelable.Creator<LocationRequest> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        long j4 = 0;
        int i = 102;
        boolean z = false;
        int i2 = Integer.MAX_VALUE;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 1:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 2:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 3:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 4:
                    z = SafeParcelReader.i(parcel2, a);
                    break;
                case 5:
                    j3 = SafeParcelReader.s(parcel2, a);
                    break;
                case 6:
                    i2 = SafeParcelReader.q(parcel2, a);
                    break;
                case 7:
                    f = SafeParcelReader.n(parcel2, a);
                    break;
                case 8:
                    j4 = SafeParcelReader.s(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new LocationRequest(i, j, j2, z, j3, i2, f, j4);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationRequest[i];
    }
}
