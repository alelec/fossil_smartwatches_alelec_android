package com.fossil.blesdk.obfuscated;

import androidx.work.BackoffPolicy;
import androidx.work.WorkInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class hl {
    @DexIgnore
    public String a;
    @DexIgnore
    public WorkInfo.State b; // = WorkInfo.State.ENQUEUED;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public aj e;
    @DexIgnore
    public aj f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public long i;
    @DexIgnore
    public yi j;
    @DexIgnore
    public int k;
    @DexIgnore
    public BackoffPolicy l;
    @DexIgnore
    public long m;
    @DexIgnore
    public long n;
    @DexIgnore
    public long o;
    @DexIgnore
    public long p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements m3<List<c>, List<WorkInfo>> {
        @DexIgnore
        /* renamed from: a */
        public List<WorkInfo> apply(List<c> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (c a : list) {
                arrayList.add(a.a());
            }
            return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public String a;
        @DexIgnore
        public WorkInfo.State b;

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || b.class != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b != bVar.b) {
                return false;
            }
            return this.a.equals(bVar.a);
        }

        @DexIgnore
        public int hashCode() {
            return (this.a.hashCode() * 31) + this.b.hashCode();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public String a;
        @DexIgnore
        public WorkInfo.State b;
        @DexIgnore
        public aj c;
        @DexIgnore
        public int d;
        @DexIgnore
        public List<String> e;
        @DexIgnore
        public List<aj> f;

        @DexIgnore
        public WorkInfo a() {
            List<aj> list = this.f;
            return new WorkInfo(UUID.fromString(this.a), this.b, this.c, this.e, (list == null || list.isEmpty()) ? aj.c : this.f.get(0), this.d);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || c.class != obj.getClass()) {
                return false;
            }
            c cVar = (c) obj;
            if (this.d != cVar.d) {
                return false;
            }
            String str = this.a;
            if (str == null ? cVar.a != null : !str.equals(cVar.a)) {
                return false;
            }
            if (this.b != cVar.b) {
                return false;
            }
            aj ajVar = this.c;
            if (ajVar == null ? cVar.c != null : !ajVar.equals(cVar.c)) {
                return false;
            }
            List<String> list = this.e;
            if (list == null ? cVar.e != null : !list.equals(cVar.e)) {
                return false;
            }
            List<aj> list2 = this.f;
            List<aj> list3 = cVar.f;
            if (list2 != null) {
                return list2.equals(list3);
            }
            if (list3 == null) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            WorkInfo.State state = this.b;
            int hashCode2 = (hashCode + (state != null ? state.hashCode() : 0)) * 31;
            aj ajVar = this.c;
            int hashCode3 = (((hashCode2 + (ajVar != null ? ajVar.hashCode() : 0)) * 31) + this.d) * 31;
            List<String> list = this.e;
            int hashCode4 = (hashCode3 + (list != null ? list.hashCode() : 0)) * 31;
            List<aj> list2 = this.f;
            if (list2 != null) {
                i = list2.hashCode();
            }
            return hashCode4 + i;
        }
    }

    /*
    static {
        dj.a("WorkSpec");
        new a();
    }
    */

    @DexIgnore
    public hl(String str, String str2) {
        aj ajVar = aj.c;
        this.e = ajVar;
        this.f = ajVar;
        this.j = yi.i;
        this.l = BackoffPolicy.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = str;
        this.c = str2;
    }

    @DexIgnore
    public long a() {
        long j2;
        boolean z = false;
        if (c()) {
            if (this.l == BackoffPolicy.LINEAR) {
                z = true;
            }
            if (z) {
                j2 = this.m * ((long) this.k);
            } else {
                j2 = (long) Math.scalb((float) this.m, this.k - 1);
            }
            return this.n + Math.min(18000000, j2);
        }
        long j3 = 0;
        if (d()) {
            long currentTimeMillis = System.currentTimeMillis();
            long j4 = this.n;
            if (j4 == 0) {
                j4 = this.g + currentTimeMillis;
            }
            if (this.i != this.h) {
                z = true;
            }
            if (z) {
                if (this.n == 0) {
                    j3 = this.i * -1;
                }
                return j4 + this.h + j3;
            }
            if (this.n != 0) {
                j3 = this.h;
            }
            return j4 + j3;
        }
        long j5 = this.n;
        if (j5 == 0) {
            j5 = System.currentTimeMillis();
        }
        return j5 + this.g;
    }

    @DexIgnore
    public boolean b() {
        return !yi.i.equals(this.j);
    }

    @DexIgnore
    public boolean c() {
        return this.b == WorkInfo.State.ENQUEUED && this.k > 0;
    }

    @DexIgnore
    public boolean d() {
        return this.h != 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || hl.class != obj.getClass()) {
            return false;
        }
        hl hlVar = (hl) obj;
        if (this.g != hlVar.g || this.h != hlVar.h || this.i != hlVar.i || this.k != hlVar.k || this.m != hlVar.m || this.n != hlVar.n || this.o != hlVar.o || this.p != hlVar.p || !this.a.equals(hlVar.a) || this.b != hlVar.b || !this.c.equals(hlVar.c)) {
            return false;
        }
        String str = this.d;
        if (str == null ? hlVar.d != null : !str.equals(hlVar.d)) {
            return false;
        }
        if (this.e.equals(hlVar.e) && this.f.equals(hlVar.f) && this.j.equals(hlVar.j) && this.l == hlVar.l) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31;
        String str = this.d;
        int hashCode2 = str != null ? str.hashCode() : 0;
        long j2 = this.g;
        long j3 = this.h;
        long j4 = this.i;
        long j5 = this.m;
        long j6 = this.n;
        long j7 = this.o;
        long j8 = this.p;
        return ((((((((((((((((((((((((hashCode + hashCode2) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + ((int) (j3 ^ (j3 >>> 32)))) * 31) + ((int) (j4 ^ (j4 >>> 32)))) * 31) + this.j.hashCode()) * 31) + this.k) * 31) + this.l.hashCode()) * 31) + ((int) (j5 ^ (j5 >>> 32)))) * 31) + ((int) (j6 ^ (j6 >>> 32)))) * 31) + ((int) (j7 ^ (j7 >>> 32)))) * 31) + ((int) (j8 ^ (j8 >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "{WorkSpec: " + this.a + "}";
    }

    @DexIgnore
    public hl(hl hlVar) {
        aj ajVar = aj.c;
        this.e = ajVar;
        this.f = ajVar;
        this.j = yi.i;
        this.l = BackoffPolicy.EXPONENTIAL;
        this.m = 30000;
        this.p = -1;
        this.a = hlVar.a;
        this.c = hlVar.c;
        this.b = hlVar.b;
        this.d = hlVar.d;
        this.e = new aj(hlVar.e);
        this.f = new aj(hlVar.f);
        this.g = hlVar.g;
        this.h = hlVar.h;
        this.i = hlVar.i;
        this.j = new yi(hlVar.j);
        this.k = hlVar.k;
        this.l = hlVar.l;
        this.m = hlVar.m;
        this.n = hlVar.n;
        this.o = hlVar.o;
        this.p = hlVar.p;
    }
}
