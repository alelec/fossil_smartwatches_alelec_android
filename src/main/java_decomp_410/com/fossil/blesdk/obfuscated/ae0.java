package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ae0 {
    @DexIgnore
    public static ae0 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public ae0(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static ae0 a(Context context) {
        bk0.a(context);
        synchronized (ae0.class) {
            if (b == null) {
                en0.a(context);
                b = new ae0(context);
            }
        }
        return b;
    }

    @DexIgnore
    public boolean a(int i) {
        on0 on0;
        String[] a2 = bn0.b(this.a).a(i);
        if (a2 != null && a2.length != 0) {
            on0 = null;
            for (String a3 : a2) {
                on0 = a(a3, i);
                if (on0.a) {
                    break;
                }
            }
        } else {
            on0 = on0.a("no pkgs");
        }
        on0.b();
        return on0.a;
    }

    @DexIgnore
    public static boolean a(PackageInfo packageInfo, boolean z) {
        gn0 gn0;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                gn0 = a(packageInfo, jn0.a);
            } else {
                gn0 = a(packageInfo, jn0.a[0]);
            }
            if (gn0 != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        if (a(packageInfo, true)) {
            if (zd0.honorsDebugCertificates(this.a)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }

    @DexIgnore
    public final on0 a(String str, int i) {
        try {
            PackageInfo a2 = bn0.b(this.a).a(str, 64, i);
            boolean honorsDebugCertificates = zd0.honorsDebugCertificates(this.a);
            if (a2 == null) {
                return on0.a("null pkg");
            }
            if (a2.signatures.length != 1) {
                return on0.a("single cert required");
            }
            hn0 hn0 = new hn0(a2.signatures[0].toByteArray());
            String str2 = a2.packageName;
            on0 a3 = en0.a(str2, hn0, honorsDebugCertificates, false);
            return (!a3.a || a2.applicationInfo == null || (a2.applicationInfo.flags & 2) == 0 || !en0.a(str2, hn0, false, true).a) ? a3 : on0.a("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException unused) {
            String valueOf = String.valueOf(str);
            return on0.a(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }

    @DexIgnore
    public static gn0 a(PackageInfo packageInfo, gn0... gn0Arr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        hn0 hn0 = new hn0(signatureArr[0].toByteArray());
        for (int i = 0; i < gn0Arr.length; i++) {
            if (gn0Arr[i].equals(hn0)) {
                return gn0Arr[i];
            }
        }
        return null;
    }
}
