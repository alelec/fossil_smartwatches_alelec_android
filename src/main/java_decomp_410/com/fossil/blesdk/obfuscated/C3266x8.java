package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x8 */
public class C3266x8 {

    @DexIgnore
    /* renamed from: a */
    public android.view.ViewParent f10861a;

    @DexIgnore
    /* renamed from: b */
    public android.view.ViewParent f10862b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.view.View f10863c;

    @DexIgnore
    /* renamed from: d */
    public boolean f10864d;

    @DexIgnore
    /* renamed from: e */
    public int[] f10865e;

    @DexIgnore
    public C3266x8(android.view.View view) {
        this.f10863c = view;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo17607a(boolean z) {
        if (this.f10864d) {
            com.fossil.blesdk.obfuscated.C1776f9.m6798E(this.f10863c);
        }
        this.f10864d = z;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo17616b() {
        return this.f10864d;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo17619c(int i) {
        return mo17611a(i, 0);
    }

    @DexIgnore
    /* renamed from: d */
    public void mo17620d(int i) {
        android.view.ViewParent a = mo17605a(i);
        if (a != null) {
            com.fossil.blesdk.obfuscated.C2004i9.m8271a(a, this.f10863c, i);
            mo17606a(i, (android.view.ViewParent) null);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo17617b(int i) {
        return mo17605a(i) != null;
    }

    @DexIgnore
    /* renamed from: c */
    public void mo17618c() {
        mo17620d(0);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17608a() {
        return mo17617b(0);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17611a(int i, int i2) {
        if (mo17617b(i2)) {
            return true;
        }
        if (!mo17616b()) {
            return false;
        }
        android.view.View view = this.f10863c;
        for (android.view.ViewParent parent = this.f10863c.getParent(); parent != null; parent = parent.getParent()) {
            if (com.fossil.blesdk.obfuscated.C2004i9.m8278b(parent, view, this.f10863c, i, i2)) {
                mo17606a(i2, parent);
                com.fossil.blesdk.obfuscated.C2004i9.m8274a(parent, view, this.f10863c, i, i2);
                return true;
            }
            if (parent instanceof android.view.View) {
                view = (android.view.View) parent;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17612a(int i, int i2, int i3, int i4, int[] iArr) {
        return mo17613a(i, i2, i3, i4, iArr, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17613a(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        int i6;
        int i7;
        int[] iArr2 = iArr;
        if (mo17616b()) {
            android.view.ViewParent a = mo17605a(i5);
            if (a == null) {
                return false;
            }
            if (i != 0 || i2 != 0 || i3 != 0 || i4 != 0) {
                if (iArr2 != null) {
                    this.f10863c.getLocationInWindow(iArr2);
                    i7 = iArr2[0];
                    i6 = iArr2[1];
                } else {
                    i7 = 0;
                    i6 = 0;
                }
                com.fossil.blesdk.obfuscated.C2004i9.m8272a(a, this.f10863c, i, i2, i3, i4, i5);
                if (iArr2 != null) {
                    this.f10863c.getLocationInWindow(iArr2);
                    iArr2[0] = iArr2[0] - i7;
                    iArr2[1] = iArr2[1] - i6;
                }
                return true;
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17614a(int i, int i2, int[] iArr, int[] iArr2) {
        return mo17615a(i, i2, iArr, iArr2, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17615a(int i, int i2, int[] iArr, int[] iArr2, int i3) {
        int i4;
        int i5;
        if (mo17616b()) {
            android.view.ViewParent a = mo17605a(i3);
            if (a == null) {
                return false;
            }
            if (i != 0 || i2 != 0) {
                if (iArr2 != null) {
                    this.f10863c.getLocationInWindow(iArr2);
                    i5 = iArr2[0];
                    i4 = iArr2[1];
                } else {
                    i5 = 0;
                    i4 = 0;
                }
                if (iArr == null) {
                    if (this.f10865e == null) {
                        this.f10865e = new int[2];
                    }
                    iArr = this.f10865e;
                }
                iArr[0] = 0;
                iArr[1] = 0;
                com.fossil.blesdk.obfuscated.C2004i9.m8273a(a, this.f10863c, i, i2, iArr, i3);
                if (iArr2 != null) {
                    this.f10863c.getLocationInWindow(iArr2);
                    iArr2[0] = iArr2[0] - i5;
                    iArr2[1] = iArr2[1] - i4;
                }
                if (iArr[0] == 0 && iArr[1] == 0) {
                    return false;
                }
                return true;
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17610a(float f, float f2, boolean z) {
        if (mo17616b()) {
            android.view.ViewParent a = mo17605a(0);
            if (a != null) {
                return com.fossil.blesdk.obfuscated.C2004i9.m8276a(a, this.f10863c, f, f2, z);
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo17609a(float f, float f2) {
        if (mo17616b()) {
            android.view.ViewParent a = mo17605a(0);
            if (a != null) {
                return com.fossil.blesdk.obfuscated.C2004i9.m8275a(a, this.f10863c, f, f2);
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.view.ViewParent mo17605a(int i) {
        if (i == 0) {
            return this.f10861a;
        }
        if (i != 1) {
            return null;
        }
        return this.f10862b;
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo17606a(int i, android.view.ViewParent viewParent) {
        if (i == 0) {
            this.f10861a = viewParent;
        } else if (i == 1) {
            this.f10862b = viewParent;
        }
    }
}
