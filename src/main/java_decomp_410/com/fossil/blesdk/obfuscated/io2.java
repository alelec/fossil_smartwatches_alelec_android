package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class io2 implements MembersInjector<NetworkChangedReceiver> {
    @DexIgnore
    public static void a(NetworkChangedReceiver networkChangedReceiver, UserRepository userRepository) {
        networkChangedReceiver.a = userRepository;
    }
}
