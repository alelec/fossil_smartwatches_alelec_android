package com.fossil.blesdk.obfuscated;

import java.io.EOFException;
import java.io.IOException;
import java.util.zip.CRC32;
import java.util.zip.Inflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qo4 implements yo4 {
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public /* final */ lo4 f;
    @DexIgnore
    public /* final */ Inflater g;
    @DexIgnore
    public /* final */ ro4 h;
    @DexIgnore
    public /* final */ CRC32 i; // = new CRC32();

    @DexIgnore
    public qo4(yo4 yo4) {
        if (yo4 != null) {
            this.g = new Inflater(true);
            this.f = so4.a(yo4);
            this.h = new ro4(this.f, this.g);
            return;
        }
        throw new IllegalArgumentException("source == null");
    }

    @DexIgnore
    public final void a(jo4 jo4, long j, long j2) {
        vo4 vo4 = jo4.e;
        while (true) {
            int i2 = vo4.c;
            int i3 = vo4.b;
            if (j < ((long) (i2 - i3))) {
                break;
            }
            j -= (long) (i2 - i3);
            vo4 = vo4.f;
        }
        while (j2 > 0) {
            int i4 = (int) (((long) vo4.b) + j);
            int min = (int) Math.min((long) (vo4.c - i4), j2);
            this.i.update(vo4.a, i4, min);
            j2 -= (long) min;
            vo4 = vo4.f;
            j = 0;
        }
    }

    @DexIgnore
    public long b(jo4 jo4, long j) throws IOException {
        int i2 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i2 < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (i2 == 0) {
            return 0;
        } else {
            if (this.e == 0) {
                c();
                this.e = 1;
            }
            if (this.e == 1) {
                long j2 = jo4.f;
                long b = this.h.b(jo4, j);
                if (b != -1) {
                    a(jo4, j2, b);
                    return b;
                }
                this.e = 2;
            }
            if (this.e == 2) {
                d();
                this.e = 3;
                if (!this.f.g()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1;
        }
    }

    @DexIgnore
    public final void c() throws IOException {
        this.f.g(10);
        byte h2 = this.f.a().h(3);
        boolean z = ((h2 >> 1) & 1) == 1;
        if (z) {
            a(this.f.a(), 0, 10);
        }
        a("ID1ID2", 8075, (int) this.f.readShort());
        this.f.skip(8);
        if (((h2 >> 2) & 1) == 1) {
            this.f.g(2);
            if (z) {
                a(this.f.a(), 0, 2);
            }
            long k = (long) this.f.a().k();
            this.f.g(k);
            if (z) {
                a(this.f.a(), 0, k);
            }
            this.f.skip(k);
        }
        if (((h2 >> 3) & 1) == 1) {
            long a = this.f.a((byte) 0);
            if (a != -1) {
                if (z) {
                    a(this.f.a(), 0, a + 1);
                }
                this.f.skip(a + 1);
            } else {
                throw new EOFException();
            }
        }
        if (((h2 >> 4) & 1) == 1) {
            long a2 = this.f.a((byte) 0);
            if (a2 != -1) {
                if (z) {
                    a(this.f.a(), 0, a2 + 1);
                }
                this.f.skip(a2 + 1);
            } else {
                throw new EOFException();
            }
        }
        if (z) {
            a("FHCRC", (int) this.f.k(), (int) (short) ((int) this.i.getValue()));
            this.i.reset();
        }
    }

    @DexIgnore
    public void close() throws IOException {
        this.h.close();
    }

    @DexIgnore
    public final void d() throws IOException {
        a("CRC", this.f.j(), (int) this.i.getValue());
        a("ISIZE", this.f.j(), (int) this.g.getBytesWritten());
    }

    @DexIgnore
    public final void a(String str, int i2, int i3) throws IOException {
        if (i3 != i2) {
            throw new IOException(String.format("%s: actual 0x%08x != expected 0x%08x", new Object[]{str, Integer.valueOf(i3), Integer.valueOf(i2)}));
        }
    }

    @DexIgnore
    public zo4 b() {
        return this.f.b();
    }
}
