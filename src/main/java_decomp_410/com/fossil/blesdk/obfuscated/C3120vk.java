package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.vk */
public class C3120vk extends com.fossil.blesdk.obfuscated.C3044uk<com.fossil.blesdk.obfuscated.C1878gk> {

    @DexIgnore
    /* renamed from: j */
    public static /* final */ java.lang.String f10313j; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("NetworkStateTracker");

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.net.ConnectivityManager f10314g; // = ((android.net.ConnectivityManager) this.f10017b.getSystemService("connectivity"));

    @DexIgnore
    /* renamed from: h */
    public com.fossil.blesdk.obfuscated.C3120vk.C3122b f10315h;

    @DexIgnore
    /* renamed from: i */
    public com.fossil.blesdk.obfuscated.C3120vk.C3121a f10316i;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vk$a")
    /* renamed from: com.fossil.blesdk.obfuscated.vk$a */
    public class C3121a extends android.content.BroadcastReceiver {
        @DexIgnore
        public C3121a() {
        }

        @DexIgnore
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C3120vk.f10313j, "Network broadcast received", new java.lang.Throwable[0]);
                com.fossil.blesdk.obfuscated.C3120vk vkVar = com.fossil.blesdk.obfuscated.C3120vk.this;
                vkVar.mo16804a(vkVar.mo17130d());
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.vk$b")
    /* renamed from: com.fossil.blesdk.obfuscated.vk$b */
    public class C3122b extends android.net.ConnectivityManager.NetworkCallback {
        @DexIgnore
        public C3122b() {
        }

        @DexIgnore
        public void onCapabilitiesChanged(android.net.Network network, android.net.NetworkCapabilities networkCapabilities) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C3120vk.f10313j, java.lang.String.format("Network capabilities changed: %s", new java.lang.Object[]{networkCapabilities}), new java.lang.Throwable[0]);
            com.fossil.blesdk.obfuscated.C3120vk vkVar = com.fossil.blesdk.obfuscated.C3120vk.this;
            vkVar.mo16804a(vkVar.mo17130d());
        }

        @DexIgnore
        public void onLost(android.net.Network network) {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(com.fossil.blesdk.obfuscated.C3120vk.f10313j, "Network connection lost", new java.lang.Throwable[0]);
            com.fossil.blesdk.obfuscated.C3120vk vkVar = com.fossil.blesdk.obfuscated.C3120vk.this;
            vkVar.mo16804a(vkVar.mo17130d());
        }
    }

    @DexIgnore
    public C3120vk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(context, zlVar);
        if (m15341f()) {
            this.f10315h = new com.fossil.blesdk.obfuscated.C3120vk.C3122b();
        } else {
            this.f10316i = new com.fossil.blesdk.obfuscated.C3120vk.C3121a();
        }
    }

    @DexIgnore
    /* renamed from: f */
    public static boolean m15341f() {
        return android.os.Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo16475b() {
        if (m15341f()) {
            try {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10313j, "Registering network callback", new java.lang.Throwable[0]);
                this.f10314g.registerDefaultNetworkCallback(this.f10315h);
            } catch (java.lang.IllegalArgumentException e) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f10313j, "Received exception while unregistering network callback", e);
            }
        } else {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10313j, "Registering broadcast receiver", new java.lang.Throwable[0]);
            this.f10017b.registerReceiver(this.f10316i, new android.content.IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16476c() {
        if (m15341f()) {
            try {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10313j, "Unregistering network callback", new java.lang.Throwable[0]);
                this.f10314g.unregisterNetworkCallback(this.f10315h);
            } catch (java.lang.IllegalArgumentException e) {
                com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9963b(f10313j, "Received exception while unregistering network callback", e);
            }
        } else {
            com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f10313j, "Unregistering broadcast receiver", new java.lang.Throwable[0]);
            this.f10017b.unregisterReceiver(this.f10316i);
        }
    }

    @DexIgnore
    /* renamed from: d */
    public com.fossil.blesdk.obfuscated.C1878gk mo17130d() {
        android.net.NetworkInfo activeNetworkInfo = this.f10314g.getActiveNetworkInfo();
        boolean z = true;
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean e = mo17131e();
        boolean a = com.fossil.blesdk.obfuscated.C2298l7.m10089a(this.f10314g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new com.fossil.blesdk.obfuscated.C1878gk(z2, e, a, z);
    }

    @DexIgnore
    /* renamed from: e */
    public final boolean mo17131e() {
        if (android.os.Build.VERSION.SDK_INT < 23) {
            return false;
        }
        android.net.NetworkCapabilities networkCapabilities = this.f10314g.getNetworkCapabilities(this.f10314g.getActiveNetwork());
        if (networkCapabilities == null || !networkCapabilities.hasCapability(16)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1878gk m15343a() {
        return mo17130d();
    }
}
