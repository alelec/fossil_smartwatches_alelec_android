package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fc */
public class C1785fc extends android.app.Fragment {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C1785fc.C1786a f5127e;

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.fc$a */
    public interface C1786a {
        @DexIgnore
        /* renamed from: a */
        void mo10445a();

        @DexIgnore
        /* renamed from: d */
        void mo10446d();

        @DexIgnore
        /* renamed from: e */
        void mo10447e();
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C1785fc m6900a(android.app.Activity activity) {
        return (com.fossil.blesdk.obfuscated.C1785fc) activity.getFragmentManager().findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag");
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6901b(android.app.Activity activity) {
        android.app.FragmentManager fragmentManager = activity.getFragmentManager();
        if (fragmentManager.findFragmentByTag("androidx.lifecycle.LifecycleDispatcher.report_fragment_tag") == null) {
            fragmentManager.beginTransaction().add(new com.fossil.blesdk.obfuscated.C1785fc(), "androidx.lifecycle.LifecycleDispatcher.report_fragment_tag").commit();
            fragmentManager.executePendingTransactions();
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo10838c(com.fossil.blesdk.obfuscated.C1785fc.C1786a aVar) {
        if (aVar != null) {
            aVar.mo10445a();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10839d(com.fossil.blesdk.obfuscated.C1785fc.C1786a aVar) {
        this.f5127e = aVar;
    }

    @DexIgnore
    public void onActivityCreated(android.os.Bundle bundle) {
        super.onActivityCreated(bundle);
        mo10836a(this.f5127e);
        mo10835a(androidx.lifecycle.Lifecycle.Event.ON_CREATE);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        mo10835a(androidx.lifecycle.Lifecycle.Event.ON_DESTROY);
        this.f5127e = null;
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        mo10835a(androidx.lifecycle.Lifecycle.Event.ON_PAUSE);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        mo10837b(this.f5127e);
        mo10835a(androidx.lifecycle.Lifecycle.Event.ON_RESUME);
    }

    @DexIgnore
    public void onStart() {
        super.onStart();
        mo10838c(this.f5127e);
        mo10835a(androidx.lifecycle.Lifecycle.Event.ON_START);
    }

    @DexIgnore
    public void onStop() {
        super.onStop();
        mo10835a(androidx.lifecycle.Lifecycle.Event.ON_STOP);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10836a(com.fossil.blesdk.obfuscated.C1785fc.C1786a aVar) {
        if (aVar != null) {
            aVar.mo10447e();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo10835a(androidx.lifecycle.Lifecycle.Event event) {
        android.app.Activity activity = getActivity();
        if (activity instanceof com.fossil.blesdk.obfuscated.C3270xb) {
            ((com.fossil.blesdk.obfuscated.C3270xb) activity).getLifecycle().mo2263a(event);
        } else if (activity instanceof androidx.lifecycle.LifecycleOwner) {
            androidx.lifecycle.Lifecycle lifecycle = ((androidx.lifecycle.LifecycleOwner) activity).getLifecycle();
            if (lifecycle instanceof androidx.lifecycle.LifecycleRegistry) {
                ((androidx.lifecycle.LifecycleRegistry) lifecycle).mo2263a(event);
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo10837b(com.fossil.blesdk.obfuscated.C1785fc.C1786a aVar) {
        if (aVar != null) {
            aVar.mo10446d();
        }
    }
}
