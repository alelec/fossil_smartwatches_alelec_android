package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class gm1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ vi1 e;
    @DexIgnore
    public /* final */ /* synthetic */ fm1 f;

    @DexIgnore
    public gm1(fm1 fm1, vi1 vi1) {
        this.f = fm1;
        this.e = vi1;
    }

    @DexIgnore
    public final void run() {
        this.e.b();
        if (ul1.a()) {
            this.e.a().a((Runnable) this);
            return;
        }
        boolean d = this.f.d();
        long unused = this.f.c = 0;
        if (d) {
            this.f.c();
        }
    }
}
