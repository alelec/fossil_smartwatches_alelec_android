package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n10 extends a10 {
    @DexIgnore
    public /* final */ byte[] m;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n10(GattCharacteristic.CharacteristicId characteristicId, byte[] bArr, Peripheral.c cVar) {
        super(BluetoothCommandId.WRITE_CHARACTERISTIC, characteristicId, cVar);
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "dataToWrite");
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.m = bArr;
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        kd4.b(peripheral, "peripheral");
        peripheral.b(i(), this.m);
        b(true);
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        return (gattOperationResult instanceof a20) && ((a20) gattOperationResult).b() == i();
    }

    @DexIgnore
    public ra0<GattOperationResult> f() {
        return b().k();
    }

    @DexIgnore
    public final byte[] j() {
        return this.m;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        JSONObject a = super.a(z);
        if (z) {
            byte[] bArr = this.m;
            if (bArr.length < 100) {
                wa0.a(a, JSONKey.RAW_DATA, k90.a(bArr, (String) null, 1, (Object) null));
                return a;
            }
        }
        wa0.a(a, JSONKey.RAW_DATA_LENGTH, Integer.valueOf(this.m.length));
        return a;
    }
}
