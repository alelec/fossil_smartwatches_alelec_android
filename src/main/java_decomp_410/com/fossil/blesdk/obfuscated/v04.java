package com.fossil.blesdk.obfuscated;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class v04 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context e;

    @DexIgnore
    public v04(Context context) {
        this.e = context;
    }

    @DexIgnore
    public final void run() {
        try {
            new Thread(new b14(this.e, (Map<String, Integer>) null, (k04) null), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            j04.m.a(th);
            j04.a(this.e, th);
        }
    }
}
