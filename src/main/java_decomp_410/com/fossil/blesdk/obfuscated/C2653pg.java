package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pg */
public class C2653pg extends com.fossil.blesdk.obfuscated.C2567og implements com.fossil.blesdk.obfuscated.C2221kg {

    @DexIgnore
    /* renamed from: f */
    public /* final */ android.database.sqlite.SQLiteStatement f8378f;

    @DexIgnore
    public C2653pg(android.database.sqlite.SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.f8378f = sQLiteStatement;
    }

    @DexIgnore
    /* renamed from: n */
    public int mo12781n() {
        return this.f8378f.executeUpdateDelete();
    }

    @DexIgnore
    /* renamed from: o */
    public long mo12782o() {
        return this.f8378f.executeInsert();
    }
}
