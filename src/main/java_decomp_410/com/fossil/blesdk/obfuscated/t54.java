package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.LegacyTokenHelper;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class t54 {
    @DexIgnore
    public String a(Context context) {
        int a = CommonUtils.a(context, "google_app_id", LegacyTokenHelper.TYPE_STRING);
        if (a == 0) {
            return null;
        }
        q44.g().d("Fabric", "Generating Crashlytics ApiKey from google_app_id in Strings");
        return a(context.getResources().getString(a));
    }

    @DexIgnore
    public boolean b(Context context) {
        if (!TextUtils.isEmpty(new k54().b(context))) {
            return true;
        }
        return !TextUtils.isEmpty(new k54().c(context));
    }

    @DexIgnore
    public boolean c(Context context) {
        int a = CommonUtils.a(context, "google_app_id", LegacyTokenHelper.TYPE_STRING);
        if (a == 0) {
            return false;
        }
        return !TextUtils.isEmpty(context.getResources().getString(a));
    }

    @DexIgnore
    public boolean d(Context context) {
        int a = CommonUtils.a(context, "io.fabric.auto_initialize", LegacyTokenHelper.TYPE_BOOLEAN);
        if (a == 0) {
            return false;
        }
        boolean z = context.getResources().getBoolean(a);
        if (z) {
            q44.g().d("Fabric", "Found Fabric auto-initialization flag for joint Firebase/Fabric customers");
        }
        return z;
    }

    @DexIgnore
    public boolean e(Context context) {
        if (CommonUtils.a(context, "com.crashlytics.useFirebaseAppId", false)) {
            return true;
        }
        if (!c(context) || b(context)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public String a(String str) {
        return CommonUtils.d(str).substring(0, 40);
    }
}
