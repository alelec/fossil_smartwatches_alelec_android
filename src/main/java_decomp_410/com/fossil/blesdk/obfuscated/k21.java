package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.blesdk.obfuscated.ge0;
import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class k21<T extends IInterface> extends oj0<T> {
    @DexIgnore
    public k21(Context context, Looper looper, int i, ge0.b bVar, ge0.c cVar, kj0 kj0) {
        super(context, looper, i, kj0, bVar, cVar);
    }

    @DexIgnore
    public boolean B() {
        return true;
    }

    @DexIgnore
    public Set<Scope> a(Set<Scope> set) {
        return oq0.a(set);
    }

    @DexIgnore
    public boolean l() {
        return !lm0.b(t());
    }
}
