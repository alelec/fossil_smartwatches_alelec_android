package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Scope;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface kh0 {
    @DexIgnore
    void a(tj0 tj0, Set<Scope> set);

    @DexIgnore
    void b(ud0 ud0);
}
