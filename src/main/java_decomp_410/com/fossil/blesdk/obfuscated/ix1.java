package com.fossil.blesdk.obfuscated;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.util.SparseArray;
import com.google.firebase.iid.zzal;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ix1 implements ServiceConnection {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ Messenger b;
    @DexIgnore
    public nx1 c;
    @DexIgnore
    public /* final */ Queue<px1<?>> d;
    @DexIgnore
    public /* final */ SparseArray<px1<?>> e;
    @DexIgnore
    public /* final */ /* synthetic */ gx1 f;

    @DexIgnore
    public ix1(gx1 gx1) {
        this.f = gx1;
        this.a = 0;
        this.b = new Messenger(new gz0(Looper.getMainLooper(), new jx1(this)));
        this.d = new ArrayDeque();
        this.e = new SparseArray<>();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0096, code lost:
        return true;
     */
    @DexIgnore
    public final synchronized boolean a(px1 px1) {
        int i = this.a;
        if (i == 0) {
            this.d.add(px1);
            bk0.b(this.a == 0);
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Starting bind to GmsCore");
            }
            this.a = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!dm0.a().a(this.f.a, intent, this, 1)) {
                a(0, "Unable to bind to service");
            } else {
                this.f.b.schedule(new kx1(this), 30, TimeUnit.SECONDS);
            }
        } else if (i == 1) {
            this.d.add(px1);
            return true;
        } else if (i == 2) {
            this.d.add(px1);
            b();
            return true;
        } else if (i != 3) {
            if (i != 4) {
                int i2 = this.a;
                StringBuilder sb = new StringBuilder(26);
                sb.append("Unknown state: ");
                sb.append(i2);
                throw new IllegalStateException(sb.toString());
            }
        }
    }

    @DexIgnore
    public final void b() {
        this.f.b.execute(new lx1(this));
    }

    @DexIgnore
    public final synchronized void c() {
        if (this.a == 2 && this.d.isEmpty() && this.e.size() == 0) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
            }
            this.a = 3;
            dm0.a().a(this.f.a, this);
        }
    }

    @DexIgnore
    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        if (iBinder == null) {
            a(0, "Null service connection");
            return;
        }
        try {
            this.c = new nx1(iBinder);
            this.a = 2;
            b();
        } catch (RemoteException e2) {
            a(0, e2.getMessage());
        }
    }

    @DexIgnore
    public final synchronized void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        a(2, "Service disconnected");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0052, code lost:
        r5 = r5.getData();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        if (r5.getBoolean("unsupported", false) == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005f, code lost:
        r1.a(new com.google.firebase.iid.zzal(4, "Not supported by GmsCore"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006b, code lost:
        r1.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        return true;
     */
    @DexIgnore
    public final boolean a(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Received response to request: ");
            sb.append(i);
            Log.d("MessengerIpcClient", sb.toString());
        }
        synchronized (this) {
            px1 px1 = this.e.get(i);
            if (px1 == null) {
                StringBuilder sb2 = new StringBuilder(50);
                sb2.append("Received response for unknown request: ");
                sb2.append(i);
                Log.w("MessengerIpcClient", sb2.toString());
                return true;
            }
            this.e.remove(i);
            c();
        }
    }

    @DexIgnore
    public final synchronized void a(int i, String str) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(str);
            Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
        }
        int i2 = this.a;
        if (i2 == 0) {
            throw new IllegalStateException();
        } else if (i2 == 1 || i2 == 2) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Unbinding service");
            }
            this.a = 4;
            dm0.a().a(this.f.a, this);
            zzal zzal = new zzal(i, str);
            for (px1 a2 : this.d) {
                a2.a(zzal);
            }
            this.d.clear();
            for (int i3 = 0; i3 < this.e.size(); i3++) {
                this.e.valueAt(i3).a(zzal);
            }
            this.e.clear();
        } else if (i2 == 3) {
            this.a = 4;
        } else if (i2 != 4) {
            int i4 = this.a;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Unknown state: ");
            sb.append(i4);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final synchronized void a() {
        if (this.a == 1) {
            a(1, "Timed out while binding");
        }
    }

    @DexIgnore
    public final synchronized void a(int i) {
        px1 px1 = this.e.get(i);
        if (px1 != null) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Timing out request: ");
            sb.append(i);
            Log.w("MessengerIpcClient", sb.toString());
            this.e.remove(i);
            px1.a(new zzal(3, "Timed out waiting for response"));
            c();
        }
    }
}
