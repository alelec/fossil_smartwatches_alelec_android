package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.view.LayoutInflater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class u0 extends ContextWrapper {
    @DexIgnore
    public int a;
    @DexIgnore
    public Resources.Theme b;
    @DexIgnore
    public LayoutInflater c;
    @DexIgnore
    public Configuration d;
    @DexIgnore
    public Resources e;

    @DexIgnore
    public u0() {
        super((Context) null);
    }

    @DexIgnore
    public final Resources a() {
        if (this.e == null) {
            Configuration configuration = this.d;
            if (configuration == null) {
                this.e = super.getResources();
            } else if (Build.VERSION.SDK_INT >= 17) {
                this.e = createConfigurationContext(configuration).getResources();
            }
        }
        return this.e;
    }

    @DexIgnore
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        boolean z = this.b == null;
        if (z) {
            this.b = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.b.setTo(theme);
            }
        }
        a(this.b, this.a, z);
    }

    @DexIgnore
    public AssetManager getAssets() {
        return getResources().getAssets();
    }

    @DexIgnore
    public Resources getResources() {
        return a();
    }

    @DexIgnore
    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.c == null) {
            this.c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.c;
    }

    @DexIgnore
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        if (theme != null) {
            return theme;
        }
        if (this.a == 0) {
            this.a = z.Theme_AppCompat_Light;
        }
        c();
        return this.b;
    }

    @DexIgnore
    public void setTheme(int i) {
        if (this.a != i) {
            this.a = i;
            c();
        }
    }

    @DexIgnore
    public u0(Context context, int i) {
        super(context);
        this.a = i;
    }

    @DexIgnore
    public u0(Context context, Resources.Theme theme) {
        super(context);
        this.b = theme;
    }

    @DexIgnore
    public void a(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }
}
