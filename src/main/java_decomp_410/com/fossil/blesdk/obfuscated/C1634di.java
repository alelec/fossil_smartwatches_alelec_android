package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.di */
public class C1634di extends com.fossil.blesdk.obfuscated.C1557ci {

    @DexIgnore
    /* renamed from: f */
    public static java.lang.reflect.Method f4388f;

    @DexIgnore
    /* renamed from: g */
    public static boolean f4389g;

    @DexIgnore
    /* renamed from: h */
    public static java.lang.reflect.Method f4390h;

    @DexIgnore
    /* renamed from: i */
    public static boolean f4391i;

    @DexIgnore
    /* renamed from: j */
    public static java.lang.reflect.Method f4392j;

    @DexIgnore
    /* renamed from: k */
    public static boolean f4393k;

    @DexIgnore
    /* renamed from: a */
    public void mo9954a(android.view.View view, android.graphics.Matrix matrix) {
        mo9956c();
        java.lang.reflect.Method method = f4392j;
        if (method != null) {
            try {
                method.invoke(view, new java.lang.Object[]{matrix});
            } catch (java.lang.reflect.InvocationTargetException unused) {
            } catch (java.lang.IllegalAccessException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9955b(android.view.View view, android.graphics.Matrix matrix) {
        mo9958d();
        java.lang.reflect.Method method = f4388f;
        if (method != null) {
            try {
                method.invoke(view, new java.lang.Object[]{matrix});
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9957c(android.view.View view, android.graphics.Matrix matrix) {
        mo9959e();
        java.lang.reflect.Method method = f4390h;
        if (method != null) {
            try {
                method.invoke(view, new java.lang.Object[]{matrix});
            } catch (java.lang.IllegalAccessException unused) {
            } catch (java.lang.reflect.InvocationTargetException e) {
                throw new java.lang.RuntimeException(e.getCause());
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public final void mo9958d() {
        if (!f4389g) {
            try {
                f4388f = android.view.View.class.getDeclaredMethod("transformMatrixToGlobal", new java.lang.Class[]{android.graphics.Matrix.class});
                f4388f.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToGlobal method", e);
            }
            f4389g = true;
        }
    }

    @DexIgnore
    /* renamed from: e */
    public final void mo9959e() {
        if (!f4391i) {
            try {
                f4390h = android.view.View.class.getDeclaredMethod("transformMatrixToLocal", new java.lang.Class[]{android.graphics.Matrix.class});
                f4390h.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi21", "Failed to retrieve transformMatrixToLocal method", e);
            }
            f4391i = true;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo9956c() {
        if (!f4393k) {
            try {
                f4392j = android.view.View.class.getDeclaredMethod("setAnimationMatrix", new java.lang.Class[]{android.graphics.Matrix.class});
                f4392j.setAccessible(true);
            } catch (java.lang.NoSuchMethodException e) {
                android.util.Log.i("ViewUtilsApi21", "Failed to retrieve setAnimationMatrix method", e);
            }
            f4393k = true;
        }
    }
}
