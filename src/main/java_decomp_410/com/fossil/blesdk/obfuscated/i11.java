package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i11 extends nz0 implements g11 {
    @DexIgnore
    public i11(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    public final void c(Status status) throws RemoteException {
        Parcel o = o();
        y01.a(o, (Parcelable) status);
        b(1, o);
    }
}
