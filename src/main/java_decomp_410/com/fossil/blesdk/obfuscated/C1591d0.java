package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.d0 */
public interface C1591d0 {
    @DexIgnore
    void onSupportActionModeFinished(androidx.appcompat.view.ActionMode actionMode);

    @DexIgnore
    void onSupportActionModeStarted(androidx.appcompat.view.ActionMode actionMode);

    @DexIgnore
    androidx.appcompat.view.ActionMode onWindowStartingSupportActionMode(androidx.appcompat.view.ActionMode.Callback callback);
}
