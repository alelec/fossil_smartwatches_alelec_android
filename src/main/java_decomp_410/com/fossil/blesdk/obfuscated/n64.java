package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class n64 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ j64 b;
    @DexIgnore
    public /* final */ m64 c;

    @DexIgnore
    public n64(j64 j64, m64 m64) {
        this(0, j64, m64);
    }

    @DexIgnore
    public long a() {
        return this.b.a(this.a);
    }

    @DexIgnore
    public n64 b() {
        return new n64(this.b, this.c);
    }

    @DexIgnore
    public n64 c() {
        return new n64(this.a + 1, this.b, this.c);
    }

    @DexIgnore
    public n64(int i, j64 j64, m64 m64) {
        this.a = i;
        this.b = j64;
        this.c = m64;
    }
}
