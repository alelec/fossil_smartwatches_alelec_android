package com.fossil.blesdk.obfuscated;

import android.content.Context;
import io.fabric.sdk.android.services.common.CommonUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class w64 implements Runnable {
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ s64 f;

    @DexIgnore
    public w64(Context context, s64 s64) {
        this.e = context;
        this.f = s64;
    }

    @DexIgnore
    public void run() {
        try {
            CommonUtils.c(this.e, "Performing time based file roll over.");
            if (!this.f.b()) {
                this.f.c();
            }
        } catch (Exception e2) {
            CommonUtils.a(this.e, "Failed to roll over file", (Throwable) e2);
        }
    }
}
