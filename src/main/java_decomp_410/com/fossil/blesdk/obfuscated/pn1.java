package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface pn1<TResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(wn1<TResult> wn1) throws Exception;
}
