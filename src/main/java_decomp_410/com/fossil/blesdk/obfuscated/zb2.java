package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class zb2 extends yb2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(R.id.vOne, 1);
        x.put(R.id.vTwo, 2);
        x.put(R.id.ftvTitle, 3);
        x.put(R.id.ftvToday, 4);
        x.put(R.id.ftv7Days, 5);
        x.put(R.id.ftvMonth, 6);
        x.put(R.id.rvOverview, 7);
    }
    */

    @DexIgnore
    public zb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 8, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public zb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[5], objArr[6], objArr[3], objArr[4], objArr[7], objArr[1], objArr[2]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
