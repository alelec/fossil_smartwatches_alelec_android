package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.locks.LockSupport;
import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wf4<T> extends uf4<T> {
    @DexIgnore
    public /* final */ Thread h;
    @DexIgnore
    public /* final */ sh4 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wf4(CoroutineContext coroutineContext, Thread thread, sh4 sh4) {
        super(coroutineContext, true);
        kd4.b(coroutineContext, "parentContext");
        kd4.b(thread, "blockedThread");
        this.h = thread;
        this.i = sh4;
    }

    @DexIgnore
    public void a(Object obj, int i2) {
        if (!kd4.a((Object) Thread.currentThread(), (Object) this.h)) {
            LockSupport.unpark(this.h);
        }
    }

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public final T m() {
        cj4 a = dj4.a();
        if (a != null) {
            a.b();
        }
        try {
            sh4 sh4 = this.i;
            T t = null;
            if (sh4 != null) {
                sh4.b(sh4, false, 1, (Object) null);
            }
            while (!Thread.interrupted()) {
                sh4 sh42 = this.i;
                long F = sh42 != null ? sh42.F() : ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
                if (e()) {
                    sh4 sh43 = this.i;
                    if (sh43 != null) {
                        sh4.a(sh43, false, 1, (Object) null);
                    }
                    cj4 a2 = dj4.a();
                    if (a2 != null) {
                        a2.d();
                    }
                    T b = mi4.b(d());
                    if (b instanceof ng4) {
                        t = b;
                    }
                    ng4 ng4 = (ng4) t;
                    if (ng4 == null) {
                        return b;
                    }
                    throw ng4.a;
                }
                cj4 a3 = dj4.a();
                if (a3 != null) {
                    a3.a(this, F);
                } else {
                    LockSupport.parkNanos(this, F);
                }
            }
            InterruptedException interruptedException = new InterruptedException();
            a((Throwable) interruptedException);
            throw interruptedException;
        } catch (Throwable th) {
            cj4 a4 = dj4.a();
            if (a4 != null) {
                a4.d();
            }
            throw th;
        }
    }
}
