package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface an1 extends IInterface {
    @DexIgnore
    void a(gn1 gn1) throws RemoteException;

    @DexIgnore
    void a(ud0 ud0, xm1 xm1) throws RemoteException;

    @DexIgnore
    void a(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException;

    @DexIgnore
    void d(Status status) throws RemoteException;

    @DexIgnore
    void e(Status status) throws RemoteException;
}
