package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.l3 */
public class C2282l3<K, V> implements java.lang.Iterable<java.util.Map.Entry<K, V>> {

    @DexIgnore
    /* renamed from: e */
    public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7114e;

    @DexIgnore
    /* renamed from: f */
    public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7115f;

    @DexIgnore
    /* renamed from: g */
    public java.util.WeakHashMap<com.fossil.blesdk.obfuscated.C2282l3.C2288f<K, V>, java.lang.Boolean> f7116g; // = new java.util.WeakHashMap<>();

    @DexIgnore
    /* renamed from: h */
    public int f7117h; // = 0;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l3$a")
    /* renamed from: com.fossil.blesdk.obfuscated.l3$a */
    public static class C2283a<K, V> extends com.fossil.blesdk.obfuscated.C2282l3.C2287e<K, V> {
        @DexIgnore
        public C2283a(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar, com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13125b(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar) {
            return cVar.f7121h;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13126c(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar) {
            return cVar.f7120g;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l3$b")
    /* renamed from: com.fossil.blesdk.obfuscated.l3$b */
    public static class C2284b<K, V> extends com.fossil.blesdk.obfuscated.C2282l3.C2287e<K, V> {
        @DexIgnore
        public C2284b(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar, com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2) {
            super(cVar, cVar2);
        }

        @DexIgnore
        /* renamed from: b */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13125b(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar) {
            return cVar.f7120g;
        }

        @DexIgnore
        /* renamed from: c */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13126c(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar) {
            return cVar.f7121h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l3$c")
    /* renamed from: com.fossil.blesdk.obfuscated.l3$c */
    public static class C2285c<K, V> implements java.util.Map.Entry<K, V> {

        @DexIgnore
        /* renamed from: e */
        public /* final */ K f7118e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ V f7119f;

        @DexIgnore
        /* renamed from: g */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7120g;

        @DexIgnore
        /* renamed from: h */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7121h;

        @DexIgnore
        public C2285c(K k, V v) {
            this.f7118e = k;
            this.f7119f = v;
        }

        @DexIgnore
        public boolean equals(java.lang.Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof com.fossil.blesdk.obfuscated.C2282l3.C2285c)) {
                return false;
            }
            com.fossil.blesdk.obfuscated.C2282l3.C2285c cVar = (com.fossil.blesdk.obfuscated.C2282l3.C2285c) obj;
            if (!this.f7118e.equals(cVar.f7118e) || !this.f7119f.equals(cVar.f7119f)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public K getKey() {
            return this.f7118e;
        }

        @DexIgnore
        public V getValue() {
            return this.f7119f;
        }

        @DexIgnore
        public int hashCode() {
            return this.f7118e.hashCode() ^ this.f7119f.hashCode();
        }

        @DexIgnore
        public V setValue(V v) {
            throw new java.lang.UnsupportedOperationException("An entry modification is not supported");
        }

        @DexIgnore
        public java.lang.String toString() {
            return this.f7118e + com.j256.ormlite.stmt.query.SimpleComparison.EQUAL_TO_OPERATION + this.f7119f;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l3$d")
    /* renamed from: com.fossil.blesdk.obfuscated.l3$d */
    public class C2286d implements java.util.Iterator<java.util.Map.Entry<K, V>>, com.fossil.blesdk.obfuscated.C2282l3.C2288f<K, V> {

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7122e;

        @DexIgnore
        /* renamed from: f */
        public boolean f7123f; // = true;

        @DexIgnore
        public C2286d() {
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13133a(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar) {
            com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2 = this.f7122e;
            if (cVar == cVar2) {
                this.f7122e = cVar2.f7121h;
                this.f7123f = this.f7122e == null;
            }
        }

        @DexIgnore
        public boolean hasNext() {
            if (!this.f7123f) {
                com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = this.f7122e;
                if (cVar == null || cVar.f7120g == null) {
                    return false;
                }
                return true;
            } else if (com.fossil.blesdk.obfuscated.C2282l3.this.f7114e != null) {
                return true;
            } else {
                return false;
            }
        }

        @DexIgnore
        public java.util.Map.Entry<K, V> next() {
            if (this.f7123f) {
                this.f7123f = false;
                this.f7122e = com.fossil.blesdk.obfuscated.C2282l3.this.f7114e;
            } else {
                com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = this.f7122e;
                this.f7122e = cVar != null ? cVar.f7120g : null;
            }
            return this.f7122e;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.l3$e")
    /* renamed from: com.fossil.blesdk.obfuscated.l3$e */
    public static abstract class C2287e<K, V> implements java.util.Iterator<java.util.Map.Entry<K, V>>, com.fossil.blesdk.obfuscated.C2282l3.C2288f<K, V> {

        @DexIgnore
        /* renamed from: e */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7125e;

        @DexIgnore
        /* renamed from: f */
        public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> f7126f;

        @DexIgnore
        public C2287e(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar, com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2) {
            this.f7125e = cVar2;
            this.f7126f = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13133a(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar) {
            if (this.f7125e == cVar && cVar == this.f7126f) {
                this.f7126f = null;
                this.f7125e = null;
            }
            com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2 = this.f7125e;
            if (cVar2 == cVar) {
                this.f7125e = mo13125b(cVar2);
            }
            if (this.f7126f == cVar) {
                this.f7126f = mo13136a();
            }
        }

        @DexIgnore
        /* renamed from: b */
        public abstract com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13125b(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar);

        @DexIgnore
        /* renamed from: c */
        public abstract com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13126c(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar);

        @DexIgnore
        public boolean hasNext() {
            return this.f7126f != null;
        }

        @DexIgnore
        public java.util.Map.Entry<K, V> next() {
            com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = this.f7126f;
            this.f7126f = mo13136a();
            return cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public final com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13136a() {
            com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = this.f7126f;
            com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2 = this.f7125e;
            if (cVar == cVar2 || cVar2 == null) {
                return null;
            }
            return mo13126c(cVar);
        }
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.l3$f */
    public interface C2288f<K, V> {
        @DexIgnore
        /* renamed from: a */
        void mo13133a(com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar);
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo12617a(K k) {
        com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = this.f7114e;
        while (cVar != null && !cVar.f7118e.equals(k)) {
            cVar = cVar.f7120g;
        }
        return cVar;
    }

    @DexIgnore
    /* renamed from: b */
    public V mo12618b(K k, V v) {
        com.fossil.blesdk.obfuscated.C2282l3.C2285c a = mo12617a(k);
        if (a != null) {
            return a.f7119f;
        }
        mo13115a(k, v);
        return null;
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.Map.Entry<K, V> mo13118c() {
        return this.f7115f;
    }

    @DexIgnore
    public java.util.Iterator<java.util.Map.Entry<K, V>> descendingIterator() {
        com.fossil.blesdk.obfuscated.C2282l3.C2284b bVar = new com.fossil.blesdk.obfuscated.C2282l3.C2284b(this.f7115f, this.f7114e);
        this.f7116g.put(bVar, false);
        return bVar;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof com.fossil.blesdk.obfuscated.C2282l3)) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C2282l3 l3Var = (com.fossil.blesdk.obfuscated.C2282l3) obj;
        if (size() != l3Var.size()) {
            return false;
        }
        java.util.Iterator it = iterator();
        java.util.Iterator it2 = l3Var.iterator();
        while (it.hasNext() && it2.hasNext()) {
            java.util.Map.Entry entry = (java.util.Map.Entry) it.next();
            java.lang.Object next = it2.next();
            if ((entry == null && next != null) || (entry != null && !entry.equals(next))) {
                return false;
            }
        }
        if (it.hasNext() || it2.hasNext()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        java.util.Iterator it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i += ((java.util.Map.Entry) it.next()).hashCode();
        }
        return i;
    }

    @DexIgnore
    public java.util.Iterator<java.util.Map.Entry<K, V>> iterator() {
        com.fossil.blesdk.obfuscated.C2282l3.C2283a aVar = new com.fossil.blesdk.obfuscated.C2282l3.C2283a(this.f7114e, this.f7115f);
        this.f7116g.put(aVar, false);
        return aVar;
    }

    @DexIgnore
    public V remove(K k) {
        com.fossil.blesdk.obfuscated.C2282l3.C2285c a = mo12617a(k);
        if (a == null) {
            return null;
        }
        this.f7117h--;
        if (!this.f7116g.isEmpty()) {
            for (com.fossil.blesdk.obfuscated.C2282l3.C2288f<K, V> a2 : this.f7116g.keySet()) {
                a2.mo13133a(a);
            }
        }
        com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = a.f7121h;
        if (cVar != null) {
            cVar.f7120g = a.f7120g;
        } else {
            this.f7114e = a.f7120g;
        }
        com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2 = a.f7120g;
        if (cVar2 != null) {
            cVar2.f7121h = a.f7121h;
        } else {
            this.f7115f = a.f7121h;
        }
        a.f7120g = null;
        a.f7121h = null;
        return a.f7119f;
    }

    @DexIgnore
    public int size() {
        return this.f7117h;
    }

    @DexIgnore
    public java.lang.String toString() {
        java.lang.StringBuilder sb = new java.lang.StringBuilder();
        sb.append("[");
        java.util.Iterator it = iterator();
        while (it.hasNext()) {
            sb.append(((java.util.Map.Entry) it.next()).toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> mo13115a(K k, V v) {
        com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar = new com.fossil.blesdk.obfuscated.C2282l3.C2285c<>(k, v);
        this.f7117h++;
        com.fossil.blesdk.obfuscated.C2282l3.C2285c<K, V> cVar2 = this.f7115f;
        if (cVar2 == null) {
            this.f7114e = cVar;
            this.f7115f = this.f7114e;
            return cVar;
        }
        cVar2.f7120g = cVar;
        cVar.f7121h = cVar2;
        this.f7115f = cVar;
        return cVar;
    }

    @DexIgnore
    /* renamed from: b */
    public com.fossil.blesdk.obfuscated.C2282l3<K, V>.d mo13117b() {
        com.fossil.blesdk.obfuscated.C2282l3<K, V>.d dVar = new com.fossil.blesdk.obfuscated.C2282l3.C2286d();
        this.f7116g.put(dVar, false);
        return dVar;
    }

    @DexIgnore
    /* renamed from: a */
    public java.util.Map.Entry<K, V> mo13116a() {
        return this.f7114e;
    }
}
