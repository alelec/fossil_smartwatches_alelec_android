package com.fossil.blesdk.obfuscated;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface mx3 {
    @DexIgnore
    public static final mx3 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements mx3 {
        @DexIgnore
        public Map<Class<?>, Set<kx3>> a(Object obj) {
            return hx3.b(obj);
        }

        @DexIgnore
        public Map<Class<?>, lx3> b(Object obj) {
            return hx3.a(obj);
        }
    }

    @DexIgnore
    Map<Class<?>, Set<kx3>> a(Object obj);

    @DexIgnore
    Map<Class<?>, lx3> b(Object obj);
}
