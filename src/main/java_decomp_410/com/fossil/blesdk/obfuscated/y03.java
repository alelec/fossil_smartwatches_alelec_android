package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class y03 extends zr2 implements x03, ws3.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public w03 j;
    @DexIgnore
    public tr3<ee2> k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return y03.m;
        }

        @DexIgnore
        public final y03 b() {
            return new y03();
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y03 e;

        @DexIgnore
        public b(y03 y03) {
            this.e = y03;
        }

        @DexIgnore
        public final void onClick(View view) {
            y03.a(this.e).k();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y03 e;

        @DexIgnore
        public c(y03 y03) {
            this.e = y03;
        }

        @DexIgnore
        public final void onClick(View view) {
            y03.a(this.e).i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ y03 e;

        @DexIgnore
        public d(y03 y03) {
            this.e = y03;
        }

        @DexIgnore
        public final void onClick(View view) {
            y03.a(this.e).j();
        }
    }

    /*
    static {
        String simpleName = y03.class.getSimpleName();
        kd4.a((Object) simpleName, "NotificationHybridEveryo\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ w03 a(y03 y03) {
        w03 w03 = y03.j;
        if (w03 != null) {
            return w03;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        w03 w03 = this.j;
        if (w03 != null) {
            w03.k();
            return true;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        kd4.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onPermissionsGranted");
        w03 w03 = this.j;
        if (w03 != null) {
            w03.l();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        ee2 ee2 = (ee2) qa.a(layoutInflater, R.layout.fragment_notification_hybrid_everyone, viewGroup, false, O0());
        ee2.u.setOnClickListener(new b(this));
        ee2.q.setOnClickListener(new c(this));
        ee2.r.setOnClickListener(new d(this));
        this.k = new tr3<>(this, ee2);
        kd4.a((Object) ee2, "binding");
        return ee2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        w03 w03 = this.j;
        if (w03 != null) {
            w03.g();
            super.onPause();
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        w03 w03 = this.j;
        if (w03 != null) {
            w03.f();
        } else {
            kd4.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(w03 w03) {
        kd4.b(w03, "presenter");
        this.j = w03;
    }

    @DexIgnore
    public void b(List<ContactWrapper> list, int i) {
        int i2 = i;
        kd4.b(list, "listContactWrapper");
        boolean z = false;
        boolean z2 = false;
        for (ContactWrapper contactWrapper : list) {
            Contact contact = contactWrapper.getContact();
            if (contact == null || contact.getContactId() != -100) {
                Contact contact2 = contactWrapper.getContact();
                if (contact2 != null && contact2.getContactId() == -200) {
                    if (contactWrapper.getCurrentHandGroup() != i2) {
                        tr3<ee2> tr3 = this.k;
                        if (tr3 != null) {
                            ee2 a2 = tr3.a();
                            if (a2 != null) {
                                FlexibleTextView flexibleTextView = a2.t;
                                if (flexibleTextView != null) {
                                    pd4 pd4 = pd4.a;
                                    String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_InMyNotifications_Text__AssignedToNumber);
                                    kd4.a((Object) a3, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                    Object[] objArr = {Integer.valueOf(contactWrapper.getCurrentHandGroup())};
                                    String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                                    kd4.a((Object) format, "java.lang.String.format(format, *args)");
                                    flexibleTextView.setText(format);
                                }
                            }
                            tr3<ee2> tr32 = this.k;
                            if (tr32 != null) {
                                ee2 a4 = tr32.a();
                                if (a4 != null) {
                                    FlexibleTextView flexibleTextView2 = a4.t;
                                    if (flexibleTextView2 != null) {
                                        flexibleTextView2.setVisibility(0);
                                    }
                                }
                                tr3<ee2> tr33 = this.k;
                                if (tr33 != null) {
                                    ee2 a5 = tr33.a();
                                    if (a5 != null) {
                                        AppCompatCheckBox appCompatCheckBox = a5.r;
                                        if (appCompatCheckBox != null) {
                                            appCompatCheckBox.setChecked(false);
                                        }
                                    }
                                } else {
                                    kd4.d("mBinding");
                                    throw null;
                                }
                            } else {
                                kd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            kd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        tr3<ee2> tr34 = this.k;
                        if (tr34 != null) {
                            ee2 a6 = tr34.a();
                            if (a6 != null) {
                                FlexibleTextView flexibleTextView3 = a6.t;
                                if (flexibleTextView3 != null) {
                                    flexibleTextView3.setText("");
                                }
                            }
                            tr3<ee2> tr35 = this.k;
                            if (tr35 != null) {
                                ee2 a7 = tr35.a();
                                if (a7 != null) {
                                    FlexibleTextView flexibleTextView4 = a7.t;
                                    if (flexibleTextView4 != null) {
                                        flexibleTextView4.setVisibility(8);
                                    }
                                }
                                tr3<ee2> tr36 = this.k;
                                if (tr36 != null) {
                                    ee2 a8 = tr36.a();
                                    if (a8 != null) {
                                        AppCompatCheckBox appCompatCheckBox2 = a8.r;
                                        if (appCompatCheckBox2 != null) {
                                            appCompatCheckBox2.setChecked(true);
                                        }
                                    }
                                } else {
                                    kd4.d("mBinding");
                                    throw null;
                                }
                            } else {
                                kd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            kd4.d("mBinding");
                            throw null;
                        }
                    }
                    z2 = true;
                }
            } else {
                if (contactWrapper.getCurrentHandGroup() != i2) {
                    tr3<ee2> tr37 = this.k;
                    if (tr37 != null) {
                        ee2 a9 = tr37.a();
                        if (a9 != null) {
                            FlexibleTextView flexibleTextView5 = a9.s;
                            if (flexibleTextView5 != null) {
                                pd4 pd42 = pd4.a;
                                String a10 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_InMyNotifications_Text__AssignedToNumber);
                                kd4.a((Object) a10, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                                Object[] objArr2 = {Integer.valueOf(contactWrapper.getCurrentHandGroup())};
                                String format2 = String.format(a10, Arrays.copyOf(objArr2, objArr2.length));
                                kd4.a((Object) format2, "java.lang.String.format(format, *args)");
                                flexibleTextView5.setText(format2);
                            }
                        }
                        tr3<ee2> tr38 = this.k;
                        if (tr38 != null) {
                            ee2 a11 = tr38.a();
                            if (a11 != null) {
                                FlexibleTextView flexibleTextView6 = a11.s;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                }
                            }
                            tr3<ee2> tr39 = this.k;
                            if (tr39 != null) {
                                ee2 a12 = tr39.a();
                                if (a12 != null) {
                                    AppCompatCheckBox appCompatCheckBox3 = a12.q;
                                    if (appCompatCheckBox3 != null) {
                                        appCompatCheckBox3.setChecked(false);
                                    }
                                }
                            } else {
                                kd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            kd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        kd4.d("mBinding");
                        throw null;
                    }
                } else {
                    tr3<ee2> tr310 = this.k;
                    if (tr310 != null) {
                        ee2 a13 = tr310.a();
                        if (a13 != null) {
                            FlexibleTextView flexibleTextView7 = a13.s;
                            if (flexibleTextView7 != null) {
                                flexibleTextView7.setText("");
                            }
                        }
                        tr3<ee2> tr311 = this.k;
                        if (tr311 != null) {
                            ee2 a14 = tr311.a();
                            if (a14 != null) {
                                FlexibleTextView flexibleTextView8 = a14.s;
                                if (flexibleTextView8 != null) {
                                    flexibleTextView8.setVisibility(8);
                                }
                            }
                            tr3<ee2> tr312 = this.k;
                            if (tr312 != null) {
                                ee2 a15 = tr312.a();
                                if (a15 != null) {
                                    AppCompatCheckBox appCompatCheckBox4 = a15.q;
                                    if (appCompatCheckBox4 != null) {
                                        appCompatCheckBox4.setChecked(true);
                                    }
                                }
                            } else {
                                kd4.d("mBinding");
                                throw null;
                            }
                        } else {
                            kd4.d("mBinding");
                            throw null;
                        }
                    } else {
                        kd4.d("mBinding");
                        throw null;
                    }
                }
                z = true;
            }
        }
        if (!z) {
            tr3<ee2> tr313 = this.k;
            if (tr313 != null) {
                ee2 a16 = tr313.a();
                if (a16 != null) {
                    FlexibleTextView flexibleTextView9 = a16.s;
                    if (flexibleTextView9 != null) {
                        flexibleTextView9.setText("");
                    }
                }
                tr3<ee2> tr314 = this.k;
                if (tr314 != null) {
                    ee2 a17 = tr314.a();
                    if (a17 != null) {
                        FlexibleTextView flexibleTextView10 = a17.s;
                        if (flexibleTextView10 != null) {
                            flexibleTextView10.setVisibility(8);
                        }
                    }
                    tr3<ee2> tr315 = this.k;
                    if (tr315 != null) {
                        ee2 a18 = tr315.a();
                        if (a18 != null) {
                            AppCompatCheckBox appCompatCheckBox5 = a18.q;
                            if (appCompatCheckBox5 != null) {
                                appCompatCheckBox5.setChecked(false);
                            }
                        }
                    } else {
                        kd4.d("mBinding");
                        throw null;
                    }
                } else {
                    kd4.d("mBinding");
                    throw null;
                }
            } else {
                kd4.d("mBinding");
                throw null;
            }
        }
        if (!z2) {
            tr3<ee2> tr316 = this.k;
            if (tr316 != null) {
                ee2 a19 = tr316.a();
                if (a19 != null) {
                    FlexibleTextView flexibleTextView11 = a19.t;
                    if (flexibleTextView11 != null) {
                        flexibleTextView11.setText("");
                    }
                }
                tr3<ee2> tr317 = this.k;
                if (tr317 != null) {
                    ee2 a20 = tr317.a();
                    if (a20 != null) {
                        FlexibleTextView flexibleTextView12 = a20.t;
                        if (flexibleTextView12 != null) {
                            flexibleTextView12.setVisibility(8);
                        }
                    }
                    tr3<ee2> tr318 = this.k;
                    if (tr318 != null) {
                        ee2 a21 = tr318.a();
                        if (a21 != null) {
                            AppCompatCheckBox appCompatCheckBox6 = a21.r;
                            if (appCompatCheckBox6 != null) {
                                appCompatCheckBox6.setChecked(false);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    kd4.d("mBinding");
                    throw null;
                }
                kd4.d("mBinding");
                throw null;
            }
            kd4.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(ContactWrapper contactWrapper) {
        kd4.b(contactWrapper, "contactWrapper");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            int currentHandGroup = contactWrapper.getCurrentHandGroup();
            w03 w03 = this.j;
            if (w03 != null) {
                ds3.a(childFragmentManager, contactWrapper, currentHandGroup, w03.h());
            } else {
                kd4.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(ArrayList<ContactWrapper> arrayList) {
        kd4.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        if (str.hashCode() == 1018078562 && str.equals("CONFIRM_REASSIGN_CONTACT") && i == R.id.tv_ok) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null) {
                ContactWrapper contactWrapper = (ContactWrapper) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER");
                if (contactWrapper != null) {
                    w03 w03 = this.j;
                    if (w03 != null) {
                        w03.a(contactWrapper);
                    } else {
                        kd4.d("mPresenter");
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        kd4.b(list, "perms");
        FLogger.INSTANCE.getLocal().d(m, ".Inside onPermissionsDenied");
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = m;
            local.d(str2, "Permission Denied : " + str);
        }
        if (pq4.a((Fragment) this, list) && isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.z(childFragmentManager);
        }
    }
}
