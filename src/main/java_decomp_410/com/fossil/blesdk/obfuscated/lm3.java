package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.km3;
import com.fossil.blesdk.obfuscated.nm3;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;
import kotlin.Pair;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class lm3 extends as2 implements gn3, ws3.g, ws3.h {
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public dn3 k;
    @DexIgnore
    public ym3 l;
    @DexIgnore
    public HashMap m;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = lm3.class.getSimpleName();
            kd4.a((Object) simpleName, "PairingFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final lm3 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            lm3 lm3 = new lm3();
            lm3.setArguments(bundle);
            return lm3;
        }
    }

    @DexIgnore
    public void A() {
        ym3 ym3 = this.l;
        if (ym3 == null || !(ym3 instanceof nm3)) {
            nm3.a aVar = nm3.n;
            dn3 dn3 = this.k;
            if (dn3 != null) {
                this.l = aVar.a(dn3.i());
                ym3 ym32 = this.l;
                if (ym32 != null) {
                    a(ym32, "", R.id.content);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        }
        ym3 ym33 = this.l;
        if (ym33 != null) {
            dn3 dn32 = this.k;
            if (dn32 != null) {
                ym33.a(dn32);
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void O() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void Q(String str) {
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void b(String str, boolean z) {
        kd4.b(str, "serial");
        ym3 ym3 = this.l;
        if (ym3 instanceof om3) {
            ((om3) ym3).v0();
            return;
        }
        this.l = om3.r.a(str, z, 1);
        ym3 ym32 = this.l;
        if (ym32 != null) {
            a(ym32, "", R.id.content);
            ym3 ym33 = this.l;
            if (ym33 != null) {
                dn3 dn3 = this.k;
                if (dn3 != null) {
                    ym33.a(dn3);
                } else {
                    kd4.d("mPairingPresenter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void c(int i, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = n.a();
        local.d(a2, "showDeviceConnectFail() - errorCode = " + i);
        if (!isActive() || getActivity() == null) {
            return;
        }
        if (str != null) {
            n();
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                aVar.a(activity, str);
                return;
            }
            kd4.a();
            throw null;
        }
        n();
        TroubleshootingActivity.a aVar2 = TroubleshootingActivity.C;
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            kd4.a((Object) activity2, "activity!!");
            aVar2.a(activity2);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void h() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.C;
            kd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void k(List<Pair<ShineDevice, String>> list) {
        kd4.b(list, "listDevices");
        ym3 ym3 = this.l;
        if (ym3 == null || !(ym3 instanceof km3)) {
            km3.a aVar = km3.q;
            dn3 dn3 = this.k;
            if (dn3 != null) {
                km3 a2 = aVar.a(dn3.i());
                a2.h(list);
                this.l = a2;
                ym3 ym32 = this.l;
                if (ym32 != null) {
                    a(ym32, "", R.id.content);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        }
        ym3 ym33 = this.l;
        if (ym33 != null) {
            dn3 dn32 = this.k;
            if (dn32 != null) {
                ym33.a(dn32);
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void l(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            String e = PortfolioApp.W.c().e();
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.f(e, childFragmentManager);
        }
    }

    @DexIgnore
    public void n() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            kd4.a((Object) activity, "activity!!");
            if (!activity.isFinishing()) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    kd4.a((Object) activity2, "activity!!");
                    if (!activity2.isDestroyed()) {
                        FragmentActivity activity3 = getActivity();
                        if (activity3 != null) {
                            activity3.finish();
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.fragment_pairing, viewGroup, false);
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        dn3 dn3 = this.k;
        if (dn3 != null) {
            dn3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        dn3 dn3 = this.k;
        if (dn3 != null) {
            dn3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        kd4.b(bundle, "outState");
        dn3 dn3 = this.k;
        if (dn3 != null) {
            if (dn3.h() != null) {
                dn3 dn32 = this.k;
                if (dn32 != null) {
                    ShineDevice h = dn32.h();
                    if (h != null) {
                        bundle.putParcelable("PAIRING_DEVICE", h);
                    }
                } else {
                    kd4.d("mPairingPresenter");
                    throw null;
                }
            }
            super.onSaveInstanceState(bundle);
            return;
        }
        kd4.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle != null && bundle.containsKey("PAIRING_DEVICE")) {
            dn3 dn3 = this.k;
            if (dn3 != null) {
                Parcelable parcelable = bundle.getParcelable("PAIRING_DEVICE");
                if (parcelable != null) {
                    dn3.a((ShineDevice) parcelable);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            dn3 dn32 = this.k;
            if (dn32 != null) {
                dn32.b(arguments.getBoolean("IS_ONBOARDING_FLOW"));
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        }
        this.l = (ym3) getChildFragmentManager().a((int) R.id.content);
        if (this.l == null) {
            nm3.a aVar = nm3.n;
            dn3 dn33 = this.k;
            if (dn33 != null) {
                this.l = aVar.a(dn33.i());
                ym3 ym3 = this.l;
                if (ym3 != null) {
                    a(ym3, "", R.id.content);
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        }
        ym3 ym32 = this.l;
        if (ym32 != null) {
            dn3 dn34 = this.k;
            if (dn34 != null) {
                ym32.a(dn34);
                dn3 dn35 = this.k;
                if (dn35 != null) {
                    ym3 ym33 = this.l;
                    dn35.a((ym33 instanceof nm3) || (ym33 instanceof km3));
                    R("scan_device_view");
                    return;
                }
                kd4.d("mPairingPresenter");
                throw null;
            }
            kd4.d("mPairingPresenter");
            throw null;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void s(String str) {
        kd4.b(str, "serial");
        if (isActive()) {
            ds3 ds3 = ds3.c;
            String e = PortfolioApp.W.c().e();
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.e(e, childFragmentManager);
        }
    }

    @DexIgnore
    public void t() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.C;
            kd4.a((Object) activity, "it");
            dn3 dn3 = this.k;
            if (dn3 != null) {
                aVar.a(activity, dn3.i());
            } else {
                kd4.d("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void y() {
        if (isActive()) {
            ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
            fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
            fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Text__PleaseCheckYourInternetConnectionAnd));
            fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionwithActions_CTA__TryAgain));
            fVar.a((int) R.id.tv_ok);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    public void a(dn3 dn3) {
        kd4.b(dn3, "presenter");
        this.k = dn3;
    }

    @DexIgnore
    public void a(String str, boolean z) {
        kd4.b(str, "serial");
        ym3 ym3 = this.l;
        if (ym3 instanceof om3) {
            ((om3) ym3).n0();
            return;
        }
        this.l = om3.r.a(str, z, 0);
        ym3 ym32 = this.l;
        if (ym32 != null) {
            a(ym32, "", R.id.content);
            ym3 ym33 = this.l;
            if (ym33 != null) {
                dn3 dn3 = this.k;
                if (dn3 != null) {
                    ym33.a(dn3);
                } else {
                    kd4.d("mPairingPresenter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public void h(List<Pair<ShineDevice, String>> list) {
        kd4.b(list, "deviceList");
        ym3 ym3 = this.l;
        if (ym3 != null && (ym3 instanceof km3)) {
            ((km3) ym3).h(list);
        }
    }

    @DexIgnore
    public void a(String str, boolean z, boolean z2) {
        kd4.b(str, "serial");
        ym3 ym3 = this.l;
        if (ym3 instanceof om3) {
            ((om3) ym3).b(z2);
            return;
        }
        this.l = om3.r.a(str, z, z2 ? 2 : 3);
        ym3 ym32 = this.l;
        if (ym32 != null) {
            a(ym32, "", R.id.content);
            ym3 ym33 = this.l;
            if (ym33 != null) {
                dn3 dn3 = this.k;
                if (dn3 != null) {
                    ym33.a(dn3);
                } else {
                    kd4.d("mPairingPresenter");
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        } else {
            kd4.a();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001c, code lost:
        if (r6.equals("SERVER_ERROR") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00f2, code lost:
        if (r6.equals("SERVER_MAINTENANCE") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00f4, code lost:
        n();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00f8, code lost:
        r0 = getActivity();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00fc, code lost:
        if (r0 == null) goto L_0x0104;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00fe, code lost:
        ((com.portfolio.platform.ui.BaseActivity) r0).a(r6, r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0109, code lost:
        throw new kotlin.TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    @DexIgnore
    public void a(String str, int i, Intent intent) {
        kd4.b(str, "tag");
        switch (str.hashCode()) {
            case -1636680713:
                break;
            case -879828873:
                if (str.equals("NETWORK_ERROR")) {
                    if (i == R.id.tv_ok) {
                        n();
                        return;
                    }
                    return;
                }
                break;
            case 39550276:
                if (str.equals("SWITCH_DEVICE_SYNC_FAIL")) {
                    if (i == R.id.tv_ok) {
                        dn3 dn3 = this.k;
                        if (dn3 != null) {
                            dn3.l();
                            return;
                        } else {
                            kd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 603997695:
                if (str.equals("SWITCH_DEVICE_WORKOUT")) {
                    String stringExtra = intent != null ? intent.getStringExtra("SERIAL") : null;
                    if (stringExtra == null) {
                        return;
                    }
                    if (i == R.id.tv_cancel) {
                        dn3 dn32 = this.k;
                        if (dn32 != null) {
                            dn32.b(stringExtra);
                            return;
                        } else {
                            kd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else if (i == R.id.tv_ok) {
                        dn3 dn33 = this.k;
                        if (dn33 != null) {
                            dn33.a(stringExtra);
                            return;
                        } else {
                            kd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else {
                        return;
                    }
                }
                break;
            case 927511079:
                if (str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
                    if (i == R.id.fb_try_again) {
                        dn3 dn34 = this.k;
                        if (dn34 != null) {
                            dn34.m();
                            return;
                        } else {
                            kd4.d("mPairingPresenter");
                            throw null;
                        }
                    } else if (i != R.id.ftv_contact_cs) {
                        if (i == R.id.iv_close) {
                            n();
                            return;
                        }
                        return;
                    } else if (getActivity() != null) {
                        HelpActivity.a aVar = HelpActivity.C;
                        FragmentActivity activity = getActivity();
                        if (activity != null) {
                            kd4.a((Object) activity, "activity!!");
                            aVar.a(activity);
                            return;
                        }
                        kd4.a();
                        throw null;
                    } else {
                        return;
                    }
                }
                break;
            case 1008390942:
                if (str.equals("NO_INTERNET_CONNECTION")) {
                    if (i == R.id.ftv_go_to_setting) {
                        FragmentActivity activity2 = getActivity();
                        if (activity2 != null) {
                            ((BaseActivity) activity2).m();
                            n();
                            return;
                        }
                        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                    } else if (i == R.id.tv_ok) {
                        n();
                        return;
                    } else {
                        return;
                    }
                }
                break;
            case 1178575340:
                break;
        }
    }

    @DexIgnore
    public void a(int i, String str, String str2) {
        kd4.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = n.a();
        local.d(a2, "showDeviceConnectFail due to network - errorCode = " + i);
        if (isActive()) {
            ds3 ds3 = ds3.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            kd4.a((Object) childFragmentManager, "childFragmentManager");
            ds3.a(i, str2, childFragmentManager);
        }
    }
}
