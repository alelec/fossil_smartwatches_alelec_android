package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t91 {
    @DexIgnore
    public static /* final */ r91 a; // = c();
    @DexIgnore
    public static /* final */ r91 b; // = new s91();

    @DexIgnore
    public static r91 a() {
        return a;
    }

    @DexIgnore
    public static r91 b() {
        return b;
    }

    @DexIgnore
    public static r91 c() {
        try {
            return (r91) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
