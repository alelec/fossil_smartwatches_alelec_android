package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.ij0;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vf0 extends cg0 {
    @DexIgnore
    public /* final */ Map<de0.f, uf0> f;
    @DexIgnore
    public /* final */ /* synthetic */ sf0 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vf0(sf0 sf0, Map<de0.f, uf0> map) {
        super(sf0, (tf0) null);
        this.g = sf0;
        this.f = map;
    }

    @DexIgnore
    public final void a() {
        sj0 sj0 = new sj0(this.g.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (de0.f next : this.f.keySet()) {
            if (!next.h() || this.f.get(next).c) {
                arrayList2.add(next);
            } else {
                arrayList.add(next);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i = sj0.a(this.g.c, (de0.f) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList2.size();
            while (i2 < size2) {
                Object obj2 = arrayList2.get(i2);
                i2++;
                i = sj0.a(this.g.c, (de0.f) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.g.a.a((og0) new wf0(this, this.g, new ud0(i, (PendingIntent) null)));
            return;
        }
        if (this.g.m && this.g.k != null) {
            this.g.k.b();
        }
        for (de0.f next2 : this.f.keySet()) {
            ij0.c cVar = this.f.get(next2);
            if (!next2.h() || sj0.a(this.g.c, next2) == 0) {
                next2.a(cVar);
            } else {
                this.g.a.a((og0) new xf0(this, this.g, cVar));
            }
        }
    }
}
