package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Calendar;
import java.util.Date;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fm3 extends bm3 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ int m; // = (Calendar.getInstance().get(1) - 110);
    @DexIgnore
    public Calendar f;
    @DexIgnore
    public int g; // = this.f.get(5);
    @DexIgnore
    public int h; // = this.f.get(2);
    @DexIgnore
    public int i; // = this.f.get(1);
    @DexIgnore
    public int j; // = this.f.getActualMaximum(5);
    @DexIgnore
    public /* final */ cm3 k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = fm3.class.getSimpleName();
        kd4.a((Object) simpleName, "BirthdayPresenter::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public fm3(cm3 cm3) {
        kd4.b(cm3, "mView");
        this.k = cm3;
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        this.f = instance;
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "Current day: " + this.g + ", new day: " + i2);
        if (i2 >= 1 && i2 <= this.j) {
            this.g = i2;
        }
    }

    @DexIgnore
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "Current month: " + this.h + ", new month: " + i2);
        if (i2 >= 1 && i2 <= 12) {
            this.h = i2;
            k();
        }
    }

    @DexIgnore
    public void c(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "Current year: " + this.i + ", new year: " + i2);
        if (i2 >= m) {
            this.i = i2;
        }
    }

    @DexIgnore
    public void f() {
        i();
        this.k.a(new Pair(1, Integer.valueOf(this.j)), new Pair(1, 12), new Pair(Integer.valueOf(m), Integer.valueOf(this.f.get(1))));
    }

    @DexIgnore
    public void g() {
        i();
    }

    @DexIgnore
    public void h() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "confirmBirthday " + this.g + '/' + this.h + '/' + this.i);
        Calendar instance = Calendar.getInstance();
        instance.set(this.i, this.h + -1, this.g);
        cm3 cm3 = this.k;
        kd4.a((Object) instance, "calendar");
        Date time = instance.getTime();
        kd4.a((Object) time, "calendar.time");
        cm3.c(time);
    }

    @DexIgnore
    public final void i() {
        Calendar instance = Calendar.getInstance();
        kd4.a((Object) instance, "Calendar.getInstance()");
        this.f = instance;
        this.g = this.f.get(5);
        this.h = this.f.get(2);
        this.i = this.f.get(1);
        this.j = this.f.getActualMaximum(5);
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    public final void k() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.i, this.h - 1, 1);
        this.j = instance.getActualMaximum(5);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "Day range of " + this.h + '/' + this.i + ": " + this.j);
        this.k.b(1, this.j);
    }
}
