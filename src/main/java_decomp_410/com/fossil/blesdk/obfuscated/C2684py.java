package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.py */
public class C2684py {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.concurrent.atomic.AtomicLong f8479a; // = new java.util.concurrent.atomic.AtomicLong(0);

    @DexIgnore
    /* renamed from: b */
    public static java.lang.String f8480b;

    @DexIgnore
    public C2684py(p011io.fabric.sdk.android.services.common.IdManager idManager) {
        byte[] bArr = new byte[10];
        mo14912c(bArr);
        mo14911b(bArr);
        mo14910a(bArr);
        java.lang.String c = p011io.fabric.sdk.android.services.common.CommonUtils.m36884c(idManager.mo44549e());
        java.lang.String a = p011io.fabric.sdk.android.services.common.CommonUtils.m36867a(bArr);
        f8480b = java.lang.String.format(java.util.Locale.US, "%s-%s-%s-%s", new java.lang.Object[]{a.substring(0, 12), a.substring(12, 16), a.subSequence(16, 20), c.substring(0, 12)}).toUpperCase(java.util.Locale.US);
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14910a(byte[] bArr) {
        byte[] b = m12487b((long) java.lang.Integer.valueOf(android.os.Process.myPid()).shortValue());
        bArr[8] = b[0];
        bArr[9] = b[1];
    }

    @DexIgnore
    /* renamed from: b */
    public final void mo14911b(byte[] bArr) {
        byte[] b = m12487b(f8479a.incrementAndGet());
        bArr[6] = b[0];
        bArr[7] = b[1];
    }

    @DexIgnore
    /* renamed from: c */
    public final void mo14912c(byte[] bArr) {
        long time = new java.util.Date().getTime();
        byte[] a = m12486a(time / 1000);
        bArr[0] = a[0];
        bArr[1] = a[1];
        bArr[2] = a[2];
        bArr[3] = a[3];
        byte[] b = m12487b(time % 1000);
        bArr[4] = b[0];
        bArr[5] = b[1];
    }

    @DexIgnore
    public java.lang.String toString() {
        return f8480b;
    }

    @DexIgnore
    /* renamed from: b */
    public static byte[] m12487b(long j) {
        java.nio.ByteBuffer allocate = java.nio.ByteBuffer.allocate(2);
        allocate.putShort((short) ((int) j));
        allocate.order(java.nio.ByteOrder.BIG_ENDIAN);
        allocate.position(0);
        return allocate.array();
    }

    @DexIgnore
    /* renamed from: a */
    public static byte[] m12486a(long j) {
        java.nio.ByteBuffer allocate = java.nio.ByteBuffer.allocate(4);
        allocate.putInt((int) j);
        allocate.order(java.nio.ByteOrder.BIG_ENDIAN);
        allocate.position(0);
        return allocate.array();
    }
}
