package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kp3 extends zr2 implements jp3 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a n; // = new a((fd4) null);
    @DexIgnore
    public tr3<ag2> j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public HashMap l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kp3.m;
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }

        @DexIgnore
        public final kp3 a(boolean z) {
            kp3 kp3 = new kp3();
            kp3.k = z;
            return kp3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kp3 e;

        @DexIgnore
        public b(kp3 kp3) {
            this.e = kp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kp3 e;

        @DexIgnore
        public c(kp3 kp3) {
            this.e = kp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kp3 e;

        @DexIgnore
        public d(kp3 kp3) {
            this.e = kp3;
        }

        @DexIgnore
        public final void onClick(View view) {
            HelpActivity.a aVar = HelpActivity.C;
            FragmentActivity activity = this.e.getActivity();
            if (activity != null) {
                kd4.a((Object) activity, "activity!!");
                aVar.a(activity);
                FragmentActivity activity2 = this.e.getActivity();
                if (activity2 != null) {
                    activity2.finish();
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                kd4.a();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = kp3.class.getSimpleName();
        kd4.a((Object) simpleName, "TroubleshootingFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String R0() {
        return m;
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void a(ip3 ip3) {
        kd4.b(ip3, "presenter");
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.j = new tr3<>(this, (ag2) qa.a(layoutInflater, R.layout.fragment_troubleshooting, viewGroup, false, O0()));
        tr3<ag2> tr3 = this.j;
        if (tr3 != null) {
            ag2 a2 = tr3.a();
            if (a2 != null) {
                kd4.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            kd4.a();
            throw null;
        }
        kd4.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        kd4.b(view, "view");
        super.onViewCreated(view, bundle);
        tr3<ag2> tr3 = this.j;
        if (tr3 != null) {
            ag2 a2 = tr3.a();
            if (a2 != null) {
                View findViewById = a2.s.findViewById(R.id.ll_battery_reinstall);
                if (this.k) {
                    kd4.a((Object) findViewById, "batteryText");
                    findViewById.setVisibility(8);
                }
                a2.q.setOnClickListener(new b(this));
                a2.t.setOnClickListener(new c(this));
                a2.r.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        kd4.d("mBinding");
        throw null;
    }
}
