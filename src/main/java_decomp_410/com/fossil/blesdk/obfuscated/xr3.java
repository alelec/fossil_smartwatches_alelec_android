package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.hr2;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xr3 extends ContentObserver implements hr2.a {
    @DexIgnore
    public static /* final */ String l; // = xr3.class.getSimpleName();
    @DexIgnore
    public /* final */ HashMap<String, Integer> a; // = new HashMap<>();
    @DexIgnore
    public /* final */ NotificationsRepository b;
    @DexIgnore
    public /* final */ ContentResolver c;
    @DexIgnore
    public /* final */ h42 d;
    @DexIgnore
    public /* final */ PortfolioApp e;
    @DexIgnore
    public /* final */ hr2 f;
    @DexIgnore
    public /* final */ en2 g;
    @DexIgnore
    public DeviceRepository h;
    @DexIgnore
    public NotificationSettingsDatabase i;
    @DexIgnore
    public long j;
    @DexIgnore
    public long k;

    @DexIgnore
    public xr3(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, h42 h42, ContentResolver contentResolver, hr2 hr2, en2 en2) {
        super((Handler) null);
        new HashMap();
        this.j = System.currentTimeMillis();
        this.k = -1;
        this.e = portfolioApp;
        this.f = hr2;
        st1.a(h42, (Object) "appExecutors cannot be NULL");
        this.d = h42;
        st1.a(notificationsRepository, (Object) "repository cannot be nulL!");
        this.b = notificationsRepository;
        st1.a(deviceRepository, (Object) "deviceRepository cannot be nulL!");
        this.h = deviceRepository;
        st1.a(notificationSettingsDatabase, (Object) "notificationSettingsDatabase cannot be null!");
        this.i = notificationSettingsDatabase;
        st1.a(contentResolver, (Object) "contentResolver cannot be null!");
        this.c = contentResolver;
        this.g = en2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003e A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    public final boolean a(String str) {
        boolean z;
        boolean z2 = false;
        Cursor cursor = null;
        try {
            cursor = this.c.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, (String[]) null, "contact_id = ?", new String[]{str}, (String) null);
            if (cursor == null) {
                z = false;
                if (cursor != null) {
                    return z;
                }
                cursor.close();
                return z;
            }
            loop0:
            while (true) {
                z = false;
                while (true) {
                    try {
                        if (!cursor.moveToNext()) {
                            break loop0;
                        } else if (cursor.getInt(cursor.getColumnIndex("starred")) == 1) {
                            z = true;
                        }
                    } catch (Exception e2) {
                        Exception exc = e2;
                        z2 = z;
                        e = exc;
                        try {
                            e.printStackTrace();
                            return z2;
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }
                }
            }
            if (cursor != null) {
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return z2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        if (r2 == null) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if (r2 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
        r2.close();
     */
    @DexIgnore
    public final String b(String str) {
        String str2 = "";
        Cursor cursor = null;
        try {
            cursor = this.c.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"photo_thumb_uri"}, "contact_id = ?", new String[]{str}, (String) null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    str2 = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        if (r1 == null) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0037, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0028, code lost:
        if (r1 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        r1.close();
     */
    @DexIgnore
    public final String c(String str) {
        String str2 = "";
        Cursor cursor = null;
        try {
            cursor = this.c.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, (String[]) null, "contact_id = ?", new String[]{str}, (String) null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    str2 = cursor.getString(cursor.getColumnIndex("display_name"));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        if (r2 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (r2 == null) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        return r1;
     */
    @DexIgnore
    public final List<String> d(String str) {
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = this.c.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, "contact_id = ?", new String[]{str}, (String) null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String string = cursor.getString(cursor.getColumnIndex("data1"));
                    if (!TextUtils.isEmpty(string) && !ts3.a((List<String>) arrayList, string).booleanValue()) {
                        arrayList.add(string);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0051, code lost:
        if (r3 == null) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0059, code lost:
        return !a(r11, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
        if (r3 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        r3.close();
     */
    @DexIgnore
    public final boolean e(String str) {
        int i2 = 0;
        Cursor cursor = null;
        try {
            cursor = this.c.query(ContactsContract.RawContacts.CONTENT_URI, new String[]{"contact_id", "version"}, "contact_id = ?", new String[]{str}, (String) null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    i2 += cursor.getInt(cursor.getColumnIndex("version"));
                }
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, "isHasNewVersion e=" + e2);
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void f(String str) {
        synchronized (this.a) {
            int size = this.a.size();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, "removeOtherContactVersionHashMap. Size = " + size);
            LinkedList<String> linkedList = new LinkedList<>();
            for (String next : this.a.keySet()) {
                if (!next.equals(str)) {
                    linkedList.add(next);
                }
            }
            for (String str3 : linkedList) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = l;
                local2.d(str4, "remove from contact versions, contactId= " + str3);
                this.a.remove(str3);
            }
        }
    }

    @DexIgnore
    public void onChange(boolean z, Uri uri) {
        String str;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = l;
        local.d(str2, "onChange selftChange=" + z + ", uri=" + uri + "update = " + this.k);
        if (System.currentTimeMillis() - this.j >= 1000) {
            this.j = System.currentTimeMillis();
            if (ns3.a.a((Context) this.e, "android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG", "android.permission.READ_CONTACTS", "android.permission.READ_SMS")) {
                Cursor query = this.c.query(ContactsContract.DeletedContacts.CONTENT_URI, new String[]{"contact_id", "contact_deleted_timestamp"}, (String) null, (String[]) null, "contact_deleted_timestamp Desc");
                if (query != null && query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex("contact_id"));
                    Long valueOf = Long.valueOf(query.getLong(query.getColumnIndex("contact_deleted_timestamp")));
                    if (!(string == null || this.k == valueOf.longValue())) {
                        boolean z2 = this.k == -1;
                        this.k = valueOf.longValue();
                        int intValue = Integer.valueOf(string).intValue();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = l;
                        local2.d(str3, "Last deleted happen for contactId=" + intValue);
                        ArrayList<ContactGroup> arrayList = new ArrayList<>();
                        List<ContactGroup> allContactGroups = this.b.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                        List<ContactGroup> allContactGroups2 = this.b.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                        if (allContactGroups != null) {
                            arrayList.addAll(allContactGroups);
                        }
                        if (allContactGroups2 != null) {
                            arrayList.addAll(allContactGroups2);
                        }
                        ArrayList<PhoneFavoritesContact> arrayList2 = new ArrayList<>();
                        if (!arrayList.isEmpty()) {
                            boolean z3 = false;
                            for (ContactGroup contactGroup : arrayList) {
                                arrayList2.clear();
                                if (contactGroup.getContacts().get(0).getContactId() == intValue) {
                                    Contact contact = contactGroup.getContacts().get(0);
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String str4 = l;
                                    local3.d(str4, "Found deleted contact in app with contactName = " + contact.getDisplayName());
                                    for (PhoneNumber number : contact.getPhoneNumbers()) {
                                        arrayList2.add(new PhoneFavoritesContact(number.getNumber()));
                                    }
                                    this.b.removeContact(contact);
                                    this.b.removeContactGroup(contactGroup);
                                    for (PhoneFavoritesContact removePhoneFavoritesContact : arrayList2) {
                                        this.b.removePhoneFavoritesContact(removePhoneFavoritesContact);
                                    }
                                    z3 = true;
                                }
                            }
                            if (z3) {
                                d();
                            }
                        }
                        if (!z2) {
                            query.close();
                            return;
                        }
                    }
                    query.close();
                }
                Cursor query2 = this.c.query(ContactsContract.Contacts.CONTENT_URI, new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX}, (String) null, (String[]) null, "contact_last_updated_timestamp Desc");
                if (query2 == null || !query2.moveToFirst()) {
                    str = "";
                } else {
                    str = query2.getString(query2.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str5 = l;
                    local4.d(str5, "Last updated happen for contactId=" + str);
                    query2.close();
                }
                if (TextUtils.isEmpty(str)) {
                    FLogger.INSTANCE.getLocal().d(l, "No change for contact, return");
                    return;
                }
                String c2 = c(str);
                boolean a2 = a(str);
                String b2 = b(str);
                List<String> d2 = d(str);
                this.g.k(true);
                if (e(str)) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str6 = l;
                    local5.d(str6, "Update Contact: " + str + " - contactName: " + c2 + " - contactImageUri: " + b2 + " - contactPhone: " + d2 + " - isFavorites:" + a2);
                    this.f.a(str, c2, d2, b2);
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2) {
        synchronized (this.a) {
            Integer num = this.a.get(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, " Enter isVersionLasted contactId= " + str + " savedVersion=" + num + " newVersion:" + i2);
            if (num == null) {
                FLogger.INSTANCE.getLocal().d(l, "Contact not found, let's update");
                this.a.put(str, Integer.valueOf(i2));
                return false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = l;
            local2.d(str3, "Found contact " + str);
            if (i2 > num.intValue()) {
                FLogger.INSTANCE.getLocal().d(l, "Saved version of this contact is old, let's update");
                this.a.put(str, Integer.valueOf(i2));
                return false;
            }
            FLogger.INSTANCE.getLocal().d(l, "Saved version of this contact is lasted, skip!");
            f(str);
            return true;
        }
    }

    @DexIgnore
    public void b() {
        this.f.a(this);
    }

    @DexIgnore
    public void c() {
        this.f.a((hr2.a) null);
    }

    @DexIgnore
    public final void d() {
        AppNotificationFilterSettings appNotificationFilterSettings;
        FLogger.INSTANCE.getLocal().d(l, "updateNotificationSettings()");
        String e2 = PortfolioApp.R.e();
        if (!TextUtils.isEmpty(e2)) {
            if (!FossilDeviceSerialPatternUtil.isDianaDevice(e2)) {
                appNotificationFilterSettings = NotificationAppHelper.b.a(this.b.getAllNotificationsByHour(e2, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), NotificationAppHelper.b.a(this.h.getSkuModelBySerialPrefix(DeviceHelper.o.b(e2)), e2));
            } else {
                appNotificationFilterSettings = new AppNotificationFilterSettings(NotificationAppHelper.b.a(this.e, this.i, this.g), System.currentTimeMillis());
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = l;
            local.d(str, "updateNotificationSettings - data = " + appNotificationFilterSettings);
            this.d.b().execute(new rr3(appNotificationFilterSettings, e2));
        }
    }

    @DexIgnore
    public void a() {
        d();
    }
}
