package com.fossil.blesdk.obfuscated;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class tr3<T> {
    @DexIgnore
    public T a;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends FragmentManager.b {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;
        @DexIgnore
        public /* final */ /* synthetic */ FragmentManager b;

        @DexIgnore
        public a(Fragment fragment, FragmentManager fragmentManager) {
            this.a = fragment;
            this.b = fragmentManager;
        }

        @DexIgnore
        public void g(FragmentManager fragmentManager, Fragment fragment) {
            if (Objects.equals(fragment, this.a)) {
                tr3.this.a = null;
                this.b.a((FragmentManager.b) this);
            }
        }
    }

    @DexIgnore
    public tr3(Fragment fragment, T t) {
        FragmentManager fragmentManager = fragment.getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.a((FragmentManager.b) new a(fragment, fragmentManager), false);
        }
        this.a = t;
    }

    @DexIgnore
    public T a() {
        return this.a;
    }
}
