package com.fossil.blesdk.obfuscated;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class wi {
    @DexIgnore
    public /* final */ DataSetObservable a; // = new DataSetObservable();

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public int a(Object obj) {
        return -1;
    }

    @DexIgnore
    public CharSequence a(int i) {
        return null;
    }

    @DexIgnore
    public Object a(ViewGroup viewGroup, int i) {
        a((View) viewGroup, i);
        throw null;
    }

    @DexIgnore
    public void a(Parcelable parcelable, ClassLoader classLoader) {
    }

    @DexIgnore
    @Deprecated
    public void a(View view) {
    }

    @DexIgnore
    public abstract boolean a(View view, Object obj);

    @DexIgnore
    public float b(int i) {
        return 1.0f;
    }

    @DexIgnore
    public Parcelable b() {
        return null;
    }

    @DexIgnore
    @Deprecated
    public void b(View view) {
    }

    @DexIgnore
    @Deprecated
    public void b(View view, int i, Object obj) {
    }

    @DexIgnore
    public void b(ViewGroup viewGroup) {
        b((View) viewGroup);
    }

    @DexIgnore
    public void c(DataSetObserver dataSetObserver) {
        this.a.unregisterObserver(dataSetObserver);
    }

    @DexIgnore
    public void a(ViewGroup viewGroup, int i, Object obj) {
        a((View) viewGroup, i, obj);
        throw null;
    }

    @DexIgnore
    public void b(ViewGroup viewGroup, int i, Object obj) {
        b((View) viewGroup, i, obj);
    }

    @DexIgnore
    public void a(ViewGroup viewGroup) {
        a((View) viewGroup);
    }

    @DexIgnore
    public void b(DataSetObserver dataSetObserver) {
        synchronized (this) {
        }
    }

    @DexIgnore
    @Deprecated
    public Object a(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    @DexIgnore
    @Deprecated
    public void a(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    @DexIgnore
    public void a(DataSetObserver dataSetObserver) {
        this.a.registerObserver(dataSetObserver);
    }
}
