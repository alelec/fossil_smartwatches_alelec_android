package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.Switch;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oa2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ SwitchCompat q;
    @DexIgnore
    public /* final */ SwitchCompat r;
    @DexIgnore
    public /* final */ RTLImageView s;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oa2(Object obj, View view, int i, SwitchCompat switchCompat, Switch switchR, SwitchCompat switchCompat2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = switchCompat;
        this.r = switchCompat2;
        this.s = rTLImageView;
    }
}
