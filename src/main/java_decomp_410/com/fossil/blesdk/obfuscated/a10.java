package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class a10 extends BluetoothCommand {
    @DexIgnore
    public boolean k;
    @DexIgnore
    public /* final */ GattCharacteristic.CharacteristicId l;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a10(BluetoothCommandId bluetoothCommandId, GattCharacteristic.CharacteristicId characteristicId, Peripheral.c cVar) {
        super(bluetoothCommandId, cVar);
        kd4.b(bluetoothCommandId, "id");
        kd4.b(characteristicId, "characteristicId");
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
        this.l = characteristicId;
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        super.a(gattOperationResult);
        this.k = false;
    }

    @DexIgnore
    public final void b(boolean z) {
        this.k = z;
    }

    @DexIgnore
    public boolean h() {
        return e().getResultCode() == BluetoothCommand.Result.ResultCode.INTERRUPTED && this.k;
    }

    @DexIgnore
    public final GattCharacteristic.CharacteristicId i() {
        return this.l;
    }

    @DexIgnore
    public JSONObject a(boolean z) {
        return wa0.a(super.a(z), JSONKey.CHANNEL_ID, this.l.getLogName$blesdk_productionRelease());
    }
}
