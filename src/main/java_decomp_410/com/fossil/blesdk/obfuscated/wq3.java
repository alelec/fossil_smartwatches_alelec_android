package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wq3 extends zr2 implements vq3, View.OnClickListener {
    @DexIgnore
    public uq3 j;
    @DexIgnore
    public HashMap k;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public void C0() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.B;
        Context context = getContext();
        if (context != null) {
            kd4.a((Object) context, "context!!");
            aVar.a(context);
            return;
        }
        kd4.a();
        throw null;
    }

    @DexIgnore
    public void F0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.G;
            kd4.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    public void N0() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean S0() {
        return false;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == R.id.fb_get_started) {
                uq3 uq3 = this.j;
                if (uq3 != null) {
                    uq3.h();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            } else if (id == R.id.iv_note) {
                uq3 uq32 = this.j;
                if (uq32 != null) {
                    uq32.i();
                } else {
                    kd4.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        kd4.b(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.fragment_welcome, viewGroup, false);
        ((FlexibleButton) inflate.findViewById(R.id.fb_get_started)).setOnClickListener(this);
        ((ImageView) inflate.findViewById(R.id.iv_note)).setOnClickListener(this);
        R("tutorial_view");
        return inflate;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        N0();
    }

    @DexIgnore
    public void onPause() {
        super.onPause();
        uq3 uq3 = this.j;
        if (uq3 != null) {
            uq3.g();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.a("");
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        uq3 uq3 = this.j;
        if (uq3 != null) {
            uq3.f();
            vl2 Q0 = Q0();
            if (Q0 != null) {
                Q0.d();
                return;
            }
            return;
        }
        kd4.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void a(uq3 uq3) {
        kd4.b(uq3, "presenter");
        this.j = uq3;
    }
}
