package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class oo4 implements yo4 {
    @DexIgnore
    public /* final */ yo4 e;

    @DexIgnore
    public oo4(yo4 yo4) {
        if (yo4 != null) {
            this.e = yo4;
            return;
        }
        throw new IllegalArgumentException("delegate == null");
    }

    @DexIgnore
    public long b(jo4 jo4, long j) throws IOException {
        return this.e.b(jo4, j);
    }

    @DexIgnore
    public final yo4 c() {
        return this.e;
    }

    @DexIgnore
    public void close() throws IOException {
        this.e.close();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + "(" + this.e.toString() + ")";
    }

    @DexIgnore
    public zo4 b() {
        return this.e.b();
    }
}
