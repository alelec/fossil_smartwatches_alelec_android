package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qq2 implements Factory<DianaSyncUseCase> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<en2> b;
    @DexIgnore
    public /* final */ Provider<vy2> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> f;
    @DexIgnore
    public /* final */ Provider<px2> g;
    @DexIgnore
    public /* final */ Provider<VerifySecretKeyUseCase> h;
    @DexIgnore
    public /* final */ Provider<UpdateFirmwareUsecase> i;
    @DexIgnore
    public /* final */ Provider<AnalyticsHelper> j;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;

    @DexIgnore
    public qq2(Provider<DianaPresetRepository> provider, Provider<en2> provider2, Provider<vy2> provider3, Provider<PortfolioApp> provider4, Provider<DeviceRepository> provider5, Provider<NotificationSettingsDatabase> provider6, Provider<px2> provider7, Provider<VerifySecretKeyUseCase> provider8, Provider<UpdateFirmwareUsecase> provider9, Provider<AnalyticsHelper> provider10, Provider<WatchFaceRepository> provider11, Provider<AlarmsRepository> provider12) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
    }

    @DexIgnore
    public static qq2 a(Provider<DianaPresetRepository> provider, Provider<en2> provider2, Provider<vy2> provider3, Provider<PortfolioApp> provider4, Provider<DeviceRepository> provider5, Provider<NotificationSettingsDatabase> provider6, Provider<px2> provider7, Provider<VerifySecretKeyUseCase> provider8, Provider<UpdateFirmwareUsecase> provider9, Provider<AnalyticsHelper> provider10, Provider<WatchFaceRepository> provider11, Provider<AlarmsRepository> provider12) {
        return new qq2(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12);
    }

    @DexIgnore
    public static DianaSyncUseCase b(Provider<DianaPresetRepository> provider, Provider<en2> provider2, Provider<vy2> provider3, Provider<PortfolioApp> provider4, Provider<DeviceRepository> provider5, Provider<NotificationSettingsDatabase> provider6, Provider<px2> provider7, Provider<VerifySecretKeyUseCase> provider8, Provider<UpdateFirmwareUsecase> provider9, Provider<AnalyticsHelper> provider10, Provider<WatchFaceRepository> provider11, Provider<AlarmsRepository> provider12) {
        return new DianaSyncUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get(), provider7.get(), provider8.get(), provider9.get(), provider10.get(), provider11.get(), provider12.get());
    }

    @DexIgnore
    public DianaSyncUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l);
    }
}
