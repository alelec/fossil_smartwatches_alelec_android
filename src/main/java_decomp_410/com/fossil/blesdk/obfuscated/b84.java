package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class b84 {
    @DexIgnore
    public /* final */ m74 a;
    @DexIgnore
    public /* final */ y74 b;
    @DexIgnore
    public /* final */ x74 c;
    @DexIgnore
    public /* final */ v74 d;
    @DexIgnore
    public /* final */ j74 e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore
    public b84(long j, m74 m74, y74 y74, x74 x74, v74 v74, j74 j74, o74 o74, int i, int i2) {
        this.f = j;
        this.a = m74;
        this.b = y74;
        this.c = x74;
        this.d = v74;
        this.e = j74;
    }

    @DexIgnore
    public boolean a(long j) {
        return this.f < j;
    }
}
