package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cg2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ScrollView q;

    @DexIgnore
    public cg2(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, ProgressBar progressBar, ScrollView scrollView) {
        super(obj, view, i);
        this.q = scrollView;
    }
}
