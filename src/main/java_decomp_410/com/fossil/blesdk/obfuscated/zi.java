package com.fossil.blesdk.obfuscated;

import android.net.Uri;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zi {
    @DexIgnore
    public /* final */ Set<a> a; // = new HashSet();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Uri a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(Uri uri, boolean z) {
            this.a = uri;
            this.b = z;
        }

        @DexIgnore
        public Uri a() {
            return this.a;
        }

        @DexIgnore
        public boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || a.class != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b != aVar.b || !this.a.equals(aVar.a)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return (this.a.hashCode() * 31) + (this.b ? 1 : 0);
        }
    }

    @DexIgnore
    public void a(Uri uri, boolean z) {
        this.a.add(new a(uri, z));
    }

    @DexIgnore
    public int b() {
        return this.a.size();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || zi.class != obj.getClass()) {
            return false;
        }
        return this.a.equals(((zi) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public Set<a> a() {
        return this.a;
    }
}
