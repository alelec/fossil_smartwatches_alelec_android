package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yd */
public class C3355yd<K, A, B> extends com.fossil.blesdk.obfuscated.C2545od<K, B> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C2545od<K, A> f11224a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> f11225b;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yd$a")
    /* renamed from: com.fossil.blesdk.obfuscated.yd$a */
    public class C3356a extends com.fossil.blesdk.obfuscated.C2545od.C2548c<K, A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2545od.C2548c f11226a;

        @DexIgnore
        public C3356a(com.fossil.blesdk.obfuscated.C2545od.C2548c cVar) {
            this.f11226a = cVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14339a(java.util.List<A> list, K k, K k2) {
            this.f11226a.mo14339a(com.fossil.blesdk.obfuscated.C2307ld.convert(com.fossil.blesdk.obfuscated.C3355yd.this.f11225b, list), k, k2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yd$b")
    /* renamed from: com.fossil.blesdk.obfuscated.yd$b */
    public class C3357b extends com.fossil.blesdk.obfuscated.C2545od.C2546a<K, A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2545od.C2546a f11228a;

        @DexIgnore
        public C3357b(com.fossil.blesdk.obfuscated.C2545od.C2546a aVar) {
            this.f11228a = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14338a(java.util.List<A> list, K k) {
            this.f11228a.mo14338a(com.fossil.blesdk.obfuscated.C2307ld.convert(com.fossil.blesdk.obfuscated.C3355yd.this.f11225b, list), k);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yd$c")
    /* renamed from: com.fossil.blesdk.obfuscated.yd$c */
    public class C3358c extends com.fossil.blesdk.obfuscated.C2545od.C2546a<K, A> {

        @DexIgnore
        /* renamed from: a */
        public /* final */ /* synthetic */ com.fossil.blesdk.obfuscated.C2545od.C2546a f11230a;

        @DexIgnore
        public C3358c(com.fossil.blesdk.obfuscated.C2545od.C2546a aVar) {
            this.f11230a = aVar;
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14338a(java.util.List<A> list, K k) {
            this.f11230a.mo14338a(com.fossil.blesdk.obfuscated.C2307ld.convert(com.fossil.blesdk.obfuscated.C3355yd.this.f11225b, list), k);
        }
    }

    @DexIgnore
    public C3355yd(com.fossil.blesdk.obfuscated.C2545od<K, A> odVar, com.fossil.blesdk.obfuscated.C2374m3<java.util.List<A>, java.util.List<B>> m3Var) {
        this.f11224a = odVar;
        this.f11225b = m3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.f11224a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.f11224a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.f11224a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(com.fossil.blesdk.obfuscated.C2545od.C2551f<K> fVar, com.fossil.blesdk.obfuscated.C2545od.C2546a<K, B> aVar) {
        this.f11224a.loadAfter(fVar, new com.fossil.blesdk.obfuscated.C3355yd.C3358c(aVar));
    }

    @DexIgnore
    public void loadBefore(com.fossil.blesdk.obfuscated.C2545od.C2551f<K> fVar, com.fossil.blesdk.obfuscated.C2545od.C2546a<K, B> aVar) {
        this.f11224a.loadBefore(fVar, new com.fossil.blesdk.obfuscated.C3355yd.C3357b(aVar));
    }

    @DexIgnore
    public void loadInitial(com.fossil.blesdk.obfuscated.C2545od.C2550e<K> eVar, com.fossil.blesdk.obfuscated.C2545od.C2548c<K, B> cVar) {
        this.f11224a.loadInitial(eVar, new com.fossil.blesdk.obfuscated.C3355yd.C3356a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(com.fossil.blesdk.obfuscated.C2307ld.C2311c cVar) {
        this.f11224a.removeInvalidatedCallback(cVar);
    }
}
