package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.tk */
public abstract class C2971tk<T> extends com.fossil.blesdk.obfuscated.C3044uk<T> {

    @DexIgnore
    /* renamed from: h */
    public static /* final */ java.lang.String f9697h; // = com.fossil.blesdk.obfuscated.C1635dj.m5871a("BrdcstRcvrCnstrntTrckr");

    @DexIgnore
    /* renamed from: g */
    public /* final */ android.content.BroadcastReceiver f9698g; // = new com.fossil.blesdk.obfuscated.C2971tk.C2972a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.tk$a")
    /* renamed from: com.fossil.blesdk.obfuscated.tk$a */
    public class C2972a extends android.content.BroadcastReceiver {
        @DexIgnore
        public C2972a() {
        }

        @DexIgnore
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            if (intent != null) {
                com.fossil.blesdk.obfuscated.C2971tk.this.mo15614a(context, intent);
            }
        }
    }

    @DexIgnore
    public C2971tk(android.content.Context context, com.fossil.blesdk.obfuscated.C3444zl zlVar) {
        super(context, zlVar);
    }

    @DexIgnore
    /* renamed from: a */
    public abstract void mo15614a(android.content.Context context, android.content.Intent intent);

    @DexIgnore
    /* renamed from: b */
    public void mo16475b() {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9697h, java.lang.String.format("%s: registering receiver", new java.lang.Object[]{getClass().getSimpleName()}), new java.lang.Throwable[0]);
        this.f10017b.registerReceiver(this.f9698g, mo15616d());
    }

    @DexIgnore
    /* renamed from: c */
    public void mo16476c() {
        com.fossil.blesdk.obfuscated.C1635dj.m5870a().mo9962a(f9697h, java.lang.String.format("%s: unregistering receiver", new java.lang.Object[]{getClass().getSimpleName()}), new java.lang.Throwable[0]);
        this.f10017b.unregisterReceiver(this.f9698g);
    }

    @DexIgnore
    /* renamed from: d */
    public abstract android.content.IntentFilter mo15616d();
}
