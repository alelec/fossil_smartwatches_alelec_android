package com.fossil.blesdk.obfuscated;

import java.util.Map;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class pb4 {
    @DexIgnore
    public static final <K, V> V a(Map<K, ? extends V> map, K k) {
        kd4.b(map, "$this$getOrImplicitDefault");
        if (map instanceof ob4) {
            return ((ob4) map).a(k);
        }
        V v = map.get(k);
        if (v != null || map.containsKey(k)) {
            return v;
        }
        throw new NoSuchElementException("Key " + k + " is missing in the map.");
    }
}
