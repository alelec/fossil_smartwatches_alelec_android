package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ah */
public class C1417ah {
    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3436zg m4440a(android.view.View view, android.view.ViewGroup viewGroup, android.graphics.Matrix matrix) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            return com.fossil.blesdk.obfuscated.C3362yg.m16839a(view, viewGroup, matrix);
        }
        return com.fossil.blesdk.obfuscated.C3287xg.m16288a(view, viewGroup);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m4441a(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            com.fossil.blesdk.obfuscated.C3362yg.m16841a(view);
        } else {
            com.fossil.blesdk.obfuscated.C3287xg.m16290b(view);
        }
    }
}
