package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.e3 */
public class C1701e3 extends android.content.res.Resources {

    @DexIgnore
    /* renamed from: b */
    public static boolean f4682b; // = false;

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.lang.ref.WeakReference<android.content.Context> f4683a;

    @DexIgnore
    public C1701e3(android.content.Context context, android.content.res.Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f4683a = new java.lang.ref.WeakReference<>(context);
    }

    @DexIgnore
    /* renamed from: b */
    public static boolean m6278b() {
        return m6277a() && android.os.Build.VERSION.SDK_INT <= 20;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.graphics.drawable.Drawable mo10302a(int i) {
        return super.getDrawable(i);
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getDrawable(int i) throws android.content.res.Resources.NotFoundException {
        android.content.Context context = (android.content.Context) this.f4683a.get();
        if (context != null) {
            return com.fossil.blesdk.obfuscated.C1526c2.m5217a().mo9388a(context, this, i);
        }
        return super.getDrawable(i);
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m6277a() {
        return f4682b;
    }
}
