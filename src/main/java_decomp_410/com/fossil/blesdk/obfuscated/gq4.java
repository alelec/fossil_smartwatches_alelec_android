package com.fossil.blesdk.obfuscated;

import org.slf4j.Marker;
import org.slf4j.event.Level;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gq4 implements fq4 {
    @DexIgnore
    public iq4 a;

    @DexIgnore
    public iq4 a() {
        return this.a;
    }

    @DexIgnore
    public void a(long j) {
    }

    @DexIgnore
    public void a(String str) {
    }

    @DexIgnore
    public void a(Throwable th) {
    }

    @DexIgnore
    public void a(Marker marker) {
    }

    @DexIgnore
    public void a(Level level) {
    }

    @DexIgnore
    public void a(Object[] objArr) {
    }

    @DexIgnore
    public void b(String str) {
    }

    @DexIgnore
    public void c(String str) {
    }

    @DexIgnore
    public void a(iq4 iq4) {
        this.a = iq4;
    }
}
