package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class b23 implements Factory<DianaCustomizeViewModel> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<ComplicationLastSettingRepository> c;
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> d;
    @DexIgnore
    public /* final */ Provider<WatchAppLastSettingRepository> e;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> f;

    @DexIgnore
    public b23(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<WatchAppLastSettingRepository> provider5, Provider<WatchFaceRepository> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static b23 a(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<WatchAppLastSettingRepository> provider5, Provider<WatchFaceRepository> provider6) {
        return new b23(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static DianaCustomizeViewModel b(Provider<DianaPresetRepository> provider, Provider<ComplicationRepository> provider2, Provider<ComplicationLastSettingRepository> provider3, Provider<WatchAppRepository> provider4, Provider<WatchAppLastSettingRepository> provider5, Provider<WatchFaceRepository> provider6) {
        return new DianaCustomizeViewModel(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public DianaCustomizeViewModel get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
