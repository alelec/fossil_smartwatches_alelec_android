package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class o12 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof o12) {
            o12 o12 = (o12) obj;
            if (this.a == o12.a && this.b == o12.b) {
                return true;
            }
            return false;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 32713) + this.b;
    }

    @DexIgnore
    public String toString() {
        return this.a + "x" + this.b;
    }
}
