package com.fossil.blesdk.obfuscated;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ConnectTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ym extends zm {
    @DexIgnore
    public /* final */ gn a;

    @DexIgnore
    public ym(gn gnVar) {
        this.a = gnVar;
    }

    @DexIgnore
    public fn b(Request<?> request, Map<String, String> map) throws IOException, AuthFailureError {
        try {
            HttpResponse a2 = this.a.a(request, map);
            int statusCode = a2.getStatusLine().getStatusCode();
            Header[] allHeaders = a2.getAllHeaders();
            ArrayList arrayList = new ArrayList(allHeaders.length);
            for (Header header : allHeaders) {
                arrayList.add(new pm(header.getName(), header.getValue()));
            }
            if (a2.getEntity() == null) {
                return new fn(statusCode, arrayList);
            }
            long contentLength = a2.getEntity().getContentLength();
            if (((long) ((int) contentLength)) == contentLength) {
                return new fn(statusCode, arrayList, (int) a2.getEntity().getContentLength(), a2.getEntity().getContent());
            }
            throw new IOException("Response too large: " + contentLength);
        } catch (ConnectTimeoutException e) {
            throw new SocketTimeoutException(e.getMessage());
        }
    }
}
