package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.text.StringsKt__StringsKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qt2 extends RecyclerView.g<b> implements Filterable {
    @DexIgnore
    public List<AppWrapper> e; // = new ArrayList();
    @DexIgnore
    public int f;
    @DexIgnore
    public List<AppWrapper> g;
    @DexIgnore
    public a h;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ eh2 a;
        @DexIgnore
        public /* final */ /* synthetic */ qt2 b;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b e;

            @DexIgnore
            public a(b bVar) {
                this.e = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.e.getAdapterPosition();
                if (adapterPosition != -1) {
                    List c = this.e.b.g;
                    if (c != null) {
                        InstalledApp installedApp = ((AppWrapper) c.get(adapterPosition)).getInstalledApp();
                        Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                        if (isSelected != null) {
                            boolean booleanValue = isSelected.booleanValue();
                            if (booleanValue) {
                                List c2 = this.e.b.g;
                                if (c2 == null) {
                                    kd4.a();
                                    throw null;
                                } else if (((AppWrapper) c2.get(adapterPosition)).getCurrentHandGroup() != this.e.b.f) {
                                    a d = this.e.b.h;
                                    if (d != null) {
                                        List c3 = this.e.b.g;
                                        if (c3 != null) {
                                            d.a((AppWrapper) c3.get(adapterPosition), booleanValue);
                                            return;
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                            a d2 = this.e.b.h;
                            if (d2 != null) {
                                List c4 = this.e.b.g;
                                if (c4 != null) {
                                    d2.a((AppWrapper) c4.get(adapterPosition), !booleanValue);
                                } else {
                                    kd4.a();
                                    throw null;
                                }
                            }
                        } else {
                            kd4.a();
                            throw null;
                        }
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qt2 qt2, eh2 eh2) {
            super(eh2.d());
            kd4.b(eh2, "binding");
            this.b = qt2;
            this.a = eh2;
            this.a.u.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(AppWrapper appWrapper) {
            kd4.b(appWrapper, "appWrapper");
            ImageView imageView = this.a.t;
            kd4.a((Object) imageView, "binding.ivAppIcon");
            ck2.a(imageView.getContext()).a(appWrapper.getUri()).a((lv<?>) ((rv) ((rv) new rv().c()).a(pp.a)).a(true)).c().a(this.a.t);
            FlexibleTextView flexibleTextView = this.a.r;
            kd4.a((Object) flexibleTextView, "binding.ftvAppName");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            flexibleTextView.setText(installedApp != null ? installedApp.getTitle() : null);
            SwitchCompat switchCompat = this.a.u;
            kd4.a((Object) switchCompat, "binding.swEnabled");
            InstalledApp installedApp2 = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected != null) {
                switchCompat.setChecked(isSelected.booleanValue());
                if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == this.b.f) {
                    FlexibleTextView flexibleTextView2 = this.a.s;
                    kd4.a((Object) flexibleTextView2, "binding.ftvAssigned");
                    flexibleTextView2.setText("");
                    FlexibleTextView flexibleTextView3 = this.a.s;
                    kd4.a((Object) flexibleTextView3, "binding.ftvAssigned");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = this.a.s;
                kd4.a((Object) flexibleTextView4, "binding.ftvAssigned");
                pd4 pd4 = pd4.a;
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_InMyNotifications_Text__AssignedToNumber);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                Object[] objArr = {Integer.valueOf(appWrapper.getCurrentHandGroup())};
                String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                kd4.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView4.setText(format);
                FlexibleTextView flexibleTextView5 = this.a.s;
                kd4.a((Object) flexibleTextView5, "binding.ftvAssigned");
                flexibleTextView5.setVisibility(0);
                SwitchCompat switchCompat2 = this.a.u;
                kd4.a((Object) switchCompat2, "binding.swEnabled");
                switchCompat2.setChecked(false);
                return;
            }
            kd4.a();
            throw null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ qt2 a;

        @DexIgnore
        public c(qt2 qt2) {
            this.a = qt2;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            kd4.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.e;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    kd4.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.e) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp != null) {
                            String title = installedApp.getTitle();
                            if (title != null) {
                                if (title != null) {
                                    str = title.toLowerCase();
                                    kd4.a((Object) str, "(this as java.lang.String).toLowerCase()");
                                    if (str != null && StringsKt__StringsKt.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                                        arrayList.add(appWrapper);
                                    }
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            }
                        }
                        str = null;
                        arrayList.add(appWrapper);
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            kd4.b(charSequence, "charSequence");
            kd4.b(filterResults, "results");
            this.a.g = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    public int getItemCount() {
        List<AppWrapper> list = this.g;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        kd4.b(viewGroup, "parent");
        eh2 a2 = eh2.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        kd4.a((Object) a2, "ItemAppHybridNotificatio\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        kd4.b(bVar, "holder");
        List<AppWrapper> list = this.g;
        if (list != null) {
            bVar.a(list.get(i));
        } else {
            kd4.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<AppWrapper> list, int i) {
        kd4.b(list, "listAppWrapper");
        this.f = i;
        this.e = list;
        this.g = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(a aVar) {
        kd4.b(aVar, "listener");
        this.h = aVar;
    }
}
