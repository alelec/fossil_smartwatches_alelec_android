package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ro extends OutputStream {
    @DexIgnore
    public /* final */ OutputStream e;
    @DexIgnore
    public byte[] f;
    @DexIgnore
    public gq g;
    @DexIgnore
    public int h;

    @DexIgnore
    public ro(OutputStream outputStream, gq gqVar) {
        this(outputStream, gqVar, 65536);
    }

    @DexIgnore
    public final void A() {
        byte[] bArr = this.f;
        if (bArr != null) {
            this.g.put(bArr);
            this.f = null;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void close() throws IOException {
        try {
            flush();
            this.e.close();
            A();
        } catch (Throwable th) {
            this.e.close();
            throw th;
        }
    }

    @DexIgnore
    public void flush() throws IOException {
        y();
        this.e.flush();
    }

    @DexIgnore
    public void write(int i) throws IOException {
        byte[] bArr = this.f;
        int i2 = this.h;
        this.h = i2 + 1;
        bArr[i2] = (byte) i;
        z();
    }

    @DexIgnore
    public final void y() throws IOException {
        int i = this.h;
        if (i > 0) {
            this.e.write(this.f, 0, i);
            this.h = 0;
        }
    }

    @DexIgnore
    public final void z() throws IOException {
        if (this.h == this.f.length) {
            y();
        }
    }

    @DexIgnore
    public ro(OutputStream outputStream, gq gqVar, int i) {
        this.e = outputStream;
        this.g = gqVar;
        this.f = (byte[]) gqVar.b(i, byte[].class);
    }

    @DexIgnore
    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    @DexIgnore
    public void write(byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.h != 0 || i4 < this.f.length) {
                int min = Math.min(i4, this.f.length - this.h);
                System.arraycopy(bArr, i5, this.f, this.h, min);
                this.h += min;
                i3 += min;
                z();
            } else {
                this.e.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
