package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class s22 extends i22 {
    @DexIgnore
    public int a() {
        return 3;
    }

    @DexIgnore
    public void a(n22 n22) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!n22.i()) {
                break;
            }
            char c = n22.c();
            n22.f++;
            a(c, sb);
            if (sb.length() % 3 == 0) {
                i22.b(n22, sb);
                int a = p22.a(n22.d(), n22.f, a());
                if (a != a()) {
                    n22.b(a);
                    break;
                }
            }
        }
        a(n22, sb);
    }

    @DexIgnore
    public int a(char c, StringBuilder sb) {
        if (c == 13) {
            sb.append(0);
        } else if (c == '*') {
            sb.append(1);
        } else if (c == '>') {
            sb.append(2);
        } else if (c == ' ') {
            sb.append(3);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            p22.a(c);
            throw null;
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    @DexIgnore
    public void a(n22 n22, StringBuilder sb) {
        n22.l();
        int a = n22.g().a() - n22.a();
        n22.f -= sb.length();
        if (n22.f() > 1 || a > 1 || n22.f() != a) {
            n22.a(254);
        }
        if (n22.e() < 0) {
            n22.b(0);
        }
    }
}
