package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i7 */
public interface C2001i7 extends android.view.MenuItem {
    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C2001i7 mo8357a(com.fossil.blesdk.obfuscated.C2382m8 m8Var);

    @DexIgnore
    /* renamed from: a */
    com.fossil.blesdk.obfuscated.C2382m8 mo8358a();

    @DexIgnore
    boolean collapseActionView();

    @DexIgnore
    boolean expandActionView();

    @DexIgnore
    android.view.View getActionView();

    @DexIgnore
    int getAlphabeticModifiers();

    @DexIgnore
    java.lang.CharSequence getContentDescription();

    @DexIgnore
    android.content.res.ColorStateList getIconTintList();

    @DexIgnore
    android.graphics.PorterDuff.Mode getIconTintMode();

    @DexIgnore
    int getNumericModifiers();

    @DexIgnore
    java.lang.CharSequence getTooltipText();

    @DexIgnore
    boolean isActionViewExpanded();

    @DexIgnore
    android.view.MenuItem setActionView(int i);

    @DexIgnore
    android.view.MenuItem setActionView(android.view.View view);

    @DexIgnore
    android.view.MenuItem setAlphabeticShortcut(char c, int i);

    @DexIgnore
    com.fossil.blesdk.obfuscated.C2001i7 setContentDescription(java.lang.CharSequence charSequence);

    @DexIgnore
    android.view.MenuItem setIconTintList(android.content.res.ColorStateList colorStateList);

    @DexIgnore
    android.view.MenuItem setIconTintMode(android.graphics.PorterDuff.Mode mode);

    @DexIgnore
    android.view.MenuItem setNumericShortcut(char c, int i);

    @DexIgnore
    android.view.MenuItem setShortcut(char c, char c2, int i, int i2);

    @DexIgnore
    void setShowAsAction(int i);

    @DexIgnore
    android.view.MenuItem setShowAsActionFlags(int i);

    @DexIgnore
    com.fossil.blesdk.obfuscated.C2001i7 setTooltipText(java.lang.CharSequence charSequence);
}
