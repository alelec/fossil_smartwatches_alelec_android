package com.fossil.blesdk.obfuscated;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ya4 extends xa4 {
    @DexIgnore
    public static final <T> List<T> a(T[] tArr) {
        kd4.b(tArr, "$this$asList");
        List<T> a = ab4.a(tArr);
        kd4.a((Object) a, "ArraysUtilJVM.asList(this)");
        return a;
    }

    @DexIgnore
    public static final <T> void b(T[] tArr) {
        kd4.b(tArr, "$this$sort");
        if (tArr.length > 1) {
            Arrays.sort(tArr);
        }
    }

    @DexIgnore
    public static /* synthetic */ Object[] a(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = objArr.length;
        }
        a(objArr, objArr2, i, i2, i3);
        return objArr2;
    }

    @DexIgnore
    public static final <T> T[] a(T[] tArr, T[] tArr2, int i, int i2, int i3) {
        kd4.b(tArr, "$this$copyInto");
        kd4.b(tArr2, ShareConstants.DESTINATION);
        System.arraycopy(tArr, i2, tArr2, i, i3 - i2);
        return tArr2;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, int i, int i2) {
        kd4.b(bArr, "$this$copyOfRangeImpl");
        wa4.a(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        kd4.a((Object) copyOfRange, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return copyOfRange;
    }

    @DexIgnore
    public static final <T> void a(T[] tArr, T t, int i, int i2) {
        kd4.b(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    @DexIgnore
    public static final <T> T[] a(T[] tArr, T t) {
        kd4.b(tArr, "$this$plus");
        int length = tArr.length;
        T[] copyOf = Arrays.copyOf(tArr, length + 1);
        copyOf[length] = t;
        kd4.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte b) {
        kd4.b(bArr, "$this$plus");
        int length = bArr.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + 1);
        copyOf[length] = b;
        kd4.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final byte[] a(byte[] bArr, byte[] bArr2) {
        kd4.b(bArr, "$this$plus");
        kd4.b(bArr2, MessengerShareContentUtility.ELEMENTS);
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + length2);
        System.arraycopy(bArr2, 0, copyOf, length, length2);
        kd4.a((Object) copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final void a(int[] iArr) {
        kd4.b(iArr, "$this$sort");
        if (iArr.length > 1) {
            Arrays.sort(iArr);
        }
    }

    @DexIgnore
    public static final <T> void a(T[] tArr, Comparator<? super T> comparator) {
        kd4.b(tArr, "$this$sortWith");
        kd4.b(comparator, "comparator");
        if (tArr.length > 1) {
            Arrays.sort(tArr, comparator);
        }
    }
}
