package com.fossil.blesdk.obfuscated;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlin.coroutines.CoroutineContext;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlinx.coroutines.CoroutineExceptionHandler;
import kotlinx.coroutines.android.AndroidExceptionPreHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vg4 {
    @DexIgnore
    public static /* final */ List<CoroutineExceptionHandler> a;

    /*
    static {
        Iterator it = Arrays.asList(new CoroutineExceptionHandler[]{new AndroidExceptionPreHandler()}).iterator();
        kd4.a((Object) it, "ServiceLoader.load(\n    \u2026.classLoader\n).iterator()");
        a = SequencesKt___SequencesKt.f(we4.a(it));
    }
    */

    @DexIgnore
    public static final void a(CoroutineContext coroutineContext, Throwable th) {
        kd4.b(coroutineContext, "context");
        kd4.b(th, "exception");
        for (CoroutineExceptionHandler handleException : a) {
            try {
                handleException.handleException(coroutineContext, th);
            } catch (Throwable th2) {
                Thread currentThread = Thread.currentThread();
                kd4.a((Object) currentThread, "currentThread");
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, wg4.a(th, th2));
            }
        }
        Thread currentThread2 = Thread.currentThread();
        kd4.a((Object) currentThread2, "currentThread");
        currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, th);
    }
}
