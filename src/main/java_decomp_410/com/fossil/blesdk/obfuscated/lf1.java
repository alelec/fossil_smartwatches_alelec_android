package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.obfuscated.zj0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lf1 extends jk0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<lf1> CREATOR; // = new sf1();
    @DexIgnore
    public /* final */ float e;
    @DexIgnore
    public /* final */ float f;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;

        @DexIgnore
        public final a a(float f) {
            this.a = f;
            return this;
        }

        @DexIgnore
        public final a b(float f) {
            this.b = f;
            return this;
        }

        @DexIgnore
        public final lf1 a() {
            return new lf1(this.b, this.a);
        }
    }

    @DexIgnore
    public lf1(float f2, float f3) {
        boolean z = -90.0f <= f2 && f2 <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f2);
        bk0.a(z, (Object) sb.toString());
        this.e = f2 + LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.f = (((double) f3) <= 0.0d ? (f3 % 360.0f) + 360.0f : f3) % 360.0f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof lf1)) {
            return false;
        }
        lf1 lf1 = (lf1) obj;
        return Float.floatToIntBits(this.e) == Float.floatToIntBits(lf1.e) && Float.floatToIntBits(this.f) == Float.floatToIntBits(lf1.f);
    }

    @DexIgnore
    public int hashCode() {
        return zj0.a(Float.valueOf(this.e), Float.valueOf(this.f));
    }

    @DexIgnore
    public String toString() {
        zj0.a a2 = zj0.a((Object) this);
        a2.a("tilt", Float.valueOf(this.e));
        a2.a("bearing", Float.valueOf(this.f));
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = kk0.a(parcel);
        kk0.a(parcel, 2, this.e);
        kk0.a(parcel, 3, this.f);
        kk0.a(parcel, a2);
    }
}
