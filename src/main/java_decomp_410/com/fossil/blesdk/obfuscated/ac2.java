package com.fossil.blesdk.obfuscated;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ac2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ OverviewDayGoalChart r;
    @DexIgnore
    public /* final */ FlexibleTextView s;

    @DexIgnore
    public ac2(Object obj, View view, int i, ConstraintLayout constraintLayout, OverviewDayGoalChart overviewDayGoalChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = overviewDayGoalChart;
        this.s = flexibleTextView;
    }
}
