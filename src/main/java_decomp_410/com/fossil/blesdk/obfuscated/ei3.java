package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ei3 implements MembersInjector<HelpActivity> {
    @DexIgnore
    public static void a(HelpActivity helpActivity, HelpPresenter helpPresenter) {
        helpActivity.B = helpPresenter;
    }
}
