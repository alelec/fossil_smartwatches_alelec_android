package com.fossil.blesdk.obfuscated;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.blesdk.obfuscated.de0;
import com.fossil.blesdk.obfuscated.kj0;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sf0 implements mg0 {
    @DexIgnore
    public /* final */ ng0 a;
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ yd0 d;
    @DexIgnore
    public ud0 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Bundle i; // = new Bundle();
    @DexIgnore
    public /* final */ Set<de0.c> j; // = new HashSet();
    @DexIgnore
    public ln1 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public tj0 o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ kj0 r;
    @DexIgnore
    public /* final */ Map<de0<?>, Boolean> s;
    @DexIgnore
    public /* final */ de0.a<? extends ln1, vm1> t;
    @DexIgnore
    public ArrayList<Future<?>> u; // = new ArrayList<>();

    @DexIgnore
    public sf0(ng0 ng0, kj0 kj0, Map<de0<?>, Boolean> map, yd0 yd0, de0.a<? extends ln1, vm1> aVar, Lock lock, Context context) {
        this.a = ng0;
        this.r = kj0;
        this.s = map;
        this.d = yd0;
        this.t = aVar;
        this.b = lock;
        this.c = context;
    }

    @DexIgnore
    public static String b(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    @DexIgnore
    public final void a(gn1 gn1) {
        if (a(0)) {
            ud0 H = gn1.H();
            if (H.L()) {
                dk0 I = gn1.I();
                ud0 I2 = I.I();
                if (!I2.L()) {
                    String valueOf = String.valueOf(I2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    b(I2);
                    return;
                }
                this.n = true;
                this.o = I.H();
                this.p = I.J();
                this.q = I.K();
                e();
            } else if (a(H)) {
                g();
                e();
            } else {
                b(H);
            }
        }
    }

    @DexIgnore
    public final <A extends de0.b, R extends me0, T extends te0<R, A>> T b(T t2) {
        this.a.r.i.add(t2);
        return t2;
    }

    @DexIgnore
    public final void b() {
    }

    @DexIgnore
    public final void c() {
        this.a.k.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (de0 next : this.s.keySet()) {
            de0.f fVar = this.a.j.get(next.a());
            z |= next.c().a() == 1;
            boolean booleanValue = this.s.get(next).booleanValue();
            if (fVar.l()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(next.a());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(fVar, new uf0(this, next, booleanValue));
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            this.r.a(Integer.valueOf(System.identityHashCode(this.a.r)));
            bg0 bg0 = new bg0(this, (tf0) null);
            de0.a<? extends ln1, vm1> aVar = this.t;
            Context context = this.c;
            Looper f2 = this.a.r.f();
            kj0 kj0 = this.r;
            this.k = (ln1) aVar.a(context, f2, kj0, kj0.j(), bg0, bg0);
        }
        this.h = this.a.j.size();
        this.u.add(qg0.a().submit(new vf0(this, hashMap)));
    }

    @DexIgnore
    public final boolean d() {
        this.h--;
        int i2 = this.h;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            Log.w("GoogleApiClientConnecting", this.a.r.q());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            b(new ud0(8, (PendingIntent) null));
            return false;
        }
        ud0 ud0 = this.e;
        if (ud0 == null) {
            return true;
        }
        this.a.q = this.f;
        b(ud0);
        return false;
    }

    @DexIgnore
    public final void e() {
        if (this.h == 0) {
            if (!this.m || this.n) {
                ArrayList arrayList = new ArrayList();
                this.g = 1;
                this.h = this.a.j.size();
                for (de0.c next : this.a.j.keySet()) {
                    if (!this.a.k.containsKey(next)) {
                        arrayList.add(this.a.j.get(next));
                    } else if (d()) {
                        f();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(qg0.a().submit(new yf0(this, arrayList)));
                }
            }
        }
    }

    @DexIgnore
    public final void f() {
        this.a.i();
        qg0.a().execute(new tf0(this));
        ln1 ln1 = this.k;
        if (ln1 != null) {
            if (this.p) {
                ln1.a(this.o, this.q);
            }
            a(false);
        }
        for (de0.c<?> cVar : this.a.k.keySet()) {
            this.a.j.get(cVar).a();
        }
        this.a.s.a(this.i.isEmpty() ? null : this.i);
    }

    @DexIgnore
    public final void g() {
        this.m = false;
        this.a.r.q = Collections.emptySet();
        for (de0.c next : this.j) {
            if (!this.a.k.containsKey(next)) {
                this.a.k.put(next, new ud0(17, (PendingIntent) null));
            }
        }
    }

    @DexIgnore
    public final void h() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Future<?> future = arrayList.get(i2);
            i2++;
            future.cancel(true);
        }
        this.u.clear();
    }

    @DexIgnore
    public final Set<Scope> i() {
        kj0 kj0 = this.r;
        if (kj0 == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(kj0.i());
        Map<de0<?>, kj0.b> f2 = this.r.f();
        for (de0 next : f2.keySet()) {
            if (!this.a.k.containsKey(next.a())) {
                hashSet.addAll(f2.get(next).a);
            }
        }
        return hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r7 != false) goto L_0x0024;
     */
    @DexIgnore
    public final void b(ud0 ud0, de0<?> de0, boolean z) {
        boolean z2;
        int a2 = de0.c().a();
        boolean z3 = false;
        if (z) {
            if (!ud0.K() && this.d.a(ud0.H()) == null) {
                z2 = false;
            } else {
                z2 = true;
            }
        }
        if (this.e == null || a2 < this.f) {
            z3 = true;
        }
        if (z3) {
            this.e = ud0;
            this.f = a2;
        }
        this.a.k.put(de0.a(), ud0);
    }

    @DexIgnore
    public final void b(ud0 ud0) {
        h();
        a(!ud0.K());
        this.a.a(ud0);
        this.a.s.a(ud0);
    }

    @DexIgnore
    public final void e(Bundle bundle) {
        if (a(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    public final void f(int i2) {
        b(new ud0(8, (PendingIntent) null));
    }

    @DexIgnore
    public final void a(ud0 ud0, de0<?> de0, boolean z) {
        if (a(1)) {
            b(ud0, de0, z);
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    public final <A extends de0.b, T extends te0<? extends me0, A>> T a(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    public final boolean a() {
        h();
        a(true);
        this.a.a((ud0) null);
        return true;
    }

    @DexIgnore
    public final boolean a(ud0 ud0) {
        return this.l && !ud0.K();
    }

    @DexIgnore
    public final void a(boolean z) {
        ln1 ln1 = this.k;
        if (ln1 != null) {
            if (ln1.c() && z) {
                this.k.g();
            }
            this.k.a();
            if (this.r.k()) {
                this.k = null;
            }
            this.o = null;
        }
    }

    @DexIgnore
    public final boolean a(int i2) {
        if (this.g == i2) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.a.r.q());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GoogleApiClientConnecting", sb.toString());
        int i3 = this.h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        Log.w("GoogleApiClientConnecting", sb2.toString());
        String b2 = b(this.g);
        String b3 = b(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(b2).length() + 70 + String.valueOf(b3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(b2);
        sb3.append(" but received callback for step ");
        sb3.append(b3);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        b(new ud0(8, (PendingIntent) null));
        return false;
    }
}
