package com.fossil.blesdk.obfuscated;

import android.util.SparseArray;
import androidx.renderscript.Allocation;
import androidx.renderscript.RSIllegalArgumentException;
import androidx.renderscript.RenderScript;
import androidx.renderscript.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gf extends ef {
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore
    public gf(long j, RenderScript renderScript) {
        super(j, renderScript);
        new SparseArray();
        new SparseArray();
        new SparseArray();
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public long a(Allocation allocation) {
        if (allocation == null) {
            return 0;
        }
        Type e = allocation.e();
        long b = e.g().b(this.c);
        RenderScript renderScript = this.c;
        long a = renderScript.a(allocation.a(renderScript), e.a(this.c, b), e.h() * e.g().e());
        allocation.a(a);
        return a;
    }

    @DexIgnore
    public void a(int i, Allocation allocation, Allocation allocation2, ff ffVar) {
        Allocation allocation3 = allocation;
        Allocation allocation4 = allocation2;
        if (allocation3 == null && allocation4 == null) {
            throw new RSIllegalArgumentException("At least one of ain or aout is required to be non-null.");
        }
        long j = 0;
        long a = allocation3 != null ? allocation3.a(this.c) : 0;
        if (allocation4 != null) {
            j = allocation4.a(this.c);
        }
        long j2 = j;
        if (ffVar != null) {
            ffVar.a();
            throw null;
        } else if (this.d) {
            long a2 = a(allocation3);
            long a3 = a(allocation4);
            RenderScript renderScript = this.c;
            renderScript.a(a(renderScript), i, a2, a3, (byte[]) null, this.d);
        } else {
            RenderScript renderScript2 = this.c;
            renderScript2.a(a(renderScript2), i, a, j2, (byte[]) null, this.d);
        }
    }

    @DexIgnore
    public void a(int i, float f) {
        RenderScript renderScript = this.c;
        renderScript.a(a(renderScript), i, f, this.d);
    }

    @DexIgnore
    public void a(int i, ef efVar) {
        ef efVar2 = efVar;
        long j = 0;
        if (this.d) {
            long a = a((Allocation) efVar2);
            RenderScript renderScript = this.c;
            renderScript.a(a(renderScript), i, efVar2 == null ? 0 : a, this.d);
            return;
        }
        RenderScript renderScript2 = this.c;
        long a2 = a(renderScript2);
        if (efVar2 != null) {
            j = efVar2.a(this.c);
        }
        renderScript2.a(a2, i, j, this.d);
    }
}
