package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.i8 */
public class C2002i8<T> extends com.fossil.blesdk.obfuscated.C1928h8<T> {

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.lang.Object f5964c; // = new java.lang.Object();

    @DexIgnore
    public C2002i8(int i) {
        super(i);
    }

    @DexIgnore
    /* renamed from: a */
    public T mo11162a() {
        T a;
        synchronized (this.f5964c) {
            a = super.mo11162a();
        }
        return a;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11163a(T t) {
        boolean a;
        synchronized (this.f5964c) {
            a = super.mo11163a(t);
        }
        return a;
    }
}
