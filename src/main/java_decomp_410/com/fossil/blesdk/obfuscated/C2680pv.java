package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pv */
public class C2680pv<R> implements com.fossil.blesdk.obfuscated.C2506nv<R>, com.fossil.blesdk.obfuscated.C2771qv<R> {

    @DexIgnore
    /* renamed from: o */
    public static /* final */ com.fossil.blesdk.obfuscated.C2680pv.C2681a f8457o; // = new com.fossil.blesdk.obfuscated.C2680pv.C2681a();

    @DexIgnore
    /* renamed from: e */
    public /* final */ int f8458e;

    @DexIgnore
    /* renamed from: f */
    public /* final */ int f8459f;

    @DexIgnore
    /* renamed from: g */
    public /* final */ boolean f8460g;

    @DexIgnore
    /* renamed from: h */
    public /* final */ com.fossil.blesdk.obfuscated.C2680pv.C2681a f8461h;

    @DexIgnore
    /* renamed from: i */
    public R f8462i;

    @DexIgnore
    /* renamed from: j */
    public com.fossil.blesdk.obfuscated.C2604ov f8463j;

    @DexIgnore
    /* renamed from: k */
    public boolean f8464k;

    @DexIgnore
    /* renamed from: l */
    public boolean f8465l;

    @DexIgnore
    /* renamed from: m */
    public boolean f8466m;

    @DexIgnore
    /* renamed from: n */
    public com.bumptech.glide.load.engine.GlideException f8467n;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.pv$a")
    /* renamed from: com.fossil.blesdk.obfuscated.pv$a */
    public static class C2681a {
        @DexIgnore
        /* renamed from: a */
        public void mo14892a(java.lang.Object obj, long j) throws java.lang.InterruptedException {
            obj.wait(j);
        }

        @DexIgnore
        /* renamed from: a */
        public void mo14891a(java.lang.Object obj) {
            obj.notifyAll();
        }
    }

    @DexIgnore
    public C2680pv(int i, int i2) {
        this(i, i2, true, f8457o);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14094a() {
    }

    @DexIgnore
    /* renamed from: a */
    public void mo9313a(com.fossil.blesdk.obfuscated.C1449aw awVar) {
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo9314a(com.fossil.blesdk.obfuscated.C2604ov ovVar) {
        this.f8463j = ovVar;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14096b() {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9316b(android.graphics.drawable.Drawable drawable) {
    }

    @DexIgnore
    /* renamed from: b */
    public void mo9317b(com.fossil.blesdk.obfuscated.C1449aw awVar) {
        awVar.mo4027a(this.f8458e, this.f8459f);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14097c() {
    }

    @DexIgnore
    /* renamed from: c */
    public void mo9318c(android.graphics.drawable.Drawable drawable) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r3 == null) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
        r3.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        return true;
     */
    @DexIgnore
    public boolean cancel(boolean z) {
        com.fossil.blesdk.obfuscated.C2604ov ovVar;
        synchronized (this) {
            if (isDone()) {
                return false;
            }
            this.f8464k = true;
            this.f8461h.mo14891a(this);
            if (z) {
                ovVar = this.f8463j;
                this.f8463j = null;
            } else {
                ovVar = null;
            }
        }
    }

    @DexIgnore
    /* renamed from: d */
    public synchronized com.fossil.blesdk.obfuscated.C2604ov mo9319d() {
        return this.f8463j;
    }

    @DexIgnore
    public R get() throws java.lang.InterruptedException, java.util.concurrent.ExecutionException {
        try {
            return mo14883a((java.lang.Long) null);
        } catch (java.util.concurrent.TimeoutException e) {
            throw new java.lang.AssertionError(e);
        }
    }

    @DexIgnore
    public synchronized boolean isCancelled() {
        return this.f8464k;
    }

    @DexIgnore
    public synchronized boolean isDone() {
        return this.f8464k || this.f8465l || this.f8466m;
    }

    @DexIgnore
    public C2680pv(int i, int i2, boolean z, com.fossil.blesdk.obfuscated.C2680pv.C2681a aVar) {
        this.f8458e = i;
        this.f8459f = i2;
        this.f8460g = z;
        this.f8461h = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo9312a(android.graphics.drawable.Drawable drawable) {
    }

    @DexIgnore
    public R get(long j, java.util.concurrent.TimeUnit timeUnit) throws java.lang.InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        return mo14883a(java.lang.Long.valueOf(timeUnit.toMillis(j)));
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized void mo9315a(R r, com.fossil.blesdk.obfuscated.C1750ew<? super R> ewVar) {
    }

    @DexIgnore
    /* renamed from: a */
    public final synchronized R mo14883a(java.lang.Long l) throws java.util.concurrent.ExecutionException, java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        if (this.f8460g && !isDone()) {
            com.fossil.blesdk.obfuscated.C3066uw.m14929a();
        }
        if (this.f8464k) {
            throw new java.util.concurrent.CancellationException();
        } else if (this.f8466m) {
            throw new java.util.concurrent.ExecutionException(this.f8467n);
        } else if (this.f8465l) {
            return this.f8462i;
        } else {
            if (l == null) {
                this.f8461h.mo14892a(this, 0);
            } else if (l.longValue() > 0) {
                long currentTimeMillis = java.lang.System.currentTimeMillis();
                long longValue = l.longValue() + currentTimeMillis;
                while (!isDone() && currentTimeMillis < longValue) {
                    this.f8461h.mo14892a(this, longValue - currentTimeMillis);
                    currentTimeMillis = java.lang.System.currentTimeMillis();
                }
            }
            if (java.lang.Thread.interrupted()) {
                throw new java.lang.InterruptedException();
            } else if (this.f8466m) {
                throw new java.util.concurrent.ExecutionException(this.f8467n);
            } else if (this.f8464k) {
                throw new java.util.concurrent.CancellationException();
            } else if (this.f8465l) {
                return this.f8462i;
            } else {
                throw new java.util.concurrent.TimeoutException();
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized boolean mo14884a(com.bumptech.glide.load.engine.GlideException glideException, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<R> bwVar, boolean z) {
        this.f8466m = true;
        this.f8467n = glideException;
        this.f8461h.mo14891a(this);
        return false;
    }

    @DexIgnore
    /* renamed from: a */
    public synchronized boolean mo14885a(R r, java.lang.Object obj, com.fossil.blesdk.obfuscated.C1517bw<R> bwVar, com.bumptech.glide.load.DataSource dataSource, boolean z) {
        this.f8465l = true;
        this.f8462i = r;
        this.f8461h.mo14891a(this);
        return false;
    }
}
