package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gt1 {
    @DexIgnore
    public static /* final */ ct1 i; // = new ct1();
    @DexIgnore
    public static /* final */ dt1 j; // = new dt1();
    @DexIgnore
    public ct1 a;
    @DexIgnore
    public ct1 b;
    @DexIgnore
    public ct1 c;
    @DexIgnore
    public ct1 d;
    @DexIgnore
    public dt1 e;
    @DexIgnore
    public dt1 f;
    @DexIgnore
    public dt1 g;
    @DexIgnore
    public dt1 h;

    @DexIgnore
    public gt1() {
        ct1 ct1 = i;
        this.a = ct1;
        this.b = ct1;
        this.c = ct1;
        this.d = ct1;
        dt1 dt1 = j;
        this.e = dt1;
        this.f = dt1;
        this.g = dt1;
        this.h = dt1;
    }

    @DexIgnore
    public void a(dt1 dt1) {
        this.e = dt1;
    }

    @DexIgnore
    public ct1 b() {
        return this.d;
    }

    @DexIgnore
    public ct1 c() {
        return this.c;
    }

    @DexIgnore
    public dt1 d() {
        return this.h;
    }

    @DexIgnore
    public dt1 e() {
        return this.f;
    }

    @DexIgnore
    public dt1 f() {
        return this.e;
    }

    @DexIgnore
    public ct1 g() {
        return this.a;
    }

    @DexIgnore
    public ct1 h() {
        return this.b;
    }

    @DexIgnore
    public dt1 a() {
        return this.g;
    }
}
