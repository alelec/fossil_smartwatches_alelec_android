package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class i8<T> extends h8<T> {
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public i8(int i) {
        super(i);
    }

    @DexIgnore
    public T a() {
        T a;
        synchronized (this.c) {
            a = super.a();
        }
        return a;
    }

    @DexIgnore
    public boolean a(T t) {
        boolean a;
        synchronized (this.c) {
            a = super.a(t);
        }
        return a;
    }
}
