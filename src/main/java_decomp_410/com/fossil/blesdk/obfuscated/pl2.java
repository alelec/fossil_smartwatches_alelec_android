package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import com.fossil.wearables.fossil.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class pl2 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = cb4.a((T[]) new String[]{"weather", "commute-time"});
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = cb4.a((T[]) new String[]{Constants.MUSIC, "weather", "commute-time"});
    @DexIgnore
    public static /* final */ ArrayList<String> c; // = cb4.a((T[]) new String[]{"commute-time"});
    @DexIgnore
    public static /* final */ pl2 d; // = new pl2();

    @DexIgnore
    public final String a(String str) {
        kd4.b(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Complications_SecondTimezoneSelectCity_Title__ChooseACity);
                kd4.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return a2;
            }
        } else if (str.equals("commute-time")) {
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DetailsCommuteTime_CTA__SetDestination);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        }
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r4.equals("commute-time") != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        if (r4.equals("weather") == false) goto L_0x0077;
     */
    @DexIgnore
    public final List<String> b(String str) {
        kd4.b(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode != 104263205) {
                if (hashCode == 1223440372) {
                }
            } else if (str.equals(Constants.MUSIC)) {
                return cb4.a((T[]) new String[]{InAppPermission.NOTIFICATION_ACCESS});
            }
            return new ArrayList();
        }
        int i = Build.VERSION.SDK_INT;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppHelper", "android.os.Build.VERSION.SDK_INT=" + i);
        if (i >= 29) {
            return cb4.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
        }
        return cb4.a((T[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
    }

    @DexIgnore
    public final Integer c(String str) {
        kd4.b(str, "watchAppId");
        if (str.hashCode() == -829740640 && str.equals("commute-time")) {
            return Integer.valueOf(R.raw.diana_commute_time_animation);
        }
        return null;
    }

    @DexIgnore
    public final boolean d(String str) {
        kd4.b(str, "watchAppId");
        List<String> b2 = b(str);
        String[] a2 = ns3.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppHelper", "isPermissionGrantedForWatchApp " + str + " granted=" + a2 + " required=" + b2);
        for (String b3 : b2) {
            if (!za4.b((T[]) a2, b3)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean e(String str) {
        kd4.b(str, "watchAppId");
        return c.contains(str);
    }

    @DexIgnore
    public final boolean f(String str) {
        kd4.b(str, "watchAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean g(String str) {
        kd4.b(str, "watchAppId");
        return a.contains(str);
    }
}
