package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d72 {
    @DexIgnore
    public final DateTime a(String str) {
        try {
            return rk2.a(DateTimeZone.UTC, str);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateTimeUTCStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final String a(DateTime dateTime) {
        String a = rk2.a(DateTimeZone.UTC, dateTime);
        kd4.a((Object) a, "DateHelper.printServerDa\u2026t(DateTimeZone.UTC, date)");
        return qf4.a(a, "Z", "+0000", true);
    }
}
