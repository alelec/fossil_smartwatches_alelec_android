package com.fossil.blesdk.obfuscated;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dd implements bd {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public dd(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof dd)) {
            return false;
        }
        dd ddVar = (dd) obj;
        if (TextUtils.equals(this.a, ddVar.a) && this.b == ddVar.b && this.c == ddVar.c) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return e8.a(this.a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
