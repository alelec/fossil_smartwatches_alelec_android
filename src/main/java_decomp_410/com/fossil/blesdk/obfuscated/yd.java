package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.ld;
import com.fossil.blesdk.obfuscated.od;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yd<K, A, B> extends od<K, B> {
    @DexIgnore
    public /* final */ od<K, A> a;
    @DexIgnore
    public /* final */ m3<List<A>, List<B>> b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends od.c<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ od.c a;

        @DexIgnore
        public a(od.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(List<A> list, K k, K k2) {
            this.a.a(ld.convert(yd.this.b, list), k, k2);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends od.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ od.a a;

        @DexIgnore
        public b(od.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list, K k) {
            this.a.a(ld.convert(yd.this.b, list), k);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends od.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ od.a a;

        @DexIgnore
        public c(od.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list, K k) {
            this.a.a(ld.convert(yd.this.b, list), k);
        }
    }

    @DexIgnore
    public yd(od<K, A> odVar, m3<List<A>, List<B>> m3Var) {
        this.a = odVar;
        this.b = m3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(ld.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(od.f<K> fVar, od.a<K, B> aVar) {
        this.a.loadAfter(fVar, new c(aVar));
    }

    @DexIgnore
    public void loadBefore(od.f<K> fVar, od.a<K, B> aVar) {
        this.a.loadBefore(fVar, new b(aVar));
    }

    @DexIgnore
    public void loadInitial(od.e<K> eVar, od.c<K, B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(ld.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
