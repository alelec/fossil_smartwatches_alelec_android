package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class a63 implements Factory<HomeHybridCustomizePresenter> {
    @DexIgnore
    public static HomeHybridCustomizePresenter a(PortfolioApp portfolioApp, z53 z53, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, en2 en2) {
        return new HomeHybridCustomizePresenter(portfolioApp, z53, microAppRepository, hybridPresetRepository, setHybridPresetToWatchUseCase, en2);
    }
}
