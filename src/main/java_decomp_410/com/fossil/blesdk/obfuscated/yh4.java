package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yh4 implements zg4 {
    @DexIgnore
    public static /* final */ yh4 e; // = new yh4();

    @DexIgnore
    public CoroutineContext A() {
        return EmptyCoroutineContext.INSTANCE;
    }
}
