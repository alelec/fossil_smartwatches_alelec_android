package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.le;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ct2 extends le.d<ActivitySummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        kd4.b(activitySummary, "oldItem");
        kd4.b(activitySummary2, "newItem");
        return kd4.a((Object) activitySummary, (Object) activitySummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        kd4.b(activitySummary, "oldItem");
        kd4.b(activitySummary2, "newItem");
        return activitySummary.getDay() == activitySummary2.getDay() && activitySummary.getMonth() == activitySummary2.getMonth() && activitySummary.getYear() == activitySummary2.getYear();
    }
}
