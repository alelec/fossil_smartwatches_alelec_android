package com.fossil.blesdk.obfuscated;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class kj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ String h;
    @DexIgnore
    public /* final */ /* synthetic */ boolean i;
    @DexIgnore
    public /* final */ /* synthetic */ dj1 j;

    @DexIgnore
    public kj1(dj1 dj1, AtomicReference atomicReference, String str, String str2, String str3, boolean z) {
        this.j = dj1;
        this.e = atomicReference;
        this.f = str;
        this.g = str2;
        this.h = str3;
        this.i = z;
    }

    @DexIgnore
    public final void run() {
        this.j.a.m().a(this.e, this.f, this.g, this.h, this.i);
    }
}
