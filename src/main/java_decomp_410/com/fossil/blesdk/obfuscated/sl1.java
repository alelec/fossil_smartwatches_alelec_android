package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sl1 implements Parcelable.Creator<rl1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = SafeParcelReader.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        long j5 = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            switch (SafeParcelReader.a(a)) {
                case 2:
                    str = SafeParcelReader.f(parcel2, a);
                    break;
                case 3:
                    str2 = SafeParcelReader.f(parcel2, a);
                    break;
                case 4:
                    str3 = SafeParcelReader.f(parcel2, a);
                    break;
                case 5:
                    str4 = SafeParcelReader.f(parcel2, a);
                    break;
                case 6:
                    j = SafeParcelReader.s(parcel2, a);
                    break;
                case 7:
                    j2 = SafeParcelReader.s(parcel2, a);
                    break;
                case 8:
                    str5 = SafeParcelReader.f(parcel2, a);
                    break;
                case 9:
                    z = SafeParcelReader.i(parcel2, a);
                    break;
                case 10:
                    z2 = SafeParcelReader.i(parcel2, a);
                    break;
                case 11:
                    j5 = SafeParcelReader.s(parcel2, a);
                    break;
                case 12:
                    str6 = SafeParcelReader.f(parcel2, a);
                    break;
                case 13:
                    j3 = SafeParcelReader.s(parcel2, a);
                    break;
                case 14:
                    j4 = SafeParcelReader.s(parcel2, a);
                    break;
                case 15:
                    i = SafeParcelReader.q(parcel2, a);
                    break;
                case 16:
                    z3 = SafeParcelReader.i(parcel2, a);
                    break;
                case 17:
                    z4 = SafeParcelReader.i(parcel2, a);
                    break;
                case 18:
                    z5 = SafeParcelReader.i(parcel2, a);
                    break;
                case 19:
                    str7 = SafeParcelReader.f(parcel2, a);
                    break;
                default:
                    SafeParcelReader.v(parcel2, a);
                    break;
            }
        }
        SafeParcelReader.h(parcel2, b);
        return new rl1(str, str2, str3, str4, j, j2, str5, z, z2, j5, str6, j3, j4, i, z3, z4, z5, str7);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new rl1[i];
    }
}
