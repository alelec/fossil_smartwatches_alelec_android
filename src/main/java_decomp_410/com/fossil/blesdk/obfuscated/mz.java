package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mz implements c00 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ c00[] b;
    @DexIgnore
    public /* final */ nz c;

    @DexIgnore
    public mz(int i, c00... c00Arr) {
        this.a = i;
        this.b = c00Arr;
        this.c = new nz(i);
    }

    @DexIgnore
    public StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr.length <= this.a) {
            return stackTraceElementArr;
        }
        StackTraceElement[] stackTraceElementArr2 = stackTraceElementArr;
        for (c00 c00 : this.b) {
            if (stackTraceElementArr2.length <= this.a) {
                break;
            }
            stackTraceElementArr2 = c00.a(stackTraceElementArr);
        }
        return stackTraceElementArr2.length > this.a ? this.c.a(stackTraceElementArr2) : stackTraceElementArr2;
    }
}
