package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class mg1 extends j61 implements kg1 {
    @DexIgnore
    public mg1(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    public final void a(hg1 hg1, rl1 rl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) hg1);
        r61.a(o, (Parcelable) rl1);
        b(1, o);
    }

    @DexIgnore
    public final void c(rl1 rl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) rl1);
        b(6, o);
    }

    @DexIgnore
    public final String d(rl1 rl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) rl1);
        Parcel a = a(11, o);
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    @DexIgnore
    public final void a(kl1 kl1, rl1 rl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) kl1);
        r61.a(o, (Parcelable) rl1);
        b(2, o);
    }

    @DexIgnore
    public final void a(rl1 rl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) rl1);
        b(4, o);
    }

    @DexIgnore
    public final void a(hg1 hg1, String str, String str2) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) hg1);
        o.writeString(str);
        o.writeString(str2);
        b(5, o);
    }

    @DexIgnore
    public final void a(long j, String str, String str2, String str3) throws RemoteException {
        Parcel o = o();
        o.writeLong(j);
        o.writeString(str);
        o.writeString(str2);
        o.writeString(str3);
        b(10, o);
    }

    @DexIgnore
    public final void a(vl1 vl1, rl1 rl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) vl1);
        r61.a(o, (Parcelable) rl1);
        b(12, o);
    }

    @DexIgnore
    public final void a(vl1 vl1) throws RemoteException {
        Parcel o = o();
        r61.a(o, (Parcelable) vl1);
        b(13, o);
    }

    @DexIgnore
    public final List<kl1> a(String str, String str2, boolean z, rl1 rl1) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        r61.a(o, z);
        r61.a(o, (Parcelable) rl1);
        Parcel a = a(14, o);
        ArrayList<kl1> createTypedArrayList = a.createTypedArrayList(kl1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<kl1> a(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        o.writeString(str3);
        r61.a(o, z);
        Parcel a = a(15, o);
        ArrayList<kl1> createTypedArrayList = a.createTypedArrayList(kl1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<vl1> a(String str, String str2, rl1 rl1) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        r61.a(o, (Parcelable) rl1);
        Parcel a = a(16, o);
        ArrayList<vl1> createTypedArrayList = a.createTypedArrayList(vl1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    public final List<vl1> a(String str, String str2, String str3) throws RemoteException {
        Parcel o = o();
        o.writeString(str);
        o.writeString(str2);
        o.writeString(str3);
        Parcel a = a(17, o);
        ArrayList<vl1> createTypedArrayList = a.createTypedArrayList(vl1.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }
}
