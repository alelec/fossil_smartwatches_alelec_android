package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.fragment.app.FragmentManager;
import com.fossil.blesdk.obfuscated.ws3;
import com.fossil.wearables.fossil.R;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.ReturnCodeRangeChecker;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DashbarData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.enums.GoalType;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import com.portfolio.platform.view.NumberPickerLarge;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ds3 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ String b; // = b;
    @DexIgnore
    public static /* final */ ds3 c; // = new ds3();

    /*
    static {
        String simpleName = ds3.class.getSimpleName();
        kd4.a((Object) simpleName, "DialogUtils::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final void A(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Android_LocationNotification_Text__ToUseYourSmartwatchsFindDevice);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026ourSmartwatchsFindDevice)");
        c(fragmentManager, a2);
    }

    @DexIgnore
    public final void B(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__LocationPermissionIsRequired);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__LocationServicesAreRequiredForLocation);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        b(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void C(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__LocationPermissionIsRequired);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidLocationDisabled_Text__ForYourDevicesLowEnergyBluetooth);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        b(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void D(FragmentManager fragmentManager) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Server_Maintenance_Header__ServerMaintenance));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Server_Maintenance_Text__OurServersAreCurrentlyUndergoingMaintenance));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Server_Maintenance_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void E(FragmentManager fragmentManager) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.error));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_PleaseTryAgain_Text__PleaseTryAgainAfterAFew));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "SERVER_MAINTENANCE");
    }

    @DexIgnore
    public final void F(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragment");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.error));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.set_alarm_error_des));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "CONFIRM_SET_ALARM_FAILED");
    }

    @DexIgnore
    public final void G(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.fragment_troubleshooting);
        fVar.a((int) R.id.iv_close);
        fVar.a((int) R.id.iv_close);
        fVar.a(fragmentManager, "DEVICE_SET_DATA_FAILED");
    }

    @DexIgnore
    public final void H(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Sync_Failed_Text__SyncingFailed));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void I(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_XTappedAfterChanges_Text__WouldYouLikeToSaveChanges));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_XTappedAfterChanges_CTA__Save));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_XTappedAfterChanges_CTA__Discard));
        fVar.a(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void J(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_FirmwareUpdateFailed_DashboardPopup_Title__UpdateYourWatch));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_FirmwareUpdateFailed_DashboardPopup_Text__ToCustomizeAndTryNewFeatures));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_FirmwareUpdateFailed_CustomizationPopup_CTA__Update));
        fVar.a(false);
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "FIRMWARE_UPDATE_FAIL");
    }

    @DexIgnore
    public final String a() {
        return b;
    }

    @DexIgnore
    public final void b(String str, FragmentManager fragmentManager) {
        kd4.b(str, "serial");
        kd4.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____QuitWorkoutAppToRemoveActive));
        pd4 pd4 = pd4.a;
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____RemovingAnActiveDeviceIsDisabled);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026AnActiveDeviceIsDisabled)");
        Object[] objArr = {str};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a((int) R.id.tv_description, format);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____QuitSync));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____DontQuit));
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "REMOVE_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final void c(String str, FragmentManager fragmentManager) {
        kd4.b(str, "serial");
        kd4.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_Title__YourHealthDataMayBeLost));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_Text__YourActiveWatchIsDisconnectedAnd));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_CTA__Switch));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_CTA__Cancel));
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "REMOVE_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void d(String str, FragmentManager fragmentManager) {
        kd4.b(str, "serial");
        kd4.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_Title__YourHealthDataMayBeLost));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_Text__YourActiveWatchIsDisconnectedAnd));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_CTA__Switch));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_CTA__Cancel));
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "SWITCH_DEVICE_ERASE_FAIL", bundle);
    }

    @DexIgnore
    public final void e(String str, FragmentManager fragmentManager) {
        kd4.b(str, "serial");
        kd4.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____QuitWorkoutApp));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____WorkoutAppIsRunningWouldYou));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____QuitSync));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string._____DontQuit));
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "SWITCH_DEVICE_WORKOUT", bundle);
    }

    @DexIgnore
    public final void f(String str, FragmentManager fragmentManager) {
        kd4.b(str, "serial");
        kd4.b(fragmentManager, "fragmentManager");
        Bundle bundle = new Bundle();
        bundle.putString("SERIAL", str);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_Title__YourHealthDataMayBeLost));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_Text__YourActiveWatchIsDisconnectedAnd));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_CTA__Switch));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SwitchWithoutSync_ActiveWatchSwitchWithoutSync_CTA__Cancel));
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "SWITCH_DEVICE_SYNC_FAIL", bundle);
    }

    @DexIgnore
    public final void g(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_DeleteAlarm_DeleteConfirmation_Text__AreYouSureYouWantTo));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_DeleteAlarm_DeleteConfirmation_CTA__Delete));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_DeleteAlarm_DeleteConfirmation_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_DELETE_ALARM");
    }

    @DexIgnore
    public final void h(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Sync_Error_Text__YourWatchIsDisconnectedPleaseSync));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.___CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "FAIL_DUE_TO_DEVICE_DISCONNECTED");
    }

    @DexIgnore
    public final void i(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_UnhappyUser_Title__YourFeedbackIsImportantToUs));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_UnhappyUser_Text__HelpUsImproveByTellingUs));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_UnhappyUser_CTA__SendFeedback));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_UnhappyUser_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "FEEDBACK_CONFIRM");
    }

    @DexIgnore
    public final void j(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPageAddError___SelectedTimeIsInTheFuture));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPageAddError___Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "GOAL_TRACKING_ADD_FUTURE_ERROR");
    }

    @DexIgnore
    public final void k(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.error));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_General_Troubleshooting_CTA__TryAgain));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "GENERAL_ERROR");
    }

    @DexIgnore
    public final void l(FragmentManager fragmentManager) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Connectivity_Error_Text__ThereWasAProblemProcessingThat));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void m(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_Title__YourAppExperience));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_Text__DoYouLikeTheApp));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_CTA__Yes));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_CTA__No));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "HAPPINESS_CONFIRM");
    }

    @DexIgnore
    public final void n(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DianaWeatherAddLocationMax_Text__YouCanAddUpTo2));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DianaWeatherAddLocationMax_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "LOCATION_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void o(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.commute_time_app__YouCanAddUpTo2));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.commute_time_app_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void p(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.commute_time_app__Name_invalid));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.commute_time_app_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "COMMUTE_TIME_MAXIMUM_REACHED_ERROR");
    }

    @DexIgnore
    public final void q(FragmentManager fragmentManager) {
        ws3.f fVar = new ws3.f(R.layout.dialog_network_one_action);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Text__PleaseCheckYourInternetConnectionAnd));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(false);
        fVar.a(fragmentManager, "NETWORK_ERROR");
    }

    @DexIgnore
    public final void r(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Logout_ConfirmLogout_Title__AreYouSure));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Logout_ConfirmLogout_Text__LoggingOutWillUnpairYourWatch));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Logout_ConfirmLogout_CTA__Logout));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Logout_ConfirmLogout_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_LOGOUT_ACCOUNT");
    }

    @DexIgnore
    public final void s(FragmentManager fragmentManager) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Text__PleaseCheckYourInternetConnectionAnd));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionwithActions_CTA__Cancel));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionwithActions_CTA__TryAgain));
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "NO_INTERNET_CONNECTION");
    }

    @DexIgnore
    public final void t(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Notifications_Onboarding_Notifications_Header_NOTIFICATION_ACCESS_REQUIRED));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Notifications_Onboarding_Notifications_Text_To_receive_notifications_we_will));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Profile_Title__Settings));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a(fragmentManager, "REQUEST_NOTIFICATION_ACCESS");
    }

    @DexIgnore
    public final void u(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_General_Error_Location_Header_TURN_ON_LOCATION);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__LocationServicesAreRequiredForLocation);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
        a(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void v(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Pair_Permission_Header_TURN_ON_LOCATION);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026_Header_TURN_ON_LOCATION)");
        String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidLocationDisabled_Text__ForYourDevicesLowEnergyBluetooth);
        kd4.a((Object) a3, "LanguageHelper.getString\u2026evicesLowEnergyBluetooth)");
        a(fragmentManager, a2, a3);
    }

    @DexIgnore
    public final void w(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Sync_Failed_Text__SyncingFailed));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a(false);
        fVar.a(fragmentManager, "SYNC_FAILED");
    }

    @DexIgnore
    public final void x(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, Constants.ACTIVITY);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.error));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_ResetPassword_WebReset_Text__Min7Characters));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "WRONG_FORMAT_PASSWORD");
    }

    @DexIgnore
    public final void y(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_AddAlarm_MaxAssigned_Text__YouveReachedTheLimitOnAlarms));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Buttons_DianaWeatherAddLocationMax_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "MAX_NUMBER_OF_ALARMS");
    }

    @DexIgnore
    public final void z(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.calls_and_messages_permission));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.this_feature_requires_contact_phone_sms_permission));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Profile_Title__Settings));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a(fragmentManager, "REQUEST_CONTACT_PHONE_SMS_PERMISSION");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, int i2, String str) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(str, "emailAddress");
        DashbarData dashbarData = new DashbarData(R.id.progressBar, i, i2);
        ws3.f fVar = new ws3.f(R.layout.fragment_getting_started);
        fVar.a((int) R.id.ftv_email, str);
        fVar.a(dashbarData);
        fVar.a((int) R.id.iv_close);
        fVar.a((int) R.id.bt_continue_sign_up);
        fVar.b(true);
        fVar.a(false);
        fVar.b((int) R.color.transparent);
        kd4.a((Object) fVar, "AlertDialogFragment.Buil\u2026olor(R.color.transparent)");
        ws3 a2 = fVar.a("EMAIL_OTP_VERIFICATION");
        kd4.a((Object) a2, "builder.create(EMAIL_OTP_VERIFICATION)");
        a2.setStyle(0, R.style.AppTheme);
        a2.show(fragmentManager, "EMAIL_OTP_VERIFICATION");
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager, String str, String str2) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_description, str2);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AllowAccesstoCameraRoll_CTA__Allow));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AllowAccesstoCameraRoll_CTA__DontAllow));
        fVar.a(fragmentManager, "REQUEST_LOCATION_SERVICE_PERMISSION");
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager, String str) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_title, str);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidInAppNotication_CTA__Allow));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_Permissions_AndroidInAppNotication_CTA__NotNow));
        fVar.a(fragmentManager, b);
    }

    @DexIgnore
    public final void d(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_Title__RateOurApp));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_Text__WereGladYoureEnjoyingTheApp));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_CTA__Rate));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.General_Review_HappyUser_CTA__Dismiss));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "APP_RATING_CONFIRM");
    }

    @DexIgnore
    public final void e(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, Constants.ACTIVITY);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_General_Error_Bluetooth_Header_TURN_ON_BLUETOOTH));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_Pairing_AndroidPermissions_Text__PleaseTurnOnBluetoothInOrder));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Profile_Title__Settings));
        fVar.a(false);
        fVar.a(fragmentManager, "BLUETOOTH_OFF");
    }

    @DexIgnore
    public final void f(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_DeleteAccount_Title__AreYouSure));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_DeleteAccount_Text__OnceYouDeleteYourAccountAll));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_DeleteAccount_CTA__Delete));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_Help_DeleteAccount_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_DELETE_ACCOUNT");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str, String str2) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_title, str);
        fVar.a((int) R.id.tv_description, str2);
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Onboarding_WithoutDevice_Profile_Title__Settings));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_InAppNotification_Template_CTA__Ok));
        fVar.a(fragmentManager, "REQUEST_OPEN_LOCATION_SERVICE");
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager, String str) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(str, "deviceName");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_RemoveWatch_Title__AreYouSureYouWantTo));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_RemoveWatch_Text__OnceRemovedYourWatchWillStop));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_RemoveWatch_CTA__Remove));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_RemoveWatch_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_REMOVE_DEVICE");
    }

    @DexIgnore
    public final void c(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SyncErrorWorkout_DiabledQuitandSync_Title__QuickWorkoutAppToSync));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SyncErrorWorkout_DiabledQuitandSync_Text__SyncIsDisabledWhileWorkoutApp));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SyncErrorWorkout_DiabledQuitandSync_CTA__QuitSync));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_SyncErrorWorkout_DiabledQuitandSync_CTA__DontQuit));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(false);
        ws3 a2 = fVar.a(fragmentManager, "ASK_TO_CANCEL_WORKOUT");
        kd4.a((Object) a2, "AlertDialogFragment.Buil\u2026r, ASK_TO_CANCEL_WORKOUT)");
        a2.setCancelable(false);
    }

    @DexIgnore
    public final void a(int i, String str, FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                l(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        s(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            l(fragmentManager);
                            return;
                        } else if (str != null) {
                            a(str, fragmentManager);
                            return;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
                D(fragmentManager);
            } else {
                E(fragmentManager);
            }
        }
    }

    @DexIgnore
    public final void b(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_XTappedAfterChanges_Text__WouldYouLikeToSaveChanges));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_XTappedAfterChanges_CTA__Save));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_XTappedAfterChanges_CTA__Discard));
        fVar.a(fragmentManager, "UNSAVED_CHANGE");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, String str) {
        kd4.b(fragmentManager, "fragmentManager");
        if (ReturnCodeRangeChecker.isSuccessReturnCode(i)) {
            FLogger.INSTANCE.getLocal().d(a, "Response is OK, no need to show error message");
        } else if (i == 401) {
        } else {
            if (i == 408) {
                l(fragmentManager);
            } else if (i != 429) {
                if (i != 500) {
                    if (i == 601) {
                        q(fragmentManager);
                        return;
                    } else if (!(i == 503 || i == 504)) {
                        if (TextUtils.isEmpty(str)) {
                            l(fragmentManager);
                            return;
                        } else if (str != null) {
                            a(str, fragmentManager);
                            return;
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                }
                D(fragmentManager);
            } else {
                E(fragmentManager);
            }
        }
    }

    @DexIgnore
    public final void a(String str, FragmentManager fragmentManager) {
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_Title__NetworkError));
        fVar.a((int) R.id.tv_description, str);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DesignPatterns_ModalError_ConnectionError_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "SERVER_ERROR");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, ContactGroup contactGroup) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(contactGroup, "contactGroup");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REMOVE_CONTACT_BUNDLE", contactGroup);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.remove_contact));
        pd4 pd4 = pd4.a;
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.remove_contact_description);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026move_contact_description)");
        Contact contact = contactGroup.getContacts().get(0);
        kd4.a((Object) contact, "contactGroup.contacts[0]");
        Object[] objArr = {contact.getDisplayName()};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a((int) R.id.tv_description, format);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyWatch_RemoveWatch_CTA__Remove));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Customization_Delete_Confirm_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_REMOVE_CONTACT", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(str, "description");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action_with_title);
        fVar.a((int) R.id.tv_title, "");
        fVar.a((int) R.id.tv_description, str);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsDiana_MoveAlerts_Error_CTA__Ok));
        fVar.a((int) R.id.tv_ok);
        fVar.a(fragmentManager, "DND_SCHEDULED_TIME_ERROR");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, ContactWrapper contactWrapper, int i, int i2) {
        String str;
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(contactWrapper, "contactWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER", contactWrapper);
        Contact contact = contactWrapper.getContact();
        if (contact == null || contact.getContactId() != -100) {
            Contact contact2 = contactWrapper.getContact();
            if (contact2 == null || contact2.getContactId() != -200) {
                Contact contact3 = contactWrapper.getContact();
                if (contact3 != null) {
                    str = contact3.getDisplayName();
                    kd4.a((Object) str, "contactWrapper.contact!!.displayName");
                } else {
                    kd4.a();
                    throw null;
                }
            } else {
                str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_Everyone_Text__MessagesFromEveryone);
                kd4.a((Object) str, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
            }
        } else {
            str = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_Everyone_Text__CallsFromEveryone);
            kd4.a((Object) str, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
        }
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        pd4 pd4 = pd4.a;
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactsReassign_Text__DoYouWantToReassignContact);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        Object[] objArr = {str, Integer.valueOf(i), Integer.valueOf(i2)};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a((int) R.id.tv_description, format);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactsReassign_CTA__Ok));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactsReassign_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_REASSIGN_CONTACT", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, String str, int i, int i2, AppWrapper appWrapper) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(str, "name");
        kd4.b(appWrapper, "appWrapper");
        Bundle bundle = new Bundle();
        bundle.putSerializable("CONFIRM_REASSIGN_APPWRAPPER", appWrapper);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action);
        pd4 pd4 = pd4.a;
        String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactsReassign_Text__DoYouWantToReassignContact);
        kd4.a((Object) a2, "LanguageHelper.getString\u2026YouWantToReassignContact)");
        Object[] objArr = {str, String.valueOf(i), String.valueOf(i2)};
        String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
        kd4.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a((int) R.id.tv_description, format);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactsReassign_CTA__Ok));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_ContactsReassign_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "CONFIRM_REASSIGN_APP", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, int i, int i2, int i3, String[] strArr) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(strArr, "displayedValues");
        ws3.f fVar = new ws3.f(R.layout.dialog_add_goal_tracking);
        fVar.a((int) R.id.fb_add);
        fVar.a((int) R.id.root);
        fVar.a((int) R.id.np_hour, 1, 12, i);
        fVar.a((int) R.id.np_minute, 0, 59, i2);
        fVar.a(R.id.np_suffix, 0, 1, i3, NumberPickerLarge.getTwoDigitFormatter(), strArr);
        fVar.a(fragmentManager, "GOAL_TRACKING_ADD");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, GoalTrackingData goalTrackingData) {
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(goalTrackingData, "goalTrackingData");
        Bundle bundle = new Bundle();
        bundle.putSerializable("GOAL_TRACKING_DELETE_BUNDLE", goalTrackingData);
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_two_action_with_title);
        fVar.a((int) R.id.tv_title, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPageDeleteConfirmation_Title__DeleteRecordedGoalTracking));
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPageDeleteConfirmation_Text__AreYouSureYouWantTo));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPageDeleteConfirmation_CTA__Delete));
        fVar.a((int) R.id.tv_cancel, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.DashboardHybrid_GoalTracking_DetailPageDeleteConfirmation_CTA__Cancel));
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_cancel);
        fVar.a(fragmentManager, "GOAL_TRACKING_DELETE", bundle);
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager) {
        kd4.b(fragmentManager, "fragmentManager");
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_description, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_MaxAssigned_Text__YouCanAssignUpTo12));
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.AlertsHybrid_AssignNotifications_MaxAssigned_CTA__Ok));
        fVar.a(fragmentManager, "NOTIFICATION_WARNING");
    }

    @DexIgnore
    public final void a(FragmentManager fragmentManager, GoalType goalType, int i) {
        String str;
        kd4.b(fragmentManager, "fragmentManager");
        kd4.b(goalType, "type");
        int i2 = cs3.a[goalType.ordinal()];
        if (i2 == 1) {
            pd4 pd4 = pd4.a;
            String a2 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyProfileDiana_SetGoalsStepsExceeded_Text__PleaseSetAGoalOfLess);
            kd4.a((Object) a2, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            Object[] objArr = {Integer.valueOf(i)};
            str = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            kd4.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 == 2) {
            pd4 pd42 = pd4.a;
            String a3 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyProfileDiana_SetGoalsActiveCaloriesExceeded_Text__PleaseSetAGoalOfLess);
            kd4.a((Object) a3, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            Object[] objArr2 = {Integer.valueOf(i)};
            str = String.format(a3, Arrays.copyOf(objArr2, objArr2.length));
            kd4.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 == 3) {
            pd4 pd43 = pd4.a;
            String a4 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyProfileDiana_SetGoalsActiveMinutesExceeded_Text__PleaseSetAGoalOfLess);
            kd4.a((Object) a4, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            Object[] objArr3 = {Integer.valueOf(i)};
            str = String.format(a4, Arrays.copyOf(objArr3, objArr3.length));
            kd4.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 == 4) {
            pd4 pd44 = pd4.a;
            String a5 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyProfileDiana_SetGoalsSleepExceeded_Text__PleaseSetASleepGoalOf);
            kd4.a((Object) a5, "LanguageHelper.getString\u2026t__PleaseSetASleepGoalOf)");
            Object[] objArr4 = {Integer.valueOf(i)};
            str = String.format(a5, Arrays.copyOf(objArr4, objArr4.length));
            kd4.a((Object) str, "java.lang.String.format(format, *args)");
        } else if (i2 != 5) {
            str = "";
        } else {
            pd4 pd45 = pd4.a;
            String a6 = sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyProfileHybrid_SetGoalsGoalTrackingExceeded_Text__PleaseSetAGoalOfLess);
            kd4.a((Object) a6, "LanguageHelper.getString\u2026xt__PleaseSetAGoalOfLess)");
            Object[] objArr5 = {Integer.valueOf(i)};
            str = String.format(a6, Arrays.copyOf(objArr5, objArr5.length));
            kd4.a((Object) str, "java.lang.String.format(format, *args)");
        }
        ws3.f fVar = new ws3.f(R.layout.dialog_confirmation_one_action);
        fVar.a((int) R.id.tv_ok);
        fVar.a((int) R.id.tv_description, str);
        fVar.a((int) R.id.tv_ok, sm2.a((Context) PortfolioApp.W.c(), (int) R.string.Profile_MyProfileDiana_SetGoalsStepsExceeded_CTA__Dismiss));
        fVar.a(fragmentManager, "NOTIFICATION_WARNING");
    }
}
