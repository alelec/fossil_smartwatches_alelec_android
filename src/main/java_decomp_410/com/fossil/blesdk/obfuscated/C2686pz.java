package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.pz */
public final class C2686pz {
    @DexIgnore
    /* renamed from: a */
    public static byte[] m12498a(java.io.InputStream inputStream) throws java.io.IOException {
        byte[] bArr = new byte[1024];
        java.io.ByteArrayOutputStream byteArrayOutputStream = new java.io.ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static byte[] m12499b(java.io.File file) {
        java.io.File a = m12494a(file, ".dmp");
        if (a == null) {
            return new byte[0];
        }
        return m12501c(a);
    }

    @DexIgnore
    /* renamed from: c */
    public static byte[] m12501c(java.io.File file) {
        return m12503d(file);
    }

    @DexIgnore
    /* renamed from: d */
    public static byte[] m12503d(java.io.File file) {
        java.io.FileInputStream fileInputStream;
        java.io.FileInputStream fileInputStream2 = null;
        try {
            fileInputStream = new java.io.FileInputStream(file);
            try {
                byte[] a = m12498a((java.io.InputStream) fileInputStream);
                p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream);
                return a;
            } catch (java.io.FileNotFoundException unused) {
                p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream);
                return null;
            } catch (java.io.IOException unused2) {
                p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream);
                return null;
            } catch (Throwable th) {
                th = th;
                fileInputStream2 = fileInputStream;
                p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream2);
                throw th;
            }
        } catch (java.io.FileNotFoundException unused3) {
            fileInputStream = null;
            p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream);
            return null;
        } catch (java.io.IOException unused4) {
            fileInputStream = null;
            p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) fileInputStream2);
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: c */
    public static byte[] m12502c(java.io.File file, android.content.Context context) throws java.io.IOException {
        java.io.BufferedReader bufferedReader;
        if (!file.exists()) {
            return null;
        }
        try {
            bufferedReader = new java.io.BufferedReader(new java.io.FileReader(file));
            try {
                byte[] a = new com.fossil.blesdk.obfuscated.C2510ny(context, new com.fossil.blesdk.obfuscated.b00()).mo14146a(bufferedReader);
                p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) bufferedReader);
                return a;
            } catch (Throwable th) {
                th = th;
                p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) bufferedReader);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            p011io.fabric.sdk.android.services.common.CommonUtils.m36872a((java.io.Closeable) bufferedReader);
            throw th;
        }
    }

    @DexIgnore
    /* renamed from: b */
    public static byte[] m12500b(java.io.File file, android.content.Context context) throws java.io.IOException {
        java.io.File a = m12494a(file, ".maps");
        if (a != null) {
            return m12502c(a, context);
        }
        java.io.File a2 = m12494a(file, ".binary_libs");
        if (a2 != null) {
            return m12497a(a2, context);
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static java.io.File m12494a(java.io.File file, java.lang.String str) {
        for (java.io.File file2 : file.listFiles()) {
            if (file2.getName().endsWith(str)) {
                return file2;
            }
        }
        return null;
    }

    @DexIgnore
    /* renamed from: a */
    public static byte[] m12497a(java.io.File file, android.content.Context context) throws java.io.IOException {
        byte[] d = m12503d(file);
        if (d == null || d.length == 0) {
            return null;
        }
        return m12495a(context, new java.lang.String(d));
    }

    @DexIgnore
    /* renamed from: a */
    public static byte[] m12496a(java.io.File file) {
        java.io.File a = m12494a(file, ".device_info");
        if (a == null) {
            return null;
        }
        return m12503d(a);
    }

    @DexIgnore
    /* renamed from: a */
    public static byte[] m12495a(android.content.Context context, java.lang.String str) throws java.io.IOException {
        return new com.fossil.blesdk.obfuscated.C2510ny(context, new com.fossil.blesdk.obfuscated.b00()).mo14147a(str);
    }
}
