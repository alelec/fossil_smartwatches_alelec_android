package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ti3 implements Factory<DeleteAccountPresenter> {
    @DexIgnore
    public static DeleteAccountPresenter a(qi3 qi3, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, kr2 kr2, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        return new DeleteAccountPresenter(qi3, deviceRepository, analyticsHelper, kr2, deleteLogoutUserUseCase);
    }
}
