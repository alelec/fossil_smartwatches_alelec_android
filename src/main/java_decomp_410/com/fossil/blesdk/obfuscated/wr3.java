package com.fossil.blesdk.obfuscated;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class wr3 {
    @DexIgnore
    public static final void a(Canvas canvas, RectF rectF, Paint paint, float f) {
        kd4.b(canvas, "$this$drawTopLeftRoundRect");
        kd4.b(rectF, "rect");
        kd4.b(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopLeftRoundRect, rect: " + rectF + ", radius: " + f);
        canvas.drawRoundRect(rectF, f, f, paint);
        Canvas canvas2 = canvas;
        Paint paint2 = paint;
        canvas2.drawRect((rectF.width() / ((float) 2)) + rectF.left, rectF.top, rectF.right, rectF.bottom, paint2);
        canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint2);
    }

    @DexIgnore
    public static final void b(Canvas canvas, RectF rectF, Paint paint, float f) {
        kd4.b(canvas, "$this$drawTopRightRoundRect");
        kd4.b(rectF, "rect");
        kd4.b(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopRightRoundRect, rect: " + rectF + ", radius: " + f);
        canvas.drawRoundRect(rectF, f, f, paint);
        Paint paint2 = paint;
        canvas.drawRect(rectF.left, rectF.top, rectF.right - (rectF.width() / ((float) 2)), rectF.bottom, paint2);
        canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint2);
    }

    @DexIgnore
    public static final void c(Canvas canvas, RectF rectF, Paint paint, float f) {
        kd4.b(canvas, "$this$drawTopRoundRect");
        kd4.b(rectF, "rect");
        kd4.b(paint, "paint");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CanvasUtils", "drawTopRoundRect, rect: " + rectF + ", radius: " + f);
        if (rectF.height() >= f) {
            canvas.drawRoundRect(rectF, f, f, paint);
            canvas.drawRect(rectF.left, rectF.top + f, rectF.right, rectF.bottom, paint);
        }
    }

    @DexIgnore
    public static final void a(Canvas canvas, float f, float f2, float f3, float f4, float f5, float f6, boolean z, boolean z2, boolean z3, boolean z4, Paint paint) {
        Canvas canvas2 = canvas;
        float f7 = f3;
        Paint paint2 = paint;
        kd4.b(canvas, "$this$drawRoundedPath");
        kd4.b(paint2, "paint");
        Path path = new Path();
        float f8 = (float) 0;
        float f9 = f5 < f8 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f5;
        float f10 = f6 < f8 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f6;
        float f11 = f7 - f;
        float f12 = f4 - f2;
        float f13 = (float) 2;
        float f14 = f11 / f13;
        if (f9 > f14) {
            f9 = f14;
        }
        if (f10 > f14) {
            f10 = f14;
        }
        float f15 = f11 - (f13 * f9);
        float f16 = f12 - (f13 * f10);
        path.moveTo(f3, f2 + f10);
        if (z2) {
            float f17 = -f10;
            path.rQuadTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f17, -f9, f17);
        } else {
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f10);
            path.rLineTo(-f9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        path.rLineTo(-f15, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        if (z) {
            float f18 = -f9;
            path.rQuadTo(f18, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f18, f10);
        } else {
            path.rLineTo(-f9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f10);
        }
        path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f16);
        if (z4) {
            path.rQuadTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f10, f9, f10);
        } else {
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f10);
            path.rLineTo(f9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        path.rLineTo(f15, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        if (z3) {
            path.rQuadTo(f9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9, -f10);
        } else {
            path.rLineTo(f9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f10);
        }
        path.rLineTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, -f16);
        path.close();
        canvas.drawPath(path, paint2);
    }
}
