package com.fossil.blesdk.obfuscated;

import kotlin.coroutines.CoroutineContext;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ak4<T> extends uf4<T> implements fc4 {
    @DexIgnore
    public /* final */ yb4<T> h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ak4(CoroutineContext coroutineContext, yb4<? super T> yb4) {
        super(coroutineContext, true);
        kd4.b(coroutineContext, "context");
        kd4.b(yb4, "uCont");
        this.h = yb4;
    }

    @DexIgnore
    public void a(Object obj, int i) {
        if (obj instanceof ng4) {
            Throwable th = ((ng4) obj).a;
            if (i != 4) {
                th = ck4.a(th, (yb4<?>) this.h);
            }
            wi4.a(this.h, th, i);
            return;
        }
        wi4.b(this.h, obj, i);
    }

    @DexIgnore
    public final boolean f() {
        return true;
    }

    @DexIgnore
    public final fc4 getCallerFrame() {
        return (fc4) this.h;
    }

    @DexIgnore
    public final StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public int j() {
        return 2;
    }
}
