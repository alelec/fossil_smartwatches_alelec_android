package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f5 */
public class C1770f5 extends com.fossil.blesdk.obfuscated.C1857g5 {

    @DexIgnore
    /* renamed from: c */
    public float f5028c; // = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;

    @DexIgnore
    /* renamed from: a */
    public void mo10739a(int i) {
        if (this.f5384b == 0 || this.f5028c != ((float) i)) {
            this.f5028c = (float) i;
            if (this.f5384b == 1) {
                mo11139b();
            }
            mo11137a();
        }
    }

    @DexIgnore
    /* renamed from: d */
    public void mo10332d() {
        super.mo10332d();
        this.f5028c = com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    /* renamed from: f */
    public void mo10740f() {
        this.f5384b = 2;
    }
}
