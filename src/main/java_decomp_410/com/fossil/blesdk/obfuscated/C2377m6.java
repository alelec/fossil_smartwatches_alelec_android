package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.m6 */
public final class C2377m6 {
    @DexIgnore
    /* renamed from: a */
    public static android.content.res.ColorStateList m10555a(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        int next;
        android.util.AttributeSet asAttributeSet = android.util.Xml.asAttributeSet(xmlPullParser);
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return m10556a(resources, xmlPullParser, asAttributeSet, theme);
        }
        throw new org.xmlpull.v1.XmlPullParserException("No start tag found");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v5, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    public static android.content.res.ColorStateList m10558b(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        android.util.AttributeSet attributeSet2 = attributeSet;
        int i = 1;
        int depth = xmlPullParser.getDepth() + 1;
        int[][] iArr = new int[20][];
        int[] iArr2 = new int[iArr.length];
        int i2 = 0;
        while (true) {
            int next = xmlPullParser.next();
            if (next == i) {
                break;
            }
            int depth2 = xmlPullParser.getDepth();
            if (depth2 < depth && next == 3) {
                break;
            }
            if (next != 2 || depth2 > depth || !xmlPullParser.getName().equals("item")) {
                android.content.res.Resources resources2 = resources;
                android.content.res.Resources.Theme theme2 = theme;
            } else {
                android.content.res.TypedArray a = m10557a(resources, theme, attributeSet2, com.fossil.blesdk.obfuscated.C3008u5.ColorStateListItem);
                int color = a.getColor(com.fossil.blesdk.obfuscated.C3008u5.ColorStateListItem_android_color, -65281);
                float f = 1.0f;
                if (a.hasValue(com.fossil.blesdk.obfuscated.C3008u5.ColorStateListItem_android_alpha)) {
                    f = a.getFloat(com.fossil.blesdk.obfuscated.C3008u5.ColorStateListItem_android_alpha, 1.0f);
                } else if (a.hasValue(com.fossil.blesdk.obfuscated.C3008u5.ColorStateListItem_alpha)) {
                    f = a.getFloat(com.fossil.blesdk.obfuscated.C3008u5.ColorStateListItem_alpha, 1.0f);
                }
                a.recycle();
                int attributeCount = attributeSet.getAttributeCount();
                int[] iArr3 = new int[attributeCount];
                int i3 = 0;
                for (int i4 = 0; i4 < attributeCount; i4++) {
                    int attributeNameResource = attributeSet2.getAttributeNameResource(i4);
                    if (!(attributeNameResource == 16843173 || attributeNameResource == 16843551 || attributeNameResource == com.fossil.blesdk.obfuscated.C2784r5.alpha)) {
                        int i5 = i3 + 1;
                        if (!attributeSet2.getAttributeBooleanValue(i4, false)) {
                            attributeNameResource = -attributeNameResource;
                        }
                        iArr3[i3] = attributeNameResource;
                        i3 = i5;
                    }
                }
                int[] trimStateSet = android.util.StateSet.trimStateSet(iArr3, i3);
                int a2 = m10554a(color, f);
                if (i2 != 0) {
                    int length = trimStateSet.length;
                }
                iArr2 = com.fossil.blesdk.obfuscated.C2706q6.m12622a(iArr2, i2, a2);
                iArr = com.fossil.blesdk.obfuscated.C2706q6.m12623a((T[]) iArr, i2, trimStateSet);
                i2++;
            }
            i = 1;
        }
        int[] iArr4 = new int[i2];
        int[][] iArr5 = new int[i2][];
        java.lang.System.arraycopy(iArr2, 0, iArr4, 0, i2);
        java.lang.System.arraycopy(iArr, 0, iArr5, 0, i2);
        return new android.content.res.ColorStateList(iArr5, iArr4);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.res.ColorStateList m10556a(android.content.res.Resources resources, org.xmlpull.v1.XmlPullParser xmlPullParser, android.util.AttributeSet attributeSet, android.content.res.Resources.Theme theme) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        java.lang.String name = xmlPullParser.getName();
        if (name.equals("selector")) {
            return m10558b(resources, xmlPullParser, attributeSet, theme);
        }
        throw new org.xmlpull.v1.XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid color state list tag " + name);
    }

    @DexIgnore
    /* renamed from: a */
    public static android.content.res.TypedArray m10557a(android.content.res.Resources resources, android.content.res.Resources.Theme theme, android.util.AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m10554a(int i, float f) {
        return (i & 16777215) | (java.lang.Math.round(((float) android.graphics.Color.alpha(i)) * f) << 24);
    }
}
