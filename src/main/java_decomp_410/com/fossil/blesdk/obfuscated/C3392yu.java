package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.yu */
public final class C3392yu implements com.fossil.blesdk.obfuscated.C2835ru {

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.Set<com.fossil.blesdk.obfuscated.C1517bw<?>> f11418e; // = java.util.Collections.newSetFromMap(new java.util.WeakHashMap());

    @DexIgnore
    /* renamed from: a */
    public void mo18346a(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar) {
        this.f11418e.add(bwVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo18347b(com.fossil.blesdk.obfuscated.C1517bw<?> bwVar) {
        this.f11418e.remove(bwVar);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo14097c() {
        for (T c : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f11418e)) {
            c.mo14097c();
        }
    }

    @DexIgnore
    /* renamed from: e */
    public void mo18348e() {
        this.f11418e.clear();
    }

    @DexIgnore
    /* renamed from: f */
    public java.util.List<com.fossil.blesdk.obfuscated.C1517bw<?>> mo18349f() {
        return com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f11418e);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14094a() {
        for (T a : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f11418e)) {
            a.mo14094a();
        }
    }

    @DexIgnore
    /* renamed from: b */
    public void mo14096b() {
        for (T b : com.fossil.blesdk.obfuscated.C3066uw.m14927a(this.f11418e)) {
            b.mo14096b();
        }
    }
}
