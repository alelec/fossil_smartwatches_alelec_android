package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.AlarmPresenter;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import dagger.internal.Factory;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class eu2 implements Factory<AlarmPresenter> {
    @DexIgnore
    public static AlarmPresenter a(yt2 yt2, String str, ArrayList<Alarm> arrayList, Alarm alarm, SetAlarms setAlarms, AlarmHelper alarmHelper, DeleteAlarm deleteAlarm, UserRepository userRepository) {
        return new AlarmPresenter(yt2, str, arrayList, alarm, setAlarms, alarmHelper, deleteAlarm, userRepository);
    }
}
