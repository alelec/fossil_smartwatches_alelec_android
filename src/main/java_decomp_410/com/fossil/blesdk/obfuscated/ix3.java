package com.fossil.blesdk.obfuscated;

import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ix3 {
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<kx3>> a;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, lx3> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ px3 d;
    @DexIgnore
    public /* final */ mx3 e;
    @DexIgnore
    public /* final */ ThreadLocal<ConcurrentLinkedQueue<c>> f;
    @DexIgnore
    public /* final */ ThreadLocal<Boolean> g;
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, Set<Class<?>>> h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ThreadLocal<ConcurrentLinkedQueue<c>> {
        @DexIgnore
        public a(ix3 ix3) {
        }

        @DexIgnore
        public ConcurrentLinkedQueue<c> initialValue() {
            return new ConcurrentLinkedQueue<>();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ThreadLocal<Boolean> {
        @DexIgnore
        public b(ix3 ix3) {
        }

        @DexIgnore
        public Boolean initialValue() {
            return false;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ kx3 b;

        @DexIgnore
        public c(Object obj, kx3 kx3) {
            this.a = obj;
            this.b = kx3;
        }
    }

    @DexIgnore
    public ix3(px3 px3) {
        this(px3, Endpoints.DEFAULT_NAME);
    }

    @DexIgnore
    public final void a(kx3 kx3, lx3 lx3) {
        try {
            Object c2 = lx3.c();
            if (c2 != null) {
                a(c2, kx3);
            }
        } catch (InvocationTargetException e2) {
            a("Producer " + lx3 + " threw an exception.", e2);
            throw null;
        }
    }

    @DexIgnore
    public void b(Object obj) {
        if (obj != null) {
            this.d.a(this);
            Map<Class<?>, lx3> b2 = this.e.b(obj);
            for (Class next : b2.keySet()) {
                lx3 lx3 = b2.get(next);
                lx3 putIfAbsent = this.b.putIfAbsent(next, lx3);
                if (putIfAbsent == null) {
                    Set<kx3> set = (Set) this.a.get(next);
                    if (set != null && !set.isEmpty()) {
                        for (kx3 a2 : set) {
                            a(a2, lx3);
                        }
                    }
                } else {
                    throw new IllegalArgumentException("Producer method for type " + next + " found on type " + lx3.a.getClass() + ", but already registered by type " + putIfAbsent.a.getClass() + CodelessMatcher.CURRENT_CLASS_NAME);
                }
            }
            Map<Class<?>, Set<kx3>> a3 = this.e.a(obj);
            for (Class next2 : a3.keySet()) {
                Set set2 = (Set) this.a.get(next2);
                if (set2 == null) {
                    set2 = new CopyOnWriteArraySet();
                    Set putIfAbsent2 = this.a.putIfAbsent(next2, set2);
                    if (putIfAbsent2 != null) {
                        set2 = putIfAbsent2;
                    }
                }
                if (!set2.addAll(a3.get(next2))) {
                    throw new IllegalArgumentException("Object already registered.");
                }
            }
            for (Map.Entry next3 : a3.entrySet()) {
                lx3 lx32 = (lx3) this.b.get((Class) next3.getKey());
                if (lx32 != null && lx32.b()) {
                    for (kx3 kx3 : (Set) next3.getValue()) {
                        if (!lx32.b()) {
                            break;
                        } else if (kx3.b()) {
                            a(kx3, lx32);
                        }
                    }
                }
            }
            return;
        }
        throw new NullPointerException("Object to register must not be null.");
    }

    @DexIgnore
    public void c(Object obj) {
        if (obj != null) {
            this.d.a(this);
            for (Map.Entry next : this.e.b(obj).entrySet()) {
                Class cls = (Class) next.getKey();
                lx3 d2 = d(cls);
                lx3 lx3 = (lx3) next.getValue();
                if (lx3 == null || !lx3.equals(d2)) {
                    throw new IllegalArgumentException("Missing event producer for an annotated method. Is " + obj.getClass() + " registered?");
                }
                ((lx3) this.b.remove(cls)).a();
            }
            for (Map.Entry next2 : this.e.a(obj).entrySet()) {
                Set<kx3> c2 = c((Class<?>) (Class) next2.getKey());
                Collection collection = (Collection) next2.getValue();
                if (c2 == null || !c2.containsAll(collection)) {
                    throw new IllegalArgumentException("Missing event handler for an annotated method. Is " + obj.getClass() + " registered?");
                }
                for (kx3 next3 : c2) {
                    if (collection.contains(next3)) {
                        next3.a();
                    }
                }
                c2.removeAll(collection);
            }
            return;
        }
        throw new NullPointerException("Object to unregister must not be null.");
    }

    @DexIgnore
    public lx3 d(Class<?> cls) {
        return (lx3) this.b.get(cls);
    }

    @DexIgnore
    public String toString() {
        return "[Bus \"" + this.c + "\"]";
    }

    @DexIgnore
    public ix3(px3 px3, String str) {
        this(px3, str, mx3.a);
    }

    @DexIgnore
    public ix3(px3 px3, String str, mx3 mx3) {
        this.a = new ConcurrentHashMap();
        this.b = new ConcurrentHashMap();
        this.f = new a(this);
        this.g = new b(this);
        this.h = new ConcurrentHashMap();
        this.d = px3;
        this.c = str;
        this.e = mx3;
    }

    @DexIgnore
    public void a(Object obj) {
        if (obj != null) {
            this.d.a(this);
            boolean z = false;
            for (Class<?> c2 : a(obj.getClass())) {
                Set<kx3> c3 = c(c2);
                if (c3 != null && !c3.isEmpty()) {
                    z = true;
                    for (kx3 b2 : c3) {
                        b(obj, b2);
                    }
                }
            }
            if (!z && !(obj instanceof jx3)) {
                a((Object) new jx3(this, obj));
            }
            a();
            return;
        }
        throw new NullPointerException("Event to post must not be null.");
    }

    @DexIgnore
    public void a() {
        if (!this.g.get().booleanValue()) {
            this.g.set(true);
            while (true) {
                try {
                    c cVar = (c) this.f.get().poll();
                    if (cVar != null) {
                        if (cVar.b.b()) {
                            a(cVar.a, cVar.b);
                        }
                    } else {
                        return;
                    }
                } finally {
                    this.g.set(false);
                }
            }
        }
    }

    @DexIgnore
    public void a(Object obj, kx3 kx3) {
        try {
            kx3.a(obj);
        } catch (InvocationTargetException e2) {
            a("Could not dispatch event: " + obj.getClass() + " to handler " + kx3, e2);
            throw null;
        }
    }

    @DexIgnore
    public Set<kx3> c(Class<?> cls) {
        return (Set) this.a.get(cls);
    }

    @DexIgnore
    public Set<Class<?>> a(Class<?> cls) {
        Set<Class<?>> set = (Set) this.h.get(cls);
        if (set != null) {
            return set;
        }
        Set<Class<?>> b2 = b(cls);
        Set<Class<?>> putIfAbsent = this.h.putIfAbsent(cls, b2);
        return putIfAbsent == null ? b2 : putIfAbsent;
    }

    @DexIgnore
    public static void a(String str, InvocationTargetException invocationTargetException) {
        Throwable cause = invocationTargetException.getCause();
        if (cause != null) {
            throw new RuntimeException(str + ": " + cause.getMessage(), cause);
        }
        throw new RuntimeException(str + ": " + invocationTargetException.getMessage(), invocationTargetException);
    }

    @DexIgnore
    public void b(Object obj, kx3 kx3) {
        this.f.get().offer(new c(obj, kx3));
    }

    @DexIgnore
    public final Set<Class<?>> b(Class<?> cls) {
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        linkedList.add(cls);
        while (!linkedList.isEmpty()) {
            Class cls2 = (Class) linkedList.remove(0);
            hashSet.add(cls2);
            Class superclass = cls2.getSuperclass();
            if (superclass != null) {
                linkedList.add(superclass);
            }
        }
        return hashSet;
    }
}
