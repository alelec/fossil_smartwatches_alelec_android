package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.il */
public interface C2036il {
    @DexIgnore
    /* renamed from: a */
    int mo12015a(androidx.work.WorkInfo.State state, java.lang.String... strArr);

    @DexIgnore
    /* renamed from: a */
    int mo12016a(java.lang.String str, long j);

    @DexIgnore
    /* renamed from: a */
    java.util.List<com.fossil.blesdk.obfuscated.C1954hl> mo12017a();

    @DexIgnore
    /* renamed from: a */
    java.util.List<com.fossil.blesdk.obfuscated.C1954hl> mo12018a(int i);

    @DexIgnore
    /* renamed from: a */
    java.util.List<com.fossil.blesdk.obfuscated.C1954hl.C1956b> mo12019a(java.lang.String str);

    @DexIgnore
    /* renamed from: a */
    void mo12020a(com.fossil.blesdk.obfuscated.C1954hl hlVar);

    @DexIgnore
    /* renamed from: a */
    void mo12021a(java.lang.String str, com.fossil.blesdk.obfuscated.C1419aj ajVar);

    @DexIgnore
    /* renamed from: b */
    java.util.List<com.fossil.blesdk.obfuscated.C1954hl> mo12022b();

    @DexIgnore
    /* renamed from: b */
    void mo12023b(java.lang.String str);

    @DexIgnore
    /* renamed from: b */
    void mo12024b(java.lang.String str, long j);

    @DexIgnore
    /* renamed from: c */
    java.util.List<java.lang.String> mo12025c();

    @DexIgnore
    /* renamed from: c */
    java.util.List<java.lang.String> mo12026c(java.lang.String str);

    @DexIgnore
    /* renamed from: d */
    int mo12027d();

    @DexIgnore
    /* renamed from: d */
    androidx.work.WorkInfo.State mo12028d(java.lang.String str);

    @DexIgnore
    /* renamed from: e */
    com.fossil.blesdk.obfuscated.C1954hl mo12029e(java.lang.String str);

    @DexIgnore
    /* renamed from: f */
    int mo12030f(java.lang.String str);

    @DexIgnore
    /* renamed from: g */
    java.util.List<com.fossil.blesdk.obfuscated.C1419aj> mo12031g(java.lang.String str);

    @DexIgnore
    /* renamed from: h */
    int mo12032h(java.lang.String str);
}
