package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.utils.EncryptionAES128;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d90 {
    @DexIgnore
    public static /* final */ EncryptionAES128.Transformation a; // = EncryptionAES128.Transformation.CTR_NO_PADDING;
    @DexIgnore
    public static /* final */ d90 b; // = new d90();

    @DexIgnore
    public final byte[] a(String str, GattCharacteristic.CharacteristicId characteristicId, EncryptionAES128.Operation operation, byte[] bArr) {
        b90 a2 = c90.b.a(str);
        byte[] a3 = a2.a();
        byte[] a4 = a2.a(characteristicId);
        if (a3 != null) {
            boolean z = false;
            if (true == (!(a3.length == 0))) {
                if (a4.length == 0) {
                    z = true;
                }
                if (!z) {
                    byte[] a5 = EncryptionAES128.a.a(operation, a, a3, a4, bArr);
                    a2.a(characteristicId, (int) ((float) Math.ceil((double) (((float) bArr.length) / ((float) 16)))));
                    return a5;
                }
            }
        }
        return bArr;
    }

    @DexIgnore
    public final byte[] b(String str, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        kd4.b(str, "macAddress");
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "data");
        return a(str, characteristicId, EncryptionAES128.Operation.ENCRYPT, bArr);
    }

    @DexIgnore
    public final byte[] a(String str, GattCharacteristic.CharacteristicId characteristicId, byte[] bArr) {
        kd4.b(str, "macAddress");
        kd4.b(characteristicId, "characteristicId");
        kd4.b(bArr, "data");
        return a(str, characteristicId, EncryptionAES128.Operation.DECRYPT, bArr);
    }
}
