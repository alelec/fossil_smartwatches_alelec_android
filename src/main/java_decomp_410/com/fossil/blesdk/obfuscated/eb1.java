package com.fossil.blesdk.obfuscated;

import com.google.android.gms.internal.measurement.zzte;
import com.google.android.gms.internal.measurement.zzuv;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class eb1<T, B> {
    @DexIgnore
    public abstract B a();

    @DexIgnore
    public abstract T a(B b);

    @DexIgnore
    public abstract void a(B b, int i, int i2);

    @DexIgnore
    public abstract void a(B b, int i, long j);

    @DexIgnore
    public abstract void a(B b, int i, zzte zzte);

    @DexIgnore
    public abstract void a(B b, int i, T t);

    @DexIgnore
    public abstract void a(T t, sb1 sb1) throws IOException;

    @DexIgnore
    public abstract void a(Object obj, T t);

    @DexIgnore
    public abstract boolean a(ma1 ma1);

    @DexIgnore
    public final boolean a(B b, ma1 ma1) throws IOException {
        int a = ma1.a();
        int i = a >>> 3;
        int i2 = a & 7;
        if (i2 == 0) {
            a(b, i, ma1.r());
            return true;
        } else if (i2 == 1) {
            b(b, i, ma1.m());
            return true;
        } else if (i2 == 2) {
            a(b, i, ma1.d());
            return true;
        } else if (i2 == 3) {
            Object a2 = a();
            int i3 = 4 | (i << 3);
            while (ma1.j() != Integer.MAX_VALUE) {
                if (!a(a2, ma1)) {
                    break;
                }
            }
            if (i3 == ma1.a()) {
                a(b, i, a(a2));
                return true;
            }
            throw zzuv.zzwt();
        } else if (i2 == 4) {
            return false;
        } else {
            if (i2 == 5) {
                a(b, i, ma1.h());
                return true;
            }
            throw zzuv.zzwu();
        }
    }

    @DexIgnore
    public abstract int b(T t);

    @DexIgnore
    public abstract void b(B b, int i, long j);

    @DexIgnore
    public abstract void b(T t, sb1 sb1) throws IOException;

    @DexIgnore
    public abstract void b(Object obj, B b);

    @DexIgnore
    public abstract T c(Object obj);

    @DexIgnore
    public abstract T c(T t, T t2);

    @DexIgnore
    public abstract B d(Object obj);

    @DexIgnore
    public abstract int e(T t);

    @DexIgnore
    public abstract void f(Object obj);
}
