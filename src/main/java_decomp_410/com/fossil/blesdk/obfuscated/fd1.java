package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class fd1 implements Parcelable.Creator<tc1> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        boolean z = false;
        ArrayList<LocationRequest> arrayList = null;
        dd1 dd1 = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                arrayList = SafeParcelReader.c(parcel, a, LocationRequest.CREATOR);
            } else if (a2 == 2) {
                z = SafeParcelReader.i(parcel, a);
            } else if (a2 == 3) {
                z2 = SafeParcelReader.i(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                dd1 = (dd1) SafeParcelReader.a(parcel, a, dd1.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new tc1(arrayList, z, z2, dd1);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new tc1[i];
    }
}
