package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Build;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class mk extends lk<gk> {
    @DexIgnore
    public mk(Context context, zl zlVar) {
        super(xk.a(context, zlVar).c());
    }

    @DexIgnore
    public boolean a(hl hlVar) {
        return hlVar.j.b() == NetworkType.CONNECTED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(gk gkVar) {
        if (Build.VERSION.SDK_INT < 26) {
            return !gkVar.a();
        }
        if (!gkVar.a() || !gkVar.d()) {
            return true;
        }
        return false;
    }
}
