package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.vw;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zp<Z> implements aq<Z>, vw.f {
    @DexIgnore
    public static /* final */ g8<zp<?>> i; // = vw.a(20, new a());
    @DexIgnore
    public /* final */ xw e; // = xw.b();
    @DexIgnore
    public aq<Z> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements vw.d<zp<?>> {
        @DexIgnore
        public zp<?> a() {
            return new zp<>();
        }
    }

    @DexIgnore
    public static <Z> zp<Z> b(aq<Z> aqVar) {
        zp<Z> a2 = i.a();
        tw.a(a2);
        zp<Z> zpVar = a2;
        zpVar.a(aqVar);
        return zpVar;
    }

    @DexIgnore
    public final void a(aq<Z> aqVar) {
        this.h = false;
        this.g = true;
        this.f = aqVar;
    }

    @DexIgnore
    public Class<Z> c() {
        return this.f.c();
    }

    @DexIgnore
    public final void d() {
        this.f = null;
        i.a(this);
    }

    @DexIgnore
    public synchronized void e() {
        this.e.a();
        if (this.g) {
            this.g = false;
            if (this.h) {
                a();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }

    @DexIgnore
    public Z get() {
        return this.f.get();
    }

    @DexIgnore
    public xw i() {
        return this.e;
    }

    @DexIgnore
    public int b() {
        return this.f.b();
    }

    @DexIgnore
    public synchronized void a() {
        this.e.a();
        this.h = true;
        if (!this.g) {
            this.f.a();
            d();
        }
    }
}
