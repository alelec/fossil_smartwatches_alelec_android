package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.le */
public class C2314le {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.util.Comparator<com.fossil.blesdk.obfuscated.C2314le.C2321g> f7191a; // = new com.fossil.blesdk.obfuscated.C2314le.C2315a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$a")
    /* renamed from: com.fossil.blesdk.obfuscated.le$a */
    public static class C2315a implements java.util.Comparator<com.fossil.blesdk.obfuscated.C2314le.C2321g> {
        @DexIgnore
        /* renamed from: a */
        public int compare(com.fossil.blesdk.obfuscated.C2314le.C2321g gVar, com.fossil.blesdk.obfuscated.C2314le.C2321g gVar2) {
            int i = gVar.f7206a - gVar2.f7206a;
            return i == 0 ? gVar.f7207b - gVar2.f7207b : i;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$b")
    /* renamed from: com.fossil.blesdk.obfuscated.le$b */
    public static abstract class C2316b {
        @DexIgnore
        /* renamed from: a */
        public abstract int mo13253a();

        @DexIgnore
        /* renamed from: a */
        public abstract boolean mo13254a(int i, int i2);

        @DexIgnore
        /* renamed from: b */
        public abstract int mo13255b();

        @DexIgnore
        /* renamed from: b */
        public abstract boolean mo13256b(int i, int i2);

        @DexIgnore
        /* renamed from: c */
        public abstract java.lang.Object mo13257c(int i, int i2);
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$c")
    /* renamed from: com.fossil.blesdk.obfuscated.le$c */
    public static class C2317c {

        @DexIgnore
        /* renamed from: a */
        public /* final */ java.util.List<com.fossil.blesdk.obfuscated.C2314le.C2321g> f7192a;

        @DexIgnore
        /* renamed from: b */
        public /* final */ int[] f7193b;

        @DexIgnore
        /* renamed from: c */
        public /* final */ int[] f7194c;

        @DexIgnore
        /* renamed from: d */
        public /* final */ com.fossil.blesdk.obfuscated.C2314le.C2316b f7195d;

        @DexIgnore
        /* renamed from: e */
        public /* final */ int f7196e;

        @DexIgnore
        /* renamed from: f */
        public /* final */ int f7197f;

        @DexIgnore
        /* renamed from: g */
        public /* final */ boolean f7198g;

        @DexIgnore
        public C2317c(com.fossil.blesdk.obfuscated.C2314le.C2316b bVar, java.util.List<com.fossil.blesdk.obfuscated.C2314le.C2321g> list, int[] iArr, int[] iArr2, boolean z) {
            this.f7192a = list;
            this.f7193b = iArr;
            this.f7194c = iArr2;
            java.util.Arrays.fill(this.f7193b, 0);
            java.util.Arrays.fill(this.f7194c, 0);
            this.f7195d = bVar;
            this.f7196e = bVar.mo13255b();
            this.f7197f = bVar.mo13253a();
            this.f7198g = z;
            mo13259a();
            mo13264b();
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo13259a() {
            com.fossil.blesdk.obfuscated.C2314le.C2321g gVar = this.f7192a.isEmpty() ? null : this.f7192a.get(0);
            if (gVar == null || gVar.f7206a != 0 || gVar.f7207b != 0) {
                com.fossil.blesdk.obfuscated.C2314le.C2321g gVar2 = new com.fossil.blesdk.obfuscated.C2314le.C2321g();
                gVar2.f7206a = 0;
                gVar2.f7207b = 0;
                gVar2.f7209d = false;
                gVar2.f7208c = 0;
                gVar2.f7210e = false;
                this.f7192a.add(0, gVar2);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo13264b() {
            int i = this.f7196e;
            int i2 = this.f7197f;
            for (int size = this.f7192a.size() - 1; size >= 0; size--) {
                com.fossil.blesdk.obfuscated.C2314le.C2321g gVar = this.f7192a.get(size);
                int i3 = gVar.f7206a;
                int i4 = gVar.f7208c;
                int i5 = i3 + i4;
                int i6 = gVar.f7207b + i4;
                if (this.f7198g) {
                    while (i > i5) {
                        mo13260a(i, i2, size);
                        i--;
                    }
                    while (i2 > i6) {
                        mo13265b(i, i2, size);
                        i2--;
                    }
                }
                for (int i7 = 0; i7 < gVar.f7208c; i7++) {
                    int i8 = gVar.f7206a + i7;
                    int i9 = gVar.f7207b + i7;
                    int i10 = this.f7195d.mo13254a(i8, i9) ? 1 : 2;
                    this.f7193b[i8] = (i9 << 5) | i10;
                    this.f7194c[i9] = (i8 << 5) | i10;
                }
                i = gVar.f7206a;
                i2 = gVar.f7207b;
            }
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo13260a(int i, int i2, int i3) {
            if (this.f7193b[i - 1] == 0) {
                mo13263a(i, i2, i3, false);
            }
        }

        @DexIgnore
        /* renamed from: a */
        public int mo13258a(int i) {
            if (i >= 0) {
                int[] iArr = this.f7193b;
                if (i < iArr.length) {
                    int i2 = iArr[i];
                    if ((i2 & 31) == 0) {
                        return -1;
                    }
                    return i2 >> 5;
                }
            }
            throw new java.lang.IndexOutOfBoundsException("Index out of bounds - passed position = " + i + ", old list size = " + this.f7193b.length);
        }

        @DexIgnore
        /* renamed from: a */
        public final boolean mo13263a(int i, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (z) {
                i2--;
                i5 = i;
                i4 = i2;
            } else {
                i5 = i - 1;
                i4 = i5;
            }
            while (i3 >= 0) {
                com.fossil.blesdk.obfuscated.C2314le.C2321g gVar = this.f7192a.get(i3);
                int i6 = gVar.f7206a;
                int i7 = gVar.f7208c;
                int i8 = i6 + i7;
                int i9 = gVar.f7207b + i7;
                int i10 = 8;
                if (z) {
                    for (int i11 = i5 - 1; i11 >= i8; i11--) {
                        if (this.f7195d.mo13256b(i11, i4)) {
                            if (!this.f7195d.mo13254a(i11, i4)) {
                                i10 = 4;
                            }
                            this.f7194c[i4] = (i11 << 5) | 16;
                            this.f7193b[i11] = (i4 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                } else {
                    for (int i12 = i2 - 1; i12 >= i9; i12--) {
                        if (this.f7195d.mo13256b(i4, i12)) {
                            if (!this.f7195d.mo13254a(i4, i12)) {
                                i10 = 4;
                            }
                            int i13 = i - 1;
                            this.f7193b[i13] = (i12 << 5) | 16;
                            this.f7194c[i12] = (i13 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                }
                i5 = gVar.f7206a;
                i2 = gVar.f7207b;
                i3--;
            }
            return false;
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo13265b(int i, int i2, int i3) {
            if (this.f7194c[i2 - 1] == 0) {
                mo13263a(i, i2, i3, true);
            }
        }

        @DexIgnore
        /* renamed from: b */
        public final void mo13266b(java.util.List<com.fossil.blesdk.obfuscated.C2314le.C2319e> list, com.fossil.blesdk.obfuscated.C3034ue ueVar, int i, int i2, int i3) {
            if (!this.f7198g) {
                ueVar.mo11202c(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.f7193b[i5] & 31;
                if (i6 == 0) {
                    ueVar.mo11202c(i + i4, 1);
                    for (com.fossil.blesdk.obfuscated.C2314le.C2319e eVar : list) {
                        eVar.f7200b--;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.f7193b[i5] >> 5;
                    com.fossil.blesdk.obfuscated.C2314le.C2319e a = m10148a(list, i7, false);
                    ueVar.mo11199a(i + i4, a.f7200b - 1);
                    if (i6 == 4) {
                        ueVar.mo11200a(a.f7200b - 1, 1, this.f7195d.mo13257c(i5, i7));
                    }
                } else if (i6 == 16) {
                    list.add(new com.fossil.blesdk.obfuscated.C2314le.C2319e(i5, i + i4, true));
                } else {
                    throw new java.lang.IllegalStateException("unknown flag for pos " + i5 + " " + java.lang.Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        /* renamed from: a */
        public void mo13261a(com.fossil.blesdk.obfuscated.C3034ue ueVar) {
            com.fossil.blesdk.obfuscated.C2016ie ieVar;
            if (ueVar instanceof com.fossil.blesdk.obfuscated.C2016ie) {
                ieVar = (com.fossil.blesdk.obfuscated.C2016ie) ueVar;
            } else {
                ieVar = new com.fossil.blesdk.obfuscated.C2016ie(ueVar);
            }
            java.util.ArrayList arrayList = new java.util.ArrayList();
            int i = this.f7196e;
            int i2 = this.f7197f;
            for (int size = this.f7192a.size() - 1; size >= 0; size--) {
                com.fossil.blesdk.obfuscated.C2314le.C2321g gVar = this.f7192a.get(size);
                int i3 = gVar.f7208c;
                int i4 = gVar.f7206a + i3;
                int i5 = gVar.f7207b + i3;
                if (i4 < i) {
                    mo13266b(arrayList, ieVar, i4, i - i4, i4);
                }
                if (i5 < i2) {
                    mo13262a(arrayList, ieVar, i4, i2 - i5, i5);
                }
                for (int i6 = i3 - 1; i6 >= 0; i6--) {
                    int[] iArr = this.f7193b;
                    int i7 = gVar.f7206a;
                    if ((iArr[i7 + i6] & 31) == 2) {
                        ieVar.mo11200a(i7 + i6, 1, this.f7195d.mo13257c(i7 + i6, gVar.f7207b + i6));
                    }
                }
                i = gVar.f7206a;
                i2 = gVar.f7207b;
            }
            ieVar.mo11922a();
        }

        @DexIgnore
        /* renamed from: a */
        public static com.fossil.blesdk.obfuscated.C2314le.C2319e m10148a(java.util.List<com.fossil.blesdk.obfuscated.C2314le.C2319e> list, int i, boolean z) {
            int size = list.size() - 1;
            while (size >= 0) {
                com.fossil.blesdk.obfuscated.C2314le.C2319e eVar = list.get(size);
                if (eVar.f7199a == i && eVar.f7201c == z) {
                    list.remove(size);
                    while (size < list.size()) {
                        list.get(size).f7200b += z ? 1 : -1;
                        size++;
                    }
                    return eVar;
                }
                size--;
            }
            return null;
        }

        @DexIgnore
        /* renamed from: a */
        public final void mo13262a(java.util.List<com.fossil.blesdk.obfuscated.C2314le.C2319e> list, com.fossil.blesdk.obfuscated.C3034ue ueVar, int i, int i2, int i3) {
            if (!this.f7198g) {
                ueVar.mo11201b(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.f7194c[i5] & 31;
                if (i6 == 0) {
                    ueVar.mo11201b(i, 1);
                    for (com.fossil.blesdk.obfuscated.C2314le.C2319e eVar : list) {
                        eVar.f7200b++;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.f7194c[i5] >> 5;
                    ueVar.mo11199a(m10148a(list, i7, true).f7200b, i);
                    if (i6 == 4) {
                        ueVar.mo11200a(i, 1, this.f7195d.mo13257c(i7, i5));
                    }
                } else if (i6 == 16) {
                    list.add(new com.fossil.blesdk.obfuscated.C2314le.C2319e(i5, i, false));
                } else {
                    throw new java.lang.IllegalStateException("unknown flag for pos " + i5 + " " + java.lang.Long.toBinaryString((long) i6));
                }
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$d")
    /* renamed from: com.fossil.blesdk.obfuscated.le$d */
    public static abstract class C2318d<T> {
        @DexIgnore
        public abstract boolean areContentsTheSame(T t, T t2);

        @DexIgnore
        public abstract boolean areItemsTheSame(T t, T t2);

        @DexIgnore
        public java.lang.Object getChangePayload(T t, T t2) {
            return null;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$e")
    /* renamed from: com.fossil.blesdk.obfuscated.le$e */
    public static class C2319e {

        @DexIgnore
        /* renamed from: a */
        public int f7199a;

        @DexIgnore
        /* renamed from: b */
        public int f7200b;

        @DexIgnore
        /* renamed from: c */
        public boolean f7201c;

        @DexIgnore
        public C2319e(int i, int i2, boolean z) {
            this.f7199a = i;
            this.f7200b = i2;
            this.f7201c = z;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$f")
    /* renamed from: com.fossil.blesdk.obfuscated.le$f */
    public static class C2320f {

        @DexIgnore
        /* renamed from: a */
        public int f7202a;

        @DexIgnore
        /* renamed from: b */
        public int f7203b;

        @DexIgnore
        /* renamed from: c */
        public int f7204c;

        @DexIgnore
        /* renamed from: d */
        public int f7205d;

        @DexIgnore
        public C2320f() {
        }

        @DexIgnore
        public C2320f(int i, int i2, int i3, int i4) {
            this.f7202a = i;
            this.f7203b = i2;
            this.f7204c = i3;
            this.f7205d = i4;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.le$g")
    /* renamed from: com.fossil.blesdk.obfuscated.le$g */
    public static class C2321g {

        @DexIgnore
        /* renamed from: a */
        public int f7206a;

        @DexIgnore
        /* renamed from: b */
        public int f7207b;

        @DexIgnore
        /* renamed from: c */
        public int f7208c;

        @DexIgnore
        /* renamed from: d */
        public boolean f7209d;

        @DexIgnore
        /* renamed from: e */
        public boolean f7210e;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2314le.C2317c m10140a(com.fossil.blesdk.obfuscated.C2314le.C2316b bVar, boolean z) {
        com.fossil.blesdk.obfuscated.C2314le.C2320f fVar;
        int b = bVar.mo13255b();
        int a = bVar.mo13253a();
        java.util.ArrayList arrayList = new java.util.ArrayList();
        java.util.ArrayList arrayList2 = new java.util.ArrayList();
        arrayList2.add(new com.fossil.blesdk.obfuscated.C2314le.C2320f(0, b, 0, a));
        int abs = java.lang.Math.abs(b - a) + b + a;
        int i = abs * 2;
        int[] iArr = new int[i];
        int[] iArr2 = new int[i];
        java.util.ArrayList arrayList3 = new java.util.ArrayList();
        while (!arrayList2.isEmpty()) {
            com.fossil.blesdk.obfuscated.C2314le.C2320f fVar2 = (com.fossil.blesdk.obfuscated.C2314le.C2320f) arrayList2.remove(arrayList2.size() - 1);
            com.fossil.blesdk.obfuscated.C2314le.C2321g a2 = m10141a(bVar, fVar2.f7202a, fVar2.f7203b, fVar2.f7204c, fVar2.f7205d, iArr, iArr2, abs);
            if (a2 != null) {
                if (a2.f7208c > 0) {
                    arrayList.add(a2);
                }
                a2.f7206a += fVar2.f7202a;
                a2.f7207b += fVar2.f7204c;
                if (arrayList3.isEmpty()) {
                    fVar = new com.fossil.blesdk.obfuscated.C2314le.C2320f();
                } else {
                    fVar = (com.fossil.blesdk.obfuscated.C2314le.C2320f) arrayList3.remove(arrayList3.size() - 1);
                }
                fVar.f7202a = fVar2.f7202a;
                fVar.f7204c = fVar2.f7204c;
                if (a2.f7210e) {
                    fVar.f7203b = a2.f7206a;
                    fVar.f7205d = a2.f7207b;
                } else if (a2.f7209d) {
                    fVar.f7203b = a2.f7206a - 1;
                    fVar.f7205d = a2.f7207b;
                } else {
                    fVar.f7203b = a2.f7206a;
                    fVar.f7205d = a2.f7207b - 1;
                }
                arrayList2.add(fVar);
                if (!a2.f7210e) {
                    int i2 = a2.f7206a;
                    int i3 = a2.f7208c;
                    fVar2.f7202a = i2 + i3;
                    fVar2.f7204c = a2.f7207b + i3;
                } else if (a2.f7209d) {
                    int i4 = a2.f7206a;
                    int i5 = a2.f7208c;
                    fVar2.f7202a = i4 + i5 + 1;
                    fVar2.f7204c = a2.f7207b + i5;
                } else {
                    int i6 = a2.f7206a;
                    int i7 = a2.f7208c;
                    fVar2.f7202a = i6 + i7;
                    fVar2.f7204c = a2.f7207b + i7 + 1;
                }
                arrayList2.add(fVar2);
            } else {
                arrayList3.add(fVar2);
            }
        }
        java.util.Collections.sort(arrayList, f7191a);
        com.fossil.blesdk.obfuscated.C2314le.C2317c cVar = new com.fossil.blesdk.obfuscated.C2314le.C2317c(bVar, arrayList, iArr, iArr2, z);
        return cVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r1[r13 - 1] < r1[(r26 + r12) + r5]) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ba, code lost:
        if (r2[r13 - 1] < r2[(r26 + r12) + 1]) goto L_0x00c7;
     */
    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2314le.C2321g m10141a(com.fossil.blesdk.obfuscated.C2314le.C2316b bVar, int i, int i2, int i3, int i4, int[] iArr, int[] iArr2, int i5) {
        boolean z;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        int i10;
        int i11;
        com.fossil.blesdk.obfuscated.C2314le.C2316b bVar2 = bVar;
        int[] iArr3 = iArr;
        int[] iArr4 = iArr2;
        int i12 = i2 - i;
        int i13 = i4 - i3;
        int i14 = 1;
        if (i12 < 1 || i13 < 1) {
            return null;
        }
        int i15 = i12 - i13;
        int i16 = ((i12 + i13) + 1) / 2;
        int i17 = (i5 - i16) - 1;
        int i18 = i5 + i16 + 1;
        java.util.Arrays.fill(iArr3, i17, i18, 0);
        java.util.Arrays.fill(iArr4, i17 + i15, i18 + i15, i12);
        boolean z3 = i15 % 2 != 0;
        int i19 = 0;
        while (i19 <= i16) {
            int i20 = -i19;
            int i21 = i20;
            while (i21 <= i19) {
                if (i21 != i20) {
                    if (i21 != i19) {
                    }
                    i10 = iArr3[(i5 + i21) - i14] + i14;
                    z2 = true;
                    i11 = i10 - i21;
                    while (i10 < i12 && i11 < i13 && bVar2.mo13256b(i + i10, i3 + i11)) {
                        i10++;
                        i11++;
                    }
                    int i22 = i5 + i21;
                    iArr3[i22] = i10;
                    if (z3 || i21 < (i15 - i19) + 1 || i21 > (i15 + i19) - 1 || iArr3[i22] < iArr4[i22]) {
                        i21 += 2;
                        i14 = 1;
                    } else {
                        com.fossil.blesdk.obfuscated.C2314le.C2321g gVar = new com.fossil.blesdk.obfuscated.C2314le.C2321g();
                        gVar.f7206a = iArr4[i22];
                        gVar.f7207b = gVar.f7206a - i21;
                        gVar.f7208c = iArr3[i22] - iArr4[i22];
                        gVar.f7209d = z2;
                        gVar.f7210e = false;
                        return gVar;
                    }
                }
                i10 = iArr3[i5 + i21 + i14];
                z2 = false;
                i11 = i10 - i21;
                while (i10 < i12) {
                    i10++;
                    i11++;
                }
                int i222 = i5 + i21;
                iArr3[i222] = i10;
                if (z3) {
                }
                i21 += 2;
                i14 = 1;
            }
            int i23 = i20;
            while (i23 <= i19) {
                int i24 = i23 + i15;
                if (i24 != i19 + i15) {
                    if (i24 != i20 + i15) {
                        i9 = 1;
                    } else {
                        i9 = 1;
                    }
                    i6 = iArr4[(i5 + i24) + i9] - i9;
                    z = true;
                    i7 = i6 - i24;
                    while (true) {
                        if (i6 > 0 && i7 > 0) {
                            i8 = i12;
                            if (!bVar2.mo13256b((i + i6) - 1, (i3 + i7) - 1)) {
                                break;
                            }
                            i6--;
                            i7--;
                            i12 = i8;
                        } else {
                            i8 = i12;
                        }
                    }
                    i8 = i12;
                    int i25 = i5 + i24;
                    iArr4[i25] = i6;
                    if (!z3 || i24 < i20 || i24 > i19 || iArr3[i25] < iArr4[i25]) {
                        i23 += 2;
                        i12 = i8;
                    } else {
                        com.fossil.blesdk.obfuscated.C2314le.C2321g gVar2 = new com.fossil.blesdk.obfuscated.C2314le.C2321g();
                        gVar2.f7206a = iArr4[i25];
                        gVar2.f7207b = gVar2.f7206a - i24;
                        gVar2.f7208c = iArr3[i25] - iArr4[i25];
                        gVar2.f7209d = z;
                        gVar2.f7210e = true;
                        return gVar2;
                    }
                } else {
                    i9 = 1;
                }
                i6 = iArr4[(i5 + i24) - i9];
                z = false;
                i7 = i6 - i24;
                while (true) {
                    if (i6 > 0) {
                        break;
                    }
                    break;
                    i6--;
                    i7--;
                    i12 = i8;
                }
                i8 = i12;
                int i252 = i5 + i24;
                iArr4[i252] = i6;
                if (!z3) {
                }
                i23 += 2;
                i12 = i8;
            }
            i19++;
            i12 = i12;
            i14 = 1;
        }
        throw new java.lang.IllegalStateException("DiffUtil hit an unexpected case while trying to calculate the optimal path. Please make sure your data is not changing during the diff calculation.");
    }
}
