package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class nb2 extends mb2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I; // = new SparseIntArray();
    @DexIgnore
    public long G;

    /*
    static {
        I.put(R.id.iv_back, 1);
        I.put(R.id.cl_container, 2);
        I.put(R.id.tv_title, 3);
        I.put(R.id.iv_email, 4);
        I.put(R.id.ftv_email_notification, 5);
        I.put(R.id.ftv_email, 6);
        I.put(R.id.cl_verification_code, 7);
        I.put(R.id.et_first_code, 8);
        I.put(R.id.et_second_code, 9);
        I.put(R.id.et_third_code, 10);
        I.put(R.id.et_fourth_code, 11);
        I.put(R.id.ftv_invalid_code, 12);
        I.put(R.id.ftv_enter_codes_guide, 13);
        I.put(R.id.ftv_resend_email, 14);
        I.put(R.id.bt_continue, 15);
    }
    */

    @DexIgnore
    public nb2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 16, H, I));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.G != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.G = 1;
        }
        g();
    }

    @DexIgnore
    public nb2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[15], objArr[2], objArr[0], objArr[7], objArr[8], objArr[11], objArr[9], objArr[10], objArr[6], objArr[5], objArr[13], objArr[12], objArr[14], objArr[1], objArr[4], objArr[3]);
        this.G = -1;
        this.s.setTag((Object) null);
        a(view);
        f();
    }
}
