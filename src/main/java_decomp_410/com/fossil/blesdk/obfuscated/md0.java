package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.fossil.blesdk.obfuscated.de0;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.clearcut.zzge$zzv$zzb;
import java.util.ArrayList;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class md0 {
    @DexIgnore
    public static /* final */ de0.g<my0> m; // = new de0.g<>();
    @DexIgnore
    public static /* final */ de0.a<my0, Object> n; // = new nd0();
    @DexIgnore
    @Deprecated
    public static /* final */ de0<Object> o; // = new de0<>("ClearcutLogger.API", n, m);
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e; // = -1;
    @DexIgnore
    public String f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public zzge$zzv$zzb h; // = zzge$zzv$zzb.zzbhk;
    @DexIgnore
    public /* final */ od0 i;
    @DexIgnore
    public /* final */ hm0 j;
    @DexIgnore
    public d k;
    @DexIgnore
    public /* final */ b l;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public zzge$zzv$zzb e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public /* final */ jy0 g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public a(md0 md0, byte[] bArr) {
            this(bArr, (c) null);
        }

        @DexIgnore
        public a(byte[] bArr, c cVar) {
            this.a = md0.this.e;
            this.b = md0.this.d;
            this.c = md0.this.f;
            md0 md0 = md0.this;
            this.d = null;
            this.e = md0.h;
            this.f = true;
            this.g = new jy0();
            this.h = false;
            this.c = md0.this.f;
            this.d = null;
            this.g.z = us0.a(md0.this.a);
            this.g.g = md0.this.j.b();
            this.g.h = md0.this.j.c();
            jy0 jy0 = this.g;
            d unused = md0.this.k;
            jy0.t = (long) (TimeZone.getDefault().getOffset(this.g.g) / 1000);
            if (bArr != null) {
                this.g.o = bArr;
            }
        }

        @DexIgnore
        public /* synthetic */ a(md0 md0, byte[] bArr, nd0 nd0) {
            this(md0, bArr);
        }

        @DexIgnore
        public void a() {
            if (!this.h) {
                this.h = true;
                rd0 rd0 = new rd0(new uy0(md0.this.b, md0.this.c, this.a, this.b, this.c, this.d, md0.this.g, this.e), this.g, (c) null, (c) null, md0.a((ArrayList<Integer>) null), (String[]) null, md0.a((ArrayList<Integer>) null), (byte[][]) null, (hm1[]) null, this.f);
                if (md0.this.l.a(rd0)) {
                    md0.this.i.a(rd0);
                } else {
                    ie0.a(Status.i, (ge0) null);
                }
            } else {
                throw new IllegalStateException("do not reuse LogEventBuilder");
            }
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        boolean a(rd0 rd0);
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        byte[] zza();
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
    }

    @DexIgnore
    public md0(Context context, int i2, String str, String str2, String str3, boolean z, od0 od0, hm0 hm0, d dVar, b bVar) {
        this.a = context;
        this.b = context.getPackageName();
        this.c = a(context);
        this.e = -1;
        this.d = str;
        this.f = str2;
        this.g = z;
        this.i = od0;
        this.j = hm0;
        this.k = new d();
        this.h = zzge$zzv$zzb.zzbhk;
        this.l = bVar;
        if (z) {
            bk0.a(str2 == null, (Object) "can't be anonymous with an upload account");
        }
    }

    @DexIgnore
    public static int a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.wtf("ClearcutLogger", "This can't happen.", e2);
            return 0;
        }
    }

    @DexIgnore
    public static md0 a(Context context, String str) {
        return new md0(context, -1, str, (String) null, (String) null, true, dw0.a(context), km0.d(), (d) null, new sy0(context));
    }

    @DexIgnore
    public static int[] a(ArrayList<Integer> arrayList) {
        if (arrayList == null) {
            return null;
        }
        int[] iArr = new int[arrayList.size()];
        int size = arrayList.size();
        int i2 = 0;
        int i3 = 0;
        while (i2 < size) {
            Integer num = arrayList.get(i2);
            i2++;
            iArr[i3] = num.intValue();
            i3++;
        }
        return iArr;
    }

    @DexIgnore
    public final a a(byte[] bArr) {
        return new a(this, bArr, (nd0) null);
    }
}
