package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.content.pm.PackageManager;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class bg1 extends ui1 {
    @DexIgnore
    public long c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Boolean e;

    @DexIgnore
    public bg1(xh1 xh1) {
        super(xh1);
    }

    @DexIgnore
    public final boolean a(Context context) {
        if (this.e == null) {
            b();
            this.e = false;
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    packageManager.getPackageInfo("com.google.android.gms", 128);
                    this.e = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return this.e.booleanValue();
    }

    @DexIgnore
    public final boolean p() {
        Calendar instance = Calendar.getInstance();
        this.c = TimeUnit.MINUTES.convert((long) (instance.get(15) + instance.get(16)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String lowerCase = locale.getLanguage().toLowerCase(Locale.ENGLISH);
        String lowerCase2 = locale.getCountry().toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder(String.valueOf(lowerCase).length() + 1 + String.valueOf(lowerCase2).length());
        sb.append(lowerCase);
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        sb.append(lowerCase2);
        this.d = sb.toString();
        return false;
    }

    @DexIgnore
    public final long s() {
        n();
        return this.c;
    }

    @DexIgnore
    public final String t() {
        n();
        return this.d;
    }
}
