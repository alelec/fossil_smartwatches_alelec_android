package com.fossil.blesdk.obfuscated;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class i61 extends vb1<i61> {
    @DexIgnore
    public static volatile i61[] i;
    @DexIgnore
    public Long c; // = null;
    @DexIgnore
    public String d; // = null;
    @DexIgnore
    public String e; // = null;
    @DexIgnore
    public Long f; // = null;
    @DexIgnore
    public Float g; // = null;
    @DexIgnore
    public Double h; // = null;

    @DexIgnore
    public i61() {
        this.b = null;
        this.a = -1;
    }

    @DexIgnore
    public static i61[] e() {
        if (i == null) {
            synchronized (zb1.b) {
                if (i == null) {
                    i = new i61[0];
                }
            }
        }
        return i;
    }

    @DexIgnore
    public final void a(ub1 ub1) throws IOException {
        Long l = this.c;
        if (l != null) {
            ub1.b(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            ub1.a(2, str);
        }
        String str2 = this.e;
        if (str2 != null) {
            ub1.a(3, str2);
        }
        Long l2 = this.f;
        if (l2 != null) {
            ub1.b(4, l2.longValue());
        }
        Float f2 = this.g;
        if (f2 != null) {
            ub1.a(5, f2.floatValue());
        }
        Double d2 = this.h;
        if (d2 != null) {
            ub1.a(6, d2.doubleValue());
        }
        super.a(ub1);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof i61)) {
            return false;
        }
        i61 i61 = (i61) obj;
        Long l = this.c;
        if (l == null) {
            if (i61.c != null) {
                return false;
            }
        } else if (!l.equals(i61.c)) {
            return false;
        }
        String str = this.d;
        if (str == null) {
            if (i61.d != null) {
                return false;
            }
        } else if (!str.equals(i61.d)) {
            return false;
        }
        String str2 = this.e;
        if (str2 == null) {
            if (i61.e != null) {
                return false;
            }
        } else if (!str2.equals(i61.e)) {
            return false;
        }
        Long l2 = this.f;
        if (l2 == null) {
            if (i61.f != null) {
                return false;
            }
        } else if (!l2.equals(i61.f)) {
            return false;
        }
        Float f2 = this.g;
        if (f2 == null) {
            if (i61.g != null) {
                return false;
            }
        } else if (!f2.equals(i61.g)) {
            return false;
        }
        Double d2 = this.h;
        if (d2 == null) {
            if (i61.h != null) {
                return false;
            }
        } else if (!d2.equals(i61.h)) {
            return false;
        }
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            return this.b.equals(i61.b);
        }
        xb1 xb12 = i61.b;
        return xb12 == null || xb12.a();
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (i61.class.getName().hashCode() + 527) * 31;
        Long l = this.c;
        int i2 = 0;
        int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
        String str = this.d;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.e;
        int hashCode4 = (hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31;
        Long l2 = this.f;
        int hashCode5 = (hashCode4 + (l2 == null ? 0 : l2.hashCode())) * 31;
        Float f2 = this.g;
        int hashCode6 = (hashCode5 + (f2 == null ? 0 : f2.hashCode())) * 31;
        Double d2 = this.h;
        int hashCode7 = (hashCode6 + (d2 == null ? 0 : d2.hashCode())) * 31;
        xb1 xb1 = this.b;
        if (xb1 != null && !xb1.a()) {
            i2 = this.b.hashCode();
        }
        return hashCode7 + i2;
    }

    @DexIgnore
    public final int a() {
        int a = super.a();
        Long l = this.c;
        if (l != null) {
            a += ub1.c(1, l.longValue());
        }
        String str = this.d;
        if (str != null) {
            a += ub1.b(2, str);
        }
        String str2 = this.e;
        if (str2 != null) {
            a += ub1.b(3, str2);
        }
        Long l2 = this.f;
        if (l2 != null) {
            a += ub1.c(4, l2.longValue());
        }
        Float f2 = this.g;
        if (f2 != null) {
            f2.floatValue();
            a += ub1.c(5) + 4;
        }
        Double d2 = this.h;
        if (d2 == null) {
            return a;
        }
        d2.doubleValue();
        return a + ub1.c(6) + 8;
    }

    @DexIgnore
    public final /* synthetic */ ac1 a(tb1 tb1) throws IOException {
        while (true) {
            int c2 = tb1.c();
            if (c2 == 0) {
                return this;
            }
            if (c2 == 8) {
                this.c = Long.valueOf(tb1.f());
            } else if (c2 == 18) {
                this.d = tb1.b();
            } else if (c2 == 26) {
                this.e = tb1.b();
            } else if (c2 == 32) {
                this.f = Long.valueOf(tb1.f());
            } else if (c2 == 45) {
                this.g = Float.valueOf(Float.intBitsToFloat(tb1.g()));
            } else if (c2 == 49) {
                this.h = Double.valueOf(Double.longBitsToDouble(tb1.h()));
            } else if (!super.a(tb1, c2)) {
                return this;
            }
        }
    }
}
