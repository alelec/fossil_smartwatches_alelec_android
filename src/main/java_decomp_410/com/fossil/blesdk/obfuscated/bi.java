package com.fossil.blesdk.obfuscated;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Log;
import android.util.Property;
import android.view.View;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bi {
    @DexIgnore
    public static /* final */ fi a;
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static /* final */ Property<View, Float> d; // = new a(Float.class, "translationAlpha");
    @DexIgnore
    public static /* final */ Property<View, Rect> e; // = new b(Rect.class, "clipBounds");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Property<View, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(bi.c(view));
        }

        @DexIgnore
        /* renamed from: a */
        public void set(View view, Float f) {
            bi.a(view, f.floatValue());
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Property<View, Rect> {
        @DexIgnore
        public b(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Rect get(View view) {
            return f9.e(view);
        }

        @DexIgnore
        /* renamed from: a */
        public void set(View view, Rect rect) {
            f9.a(view, rect);
        }
    }

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 22) {
            a = new ei();
        } else if (i >= 21) {
            a = new di();
        } else if (i >= 19) {
            a = new ci();
        } else {
            a = new fi();
        }
    }
    */

    @DexIgnore
    public static void a(View view, float f) {
        a.a(view, f);
    }

    @DexIgnore
    public static ai b(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new zh(view);
        }
        return yh.c(view);
    }

    @DexIgnore
    public static float c(View view) {
        return a.b(view);
    }

    @DexIgnore
    public static ji d(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new ii(view);
        }
        return new hi(view.getWindowToken());
    }

    @DexIgnore
    public static void e(View view) {
        a.c(view);
    }

    @DexIgnore
    public static void a(View view) {
        a.a(view);
    }

    @DexIgnore
    public static void c(View view, Matrix matrix) {
        a.c(view, matrix);
    }

    @DexIgnore
    public static void a(View view, int i) {
        a();
        Field field = b;
        if (field != null) {
            try {
                b.setInt(view, i | (field.getInt(view) & -13));
            } catch (IllegalAccessException unused) {
            }
        }
    }

    @DexIgnore
    public static void b(View view, Matrix matrix) {
        a.b(view, matrix);
    }

    @DexIgnore
    public static void a(View view, Matrix matrix) {
        a.a(view, matrix);
    }

    @DexIgnore
    public static void a(View view, int i, int i2, int i3, int i4) {
        a.a(view, i, i2, i3, i4);
    }

    @DexIgnore
    public static void a() {
        if (!c) {
            try {
                b = View.class.getDeclaredField("mViewFlags");
                b.setAccessible(true);
            } catch (NoSuchFieldException unused) {
                Log.i("ViewUtils", "fetchViewFlagsField: ");
            }
            c = true;
        }
    }
}
