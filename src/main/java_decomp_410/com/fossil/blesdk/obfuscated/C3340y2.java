package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.y2 */
public class C3340y2 extends com.fossil.blesdk.obfuscated.C2699q2 {

    @DexIgnore
    /* renamed from: b */
    public /* final */ java.lang.ref.WeakReference<android.content.Context> f11149b;

    @DexIgnore
    public C3340y2(android.content.Context context, android.content.res.Resources resources) {
        super(resources);
        this.f11149b = new java.lang.ref.WeakReference<>(context);
    }

    @DexIgnore
    public android.graphics.drawable.Drawable getDrawable(int i) throws android.content.res.Resources.NotFoundException {
        android.graphics.drawable.Drawable drawable = super.getDrawable(i);
        android.content.Context context = (android.content.Context) this.f11149b.get();
        if (!(drawable == null || context == null)) {
            com.fossil.blesdk.obfuscated.C1526c2.m5217a();
            com.fossil.blesdk.obfuscated.C1526c2.m5221a(context, i, drawable);
        }
        return drawable;
    }
}
