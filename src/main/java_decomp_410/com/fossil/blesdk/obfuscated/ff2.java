package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ff2 extends ef2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j L; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray M; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout J;
    @DexIgnore
    public long K;

    /*
    static {
        M.put(R.id.iv_close, 1);
        M.put(R.id.progressBar, 2);
        M.put(R.id.tv_title, 3);
        M.put(R.id.sv_container, 4);
        M.put(R.id.input_email, 5);
        M.put(R.id.et_email, 6);
        M.put(R.id.iv_checked_email, 7);
        M.put(R.id.input_first_name, 8);
        M.put(R.id.et_first_name, 9);
        M.put(R.id.iv_checked_first_name, 10);
        M.put(R.id.input_last_name, 11);
        M.put(R.id.et_last_name, 12);
        M.put(R.id.iv_checked_last_name, 13);
        M.put(R.id.input_birthday, 14);
        M.put(R.id.et_birthday, 15);
        M.put(R.id.iv_checked_birthday, 16);
        M.put(R.id.ftv_gender, 17);
        M.put(R.id.fb_male, 18);
        M.put(R.id.fb_female, 19);
        M.put(R.id.fb_other, 20);
        M.put(R.id.cb_terms_service, 21);
        M.put(R.id.ftv_terms_service, 22);
        M.put(R.id.cb_collect_data, 23);
        M.put(R.id.ftv_allow_gather_data, 24);
        M.put(R.id.fb_create_account, 25);
    }
    */

    @DexIgnore
    public ff2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 26, L, M));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.K = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.K != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.K = 1;
        }
        g();
    }

    @DexIgnore
    public ff2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[23], objArr[21], objArr[15], objArr[6], objArr[9], objArr[12], objArr[25], objArr[19], objArr[18], objArr[20], objArr[24], objArr[17], objArr[22], objArr[14], objArr[5], objArr[8], objArr[11], objArr[16], objArr[7], objArr[10], objArr[13], objArr[1], objArr[2], objArr[4], objArr[3]);
        this.K = -1;
        this.J = objArr[0];
        this.J.setTag((Object) null);
        a(view);
        f();
    }
}
