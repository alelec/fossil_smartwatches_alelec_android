package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.f3 */
public class C1766f3 {

    @DexIgnore
    /* renamed from: a */
    public static java.lang.reflect.Method f5007a;

    /*
    static {
        if (android.os.Build.VERSION.SDK_INT >= 18) {
            try {
                f5007a = android.view.View.class.getDeclaredMethod("computeFitSystemWindows", new java.lang.Class[]{android.graphics.Rect.class, android.graphics.Rect.class});
                if (!f5007a.isAccessible()) {
                    f5007a.setAccessible(true);
                }
            } catch (java.lang.NoSuchMethodException unused) {
                android.util.Log.d("ViewUtils", "Could not find method computeFitSystemWindows. Oh well.");
            }
        }
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static boolean m6703a(android.view.View view) {
        return com.fossil.blesdk.obfuscated.C1776f9.m6845k(view) == 1;
    }

    @DexIgnore
    /* renamed from: b */
    public static void m6704b(android.view.View view) {
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            try {
                java.lang.reflect.Method method = view.getClass().getMethod("makeOptionalFitsSystemWindows", new java.lang.Class[0]);
                if (!method.isAccessible()) {
                    method.setAccessible(true);
                }
                method.invoke(view, new java.lang.Object[0]);
            } catch (java.lang.NoSuchMethodException unused) {
                android.util.Log.d("ViewUtils", "Could not find method makeOptionalFitsSystemWindows. Oh well...");
            } catch (java.lang.reflect.InvocationTargetException e) {
                android.util.Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", e);
            } catch (java.lang.IllegalAccessException e2) {
                android.util.Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", e2);
            }
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m6702a(android.view.View view, android.graphics.Rect rect, android.graphics.Rect rect2) {
        java.lang.reflect.Method method = f5007a;
        if (method != null) {
            try {
                method.invoke(view, new java.lang.Object[]{rect, rect2});
            } catch (java.lang.Exception e) {
                android.util.Log.d("ViewUtils", "Could not invoke computeFitSystemWindows", e);
            }
        }
    }
}
