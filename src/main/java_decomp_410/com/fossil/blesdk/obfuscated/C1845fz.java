package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.fz */
public class C1845fz {

    @DexIgnore
    /* renamed from: f */
    public static /* final */ android.content.IntentFilter f5331f; // = new android.content.IntentFilter("android.intent.action.BATTERY_CHANGED");

    @DexIgnore
    /* renamed from: g */
    public static /* final */ android.content.IntentFilter f5332g; // = new android.content.IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");

    @DexIgnore
    /* renamed from: h */
    public static /* final */ android.content.IntentFilter f5333h; // = new android.content.IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");

    @DexIgnore
    /* renamed from: a */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f5334a; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.content.Context f5335b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.content.BroadcastReceiver f5336c; // = new com.fossil.blesdk.obfuscated.C1845fz.C1847b();

    @DexIgnore
    /* renamed from: d */
    public /* final */ android.content.BroadcastReceiver f5337d; // = new com.fossil.blesdk.obfuscated.C1845fz.C1846a();

    @DexIgnore
    /* renamed from: e */
    public boolean f5338e;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fz$a")
    /* renamed from: com.fossil.blesdk.obfuscated.fz$a */
    public class C1846a extends android.content.BroadcastReceiver {
        @DexIgnore
        public C1846a() {
        }

        @DexIgnore
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            boolean unused = com.fossil.blesdk.obfuscated.C1845fz.this.f5338e = true;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.fz$b")
    /* renamed from: com.fossil.blesdk.obfuscated.fz$b */
    public class C1847b extends android.content.BroadcastReceiver {
        @DexIgnore
        public C1847b() {
        }

        @DexIgnore
        public void onReceive(android.content.Context context, android.content.Intent intent) {
            boolean unused = com.fossil.blesdk.obfuscated.C1845fz.this.f5338e = false;
        }
    }

    @DexIgnore
    public C1845fz(android.content.Context context) {
        this.f5335b = context;
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11092b() {
        boolean z = true;
        if (!this.f5334a.getAndSet(true)) {
            android.content.Intent registerReceiver = this.f5335b.registerReceiver((android.content.BroadcastReceiver) null, f5331f);
            int i = -1;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("status", -1);
            }
            if (!(i == 2 || i == 5)) {
                z = false;
            }
            this.f5338e = z;
            this.f5335b.registerReceiver(this.f5337d, f5332g);
            this.f5335b.registerReceiver(this.f5336c, f5333h);
        }
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo11093c() {
        return this.f5338e;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11091a() {
        if (this.f5334a.getAndSet(false)) {
            this.f5335b.unregisterReceiver(this.f5337d);
            this.f5335b.unregisterReceiver(this.f5336c);
        }
    }
}
