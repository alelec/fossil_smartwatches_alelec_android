package com.fossil.blesdk.obfuscated;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ch2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CardView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ SwitchCompat t;

    @DexIgnore
    public ch2(Object obj, View view, int i, CardView cardView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, SwitchCompat switchCompat) {
        super(obj, view, i);
        this.q = cardView;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = switchCompat;
    }

    @DexIgnore
    public static ch2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qa.a());
    }

    @DexIgnore
    @Deprecated
    public static ch2 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (ch2) ViewDataBinding.a(layoutInflater, (int) R.layout.item_alarm, viewGroup, z, obj);
    }
}
