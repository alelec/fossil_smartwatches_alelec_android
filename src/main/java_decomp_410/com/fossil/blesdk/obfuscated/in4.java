package com.fossil.blesdk.obfuscated;

import com.facebook.GraphRequest;
import com.fossil.blesdk.obfuscated.yl4;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class in4 implements zm4 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ wm4 b;
    @DexIgnore
    public /* final */ lo4 c;
    @DexIgnore
    public /* final */ ko4 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public long f; // = 262144;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class b implements yo4 {
        @DexIgnore
        public /* final */ po4 e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public long g;

        @DexIgnore
        public b() {
            this.e = new po4(in4.this.c.b());
            this.g = 0;
        }

        @DexIgnore
        public final void a(boolean z, IOException iOException) throws IOException {
            in4 in4 = in4.this;
            int i = in4.e;
            if (i != 6) {
                if (i == 5) {
                    in4.a(this.e);
                    in4 in42 = in4.this;
                    in42.e = 6;
                    wm4 wm4 = in42.b;
                    if (wm4 != null) {
                        wm4.a(!z, in42, this.g, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + in4.this.e);
            }
        }

        @DexIgnore
        public zo4 b() {
            return this.e;
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            try {
                long b = in4.this.c.b(jo4, j);
                if (b > 0) {
                    this.g += b;
                }
                return b;
            } catch (IOException e2) {
                a(false, e2);
                throw e2;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements xo4 {
        @DexIgnore
        public /* final */ po4 e; // = new po4(in4.this.d.b());
        @DexIgnore
        public boolean f;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            if (this.f) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                in4.this.d.a(j);
                in4.this.d.a("\r\n");
                in4.this.d.a(jo4, j);
                in4.this.d.a("\r\n");
            }
        }

        @DexIgnore
        public zo4 b() {
            return this.e;
        }

        @DexIgnore
        public synchronized void close() throws IOException {
            if (!this.f) {
                this.f = true;
                in4.this.d.a("0\r\n\r\n");
                in4.this.a(this.e);
                in4.this.e = 3;
            }
        }

        @DexIgnore
        public synchronized void flush() throws IOException {
            if (!this.f) {
                in4.this.d.flush();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends b {
        @DexIgnore
        public /* final */ zl4 i;
        @DexIgnore
        public long j; // = -1;
        @DexIgnore
        public boolean k; // = true;

        @DexIgnore
        public d(zl4 zl4) {
            super();
            this.i = zl4;
        }

        @DexIgnore
        public long b(jo4 jo4, long j2) throws IOException {
            if (j2 < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j2);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (!this.k) {
                return -1;
            } else {
                long j3 = this.j;
                if (j3 == 0 || j3 == -1) {
                    c();
                    if (!this.k) {
                        return -1;
                    }
                }
                long b = super.b(jo4, Math.min(j2, this.j));
                if (b != -1) {
                    this.j -= b;
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            }
        }

        @DexIgnore
        public final void c() throws IOException {
            if (this.j != -1) {
                in4.this.c.i();
            }
            try {
                this.j = in4.this.c.l();
                String trim = in4.this.c.i().trim();
                if (this.j < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.j + trim + "\"");
                } else if (this.j == 0) {
                    this.k = false;
                    bn4.a(in4.this.a.g(), this.i, in4.this.f());
                    a(true, (IOException) null);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (this.k && !jm4.a((yo4) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, (IOException) null);
                }
                this.f = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements xo4 {
        @DexIgnore
        public /* final */ po4 e; // = new po4(in4.this.d.b());
        @DexIgnore
        public boolean f;
        @DexIgnore
        public long g;

        @DexIgnore
        public e(long j) {
            this.g = j;
        }

        @DexIgnore
        public void a(jo4 jo4, long j) throws IOException {
            if (!this.f) {
                jm4.a(jo4.B(), 0, j);
                if (j <= this.g) {
                    in4.this.d.a(jo4, j);
                    this.g -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.g + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        public zo4 b() {
            return this.e;
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                this.f = true;
                if (this.g <= 0) {
                    in4.this.a(this.e);
                    in4.this.e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            if (!this.f) {
                in4.this.d.flush();
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends b {
        @DexIgnore
        public long i;

        @DexIgnore
        public f(in4 in4, long j) throws IOException {
            super();
            this.i = j;
            if (this.i == 0) {
                a(true, (IOException) null);
            }
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!this.f) {
                long j2 = this.i;
                if (j2 == 0) {
                    return -1;
                }
                long b = super.b(jo4, Math.min(j2, j));
                if (b != -1) {
                    this.i -= b;
                    if (this.i == 0) {
                        a(true, (IOException) null);
                    }
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (this.i != 0 && !jm4.a((yo4) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, (IOException) null);
                }
                this.f = true;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends b {
        @DexIgnore
        public boolean i;

        @DexIgnore
        public g(in4 in4) {
            super();
        }

        @DexIgnore
        public long b(jo4 jo4, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.f) {
                throw new IllegalStateException("closed");
            } else if (this.i) {
                return -1;
            } else {
                long b = super.b(jo4, j);
                if (b != -1) {
                    return b;
                }
                this.i = true;
                a(true, (IOException) null);
                return -1;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.f) {
                if (!this.i) {
                    a(false, (IOException) null);
                }
                this.f = true;
            }
        }
    }

    @DexIgnore
    public in4(OkHttpClient okHttpClient, wm4 wm4, lo4 lo4, ko4 ko4) {
        this.a = okHttpClient;
        this.b = wm4;
        this.c = lo4;
        this.d = ko4;
    }

    @DexIgnore
    public xo4 a(dm4 dm4, long j) {
        if ("chunked".equalsIgnoreCase(dm4.a("Transfer-Encoding"))) {
            return c();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void b() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public xo4 c() {
        if (this.e == 1) {
            this.e = 2;
            return new c();
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void cancel() {
        tm4 c2 = this.b.c();
        if (c2 != null) {
            c2.b();
        }
    }

    @DexIgnore
    public yo4 d() throws IOException {
        if (this.e == 4) {
            wm4 wm4 = this.b;
            if (wm4 != null) {
                this.e = 5;
                wm4.e();
                return new g(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public final String e() throws IOException {
        String e2 = this.c.e(this.f);
        this.f -= (long) e2.length();
        return e2;
    }

    @DexIgnore
    public yl4 f() throws IOException {
        yl4.a aVar = new yl4.a();
        while (true) {
            String e2 = e();
            if (e2.length() == 0) {
                return aVar.a();
            }
            hm4.a.a(aVar, e2);
        }
    }

    @DexIgnore
    public yo4 b(long j) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new f(this, j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(dm4 dm4) throws IOException {
        a(dm4.c(), fn4.a(dm4, this.b.c().f().b().type()));
    }

    @DexIgnore
    public em4 a(Response response) throws IOException {
        wm4 wm4 = this.b;
        wm4.f.e(wm4.e);
        String e2 = response.e(GraphRequest.CONTENT_TYPE_HEADER);
        if (!bn4.b(response)) {
            return new en4(e2, 0, so4.a(b(0)));
        }
        if ("chunked".equalsIgnoreCase(response.e("Transfer-Encoding"))) {
            return new en4(e2, -1, so4.a(a(response.L().g())));
        }
        long a2 = bn4.a(response);
        if (a2 != -1) {
            return new en4(e2, a2, so4.a(b(a2)));
        }
        return new en4(e2, -1, so4.a(d()));
    }

    @DexIgnore
    public void a() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public void a(yl4 yl4, String str) throws IOException {
        if (this.e == 0) {
            this.d.a(str).a("\r\n");
            int b2 = yl4.b();
            for (int i = 0; i < b2; i++) {
                this.d.a(yl4.a(i)).a(": ").a(yl4.b(i)).a("\r\n");
            }
            this.d.a("\r\n");
            this.e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public Response.a a(boolean z) throws IOException {
        int i = this.e;
        if (i == 1 || i == 3) {
            try {
                hn4 a2 = hn4.a(e());
                Response.a aVar = new Response.a();
                aVar.a(a2.a);
                aVar.a(a2.b);
                aVar.a(a2.c);
                aVar.a(f());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.e = 3;
                    return aVar;
                }
                this.e = 4;
                return aVar;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.e);
        }
    }

    @DexIgnore
    public xo4 a(long j) {
        if (this.e == 1) {
            this.e = 2;
            return new e(j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public yo4 a(zl4 zl4) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new d(zl4);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(po4 po4) {
        zo4 g2 = po4.g();
        po4.a(zo4.d);
        g2.a();
        g2.b();
    }
}
