package com.fossil.blesdk.obfuscated;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class cg1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ eg1 f;

    @DexIgnore
    public cg1(xh1 xh1, String str, String str2, String str3, long j, long j2, eg1 eg1) {
        bk0.b(str2);
        bk0.b(str3);
        bk0.a(eg1);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        long j3 = this.e;
        if (j3 != 0 && j3 > this.d) {
            xh1.d().v().a("Event created with reverse previous/current timestamps. appId, name", tg1.a(str2), tg1.a(str3));
        }
        this.f = eg1;
    }

    @DexIgnore
    public final cg1 a(xh1 xh1, long j) {
        return new cg1(xh1, this.c, this.a, this.b, this.d, j, this.f);
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public cg1(xh1 xh1, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        eg1 eg1;
        bk0.b(str2);
        bk0.b(str3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        long j3 = this.e;
        if (j3 != 0 && j3 > this.d) {
            xh1.d().v().a("Event created with reverse previous/current timestamps. appId", tg1.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            eg1 = new eg1(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String str4 = (String) it.next();
                if (str4 == null) {
                    xh1.d().s().a("Param name can't be null");
                    it.remove();
                } else {
                    Object a2 = xh1.s().a(str4, bundle2.get(str4));
                    if (a2 == null) {
                        xh1.d().v().a("Param value can't be null", xh1.r().b(str4));
                        it.remove();
                    } else {
                        xh1.s().a(bundle2, str4, a2);
                    }
                }
            }
            eg1 = new eg1(bundle2);
        }
        this.f = eg1;
    }
}
