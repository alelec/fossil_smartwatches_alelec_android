package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface dg4<T> extends yb4<T> {

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* synthetic */ Object a(dg4 dg4, Object obj, Object obj2, int i, Object obj3) {
            if (obj3 == null) {
                if ((i & 2) != 0) {
                    obj2 = null;
                }
                return dg4.a(obj, obj2);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: tryResume");
        }
    }

    @DexIgnore
    Object a(T t, Object obj);

    @DexIgnore
    void a(ug4 ug4, T t);

    @DexIgnore
    void b(xc4<? super Throwable, qa4> xc4);

    @DexIgnore
    void b(Object obj);

    @DexIgnore
    boolean isActive();
}
