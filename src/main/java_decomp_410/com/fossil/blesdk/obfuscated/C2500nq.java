package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.nq */
public final class C2500nq implements com.fossil.blesdk.obfuscated.C1820fq<int[]> {
    @DexIgnore
    /* renamed from: a */
    public java.lang.String mo11011a() {
        return "IntegerArrayPool";
    }

    @DexIgnore
    /* renamed from: b */
    public int mo11012b() {
        return 4;
    }

    @DexIgnore
    /* renamed from: a */
    public int mo11010a(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public int[] newArray(int i) {
        return new int[i];
    }
}
