package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.device.logic.request.code.AuthenticationKeyType;
import com.fossil.blesdk.device.logic.request.code.AuthenticationOperationCode;
import com.fossil.blesdk.setting.JSONKey;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class m70 extends j70 {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ AuthenticationKeyType L;
    @DexIgnore
    public /* final */ byte[] M;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m70(Peripheral peripheral, AuthenticationKeyType authenticationKeyType, byte[] bArr) {
        super(peripheral, AuthenticationOperationCode.SEND_PHONE_RANDOM_NUMBER, RequestId.SEND_PHONE_RANDOM_NUMBER, 0, 8, (fd4) null);
        kd4.b(peripheral, "peripheral");
        kd4.b(authenticationKeyType, "keyType");
        kd4.b(bArr, "randomNumber");
        this.L = authenticationKeyType;
        this.M = bArr;
    }

    @DexIgnore
    public byte[] C() {
        byte[] array = ByteBuffer.allocate(this.M.length + 1).order(ByteOrder.LITTLE_ENDIAN).put(this.L.getId$blesdk_productionRelease()).put(this.M).array();
        kd4.a((Object) array, "ByteBuffer.allocate(1 + \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] I() {
        return this.K;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        kd4.b(bArr, "responseData");
        JSONObject a2 = super.a(bArr);
        if (bArr.length >= 17) {
            if (this.L.getId$blesdk_productionRelease() == bArr[0]) {
                this.K = ya4.a(bArr, 1, 17);
                wa0.a(a2, JSONKey.BOTH_SIDES_RANDOM_NUMBERS, k90.a(this.K, (String) null, 1, (Object) null));
                b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.SUCCESS, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
            } else {
                b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.WRONG_AUTHENTICATION_KEY_TYPE, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
            }
        } else {
            b(Request.Result.copy$default(n(), (RequestId) null, Request.Result.ResultCode.INVALID_RESPONSE_LENGTH, (BluetoothCommand.Result) null, (o70) null, 13, (Object) null));
        }
        c(true);
        return a2;
    }

    @DexIgnore
    public JSONObject t() {
        return wa0.a(wa0.a(super.t(), JSONKey.AUTHENTICATION_KEY_TYPE, this.L.getLogName$blesdk_productionRelease()), JSONKey.PHONE_RANDOM_NUMBER, k90.a(this.M, (String) null, 1, (Object) null));
    }

    @DexIgnore
    public JSONObject u() {
        return wa0.a(super.u(), JSONKey.BOTH_SIDES_RANDOM_NUMBERS, k90.a(this.K, (String) null, 1, (Object) null));
    }
}
