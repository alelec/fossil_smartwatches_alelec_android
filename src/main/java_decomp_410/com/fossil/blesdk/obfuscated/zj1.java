package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class zj1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qj1 e;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 f;

    @DexIgnore
    public zj1(vj1 vj1, qj1 qj1) {
        this.f = vj1;
        this.e = qj1;
    }

    @DexIgnore
    public final void run() {
        kg1 d = this.f.d;
        if (d == null) {
            this.f.d().s().a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.e == null) {
                d.a(0, (String) null, (String) null, this.f.getContext().getPackageName());
            } else {
                d.a(this.e.c, this.e.a, this.e.b, this.f.getContext().getPackageName());
            }
            this.f.C();
        } catch (RemoteException e2) {
            this.f.d().s().a("Failed to send current screen to the service", e2);
        }
    }
}
