package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ud1 extends g41 implements td1 {
    @DexIgnore
    public ud1() {
        super("com.google.android.gms.location.ILocationCallback");
    }

    @DexIgnore
    public static td1 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationCallback");
        return queryLocalInterface instanceof td1 ? (td1) queryLocalInterface : new vd1(iBinder);
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a((LocationResult) q41.a(parcel, LocationResult.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            a((LocationAvailability) q41.a(parcel, LocationAvailability.CREATOR));
        }
        return true;
    }
}
