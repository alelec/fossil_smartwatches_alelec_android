package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ui1 extends ti1 {
    @DexIgnore
    public boolean b;

    @DexIgnore
    public ui1(xh1 xh1) {
        super(xh1);
        this.a.a(this);
    }

    @DexIgnore
    public final boolean m() {
        return this.b;
    }

    @DexIgnore
    public final void n() {
        if (!m()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    @DexIgnore
    public final void o() {
        if (!this.b) {
            q();
            this.a.G();
            this.b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    @DexIgnore
    public abstract boolean p();

    @DexIgnore
    public void q() {
    }

    @DexIgnore
    public final void r() {
        if (this.b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!p()) {
            this.a.G();
            this.b = true;
        }
    }
}
