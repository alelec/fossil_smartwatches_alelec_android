package com.fossil.blesdk.obfuscated;

import android.os.IBinder;
import android.os.IInterface;
import com.fossil.blesdk.obfuscated.de0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class gk0<T extends IInterface> extends oj0<T> {
    @DexIgnore
    public /* final */ de0.h<T> E;

    @DexIgnore
    public de0.h<T> G() {
        return this.E;
    }

    @DexIgnore
    public T a(IBinder iBinder) {
        return this.E.a(iBinder);
    }

    @DexIgnore
    public int i() {
        return super.i();
    }

    @DexIgnore
    public String y() {
        return this.E.n();
    }

    @DexIgnore
    public String z() {
        return this.E.o();
    }

    @DexIgnore
    public void a(int i, T t) {
        this.E.a(i, t);
    }
}
