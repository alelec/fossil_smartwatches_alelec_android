package com.fossil.blesdk.obfuscated;

import android.annotation.SuppressLint;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ix {
    @DexIgnore
    public /* final */ g74 a;

    @DexIgnore
    public ix(g74 g74) {
        this.a = g74;
    }

    @DexIgnore
    public static ix a(Context context) {
        return new ix(new h74(context, Constants.USER_SETTING));
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public void b() {
        g74 g74 = this.a;
        g74.a(g74.edit().putBoolean("analytics_launched", true));
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public boolean a() {
        return this.a.get().getBoolean("analytics_launched", false);
    }
}
