package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class iq0 implements Parcelable.Creator<hq0> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        ArrayList<zo0> arrayList = null;
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 1) {
                arrayList = SafeParcelReader.c(parcel, a, zo0.CREATOR);
            } else if (a2 != 2) {
                SafeParcelReader.v(parcel, a);
            } else {
                status = (Status) SafeParcelReader.a(parcel, a, Status.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new hq0(arrayList, status);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new hq0[i];
    }
}
