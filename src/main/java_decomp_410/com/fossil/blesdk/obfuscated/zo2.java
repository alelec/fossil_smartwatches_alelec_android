package com.fossil.blesdk.obfuscated;

import com.google.gson.Gson;
import com.portfolio.platform.data.model.ServerError;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class zo2<T> implements er4<T> {
    @DexIgnore
    public abstract void a(Call<T> call, qr4<T> qr4);

    @DexIgnore
    public abstract void a(Call<T> call, ServerError serverError);

    @DexIgnore
    public abstract void a(Call<T> call, Throwable th);

    @DexIgnore
    public void onFailure(Call<T> call, Throwable th) {
        kd4.b(call, "call");
        kd4.b(th, "t");
        a(call, th);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        if (r0 != null) goto L_0x0073;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0090, code lost:
        if (r0 != null) goto L_0x0097;
     */
    @DexIgnore
    public void onResponse(Call<T> call, qr4<T> qr4) {
        String str;
        String str2;
        String str3;
        kd4.b(call, "call");
        kd4.b(qr4, "response");
        if (qr4.d()) {
            a(call, qr4);
            return;
        }
        int b = qr4.b();
        if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            em4 c = qr4.c();
            if (c != null) {
                str = c.F();
            }
            str = qr4.e();
            serverError.setMessage(str);
            a(call, serverError);
            return;
        }
        try {
            Gson gson = new Gson();
            em4 c2 = qr4.c();
            if (c2 != null) {
                str3 = c2.F();
                if (str3 != null) {
                    ServerError serverError2 = (ServerError) gson.a(str3, ServerError.class);
                    kd4.a((Object) serverError2, "serverError");
                    a(call, serverError2);
                }
            }
            str3 = qr4.e();
            ServerError serverError22 = (ServerError) gson.a(str3, ServerError.class);
            kd4.a((Object) serverError22, "serverError");
            a(call, serverError22);
        } catch (Exception unused) {
            ServerError serverError3 = new ServerError();
            serverError3.setCode(Integer.valueOf(b));
            em4 c3 = qr4.c();
            if (c3 != null) {
                str2 = c3.F();
            }
            str2 = qr4.e();
            serverError3.setMessage(str2);
            a(call, serverError3);
        }
    }
}
