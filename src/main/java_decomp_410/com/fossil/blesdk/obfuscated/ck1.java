package com.fossil.blesdk.obfuscated;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class ck1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ hg1 g;
    @DexIgnore
    public /* final */ /* synthetic */ rl1 h;
    @DexIgnore
    public /* final */ /* synthetic */ String i;
    @DexIgnore
    public /* final */ /* synthetic */ vj1 j;

    @DexIgnore
    public ck1(vj1 vj1, boolean z, boolean z2, hg1 hg1, rl1 rl1, String str) {
        this.j = vj1;
        this.e = z;
        this.f = z2;
        this.g = hg1;
        this.h = rl1;
        this.i = str;
    }

    @DexIgnore
    public final void run() {
        kg1 d = this.j.d;
        if (d == null) {
            this.j.d().s().a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.e) {
            this.j.a(d, this.f ? null : this.g, this.h);
        } else {
            try {
                if (TextUtils.isEmpty(this.i)) {
                    d.a(this.g, this.h);
                } else {
                    d.a(this.g, this.i, this.j.d().C());
                }
            } catch (RemoteException e2) {
                this.j.d().s().a("Failed to send event to the service", e2);
            }
        }
        this.j.C();
    }
}
