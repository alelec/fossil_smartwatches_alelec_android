package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.we */
public abstract class C3214we {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.recyclerview.widget.RecyclerView.C0236m f10613a;

    @DexIgnore
    /* renamed from: b */
    public int f10614b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ android.graphics.Rect f10615c;

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.we$a")
    /* renamed from: com.fossil.blesdk.obfuscated.we$a */
    public static class C3215a extends com.fossil.blesdk.obfuscated.C3214we {
        @DexIgnore
        public C3215a(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
            super(mVar, (com.fossil.blesdk.obfuscated.C3214we.C3215a) null);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17429a() {
            return this.f10613a.mo2971r();
        }

        @DexIgnore
        /* renamed from: b */
        public int mo17432b() {
            return this.f10613a.mo2971r() - this.f10613a.mo2969p();
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17435c(android.view.View view) {
            androidx.recyclerview.widget.RecyclerView.LayoutParams layoutParams = (androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams();
            return this.f10613a.mo2949g(view) + layoutParams.topMargin + layoutParams.bottomMargin;
        }

        @DexIgnore
        /* renamed from: d */
        public int mo17437d(android.view.View view) {
            return this.f10613a.mo2945f(view) - ((androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams()).leftMargin;
        }

        @DexIgnore
        /* renamed from: e */
        public int mo17439e(android.view.View view) {
            this.f10613a.mo2894a(view, true, this.f10615c);
            return this.f10615c.right;
        }

        @DexIgnore
        /* renamed from: f */
        public int mo17440f() {
            return this.f10613a.mo2967o();
        }

        @DexIgnore
        /* renamed from: g */
        public int mo17442g() {
            return (this.f10613a.mo2971r() - this.f10613a.mo2967o()) - this.f10613a.mo2969p();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17431a(int i) {
            this.f10613a.mo2943e(i);
        }

        @DexIgnore
        /* renamed from: b */
        public int mo17433b(android.view.View view) {
            androidx.recyclerview.widget.RecyclerView.LayoutParams layoutParams = (androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams();
            return this.f10613a.mo2953h(view) + layoutParams.leftMargin + layoutParams.rightMargin;
        }

        @DexIgnore
        /* renamed from: f */
        public int mo17441f(android.view.View view) {
            this.f10613a.mo2894a(view, true, this.f10615c);
            return this.f10615c.left;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17430a(android.view.View view) {
            return this.f10613a.mo2956i(view) + ((androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams()).rightMargin;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17434c() {
            return this.f10613a.mo2969p();
        }

        @DexIgnore
        /* renamed from: d */
        public int mo17436d() {
            return this.f10613a.mo2972s();
        }

        @DexIgnore
        /* renamed from: e */
        public int mo17438e() {
            return this.f10613a.mo2955i();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.we$b")
    /* renamed from: com.fossil.blesdk.obfuscated.we$b */
    public static class C3216b extends com.fossil.blesdk.obfuscated.C3214we {
        @DexIgnore
        public C3216b(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
            super(mVar, (com.fossil.blesdk.obfuscated.C3214we.C3215a) null);
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17429a() {
            return this.f10613a.mo2952h();
        }

        @DexIgnore
        /* renamed from: b */
        public int mo17432b() {
            return this.f10613a.mo2952h() - this.f10613a.mo2965n();
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17435c(android.view.View view) {
            androidx.recyclerview.widget.RecyclerView.LayoutParams layoutParams = (androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams();
            return this.f10613a.mo2953h(view) + layoutParams.leftMargin + layoutParams.rightMargin;
        }

        @DexIgnore
        /* renamed from: d */
        public int mo17437d(android.view.View view) {
            return this.f10613a.mo2958j(view) - ((androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams()).topMargin;
        }

        @DexIgnore
        /* renamed from: e */
        public int mo17439e(android.view.View view) {
            this.f10613a.mo2894a(view, true, this.f10615c);
            return this.f10615c.bottom;
        }

        @DexIgnore
        /* renamed from: f */
        public int mo17440f() {
            return this.f10613a.mo2970q();
        }

        @DexIgnore
        /* renamed from: g */
        public int mo17442g() {
            return (this.f10613a.mo2952h() - this.f10613a.mo2970q()) - this.f10613a.mo2965n();
        }

        @DexIgnore
        /* renamed from: a */
        public void mo17431a(int i) {
            this.f10613a.mo2946f(i);
        }

        @DexIgnore
        /* renamed from: b */
        public int mo17433b(android.view.View view) {
            androidx.recyclerview.widget.RecyclerView.LayoutParams layoutParams = (androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams();
            return this.f10613a.mo2949g(view) + layoutParams.topMargin + layoutParams.bottomMargin;
        }

        @DexIgnore
        /* renamed from: f */
        public int mo17441f(android.view.View view) {
            this.f10613a.mo2894a(view, true, this.f10615c);
            return this.f10615c.top;
        }

        @DexIgnore
        /* renamed from: a */
        public int mo17430a(android.view.View view) {
            return this.f10613a.mo2942e(view) + ((androidx.recyclerview.widget.RecyclerView.LayoutParams) view.getLayoutParams()).bottomMargin;
        }

        @DexIgnore
        /* renamed from: c */
        public int mo17434c() {
            return this.f10613a.mo2965n();
        }

        @DexIgnore
        /* renamed from: d */
        public int mo17436d() {
            return this.f10613a.mo2955i();
        }

        @DexIgnore
        /* renamed from: e */
        public int mo17438e() {
            return this.f10613a.mo2972s();
        }
    }

    @DexIgnore
    public /* synthetic */ C3214we(androidx.recyclerview.widget.RecyclerView.C0236m mVar, com.fossil.blesdk.obfuscated.C3214we.C3215a aVar) {
        this(mVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3214we m15740a(androidx.recyclerview.widget.RecyclerView.C0236m mVar, int i) {
        if (i == 0) {
            return m15739a(mVar);
        }
        if (i == 1) {
            return m15741b(mVar);
        }
        throw new java.lang.IllegalArgumentException("invalid orientation");
    }

    @DexIgnore
    /* renamed from: b */
    public static com.fossil.blesdk.obfuscated.C3214we m15741b(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        return new com.fossil.blesdk.obfuscated.C3214we.C3216b(mVar);
    }

    @DexIgnore
    /* renamed from: a */
    public abstract int mo17429a();

    @DexIgnore
    /* renamed from: a */
    public abstract int mo17430a(android.view.View view);

    @DexIgnore
    /* renamed from: a */
    public abstract void mo17431a(int i);

    @DexIgnore
    /* renamed from: b */
    public abstract int mo17432b();

    @DexIgnore
    /* renamed from: b */
    public abstract int mo17433b(android.view.View view);

    @DexIgnore
    /* renamed from: c */
    public abstract int mo17434c();

    @DexIgnore
    /* renamed from: c */
    public abstract int mo17435c(android.view.View view);

    @DexIgnore
    /* renamed from: d */
    public abstract int mo17436d();

    @DexIgnore
    /* renamed from: d */
    public abstract int mo17437d(android.view.View view);

    @DexIgnore
    /* renamed from: e */
    public abstract int mo17438e();

    @DexIgnore
    /* renamed from: e */
    public abstract int mo17439e(android.view.View view);

    @DexIgnore
    /* renamed from: f */
    public abstract int mo17440f();

    @DexIgnore
    /* renamed from: f */
    public abstract int mo17441f(android.view.View view);

    @DexIgnore
    /* renamed from: g */
    public abstract int mo17442g();

    @DexIgnore
    /* renamed from: h */
    public int mo17443h() {
        if (Integer.MIN_VALUE == this.f10614b) {
            return 0;
        }
        return mo17442g() - this.f10614b;
    }

    @DexIgnore
    /* renamed from: i */
    public void mo17444i() {
        this.f10614b = mo17442g();
    }

    @DexIgnore
    public C3214we(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        this.f10614b = Integer.MIN_VALUE;
        this.f10615c = new android.graphics.Rect();
        this.f10613a = mVar;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C3214we m15739a(androidx.recyclerview.widget.RecyclerView.C0236m mVar) {
        return new com.fossil.blesdk.obfuscated.C3214we.C3215a(mVar);
    }
}
