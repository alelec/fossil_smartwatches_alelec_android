package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.command.BluetoothCommandId;
import com.fossil.blesdk.device.core.gatt.operation.GattOperationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class d10 extends BluetoothCommand {
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public Peripheral.HIDState l; // = Peripheral.HIDState.DISCONNECTED;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d10(Peripheral.c cVar) {
        super(BluetoothCommandId.CONNECT_HID, cVar);
        kd4.b(cVar, "bluetoothGattOperationCallbackProvider");
    }

    @DexIgnore
    public void a(Peripheral peripheral) {
        kd4.b(peripheral, "peripheral");
        peripheral.b();
    }

    @DexIgnore
    public boolean b(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        if (gattOperationResult instanceof s10) {
            s10 s10 = (s10) gattOperationResult;
            if (s10.b() == Peripheral.HIDState.CONNECTED || s10.b() == Peripheral.HIDState.DISCONNECTED) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int d() {
        return this.k;
    }

    @DexIgnore
    public ra0<GattOperationResult> f() {
        return b().a();
    }

    @DexIgnore
    public final Peripheral.HIDState i() {
        return this.l;
    }

    @DexIgnore
    public void a(GattOperationResult gattOperationResult) {
        BluetoothCommand.Result result;
        kd4.b(gattOperationResult, "gattOperationResult");
        d(gattOperationResult);
        if (gattOperationResult.a().getResultCode() != GattOperationResult.GattResult.ResultCode.SUCCESS) {
            BluetoothCommand.Result a = BluetoothCommand.Result.Companion.a(gattOperationResult.a());
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, a.getResultCode(), a.getGattResult(), 1, (Object) null);
        } else if (this.l == Peripheral.HIDState.CONNECTED) {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.SUCCESS, (GattOperationResult.GattResult) null, 5, (Object) null);
        } else {
            result = BluetoothCommand.Result.copy$default(e(), (BluetoothCommandId) null, BluetoothCommand.Result.ResultCode.UNEXPECTED_RESULT, (GattOperationResult.GattResult) null, 5, (Object) null);
        }
        a(result);
    }

    @DexIgnore
    public void d(GattOperationResult gattOperationResult) {
        kd4.b(gattOperationResult, "gattOperationResult");
        this.l = ((s10) gattOperationResult).b();
    }
}
