package com.fossil.blesdk.obfuscated;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rm1 extends IInterface {
    @DexIgnore
    sn0 a(sn0 sn0, int i, int i2, String str, int i3) throws RemoteException;
}
