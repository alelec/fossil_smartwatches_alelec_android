package com.fossil.blesdk.obfuscated;

import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class sh4 extends ug4 {
    @DexIgnore
    public long e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public kj4<mh4<?>> g;

    @DexIgnore
    public static /* synthetic */ void b(sh4 sh4, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            sh4.c(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: incrementUseCount");
    }

    @DexIgnore
    public long C() {
        kj4<mh4<?>> kj4 = this.g;
        if (kj4 == null || kj4.b()) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return 0;
    }

    @DexIgnore
    public final boolean D() {
        return this.e >= b(true);
    }

    @DexIgnore
    public final boolean E() {
        kj4<mh4<?>> kj4 = this.g;
        if (kj4 != null) {
            return kj4.b();
        }
        return true;
    }

    @DexIgnore
    public long F() {
        if (!G()) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        return C();
    }

    @DexIgnore
    public final boolean G() {
        kj4<mh4<?>> kj4 = this.g;
        if (kj4 != null) {
            mh4 c = kj4.c();
            if (c != null) {
                c.run();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean H() {
        return false;
    }

    @DexIgnore
    public final void a(mh4<?> mh4) {
        kd4.b(mh4, "task");
        kj4<mh4<?>> kj4 = this.g;
        if (kj4 == null) {
            kj4 = new kj4<>();
            this.g = kj4;
        }
        kj4.a(mh4);
    }

    @DexIgnore
    public final long b(boolean z) {
        return z ? 4294967296L : 1;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.e += b(z);
        if (!z) {
            this.f = true;
        }
    }

    @DexIgnore
    public void shutdown() {
    }

    @DexIgnore
    public static /* synthetic */ void a(sh4 sh4, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                z = false;
            }
            sh4.a(z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: decrementUseCount");
    }

    @DexIgnore
    public final void a(boolean z) {
        this.e -= b(z);
        if (this.e <= 0) {
            if (ch4.a()) {
                if (!(this.e == 0)) {
                    throw new AssertionError();
                }
            }
            if (this.f) {
                shutdown();
            }
        }
    }
}
