package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.zz */
public class C3475zz {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ com.fossil.blesdk.obfuscated.C2610oy f11674a; // = com.fossil.blesdk.obfuscated.C2610oy.m12009a("0");

    @DexIgnore
    /* renamed from: b */
    public static /* final */ com.fossil.blesdk.obfuscated.C2610oy f11675b; // = com.fossil.blesdk.obfuscated.C2610oy.m12009a("Unity");

    @DexIgnore
    /* renamed from: a */
    public static void m17608a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.lang.String str, java.lang.String str2, long j) throws java.lang.Exception {
        codedOutputStream.mo4073a(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str2));
        codedOutputStream.mo4073a(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        codedOutputStream.mo4072a(3, j);
    }

    @DexIgnore
    /* renamed from: b */
    public static int m17614b(com.fossil.blesdk.obfuscated.C2610oy oyVar) {
        return com.crashlytics.android.core.CodedOutputStream.m2712b(1, oyVar) + 0;
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17610a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.lang.String str, java.lang.String str2, java.lang.String str3, java.lang.String str4, java.lang.String str5, int i, java.lang.String str6) throws java.lang.Exception {
        com.fossil.blesdk.obfuscated.C2610oy a = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str);
        com.fossil.blesdk.obfuscated.C2610oy a2 = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str2);
        com.fossil.blesdk.obfuscated.C2610oy a3 = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str3);
        com.fossil.blesdk.obfuscated.C2610oy a4 = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str4);
        com.fossil.blesdk.obfuscated.C2610oy a5 = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str5);
        com.fossil.blesdk.obfuscated.C2610oy a6 = str6 != null ? com.fossil.blesdk.obfuscated.C2610oy.m12009a(str6) : null;
        codedOutputStream.mo4084c(7, 2);
        codedOutputStream.mo4089e(m17592a(a, a2, a3, a4, a5, i, a6));
        codedOutputStream.mo4073a(1, a);
        codedOutputStream.mo4073a(2, a3);
        codedOutputStream.mo4073a(3, a4);
        codedOutputStream.mo4084c(5, 2);
        codedOutputStream.mo4089e(m17614b(a2));
        codedOutputStream.mo4073a(1, a2);
        codedOutputStream.mo4073a(6, a5);
        if (a6 != null) {
            codedOutputStream.mo4073a(8, f11675b);
            codedOutputStream.mo4073a(9, a6);
        }
        codedOutputStream.mo4071a(10, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17611a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.lang.String str, java.lang.String str2, boolean z) throws java.lang.Exception {
        com.fossil.blesdk.obfuscated.C2610oy a = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str);
        com.fossil.blesdk.obfuscated.C2610oy a2 = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str2);
        codedOutputStream.mo4084c(8, 2);
        codedOutputStream.mo4089e(m17593a(a, a2, z));
        codedOutputStream.mo4071a(1, 3);
        codedOutputStream.mo4073a(2, a);
        codedOutputStream.mo4073a(3, a2);
        codedOutputStream.mo4074a(4, z);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17601a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, int i, java.lang.String str, int i2, long j, long j2, boolean z, java.util.Map<p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType, java.lang.String> map, int i3, java.lang.String str2, java.lang.String str3) throws java.lang.Exception {
        com.crashlytics.android.core.CodedOutputStream codedOutputStream2 = codedOutputStream;
        com.fossil.blesdk.obfuscated.C2610oy a = m17599a(str);
        com.fossil.blesdk.obfuscated.C2610oy a2 = m17599a(str3);
        com.fossil.blesdk.obfuscated.C2610oy a3 = m17599a(str2);
        codedOutputStream2.mo4084c(9, 2);
        com.fossil.blesdk.obfuscated.C2610oy oyVar = a3;
        codedOutputStream2.mo4089e(m17585a(i, a, i2, j, j2, z, map, i3, a3, a2));
        codedOutputStream2.mo4071a(3, i);
        codedOutputStream2.mo4073a(4, a);
        codedOutputStream2.mo4087d(5, i2);
        codedOutputStream2.mo4072a(6, j);
        codedOutputStream2.mo4072a(7, j2);
        codedOutputStream2.mo4074a(10, z);
        for (java.util.Map.Entry next : map.entrySet()) {
            codedOutputStream2.mo4084c(11, 2);
            codedOutputStream2.mo4089e(m17594a((p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType) next.getKey(), (java.lang.String) next.getValue()));
            codedOutputStream2.mo4071a(1, ((p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType) next.getKey()).protobufIndex);
            codedOutputStream2.mo4073a(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a((java.lang.String) next.getValue()));
        }
        codedOutputStream2.mo4087d(12, i3);
        if (oyVar != null) {
            codedOutputStream2.mo4073a(13, oyVar);
        }
        if (a2 != null) {
            codedOutputStream2.mo4073a(14, a2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17609a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.lang.String str, java.lang.String str2, java.lang.String str3) throws java.lang.Exception {
        if (str == null) {
            str = "";
        }
        com.fossil.blesdk.obfuscated.C2610oy a = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str);
        com.fossil.blesdk.obfuscated.C2610oy a2 = m17599a(str2);
        com.fossil.blesdk.obfuscated.C2610oy a3 = m17599a(str3);
        int b = com.crashlytics.android.core.CodedOutputStream.m2712b(1, a) + 0;
        if (str2 != null) {
            b += com.crashlytics.android.core.CodedOutputStream.m2712b(2, a2);
        }
        if (str3 != null) {
            b += com.crashlytics.android.core.CodedOutputStream.m2712b(3, a3);
        }
        codedOutputStream.mo4084c(6, 2);
        codedOutputStream.mo4089e(b);
        codedOutputStream.mo4073a(1, a);
        if (str2 != null) {
            codedOutputStream.mo4073a(2, a2);
        }
        if (str3 != null) {
            codedOutputStream.mo4073a(3, a3);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17602a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, long j, java.lang.String str, com.fossil.blesdk.obfuscated.d00 d00, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, java.lang.Thread[] threadArr, java.util.List<java.lang.StackTraceElement[]> list, java.util.Map<java.lang.String, java.lang.String> map, com.fossil.blesdk.obfuscated.C2169jz jzVar, android.app.ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i, java.lang.String str2, java.lang.String str3, java.lang.Float f, int i2, boolean z, long j2, long j3) throws java.lang.Exception {
        com.fossil.blesdk.obfuscated.C2610oy oyVar;
        com.crashlytics.android.core.CodedOutputStream codedOutputStream2 = codedOutputStream;
        java.lang.String str4 = str3;
        com.fossil.blesdk.obfuscated.C2610oy a = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str2);
        if (str4 == null) {
            oyVar = null;
        } else {
            oyVar = com.fossil.blesdk.obfuscated.C2610oy.m12009a(str4.replace(com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, ""));
        }
        com.fossil.blesdk.obfuscated.C2610oy oyVar2 = oyVar;
        com.fossil.blesdk.obfuscated.C2610oy b = jzVar.mo12518b();
        if (b == null) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "No log data to include with this event.");
        }
        jzVar.mo12514a();
        codedOutputStream2.mo4084c(10, 2);
        codedOutputStream2.mo4089e(m17586a(j, str, d00, thread, stackTraceElementArr, threadArr, list, 8, map, runningAppProcessInfo, i, a, oyVar2, f, i2, z, j2, j3, b));
        codedOutputStream2.mo4072a(1, j);
        codedOutputStream2.mo4073a(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        m17605a(codedOutputStream, d00, thread, stackTraceElementArr, threadArr, list, 8, a, oyVar2, map, runningAppProcessInfo, i);
        m17607a(codedOutputStream, f, i2, z, i, j2, j3);
        m17606a(codedOutputStream2, b);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17605a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, com.fossil.blesdk.obfuscated.d00 d00, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, java.lang.Thread[] threadArr, java.util.List<java.lang.StackTraceElement[]> list, int i, com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2, java.util.Map<java.lang.String, java.lang.String> map, android.app.ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) throws java.lang.Exception {
        codedOutputStream.mo4084c(3, 2);
        codedOutputStream.mo4089e(m17589a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2, map, runningAppProcessInfo, i2));
        m17604a(codedOutputStream, d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2);
        if (map != null && !map.isEmpty()) {
            m17613a(codedOutputStream, map);
        }
        if (runningAppProcessInfo != null) {
            codedOutputStream.mo4074a(3, runningAppProcessInfo.importance != 100);
        }
        codedOutputStream.mo4087d(4, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17604a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, com.fossil.blesdk.obfuscated.d00 d00, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, java.lang.Thread[] threadArr, java.util.List<java.lang.StackTraceElement[]> list, int i, com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2) throws java.lang.Exception {
        codedOutputStream.mo4084c(1, 2);
        codedOutputStream.mo4089e(m17588a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2));
        m17612a(codedOutputStream, thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            m17612a(codedOutputStream, threadArr[i2], list.get(i2), 0, false);
        }
        m17603a(codedOutputStream, d00, 1, i, 2);
        codedOutputStream.mo4084c(3, 2);
        codedOutputStream.mo4089e(m17584a());
        codedOutputStream.mo4073a(1, f11674a);
        codedOutputStream.mo4073a(2, f11674a);
        codedOutputStream.mo4072a(3, 0);
        codedOutputStream.mo4084c(4, 2);
        codedOutputStream.mo4089e(m17591a(oyVar, oyVar2));
        codedOutputStream.mo4072a(1, 0);
        codedOutputStream.mo4072a(2, 0);
        codedOutputStream.mo4073a(3, oyVar);
        if (oyVar2 != null) {
            codedOutputStream.mo4073a(4, oyVar2);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17613a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.util.Map<java.lang.String, java.lang.String> map) throws java.lang.Exception {
        for (java.util.Map.Entry next : map.entrySet()) {
            codedOutputStream.mo4084c(2, 2);
            codedOutputStream.mo4089e(m17597a((java.lang.String) next.getKey(), (java.lang.String) next.getValue()));
            codedOutputStream.mo4073a(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a((java.lang.String) next.getKey()));
            java.lang.String str = (java.lang.String) next.getValue();
            if (str == null) {
                str = "";
            }
            codedOutputStream.mo4073a(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17603a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, com.fossil.blesdk.obfuscated.d00 d00, int i, int i2, int i3) throws java.lang.Exception {
        codedOutputStream.mo4084c(i3, 2);
        codedOutputStream.mo4089e(m17587a(d00, 1, i2));
        codedOutputStream.mo4073a(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a(d00.f4230b));
        java.lang.String str = d00.f4229a;
        if (str != null) {
            codedOutputStream.mo4073a(3, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        }
        int i4 = 0;
        for (java.lang.StackTraceElement a : d00.f4231c) {
            m17600a(codedOutputStream, 4, a, true);
        }
        com.fossil.blesdk.obfuscated.d00 d002 = d00.f4232d;
        if (d002 == null) {
            return;
        }
        if (i < i2) {
            m17603a(codedOutputStream, d002, i + 1, i2, 6);
            return;
        }
        while (d002 != null) {
            d002 = d002.f4232d;
            i4++;
        }
        codedOutputStream.mo4087d(7, i4);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17612a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, int i, boolean z) throws java.lang.Exception {
        codedOutputStream.mo4084c(1, 2);
        codedOutputStream.mo4089e(m17598a(thread, stackTraceElementArr, i, z));
        codedOutputStream.mo4073a(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a(thread.getName()));
        codedOutputStream.mo4087d(2, i);
        for (java.lang.StackTraceElement a : stackTraceElementArr) {
            m17600a(codedOutputStream, 3, a, z);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17600a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, int i, java.lang.StackTraceElement stackTraceElement, boolean z) throws java.lang.Exception {
        codedOutputStream.mo4084c(i, 2);
        codedOutputStream.mo4089e(m17596a(stackTraceElement, z));
        if (stackTraceElement.isNativeMethod()) {
            codedOutputStream.mo4072a(1, (long) java.lang.Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            codedOutputStream.mo4072a(1, 0);
        }
        codedOutputStream.mo4073a(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(stackTraceElement.getClassName() + com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            codedOutputStream.mo4073a(3, com.fossil.blesdk.obfuscated.C2610oy.m12009a(stackTraceElement.getFileName()));
        }
        int i2 = 4;
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            codedOutputStream.mo4072a(4, (long) stackTraceElement.getLineNumber());
        }
        if (!z) {
            i2 = 0;
        }
        codedOutputStream.mo4087d(5, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17607a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, java.lang.Float f, int i, boolean z, int i2, long j, long j2) throws java.lang.Exception {
        codedOutputStream.mo4084c(5, 2);
        codedOutputStream.mo4089e(m17595a(f, i, z, i2, j, j2));
        if (f != null) {
            codedOutputStream.mo4070a(1, f.floatValue());
        }
        codedOutputStream.mo4081b(2, i);
        codedOutputStream.mo4074a(3, z);
        codedOutputStream.mo4087d(4, i2);
        codedOutputStream.mo4072a(5, j);
        codedOutputStream.mo4072a(6, j2);
    }

    @DexIgnore
    /* renamed from: a */
    public static void m17606a(com.crashlytics.android.core.CodedOutputStream codedOutputStream, com.fossil.blesdk.obfuscated.C2610oy oyVar) throws java.lang.Exception {
        if (oyVar != null) {
            codedOutputStream.mo4084c(6, 2);
            codedOutputStream.mo4089e(m17590a(oyVar));
            codedOutputStream.mo4073a(1, oyVar);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17592a(com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2, com.fossil.blesdk.obfuscated.C2610oy oyVar3, com.fossil.blesdk.obfuscated.C2610oy oyVar4, com.fossil.blesdk.obfuscated.C2610oy oyVar5, int i, com.fossil.blesdk.obfuscated.C2610oy oyVar6) {
        int b = m17614b(oyVar2);
        int b2 = com.crashlytics.android.core.CodedOutputStream.m2712b(1, oyVar) + 0 + com.crashlytics.android.core.CodedOutputStream.m2712b(2, oyVar3) + com.crashlytics.android.core.CodedOutputStream.m2712b(3, oyVar4) + com.crashlytics.android.core.CodedOutputStream.m2725l(5) + com.crashlytics.android.core.CodedOutputStream.m2723j(b) + b + com.crashlytics.android.core.CodedOutputStream.m2712b(6, oyVar5);
        if (oyVar6 != null) {
            b2 = b2 + com.crashlytics.android.core.CodedOutputStream.m2712b(8, f11675b) + com.crashlytics.android.core.CodedOutputStream.m2712b(9, oyVar6);
        }
        return b2 + com.crashlytics.android.core.CodedOutputStream.m2716e(10, i);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17593a(com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2, boolean z) {
        return com.crashlytics.android.core.CodedOutputStream.m2716e(1, 3) + 0 + com.crashlytics.android.core.CodedOutputStream.m2712b(2, oyVar) + com.crashlytics.android.core.CodedOutputStream.m2712b(3, oyVar2) + com.crashlytics.android.core.CodedOutputStream.m2713b(4, z);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17594a(p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType deviceIdentifierType, java.lang.String str) {
        return com.crashlytics.android.core.CodedOutputStream.m2716e(1, deviceIdentifierType.protobufIndex) + com.crashlytics.android.core.CodedOutputStream.m2712b(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17585a(int i, com.fossil.blesdk.obfuscated.C2610oy oyVar, int i2, long j, long j2, boolean z, java.util.Map<p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType, java.lang.String> map, int i3, com.fossil.blesdk.obfuscated.C2610oy oyVar2, com.fossil.blesdk.obfuscated.C2610oy oyVar3) {
        int i4;
        int i5;
        int i6 = 0;
        int e = com.crashlytics.android.core.CodedOutputStream.m2716e(3, i) + 0;
        if (oyVar == null) {
            i4 = 0;
        } else {
            i4 = com.crashlytics.android.core.CodedOutputStream.m2712b(4, oyVar);
        }
        int g = e + i4 + com.crashlytics.android.core.CodedOutputStream.m2720g(5, i2) + com.crashlytics.android.core.CodedOutputStream.m2711b(6, j) + com.crashlytics.android.core.CodedOutputStream.m2711b(7, j2) + com.crashlytics.android.core.CodedOutputStream.m2713b(10, z);
        if (map != null) {
            for (java.util.Map.Entry next : map.entrySet()) {
                int a = m17594a((p011io.fabric.sdk.android.services.common.IdManager.DeviceIdentifierType) next.getKey(), (java.lang.String) next.getValue());
                g += com.crashlytics.android.core.CodedOutputStream.m2725l(11) + com.crashlytics.android.core.CodedOutputStream.m2723j(a) + a;
            }
        }
        int g2 = g + com.crashlytics.android.core.CodedOutputStream.m2720g(12, i3);
        if (oyVar2 == null) {
            i5 = 0;
        } else {
            i5 = com.crashlytics.android.core.CodedOutputStream.m2712b(13, oyVar2);
        }
        int i7 = g2 + i5;
        if (oyVar3 != null) {
            i6 = com.crashlytics.android.core.CodedOutputStream.m2712b(14, oyVar3);
        }
        return i7 + i6;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17591a(com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2) {
        int b = com.crashlytics.android.core.CodedOutputStream.m2711b(1, 0) + 0 + com.crashlytics.android.core.CodedOutputStream.m2711b(2, 0) + com.crashlytics.android.core.CodedOutputStream.m2712b(3, oyVar);
        return oyVar2 != null ? b + com.crashlytics.android.core.CodedOutputStream.m2712b(4, oyVar2) : b;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17586a(long j, java.lang.String str, com.fossil.blesdk.obfuscated.d00 d00, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, java.lang.Thread[] threadArr, java.util.List<java.lang.StackTraceElement[]> list, int i, java.util.Map<java.lang.String, java.lang.String> map, android.app.ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2, com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2, java.lang.Float f, int i3, boolean z, long j2, long j3, com.fossil.blesdk.obfuscated.C2610oy oyVar3) {
        long j4 = j;
        int b = com.crashlytics.android.core.CodedOutputStream.m2711b(1, j) + 0 + com.crashlytics.android.core.CodedOutputStream.m2712b(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        int a = m17589a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2, map, runningAppProcessInfo, i2);
        int a2 = m17595a(f, i3, z, i2, j2, j3);
        int l = b + com.crashlytics.android.core.CodedOutputStream.m2725l(3) + com.crashlytics.android.core.CodedOutputStream.m2723j(a) + a + com.crashlytics.android.core.CodedOutputStream.m2725l(5) + com.crashlytics.android.core.CodedOutputStream.m2723j(a2) + a2;
        if (oyVar3 == null) {
            return l;
        }
        int a3 = m17590a(oyVar3);
        return l + com.crashlytics.android.core.CodedOutputStream.m2725l(6) + com.crashlytics.android.core.CodedOutputStream.m2723j(a3) + a3;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17589a(com.fossil.blesdk.obfuscated.d00 d00, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, java.lang.Thread[] threadArr, java.util.List<java.lang.StackTraceElement[]> list, int i, com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2, java.util.Map<java.lang.String, java.lang.String> map, android.app.ActivityManager.RunningAppProcessInfo runningAppProcessInfo, int i2) {
        int a = m17588a(d00, thread, stackTraceElementArr, threadArr, list, i, oyVar, oyVar2);
        int l = com.crashlytics.android.core.CodedOutputStream.m2725l(1) + com.crashlytics.android.core.CodedOutputStream.m2723j(a) + a;
        boolean z = false;
        int i3 = l + 0;
        if (map != null) {
            for (java.util.Map.Entry next : map.entrySet()) {
                int a2 = m17597a((java.lang.String) next.getKey(), (java.lang.String) next.getValue());
                i3 += com.crashlytics.android.core.CodedOutputStream.m2725l(2) + com.crashlytics.android.core.CodedOutputStream.m2723j(a2) + a2;
            }
        }
        if (runningAppProcessInfo != null) {
            if (runningAppProcessInfo.importance != 100) {
                z = true;
            }
            i3 += com.crashlytics.android.core.CodedOutputStream.m2713b(3, z);
        }
        return i3 + com.crashlytics.android.core.CodedOutputStream.m2720g(4, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17588a(com.fossil.blesdk.obfuscated.d00 d00, java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, java.lang.Thread[] threadArr, java.util.List<java.lang.StackTraceElement[]> list, int i, com.fossil.blesdk.obfuscated.C2610oy oyVar, com.fossil.blesdk.obfuscated.C2610oy oyVar2) {
        int a = m17598a(thread, stackTraceElementArr, 4, true);
        int length = threadArr.length;
        int l = com.crashlytics.android.core.CodedOutputStream.m2725l(1) + com.crashlytics.android.core.CodedOutputStream.m2723j(a) + a + 0;
        for (int i2 = 0; i2 < length; i2++) {
            int a2 = m17598a(threadArr[i2], list.get(i2), 0, false);
            l += com.crashlytics.android.core.CodedOutputStream.m2725l(1) + com.crashlytics.android.core.CodedOutputStream.m2723j(a2) + a2;
        }
        int a3 = m17587a(d00, 1, i);
        int a4 = m17584a();
        int a5 = m17591a(oyVar, oyVar2);
        return l + com.crashlytics.android.core.CodedOutputStream.m2725l(2) + com.crashlytics.android.core.CodedOutputStream.m2723j(a3) + a3 + com.crashlytics.android.core.CodedOutputStream.m2725l(3) + com.crashlytics.android.core.CodedOutputStream.m2723j(a4) + a4 + com.crashlytics.android.core.CodedOutputStream.m2725l(3) + com.crashlytics.android.core.CodedOutputStream.m2723j(a5) + a5;
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17597a(java.lang.String str, java.lang.String str2) {
        int b = com.crashlytics.android.core.CodedOutputStream.m2712b(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        if (str2 == null) {
            str2 = "";
        }
        return b + com.crashlytics.android.core.CodedOutputStream.m2712b(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str2));
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17595a(java.lang.Float f, int i, boolean z, int i2, long j, long j2) {
        int i3 = 0;
        if (f != null) {
            i3 = 0 + com.crashlytics.android.core.CodedOutputStream.m2710b(1, f.floatValue());
        }
        return i3 + com.crashlytics.android.core.CodedOutputStream.m2718f(2, i) + com.crashlytics.android.core.CodedOutputStream.m2713b(3, z) + com.crashlytics.android.core.CodedOutputStream.m2720g(4, i2) + com.crashlytics.android.core.CodedOutputStream.m2711b(5, j) + com.crashlytics.android.core.CodedOutputStream.m2711b(6, j2);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17590a(com.fossil.blesdk.obfuscated.C2610oy oyVar) {
        return com.crashlytics.android.core.CodedOutputStream.m2712b(1, oyVar);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17587a(com.fossil.blesdk.obfuscated.d00 d00, int i, int i2) {
        int i3 = 0;
        int b = com.crashlytics.android.core.CodedOutputStream.m2712b(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a(d00.f4230b)) + 0;
        java.lang.String str = d00.f4229a;
        if (str != null) {
            b += com.crashlytics.android.core.CodedOutputStream.m2712b(3, com.fossil.blesdk.obfuscated.C2610oy.m12009a(str));
        }
        int i4 = b;
        for (java.lang.StackTraceElement a : d00.f4231c) {
            int a2 = m17596a(a, true);
            i4 += com.crashlytics.android.core.CodedOutputStream.m2725l(4) + com.crashlytics.android.core.CodedOutputStream.m2723j(a2) + a2;
        }
        com.fossil.blesdk.obfuscated.d00 d002 = d00.f4232d;
        if (d002 == null) {
            return i4;
        }
        if (i < i2) {
            int a3 = m17587a(d002, i + 1, i2);
            return i4 + com.crashlytics.android.core.CodedOutputStream.m2725l(6) + com.crashlytics.android.core.CodedOutputStream.m2723j(a3) + a3;
        }
        while (d002 != null) {
            d002 = d002.f4232d;
            i3++;
        }
        return i4 + com.crashlytics.android.core.CodedOutputStream.m2720g(7, i3);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17584a() {
        return com.crashlytics.android.core.CodedOutputStream.m2712b(1, f11674a) + 0 + com.crashlytics.android.core.CodedOutputStream.m2712b(2, f11674a) + com.crashlytics.android.core.CodedOutputStream.m2711b(3, 0);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17596a(java.lang.StackTraceElement stackTraceElement, boolean z) {
        int i;
        int i2 = 0;
        if (stackTraceElement.isNativeMethod()) {
            i = com.crashlytics.android.core.CodedOutputStream.m2711b(1, (long) java.lang.Math.max(stackTraceElement.getLineNumber(), 0));
        } else {
            i = com.crashlytics.android.core.CodedOutputStream.m2711b(1, 0);
        }
        int b = i + 0 + com.crashlytics.android.core.CodedOutputStream.m2712b(2, com.fossil.blesdk.obfuscated.C2610oy.m12009a(stackTraceElement.getClassName() + com.facebook.appevents.codeless.CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName()));
        if (stackTraceElement.getFileName() != null) {
            b += com.crashlytics.android.core.CodedOutputStream.m2712b(3, com.fossil.blesdk.obfuscated.C2610oy.m12009a(stackTraceElement.getFileName()));
        }
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            b += com.crashlytics.android.core.CodedOutputStream.m2711b(4, (long) stackTraceElement.getLineNumber());
        }
        if (z) {
            i2 = 2;
        }
        return b + com.crashlytics.android.core.CodedOutputStream.m2720g(5, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public static int m17598a(java.lang.Thread thread, java.lang.StackTraceElement[] stackTraceElementArr, int i, boolean z) {
        int b = com.crashlytics.android.core.CodedOutputStream.m2712b(1, com.fossil.blesdk.obfuscated.C2610oy.m12009a(thread.getName())) + com.crashlytics.android.core.CodedOutputStream.m2720g(2, i);
        for (java.lang.StackTraceElement a : stackTraceElementArr) {
            int a2 = m17596a(a, z);
            b += com.crashlytics.android.core.CodedOutputStream.m2725l(3) + com.crashlytics.android.core.CodedOutputStream.m2723j(a2) + a2;
        }
        return b;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2610oy m17599a(java.lang.String str) {
        if (str == null) {
            return null;
        }
        return com.fossil.blesdk.obfuscated.C2610oy.m12009a(str);
    }
}
