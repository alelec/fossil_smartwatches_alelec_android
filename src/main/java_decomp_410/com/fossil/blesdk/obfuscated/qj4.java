package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class qj4 extends sj4 {
    @DexIgnore
    public final boolean j() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean m() {
        return c() == this;
    }
}
