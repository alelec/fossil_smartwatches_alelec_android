package com.fossil.blesdk.obfuscated;

import android.content.Context;
import com.crashlytics.android.answers.SamplingEventFilter;
import com.crashlytics.android.answers.SessionEvent;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class ox implements by {
    @DexIgnore
    public /* final */ v44 a;
    @DexIgnore
    public /* final */ z64 b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ yx d;
    @DexIgnore
    public /* final */ ScheduledExecutorService e;
    @DexIgnore
    public /* final */ AtomicReference<ScheduledFuture<?>> f; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ cy g;
    @DexIgnore
    public /* final */ rx h;
    @DexIgnore
    public t64 i;
    @DexIgnore
    public k54 j; // = new k54();
    @DexIgnore
    public px k; // = new ux();
    @DexIgnore
    public boolean l; // = true;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public volatile int n; // = -1;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public ox(v44 v44, Context context, ScheduledExecutorService scheduledExecutorService, yx yxVar, z64 z64, cy cyVar, rx rxVar) {
        this.a = v44;
        this.c = context;
        this.e = scheduledExecutorService;
        this.d = yxVar;
        this.b = z64;
        this.g = cyVar;
        this.h = rxVar;
    }

    @DexIgnore
    public void a(j74 j74, String str) {
        String str2;
        String str3;
        this.i = jx.a(new zx(this.a, str, j74.a, this.b, this.j.d(this.c)));
        this.d.a(j74);
        this.o = j74.e;
        this.p = j74.f;
        y44 g2 = q44.g();
        StringBuilder sb = new StringBuilder();
        sb.append("Firebase analytics forwarding ");
        String str4 = "enabled";
        sb.append(this.o ? str4 : "disabled");
        g2.d("Answers", sb.toString());
        y44 g3 = q44.g();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Firebase analytics including purchase events ");
        if (this.p) {
            str2 = str4;
        } else {
            str2 = "disabled";
        }
        sb2.append(str2);
        g3.d("Answers", sb2.toString());
        this.l = j74.g;
        y44 g4 = q44.g();
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Custom event tracking ");
        if (this.l) {
            str3 = str4;
        } else {
            str3 = "disabled";
        }
        sb3.append(str3);
        g4.d("Answers", sb3.toString());
        this.m = j74.h;
        y44 g5 = q44.g();
        StringBuilder sb4 = new StringBuilder();
        sb4.append("Predefined event tracking ");
        if (!this.m) {
            str4 = "disabled";
        }
        sb4.append(str4);
        g5.d("Answers", sb4.toString());
        if (j74.j > 1) {
            q44.g().d("Answers", "Event sampling enabled");
            this.k = new SamplingEventFilter(j74.j);
        }
        this.n = j74.b;
        a(0, (long) this.n);
    }

    @DexIgnore
    public boolean b() {
        try {
            return this.d.g();
        } catch (IOException e2) {
            CommonUtils.a(this.c, "Failed to roll file over.", (Throwable) e2);
            return false;
        }
    }

    @DexIgnore
    public void c() {
        if (this.f.get() != null) {
            CommonUtils.c(this.c, "Cancelling time-based rollover because no events are currently being generated.");
            this.f.get().cancel(false);
            this.f.set((Object) null);
        }
    }

    @DexIgnore
    public void d() {
        this.d.a();
    }

    @DexIgnore
    public void e() {
        if (this.n != -1) {
            a((long) this.n, (long) this.n);
        }
    }

    @DexIgnore
    public void a(SessionEvent.b bVar) {
        SessionEvent a2 = bVar.a(this.g);
        if (!this.l && SessionEvent.Type.CUSTOM.equals(a2.c)) {
            y44 g2 = q44.g();
            g2.d("Answers", "Custom events tracking disabled - skipping event: " + a2);
        } else if (!this.m && SessionEvent.Type.PREDEFINED.equals(a2.c)) {
            y44 g3 = q44.g();
            g3.d("Answers", "Predefined events tracking disabled - skipping event: " + a2);
        } else if (this.k.a(a2)) {
            y44 g4 = q44.g();
            g4.d("Answers", "Skipping filtered event: " + a2);
        } else {
            try {
                this.d.a(a2);
            } catch (IOException e2) {
                y44 g5 = q44.g();
                g5.e("Answers", "Failed to write event: " + a2, e2);
            }
            e();
            boolean z = SessionEvent.Type.CUSTOM.equals(a2.c) || SessionEvent.Type.PREDEFINED.equals(a2.c);
            boolean equals = "purchase".equals(a2.g);
            if (this.o && z) {
                if (!equals || this.p) {
                    try {
                        this.h.a(a2);
                    } catch (Exception e3) {
                        y44 g6 = q44.g();
                        g6.e("Answers", "Failed to map event to Firebase: " + a2, e3);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a() {
        if (this.i == null) {
            CommonUtils.c(this.c, "skipping files send because we don't yet know the target endpoint");
            return;
        }
        CommonUtils.c(this.c, "Sending all files");
        List<File> d2 = this.d.d();
        int i2 = 0;
        while (true) {
            try {
                if (d2.size() <= 0) {
                    break;
                }
                CommonUtils.c(this.c, String.format(Locale.US, "attempt to send batch of %d files", new Object[]{Integer.valueOf(d2.size())}));
                boolean a2 = this.i.a(d2);
                if (a2) {
                    i2 += d2.size();
                    this.d.a(d2);
                }
                if (!a2) {
                    break;
                }
                d2 = this.d.d();
            } catch (Exception e2) {
                Context context = this.c;
                CommonUtils.a(context, "Failed to send batch of analytics files to server: " + e2.getMessage(), (Throwable) e2);
            }
        }
        if (i2 == 0) {
            this.d.b();
        }
    }

    @DexIgnore
    public void a(long j2, long j3) {
        if (this.f.get() == null) {
            w64 w64 = new w64(this.c, this);
            Context context = this.c;
            CommonUtils.c(context, "Scheduling time based file roll over every " + j3 + " seconds");
            try {
                this.f.set(this.e.scheduleAtFixedRate(w64, j2, j3, TimeUnit.SECONDS));
            } catch (RejectedExecutionException e2) {
                CommonUtils.a(this.c, "Failed to schedule time based file roll over", (Throwable) e2);
            }
        }
    }
}
