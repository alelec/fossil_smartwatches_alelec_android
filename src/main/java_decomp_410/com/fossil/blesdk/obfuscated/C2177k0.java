package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.k0 */
public class C2177k0 {

    @DexIgnore
    /* renamed from: d */
    public static com.fossil.blesdk.obfuscated.C2177k0 f6642d;

    @DexIgnore
    /* renamed from: a */
    public /* final */ android.content.Context f6643a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ android.location.LocationManager f6644b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ com.fossil.blesdk.obfuscated.C2177k0.C2178a f6645c; // = new com.fossil.blesdk.obfuscated.C2177k0.C2178a();

    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.k0$a")
    /* renamed from: com.fossil.blesdk.obfuscated.k0$a */
    public static class C2178a {

        @DexIgnore
        /* renamed from: a */
        public boolean f6646a;

        @DexIgnore
        /* renamed from: b */
        public long f6647b;

        @DexIgnore
        /* renamed from: c */
        public long f6648c;

        @DexIgnore
        /* renamed from: d */
        public long f6649d;

        @DexIgnore
        /* renamed from: e */
        public long f6650e;

        @DexIgnore
        /* renamed from: f */
        public long f6651f;
    }

    @DexIgnore
    public C2177k0(android.content.Context context, android.location.LocationManager locationManager) {
        this.f6643a = context;
        this.f6644b = locationManager;
    }

    @DexIgnore
    /* renamed from: a */
    public static com.fossil.blesdk.obfuscated.C2177k0 m9259a(android.content.Context context) {
        if (f6642d == null) {
            android.content.Context applicationContext = context.getApplicationContext();
            f6642d = new com.fossil.blesdk.obfuscated.C2177k0(applicationContext, (android.location.LocationManager) applicationContext.getSystemService(com.facebook.places.model.PlaceFields.LOCATION));
        }
        return f6642d;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo12536b() {
        com.fossil.blesdk.obfuscated.C2177k0.C2178a aVar = this.f6645c;
        if (mo12537c()) {
            return aVar.f6646a;
        }
        android.location.Location a = mo12533a();
        if (a != null) {
            mo12535a(a);
            return aVar.f6646a;
        }
        android.util.Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = java.util.Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    @DexIgnore
    /* renamed from: c */
    public final boolean mo12537c() {
        return this.f6645c.f6651f > java.lang.System.currentTimeMillis();
    }

    @DexIgnore
    @android.annotation.SuppressLint({"MissingPermission"})
    /* renamed from: a */
    public final android.location.Location mo12533a() {
        android.location.Location location = null;
        android.location.Location a = com.fossil.blesdk.obfuscated.C2296l6.m10076b(this.f6643a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? mo12534a("network") : null;
        if (com.fossil.blesdk.obfuscated.C2296l6.m10076b(this.f6643a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = mo12534a("gps");
        }
        return (location == null || a == null) ? location != null ? location : a : location.getTime() > a.getTime() ? location : a;
    }

    @DexIgnore
    /* renamed from: a */
    public final android.location.Location mo12534a(java.lang.String str) {
        try {
            if (this.f6644b.isProviderEnabled(str)) {
                return this.f6644b.getLastKnownLocation(str);
            }
            return null;
        } catch (java.lang.Exception e) {
            android.util.Log.d("TwilightManager", "Failed to get last known location", e);
            return null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo12535a(android.location.Location location) {
        long j;
        com.fossil.blesdk.obfuscated.C2177k0.C2178a aVar = this.f6645c;
        long currentTimeMillis = java.lang.System.currentTimeMillis();
        com.fossil.blesdk.obfuscated.C2085j0 a = com.fossil.blesdk.obfuscated.C2085j0.m8720a();
        com.fossil.blesdk.obfuscated.C2085j0 j0Var = a;
        j0Var.mo12178a(currentTimeMillis - com.sina.weibo.sdk.statistic.LogBuilder.MAX_INTERVAL, location.getLatitude(), location.getLongitude());
        long j2 = a.f6248a;
        j0Var.mo12178a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a.f6250c == 1;
        long j3 = a.f6249b;
        long j4 = j2;
        long j5 = a.f6248a;
        long j6 = j3;
        boolean z2 = z;
        a.mo12178a(com.sina.weibo.sdk.statistic.LogBuilder.MAX_INTERVAL + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j7 = a.f6249b;
        if (j6 == -1 || j5 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            j = (currentTimeMillis > j5 ? 0 + j7 : currentTimeMillis > j6 ? 0 + j5 : 0 + j6) + 60000;
        }
        aVar.f6646a = z2;
        aVar.f6647b = j4;
        aVar.f6648c = j6;
        aVar.f6649d = j5;
        aVar.f6650e = j7;
        aVar.f6651f = j;
    }
}
