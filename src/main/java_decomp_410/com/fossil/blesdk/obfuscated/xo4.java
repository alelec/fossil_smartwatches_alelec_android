package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface xo4 extends Closeable, Flushable {
    @DexIgnore
    void a(jo4 jo4, long j) throws IOException;

    @DexIgnore
    zo4 b();

    @DexIgnore
    void close() throws IOException;

    @DexIgnore
    void flush() throws IOException;
}
