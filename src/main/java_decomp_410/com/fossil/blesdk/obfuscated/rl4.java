package com.fossil.blesdk.obfuscated;

import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface rl4 {
    @DexIgnore
    public static final rl4 a = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements rl4 {
        @DexIgnore
        public List<ql4> a(zl4 zl4) {
            return Collections.emptyList();
        }

        @DexIgnore
        public void a(zl4 zl4, List<ql4> list) {
        }
    }

    @DexIgnore
    List<ql4> a(zl4 zl4);

    @DexIgnore
    void a(zl4 zl4, List<ql4> list);
}
