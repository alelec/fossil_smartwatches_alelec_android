package com.fossil.blesdk.obfuscated;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class bc {
    @DexIgnore
    public Map<String, Integer> a; // = new HashMap();

    @DexIgnore
    public boolean a(String str, int i) {
        Integer num = this.a.get(str);
        boolean z = false;
        int intValue = num != null ? num.intValue() : 0;
        if ((intValue & i) != 0) {
            z = true;
        }
        this.a.put(str, Integer.valueOf(i | intValue));
        return !z;
    }
}
