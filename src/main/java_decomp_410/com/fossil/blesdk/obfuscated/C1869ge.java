package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.ge */
public final class C1869ge implements com.fossil.blesdk.obfuscated.C3034ue {

    @DexIgnore
    /* renamed from: a */
    public /* final */ androidx.recyclerview.widget.RecyclerView.C0227g f5420a;

    @DexIgnore
    public C1869ge(androidx.recyclerview.widget.RecyclerView.C0227g gVar) {
        this.f5420a = gVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11199a(int i, int i2) {
        this.f5420a.notifyItemMoved(i, i2);
    }

    @DexIgnore
    /* renamed from: b */
    public void mo11201b(int i, int i2) {
        this.f5420a.notifyItemRangeInserted(i, i2);
    }

    @DexIgnore
    /* renamed from: c */
    public void mo11202c(int i, int i2) {
        this.f5420a.notifyItemRangeRemoved(i, i2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo11200a(int i, int i2, java.lang.Object obj) {
        this.f5420a.notifyItemRangeChanged(i, i2, obj);
    }
}
