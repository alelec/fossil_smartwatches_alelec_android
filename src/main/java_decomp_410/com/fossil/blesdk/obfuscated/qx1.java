package com.fossil.blesdk.obfuscated;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class qx1 extends px1<Bundle> {
    @DexIgnore
    public qx1(int i, int i2, Bundle bundle) {
        super(i, 1, bundle);
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        Bundle bundle2 = bundle.getBundle("data");
        if (bundle2 == null) {
            bundle2 = Bundle.EMPTY;
        }
        a(bundle2);
    }

    @DexIgnore
    public final boolean a() {
        return false;
    }
}
