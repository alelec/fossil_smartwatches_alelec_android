package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class kf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ RecyclerViewAlphabetIndex t;
    @DexIgnore
    public /* final */ FlexibleEditText u;
    @DexIgnore
    public /* final */ RecyclerView v;

    @DexIgnore
    public kf2(Object obj, View view, int i, RTLImageView rTLImageView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view2, RecyclerViewAlphabetIndex recyclerViewAlphabetIndex, FlexibleEditText flexibleEditText, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = imageView;
        this.s = flexibleTextView2;
        this.t = recyclerViewAlphabetIndex;
        this.u = flexibleEditText;
        this.v = recyclerView;
    }
}
