package com.fossil.blesdk.obfuscated;

import android.os.Parcelable;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface i23 extends v52<h23> {
    @DexIgnore
    void C(String str);

    @DexIgnore
    void a(int i, int i2, String str, String str2);

    @DexIgnore
    void a(String str, String str2, String str3, String str4);

    @DexIgnore
    void a(List<Category> list);

    @DexIgnore
    void a(boolean z, String str, String str2, Parcelable parcelable);

    @DexIgnore
    void b(Complication complication);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    void f(String str);

    @DexIgnore
    void g(String str);

    @DexIgnore
    void m(List<Complication> list);
}
