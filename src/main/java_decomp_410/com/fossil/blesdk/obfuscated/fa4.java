package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.r84;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.schedulers.ScheduledRunnable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class fa4 extends r84.b implements y84 {
    @DexIgnore
    public /* final */ ScheduledExecutorService e;
    @DexIgnore
    public volatile boolean f;

    @DexIgnore
    public y84 a(Runnable runnable) {
        return a(runnable, 0, (TimeUnit) null);
    }

    @DexIgnore
    public void dispose() {
        if (!this.f) {
            this.f = true;
            this.e.shutdownNow();
        }
    }

    @DexIgnore
    public y84 a(Runnable runnable, long j, TimeUnit timeUnit) {
        if (this.f) {
            return EmptyDisposable.INSTANCE;
        }
        return a(runnable, j, timeUnit, (i94) null);
    }

    @DexIgnore
    public ScheduledRunnable a(Runnable runnable, long j, TimeUnit timeUnit, i94 i94) {
        Future future;
        ScheduledRunnable scheduledRunnable = new ScheduledRunnable(ia4.a(runnable), i94);
        if (i94 != null && !i94.b(scheduledRunnable)) {
            return scheduledRunnable;
        }
        if (j <= 0) {
            try {
                future = this.e.submit(scheduledRunnable);
            } catch (RejectedExecutionException e2) {
                if (i94 != null) {
                    i94.a(scheduledRunnable);
                }
                ia4.b(e2);
            }
        } else {
            future = this.e.schedule(scheduledRunnable, j, timeUnit);
        }
        scheduledRunnable.setFuture(future);
        return scheduledRunnable;
    }

    @DexIgnore
    public void a() {
        if (!this.f) {
            this.f = true;
            this.e.shutdown();
        }
    }
}
