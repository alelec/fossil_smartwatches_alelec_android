package com.fossil.blesdk.obfuscated;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;
import com.fossil.wearables.fossil.R;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class lg2 extends kg2 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ScrollView G;
    @DexIgnore
    public long H;

    /*
    static {
        J.put(R.id.bt_back, 1);
        J.put(R.id.tv_title, 2);
        J.put(R.id.iv_device, 3);
        J.put(R.id.tv_device_name, 4);
        J.put(R.id.tv_connection_status, 5);
        J.put(R.id.bt_active, 6);
        J.put(R.id.bt_connect, 7);
        J.put(R.id.cv_detail, 8);
        J.put(R.id.tv_last_sync, 9);
        J.put(R.id.tv_last_sync_value, 10);
        J.put(R.id.space_1, 11);
        J.put(R.id.tv_fw_version, 12);
        J.put(R.id.tv_fw_version_value, 13);
        J.put(R.id.space_2, 14);
        J.put(R.id.tv_serial, 15);
        J.put(R.id.tv_serial_value, 16);
        J.put(R.id.tv_vibration, 17);
        J.put(R.id.fb_vibration_low, 18);
        J.put(R.id.fb_vibration_medium, 19);
        J.put(R.id.fb_vibration_high, 20);
        J.put(R.id.tv_find_device, 21);
        J.put(R.id.space_3, 22);
        J.put(R.id.tv_calibration, 23);
        J.put(R.id.tv_calibration_disconnected, 24);
        J.put(R.id.tv_remove_device, 25);
    }
    */

    @DexIgnore
    public lg2(pa paVar, View view) {
        this(paVar, view, ViewDataBinding.a(paVar, view, 26, I, J));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.H != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.H = 1;
        }
        g();
    }

    @DexIgnore
    public lg2(pa paVar, View view, Object[] objArr) {
        super(paVar, view, 0, objArr[6], objArr[1], objArr[7], objArr[8], objArr[20], objArr[18], objArr[19], objArr[3], objArr[11], objArr[14], objArr[22], objArr[23], objArr[24], objArr[5], objArr[4], objArr[21], objArr[12], objArr[13], objArr[9], objArr[10], objArr[25], objArr[15], objArr[16], objArr[2], objArr[17]);
        this.H = -1;
        this.G = objArr[0];
        this.G.setTag((Object) null);
        a(view);
        f();
    }
}
