package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.gk */
public class C1878gk {

    @DexIgnore
    /* renamed from: a */
    public boolean f5458a;

    @DexIgnore
    /* renamed from: b */
    public boolean f5459b;

    @DexIgnore
    /* renamed from: c */
    public boolean f5460c;

    @DexIgnore
    /* renamed from: d */
    public boolean f5461d;

    @DexIgnore
    public C1878gk(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f5458a = z;
        this.f5459b = z2;
        this.f5460c = z3;
        this.f5461d = z4;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo11251a() {
        return this.f5458a;
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo11252b() {
        return this.f5460c;
    }

    @DexIgnore
    /* renamed from: c */
    public boolean mo11253c() {
        return this.f5461d;
    }

    @DexIgnore
    /* renamed from: d */
    public boolean mo11254d() {
        return this.f5459b;
    }

    @DexIgnore
    public boolean equals(java.lang.Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || com.fossil.blesdk.obfuscated.C1878gk.class != obj.getClass()) {
            return false;
        }
        com.fossil.blesdk.obfuscated.C1878gk gkVar = (com.fossil.blesdk.obfuscated.C1878gk) obj;
        if (this.f5458a == gkVar.f5458a && this.f5459b == gkVar.f5459b && this.f5460c == gkVar.f5460c && this.f5461d == gkVar.f5461d) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.f5458a ? 1 : 0;
        if (this.f5459b) {
            i += 16;
        }
        if (this.f5460c) {
            i += 256;
        }
        return this.f5461d ? i + 4096 : i;
    }

    @DexIgnore
    public java.lang.String toString() {
        return java.lang.String.format("[ Connected=%b Validated=%b Metered=%b NotRoaming=%b ]", new java.lang.Object[]{java.lang.Boolean.valueOf(this.f5458a), java.lang.Boolean.valueOf(this.f5459b), java.lang.Boolean.valueOf(this.f5460c), java.lang.Boolean.valueOf(this.f5461d)});
    }
}
