package com.fossil.blesdk.obfuscated;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class sb4 extends rb4 {
    @DexIgnore
    public static final <K, V> List<Pair<K, V>> a(Map<? extends K, ? extends V> map) {
        kd4.b(map, "$this$toList");
        if (map.size() == 0) {
            return cb4.a();
        }
        Iterator<Map.Entry<? extends K, ? extends V>> it = map.entrySet().iterator();
        if (!it.hasNext()) {
            return cb4.a();
        }
        Map.Entry next = it.next();
        if (!it.hasNext()) {
            return bb4.a(new Pair(next.getKey(), next.getValue()));
        }
        ArrayList arrayList = new ArrayList(map.size());
        arrayList.add(new Pair(next.getKey(), next.getValue()));
        do {
            Map.Entry next2 = it.next();
            arrayList.add(new Pair(next2.getKey(), next2.getValue()));
        } while (it.hasNext());
        return arrayList;
    }
}
