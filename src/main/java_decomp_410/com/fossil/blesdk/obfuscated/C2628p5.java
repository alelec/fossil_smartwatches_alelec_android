package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.p5 */
public final class C2628p5<T> {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1862g8<java.util.ArrayList<T>> f8291a; // = new com.fossil.blesdk.obfuscated.C1928h8(10);

    @DexIgnore
    /* renamed from: b */
    public /* final */ androidx.collection.SimpleArrayMap<T, java.util.ArrayList<T>> f8292b; // = new androidx.collection.SimpleArrayMap<>();

    @DexIgnore
    /* renamed from: c */
    public /* final */ java.util.ArrayList<T> f8293c; // = new java.util.ArrayList<>();

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.util.HashSet<T> f8294d; // = new java.util.HashSet<>();

    @DexIgnore
    /* renamed from: a */
    public void mo14622a(T t) {
        if (!this.f8292b.containsKey(t)) {
            this.f8292b.put(t, null);
        }
    }

    @DexIgnore
    /* renamed from: b */
    public boolean mo14627b(T t) {
        return this.f8292b.containsKey(t);
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.List mo14629c(T t) {
        return this.f8292b.get(t);
    }

    @DexIgnore
    /* renamed from: d */
    public java.util.List<T> mo14630d(T t) {
        int size = this.f8292b.size();
        java.util.ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            java.util.ArrayList e = this.f8292b.mo1229e(i);
            if (e != null && e.contains(t)) {
                if (arrayList == null) {
                    arrayList = new java.util.ArrayList();
                }
                arrayList.add(this.f8292b.mo1224c(i));
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* renamed from: e */
    public boolean mo14631e(T t) {
        int size = this.f8292b.size();
        for (int i = 0; i < size; i++) {
            java.util.ArrayList e = this.f8292b.mo1229e(i);
            if (e != null && e.contains(t)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* renamed from: b */
    public final java.util.ArrayList<T> mo14626b() {
        java.util.ArrayList<T> a = this.f8291a.mo11162a();
        return a == null ? new java.util.ArrayList<>() : a;
    }

    @DexIgnore
    /* renamed from: c */
    public java.util.ArrayList<T> mo14628c() {
        this.f8293c.clear();
        this.f8294d.clear();
        int size = this.f8292b.size();
        for (int i = 0; i < size; i++) {
            mo14624a(this.f8292b.mo1224c(i), this.f8293c, this.f8294d);
        }
        return this.f8293c;
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14623a(T t, T t2) {
        if (!this.f8292b.containsKey(t) || !this.f8292b.containsKey(t2)) {
            throw new java.lang.IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
        }
        java.util.ArrayList arrayList = this.f8292b.get(t);
        if (arrayList == null) {
            arrayList = mo14626b();
            this.f8292b.put(t, arrayList);
        }
        arrayList.add(t2);
    }

    @DexIgnore
    /* renamed from: a */
    public void mo14621a() {
        int size = this.f8292b.size();
        for (int i = 0; i < size; i++) {
            java.util.ArrayList e = this.f8292b.mo1229e(i);
            if (e != null) {
                mo14625a(e);
            }
        }
        this.f8292b.clear();
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14624a(T t, java.util.ArrayList<T> arrayList, java.util.HashSet<T> hashSet) {
        if (!arrayList.contains(t)) {
            if (!hashSet.contains(t)) {
                hashSet.add(t);
                java.util.ArrayList arrayList2 = this.f8292b.get(t);
                if (arrayList2 != null) {
                    int size = arrayList2.size();
                    for (int i = 0; i < size; i++) {
                        mo14624a(arrayList2.get(i), arrayList, hashSet);
                    }
                }
                hashSet.remove(t);
                arrayList.add(t);
                return;
            }
            throw new java.lang.RuntimeException("This graph contains cyclic dependencies");
        }
    }

    @DexIgnore
    /* renamed from: a */
    public final void mo14625a(java.util.ArrayList<T> arrayList) {
        arrayList.clear();
        this.f8291a.mo11163a(arrayList);
    }
}
