package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class ah2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;

    @DexIgnore
    public ah2(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageButton imageButton, ImageView imageView, View view2) {
        super(obj, view, i);
        this.q = constraintLayout;
    }
}
