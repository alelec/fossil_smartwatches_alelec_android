package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.os */
public final class C2590os extends com.fossil.blesdk.obfuscated.C2060is<android.graphics.Bitmap> {

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C2149jq f8203b; // = new com.fossil.blesdk.obfuscated.C2236kq();

    @DexIgnore
    /* renamed from: a */
    public com.fossil.blesdk.obfuscated.C1438aq<android.graphics.Bitmap> mo12078a(android.graphics.ImageDecoder.Source source, int i, int i2, android.graphics.ImageDecoder.OnHeaderDecodedListener onHeaderDecodedListener) throws java.io.IOException {
        android.graphics.Bitmap decodeBitmap = android.graphics.ImageDecoder.decodeBitmap(source, onHeaderDecodedListener);
        if (android.util.Log.isLoggable("BitmapImageDecoder", 2)) {
            android.util.Log.v("BitmapImageDecoder", "Decoded [" + decodeBitmap.getWidth() + "x" + decodeBitmap.getHeight() + "] for [" + i + "x" + i2 + "]");
        }
        return new com.fossil.blesdk.obfuscated.C2675ps(decodeBitmap, this.f8203b);
    }
}
