package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class xx {
    @DexIgnore
    public long a;
    @DexIgnore
    public n64 b;

    @DexIgnore
    public xx(n64 n64) {
        if (n64 != null) {
            this.b = n64;
            return;
        }
        throw new NullPointerException("retryState must not be null");
    }

    @DexIgnore
    public boolean a(long j) {
        return j - this.a >= this.b.a() * 1000000;
    }

    @DexIgnore
    public void b(long j) {
        this.a = j;
        this.b = this.b.c();
    }

    @DexIgnore
    public void a() {
        this.a = 0;
        this.b = this.b.b();
    }
}
