package com.fossil.blesdk.obfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;
import com.google.android.gms.common.internal.safeparcel.SafeParcelReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xc0 implements Parcelable.Creator<SignInConfiguration> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = SafeParcelReader.b(parcel);
        String str = null;
        GoogleSignInOptions googleSignInOptions = null;
        while (parcel.dataPosition() < b) {
            int a = SafeParcelReader.a(parcel);
            int a2 = SafeParcelReader.a(a);
            if (a2 == 2) {
                str = SafeParcelReader.f(parcel, a);
            } else if (a2 != 5) {
                SafeParcelReader.v(parcel, a);
            } else {
                googleSignInOptions = (GoogleSignInOptions) SafeParcelReader.a(parcel, a, GoogleSignInOptions.CREATOR);
            }
        }
        SafeParcelReader.h(parcel, b);
        return new SignInConfiguration(str, googleSignInOptions);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new SignInConfiguration[i];
    }
}
