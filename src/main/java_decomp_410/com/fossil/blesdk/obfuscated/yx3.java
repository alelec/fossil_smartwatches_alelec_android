package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.NetworkRequestHandler;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class yx3 {
    @DexIgnore
    public /* final */ b a; // = new b();
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ Downloader d;
    @DexIgnore
    public /* final */ Map<String, sx3> e;
    @DexIgnore
    public /* final */ Map<Object, qx3> f;
    @DexIgnore
    public /* final */ Map<Object, qx3> g;
    @DexIgnore
    public /* final */ Set<Object> h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public /* final */ tx3 k;
    @DexIgnore
    public /* final */ ky3 l;
    @DexIgnore
    public /* final */ List<sx3> m;
    @DexIgnore
    public /* final */ c n;
    @DexIgnore
    public /* final */ boolean o;
    @DexIgnore
    public boolean p;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Handler {
        @DexIgnore
        public /* final */ yx3 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.blesdk.obfuscated.yx3$a$a")
        /* renamed from: com.fossil.blesdk.obfuscated.yx3$a$a  reason: collision with other inner class name */
        public class C0110a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Message e;

            @DexIgnore
            public C0110a(a aVar, Message message) {
                this.e = message;
            }

            @DexIgnore
            public void run() {
                throw new AssertionError("Unknown handler message received: " + this.e.what);
            }
        }

        @DexIgnore
        public a(Looper looper, yx3 yx3) {
            super(looper);
            this.a = yx3;
        }

        @DexIgnore
        public void handleMessage(Message message) {
            boolean z = false;
            switch (message.what) {
                case 1:
                    this.a.e((qx3) message.obj);
                    return;
                case 2:
                    this.a.d((qx3) message.obj);
                    return;
                case 4:
                    this.a.f((sx3) message.obj);
                    return;
                case 5:
                    this.a.g((sx3) message.obj);
                    return;
                case 6:
                    this.a.a((sx3) message.obj, false);
                    return;
                case 7:
                    this.a.b();
                    return;
                case 9:
                    this.a.b((NetworkInfo) message.obj);
                    return;
                case 10:
                    yx3 yx3 = this.a;
                    if (message.arg1 == 1) {
                        z = true;
                    }
                    yx3.b(z);
                    return;
                case 11:
                    this.a.a(message.obj);
                    return;
                case 12:
                    this.a.b(message.obj);
                    return;
                default:
                    Picasso.p.post(new C0110a(this, message));
                    return;
            }
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends HandlerThread {
        @DexIgnore
        public b() {
            super("Picasso-Dispatcher", 10);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ yx3 a;

        @DexIgnore
        public c(yx3 yx3) {
            this.a = yx3;
        }

        @DexIgnore
        public void a() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            if (this.a.o) {
                intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            this.a.b.registerReceiver(this, intentFilter);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                    if (intent.hasExtra("state")) {
                        this.a.a(intent.getBooleanExtra("state", false));
                    }
                } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                    this.a.a(((ConnectivityManager) oy3.a(context, "connectivity")).getActiveNetworkInfo());
                }
            }
        }
    }

    @DexIgnore
    public yx3(Context context, ExecutorService executorService, Handler handler, Downloader downloader, tx3 tx3, ky3 ky3) {
        this.a.start();
        oy3.a(this.a.getLooper());
        this.b = context;
        this.c = executorService;
        this.e = new LinkedHashMap();
        this.f = new WeakHashMap();
        this.g = new WeakHashMap();
        this.h = new HashSet();
        this.i = new a(this.a.getLooper(), this);
        this.d = downloader;
        this.j = handler;
        this.k = tx3;
        this.l = ky3;
        this.m = new ArrayList(4);
        this.p = oy3.d(this.b);
        this.o = oy3.b(context, "android.permission.ACCESS_NETWORK_STATE");
        this.n = new c(this);
        this.n.a();
    }

    @DexIgnore
    public void a(qx3 qx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(2, qx3));
    }

    @DexIgnore
    public void b(qx3 qx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(1, qx3));
    }

    @DexIgnore
    public void c(sx3 sx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(6, sx3));
    }

    @DexIgnore
    public void d(sx3 sx3) {
        Handler handler = this.i;
        handler.sendMessageDelayed(handler.obtainMessage(5, sx3), 500);
    }

    @DexIgnore
    public void e(qx3 qx3) {
        a(qx3, true);
    }

    @DexIgnore
    public void f(sx3 sx3) {
        if (MemoryPolicy.shouldWriteToMemoryCache(sx3.i())) {
            this.k.a(sx3.g(), sx3.l());
        }
        this.e.remove(sx3.g());
        a(sx3);
        if (sx3.j().n) {
            oy3.a("Dispatcher", "batched", oy3.a(sx3), "for completion");
        }
    }

    @DexIgnore
    public void g(sx3 sx3) {
        if (!sx3.n()) {
            boolean z = false;
            if (this.c.isShutdown()) {
                a(sx3, false);
                return;
            }
            NetworkInfo networkInfo = null;
            if (this.o) {
                networkInfo = ((ConnectivityManager) oy3.a(this.b, "connectivity")).getActiveNetworkInfo();
            }
            boolean z2 = networkInfo != null && networkInfo.isConnected();
            boolean a2 = sx3.a(this.p, networkInfo);
            boolean o2 = sx3.o();
            if (!a2) {
                if (this.o && o2) {
                    z = true;
                }
                a(sx3, z);
                if (z) {
                    e(sx3);
                }
            } else if (!this.o || z2) {
                if (sx3.j().n) {
                    oy3.a("Dispatcher", "retrying", oy3.a(sx3));
                }
                if (sx3.f() instanceof NetworkRequestHandler.ContentLengthException) {
                    sx3.m |= NetworkPolicy.NO_CACHE.index;
                }
                sx3.r = this.c.submit(sx3);
            } else {
                a(sx3, o2);
                if (o2) {
                    e(sx3);
                }
            }
        }
    }

    @DexIgnore
    public void a(NetworkInfo networkInfo) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(9, networkInfo));
    }

    @DexIgnore
    public void b(sx3 sx3) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(4, sx3));
    }

    @DexIgnore
    public final void c(qx3 qx3) {
        Object j2 = qx3.j();
        if (j2 != null) {
            qx3.k = true;
            this.f.put(j2, qx3);
        }
    }

    @DexIgnore
    public void d(qx3 qx3) {
        String c2 = qx3.c();
        sx3 sx3 = this.e.get(c2);
        if (sx3 != null) {
            sx3.b(qx3);
            if (sx3.a()) {
                this.e.remove(c2);
                if (qx3.f().n) {
                    oy3.a("Dispatcher", "canceled", qx3.h().d());
                }
            }
        }
        if (this.h.contains(qx3.i())) {
            this.g.remove(qx3.j());
            if (qx3.f().n) {
                oy3.a("Dispatcher", "canceled", qx3.h().d(), "because paused request got canceled");
            }
        }
        qx3 remove = this.f.remove(qx3.j());
        if (remove != null && remove.f().n) {
            oy3.a("Dispatcher", "canceled", remove.h().d(), "from replaying");
        }
    }

    @DexIgnore
    public final void e(sx3 sx3) {
        qx3 c2 = sx3.c();
        if (c2 != null) {
            c(c2);
        }
        List<qx3> d2 = sx3.d();
        if (d2 != null) {
            int size = d2.size();
            for (int i2 = 0; i2 < size; i2++) {
                c(d2.get(i2));
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        Handler handler = this.i;
        handler.sendMessage(handler.obtainMessage(10, z ? 1 : 0, 0));
    }

    @DexIgnore
    public void b(Object obj) {
        if (this.h.remove(obj)) {
            ArrayList arrayList = null;
            Iterator<qx3> it = this.g.values().iterator();
            while (it.hasNext()) {
                qx3 next = it.next();
                if (next.i().equals(obj)) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(next);
                    it.remove();
                }
            }
            if (arrayList != null) {
                Handler handler = this.j;
                handler.sendMessage(handler.obtainMessage(13, arrayList));
            }
        }
    }

    @DexIgnore
    public void a(qx3 qx3, boolean z) {
        if (this.h.contains(qx3.i())) {
            this.g.put(qx3.j(), qx3);
            if (qx3.f().n) {
                String d2 = qx3.b.d();
                oy3.a("Dispatcher", "paused", d2, "because tag '" + qx3.i() + "' is paused");
                return;
            }
            return;
        }
        sx3 sx3 = this.e.get(qx3.c());
        if (sx3 != null) {
            sx3.a(qx3);
        } else if (!this.c.isShutdown()) {
            sx3 a2 = sx3.a(qx3.f(), this, this.k, this.l, qx3);
            a2.r = this.c.submit(a2);
            this.e.put(qx3.c(), a2);
            if (z) {
                this.f.remove(qx3.j());
            }
            if (qx3.f().n) {
                oy3.a("Dispatcher", "enqueued", qx3.b.d());
            }
        } else if (qx3.f().n) {
            oy3.a("Dispatcher", "ignored", qx3.b.d(), "because shut down");
        }
    }

    @DexIgnore
    public void b() {
        ArrayList arrayList = new ArrayList(this.m);
        this.m.clear();
        Handler handler = this.j;
        handler.sendMessage(handler.obtainMessage(8, arrayList));
        a((List<sx3>) arrayList);
    }

    @DexIgnore
    public void b(boolean z) {
        this.p = z;
    }

    @DexIgnore
    public void b(NetworkInfo networkInfo) {
        ExecutorService executorService = this.c;
        if (executorService instanceof fy3) {
            ((fy3) executorService).a(networkInfo);
        }
        if (networkInfo != null && networkInfo.isConnected()) {
            a();
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (this.h.add(obj)) {
            Iterator<sx3> it = this.e.values().iterator();
            while (it.hasNext()) {
                sx3 next = it.next();
                boolean z = next.j().n;
                qx3 c2 = next.c();
                List<qx3> d2 = next.d();
                boolean z2 = d2 != null && !d2.isEmpty();
                if (c2 != null || z2) {
                    if (c2 != null && c2.i().equals(obj)) {
                        next.b(c2);
                        this.g.put(c2.j(), c2);
                        if (z) {
                            oy3.a("Dispatcher", "paused", c2.b.d(), "because tag '" + obj + "' was paused");
                        }
                    }
                    if (z2) {
                        for (int size = d2.size() - 1; size >= 0; size--) {
                            qx3 qx3 = d2.get(size);
                            if (qx3.i().equals(obj)) {
                                next.b(qx3);
                                this.g.put(qx3.j(), qx3);
                                if (z) {
                                    oy3.a("Dispatcher", "paused", qx3.b.d(), "because tag '" + obj + "' was paused");
                                }
                            }
                        }
                    }
                    if (next.a()) {
                        it.remove();
                        if (z) {
                            oy3.a("Dispatcher", "canceled", oy3.a(next), "all actions paused");
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public void a(sx3 sx3, boolean z) {
        if (sx3.j().n) {
            String a2 = oy3.a(sx3);
            StringBuilder sb = new StringBuilder();
            sb.append("for error");
            sb.append(z ? " (will replay)" : "");
            oy3.a("Dispatcher", "batched", a2, sb.toString());
        }
        this.e.remove(sx3.g());
        a(sx3);
    }

    @DexIgnore
    public final void a() {
        if (!this.f.isEmpty()) {
            Iterator<qx3> it = this.f.values().iterator();
            while (it.hasNext()) {
                qx3 next = it.next();
                it.remove();
                if (next.f().n) {
                    oy3.a("Dispatcher", "replaying", next.h().d());
                }
                a(next, false);
            }
        }
    }

    @DexIgnore
    public final void a(sx3 sx3) {
        if (!sx3.n()) {
            this.m.add(sx3);
            if (!this.i.hasMessages(7)) {
                this.i.sendEmptyMessageDelayed(7, 200);
            }
        }
    }

    @DexIgnore
    public final void a(List<sx3> list) {
        if (list != null && !list.isEmpty() && list.get(0).j().n) {
            StringBuilder sb = new StringBuilder();
            for (sx3 next : list) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append(oy3.a(next));
            }
            oy3.a("Dispatcher", "delivered", sb.toString());
        }
    }
}
