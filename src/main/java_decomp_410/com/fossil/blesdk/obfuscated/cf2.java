package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class cf2 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ LinearLayout D;
    @DexIgnore
    public /* final */ LinearLayout E;
    @DexIgnore
    public /* final */ ScrollView F;
    @DexIgnore
    public /* final */ CustomEditGoalView q;
    @DexIgnore
    public /* final */ CustomEditGoalView r;
    @DexIgnore
    public /* final */ CustomEditGoalView s;
    @DexIgnore
    public /* final */ CustomEditGoalView t;
    @DexIgnore
    public /* final */ FlexibleEditText u;
    @DexIgnore
    public /* final */ FlexibleEditText v;
    @DexIgnore
    public /* final */ FlexibleEditText w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cf2(Object obj, View view, int i, Barrier barrier, ConstraintLayout constraintLayout, CustomEditGoalView customEditGoalView, CustomEditGoalView customEditGoalView2, CustomEditGoalView customEditGoalView3, CustomEditGoalView customEditGoalView4, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleEditText flexibleEditText3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, Guideline guideline, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, ScrollView scrollView, FlexibleTextView flexibleTextView6, View view2, View view3, Guideline guideline2) {
        super(obj, view, i);
        this.q = customEditGoalView;
        this.r = customEditGoalView2;
        this.s = customEditGoalView3;
        this.t = customEditGoalView4;
        this.u = flexibleEditText;
        this.v = flexibleEditText2;
        this.w = flexibleEditText3;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = flexibleTextView4;
        this.B = flexibleTextView5;
        this.C = rTLImageView;
        this.D = linearLayout;
        this.E = linearLayout2;
        this.F = scrollView;
    }
}
