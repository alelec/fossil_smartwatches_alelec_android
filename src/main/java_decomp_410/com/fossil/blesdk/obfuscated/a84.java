package com.fossil.blesdk.obfuscated;

import io.fabric.sdk.android.services.settings.SettingsCacheBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface a84 {
    @DexIgnore
    b84 a();

    @DexIgnore
    b84 a(SettingsCacheBehavior settingsCacheBehavior);
}
