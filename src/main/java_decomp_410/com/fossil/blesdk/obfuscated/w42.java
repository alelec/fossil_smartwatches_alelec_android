package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class w42 implements Factory<ContentResolver> {
    @DexIgnore
    public /* final */ n42 a;

    @DexIgnore
    public w42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    public static w42 a(n42 n42) {
        return new w42(n42);
    }

    @DexIgnore
    public static ContentResolver b(n42 n42) {
        return c(n42);
    }

    @DexIgnore
    public static ContentResolver c(n42 n42) {
        ContentResolver e = n42.e();
        n44.a(e, "Cannot return null from a non-@Nullable @Provides method");
        return e;
    }

    @DexIgnore
    public ContentResolver get() {
        return b(this.a);
    }
}
