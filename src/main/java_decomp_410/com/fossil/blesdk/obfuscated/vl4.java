package com.fossil.blesdk.obfuscated;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import okhttp3.Protocol;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vl4 {
    @DexIgnore
    public static /* final */ vl4 a; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vl4 {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public vl4 a(jl4 jl4) {
            return vl4.this;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        vl4 a(jl4 jl4);
    }

    @DexIgnore
    public static c a(vl4 vl4) {
        return new b();
    }

    @DexIgnore
    public void a(jl4 jl4) {
    }

    @DexIgnore
    public void a(jl4 jl4, long j) {
    }

    @DexIgnore
    public void a(jl4 jl4, dm4 dm4) {
    }

    @DexIgnore
    public void a(jl4 jl4, nl4 nl4) {
    }

    @DexIgnore
    public void a(jl4 jl4, xl4 xl4) {
    }

    @DexIgnore
    public void a(jl4 jl4, IOException iOException) {
    }

    @DexIgnore
    public void a(jl4 jl4, String str) {
    }

    @DexIgnore
    public void a(jl4 jl4, String str, List<InetAddress> list) {
    }

    @DexIgnore
    public void a(jl4 jl4, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    @DexIgnore
    public void a(jl4 jl4, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol) {
    }

    @DexIgnore
    public void a(jl4 jl4, InetSocketAddress inetSocketAddress, Proxy proxy, Protocol protocol, IOException iOException) {
    }

    @DexIgnore
    public void a(jl4 jl4, Response response) {
    }

    @DexIgnore
    public void b(jl4 jl4) {
    }

    @DexIgnore
    public void b(jl4 jl4, long j) {
    }

    @DexIgnore
    public void b(jl4 jl4, nl4 nl4) {
    }

    @DexIgnore
    public void c(jl4 jl4) {
    }

    @DexIgnore
    public void d(jl4 jl4) {
    }

    @DexIgnore
    public void e(jl4 jl4) {
    }

    @DexIgnore
    public void f(jl4 jl4) {
    }

    @DexIgnore
    public void g(jl4 jl4) {
    }
}
