package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.vb1;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class vb1<M extends vb1<M>> extends ac1 {
    @DexIgnore
    public xb1 b;

    @DexIgnore
    public int a() {
        if (this.b == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.b.b(); i2++) {
            i += this.b.b(i2).zzf();
        }
        return i;
    }

    @DexIgnore
    public final /* synthetic */ ac1 c() throws CloneNotSupportedException {
        return (vb1) clone();
    }

    @DexIgnore
    public /* synthetic */ Object clone() throws CloneNotSupportedException {
        vb1 vb1 = (vb1) super.clone();
        zb1.a(this, vb1);
        return vb1;
    }

    @DexIgnore
    public void a(ub1 ub1) throws IOException {
        if (this.b != null) {
            for (int i = 0; i < this.b.b(); i++) {
                this.b.b(i).a(ub1);
            }
        }
    }

    @DexIgnore
    public final boolean a(tb1 tb1, int i) throws IOException {
        int a = tb1.a();
        if (!tb1.b(i)) {
            return false;
        }
        int i2 = i >>> 3;
        cc1 cc1 = new cc1(i, tb1.a(a, tb1.a() - a));
        yb1 yb1 = null;
        xb1 xb1 = this.b;
        if (xb1 == null) {
            this.b = new xb1();
        } else {
            yb1 = xb1.a(i2);
        }
        if (yb1 == null) {
            yb1 = new yb1();
            this.b.a(i2, yb1);
        }
        yb1.a(cc1);
        return true;
    }
}
