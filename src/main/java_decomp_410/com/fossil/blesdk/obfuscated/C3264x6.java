package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.x6 */
public class C3264x6 extends com.fossil.blesdk.obfuscated.C1394a7 {

    @DexIgnore
    /* renamed from: a */
    public static /* final */ java.lang.Class f10851a;

    @DexIgnore
    /* renamed from: b */
    public static /* final */ java.lang.reflect.Constructor f10852b;

    @DexIgnore
    /* renamed from: c */
    public static /* final */ java.lang.reflect.Method f10853c;

    @DexIgnore
    /* renamed from: d */
    public static /* final */ java.lang.reflect.Method f10854d;

    /*
    static {
        java.lang.reflect.Method method;
        java.lang.reflect.Method method2;
        java.lang.Class<?> cls;
        java.lang.reflect.Constructor<?> constructor = null;
        try {
            cls = java.lang.Class.forName("android.graphics.FontFamily");
            java.lang.reflect.Constructor<?> constructor2 = cls.getConstructor(new java.lang.Class[0]);
            method = cls.getMethod("addFontWeightStyle", new java.lang.Class[]{java.nio.ByteBuffer.class, java.lang.Integer.TYPE, java.util.List.class, java.lang.Integer.TYPE, java.lang.Boolean.TYPE});
            method2 = android.graphics.Typeface.class.getMethod("createFromFamiliesWithDefault", new java.lang.Class[]{java.lang.reflect.Array.newInstance(cls, 1).getClass()});
            constructor = constructor2;
        } catch (java.lang.ClassNotFoundException | java.lang.NoSuchMethodException e) {
            android.util.Log.e("TypefaceCompatApi24Impl", e.getClass().getName(), e);
            cls = null;
            method2 = null;
            method = null;
        }
        f10852b = constructor;
        f10851a = cls;
        f10853c = method;
        f10854d = method2;
    }
    */

    @DexIgnore
    /* renamed from: a */
    public static boolean m16160a() {
        if (f10853c == null) {
            android.util.Log.w("TypefaceCompatApi24Impl", "Unable to collect necessary private methods.Fallback to legacy implementation.");
        }
        return f10853c != null;
    }

    @DexIgnore
    /* renamed from: b */
    public static java.lang.Object m16162b() {
        try {
            return f10852b.newInstance(new java.lang.Object[0]);
        } catch (java.lang.IllegalAccessException | java.lang.InstantiationException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static boolean m16161a(java.lang.Object obj, java.nio.ByteBuffer byteBuffer, int i, int i2, boolean z) {
        try {
            return ((java.lang.Boolean) f10853c.invoke(obj, new java.lang.Object[]{byteBuffer, java.lang.Integer.valueOf(i), null, java.lang.Integer.valueOf(i2), java.lang.Boolean.valueOf(z)})).booleanValue();
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public static android.graphics.Typeface m16159a(java.lang.Object obj) {
        try {
            java.lang.Object newInstance = java.lang.reflect.Array.newInstance(f10851a, 1);
            java.lang.reflect.Array.set(newInstance, 0, obj);
            return (android.graphics.Typeface) f10854d.invoke((java.lang.Object) null, new java.lang.Object[]{newInstance});
        } catch (java.lang.IllegalAccessException | java.lang.reflect.InvocationTargetException e) {
            throw new java.lang.RuntimeException(e);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8577a(android.content.Context context, android.os.CancellationSignal cancellationSignal, com.fossil.blesdk.obfuscated.C3012u7.C3018f[] fVarArr, int i) {
        java.lang.Object b = m16162b();
        androidx.collection.SimpleArrayMap simpleArrayMap = new androidx.collection.SimpleArrayMap();
        for (com.fossil.blesdk.obfuscated.C3012u7.C3018f fVar : fVarArr) {
            android.net.Uri c = fVar.mo16677c();
            java.nio.ByteBuffer byteBuffer = (java.nio.ByteBuffer) simpleArrayMap.get(c);
            if (byteBuffer == null) {
                byteBuffer = com.fossil.blesdk.obfuscated.C1466b7.m4803a(context, cancellationSignal, c);
                simpleArrayMap.put(c, byteBuffer);
            }
            if (!m16161a(b, byteBuffer, fVar.mo16676b(), fVar.mo16678d(), fVar.mo16679e())) {
                return null;
            }
        }
        return android.graphics.Typeface.create(m16159a(b), i);
    }

    @DexIgnore
    /* renamed from: a */
    public android.graphics.Typeface mo8578a(android.content.Context context, com.fossil.blesdk.obfuscated.C2528o6.C2530b bVar, android.content.res.Resources resources, int i) {
        java.lang.Object b = m16162b();
        for (com.fossil.blesdk.obfuscated.C2528o6.C2531c cVar : bVar.mo14259a()) {
            java.nio.ByteBuffer a = com.fossil.blesdk.obfuscated.C1466b7.m4802a(context, resources, cVar.mo14261b());
            if (a == null || !m16161a(b, a, cVar.mo14262c(), cVar.mo14264e(), cVar.mo14265f())) {
                return null;
            }
        }
        return m16159a(b);
    }
}
