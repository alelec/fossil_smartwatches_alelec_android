package com.fossil.blesdk.obfuscated;

import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class tl2 {
    @DexIgnore
    public /* final */ Map<String, String> a; // = new HashMap();
    @DexIgnore
    public /* final */ AnalyticsHelper b;

    @DexIgnore
    public tl2(AnalyticsHelper analyticsHelper) {
        kd4.b(analyticsHelper, "mAnalyticsInstance");
        this.b = analyticsHelper;
    }

    @DexIgnore
    public final tl2 a(String str, String str2) {
        kd4.b(str, "propertyName");
        kd4.b(str2, "propertyValue");
        this.a.put(str, str2);
        return this;
    }

    @DexIgnore
    public final void a() {
        this.b.a(this.a);
    }
}
