package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.az */
public class C1452az implements java.lang.Thread.UncaughtExceptionHandler {

    @DexIgnore
    /* renamed from: a */
    public /* final */ com.fossil.blesdk.obfuscated.C1452az.C1453a f3630a;

    @DexIgnore
    /* renamed from: b */
    public /* final */ com.fossil.blesdk.obfuscated.C1452az.C1454b f3631b;

    @DexIgnore
    /* renamed from: c */
    public /* final */ boolean f3632c;

    @DexIgnore
    /* renamed from: d */
    public /* final */ java.lang.Thread.UncaughtExceptionHandler f3633d;

    @DexIgnore
    /* renamed from: e */
    public /* final */ java.util.concurrent.atomic.AtomicBoolean f3634e; // = new java.util.concurrent.atomic.AtomicBoolean(false);

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.az$a */
    public interface C1453a {
        @DexIgnore
        /* renamed from: a */
        void mo4186a(com.fossil.blesdk.obfuscated.C1452az.C1454b bVar, java.lang.Thread thread, java.lang.Throwable th, boolean z);
    }

    @DexIgnore
    /* renamed from: com.fossil.blesdk.obfuscated.az$b */
    public interface C1454b {
        @DexIgnore
        /* renamed from: a */
        com.fossil.blesdk.obfuscated.b84 mo4190a();
    }

    @DexIgnore
    public C1452az(com.fossil.blesdk.obfuscated.C1452az.C1453a aVar, com.fossil.blesdk.obfuscated.C1452az.C1454b bVar, boolean z, java.lang.Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.f3630a = aVar;
        this.f3631b = bVar;
        this.f3632c = z;
        this.f3633d = uncaughtExceptionHandler;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean mo8991a() {
        return this.f3634e.get();
    }

    @DexIgnore
    public void uncaughtException(java.lang.Thread thread, java.lang.Throwable th) {
        this.f3634e.set(true);
        try {
            this.f3630a.mo4186a(this.f3631b, thread, th, this.f3632c);
        } catch (java.lang.Exception e) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30063e("CrashlyticsCore", "An error occurred in the uncaught exception handler", e);
        } catch (Throwable th2) {
            com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
            this.f3633d.uncaughtException(thread, th);
            this.f3634e.set(false);
            throw th2;
        }
        com.fossil.blesdk.obfuscated.q44.m26805g().mo30060d("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
        this.f3633d.uncaughtException(thread, th);
        this.f3634e.set(false);
    }
}
