package com.fossil.blesdk.obfuscated;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class xm1 extends jk0 implements me0 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<xm1> CREATOR; // = new ym1();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Intent g;

    @DexIgnore
    public xm1(int i, int i2, Intent intent) {
        this.e = i;
        this.f = i2;
        this.g = intent;
    }

    @DexIgnore
    public final Status G() {
        if (this.f == 0) {
            return Status.i;
        }
        return Status.m;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a = kk0.a(parcel);
        kk0.a(parcel, 1, this.e);
        kk0.a(parcel, 2, this.f);
        kk0.a(parcel, 3, (Parcelable) this.g, i, false);
        kk0.a(parcel, a);
    }

    @DexIgnore
    public xm1() {
        this(0, (Intent) null);
    }

    @DexIgnore
    public xm1(int i, Intent intent) {
        this(2, 0, (Intent) null);
    }
}
