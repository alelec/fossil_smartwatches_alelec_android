package com.fossil.blesdk.obfuscated;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class an0 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public an0(Context context) {
        this.a = context;
    }

    @DexIgnore
    public ApplicationInfo a(String str, int i) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getApplicationInfo(str, i);
    }

    @DexIgnore
    public PackageInfo b(String str, int i) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getPackageInfo(str, i);
    }

    @DexIgnore
    public final PackageInfo a(String str, int i, int i2) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getPackageInfo(str, 64);
    }

    @DexIgnore
    public CharSequence b(String str) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getApplicationLabel(this.a.getPackageManager().getApplicationInfo(str, 0));
    }

    @DexIgnore
    public final String[] a(int i) {
        return this.a.getPackageManager().getPackagesForUid(i);
    }

    @DexIgnore
    @TargetApi(19)
    public final boolean a(int i, String str) {
        if (pm0.e()) {
            try {
                ((AppOpsManager) this.a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException unused) {
                return false;
            }
        } else {
            String[] packagesForUid = this.a.getPackageManager().getPackagesForUid(i);
            if (!(str == null || packagesForUid == null)) {
                for (String equals : packagesForUid) {
                    if (str.equals(equals)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    @DexIgnore
    public int a(String str) {
        return this.a.checkCallingOrSelfPermission(str);
    }

    @DexIgnore
    public boolean a() {
        if (Binder.getCallingUid() == Process.myUid()) {
            return zm0.a(this.a);
        }
        if (!pm0.i()) {
            return false;
        }
        String nameForUid = this.a.getPackageManager().getNameForUid(Binder.getCallingUid());
        if (nameForUid != null) {
            return this.a.getPackageManager().isInstantApp(nameForUid);
        }
        return false;
    }
}
