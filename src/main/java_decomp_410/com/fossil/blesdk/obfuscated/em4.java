package com.fossil.blesdk.obfuscated;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class em4 implements Closeable {
    @DexIgnore
    public Reader e;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends em4 {
        @DexIgnore
        public /* final */ /* synthetic */ am4 f;
        @DexIgnore
        public /* final */ /* synthetic */ long g;
        @DexIgnore
        public /* final */ /* synthetic */ lo4 h;

        @DexIgnore
        public a(am4 am4, long j, lo4 lo4) {
            this.f = am4;
            this.g = j;
            this.h = lo4;
        }

        @DexIgnore
        public long C() {
            return this.g;
        }

        @DexIgnore
        public am4 D() {
            return this.f;
        }

        @DexIgnore
        public lo4 E() {
            return this.h;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Reader {
        @DexIgnore
        public /* final */ lo4 e;
        @DexIgnore
        public /* final */ Charset f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public Reader h;

        @DexIgnore
        public b(lo4 lo4, Charset charset) {
            this.e = lo4;
            this.f = charset;
        }

        @DexIgnore
        public void close() throws IOException {
            this.g = true;
            Reader reader = this.h;
            if (reader != null) {
                reader.close();
            } else {
                this.e.close();
            }
        }

        @DexIgnore
        public int read(char[] cArr, int i, int i2) throws IOException {
            if (!this.g) {
                Reader reader = this.h;
                if (reader == null) {
                    InputStreamReader inputStreamReader = new InputStreamReader(this.e.m(), jm4.a(this.e, this.f));
                    this.h = inputStreamReader;
                    reader = inputStreamReader;
                }
                return reader.read(cArr, i, i2);
            }
            throw new IOException("Stream closed");
        }
    }

    @DexIgnore
    public static em4 a(am4 am4, String str) {
        Charset charset = jm4.i;
        if (am4 != null) {
            charset = am4.a();
            if (charset == null) {
                charset = jm4.i;
                am4 = am4.b(am4 + "; charset=utf-8");
            }
        }
        jo4 jo4 = new jo4();
        jo4.a(str, charset);
        return a(am4, jo4.B(), jo4);
    }

    @DexIgnore
    public final Reader A() {
        Reader reader = this.e;
        if (reader != null) {
            return reader;
        }
        b bVar = new b(E(), B());
        this.e = bVar;
        return bVar;
    }

    @DexIgnore
    public final Charset B() {
        am4 D = D();
        return D != null ? D.a(jm4.i) : jm4.i;
    }

    @DexIgnore
    public abstract long C();

    @DexIgnore
    public abstract am4 D();

    @DexIgnore
    public abstract lo4 E();

    @DexIgnore
    public final String F() throws IOException {
        lo4 E = E();
        try {
            return E.a(jm4.a(E, B()));
        } finally {
            jm4.a((Closeable) E);
        }
    }

    @DexIgnore
    public void close() {
        jm4.a((Closeable) E());
    }

    @DexIgnore
    public final InputStream y() {
        return E().m();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final byte[] z() throws IOException {
        long C = C();
        if (C <= 2147483647L) {
            lo4 E = E();
            try {
                byte[] f = E.f();
                jm4.a((Closeable) E);
                if (C == -1 || C == ((long) f.length)) {
                    return f;
                }
                throw new IOException("Content-Length (" + C + ") and stream length (" + f.length + ") disagree");
            } catch (Throwable th) {
                jm4.a((Closeable) E);
                throw th;
            }
        } else {
            throw new IOException("Cannot buffer entire body for content length: " + C);
        }
    }

    @DexIgnore
    public static em4 a(am4 am4, byte[] bArr) {
        jo4 jo4 = new jo4();
        jo4.write(bArr);
        return a(am4, (long) bArr.length, jo4);
    }

    @DexIgnore
    public static em4 a(am4 am4, long j, lo4 lo4) {
        if (lo4 != null) {
            return new a(am4, j, lo4);
        }
        throw new NullPointerException("source == null");
    }
}
