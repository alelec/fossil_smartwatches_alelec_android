package com.fossil.blesdk.obfuscated;

import android.content.Context;
import android.os.Bundle;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class dz implements my {
    @DexIgnore
    public static /* final */ List<Class<?>> c; // = Collections.unmodifiableList(Arrays.asList(new Class[]{String.class, String.class, Bundle.class, Long.class}));
    @DexIgnore
    public /* final */ uy a;
    @DexIgnore
    public Object b;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements InvocationHandler {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean a(Object obj, Object obj2) {
            if (obj == obj2) {
                return true;
            }
            if (obj2 == null || !Proxy.isProxyClass(obj2.getClass()) || !super.equals(Proxy.getInvocationHandler(obj2))) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public Object invoke(Object obj, Method method, Object[] objArr) {
            String name = method.getName();
            if (objArr == null) {
                objArr = new Object[0];
            }
            if (objArr.length == 1 && name.equals("equals")) {
                return Boolean.valueOf(a(obj, objArr[0]));
            }
            if (objArr.length == 0 && name.equals("hashCode")) {
                return Integer.valueOf(super.hashCode());
            }
            if (objArr.length == 0 && name.equals("toString")) {
                return super.toString();
            }
            if (objArr.length == 4 && name.equals("onEvent") && dz.a(objArr)) {
                String str = (String) objArr[0];
                String str2 = (String) objArr[1];
                Bundle bundle = (Bundle) objArr[2];
                if (str != null && !str.equals("crash")) {
                    dz.b(dz.this.a, str2, bundle);
                    return null;
                }
            }
            StringBuilder sb = new StringBuilder("Unexpected method invoked on AppMeasurement.EventListener: " + name + "(");
            for (int i = 0; i < objArr.length; i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append(objArr[i].getClass().getName());
            }
            sb.append("); returning null");
            q44.g().e("CrashlyticsCore", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public dz(uy uyVar) {
        this.a = uyVar;
    }

    @DexIgnore
    public synchronized Object b(Class cls) {
        if (this.b == null) {
            this.b = Proxy.newProxyInstance(this.a.l().getClassLoader(), new Class[]{cls}, new a());
        }
        return this.b;
    }

    @DexIgnore
    public boolean register() {
        Class<?> a2 = a("com.google.android.gms.measurement.AppMeasurement");
        if (a2 == null) {
            q44.g().d("CrashlyticsCore", "Firebase Analytics is not present; you will not see automatic logging of events before a crash occurs.");
            return false;
        }
        Object a3 = a(a2);
        if (a3 == null) {
            q44.g().w("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not create an instance of Firebase Analytics.");
            return false;
        }
        Class<?> a4 = a("com.google.android.gms.measurement.AppMeasurement$OnEventListener");
        if (a4 == null) {
            q44.g().w("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Could not get class com.google.android.gms.measurement.AppMeasurement$OnEventListener");
            return false;
        }
        try {
            a2.getDeclaredMethod("registerOnMeasurementEventListener", new Class[]{a4}).invoke(a3, new Object[]{b(a4)});
        } catch (NoSuchMethodException e) {
            q44.g().a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: Method registerOnMeasurementEventListener not found.", (Throwable) e);
            return false;
        } catch (Exception e2) {
            y44 g = q44.g();
            g.a("CrashlyticsCore", "Cannot register AppMeasurement Listener for Crashlytics breadcrumbs: " + e2.getMessage(), (Throwable) e2);
        }
        return true;
    }

    @DexIgnore
    public final Class<?> a(String str) {
        try {
            return this.a.l().getClassLoader().loadClass(str);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final Object a(Class<?> cls) {
        try {
            return cls.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke(cls, new Object[]{this.a.l()});
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object[] objArr) {
        if (objArr.length != c.size()) {
            return false;
        }
        Iterator<Class<?>> it = c.iterator();
        for (Object obj : objArr) {
            if (!obj.getClass().equals(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static void b(uy uyVar, String str, Bundle bundle) {
        try {
            uyVar.a("$A$:" + a(str, bundle));
        } catch (JSONException unused) {
            y44 g = q44.g();
            g.w("CrashlyticsCore", "Unable to serialize Firebase Analytics event; " + str);
        }
    }

    @DexIgnore
    public static String a(String str, Bundle bundle) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        for (String str2 : bundle.keySet()) {
            jSONObject2.put(str2, bundle.get(str2));
        }
        jSONObject.put("name", str);
        jSONObject.put("parameters", jSONObject2);
        return jSONObject.toString();
    }
}
