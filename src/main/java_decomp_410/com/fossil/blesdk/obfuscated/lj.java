package com.fossil.blesdk.obfuscated;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public abstract class lj {
    @DexIgnore
    public static /* final */ String a; // = dj.a("WorkerFactory");

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends lj {
        @DexIgnore
        public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
            return null;
        }
    }

    @DexIgnore
    public static lj a() {
        return new a();
    }

    @DexIgnore
    public abstract ListenableWorker a(Context context, String str, WorkerParameters workerParameters);

    @DexIgnore
    public final ListenableWorker b(Context context, String str, WorkerParameters workerParameters) {
        ListenableWorker listenableWorker;
        ListenableWorker a2 = a(context, str, workerParameters);
        if (a2 == null) {
            Class<? extends U> cls = null;
            try {
                cls = Class.forName(str).asSubclass(ListenableWorker.class);
            } catch (ClassNotFoundException unused) {
                dj a3 = dj.a();
                String str2 = a;
                a3.b(str2, "Class not found: " + str, new Throwable[0]);
            }
            if (cls != null) {
                try {
                    listenableWorker = (ListenableWorker) cls.getDeclaredConstructor(new Class[]{Context.class, WorkerParameters.class}).newInstance(new Object[]{context, workerParameters});
                } catch (Exception e) {
                    dj a4 = dj.a();
                    String str3 = a;
                    a4.b(str3, "Could not instantiate " + str, e);
                }
                if (listenableWorker != null || !listenableWorker.g()) {
                    return listenableWorker;
                }
                throw new IllegalStateException(String.format("WorkerFactory (%s) returned an instance of a ListenableWorker (%s) which has already been invoked. createWorker() must always return a new instance of a ListenableWorker.", new Object[]{getClass().getName(), str}));
            }
        }
        listenableWorker = a2;
        if (listenableWorker != null) {
        }
        return listenableWorker;
    }
}
