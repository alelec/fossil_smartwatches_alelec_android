package com.fossil.blesdk.obfuscated;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class n31 extends s31 {
    @DexIgnore
    public /* final */ ue0<Status> e;

    @DexIgnore
    public n31(ue0<Status> ue0) {
        this.e = ue0;
    }

    @DexIgnore
    public final void a(o31 o31) {
        this.e.a(o31.G());
    }
}
