package com.fossil.blesdk.obfuscated;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import com.fossil.blesdk.obfuscated.i62;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.ContactWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class sx2 extends i62<b, c, i62.a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.b {
        @DexIgnore
        public /* final */ List<ContactWrapper> a;
        @DexIgnore
        public /* final */ List<ContactWrapper> b;

        @DexIgnore
        public b(List<ContactWrapper> list, List<ContactWrapper> list2) {
            kd4.b(list, "contactWrapperListRemoved");
            kd4.b(list2, "contactWrapperListAdded");
            this.a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<ContactWrapper> a() {
            return this.b;
        }

        @DexIgnore
        public final List<ContactWrapper> b() {
            return this.a;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.c {
        @DexIgnore
        public c(boolean z) {
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = sx2.class.getSimpleName();
        kd4.a((Object) simpleName, "SaveContactGroupsNotific\u2026on::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public sx2(NotificationsRepository notificationsRepository) {
        kd4.b(notificationsRepository, "notificationsRepository");
        st1.a(notificationsRepository, "notificationsRepository cannot be null!", new Object[0]);
        kd4.a((Object) notificationsRepository, "Preconditions.checkNotNu\u2026ository cannot be null!\")");
        this.d = notificationsRepository;
    }

    @DexIgnore
    public final void b(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact removePhoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(removePhoneFavoritesContact);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0274 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0185  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01cd  */
    public final void c(List<ContactWrapper> list) {
        Iterator<ContactWrapper> it;
        Contact contact;
        if (!list.isEmpty()) {
            List<ContactGroup> allContactGroups = this.d.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ContactProvider b2 = dn2.p.a().b();
            Iterator<ContactWrapper> it2 = list.iterator();
            while (it2.hasNext()) {
                ContactWrapper next = it2.next();
                if (allContactGroups != null) {
                    Iterator<ContactGroup> it3 = allContactGroups.iterator();
                    boolean z = false;
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        ContactGroup next2 = it3.next();
                        kd4.a((Object) next2, "contactGroup");
                        Iterator<Contact> it4 = next2.getContacts().iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                it = it2;
                                break;
                            }
                            Contact next3 = it4.next();
                            if (!(next.getContact() == null || next3 == null)) {
                                int contactId = next3.getContactId();
                                Contact contact2 = next.getContact();
                                if (contact2 != null && contactId == contact2.getContactId()) {
                                    Contact contact3 = next.getContact();
                                    Boolean valueOf = contact3 != null ? Boolean.valueOf(contact3.isUseCall()) : null;
                                    if (valueOf != null) {
                                        next3.setUseCall(valueOf.booleanValue());
                                        Contact contact4 = next.getContact();
                                        Boolean valueOf2 = contact4 != null ? Boolean.valueOf(contact4.isUseSms()) : null;
                                        if (valueOf2 != null) {
                                            next3.setUseSms(valueOf2.booleanValue());
                                            Contact contact5 = next.getContact();
                                            next3.setFirstName(contact5 != null ? contact5.getFirstName() : null);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String str = e;
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("Contact Id = ");
                                            Contact contact6 = next.getContact();
                                            sb.append(contact6 != null ? Integer.valueOf(contact6.getContactId()) : null);
                                            sb.append(", ");
                                            it = it2;
                                            sb.append("Contact name = ");
                                            Contact contact7 = next.getContact();
                                            sb.append(contact7 != null ? contact7.getFirstName() : null);
                                            sb.append(',');
                                            sb.append("Contact db row = ");
                                            Contact contact8 = next.getContact();
                                            sb.append(contact8 != null ? Integer.valueOf(contact8.getDbRowId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact phone = ");
                                            sb.append(next.getPhoneNumber());
                                            local.d(str, sb.toString());
                                            next.setContact(next3);
                                            b2.removeContactGroup(next2);
                                            allContactGroups.remove(next2);
                                            z = true;
                                        } else {
                                            kd4.a();
                                            throw null;
                                        }
                                    } else {
                                        kd4.a();
                                        throw null;
                                    }
                                }
                            }
                            it2 = it2;
                        }
                        if (z) {
                            break;
                        }
                        it2 = it;
                    }
                    ContactGroup contactGroup = new ContactGroup();
                    contactGroup.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                    contactGroup.setHour(next.getCurrentHandGroup());
                    arrayList2.add(contactGroup);
                    contact = next.getContact();
                    if (contact != null) {
                        contact.setContactGroup(contactGroup);
                    }
                    if (contact != null) {
                        Contact contact9 = next.getContact();
                        Boolean valueOf3 = contact9 != null ? Boolean.valueOf(contact9.isUseCall()) : null;
                        if (valueOf3 != null) {
                            contact.setUseCall(valueOf3.booleanValue());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    if (contact != null) {
                        Contact contact10 = next.getContact();
                        Boolean valueOf4 = contact10 != null ? Boolean.valueOf(contact10.isUseSms()) : null;
                        if (valueOf4 != null) {
                            contact.setUseSms(valueOf4.booleanValue());
                        } else {
                            kd4.a();
                            throw null;
                        }
                    }
                    if (contact != null) {
                        contact.setUseEmail(false);
                    }
                    if (contact == null) {
                        arrayList.add(contact);
                        ContentResolver contentResolver = PortfolioApp.W.c().getContentResolver();
                        if (next.hasPhoneNumber()) {
                            Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, "contact_id=" + contact.getContactId(), (String[]) null, (String) null);
                            if (query != null) {
                                while (query.moveToNext()) {
                                    try {
                                        PhoneNumber phoneNumber = new PhoneNumber();
                                        phoneNumber.setNumber(query.getString(query.getColumnIndex("data1")));
                                        phoneNumber.setContact(contact);
                                        if (!ts3.a((List<PhoneNumber>) arrayList3, phoneNumber).booleanValue()) {
                                            arrayList3.add(phoneNumber);
                                        }
                                        if (next.isFavorites()) {
                                            arrayList4.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                        }
                                    } catch (Exception e2) {
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String str2 = e;
                                        local2.e(str2, "Error Inside " + e + ".saveContactToFSL - ex=" + e2);
                                    } catch (Throwable th) {
                                        query.close();
                                        throw th;
                                    }
                                }
                                query.close();
                            }
                        }
                        it2 = it;
                    } else {
                        kd4.a();
                        throw null;
                    }
                }
                it = it2;
                ContactGroup contactGroup2 = new ContactGroup();
                contactGroup2.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                contactGroup2.setHour(next.getCurrentHandGroup());
                arrayList2.add(contactGroup2);
                contact = next.getContact();
                if (contact != null) {
                }
                if (contact != null) {
                }
                if (contact != null) {
                }
                if (contact != null) {
                }
                if (contact == null) {
                }
            }
            this.d.saveContactGroupList(arrayList2);
            this.d.saveListContact(arrayList);
            this.d.saveListPhoneNumber(arrayList3);
            d(arrayList4);
        }
    }

    @DexIgnore
    public final void d(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact savePhoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(savePhoneFavoritesContact);
        }
    }

    @DexIgnore
    public void a(b bVar) {
        kd4.b(bVar, "requestValues");
        a(bVar.b());
        c(bVar.a());
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveContactGroupsNotification done");
        a().onSuccess(new c(true));
    }

    @DexIgnore
    public final void a(List<ContactWrapper> list) {
        if (!list.isEmpty()) {
            ContactProvider b2 = dn2.p.a().b();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (ContactWrapper contact : list) {
                Contact contact2 = contact.getContact();
                if (contact2 != null) {
                    arrayList2.add(contact2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    local.d(str, "Removed contact = " + contact2.getFirstName() + " row id = " + contact2.getDbRowId());
                    for (ContactGroup next : b2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue())) {
                        kd4.a((Object) next, "contactGroupItem");
                        Iterator<Contact> it = next.getContacts().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Contact next2 = it.next();
                            int contactId = contact2.getContactId();
                            kd4.a((Object) next2, "contactItem");
                            if (contactId == next2.getContactId()) {
                                contact2.setDbRowId(next2.getDbRowId());
                                arrayList.add(next);
                                break;
                            }
                        }
                    }
                    for (PhoneNumber next3 : contact2.getPhoneNumbers()) {
                        kd4.a((Object) next3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                        arrayList3.add(new PhoneFavoritesContact(next3.getNumber()));
                    }
                } else {
                    kd4.a();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            b(arrayList3);
        }
    }
}
