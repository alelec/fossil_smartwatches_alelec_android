package com.fossil.blesdk.obfuscated;

import android.os.Build;
import androidx.work.NetworkType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class yi {
    @DexIgnore
    public static /* final */ yi i; // = new a().a();
    @DexIgnore
    public NetworkType a; // = NetworkType.NOT_REQUIRED;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public long f; // = -1;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public zi h; // = new zi();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public NetworkType c; // = NetworkType.NOT_REQUIRED;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public zi h; // = new zi();

        @DexIgnore
        public a a(NetworkType networkType) {
            this.c = networkType;
            return this;
        }

        @DexIgnore
        public yi a() {
            return new yi(this);
        }
    }

    @DexIgnore
    public yi() {
    }

    @DexIgnore
    public void a(NetworkType networkType) {
        this.a = networkType;
    }

    @DexIgnore
    public NetworkType b() {
        return this.a;
    }

    @DexIgnore
    public void c(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public void d(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public boolean e() {
        return this.h.b() > 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || yi.class != obj.getClass()) {
            return false;
        }
        yi yiVar = (yi) obj;
        if (this.b == yiVar.b && this.c == yiVar.c && this.d == yiVar.d && this.e == yiVar.e && this.f == yiVar.f && this.g == yiVar.g && this.a == yiVar.a) {
            return this.h.equals(yiVar.h);
        }
        return false;
    }

    @DexIgnore
    public boolean f() {
        return this.d;
    }

    @DexIgnore
    public boolean g() {
        return this.b;
    }

    @DexIgnore
    public boolean h() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.f;
        long j2 = this.g;
        return (((((((((((((this.a.hashCode() * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + ((int) (j ^ (j >>> 32)))) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + this.h.hashCode();
    }

    @DexIgnore
    public boolean i() {
        return this.e;
    }

    @DexIgnore
    public void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public void b(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public long c() {
        return this.f;
    }

    @DexIgnore
    public long d() {
        return this.g;
    }

    @DexIgnore
    public void a(long j) {
        this.f = j;
    }

    @DexIgnore
    public void b(long j) {
        this.g = j;
    }

    @DexIgnore
    public void a(zi ziVar) {
        this.h = ziVar;
    }

    @DexIgnore
    public zi a() {
        return this.h;
    }

    @DexIgnore
    public yi(a aVar) {
        this.b = aVar.a;
        this.c = Build.VERSION.SDK_INT >= 23 && aVar.b;
        this.a = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        if (Build.VERSION.SDK_INT >= 24) {
            this.h = aVar.h;
            this.f = aVar.f;
            this.g = aVar.g;
        }
    }

    @DexIgnore
    public yi(yi yiVar) {
        this.b = yiVar.b;
        this.c = yiVar.c;
        this.a = yiVar.a;
        this.d = yiVar.d;
        this.e = yiVar.e;
        this.h = yiVar.h;
    }
}
