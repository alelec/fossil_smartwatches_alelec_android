package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.device.DeviceInformation;
import com.fossil.blesdk.device.asyncevent.AsyncEvent;
import com.fossil.blesdk.device.core.Peripheral;
import com.fossil.blesdk.device.core.command.BluetoothCommand;
import com.fossil.blesdk.device.core.gatt.GattCharacteristic;
import com.fossil.blesdk.device.logic.request.Request;
import com.fossil.blesdk.device.logic.request.RequestId;
import com.fossil.blesdk.log.sdklog.EventType;
import com.fossil.blesdk.log.sdklog.SdkLogEntry;
import com.fossil.blesdk.setting.JSONKey;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class h70 extends Request {
    @DexIgnore
    public xc4<? super AsyncEvent, qa4> A;
    @DexIgnore
    public long B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h70(Peripheral peripheral) {
        super(RequestId.STREAMING, peripheral, 0, 4, (fd4) null);
        kd4.b(peripheral, "peripheral");
    }

    @DexIgnore
    public void a(long j) {
        this.B = j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0058, code lost:
        if (r4 != null) goto L_0x005d;
     */
    @DexIgnore
    public void b(c20 c20) {
        Object obj;
        kd4.b(c20, "characteristicChangedNotification");
        if (c20.a() == GattCharacteristic.CharacteristicId.ASYNC) {
            AsyncEvent a = d30.b.a(i().k(), c20.b());
            da0 da0 = da0.l;
            EventType eventType = EventType.RESPONSE;
            String k = i().k();
            String j = j();
            String k2 = k();
            JSONObject a2 = wa0.a(new JSONObject(), JSONKey.RAW_DATA, k90.a(c20.b(), (String) null, 1, (Object) null));
            JSONKey jSONKey = JSONKey.DEVICE_EVENT;
            if (a != null) {
                obj = a.toJSONObject();
            }
            obj = JSONObject.NULL;
            AsyncEvent asyncEvent = a;
            SdkLogEntry sdkLogEntry = r3;
            SdkLogEntry sdkLogEntry2 = new SdkLogEntry("streaming", eventType, k, j, k2, true, (String) null, (DeviceInformation) null, (ea0) null, wa0.a(a2, jSONKey, obj), 448, (fd4) null);
            da0.b(sdkLogEntry);
            if (asyncEvent != null) {
                xc4<? super AsyncEvent, qa4> xc4 = this.A;
                if (xc4 != null) {
                    qa4 invoke = xc4.invoke(asyncEvent);
                    return;
                }
                return;
            }
        }
    }

    @DexIgnore
    public final h70 d(xc4<? super AsyncEvent, qa4> xc4) {
        kd4.b(xc4, "actionOnEventReceived");
        this.A = xc4;
        return this;
    }

    @DexIgnore
    public BluetoothCommand h() {
        return null;
    }

    @DexIgnore
    public long m() {
        return this.B;
    }

    @DexIgnore
    public void s() {
    }

    @DexIgnore
    public void a(d20 d20) {
        kd4.b(d20, "connectionStateChangedNotification");
        if (d20.a() == Peripheral.State.DISCONNECTED) {
            d30.b.a(i().k());
        }
    }
}
