package com.fossil.blesdk.obfuscated;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface px3 {
    @DexIgnore
    public static final px3 a = new a();
    @DexIgnore
    public static final px3 b = new b();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements px3 {
        @DexIgnore
        public void a(ix3 ix3) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements px3 {
        @DexIgnore
        public void a(ix3 ix3) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new IllegalStateException("Event bus " + ix3 + " accessed from non-main thread " + Looper.myLooper());
            }
        }
    }

    @DexIgnore
    void a(ix3 ix3);
}
