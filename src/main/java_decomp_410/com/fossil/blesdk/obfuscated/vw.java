package com.fossil.blesdk.obfuscated;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class vw {
    @DexIgnore
    public static /* final */ g<Object> a; // = new a();

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g<Object> {
        @DexIgnore
        public void a(Object obj) {
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements d<List<T>> {
        @DexIgnore
        public List<T> a() {
            return new ArrayList();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements g<List<T>> {
        @DexIgnore
        public void a(List<T> list) {
            list.clear();
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        T a();
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        xw i();
    }

    @DexIgnore
    public interface g<T> {
        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    public static <T extends f> g8<T> a(int i, d<T> dVar) {
        return a(new i8(i), dVar);
    }

    @DexIgnore
    public static <T> g8<List<T>> b() {
        return a(20);
    }

    @DexIgnore
    public static <T> g8<List<T>> a(int i) {
        return a(new i8(i), new b(), new c());
    }

    @DexIgnore
    public static <T extends f> g8<T> a(g8<T> g8Var, d<T> dVar) {
        return a(g8Var, dVar, a());
    }

    @DexIgnore
    public static <T> g8<T> a(g8<T> g8Var, d<T> dVar, g<T> gVar) {
        return new e(g8Var, dVar, gVar);
    }

    @DexIgnore
    public static <T> g<T> a() {
        return a;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements g8<T> {
        @DexIgnore
        public /* final */ d<T> a;
        @DexIgnore
        public /* final */ g<T> b;
        @DexIgnore
        public /* final */ g8<T> c;

        @DexIgnore
        public e(g8<T> g8Var, d<T> dVar, g<T> gVar) {
            this.c = g8Var;
            this.a = dVar;
            this.b = gVar;
        }

        @DexIgnore
        public T a() {
            T a2 = this.c.a();
            if (a2 == null) {
                a2 = this.a.a();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + a2.getClass());
                }
            }
            if (a2 instanceof f) {
                ((f) a2).i().a(false);
            }
            return a2;
        }

        @DexIgnore
        public boolean a(T t) {
            if (t instanceof f) {
                ((f) t).i().a(true);
            }
            this.b.a(t);
            return this.c.a(t);
        }
    }
}
