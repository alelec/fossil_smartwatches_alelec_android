package com.fossil.blesdk.obfuscated;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
/* renamed from: com.fossil.blesdk.obfuscated.j6 */
public final class C2107j6 {
    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v1, resolved type: android.os.CancellationSignal} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v2, resolved type: android.os.CancellationSignal} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v3, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v4, resolved type: android.os.CancellationSignal} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    public static android.database.Cursor m8847a(android.content.ContentResolver contentResolver, android.net.Uri uri, java.lang.String[] strArr, java.lang.String str, java.lang.String[] strArr2, java.lang.String str2, com.fossil.blesdk.obfuscated.C2379m7 m7Var) {
        android.os.CancellationSignal cancellationSignal;
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            if (m7Var != null) {
                try {
                    cancellationSignal = m7Var.mo13524b();
                } catch (java.lang.Exception e) {
                    if (e instanceof android.os.OperationCanceledException) {
                        throw new androidx.core.p001os.OperationCanceledException();
                    }
                    throw e;
                }
            } else {
                cancellationSignal = null;
            }
            return contentResolver.query(uri, strArr, str, strArr2, str2, cancellationSignal);
        }
        if (m7Var != null) {
            m7Var.mo13526d();
        }
        return contentResolver.query(uri, strArr, str, strArr2, str2);
    }
}
