package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.o81;
import com.google.android.gms.internal.measurement.zzxs;
import com.google.android.gms.internal.measurement.zzxx;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public interface o81<T extends o81<T>> extends Comparable<T> {
    @DexIgnore
    da1 a(da1 da1, da1 da12);

    @DexIgnore
    x91 a(x91 x91, w91 w91);

    @DexIgnore
    boolean e();

    @DexIgnore
    zzxs f();

    @DexIgnore
    boolean g();

    @DexIgnore
    zzxx h();

    @DexIgnore
    int zzc();
}
