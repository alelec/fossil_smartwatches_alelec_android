package com.fossil.blesdk.obfuscated;

import com.fossil.blesdk.obfuscated.i62;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class t03 extends i62<c, d, b> {
    @DexIgnore
    public static /* final */ String d;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements i62.a {
        @DexIgnore
        public b(String str) {
            kd4.b(str, "message");
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements i62.b {
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements i62.c {
        @DexIgnore
        public /* final */ List<ContactGroup> a;

        @DexIgnore
        public d(List<? extends ContactGroup> list) {
            kd4.b(list, "contactGroups");
            this.a = list;
        }

        @DexIgnore
        public final List<ContactGroup> a() {
            return this.a;
        }
    }

    /*
    static {
        new a((fd4) null);
        String simpleName = t03.class.getSimpleName();
        kd4.a((Object) simpleName, "GetAllHybridContactGroups::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public void a(c cVar) {
        FLogger.INSTANCE.getLocal().d(d, "executeUseCase");
        List<ContactGroup> allContactGroups = dn2.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
        if (allContactGroups != null) {
            a().onSuccess(new d(allContactGroups));
        } else {
            a().a(new b("Get all contact group failed"));
        }
    }
}
