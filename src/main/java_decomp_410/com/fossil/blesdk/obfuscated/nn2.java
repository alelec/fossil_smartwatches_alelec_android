package com.fossil.blesdk.obfuscated;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.blesdk.obfuscated.ir3;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class nn2 extends BroadcastReceiver {
    @DexIgnore
    public en2 a;
    @DexIgnore
    public ir3 b;
    @DexIgnore
    public AlarmHelper c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(fd4 fd4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.e<ir3.d, ir3.c> {
        @DexIgnore
        public /* final */ /* synthetic */ nn2 a;

        @DexIgnore
        public b(nn2 nn2) {
            this.a = nn2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ir3.d dVar) {
            kd4.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "Re-schedule SyncDataReminder");
            this.a.a().e();
        }

        @DexIgnore
        public void a(ir3.c cVar) {
            kd4.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", cVar.toString());
        }
    }

    /*
    static {
        new a((fd4) null);
    }
    */

    @DexIgnore
    public nn2() {
        PortfolioApp.W.c().g().a(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.c;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        kd4.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("TimeTickReceiver", "onReceive() - action = " + action);
        en2 en2 = this.a;
        if (en2 != null) {
            long g = en2.g(PortfolioApp.W.c().e());
            if (!TextUtils.isEmpty(action) && kd4.a((Object) action, (Object) "android.intent.action.TIME_TICK") && System.currentTimeMillis() - g <= ScanService.BLE_SCAN_TIMEOUT) {
                FossilNotificationBar.Companion companion = FossilNotificationBar.c;
                if (context != null) {
                    companion.a(context);
                } else {
                    kd4.a();
                    throw null;
                }
            }
            if (kd4.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
                TimeZone timeZone = TimeZone.getDefault();
                try {
                    Calendar instance = Calendar.getInstance();
                    kd4.a((Object) instance, "calendar");
                    Calendar instance2 = Calendar.getInstance();
                    kd4.a((Object) instance2, "Calendar.getInstance()");
                    instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                    Calendar instance3 = Calendar.getInstance();
                    kd4.a((Object) instance3, "Calendar.getInstance()");
                    if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                        FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "DST Changed.");
                        ir3 ir3 = this.b;
                        if (ir3 != null) {
                            ir3.a(new ir3.b(), new b(this));
                        } else {
                            kd4.d("mDstChangeUseCase");
                            throw null;
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("TimeTickReceiver", ".timeZoneChangeReceiver - ex=" + e);
                }
            }
        } else {
            kd4.d("mSharedPreferencesManager");
            throw null;
        }
    }
}
