package com.fossil.blesdk.obfuscated;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public class cu3 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static /* final */ String d; // = "cu3";
    @DexIgnore
    public /* final */ FragmentManager a;
    @DexIgnore
    public /* final */ List<Fragment> b;
    @DexIgnore
    public int[] c;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public a(cu3 cu3, View view) {
            super(view);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder e;

        @DexIgnore
        public b(RecyclerView.ViewHolder viewHolder) {
            this.e = viewHolder;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            this.e.itemView.removeOnAttachStateChangeListener(this);
            zr2 zr2 = (zr2) cu3.this.b.get(this.e.getAdapterPosition());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = cu3.d;
            local.d(str, "onBindViewHolder tag=" + zr2.R0() + " childFragmentManager=" + cu3.this.a + "this=" + this);
            Fragment a = cu3.this.a.a(zr2.R0());
            if (a != null) {
                bb a2 = cu3.this.a.a();
                a2.d(a);
                a2.d();
            }
            bb a3 = cu3.this.a.a();
            a3.a(view.getId(), zr2, zr2.R0());
            a3.d();
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public cu3(FragmentManager fragmentManager, List<Fragment> list) {
        this.a = fragmentManager;
        this.b = list;
    }

    @DexIgnore
    public int getItemCount() {
        int size = this.b.size();
        int[] iArr = this.c;
        if (iArr == null || size != iArr.length) {
            this.c = new int[size];
        }
        return size;
    }

    @DexIgnore
    public long getItemId(int i) {
        this.c[i] = this.b.get(i).getId();
        int[] iArr = this.c;
        if (iArr[i] == 0) {
            iArr[i] = View.generateViewId();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = d;
            local.d(str, "getItemId: position = " + i + ", id = " + this.c[i]);
        }
        return (long) this.c[i];
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "onBindViewHolder: position = " + i);
        Fragment a2 = this.a.a(viewHolder.itemView.getId());
        if (a2 != null) {
            bb a3 = this.a.a();
            a3.d(a2);
            a3.d();
        }
        viewHolder.itemView.setId((int) getItemId(i));
        viewHolder.itemView.addOnAttachStateChangeListener(new b(viewHolder));
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        return new a(this, frameLayout);
    }
}
